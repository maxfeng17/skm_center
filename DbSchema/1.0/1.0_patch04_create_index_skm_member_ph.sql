
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[skm_member_ph]') AND name = N'ix_skm_member_ph_with_skm_token') 
Begin
	CREATE NONCLUSTERED INDEX [ix_skm_member_ph_with_skm_token] ON [skm_member_ph]
	(
		[skm_token] ASC
	)
    INCLUDE(create_time,ph1,ph2,ph3)
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
End



IF not exists(
Select * From sys.columns
Where object_id =(Select object_id From sys.objects where name='external_deal')
And name='not_verified_flag') Begin

   alter table external_deal
add not_verified_flag bit

End
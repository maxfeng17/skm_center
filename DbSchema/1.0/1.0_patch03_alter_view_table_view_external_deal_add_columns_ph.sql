USE [lunchking_center]
GO

/****** Object:  View [dbo].[view_external_deal]    Script Date: 2020/1/10 �U�� 03:18:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[view_external_deal]
AS
SELECT          deal.Guid, deal.active_type, deal.title, deal.product_code, deal.item_orig_price, deal.discount_type, deal.discount, 
                            deal.order_total_limit, deal.description, deal.introduction, deal.category_list, deal.tag_list, 
                            deal.business_hour_order_time_s, deal.business_hour_order_time_e, deal.business_hour_deliver_time_s, 
                            deal.business_hour_deliver_time_e, deal.settlement_time, deal.beacon_type, deal.beacon_message, deal.status, 
                            deal.create_id, deal.create_time, deal.modify_id, deal.modify_time, deal.come_from_type, deal.item_no, deal.remark, 
                            deal.special_item_no, deal.image_path, deal.app_deal_pic, deal.buy, deal.free, sr.GUID AS seller_guid, 
                            st.GUID AS store_guid, rs.bid, sr.seller_name, st.seller_name AS store_name, shoppe.brand_counter_code, 
                            shoppe.brand_counter_name, shoppe.shop_code, shoppe.shop_name, shoppe.skm_store_id, shoppe.floor, 
                            shoppe.is_shoppe, shoppe.is_available, rs.is_del, rs.parent_seller_guid, deal.deal_version, deal.is_prize_deal, 
                            deal.is_hq_deal, deal.deal_icon_id, deal.is_crm_deal, deal.is_skm_pay, deal.skm_pay_burning_point, 
                            deal.excluding_tax_price, deal.tax_rate, deal.is_repeat_purchase, deal.skm_orig_price, deal.repeat_purchase_type, 
                            deal.repeat_purchase_count, deal.delivery_type, deal.delivery_product_code, deal.enable_install,deal.ph
FROM              dbo.external_deal AS deal WITH (nolock) INNER JOIN
                            dbo.external_deal_relation_store AS rs WITH (nolock) ON deal.Guid = rs.deal_guid INNER JOIN
                            dbo.skm_shoppe AS shoppe WITH (nolock) ON rs.store_guid = shoppe.store_guid INNER JOIN
                            dbo.seller AS st WITH (nolock) ON rs.store_guid = st.GUID INNER JOIN
                            dbo.seller AS sr WITH (nolock) ON sr.GUID = rs.seller_guid
GO



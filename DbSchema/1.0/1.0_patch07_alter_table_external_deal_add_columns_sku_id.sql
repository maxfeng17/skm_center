IF not exists(
Select * From sys.columns
Where object_id =(Select object_id From sys.objects where name='external_deal')
And name='sku_id') Begin

    ALTER TABLE dbo.external_deal ADD
	sku_id varchar(50) NULL

End

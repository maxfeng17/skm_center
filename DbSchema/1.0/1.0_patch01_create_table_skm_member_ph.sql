


if not exists(select * from sys.tables where name='skm_member_ph')
begin
	CREATE TABLE [dbo].[skm_member_ph](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[skm_token] [nvarchar](50) NOT NULL,
		[ph1] [varchar](2) NULL,
		[ph2] [varchar](2) NULL,
		[ph3] [varchar](2) NULL,
		[create_time] [datetime] NULL,
	 CONSTRAINT [PK_skm_member_ph] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
end



﻿using System.ComponentModel.Composition;
using Autofac;
using LunchKingSite.BizLogic.Component;
using Vodka.Container;

namespace LunchKingSite.BizLogic
{
    [Export(typeof(IModuleRegistrar))]
    public class BizLogicModule : IModuleRegistrar
    {
        public static IContainer Container { get; private set; }

        public void RegisterWithContainer(ContainerBuilder builder)
        {
            builder.RegisterType<Component.CpaService>().AsSelf().AsImplementedInterfaces().InstancePerDependency();
            builder.RegisterType<PushService>().As<PushService>()
              .Named<PushService>(PushService._PERSON_PUSH_SERVICE)
              .SingleInstance();
        }

        public void Initialize(IContainer container)
        {
            Container = container;
        }
    }
}
﻿using System;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic
{
    public class WebUtility
    {
        public static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public static ISysConfProvider SystemConfig
        {
            get
            {
                return cp;
            }
        }

        public static string GetSiteRoot()
        {
            return GetSiteRoot(false);
        }

        public static string GetSiteRoot(bool forceSecure)
        {
            return ProviderFactory.Instance().GetConfig().SiteUrl;
            string protocol = forceSecure ? "1" : HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
            {
                protocol = "http://";
            }
            else
            {
                protocol = "https://";
            }

            string siteRoot = (string)HttpContext.Current.Cache["SiteRoot"];
            if (string.IsNullOrEmpty(siteRoot))
            {
                string port = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];

                if (port == null || port == "80" || port == "443" || port == "7777") //port 7777 is because of our SIT environment
                {
                    port = string.Empty;
                }
                else
                {
                    port = ":" + port;
                }

                ISysConfProvider config = ProviderFactory.Instance().GetConfig();
                siteRoot = config.SiteUrl.Replace("http://", string.Empty) + port;
                HttpContext.Current.Cache["SiteRoot"] = siteRoot;
            }

            return protocol + siteRoot;
        }

    }
}

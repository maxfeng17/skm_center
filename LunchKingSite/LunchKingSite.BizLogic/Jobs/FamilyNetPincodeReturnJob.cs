﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Models.QueueModels;

namespace LunchKingSite.BizLogic.Jobs
{
    public class FamilyNetPincodeReturnJob : IJob
    {
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public void Execute(JobSetting js)
        {
            if (DateTime.Now.IsBetween(config.FamilyNetReturnApiStopTimeS, config.FamilyNetReturnApiStopTimeE))
            {
                return;
            }

            //補退卡在退貨中的pincode
            FamiGroupCoupon.ExecutePincodeReturnRetry();

            //檔次兌換截止後(非結檔)註銷過期的pincode

            if (config.FamilyNetExpiredPinAutoReturn)
            {
                //撈出訂單
                var list = sp.BusinessHourGetFamiExpiredDeal(config.FamilyExpiredReturnDay);
                //MQ
                IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
                foreach (var o in list)
                {
                    var famiMq = new FamiReturnPincodeMq
                    {
                        OrderGuid = o.Key,
                        Bid = o.Value
                    };
                    mqp.Send(FamiReturnPincodeMq._QUEUE_NAME, famiMq);
                }
            }

            //todo:寄發補退完成的信、log
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System;

namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary> 每日上傳銷售總額資訊 </summary>
    public class SkmPayPreDayClearPosJob : IJob
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(SkmPayPreDayClearPosJob));
        private static readonly ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public SkmPayPreDayClearPosJob()
        {

        }
        public void Execute(JobSetting js)
        {
            try
            {
                DateTime execDate = new DateTime();
                if (string.IsNullOrEmpty(config.SkmPayPreDayClearPosExecDate) ||
                    !DateTime.TryParse(config.SkmPayPreDayClearPosExecDate, out execDate))
                {
                    execDate = DateTime.Now.AddDays(-1);
                }
                SkmFacade.SkmPayClearPosByDate(execDate);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
    }
}

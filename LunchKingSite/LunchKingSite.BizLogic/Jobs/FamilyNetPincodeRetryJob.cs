﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class FamilyNetPincodeRetryJob : IJob
    {
        public void Execute(JobSetting js)
        {
            FamiGroupCoupon.ExecutePincodeOrderRetry();
        }
    }
}

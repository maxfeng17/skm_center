﻿using System;
using System.Net.Mail;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Jobs
{
    public class EventPromoJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(EventPromoJob));
        private static ISysConfProvider _config;
        private static EventPromoJob _theJob;

        static EventPromoJob()
        {
            _theJob = new EventPromoJob();
        }

        public static EventPromoJob Instance()
        {
            return _theJob;
        }

        private EventPromoJob()
        {
            _config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                string eventCode = "2013_SKM"; // 新光商圈專案
                DateTime dateStart = new DateTime(2013,9,26);
                DateTime dateEnd = DateTime.Now.Date;
                PromotionFacade.GenerateEventPromoItemByPeriod(eventCode, dateStart, dateEnd);
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(_config.AdminEmail);
                msg.Subject = "EventPromoJob Error";
                msg.From = new MailAddress(_config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error("EventPromoJob Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
 }

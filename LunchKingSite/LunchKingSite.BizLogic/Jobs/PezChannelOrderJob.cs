﻿using System;
using System.Net.Mail;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Jobs
{
    public class PezShoppingGuideOrderJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(PezShoppingGuideOrderJob));
        private static ISysConfProvider config;
        private static PezShoppingGuideOrderJob theJob;

        static PezShoppingGuideOrderJob()
        {
            theJob = new PezShoppingGuideOrderJob();
        }

        public static PezShoppingGuideOrderJob Instance()
        {
            return theJob;
        }

        private PezShoppingGuideOrderJob()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                if (config.EnablePezChannel)
                {
                    DateTime dateStart = DateTime.Now.AddDays(-1).Date.SetTime(0, 0, 0);
                    DateTime dateEnd = DateTime.Now;

                    OrderFacade.PezChannelOrderAPI(dateStart, dateEnd);
                }
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = "PezShoppingGuideOrderJob Error";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = false;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error("PezShoppingGuideOrderJob Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
 }

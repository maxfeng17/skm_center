﻿using System;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class CustomerServiceTransferNoReplyJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(CustomerServiceTransferNoReplyJob));
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        public void Execute(JobSetting js)
        {
            try
            {
                var now = DateTime.Now;

                //24小時已讀未回
                var baseDate = now.AddHours(-config.TransferNoReplyWithinHours);
                int minute = now.Minute < 30 ? 0 : 30;
                DateTime endTime = new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, baseDate.Hour, minute, 0);

                ////30分已讀未回
                //int minute = now.Minute < 30 ? 0 : 30;
                //DateTime endTime = new DateTime(now.Year, now.Month, now.Day, now.Hour, minute, 0);
                
                DateTime startTime = endTime.AddMinutes(-30);
                int notifyCount = EmailFacade.SendTransferNoReplyToCs(startTime, endTime);
                pp.ChangeLogInsert("CustomerServiceTransferNoReplyJob", "",
                    startTime.ToString("yyyy-MM-dd HH:mm") + "~" + endTime.ToString("yyyy-MM-dd HH:mm") +
                    " Job 寄出廠商已讀未回通知共: " + notifyCount + " 筆");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace LunchKingSite.BizLogic.Jobs
{
    class GoogleAnalyticsJob : IJob
    {
        private static ISysConfProvider config;
        private static IMarketingProvider mkp;
        private static ILog logger = LogManager.GetLogger(typeof(GoogleAnalyticsJob));
        private delegate void JobDelegate(ref List<ExecuteResult> result);

        static GoogleAnalyticsJob()
        {
            config = ProviderFactory.Instance().GetConfig();
            mkp = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
        }

        public void Execute(Core.Configuration.JobSetting js)
        {
            List<ExecuteResult> resultInfos = new List<ExecuteResult>();

            JobDelegate j = delegate { };
            string mode = js.Parameters["mode"] != null ? js.Parameters["mode"] : "";
            //執行要按照順序
            switch (mode)
            {
                case "DailyCheck":
                    j = GaResultCheck;
                    break;
            }

            if (j == null)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = "執行JOB參數錯誤",
                    IsSuccess = false,
                    Message = string.Format("JOB參數錯誤：{0}", mode)
                };

                SendResultMail(config.AdminEmail, new List<ExecuteResult> { errMsg }, "PCPCheckExpiration Error " + mode);
                return;
            }

            try
            {
                j(ref resultInfos);
            }
            catch (Exception ex)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = "執行JOB錯誤",
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };

                SendResultMail(config.AdminEmail, new List<ExecuteResult> { errMsg }, "PCPCheckExpiration Error " + mode);
                throw;
            }

            try
            {
                DateTime d = DateTime.Today.AddDays(-1);
                AnalyticsFacade.GetGaBySite(GaViewIDMap.Lunchking_17LIFE, d);
                AnalyticsFacade.GetGaBySite(GaViewIDMap.Zoo_17LIFE_APP, d);
                //AnalyticsFacade.GetGaByList(d);
                //AnalyticsFacade.GetGaByOnlineDeal(d);
            }
            catch (Exception error)
            {
                LogManager.GetLogger("LogToDb").Error("GA Error!" + error.Message);
            }
            //SendResultMail(config.AdminEmail, resultInfos);
        }

        private void GaResultCheck(ref List<ExecuteResult> result)
        {
            foreach (var item in Enum.GetValues(typeof(GaViewIDMap)))
            {
                AnalyticsFacade.GaUpdate(((int)item).ToString()
                        , string.Format("{0:yyyy-MM-dd}", DateTime.Now.Date.AddDays(-8))
                        , string.Format("{0:yyyy-MM-dd}", DateTime.Now.Date.AddDays(-1))
                        , new GaQueryMatrics().GetQueryProperty());
            }
        }

        private void SendResultMail(string mailTo, List<ExecuteResult> resultInfos, string subject = "")
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in resultInfos.OrderBy(x => x.IsSuccess).GroupBy(x => x.IsSuccess))
            {
                sb.Append(string.Format("【{0}】執行結果如下:<br/>", item.Key));
                foreach (var detail in item)
                {
                    sb.Append(string.Format("{0}<br/>", detail.Message));
                }
                sb.Append(string.Format("【{0}】執行結果結束:<br/>", item.Key));
                sb.Append(string.Format("＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br/>"));
            }

            try
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(mailTo);
                msg.Subject = string.Format("{0}：[系統]{1}", Environment.MachineName, "PCPJob Running");
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = sb.ToString();
                //PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                logger.Error("PCPCheck running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

        private class ExecuteResult
        {
            public string title { get; set; }
            public bool IsSuccess { get; set; }
            public string Message { get; set; }
        }
    }
}

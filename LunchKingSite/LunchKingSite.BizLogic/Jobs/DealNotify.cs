﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace LunchKingSite.BizLogic.Jobs
{
    //跟出貨相關的通知信
    public class DealNotify : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(PaymentCharge));
        private DateTime deliverTimeS = DateTime.Now;
        protected static IAccountingProvider ap;
        protected static IPponProvider pp;
        protected static IHumanProvider hp;
        protected static ISysConfProvider config;
        protected static ISellerProvider sp;
        protected static IHiDealProvider hdpro;
        protected static IOrderProvider op;
        protected static IMemberProvider mp;
        protected static IVerificationProvider vp;

        static DealNotify()
        {
            config = ProviderFactory.Instance().GetConfig();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            hdpro = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
        }

        public void Execute(JobSetting js)
        {
            DateTime now = DateTime.Now.Date;
            DateTime LastMonth = now.AddMonths(-1);
            string subject = "主旨";

            #region 通知管理者已開始寄通知信

            try
            {
                MailMessage msgStart = new MailMessage();
                AddMsgTo(msgStart, config.AdminEmail);
                msgStart.Subject = Environment.MachineName + ": DealNotify Start Running";
                msgStart.From = new MailAddress(config.AdminEmail);
                msgStart.Body = string.Empty;
                PostMan.Instance().Send(msgStart, SendPriorityType.Normal);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            #endregion

            try
            {
                #region 出貨提醒通知信

                #region 新增訂單出貨通知

                EmailFacade.SendNewShipOrderToSeller();

                #endregion

                #region [已作廢] P 好康

                ////抓出 1.今天開始寄送 2.宅配消費的檔次 3.新版核銷 4.排除母檔! 5.達到門檻 (For店家開始出貨通知信)
                //IEnumerable<ViewDealPropertyBusinessHourContent> vdpCol = pp.ViewDealPropertyBusinessHourContentGetListPaging
                //    (0, 0, ViewDealPropertyBusinessHourContent.Columns.BusinessHourGuid,
                //    ViewDealPropertyBusinessHourContent.Columns.BusinessHourDeliverTimeS + " = " + deliverTimeS.ToString("yyyy/MM/dd"),
                //    ViewDealPropertyBusinessHourContent.Columns.DeliveryType + " = " + (int)DeliveryType.ToHouse);
                //vdpCol = vdpCol.Where(x => !Helper.IsFlagSet(x.BusinessHourStatus, BusinessHourStatus.ComboDealMain) &&
                //                           x.OrderedQuantity >= x.BusinessHourOrderMinimum);

                //foreach (var viewDealProperty in vdpCol)
                //{
                    #region 店家開始出貨通知信

                    //var template = TemplateFactory.Instance().GetTemplate<SellerDeliverInform>();
                    //template.Vdpb = viewDealProperty;
                    ////多檔次的母檔
                    //template.MainDeal = null;

                    //try
                    //{
                    //    var msg = new MailMessage();
                    //    msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                    //    AddMsgTo(msg, viewDealProperty.SellerEmail);
                    //    if (RegExRules.CheckEmail(viewDealProperty.ReturnedPersonEmail))
                    //    {
                    //        AddMsgTo(msg, viewDealProperty.ReturnedPersonEmail);
                    //    }
                    //    msg.Subject = ReplaceLine(template.MailSubject);
                    //    msg.Body = template.ToString();
                    //    msg.IsBodyHtml = true;
                    //    PostMan.Instance().Send(msg, SendPriorityType.Normal);
                    //}
                    //catch (Exception ex)
                    //{
                    //    logger.Error(ex + " sellerEmail=" + viewDealProperty.SellerEmail + "ReturnedPersonEmail=" + viewDealProperty.ReturnedPersonEmail);
                    //}

                    #endregion

                    #region [已作廢] 業務出貨提醒通知信

                    //if (!string.IsNullOrEmpty(viewDealProperty.Email))
                    //{
                    //    MailMessage msg = new MailMessage();
                    //    msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                    //    msg.To.Add(viewDealProperty.Email);
                    //    msg.Subject = ReplaceLine("【17Life】出貨提醒(" + viewDealProperty.SellerName + ")");
                    //    DateTime ds, de;    //開始寄送日與結束寄送日
                    //    if (DateTime.TryParse(viewDealProperty.BusinessHourDeliverTimeS.ToString(), out ds) && DateTime.TryParse(viewDealProperty.BusinessHourDeliverTimeE.ToString(), out de))
                    //    {
                    //        msg.Body = "親愛的 " + viewDealProperty.EmpName + " 業務，您好：<br/><br/>" +
                    //            "您負責的【" + viewDealProperty.DealName + "】於" + ds.ToString("yyyy/MM/dd") + "開始陸續出貨，<br/>" +
                    //            "最晚於" + de.ToString("yyyy/MM/dd") + "前出貨完畢，還請協助與店家確認是否順利出貨。謝謝您！";
                    //        msg.IsBodyHtml = true;
                    //        PostMan.Instance().Send(msg, SendPriorityType.Normal);
                    //    }
                    //}
                    #endregion

                    #region [已作廢] 消費者出貨提醒通知信

                    ////依已知檔次抓出憑證, 並寄出貨提醒給消費者
                    //ViewPponCouponCollection vpcCol = pp.ViewPponCouponGetList(0, 0, ViewPponCoupon.Columns.BusinessHourGuid,
                    //    ViewPponCoupon.Columns.BusinessHourGuid + " = " + viewDealProperty.BusinessHourGuid);

                    ////必須為成立單, 非取消單, 且非系統輸入 (系統輸入的是核銷資訊)
                    //IEnumerable<ViewPponCoupon> vpcC = vpcCol.Where(x => Helper.IsFlagSet(x.OrderStatus, (int)OrderStatus.Complete) &&
                    //                                                    !Helper.IsFlagSet(x.OrderStatus, (int)OrderStatus.Cancel) &&
                    //                                                    !Helper.IsFlagSet(x.OrderDetailStatus, (int)OrderDetailStatus.SystemEntry));

                    //foreach (ViewPponCoupon viewPponCoupon in vpcC)
                    //{
                    //    if (RegExRules.CheckEmail(viewPponCoupon.MemberEmail))
                    //    {
                    //        try
                    //        {
                    //            MailMessage msg2 = new MailMessage();
                    //            PponShippingNotify template = TemplateFactory.Instance().GetTemplate<PponShippingNotify>();
                    //            template.Vdpb = viewDealProperty;
                    //            template.TheOrderDetail = viewPponCoupon;
                    //            template.AllCountry = OrderFacade.GetRandomTodayPponDeal(3, PponCityGroup.DefaultPponCityGroup.AllCountry.CityId);
                    //            template.CityDeal = OrderFacade.GetRandomCityDeal(viewPponCoupon.BusinessHourGuid);
                    //            template.ServerConfig = config;
                    //            msg2.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                    //            msg2.To.Add(viewPponCoupon.MemberEmail);
                    //            msg2.Subject = "出貨通知({seller_name})".Replace("{seller_name}", viewDealProperty.SellerName);
                    //            msg2.Body = template.ToString();
                    //            msg2.IsBodyHtml = true;
                    //            PostMan.Instance().Send(msg2, SendPriorityType.Normal);
                    //        }
                    //        catch (Exception ex)
                    //        {

                    //        }
                    //    }
                    //}

                    #endregion
                //}

                #endregion

                #region [已作廢] 品生活

                //HiDealProductCollection hdCol = hdpro.HiDealProductGetList(0, 0, HiDealProduct.Columns.Id,
                //    HiDealProduct.Columns.IsHomeDelivery + "=" + true.ToString(),
                //    HiDealProduct.Columns.UseStartTime + "=" + deliverTimeS.ToShortDateString(),
                //    HiDealProduct.Columns.VendorBillingModel + "=" + (int)VendorBillingModel.BalanceSheetSystem);
                //foreach (HiDealProduct hdp in hdCol)
                //{
                //    ViewHiDealProductInformEmail hdpim = hdpro.ViewHiDealProductInformEmailGetByProductId(hdp.Id);

                //    if (RegExRules.CheckEmail(hdpim.SellerEmail))
                //    {
                //        HiDealSellerDeliverInform template = TemplateFactory.Instance().GetTemplate<HiDealSellerDeliverInform>();
                //        template.DealName = hdpim.DealName;
                //        template.TheProduct = hdp;
                //        template.ServerConfig = config;

                //        MailMessage msg = new MailMessage();
                //        msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                //        msg.To.Add(hdpim.SellerEmail);
                //        if (RegExRules.CheckEmail(hdpim.EmpEmail))
                //        {
                //            msg.To.Add(hdpim.EmpEmail);
                //        }
                //        if (RegExRules.CheckEmail(hdpim.ReturnedPersonEmail) && hdpim.SellerEmail != hdpim.ReturnedPersonEmail)
                //        {
                //            msg.To.Add(hdpim.ReturnedPersonEmail);
                //        }
                //        msg.Subject = ReplaceLine(template.MailSubject + "_" + template.TheProduct.Name);
                //        msg.Body = template.ToString();
                //        msg.IsBodyHtml = true;
                //        PostMan.Instance().Send(msg, SendPriorityType.Normal);
                //    }
                //}
                #endregion

                #endregion

                #region 出貨期限通知信

                #region P 好康

                //New 出貨到期通知信 (店家) (到期前2日、不包含24H)
                EmailFacade.SendExpiringOrdersToSeller(false);

                //超過出貨期限通知 (店家)
                EmailFacade.SendExpiringOrdersToSeller(true);

                #endregion
                
                #region [已作廢] P 好康

                ////1.未填出貨回覆日 2.新版核銷 3.宅配檔 4.子檔 5.已結檔且達到門檻
                //int tempSlug;
                //var dpbh = pp.ViewDealPropertyBusinessHourContentGetListPaging(0, 0, string.Empty,
                //    ViewDealPropertyBusinessHourContent.Columns.ShippedDate + " is null ",
                //    ViewDealPropertyBusinessHourContent.Columns.VendorBillingModel + " = " + (int)VendorBillingModel.BalanceSheetSystem,
                //    ViewDealPropertyBusinessHourContent.Columns.DeliveryType + " = " + (int)DeliveryType.ToHouse)
                //            .Where(x => !Helper.IsFlagSet(x.BusinessHourStatus, BusinessHourStatus.ComboDealMain) &&
                //                        int.TryParse(x.Slug, out tempSlug) &&
                //                        tempSlug >= x.BusinessHourOrderMinimum)
                //            .ToList();

                ////篩出當日為 "出貨截止日-3日" 的宅配檔次, 以寄發 "出貨到期通知"
                ////var deliverTimeUp = dpbh.Where(x => DateTime.Today.Date == Convert.ToDateTime(x.BusinessHourDeliverTimeE).AddDays(-3).Date);
                
                ////篩出當日大於 "出貨截止日+1日" 的宅配檔次, 以寄發 "超過出貨期限通知". 
                ////var deliverExceed = dpbh.Where(x => DateTime.Today.Date >= Convert.ToDateTime(x.BusinessHourDeliverTimeE).AddDays(1));

                ////2015/1/23 註記 抓取銷售份數 --暫不用deal_sales_info資訊(未扣除結檔後退貨)
                //var dealSalesSummarys = vp.GetPponToHouseDealSummary(dpbh.Select(x => x.BusinessHourGuid))
                //                        .ToDictionary(x => x.MerchandiseGuid, x => x);
                ////抓取退貨處理中之退貨份數
                //var returnningForms = op.ViewOrderReturnFormListGetListByDealGuid(dpbh.Select(x => x.BusinessHourGuid))
                //                            .Where(x => x.ProgressStatus == (int) ProgressStatus.Processing ||
                //                                        x.ProgressStatus == (int) ProgressStatus.AtmFailed ||
                //                                        x.ProgressStatus == (int) ProgressStatus.AtmQueueing ||
                //                                        x.ProgressStatus == (int) ProgressStatus.AtmQueueSucceeded)
                //                            .ToDictionary(x => x.ReturnFormId, x => x.ProductGuid);
                //var returnningCounts = op.ReturnFormRefundGetList(returnningForms.Keys)
                //                            .Where(x => x.IsFreight == false)
                //                            .GroupBy(x => x.ReturnFormId)
                //                            .ToDictionary(x => x.Key, x => x.Count());
                //var dealRefundInfo = (from rc in returnningCounts
                //                      join rf in returnningForms on rc.Key equals rf.Key
                //                      group rc by rf.Value
                //                          into dr
                //                          select new
                //                          {
                //                              ProductGuid = dr.Key,
                //                              RefundCount = dr.Sum(x => x.Value)
                //                          })
                //                    .ToDictionary(x => x.ProductGuid, x => x.RefundCount);

                #region [已作廢] 出貨到期通知信 (店家)

                //foreach (var dpb in deliverTimeUp)
                //{
                //    //若無出貨份數不須寄發通知信
                //    if (!dealSalesSummarys.ContainsKey(dpb.BusinessHourGuid) ||
                //        dealSalesSummarys[dpb.BusinessHourGuid].UnverifiedCount == 0)
                //    {
                //        continue;
                //    }
                //    var orderedQuantity = dealSalesSummarys[dpb.BusinessHourGuid].UnverifiedCount +
                //                          dealSalesSummarys[dpb.BusinessHourGuid].VerifiedCount;
                //    //檢查訂購份數扣除退貨份數 是否仍有出貨份數 若無出貨份數不須寄發通知信
                //    if (dealRefundInfo.ContainsKey(dpb.BusinessHourGuid))
                //    {
                //        if (orderedQuantity <= dealRefundInfo[dpb.BusinessHourGuid])
                //        {
                //            continue;
                //        }
                //    }

                //    var template = TemplateFactory.Instance().GetTemplate<SellerDeliverTimeupInform>();
                //    template.Vdpb = dpb;
                //    template.MainDeal = null;

                //    try
                //    {
                //        var msg = new MailMessage();
                //        msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                //        AddMsgTo(msg, dpb.SellerEmail);
                //        if (RegExRules.CheckEmail(dpb.ReturnedPersonEmail) && dpb.SellerEmail != dpb.ReturnedPersonEmail)
                //        {
                //            AddMsgTo(msg, dpb.ReturnedPersonEmail);
                //        }
                //        msg.Subject = ReplaceLine(template.MailSubject);
                //        msg.Body = template.ToString();
                //        msg.IsBodyHtml = true;
                //        PostMan.Instance().Send(msg, SendPriorityType.Normal);
                //    }
                //    catch (Exception ex)
                //    {
                //        logger.Error(ex + " sellerEmail=" + dpb.SellerEmail + "ReturnedPersonEmail=" + dpb.ReturnedPersonEmail);
                //    }
                //}

                #endregion

                #region [已作廢] 超過出貨期限通知 (店家)

                //foreach (var dpb in deliverExceed)
                //{
                //    //若無出貨份數不須寄發通知信
                //    if (!dealSalesSummarys.ContainsKey(dpb.BusinessHourGuid) ||
                //        dealSalesSummarys[dpb.BusinessHourGuid].UnverifiedCount == 0)
                //    {
                //        continue;
                //    }
                //    var orderedQuantity = dealSalesSummarys[dpb.BusinessHourGuid].UnverifiedCount +
                //                          dealSalesSummarys[dpb.BusinessHourGuid].VerifiedCount;
                //    //檢查訂購份數扣除退貨份數 是否仍有出貨份數 若無出貨份數不須寄發通知信
                //    if (dealRefundInfo.ContainsKey(dpb.BusinessHourGuid))
                //    {
                //        if (orderedQuantity <= dealRefundInfo[dpb.BusinessHourGuid])
                //        {
                //            continue;
                //        }
                //    }

                //    var template = TemplateFactory.Instance().GetTemplate<SellerDeliverExceedInform>();
                //    template.Vdpb = dpb;
                //    template.MainDeal = null;

                //    try
                //    {
                //        var msg = new MailMessage();
                //        msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                //        AddMsgTo(msg, dpb.SellerEmail);
                //        if (RegExRules.CheckEmail(dpb.ReturnedPersonEmail) && dpb.SellerEmail != dpb.ReturnedPersonEmail)
                //        {
                //            msg.To.Add(dpb.ReturnedPersonEmail);
                //        }
                //        msg.Subject = ReplaceLine(template.MailSubject);
                //        msg.Body = template.ToString();
                //        msg.IsBodyHtml = true;
                //        PostMan.Instance().Send(msg, SendPriorityType.Normal);
                //    }
                //    catch (Exception ex)
                //    {
                //        logger.Error(ex + "sellerEmail=" + dpb.SellerEmail + "ReturnedPersonEmail=" + dpb.ReturnedPersonEmail);
                //    }
                //}

                #endregion

                #region [已作廢] 超過出貨期限通知 (業務&客服)

                //篩出 1.宅配 2.今天是出貨截止日+3日 的檔次
                //var staffDeliverExceed = dpbh.Where(x => DateTime.Today.Date == Convert.ToDateTime(x.BusinessHourDeliverTimeE).AddDays(3).Date);
                //foreach (var vdp in staffDeliverExceed)
                //{
                //    //若無出貨份數不須寄發通知信
                //    if (!dealSalesSummarys.ContainsKey(vdp.BusinessHourGuid) ||
                //        dealSalesSummarys[vdp.BusinessHourGuid].UnverifiedCount == 0)
                //    {
                //        continue;
                //    }
                //    //檢查訂購份數扣除退貨份數 是否仍有出貨份數 若無出貨份數不須寄發通知信
                //    if (dealRefundInfo.ContainsKey(vdp.BusinessHourGuid))
                //    {
                //        if ((dealSalesSummarys[vdp.BusinessHourGuid].UnverifiedCount + dealSalesSummarys[vdp.BusinessHourGuid].VerifiedCount) <= dealRefundInfo[vdp.BusinessHourGuid])
                //        {
                //            continue;
                //        }
                //    }

                //    try
                //    {
                //        int orderCount = op.ViewOrderShipListGetCountByProductGuid(vdp.BusinessHourGuid, OrderClassification.LkSite);
                //        var msg = new MailMessage();
                //        msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                //        msg.To.Add(config.CodShipEmail);
                //        msg.To.Add(config.SalesAssistantEmail);
                //        if (RegExRules.CheckEmail(vdp.Email))
                //        {
                //            msg.To.Add(vdp.Email);
                //        }
                //        msg.Subject = "逾期！請督促廠商完成出貨：［" + vdp.UniqueId + "］" + vdp.CouponUsage;
                //        msg.Body = string.Format(@"親愛的同仁：<br/><br/>
                //                    您負責的：{0}，<font color='red'>已超過出貨期限：{1}</font>。<br/><br/>
                //                    請您盡快督促廠商完成出貨{4}。<br/><br/>為避免影響廠商請款進度或遭罰款：<br/>
                //                    (1) 請您盡快連絡廠商完成出貨，並填寫線上出貨單。<br/>
                //                    (2) 若確認廠商需延遲出貨，請協助申請寄發會員通知信。<br/><br/>
                //                    ※  若廠商延遲出貨，將會有賠償金額產生。<br/>
                //                    罰款準則：單筆訂單結帳金額500元以下，罰金30元；500~999元，罰金50元；1000元以上，罰金100元<br/><br/>
                //                    廠商聯絡資訊：{2} ({3})"
                //        , "[" + vdp.UniqueId + "]" + vdp.CouponUsage, (Convert.ToDateTime(vdp.BusinessHourDeliverTimeE)).ToShortDateString()
                //        ,vdp.ReturnedPersonTel, vdp.ReturnedPersonName, orderCount > 0 ? "，包含<font color='red'>未填單" + orderCount + "筆</font>" : string.Empty);
                //        msg.IsBodyHtml = true;
                //        PostMan.Instance().Send(msg, SendPriorityType.Normal);
                //    }
                //    catch (Exception ex)
                //    {
                //        logger.Error(ex);
                //    }
                //}

                #endregion

                #endregion

                #region [已作廢] 品生活

                ////抓出 "有任何訂單未壓出貨日期" 的產品
                //HiDealProductCollection hdpCol = hdpro.HiDealProductCollectionGetListByNoOrderShip();

                ////篩出當日介於 "出貨截止日-3日" 和 "出貨截止日" 的產品, 以寄發 "出貨到期通知"
                //IEnumerable<HiDealProduct> hdTimeUp = hdpCol.Where(x => now >= Convert.ToDateTime(x.UseEndTime).AddDays(-3))
                //    .Where(x => now <= Convert.ToDateTime(x.UseEndTime)).Where(x => x.IsHomeDelivery == true);
                ////篩出當日大於 "出貨截止日+1日" 的產品, 以寄發 "超過出貨期限通知"
                //IEnumerable<HiDealProduct> hdExceed = hdpCol.Where(x => now >= Convert.ToDateTime(x.UseEndTime).AddDays(1))
                //    .Where(x => x.IsHomeDelivery == true);

                #region [已作廢] 店家出貨到期通知信

                //foreach (HiDealProduct hdp in hdTimeUp) 
                //{
                //    ViewHiDealProductInformEmail hiDealProductInformEmail = hdpro.ViewHiDealProductInformEmailGetByProductId(hdp.Id);

                //    if (RegExRules.CheckEmail(hiDealProductInformEmail.SellerEmail))
                //    {
                //        HiDealSellerDeliverTimeupInform template = TemplateFactory.Instance().GetTemplate<HiDealSellerDeliverTimeupInform>();
                //        template.DealName = hiDealProductInformEmail.DealName;
                //        template.TheProduct = hdp;
                //        template.ServerConfig = config;

                //        MailMessage msg = new MailMessage();
                //        msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                //        msg.To.Add(hiDealProductInformEmail.SellerEmail);
                //        if (RegExRules.CheckEmail(hiDealProductInformEmail.EmpEmail))
                //        {
                //            msg.To.Add(hiDealProductInformEmail.EmpEmail);
                //        }
                //        if (RegExRules.CheckEmail(hiDealProductInformEmail.ReturnedPersonEmail) 
                //        {
                //            && hiDealProductInformEmail.SellerEmail != hiDealProductInformEmail.ReturnedPersonEmail)
                //        }
                //            msg.To.Add(hiDealProductInformEmail.ReturnedPersonEmail);
                //        msg.Subject = ReplaceLine(template.MailSubject + "_" + template.TheProduct.Name);
                //        msg.Body = template.ToString();
                //        msg.IsBodyHtml = true;
                //        PostMan.Instance().Send(msg, SendPriorityType.Normal);
                //    } 
                //}

                #endregion

                #region [已作廢] 超過出貨期限通知信 (For店家)

                //foreach (HiDealProduct hdp in hdExceed) 
                //{
                //    //撈出業務Name與業務Email
                //    ViewHiDealProductInformEmail hiDealProductInformEmail = hdpro.ViewHiDealProductInformEmailGetByProductId(hdp.Id);

                //    if (RegExRules.CheckEmail(hiDealProductInformEmail.SellerEmail))
                //    {
                //        HiDealSellerDeliverExceedInform template = TemplateFactory.Instance().GetTemplate<HiDealSellerDeliverExceedInform>();
                //        template.DealName = hiDealProductInformEmail.DealName;
                //        template.TheProduct = hdp;
                //        template.ServerConfig = config;

                //        MailMessage msg = new MailMessage();
                //        msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                //        msg.To.Add(hiDealProductInformEmail.SellerEmail);
                //        if (RegExRules.CheckEmail(hiDealProductInformEmail.EmpEmail))
                //        {
                //            msg.To.Add(hiDealProductInformEmail.EmpEmail);
                //        }
                //        if (RegExRules.CheckEmail(hiDealProductInformEmail.ReturnedPersonEmail)
                //            && hiDealProductInformEmail.SellerEmail != hiDealProductInformEmail.ReturnedPersonEmail)
                //        {
                //            msg.To.Add(hiDealProductInformEmail.ReturnedPersonEmail);
                //        }
                //        msg.Subject = ReplaceLine(template.MailSubject + "_" + template.TheProduct.Name);
                //        msg.Body = template.ToString();
                //        msg.IsBodyHtml = true;
                //        PostMan.Instance().Send(msg, SendPriorityType.Normal);
                //    } 
                //}

                #endregion

                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            #region 暫付七成付款異常通知

            try
            {
                subject = "暫付七成付款異常通知";
                DataTable partialnotice = op.TempPaymentAlertGetTable(
                        Helper.GetDateOfWeek(DateTime.Now.Date.AddDays(-7), DayOfWeek.Monday).ToString("yyyy/MM/dd"),
                        Helper.GetDateOfWeek(DateTime.Now.Date, DayOfWeek.Sunday).ToString("yyyy/MM/dd"));
                if (partialnotice.Rows.Count > 0)
                {
                    var query = from x in partialnotice.AsEnumerable()
                                select new
                                {
                                    item_name = x.Field<string>("item_name"),
                                    Bid = x.Field<Guid>("guid").ToString(),
                                    unique_id = x.Field<int>("unique_id")
                                };
                    var mailContent = new StringBuilder();
                    mailContent.Append("親愛的客服人員，您好：<br/>以下檔次暫付七成失敗，請協助確認。<br/>");

                    foreach (var item in query)
                    {
                        mailContent.Append(string.Format(@"<a href=""{0}"">({1}){2}</a><br/>",
                            config.SiteUrl + "/controlroom/ppon/setup.aspx?bid=" + item.Bid,
                            item.unique_id, item.item_name));
                    }

                    mailContent.Append(string.Format(@"馬上至<a href=""{0}"">商品檔週期維護後台</a><br/>",
                            config.SiteUrl + "/controlroom/finance/ToHouseDealPaymentAudit"));

                    SendEmail(config.AdminEmail, new[] { config.FinanceAccountsEmail }, subject, mailContent.ToString());
                }
            }
            catch (Exception ex)
            {
                SendEmail(config.AdminEmail, new[] {config.AdminEmail}, subject,
                    string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
                logger.Error(ex);
            }

            #endregion

            #region 暫付七成通知

            try
            {
                subject = "暫付七成通知";
                DataTable noticeBid = op.NoticePartillyPaymentGetTable(DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd"));
                if (noticeBid.Rows.Count > 0)
                {
                    var query = from x in noticeBid.AsEnumerable()
                                select new
                                {
                                    item_name = x.Field<string>("item_name"),
                                    Bid = x.Field<Guid>("business_hour_guid").ToString(),
                                    unique_id = x.Field<int>("unique_id"),
                                    mail = x.Field<string>("mail"),
                                    seller_name = x.Field<string>("seller_name")
                                };

                    foreach (var item in query.GroupBy(x => new {x.seller_name, x.mail}))
                    {
                        string seller = item.Key.seller_name;
                        string mail = item.Key.mail;
                        var sb = new StringBuilder();
                        foreach (var detail in item)
                        {
                            sb.Append(string.Format("({0}){1}<br/>", detail.unique_id, detail.item_name));
                        }

                        SendPartialPaymentNotice(seller, mail, sb.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                SendEmail(config.AdminEmail, new[] {config.AdminEmail}, subject,
                    string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
                logger.Error(ex);
            }

            #endregion

            #region [已作廢] 延遲出貨罰款通知

            //try
            //{
            //    subject = "延遲出貨罰款通知";
            //    DataTable delayAmercementBid =
            //        pp.DelayBidAmercementGetTable(DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd"));
            //    if (delayAmercementBid.Rows.Count > 0)
            //    {
            //        var mailContent = new StringBuilder();

            //        mailContent.Append("親愛的企服人員，您好：<br/>以下檔次需延遲出貨罰款，請協助確認。<br/>");

            //        var query = from x in delayAmercementBid.AsEnumerable()
            //                    select new
            //                    {
            //                        Title = x.Field<string>("Title"),
            //                        Bid = x.Field<Guid>("bid").ToString(),
            //                        unique_id = x.Field<int>("unique_id")
            //                    };

            //        foreach (var item in query)
            //        {
            //            mailContent.Append(string.Format(@"<a href=""{0}"">({1}){2}</a><br/>",
            //                config.SiteUrl + "/controlroom/ppon/setup.aspx?bid=" + item.Bid,
            //                item.unique_id, item.Title));
            //        }
            //        SendEmail(config.AdminEmail, new[] { config.CsReturnEmail, config.CodShipEmail }, subject,
            //            mailContent.ToString());
            //    }
            //}
            //catch (Exception ex)
            //{
            //    SendEmail(config.AdminEmail, new[] {config.AdminEmail}, subject,
            //        string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
            //    logger.Error(ex);
            //}

            #endregion

            #region [已作廢] 延遲出貨通知

            //try
            //{
            //    subject = "延遲出貨通知";
            //    DataTable delayBid = pp.GetDelayBidTable(DateTime.Now.AddDays(-2).ToString("yyyy/MM/dd"));
            //    if (delayBid.Rows.Count > 0)
            //    {
            //        var mailContent = new StringBuilder();

            //        mailContent.Append("親愛的企服人員，您好：<br/>以下檔次已延遲出貨，請協助確認。<br/>");

            //        var query = from x in delayBid.AsEnumerable()
            //                    select new
            //                    {
            //                        Title = x.Field<string>("Title"),
            //                        Bid = x.Field<Guid>("bid").ToString(),
            //                        unique_id = x.Field<int>("unique_id")
            //                    };

            //        foreach (var item in query)
            //        {
            //            mailContent.Append(string.Format(@"<a href=""{0}"">({1}){2}</a><br/>",
            //                config.SiteUrl + "/controlroom/ppon/setup.aspx?bid=" + item.Bid,
            //                item.unique_id, item.Title));
            //        }
            //        SendEmail(config.AdminEmail, new[] { config.CsReturnEmail, config.CodShipEmail }, subject,
            //            mailContent.ToString());
            //    }
            //}
            //catch (Exception ex)
            //{
            //    SendEmail(config.AdminEmail, new[] {config.AdminEmail}, subject,
            //        string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
            //    logger.Error(ex);
            //}

            #endregion

            #region 使用者評價通知
            try
            {
                DataTable GetUserInfo = mp.EvaluateUserGet();
                var template = TemplateFactory.Instance().GetTemplate<EvaluateUserGetInfo>();
                if (GetUserInfo.Rows.Count > 0)
                {
                    var query = from row in GetUserInfo.AsEnumerable()
                                group row by new
                                {
                                    Bid = row.Field<Guid>("business_hour_guid"),
                                    usermail = row.Field<string>("user_name"),
                                    username = row.Field<string>("UserName"),
                                    title = row.Field<string>("item_name"),
                                    content = row.Field<string>("name")
                                } into grp
                                select new
                                {
                                    bid = grp.Key.Bid,
                                    usermail = grp.Key.usermail,
                                    username = grp.Key.username,
                                    title = grp.Key.title,
                                    content = grp.Key.content
                                };

                    string list = string.Empty;
                    foreach (var item in query)
                    {
                        try
                        {
                            #region 發送各個使用者評價通知
                            template.SiteUrl = config.SiteUrl;
                            template.SiteServiceUrl = config.SiteServiceUrl;
                            template.MemberName = item.username;
                            var bidList = new StringBuilder();
                            var query2 = from row in GetUserInfo.AsEnumerable()
                                         where row.Field<Guid>("business_hour_guid") == item.bid && row.Field<string>("user_name") == item.usermail
                                         select new
                                         {
                                             tid = row.Field<Guid>("trust_id"),
                                             csn = row.Field<string>("coupon_sequence_number"),
                                             Bid = row.Field<Guid>("business_hour_guid"),
                                             usermail = row.Field<string>("user_name"),
                                             title = row.Field<string>("item_name"),
                                             username = row.Field<string>("UserName"),
                                             usage_verified_time = row.Field<DateTime>("usage_verified_time"),
                                             mainBid = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(row.Field<Guid>("business_hour_guid")).MainBid,
                                             order_guid = row.Field<Guid>("order_guid")
                                         };

                            list += string.Join("\n", query2.Select(x => string.Format("trust_id：{0},核銷日期：{1}", x.tid, x.usage_verified_time.ToString())).ToArray());
                            list += "\n";


                            Guid order_guid = new Guid();
                            bool isFirst = true;
                            foreach (var item2 in query2)
                            {
                                bidList.Append("<tr>");
                                if (isFirst)
                                {
                                    var picBid = (item2.mainBid == null) ? item2.Bid : (Guid)item2.mainBid;
                                    var _EventImagePath = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(picBid).EventImagePath;
                                    bidList.Append(string.Format(@"<td rowspan=""{2}"" style=""padding: 10px 20px; border-bottom:1px solid #FFE066;"">{3}<a href=""{0}/{1}"" target=""_blank"" style=""color:#08C;"">點此觀看</a></td>",
                                        config.SiteUrl,
                                        item2.Bid,
                                        query2.Count().ToString(),
                                        (_EventImagePath == null) ? " " : ("<img src='" + ImageFacade.GetMediaPathsFromRawData(_EventImagePath, MediaType.PponDealPhoto)[0] + "' /><br />")));
                                    isFirst = false;
                                }
                                bidList.Append(string.Format(@"<td style=""padding: 10px 20px; border-bottom:1px solid #FFE066;"">{0}</td>", item2.csn));
                                bidList.Append(string.Format(@"<td style=""padding: 10px 20px; border-bottom:1px solid #FFE066;""><a href=""{0}/User/EvaluateStar.aspx?tid={1}&ft={2}"" class=""EvaluationLink"">我要評價</a></td>", config.SiteUrl, item2.tid, EvaluateSourceType.Email));
                                bidList.Append("</tr>");
                                order_guid = item2.order_guid;
                            }

                            //加入計算開信率
                            bidList.Append(string.Format(@"<tr style=""display:none !important;""><td><img src=""{0}/User/EvaluateStar.aspx?oguid={1}""></td></tr>", config.SiteUrl, order_guid.ToString()));

                            template.Bid_Short_Title = item.title;
                            template.Bid_List = bidList.ToString();
                            template.Bid_Name = item.content;
                            MailMessage msg = new MailMessage();
                            msg.From = new MailAddress(config.ServiceEmail, config.EvaluateServiceName);
                            msg.To.Add(MemberFacade.GetUserEmail(item.usermail));
                            msg.Subject = "您對 " + item.title + " 的消費體驗還滿意嗎？";
                            msg.Body = template.ToString();
                            msg.IsBodyHtml = true;
                            PostMan.Instance().Send(msg, SendPriorityType.Normal);

                            //紀錄發信LOG
                            UserEvaluateLog uelog = mp.UserEvaluateLogGetByOGuid(order_guid);
                            if (uelog.Id == 0)
                            {
                                uelog = new UserEvaluateLog();
                                uelog.OrderGuid = order_guid;
                                uelog.Sendtime = DateTime.Now;
                                uelog.IsOpen = false;
                                mp.UserEvaluateLogSet(uelog);
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            logger.Info("發送評價信失敗：\n" + ex.ToString());
                            logger.Info("item = " + new JsonSerializer().Serialize(item));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("取得「使用者評價通知」清單失敗：\n" + ex.ToString());
            }
            #endregion

            #region 消費者高負評警訊
            try
            {
                DataTable GetUserInfo = mp.EvaluateUserByNegativeComments();
                var template = TemplateFactory.Instance().GetTemplate<EvaluateWarning>();
                if (GetUserInfo.Rows.Count > 0)
                {
                    var query = GetUserInfo.AsEnumerable().Where(b => (double)(b.Field<int>("NegativeCommentsCount") / b.Field<int>("TotalVerifiedCount")) > ((double)config.NegativeCommentsRate / 100)).GroupBy(x => new
                    {
                        DealName = x.Field<string>("title"),
                        CouponUsage = x.Field<string>("coupon_usage"),
                        unique_id = x.Field<int>("unique_id"),
                        OrderTimeS = x.Field<DateTime>("business_hour_order_time_s"),
                        OrderTimeE = x.Field<DateTime>("business_hour_order_time_e"),
                        DeliverTimeS = x.Field<DateTime>("business_hour_deliver_time_s"),
                        DeliverTimeE = x.Field<DateTime>("business_hour_deliver_time_e"),
                        SalesEmail = x.Field<string>("sales_email"),
                        SalesManager = x.Field<string>("manager_email"),
                    }).Select(g => new
                    {
                        DealName = g.Key.DealName,
                        CouponUsage = g.Key.CouponUsage,
                        unique_id = g.Key.unique_id,
                        OrderTimeS = g.Key.OrderTimeS,
                        OrderTimeE = g.Key.OrderTimeE,
                        DeliverTimeS = g.Key.DeliverTimeS,
                        DeliverTimeE = g.Key.DeliverTimeE,
                        SalesEmail = g.Key.SalesEmail,
                        SalesManager = g.Key.SalesManager,
                    });

                    if (query.Count() > 0)
                    {
                        foreach (var item in query)
                        {
                            StringBuilder AvgPoint = new StringBuilder();
                            StringBuilder NegativeCommentsRate = new StringBuilder();
                            int[] item_array = { 1, 2, 6, 4 };
                            foreach (int i in item_array)
                            {
                                var item2 = GetUserInfo.AsEnumerable().Where(x => x.Field<int>("unique_id") == item.unique_id && x.Field<int>("item_id") == i).FirstOrDefault();
                                if (item2 != null)
                                {
                                    AvgPoint.Append(string.Format(@"<td align=""center"" style=""border-bottom: 1px solid #d0d0d0; border-right: 1px solid #d0d0d0;"">{0}</td>", item2.Field<int>("AvgPoint").ToString()));

                                    NegativeCommentsRate.Append(string.Format((double)(item2.Field<int>("NegativeCommentsCount") / item2.Field<int>("TotalVerifiedCount")) > ((double)config.NegativeCommentsRate / 100) ? @"<td align=""center"" style=""border-right: 1px solid #d0d0d0;color:#ff0000;"">{0}<br/><span style="" font-size: 12px;"">({1}/{2})</span></td>" : @"<td align=""center"" style=""border-right: 1px solid #d0d0d0;"">{0}<br/><span style="" font-size: 12px;"">({1}/{2})</span></td>", string.Format("{0:0.####%}", (double)(item2.Field<int>("NegativeCommentsCount") / item2.Field<int>("TotalVerifiedCount"))), item2.Field<int>("NegativeCommentsCount").ToString(), item2.Field<int>("TotalVerifiedCount").ToString()));
                                }
                                else
                                {
                                    AvgPoint.Append(string.Format(@"<td align=""center"" style=""border-bottom: 1px solid #d0d0d0; border-right: 1px solid #d0d0d0;"">－</td>"));
                                    NegativeCommentsRate.Append(string.Format(@"<td align=""center"" style=""border-right: 1px solid #d0d0d0;"">－</td>"));
                                }
                            }
                            template.DealName = item.DealName;
                            template.UniqueId = item.unique_id.ToString();
                            template.OrderTimeS = item.OrderTimeS.ToString("yyyy/MM/dd");
                            template.OrderTimeE = item.OrderTimeE.ToString("yyyy/MM/dd");
                            template.DeliverTimeS = item.DeliverTimeS.ToString("yyyy/MM/dd");
                            template.DeliverTimeE = item.DeliverTimeE.ToString("yyyy/MM/dd");
                            template.AveragePointCollection = AvgPoint.ToString();
                            template.NegativeCommentsRateCollection = NegativeCommentsRate.ToString();

                            MailMessage msg = new MailMessage();
                            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                            if (item.SalesEmail != null && item.SalesEmail.Contains("@"))
                            {
                                msg.To.Add(item.SalesEmail);
                            }
                            if (item.SalesManager != null && item.SalesManager.Contains("@"))
                            {
                                msg.To.Add(item.SalesManager);
                            }
                            if (msg.To.Count > 0)
                            {
                                msg.Subject = "消費者高負評警訊：" + "【" + item.CouponUsage + "】";
                                msg.Body = template.ToString();
                                msg.IsBodyHtml = true;
                                PostMan.Instance().Send(msg, SendPriorityType.Normal);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            #endregion

            #region 檢查deal_sales_info ordered_quantity 與即時統計 是否有出入

            try
            {
                //deal_sales_info 規則調整為2015-02-09(先前有誤將結檔後退貨份數也納入ordered_quantity中)
                //檢查上一週結檔 之 deal_sales_info ordered_quantity是否與即時統計有出入
                //連續檢查7天(觀察結檔後退貨是否計算正常)
                int tempSlug;
                var today = DateTime.Today;
                var dpbh = pp.ViewDealPropertyBusinessHourContentGetListPaging(0, 0, string.Empty,
                    ViewDealPropertyBusinessHourContent.Columns.ShippedDate + " is null ",
                    ViewDealPropertyBusinessHourContent.Columns.VendorBillingModel + " = " + (int)VendorBillingModel.BalanceSheetSystem,
                    ViewDealPropertyBusinessHourContent.Columns.DeliveryType + " = " + (int)DeliveryType.ToHouse,
                    string.Format("{0} between {1} and {2}", ViewDealPropertyBusinessHourContent.Columns.BusinessHourOrderTimeE
                                                           , today.AddDays((-7 - (int)today.DayOfWeek))
                                                           , today.AddDays(-(int)today.DayOfWeek + 1)))
                            .Where(x => !Helper.IsFlagSet(x.BusinessHourStatus, BusinessHourStatus.ComboDealMain) &&
                                        int.TryParse(x.Slug, out tempSlug) &&
                                        tempSlug >= x.BusinessHourOrderMinimum)
                            .ToList();

                //抓取即時統計銷售份數
                var dealSalesSummarys = vp.GetPponToHouseDealSummary(dpbh.Select(x => x.BusinessHourGuid))
                                        .ToDictionary(x => x.MerchandiseGuid, x => x);

                /* 於更新 deal_sales_info refresh進版後再進行檢查通知
                 * 檢查deal_sales_info ordered_quantity 和 實時計算結果是否一致 若不一致則發信通知
                 * 以觀察是否可改回使用 deal_sales_info ordered_quantity */

                var orderedQuantityMismatchDesc = new StringBuilder();
                foreach (var dpb in dpbh)
                {
                    if (!dealSalesSummarys.ContainsKey(dpb.BusinessHourGuid))
                    {
                        continue;
                    }
                    var orderedQuantity = dealSalesSummarys[dpb.BusinessHourGuid].UnverifiedCount +
                                          dealSalesSummarys[dpb.BusinessHourGuid].VerifiedCount;
                    if (dpb.OrderedQuantity != orderedQuantity)
                    {
                        orderedQuantityMismatchDesc.Append(string.Format("檔次bid:{0} 結檔日期:{1} ordered_quantity:{2} 即時統計:{3}<br/>"
                            , dpb.BusinessHourGuid, dpb.BusinessHourOrderTimeE, dpb.OrderedQuantity, orderedQuantity));
                    }
                }

                if (orderedQuantityMismatchDesc.Length > 0)
                {
                    try
                    {
                        var msg = new MailMessage();
                        msg.From = new MailAddress(config.AdminEmail);
                        msg.To.Add(config.ItPAD);

                        msg.Subject = "Deal_Sales_Info ordered_quantity與即時統計結果有出入";
                        msg.Body = string.Format("不一致結果如下:<br/><br/>{0}", orderedQuantityMismatchDesc);
                        msg.IsBodyHtml = true;
                        PostMan.Instance().Send(msg, SendPriorityType.Normal);
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            #endregion  檢查deal_sales_info ordered_quantity 與即時統計 是否有出入

            #region 先收單據後付款:宅配檔對帳單應付金額為負通知信

            try
            {
                var bsInfos = ap.GetPayByBillBalanceSheetHasNegativeAmount(DateTime.Now.AddDays(-1), DateTime.Now);
                if (bsInfos.AsEnumerable().Any())
                {
                    BalanceSheetManager.ProductBalanceSheetWithNegativeAmountMailNotify(bsInfos);
                }
            }
            catch (Exception ex)
            {
                SendEmail(config.AdminEmail, new[] { config.FinanceAccountsEmail }, subject,
                    string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
            }

            #endregion 先收單據後付款:宅配檔對帳單應付金額為負通知信

            #region 退貨處理相關通知

            #region 退貨處理截止前通知(商家)

            try
            {
                //第二次退貨通知信
                //退貨申請日之後的第四天
                EmailFacade.SendFinalReturnNotifyToSellers();
            }
            catch (Exception ex)
            {
                SendEmail(config.AdminEmail, new[] { config.AdminEmail }, subject,
                       string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
                logger.Error(ex);
            }

            #endregion

            #region CSD退貨未完成

            try
            {
                //退貨申請日之後的第8天
                EmailFacade.SendNotCompleteReturnMailToCSD(VendorProgressStatus.Processing);
            }
            catch (Exception ex)
            {
                SendEmail(config.AdminEmail, new[] { config.AdminEmail }, subject,
                       string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
                logger.Error(ex);
            }

            #endregion

            #region CSD無法完成退貨

            try
            {
                EmailFacade.SendNotCompleteReturnMailToCSD(VendorProgressStatus.Unreturnable);
            }
            catch (Exception ex)
            {
                SendEmail(config.AdminEmail, new[] { config.AdminEmail }, subject,
                       string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
                logger.Error(ex);
            }

            #endregion

            #endregion


            if (config.EnabledExchangeProcess)
            { 

                #region 每日換貨申請通知(商家)

                try
                {
                    subject = "每日換貨統計通知(商家)";
                    var filter = new List<string>();
                    filter.Add(string.Format("{0} = {1}", ViewPponOrderReturnList.Columns.Type, (int)OrderReturnType.Exchange));
                    filter.Add(string.Format("{0} between {1} and {2}", ViewPponOrderReturnList.Columns.ReturnCreateTime, DateTime.Today.AddDays(-1).ToString("yyyy/MM/dd"), DateTime.Today.ToString("yyyy/MM/dd")));
                    filter.Add(string.Format("{0} in ({1}, {2})", ViewPponOrderReturnList.Columns.ReturnStatus, (int)OrderReturnStatus.ExchangeProcessing, (int)OrderReturnStatus.SendToSeller));
                    var orderLists = op.ViewPponOrderReturnListPaging(0, 0, ViewPponOrderReturnList.Columns.ReturnCreateTime, filter.ToArray());
                    if (orderLists.Count > 0)
                    {
                        var query = from x in orderLists
                                    select new
                                    {
                                        ItemName = x.ItemName,
                                        ApplicationTime = x.ReturnCreateTime.ToString("yyyy/MM/dd HH:mm"),
                                        OrderId = x.OrderId,
                                        ShipTime = x.OrignalShipTime.HasValue ? x.OrignalShipTime.Value.ToString("yyyy/MM/dd") : "尚未出貨",
                                        Reason = x.Reason,
                                        SellerName = x.SellerName,
                                        ReturnedPersonEmail = x.ReturnedPersonEmail
                                    };

                        foreach (var item in query.GroupBy(x => new { x.SellerName, x.ReturnedPersonEmail }))
                        {
                            string seller = item.Key.SellerName;
                            string mail = item.Key.ReturnedPersonEmail;
                            var sb = new StringBuilder();
                            foreach (var detail in item)
                            {
                                sb.Append("<tr style='border: 1px solid #808080'>");
                                sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", detail.ApplicationTime));
                                sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", detail.OrderId));
                                sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", detail.ItemName));
                                sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", detail.ShipTime));
                                sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", detail.Reason));
                                sb.Append("</tr>");
                            }

                            SendExchangeOrderNotifyMail(seller, mail, sb.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    SendEmail(config.AdminEmail, new[] { config.AdminEmail }, subject,
                        string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
                    logger.Error(ex);
                }

                #endregion 每日換貨申請通知(商家)

                #region COD前一日完成換貨統計

                try
                {
                    subject = string.Format("17Life每日完成換貨統計<{0}>", DateTime.Today.ToString("yyyy/MM/dd"));
                    var filter = new List<string>();
                    filter.Add(string.Format("{0} = {1}", ViewPponOrderReturnList.Columns.Type, (int)OrderReturnType.Exchange));
                    filter.Add(string.Format("{0} in ({1}, {2})", ViewPponOrderReturnList.Columns.ReturnStatus, (int)OrderReturnStatus.ExchangeProcessing, (int)OrderReturnStatus.SendToSeller));
                    filter.Add(string.Format("{0} = {1}", ViewPponOrderReturnList.Columns.VendorProgressStatus, (int)ExchangeVendorProgressStatus.ExchangeCompleted));
                    var orderLists = op.ViewPponOrderReturnListPaging(0, 0, ViewPponOrderReturnList.Columns.ReturnCreateTime, filter.ToArray());
                    if (orderLists.Count > 0)
                    {
                        var query = from x in orderLists
                                    orderby x.SellerName, x.ReturnCreateTime
                                    select new
                                    {
                                        ItemName = x.ItemName,
                                        ApplicationTime = x.ReturnCreateTime.ToString("yyyy/MM/dd HH:mm"),
                                        OrderId = x.OrderId,
                                        OrderGuid = x.OrderGuid,
                                        ShipTime = x.OrignalShipTime.HasValue ? x.OrignalShipTime.Value.ToString("yyyy/MM/dd") : "尚未出貨",
                                        Reason = x.Reason,
                                        SellerName = x.SellerName,
                                        VendorFinishTime = x.VendorProcessTime.HasValue ?  x.VendorProcessTime.Value.ToString("yyyy/MM/dd") : string.Empty,
                                        SalesName = x.DealEmpName
                                    };

                        var sb = new StringBuilder();
                        sb.Append("<table style='width:740; border: 1px solid #808080' align='center'>");
                        sb.Append("<tr style='border: 1px solid #808080'>");
                        sb.Append("<td style='border: 1px solid #808080'>申換日期/時間</td>");
                        sb.Append("<td style='border: 1px solid #808080'>商家名稱</td>");
                        sb.Append("<td style='border: 1px solid #808080'>訂單編號</td>");
                        sb.Append("<td style='border: 1px solid #808080'>好康名稱(短標)</td>");
                        sb.Append("<td style='border: 1px solid #808080'>出貨日期</td>");
                        sb.Append("<td style='border: 1px solid #808080'>申換原因與說明</td>");
                        sb.Append("<td style='border: 1px solid #808080'>廠商完成時間</td>");
                        sb.Append("<td style='border: 1px solid #808080'>業務姓名</td>");
                        sb.Append("</tr>");

                        foreach (var item in query)
                        {
                            var orderHref = string.Format("{0}/controlroom/order/order_detail.aspx?oid={1}", config.SiteUrl, item.OrderGuid);
                            sb.Append("<tr style='border: 1px solid #808080'>");
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.ApplicationTime));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.SellerName));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'><a href='{0}'>{1}</a></td>", orderHref, item.OrderId));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.ItemName));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.ShipTime));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.Reason));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.VendorFinishTime));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.SalesName));
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");

                        SendNotifyMailTo17(config.AdminEmail, new[] { config.CsdEmail }, subject, sb.ToString());
                    }
                }
                catch (Exception ex)
                {
                    SendEmail(config.AdminEmail, new[] { config.AdminEmail }, subject,
                        string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
                    logger.Error(ex);
                }

                #endregion COD前一日完成換貨統計

                #region COD所有換貨未完成統計

                try
                {
                    subject = string.Format("17Life換貨未完成統計<{0}>", DateTime.Today.ToString("yyyy/MM/dd"));
                    var filter = new List<string>();
                    filter.Add(string.Format("{0} = {1}", ViewPponOrderReturnList.Columns.Type, (int)OrderReturnType.Exchange));
                    filter.Add(string.Format("{0} in ({1}, {2})", ViewPponOrderReturnList.Columns.ReturnStatus, (int)OrderReturnStatus.ExchangeProcessing, (int)OrderReturnStatus.SendToSeller));
                    filter.Add(string.Format("{0} <> {1}", ViewPponOrderReturnList.Columns.VendorProgressStatus, (int)ExchangeVendorProgressStatus.ExchangeCompleted));
                    var orderLists = op.ViewPponOrderReturnListPaging(0, 0, ViewPponOrderReturnList.Columns.ReturnCreateTime, filter.ToArray());
                    if (orderLists.Any())
                    {
                        var query = from x in orderLists
                                    orderby x.SellerName, x.ReturnCreateTime
                                    select new
                                    {
                                        ItemName = x.ItemName,
                                        ApplicationTime = x.ReturnCreateTime.ToString("yyyy/MM/dd HH:mm"),
                                        SellerName = x.SellerName,
                                        OrderId = x.OrderId,
                                        OrderGuid = x.OrderGuid,
                                        ShipTime = x.OrignalShipTime.HasValue ? x.OrignalShipTime.Value.ToString("yyyy/MM/dd") : "尚未出貨",
                                        Reason = x.Reason,
                                        ProgressStatusDesc = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ExchangeVendorProgressStatus)x.VendorProgressStatus),
                                        Memo = x.VendorMemo,
                                        SalesName = x.DealEmpName
                                    };
                        var sb = new StringBuilder();
                        sb.Append("<table style='width:740; cellpadding:0; cellspacing:0; border: 1px solid #808080' align='center'>");
                        sb.Append("<tr style='border: 1px solid #808080'>");
                        sb.Append("<td style='border: 1px solid #808080'>申換日期/時間</td>");
                        sb.Append("<td style='border: 1px solid #808080'>商家名稱</td>");
                        sb.Append("<td style='border: 1px solid #808080'>訂單編號</td>");
                        sb.Append("<td style='border: 1px solid #808080'>好康名稱(短標)</td>");
                        sb.Append("<td style='border: 1px solid #808080'>出貨日期</td>");
                        sb.Append("<td style='border: 1px solid #808080'>申換原因與說明</td>");
                        sb.Append("<td style='border: 1px solid #808080'>處理進度</td>");
                        sb.Append("<td style='border: 1px solid #808080'>處理備註</td>");
                        sb.Append("<td style='border: 1px solid #808080'>業務姓名</td>");
                        sb.Append("</tr>");

                        foreach (var item in query)
                        {
                            var orderHref = string.Format("{0}/controlroom/order/order_detail.aspx?oid={1}", config.SiteUrl, item.OrderGuid);
                            sb.Append("<tr style='border: 1px solid #808080'>");
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.ApplicationTime));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.SellerName));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'><a href='{0}'>{1}</a></td>", orderHref, item.OrderId));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.ItemName));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.ShipTime));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.Reason));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.ProgressStatusDesc));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.Memo));
                            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", item.SalesName));
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");

                        SendNotifyMailTo17(config.AdminEmail, new[] { config.CsdEmail}, subject, sb.ToString());
                    }
                }
                catch (Exception ex)
                {
                    SendEmail(config.AdminEmail, new[] { config.AdminEmail }, subject,
                        string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
                    logger.Error(ex);
                }

                #endregion COD所有換貨未完成統計
            }
        }
        /// <summary>
        /// 常常有人加了很多個收件者，所以統一檢查。
        /// </summary>
        /// <param name="msg">object</param>
        /// <param name="to">mail to</param>
        private void AddMsgTo(MailMessage msg, string to)
        {
            if (RegExRules.IsEmailContainsSeperator(to))
            {
                msg.To.Add(RegExRules.CheckMultiEmail(to));
            }
            else
            {
                if (RegExRules.CheckEmail(to))
                {
                    msg.To.Add(to);
                }
            }
        }

        private string ReplaceLine(string subject)
        {
            subject = subject.Replace('\r', ' ').Replace('\n', ' '); //移除換行字元
            return subject;
        }

        private void SendPartialPaymentNotice(string seller, string sellerMail, string ProdeuctList)
        {
            try
            {
                var msg = new MailMessage();
                var template = TemplateFactory.Instance().GetTemplate<PartiallyPaymentNoticeMail>();
                template.ProdeuctList = ProdeuctList;
                template.Seller = seller;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;
                msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                AddMsgTo(msg, sellerMail);
                msg.To.Add(config.FinanceAccountsEmail);
                msg.Subject = string.Format("17Life暫付七成通知({0})", seller);
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Normal);
            }
            catch (Exception ex)
            {
                logger.Error("DealNotify running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

        private void SendExchangeOrderNotifyMail(string seller, string sellerMail, string orderList)
        {
            try
            {
                if (RegExRules.CheckEmail(sellerMail))
                {                    
                    var msg = new MailMessage();
                    var template = TemplateFactory.Instance().GetTemplate<ExchangeOrderNotifyMail>();
                    template.ExchangeOrderList = orderList;
                    template.Seller = seller;
                    template.SiteUrl = config.SiteUrl;
                    template.SiteServiceUrl = config.SiteServiceUrl;
                    msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                    AddMsgTo(msg, sellerMail);
                    msg.Subject = string.Format("17Life每日換貨統計通知<{0}>", DateTime.Today.ToString("yyyy/MM/dd"));
                    msg.Body = template.ToString();
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Normal);
                }
            }
            catch (Exception ex)
            {
                logger.Error("DealNotify running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

        private void SendNotifyMailTo17(string from, IEnumerable<string> to, string subject, string mailContent)
        {
            try
            {                
                var msg = new MailMessage();
                var template = TemplateFactory.Instance().GetTemplate<NotifyMailTo17>();
                template.MailContent = mailContent;
                msg.From = new MailAddress(from);
                foreach (string s in to)
                {
                    msg.To.Add(s);
                }
                msg.Subject = subject;
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Async);
            }
            catch (Exception ex)
            {
                logger.Error("DealNotify running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

        private void SendEmail(string from, IEnumerable<string> to, string subject, string body, bool ishtml = true)
        {
            try
            {
                var msg = new MailMessage();

                foreach (string s in to)
                {
                    msg.To.Add(s);
                }
                msg.Subject = subject;
                msg.From = new MailAddress(from);
                msg.IsBodyHtml = ishtml;
                msg.Body = body;
                PostMan.Instance().Send(msg, SendPriorityType.Async);
            }
            catch (Exception ex)
            {
                logger.Error("DealNotify running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
}

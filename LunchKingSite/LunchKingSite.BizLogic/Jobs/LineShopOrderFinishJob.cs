﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Jobs
{
    public class LineShopOrderFinishJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(LineShopOrderFinishJob));
        private static ISysConfProvider config;
        private static LineShopOrderFinishJob theJob;

        static LineShopOrderFinishJob()
        {
            theJob = new LineShopOrderFinishJob();
        }

        public static LineShopOrderFinishJob Instance()
        {
            return theJob;
        }

        private LineShopOrderFinishJob()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            if (!config.EnableLineShop) return;
            string result = string.Empty;
            //每日回傳前一日可請款/發點訂單
            DateTime finishdate = DateTime.Today.AddDays(-1);

            OrderFacade.LineShopOrderFinishAPI(finishdate);
        }
    }
}

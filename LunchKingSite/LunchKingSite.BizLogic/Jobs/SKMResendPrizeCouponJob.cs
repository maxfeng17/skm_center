﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using System;

namespace LunchKingSite.BizLogic.Jobs
{
    public class SKMResendPrizeCouponJob : IJob
    {
        public void Execute(JobSetting js)
        {
            SkmFacade.ResendPrizeCoupon();
        }
    }
}

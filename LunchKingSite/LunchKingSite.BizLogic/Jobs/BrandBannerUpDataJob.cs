﻿using System;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.BizLogic.Facade;
using System.Net.Mail;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Jobs
{
    class BrandBannerUpDataJob : IJob
    {
        public void Execute(JobSetting js)
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            try
            { 
                MarketingFacade.UpdataBrandBanner();
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = "BrandBanner更新完成";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = "";
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = "BrandBanner更新發生問題";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }
    }
}

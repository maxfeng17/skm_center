﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using log4net;
using System.Net.Mail;

namespace LunchKingSite.BizLogic.Jobs
{
    class EdmStatusUpdateJob : IJob
    {
        private ILog log = LogManager.GetLogger(typeof(DiscountUseJob));
        public void Execute(JobSetting js)
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();

            try
            {
                EmailFacade.EdmStatusUpdate();
                MailMessage msg = new MailMessage();
                msg.To.Add("alan_lo@17life.com");
                msg.Subject = "EDM狀態更新成功";
                msg.From = new MailAddress("alan_lo@17life.com");
                msg.IsBodyHtml = true;
                msg.Body = "";
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add("alan_lo@17life.com");
                msg.Subject = "EDM 狀態更新 Error";
                msg.From = new MailAddress("alan_lo@17life.com");
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }
    }
}

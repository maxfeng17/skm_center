﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Data;
using System.Diagnostics;
using System.Net.Mail;
using System.Text;

namespace LunchKingSite.BizLogic.Jobs
{
    public class PaymentCharge : IJob
    {
        private static IOrderProvider op;
        private static IPponProvider pp;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(PaymentCharge));
        private StringBuilder sbLog = new StringBuilder();

        static PaymentCharge()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                string mode = js.Parameters["mode"];

                if (string.Equals(mode, "result", StringComparison.OrdinalIgnoreCase))
                {
                    CreditcardProcessor.OldNCCCResult();
                }

                #region Apply

                else if (string.Equals(mode, "apply", StringComparison.OrdinalIgnoreCase))
                {
                    Stopwatch watch = new Stopwatch();
                    watch.Start();

                    PaymentTransactionCollection paymentTransactionsforCharging = op.PaymentTransactionGetListForCharging();
                    PaymentTransactionCollection paymentTransactionsforManualCharging = op.PaymentTransactionGetListForCharging(true);
                    sbLog.AppendLine(string.Format("PaymentCharge is running. Step 1 經過 {0} 秒.", watch.Elapsed.TotalSeconds));
                    CreditcardProcessor.OldNCCCApply(paymentTransactionsforCharging);
                    sbLog.AppendLine(string.Format("PaymentCharge is running. Step 2 經過 {0} 秒.", watch.Elapsed.TotalSeconds));
                    sbLog.AppendLine();
                    sbLog.AppendLine(Environment.MachineName + ": 請款作業(單筆)");
                    sbLog.AppendLine("=========================================");

                    CreditcardProcessor.SequentialApply(paymentTransactionsforCharging, config.PaymentChargeAndRefundDebugMode);
                    CreditcardProcessor.SequentialApply(paymentTransactionsforManualCharging, config.PaymentChargeAndRefundDebugMode, true);
                    
                    sbLog.AppendLine(string.Format("PaymentCharge is running. Step 3 經過 {0} 秒.", watch.Elapsed.TotalSeconds));

                    watch.Stop();
                }

                #endregion Apply

                #region 寄發處理結果

                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = Environment.MachineName + ": PaymentCharge has run. Mode:" + mode;
                msg.From = new MailAddress(config.AdminEmail);
                msg.Body = sbLog.ToString();
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);

                #endregion 寄發處理結果
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = Environment.MachineName + ": PaymentCharge Error";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;
                if (sbLog.Length > 0)
                {
                    msg.Body = string.Format("{0}\r\n\r\n錯誤訊息: {1}\r\n{2}",
                    sbLog,
                    ex.Message, ex.StackTrace);
                }
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error(ex);
            }
        }
    }
}
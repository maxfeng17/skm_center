﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Mail;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using log4net;
using LunchKingSite.Core.Enumeration;


namespace LunchKingSite.BizLogic.Jobs
{
    public class SendMgmGiftNoticeToReceiver :IJob
    {
        private static IMGMProvider _mgmProv;
        private static IPponProvider _pponProv;
        private static IMemberProvider _memberProv;
        private static IBookingSystemProvider _bookingProvider;
        private static ISysConfProvider _config;
        private static ILog log = LogManager.GetLogger(typeof(PponDealChore));
        private static readonly EmailAgent agent = new EmailAgent();
        private static SendMgmGiftNoticeToReceiver _theJob;

        static SendMgmGiftNoticeToReceiver()
        {
            _theJob = new SendMgmGiftNoticeToReceiver();
        }

        public static SendMgmGiftNoticeToReceiver Instance()
        {
            return _theJob;
        }

        private SendMgmGiftNoticeToReceiver()
        {
            _mgmProv = ProviderFactory.Instance().GetProvider<IMGMProvider>();
            _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _memberProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _bookingProvider = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();
            _config = ProviderFactory.Instance().GetConfig();

        }

        public void Execute(JobSetting js)
        {
            try
            {
                SendToReceiverMail();
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(_config.AdminEmail);
                msg.Subject = "SendMgmGiftNoticeToReceiver Error";
                msg.From = new MailAddress(_config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }


        private void SendToReceiverMail()
        {
            DateTime today = DateTime.Today;

            //從兌換開始至兌換結束為今日起三個月內之檔次皆取出(篩選有送禮成功且無送兌換者)
            ViewPponDealCollection vpdc = _pponProv.ViewPponDealGetListByDeliverTimeStartForSendGift(DateTime.Today);

            foreach (ViewPponDeal vpd in vpdc)
            {
                //先檢查此檔次有沒有送禮狀況
                ViewMgmGiftCollection vmgc = _mgmProv.GiftGetByBidForSendMail(vpd.BusinessHourGuid);
                if (vmgc.Count>0)
                {
                    ViewPponStoreCollection stores = _pponProv.ViewPponStoreGetListByBidWithNoLock(vpd.BusinessHourGuid, VbsRightFlag.VerifyShop);
                    foreach (var sotre in stores)
                    {
                        string changedExpireDate = CouponFacade.GetFinalExpireDateContent(Guid.Empty, vpd.BusinessHourGuid, sotre.StoreGuid);
                        DateTime start = Convert.ToDateTime(vpd.BusinessHourDeliverTimeS);
                        DateTime end = !string.IsNullOrEmpty(changedExpireDate) ? Convert.ToDateTime(changedExpireDate) : Convert.ToDateTime(vpd.BusinessHourDeliverTimeE);
                        TimeSpan ts = new TimeSpan(end.Ticks - start.Ticks);
                        TimeSpan tsWithStart = new TimeSpan(today.Ticks - start.Ticks);
                        TimeSpan tsWithEnd = new TimeSpan(end.Ticks - today.Ticks);

                        //先判斷兌換區間
                        //  1. 超過3個月(91天以上)
                        //  2. 介於六周至3個月之間(43~90天之間)
                        //  3. 六周以內(42天以內)
                        MailInfo mailInfo = new MailInfo();
                        try
                        {
                            if (ts.TotalDays > 90)
                                mailInfo = ActGreaterThenOrEqualTime(vpd, tsWithStart, tsWithEnd, end);
                            else if (ts.TotalDays > 42 && ts.TotalDays <= 90)
                                mailInfo = ActWithinTime(vpd, tsWithStart, tsWithEnd, end);
                            else if (ts.TotalDays <= 42)
                                mailInfo = ActSmallerThanOrEqualTime(vpd, tsWithStart, tsWithEnd, end);
                        }
                        catch (Exception e)
                        {
                            log.Warn("MGM收禮者使用通知 - 取得 Template Error (EmailFacade.SendToReceiverMail):" + e);
                        }


                        //根據每一檔取出該檔有送禮且對方收禮但未使用者，
                        if (mailInfo.HasValue)
                        {
                            try
                            {
                                #region 使用完整領取
                                //使用完整領取
                                DataTable dtMembers = _memberProv.GetMemberListByDepartmentAndBusinessHourAndCouponForSendGift(vpd.BusinessHourGuid, sotre.StoreGuid);
                                int OriBusinessHourStatus = 0;
                                string itemName = string.Empty;
                                foreach (DataRow dr in dtMembers.Rows)
                                {
                                    OriBusinessHourStatus = _pponProv.ViewPponCouponGetFirstBusinessHourStatus(vpd.BusinessHourGuid, sotre.StoreGuid, Guid.Parse(dr["order_guid"].ToString()));
                                    //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                                    if ((OriBusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                                    {
                                        //多檔次子檔訂單需取得主檔的圖片
                                        Item mainDealContent = _pponProv.ItemGetBySubDealBid(vpd.BusinessHourGuid);
                                        if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                                        {
                                            itemName = mainDealContent.ItemName;
                                        }
                                    }
                                    else
                                    {
                                        itemName = dr["item_name"].ToString();
                                    }

                                    string userName = dr["user_name"].ToString();
                                    var email = MemberFacade.GetUserEmail(userName);
                                    if (RegExRules.CheckEmail(email))
                                    {
                                        PostMan.Instance().Send(
                                        agent.GetMailMessage(_config.ServiceEmail
                                                          , email
                                                          , mailInfo.Subject.Replace("{item_name}", itemName + dr["count"].ToString() + "份")
                                                          , mailInfo.Template
                                                                .Replace("{member_name}", dr["member_name"].ToString())
                                                                .Replace("{order_detail_item_name}", itemName + dr["count"].ToString() + "份")
                                                          , _config.ServiceName
                                            ), SendPriorityType.Immediate);
                                    }
                                }
                                #endregion


                                #region 使用快速領取
                                //使用快速領取
                                DataTable quickMember = _mgmProv.GetViewMgmGiftByStoreGuidAndBusinessHourSendGift(vpd.BusinessHourGuid, sotre.StoreGuid);
                                OriBusinessHourStatus = 0;
                                itemName = string.Empty;
                                foreach (DataRow dr in quickMember.Rows)
                                {
                                    OriBusinessHourStatus = _pponProv.ViewPponCouponGetFirstBusinessHourStatus(vpd.BusinessHourGuid, sotre.StoreGuid, Guid.Parse(dr["order_guid"].ToString()));
                                    //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                                    if ((OriBusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                                    {
                                        //多檔次子檔訂單需取得主檔的圖片
                                        Item mainDealContent = _pponProv.ItemGetBySubDealBid(vpd.BusinessHourGuid);
                                        if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                                        {
                                            itemName = mainDealContent.ItemName;
                                        }
                                    }
                                    else
                                    {
                                        itemName = dr["item_name"].ToString();
                                    }

                                    var email = dr["quick_info"].ToString();
                                    if (RegExRules.CheckEmail(email))
                                    {
                                        PostMan.Instance().Send(
                                        agent.GetMailMessage(_config.ServiceEmail
                                                          , email
                                                          , mailInfo.Subject.Replace("{item_name}", itemName + dr["count"].ToString() + "份")
                                                          , mailInfo.Template
                                                                .Replace("{member_name}", "顧客")
                                                                .Replace("{order_detail_item_name}", itemName + dr["count"].ToString() + "份")
                                                          , _config.ServiceName
                                            ), SendPriorityType.Immediate);
                                    }
                                }
                                #endregion

                            }
                            catch (Exception ex)
                            {
                                log.Warn("MGM收禮者使用通知 - 寄送 Error (EmailFacade.SendToReceiverMail):" + ex);
                            }
                        }

                    }
                }
            }
        }


        #region 超過三個月(90天)
        private MailInfo ActGreaterThenOrEqualTime(ViewPponDeal vpd, TimeSpan tsStart, TimeSpan tsEnd, DateTime end)
        {
            MailInfo result = new MailInfo();
            if (tsEnd.TotalDays > 30) //至結束前一個月
            {
                if ((int)tsStart.TotalDays % 21 == 0 && tsStart.TotalDays > 20)   //開跑後隔三周(三周、六周、九周依此類推)，取得"使用"提醒樣版
                {
                    result.Subject = string.Format("好康使用提醒通知({0}，將於{1}到期，請儘速使用)", "{item_name}", end.ToString("yyyy/MM/dd"));
                    result.Template = GetMgmGiftToReceiverWarningMail(vpd, end);
                }
                //為了測試寫出來的error function
                else
                {
                    result.Subject = string.Format("好康即將到期通知({0}，將於{1}到期，請儘速使用)", "{item_name}", end.ToString("yyyy/MM/dd"));
                    result.Template = GetMgmGiftToReceiverExpiredMail(vpd, end);
                }
            }
            else if (((int)tsEnd.TotalDays).EqualsAny(7, 30))  //結束前1週、一個月，取得"到期"提醒樣版
            {
                result.Subject = string.Format("好康即將到期通知({0}，將於{1}到期，請儘速使用)", "{item_name}", end.ToString("yyyy/MM/dd"));
                result.Template = GetMgmGiftToReceiverExpiredMail(vpd, end);
            }
            return result;
        }
        #endregion

        #region 介於六周至三個月之間(43~90天)
        private MailInfo ActWithinTime(ViewPponDeal vpd, TimeSpan tsStart, TimeSpan tsEnd, DateTime end)
        {
            MailInfo result = new MailInfo();

            if ((int)tsStart.TotalDays == 21)    //開始後3週，取得"使用"提醒樣版
            {
                result.Subject = string.Format("好康使用提醒通知({0}，將於{1}到期，請儘速使用)", "{item_name}", end.ToString("yyyy/MM/dd"));
                result.Template = GetMgmGiftToReceiverWarningMail(vpd, end);
            }

            if (((int)tsEnd.TotalDays).EqualsAny(7, 14))   //結束前1、2週，取得"到期"提醒樣版
            {
                result.Subject = string.Format("好康即將到期通知({0}，將於{1}到期，請儘速使用)", "{item_name}", end.ToString("yyyy/MM/dd"));
                result.Template = GetMgmGiftToReceiverExpiredMail(vpd, end);
            }
            return result;
        }
        #endregion

        #region 六周以內(42天)
        private MailInfo ActSmallerThanOrEqualTime(ViewPponDeal vpd, TimeSpan tsStart, TimeSpan tsEnd, DateTime end)
        {
            MailInfo rtnDic = new MailInfo();

            if ((int)tsStart.TotalDays == 7)    //開始後1週，取得"使用"提醒樣版
            {
                rtnDic.Subject = string.Format("好康使用提醒通知({0}，將於{1}到期，請儘速使用)", "{item_name}", end.ToString("yyyy/MM/dd"));
                rtnDic.Template = GetMgmGiftToReceiverWarningMail(vpd, end);
            }

            if ((int)tsEnd.TotalDays == 7)   //結束前1週，取得"到期"提醒樣版
            {
                rtnDic.Subject = string.Format("好康即將到期通知({0}，將於{1}到期，請儘速使用)", "{item_name}", end.ToString("yyyy/MM/dd"));
                rtnDic.Template = GetMgmGiftToReceiverExpiredMail(vpd, end);
            }
            return rtnDic;
        }
        #endregion

        #region 好康到期提醒
        private string GetMgmGiftToReceiverExpiredMail(ViewPponDeal vpd, DateTime end)
        {
            string rtnTemp = string.Empty;
            try
            {
                MgmGiftToReceiverExpiredMail template = TemplateFactory.Instance().GetTemplate<MgmGiftToReceiverExpiredMail>();
                template.TheMemberName = "{member_name}";
                template.ItemName = "{order_detail_item_name}";
                template.TheDeal = vpd;
                template.PromoDeals = OrderFacade.GetPromoDealsForNoticeMail(vpd);
                template.ServerConfig = _config;
                template.EndTime = end;
                rtnTemp = template.ToString();
            }
            catch (Exception e)
            {
                log.Warn("好康到期提醒通知 Error:" + e);
            }
            return rtnTemp;
        }
        #endregion

        #region 好康使用提醒
        private string GetMgmGiftToReceiverWarningMail(ViewPponDeal vpd, DateTime end)
        {
            string rtnTemp = string.Empty;
            try
            {
                MgmGiftToReceiverWarningMail template = TemplateFactory.Instance().GetTemplate<MgmGiftToReceiverWarningMail>();
                template.TheMemberName = "{member_name}";
                template.TheDeal = vpd;
                template.ItemName = vpd.ItemName;
                template.EndTime = end;
                rtnTemp = template.ToString();
            }
            catch (Exception e)
            {
                log.Warn("好康使用提醒通知 Error:" + e);
            }
            return rtnTemp;
        }
        #endregion

        internal class MailInfo
        {
            public MailInfo()
            {
                Subject = "";
                Template = "";
            }

            public bool HasValue
            {
                get { return string.IsNullOrEmpty(Subject) == false; }
            }

            public string Subject { get; set; }
            public string Template { get; set; }
        }

    }


}

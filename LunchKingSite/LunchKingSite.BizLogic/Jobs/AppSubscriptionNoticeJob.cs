﻿using System;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    class AppSubscriptionNoticeJob : IJob
    {
        private static ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(AppSubscriptionNoticeJob));

        public void Execute(JobSetting js)
        {
            try
            {
                //推播動作
                NotificationFacade.PushMemberCollectDeal(DateTime.Now.AddHours(_config.SubscriptionNoticeLimitHours));
            }
            catch (Exception ex)
            {
                logger.Error("AppSubscriptionNoticeJob Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
}

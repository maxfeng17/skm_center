using System;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Constant;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs
{
    public class NightlyCleaner : IJob
    {
        protected static ISellerProvider sp;
        protected static IOrderProvider op;
        protected static IItemProvider ip;
        protected static ISysConfProvider conf;
        private static ILog log = LogManager.GetLogger(typeof(NightlyCleaner));

        static NightlyCleaner()
        {
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            conf = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            // clean up shopping cart
            op.ShoppingCartPurge(DateTime.Today.AddDays(conf.ShoppingCartReserveDays * -1));

            // unlock all locked orders
            op.OrderUnlockAll();

            // purge expired group order
            this.PurgeExpiredGroupOrder();
        }

        /// <summary>
        /// Purges the expired group order.
        /// </summary>
        private void PurgeExpiredGroupOrder()
        {
            DateTime deadline = DateTime.Today.AddDays(conf.GroupOrderReserveDays * -1);
            GroupOrderCollection gos = op.GroupOrderGetList(GroupOrder.Columns.Status + " is null");
            foreach (GroupOrder g in gos)
                if (g.CloseTime < deadline)
                    op.OrderDelete(g.OrderGuid.Value);

            // purge completed group order records that are over X days old to slim db
            // not an efficient way to do mass delete, but is able do the job for now
            int serviceUserId = MemberFacade.GetUniqueId(conf.ServiceEmail, true);
            OrderCollection oCol = op.OrderGetListPaging(1, -1, OrderStatus.GroupOrder | OrderStatus.Complete, null,
                                                         Order.Columns.CreateTime + " < " + deadline.ToShortDateString(),
                                                         Order.Columns.UserId + " <> " + serviceUserId);
            foreach (Order o in oCol)
                op.GroupOrderDelete(GroupOrder.Columns.OrderGuid, o.Guid);
        }
    }
}

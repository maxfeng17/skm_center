﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System.Net.Mail;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component.Template;

namespace LunchKingSite.BizLogic.Jobs
{
    public class MailToPhotographer : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(PaymentCharge));
        protected static IOrderProvider op;
        protected static ISellerProvider sp;
        protected static IPponProvider pp;
        protected static ISysConfProvider config;

        static MailToPhotographer()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            //20180430在地提出停止拍排程,若有需要再加入config
            MailToPhotographerJob();
        }

        public static void MailToPhotographerJob()
        {
            //組成資料並備份、發送
            try
            {
                DateTime currentTime = new System.DateTime();
                DateTime sinceTime = new System.DateTime();
                currentTime = DateTime.Now;
                sinceTime = Convert.ToDateTime("2016/06/01");

                bool dataIsNullFalg = true; //有無資料旗標
                const string dirUrl = "C:\\Temp\\PhotographerDataTable\\";
                string fileName = string.Format("{0}_約拍資料.csv", currentTime.ToString("yyyyMMdd"));
                string heander = "提案單狀態,業務區域,業務,單號,商家名稱,商品內容,拍攝地點,現場聯絡人姓名,現場聯絡人電話,現場聯絡人手機,議重點拍攝項目/拍攝需求,店家希望拍照時間,參考網址/參考資料,";
                //目前狀態為約拍中，且非攝影確認
                var datas = sp.ViewProposalSellerGetListForPhotohrapher(sinceTime, currentTime);
                List<string> rowList = new List<string>();

                foreach (var item in datas)
                {
                    //直接在sql撈
                    //bool PhotohrapherAppoint = Helper.IsFlagSet(item.PhotographerAppointFlag,
                    //    ProposalPhotographer.PhotographerAppoint); //約拍中
                    //bool PhotohrapherAppointCheck = Helper.IsFlagSet(item.PhotographerAppointFlag,
                    //    ProposalPhotographer.PhotographerAppointCheck); //已約拍
                    //bool PhotohrapherAppointCancel = Helper.IsFlagSet(item.PhotographerAppointFlag,
                    //    ProposalPhotographer.PhotographerAppointCancel); //取消約拍
                    //bool PhotographerNeed = Helper.IsFlagSet(item.SpecialFlag, ProposalSpecialFlag.Photographers); //需拍攝

                    ////檢查狀態是否為("需約拍"或"約拍中)"且(不為"已約拍"或"約拍取消")
                    //if ((!PhotographerNeed && !PhotohrapherAppoint) || PhotohrapherAppointCheck || PhotohrapherAppointCancel)
                    //    continue;

                    dataIsNullFalg = false; //有資料

                    Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(item.SpecialFlagText);

                    string photoString = string.Empty;

                    //防呆-檢查索引值是否有Photographers
                    if (specialText.Any(x => x.Key == ProposalSpecialFlag.Photographers)) { 
                        photoString = specialText[ProposalSpecialFlag.Photographers];
                    }

                    string photoStatus = "送出排檔"; //最多只到送出排檔

                    //提案單狀態
                    if (Helper.IsFlagSet(item.BusinessCreateFlag,
                        ProposalBusinessCreateFlag.SettingCheck))
                    {
                        photoStatus = "送出排檔";
                    }
                    else if (Helper.IsFlagSet(item.BusinessCreateFlag,
                        ProposalBusinessCreateFlag.Created))
                    {
                        photoStatus = "建檔確認";
                    }
                    else if (Helper.IsFlagSet(item.ApproveFlag,
                        ProposalApproveFlag.QCcheck))
                    {
                        photoStatus = "QC確認";
                    }
                    else if (Helper.IsFlagSet(item.ApplyFlag,
                        ProposalApplyFlag.Apply))
                    {
                        photoStatus = "提案申請";
                    }
                    else if (!Helper.IsFlagSet(item.ApplyFlag,
                        ProposalApplyFlag.Apply))
                    {
                        photoStatus = "草稿";
                    }

                    //取代段行 避免格式錯亂
                    photoString = photoString.Replace("\r", "");
                    photoString = photoString.Replace("\n", "");

                    //csv資料如下 "攝影師名稱|時間|先生小姐|拍攝地點..."
                    List<string> bodyList = photoString.Split('|').ToList();
                    System.Text.StringBuilder rowListItem = new System.Text.StringBuilder();

                    string photographPlace = string.Empty;    //拍攝地點
                    string contactName = string.Empty;        //現場聯絡人
                    string sex = string.Empty;                //先生/小姐
                    string contactPhone = string.Empty;       //現場聯絡人電話
                    string contactMobilePhone = string.Empty; //現場聯絡人手機
                    string focalPoint = string.Empty;         //建議重點拍攝項目/拍攝需求
                    string photographTime = string.Empty;     //店家希望拍照時間
                    string reference = string.Empty;          //參考網址/資料

                    rowListItem.Append(photoStatus + "," +
                                       item.DeptName + "," +
                                       item.EmpName + "," +
                                       item.Id + "," +
                                       item.SellerName + ",");
                    if (item.DeliveryType == (int) DeliveryType.ToShop || item.DealType == (int) ProposalDealType.Travel)
                    {
                        rowListItem.Append(item.BrandName + ",");

                        foreach (var bodyItem in bodyList)
                        {
                            switch (bodyList.IndexOf(bodyItem))
                            {
                                case 0:
                                    photographPlace = bodyItem + ",";
                                    break;
                                case 1:
                                    contactPhone = "'" + bodyItem + ",";
                                    break;
                                case 2:
                                    contactMobilePhone = "'" + bodyItem + ",";
                                    break;
                                case 3:
                                    focalPoint = bodyItem + ",";
                                    break;
                                case 4:
                                    photographTime = bodyItem + ",";
                                    break;
                                case 5:
                                    reference = bodyItem + ",";
                                    break;
                                case 6:
                                    contactName = bodyItem;
                                    break;
                                case 7:
                                    if (bodyItem == "F")
                                    {
                                        sex = "小姐,";
                                    }
                                    else if (bodyItem == "M")
                                    {
                                        sex = "先生,";
                                    }
                                    break;
                            }
                        }
                        rowListItem.Append(photographPlace + contactName + sex + contactPhone + contactMobilePhone + focalPoint + photographTime + reference);
                    }
                    else
                    {
                        rowListItem.Append(item.BrandName + ",,,,,");

                        foreach (var bodyItem in bodyList)
                        {

                            switch (bodyList.IndexOf(bodyItem))
                            {
                                case 0: //拍攝需求
                                    focalPoint = bodyItem + ",,";
                                    break;
                                case 1: //參考資料
                                    reference = bodyItem + ",";
                                    break;
                            }
                        }
                        rowListItem.Append(focalPoint + reference);
                    }
                    rowList.Add(rowListItem.ToString());
                }

                //Sendmail
                SendMailWithDocumentFactory sendMailWithDocument = new SendMailWithDocumentFactory();
                sendMailWithDocument.Header = heander;
                sendMailWithDocument.RowList = rowList;


                //主要收件者
                List<MailAddress> mailTo = new List<MailAddress>();
                string[] email = config.ProposalPhotographerAppointEmail.Split(';');
                for (int i = 0; i < email.Count(); i++)
                {
                    mailTo.Add(new MailAddress(email[i]));
                }

                //CC收件者
                List<MailAddress> mailCC = new List<MailAddress>();
                mailCC.Add(new MailAddress(config.SupportingLeaderEmail));

                string[] emailCC = config.ProposalPhotographerAppointCCEmail.Split(';');
                for (int i = 0; i < emailCC.Count(); i++)
                {
                    mailCC.Add(new MailAddress(emailCC[i]));
                }
                string subject = "17Life 約拍通知信-約拍資料";
                string mailBody = dataIsNullFalg ? "無符合約拍的資料。" : "約拍資料如附件。";

                sendMailWithDocument.csvModelGen(dirUrl, fileName).sendMail(mailTo, mailCC, subject, mailBody); //創建檔案後送電子郵件
            }
            catch (Exception ex)
            {
                logger.Error(ex + "17life約拍通知 約拍資料發送失敗");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Transactions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using log4net;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.BizLogic.Jobs
{
    public class SendVerificationNoticeEmail : IJob
    {
        private static IOrderProvider op;
        private static IMemberProvider mp;
        private static ISysConfProvider cp;
        private static ILog logger = LogManager.GetLogger(typeof(SendVerificationNoticeEmail));

        /// <summary>
        /// 待核銷檔次列表中的tr版型
        /// </summary>
        private string _newTRSet
        {
            get
            {
                return @"
<tr>
	<td>{0}</td>
	<td style='text-align:left;'>{1}</td>
	<td>{2}</td>
	<td>{3}</td>
</tr>";
            }
        }
        

        static SendVerificationNoticeEmail()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            cp = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            List<ViewPponDeal> dealList = SellerFacade.GetRemindVerificationDealList()
                .Where(x => !string.IsNullOrWhiteSpace(x.SellerEmail) && //排除Email沒填
                            !Helper.IsFlagSet(x.BusinessHourStatus, BusinessHourStatus.ComboDealMain))//排除多檔次的主檔
                .ToList<ViewPponDeal>();

            List<MailMessage> mailList = ProduceToSendMail(dealList);

            try
            {
                PostMan.Instance().Send(ProduceNotifyAdminMail("start"), SendPriorityType.Immediate);
                foreach (MailMessage msg in mailList)
                {
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);  
                }
                PostMan.Instance().Send(ProduceNotifyAdminMail("end"), SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                logger.Error("SendVerificationNoticeEmail running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

        /// <summary>
        /// 產生待寄的核銷通知信
        /// </summary>
        /// <param name="dealList">賣家、檔次對應資料清單</param>
        /// <returns></returns>
        private List<MailMessage> ProduceToSendMail(List<ViewPponDeal> dealList)
        {
            List<MailMessage> mailList = new List<MailMessage>();

            List<ViewPponDeal> sellerList = dealList.GroupBy(x => x.SellerId).Select(x => x.FirstOrDefault()).ToList();

            foreach (ViewPponDeal seller in sellerList)
            {
                #region 設定對應Template的Model內容
                VerificationNoticeMailInformation mailInfo = new VerificationNoticeMailInformation();
                mailInfo.siteUrl = cp.SiteUrl;
                mailInfo.sellerName = seller.SellerName;

                #region 組tbody內html (待核銷檔次列表)
                List<ViewPponDeal> sellerDeals = dealList.Where(x => x.SellerId == seller.SellerId).ToList();
                foreach (ViewPponDeal deal in sellerDeals)
                {
                    DateTime? endDate = deal.ChangedExpireDate ?? deal.BusinessHourDeliverTimeE;
                    mailInfo.tbody_DealList += string.Format(_newTRSet, deal.UniqueId, deal.ItemName,
                                                 string.Format("{0:yyyy/MM/dd}~{1:yyyy/MM/dd}", deal.BusinessHourDeliverTimeS, endDate),
                                                 string.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(endDate).AddDays(2)));
                }
                #endregion

                #endregion

                #region 初始化Template並將Model資料注入
                var template = TemplateFactory.Instance().GetTemplate<VerificationNoticeEmail>();
                template.siteUrl = mailInfo.siteUrl;
                template.sellerName = mailInfo.sellerName;
                template.tbody_DealList = mailInfo.tbody_DealList;

                #endregion

                #region 設定MailMessage
                MailMessage msg = new MailMessage();
                msg.Subject = "17Life核銷期限提醒";
                msg.From = new MailAddress(cp.CodLocalEmail);
                foreach (string email in seller.SellerEmail.Split(new char[] { ',', ';', '/' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    try
                    {
                        string tempEmail = seller.SellerEmail.TrimStart().TrimEnd();
                        if (!string.IsNullOrWhiteSpace(tempEmail))
                        {
                            msg.To.Add(new MailAddress(tempEmail));
                        }
                    }
                    catch (Exception)
                    {
                        //錯誤的email address格式
                    }
                }
                msg.IsBodyHtml = true;
                msg.Body = Convert.ToString(template);
                #endregion

                if (msg.To.Count > 0)
                {
                    mailList.Add(msg);
                }
            }

            return mailList;
        }

        /// <summary>
        /// 通知管理者開始/結束寄送通知信
        /// </summary>
        /// <param name="type">start/end</param>
        private MailMessage ProduceNotifyAdminMail(string type)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(cp.AdminEmail);
            if (type == "start")
                msg.Subject = "17Life核銷期限提醒信發送開始";
            else
                msg.Subject = "17Life核銷期限提醒信發送結束";
            msg.To.Add(new MailAddress(cp.AdminEmail));
            msg.IsBodyHtml = true;

            return msg;
        }
    }
    
}
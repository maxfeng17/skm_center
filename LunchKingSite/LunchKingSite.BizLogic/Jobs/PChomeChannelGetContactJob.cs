﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Component;
using log4net;
using LunchKingSite.Core.Interface;
using System.Text;
using System.Net;
using System.IO;
using LunchKingSite.Core.Enumeration;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Models.PchomeChannel;
using System.Web.Hosting;
using System.Collections.Specialized;
using LunchKingSite.BizLogic.Models.CustomerService;

namespace LunchKingSite.BizLogic.Jobs
{
    public class PChomeChannelGetContactJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(PChomeChannelGetContactJob).Name);
        private static IOAuthProvider oauthp = ProviderFactory.Instance().GetProvider<IOAuthProvider>();
        private static IChannelProvider chp = ProviderFactory.Instance().GetProvider<IChannelProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        
        public void Execute(JobSetting js)
        {
            GetContact();
            ReplyContact();

            GetComplaint();
            ReplyComplaint();


        }

        /// <summary>
        /// 取得PChome聯絡單
        /// </summary>
        private void GetContact()
        {
            try
            {
                string startDate = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
                string endDate = DateTime.Now.ToString("yyyy/MM/dd");
                PChomeContactList list = PChomeChannelAPI.GetContactList(startDate, endDate);
                if (list.TotalRows > 0)
                {
                    string contactId = string.Join(",", list.Rows.Select(x => x.Id));
                    List<PChomeContactDetail> details = PChomeChannelAPI.GetContactDetail(contactId);
                    int userId = mp.MemberUniqueIdGet(config.TmallDefaultUserName);
                    //OauthClient client = oauthp.ClientGetByName("PChome");
                    PchomeChannelContactCollection contacts = new PchomeChannelContactCollection();
                    foreach (PChomeContactDetail detail in details)
                    {
                        try
                        {
                            //test
                            //detail.OrderId = "20190802-2";
                            //detail.Id = DateTime.Now.ToString("yyyyMMddHHss");

                            //string orderId = detail.OrderId;//要再轉換成17的定編
                            //Order order = op.OrderGetByOrderId(orderId);
                            //OrderCorresponding od = op.OrderCorrespondingListGetByOrderGuid(order.Guid);
                            //if (od != null && od.IsLoaded)
                            //{
                            //    PchomeChannelContact pcp = chp.PchomeChannelContactGetById(detail.Id);
                            //    if (!pcp.IsLoaded)
                            //    {
                            //        ApiSendData sendData = new ApiSendData();
                            //        sendData.UserId = od.UserId.ToString();
                            //        sendData.Mobile = od.Mobile;
                            //        sendData.Email = config.TmallDefaultUserName;
                            //        sendData.Name = "天貓";
                            //        sendData.Content = detail.MsgFromPchome;
                            //        sendData.Source = (int)Source.web;
                            //        sendData.OrderGuid = od.OrderGuid.ToString();
                            //        sendData.CategoryId = 0;//無法與PChome對應
                            //        sendData.IssueFromType = (int)IssueFromType.PChome;

                            //        string serviceNo = "";
                            //        bool result = CustomerServiceFacade.CreateOrderIssue(config.TmallDefaultUserName, od.UserId, sendData, true, out serviceNo);
                            //        if (result)
                            //        {
                            //            PchomeChannelContact contact = new PchomeChannelContact();
                            //            contact.ContactId = detail.Id;
                            //            contact.ServiceNo = serviceNo;
                            //            contact.CreateId = "sys";
                            //            contact.CreateTime = DateTime.Now;
                            //            contacts.Add(contact);
                            //        }
                            //        else
                            //        {
                            //            logger.Error("PChome聯絡單寫入失敗 orderRelatedId=" + detail.OrderId);
                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    logger.Error("PChome聯絡單找無對應訂單 orderRelatedId=" + detail.OrderId);
                            //}

                            PchomeChannelContact pcp = chp.PchomeChannelContactGetById(detail.Id);
                            string prodId = detail.ProdId;
                            Guid bid = chp.PChomeChannelProdGetByProdId(prodId).Bid;
                            int unieuqeId = pp.DealPropertyGet(bid).UniqueId;
                            if (!pcp.IsLoaded)
                            {
                                ApiSendData sendData = new ApiSendData();
                                sendData.UserId = userId.ToString();
                                sendData.Mobile = "";
                                sendData.Email = config.TmallDefaultUserName;
                                sendData.Name = "天貓";
                                sendData.Content = "PChome ID：" + detail.Id.ToString() + "<br />" +
                                                   "PChome OrderId：" + detail.OrderId.ToString() + "<br />" +
                                                   "17Life 檔號：" + unieuqeId.ToString() + "<br />" +
                                                   "客戶來信內容：" + detail.MsgFromCustomer + "<br />" +
                                                   "PChome異動內容：" + detail.MsgFromPchome;
                                sendData.Source = (int)Source.web;
                                sendData.CategoryId = 0;//無法與PChome對應
                                sendData.IssueFromType = (int)IssueFromType.PChome;

                                ServiceData service;
                                string errMsg;
                                bool result = CustomerServiceFacade.CreateIssue(config.TmallDefaultUserName, userId, sendData, out service, out errMsg);
                                if (result)
                                {
                                    PchomeChannelContact contact = new PchomeChannelContact();
                                    contact.ContactId = detail.Id;
                                    contact.ServiceNo = service.ServiceNo;
                                    contact.CreateId = "sys";
                                    contact.CreateTime = DateTime.Now;
                                    contacts.Add(contact);
                                }
                                else
                                {
                                    logger.Error("PChome聯絡單寫入失敗 id=" + detail.Id);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            logger.Error("PChome聯絡單寫入失敗 id=" + detail.Id, ex);
                        }

                    }
                    chp.PChomeChannelContactSetList(contacts);
                }
                
            }
            catch (Exception ex)
            {
                logger.Error("PChome聯絡單GetContact失敗", ex);
            }


        }

        /// <summary>
        /// 回復聯絡單
        /// </summary>
        private void ReplyContact()
        {
            PchomeChannelContactCollection toReply = chp.PchomeChannelContactGetToReply();
            foreach (PchomeChannelContact reply in toReply)
            {
                try
                {
                    List<CustomerServiceOutsideLog> logs = sp.GetCustomerServiceOutsideLogBySerivceNo(reply.ServiceNo).Where(x => x.Method == (int)methodConvert.online).ToList();
                    if (logs.Any())
                    {
                        PChomeContactDetail detail = PChomeChannelAPI.GetContactDetail(reply.ContactId).FirstOrDefault();
                        if (detail.Status != "Closed")
                        {
                            bool result = PChomeChannelAPI.ReplyContact(reply.ContactId, string.Join("<br>", logs.OrderBy(x => x.CreateTime).Select(x => x.Content)));
                            if (result)
                            {
                                reply.ReplyTime = DateTime.Now;
                                reply.ModifyId = "sys";
                                reply.ModifyTime = DateTime.Now;
                                chp.PchomeChannelContactSet(reply);
                            }
                            else
                            {
                                logger.Error("PChome聯絡單回覆失敗 contact_id=" + reply.ContactId);
                            }
                        }
                        else
                        {
                            //可能PChome自行結案,17不需再回覆
                            reply.ReplyTime = DateTime.Now;
                            reply.ModifyId = "PChome";
                            reply.ModifyTime = DateTime.Now;
                            chp.PchomeChannelContactSet(reply);
                        }
                        
                    }
                    
                }
                catch (Exception ex)
                {
                    logger.Error("PChome聯絡單回覆失敗 contact_id=" + reply.ContactId, ex);
                }
                
            }
        }

        /// <summary>
        /// 取得PChome客訴單
        /// </summary>
        private void GetComplaint()
        {
            try
            {
                string startDate = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
                string endDate = DateTime.Now.ToString("yyyy/MM/dd");
                PChomeComplaintList list = PChomeChannelAPI.GetComplaintList(startDate, endDate);
                if (list.TotalRows > 0)
                {
                    string complaintId = string.Join(",", list.Rows.Select(x => x.UId));
                    List<PChomeComplaintDetail> details = PChomeChannelAPI.GetComplaintDetail(complaintId);
                    int userId = mp.MemberUniqueIdGet(config.TmallDefaultUserName);
                    //OauthClient client = oauthp.ClientGetByName("PChome");
                    PchomeChannelComplaintCollection complaints = new PchomeChannelComplaintCollection();
                    foreach (PChomeComplaintDetail detail in details)
                    {
                        try
                        {
                            //test
                            //detail.OrderId = "20190802-3";
                            //detail.Id = "S" + DateTime.Now.ToString("yyyyMMdd") + new Random().Next(11111,99999);

                            //string orderId = detail.OrderId;//要再轉換成17的定編
                            //Order order = op.OrderGetByOrderId(orderId);
                            //OrderCorresponding od = op.OrderCorrespondingListGetByOrderGuid(order.Guid);
                            //if (od != null && od.IsLoaded)
                            //{
                            //    string uid = detail.Id + "-" + detail.OrderId;//客訴單+訂單編號
                            //    PchomeChannelComplaint pcp = chp.PchomeChannelComplaintGetById(uid);
                            //    if (!pcp.IsLoaded)
                            //    {
                            //        ApiSendData sendData = new ApiSendData();
                            //        sendData.UserId = od.UserId.ToString();
                            //        sendData.Mobile = od.Mobile;
                            //        sendData.Email = config.TmallDefaultUserName;
                            //        sendData.Name = "天貓";
                            //        sendData.Content = detail.Question;
                            //        sendData.Source = (int)Source.web;
                            //        sendData.OrderGuid = od.OrderGuid.ToString();
                            //        sendData.CategoryId = 0;//無法與PChome對應
                            //        sendData.IssueFromType = (int)IssueFromType.PChome;

                            //        string serviceNo = "";
                            //        bool result = CustomerServiceFacade.CreateOrderIssue(config.TmallDefaultUserName, od.UserId, sendData, true, out serviceNo);
                            //        if (result)
                            //        {
                            //            PchomeChannelComplaint complaint = new PchomeChannelComplaint();
                            //            complaint.ComplaintId = uid;
                            //            complaint.ServiceNo = serviceNo;
                            //            complaint.CreateId = "sys";
                            //            complaint.CreateTime = DateTime.Now;
                            //            complaints.Add(complaint);
                            //        }
                            //        else
                            //        {
                            //            logger.Error("PChome客訴單寫入失敗 orderRelatedId=" + detail.OrderId);
                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    logger.Error("PChome客訴單找無對應訂單 orderRelatedId=" + detail.OrderId);
                            //}

                            string uid = detail.Id + "-" + detail.OrderId;
                            PchomeChannelComplaint pcp = chp.PchomeChannelComplaintGetById(uid);
                            //避免PChome重新備註的案件重複拋回，以最新案件檢查ReplyTime
                            if (!pcp.IsLoaded || pcp.ReplyTime != null)
                            {
                                ApiSendData sendData = new ApiSendData();
                                sendData.UserId = userId.ToString();
                                sendData.Mobile = "";
                                sendData.Email = config.TmallDefaultUserName;
                                sendData.Name = "天貓";
                                sendData.Content = "PChome ID：" + detail.Id.ToString() + "<br />" +
                                                   "PChome OrderId：" + detail.OrderId.ToString() + "<br />" +
                                                   "客訴問題：" + detail.Question + "<br />" +
                                                   "訂單明細資訊：" + detail.OrderInfo;
                                sendData.Source = (int)Source.web;
                                sendData.CategoryId = 0;//無法與PChome對應
                                sendData.IssueFromType = (int)IssueFromType.PChome;
                                if (pcp.IsLoaded)
                                {
                                    //之前回覆過的，PC又備註，17建立為相關案件
                                    PChomeComplaintReply reply = PChomeChannelAPI.GetComplaintReply(detail.Id).FirstOrDefault();
                                    sendData.ServiceNo = pcp.ServiceNo;
                                    sendData.Content = "PChome ID：" + detail.Id.ToString() + "<br />" +
                                                       "PChome OrderId：" + detail.OrderId.ToString() + "<br />" +
                                                       "PChome備註：" + reply.Reply.LastOrDefault().Remark;
                                }

                                ServiceData service;
                                string errMsg;
                                bool result = CustomerServiceFacade.CreateIssue(config.TmallDefaultUserName, userId, sendData, out service, out errMsg);
                                if (result)
                                {
                                    PchomeChannelComplaint complaint = new PchomeChannelComplaint();
                                    complaint.ComplaintId = uid;
                                    complaint.ServiceNo = service.ServiceNo;
                                    complaint.CreateId = "sys";
                                    complaint.CreateTime = DateTime.Now;
                                    complaints.Add(complaint);
                                }
                                else
                                {
                                    logger.Error("PChome客訴單寫入失敗 id=" + detail.Id);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            logger.Error("PChome客訴單寫入失敗 id=" + detail.Id, ex);
                        }
                    }
                    chp.PchomeChannelComplaintSetList(complaints);
                }
                
            }
            catch (Exception ex)
            {
                logger.Error("PChome客訴單GetComplaint失敗", ex);
            }


        }

        /// <summary>
        /// 回復客訴單
        /// </summary>
        private void ReplyComplaint()
        {
            PchomeChannelComplaintCollection toReply = chp.PchomeChannelComplaintGetToReply();
            if (toReply.Any())
            {
                string startDate = toReply.Min(x => x.CreateTime).ToString("yyyy/MM/dd");
                string endDate = DateTime.Now.ToString("yyyy/MM/dd");
                PChomeComplaintList list = PChomeChannelAPI.GetComplaintList(startDate, endDate);
                List<string> uidList = list.Rows.Select(x => x.UId).ToList();

                foreach (PchomeChannelComplaint reply in toReply)
                {
                    try
                    {
                        List<CustomerServiceOutsideLog> logs = sp.GetCustomerServiceOutsideLogBySerivceNo(reply.ServiceNo).Where(x => x.Method == (int)methodConvert.online).ToList();
                        if (logs.Any())
                        {
                            if (uidList.Contains(reply.ComplaintId))
                            {
                                string id = reply.ComplaintId.Split("-")[0];
                                string orderId = reply.ComplaintId.Split("-")[1] + "-" + reply.ComplaintId.Split("-")[2];
                                ComplaintReply replyContent = new ComplaintReply
                                {
                                    OrderId = orderId,
                                    Reply = string.Join("<br>", logs.OrderBy(x => x.CreateTime).Select(x => x.Content)),
                                };
                                bool result = PChomeChannelAPI.ReplyComplaint(id, replyContent);
                                if (result)
                                {
                                    reply.ReplyTime = DateTime.Now;
                                    reply.ModifyId = "sys";
                                    reply.ModifyTime = DateTime.Now;
                                    chp.PchomeChannelComplaintSet(reply);
                                }
                                else
                                {
                                    logger.Error("PChome客訴單回覆失敗 complaint_id=" + reply.ComplaintId);
                                }
                            }
                            else
                            {
                                //可能轉回PChome自行回覆,17不需再回覆
                                reply.ReplyTime = DateTime.Now;
                                reply.ModifyId = "PChome";
                                reply.ModifyTime = DateTime.Now;
                                chp.PchomeChannelComplaintSet(reply);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("PChome客訴單回覆失敗 complaint_id=" + reply.ComplaintId, ex);
                    }
                }
            }
        }
    }
}

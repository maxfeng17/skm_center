﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class ShopBackValidationJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(ShopBackValidationJob));
        public void Execute(JobSetting js)
        {
            DateTime endTime = DateTime.Today;
            //buffer是一週
            DateTime startTime = DateTime.Today.AddDays(-7);
            string result = OrderFacade.ShopBackValidationAPI(startTime, endTime);

            logger.Info(string.Format("ShopBackValidationAPI Job =>{0}",result));
        }
    }
}

﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using log4net;
namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary>
    /// 檔次評價資料匯整
    /// </summary>
    public class SetDealEvaluateStarJob : IJob
    {
        private static ILog log = LogManager.GetLogger(typeof(SetDealEvaluateStarJob));
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        public void Execute(JobSetting js)
        {
            try
            {
                var modifyDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0);

                //最近檔次與分店對照資料不存在才做
                if (!pp.HaveVerifiedStoreEvaluateBidByModifyDate(modifyDate))
                {
                    //檔次與分店對照暫存資料
                    pp.SetVerifiedStoreEvaluateBid(modifyDate);
                }
                //刪除以前的舊資料
                pp.DeleteVerifiedStoreEvaluateBidByModifyDate(modifyDate.AddHours(-2));
                
                //目前檔次評價暫存資料
                pp.SetPponDealEvaluateStar();
            }
            catch (Exception ex)
            {
                log.Error("SetDealEvaluateStar running alert Error:" + ex.Message + "\n\r" + ex.StackTrace);
            }
        }
    }
}

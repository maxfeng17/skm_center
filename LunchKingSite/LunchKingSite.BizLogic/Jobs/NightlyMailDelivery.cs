﻿using System;
using System.Linq;
using System.Net.Mail;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs
{
    public class NightlyMailDelivery : IJob
    {
        public void Execute(JobSetting js)
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

            if (js.Parameters == null || string.IsNullOrEmpty(js.Parameters["c"]) || string.IsNullOrEmpty(js.Parameters["s"]))
                return;

            string content = js.Parameters["c"];
            MemberStatusFlag memberStatus = !string.IsNullOrEmpty(js.Parameters["t"]) ? (MemberStatusFlag) int.Parse(js.Parameters["t"]) : MemberStatusFlag.None;
            string subject = js.Parameters["s"];

            MemberCollection mc;
            if (!string.IsNullOrEmpty(js.Parameters["cf"]) || !string.IsNullOrEmpty(js.Parameters["bf"]))
                mc = mp.MemberGetListByCity(memberStatus,
                                            js.Parameters["cf"] != null ? js.Parameters["cf"].Split(new[] {"|"}, StringSplitOptions.RemoveEmptyEntries) : null,
                                            js.Parameters["bf"] != null ? js.Parameters["bf"].Split(new[] {"|"}, StringSplitOptions.RemoveEmptyEntries) : null);
            else if (!string.IsNullOrEmpty(js.Parameters["sf"]))
                mc = mp.MemberGetListDeliverableBySeller(memberStatus, (from x in (js.Parameters["sf"].Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries)) select new Guid(x)).ToArray());
            else
                mc = mp.MemberGetListByStatus(memberStatus, memberStatus, null);

            MailAddressCollection adrc = new MailAddressCollection();
            for (int i = 0; i < mc.Count; i++)
            {
                Member mb = mc[i];
                if (System.Text.RegularExpressions.Regex.IsMatch(mb.UserEmail, @"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$")) adrc.Add(mb.UserEmail);

                if (adrc.Count == 150 || i == mc.Count - 1)
                {
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);

                    foreach (MailAddress adr in adrc)
                        msg.Bcc.Add(adr);

                    msg.Subject = subject;
                    msg.Body = content;
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    adrc.Clear();
                }
            }
        }
    }
}

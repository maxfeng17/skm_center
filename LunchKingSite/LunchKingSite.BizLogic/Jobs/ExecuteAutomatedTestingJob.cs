﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using log4net;
using System.Net;
using System.Collections.Specialized;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Jobs
{
    public class ExecuteAutomatedTestingJob : IJob
    {
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(SendVerificationNoticeEmail));
        
        static ExecuteAutomatedTestingJob()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var responseString = client.GetAsync(config.ExecuteAutomatedTestingAPI).Result;
                }
            }
            catch (Exception ex)
            {
                logger.Error("ExecuteAutomatedTestingJob running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
        
    }
    
}
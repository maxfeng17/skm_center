﻿using log4net;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using System.Net.Mail;
using System.Web;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary>
    /// 商家客服單轉單通知
    /// </summary>
    public class VbsCustomerServiceNoticeMail : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(VbsCustomerServiceNoticeMail));
        private static ISellerProvider sp;
        private static IOrderProvider op;
        private static IHumanProvider hmp;
        private static Core.IServiceProvider svp;
        private static ISysConfProvider config;
        private static VbsCustomerServiceNoticeMail theJob;

        static VbsCustomerServiceNoticeMail()
        {
            theJob = new VbsCustomerServiceNoticeMail();
        }

        public static VbsCustomerServiceNoticeMail Instance()
        {
            return theJob;
        }
        
        private VbsCustomerServiceNoticeMail()
        {
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            hmp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            svp = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            /*
             * 時段:
             * 9:00發信，統計昨日14:01:00～今日8:59:59 中所有新增的待處理轉單案件
             * 14:00發信，統計今日9:00:00～14:00:59 中所有新增的待處理種單案件
             */

            DateTime startDate = new DateTime(1900, 1, 1);
            DateTime endDate = new DateTime(1900, 1, 1);
            string startRangeDescription = string.Empty;
            string endRangeDescription = string.Empty;

            //不寫死, 如果要手動執行至少會產出9點~14點的資料
            if (DateTime.Now.Hour == 9)
            {
                startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1, 14, 1, 0);
                endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 59, 59);
                startRangeDescription = DateTime.Now.AddDays(-1).ToString("MM/dd") + " 14:01";
                endRangeDescription = DateTime.Now.ToString("MM/dd") + " 08:59";
            }
            else
            {
                startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 9, 0, 0);
                endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 59);
                startRangeDescription = DateTime.Now.ToString("MM/dd") + " 09:00";
                endRangeDescription = DateTime.Now.ToString("MM/dd") + " 14:00";
            }

            #region 抓取欲通知的商家(轉單)
            //客服案件資訊
            var serviceMessage = svp.CustomerServiceMessageByStatusGet((int)statusConvert.transfer);

            //時間內未處理案件
            var rangeServiceMessage = serviceMessage.Where(x => ((x.ModifyTime ?? x.CreateTime) >= startDate && (x.ModifyTime ?? x.CreateTime) >= endDate)).ToList();
                                     
            List<Guid> orderGuids = serviceMessage.Select(x => (Guid)x.OrderGuid).ToList();
            if (orderGuids == null || orderGuids.Count <= 0)
            {
                return;
            }

            List<Guid> rangeOrderGuids = rangeServiceMessage.Select(x => (Guid)x.OrderGuid).ToList();

            var orderInfo = op.OrderGet(orderGuids);
            var rangeOrderInfo = orderInfo.Where(x => rangeOrderGuids.Contains(x.Guid));
            var sellers = sp.SellerGetList(orderInfo.Select(x => x.SellerGuid).Distinct().ToList());
            #endregion

            
            string subject = "17Life商家待處理客服案件通知";
            string content = @"親愛的廠商您好:<br />
                               <div>
                               提醒您自 {0} 至 {1} (統計區間) 您的待處理客服案件新增了 {2} 件，目前累積共 {3} 件尚<br />
                               未處理完成，請您盡快協助至下列商家系統網址檢視案件狀況及回覆處理進度，以免影響您與<br />
                               消費者的權益，感謝您的幫忙。
                               <br /><br />
                               開啟商家系統 <a href='{4}'>客服處理案件頁面</a><br /><br />

                               本信件為系統自動發送與統計，請勿直接回覆，若您有任何問題請與您的服務專員連繫，謝謝。
                               </div>";

            int totalCount = 0;
            int rangeCount = 0;

            foreach (var row in sellers)
            {
                totalCount = 0;
                rangeCount = 0;
                string userEmail = string.Empty;
                List<string> mails = new List<string>();

                #region 抓取商家聯絡人mail
                List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(row.Contacts);
                if (contacts != null)
                {
                    foreach (var c in contacts)
                    {
                        if (c.Type == ((int)ProposalContact.Normal).ToString() || c.Type == ((int)ProposalContact.ReturnPerson).ToString())
                        {
                            if (!string.IsNullOrEmpty(c.ContactPersonEmail))
                            {
                                if (!mails.Contains(c.ContactPersonEmail))
                                {
                                    mails.Add(c.ContactPersonEmail);
                                }                                
                            }
                        }
                    }
                }
                #endregion

                userEmail = string.Join(",", mails);

                var mailTo = RegExRules.CheckMultiEmail(userEmail);
                if (string.IsNullOrEmpty(mailTo))
                {
                    string errorMsg = string.Format("商家:{0}<a href='{1}'>{2}</a> 聯絡人mail有誤，無法發送客服信件。聯絡人Email: \"{3}\"", row.SellerId, config.SiteUrl + "/sal/SellerContent.aspx?sid=" + row.Guid, row.SellerName, userEmail);
                    try
                    {
                        EmailFacade.SendErrorEmail(row.Guid, errorMsg);
                    }
                    catch { }
                    
                    logger.Warn(errorMsg);
                    continue;
                }
                
                //未處理案件總筆數
                totalCount = orderInfo.Where(x => x.SellerGuid == row.Guid).Count();
                //時間內未處理案件筆數
                rangeCount = rangeOrderInfo.Where(x => x.SellerGuid == row.Guid).Count();
                //內容
                content = string.Format(content, startRangeDescription, endRangeDescription, rangeCount, totalCount, config.SiteUrl + "/vbs/ServiceProcess");

                string[] mailList = mailTo.Split(",");
                foreach(string m in mailList)
                {
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(config.SystemEmail);
                    msg.To.Add(m);
                    msg.Subject = subject;
                    msg.Body = HttpUtility.HtmlDecode(content);
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
            }            
        }
    }
}

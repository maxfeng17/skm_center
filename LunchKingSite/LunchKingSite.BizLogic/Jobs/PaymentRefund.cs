﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mail;
using System.Text;
using System.Linq;

namespace LunchKingSite.BizLogic.Jobs
{
    public class PaymentRefund : IJob
    {
        private static IOrderProvider op;
        private static IMemberProvider mp;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(PaymentRefund));
        private StringBuilder sbLog = new StringBuilder();

        static PaymentRefund()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                string mode = js.Parameters["mode"];
     
                sbLog.AppendLine(Environment.MachineName + ": 退款申請處理前資料準備");

                ExpiredOrderRefund(ref sbLog);
                PreRefund(ref sbLog);
                NoReceiptPaidOrderCancel(ref sbLog);

                var timer = new Stopwatch();
                timer.Start();

                var forRefunding = PaymentFacade.GetPaymentTransactionListForRefundingAndUpdateStatusToRequested();
                var forManualRefunding = PaymentFacade.GetPaymentTransactionListForRefundingAndUpdateStatusToRequested(true);

                timer.Stop();
                sbLog.AppendLine(string.Format("PaymentTransaction處理時間花費:{0:g}", timer.Elapsed));

                var withoutBatch = false;
                    
                withoutBatch = true;
                sbLog.AppendLine(Environment.MachineName + ": 退款申請(單筆)");
                sbLog.AppendLine("退款筆數:" + forRefunding.Count);
                    
                // for old nccc
                CreditcardProcessor.OldNCCCApply(forRefunding);

                CreditcardProcessor.SequentialApply(forRefunding, config.PaymentChargeAndRefundDebugMode);
                CreditcardProcessor.SequentialApply(forManualRefunding, config.PaymentChargeAndRefundDebugMode, true);

               
                #region 寄發處理結果

                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = Environment.MachineName + ": PaymentRefund has run. Mode:" + mode;
                msg.From = new MailAddress(config.AdminEmail);
                msg.Body = sbLog.ToString();
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);

                #endregion 寄發處理結果
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = Environment.MachineName + ": PaymentRefund Error";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;
                if (sbLog.Length > 0)
                {
                    msg.Body = string.Format("{0}\r\n\r\n[mode]{1}\r\n錯誤訊息: {2}{3}",
                        sbLog,
                        js.Parameters["mode"],
                        ex.Message, ex.StackTrace);
                }

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error(ex);
            }

            #region 品生活部分

            try
            {
                //查詢14天以上的退貨資料，並進行退貨
                HiDealReturnedManager.AutoRefundWork();
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = "Piinlife PaymentRefund Error";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error(ex);
            }

            #endregion 品生活部分
        }

        /// <summary>
        /// 超取部分先付部分超取之未取貨訂單取消
        /// </summary>
        /// <param name="sbLog"></param>
        private void NoReceiptPaidOrderCancel(ref StringBuilder sbLog)
        {
            var timer = new Stopwatch();
            timer.Start();
            CashTrustLogCollection ctCol = mp.CashTrustLogGetListByNoReceiptPaid();

            foreach (var o in ctCol.GroupBy(x =>new { OrderGuid = x.OrderGuid, UserId = x.UserId }))
            {
                //處理邀請得紅利的部分
                MemberFacade.MemberPromotionDeductionWhenOrderCancel(o.Key.OrderGuid, MemberFacade.GetUserName(o.Key.UserId));

                foreach (var ct in ctCol.Where(x => x.OrderGuid == o.Key.OrderGuid))
                {
                    PaymentFacade.CancelPaymentTransactionByCouponForIncompleteOrder(ct, "sys");
                }
                CommonFacade.AddAudit(o.Key.OrderGuid, AuditType.Payment, "已檢查超取超付未完成付款單，若有其他付款項目皆已退回", "sys", true);
                OrderFacade.SendOrderCancelMail(o.Key.OrderGuid, false, true);
            }

        }

        /// <summary>
        /// 憑證檔次和宅配檔次自動退貨處理
        /// </summary>
        private void ExpiredOrderRefund(ref StringBuilder sbLog)
        {
            var timer = new Stopwatch();
            timer.Start();

            #region 憑證訂單逾期未使用自動退購物金

            //處理過期的檔次訂單 轉進退貨單中 (到期日在2015/5/1後的檔次始適用)
            OrderCollection oCol = op.GetExpriedOrderList();
            
            var timeStampGetExpiredOrder = timer.Elapsed;
            
            var returnReason = "憑證逾期，系統自動退購物金";
            foreach (Order o in oCol)
            {
                IEnumerable<int> refundableCouponIds = ReturnService.GetRefundableCouponCtlogs(o.Guid, BusinessModel.Ppon).Select(x => (int)x.CouponId).ToList();
                if (refundableCouponIds.Any())
                {
                    int? returnFormId;
                    CreateReturnFormResult result = ReturnService.CreateCouponReturnForm(
                        o.Guid, refundableCouponIds, true, "sys@17life.com", returnReason, out returnFormId, false, true);
                    if (result == CreateReturnFormResult.Created)
                    {
                        string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId, returnReason);
                        CommonFacade.AddAudit(o.Guid, AuditType.Refund, auditMessage, "sys", false);
                    }
                }
            }

            var timeStampCreateReturnForm = timer.Elapsed.Subtract(timeStampGetExpiredOrder);
            timer.Stop();
            sbLog.AppendLine(string.Format("ExpiredOrderRefund處理總時間花費: {0:g} ,\r\n\t撈取過期檔次訂單: {1:g} \r\n\t建立退貨單： {2:g}", 
                                timer.Elapsed, 
                                timeStampGetExpiredOrder, 
                                timeStampCreateReturnForm));

            #endregion 憑證訂單逾期未使用自動退購物金

            #region 宅配退貨單廠商逾期未處理自動更新廠商處理狀態為退貨完成

            //待正式開啟功能 需重新檢視撈取退貨單規則是否需調整
            //if (conf.EnableProductAutoReturnProcess)
            //{ 
            //    returnReason = "廠商未於期限內更新處理進度";
            //    var returnFormIds = op.GetExpiredProductReturnFormList().Select(x => x.Id).Distinct();
            //    foreach (var returnFormId in returnFormIds)
            //    {
            //        var returnForm = ReturnFormRepository.FindById(returnFormId);
            //        returnForm.OutOfTimeRangeUpdateVendorProgress();
            //        ReturnFormRepository.Save(returnForm);
            //        string auditMessage = string.Format("退貨單({0})更新廠商處理進度為[系統自動退]. 原因:{1}", returnFormId, returnReason);
            //        CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, auditMessage, "sys", false);
            //    }
            //}

            #endregion  宅配退貨單廠商逾期未處理自動更新廠商處理狀態為退貨完成

            #region 超取逾期未取自動退購物金

            //超取先付款未取貨
            oCol.Clear();
            oCol = op.GetNoReceiptPaidIspOrder();

            returnReason = "超取逾期未取，系統自動退購物金";
            foreach (Guid oid in oCol.GroupBy(x=>x.Guid).Select(x=>x.Key))
            {
                //宅配退貨申請單，抓出 1.非退貨中 2.非已退貨 3.非換貨中 
                List<int> orderProductIds = op.OrderProductGetListByOrderGuid(oid).Where(x => x.IsCurrent && !x.IsReturning && !x.IsReturned && !x.IsExchanging).Select(x => x.Id).ToList();
                if (orderProductIds.Any())
                {
                    int? returnFormId;
                    CreateReturnFormResult result = ReturnService.CreateRefundForm(oid, orderProductIds, true, "sys@17life.com", returnReason,
                        null, null, null, null, out returnFormId, false);
                    if (result == CreateReturnFormResult.Created)
                    {
                        string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId, returnReason);
                        CommonFacade.AddAudit(oid, AuditType.Refund, auditMessage, "sys", false);
                    }
                }
            }
            #endregion

            #region 萊爾富咖啡訂單逾期未使用自動退購物金

            timer.Start();
            //處理過期的檔次訂單 轉進退貨單中 (到期日在2015/5/1後的檔次始適用)
            OrderCollection hiLifeCol = op.GetHiLifeExpriedOrderList();

            var timeStampGetHiLifeExpriedOrder = timer.Elapsed;

            returnReason = "憑證逾期，系統自動退購物金";
            foreach (Order o in hiLifeCol)
            {
                string errorMsg = string.Empty;
                if (config.EnableHiLifeDealSetup && !OrderFacade.HiLifePincodeReturn(o.Guid, out errorMsg))
                {
                    continue;
                }
                else
                {
                    IEnumerable<int> refundableCouponIds = ReturnService.GetRefundableCouponCtlogs(o.Guid, BusinessModel.Ppon).Select(x => (int)x.CouponId).ToList();
                    if (refundableCouponIds.Any())
                    {
                        int? returnFormId;
                        CreateReturnFormResult result = ReturnService.CreateCouponReturnForm(
                            o.Guid, refundableCouponIds, true, "sys@17life.com", returnReason, out returnFormId, false, true);
                        if (result == CreateReturnFormResult.Created)
                        {
                            string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId, returnReason);
                            CommonFacade.AddAudit(o.Guid, AuditType.Refund, auditMessage, "sys", false);
                        }
                    }
                }

                
            }

            var timeStampCreateHiLifeReturnForm = timer.Elapsed.Subtract(timeStampGetHiLifeExpriedOrder);
            timer.Stop();
            sbLog.AppendLine(string.Format("HiLifeExpiredOrderRefund處理總時間花費: {0:g} ,\r\n\t撈取過期檔次訂單: {1:g} \r\n\t建立退貨單： {2:g}",
                                timer.Elapsed,
                                timeStampGetHiLifeExpriedOrder,
                                timeStampCreateHiLifeReturnForm));

            #endregion 憑證訂單逾期未使用自動退購物金

        }

        /// <summary>
        /// 處理退貨申請單
        /// </summary>
        private void PreRefund(ref StringBuilder sbLog)
        {
            var timer = new Stopwatch();
            timer.Start();

            IList<ReturnFormEntity> returnForms = ReturnFormRepository.FindScheduledForRefunding();

            //取得被列入忽略批次退貨的訂單
            var ignoreOrderDic = ReturnFormRepository.GetIgnoreBatchRefundOrders(returnForms);

            var timeStampGetRefundReturnForm = timer.Elapsed;

            //預期會批量更新退訂資料，所以先停用銷售數字的更新，等退貨操作完成再啟用
            PayEvents.Stop();
            foreach (ReturnFormEntity form in returnForms)
            {
                try
                {
                    //訂單符合略過退貨條件，不做退貨
                    if (ignoreOrderDic.ContainsKey(form.OrderGuid))
                    {
                        continue;
                    }
                    ReturnService.Refund(form);
                }
                catch (Exception ex)
                {
                    var msg = new MailMessage();
                    msg.From = new MailAddress(config.AdminEmail);
                    msg.To.Add(new MailAddress(config.ItTesterEmail));
                    msg.Subject = "PaymentRefund at Refund exception!";
                    msg.Body = string.Format("退貨單(id={0}) PaymentRefund.PreRefund 退款發生問題!{1}{2}",
                        form.Id, Environment.NewLine, ex.StackTrace.Length > 2500 ? ex.StackTrace.Substring(0, 2500) : ex.StackTrace);
                    msg.IsBodyHtml = false;
                    PostMan.Instance().Send(msg, SendPriorityType.Normal);
                }
            }

            var timeStampRefundProcess = timer.Elapsed.Subtract(timeStampGetRefundReturnForm);
            timer.Stop();
            sbLog.AppendLine(string.Format("PreRefund處理總時間花費: {0:g} ,\r\n\t撈取可退款之退貨單: {1:g} \r\n\t退款作業處理： {2:g}",
                                timer.Elapsed,
                                timeStampGetRefundReturnForm,
                                timeStampRefundProcess));

            PayEvents.Start();
        }

    }
}
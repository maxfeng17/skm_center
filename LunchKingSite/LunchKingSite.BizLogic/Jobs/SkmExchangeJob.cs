﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using log4net;
using LunchKingSite.BizLogic;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Interface;
using System.IO;
using System.Linq;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;

namespace LunchKingSite.BizLogic.Jobs
{
    public class SkmExchangeJob : IJob
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PaymentCharge));
        private static readonly ISysConfProvider Config = ProviderFactory.Instance().GetConfig();
        private static readonly ISkmProvider SkmMp = ProviderFactory.Instance().GetProvider<ISkmProvider>();
        private static readonly ISellerProvider Sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        
        public void Execute(JobSetting js)
        {
            InsertExchangeForSkmShoppe();
        }

        /// <summary>
        /// 建立贈品處資料再Skm_Shoppe的Table中
        /// </summary>
        public static void InsertExchangeForSkmShoppe()
        {
            try
            {
                var skmExchanges = SkmMp.SkmExchangeGet();

                if (skmExchanges.Count == 0)
                {
                    Logger.Error("新增新光贈品處，table skm_exchange無資料");
                    return;
                }
                
                using (var tran = TransactionScopeBuilder.CreateReadCommitted())
                {
                    foreach (var skmExchange in skmExchanges)
                    {
                        GetSkmShoppeInfo(skmExchange);
                    }
                    tran.Complete();
                }
                SortedDictionary<string, string> serverIPs = ProviderFactory.Instance().GetSerializer().Deserialize<SortedDictionary<string, string>>(Config.ServerIPs);
                string clearSellerCacheResult = SystemFacade.SyncCommand(serverIPs, SystemFacade._CHANNEL_SELLER_TREE_CLEAR, string.Empty, string.Empty);
                Logger.InfoFormat("SkmResetSellerTreeCache:{0}", clearSellerCacheResult);
            }
            catch (Exception ex)
            {
                Logger.Error(ex + " 新增新光贈品處異常");
            }
        }

        public static void GetSkmShoppeInfo(SkmExchange skmshop)
        {
            const string exchangeCode = "999"; //贈品櫃位代碼
            var skmSeller = SkmMp.ShoppeGetSellerByShopeCode(skmshop.ShopCode);
            var exchangeBrndCode = skmSeller.ShopCode + exchangeCode + skmshop.Floor;

            //IsExistSkmShoppe
            SkmShoppeCollection shoppes = SkmMp.SkmShoppeGetAllShoppe(skmSeller.StoreGuid ?? Guid.Empty, skmshop.ShopCode); //把該櫃位撈出來
            SellerCollection sellerCol = new SellerCollection();
            DateTime now = DateTime.Now;
            //shoppes.ForEach(x => x.IsAvailable = false); //預設都先沒開啟，後續更新

            if (SkmMp.IsExistSkmShoppe(skmSeller.ShopCode, exchangeBrndCode))
            {
                //update shoppe
                Guid storeGuid = Guid.Empty;
                shoppes.Where(x => (x.ShopCode == skmSeller.ShopCode && x.BrandCounterCode == exchangeBrndCode))
                    .ForEach(i =>
                    {
                        i.BrandCounterName = exchangeBrndCode;
                        i.ShopName = skmSeller.ShopName + "-贈品處";
                        i.CategoryId = skmSeller.CategoryId;
                        i.CategoryName = skmSeller.CategoryName;
                        i.SubcategoryId = skmSeller.SubcategoryId;
                        i.SubcategoryName = skmSeller.SubcategoryName;
                        i.IsAvailable = true;
                        i.IsShoppe = true;
                        i.IsExchange = true;
                        i.Floor = skmshop.Floor;
                        storeGuid = i.StoreGuid ?? Guid.Empty;
                    });
                if (storeGuid != Guid.Empty)
                {
                    var seller = Sp.SellerGet(storeGuid);
                    if (seller.IsLoaded && seller.SellerName != skmSeller.ShopName + "-贈品處")
                    {
                        seller.SellerName = skmSeller.ShopName + "-贈品處";
                        sellerCol.Add(seller);
                    }
                }
            }
            else
            {
                //new shoppe
                Guid storeGuid = Guid.NewGuid();
                SkmShoppe shoppe = new SkmShoppe()
                {
                    StoreGuid = storeGuid,
                    ShopCode = skmshop.ShopCode,
                    ShopName = skmSeller.ShopName,
                    BrandCounterCode = exchangeBrndCode,
                    BrandCounterName = skmSeller.ShopName + "-贈品處",
                    CategoryId = skmSeller.CategoryId,
                    CategoryName = skmSeller.CategoryName,
                    SubcategoryId = skmSeller.SubcategoryId,
                    SubcategoryName = skmSeller.SubcategoryName,
                    Floor = skmshop.Floor,
                    SellerGuid = skmSeller.SellerGuid,
                    IsAvailable = true,
                    IsShoppe = true,
                    IsExchange = true,
                    ParentSellerGuid = skmSeller.SellerGuid ?? Guid.Empty
                };
                SkmMp.ShoppeSet(shoppe);

                //new seller
                Seller seller = new Seller()
                {
                    Guid = storeGuid,
                    SellerName = skmSeller.ShopName + "-贈品處",
                    SellerId = SellerFacade.GetNewSellerID(),
                    CreateId = "sys@17life.com",
                    CreateTime = DateTime.Now,
                    Department = (int) DepartmentTypes.Ppon,
                    CityId = Config.SkmCityId,
                    IsCloseDown = false
                };
                Sp.SellerSet(seller);

                //new sellerTree
                SellerTree sellerTree = new SellerTree()
                {
                    SellerGuid = storeGuid,
                    ParentSellerGuid = skmSeller.StoreGuid ?? Guid.Empty,
                    RootSellerGuid = Config.SkmRootSellerGuid,
                    CreateId = "sys@17life.com",
                    CreateTime = now
                };
                Sp.SellerTreeSet(sellerTree);

                //new store category
                int shopcode = 0;
                StoreCategory sc = new StoreCategory()
                {
                    StoreGuid = storeGuid,
                    CategoryCode = int.TryParse(skmshop.ShopCode, out shopcode) ? shopcode : 0,
                    Valid = 1,
                    CreateType = (int) LunchKingSite.Core.Enumeration.MrtReleaseshipStoreSettingType.Skm,
                    ModifyId = "sys@17life.com",
                    ModifyTime = now,
                };
                Sp.StoreCategorySet(sc);
            }

            //update shoppe
            SkmMp.ShoppeCollectionSet(shoppes);

            //update exchange 7X
            skmshop.BrandCounterCode = exchangeBrndCode;
            SkmMp.SkmExchangeSet(skmshop);

            //update Seller
            if (sellerCol.Any())
            {
                Sp.SellerSaveAll(sellerCol);
            }
        }
    }
}

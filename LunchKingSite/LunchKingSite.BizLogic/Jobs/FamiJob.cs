﻿using System;
using System.Net.Mail;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Jobs
{
    public class FamiJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(FamiJob));
        private static ISysConfProvider _config;
        private static FamiJob _theJob;

        static FamiJob()
        {
            _theJob = new FamiJob();
        }

        public static FamiJob Instance()
        {
            return _theJob;
        }

        private FamiJob()
        {
            _config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                OrderFacade.SetPeztempStatusExpiredWithDeliverTimeEndForFami((int)PeztempServiceCode.Famiport);
            }
            catch (Exception ex)
            {
                logger.Error("FamiPortJob Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
 }

﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Jobs
{
    public class MmberCheckoutInfoRefreshFamilyInfoJob : IJob
    {
        private static IMemberProvider _memberProvider = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public class ChannalStoreModel
        {
            public string id { get; set; }
            public string storeId { get; set; }
            public string storeName { get; set; }
            public string storeAddress { get; set; }
            public string telNo { get; set; }
        }

        public void Execute(JobSetting js)
        {
            RefreshChannelStoreInfo();
        }

        public static void RefreshChannelStoreInfo()
        {
            try
            {
                var storeIdApiListByFamily = JsonConvert.DeserializeObject<List<ChannalStoreModel>>(ISPFacade.IspGetServiceChannelStoreData(null, ServiceChannel.FamilyMart)).Select(p => p.storeId).Distinct();
                var storeIdApiListBySeven = JsonConvert.DeserializeObject<List<ChannalStoreModel>>(ISPFacade.IspGetServiceChannelStoreData(null, ServiceChannel.SevenEleven)).Select(p => p.storeId).Distinct();

                var allMemberCheckInfo = _memberProvider.MemberCheckoutInfoCollectionGet();
                var allMemberCheckInfoByFamily = allMemberCheckInfo.Where(p => !string.IsNullOrEmpty(p.FamilyStoreId)).Select(p => p.FamilyStoreId).Distinct();
                var allMemberCheckInfoBySeven = allMemberCheckInfo.Where(p => !string.IsNullOrEmpty(p.SevenStoreId)).Select(p => p.SevenStoreId).Distinct();

                foreach (var storeId in allMemberCheckInfoByFamily)
                {
                    var reNewStoreId = storeId.Length > 6 ? storeId.Substring(1, 6) : storeId;
                    if (!storeIdApiListByFamily.Contains(reNewStoreId))//不存在famiStore集合內, 則清空membercheck info. fami資訊
                    {
                        _memberProvider.MemberCheckoutInfoUpdateByStoreId(storeId, ServiceChannel.FamilyMart);
                    }
                }

                foreach (var storeId in allMemberCheckInfoBySeven)
                {
                    var reNewStoreId = storeId;
                    if (!storeIdApiListBySeven.Contains(reNewStoreId))//不存在SevenStore集合內, 則清空membercheck info. fami資訊
                    {
                        _memberProvider.MemberCheckoutInfoUpdateByStoreId(storeId, ServiceChannel.SevenEleven);
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}

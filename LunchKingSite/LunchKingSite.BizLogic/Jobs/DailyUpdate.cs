﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.EInvoice;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using log4net;

namespace LunchKingSite.BizLogic.Jobs
{
    public class DailyUpdate : IJob
    {
        protected static ISystemProvider sys;
        protected static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(DailyUpdate));

        static DailyUpdate()
        {
            sys = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                LoveCodeUpdate();
            }
            catch (Exception ex)
            {
                logger.Warn("更新愛心碼失敗，跟政府的網站溝通不順造成，因為我們在資料庫也有存一份愛心碼資料，所以如果這錯誤是偶爾出現，不用太在意", ex);
            }
        }

        private static void LoveCodeUpdate()
        {
            List<LoveCode> loveCodes = LoveCodeManager.GetAllLoveCodesFromGovService();
            if (loveCodes.Count > 0)
            {
                SystemData loveCodeData = sys.SystemDataGet("LoveCode");
                if (loveCodeData.IsLoaded == false)
                {
                    loveCodeData = new SystemData
                    {
                        CreateId = config.SystemEmail,
                        CreateTime = DateTime.Now,
                        Name = "LoveCode"
                    };
                }
                else
                {
                    loveCodeData.ModifyId = config.SystemEmail;
                    loveCodeData.ModifyTime = DateTime.Now;
                }
                loveCodeData.Data = LoveCodeManager.LoveCodesToJson(loveCodes);
                sys.SystemDataSet(loveCodeData);
                LoveCodeManager.Instance.ReloadDefaultManager();
            }
        }
    }
}

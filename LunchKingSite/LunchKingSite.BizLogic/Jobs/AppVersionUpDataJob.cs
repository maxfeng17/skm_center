﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs
{
    public class AppVersionUpDataJob : IJob
    {
        private static ISystemProvider ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        private static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        private ILog log = LogManager.GetLogger(typeof(DiscountUseJob));
        public void Execute(JobSetting js)
        {
            try
            {
                log.Debug("AppVersionUpDataJob Start");
                var appVersionList = ss.AppVersionGetList().ToList();
                var tmpdata = ss.AppVersionGetList().ToList().Where(x => x.DeviceType == 9527);
                var tmpList = tmpdata as IList<AppVersion> ?? tmpdata.ToList();
                if (tmpList.Any())
                {
                    foreach (var appVersion in tmpList)
                    {
                        var latestVersion = appVersion.LatestVersion;
                        DateTime upDataTime;
                        if (!string.IsNullOrEmpty(latestVersion) && DateTime.TryParse(latestVersion, out upDataTime))
                        {
                            if (upDataTime <= DateTime.Now)
                            {
                                var tmpName = appVersion.AppName.Replace("(TemporaryData)", "");
                                var upData = appVersionList.First(x => x.AppName == tmpName);
                                upData.AppConfig = appVersion.AppConfig;
                                ss.UpdateAppVersion(upData);
                                appVersion.LatestVersion = "";
                                appVersion.AppConfig = "";
                                ss.UpdateAppVersion(appVersion);
                                SortedDictionary<string, string> serverIPs =
                                ProviderFactory.Instance().GetSerializer().Deserialize<SortedDictionary<string, string>>(cp.ServerIPs);
                                SystemFacade.SyncCommand(serverIPs, SystemFacade._API_SYSTEM_MANAGER, string.Empty, string.Empty);
                            }
                        }
                    }
                }
                log.Debug("AppVersionUpDataJob End");
            }
            catch (Exception ex)
            {
                log.Error("AppVersionUpDataJob alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }          
        }
    }
}

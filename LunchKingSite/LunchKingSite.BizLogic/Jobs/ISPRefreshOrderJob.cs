﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class ISPRefreshOrderJob : IJob
    {
        public void Execute(JobSetting js)
        {
            ISPFacade.RefreshOrder();
        }
    }
}

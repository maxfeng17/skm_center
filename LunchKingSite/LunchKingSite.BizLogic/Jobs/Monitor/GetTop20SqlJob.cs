﻿using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Jobs.Monitor
{
    //TODO 未完成
    public class GetTop20SqlJob : IJob
    {
        public void Execute(JobSetting js)
        {
            List<Top20Model> model = GetTop20Sql();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"<table rules='all' style='font-size:10pt;border-color:#3a3a3a;border:1px solid #ccc'>                
                    <thead>
                        <tr>
                            <th>排名</th><th>SQL 語法</th>
                            <th>執行次數</th><th>平均執行時間(秒)</th>
                            <th>總執行時間(秒)</th><th>平均CPU 時間(ms)</th>
                            <th>平均 I/O 次數</th><th>總 I/O 次數</th>
                        </tr>
                    </thead>
                    <tbody>");
            foreach (Top20Model item in model)
            {
                sb.AppendLine("<tr>");
                sb.AppendFormat("<td style='text-align:center'>{0}</td>", item.Rank);
                sb.AppendFormat("<td style='width:600px;word-break:break-all'>{0}</td>", GetSqlText(item.Text));
                sb.AppendFormat("<td style='text-align:right'>{0}</td>", item.ExecutionCount.ToString("N0"));
                if (item.AvgSecs < 1)
                {
                    sb.AppendFormat("<td style='text-align:right'>{0}</td>", item.AvgSecs.ToString("N2"));
                }
                else
                {
                    sb.AppendFormat("<td style='text-align:right; color:red; font-weight:bold'>{0}</td>", item.AvgSecs.ToString("N2"));
                }
                sb.AppendFormat("<td style='text-align:right'>{0}</td>", item.TotalSecs.ToString("N0"));
                sb.AppendFormat("<td style='text-align:right'>{0}</td>", item.AvgCpu.ToString("N0"));
                sb.AppendFormat("<td style='text-align:right'>{0}</td>", item.AvgIO.ToString("N0"));
                sb.AppendFormat("<td style='text-align:right'>{0}</td>", item.TotalIO.ToString("N0"));
                sb.AppendLine("</tr>");
            }
            sb.AppendLine(@"</tbody></table>");
            var mm = new MailMessage
            {
                From = new MailAddress("db@17life.com", "資料庫通知信"),
                Body = sb.ToString(),
                Subject = "TOP 20 耗時SQL",
                BodyEncoding = Encoding.UTF8,
                SubjectEncoding = Encoding.UTF8,
                IsBodyHtml = true
            };
            mm.To.Add("itstaff@17life.com");
            PostMan.Instance().Send(mm, SendPriorityType.Immediate);
        }


        static Regex regex = new Regex(@"\(@[\w ,@]+\) ", RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase);
        
        private string GetSqlText(string text)
        {
            text =  regex.Replace(text, string.Empty);
            if (text.Length > 2000)
            {
                text = text.Substring(0, 2000) + "...... 長度太長，以下略";
            }
            return text;
        }

        private List<Top20Model> GetTop20Sql()
        {
            string sql = @"
SELECT TOP 20  
	text,
	execution_count, 
	 (CAST(total_elapsed_time / 1000000.0 / execution_count AS DECIMAL(16, 2))) as avg_secs,
	total_elapsed_time as total_secs,
	(qs.total_worker_time/1000 /qs.execution_count) as avg_cpu,
	((qs.total_logical_reads + qs.total_logical_writes) / qs.execution_count) as avg_io,
	(qs.total_logical_reads + qs.total_logical_writes) as total_io	
	FROM sys.dm_exec_query_stats qs
	CROSS APPLY sys.dm_exec_sql_text(qs.sql_handle) qt
	CROSS APPLY sys.dm_exec_query_plan(qs.plan_handle) qp
	WHERE qs.execution_count > 10 --and qs.last_execution_time >= convert(varchar(10),getdate()-1,112) + ' 12:00:00'
		AND SUBSTRING(SUBSTRING(qt.TEXT, (qs.statement_start_offset/2)+1, ((CASE qs.statement_end_offset WHEN -1 THEN DATALENGTH(qt.TEXT) 
		 ELSE qs.statement_end_offset END - qs.statement_start_offset)/2)+1),1,15) ! = 'WAITFOR(RECEIVE'
		 ORDER BY total_elapsed_time DESC, qs.total_worker_time/qs.execution_count DESC

";
            QueryCommand qc = new QueryCommand(sql, Member.Schema.Provider.Name);
            List<Top20Model> model = new List<Top20Model>();
            int rank = 0;
            using (IDataReader dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    rank++;
                    model.Add(new Top20Model
                    {
                        Rank = rank,
                        Text = dr.GetString(0),
                        ExecutionCount = dr.GetInt64(1),
                        AvgSecs = dr.GetDecimal(2),
                        TotalSecs = dr.GetInt64(3),
                        AvgCpu = dr.GetInt64(4),
                        AvgIO = dr.GetInt64(5),
                        TotalIO = dr.GetInt64(6)
                    });
                }
            }
            return model;
        }


        public class Top20Model
        {
            public int Rank { get; set; }
            public string Text { get; set; }
            public long ExecutionCount { get; set; }
            public decimal AvgSecs { get; set; }
            public long TotalSecs { get; set; }
            public long AvgCpu { get; set; }
            public long AvgIO { get; set; }
            public long TotalIO { get; set; }

        }
    }

}

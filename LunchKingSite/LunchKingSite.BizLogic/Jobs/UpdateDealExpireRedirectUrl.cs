﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using log4net;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Component.Template;
using System.Text;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Component.API;
using System.Web;

namespace LunchKingSite.BizLogic.Jobs
{
    /*
     * 每日更新檔次ExpireRedirectUrl相關檔次欄位
     * */
    public class UpdateDealExpireRedirectUrl : IJob
    {
        private static IOrderProvider op;
        private static IPponProvider pp;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(CouponSettlement));

        static UpdateDealExpireRedirectUrl()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                
                int maxNumber = 1000;
                List<IViewPponDeal> vpds = new List<IViewPponDeal>();
                DateTime startTime = DateTime.Now;

                DataTable dt = pp.BusinessHourGetExpireRedirectUrl(DateTime.Now.AddDays(-30 * 6), DateTime.Now.AddDays(0));

                int startIdx = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (startIdx > maxNumber)
                    {
                        //每次最多更新N筆
                        break;
                    }
                    Guid bid = Guid.Empty;
                    if(Guid.TryParse(dr["business_hour_guid"].ToString(), out bid))
                    {
                        IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                        IViewPponDeal similarDeal = PponFacade.GetSimilarDeal(theDeal);
                        BusinessHour bh = pp.BusinessHourGet(bid);
                        string expireRedirectUrl = string.Empty;
                        bool useExpireRedirectUrlAsCanonical = (similarDeal != null);
                        expireRedirectUrl = PponFacade.ExpireRedirectUrl(similarDeal, bh.PicAlt);
                        //更新資料
                        bh.UseExpireRedirectUrlAsCanonical = useExpireRedirectUrlAsCanonical;
                        bh.ExpireRedirectDisplay = (int)ExpireRedirectDisplay.Display1;
                        bh.ExpireRedirectChangeDate = DateTime.Now;
                        bh.ExpireRedirectUrl = expireRedirectUrl;
                        pp.BusinessHourSet(bh);
                    }
                    startIdx++;
                }
                DateTime endTime = DateTime.Now;
                TimeSpan ts = endTime - startTime;
                Console.WriteLine(ts.TotalMinutes);
            }
            catch(Exception ex)
            {
                logger.Error(ex);
            }

        }
    }
}

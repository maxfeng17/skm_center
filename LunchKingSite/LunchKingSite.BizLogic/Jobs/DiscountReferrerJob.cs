﻿using System;
using System.Net.Mail;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component;
using System.Collections.Generic;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs
{
    public class DiscountReferrerJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(DiscountReferrerJob));
        private static ISysConfProvider config;
        private static DiscountReferrerJob _theJob;

        static DiscountReferrerJob()
        {
            _theJob = new DiscountReferrerJob();
        }

        public static DiscountReferrerJob Instance()
        {
            return _theJob;
        }

        private DiscountReferrerJob()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                logger.Info("DiscountReferrerJob start");
                int qty = 999999;
                decimal expireMonths = config.DiscountReferrerExpireMonths;
                DateTime now = DateTime.Now;
                PromotionFacade.GenerateDiscountReferrer(expireMonths, now, qty);
                logger.Info("DiscountReferrerJob end");
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = "DiscountReferrerJob Error";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error("DiscountReferrerJob Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
 }

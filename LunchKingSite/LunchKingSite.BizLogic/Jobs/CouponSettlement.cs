﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Transactions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using log4net;

namespace LunchKingSite.BizLogic.Jobs
{
    public class CouponSettlement : IJob
    {
        private static IOrderProvider op;
        private static IMemberProvider mp;
        private static ISellerProvider sp;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(CouponSettlement));

        static CouponSettlement()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            //step1.每半小時run一次
            //step2.撈出小於NOW的SKM檔次清算時間
            //step3.清算檔次底下未使用的憑證 做退貨處理 並重設settlement time + 1 day
            //step4.api 呼叫剩餘索取數量 算出最大購買量減去ctl.status >= 2
            //fin 更新清算時間 但如果大於截止日 則不更新
            DateTime now = DateTime.Now;
            CouponSettlementReturn(now);
        }

        private void CouponSettlementReturn(DateTime d)
        {
            if (config.IsEnableCouponSettlement)
            {
                #region 清算未使用憑證做退貨處理
                ViewPponOrderCollection oCol = op.GetNoUseCouponOrderList(d);

                var returnReason = "清算未使用憑證";
                foreach (ViewPponOrder o in oCol)
                {
                    IEnumerable<int> refundableCouponIds = ReturnService.GetRefundableCouponCtlogs(o.Guid, BusinessModel.Ppon, (int)OrderClassification.Skm).Select(x => (int)x.CouponId).ToList();
                    if (refundableCouponIds.Any())
                    {
                        int? returnFormId;
                        CreateReturnFormResult result = ReturnService.CreateCouponReturnForm(
                            o.Guid, refundableCouponIds, true, "sys@17life.com", returnReason, out returnFormId, false, true, ThirdPartyPayment.None, OrderClassification.Skm);
                        string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId, returnReason);
                        CommonFacade.AddAudit(o.Guid, AuditType.Refund, auditMessage, "sys", false);
                    }

                    ReturnForm form = op.ReturnFormGetListByOrderGuid(o.Guid).FirstOrDefault(x => x.ProgressStatus == (int)ProgressStatus.Processing);
                    if (form != null)
                    {
                        try
                        {
                            var formEntity = new ReturnFormEntity(form.Id);
                            ReturnService.Refund(formEntity, "sys", true, false, false);
                        }
                        catch (Exception ex)
                        {
                            var msg = new MailMessage();
                            msg.From = new MailAddress(config.AdminEmail);
                            msg.To.Add(new MailAddress(config.ItTesterEmail));
                            msg.Subject = "[SKM]PaymentRefund at Refund exception!";
                            msg.Body = string.Format("退貨單(id={0}) PaymentRefund.PreRefund 退款發生問題!{1}{2}",
                                form.Id, Environment.NewLine, ex.StackTrace.Length > 2500 ? ex.StackTrace.Substring(0, 2500) : ex.StackTrace);
                            msg.IsBodyHtml = false;
                            PostMan.Instance().Send(msg, SendPriorityType.Normal);
                        }
                    }
                }
                #endregion 清算未使用憑證做退貨處理
            }
            
            #region 更新所有檔次清算時間
            List<Guid> bids = sp.BusinessHourGetListBySettlementTime(d.AddMinutes(-30), d).GroupBy(x => x.Guid).Select(s => s.Key).ToList();
            UpdateSettlementTime(bids, d.AddDays(1));
            #endregion 更新所有檔次清算時間
        }

        private void UpdateSettlementTime(List<Guid> bids, DateTime d)
        {
            BusinessHourCollection result = sp.BusinessHourGetList(bids);
            foreach (BusinessHour bh in result)
            {
                bh.SettlementTime = new DateTime(d.Year, d.Month, d.Day, bh.SettlementTime.Value.Hour, bh.SettlementTime.Value.Minute, bh.SettlementTime.Value.Second);
                sp.BusinessHourSet(bh);
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using log4net;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs
{
    public class VendorPaymentOverdueJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(VendorPaymentOverdueJob));
        protected static IAccountingProvider ap;
        protected static IOrderProvider op;
        protected static ISysConfProvider config;

        static VendorPaymentOverdueJob()
        {
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            Guid errorBid = Guid.Empty;
            Guid errorOrderGuid = Guid.Empty;
            try
            {
                //9/1後的最後出貨日開始算
                var viewExpiringOrders = op.ViewOverdueOrderCollectionGet().Where(x=>x.LastShipDate > DateTime.Parse("2017/09/01")).Cast<IViewExpiringOrder>().ToList();
                
                if (viewExpiringOrders.Any())
                {
                    var sellerGroup = viewExpiringOrders.Select(x => x.SellerGuid).Distinct().ToList();
                    var sellerOrderDic = sellerGroup.ToDictionary(
                        sellerGuid => sellerGuid,
                        sellerGuid => viewExpiringOrders.Where(s => s.SellerGuid == sellerGuid).OrderBy(x => x.LastShipDate).ThenBy(x => x.UniqueId).ToList());


                    foreach (var sellerGuid in sellerGroup)
                    {
                        #region 逾期出貨罰款
                       
                        VendorPaymentOverdueCollection vpoc = new VendorPaymentOverdueCollection();
                        foreach (var order in sellerOrderDic[sellerGuid].OrderBy(x => x.UniqueId))
                        {
                            List<string> orderList = order.OrderList.TrimEnd(",").Split(",").ToList();
                            //逾期出貨寫入罰款資料
                            foreach (string tmpOrderGuid in orderList)
                            {
                                Guid orderGuid = Guid.Empty;
                                Guid.TryParse(tmpOrderGuid, out orderGuid);

                                errorBid = order.BusinessHourGuid;
                                errorOrderGuid = orderGuid;
                                VendorPaymentOverdue overdue = ap.VendorPaymentOverdueGetByOrderGuid(orderGuid);
                                if (!overdue.IsLoaded)
                                {
                                    //不重複扣款
                                    var vpo = new VendorPaymentOverdue
                                    {
                                        BusinessHourGuid = order.BusinessHourGuid,
                                        OrderGuid = orderGuid,
                                        Reason = "延遲上傳出貨資訊-sys",
                                        PromotionAmount = 0,
                                        Amount = -200,
                                        BalanceSheetId = null,
                                        CreateId = config.SystemEmail,
                                        CreateTime = DateTime.Now
                                    };
                                    vpoc.Add(vpo);
                                }

                            }
                        }
                        ap.VendorPaymentOverdueSetList(vpoc);
                        
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex + "逾期出貨扣款job錯誤 bid:" + errorBid.ToString() + " orderGuid:" + errorOrderGuid.ToString());
            }
        }


    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Jobs
{
    public class LineShopOrderInfoJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(LineShopOrderInfoJob));
        private static ISysConfProvider config;
        private static LineShopOrderInfoJob theJob;
        static LineShopOrderInfoJob()
        {
            theJob = new LineShopOrderInfoJob();
        }

        public static LineShopOrderInfoJob Instance()
        {
            return theJob;
        }

        private LineShopOrderInfoJob()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            if (!config.EnableLineShop) return;
            try
            {
                //若是用atm、超商繳款，只有當日繳完，才是有效訂單。另加一天作為buffer，共兩天
                DateTime startTime = DateTime.Today.AddDays(-2);
                DateTime endTime = DateTime.Now;

                OrderFacade.LineShopOrderInfoAPI(startTime, endTime);
            }
            catch (Exception exc)
            {
                logger.Error("LineShopOrderInfoJob Error:" + exc.ToString());
            }
        }
    }
}

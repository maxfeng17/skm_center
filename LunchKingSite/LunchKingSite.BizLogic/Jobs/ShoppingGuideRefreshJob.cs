﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class ShoppingGuideRefreshJob : IJob
    {
        public void Execute(JobSetting js)
        {
            ChannelFacade.RefreshShoppingGuideCache();
        }
    }
}

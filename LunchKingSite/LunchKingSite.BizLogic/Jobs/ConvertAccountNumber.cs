﻿using System.Linq;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using log4net;

namespace LunchKingSite.BizLogic.Jobs
{
    public class ConvertAccountNumber : IJob
    {
        private static IOrderProvider op;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(ConvertAccountNumber));

        static ConvertAccountNumber()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            var count = config.EncryptCount;
            if(count > 0)
            {
                var source = op.MasterPassUnencryptGetList(count);
                if (source.Any())
                {                    
                    foreach (var item in source)
                    {
                        var result = new MasterpassLogTemp();
                        result.AccountNumber = MemberFacade.EncryptCreditCardInfo(item.AccountNumber, item.OrderGuid.ToString().Substring(0, 16));
                        result.LogId = item.Id;
                        op.MasterPassTempAdd(result);
                    }
                }
            }
        }
    }
}


﻿using System;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.BizLogic.Jobs
{
    public class RefreshGameRoundStatusJob : IJob
    {
        static IGameProvider gp = ProviderFactory.Instance().GetDefaultProvider<IGameProvider>();
        static ILog logger = LogManager.GetLogger(typeof(RefreshGameRoundStatusJob));
        public void Execute(JobSetting js)
        {
            try
            {
                if (GameFacade.GetCurrentGameCampaign() == null)
                {
                    return;
                }
                GameFacade.SetExpiredGameRoundStatus();
                logger.Info("RefreshGameRoundStatusJob done.");
            }
            catch (Exception ex)
            {
                logger.Error("RefreshGameRoundStatusJob fail.", ex);
            }
        }
    }
}

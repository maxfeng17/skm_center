﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using log4net;
using System.Net.Mail;

namespace LunchKingSite.BizLogic.Jobs
{
    class SendSoloEdmMail : IJob
    {
        public void Execute(JobSetting js)
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();

            try
            {
                string result = EmailFacade.SendSoloEdm();
                if (!string.IsNullOrEmpty(result))
                {
                    MailMessage msg = new MailMessage();
                    msg.To.Add(config.AdminEmail);
                    msg.Subject = "SoloEDM發送結束";
                    msg.From = new MailAddress(config.AdminEmail);
                    msg.IsBodyHtml = true;
                    msg.Body = result;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = "SoloEDM發生問題";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }
    }
}

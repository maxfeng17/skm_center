﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.OrderEntities;
using LunchKingSite.Core.Models.PponEntities;

namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary>
    /// 從每日匯入的 DealPageView 與 [Order], cash_trust_log 等，建立每日的銷售資料
    /// </summary>
    public class GenerateDealDailySalesInfoJob : IJob
    {
        private IPponEntityProvider pep = ProviderFactory.Instance().GetDefaultProvider<IPponEntityProvider>();
        private IOrderEntityProvider oep = ProviderFactory.Instance().GetDefaultProvider<IOrderEntityProvider>();
        public void Execute(JobSetting js)
        {

            //int days = -7;
            DateTime syncDate = DateTime.Today.AddDays(-1);            

            if (pep.GetDealDailySalesInfoCount(syncDate) == 0)
            {
                Sync(syncDate);
            }


            //for (int i = days + 1; i <= 0; i++)
            //{
            //    DateTime theDate = checkEndDate.AddDays(i);

            //    if (pep.GetDealDailySalesInfoCount(theDate) == 0)
            //    {
            //        Sync(theDate, theDate.AddDays(1));
            //    }
            //}

        }

        private void Sync(DateTime startDate)
        {
            DateTime endDate = startDate.AddDays(1);
            //前6天的資料，加上當天的資料，共7天的資料，用來算週轉換率。
            Dictionary<Guid, List<DealDailySalesInfo>> itemsPrior6Days =
                pep.GetPriorDealDailySalesInfos(startDate.AddDays(-6), startDate);
            List<DealDailySalesInfo> items = GetDealDailySalesInfos(startDate, endDate);
            SetWeeklyConverations(items, itemsPrior6Days);
            
            pep.DeleteDealDailySalesInfo(startDate);
            pep.BulkInsertDealDailySalesInfo(items);
        }

        private void SetWeeklyConverations(List<DealDailySalesInfo> items, Dictionary<Guid, List<DealDailySalesInfo>> priorDaysItems)
        {
            foreach (var item in items)
            {
                List<DealDailySalesInfo> priorItems = null;
                if (priorDaysItems.TryGetValue(item.MainDealId, out priorItems) == false)
                {
                    item.WeeklyConversionRate = item.ConversionRate;
                    item.WebWeeklyConversionRate = item.WebConversionRate;
                    item.AppWeeklyConversionRate = item.AppConversionRate;
                    item.MwebWeeklyConversionRate = item.MwebConversionRate;
                    item.IosAppWeeklyConversionRate = item.IosAppConversionRate;
                    item.AndroidAppWeeklyConversionRate = item.AndroidAppConversionRate;
                    item.IosWeeklyConversionRate = item.IosConversionRate;
                    item.AndroidWeeklyConversionRate = item.AndroidConversionRate;
                    continue;
                }
                if (item.ViewCount + priorItems.Sum(t => t.ViewCount) > 0)
                {
                    item.WeeklyConversionRate = (decimal)(item.OrderCount + priorItems.Sum(t => t.OrderCount))
                        / (item.ViewCount + priorItems.Sum(t => t.ViewCount));
                }
                if (item.WebViewCount + priorItems.Sum(t=>t.WebViewCount) > 0)
                {
                    item.WebWeeklyConversionRate = (decimal)(item.WebOrderCount + priorItems.Sum(t=>t.WebOrderCount)) 
                        / (item.WebViewCount + priorItems.Sum(t=>t.WebViewCount));
                }
                if (item.AppViewCount + priorItems.Sum(t=>t.AppViewCount) > 0)
                {
                    item.AppWeeklyConversionRate = (decimal)(item.AppOrderCount + priorItems.Sum(t=>t.AppOrderCount))
                        / (item.AppViewCount + priorItems.Sum(t=>t.AppViewCount));
                }
                if (item.MwebViewCount + priorItems.Sum(t=>t.MwebViewCount) > 0)
                {
                    item.MwebWeeklyConversionRate = (decimal)(item.MwebOrderCount + priorItems.Sum(t=>t.MwebOrderCount))
                        / (item.MwebViewCount + priorItems.Sum(t=>t.MwebViewCount));
                }
                if (item.IosAppViewCount + priorItems.Sum(t=>t.IosAppViewCount) > 0)
                {
                    item.IosAppWeeklyConversionRate = (decimal)(item.IosAppOrderCount + priorItems.Sum(t=>t.IosAppOrderCount))
                        / (item.IosAppViewCount + priorItems.Sum(t=>t.IosAppViewCount));
                }
                if (item.AndroidAppViewCount + priorItems.Sum(t=>t.AndroidAppViewCount) > 0)
                {
                    item.AndroidAppWeeklyConversionRate = (decimal)(item.AndroidAppOrderCount + priorItems.Sum(t => t.AndroidAppOrderCount))
                        / (item.AndroidAppViewCount + priorItems.Sum(t => t.AndroidAppViewCount));
                }
                if (item.IosViewCount + priorItems.Sum(t=>t.IosViewCount) > 0)
                {
                    item.IosWeeklyConversionRate =(decimal)(item.IosOrderCount + priorItems.Sum(t=>t.IosOrderCount)) 
                        / (item.IosViewCount + priorItems.Sum(t=>t.IosViewCount));
                }
                if (item.AndroidViewCount + priorItems.Sum(t=>t.AndroidViewCount) > 0)
                {
                    item.AndroidWeeklyConversionRate = (decimal)(item.AndroidOrderCount + priorItems.Sum(t => t.AndroidOrderCount))
                        / (item.AndroidViewCount + priorItems.Sum(t => t.AndroidViewCount));
                }
            }
        }

        private List<DealDailySalesInfo> GetDealDailySalesInfos(DateTime startDate, DateTime endDate)
        {
            List<DealPageView> pageViews = pep.GetDealPageViews(startDate, endDate);
            List<ViewOrderEntity> orderEntities = oep.GetSuccessfulViewOrderEntities(startDate, endDate);


            Dictionary<Guid, List<DealPageView>> groupingPageViews =
                pageViews.GroupBy(t => t.MainDealId).ToDictionary(t => t.Key, t => t.ToList());

            Dictionary<Guid, List<ViewOrderEntity>> groupingOrders = orderEntities
                .GroupBy(t => t.MainDealId).ToDictionary(t => t.Key, t => t.ToList());

            DateTime now = DateTime.Now;

            List<DealDailySalesInfo> items = new List<DealDailySalesInfo>();
            foreach (var pair in groupingPageViews)
            {
                DealDailySalesInfo item = new DealDailySalesInfo();
                var dpvs = pair.Value;
                Guid mainDealId = pair.Key;
                int webViewCount = dpvs.Where(t => t.DeviceType == 1).Select(t => t.VisitorIdentity).Distinct().Count();
                int iOSViewCount = dpvs.Where(t => t.DeviceType == 2).Select(t => t.VisitorIdentity).Distinct().Count();
                int androidViewCount = dpvs.Where(t => t.DeviceType == 3).Select(t => t.VisitorIdentity).Distinct().Count();
                int mWebViewCount = dpvs.Where(t => t.DeviceType == 5).Select(t => t.VisitorIdentity).Distinct().Count();
                int mWebAndroidViewCount = dpvs.Where(t => t.DeviceType == 6).Select(t => t.VisitorIdentity).Distinct().Count();
                int mWebiOSViewCount = dpvs.Where(t => t.DeviceType >= 7 && t.DeviceType <= 9).Select(t => t.VisitorIdentity).Distinct().Count();
                //item.ViewCount = webViewCount + iOSViewCount + androidViewCount + mWebViewCount + mWebAndroidViewCount + mWebiOSViewCount;
                item.ViewCount = dpvs.Select(t => t.VisitorIdentity).Distinct().Count();
                item.WebViewCount = webViewCount;
                item.AppViewCount = iOSViewCount + androidViewCount;
                item.MwebViewCount = mWebViewCount + mWebAndroidViewCount + mWebiOSViewCount;
                item.IosAppViewCount = iOSViewCount;
                item.AndroidAppViewCount = androidViewCount;
                item.IosViewCount = iOSViewCount + mWebiOSViewCount;
                item.AndroidViewCount = androidViewCount + mWebAndroidViewCount;
                List<ViewOrderEntity> orders;
                if (groupingOrders.TryGetValue(mainDealId, out orders))
                {
                    int webOrderCount = orders.Where(t => t.OrderFromType == 1).Count();
                    int iOSOrderCount = orders.Where(t => t.OrderFromType == 2).Count();
                    int androidOrderCount = orders.Where(t => t.OrderFromType == 3).Count();
                    int mWebOrderCount = orders.Where(t => t.OrderFromType == 5).Count();
                    int mWebAndroidOrderCount = orders.Where(t => t.OrderFromType == 6).Count();
                    int mWebiOSOrderCount = orders.Where(t => t.OrderFromType >= 7 && t.OrderFromType <= 9).Count();
                    item.OrderCount = orders.Count;
                    item.WebOrderCount = webOrderCount;
                    item.AppOrderCount = iOSOrderCount + androidOrderCount;
                    item.MwebOrderCount = mWebOrderCount + mWebAndroidOrderCount + mWebiOSOrderCount;
                    item.IosAppOrderCount = iOSOrderCount;
                    item.AndroidAppOrderCount = androidOrderCount;
                    item.IosOrderCount = iOSOrderCount + mWebiOSOrderCount;
                    item.AndroidOrderCount = androidOrderCount + mWebAndroidOrderCount;


                    int webTurnover = orders.Where(t => t.OrderFromType == 1).Select(t => t.Turnover).Sum();
                    int iOSTurnover = orders.Where(t => t.OrderFromType == 2).Select(t => t.Turnover).Sum();
                    int androidTurnover = orders.Where(t => t.OrderFromType == 3).Select(t => t.Turnover).Sum();
                    int mWebTurnover = orders.Where(t => t.OrderFromType == 5).Select(t => t.Turnover).Sum();
                    int mWebAndroidTurnover = orders.Where(t => t.OrderFromType == 6).Select(t => t.Turnover).Sum();
                    int mWebiOSTurnover = orders.Where(t => t.OrderFromType >= 7 && t.OrderFromType <= 9).Select(t => t.Turnover).Sum();
                    item.Turnover = orders.Sum(t => t.Turnover);
                    item.WebTurnover = webTurnover;
                    item.AppTurnover = iOSTurnover + androidTurnover;
                    item.MwebTurnover = mWebTurnover + mWebAndroidTurnover + mWebiOSTurnover;
                    item.IosAppTurnover = iOSTurnover;
                    item.AndroidAppTurnover = androidTurnover;
                    item.IosTurnover = iOSTurnover + mWebiOSTurnover;
                    item.AndroidTurnover = androidTurnover + mWebAndroidTurnover;

                    if (item.ViewCount > 0)
                    {
                        item.ConversionRate = (decimal)item.OrderCount / item.ViewCount;
                    }
                    if (item.WebViewCount > 0)
                    {
                        item.WebConversionRate = (decimal)item.WebOrderCount / item.WebViewCount;
                    }
                    if (item.AppViewCount > 0)
                    {
                        item.AppConversionRate = (decimal)item.AppOrderCount / item.AppViewCount;
                    }
                    if (item.MwebViewCount > 0)
                    {
                        item.MwebConversionRate = (decimal)item.MwebOrderCount / item.MwebViewCount;
                    }
                    if (item.IosAppViewCount > 0)
                    {
                        item.IosAppConversionRate = (decimal)item.IosAppOrderCount / item.IosAppViewCount;
                    }
                    if (item.AndroidAppViewCount > 0)
                    {
                        item.AndroidAppConversionRate = (decimal)item.AndroidAppOrderCount / item.AndroidAppViewCount;
                    }
                    if (item.IosViewCount > 0)
                    {
                        item.IosConversionRate = (decimal)item.IosOrderCount / item.IosViewCount;
                    }
                    if (item.AndroidViewCount > 0)
                    {
                        item.AndroidConversionRate = (decimal)item.AndroidOrderCount / item.AndroidViewCount;
                    }
                }
                item.TheDate = startDate;
                item.MainDealId = pair.Key;
                item.CreateTime = now;
                items.Add(item);
            }
            return items;
        }

    }
}

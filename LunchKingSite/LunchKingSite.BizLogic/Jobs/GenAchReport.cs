﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using System.Linq;

namespace LunchKingSite.BizLogic.Jobs
{
    public class GenAchReport : IJob
    {
        protected static ISysConfProvider cp;
        protected static IOrderProvider op;      
        protected static IMemberProvider mp;
        protected static IAccountingProvider ap;
        protected static IPponProvider pp;
        protected static ISellerProvider sp;
        protected static IHiDealProvider hp;

        static GenAchReport()
        {
            cp = ProviderFactory.Instance().GetConfig();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        }

        public void Execute(JobSetting js)
        {
            string mode = (js == null || js.Parameters["mode"] == null)
                            ? string.Empty 
                            : js.Parameters["mode"];

            BalanceSheetCollection bsCol = ap.BalanceSheetGetList(null, BalanceSheet.Columns.IsConfirmedReadyToPay + " = " + true,
                                                                        BalanceSheet.Columns.IsManual + " = " + false,
                                                                        BalanceSheet.Columns.PayReportId + " is null ",
                                                                        BalanceSheet.Columns.IntervalEnd + " <= " + DateTime.Today,
                                                                        BalanceSheet.Columns.ProductType + " = " + (int)BusinessModel.Ppon);
            switch (mode)
            {
                case "PayByBill":

                    #region 先收單據後付款

                    //須依不同出帳方式 產出Ach檔案
                    var bsCols = bsCol.Where(x => (x.BalanceSheetType == (int)BalanceSheetType.WeeklyToHouseBalanceSheet
                                               || x.BalanceSheetType == (int)BalanceSheetType.MonthlyToHouseBalanceSheet
                                               || x.BalanceSheetType == (int)BalanceSheetType.FlexibleToHouseBalanceSheet
                                               || x.BalanceSheetType == (int)BalanceSheetType.MonthlyPayBalanceSheet
                                               || x.BalanceSheetType == (int)BalanceSheetType.WeeklyPayBalanceSheet
                                               || x.BalanceSheetType == (int)BalanceSheetType.FlexiblePayBalanceSheet)
                                               && x.EstAmount > 0);
                    if (!bsCols.Any())
                    {
                        return;
                    }

                    //請款凍結檔次不產出ACH匯款檔案 
                    var dealFreezeInfos = pp.DealAccountingGet(bsCols.Select(x => x.ProductGuid))
                        .ToDictionary(x => x.BusinessHourGuid,
                                      x => Helper.IsFlagSet(x.Flag, (int) AccountingFlag.Freeze));

                    #region 宅配

                    //宅配週結、月結及彈性請款
                    var bsInfos = bsCols.Where(x => x.BalanceSheetType == (int)BalanceSheetType.WeeklyToHouseBalanceSheet
                                                 || x.BalanceSheetType == (int)BalanceSheetType.MonthlyToHouseBalanceSheet
                                                 || x.BalanceSheetType == (int)BalanceSheetType.FlexibleToHouseBalanceSheet);
                    if (bsInfos.Any())
                    {

                        AchRemit(Guid.NewGuid(), bsInfos.Where(x => dealFreezeInfos.ContainsKey(x.ProductGuid) && !dealFreezeInfos[x.ProductGuid])
                                                        .OrderBy(x => x.ConfirmedUserName)
                                                        .ThenBy(x => x.ConfirmedTime));
                    }    
                    
                    #endregion 宅配

                    #region 憑證

                    //週結、月結及彈性請款
                    bsInfos = bsCol.Where(x =>  x.BalanceSheetType == (int)BalanceSheetType.WeeklyPayBalanceSheet
                                             || x.BalanceSheetType == (int)BalanceSheetType.MonthlyPayBalanceSheet
                                             || x.BalanceSheetType == (int)BalanceSheetType.FlexiblePayBalanceSheet);
                    if (bsInfos.Any())
                    {
                        AchRemit(Guid.NewGuid(), bsInfos.Where(x => dealFreezeInfos.ContainsKey(x.ProductGuid) && !dealFreezeInfos[x.ProductGuid])
                                                        .OrderBy(x => x.ConfirmedUserName)
                                                        .ThenBy(x => x.ConfirmedTime));
                    }       
             
                    #endregion 憑證

                    #endregion 先收單據後付款

                    break;
                default:

                    #region P好康ACH週結

                    IEnumerable<WeeklyPayReport> reportCol = op.WeeklyPayReportGetList(0, 0, null, WeeklyPayReport.Columns.IntervalEnd + " = " + DateTime.Today);
                    Guid weeklyReportGuid = (reportCol.Any() && reportCol.First().ReportGuid != null) ? reportCol.First().ReportGuid : Guid.NewGuid();
                    AchRemit(weeklyReportGuid, bsCol.Where(x => x.BalanceSheetType == (int)BalanceSheetType.WeeklyPayWeekBalanceSheet));
            
                    #endregion P好康ACH週結

                    break;
            }

        }

        private void AchRemit(Guid reportGuid, IEnumerable<BalanceSheet> bsCol)
        {
            if (bsCol == null || !bsCol.Any())
            {
                return;
            }

            foreach (var bs in bsCol)
            {
                var bsModel = new BalanceSheetModel(bs.Id);

                BusinessHour bh = sp.BusinessHourGet(bs.ProductGuid);
                CashTrustLogCollection ctLogNormal = mp.CashTrustLogGetListByBalanceSheetId(bs.Id, BalanceSheetDetailStatus.Normal);
                CashTrustLogCollection ctLogDeduction = mp.CashTrustLogGetListByBalanceSheetId(bs.Id, BalanceSheetDetailStatus.Deduction);

                int payReportId = 0;
                if (ctLogNormal.Count() - ctLogDeduction.Count() != 0)
                {
                    DealCostCollection costs = pp.DealCostGetList(bs.ProductGuid);
                    DealProperty dp = pp.DealPropertyGet(bs.ProductGuid);
                    decimal cost = (costs.Any() && costs.Max(x => x.Cost).HasValue) ? costs.Max(x => x.Cost).Value : 0;
                    cost = cost > 0 && dp.SaleMultipleBase > 0 ? cost / dp.SaleMultipleBase.Value : cost;
                    //填入CashTrustLog報表代號
                    mp.CashTrustLogToUpdateForAch(reportGuid, bs.Id);

                    #region 新增WeeklyPayReport資料
                    WeeklyPayReport report = new WeeklyPayReport();

                    report.BusinessHourGuid = bs.ProductGuid;
                    report.IntervalStart = bs.IntervalStart;
                    report.IntervalEnd = bs.IntervalEnd;
                    report.CreditCard = ctLogNormal.Sum(x => x.CreditCard) - ctLogDeduction.Sum(x => x.CreditCard);
                    report.Atm = ctLogNormal.Sum(x => x.Atm) - ctLogDeduction.Sum(x => x.Atm);
                    report.ReportGuid = reportGuid;
                    report.IsLastWeek = (bs.IntervalStart <= bh.BusinessHourDeliverTimeE && bh.BusinessHourDeliverTimeE < bs.IntervalEnd);
                    report.Cost = cost;
                    report.TotalCount = ctLogNormal.Count() - ctLogDeduction.Count();
                    report.TotalSum = bsModel.GetAccountsPayable().TotalAmount;
                    report.UserId = cp.SystemEmail;
                    report.CreateTime = DateTime.Now;
                    report.StoreGuid = bs.StoreGuid;
                    report.FundTransferType = (bs.IsManual) ?(int)FundTransferType.FinDept :(int)FundTransferType.CtAch;

                    payReportId = op.SetWeeklyPayReport(report);
                    #endregion
                }
                else
                {
                    if (bs.GenerationFrequency == (int)BalanceSheetGenerationFrequency.WeekBalanceSheet)
                    {
                        bs.Year = DateTime.Now.Year;
                        bs.Month = DateTime.Now.Month;
                    }
                }

                #region 對帳單回填reportId
                bs.PayReportId = payReportId;
                ap.BalanceSheetSet(bs);
                #endregion
            }

            #region 加入統計列

            var bsInfo = bsCol.First();
            if (!bsInfo.IsManual)
            {
                IEnumerable<WeeklyPayReport> weeklyReports = op.WeeklyPayReportGetList(0, 0, null, WeeklyPayReport.Columns.ReportGuid + " = " + reportGuid);
                
                if (weeklyReports.Any())
                {
                    var intervalStart = DateTime.Now.Date.AddDays(-7);
                    var intervalEnd = DateTime.Now.Date;
                    if (bsInfo.BalanceSheetType != (int) BalanceSheetType.WeeklyPayWeekBalanceSheet)
                    {
                        //先收單據後付款檔次 依據確認單據時間 產出weekly_pay_report資料
                        intervalStart = bsCol.Min(b => b.ConfirmedTime).HasValue
                                        ? bsCol.Min(b => b.ConfirmedTime).Value
                                        : intervalStart;
                        intervalEnd = bsCol.Max(b => b.ConfirmedTime).HasValue
                                        ? bsCol.Max(b => b.ConfirmedTime).Value
                                        : intervalEnd;
                    }
                    var report = new WeeklyPayReport();

                    report.BusinessHourGuid = reportGuid;
                    report.IntervalStart = intervalStart;
                    report.IntervalEnd = intervalEnd;
                    report.CreditCard = weeklyReports.Sum(x => x.CreditCard);
                    report.Atm = weeklyReports.Sum(x => x.Atm);
                    report.ReportGuid = reportGuid;
                    report.UserId = cp.SystemEmail;
                    report.CreateTime = DateTime.Now;
                    report.ErrorInSum = weeklyReports.Sum(x => x.ErrorInSum);
                    report.ErrorOutSum = weeklyReports.Sum(x => x.ErrorOutSum);
                    report.ErrorIn = weeklyReports.Sum(x => x.ErrorIn);
                    report.ErrorOut = weeklyReports.Sum(x => x.ErrorOut);
                    report.TotalCount = weeklyReports.Sum(x => x.TotalCount);
                    report.TotalSum = weeklyReports.Sum(x => x.TotalSum);
                    report.FundTransferType = (int)FundTransferType.CtAch;

                    op.SetWeeklyPayReport(report);
                }
            }

            #endregion
        }

        #region 品生活ACH匯款方法
        /*
        private void AchRemitForPiinlife(Guid reportGuid, IEnumerable<BalanceSheet> bsCol)
        {
            if (bsCol == null || bsCol.Count() == 0)
            {
                return;
            }

            foreach (var bs in bsCol)
            {
                HiDealProduct product = hp.HiDealProductGet(bs.ProductGuid);
                ViewHiDealCouponTrustStatusCollection ctLogNormal = hp.ViewHiDealCouponTrustStatusGetListByBalanceSheetId(bs.Id, BalanceSheetDetailStatus.Normal);
                ViewHiDealCouponTrustStatusCollection ctLogDeduction = hp.ViewHiDealCouponTrustStatusGetListByBalanceSheetId(bs.Id, BalanceSheetDetailStatus.Deduction);

                int payReportId = 0;
                if (ctLogNormal.Sum(x => x.Cost) - ctLogDeduction.Sum(x => x.Cost) > 0)
                {
                    //填入CashTrustLog報表代號
                    mp.CashTrustLogToUpdateForAch(reportGuid, bs.Id);

                    #region 新增WeeklyPayReport資料
                    HiDealWeeklyPayReport report = new HiDealWeeklyPayReport();

                    report.DealId = product.DealId;
                    report.ProductId = product.Id;
                    report.IntervalStart = bs.IntervalStart;
                    report.IntervalEnd = bs.IntervalEnd;
                    report.ReportGuid = reportGuid;
                    report.IsLastWeek = (bs.IntervalStart <= product.UseEndTime && product.UseEndTime < bs.IntervalEnd);
                    report.TotalCount = ctLogNormal.Count() - ctLogDeduction.Count();
                    report.TotalSum = ctLogNormal.Sum(x => x.Cost) - ctLogDeduction.Sum(x => x.Cost);
                    report.Creator = cp.SystemEmail;
                    report.CreateTime = DateTime.Now;
                    report.StoreGuid = bs.StoreGuid;
                    report.IsSummary = false;

                    payReportId = hp.HiDealWeeklyPayReportSet(report);
                    #endregion
                }
                else
                {
                    if (bs.GenerationFrequency == (int)BalanceSheetGenerationFrequency.WeekBalanceSheet)
                    {
                        bs.Year = DateTime.Now.Year;
                        bs.Month = DateTime.Now.Month;
                    }
                }

                #region 對帳單回填reportId
                bs.PayReportId = payReportId;
                ap.BalanceSheetSet(bs);
                #endregion
            }

            #region 加入統計列
            if (!bsCol.First().IsManual)
            {
                IEnumerable<HiDealWeeklyPayReport> weeklyReports = hp.HiDealWeeklyPayReportCollectionGetList(0, 0, null, HiDealWeeklyPayReport.Columns.ReportGuid + " = " + reportGuid);

                if (weeklyReports.Count() > 0)
                {
                    HiDealWeeklyPayReport report = new HiDealWeeklyPayReport();

                    report.DealId = 0;
                    report.ProductId = 0;
                    report.IntervalStart = weeklyReports.Min(x => x.IntervalStart);
                    report.IntervalEnd = weeklyReports.Max(x => x.IntervalEnd);
                    report.ReportGuid = reportGuid;
                    report.TotalCount = weeklyReports.Sum(x => x.TotalCount);
                    report.TotalSum = weeklyReports.Sum(x => x.TotalSum);
                    report.Creator = cp.SystemEmail;
                    report.CreateTime = DateTime.Now;
                    report.IsSummary = true;

                    hp.HiDealWeeklyPayReportSet(report);
                }
            }
            #endregion
        }
        */
        #endregion
    }
}
﻿using System;
using System.Net;
using LunchKingSite.BizLogic.Component.API;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System.Linq;
using LunchKingSite.DataOrm;
using LunchKingSite.SsBLL;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Linq;
using System.Reflection;


namespace LunchKingSite.BizLogic.Jobs
{
    public class DataManagerResetJob : IJob
    {
        private static ISystemProvider _systemProv;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DataManagerResetJob));
        private static readonly string HostName = Dns.GetHostName();

        static DataManagerResetJob()
        {
            _systemProv = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        }

        public void Execute(JobSetting js)
        {
            DataManagerReset();
        }

        public static void DataManagerReset()
        {

            //好康的ViewPponDealManager一定要重整。
            ViewPponDealManager.ReloadDefaultManager();


            var coll = _systemProv.DataManagerResettimeGetListWithEnabled();

            foreach (var resettime in coll)
            {
                //Deal with RunOnce jobs
                if (resettime.JobType == (int)DataManagerResettimeJobType.RunOnce)
                {
                    DataManagerResettime single = _systemProv.DataManagerResettimeGetWithJobType(
                        (int)DataManagerResettimeJobType.RunOnce, false, resettime.NextRunningTime, HostName)
                        .FirstOrDefault();

                    if (single == null) //Never Run at this Server
                    {
                        bool success = RunReset(resettime);

                        if (success)
                        {
                            _systemProv.DataManagerResettimeSetData(new DataManagerResettime
                            {
                                ManagerName = resettime.ManagerName,
                                LastRunningTime = DateTime.Now,
                                NextRunningTime = resettime.NextRunningTime,
                                IntervalMinute = resettime.IntervalMinute,
                                Enabled = false,
                                JobType = resettime.JobType,
                                ServerName = HostName
                            });
                        }
                    }
                }
                else
                {
                    RunReset(resettime);
                }
            }
            _systemProv.DataManagerResettimeSetList(coll);
        }

        /// <summary>
        /// 執行DataManagerResettime
        /// </summary>
        /// <param name="resettime"></param>
        /// <returns></returns>
        private static bool RunReset(DataManagerResettime resettime)
        {
            try
            {
                switch (resettime.ManagerName)
                {
                    case ViewPponStoreManager.ManagerNameForJob:
                        if ((resettime.ServerName == null || resettime.ServerName == HostName) && (resettime.NextRunningTime <= DateTime.Now))
                        {
                            ViewPponStoreManager.ReloadDefaultManager();
                            resettime.LastRunningTime = DateTime.Now;
                            resettime.NextRunningTime =
                                Helper.GetNextDataManagerResetRunningTime(resettime.LastRunningTime.Value, resettime);
                            return true;
                        }
                        break;
                    case ApiVourcherManager.ManagerNameForJob:
                        if ((resettime.ServerName == null || resettime.ServerName == HostName) && (resettime.NextRunningTime <= DateTime.Now))
                        {
                            ApiVourcherManager.ReloadDataManager();
                            resettime.LastRunningTime = DateTime.Now;
                            resettime.NextRunningTime =
                                Helper.GetNextDataManagerResetRunningTime(resettime.LastRunningTime.Value, resettime);
                            return true;
                        }
                        break;
                    case ApiPromoEventManager.ManagerNameForJob:
                        if ((resettime.ServerName == null || resettime.ServerName == HostName) && (resettime.NextRunningTime <= DateTime.Now))
                        {
                            ApiPromoEventManager.ReloadDataManager();
                            resettime.LastRunningTime = DateTime.Now;
                            resettime.NextRunningTime =
                                Helper.GetNextDataManagerResetRunningTime(resettime.LastRunningTime.Value, resettime);
                            return true;
                        }
                        break;
                    default://Deal with JSON Object
                        if (!IsJsonValidate(resettime.ManagerName))
                        {
                            break;
                        }

                        DataManagerResetViewModel reset = JsonConvert.DeserializeObject<DataManagerResetViewModel>(resettime.ManagerName);
                        //reflection invoke method

                        var macroClasses = Assembly.GetExecutingAssembly().GetTypes().Where(
                            x => x.Namespace != null && x.Namespace.Contains(reset.NameSpace) 
                            && x.Name.Equals(reset.ClassName));
                        foreach (var tempClass in macroClasses)
                        {
                            try
                            {
                                // using reflection I will be able to run the method as:
                                tempClass.GetMethod(reset.MethodName).Invoke(null, reset.Parameters);
                                resettime.LastRunningTime = DateTime.Now;
                                return true;
                            }
                            catch (Exception ex)
                            {
                                Log.Error("DataManagerResetJob Error:ManagerName is " + resettime.ManagerName, ex);
                                return false;
                            }
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                Log.Error("DataManagerResetJob Error:ManagerName is " + resettime.ManagerName, e);
            }
            return false;
        }
        /// <summary>
        /// 判斷Json字串格式
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        private static bool IsJsonValidate(string json)
        {
            try
            {
                JObject data = JObject.Parse(json);
                var nameSpace = data.Properties().Where(p => p.Name == "NameSpace").Select(p => p.Value).FirstOrDefault();
                var className = data.Properties().Where(p => p.Name == "ClassName").Select(p => p.Value).FirstOrDefault();
                var methodName = data.Properties().Where(p => p.Name == "MethodName").Select(p => p.Value).FirstOrDefault();

                if (string.IsNullOrWhiteSpace(nameSpace.Value<string>()) || 
                    string.IsNullOrWhiteSpace(className.Value<string>()) || 
                    string.IsNullOrWhiteSpace(methodName.Value<string>()))
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
            }

            return false;
        }
    }
}

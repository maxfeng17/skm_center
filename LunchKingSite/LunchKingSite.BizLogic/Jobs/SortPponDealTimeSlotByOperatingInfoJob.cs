﻿using System;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using log4net;

namespace LunchKingSite.BizLogic.Jobs
{
    class SortPponDealTimeSlotByOperatingInfoJob : IJob
    {
        private static ILog Log = LogManager.GetLogger(typeof(SortPponDealTimeSlotByOperatingInfoJob));
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public void Execute(JobSetting js)
        {
            if (config.IsSortDealTimeSlotSequence)
            {
                try
                {
                    PponFacade.SortDealTimeSlotByOperatingInfo(-1, "JOB排序");
                }
                catch (Exception ex)
                {
                    Log.Error("SortPponDealTimeSlotByOperatingInfoJob running alert Error:" + ex.Message + "<br/>" +
                              ex.StackTrace);
                }
            }
            else
            {
                Log.Info("DealTimeSlot 自動排序未啟用");
            }
        }
    }
}

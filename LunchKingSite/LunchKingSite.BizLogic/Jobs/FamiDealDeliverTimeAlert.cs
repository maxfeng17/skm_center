﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Jobs
{
    public class FamiDealDeliverTimeAlert : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(FamiDealDeliverTimeAlert));
        private static ISysConfProvider _config;
        private static FamiDealDeliverTimeAlert _theJob;

        static FamiDealDeliverTimeAlert() 
        {
            _theJob = new FamiDealDeliverTimeAlert();
        }

        public static FamiDealDeliverTimeAlert Instance()
        {
            return _theJob;
        }

        private FamiDealDeliverTimeAlert()
        {
            _config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js) 
        {
            try
            {
                OrderFacade.SendFamiDealDeliverTimeAlert(_config.FamiDealDeliverTimeAlertDay);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("FamiDealDeliverTimeAlert Message:({0}), {1}", ex.Message, ex.StackTrace);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs
{
    public class CheckPponStoreOrderedQuantity : IJob
    {
        protected static IPponProvider pp;

        private static ILog logger = LogManager.GetLogger(typeof(CheckPponStoreOrderedQuantity));

        static CheckPponStoreOrderedQuantity()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        public void Execute(JobSetting js)
        {
            StringBuilder sbLog = new StringBuilder();
            var orderQuantitys = pp.ViewOrderStoreQuantityGetListByToday();
            foreach (var o in orderQuantitys)
            {
                if (o.StoreGuid != null)
                {
                    var pponStore = pp.PponStoreGet(o.BusinessHourGuid, o.StoreGuid.Value);
                    if (pponStore.IsLoaded)
                    {
                        if (pponStore.OrderedQuantity != o.OrderedQuantity)
                        {
                            sbLog.AppendFormat("{0},{1},{2}\r\n", pponStore.StoreGuid, pponStore.OrderedQuantity, o.OrderedQuantity);
                            pp.PponStoreUpdateOrderQuantity(
                                pponStore.BusinessHourGuid, pponStore.StoreGuid, o.OrderedQuantity ?? 0, true);
                        }
                    }
                }
            }
            if (sbLog.Length > 0)
            {
                logger.Info("CheckPponStoreOrderedQuantity發揮了作用:\r\n" + sbLog);
            }
        }
    }
}

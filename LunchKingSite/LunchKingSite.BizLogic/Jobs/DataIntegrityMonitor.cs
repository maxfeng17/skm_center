﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Mail;
using LunchKingSite.Core;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System.Data;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.BizLogic.Jobs
{
    public class DataIntegrityMonitor : IJob
    {
        private static IPponProvider pp;
        private static IOrderProvider op;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(DataIntegrityMonitor));

        static DataIntegrityMonitor()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            //未成立單款項退回
            OrderCollection rawOrders = pp.OverchargedMonitorJob();

            List<Order> ordersNoCart = rawOrders.Where(t => t.CartGuid == null).ToList();

            string orderId = string.Empty;
            try
            {
                foreach (var o in ordersNoCart)
                {
                    orderId = o.OrderId;
                    PaymentFacade.CancelPaymentTransactionByOrderForIncompleteOrder(o, "sys");
                }
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = Environment.MachineName + ": 未成立單款項退回問題:" + orderId;
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error("未成立單款項退回問題: orderId=" + orderId + ex.Message + "<br/>" + ex.StackTrace);
            }

            string cartGuidString = string.Empty;
            List<Order> ordersWithCart = rawOrders.Where(t => t.CartGuid != null).ToList();
            try
            {
                foreach (Guid? cartGuid in ordersWithCart.Select(t => t.CartGuid).Distinct())
                {
                    cartGuidString = cartGuid.ToString();
                    ShoppingCartGroup sc = op.ShoppingCartGroupGet(cartGuid.GetValueOrDefault());
                    if (sc.IsLoaded == false)
                    {
                        continue;
                    }
                    PaymentFacade.CancelPaymentTransactionByOrderForIncompleteOrder(sc, "sys");
                }
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = Environment.MachineName + ": 未成立單款項退回問題:" + orderId;
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error("未成立單款項退回問題: cartGuid=" + cartGuidString + ex.Message + "<br/>" + ex.StackTrace);
            }

            #region ApplePay未成單退回
            PaymentTransactionCollection pts = pp.OverchargedWithoutOrderMonitorJob();
            if (pts.Count > 0)
            {
                string transId = string.Empty;
                try
                {
                    foreach (var pt in pts)
                    {
                        transId = pt.TransId;
                        if (Component.CreditCardUtility.AuthenticationReverse(transId,string.Empty))
                        {
                            PaymentFacade.CancelPaymentTransactionByTransId(transId, "sys", "Unorderable. ");
                        };
                    }
                }
                catch (Exception ex)
                {
                    MailMessage msg = new MailMessage();
                    msg.To.Add(config.AdminEmail);
                    msg.Subject = Environment.MachineName + ": ApplePay未成立單款項退回問題:" + transId;
                    msg.From = new MailAddress(config.AdminEmail);
                    msg.IsBodyHtml = true;
                    msg.Body = ex.Message + ex.StackTrace;

                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    logger.Error("ApplePay未成立單款項退回問題:" + transId + ex.Message + "<br/>" + ex.StackTrace);
                }
            }

            #endregion

            #region OTP已授權但無postBack回來成立訂單
            string OTPTransId = string.Empty;
            try
            {
                PaymentTransactionCollection list = pp.GetOtpNotPostBack();
                
                foreach (PaymentTransaction pt in list)
                {
                    OTPTransId = pt.TransId;
                    CreditCardQueryDetail detail = CreditCardUtility.Query(pt.TransId);
                    if (detail.ReturnCode == "00" && detail.OrderStatus == "02")
                    {
                        //訂單未成立但已授權
                        CreditCardUtility.AuthenticationReverse(pt.TransId, pt.AuthCode);

                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon),
                        PayTransPhase.Canceled);
                        pt.Status = status;
                        op.PaymentTransactionSet(pt);
                        CommonFacade.AddAudit(pt.OrderGuid.ToString(), AuditType.Payment, "OTP未成立單取消授權", "sys", true);
                    }
                }

            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = Environment.MachineName + ": OTP未成立單取消授權問題:" + OTPTransId;
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error("OTP未成立單取消授權問題:" + OTPTransId + ex.Message + "<br/>" + ex.StackTrace);
            }
            #endregion


            #region 訂單與刷卡授權不一致
            string InconsistentTransId = string.Empty;
            try
            {
                List<string> list = pp.GetCreditcatrInconsistent();

                foreach (string pt in list)
                {
                    InconsistentTransId = pt;
                    logger.Error("訂單與刷卡授權不一致:" + pt);
                }

            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = Environment.MachineName + ": 訂單與刷卡授權不一致問題:" + InconsistentTransId;
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error("訂單與刷卡授權不一致問題:" + InconsistentTransId + ex.Message + "<br/>" + ex.StackTrace);
            }
            #endregion


        }
    }
}

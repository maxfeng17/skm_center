﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.Skm;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Jobs
{
    public class SkmOrderPushReminderPostJob : IJob
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SkmOrderPushReminderPostJob));

        public void Execute(JobSetting js)
        {
            PostData();
        }

        private void PostData()
        {
            var failedResults = new List<SkmPushResponse>();

            foreach (var item in SkmFacade.GetPushData())
            {
                var response = SkmFacade.PostSkmPushData(item);

                if (!SkmFacade.IsSkmPushSuccessed(response))
                {
                    failedResults.Add(response);
                }
            }

            if (failedResults.Any())
            {
                Logger.ErrorFormat("failedResults : {0}", JsonConvert.SerializeObject(failedResults));
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class AutoVerifyCoupon : IJob
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(AutoVerifyCoupon));
        
        static AutoVerifyCoupon()
        {
        }

        public void Execute(JobSetting js)
        {
            try
            {
                CouponFacade.AutoVerifyCouponJob();
            }
            catch (Exception ex)
            {
                logger.Error("富利核銷發生錯誤:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

    }

}
﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using System;

namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary> Skm Pay OTP 逾時退款 </summary>
    public class SkmPayOTPTimeoutRefundJob : IJob
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(SkmPayOTPTimeoutRefundJob));
        public void Execute(JobSetting js)
        {
            //精選優惠
            logger.Info("SkmPayOTPTimeoutRefundJob Start");
            try
            {
                SkmFacade.SkmPayOTPTimeoutRefundJob(logger);
            }
            catch (Exception ex)
            {
                logger.Error("SkmPayOTPTimeoutRefundJob.EX:" + ex);
            }
            logger.Info("SkmPayOTPTimeoutRefundJob End");

            //停車
            logger.Info("SkmParkingPayOTPTimeoutRefundJob Start");
            try
            {
                SkmFacade.SkmPayParkingOTPTimeoutRefundJob(logger);
            }
            catch (Exception ex)
            {
                logger.Error("SkmParkingPayOTPTimeoutRefundJob.EX:" + ex);
            }
            logger.Info("SkmParkingPayOTPTimeoutRefundJob End");
            
        }
    }
}

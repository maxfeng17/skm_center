﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Component;
using log4net;
using LunchKingSite.Core.Interface;
using System.Text;
using System.Net;
using System.IO;
using LunchKingSite.Core.Enumeration;
using System.Linq;

namespace LunchKingSite.BizLogic.Jobs
{
    public class HiLifeBatchReturnJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(HiLifeFacade).Name);
        private static IHiLifeProvider hiLife = ProviderFactory.Instance().GetProvider<IHiLifeProvider>();
        protected static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public void Execute(JobSetting js)
        {
            BatchReturn();
            BatchVerify();
        }


        /// <summary>
        /// 上傳退貨資訊至FTP
        /// </summary>
        public void BatchReturn()
        {
            try
            {
                DateTime time = DateTime.Now.AddMinutes(-15);

                var json = new JsonSerializer();

                List<HiLifeReturnLog> returnList = hiLife.GetHiLifeReturnDataByTime(time);
                if (returnList.Any())
                {
                    HiLifeReturnLogModel model = new HiLifeReturnLogModel();
                    List<HiLifeReturnDetailLogModel> detailList = new List<Jobs.HiLifeReturnDetailLogModel>();

                    foreach (HiLifeReturnLog log in returnList)
                    {
                        HiLifeReturnDetailLogModel detail = new HiLifeReturnDetailLogModel();
                        detail.Pincode = log.Pincode;
                        detail.ReturnDate = log.ReturnDate;
                        detailList.Add(detail);
                    }
                    model.Detail = detailList;
                    model.Time = time.ToString("yyyyMMddHHmm00");
                    string jsonData = json.Serialize(model);


                    string strResult = string.Empty;
                    ApiResult apiResult = null;
                    int times = 0;
                    do
                    {
                        strResult = HiLifeFacade.ApiRequest("api/HiLife/PostBatchReturnData", jsonData);
                        strResult = strResult.Replace("State", "Code");
                        apiResult = json.Deserialize<ApiResult>(strResult);
                        times++;
                    }
                    while (apiResult.Code != ApiResultCode.Success && times < 4);


                    if (apiResult.Code == ApiResultCode.Success)
                    {
                        //成功
                    }
                    else
                    {
                        logger.Error("HiLifeBatchReturnJob Error " + (times - 1).ToString() + "次仍失敗：" + json.Serialize(apiResult));
                    }
                }
                
            }
            catch (Exception ex)
            {
                logger.Error("HiLifeBatchReturnJob Error", ex);
            }
        }

        /// <summary>
        /// 從FTP下載核銷資訊
        /// </summary>
        public void BatchVerify()
        {
            try
            {
                var json = new JsonSerializer();
                string strResult = string.Empty;
                ApiResult apiResult = null;
                int times = 0;
                do
                {
                    //呼叫失敗重複呼叫3次
                    strResult = HiLifeFacade.WebGetRequest("api/HiLife/GetBatchVerifyData", "");
                    strResult = strResult.Replace("State", "Code");
                    apiResult = json.Deserialize<ApiResult>(strResult);
                    times++;
                }
                while (apiResult.Code != ApiResultCode.Success && times < 4);

                List<string> errorResult = new List<string>();
                if (apiResult.Code == ApiResultCode.Success)
                {
                    Guid bathGuid = Guid.NewGuid();
                    List<string> verifyData = json.Deserialize<List<string>>(apiResult.Data.ToString());

                    foreach (var data in verifyData)
                    {
                        string batchFileName = data.Split('|')[0];
                        string fileData = data.Split('|')[1];

                        if (fileData.Length > 60)
                        {
                            try
                            {

                                HiLifeVerifyLog entity = new HiLifeVerifyLog();
                                entity.ExchangeTranTime = fileData.Substring(0, 15);//15
                                entity.StoreCode = fileData.Substring(16, 4);//4
                                entity.MachineCode = fileData.Substring(21, 1);//1
                                entity.ExchangeDate = fileData.Substring(23, 8);//8
                                entity.InvoiceNumber = fileData.Substring(32, 10);//10

                                string realPincode = fileData.Substring(43).Replace("_", "");//20
                                entity.Pincode = realPincode;
                                entity.VerifyType = (int)HiLifeVerifyType.BatchFile;

                                entity.HilifeInput = fileData;
                                entity.BatchId = bathGuid;
                                entity.IsSuccessFlag = "1";
                                entity.BatchFileName = batchFileName;
                                entity.IpAddress = Helper.GetClientIP();
                                //因為是補核銷，已核銷的就不理了
                                var pincodeData = hiLife.GetHiLifePincode(entity.Pincode);
                                if (pincodeData != null && pincodeData.IsVerified)
                                    continue;

                                ApiResult result = HiLifeFacade.PincodeCheckAndVerify(entity.Pincode, entity);
                                if (!result.Code.Equals(ApiResultCode.Success))
                                {
                                    errorResult.Add(json.Serialize(result) + fileData);
                                }

                            }
                            catch (Exception e)
                            {
                                HiLifeFacade.SaveParseErrorLog("HiLife Job parse field is wrong", bathGuid, fileData, null, batchFileName, HiLifeVerifyType.BatchFile);
                                logger.Error(e.ToString());
                                throw;
                            }
                        }
                        else
                        {
                            HiLifeFacade.SaveParseErrorLog("HiLife Job input is wrong length", bathGuid, fileData, null, batchFileName, HiLifeVerifyType.BatchFile);
                        }
                    }

                    if (errorResult.Any())
                    {
                        logger.Error(string.Join(",", errorResult));
                    }
                }
                else
                {
                    logger.Error("HiLifeBatchVerifyJob Error " + (times - 1).ToString() + "次仍失敗：" + json.Serialize(apiResult));
                }
            }
            catch (Exception ex)
            {
                logger.Error("HiLifeBatchVerifyJob Error", ex);
            }
        }
    }

    public class HiLifeReturnLogModel
    {
        public string Time { get; set; }
        public List<HiLifeReturnDetailLogModel> Detail { get; set; }
    }

    public class HiLifeReturnDetailLogModel
    {
        public string Pincode { get; set; }
        public string ReturnDate { get; set; }
    }
}

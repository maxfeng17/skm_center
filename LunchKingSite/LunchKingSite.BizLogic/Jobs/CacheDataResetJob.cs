﻿using System;
using System.Collections.Concurrent;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;
using LunchKingSite.BizLogic.Component;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Jobs
{
    public class CacheDataResetJob : IJob
    {
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(CacheDataResetJob));
        private static ICacheProvider cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
        private static ISerializer serializer = ProviderFactory.Instance().GetSerializer();

        static CacheDataResetJob()
        {
        }
        public void Execute(JobSetting js)
        {
            CacheDataReset();
        }

        public static void CacheDataReset()
        {
            DateTime now = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            DateTime baseDate = DateTime.ParseExact(DateTime.Today.ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
            ViewPponDealCollection todayBaseDeals = pp.ViewPponDealGetListByDayWithNoLock(baseDate.AddDays(-1), baseDate.AddDays(1));

            Dictionary<Guid, List<int>> tempDic = pp.CategoryDealsGetListInEffectiveTime();
            var _dealsMapCategories = new ConcurrentDictionary<Guid, List<int>>(tempDic);

            StringBuilder sbFixLog = new StringBuilder();
            foreach (var deal in todayBaseDeals)
            {
                if (string.IsNullOrEmpty(deal.CategoryList))
                {
                    if (_dealsMapCategories.ContainsKey(deal.BusinessHourGuid))
                    {
                        deal.CategoryList = ProviderFactory.Instance().GetSerializer().Serialize(
                            _dealsMapCategories[deal.BusinessHourGuid]);
                        //回頭更新資料庫
                        var dp = pp.DealPropertyGet(deal.BusinessHourGuid);
                        if (dp.IsLoaded)
                        {
                            dp.CategoryList = deal.CategoryList;
                            pp.DealPropertySet(dp);
                        }
                        if (sbFixLog.Length < 800)
                        {
                            sbFixLog.AppendLine(dp.BusinessHourGuid.ToString());
                        }
                    }
                }
            }
            if (sbFixLog.Length > 0)
            {
                LogManager.GetLogger(LineAppender._LINE).Info("修正CategoryList:\r\n" + sbFixLog);
            }


            sb.AppendLine("CacheDataResetJob");
            sb.AppendLine("step1: " + (DateTime.Now - now).TotalSeconds.ToString("0.##"));
            now = DateTime.Now;

            //修改coupon_event_content.description
            //if (config.EnableImagesCDN)
            //{
            //    SetCouponEventContentImagesToCDN(todayBaseDeal, baseDate.AddDays(-1), baseDate.AddDays(1));
            //}
            //if (config.SwitchableHttpsEnabled)
            //{
            //    SetContentUrlsToHttps(todayBaseDeal);
            //}

            sb.AppendLine("step2: " + (DateTime.Now - now).TotalSeconds.ToString("0.##"));
            now = DateTime.Now;
            foreach(var deal in todayBaseDeals)
            {
                cache.Set("vpd::" + deal.BusinessHourGuid, deal, new TimeSpan(24, 0, 0), config.CacheItemCompress);
            }

            sb.AppendLine("step3: " + (DateTime.Now - now).TotalSeconds.ToString("0.##"));


            if (config.EnableAutoFixLostDealTimeSlotWorker)
            {
                now = DateTime.Now;
                new LostDealTimeSlotWorker().Patch();
                sb.AppendLine("step4-FixLostDealTimeSlot: " + (DateTime.Now - now).TotalSeconds.ToString("0.##"));
            }

            now = DateTime.Now;
            new LostCityListWorker().Patch();
            sb.AppendLine("step5-FixCityList: " + (DateTime.Now - now).TotalSeconds.ToString("0.##"));

            logger.Info(sb.ToString());
        }



        /*
        private static void SetContentUrlsToHttps(ViewPponDealCollection vpdc)
        {
            foreach (var item in vpdc)
            {
                if (string.IsNullOrEmpty(item.Description))
                {
                    continue;
                }
                item.Description = item.Description.Replace("http://", "https://");
            }
        }

        private static void SetCouponEventContentImagesToCDN(ViewPponDealCollection vpdc, DateTime sDate, DateTime eDate)
        {
            Dictionary<Guid, List<ImgPool>> imgPools =
                pp.ImgPoolGetListByDayWithNoLock(sDate, eDate).GroupBy(p => p.Bid).ToDictionary(x => x.Key, x => x.ToList());

            if (imgPools.Count == 0)
            {
                return;
            }
            foreach (var item in vpdc)
            {
                if (string.IsNullOrEmpty(item.Description))
                {
                    continue;
                }
                if (imgPools.ContainsKey(item.BusinessHourGuid) == false)
                {
                    continue;
                }
                //替換成CDN的圖片網址
                foreach (var imgpool in imgPools[item.BusinessHourGuid])
                {
                    item.Description = item.Description.Replace(imgpool.Host + imgpool.Path,
                        string.Format("{0}{1}?{2}", config.ImagePoolUrl, imgpool.Path, item.EventModifyTime.ToJavaScriptseconds()));
                }
            }
        }
        */
    }

    /// <summary>
    /// 修補有deal_time_slot ，卻沒有citylist的deal_property
    /// </summary>
    public class LostCityListWorker
    {
       private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(CacheDataResetJob));
        private static ISerializer serializer = ProviderFactory.Instance().GetSerializer();

        public void Patch()
        {
            List<Guid> lostBIds = GetLostCityListBids();
            foreach (Guid bid in lostBIds)
            {
                string message;
                bool result = Fix(bid, out message);
                if (string.IsNullOrEmpty(message) == false)
                {
                    logger.Warn(message);
                    LogManager.GetLogger(LineAppender._LINE).Info(message);
                }
            }
        }

        private bool Fix(Guid bid, out string message)
        {            
            try
            {
                DealProperty dp = pp.DealPropertyGet(bid);
                if (dp.IsLoaded == false)
                {
                    throw new Exception(string.Format("DealProperty {0} not found", bid));
                }

                List<int> cityIds = ViewPponDealManager.DefaultManager
                    .DealTimeSlotGetByBidAndTime(bid, DateTime.Now).Select(t => t.CityId).ToList();

                string cityList0 = dp.CityList;
                string cityList2 = "[" + string.Join(",", cityIds) + "]";

                if (dp.CityList != cityList2)
                {
                    dp.CityList = cityList2;
                    pp.DealPropertySet(dp);
                }

                message = string.Format("修正遺失的CityList, {0} 完成, 將 {1} 替換成 {2}", bid, cityList0, cityList2);                
                
                return true;
            }
            catch (Exception ex)
            {
                message = string.Format("修正遺失的CityList, {0} 失敗。原因是 {1}", bid, ex.ToString());
                return false;
            }
        }

        private List<Guid> GetLostCityListBids()
        {
            string sql = @"
select bh.guid from business_hour bh with(nolock)
inner join deal_property dp with(nolock) on dp.business_hour_guid = bh.GUID
inner join deal_time_slot dts with(nolock) on dts.business_hour_GUID = bh.guid and getdate() between dts.effective_start and dts.effective_end
where bh.guid in 
(select distinct dts.business_hour_GUID from deal_time_slot dts where GETDATE() between dts.effective_start and dts.effective_end)
and dp.city_list = '[]'
group by bh.GUID
having count(distinct dts.city_id) > 0 
";
            QueryCommand qc = new QueryCommand(sql, DealTimeSlot.Schema.Provider.Name);
            List<Guid> result = new List<Guid>();

            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetGuid(0));
                }
            }

            return result;
        }
    }

    /// <summary>
    /// 事後修補遺失的 deal_time_slot
    /// 此問題己被修正，目前只是留再檢查一段時間
    /// </summary>
    public class LostDealTimeSlotWorker
    {
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(CacheDataResetJob));
        private static ISerializer serializer = ProviderFactory.Instance().GetSerializer();

        public void Patch()
        {
            List<Guid> lostBIds = GetLostBidAtDealTimeSlot();
            foreach(Guid bid in lostBIds)
            {
                string message;
                bool result = Fix(bid, out message);
                if (string.IsNullOrEmpty(message)==false)
                {
                    logger.Warn(message);
                    LogManager.GetLogger(LineAppender._LINE).Info(message);
                }
            }
        }

        private bool Fix(Guid bid, out string message)
        {
            StringBuilder sb = new StringBuilder();            
            try
            {
                DealTimeSlotCollection slots = pp.DealTimeSlotGetAllCol(bid);
                Dictionary<int, IGrouping<int, DealTimeSlot>> dict = slots.GroupBy(t => t.CityId).ToDictionary(t => t.Key, t => t);
                foreach (var pair in dict)
                {
                    List<DealTimeSlot> citySlots = new List<DealTimeSlot>(pair.Value).OrderBy(t => t.EffectiveStart).ToList();
                    DealTimeSlot prev;
                    DealTimeSlot curr;
                    for (int i = 1; i < citySlots.Count; i++)
                    {
                        prev = citySlots[i - 1];
                        curr = citySlots[i];
                        if (prev.EffectiveEnd != curr.EffectiveStart)
                        {
                            if (DateTime.Now > prev.EffectiveEnd && DateTime.Now < curr.EffectiveStart)
                            {
                                DealTimeSlot newSlot = new DealTimeSlot
                                {
                                    CityId = prev.CityId,
                                    BusinessHourGuid = prev.BusinessHourGuid,
                                    IsInTurn = prev.IsInTurn,
                                    IsHotDeal = prev.IsHotDeal,
                                    Sequence = prev.Sequence,
                                    IsLockSeq = prev.IsLockSeq,
                                    Status = prev.Status,
                                    EffectiveStart = prev.EffectiveEnd,
                                    EffectiveEnd = curr.EffectiveStart
                                };
                                pp.DealTimeSlotSet(newSlot);
                                if (sb.Length > 0)
                                {
                                    sb.Append(", ");
                                }
                                sb.Append(serializer.Serialize(new
                                {                                    
                                    CityId = prev.CityId,
                                    BusinessHourGuid = prev.BusinessHourGuid,
                                    IsInTurn = prev.IsInTurn,
                                    IsHotDeal = prev.IsHotDeal,
                                    Sequence = prev.Sequence,
                                    IsLockSeq = prev.IsLockSeq,
                                    Status = prev.Status,
                                    EffectiveStart = prev.EffectiveEnd,
                                    EffectiveEnd = curr.EffectiveStart
                                }));
                            }                            
                        }
                    }
                }
                if (sb.Length == 0)
                {
                    sb.Insert(0, "修正遺失的DealTimeSlot " + bid + "，疑有錯誤，但並沒有符合條件的修正方式");
                }
                else
                {
                    sb.Insert(0, "修正遺失的DealTimeSlot ");
                }
                
                message = sb.ToString();
                return true;
            }
            catch (Exception ex)
            {
                message = ex.ToString();
                return false;
            }
        }

        private List<Guid> GetLostBidAtDealTimeSlot()
        {
            string sql = @"
select bh.guid from business_hour bh with(nolock)
where getdate() between bh.business_hour_order_time_s and bh.business_hour_order_time_e
and bh.business_hour_status & 2048 = 0
and bh.GUID not in

(select distinct dts.business_hour_GUID from deal_time_slot dts with(nolock) where 
  getdate() between dts.effective_start and dts.effective_end 
) 

and bh.GUID in
(select distinct dts.business_hour_GUID from deal_time_slot dts with(nolock) 
where dts.city_id not in (496) -- 不檢查新光檔
) 

";
            QueryCommand qc = new QueryCommand(sql, DealTimeSlot.Schema.Provider.Name);
            List<Guid> result = new List<Guid>();

            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetGuid(0));
                }
            }

            return result;
        }
    }
}
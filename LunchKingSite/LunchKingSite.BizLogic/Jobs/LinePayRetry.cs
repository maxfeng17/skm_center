﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.BizLogic.Jobs
{
    public class LinePayRetry : IJob
    {
        private static readonly IOrderProvider Op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static readonly ISysConfProvider Cp = ProviderFactory.Instance().GetConfig();
        private static readonly IMemberProvider Mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static readonly IPponProvider Pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        public void Execute(JobSetting js)
        {
            #region BuyProcessRetry
            
            List<LinePayTransStatus> retryStatus = new List<LinePayTransStatus> { LinePayTransStatus.VoidFail
                , LinePayTransStatus.VoidConnectFail
            ,LinePayTransStatus.BuyFailRefundFail
            ,LinePayTransStatus.BuyFailRefundConnectFail
            ,LinePayTransStatus.OrderFailRefundFail };

            Dictionary<LinePayTransStatus, LinePayTransLogCollection> retryQueue =
                new Dictionary<LinePayTransStatus, LinePayTransLogCollection>();

            //取得需要 retry 的資料, (未成功且 retry 未達上限)
            foreach (var status in retryStatus)
            {
                var col = Op.LinePayTransLogGetRetryData(status, GetCheckColumn(status), Cp.LinePayApiRetryTimes);
                if (!col.Any()) continue;

                //將連線不成功原因統一歸至失敗
                var tempStatus = status;
                if (status == LinePayTransStatus.CaptureConnectFail) tempStatus = LinePayTransStatus.CaptureFail;
                if (status == LinePayTransStatus.VoidConnectFail) tempStatus = LinePayTransStatus.VoidFail;
                if (status == LinePayTransStatus.BuyFailRefundConnectFail) tempStatus = LinePayTransStatus.BuyFailRefundFail;
                if (status == LinePayTransStatus.OrderFailRefundFail) tempStatus = LinePayTransStatus.BuyFailRefundFail;

                if (retryQueue.ContainsKey(tempStatus))
                {
                    retryQueue[tempStatus].AddRange(col);
                }
                else
                {
                    retryQueue.Add(tempStatus, col);
                }
            }

            if (retryQueue.Any())
            {
                RetryTrans(retryQueue);
            }

            #endregion BuyProcessRetry

            #region RefundProcess

            var retryCol = Op.LinePayRefundLogGetRetryData();
            if (retryCol.Any())
            {
                foreach (var retryItem in retryCol)
                {
                    retryItem.RetryTime++;
                    var check = true;
                    var linePayTransLog = Op.LinePayTransLogGet(retryItem.LinePayTransLogId);
                    var returnForm = new ReturnFormEntity(retryItem.ReturnFormId);

                    if (!linePayTransLog.IsLoaded || returnForm.IsNew)
                    {
                        check = false;
                    }

                    //檢查是否有退款單與是否已退款完成
                    var returnFormRefund = returnForm.ReturnFormRefunds.Where(x => x.TrustId == retryItem.TrustId).ToList();
                    if (!returnFormRefund.Any() || returnFormRefund.First().IsRefunded == true)
                    {
                        check = false;
                    }

                    //計算已退款金額
                    var alreadyRefundTotal = 0;
                    var alreadyRefundCt = Mp.CashTrustLogGetList(returnForm.ReturnFormRefunds.Where(x => x.IsRefunded == true).Select(x => x.TrustId)).ToList();
                    if (alreadyRefundCt.Any())
                    {
                        var tempCt = alreadyRefundCt.Where(x => x.Status == (int)TrustStatus.Refunded).ToList();
                        if (tempCt.Any())
                        {
                            alreadyRefundTotal = tempCt.Sum(x => x.Amount);
                        }
                    }

                    //已退款金額需與目前累積退款金額相等
                    if (alreadyRefundTotal != linePayTransLog.AlreadyRefundTotal)
                    {
                        check = false;
                    }

                    var ctl = Mp.CashTrustLogGet(retryItem.TrustId);

                    string returnReason = string.Empty;
                    bool canRetry = false;
                    if (check && LinePayUtility.Refund(retryItem, linePayTransLog, ctl, out canRetry, out returnReason))
                    {
                        returnForm.RefundProcessedForRetry(retryItem.TrustId, "sys", true, false, string.Empty);
                        CommonFacade.AddAudit(ctl.OrderGuid, AuditType.OrderDetail, string.Format("Line Pay退款重試成功 Trust_id:{0}", retryItem.TrustId), "sys", true);
                        ReturnService.AuditCtlogRefundSuccess(returnForm.OrderGuid, ctl, "sys");
                        Order order = Op.OrderGet(returnForm.OrderGuid);
                        ViewPponDeal deal = Pp.ViewPponDealGet(order.ParentOrderId.Value);
                        OrderFacade.SendCustomerRefundCompleteNoticeMail(returnForm, deal);
                        CommonFacade.AddAudit(order.Guid, AuditType.Order, "寄出處理成功通知", "sys", true);
                    }
                    else
                    {
                        if (canRetry && retryItem.RetryTime < Cp.LinePayApiRetryTimes)
                        {
                            returnForm.RefundProcessedForRetry(retryItem.TrustId, "sys", false, true
                                , string.Format("退款重試中retry({0}).", retryItem.RetryTime));
                        }
                        else
                        {
                            var failReason = (canRetry && retryItem.RetryTime >= Cp.LinePayApiRetryTimes)
                                ? string.Format("重試退款已達上限{0}, 退款失敗.", retryItem.RetryTime)
                                : returnReason;

                            returnForm.RefundProcessedForRetry(retryItem.TrustId, "sys", false, false
                                , string.Format("退款失敗({0}), {1}", retryItem.RetryTime
                                    , failReason));
                        }
                    }

                    returnForm.SetFinishTime(DateTime.Now);
                    ReturnFormRepository.Save(returnForm);
                }
                
            }

            #endregion RefundProcess
        }

        private string GetCheckColumn(LinePayTransStatus status)
        {
            switch (status)
            {
                case LinePayTransStatus.VoidFail:
                case LinePayTransStatus.VoidConnectFail:
                    return LinePayTransLog.Columns.VoidRetryTimes;
                case LinePayTransStatus.BuyFailRefundFail:
                case LinePayTransStatus.BuyFailRefundConnectFail:
                case LinePayTransStatus.OrderFailRefundFail:
                    return LinePayTransLog.Columns.BuyFailRefundRetryTimes;
                case LinePayTransStatus.CaptureFail:
                case LinePayTransStatus.CaptureConnectFail:
                    return LinePayTransLog.Columns.CaptureRetryTimes;
                default:
                    return string.Empty;
            }
        }

        private void RetryTrans(Dictionary<LinePayTransStatus, LinePayTransLogCollection> retryQueue)
        {
            foreach (var item in retryQueue)
            {
                switch (item.Key)
                {
                    case LinePayTransStatus.CaptureFail: //請款 retry
                        foreach (LinePayTransLog linePayTrans in item.Value)
                        {
                            PaymentTransaction pt = Op.PaymentTransactionGet(linePayTrans.TransactionId,
                                PaymentType.LinePay, PayTransType.Authorization);
                            if (pt.IsLoaded)
                            {
                                if (LinePayUtility.Capture(linePayTrans, (int)pt.Amount))
                                {
                                    DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(pt.Status);
                                    PaymentFacade.NewTransaction(pt.TransId, pt.OrderGuid ?? Guid.Empty, PaymentType.LinePay, pt.Amount,
                                        pt.AuthCode, PayTransType.Charging, DateTime.Now, "sys", "LinePay 請款 retry",
                                        Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful),
                                        PayTransResponseType.OK, (OrderClassification)pt.OrderClassification.Value, null
                                        , PaymentAPIProvider.LinePay);
                                }
                            }
                            else
                            {
                                linePayTrans.TransMsg
                                    = string.Format("Retry capture發生錯誤(找不到 PaymentTransaction TranId:{0}, PaymentType{1}, TransType:{2})"
                                    , linePayTrans.TransactionId, Core.PaymentType.LinePay, PayTransType.Charging);
                            }
                            linePayTrans.CaptureRetryTimes++;
                            linePayTrans.ModifyTime = DateTime.Now;
                            Op.LinePayTransLogSet(linePayTrans);
                        }
                        break;
                    case LinePayTransStatus.VoidFail: //取消授權 retry
                        foreach (LinePayTransLog linePayTrans in item.Value)
                        {
                            PaymentTransaction pt = Op.PaymentTransactionGet(linePayTrans.TransactionId,
                                PaymentType.LinePay, PayTransType.Authorization);
                            if (pt.IsLoaded)
                            {
                                LinePayUtility.VoidTrans(linePayTrans);
                            }
                            else
                            {
                                linePayTrans.TransMsg
                                    = string.Format("Retry void發生錯誤(找不到 PaymentTransaction TranId:{0}, PaymentType{1}, TransType:{2})"
                                    , linePayTrans.TransactionId, Core.PaymentType.LinePay, PayTransType.Authorization);
                            }

                            linePayTrans.VoidRetryTimes++;
                            linePayTrans.ModifyTime = DateTime.Now;
                            Op.LinePayTransLogSet(linePayTrans);
                        }
                        break;
                    case LinePayTransStatus.BuyFailRefundFail: //退款 retry
                        foreach (LinePayTransLog linePayTrans in item.Value)
                        {
                            linePayTrans.BuyFailRefundRetryTimes++;
                            linePayTrans.ModifyTime = DateTime.Now;

                            PaymentTransaction pt = Op.PaymentTransactionGet(linePayTrans.TransactionId,
                                PaymentType.LinePay, PayTransType.Authorization);

                            if (pt != null && pt.IsLoaded)
                            {
                                if (LinePayUtility.BuyFailRefund(linePayTrans, (int)pt.Amount))
                                {
                                    pt.Result = (int)PayTransResponseType.OK;
                                }       
                                pt.Message = string.Format("LinePay buy頁購買失敗退款 retry({0}).", linePayTrans.BuyFailRefundRetryTimes);                             
                                Op.PaymentTransactionSet(pt);
                            }
                            else
                            {
                                linePayTrans.TransMsg
                                    = string.Format("Retry refund發生錯誤(找不到 PaymentTransaction TranId:{0}, PaymentType{1}, TransType:{2})"
                                    , linePayTrans.TransactionId, Core.PaymentType.LinePay, PayTransType.Authorization);
                            }
                            Op.LinePayTransLogSet(linePayTrans);
                        }
                        break;
                }
            }
        }
    }
}

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System;
using System.Data;
using System.Net.Mail;
using System.IO;
using log4net;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.BizLogic.Jobs
{
    public class EinvoiceUpload : IJob
    {
        protected static ISysConfProvider config;
        protected static IPponProvider pp;
        private static IOrderProvider op;
        private static ILog logger = LogManager.GetLogger(typeof(EinvoiceUpload));


        static EinvoiceUpload()
        {
            config = ProviderFactory.Instance().GetConfig();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        public void Execute(JobSetting js)
        {
            DateTime now = System.DateTime.Today;
            string mode = js.Parameters["mode"];
            if (string.Equals(mode, "InvoiceMedia", StringComparison.OrdinalIgnoreCase))
            {
                EinvoiceFacade.GenerateInvocieMedia();
            }
            else
            {
                DateTime start = DateTime.Today.AddDays(config.InvoiceDelayDays);
                DateTime end = DateTime.Today.AddDays(config.InvoiceDelayDays + 1);
                DateTime invoicedDate = DateTime.Today.AddDays(config.InvoiceDelayDays);
                string invoiceMsg = string.Empty;
                try
                {
                    EinvoiceFacade.GenerateVendorEinvoice();
                    invoiceMsg = EinvoiceFacade.GenerateEinvoiceNumber(start, end, invoicedDate);
                    EinvoiceFacade.GenerateUploadFiles(start, end, invoicedDate);
                    EinvoiceFacade.GenerateUploadVendorEinvoiceFiles(start, end, invoicedDate);
                    EinvoiceFacade.GenerateVoidUploadFiles(start, end); //產生註銷
                    EinvoiceFacade.GenerateVoidAllowanceEinovice(start, end); //產生作廢折讓(D0501)
                    if (config.EnbaledTaishinEcPayApi)
                    {
                        invoiceMsg += EinvoiceFacade.GeneratePartnerEinvoiceNumber(start, end, invoicedDate);
                        //upload file
                        EinvoiceFacade.GeneratePartnerEinvoiceUploadFile(start, end);
                    }
                    EinvoiceFacade.SendEinvoiceWinnerEmail(invoicedDate, end, true, EinvoiceMailType.EinvoiceInfoMail, true, InvoiceVersion.CarrierEra);
                }
                catch (Exception e)
                {
                    MailMessage msg = new MailMessage();
                    msg.To.Add(config.AdminEmail);
                    msg.Subject = Environment.MachineName + ": [系統]" + invoicedDate.ToString("yyyy-MM-dd") + "發票上傳發生異常";
                    msg.From = new MailAddress(config.SystemEmail);
                    msg.IsBodyHtml = true;
                    msg.Body = "本日發票上傳發生異常:" + e.Message;

                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }

                try
                {
                    MailMessage msg = new MailMessage();
                    msg.To.Add(config.AdminEmail);
                    msg.Subject = Environment.MachineName + ": [系統]產生" + invoicedDate.ToString("yyyy-MM-dd") + "發票";
                    msg.From = new MailAddress(config.SystemEmail);
                    msg.IsBodyHtml = true;
                    msg.Body = invoiceMsg;

                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
                catch (Exception e)
                {
                    throw (e);
                }

                #region 產生折讓作廢
                //產生折讓作廢流程:
                //每兩個月執行一次，媒體申報的月份1號執行(單月份)
                //批次跑，一次產生一個月，每次會產折讓跟作廢einvoice_detail，以及要上傳Turnkey的XML
                //每天執行電子折讓 兩個月執行紙本折讓及電子折讓作廢

                //couple monthly:紙本折讓-折讓作廢 + 電子折讓-作廢處理
                if (now.Month % 2 == 1 && now.AddDays(-config.InvoiceBufferDays).Day == 1)
                {
                    ProcessEinvoiceAllowance(AllowanceProcessFrequency.CoupleMonthly, now);
                    UserRefundFacade.ProcessReturnFormCreditNoteType(now.AddMonths(-2).GetFirstDayOfMonth(), now);
                }

                //daily:電子折讓-折讓處理
                ProcessEinvoiceAllowance(AllowanceProcessFrequency.Daily, now);

                #endregion 產生折讓作廢

                #region 通知二聯改三聯發票已產生
                DataTable dt2To3 = pp.GetEinvoiceMain2To3();
                if (dt2To3.Rows.Count > 0)
                {
                    try
                    {
                        MailMessage msg = new MailMessage();
                        msg.To.Add(config.FinanceEmail);
                        msg.CC.Add(config.ItPAD);
                        msg.Subject = Environment.MachineName + ": [系統]二聯改三聯發票已產生";
                        msg.From = new MailAddress(config.AdminEmail);
                        msg.IsBodyHtml = true;
                        msg.Body = "發票已產生。名單如下:<br/>";
                        for (int i = 0; i < dt2To3.Rows.Count; i++)
                        {
                            msg.Body += dt2To3.Rows[i][0] + "：" + dt2To3.Rows[i][1] + "<br/>";
                        }

                        PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    }
                    catch (Exception e)
                    {
                        throw (e);
                    }
                }
                #endregion

                #region 發票未上傳警告
                if (!Directory.Exists(string.Format(@"C:\eInvoiceText\C0401\BAK\{0}\", DateTime.Now.AddDays(-1).ToString("yyyyMMdd"))))
                {
                    MailMessage msg = new MailMessage();
                    msg.To.Add(config.AdminEmail);
                    msg.Subject = Environment.MachineName + ": [系統]發票未上傳警告";
                    msg.From = new MailAddress(config.SystemEmail);
                    msg.IsBodyHtml = true;
                    msg.Body = "昨日發票尚未上傳，請檢查";

                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
                #endregion 發票未上傳警告

            }

            #region 更新電子發票中獎清單，2016/12/14由於國稅局是北七，釋出API卻不合他們法規上的建置，只好撤掉不用
            ////ex:now is 6/1,-1month=5,d_end=5/1,d_start=d_end-2month=3/1
            ////ex:now is 4/1,-1month=3,d_end=3/1,d_start=d_end-2month=1/1           
            //if (now.Month % 2 == 0 && now.Day == 1)
            //{
            //    try
            //    {

            //        int winResult = 0;
            //        string strResult = "";
            //        var sDate = new System.Globalization.TaiwanCalendar();
            //        DateTime d_end = new DateTime(int.Parse(now.Year.ToString()), int.Parse(now.AddMonths(-1).Month.ToString().Trim()), 1);
            //        DateTime d_start = d_end.AddMonths(-2);
            //        string sysDate = sDate.GetYear(now).ToString().Trim() + d_start.AddMonths(1).Month.ToString().PadLeft(2, '0');
            //        string targetUrl = string.Format(@"http://www.einvoice.nat.gov.tw/PB2CAPIVAN/invapp/InvApp?version=0.2&action=QryWinningList&invTerm={0}&appID=EINV1201410300635", sysDate);

            //        System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(targetUrl);
            //        request.Method = "GET";
            //        request.ContentType = "application/x-www-form-urlencoded";
            //        request.Timeout = 10000;

            //        string result = "";
            //        // 取得回應資料
            //        using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
            //        {
            //            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
            //            {
            //                result = sr.ReadToEnd();
            //            }
            //        }
            //        JObject jb = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(result);
            //        string v = jb.Property("v").Value.ToString();
            //        string code = jb.Property("code").Value.ToString();
            //        string msg = jb.Property("code").Value.ToString();
            //        string invoYm = jb.Property("invoYm").Value.ToString();
            //        string superPrizeNo = jb.Property("superPrizeNo").Value.ToString();//1000萬
            //        string spcPrizeNo = jb.Property("spcPrizeNo").Value.ToString();//200萬
            //        string spcPrizeNo2 = jb.Property("spcPrizeNo2").Value.ToString();//200萬2
            //        string spcPrizeNo3 = jb.Property("spcPrizeNo3").Value.ToString();//200萬3
            //        string firstPrizeNo1 = jb.Property("firstPrizeNo1").Value.ToString();//頭獎
            //        string firstPrizeNo2 = jb.Property("firstPrizeNo2").Value.ToString();//頭獎
            //        string firstPrizeNo3 = jb.Property("firstPrizeNo3").Value.ToString();//頭獎
            //        string firstPrizeNo4 = jb.Property("firstPrizeNo4").Value.ToString();//頭獎
            //        string firstPrizeNo5 = jb.Property("firstPrizeNo5").Value.ToString();//頭獎
            //        string firstPrizeNo6 = jb.Property("firstPrizeNo6").Value.ToString();//頭獎
            //        string firstPrizeNo7 = jb.Property("firstPrizeNo7").Value.ToString();//頭獎
            //        string firstPrizeNo8 = jb.Property("firstPrizeNo8").Value.ToString();//頭獎
            //        string firstPrizeNo9 = jb.Property("firstPrizeNo9").Value.ToString();//頭獎
            //        string firstPrizeNo10 = jb.Property("firstPrizeNo10").Value.ToString();//頭獎
            //        string sixthPrizeNo1 = jb.Property("sixthPrizeNo1").Value.ToString();//六獎200
            //        string sixthPrizeNo2 = jb.Property("sixthPrizeNo2").Value.ToString();//六獎200
            //        string sixthPrizeNo3 = jb.Property("sixthPrizeNo3").Value.ToString();//六獎200
            //        string sixthPrizeNo4 = jb.Property("sixthPrizeNo4").Value.ToString();//六獎200
            //        string sixthPrizeNo5 = jb.Property("sixthPrizeNo5").Value.ToString();//六獎200
            //        string sixthPrizeNo6 = jb.Property("sixthPrizeNo6").Value.ToString();//六獎200
            //        if (msg != "無此期別資料")
            //        {
            //            if (invoYm == sysDate)
            //            {
            //                if (superPrizeNo != "" && superPrizeNo.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, superPrizeNo); strResult += string.Format("1000萬獎筆數:{0};</br>", winResult); }
            //                if (spcPrizeNo != "" && spcPrizeNo.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, spcPrizeNo); strResult += string.Format("200萬獎筆數:{0};</br>", winResult); }
            //                if (spcPrizeNo2 != "" && spcPrizeNo2.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, spcPrizeNo2); strResult += string.Format("200萬2獎筆數:{0};</br>", winResult); }
            //                if (spcPrizeNo3 != "" && spcPrizeNo3.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, spcPrizeNo3); strResult += string.Format("200萬3獎筆數:{0};</br>", winResult); }
            //                if (firstPrizeNo1 != "" && firstPrizeNo1.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, firstPrizeNo1.Substring(firstPrizeNo1.Length - 3, 3)); strResult += string.Format("頭獎號碼1中獎後三碼筆數:{0};</br>", winResult); }
            //                if (firstPrizeNo2 != "" && firstPrizeNo2.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, firstPrizeNo2.Substring(firstPrizeNo2.Length - 3, 3)); strResult += string.Format("頭獎號碼2中獎後三碼筆數:{0};</br>", winResult); }
            //                if (firstPrizeNo3 != "" && firstPrizeNo3.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, firstPrizeNo3.Substring(firstPrizeNo3.Length - 3, 3)); strResult += string.Format("頭獎號碼3中獎後三碼筆數:{0};</br>", winResult); }
            //                if (firstPrizeNo4 != "" && firstPrizeNo4.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, firstPrizeNo4.Substring(firstPrizeNo4.Length - 3, 3)); strResult += string.Format("頭獎號碼4中獎後三碼筆數:{0};</br>", winResult); }
            //                if (firstPrizeNo5 != "" && firstPrizeNo5.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, firstPrizeNo5.Substring(firstPrizeNo5.Length - 3, 3)); strResult += string.Format("頭獎號碼5中獎後三碼筆數:{0};</br>", winResult); }
            //                if (firstPrizeNo6 != "" && firstPrizeNo6.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, firstPrizeNo6.Substring(firstPrizeNo6.Length - 3, 3)); strResult += string.Format("頭獎號碼6中獎後三碼筆數:{0};</br>", winResult); }
            //                if (firstPrizeNo7 != "" && firstPrizeNo7.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, firstPrizeNo7.Substring(firstPrizeNo7.Length - 3, 3)); strResult += string.Format("頭獎號碼7中獎後三碼筆數:{0};</br>", winResult); }
            //                if (firstPrizeNo8 != "" && firstPrizeNo8.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, firstPrizeNo8.Substring(firstPrizeNo8.Length - 3, 3)); strResult += string.Format("頭獎號碼8中獎後三碼筆數:{0};</br>", winResult); }
            //                if (firstPrizeNo9 != "" && firstPrizeNo9.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, firstPrizeNo9.Substring(firstPrizeNo9.Length - 3, 3)); strResult += string.Format("頭獎號碼9中獎後三碼筆數:{0};</br>", winResult); }
            //                if (firstPrizeNo10 != "" && firstPrizeNo10.Length == 8) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, firstPrizeNo10.Substring(firstPrizeNo10.Length - 3, 3)); strResult += string.Format("頭獎10中獎後三碼筆數:{0};</br>", winResult); }
            //                if (sixthPrizeNo1 != "" && sixthPrizeNo1.Length == 3) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, sixthPrizeNo1); strResult += string.Format("六獎號碼1中獎筆數:{0};</br>", winResult); }
            //                if (sixthPrizeNo2 != "" && sixthPrizeNo2.Length == 3) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, sixthPrizeNo2); strResult += string.Format("六獎號碼2中獎筆數:{0};</br>", winResult); }
            //                if (sixthPrizeNo3 != "" && sixthPrizeNo3.Length == 3) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, sixthPrizeNo3); strResult += string.Format("六獎號碼3中獎筆數:{0};</br>", winResult); }
            //                if (sixthPrizeNo4 != "" && sixthPrizeNo4.Length == 3) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, sixthPrizeNo4); strResult += string.Format("六獎號碼4中獎筆數:{0};</br>", winResult); }
            //                if (sixthPrizeNo5 != "" && sixthPrizeNo5.Length == 3) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, sixthPrizeNo5); strResult += string.Format("六獎號碼5中獎筆數:{0};</br>", winResult); }
            //                if (sixthPrizeNo6 != "" && sixthPrizeNo6.Length == 3) { winResult = op.EinvoiceMainCollectionSetWinningStatusCount(d_start, d_end, sixthPrizeNo6); strResult += string.Format("六獎號碼6中獎筆數:{0};</br>", winResult); }

            //                //回壓完之後，直接寄信通知
            //                string oldVersion = EinvoiceFacade.SendEinvoiceWinnerEmail(d_start, d_end, true, EinvoiceMailType.EvincoeWinnerMail, true, InvoiceVersion.Old);
            //                string carrierVersion = EinvoiceFacade.SendEinvoiceWinnerEmail(d_start, d_end, true, EinvoiceMailType.EvincoeWinnerMail, true, InvoiceVersion.CarrierEra);
            //                //回傳一份給系統告知成功失敗
            //                MailMessage msgSuccess = new MailMessage();
            //                msgSuccess.To.Add(config.AdminEmail);
            //                msgSuccess.Subject = "EinvoiceWinnersUpdateJob Success";
            //                msgSuccess.From = new MailAddress(config.AdminEmail);
            //                msgSuccess.IsBodyHtml = true;
            //                msgSuccess.Body = string.Format("中獎發票通知信: 舊制電子發票- {0}；新制發票載具- {1} ;</br>中獎筆數清單-{2}", oldVersion, carrierVersion, strResult);
            //                PostMan.Instance().Send(msgSuccess, SendPriorityType.Immediate);
            //            }
            //        }


            //    }
            //    catch (Exception ex)
            //    {
            //        MailMessage msg = new MailMessage();
            //        msg.To.Add(config.AdminEmail);
            //        msg.Subject = "EinvoiceWinnersUpdateJob Error";
            //        msg.From = new MailAddress(config.AdminEmail);
            //        msg.IsBodyHtml = true;
            //        msg.Body = ex.Message + ex.StackTrace;
            //        PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            //        logger.Error("EinvoiceWinnersUpdateJob Error:" + ex.Message + "<br/>" + ex.StackTrace);
            //    }
            //}
            #endregion 更新電子發票中獎清單，2016/12/14由於國稅局是北七，釋出API卻不合他們法規上的建置，只好撤掉不用

            #region 寄送電子發票中獎

            EinvoiceFacade.SendEinvoiceWinnerEmail();

            #endregion 寄送電子發票中獎
        }

        private bool ProcessEinvoiceAllowance(AllowanceProcessFrequency frequency, DateTime now)
        {
            var processRange = string.Empty;
            try
            {
                DateTime dend = new DateTime();
                DateTime dstart = new DateTime();
                string rtnMsg = "";
                if (frequency == AllowanceProcessFrequency.CoupleMonthly)
                {
                    for (int i = -2; i < 0; i++)
                    {
                        dstart = now.AddMonths(i);
                        dend = now.AddMonths(i + 1);
                        rtnMsg += GenerateAllowanceAndCancelInfo(dstart, dend, now, false);
                    }
                    processRange = string.Format("{0} ~ {1}", now.AddMonths(-2).ToShortDateString(), now.ToShortDateString());
                }
                else
                {
                    dstart = now.AddDays(config.InvoiceDelayDays);
                    dend = now.AddDays(config.InvoiceDelayDays + 1);
                    rtnMsg = GenerateAllowanceAndCancelInfo(dstart, dend, now, true);
                    processRange = string.Format("{0} ~ {1}", dstart.ToShortDateString(), dend.ToShortDateString());
                    EinvoiceFacade.GenerateVendorCancelUploadFiles(dstart, dend, now);
                }

                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = string.Format("{0}: [系統]產生{1}折讓作廢發票", Environment.MachineName, processRange);
                msg.From = new MailAddress(config.SystemEmail);
                msg.IsBodyHtml = true;
                msg.Body = rtnMsg;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception e)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = string.Format("{0}: [系統]{1}折讓作廢發票產生發生異常", Environment.MachineName, processRange);
                msg.From = new MailAddress(config.SystemEmail);
                msg.IsBodyHtml = true;
                msg.Body = "本日折讓作廢發票產生發生異常:" + e.Message;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error("EinvoiceAllowanceAndCancelUploadJob Error:" + e.Message + "<br/>" + e.StackTrace);

                return false;
            }

            return true;
        }

        private string GenerateAllowanceAndCancelInfo(DateTime dstart, DateTime dend, DateTime now, bool elecAllowanceOnly)
        {
            var rtnMsg = dstart.ToShortDateString() + "~" + dend.ToShortDateString() + " : " + EinvoiceFacade.AddEinvoiceAllowance(dstart, dend, elecAllowanceOnly) + "<br/>";
            EinvoiceFacade.GenerateAllowanceUploadFiles(dstart, dend, now);
            EinvoiceFacade.GenerateCancelUploadFiles(dstart, dend, now);

            return rtnMsg;
        }

        private enum AllowanceProcessFrequency
        {
            CoupleMonthly,
            Daily
        }
    }
}

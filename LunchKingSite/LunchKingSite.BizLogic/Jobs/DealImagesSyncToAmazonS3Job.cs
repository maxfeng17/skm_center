﻿using System;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component;
using System.Collections.Generic;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary>
    /*
         1.取得目前的 一般檔，多檔次主檔
         2.過瀘出 status 進行操作
            status:刪除中
                s3_mark_delete = true
                s3_is_ready = true
            status:同步中
                s3_mark_sync = true
                s3_is_ready = false
         3.對於刪除中的檔次 執行雲端刪除
            關於 media
            閼於 imageU
         4.對於同步中的檔次 上傳圖檔到雲端
            關於 media
            閼於 imageU                                  
                 
         頁面顯示deal media圖檔(檔次大圖)時，判斷s3_ready 即顯示置於雲端的圖片                 
        */
    /// </summary>
    public class DealImagesSyncToAmazonS3Job : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(DealImagesSyncToAmazonS3Job));
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        private static object thisLock = new object();
        private static bool _isRunning;
        protected static bool IsRunning
        {
            get
            {
                lock (thisLock)
                {
                    return _isRunning;
                }
            }
            set
            {
                lock (thisLock)
                {
                    _isRunning = value;
                }
            }
        }


        public DealImagesSyncToAmazonS3Job()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            if (IsRunning)
            {
                return;
            }
            IsRunning = true;

            try
            {
                DoSync();
            }
            catch (Exception ex)
            {
                logger.Error("DealImagesSyncToAmazonS3Job Error:", ex);
            }

            try
            {
                DoDelete();
            }
            catch (Exception ex)
            {
                logger.Error("DealImagesSyncToAmazonS3Job Error:", ex);
            }

            IsRunning = false;
        }

        private void DoDelete()
        {
            StringBuilder sbLog = new StringBuilder();
            List<DealCloudStorage> dealCloudStorags = pp.DealCloudStorageGetListByMarkDelete();
            foreach (var storage in dealCloudStorags)
            {
                try
                {
                    CloudFacade.DeleteDealImagesAtS3(storage.BusinessHourGuid, storage, sbLog);
                }
                catch (Exception)
                {
                    sbLog.AppendLine(storage.BusinessHourGuid.ToString());
                }
            }

            if (sbLog.Length > 0)
            {
                logger.Warn("刪除自 CDN 失敗. \r\n" + sbLog);
            }
        }

        private void DoSync()
        {
            StringBuilder sbLog = new StringBuilder();
            List<DealCloudStorage> dealCloudStorags = pp.DealCloudStorageGetListByMarkSync();
            foreach (var storage in dealCloudStorags)
            {
                try
                {
                    if (storage.S3IsReady)
                    {
                        CloudFacade.DeleteDealImagesAtS3(storage.BusinessHourGuid, storage, sbLog);
                    }
                    CloudFacade.UploadDealImagesToS3(storage.BusinessHourGuid, storage);
                }
                catch (Exception)
                {
                    sbLog.AppendLine(storage.BusinessHourGuid.ToString());
                }
            }

            if (sbLog.Length > 0)
            {
                logger.Warn("同步至 CDN 失敗. \r\n" + sbLog);
            }
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mail;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.BizLogic.Jobs
{
    public class NotifyChannelRefund : IJob
    {
        private static IOrderProvider op;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(NotifyChannelRefund));
        private StringBuilder sbLog = new StringBuilder();

        static NotifyChannelRefund()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                
                Dictionary<int, string> dic = op.GetToNotifyChannelReturnForm();
                if (dic.Any())
                {
                    var para = new SCMWebServiceLogin
                    {
                        clientId = config.PezAPIClientId,
                        clientSecret = config.PezAPIClientSecret
                    };

                    var merchant = new SCMWebServiceMerchant
                    {
                        usrNum = config.PezAPIUsrNum,
                        venNum = config.PezAPIVenNum
                    };


                    string token = PayEasyAPI.GetPezToken(para);

                    if (!string.IsNullOrEmpty(token))
                    {
                        foreach (KeyValuePair<int, string> item in dic)
                        {
                            ReturnForm rf = op.ReturnFormGet(item.Key);
                            OrderCorresponding oc = op.OrderCorrespondingListGetByOrderGuid(rf.OrderGuid);

                            //pez為「到貨」才能呼叫退貨
                            var supplierData = new SCMWebServiceSupplierData
                            {
                                shipStartDate = oc.CreatedTime.ToString("yyyy-MM-dd"),
                                shipEndDate = oc.CreatedTime.AddDays(1).ToString("yyyy-MM-dd"),
                                dataIndex = 1,
                                ordId = oc.RelatedOrderId,
                                orderKindType = "HOME",
                                orderType = "EC",
                                orderDealType = "ALL"
                            };


                            var supplier = new SCMWebServiceSupplier
                            {
                                merchant = merchant,
                                data = supplierData,
                            };


                            SCMWebServiceSupplierResult result = PayEasyAPI.GetSupplierByCondition(supplier, token);
                            if (result.success && result.dataResults.Any())
                            {
                                if (result.dataResults[0].orderStatusType == "SEND")
                                {
                                    var data = new ReturnGoodsData
                                    {
                                        ordId = item.Value
                                    };

                                    var returnGoods = new SCMWebServiceReturnGoods
                                    {
                                        merchant = merchant,
                                        data = data
                                    };

                                    bool isSuccess = PayEasyAPI.PezReturnGoods(returnGoods, token);
                                    if (isSuccess)
                                    {

                                        rf.NotifyChannel = (int)NotifyChannel.Refund;
                                        op.ReturnFormSet(rf);
                                    }
                                    else
                                    {
                                        logger.Error("NotifyChannelRefund Error returnFormId" + item.Key + "通知pez錯誤");
                                    }
                                }
                                else if (result.dataResults[0].orderStatusType == "CANCEL")
                                {
                                    rf.NotifyChannel = (int)NotifyChannel.Cancel;
                                    op.ReturnFormSet(rf);
                                }
                            }
                        }
                    }
                    else
                    {
                        logger.Error("NotifyChannelRefund Error 未取得pez token");
                    }
                    
                }
            }
            catch (Exception ex)
            {
                logger.Error("NotifyChannelRefund Error " + ex.Message + ex.StackTrace);

            }
        } 

    }
}
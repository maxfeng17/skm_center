﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System.Net.Mail;
using log4net;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs
{
    public class UpdateCreditcardFraudDataJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(UpdateCreditcardFraudDataJob));
        protected static IOrderProvider op;
        protected static ISysConfProvider config;
        public static string message = string.Empty;
        public static DateTime sDataTime = DateTime.Now;
        public static DateTime eDataTime = DateTime.Now;

        static UpdateCreditcardFraudDataJob()
        {
            op = ProviderFactory.Instance().GetDefaultProvider<IOrderProvider>();
            config = ProviderFactory.Instance().GetDefaultProvider<ISysConfProvider>();
        }

        public void Execute(JobSetting js)
        {
            message = string.Empty;

            sDataTime = DateTime.Now.AddDays(-3).Date;
            eDataTime = DateTime.Now.Date;



            UpdateCreditcardFraudDataByUserId();
            UpdateCreditcardFraudDataByErrorcard();
            UpdateCreditcardFraudDataByCreateIP();
            if (eDataTime.DayOfWeek == DayOfWeek.Friday)
            {
                UpdateCreditcarOTPWhitelist();
            }
            

            MailMessage msg = new MailMessage();
            msg.To.Add(config.AdminEmail);
            msg.Subject = string.Format("{0}： [系統]{1}", Environment.MachineName , "UpdateCreditcardFraudDataJob");
            msg.From = new MailAddress(config.AdminEmail);
            msg.IsBodyHtml = true;
            msg.Body = message;

            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }

        /// <summary>
        /// 盜刷userId的足跡
        /// </summary>
        private static void UpdateCreditcardFraudDataByUserId()
        {
            int? tempUserId = 0;
            try
            {
                DateTime sTime = DateTime.Now;

                //這邊會重複ex1114500400
                //可以在SQ做已存在排除
                Dictionary<int, int?> userIdList = op.CreditcardFraudDataGetUserId(sDataTime,eDataTime).Select(x => new { Id = x.Id, UserId = x.UserId }).Distinct().ToDictionary(x => x.Id, x => x.UserId);
                CreditcardFraudDatumCollection cfdc = new CreditcardFraudDatumCollection();
                

                foreach (KeyValuePair<int, int?> userId in userIdList)
                {
                    if (userId.Value != null)
                    {
                        tempUserId = userId.Value;
                        DataTable data = op.CreditcardFraudDataGetByUserId(Convert.ToInt32(userId.Value));
                        if (data.Rows.Count > 0)
                        {
                            //同帳號不同IP直接列入風險IP
                            if (data.Select("user_id=" + userId.Value).Count() > 0)
                            {
                                DataTable ipInsert = data.Select("user_id=" + userId.Value).CopyToDataTable().DefaultView.ToTable(true, new string[] { "source_ip", "user_id", "create_time" });
                                if (ipInsert.Rows.Count > 0)
                                {
                                    foreach (DataRow row in ipInsert.Rows)
                                    {
                                        CreditcardFraudDatum cfd = new CreditcardFraudDatum();
                                        cfd.UserId = Convert.ToInt32(row["user_id"].ToString());
                                        cfd.Ip = row["source_ip"].ToString();
                                        cfd.CreateId = config.SystemEmail;
                                        cfd.CreateTime = DateTime.Now;
                                        cfd.Type = (int)CreditcardFraudDataType.IP;
                                        cfd.RelateId = userId.Key;//CreditcardFraudData.Id
                                        cfd.RelateTime = Convert.ToDateTime(row["create_time"].ToString());

                                        //確認有無已存在資料
                                        CreditcardFraudDatum oldData = op.CreditcardFraudDataGet(false, cfd.Ip, "", "", null, ((int)CreditcardFraudDataType.IP).ToString());
                                        //確認有無於匯入資料
                                        int importData = cfdc.Where(x => x.Ip == cfd.Ip && x.Type == (int)CreditcardFraudDataType.IP).Count();


                                        if (!oldData.IsLoaded && importData == 0)
                                        {
                                            cfdc.Add(cfd);
                                        }
                                    }
                                }
                            }

                            //不同帳號需先確認
                            if (data.Select("user_id<>" + userId.Value + " and black=0").Count() > 0)
                            {
                                DataTable userIdCheck = data.Select("user_id<>" + userId.Value + " and black=0").CopyToDataTable().DefaultView.ToTable(true, new string[] { "source_ip", "user_id", "create_time" });
                                if (userIdCheck.Rows.Count > 0)
                                {
                                    foreach (DataRow row in userIdCheck.Rows)
                                    {
                                        CreditcardFraudDatum cfd = new CreditcardFraudDatum();
                                        cfd.UserId = Convert.ToInt32(row["user_id"].ToString());
                                        cfd.Ip = row["source_ip"].ToString();
                                        cfd.CreateId = config.SystemEmail;
                                        cfd.CreateTime = DateTime.Now;
                                        cfd.Type = (int)CreditcardFraudDataType.CheckAccount;
                                        cfd.RelateId = userId.Key;//CreditcardFraudData.Id
                                        cfd.RelateTime = Convert.ToDateTime(row["create_time"].ToString());

                                        //確認有無已存在資料
                                        CreditcardFraudDatum oldData = op.CreditcardFraudDataGet(false, "", "", "", cfd.UserId, ((int)CreditcardFraudDataType.Account).ToString() + "," + ((int)CreditcardFraudDataType.CheckAccount).ToString() + "," + ((int)CreditcardFraudDataType.SafeAccount).ToString());
                                        //確認有無於匯入資料
                                        int importData = cfdc.Where(x => x.UserId == cfd.UserId && x.Type == (int)CreditcardFraudDataType.CheckAccount).Count();


                                        if (!oldData.IsLoaded && importData == 0)
                                        {
                                            cfdc.Add(cfd);
                                        }
                                    }
                                }
                            }
                            
                            

                        }
                    }
                    
                    
                }

                if (cfdc.Count > 0)
                {
                    op.CreditcardFraudDataBulkInsert(cfdc);
                }
                DateTime eTime = DateTime.Now;
                message += "UpdateCreditcardFraudDataByUserId：" + cfdc.Count.ToString() + "筆 <br> 執行時間：" + sTime.ToString() + " <br> 結束時間：" + eTime.ToString() + "<br>";
            }
            catch (Exception ex)
            {
                logger.Error(ex + "更新疑似盜刷資料錯誤" + tempUserId.Value.ToString());
            }
        }

        /// <summary>
        /// 使用過掛失卡、遺失卡、被竊卡、卡片異常、否認交易、特殊客戶
        /// </summary>
        private void UpdateCreditcardFraudDataByErrorcard()
        {
            int? tempUserId = 0;
            try
            {
                DateTime sTime = DateTime.Now;
                DataTable userIdList = op.CreditcardFraudDataGetByErrorCard(sDataTime, eDataTime);
                CreditcardFraudDatumCollection cfdc = new CreditcardFraudDatumCollection();


                foreach (DataRow row in userIdList.Rows)
                {
                    tempUserId = Convert.ToInt32(row["member_id"].ToString());

                    CreditcardFraudDatum cfd = new CreditcardFraudDatum();
                    cfd.Ip = row["consumer_ip"].ToString();
                    cfd.UserId = Convert.ToInt32(row["member_id"].ToString());
                    cfd.CreateId = config.SystemEmail;
                    cfd.CreateTime = DateTime.Now;
                    cfd.Type = (int)CreditcardFraudDataType.Account;
                    cfd.RelateId = Convert.ToInt32(row["id"].ToString());//CreditcardOrder.Id
                    cfd.RelateTime = Convert.ToDateTime(row["create_time"].ToString());

                    //確認有無已存在資料-直接從SQL排掉
                    //確認有無於匯入資料-直接從SQL排掉
                    cfdc.Add(cfd);
                }
                if (cfdc.Count > 0)
                {
                    op.CreditcardFraudDataBulkInsert(cfdc);
                }
                DateTime eTime = DateTime.Now;
                message += "UpdateCreditcardFraudDataByErrorcard：" + cfdc.Count.ToString() + "筆 <br> 執行時間：" + sTime.ToString() + " <br> 結束時間：" + eTime.ToString() + "<br>";
            }
            catch (Exception ex)
            {
                logger.Error(ex + "更新疑似盜刷資料錯誤" + tempUserId.Value.ToString());
            }
            
        }


        /// <summary>
        /// 盜刷IP的足跡
        /// </summary>
        private void UpdateCreditcardFraudDataByCreateIP()
        {
            string tempip = "";
            try
            {
                DateTime sTime = DateTime.Now;
                //可以在SQ做已存在排除
                Dictionary<int, string> IpList = op.CreditcardFraudDataGetIp(sDataTime, eDataTime).Select(x => new { Id = x.Id, Ip = x.Ip }).Distinct().ToDictionary(x => x.Id, x => x.Ip);
                CreditcardFraudDatumCollection cfdc = new CreditcardFraudDatumCollection();


                foreach (KeyValuePair<int, string> ip in IpList)
                {
                    tempip = ip.Value;
                    if (!string.IsNullOrEmpty(ip.Value))
                    {
                        DataTable data = op.CreditcardFraudDataGetByCreateIp(ip.Value);
                        if (data.Rows.Count > 0)
                        {
                            foreach (DataRow row in data.Rows)
                            {
                                CreditcardFraudDatum cfd = new CreditcardFraudDatum();
                                cfd.Ip = ip.Value;
                                cfd.UserId = Convert.ToInt32(row["user_id"].ToString());
                                cfd.CreateId = config.SystemEmail;
                                cfd.CreateTime = DateTime.Now;
                                cfd.Type = (int)CreditcardFraudDataType.CheckAccount;
                                cfd.RelateId = ip.Key;//AccountAudit.Id
                                cfd.RelateTime = Convert.ToDateTime(row["create_time"].ToString());

                                //確認有無已存在資料
                                CreditcardFraudDatum oldData = op.CreditcardFraudDataGet(false, "", "", "", cfd.UserId, ((int)CreditcardFraudDataType.Account).ToString() + "," + ((int)CreditcardFraudDataType.CheckAccount).ToString() + "," + ((int)CreditcardFraudDataType.SafeAccount).ToString());
                                //確認有無於匯入資料
                                int importData = cfdc.Where(x => x.UserId == cfd.UserId && x.Type == (int)CreditcardFraudDataType.CheckAccount).Count();

                                if (!oldData.IsLoaded && importData == 0)
                                {
                                    cfdc.Add(cfd);
                                }
                            }
                        }
                    }
                }

                if (cfdc.Count > 0)
                {
                    op.CreditcardFraudDataBulkInsert(cfdc);
                }
                DateTime eTime = DateTime.Now;
                message += "UpdateCreditcardFraudDataByCreateIP：" + cfdc.Count.ToString() + "筆 <br> 執行時間：" + sTime.ToString() + " <br> 結束時間：" + eTime.ToString() + "<br>";
            }
            catch (Exception ex)
            {
                logger.Error(ex + "更新疑似盜刷資料錯誤" + tempip.ToString());
            }
        }


        /// <summary>
        /// 撈OTP白名單
        /// </summary>
        public static void UpdateCreditcarOTPWhitelist()
        {
            try
            {
                DateTime sTime = DateTime.Now;

                //truncare 白名單
                op.TruncateCreditcardOtpWhitelist();

                //撈出白名單
                CreditcardOtpWhitelistCollection list = op.CreditcardOTPWhiteList();
                op.CreditcardOtpWhitelistBulkInsert(list);

                //排除盜刷地址userid
                int delete = op.DeleteExcludeWhiteList();

                DateTime eTime = DateTime.Now;
                message += "UpdateCreditcarOTPWhitelist：" + list.Count.ToString() + "-" + delete.ToString() + "筆 <br> 執行時間：" + sTime.ToString() + " <br> 結束時間：" + eTime.ToString() + "<br>";

            }
            catch (Exception ex)
            {
                logger.Error(ex + "更新OTP白名單資料錯誤" + ex.Message);
            }
        }



    }
}

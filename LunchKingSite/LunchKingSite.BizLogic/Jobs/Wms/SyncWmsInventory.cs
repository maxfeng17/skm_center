﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.Wms;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using LunchKingSite.SsBLL;
using Newtonsoft.Json;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Jobs.Wms
{
    /// <summary>
    /// 同步PChome倉庫的庫存
    /// </summary>
    public class SyncWmsInventory : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(SyncWmsInventory));
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static IPponProvider _pp = ProviderFactory.Instance().GetDefaultProvider<IPponProvider>();

        internal class ProductChangingItem
        {
            public Guid ItemGuid { get; set; }
            public int WmsStock { get; set; }
            public int E7Stock { get; set; }
        }

        public void Execute(JobSetting js)
        {
            
            UpdateWmsInventory();

            UpdateProductItemStock();

            MinusAtmProductItemStock();//扣掉ATM未成單的庫存數
        }

        public static void UpdateProductItemStock()
        {
            string sql = @"
                        select 
	                        wpii.item_guid, wpii.good_onsale, pi.stock		
	                        from wms_product_item_inventory wpii
                        inner join product_item pi with(nolock) on wpii.item_guid = pi.guid
                        and pi.stock <> wpii.good_onsale
                        ";

            QueryCommand qc = new QueryCommand(sql, LunchKingSite.DataOrm.WmsProductItemInventory.Schema.Provider.Name);
            List<ProductChangingItem> changingItems = new List<ProductChangingItem>();

            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    ProductChangingItem item = new ProductChangingItem
                    {
                        ItemGuid = reader.GetGuid(0),
                        WmsStock = reader.GetInt32(1),
                        E7Stock = reader.GetInt32(2)
                    };
                    changingItems.Add(item);
                }
            }

            foreach (ProductChangingItem item in changingItems)
            {
                var vpi = _pp.ViewProductItemGet(item.ItemGuid);
                try
                {
                    if (vpi == null || vpi.IsLoaded == false)
                    {
                        throw new Exception("Product Item 找不到, UtemGuid=" + item.ItemGuid);
                    }
                    using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                    {
                        ProductItemStock stock = new ProductItemStock()
                        {
                            Guid = Guid.NewGuid(),
                            InfoGuid = vpi.InfoGuid.GetValueOrDefault(),
                            ItemGuid = vpi.ItemGuid,
                            BeginningStock = item.E7Stock,
                            AdjustStock = item.WmsStock - item.E7Stock,
                            EndingStock = item.WmsStock,
                            CreateId = "job",
                            CreateTime = DateTime.Now,
                            ModifyId = "job",
                            ModifyTime = DateTime.Now
                        };
                        _pp.ProductItemStockSet(stock);
                        _pp.ProductItemStockIncrease(item.ItemGuid, stock.AdjustStock, "job");

                        tx.Complete();
                    }

                    if (vpi.InfoGuid != null)
                    {
                        PponFacade.UpdateDealAllMaxItemCount((Guid)vpi.InfoGuid);
                    }
                } catch (Exception ex)
                {
                    logger.WarnFormat("UpdateProductItemStock, errorItem={0}, {1}", item.ItemGuid, ex);
                }
            }
        }

        private void UpdateWmsInventory()
        {
            try
            {
                string sql = @"
select
 pi.pchome_prod_id from product_item pi with(nolock)
where
pi.warehouse_type = 1
and pchome_prod_id is not null
";

                QueryCommand qc = new QueryCommand(sql, LunchKingSite.DataOrm.WmsPurchaseOrder.Schema.Provider.Name);
                List<string> pchomeProdIds = new List<string>();
                using (var reader = DataService.GetReader(qc))
                {
                    while (reader.Read())
                    {
                        string pchomeProdId = reader.GetString(0);
                        pchomeProdIds.Add(pchomeProdId);
                    }
                }

                int takeSize = config.SyncInventoryBatchSize;
                List<Models.PchomeWarehouse.WmsInventory> allInventories = new List<Models.PchomeWarehouse.WmsInventory>();
                for (int i = 0; i < pchomeProdIds.Count; i += takeSize)
                {
                    List<string> partIds = pchomeProdIds.Skip(i).Take(takeSize).ToList();
                    List<Models.PchomeWarehouse.WmsInventory> inventories = PchomeWmsAPI.GetInventories(partIds.ToArray());
                    allInventories.AddRange(inventories);

                    foreach (Models.PchomeWarehouse.WmsInventory model in inventories)
                    {
                        Guid itemGuid = Guid.Parse(model.VendorPId);
                        var inv = _wp.WmsProductItemInventoryGet(itemGuid);
                        if (inv.IsLoaded == false)
                        {
                            inv = new DataOrm.WmsProductItemInventory();
                        }
                        inv.ItemGuid = itemGuid;
                        inv.PchomeProdId = model.Id;
                        inv.Checking = model.Status.First(t => t.Message == "Checking").Qty;
                        inv.GoodOnsale = model.Status.First(t => t.Message == "Good_OnSale").Qty;
                        inv.GoodWaiting = model.Status.First(t => t.Message == "Good_Waiting").Qty;
                        inv.Bad = model.Status.First(t => t.Message == "Bad").Qty;
                        inv.Lost = model.Status.First(t => t.Message == "Lost").Qty;
                        inv.Refund = model.Status.First(t => t.Message == "Refund").Qty;
                        try
                        {
                            _wp.WmsProductItemInventorySet(inv);
                        }
                        catch (Exception ex)
                        {
                            logger.WarnFormat("WmsProductItemInventorySet fail, id={0}, data={1}, apiData={2}, ex={3}",
                                itemGuid,
                                JsonConvert.SerializeObject(inv),
                                JsonConvert.SerializeObject(model),
                                ex);
                            throw;
                        }
                    }
                }

                if (pchomeProdIds.Count != allInventories.Count)
                {
                    logger.WarnFormat("SyncWmsInventory 從Pchome更新存貨資料，{0}, {1}",
                        pchomeProdIds.Count, allInventories.Count);
                }

            }
            catch (Exception ex)
            {
                logger.WarnFormat("SyncWmsInventory 從Pchome更新存貨資料，發生錯誤，{0}", ex);
            }
        }

        private void MinusAtmProductItemStock()
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine("MinusAtmProductItemStock");
                //找出訂單：ATM/PCHome/未成單，將17life的庫存先扣除，避免超賣
                Dictionary<Guid, Guid> oids = _wp.WmsAmtOrderGet();

                builder.AppendLine(new Core.JsonSerializer().Serialize(oids));

                foreach (var oid in oids)
                {
                    builder.AppendLine("oid.Key" + oid.Key);

                    string sql = string.Format(@"select opo.option_id
                                from order_product op with(nolock)
                                left
                                join Order_Product_Option opo with(nolock) on op.id = opo.order_product_id
                                where op.order_guid = '{0}'",
                                oid.Key
                                );

                    if (PponBuyFacade.EnablePchomeDeductingInventory(oid.Value))
                    {
                        builder.AppendLine("EnablePchomeDeductingInventory");

                        builder.AppendLine(sql);

                        QueryCommand qc = new QueryCommand(sql, LunchKingSite.DataOrm.WmsPurchaseOrder.Schema.Provider.Name);
                        List<string> pchomeProdIds = new List<string>();
                        using (var reader = DataService.GetReader(qc))
                        {
                            while (reader.Read())
                            {
                                int optionId = reader.GetInt32(0);
                                builder.AppendLine("[optionId]" + optionId.ToString());

                                PponOption po = _pp.PponOptionGet(optionId);
                                if (po != null && po.IsLoaded)
                                {
                                    builder.AppendLine("[po]");

                                    if (po.ItemGuid != null)
                                    {
                                        builder.AppendLine("[ItemGuid]" + po.ItemGuid.Value);
                                        ProductItem item = _pp.ProductItemGet(po.ItemGuid.Value);

                                        if (item != null && item.IsLoaded)
                                        {
                                            builder.AppendLine("[ProductItemStock]");

                                            ProductItemStock stock = new ProductItemStock()
                                            {
                                                Guid = Guid.NewGuid(),
                                                InfoGuid = item.InfoGuid,
                                                ItemGuid = item.Guid,
                                                BeginningStock = item.Stock,
                                                AdjustStock = -1,
                                                EndingStock = item.Stock - 1,
                                                CreateId = "job",
                                                CreateTime = DateTime.Now,
                                                ModifyId = "job",
                                                ModifyTime = DateTime.Now
                                            };
                                            _pp.ProductItemStockSet(stock);

                                            builder.AppendLine("[ProductItemStockSet]");

                                            _pp.ProductItemStockIncrease(item.Guid, -1, "job");
                                            builder.AppendLine("[ProductItemStockIncrease]");
                                        }
                                    }
                                }
                            }
                        }
                    } 
                }

                ProposalFacade.ProposalPerformanceLogSet("MinusAtmProductItemStock", builder.ToString(), "job");
            }
            catch (Exception ex)
            {
                logger.WarnFormat("SyncWmsInventory 扣除ATM訂單，發生錯誤，{0}", ex);
            }
        }
    }
}

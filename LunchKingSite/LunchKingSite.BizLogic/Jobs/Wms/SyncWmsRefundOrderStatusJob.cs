﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using LunchKingSite.BizLogic.Models.Wms;
using System.Collections.Generic;
using Newtonsoft.Json;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Jobs.Wms
{
    /// <summary>
    /// 更新PCHOME退貨狀態
    /// </summary>
    public class SyncWmsRefundOrderStatusJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(SyncWmsRefundOrderStatusJob));
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISysConfProvider _config = ProviderFactory.Instance().GetProvider<ISysConfProvider>();


        private static object thisLock = new object();
        private static bool _isRunning;

        protected static bool IsRunning
        {
            get
            {
                lock (thisLock)
                {
                    return _isRunning;
                }
            }
            set
            {
                lock (thisLock)
                {
                    _isRunning = value;
                }
            }
        }

        public void Execute(JobSetting js)
        {
            if (IsRunning)
            {
                return;
            }
            IsRunning = true;


            DateTime now = DateTime.Now;
            var holiday = _pp.GetTodayVacation();

            SyncWmsRefundOrderStatus();

            if (now.DayOfWeek != DayOfWeek.Saturday && now.DayOfWeek != DayOfWeek.Sunday && !holiday.IsLoaded)
            {
                //排除工作日,假日不更新
                UpdateReturnStatus();
            }
            
            IsRunning = false;
        }

        /// <summary>
        /// 同步PCGOME退貨狀態
        /// </summary>
        private static void SyncWmsRefundOrderStatus()
        {
            WmsRefundOrderCollection orders = _wp.WmsRefundOrderGetByUpdate();
            foreach (WmsRefundOrder order in orders)
            {
                try
                {
                    //查詢退貨狀態
                    //API
                    WmsRefund wmsRefundOrderInfo = PchomeWmsAPI.GetRefund(order.PchomeRefundId);
                    //test
                    if (!string.IsNullOrEmpty(_config.WmsRefundAPI))
                    {
                        wmsRefundOrderInfo = JsonConvert.DeserializeObject<WmsRefund>(_config.WmsRefundAPI);
                    }

                    if (wmsRefundOrderInfo.Id != order.PchomeRefundId)
                        continue;
                    bool isUpdate = false;
                    bool isUpdateArrival = false;
                    int status = (int)Enum.Parse(typeof(WmsRefundStatus), wmsRefundOrderInfo.State, true);

                    if (order.Status != status)
                    {
                        //狀態不一樣再更新
                        order.Status = (int)Enum.Parse(typeof(WmsRefundStatus), wmsRefundOrderInfo.State, true);
                        isUpdate = true;
                    }

                    //查詢退貨明細
                    DateTime hasPickupDate = DateTime.MinValue;
                    DateTime arrivalDate = DateTime.MinValue;
                    DateTime tempHasPickupDate = DateTime.MinValue;
                    DateTime tempArrivalDate = DateTime.MinValue;
                    int pickupCount = 0;
                    int arrivalCount = 0;
                    //API
                    WmsRefundTrace wmsRefundOrderTrace = PchomeWmsAPI.GetRefundTrace(order.PchomeRefundId);
                    //test
                    if (!string.IsNullOrEmpty(_config.WmsRefundLogisticAPI))
                    {
                        wmsRefundOrderTrace = JsonConvert.DeserializeObject<WmsRefundTrace>(_config.WmsRefundLogisticAPI);
                    }

                    if (wmsRefundOrderTrace.RefundId != order.PchomeRefundId)
                        continue;

                    List<WmsRefundTrace.LogisticTraceInfo> infos = wmsRefundOrderTrace.Info;


                    foreach (WmsRefundTrace.LogisticTraceInfo info in infos)
                    {
                        List<WmsRefundTrace.LogisticTraceProgressStatus> progressStatuss = info.ProgressStatus;

                        if (progressStatuss.Exists(x => x.Message == WmsRefundProgressStatus.HasPickup.ToString()))
                        {
                            pickupCount++;
                            tempHasPickupDate = Convert.ToDateTime(progressStatuss.Find(x => x.Message == WmsRefundProgressStatus.HasPickup.ToString()).Date);
                            if (tempHasPickupDate > hasPickupDate)
                                hasPickupDate = tempHasPickupDate;
                        }


                        if (progressStatuss.Exists(x => x.Message == WmsRefundProgressStatus.Arrival.ToString()))
                        {
                            arrivalCount++;
                            tempArrivalDate = Convert.ToDateTime(progressStatuss.Find(x => x.Message == WmsRefundProgressStatus.Arrival.ToString()).Date);
                            if (tempArrivalDate > arrivalDate)
                                arrivalDate = tempArrivalDate;
                        }


                    }

                    //若有多info,皆取貨(配送)完畢才算完成

                    if (order.ShipInfo != Newtonsoft.Json.JsonConvert.SerializeObject(wmsRefundOrderTrace))
                    {
                        order.ShipInfo = Newtonsoft.Json.JsonConvert.SerializeObject(wmsRefundOrderTrace);
                        isUpdate = true;
                    }

                    if (infos.Count == pickupCount && order.HasPickupTime == null && hasPickupDate!= DateTime.MinValue)
                    {
                        order.HasPickupTime = hasPickupDate;
                        isUpdate = true;
                    }
                        
                    if (infos.Count == arrivalCount && order.ArrivalTime == null && arrivalDate != DateTime.MinValue)
                    {
                        order.ArrivalTime = arrivalDate;
                        isUpdate = true;
                        isUpdateArrival = true;
                       
                    }

                    if (isUpdate)
                    {
                        order.ModifyId = "sys";
                        order.ModifyTime = DateTime.Now;
                        _wp.WmsRefundOrderSet(order);


                        WmsRefundOrderStatusLog log = WmsFacade.MakeWmsRefundOrderStatusLog(order, "sys", DateTime.Now);
                        _wp.WmsRefundOrderStatusLogSet(log);
                    }

                    if (isUpdateArrival)
                    {
                        EmailFacade.SendWmsProductRefundToSeller(order.ReturnFormId);
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("SyncWmsRefundOrderStatusJob error. return_form_id={0}, ex={1}", order.ReturnFormId, ex);
                }

            }
        }

        /// <summary>
        /// 根據PCHOME退貨狀態來更新本站退貨單
        /// </summary>
        private static void UpdateReturnStatus()
        {
            try
            {
                
                //3天內已配達
                ReturnFormCollection rfcArrival = _wp.WmsRefundOrderGetByArrivaled(1);
                foreach (ReturnForm rf in rfcArrival)
                {
                    rf.ProgressStatus = (int)ProgressStatus.ConfirmedForVendor;
                    rf.VendorProgressStatus = (int)VendorProgressStatus.ConfirmedForVendor;
                    rf.ModifyId = "sys";
                    rf.ModifyTime = DateTime.Now;
                    _op.ReturnFormSet(rf);

                    ReturnFormStatusLog log = ReturnFormRepository.MakeStatusLog(rf, "sys", DateTime.Now);
                    _op.ReturnFormStatusLogSet(log);
                }

                //取貨三天後未配達,經客服處理後取得送達日
                ReturnFormCollection rfcAfterCheckArrival = _wp.WmsRefundOrderGetByArrivaled(2);
                foreach (ReturnForm rf in rfcAfterCheckArrival)
                {
                    rf.ProgressStatus = (int)ProgressStatus.ConfirmedForVendor;
                    rf.VendorProgressStatus = (int)VendorProgressStatus.ConfirmedForVendor;
                    rf.ModifyId = "sys";
                    rf.ModifyTime = DateTime.Now;
                    _op.ReturnFormSet(rf);

                    ReturnFormStatusLog log = ReturnFormRepository.MakeStatusLog(rf, "sys", DateTime.Now);
                    _op.ReturnFormStatusLogSet(log);
                }

                //3天後尚未配達
                ReturnFormCollection rfcNotArrival = _wp.WmsRefundOrderGetByArrivaled(3);
                foreach (ReturnForm rf in rfcNotArrival)
                {
                    rf.ProgressStatus = (int)ProgressStatus.ConfirmedForUnArrival;
                    rf.VendorProgressStatus = (int)VendorProgressStatus.ConfirmedForUnArrival;
                    rf.ModifyId = "sys";
                    rf.ModifyTime = DateTime.Now;
                    _op.ReturnFormSet(rf);

                    ReturnFormStatusLog log = ReturnFormRepository.MakeStatusLog(rf, "sys", DateTime.Now);
                    _op.ReturnFormStatusLogSet(log);
                }

                //配達3天後廠商未回壓
                ReturnFormCollection rfcVendorNotConfirm = _wp.WmsRefundOrderGetByArrivaled(4);
                foreach (ReturnForm rf in rfcVendorNotConfirm)
                {
                    rf.ProgressStatus = (int)ProgressStatus.Processing;
                    rf.VendorProgressStatus = (int)VendorProgressStatus.Automatic;
                    rf.ModifyId = "sys";
                    rf.ModifyTime = DateTime.Now;
                    _op.ReturnFormSet(rf);

                    ReturnFormStatusLog log = ReturnFormRepository.MakeStatusLog(rf, "sys", DateTime.Now);
                    _op.ReturnFormStatusLogSet(log);
                }

                //出貨前就退貨
                ReturnFormCollection rfcNotShip = _wp.WmsRefundOrderGetByArrivaled(5);
                foreach (ReturnForm rf in rfcNotShip)
                {
                    rf.ProgressStatus = (int)ProgressStatus.Processing;
                    rf.VendorProgressStatus = (int)VendorProgressStatus.Automatic;
                    rf.ModifyId = "sys";
                    rf.ModifyTime = DateTime.Now;
                    _op.ReturnFormSet(rf);

                    ReturnFormStatusLog log = ReturnFormRepository.MakeStatusLog(rf, "sys", DateTime.Now);
                    _op.ReturnFormStatusLogSet(log);
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SyncWmsRefundOrderStatusJob error. ex={0}", ex);
            }
        }
    }
}

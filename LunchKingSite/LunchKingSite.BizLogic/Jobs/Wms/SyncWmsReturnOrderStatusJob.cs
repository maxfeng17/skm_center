﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models.Wms;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs.Wms
{
    /// <summary>
    /// 呼叫 PChome Wms倉儲系統API，同步還貨交易的狀態
    /// </summary>
    public class SyncWmsReturnOrderStatusJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(SyncWmsReturnOrderStatusJob));
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static object thisLock = new object();
        private static bool _isRunning;

        protected static bool IsRunning
        {
            get
            {
                lock (thisLock)
                {
                    return _isRunning;
                }
            }
            set
            {
                lock (thisLock)
                {
                    _isRunning = value;
                }
            }
        }

        public void Execute(JobSetting js)
        {
            if (IsRunning)
            {
                return;
            }
            IsRunning = true;

            string[] returnIds;
            List<WmsReturn> getReturns = new List<WmsReturn>();
            WmsReturnOrderCollection wrol = new WmsReturnOrderCollection();
            List<int> returnStatus = new List<int>();
            returnStatus.AddRange(new List<int> { (int)WmsReturnOrderStatus.Initial, (int)WmsReturnOrderStatus.Apply,(int)WmsReturnOrderStatus.Fail, (int)WmsReturnOrderStatus.Verify ,(int)WmsReturnOrderStatus.WmsCancel, (int)WmsReturnOrderStatus.WmsShip });
            //抓取倉儲申請中的資料，需排除Del & Shipped
            var vwrol = _wp.ViewWmsReturnOrdermList();
            returnIds = vwrol.Where(x => x.Invalidation == (int)WmsInvalidation.No && !returnStatus.Contains(x.Status)).Select(x => x.PchomeReturnOrderId).ToArray();
            //呼叫Wms API(查詢還貨單資訊 5.2.5)
            getReturns = PchomeWmsAPI.GetReturns(returnIds);


            foreach (var item in getReturns)
            {
                try
                {
                    //先取得原本的資料
                    wrol = _wp.WmsReturnOrderByPchomeReturnId(item.Id);

                    if (wrol.Count()>0)
                    {
                        foreach (var wro in wrol)
                        {
                            if (!string.IsNullOrEmpty(item.ApplyDate))
                                wro.ApplyDate = DateTime.Parse(item.ApplyDate);
                            if (!string.IsNullOrEmpty(item.ReadyDate))
                                wro.ReadyDate = DateTime.Parse(item.ReadyDate);
                            if (!string.IsNullOrEmpty(item.ShipDate))
                                wro.ShipDate = DateTime.Parse(item.ShipDate);

                            switch (item.Status)
                            {
                                case "Init":
                                    wro.Status = (int)WmsReturnOrderStatus.Submit;
                                    _wp.WmsReturnOrderSet(wro);
                                    break;
                                case "Examine":
                                    wro.Status = (int)WmsReturnOrderStatus.WmsPass;
                                    _wp.WmsReturnOrderSet(wro);
                                    break;
                                case "ReadyToGo":
                                    wro.Status = (int)WmsReturnOrderStatus.WmsOutBound;
                                    _wp.WmsReturnOrderSet(wro);
                                    break;
                                case "Del":
                                    wro.Status = (int)WmsReturnOrderStatus.WmsCancel;
                                    _wp.WmsReturnOrderSet(wro);
                                    break;
                                case "Shipped":
                                    wro.Status = (int)WmsReturnOrderStatus.WmsShip;
                                    _wp.WmsReturnOrderSet(wro);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("SyncWmsReturnOrderStatusJob error. Pchome_Return_Id={0}, ex={1}", item.Id, ex);
                }
            }


            IsRunning = false;
        }
    }
}

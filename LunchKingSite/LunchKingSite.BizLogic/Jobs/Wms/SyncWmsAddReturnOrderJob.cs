﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.Wms;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System;
using System.Collections.Generic;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Models.PponEntities;

namespace LunchKingSite.BizLogic.Jobs.Wms
{
    /// <summary>
    /// 呼叫 PChome Wms倉儲系統API，新增還貨單
    /// </summary>
    public class SyncWmsAddReturnOrderJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(SyncWmsAddReturnOrderJob));
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static object thisLock = new object();
        private static bool _isRunning;

        protected static bool IsRunning
        {
            get
            {
                lock (thisLock)
                {
                    return _isRunning;
                }
            }
            set
            {
                lock (thisLock)
                {
                    _isRunning = value;
                }
            }
        }

        public void Execute(JobSetting js)
        {
            if (IsRunning)
            {
                return;
            }
            IsRunning = true;

            WmsAddReturn wmsReturn = new WmsAddReturn();
            WmsAddReturn.ReturnDetail detail = new WmsAddReturn.ReturnDetail();
            WmsPerson receiveContact = new WmsPerson();
            WmsContact contact = new WmsContact();
            WmsReturnOrder wro = new WmsReturnOrder();
            List<Guid> orderIdList = new List<Guid>();

            //抓取倉儲已審核的資料，並且沒有作廢
            var vwrol = _wp.ViewWmsReturnOrdermListByStatus((int)WmsReturnOrderStatus.Verify, (int)WmsInvalidation.No);

            foreach (var item in vwrol)
            {
                try
                {
                    contact = _wp.WmsContactGet(item.AccountId);

                    detail = new WmsAddReturn.ReturnDetail
                        {
                            ProdId = item.PchomeProdId,
                            Qty = item.Qty,
                            Remark = item.Remark
                        };

                    if (contact!=null)
                    {
                        receiveContact = new WmsPerson
                        {
                            Name = string.IsNullOrEmpty(contact.ReturnerName) ? "" : contact.ReturnerName,
                            Tel = string.IsNullOrEmpty(contact.ReturnerTel) ? "" : contact.ReturnerTel,
                            Mobile = string.IsNullOrEmpty(contact.ReturnerMobile) ? "" : contact.ReturnerMobile,
                            Email = string.IsNullOrEmpty(contact.ReturnerEmail) ? "" : contact.ReturnerEmail,
                            Zip = string.IsNullOrEmpty(contact.ReturnerZip) ? "" : contact.ReturnerZip,
                            Address = string.IsNullOrEmpty(contact.ReturnerAddress) ? "" : contact.ReturnerAddress
                        };
                    }


                    wmsReturn = new WmsAddReturn
                    {
                        Detail = detail,
                        SupplyId = item.AccountId,
                        isSelfPick = (int)WmsIsSelfPick.No,
                        ReceiveContact = receiveContact
                    };

                    if (PchomeWmsAPI.AddReturn(wmsReturn))
                    {
                        //回寫pchomeReturnId
                        wro = _wp.WmsReturnOrderGet(item.ReturnOrderGuid);
                        if (wro != null)
                        {
                            wro.PchomeReturnOrderId = wmsReturn.Id;
                            wro.Status = (int)WmsReturnOrderStatus.Submit;
                            _wp.WmsReturnOrderSet(wro);
                        }
                    }
                    else
                    {
                        //回寫錯誤訊息
                        wro = _wp.WmsReturnOrderGet(item.ReturnOrderGuid);
                        if (wro != null)
                        {
                            wro.Invalidation = (int)WmsInvalidation.Yes;
                            wro.Status = (int)WmsReturnOrderStatus.WmsFail;
                            wro.ErrorX = wmsReturn.Error;
                            _wp.WmsReturnOrderSet(wro);

                            orderIdList = new List<Guid>();
                            orderIdList.Add(wro.Guid);

                            ProposalFacade.SendWmsReturnOrderMail(orderIdList, false, false, true);
                        }
                    }

                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("SyncWmsAddReturnOrderJob error. Wms_Return_Id={0}, ex={1}", item.ReturnOrderGuid, ex);
                }
            }


            IsRunning = false;
        }
    }
}

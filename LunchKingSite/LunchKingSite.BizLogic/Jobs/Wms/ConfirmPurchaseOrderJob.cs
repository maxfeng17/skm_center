﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Jobs.Wms
{
    /// <summary>
    /// 呼叫 PChome Wms倉儲系統API，取得進貨單的狀態，確認後更新本機端商品庫存
    /// </summary>
    public class ConfirmPurchaseOrderJob : IJob
    {
        private ILog logger = LogManager.GetLogger(typeof(ConfirmPurchaseOrderJob));
        private IWmsProvider _wp = ProviderFactory.Instance().GetDefaultProvider<IWmsProvider>();
        private IPponProvider _pp = ProviderFactory.Instance().GetDefaultProvider<IPponProvider>();

        public void Execute(JobSetting js)
        {
            var wpoCol = _wp.WmsPurchaseOrderGetListForWmsCheck();

            foreach(WmsPurchaseOrder wpo in wpoCol)
            {
                if (string.IsNullOrEmpty(wpo.PchomePurchaseOrderId))
                {
                    continue;
                }
                try
                {
                    Models.Wms.WmsPurchaseOrder apiOrder = PchomeWmsAPI.GetPurchaseOrder(wpo.PchomePurchaseOrderId);
                    if (apiOrder.Status != "ValidReceipt" && apiOrder.Status != "Arrived" && apiOrder.Status != "Inventory" && apiOrder.Status != "Cancel")
                    {
                        continue;
                    }
                    using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                    {
                        WmsPurchaseOrder wpoUpd = _wp.WmsPurchaseOrderGetWithUpdateLock(wpo.Guid);
                        if (wpoUpd.Status == (int)PurchaseOrderStatus.WmsInventory ||
                            wpoUpd.Status == (int)PurchaseOrderStatus.WmsCancel)
                        {
                            continue;
                        }
                        if (apiOrder.Status == "ValidReceipt" && wpo.ApplyDate != null)
                        {
                            continue;
                        }
                        if (apiOrder.Status == "Arrived" && wpo.Status == (int)PurchaseOrderStatus.WmsArrived)
                        {
                            continue;
                        }
                        if (apiOrder.Status == "Arrived" && wpo.Status != (int)PurchaseOrderStatus.WmsArrived)
                        {
                            wpoUpd.Status = (int)PurchaseOrderStatus.WmsArrived;
                        }
                        else if (apiOrder.Status == "Cancel")
                        {
                            wpoUpd.Status = (int)PurchaseOrderStatus.WmsCancel;
                        }
                        else if (apiOrder.Status == "Inventory")
                        {
                            wpoUpd.Status = (int)PurchaseOrderStatus.WmsInventory;
                            wpoUpd.ActualQty = apiOrder.ActualQty;
                            wpoUpd.RefundQty = apiOrder.RefundQty;
                        }


                        //因避免狀態快速跳過,統一在此更新
                        if (!string.IsNullOrEmpty(apiOrder.ExpireDate))
                        {
                            wpoUpd.ExpireDate = Convert.ToDateTime(apiOrder.ExpireDate);
                        }
                        if (!string.IsNullOrEmpty(apiOrder.ArrivalDate))
                        {
                            wpoUpd.ArrivalDate = Convert.ToDateTime(apiOrder.ArrivalDate);
                        }
                        if (!string.IsNullOrEmpty(apiOrder.EntryDate))
                        {
                            wpoUpd.EntryDate = Convert.ToDateTime(apiOrder.EntryDate);
                        }
                        if (!string.IsNullOrEmpty(apiOrder.ApplyDate))
                        {
                            wpoUpd.ApplyDate = Convert.ToDateTime(apiOrder.ApplyDate);
                        }

                        wpoUpd.ModifyId = "sys";
                        wpoUpd.ModifyTime = DateTime.Now;
                        _wp.WmsPurchaseOrderSet(wpoUpd);
                        WmsFacade.SaveWmsPurchaseOrderLog(new List<Guid> {wpoUpd.Guid}, wpoUpd.Status, wpoUpd.ModifyId);

                        var vpi = _pp.ViewProductItemGet(wpo.ItemGuid);
                        if (vpi == null || vpi.IsLoaded == false)
                        {
                            throw new Exception("Product Item 找不到, ItemGuid=" + wpo.ItemGuid);
                        }
                        //這邊鎖的重點在 WmsPurchaseOrderGetWithUpdateLock
                        //預期是 要保證改成 WmsInventory 狀態，只會有一次。
                        //且在該次更新庫存
                        //因為也沒有地方的地方會改庫存數字
                        if (wpoUpd.Status == (int)PurchaseOrderStatus.WmsInventory && apiOrder.ActualQty > 0)
                        {
                            ProductItemStock stock = new ProductItemStock()
                            {
                                Guid = Guid.NewGuid(),
                                InfoGuid = vpi.InfoGuid.GetValueOrDefault(),
                                ItemGuid = vpi.ItemGuid,
                                BeginningStock = vpi.Stock,
                                AdjustStock = apiOrder.ActualQty,
                                EndingStock = vpi.Stock + apiOrder.ActualQty,
                                CreateId = "sys",
                                CreateTime = DateTime.Now,
                                ModifyId = "sys",
                                ModifyTime = DateTime.Now
                            };
                            _pp.ProductItemStockSet(stock);

                            _pp.ProductItemStockIncrease(wpo.ItemGuid, apiOrder.ActualQty, "sys");

                            if (vpi.InfoGuid!=null)
                            {
                                PponFacade.UpdateDealAllMaxItemCount((Guid)vpi.InfoGuid);
                            }
                        }
                        tx.Complete();
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("ConfirmPurchaseOrderJob error. wpo's guid={0}, ex={1}", wpo.Guid, ex);
                }
            }
        }
    }
}

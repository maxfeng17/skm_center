﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Jobs.Wms
{
    /// <summary>
    /// 失敗訂單通知客服
    /// </summary>
    public class FailureOrderCsdNoticeJob : IJob
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(SyncWmsOrderJob));
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static object thisLock = new object();
        private static bool _isRunning;
        protected static bool IsRunning
        {
            get
            {
                lock (thisLock)
                {
                    return _isRunning;
                }
            }
            set
            {
                lock (thisLock)
                {
                    _isRunning = value;
                }
            }
        }

        public void Execute(JobSetting js)
        {
            if (IsRunning)
            {
                return;
            }
            IsRunning = true;

            try
            {
                WmsOrderCollection woc = _wp.WmsOrderGetFailure(DateTime.Today.AddDays(-30), DateTime.Today.AddDays(1));

                List<Guid> orderGuids = new List<Guid>();

                foreach (WmsOrder wo in woc)
                {
                    ReturnFormCollection rfc = _op.ReturnFormGetListByOrderGuid(wo.OrderGuid);
                    if (rfc.Count == 0)
                    {
                        //未申請退貨
                        orderGuids.Add(wo.OrderGuid);
                    }
                }

                MailToCsd(orderGuids);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("FailureOrderCsdNoticeJob error. ex={0}", ex);
            }

            IsRunning = false;
        }

        /// <summary>
        /// 寄信給客服
        /// </summary>
        private void MailToCsd(List<Guid> orderGuids)
        {
            StringBuilder builder = new StringBuilder();

            if(orderGuids.Count == 0)
            {
                return;
            }

            builder.AppendLine("<table>");
            builder.AppendLine("<tr><td>訂購時間</td><td>訂單編號</td><td>檔次</td><td>完成付款日</td><td>EMAIL</td></tr>");

            int failCnt = 0;
            foreach (Guid orderGuid in orderGuids)
            {
                var order = _op.OrderGet(orderGuid);
                if (order != null && order.IsLoaded)
                {
                    DataOrm.CashTrustLog ctl = _mp.CashTrustLogGetByOrderId(order.Guid);
                    string itemName = string.Empty;
                    if (ctl != null && ctl.IsLoaded)
                    {
                        if(ctl.Atm == 0)
                        {
                            continue;
                        }
                        DataOrm.IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(ctl.BusinessHourGuid.GetValueOrDefault());
                        itemName = vpd.ItemName;
                    }
                    builder.AppendLine("<tr>");

                    builder.AppendLine(string.Format("<td>{0}</td>", order.CreateTime.ToString("yyyy/MM/dd HH:mm:ss")));//訂購時間
                    builder.AppendLine(string.Format("<td>{0}</td>", order.OrderId));//訂單編號
                    builder.AppendLine(string.Format("<td>{0}</td>", itemName));//檔次
                    DateTime? payDate = OrderFacade.GetPaymentCompletedDate(order.Guid);
                    string completeDate = string.Empty;
                    if (payDate != null)
                    {
                        completeDate = payDate.Value.ToString("yyyy/MM/dd");
                    }
                    builder.AppendLine(string.Format("<td>{0}</td>", completeDate));//付款日

                    DataOrm.Member m = _mp.MemberGet(order.UserId);
                    string email = string.Empty;
                    if (m != null && m.IsLoaded)
                    {
                        email = m.UserEmail;
                    }
                    builder.AppendLine(string.Format("<td>{0}</td>", email));//EMAIL

                    builder.AppendLine("</tr>");
                    failCnt++;
                }
            }

            builder.AppendLine("</table>");

            if(failCnt > 0)
            {
                string subject = string.Format("ATM訂單無法完成出貨訂單清冊_{0}", DateTime.Now.ToString("yyyyMMdd"));
                List<string> mailList = new List<string> { config.CsdEmail };
                string[] mls = config.VbsShortageMail.Split(",");
                List<string> mailCCUser = new List<string>();
                mailCCUser.AddRange(mls);
                foreach (string m in mailList)
                {
                    SendMail(subject, mailList, mailCCUser, builder.ToString());
                }
            }
        }

        private void SendMail(string subject, List<string> mailToUser, List<string> mailCCUser, string content)
        {
            mailToUser = mailToUser.Distinct().ToList();
            try
            {
                foreach (string email in mailToUser)
                {
                    string[] emails = email.Split(";");
                    foreach (string mail in emails)
                    {
                        if (RegExRules.CheckEmail(mail))
                        {
                            MailMessage msg = new MailMessage();
                            msg.From = new MailAddress(config.SystemEmail);
                            if (mailCCUser != null)
                            {
                                foreach (string ccemail in mailCCUser)
                                {
                                    msg.CC.Add(ccemail);
                                }
                            }
                            msg.To.Add(mail);
                            msg.Subject = subject;
                            msg.Body = System.Web.HttpUtility.HtmlDecode(content);
                            msg.IsBodyHtml = true;
                            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("subject={0}, content={1}, ex={2}", subject, content, ex.Message));
            }
        }
    }
}

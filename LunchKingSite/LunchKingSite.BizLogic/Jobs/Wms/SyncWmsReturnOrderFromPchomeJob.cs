﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models.Wms;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs.Wms
{
    /// <summary>
    /// 呼叫 PChome Wms倉儲系統API，取得一段時間內的Pchome還貨資料
    /// 比對17Life內資料，查無資料的ReturnId送往查詢進行資料建檔
    /// </summary>
    public class SyncWmsReturnOrderFromPchomeJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(SyncWmsReturnOrderFromPchomeJob));
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static ISellerProvider _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static object thisLock = new object();
        private static bool _isRunning;

        protected static bool IsRunning
        {
            get
            {
                lock (thisLock)
                {
                    return _isRunning;
                }
            }
            set
            {
                lock (thisLock)
                {
                    _isRunning = value;
                }
            }
        }

        public void Execute(JobSetting js)
        {
            SyncWmsReturnOrderFromPchome(default(DateTime));
        }

        public static void SyncWmsReturnOrderFromPchome(DateTime inputSDate)
        { 
            if (IsRunning)
            {
                return;
            }
            IsRunning = true;

            DateTime startApplyDate = DateTime.Now;
            DateTime endApplyDate = DateTime.Now.AddDays(1);
            string[] searchReturnIds;
            List<WmsReturn> getReturns = new List<WmsReturn>();
            WmsReturnOrder wmsReturn = new WmsReturnOrder();
            ProductItem pi = new ProductItem();
            ProductInfo pInfo = new ProductInfo();
            SellerSale sellersale = new SellerSale();
            int status = 0;
            DateTime applyDate = default(DateTime);
            DateTime readyDate = default(DateTime);
            DateTime shipDate = default(DateTime);
            DateTime sDate = default(DateTime);
            DateTime eDate = default(DateTime);
            List<string> returnId = new List<string>();

            //取出所有資料庫資料
            var vwrol = _wp.ViewWmsReturnOrdermList();
            if (inputSDate==default(DateTime))
            {
                sDate = DateTime.Parse(DateTime.Now.AddDays(-7).ToString("yyyy/MM/dd"));
                eDate = DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy/MM/dd"));
                //取得資料庫內所有資料(7天前~當天)
                returnId = vwrol.Where(x => x.CreateTime >= sDate && x.CreateTime <= eDate).Select(x => x.PchomeReturnOrderId).ToList();
            }
            else
            {
                //取得資料庫內所有資料
                returnId=vwrol.Select(x => x.PchomeReturnOrderId).ToList();
                startApplyDate = inputSDate;
            }
            //呼叫Wms API 查詢還貨清單:用還貨申請日期區間 (5.2.6)
            WmsReturnQueryList wmql = PchomeWmsAPI.GetWmsReturnQueryList(startApplyDate, endApplyDate);

            searchReturnIds = wmql.Rows.Select(x => x.Id).ToList().Except(returnId).ToArray();

            //呼叫Wms API(查詢還貨單資訊 5.2.5)
            getReturns = PchomeWmsAPI.GetReturns(searchReturnIds);

            foreach (var item in getReturns)
            {
                applyDate = default(DateTime);
                readyDate = default(DateTime);
                shipDate = default(DateTime);

                DateTime.TryParse(item.ApplyDate, out applyDate);
                DateTime.TryParse(item.ReadyDate, out readyDate);
                DateTime.TryParse(item.ShipDate, out shipDate);

                switch (item.Status)
                {
                    case "Init":
                        status = (int)WmsReturnOrderStatus.Submit;
                        break;
                    case "Examine":
                        status = (int)WmsReturnOrderStatus.WmsPass;
                        break;
                    case "ReadyToGo":
                        status = (int)WmsReturnOrderStatus.WmsOutBound;
                        break;
                    case "Del":
                        status = (int)WmsReturnOrderStatus.WmsCancel;
                        break;
                    case "Shipped":
                        status = (int)WmsReturnOrderStatus.WmsShip;
                        break;
                    default:
                        break;
                }
                foreach (var detail in item.Detail)
                {
                    try
                    {

                        //取得Product_item
                        pi = _pp.GetProductItemByProdId(detail.ProdId);
                        //取得Product_Info
                        pInfo = _pp.ProductInfoGet(pi.InfoGuid);

                        if (pi.IsLoaded)
                        {
                            //取得seller，saleGroup並沒有列舉，檢查正式機資料庫只有1進行抓取
                            sellersale = _sp.SellerSaleGetBySellerGuid(pInfo.SellerGuid,1,(int)SellerSalesType.Operation);

                            if (sellersale.IsLoaded)
                            {
                                //新增Wms_Return_Order
                                wmsReturn = new WmsReturnOrder();
                                wmsReturn.Guid = Guid.NewGuid();
                                wmsReturn.ItemGuid = pi.Guid;
                                wmsReturn.Qty = detail.Qty;
                                wmsReturn.SalesId = sellersale.SellerSalesId;
                                wmsReturn.Status = status;
                                wmsReturn.PchomeReturnOrderId = item.Id;
                                wmsReturn.Invalidation = item.isSelfPick;
                                wmsReturn.CreateId = item.SupplyId;
                                wmsReturn.CreateTime = applyDate;
                                if (applyDate != default(DateTime))
                                    wmsReturn.ApplyDate = applyDate;
                                if (readyDate != default(DateTime))
                                    wmsReturn.ReadyDate = readyDate;
                                if (shipDate != default(DateTime))
                                    wmsReturn.ShipDate = shipDate;
                                wmsReturn.Remark = detail.Remark;
                                wmsReturn.Source = (int)WmsReturnSource.PChome;
                                wmsReturn.Reason = detail.Reason;

                                _wp.WmsReturnOrderSet(wmsReturn);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        logger.ErrorFormat("SyncWmsReturnOrderFromPchomeJob error. Pchome_Return_Id={0},Pchome_Product_Id={1}, ex={2}", item.Id, detail.ProdId, ex);
                    }
                }
            }


            IsRunning = false;
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.SsBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Jobs.Wms
{
    /// <summary>
    /// 呼叫 PChome Wms倉儲系統API，將24H出貨的訂單，傳給Pchome
    /// </summary>
    public class SyncWmsOrderJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(SyncWmsOrderJob));
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static object thisLock = new object();
        private static bool _isRunning;

        protected static bool IsRunning
        {
            get
            {
                lock (thisLock)
                {
                    return _isRunning;
                }
            }
            set
            {
                lock (thisLock)
                {
                    _isRunning = value;
                }
            }
        }

        public void Execute(JobSetting js)
        {
            if (IsRunning)
            {
                return;
            }
            IsRunning = true;

            var readyTransferWmsOrders = WmsFacade.GetReadyTransferOfWmsOrders();
            foreach (var order in readyTransferWmsOrders)
            {
                try
                {
                    string content;

                    if (PchomeWmsAPI.AddOrder(order, out content))
                    {
                        var wo = _wp.WmsOrderGet(Guid.Parse(order.VendorOrderId));
                        wo.SyncTime = DateTime.Now;
                        wo.Status = (int)WmsOrderSendStatus.Success;
                        wo.WmsOrderId = order.Id;
                        _wp.WmsOrderSet(wo);
                    }
                    else
                    {
                        var wo = _wp.WmsOrderGet(Guid.Parse(order.VendorOrderId));
                        wo.SyncTime = DateTime.Now;
                        wo.Status = (int)WmsOrderSendStatus.failure;
                        wo.Message = content;
                        _wp.WmsOrderSet(wo);

                        SendMail(wo.OrderGuid);

                        logger.WarnFormat("SyncWmsOrderJob add order failed. order_guid={0}, message={1}", order.VendorOrderId, content);
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("SyncWmsOrderJob error. order_guid={0}, ex={1}", order.VendorOrderId, ex);
                }
            }

            IsRunning = false;
        }

        private void SendMail(Guid oid)
        {
            try
            {
                DataOrm.Order o = _op.OrderGet(oid);
                if (Helper.IsFlagSet(o.OrderStatus, OrderStatus.ATMOrder) == false)
                {
                    //非ATM訂單不寄發通知信件
                    return;
                }
                if (o == null || o.IsLoaded == false)
                {
                    logger.WarnFormat("SyncWmsOrderJob send mail failed. orderId={0} - [A1]", o.Guid);
                    return;
                }

                if (Helper.IsFlagSet(o.OrderStatus, OrderStatus.Complete) == false)
                {
                    logger.WarnFormat("SyncWmsOrderJob send mail failed. orderId={0} - [A2]", o.OrderId);
                    return;
                }

                DataOrm.OrderDetailCollection odc = _op.OrderDetailGetList(o.Guid);
                int quantity = odc.Select(x => x.ItemQuantity).Sum();

                DataOrm.Member mem = _mp.MemberGet(o.UserId);
                if (mem == null || mem.IsLoaded == false)
                {
                    logger.WarnFormat("SyncWmsOrderJob send mail failed. orderId={0} - [A3]", o.OrderId);
                    return;
                }
                DataOrm.CashTrustLogCollection ctls = _mp.CashTrustLogListGetByOrderId(o.Guid);
                if (ctls.Count == 0)
                {
                    logger.WarnFormat("SyncWmsOrderJob send mail failed. orderId={0} - [A4]", o.OrderId);
                    return;
                }
                DataOrm.IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(ctls.FirstOrDefault().BusinessHourGuid.GetValueOrDefault());

                DataOrm.ViewCouponListMain main = MemberFacade.ViewCouponListMainGetByOrderGuid(mem.UniqueId, o.Guid).FirstOrDefault();
                Models.ShipInfo shipInfo = OrderFacade.GetOrderShipInfo(main);

                var template = TemplateFactory.Instance().GetTemplate<Core.Component.Template.WmsPchomeAtmOrderFailMail>();
                template.MemberName = string.Format("{0}{1}", mem.LastName, mem.FirstName);
                template.OrderDate = o.CreateTime.ToString("yyyy/MM/dd");
                template.OrderId = o.OrderId;
                template.DealName = vpd.ItemName;
                template.Quantity = quantity;
                template.OrderStatus = shipInfo.ShipStatusDesc;
                template.Atm = ctls.Select(x => x.Atm).Sum();

                string subject = string.Format("17Life ATM訂單缺貨通知({0})", o.OrderId);
                List<string> mailToList = new List<string>();
                mailToList.Add(mem.UserEmail);
                string[] mls = config.VbsShortageMail.Split(",");
                mailToList.AddRange(mls);

                foreach (string mailTo in mailToList)
                {
                    SendMail(subject, mailTo, template.ToString());
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("SyncWmsOrderJob send mail failed. orderId={0} , ex={1} - [A2]", oid, ex);
            }
        }

        private void SendMail(string subject, string mailTo, string content)
        {
            MailMessage msg = new MailMessage();
            msg.Subject = subject;
            msg.From = new MailAddress(config.ServiceEmail, config.ServiceEmail);
            msg.To.Add(mailTo);
            msg.Body = content;
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }
    }
}

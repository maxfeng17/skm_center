﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.Wms;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.SsBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Jobs.Wms
{
    /// <summary>
    /// 呼叫 PChome Wms倉儲系統API，取得出貨單的狀態，更新本機端出貨單進度
    /// 若已出貨且有物流資訊，就會通知客戶
    /// </summary>
    public class SyncWmsOrderDeliveryJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(SyncWmsOrderJob));
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();

        private static object thisLock = new object();
        private static bool _isRunning;

        protected static bool IsRunning
        {
            get
            {
                lock (thisLock)
                {
                    return _isRunning;
                }
            }
            set
            {
                lock (thisLock)
                {
                    _isRunning = value;
                }
            }
        }

        public void Execute(JobSetting js)
        {
            if (IsRunning)
            {
                return;
            }
            IsRunning = true;

            var readySyncShipStatusOfWmsOrders = WmsFacade.GetReadySyncShipStatusOfWmsOrders();
            foreach (var item in readySyncShipStatusOfWmsOrders)
            {
                try
                {
                    //查詢訂單
                    var wmsOrdersInfo = PchomeWmsAPI.GetOrder(item.WmsOrderId);

                    if (wmsOrdersInfo.ShipStatus == WmsOrderShipStatus.Init.ToString() || wmsOrdersInfo.ShipStatus == WmsOrderShipStatus.Progressing.ToString())
                    {
                        WmsFacade.UpdateWmsOrderByWmsOrderInfo(item.OrderGuid, (int)WmsDeliveryStatus.Processing, null, false);
                    }
                    else if (wmsOrdersInfo.ShipStatus == WmsOrderShipStatus.Shipped.ToString())
                    {
                        //查詢出貨進度:用多個訂單編號(限筆數)
                        List<WmsOrderProgressStatus> orderProgressStatus = PchomeWmsAPI.GetOrderProgressStatus(item.WmsOrderId); //一張訂單pchome可能分多次揀貨

                        if (WmsFacade.ProgressStatusNotChanged(item.OrderGuid ,orderProgressStatus) 
                            && (item.SendNotification || !WmsFacade.NeedNotifiedByProgressStatus(orderProgressStatus.First())))
                        {
                            continue;
                        }

                        //查詢出貨物流資訊
                        List<WmsLogistic> wmsLogistics = PchomeWmsAPI.GetLogistics(item.WmsOrderId);
                                                
                        List<CurrentWmsOrderProgress> currentWmsOrderProgresses
                            = WmsFacade.GetWmsOrderProgresses(orderProgressStatus, wmsLogistics, item.OrderGuid);

                        //取第一筆出貨單作為訂單狀態
                        var firstCurrentWmsOrderProgresses = currentWmsOrderProgresses.FirstOrDefault();
                        //要傳通知
                        bool wantToSend = firstCurrentWmsOrderProgresses != null 
                            && (!item.SendNotification
                            && (firstCurrentWmsOrderProgresses.CurrentProgressStatus == (int)WmsDeliveryStatus.Shipping
                            || firstCurrentWmsOrderProgresses.CurrentProgressStatus == (int)WmsDeliveryStatus.Missed));

                        if (wantToSend)
                        {
                            WmsFacade.SendShipmentNotification(item.OrderGuid, firstCurrentWmsOrderProgresses);
                        }

                        int wmsShipStatus = (int)WmsDeliveryStatus.Processing;
                        DateTime? wmsShipDate = null;
                        if (firstCurrentWmsOrderProgresses != null)
                        {
                            wmsShipStatus = firstCurrentWmsOrderProgresses.CurrentProgressStatus;
                            wmsShipDate = firstCurrentWmsOrderProgresses.ShipDate;
                        }

                        //更新出貨單
                        WmsFacade.UpdateWmsOrderShipByWmsOrderShipInfo(currentWmsOrderProgresses);
                        //更新訂單出貨狀態
                        WmsFacade.UpdateWmsOrderByWmsOrderInfo(item.OrderGuid, wmsShipStatus, wmsShipDate, wantToSend);
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("SyncWmsOrderDeliveryJob error. order_guid={0}, ex={1}", item.OrderGuid, ex);
                }

            }

            IsRunning = false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs
{
    public class MembershipCardRegionResetJob : IJob
    {
        private static ISystemProvider _systemProvider;
        private static ILocationProvider _locationProvider;

        static MembershipCardRegionResetJob()
        {
            _systemProvider = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _locationProvider = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        }

        public void Execute(JobSetting js)
        {
            //取得即時的資料
            ApiMembershipCardRegionListCatch realTimeData = ApiMembershipCardRegionManager.GetCatchDataByDB();
            //取得暫存的資料
            ApiMembershipCardRegionListCatch catchData = new ApiMembershipCardRegionListCatch();
            SystemData data = _systemProvider.SystemDataGet(ApiMembershipCardRegionManager.SystemDataName);
            if (data.IsLoaded)
            {
                //直接以systemData的資料為準
                JsonSerializer json = new JsonSerializer();
                try
                {
                    catchData =
                        json.Deserialize<ApiMembershipCardRegionListCatch>(data.Data);
                }
                catch
                {
                    //json轉換出錯，異動DATA內容
                    data.Data = (new JsonSerializer()).Serialize(realTimeData);
                    data.ModifyId = "sys@17life.com";
                    data.ModifyTime = DateTime.Now;
                    _systemProvider.SystemDataSet(data);
                    return;
                }
                //暫存與即時資料比對版本，若與即時資料不同，以即時資料的內容為準
                if (realTimeData.Version != catchData.Version)
                {
                    data.Data = (new JsonSerializer()).Serialize(realTimeData);
                    data.ModifyId = "sys@17life.com";
                    data.ModifyTime = DateTime.Now;
                    _systemProvider.SystemDataSet(data);
                }
            }
            else
            {
                //沒有讀到資料。新增即時的資料
                data.Name = ApiMembershipCardRegionManager.SystemDataName;
                data.Data = (new JsonSerializer()).Serialize(realTimeData);
                data.CreateId = "sys@17life.com";
                data.CreateTime = DateTime.Now;

                _systemProvider.SystemDataSet(data);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using log4net;

namespace LunchKingSite.BizLogic.Jobs
{
    public class SkmMemberPhSyncJob : IJob {
        private static ILog logger = LogManager.GetLogger(typeof(SkmMemberPhSyncJob));
        public void Execute(JobSetting js) {
            SkmFacade.SkmMemberPhSyncJob();
            logger.Info("SkmMemberPhSyncJob, Updated complete table skm_member_ph");
        }
    }
}

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class PponDealsIndexJob : IJob
    {
        public void Execute(JobSetting js)
        {
            // 依據搜尋紀錄更新關鍵字資料庫
            PponFacade.UpdateKeywords();
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * 轉介人員獎金計算日期更新
 * */
namespace LunchKingSite.BizLogic.Jobs
{
    public class SellerRefferUpdate : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(PaymentCharge));
        private DateTime now = DateTime.Now;
        private static ISysConfProvider config;
        private static IPponProvider pp;
        private static ISellerProvider sp;
        private static IHumanProvider hp;

        static SellerRefferUpdate()
        {
            config = ProviderFactory.Instance().GetConfig();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        }
        public void Execute(JobSetting js)
        {
            DateTime today = DateTime.Now;

            var vpds = ViewPponDealManager.DefaultManager.ViewPponDealGetList(true);
            List<Guid> sellerGuids = vpds.Select(x => x.SellerGuid).Distinct().ToList();

            List<Guid> newSellerGuids = sp.SellerGetList(sellerGuids).ToList().Where(x => !string.IsNullOrEmpty(x.ReferralSale))
                .Select(x => x.Guid).Distinct().ToList();

            #region 找出轉介人員建立後的第一筆開檔檔次
            foreach (Guid guid in newSellerGuids)
            {
                Seller seller = sp.SellerGet(guid);
                if (seller.IsLoaded)
                {
                    if (!string.IsNullOrEmpty(seller.ReferralSale))
                    {
                        if (seller.ReferralSalePercent == null || seller.ReferralSalePercent == 0)
                        {
                            continue;
                        }
                        //有設定轉介人員
                        var data = vpds.Where(x => x.SellerGuid == guid && x.BusinessHourOrderTimeS >= seller.ReferralSaleCreateTime)
                                    .OrderBy(x => x.BusinessHourOrderTimeS).FirstOrDefault();
                        if (data != null)
                        {
                            if (seller.ReferralSaleBeginTime == null)
                            {
                                if (data.BusinessHourOrderTimeS.Year != DateTime.MaxValue.Year)
                                {
                                    string beginDate = data.BusinessHourOrderTimeS.ToString("yyyy/MM/dd");
                                    string endDate = data.BusinessHourOrderTimeS.AddYears(1).AddDays(-1).ToString("yyyy/MM/dd");
                                    seller.ReferralSaleBeginTime = DateTime.Parse(beginDate);
                                    seller.ReferralSaleEndTime = DateTime.Parse(endDate);

                                    sp.SellerSet(seller);

                                    SellerFacade.SellerLogSet(seller.Guid, "系統異動轉介人(A/B)時間: " + beginDate + "~" + endDate, "sys");
                                }
                            }
                            else
                            {
                                if (data.BusinessHourOrderTimeS.Year != DateTime.MaxValue.Year)
                                {
                                    if (data.BusinessHourOrderTimeS.Year != (seller.ReferralSaleBeginTime ?? DateTime.MaxValue).Year)
                                    {
                                        string beginDate = data.BusinessHourOrderTimeS.ToString("yyyy/MM/dd");
                                        string endDate = data.BusinessHourOrderTimeS.AddYears(1).AddDays(-1).ToString("yyyy/MM/dd");

                                        seller.ReferralSaleBeginTime = DateTime.Parse(beginDate);
                                        seller.ReferralSaleEndTime = DateTime.Parse(endDate);

                                        sp.SellerSet(seller);

                                        SellerFacade.SellerLogSet(seller.Guid, "系統異動轉介人(A/B)時間: " + beginDate + "~" + endDate, "sys");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region 檢查離職人員(抓一個月內的離職人員)
            List<ViewEmployee> emps = hp.ViewEmployeeCollectionGetByFilter(ViewEmployee.Columns.IsInvisible, false, false)
                                            .Where(x => (x.DepartureDate ?? DateTime.MaxValue) >= today.AddDays(-30)
                                                && (x.DepartureDate ?? DateTime.MaxValue) <= today).ToList();
            foreach (ViewEmployee emp in emps)
            {
                //員工資料維護中，具有離職日期或隱藏後，則直接消除該欄位值
                //A欄
                SellerCollection OSellers = sp.SellerGetList(Seller.Columns.ReferralSale, emp.Email);
                foreach (Seller seller in OSellers)
                {
                    seller.ReferralSale = null;
                    seller.ReferralSaleBeginTime = null;
                    seller.ReferralSaleEndTime = null;
                    seller.ReferralSaleCreateTime = DateTime.Now;
                    sp.SellerSet(seller);
                }
            }

            #endregion

        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mail;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.BizLogic.Jobs
{
    public class CheckChannelOrderStatus : IJob
    {
        private static IOrderProvider op;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(CheckChannelOrderStatus));
        private StringBuilder sbLog = new StringBuilder();

        static CheckChannelOrderStatus()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                OrderCorrespondingCollection occ = op.OrderCorrespondingGetByOrderCheck((int)AgentChannel.PayEasyOrder, (int)ChannelOrderCheck.Init);
                if (occ.Any())
                {
                    var para = new SCMWebServiceLogin
                    {
                        clientId = config.PezAPIClientId,
                        clientSecret = config.PezAPIClientSecret
                    };

                    var merchant = new SCMWebServiceMerchant
                    {
                        usrNum = config.PezAPIUsrNum,
                        venNum = config.PezAPIVenNum
                    };
                    string returnReason = "PayEasy找無該訂單，系統自動退貨";

                    string token = PayEasyAPI.GetPezToken(para);


                    foreach (OrderCorresponding oc in occ)
                    {
                        Order o = op.OrderGet(oc.OrderGuid);
                        if (Helper.IsFlagSet(o.OrderStatus,OrderStatus.Complete))
                        {
                            var data = new SCMWebServiceSupplierData
                            {
                                shipStartDate = oc.CreatedTime.ToString("yyyy-MM-dd"),
                                shipEndDate = oc.CreatedTime.AddDays(1).ToString("yyyy-MM-dd"),
                                dataIndex = 1,
                                ordId = oc.RelatedOrderId,
                                orderKindType = "HOME",
                                orderType = "EC",
                                orderDealType = "ALL"
                            };

                            var supplier = new SCMWebServiceSupplier
                            {
                                merchant = merchant,
                                data = data,
                            };

                            SCMWebServiceSupplierResult supplierResult = PayEasyAPI.GetSupplierByCondition(supplier, token);
                            if (supplierResult.success && !supplierResult.dataResults.Any())
                            {
                                //宅配退貨申請單，抓出 1.非退貨中 2.非已退貨 3.非換貨中 
                                List<int> orderProductIds = op.OrderProductGetListByOrderGuid(oc.OrderGuid).Where(x => x.IsCurrent && !x.IsReturning && !x.IsReturned && !x.IsExchanging).Select(x => x.Id).ToList();
                                if (orderProductIds.Any())
                                {
                                    int? returnFormId;
                                    CreateReturnFormResult result = ReturnService.CreateRefundForm(oc.OrderGuid, orderProductIds, false, "sys@17life.com", returnReason,
                                        null, null, false, null, out returnFormId, false);


                                    if (result == CreateReturnFormResult.Created)
                                    {
                                        string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId, returnReason);
                                        CommonFacade.AddAudit(oc.OrderGuid, AuditType.Refund, auditMessage, "sys", false);

                                        oc.OrderCheck = (int)ChannelOrderCheck.NotFound;
                                    }
                                    else
                                    {
                                        logger.Error("CheckChannelOrderStatus Error 退貨失敗 orderGuid=" + oc.OrderGuid.ToString());
                                    }
                                }
                            }
                            else if (supplierResult.success && supplierResult.dataResults.Any())
                            {
                                oc.OrderCheck = (int)ChannelOrderCheck.Success;

                            }
                        }
                        else
                        {
                            //訂單購買根本就失敗
                            oc.OrderCheck = (int)ChannelOrderCheck.OrderNotComplete;
                        }

                        op.OrderCorrespondingSet(oc);

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("CheckChannelOrderStatus Error" + ex.Message + ex.StackTrace);
            }
        }

       

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Transactions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using log4net;

namespace LunchKingSite.BizLogic.Jobs
{
    public class AtmClean : IJob
    {
        private static IOrderProvider Op;
        private static IMemberProvider Mp;
        private static ISysConfProvider Config;
        private static ILog logger = LogManager.GetLogger(typeof(AtmClean));
        static AtmClean()
        {
            Op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            Mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            Config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            #region 處理未完成單
            CtAtmCollection atmCol = Op.CtAtmGetPast(DateTime.Parse(DateTime.Now.AddDays((-1)*Config.AtmRemitDeadline).ToShortDateString()));
            foreach (var atm in atmCol)
            {
                try
                {
                    using (var ts = TransactionScopeBuilder.CreateReadCommitted())
                    {
                        atm.Status = (int)AtmStatus.Expired;
                        Op.CtAtmSet(atm);

                        //處理邀請得紅利的部分
                        MemberFacade.MemberPromotionDeductionWhenOrderCancel(atm.OrderGuid, MemberFacade.GetUserName(atm.UserId));

                        CashTrustLogCollection ctCol = Mp.CashTrustLogGetListByOrderGuid(atm.OrderGuid, OrderClassification.LkSite);
                        foreach (var ct in ctCol)
                        {
                            PaymentFacade.CancelPaymentTransactionByCouponForIncompleteOrder(ct, "sys");
                        }
                        CommonFacade.AddAudit(atm.OrderGuid, AuditType.Payment, "已檢查ATM未付款單，若有其他付款項目皆已退回", "sys", true);
                        OrderFacade.SendOrderCancelMail(atm.OrderGuid, false, true);
                        ts.Complete();
                    }
                }
                catch (Exception ex)
                {
                    DoMail(atm.OrderId, ex.Message + ":" + ex.InnerException.InnerException.Message);
                }
            }
            #endregion

            #region 檢查ATM退款成功後,接著退其他款項
            
            CtAtmRefundCollection atmRefundCol = Op.CtAtmRefundGetList(CtAtmRefund.Columns.Id,
                                                                       CtAtmRefund.Columns.Status + " in (" + (int)AtmRefundStatus.RefundSuccess + "," + (int)AtmRefundStatus.RefundFail + ")",
                                                                       CtAtmRefund.Columns.Flag + "<>" + (int)AtmRefundFlag.Checked);

            foreach (var atmRefund in atmRefundCol)
            {
                string msg = string.Format("ATM退款{0}：戶名:{1}，ID:{2}，{3}({4})，帳戶:{5}，手機:{6}", 
                    atmRefund.Status == (int)AtmRefundStatus.RefundSuccess ? "成功" : "失敗",
                    atmRefund.AccountName,
                    atmRefund.Id,
                    atmRefund.BankName,
                    atmRefund.BranchName,
                    atmRefund.AccountNumber,
                    atmRefund.Phone);

                CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.OrderDetail, msg, "sys", true);

                if (atmRefund.ReturnFormId.HasValue)
                {
                    ReturnFormEntity returnForm = ReturnFormRepository.FindById(atmRefund.ReturnFormId.Value);

                    if (int.Equals((int) AtmRefundStatus.RefundFail, atmRefund.Status))
                    {
                        returnForm.ChangeProgressStatus(ProgressStatus.AtmFailed);
                        ReturnFormRepository.Save(returnForm);
                    }
                    else if (int.Equals((int)AtmRefundStatus.RefundSuccess, atmRefund.Status))
                    {
                        returnForm.ChangeProgressStatus(ProgressStatus.AtmQueueSucceeded);
                        ReturnFormRepository.Save(returnForm);
                        try
                        {
                            ReturnService.Refund(returnForm);
                        }
                        catch (Exception ex)
                        {
                            var errMsg = new MailMessage();
                            errMsg.From = new MailAddress(Config.AdminEmail);
                            errMsg.To.Add(new MailAddress(Config.ItTesterEmail));
                            errMsg.Subject = "AtmClean Refund exception!";
                            errMsg.Body = string.Format("退貨單(id={0})退款發生問題!{1}{2}",
                                returnForm.Id, Environment.NewLine, ex.StackTrace.Substring(0, 2500));
                            errMsg.IsBodyHtml = false;
                            PostMan.Instance().Send(errMsg, SendPriorityType.Normal);
                        }
                    }
                }
                else
                {
                    if (int.Equals((int)AtmRefundStatus.RefundFail, atmRefund.Status))
                    {
                        continue;
                    }

                    bool isRefundATM = false;
                    CashTrustLogCollection ctReturnedCol = new CashTrustLogCollection();
                    IList<string> tids = (!string.IsNullOrEmpty(atmRefund.CouponList)) ? atmRefund.CouponList.Split(';') : null;
                    CashTrustLogCollection ctCol = Mp.CashTrustLogGetListByOrderGuid(atmRefund.OrderGuid, OrderClassification.LkSite);

                    foreach (var ct in ctCol)
                    {
                        if (tids == null || tids.Contains(ct.TrustId.ToString()))
                        {
                            if (PaymentFacade.CancelPaymentTransactionByCouponForTrust(ct, "sys"))
                            {
                                isRefundATM = PaymentFacade.RefundAtmByCouponForTrust(ct, "sys");

                                #region 部分退貨紀錄
                                if (!Helper.IsFlagSet(ct.SpecialStatus, TrustSpecialStatus.Freight))
                                {
                                    CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.OrderDetail, "退憑證:#" + ct.CouponSequenceNumber, "sys", true);
                                }
                                else
                                {
                                    CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.Payment, "退運費" + ct.Amount + "元", "sys", true);
                                }
                                #endregion

                                ctReturnedCol.Add(ct);
                            }
                        }
                    }

                    #region 成功退貨與否的處理
                    if (ctReturnedCol.Count() > 0)
                    {
                        OrderDetailCollection odCol = Op.OrderDetailGetList(0, 0, null, atmRefund.OrderGuid, OrderDetailTypes.Regular);
                        int totalQuantity = odCol.Sum(x => x.ItemQuantity);
                        //訂單取消
                        OrderFacade.SetOrderStatus(atmRefund.OrderGuid, "sys", OrderStatus.Cancel, true);
                        Order o = Op.OrderGet(atmRefund.OrderGuid);
                        GroupOrder gpo = Op.GroupOrderGetByOrderGuid(o.ParentOrderId.GetValueOrDefault());
                        if (gpo.IsLoaded == false)
                        {
                            logger.ErrorFormat("AtmClear查詢GroupOrder無資料, orderGuid = {0}", atmRefund.OrderGuid);
                        }
                        OrderFacade.SetOrderStatus(atmRefund.OrderGuid, "sys", OrderStatus.Cancel, true, gpo.BusinessHourGuid);
                        CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.Order, "訂單取消", "sys", false);
                        //同步按下結案
                        OrderFacade.SetOrderStatus(atmRefund.OrderGuid, "sys", OrderStatus.Closed, true);
                        CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.OrderStatusLog, "結案", "sys", false);
                        //同步更改Log為成功
                        string auditOrderStatusLog = PaymentFacade.SaveOrderStatusLog(OrderLogStatus.Success, string.Empty, atmRefund.OrderGuid, "sys");
                        if (!string.IsNullOrEmpty(auditOrderStatusLog))
                        {
                            CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.Order, auditOrderStatusLog, "sys",
                                false);
                        }

                        //同步寄出處理通知
                        string userEmail = MemberFacade.GetUserEmail(atmRefund.UserId);
                        if (RegExRules.CheckEmail(userEmail))
                        {
                            if (ctReturnedCol.Count() == totalQuantity)
                            {
                                //全退的話要列出運費金額，因此要傳入包含運費在內的CashTrustLogCollection
                                if (ctCol.Where(x => Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).Any())
                                {
                                    ctReturnedCol.Add(ctCol.Where(x => Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).FirstOrDefault());
                                }
                                OrderFacade.SendPponMail(MailContentType.ReturnAll, atmRefund.OrderGuid, isRefundATM, ctReturnedCol);
                            }
                            else
                            {
                                OrderFacade.SendPponMail(MailContentType.ReturnPartial, atmRefund.OrderGuid, isRefundATM, ctReturnedCol);
                            }
                            CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.Order, "寄出處理通知", "sys", true);
                        }
                        else
                        {
                            CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.Order, "消費者Email格式錯誤無法寄出通知", "sys",
                                true);
                        }
                    }
                    else if (!string.IsNullOrEmpty(atmRefund.CouponList))
                    {
                        CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.Order, "此訂單無可供退貨之憑證或份數", "sys", true);
                        //同步更改Log為失敗
                        string auditOrderStatusLog = PaymentFacade.SaveOrderStatusLog(OrderLogStatus.Failure, string.Empty, atmRefund.OrderGuid, "sys");

                        if (!string.IsNullOrEmpty(auditOrderStatusLog))
                        {
                            CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.Order, auditOrderStatusLog, "sys",
                                false);
                        }

                        //寄出處理失敗信
                        /*
                        if (rules.ChkEmail(atmRefund.UserName))
                        {
                            OrderFacade.SendPponMail(MailContentType.ReturnFailure, atmRefund.OrderGuid, isRefundATM);
                            CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.Order, "寄出處理失敗通知", "sys", true);
                        }
                        else
                            CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.Order, "消費者Email格式錯誤無法寄出通知", "sys", true);
                        */
                    }
                    #endregion
                }

                #region 將flag設為已檢查
                atmRefund.Flag |= (int)AtmRefundFlag.Checked;
                Op.CtAtmRefundSet(atmRefund);
                #endregion
            }
            #endregion

            #region 檢查ATM退款失敗，回覆錯誤訊息
            CtAtmRefundCollection atmRefundColF = Op.CtAtmRefundGetList(CtAtmRefund.Columns.Id,
                                                    CtAtmRefund.Columns.Status + "=" + (int)AtmRefundStatus.RefundFail,
                                                    CtAtmRefund.Columns.Flag + "<>" + (int)AtmRefundFlag.Checked);
            foreach (var atmRefundF in atmRefundColF)
            {
                CommonFacade.AddAudit(atmRefundF.OrderGuid, AuditType.OrderDetail, "ATM退款失敗：戶名:" + atmRefundF.AccountName + "，ID:" + atmRefundF.Id + "，"
                    + atmRefundF.BankName + "(" + atmRefundF.BranchName + ")，帳戶:" + atmRefundF.AccountNumber + ")，手機:" + atmRefundF.Phone + ")，失敗原因:" + atmRefundF.FailReason, "sys", true);

                #region 將flag設為已檢查
                atmRefundF.Flag |= (int)AtmRefundFlag.Checked;
                Op.CtAtmRefundSet(atmRefundF);
                #endregion
            }
            #endregion
        }

        public void DoMail(string orderId,string exceptionMsg)
        {
            try
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(Config.AdminEmail);
                msg.Subject = "[系統]ATM清除工作發生錯誤";
                msg.From = new MailAddress(Config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = "在 " + DateTime.Now.ToString() + " 時發現了ATM清除工作發生錯誤，請檢查是否有問題。";
                msg.Body += "<br/>訂單編號:" + orderId + "<br/>error:" + exceptionMsg; 
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
    }
}

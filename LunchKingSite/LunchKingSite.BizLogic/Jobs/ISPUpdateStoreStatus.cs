﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.BizLogic.Jobs
{
    public class ISPUpdateStoreStatus : IJob
    {
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        public void Execute(JobSetting js)
        {
            //取出建檔中、測標中的商家 檢查狀態是否有更新
            var vipc = sp.GetISPStoreStauts();
            if (vipc.Count > 0)
            {
                foreach (var vip in vipc)
                {
                    ISPFacade.UpdateStoreStatus(vip);
                }
            }
        }
    }
}
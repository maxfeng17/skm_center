﻿using System;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary>
    /// 推送廣告類型大量的推播
    /// </summary>
    public class AppNotificationJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(AppNotificationJob));
        public void Execute(JobSetting js)
        {
            try
            {
                //推播動作
                int pushPeriodMinutes = -15;
                NotificationFacade.PushAppSchedulMessage(DateTime.Now.AddMinutes(pushPeriodMinutes), DateTime.Now);
            }
            catch (Exception ex)
            {
                logger.Error("AppNotificationJob Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
}

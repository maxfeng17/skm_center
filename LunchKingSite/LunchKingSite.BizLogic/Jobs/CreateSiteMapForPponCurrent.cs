﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System.Net.Mail;
using LunchKingSite.BizLogic.Facade;
using System.Threading.Tasks;
using System.IO;

namespace LunchKingSite.BizLogic.Jobs
{
    public class CreateSiteMapForPponCurrent : IJob
    {
        private static IPponProvider _pponProv;
        private static IOrderProvider _orderProv;
        private static ISysConfProvider _config;
        private static ILog log = LogManager.GetLogger(typeof(CreateSiteMapForPponCurrent));
        private static CreateSiteMapForPponCurrent _theJob;

        static CreateSiteMapForPponCurrent()
        {
            _theJob = new CreateSiteMapForPponCurrent();
        }

        public static CreateSiteMapForPponCurrent Instance()
        {
            return _theJob;
        }

        private CreateSiteMapForPponCurrent()
        {
            _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _orderProv = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                PromotionFacade.SitemapCreateCurrentDeal(DateTime.Today);

                MailMessage msg = new MailMessage();
                msg.To.Add(_config.AdminEmail);
                msg.Subject = "CreateXmlForCurrentDeal Start Running";
                msg.From = new MailAddress(_config.AdminEmail);
                msg.Body = "[系統]CreateXmlForCurrentDeal is running.";
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                log.Error("CreateXmlForCurrentDeal running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
}

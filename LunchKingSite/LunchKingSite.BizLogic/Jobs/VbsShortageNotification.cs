﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Transactions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using log4net;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Component.Template;
using System.Text;

/*
 * 商品庫存不足通知信
 * 1. 廠商
 * 2. 業助
 * 3. 業務
 * 4. Elin
 * */
namespace LunchKingSite.BizLogic.Jobs
{
    public class VbsShortageNotification : IJob
    {
        private static IOrderProvider op;
        private static IHumanProvider hmp;
        private static ISellerProvider sp;
        private static IPponProvider pp;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(CouponSettlement));

        static VbsShortageNotification()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            hmp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[Begin]");
            Dictionary<Guid, List<ShortageNotificationToSellerMail.ProductItemShortage>> itemList = GetVbsProductShortage(ref builder);
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + new JsonSerializer().Serialize(itemList));

            LoadData(itemList, false, ref builder);

            Dictionary<Guid, List<ShortageNotificationToSellerMail.ProductItemShortage>> wmsItemList = GetVbsProductShortage(ref builder, true);
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + new JsonSerializer().Serialize(wmsItemList));

            LoadData(wmsItemList, true, ref builder);

            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[End]");
            ProposalFacade.ProposalPerformanceLogSet("VbsShortageNotification", builder.ToString(), "sys");
        }

        private void LoadData(Dictionary<Guid, List<ShortageNotificationToSellerMail.ProductItemShortage>> itemList, bool isWms, ref StringBuilder builder)
        {
            foreach (var data in itemList)
            {
                ShortageNotificationToSellerMail.ProductShortageModel model = new ShortageNotificationToSellerMail.ProductShortageModel();
                List<ShortageNotificationToSellerMail.ProductItemShortage> itemShortage = new List<ShortageNotificationToSellerMail.ProductItemShortage>();
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + new JsonSerializer().Serialize(data.Value));

                foreach (var item in data.Value)
                {
                    ShortageNotificationToSellerMail.ProductItemShortage shortage = new ShortageNotificationToSellerMail.ProductItemShortage()
                    {
                        ProductName = item.ProductName,
                        ProductCode = item.ProductCode,
                        ProductNo = item.ProductNo,
                        SpecName = item.SpecName,
                        GTINs = item.GTINs,
                        Stock = item.Stock,
                        SafetyStock = item.SafetyStock,
                        SalesMail = item.SalesMail
                    };
                    itemShortage.Add(shortage);
                }

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + new JsonSerializer().Serialize(itemShortage));

                if (itemShortage.Count() > 0)
                {
                    model.ItemList = itemShortage;
                    SendMail(data.Key, model, ref builder, isWms);
                }
            }
        }

        private Dictionary<Guid, List<ShortageNotificationToSellerMail.ProductItemShortage>> GetVbsProductShortage(ref StringBuilder builder, bool isWms = false)
        {
            ProductItemCollection items = pp.ProductItemGetListByShortage();

            var result = new ProductInfoCollection();
            List<Guid> itemsList = items.Select(x => x.InfoGuid).Distinct().ToList();
            var idx = 0;
            while (idx <= itemsList.Count - 1)
            {
                int batchLimit = 500;  // sql server cannot take more than 2100 parameters.
                var batchBh = pp.ProductInfoListGet(itemsList.Skip(idx).Take(batchLimit).ToList());
                result.AddRange(batchBh);
                idx += batchLimit;
            }


            ProductInfoCollection products = result;
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + new JsonSerializer().Serialize(products));

            Dictionary<Guid, List<ShortageNotificationToSellerMail.ProductItemShortage>> itemList = new Dictionary<Guid, List<ShortageNotificationToSellerMail.ProductItemShortage>>();
            //取得庫存不足的Bid
            foreach (ProductItem item in items)
            {
                List<Guid> bids = sp.ProposalMultiOptionSpecGetByItem(item.Guid)
                                 .Select(x => x.BusinessHourGuid).ToList();

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + new JsonSerializer().Serialize(bids));

                ProductInfo product = products.Where(x => x.Guid == item.InfoGuid).FirstOrDefault();
                if (product != null)
                {
                    ShortageNotificationToSellerMail.ProductItemShortage productShortage = new ShortageNotificationToSellerMail.ProductItemShortage
                    {
                        ProductNo = item.ProductNo,
                        ProductName = product.ProductBrandName + product.ProductName,
                        SpecName = item.SpecName,
                        ProductCode = item.ProductCode,
                        GTINs = item.Gtins,
                        Stock = item.Stock,
                        SafetyStock = item.SafetyStock,
                    };

                    foreach (Guid bid in bids.Distinct())
                    {
                        ProposalMultiDeal pmd = sp.ProposalMultiDealGetByBid(bid);
                        if (pmd.IsLoaded)
                        {
                            bool isChecked = false;
                            List<ProposalMultiDealsSpec> dbSpecs = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(pmd.Options);
                            foreach(ProposalMultiDealsSpec dbSpec in dbSpecs)
                            {
                                foreach(var spec in dbSpec.Items)
                                {
                                    isChecked = true;
                                }
                            }
                            if (!isChecked)
                            {
                                continue;
                            }
                        }

                        IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                        if (theDeal.BusinessHourOrderTimeS <= DateTime.Now && theDeal.BusinessHourOrderTimeE >= DateTime.Now && theDeal.Slug == null && !Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
                        {
                            //在線且銷售中
                        }
                        else
                        {
                            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 非在線檔次");
                            //只判斷在線檔次
                            continue;
                        }
                        DealProperty dp = pp.DealPropertyGet(bid);
                        if (dp.IsWms != isWms)
                        {
                            continue;
                        }
                        if (!dp.IsLoaded)
                        {
                            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " DealProperty異常");
                            continue;
                        }
                        int operationSalesId = dp.OperationSalesId.GetValueOrDefault(0);
                        List<string> salesMail = new List<string>();
                        if (operationSalesId != 0)
                        {
                            ViewEmployee emp = hmp.ViewEmployeeGet(ViewEmployee.Columns.UserId, operationSalesId);
                            if (emp.IsLoaded)
                            {
                                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "業務人員mail=" + emp.Email);
                                salesMail.Add(emp.Email);
                            }
                        }
                        productShortage.SalesMail = salesMail;

                        if (!itemList.ContainsKey(theDeal.SellerGuid))
                        {
                            List<ShortageNotificationToSellerMail.ProductItemShortage> dataList = new List<ShortageNotificationToSellerMail.ProductItemShortage>() { productShortage };

                            itemList.Add(theDeal.SellerGuid, dataList);
                        }
                        else
                        {
                            var data = itemList.Where(x => x.Key == theDeal.SellerGuid).FirstOrDefault();
                            data.Value.Add(productShortage);
                        }
                    }
                }
            }
            return itemList;
        }

        private void SendMail(Guid sellerGuid, ShortageNotificationToSellerMail.ProductShortageModel productInfo, ref StringBuilder builder, bool isWms)
        {
            Seller seller = sp.SellerGet(sellerGuid);
            if (seller.IsLoaded)
            {
                List<string> mailList = new List<string>();

                string sellerEmail = string.Empty;
                List<string> sellerMails = new List<string>();
                //商家聯絡人
                List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                if (contacts != null)
                {
                    foreach (var c in contacts)
                    {
                        if (c.Type == ((int)ProposalContact.Normal).ToString() || c.Type == ((int)ProposalContact.ReturnPerson).ToString())
                        {
                            if (!string.IsNullOrEmpty(c.ContactPersonEmail))
                            {
                                if (!sellerMails.Contains(c.ContactPersonEmail))
                                {
                                    sellerMails.Add(c.ContactPersonEmail);
                                }
                            }
                        }
                    }
                }

                sellerEmail = string.Join(",", sellerMails);

                var mailTo = RegExRules.CheckMultiEmail(sellerEmail);
                if (string.IsNullOrEmpty(mailTo))
                {
                    string errorMsg = string.Format("商家:{0}<a href='{1}'>{2}</a> 聯絡人mail有誤，無法發送商品庫存量不足通知信件。聯絡人Email: \"{3}\"", seller.SellerId, config.SiteUrl + "/sal/SellerContent.aspx?sid=" + seller.Guid, seller.SellerName, sellerEmail);
                    try
                    {
                        EmailFacade.SendErrorEmail(seller.Guid, errorMsg);
                    }
                    catch { }

                    logger.Warn(errorMsg);
                }
                else
                {
                    mailList.AddRange(mailTo.Split(","));
                }


                //業務及業助
                foreach (var item in productInfo.ItemList)
                {
                    if (item.SalesMail != null)
                    {
                        foreach (string sm in item.SalesMail)
                        {
                            if (!mailList.Contains(sm))
                            {
                                mailList.Add(sm);
                            }
                        }
                    }
                }
                //全國宅配特助群組
                //取消通知業助
                //mailList.Add(config.OadEmail);

                //Elin
                string[] list = config.VbsShortageMail.Split(",");
                foreach(string l in list)
                {
                    if (!mailList.Contains(l))
                    {
                        mailList.Add(l);
                    }
                }

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + new JsonSerializer().Serialize(mailList));

                #region Send mail
                if (mailList.Count() > 0)
                {
                    //string mailTo = string.Join(",", mailList);
                    var template = TemplateFactory.Instance().GetTemplate<ShortageNotificationToSellerMail>();
                    template.HttpsSiteUrl = config.SSLSiteUrl;
                    template.SiteUrl = config.SiteUrl;
                    template.ProductInfo = productInfo;
                    template.SellerName = seller.SellerName;
                    template.IsWms = isWms;
                    try
                    {
                        foreach(string mail in mailList)
                        {
                            MailMessage msg = new MailMessage();
                            msg.Subject = string.Format("17Life【{0}】商品庫存量不足通知({1})", seller.SellerName, isWms ? "24H到貨" : "一般宅配");
                            msg.From = new MailAddress(config.ServiceEmail, config.ServiceEmail);
                            msg.To.Add(mail);
                            msg.Body = template.ToString();
                            msg.IsBodyHtml = true;
                            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                    }
                }
                #endregion
            }
        }
    }
}

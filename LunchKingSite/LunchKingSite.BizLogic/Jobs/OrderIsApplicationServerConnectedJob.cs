﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Jobs
{
    class OrderIsApplicationServerConnectedJob : IJob
    {
        public void Execute(JobSetting js)
        {
            ISPFacade.IspMakeOrder(null);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Facade;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System.Net.Mail;

namespace LunchKingSite.BizLogic.Jobs
{
    public class PponDealChore : IJob
    {
        private static IPponProvider _pponProv;
        private static ISellerProvider _sellerProv;
        private static IOrderProvider _orderProv;
        private static ISysConfProvider _config;
        private static ILog log = LogManager.GetLogger(typeof(PponDealChore));
        private static PponDealChore _theJob;

        static PponDealChore()
        {
            _theJob = new PponDealChore();
        }

        public static PponDealChore Instance()
        {
            return _theJob;
        }

        private PponDealChore()
        {
            _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sellerProv = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _orderProv = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(_config.AdminEmail);
                msg.Subject = Environment.MachineName + ": [系統]PponDealChore Start Running";
                msg.From = new MailAddress(_config.AdminEmail);
                msg.Body = "PponDealChore is running.";
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                log.Error("PponDealChore running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }

            try
            {
                // getting deals that will begin within next day
                ViewPponDealCollection deals = _pponProv.ViewPponDealGetListPaging(1, -1, ViewPponDeal.Columns.BusinessHourOrderTimeE,
                    ViewPponDeal.Columns.BusinessHourOrderTimeS + " between " + DateTime.Today.ToString("yyyy/MM/dd HH:mm") + " and " + DateTime.Today.AddDays(1).ToString("yyyy/MM/dd HH:mm"));

                // nothihng to do
                if (deals == null || deals.Count <= 0) return;

                StartDeal(from x in deals where x.GetDealStage() == PponDealStage.Created select x);
            }
            catch (Exception ex)
            {
                log.Error("PponDealChore Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

        private void StartDeal(IEnumerable<ViewPponDeal> deals)
        {
            // initializing the upcoming deals
            List<Guid> processed = new List<Guid>();
            // TODO: this is to take care of current issue that ViewPponDeal will return multiple entries of the same deal, only with different parent_city.
            // TODO: once ViewPponDeal is detached with delivery_city, these code should be removed.
            foreach (ViewPponDeal d in deals.Where(d => !processed.Contains(d.BusinessHourGuid)))
            {
                processed.Add(d.BusinessHourGuid);
                MakeGroupOrder(_config.ServiceEmail, _config.ServiceName, d.BusinessHourGuid, DateTime.Now, d.BusinessHourOrderTimeE);
            }
        }

        private Guid MakeGroupOrder(string memberEmail, string memberName, Guid bizHourGuid, DateTime createTime, DateTime closeTime)
        {
            GroupOrder go = new GroupOrder();

            ViewSellerCityBizHour vscbh = _sellerProv.ViewSellerCityBizHourGet(ViewSellerCityBizHour.Columns.BusinessHourGuid, bizHourGuid);

            if (!vscbh.IsLoaded)
                return Guid.Empty;

            Order ord = FillOrderInfoStage1(memberEmail, memberName, vscbh);
            ord.OrderStatus = (int)OrderStatus.GroupOrder;

            go.OrderGuid = ord.Guid;
            go.BusinessHourGuid = bizHourGuid;
            go.MemberUserName = memberEmail;
            go.CreateTime = createTime;
            go.CloseTime = closeTime;
            go.Guid = Helper.GetNewGuid(go);
            go.Status = (int)GroupOrderStatus.NotNotified;

            return go.Guid;
        }

        private Order FillOrderInfoStage1(string memberEmail, string memberName, ViewSellerCityBizHour vscb)
        {
            Order outOrd = new Order();
            outOrd.Guid = Helper.GetNewGuid(outOrd);
            outOrd.SellerGuid = vscb.SellerGuid;
            outOrd.SellerName = vscb.SellerName;
            outOrd.UserId =MemberFacade.GetUniqueId(memberEmail, true);
            outOrd.MemberName = memberName;
            outOrd.Subtotal = outOrd.Total = 0M;
            outOrd.CreateId = memberEmail;
            if (outOrd.CreateId.Length > Order.CreateIdColumn.MaxLength)
                outOrd.CreateId = outOrd.CreateId.Substring(0, Order.CreateIdColumn.MaxLength);
            outOrd.CreateTime = DateTime.Now;

            return outOrd;
        }
    }
}

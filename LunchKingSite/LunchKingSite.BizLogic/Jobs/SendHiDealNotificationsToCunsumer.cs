﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Enumeration;
using SubSonic;

namespace LunchKingSite.BizLogic.Jobs
{
    public class SendHiDealNotificationsToCunsumer : IJob
    {
        public void Execute(JobSetting js)
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();

            #region 寄送優惠使用提醒

            DataTable toUseProducts = GetUsageNotificationProductInfo();

            foreach (DataRow productRow in toUseProducts.Rows)
            {
                DataRow prodRow = productRow;
                int prodId = int.Parse(prodRow["ProductId"].ToString());
                DataTable members = GetMemberListByProdId(prodId);

                IEnumerable<DataRow> enumMembers = members.AsEnumerable();
                Action<DataRow> sendStartUseReminder =
                    (memberRow) =>
                    {
                        StartUseReminderInformation information = new StartUseReminderInformation
                        {
                            BuyerName = memberRow["MemberName"].ToString(),
                            ExpireDate = ((DateTime)prodRow["ExpireDate"]).ToString("yyyy/MM/dd"),
                            PurchasedItemName = prodRow["PurchasedItemName"].ToString(),
                            OrderDetailUrl = string.Format("{0}/", config.SiteUrl),
                            HDmailHeaderUrl = config.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Header.png",
                            HDmailFooterUrl = config.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Footer.png"
                        };

                        string address = memberRow["Email"].ToString();
                        HiDealMailFacade.SendStartUseReminder(address, information);
                    };

                Parallel.ForEach(enumMembers, sendStartUseReminder);
            }

            #endregion
            
            #region 寄送優惠到期提醒

            DataTable almostExpireProducts = GetExpireNotificationProductInfo();

            foreach(DataRow expireProductRow in almostExpireProducts.Rows)
            {
                DataRow expireProdRow = expireProductRow;
                int prodId = int.Parse(expireProdRow["ProductId"].ToString());
                DataTable members = GetMemberListByProdId(prodId);

                IEnumerable<DataRow> enumMembers = members.AsEnumerable();
                Action<DataRow> sendExpireReminder =
                    (memberRow) =>
                    {
                        ExpireReminderInformation information = new ExpireReminderInformation
                                                                    {
                                                                        BuyerName = memberRow["MemberName"].ToString(),
                                                                        ExpireDate = ((DateTime)expireProdRow["ExpireDate"]).ToString("yyyy/MM/dd"),
                                                                        PurchasedItemName = expireProdRow["PurchasedItemName"].ToString(),
                                                                        OrderDetailUrl = string.Format("{0}/", config.SiteUrl),
                                                                        HDmailHeaderUrl = config.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Header.png",
                                                                        HDmailFooterUrl = config.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Footer.png"
                                                                    };
                        string address = memberRow["Email"].ToString();
                        HiDealMailFacade.SendExpireReminder(address, information);
                    };

                Parallel.ForEach(enumMembers, sendExpireReminder);
            }

            #endregion
        }
        
        /// <summary>
        /// 取得需要送使用提醒通知信的產品相關訊息.
        /// </summary>
        /// <returns></returns>
        private DataTable GetUsageNotificationProductInfo()
        {
            //優惠使用提醒規則:
            //使用期間為一個月以內：開始使用一周後寄發
            //使用期間為一個月以上，三個月以內的好康：開始使用三周寄發
            //三個月(包含)以上：開始使用後隔三周(三周、六周、九周依此類推)，一直到結束前一個月寄發

            string sql = @"
                SELECT 
                    deal.name + ' ' + prod.name AS PurchasedItemName, 
                    prod.use_end_time AS ExpireDate, 
                    prod.id AS ProductId
                FROM hi_deal_product prod
                INNER JOIN hi_deal_deal deal
                    ON prod.deal_id = deal.id
                WHERE 
                (use_end_time < DATEADD(MM, 1, use_start_time)
                AND GETDATE() BETWEEN use_start_time AND use_end_time
                AND use_start_time = DATEADD(DD, -7, @d) )
                OR
                (use_end_time between DATEADD(MM, 1, use_start_time) AND DATEADD(MM, 3, use_start_time)
                AND GETDATE() BETWEEN use_start_time AND use_end_time
                AND use_start_time = DATEADD(DD, -21, @d) )
                OR
                (use_end_time >= DATEADD(MM, 3, use_start_time)
                AND GETDATE() BETWEEN use_start_time AND use_end_time
                AND CAST(@d - use_start_time AS int) % 21 = 0
                AND use_start_time != @d
                AND use_end_time > DATEADD(MM, 1, @d) )";

            string today = DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss.000");
            IDataReader reader = new InlineQuery().ExecuteReader(sql, today);
            
            DataTable dt = new DataTable();
            using (reader)
            {
                dt.Load(reader);
            }
            return dt;
        }

        /// <summary>
        /// 非商品, 已核銷, 未退貨
        /// </summary>
        /// <param name="prodId"></param>
        /// <returns></returns>
        private DataTable GetMemberListByProdId(int prodId)
        {
            //名單規則: 排除(1) 已使用的  (2) 已退貨的  (3) 商品類
            string sql = @"              
                SELECT 
                    MIN(m.last_name + m.first_name) AS MemberName,
                    m.user_name AS Email
                FROM hi_deal_product prod
                INNER JOIN hi_deal_order_detail od
                    ON prod.id = od.product_id
                INNER JOIN hi_deal_order o
                    ON od.hi_deal_order_guid = o.guid
                INNER JOIN member m
                    ON o.user_id = m.unique_id  
                INNER JOIN cash_trust_log ctlog
                    ON od.guid = ctlog.order_detail_guid
                WHERE prod.id = @prodId
                    AND od.product_type <> 2  --運費
                    AND ctlog.status IN (0,1,5) 
                GROUP BY m.user_name";

            IDataReader reader = new InlineQuery().ExecuteReader(sql, prodId);

            DataTable dt = new DataTable();
            using (reader)
            {
                dt.Load(reader);
            }
            return dt;
        }

        /// <summary>
        /// 取得需要送使用到期通知信的產品相關訊息.
        /// </summary>
        private DataTable GetExpireNotificationProductInfo()
        {
            //優惠到期提醒規則:
            //使用期間為一個月以內：結束前一周寄發
            //使用期間為一個月以上，三個月以內的好康：開始使用六周寄發
            //三個月(包含)以上：結束前一個月寄發

            string sql = @"
                SELECT 
                    deal.name + ' ' + prod.name AS PurchasedItemName, 
                    prod.use_end_time AS ExpireDate, 
                    prod.id AS ProductId
                FROM hi_deal_product prod
                INNER JOIN hi_deal_deal deal
                    ON prod.deal_id = deal.id
                WHERE 
                (use_end_time < DATEADD(MM, 1, use_start_time)
                AND GETDATE() BETWEEN use_start_time AND use_end_time
                AND use_end_time = DATEADD(DD, 7, @d) )
                OR
                (use_end_time between DATEADD(MM, 1, use_start_time) AND DATEADD(MM, 3, use_start_time)
                AND GETDATE() BETWEEN use_start_time AND use_end_time
                AND use_end_time = DATEADD(DD, 42, @d) )
                OR
                (use_end_time >= DATEADD(MM, 3, use_start_time)
                AND GETDATE() BETWEEN use_start_time AND use_end_time                
                AND @d = DATEADD(MM, -1, use_end_time) )";

            string today = DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss.000");
            IDataReader reader = new InlineQuery().ExecuteReader(sql, today);

            DataTable dt = new DataTable();
            using (reader)
            {
                dt.Load(reader);
            }
            return dt;
        }
    }
}

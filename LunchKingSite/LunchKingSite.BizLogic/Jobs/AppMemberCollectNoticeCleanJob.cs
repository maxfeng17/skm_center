﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System.Net.Mail;

namespace LunchKingSite.BizLogic.Jobs
{
    class AppMemberCollectNoticeCleanJob : IJob
    {
        private static ILog log = LogManager.GetLogger(typeof(PponDealChore));
        private static AppMemberCollectNoticeCleanJob _theJob;

        static AppMemberCollectNoticeCleanJob()
        {
            _theJob = new AppMemberCollectNoticeCleanJob();
        }

        public static AppMemberCollectNoticeCleanJob Instance()
        {
            return _theJob;
        }

        private AppMemberCollectNoticeCleanJob()
        {
        }

        public void Execute(JobSetting js)
        {
            //檢查所有refresh_token，過期的，將裝置與user關連砍斷
            OAuthFacade.MemberCollectNoticeClear();
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace LunchKingSite.BizLogic.Jobs
{
    public class ShipStatusUpdate : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(ShipStatusUpdate));
        private static IMemberProvider mp;
        private static IOrderProvider op;
        private static IPponProvider pp;
        private static ISysConfProvider config;
        private static IVerificationProvider vp;
        private delegate void JobDelegate(ref List<ExecuteResult> re, ref List<ExecuteResult> error);

        private static TimeingMethod _timeingMethod;


        static ShipStatusUpdate()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            config = ProviderFactory.Instance().GetConfig();
            _timeingMethod = new TimeingMethod();
        }

        public void Execute(JobSetting js)
        {
            List<ExecuteResult> resultInfos = new List<ExecuteResult>();
            List<ExecuteResult> errorInfos = new List<ExecuteResult>();

            JobDelegate j;
            string mode = js.Parameters["mode"] != null ? js.Parameters["mode"] : "";
            //執行要按照順序
            switch (mode)
            {
                case "BSCreateDate":
                    j = UpdateComboMainBalanceSheetCreateDate;
                    break;
                case "PartiallyPaymentDate":
                    j = UpdatePartiallyPaymentDate;
                    break;
                default:
                    j = UpdateCashTrustLogStatus;
                    j += UpdateShippedDateHomeDelivery;
                    j += UpdateComboMainShippedDate;
                    j += UpdateFinalBalanceSheetDate;
                    j += UpdateComboMainFinalBalanceSheetDate;
                    break;
            }

            try
            {
                j(ref resultInfos, ref errorInfos);
            }
            catch (Exception ex)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = "執行JOB錯誤",
                    IndentityId = "執行錯誤：",
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };

                SendResultMail(new string[] { config.AdminEmail },
                    new List<ExecuteResult> { errMsg }, "Ship Status Update Error");
                throw;
            }


            SendResultMail(new string[] { config.AdminEmail }, 
                errorInfos, "Ship Status Update Error");
            SendResultMail(new string[] { config.AdminEmail }, 
                resultInfos);
        }

        private void UpdateComboMainBalanceSheetCreateDate(ref List<ExecuteResult> resultInfos, ref List<ExecuteResult> errorInfos)
        {
            //基準時間 為今天00:00:00
            DateTime now = DateTime.Now.Date;
            #region 更新多檔次主檔的對帳單開立日

            string purpose = "更新多檔次主檔的對帳單開立日";

            Guid productGuid = new Guid();
            DealAccounting da = new DealAccounting();
            try
            {
                _timeingMethod.Start();

                //找到多檔次的母檔ID(出貨日為null)
                List<DataRow> dealList = op.GetProductMainComboGuidAndBalanceSheetCreateDate(now).AsEnumerable().ToList();

                _timeingMethod.Stop(purpose, "DB running", resultInfos);


                if (dealList.Any())
                {
                    _timeingMethod.Start();

                    Dictionary<Guid, DealAccounting> daList = pp.DealAccountingGet(
                        dealList.Select(x => x.Field<Guid>("product_guid")).ToList())
                        .ToDictionary(x => x.BusinessHourGuid, x => x);

                    foreach (DataRow row in dealList)
                    {
                        DateTime bsCreateDate;
                        if (!string.IsNullOrEmpty(row["balance_sheet_create_date"].ToString())
                            && DateTime.TryParse(row["balance_sheet_create_date"].ToString(), out bsCreateDate)
                            && !string.IsNullOrEmpty(row["product_guid"].ToString())
                            && Guid.TryParse(row["product_guid"].ToString(), out productGuid))
                        {
                            da = daList.ContainsKey(productGuid) ? daList[productGuid] : null;
                            if (da != null)
                            {
                                try
                                {
                                    da.BalanceSheetCreateDate = bsCreateDate;
                                    pp.DealAccountingSet(da);

                                    CommonFacade.AddAudit(productGuid, AuditType.DealAccounting, string.Format("Job:ShipStatusUpdate-{0} 更新對帳單開立日為：{1}", purpose, bsCreateDate.ToShortDateString()), "sys", false);
                                }
                                catch (Exception ex)
                                {
                                    var errMsg = new ExecuteResult
                                    {
                                        title = purpose,
                                        IndentityId = "BusinessHourGuid：" + productGuid.ToString(),
                                        IsSuccess = false,
                                        Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                                    };
                                    resultInfos.Add(errMsg);
                                }
                            }
                        }
                    }
                    _timeingMethod.Stop(purpose, "AP running", resultInfos);
                }
            }
            catch (Exception ex)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = purpose,
                    IndentityId = "更新多檔次主檔的對帳單開立日",
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };
                errorInfos.Add(errMsg);
            }

            #endregion
        }

        private void UpdatePartiallyPaymentDate(ref List<ExecuteResult> resultInfos, ref List<ExecuteResult> errorInfos)
        {
            #region 更新暫付七成付款日

            string purpose = "更新暫付七成付款日";

            try
            {
                DateTime lastweek = DateTime.Now.Date.AddDays(-7);
                //找到檔次購買數量(total)及退貨數量(returntotal)
                //計算是否暫付七成（returntotal/total <0.3），正常檔次  可暫付七成
                //依照母檔檢查所有子檔是否須更新暫付七成日
                //異常檔次發信      正常檔次更新暫付七成付款日
                _timeingMethod.Start();

                var dealList = op.TempPaymentGetTable(Helper.GetDateOfWeek(lastweek, DayOfWeek.Monday).ToString("yyyy/MM/dd"), Helper.GetDateOfWeek(DateTime.Now.Date, DayOfWeek.Sunday).ToString("yyyy/MM/dd")).AsEnumerable().ToList();

                List<VendorDealSalesCount> dealSalesSummarys = new List<VendorDealSalesCount>();
                DataTable dealReturning = new DataTable();

                if (dealList.Any())
                {
                    dealSalesSummarys = vp.GetPponToHouseDealSummary(dealList.Select(x => x.Field<Guid>("bid")).ToList());
                    dealReturning = op.ReturningCountGetTable(dealList.Select(x => x.Field<Guid>("bid")).ToList());
                }

                _timeingMethod.Stop(purpose, "DB running", resultInfos);

                if (dealSalesSummarys.Any())
                {
                    _timeingMethod.Start();

                    #region 計算所有檔次數量
                    var tempTotalQ = from x in dealList
                                     join y in dealSalesSummarys
                                     on x.Field<Guid>("bid") equals y.MerchandiseGuid
                                     select new ExecuteResultByTempPayment
                                     {
                                         Bid = x.Field<Guid>("bid"),
                                         MainGuid = x.Field<Guid>("mid"),
                                         Total = x.Field<int>("slug"),
                                         TotalReturn = y.ReturnedCount
                                     };
                    #endregion
                    #region 計算退貨數量，正在退
                    var tempReturnQ = from x in dealReturning.AsEnumerable()
                                      select new ExecuteResultByTempPayment
                                      {
                                          Bid = x.Field<Guid>("bid"),
                                          TotalReturn = x.Field<int>("returning_count")
                                      };
                    #endregion

                    var tempTotal = tempTotalQ as ExecuteResultByTempPayment[] ?? tempTotalQ.ToArray();
                    var tempReturn = tempReturnQ.ToArray();

                    #region 計算是否需要更新暫付七成付款日
                    var tempResultQ = from total in tempTotal
                                     //退貨份數為正在退及退貨完成
                                     let countReturn = total.TotalReturn +
                                         (tempReturn.Where(x => x.Bid == total.Bid)
                                                    .Select(x => x.TotalReturn).FirstOrDefault() ?? 0)
                                     let payment = total.Total == 0 ||
                                                   Math.Round(((double)countReturn / (double)total.Total), 2, MidpointRounding.AwayFromZero) < 0.3
                                     select new ExecuteResultByTempPayment
                                     {
                                         Bid = total.Bid,
                                         MainGuid = total.MainGuid,
                                         UniqueId = total.UniqueId,
                                         Name = total.Name,
                                         Total = total.Total,
                                         TotalReturn = countReturn,
                                         PaymantSuccess = payment
                                     };
                    #endregion

                    var tempResult = tempResultQ as ExecuteResultByTempPayment[] ?? tempResultQ.ToArray();

                    //依照母檔檔次決定更新暫付七成付款日
                    //母檔下的一檔子檔正常   即更新暫付七成付款日
                    var tempMain = tempResult.GroupBy(x => x.MainGuid);

                    //將需要更新檔次多檔次及單檔整合
                    //作為需要更新DB檔次的條件

                    var queryGuids = tempResult.GroupBy(x => x.Bid).Select(x => x.Key).ToList();
                    queryGuids.AddRange(tempResult.GroupBy(x => x.MainGuid).Select(x => x.Key).ToList());
                    //從DB撈資料 準備更新
                    var daList = pp.DealAccountingGet(queryGuids).ToDictionary(x => x.BusinessHourGuid, x => x);
                    foreach (var items in tempMain)
                    {
                        var tmpMainGuid = items.Key;

                        //單/多檔次
                        if (tempResult.Any(x => x.MainGuid == x.Bid && x.Bid == tmpMainGuid))
                        {
                            //更新檔次
                            if (tempResult.Any(x => x.MainGuid == tmpMainGuid && x.PaymantSuccess))
                            {
                                UpdateTemporaryPayment(daList, tmpMainGuid, purpose, resultInfos, errorInfos);
                            }
                            else
                            {
                                UpdatePartialFail(tmpMainGuid, "退換貨異常", resultInfos, errorInfos);
                            }
                        }
                        else
                        {
                            //更新母檔
                            if (tempResult.Any(x => x.MainGuid == tmpMainGuid && x.PaymantSuccess))
                            {
                                UpdateTemporaryPayment(daList, tmpMainGuid, purpose, resultInfos, errorInfos);
                            }
                            else
                            {
                                UpdatePartialFail(tmpMainGuid, "退換貨異常", resultInfos, errorInfos);
                            }

                            //更新子檔
                            foreach (var item in tempResult.Where(x => x.MainGuid == tmpMainGuid))
                            {
                                if (item.PaymantSuccess)
                                {
                                    //多檔次BY母檔搜尋
                                    //可能子檔沒有成檔所以排除
                                    if (daList[item.Bid].ShippedDate != null)
                                    {
                                        UpdateTemporaryPayment(daList, item.Bid, purpose, resultInfos, errorInfos);
                                    }
                                }
                                else
                                {
                                    UpdatePartialFail(item.Bid, "退換貨異常", resultInfos, errorInfos);
                                }
                            }
                        }
                    }

                    _timeingMethod.Stop(purpose, "AP running", resultInfos);
                }
            }
            catch (Exception ex)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = purpose,
                    IndentityId = "抓取Ppon檔次 deal_accounting 暫付七成付款日",
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };
                errorInfos.Add(errMsg);
            }

            #endregion
        }

        private void UpdateFinalBalanceSheetDate(ref List<ExecuteResult> resultInfos, ref List<ExecuteResult> errorInfos)
        {
            //基準時間 為今天00:00:00
            DateTime now = DateTime.Now.Date;
            DateTime lastMonth = new DateTime(2014, 8, 1);

            #region 更新檔次的已核對對帳單

            string purpose = "更新檔次的已核對對帳單";

            Guid productGuid = new Guid();
            DealAccounting da = new DealAccounting();
            try
            {
                _timeingMethod.Start();

                //找到多檔次的母檔ID(出貨日為null) 
                List<DataRow> dealList = op.ProductProgressStatusGetTable(lastMonth.ToString("yyyy/MM/dd"), now.ToString("yyyy/MM/dd"), true).AsEnumerable().ToList();

                _timeingMethod.Stop(purpose, "DB running", resultInfos);

                if (dealList.Any())
                {
                    _timeingMethod.Start();

                    Dictionary<Guid, DealAccounting> daList = pp.DealAccountingGet(
                        dealList.Select(x => x.Field<Guid>("GUID")).ToList())
                        .ToDictionary(x => x.BusinessHourGuid, x => x);
                    foreach (DataRow row in dealList)
                    {
                        if (!string.IsNullOrEmpty(row["GUID"].ToString())
                            && Guid.TryParse(row["GUID"].ToString(), out productGuid))
                        {
                            da = daList.ContainsKey(productGuid) ? daList[productGuid] : null;
                            if (da != null)
                            {
                                try
                                {
                                    da.FinalBalanceSheetDate = now;
                                    pp.DealAccountingSet(da);

                                    CommonFacade.AddAudit(productGuid, AuditType.DealAccounting, string.Format("Job:ShipStatusUpdate-{0} 更新已核對對帳單為：{1}", purpose, now.ToShortDateString()), "sys", false);
                                }
                                catch (Exception ex)
                                {
                                    ExecuteResult errMsg = new ExecuteResult
                                    {
                                        title = purpose,
                                        IndentityId = "BusinessHourGuid：" + productGuid.ToString(),
                                        IsSuccess = false,
                                        Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                                    };
                                    resultInfos.Add(errMsg);
                                }
                            }
                        }
                    }

                    _timeingMethod.Stop(purpose, "AP running", resultInfos);
                }
            }
            catch (Exception ex)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = purpose,
                    IndentityId = "更新檔次的已核對對帳單",
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };
                errorInfos.Add(errMsg);
            }

            #endregion
        }

        private void UpdateComboMainFinalBalanceSheetDate(ref List<ExecuteResult> resultInfos, ref List<ExecuteResult> errorInfos)
        {
            //基準時間 為今天00:00:00
            DateTime now = DateTime.Now.Date;
            #region 更新多檔次主檔的已核對對帳單

            string purpose = "更新多檔次主檔的已核對對帳單";

            Guid productGuid = new Guid();
            DealAccounting da = new DealAccounting();
            try
            {
                _timeingMethod.Start();

                //找到多檔次的母檔ID(出貨日為null)
                List<DataRow> dealList = op.GetProductMainComboGuidAndFinalBalanceSheetDate(now).AsEnumerable().ToList();

                _timeingMethod.Stop(purpose, "DB running", resultInfos);


                if (dealList.Any())
                {
                    _timeingMethod.Start();

                    Dictionary<Guid, DealAccounting> daList = pp.DealAccountingGet(
                        dealList.Select(x => x.Field<Guid>("product_guid")).ToList())
                        .ToDictionary(x => x.BusinessHourGuid, x => x);

                    foreach (DataRow row in dealList)
                    {
                        DateTime finalBalanceSheetDate;
                        if (!string.IsNullOrEmpty(row["final_balance_sheet_date"].ToString())
                            && DateTime.TryParse(row["final_balance_sheet_date"].ToString(), out finalBalanceSheetDate)
                            && !string.IsNullOrEmpty(row["product_guid"].ToString())
                            && Guid.TryParse(row["product_guid"].ToString(), out productGuid))
                        {
                            da = daList.ContainsKey(productGuid) ? daList[productGuid] : null;
                            if (da != null)
                            {
                                try
                                {
                                    da.FinalBalanceSheetDate = finalBalanceSheetDate;
                                    pp.DealAccountingSet(da);

                                    CommonFacade.AddAudit(productGuid, AuditType.DealAccounting, string.Format("Job:ShipStatusUpdate-{0} 更新已核對對帳單為：{1}", purpose, finalBalanceSheetDate.ToShortDateString()), "sys", false);
                                }
                                catch (Exception ex)
                                {
                                    var errMsg = new ExecuteResult
                                    {
                                        title = purpose,
                                        IndentityId = "BusinessHourGuid：" + productGuid.ToString(),
                                        IsSuccess = false,
                                        Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                                    };
                                    resultInfos.Add(errMsg);
                                }
                            }
                        }
                    }
                    _timeingMethod.Stop(purpose, "AP running", resultInfos);
                }
            }
            catch (Exception ex)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = purpose,
                    IndentityId = "更新多檔次主檔的已核對對帳單",
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };
                errorInfos.Add(errMsg);
            }

            #endregion
        }

        private void UpdateComboMainShippedDate(ref List<ExecuteResult> resultInfos, ref List<ExecuteResult> errorInfos)
        {
            //基準時間 為今天00:00:00
            DateTime now = DateTime.Now.Date;
            #region 更新多檔次主檔的出貨回覆日

            string purpose = "更新多檔次主檔的出貨回覆日";

            Guid productGuid = new Guid();
            DealAccounting da = new DealAccounting();
            try
            {
                _timeingMethod.Start();

                //找到多檔次的母檔ID(出貨日為null)
                List<DataRow> dealList = op.GetProductMainComboGuidAndMaxShipTime(now).AsEnumerable().ToList();
                _timeingMethod.Stop(purpose, "DB running", resultInfos);
                if (dealList.Any())
                {
                    _timeingMethod.Start();

                    Dictionary<Guid, DealAccounting> daList = pp.DealAccountingGet(
                        dealList.Select(x => x.Field<Guid>("product_guid")).ToList())
                        .ToDictionary(x => x.BusinessHourGuid, x => x);
                    //resultInfos = new List<ExecuteResult>();

                    foreach (DataRow row in dealList)
                    {
                        DateTime shippedDate;
                        if (!string.IsNullOrEmpty(row["max_shipped_date"].ToString())
                            && DateTime.TryParse(row["max_shipped_date"].ToString(), out shippedDate)
                            && !string.IsNullOrEmpty(row["product_guid"].ToString())
                            && Guid.TryParse(row["product_guid"].ToString(), out productGuid))
                        {
                            //子檔若有未結檔的檔次，母檔不應該押上shipped_date，否則無法出貨
                            Guid bid = Guid.Empty;
                            Guid.TryParse(row["product_guid"].ToString(), out bid);
                            var comboDeals = Component.ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(bid);
                            var comboDeal = comboDeals.Where(x => x.BusinessHourDeliverTimeE >= DateTime.Now && x.Slug == null).FirstOrDefault();
                            if(comboDeal != null)
                            {
                                continue;
                            }

                            shippedDate = GetShippedDate(shippedDate, now);
                            da = daList.ContainsKey(productGuid) ? daList[productGuid] : null;
                            if (da != null)
                            {
                                try
                                {
                                    da.ShippedDate = shippedDate;
                                    pp.DealAccountingSet(da);

                                    CommonFacade.AddAudit(productGuid, AuditType.DealAccounting, string.Format("Job:ShipStatusUpdate-{0} 更新出貨回覆日為：{1}", purpose, shippedDate.ToShortDateString()), "sys", false);
                                }
                                catch (Exception ex)
                                {
                                    ExecuteResult errMsg = new ExecuteResult
                                    {
                                        title = purpose,
                                        IndentityId = "BusinessHourGuid：" + productGuid.ToString(),
                                        IsSuccess = false,
                                        Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                                    };
                                    resultInfos.Add(errMsg);
                                }
                            }
                        }
                    }
                    _timeingMethod.Stop(purpose, "AP running", resultInfos);
                }
            }
            catch (Exception ex)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = purpose,
                    IndentityId = "更新多檔次主檔的出貨回覆日",
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };
                errorInfos.Add(errMsg);
            }

            #endregion
        }
        
        private void UpdateShippedDateHomeDelivery(ref List<ExecuteResult> resultInfos, ref List<ExecuteResult> errorInfos)
        {
            //基準時間 為今天00:00:00
            DateTime now = DateTime.Now.Date;

            List<DataRow> dealList;
            DateTime shippedDate;

            #region 更新出貨回覆日(使用商家系統之宅配檔次)

            #region Ppon

            string purpose = "更新Ppon檔次 deal_accounting出貨回覆日";
            Guid productGuid = new Guid();
            DealAccounting da = new DealAccounting();
            try
            {
                _timeingMethod.Start();

                dealList = op.GetProductGuidAndMaxShipTime(OrderClassification.LkSite, now).AsEnumerable().ToList();
                Dictionary<Guid, DealAccounting> daList = pp.DealAccountingGet(
                    dealList.Select(x => x.Field<Guid>("product_guid")).ToList())
                    .ToDictionary(x => x.BusinessHourGuid, x => x);
                //resultInfos = new List<ExecuteResult>();
                _timeingMethod.Stop(purpose, "DB running", resultInfos);
                _timeingMethod.Start();

                foreach (DataRow row in dealList)
                {
                    
                    if (!string.IsNullOrEmpty(row["product_guid"].ToString())
                        && Guid.TryParse(row["product_guid"].ToString(), out productGuid))
                    {

                        shippedDate = op.ViewOrderShipListGetListByProductGuid(productGuid, OrderClassification.LkSite, OrderShipType.Normal).Max(x => x.ShipTime) ?? DateTime.MinValue;
                        if (shippedDate != DateTime.MinValue)
                        {
                            shippedDate = GetShippedDate(shippedDate, now);
                            da = daList.ContainsKey(productGuid) ? daList[productGuid] : null;
                            if (da != null)
                            {
                                try
                                {
                                    da.ShippedDate = shippedDate;
                                    pp.DealAccountingSet(da);

                                    CommonFacade.AddAudit(productGuid, AuditType.DealAccounting, string.Format("Job:ShipStatusUpdate-{0} 更新出貨回覆日為：{1}", purpose, shippedDate.ToShortDateString()), "sys", false);
                                }
                                catch (Exception ex)
                                {
                                    ExecuteResult errMsg = new ExecuteResult
                                    {
                                        title = purpose,
                                        IndentityId = "BusinessHourGuid：" + productGuid.ToString(),
                                        IsSuccess = false,
                                        Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                                    };
                                    resultInfos.Add(errMsg);
                                }
                            }
                        }
                        
                    }
                }

                _timeingMethod.Stop(purpose, "AP running", resultInfos);
            }
            catch (Exception ex)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = purpose,
                    IndentityId = "更新Ppon檔次 deal_accounting出貨回覆日",
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };
                errorInfos.Add(errMsg);
            }

            #endregion Ppon            

            #endregion 更新出貨回覆日(使用商家系統之宅配檔次)
        }

        private void UpdateCashTrustLogStatus(ref List<ExecuteResult> resultInfos, ref List<ExecuteResult> errorInfos)
        {
            //基準時間 為今天00:00:00
            DateTime now = DateTime.Now.Date;

            #region 更新cash_trust_log 出貨狀態

            #region Ppon

            string purpose = "更新Ppon檔次 cash_trust_log出貨狀態";
            //抓取出貨日期<今天00:00:00且狀態為未出貨關聯該訂單之cash_trust_log 
            try
            {
                _timeingMethod.Start();
                CashTrustLogCollection ctls = mp.CashTrustLogGetUnShipOrderListByOrderClassification(OrderClassification.LkSite, now);
                _timeingMethod.Stop(purpose, "DB running", resultInfos);
                _timeingMethod.Start();
                //更新cash_trust_log status為已出貨
                foreach (CashTrustLog ctl in ctls)
                {
                    try
                    {
                        ctl.Status = (int)TrustStatus.Verified; //已出貨視為已核銷
                        ctl.ModifyTime = DateTime.Now;

                        CashTrustStatusLog ctsl = new CashTrustStatusLog();
                        ctsl.TrustId = ctl.TrustId;
                        ctsl.TrustProvider = ctl.TrustProvider;
                        ctsl.Amount = ctl.Scash + ctl.CreditCard;
                        ctsl.Status = ctl.Status;
                        ctsl.ModifyTime = ctl.ModifyTime;
                        ctsl.CreateId = "system";

                        //更新cash_trust_status_log
                        if (mp.CashTrustLogSet(ctl))
                        {
                            mp.CashTrustStatusLogSet(ctsl);
                        }
                    }
                    catch (Exception ex)
                    {
                        ExecuteResult errMsg = new ExecuteResult
                        {
                            title = purpose,
                            IndentityId = "TrustId：" + ctl.TrustId.ToString(),
                            IsSuccess = false,
                            Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                        };
                        errorInfos.Add(errMsg);
                    }
                }
                _timeingMethod.Stop(purpose, "AP running", resultInfos);

                resultInfos.Add(new ExecuteResult
                {
                    title = purpose,
                    IndentityId = "",
                    IsSuccess = true,
                    Message = "done"
                });
            }
            catch (Exception ex)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = purpose,
                    IndentityId = "更新Ppon檔次 cash_trust_log出貨狀態",
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };
                errorInfos.Add(errMsg);
            }

            

            #endregion Ppon           

            #endregion 更新cash_trust_log 出貨狀態
        }

        private void UpdateTemporaryPayment(Dictionary<Guid, DealAccounting> daList, Guid tmpGuid, string purpose, List<ExecuteResult> resultInfos, List<ExecuteResult> errorInfos)
        {
            try
            {
                var da = daList[tmpGuid];
                var updateDate = Helper.GetDateOfWeek(DateTime.Now.Date, DayOfWeek.Friday);
                da.PartiallyPaymentDate = updateDate;
                pp.DealAccountingSet(da);
                
                CommonFacade.AddAudit(tmpGuid, AuditType.DealAccounting, string.Format("Job:ShipStatusUpdate-{0} 更新暫付七成付款日：{1}", purpose, updateDate.ToShortDateString()), "sys", false);
            }
            catch (Exception ex)
            {
                var errMsg = new ExecuteResult
                {
                    title = purpose,
                    IndentityId = "BusinessHourGuid：" + tmpGuid,
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };
                errorInfos.Add(errMsg);
            }
        }

        private void UpdatePartialFail(Guid tmpGuid, string purpose, List<ExecuteResult> resultInfos, List<ExecuteResult> errorInfos)
        {
            try
            {
                GroupOrder god = op.GroupOrderGetByBid(tmpGuid);

                god.Status = (int)Helper.SetFlag(true, (long)god.Status, GroupOrderStatus.PartialFail);

                op.GroupOrderSet(god);

                CommonFacade.AddAudit(tmpGuid, AuditType.DealAccounting, string.Format("Job:ShipStatusUpdate-{0} ", purpose), "sys", false);
            }
            catch (Exception ex)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = purpose,
                    IndentityId = "BusinessHourGuid：" + tmpGuid,
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };
                errorInfos.Add(errMsg);
            }
        }

        private DateTime GetShippedDate(DateTime shippedDate, DateTime now)
        {
            if (DateTime.Compare(shippedDate.Date, now.Date) < 0)
            {
                shippedDate = now.AddDays(-1);
            }

            return shippedDate;
        }

        private void SendResultMail(string[] mailTos, List<ExecuteResult> resultInfos, string subject = "")
        {
            if (!resultInfos.Any())
            {
                return;
            }

            StringBuilder sb = new StringBuilder();

            foreach (var item in resultInfos.GroupBy(x => x.title))
            {
                sb.Append(string.Format("【{0}】 執行結果已記錄至Audit，執行時間:<br/>", item.Key));
                foreach (var detail in item)
                {
                    if (!detail.IsSuccess)
                    {
                        sb.Append("處理失敗，原因：");
                    }
                    sb.Append(string.Format("{0}{1}<br/>", detail.IndentityId, detail.Message));
                }
            }

            try
            {
                MailMessage msg = new MailMessage();
                foreach (string mailTo in mailTos)
                {
                    msg.To.Add(mailTo);
                }
                msg.Subject = string.Format("{0}： [系統]{1}", Environment.MachineName
                                                             , string.IsNullOrEmpty(subject)
                                                                ? "ShipStatusUpdate Start Running"
                                                                : subject);
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = sb.ToString();

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                logger.Error("ShipStatusUpdate running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

        private class TimeingMethod
        {
            private readonly Stopwatch _stopwatch;

            public TimeingMethod()
            {
                _stopwatch = new Stopwatch();
            }

            internal void Start()
            {
                _stopwatch.Reset();
                _stopwatch.Start();
            }

            internal void Stop(string p, string target, List<ExecuteResult> resultInfos)
            {
                resultInfos.Add(new ExecuteResult
                {
                    title = p,
                    IndentityId = "",
                    IsSuccess = true,
                    Message = string.Format("{0}：{1}", target, _stopwatch.Elapsed)
                });
            }
        }

        private class ExecuteResult
        {
            public string title { get; set; }
            public string IndentityId { get; set; }
            public bool IsSuccess { get; set; }
            public string Message { get; set; }
        }

        private class ExecuteResultByTempPayment
        {
            public Guid MainGuid { get; set; }
            public Guid Bid { get; set; }
            public int Total { get; set; }
            public int? TotalReturn { get; set; }
            public string Name { get; set; }
            public int UniqueId { get; set; }
            public bool PaymantSuccess { get; set; }
        }
    }
}

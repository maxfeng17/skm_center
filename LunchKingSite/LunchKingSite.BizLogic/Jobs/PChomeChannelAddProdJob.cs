﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Component;
using log4net;
using LunchKingSite.Core.Interface;
using System.Text;
using System.Net;
using System.IO;
using LunchKingSite.Core.Enumeration;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Models.PchomeChannel;
using System.Web.Hosting;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Drawing;
using LunchKingSite.Core.Models;

namespace LunchKingSite.BizLogic.Jobs
{
    public class PChomeChannelAddProdJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(PChomeChannelAddProdJob).Name);
        private static IOAuthProvider oauthp = ProviderFactory.Instance().GetProvider<IOAuthProvider>();
        private static IChannelProvider chp = ProviderFactory.Instance().GetProvider<IChannelProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        private static ISystemProvider sys = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        public static List<int> restaurantList = new List<int>();
        public static List<int> restaurantBuffetList = new List<int>();
        public static List<int> spaList = new List<int>();
        public static List<int> hotelList = new List<int>();
        public static List<int> ticketList = new List<int>();
        public static List<int> courseList = new List<int>();
        public void Execute(JobSetting js)
        {

            string mode = js.Parameters["mode"];

            #region 撈檔
            List<MultipleMainDealPreview> deals = new List<Component.MultipleMainDealPreview>();
            List<MultipleMainDealPreview> taipeifoolDeals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria((int)ChannelCategory.Food, PponCityGroup.DefaultPponCityGroup.TaipeiCity.ChannelAreaId, new List<int>());
            List<MultipleMainDealPreview> taoyuanFoolDeals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria((int)ChannelCategory.Food, PponCityGroup.DefaultPponCityGroup.Taoyuan.ChannelAreaId, new List<int>());
            List<MultipleMainDealPreview> hsinchuFoolDeals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria((int)ChannelCategory.Food, PponCityGroup.DefaultPponCityGroup.Hsinchu.ChannelAreaId, new List<int>());
            List<MultipleMainDealPreview> taichungFoolDeals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria((int)ChannelCategory.Food, PponCityGroup.DefaultPponCityGroup.Taichung.ChannelAreaId, new List<int>());
            List<MultipleMainDealPreview> tainanFoolDeals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria((int)ChannelCategory.Food, PponCityGroup.DefaultPponCityGroup.Tainan.ChannelAreaId, new List<int>());
            List<MultipleMainDealPreview> kaohsiungFoolDeals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria((int)ChannelCategory.Food, PponCityGroup.DefaultPponCityGroup.Kaohsiung.ChannelAreaId, new List<int>());

            List<MultipleMainDealPreview> travelDeals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria((int)ChannelCategory.Travel, null, new List<int>());
            List<MultipleMainDealPreview> beautyDeals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria((int)ChannelCategory.Beauty, null, new List<int>());
            //品生活與在地重複,不串
            //List<MultipleMainDealPreview> piinlifeDeals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(148, null, new List<int>());

            deals.AddRange(taipeifoolDeals);
            deals.AddRange(taoyuanFoolDeals);
            deals.AddRange(hsinchuFoolDeals);
            deals.AddRange(taichungFoolDeals);
            deals.AddRange(tainanFoolDeals);
            deals.AddRange(kaohsiungFoolDeals);

            deals.AddRange(travelDeals);
            deals.AddRange(beautyDeals);
            //deals.AddRange(piinlifeDeals);
            #endregion

            #region 排除不符合檔次
            //取最低毛利率的值
            OauthClient client = oauthp.ClientGetByName("PChome");
            var channelClientProperty = OAuthFacade.GetChannelClientProperty(client.AppId);

            List<Guid> excludedGuid = ChannelFacade.ChannelExcludedGuid(deals.Select(x => x.PponDeal).ToList(), channelClientProperty, new List<int>(), (int)AgentChannel.PChome);
            deals = ChannelFacade.ChannelComboDealBlackWhiteList(deals, null, null, null, (int)AgentChannel.PChome, (int)DeliveryType.ToShop, ref excludedGuid);
            deals = deals.Where(x => !excludedGuid.Contains(x.PponDeal.BusinessHourGuid)).Distinct().ToList();
            deals = deals.Where(x => (x.PponDeal.DeliveryType ?? 1) == (int)DeliveryType.ToShop).ToList();

            #endregion

            if (mode == "add")
            {
                #region 記錄撈出的檔次
                int totalDeals = 0;
                totalDeals = deals.Where(x => !x.PponDeal.ComboDelas.Any()).Select(x => x.PponDeal.BusinessHourGuid).Count();
                totalDeals = totalDeals + deals.Where(x => x.PponDeal.ComboDelas.Any()).SelectMany(x => x.PponDeal.ComboDelas).Select(x => x.BusinessHourGuid).Count();
                logger.Info("總共檔次" + totalDeals);
                logger.Info("單檔次：" + string.Join(Environment.NewLine, deals.Where(x => !x.PponDeal.ComboDelas.Any()).Select(x => x.PponDeal.BusinessHourGuid)));
                logger.Info("子檔次：" + string.Join(Environment.NewLine, deals.Where(x => x.PponDeal.ComboDelas.Any()).SelectMany(x => x.PponDeal.ComboDelas).Select(x => x.BusinessHourGuid)));

                if (excludedGuid.Any())
                {
                    logger.Info("排除檔次：" + string.Join(Environment.NewLine, excludedGuid));
                } 
                #endregion

                AddProd(deals);
            }
            else if (mode == "updateHour")
            {
                UpdateIsShelf(deals);
                UpdateIsVerify();
                UpdateIsShelf();
                UpdateIsArchive();
                UpdateStock(false);
            }
            else if (mode == "updateMin")
            {
                UpdateStock(true);
            }
        }

        /// <summary>
        /// 新增電子票券
        /// </summary>
        public static void AddProd(List<MultipleMainDealPreview> deals)
        {
            DateTime start = DateTime.Now;
            PchomeChannelProdCollection list = new PchomeChannelProdCollection();

            #region 先取得分類code
            SystemCodeCollection allDealType = sys.SystemCodeGetListByCodeGroup("DealType");
            int restaurant = allDealType.Where(x => x.CodeName == "餐廳" && !x.ParentCodeId.HasValue).FirstOrDefault().CodeId;
            int spa = allDealType.Where(x => x.CodeName == "SPA/醫美" && !x.ParentCodeId.HasValue).FirstOrDefault().CodeId;
            int hotel = allDealType.Where(x => x.CodeName == "住宿/門票" && !x.ParentCodeId.HasValue).FirstOrDefault().CodeId;
            int course = allDealType.Where(x => x.CodeName == "課程" && !x.ParentCodeId.HasValue).FirstOrDefault().CodeId;
            restaurantList = allDealType.Where(x => x.ParentCodeId == restaurant && !x.CodeName.Contains("餐廳_吃到飽")).Select(x => x.CodeId).ToList();
            restaurantBuffetList = allDealType.Where(x => x.ParentCodeId == restaurant && x.CodeName.Contains("餐廳_吃到飽")).Select(x => x.CodeId).ToList();
            spaList = allDealType.Where(x => x.ParentCodeId == spa).Select(x => x.CodeId).ToList();
            hotelList = allDealType.Where(x => x.ParentCodeId == hotel && x.CodeName.Contains("住宿")).Select(x => x.CodeId).ToList();
            ticketList = allDealType.Where(x => x.ParentCodeId == hotel && x.CodeName.Contains("門票")).Select(x => x.CodeId).ToList();
            courseList = allDealType.Where(x => x.ParentCodeId == course).Select(x => x.CodeId).ToList(); 
            #endregion

            //測試某檔
            if (!string.IsNullOrEmpty(cp.PChomeChannelTestBid))
            {
                deals = deals.Where(x => x.PponDeal.BusinessHourGuid.ToString() == cp.PChomeChannelTestBid.ToLower()).ToList();

                //7f95d215-9a68-41e3-8db3-bc864ab552c1
            }
            foreach (MultipleMainDealPreview main in deals)
            {
                if (list.Count > 100)
                {
                    //不要一次拋太多
                    break;
                }
                try
                {
                    if (main.PponDeal.ComboDelas.Any())
                    {
                        //子檔
                        foreach (ViewComboDeal sub in main.PponDeal.ComboDelas)
                        {
                            PchomeChannelProd deal = chp.PChomeChannelProdGetByBid(sub.BusinessHourGuid);
                            DateTime useTime = sub.ChangedExpireDate.HasValue ? sub.ChangedExpireDate.Value : sub.BusinessHourDeliverTimeE.Value;
                            if (deal.IsLoaded)
                                continue;
                            #region 上傳大圖
                            List<UpdateFile> filePic;
                            bool uploadPicResult;
                            if (string.IsNullOrEmpty(main.PponDeal.AppDealPic))
                            {
                                logger.Error("PChome上傳大圖File錯誤,無大圖,bid=" + sub.BusinessHourGuid.ToString());
                                continue;
                            }
                            uploadPicResult = UploadPicFile(sub.BusinessHourGuid, main.PponDeal.AppDealPic, out filePic);
                            #endregion

                            if (uploadPicResult)
                            {
                                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(sub.BusinessHourGuid);
                                PChomeETicketProd paramETicketProd = new PChomeETicketProd();
                                paramETicketProd.VendorPId = sub.UniqueId.ToString();
                                paramETicketProd.Name = sub.CouponUsage;
                                paramETicketProd.Nick = sub.CouponUsage;

                                ProdPrice price = new ProdPrice();
                                price.M = sub.ItemOrigPrice;
                                price.P = sub.ItemPrice;
                                paramETicketProd.Price = price;
                                ProdPic pic = new ProdPic();
                                pic._360x360 = filePic.Count > 0 ? filePic[0].PCPath : "";
                                pic._120x120 = filePic.Count > 1 ? filePic[1].PCPath : "";
                                paramETicketProd.Pic = pic;

                                paramETicketProd.Qty = (Convert.ToInt32(vpd.OrderedQuantity) > Convert.ToInt32(vpd.OrderTotalLimit)) ? 0 : Convert.ToInt32(vpd.OrderTotalLimit) - Convert.ToInt32(vpd.OrderedQuantity);
                                paramETicketProd.StoreId = GetStoreId(vpd.DealTypeDetail ?? 0, vpd.IsChannelGift);
                                paramETicketProd.isSingleton = 1;
                                paramETicketProd.isETicket = 1;

                                if (!string.IsNullOrEmpty(paramETicketProd.StoreId))
                                {
                                    bool result = PChomeChannelAPI.AddETicketProd(paramETicketProd);
                                    if (result)
                                    {
                                        PchomeChannelProd prod = new PchomeChannelProd();
                                        prod.ProdId = paramETicketProd.Id;
                                        prod.ProdGroupId = paramETicketProd.GroupId;
                                        prod.IsEticket = Convert.ToBoolean(paramETicketProd.isETicket);
                                        prod.Bid = sub.BusinessHourGuid;
                                        prod.CreateTime = DateTime.Now;
                                        prod.CreateId = "sys";
                                        prod.IsShelf = (int)PChomeIsShelf.Offline;
                                        list.Add(prod);


                                        //編輯slogan(一姬)
                                        var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(main.PponDeal);
                                        ProdEditDesc paramEditDesc = new ProdEditDesc();
                                        paramEditDesc.GroupId = prod.ProdGroupId;
                                        paramEditDesc.Slogan = ChannelFacade.GetPChomeSlogan(contentLite.Remark, sub.BusinessHourDeliverTimeS.Value, useTime);
                                        bool descResult = PChomeChannelAPI.EditDesc(paramEditDesc);
                                        if (!descResult)
                                        {
                                            logger.Error("PChome編輯Desc錯誤,bid=" + sub.BusinessHourGuid.ToString());
                                        }

                                        //文案(詳細介紹) + 權益說明
                                        ProdEditIntro paramEditIntro = new ProdEditIntro();
                                        List<IntroDetail> paramIntroDetailList = new List<IntroDetail>();


                                        #region 上傳圖文
                                        bool uploadIntroResult;
                                        uploadIntroResult = UploadIntroFile(sub.BusinessHourGuid, contentLite.Description, contentLite.Restrictions, contentLite.Availability, out paramIntroDetailList);
                                        #endregion

                                        if (uploadIntroResult)
                                        {
                                            paramEditIntro.GroupId = prod.ProdGroupId;
                                            paramEditIntro.Info = paramIntroDetailList;

                                            bool introResult = PChomeChannelAPI.EditIntro(paramEditIntro);
                                            if (!introResult)
                                            {
                                                logger.Error("PChome編輯Intro錯誤,bid=" + sub.BusinessHourGuid.ToString());
                                            }
                                        }
                                        else
                                        {
                                            logger.Error("PChome上傳圖文File錯誤,bid=" + sub.BusinessHourGuid.ToString());
                                        }
                                    }
                                    else
                                    {
                                        logger.Error("PChome新增Prod錯誤,bid=" + sub.BusinessHourGuid.ToString());
                                    }
                                }
                                else
                                {
                                    logger.Error("PChome新增Prod錯誤,分類不正確,bid=" + main.PponDeal.BusinessHourGuid.ToString() + ",Deal_Type_Detail=" + (vpd.DealTypeDetail ?? 0).ToString() + ",Is_Channel_Gift=" + vpd.IsChannelGift.ToString());
                                }
                            }
                            else
                            {
                                logger.Error("PChome上傳大圖File錯誤 bid=" + sub.BusinessHourGuid.ToString());
                            }
                        }
                    }
                    else
                    {
                        PchomeChannelProd deal = chp.PChomeChannelProdGetByBid(main.PponDeal.BusinessHourGuid);
                        DateTime useTime = main.PponDeal.ChangedExpireDate.HasValue ? main.PponDeal.ChangedExpireDate.Value : main.PponDeal.BusinessHourDeliverTimeE.Value;
                        if (deal.IsLoaded)
                            continue;
                        #region 上傳大圖
                        List<UpdateFile> filePic;
                        bool uploadPicResult;
                        if (string.IsNullOrEmpty(main.PponDeal.AppDealPic))
                        {
                            logger.Error("PChome上傳大圖File錯誤,無大圖,bid=" + main.PponDeal.BusinessHourGuid.ToString());
                            continue;
                        }
                        uploadPicResult = UploadPicFile(main.PponDeal.BusinessHourGuid, main.PponDeal.AppDealPic, out filePic);
                        #endregion

                        if (uploadPicResult)
                        {
                            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(main.PponDeal.BusinessHourGuid);
                            PChomeETicketProd paramETicketProd = new PChomeETicketProd();
                            paramETicketProd.VendorPId = main.PponDeal.UniqueId.ToString();
                            paramETicketProd.Name = main.PponDeal.ItemName;
                            paramETicketProd.Nick = main.PponDeal.ItemName;

                            ProdPrice price = new ProdPrice();
                            price.M = main.PponDeal.ItemOrigPrice;
                            price.P = main.PponDeal.ItemPrice;
                            paramETicketProd.Price = price;
                            ProdPic pic = new ProdPic();
                            pic._360x360 = filePic.Count > 0 ? filePic[0].PCPath : "";
                            pic._120x120 = filePic.Count > 1 ? filePic[1].PCPath : "";
                            paramETicketProd.Pic = pic;

                            paramETicketProd.Qty = (Convert.ToInt32(vpd.OrderedQuantity) > Convert.ToInt32(vpd.OrderTotalLimit)) ? 0 : Convert.ToInt32(vpd.OrderTotalLimit) - Convert.ToInt32(vpd.OrderedQuantity);
                            paramETicketProd.StoreId = GetStoreId(vpd.DealTypeDetail ?? 0, vpd.IsChannelGift);
                            paramETicketProd.isSingleton = 1;
                            paramETicketProd.isETicket = 1;

                            if (!string.IsNullOrEmpty(paramETicketProd.StoreId))
                            {
                                bool result = PChomeChannelAPI.AddETicketProd(paramETicketProd);
                                if (result)
                                {
                                    PchomeChannelProd prod = new PchomeChannelProd();
                                    prod.ProdId = paramETicketProd.Id;
                                    prod.ProdGroupId = paramETicketProd.GroupId;
                                    prod.IsEticket = Convert.ToBoolean(paramETicketProd.isETicket);
                                    prod.Bid = main.PponDeal.BusinessHourGuid;
                                    prod.CreateTime = DateTime.Now;
                                    prod.CreateId = "sys";
                                    prod.IsShelf = (int)PChomeIsShelf.Offline;
                                    list.Add(prod);

                                    //編輯slogan(一姬)
                                    var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(main.PponDeal);
                                    ProdEditDesc paramEditDesc = new ProdEditDesc();
                                    paramEditDesc.GroupId = prod.ProdGroupId;
                                    paramEditDesc.Slogan = ChannelFacade.GetPChomeSlogan(contentLite.Remark, main.PponDeal.BusinessHourDeliverTimeS.Value, useTime);
                                    bool descResult = PChomeChannelAPI.EditDesc(paramEditDesc);

                                    if (!descResult)
                                    {
                                        logger.Error("PChome編輯Desc錯誤,bid=" + main.PponDeal.BusinessHourGuid.ToString());
                                    }

                                    //文案(詳細介紹) + 權益說明
                                    ProdEditIntro paramEditIntro = new ProdEditIntro();
                                    List<IntroDetail> paramIntroDetailList = new List<IntroDetail>();


                                    #region 上傳圖文
                                    bool uploadIntroResult;
                                    uploadIntroResult = UploadIntroFile(main.PponDeal.BusinessHourGuid, contentLite.Description, contentLite.Restrictions, contentLite.Availability, out paramIntroDetailList);

                                    #endregion

                                    if (uploadIntroResult)
                                    {
                                        paramEditIntro.GroupId = prod.ProdGroupId;
                                        paramEditIntro.Info = paramIntroDetailList;

                                        bool introResult = PChomeChannelAPI.EditIntro(paramEditIntro);
                                        if (!introResult)
                                        {
                                            logger.Error("PChome編輯Intro錯誤,bid=" + main.PponDeal.BusinessHourGuid.ToString());
                                        }
                                    }
                                    else
                                    {
                                        logger.Error("PChome上傳圖文File錯誤,bid=" + main.PponDeal.BusinessHourGuid.ToString());
                                    }
                                }
                                else
                                {
                                    logger.Error("PChome新增Prod錯誤,bid=" + main.PponDeal.BusinessHourGuid.ToString());
                                }
                            }
                            else
                            {
                                logger.Error("PChome新增Prod錯誤,分類不正確,bid=" + main.PponDeal.BusinessHourGuid.ToString() + ",Deal_Type_Detail=" + (vpd.DealTypeDetail ?? 0).ToString() + ",Is_Channel_Gift=" + vpd.IsChannelGift.ToString());
                            }
                        }
                        else
                        {
                            logger.Error("PChome上傳大圖File錯誤,bid=" + main.PponDeal.BusinessHourGuid.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("PChome拋檔錯誤,bid=" + main.PponDeal.BusinessHourGuid.ToString(), ex);
                }
            }


            chp.PChomeChannelProdSetList(list);

            DateTime end = DateTime.Now;
            logger.Info("PChomeChannelAddProdJob " + start.ToString() + " " + end.ToString());
        }

        /// <summary>
        /// 更新是否已審核(無PM審核狀態可取,若曾經有上架狀態就視為審核過)
        /// </summary>
        public static void UpdateIsVerify()
        {
            PchomeChannelProdCollection prods = chp.PchomeChannelProdGetByShelfVerify((int)PChomeIsShelf.Offline, null);
            foreach (PchomeChannelProd prod in prods)
            {
                try
                {
                    PChomeETicketProd Eprod = PChomeChannelAPI.GetProd(prod.ProdId).FirstOrDefault();
                    if (Eprod.isShelf == (int)PChomeIsShelf.Online)
                    {
                        prod.IsVerify = true;
                        prod.VerifyTime = DateTime.Now;
                        prod.ModifyId = "sys";
                        prod.ModifyTime = DateTime.Now;
                        chp.PchomeChannelProdSet(prod);
                    }

                }
                catch (Exception ex)
                {
                    logger.Error("PChome UpdateIsVerify錯誤,pordId=" + prod.ProdId.ToString(), ex);
                }

            }


        }

        /// <summary>
        /// 更新上架狀態(只更新審核過的商品)
        /// </summary>
        public static void UpdateIsShelf()
        {
            PchomeChannelProdCollection prods = chp.PchomeChannelProdGetByShelfVerify((int)PChomeIsShelf.Online, null);
            foreach (PchomeChannelProd prod in prods)
            {
                try
                {
                    PChomeETicketProd Eprod = PChomeChannelAPI.GetProd(prod.ProdId).FirstOrDefault();
                    if (prod.IsShelf != Eprod.isShelf)
                    {
                        prod.IsShelf = Eprod.isShelf;
                        prod.ModifyId = "sys";
                        prod.ModifyTime = DateTime.Now;
                        chp.PchomeChannelProdSet(prod);
                    }

                }
                catch (Exception ex)
                {
                    logger.Error("PChome UpdateIsShelf錯誤,pordId=" + prod.ProdId.ToString(), ex);
                }

            }


        }

        /// <summary>
        /// 更新垃圾桶狀態(只更新審核過的商品)
        /// </summary>
        public static void UpdateIsArchive()
        {
            PchomeChannelProdCollection prods = chp.PchomeChannelProdGetByShelfVerify((int)PChomeIsShelf.Online, null);
            foreach (PchomeChannelProd prod in prods)
            {
                try
                {
                    PChomeETicketProd Eprod = PChomeChannelAPI.GetProd(prod.ProdId).FirstOrDefault();
                    if (prod.IsArchive != Eprod.isArchive)
                    {
                        prod.IsArchive = Eprod.isArchive;
                        prod.ModifyId = "sys";
                        prod.ModifyTime = DateTime.Now;
                        chp.PchomeChannelProdSet(prod);
                    }

                }
                catch (Exception ex)
                {
                    logger.Error("PChome UpdateIsArchive錯誤,pordId=" + prod.ProdId.ToString(), ex);
                }

            }


        }

        /// <summary>
        /// 更新需上架/下架商品(黑白名單、過期....)
        /// </summary>
        public static void UpdateIsShelf(List<MultipleMainDealPreview> deals)
        {
            //本次符合的商品
            List<Guid> list = new List<Guid>();
            list.AddRange(deals.Where(x => !x.PponDeal.ComboDelas.Any()).Select(x => x.PponDeal.BusinessHourGuid).ToList());
            list.AddRange(deals.Where(x => x.PponDeal.ComboDelas.Any()).SelectMany(x => x.PponDeal.ComboDelas).Select(x => x.BusinessHourGuid).ToList());

            //目前上架商品,改為下架
            //若原單/子檔變成母檔也改為下架
            PchomeChannelProdCollection online = chp.PchomeChannelProdGetByShelfVerify((int)PChomeIsShelf.Online, (int)PChomeIsShelf.Online);
            foreach (PchomeChannelProd prod in online)
            {
                try
                {
                    //排除垃圾桶商品
                    if (prod.IsArchive == 0)
                    {
                        bool isOffline = !list.Contains(prod.Bid);//某商品不包含本次→下架
                        if (isOffline)
                        {
                            ProdEditIsShelf isShelf = new ProdEditIsShelf();
                            isShelf.GroupId = prod.ProdGroupId;
                            isShelf.isShelf = (int)PChomeIsShelf.Offline;
                            isShelf.OffShelfReason = "0";//0表示系統下架
                            bool result = PChomeChannelAPI.EditIsShelf(new List<ProdEditIsShelf> { isShelf });

                            if (result)
                            {
                                prod.IsShelf = (int)PChomeIsShelf.Offline;
                                prod.ModifyId = "sys";
                                prod.ModifyTime = DateTime.Now;
                                chp.PchomeChannelProdSet(prod);
                            }
                            else
                            {
                                logger.Error("PChome UpdateIsShelf錯誤,pordId=" + prod.ProdId.ToString());

                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    logger.Error("PChome UpdateIsShelf錯誤,pordId=" + prod.ProdId.ToString(), ex);
                }

            }

            //目前下架商品,改為上架
            PchomeChannelProdCollection offline = chp.PchomeChannelProdGetByShelfVerify((int)PChomeIsShelf.Online, (int)PChomeIsShelf.Offline);
            foreach (PchomeChannelProd prod in offline)
            {
                try
                {
                    //排除垃圾桶商品
                    if (prod.IsArchive == 0)
                    {
                        bool isOnline = list.Contains(prod.Bid);//原下架的有包含本次→上架
                        if (isOnline)
                        {
                            PChomeETicketProd Eprod = PChomeChannelAPI.GetProd(prod.ProdId).FirstOrDefault();
                            //特定原因下架,無法由系統重新上架
                            if (Eprod.OffShelfReason != "1" && Eprod.OffShelfReason != "2" && Eprod.OffShelfReason != "3" && Eprod.OffShelfReason != "4")
                            {
                                ProdEditIsShelf isShelf = new ProdEditIsShelf();
                                isShelf.GroupId = prod.ProdGroupId;
                                isShelf.isShelf = (int)PChomeIsShelf.Online;
                                isShelf.OffShelfReason = "";//上架填入空值即可
                                bool result = PChomeChannelAPI.EditIsShelf(new List<ProdEditIsShelf> { isShelf });

                                if (result)
                                {
                                    prod.IsShelf = (int)PChomeIsShelf.Online;
                                    prod.ModifyId = "sys";
                                    prod.ModifyTime = DateTime.Now;
                                    chp.PchomeChannelProdSet(prod);
                                }
                                else
                                {
                                    logger.Error("PChome UpdateIsShelf錯誤,pordId=" + prod.ProdId.ToString());

                                }
                            }
                            
                        }
                    }

                }
                catch (Exception ex)
                {
                    logger.Error("PChome UpdateIsShelf錯誤,pordId=" + prod.ProdId.ToString(), ex);
                }
            }
        }

        /// <summary>
        /// 更新線上的庫存(只更新上架商品)
        /// </summary>
        public static void UpdateStock(bool isShortTime)
        {
            List<PchomeChannelProd> prods = chp.PchomeChannelProdGetByShelfVerify((int)PChomeIsShelf.Online, (int)PChomeIsShelf.Online).ToList();
            if (isShortTime)
            {
                //部分檔次需短時間更新
                if (!string.IsNullOrEmpty(cp.PChomeChannelShortTimeUpdateStockBid))
                {
                    List<string> bids = cp.PChomeChannelShortTimeUpdateStockBid.Split(";").ToList();
                    List<Guid> updateBids = bids.ConvertAll<Guid>(i => Guid.Parse(i));
                    prods = prods.Where(x => updateBids.Contains(x.Bid)).ToList();
                }
                else
                {
                    prods = new List<PchomeChannelProd>();
                }

            }
            foreach (PchomeChannelProd prod in prods)
            {
                try
                {
                    if (!string.IsNullOrEmpty(cp.PChomeChannelNoUpdateStockBid) && cp.PChomeChannelNoUpdateStockBid.Contains(prod.Bid.ToString()))
                    {
                        continue;
                    }
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(prod.Bid);
                    Guid mainBid = deal.MainBid == null ? deal.BusinessHourGuid : Guid.Parse(deal.MainBid.ToString());
                    ProdEditQry qty = new ProdEditQry();
                    qty.Id = prod.ProdId;
                    qty.Qty = (Convert.ToInt32(deal.OrderedQuantity) > Convert.ToInt32(deal.OrderTotalLimit)) ? 0 : Convert.ToInt32(deal.OrderTotalLimit) - Convert.ToInt32(deal.OrderedQuantity);

                    //PChome庫存最多1200
                    if (qty.Qty > 1200)
                        qty.Qty = 1200;

                    bool result = PChomeChannelAPI.EditQty(new List<ProdEditQry> { qty });

                    if (!result)
                    {
                        logger.Error("PChome UpdateStock錯誤,pordId=" + prod.ProdId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("PChome UpdateStock錯誤,pordId=" + prod.ProdId.ToString(), ex);
                }

            }

        }

        /// <summary>
        /// 上傳大圖
        /// </summary>
        /// <param name="main"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool UploadPicFile(Guid bid, string appDealPic, out List<UpdateFile> files)
        {
            files = new List<UpdateFile>();
            try
            {
                bool uploadResult = false;
                files = new List<UpdateFile>();
                string[] imagesArray = ImageFacade.GetMediaPathsFromRawData(appDealPic, MediaType.PponDealPhoto).Where((x, i) => i == 0).ToArray();
                string DealImage = imagesArray.FirstOrDefault();
                int index = DealImage.ToLower().IndexOf("media");
                DealImage = @"~/" + DealImage.Substring(index);
                if (DealImage.Contains("?"))
                {
                    DealImage = DealImage.Substring(0, DealImage.IndexOf("?"));
                    DealImage = System.Web.HttpUtility.UrlDecode(DealImage);
                }


                #region 處理PChome大圖最大640
                string physicalPath = HostingEnvironment.MapPath(DealImage);
                Image smallImage = System.Drawing.Image.FromFile(physicalPath);
                int swidth = smallImage.Width;
                int sheight = smallImage.Height;
                int changewidth = 640;
                string small640Path = physicalPath;
                small640Path = Path.GetDirectoryName(small640Path) + @"\" + Path.GetFileNameWithoutExtension(small640Path) + "_PC640" + Path.GetExtension(small640Path);


                if (swidth != changewidth)
                {
                    System.Drawing.Image smallresizeimage = ImageFacade.ResizeImage(smallImage, new System.Drawing.Size(changewidth, sheight * changewidth / swidth), true);

                    string contentType = MimeTypes.GetMimeTypeByExtension(Path.GetExtension(small640Path));
                    var codecInfo = ImageFacade.GetImageCodecInfo(contentType);
                    var encoderParam = ImageFacade.GetEncoderParameters(80);

                    smallImage.Dispose();
                    smallresizeimage.Save(small640Path, codecInfo, encoderParam);
                    smallresizeimage.Dispose();

                    //新檔名指定回去
                    physicalPath = small640Path;

                    if (!File.Exists(small640Path))
                        return false;
                }


                files.Add(new UpdateFile
                {
                    E7PhysicalPath = physicalPath,
                    E7FileName = Path.GetFileName(physicalPath),
                    PCPath = "",
                    PCUrl = ""
                });

                #endregion

                #region 處理PChome小圖最大320
                physicalPath = HostingEnvironment.MapPath(DealImage);
                smallImage = System.Drawing.Image.FromFile(physicalPath);
                swidth = smallImage.Width;
                sheight = smallImage.Height;
                changewidth = 320;
                string small320Path = physicalPath;
                small320Path = Path.GetDirectoryName(small320Path) + @"\" + Path.GetFileNameWithoutExtension(small320Path) + "_PC320" + Path.GetExtension(small320Path);


                if (swidth != changewidth)
                {
                    System.Drawing.Image smallresizeimage = ImageFacade.ResizeImage(smallImage, new System.Drawing.Size(changewidth, sheight * changewidth / swidth), true);

                    string contentType = MimeTypes.GetMimeTypeByExtension(Path.GetExtension(small320Path));
                    var codecInfo = ImageFacade.GetImageCodecInfo(contentType);
                    var encoderParam = ImageFacade.GetEncoderParameters(80);

                    smallImage.Dispose();
                    smallresizeimage.Save(small320Path, codecInfo, encoderParam);
                    smallresizeimage.Dispose();

                    //新檔名指定回去
                    physicalPath = small320Path;

                    if (!File.Exists(small320Path))
                        return false;
                }

                files.Add(new UpdateFile
                {
                    E7PhysicalPath = physicalPath,
                    E7FileName = Path.GetFileName(physicalPath),
                    PCPath = "",
                    PCUrl = ""
                });
                #endregion

                //NameValueCollection nvc = new NameValueCollection();
                //nvc.Add("files[0]", file.E7PhysicalPath[0] + ";filename=" + Path.GetFileName(file.E7PhysicalPath[0]));
                //nvc.Add("files[1]", file.E7PhysicalPath[1] + ";filename=" + Path.GetFileName(file.E7PhysicalPath[1]));
                //file.nvc = nvc;

                uploadResult = PChomeChannelAPI.UpdateFile(files);
                //暫存而已,處理完即刪掉
                if (File.Exists(small640Path))
                    File.Delete(small640Path);
                if (File.Exists(small320Path))
                    File.Delete(small320Path);

                return uploadResult;
            }
            catch (Exception ex)
            {
                logger.Error("PChome UploadPicFile錯誤,bid=" + bid.ToString(), ex);
                return false;
            }
        }

        /// <summary>
        /// 上傳圖文的圖
        /// </summary>
        /// <param name="main"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool UploadIntroFile(Guid bid, string description, string restrition, string availability, out List<IntroDetail> paramIntroDetailList)
        {
            paramIntroDetailList = new List<IntroDetail>();
            string physicalPath = "";
            string image = "";
            try
            {
                bool uploadResult = false;
                bool result = false;

                List<UpdateFile> fileIntro = new List<UpdateFile>();


                string CustomerNewLink = "https://ecvip.pchome.com.tw/web/pages/eticketinfo.htm";
                description = description.Replace(System.Environment.NewLine, string.Empty);

                restrition = restrition.Replace("https://www.17life.com/Ppon/NewbieGuide.aspx?s=3&q=20428#anc20428", CustomerNewLink);
                restrition = restrition.Replace("https://www.17life.com/Ppon/NewbieGuide.aspx?s=3&amp;q=20428#anc20428", CustomerNewLink);
                restrition = restrition.Replace("https://www.17life.com/ppon/newbieguide.aspx?s=3", CustomerNewLink);
                restrition = restrition.Replace(System.Environment.NewLine, string.Empty);
                restrition = restrition.Replace("●紙本/APP/簡訊憑證皆可使用", "●本電子票券不限本人使用，應妥善保存。若因電子票券遺失或公開序號造成序號被第三方兌換所產生之損失，須由持有者自行承擔。使用時店家將會核對憑證編號，一組編號限用一次；電子票券不得與其他優惠合併使用，且恕無法兌換現金。");//PChome可能沒有紙本與簡訊
                string restrition2 = "<span style=\"color: blue; \">票券應記載事項</span>" + "<br />";
                restrition2 += @"發行單位：康太數位整合股份有限公司 &nbsp; &nbsp; 負責人：李易騰 <br />
                                統一編號：24317014 <br />
                                實收資本額：180,000,000 <br />
                                地址：台北市中山區中山北路一段11號13樓 電話 :2521 - 9131 <br />
                                信託期間：發售日起一年內 <br />
                                本商品(服務)禮券所收取之金額，已存入發行人於台新銀行、陽信銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用。<br />
                                在電子票券優惠期間內：尚未到店兌換的電子票券，可辦理全額無息退費，作業手續費$0。<br />
                                超過電子票券優惠期間：尚未到店兌換的電子票券，可辦理全額無息退費，作業手續費$0。<br />
                                兌換優惠期間內，與發行人合作之店家倒閉或因故無法提供商品或服務，可辦理全額無息退費，作業手續費$0。<br />
                                <br />
                                ※部分電子票券所表彰之內容為依據特定單一時間與特定場合所提供之服務(包括但不限於演唱會、 音樂會或展覽)，當電子票券持有人因故未能於該特定時間與特定場合兌換該服務，恕無法要求退費或另行補足差額重現該服務。<br />
                                ※本電子票券所表彰之商品或服務內容由與發行人合作之店家提供，PChome則為電子票券銷售服務提供者。請電子票券持有人務必於兌換優惠期間內使用，並確實了解本電子票券及商品頁面所載明之使用說明（包括但不限於：使用時段、事前預約規定、內用或外帶、每人使用限制）<br />
                                ※消費爭議處理申訴專線：2704 - 0999";
                restrition2 = restrition2.Replace(System.Environment.NewLine, string.Empty);


                List<string> imgTags = new List<string>();
                List<string> imgHrefs = new List<string>();
                List<string> tempDelete = new List<string>();
                //string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
                string regexImgSrc = "<img.+?src=[\"'](.+?)[\"'].*?>";
                MatchCollection matchesImgSrc = Regex.Matches(description, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                foreach (Match m in matchesImgSrc)
                {
                    string tag = m.Groups[0].Value;
                    string href = m.Groups[1].Value;
                    imgTags.Add(tag);
                    imgHrefs.Add(href);
                }

                if (imgHrefs.Count > 0)
                {
                    int batchLimit = 20;
                    while (fileIntro.Count <= imgHrefs.Count - 1)
                    {
                        List<string> tempImgHrefs = imgHrefs.Skip(fileIntro.Count).Take(batchLimit).ToList();
                        List<UpdateFile> tempFileIntro = new List<UpdateFile>();
                        for (int count = 0; count < tempImgHrefs.Count; count++)
                        {
                            //image = https://localhost/Images/imagesU/9/99eacc75-4a35-4bf2-8ffe-5d7eac72822f/1g(3).jpg
                            image = tempImgHrefs[count];
                            image = System.Web.HttpUtility.UrlDecode(image);

                            //相對路徑
                            if (image.Contains(@"../.."))
                            {
                                image = image.Replace(@"../..", cp.SSLSiteUrl);
                            }

                            bool innerImage = image.ToLower().IndexOf(cp.SSLSiteUrl + "/images") >= 0;
                            int index = image.ToLower().IndexOf("images");

                            if (innerImage)
                            {
                                //內部圖片
                                image = @"~/" + image.Substring(index);
                                if (image.Contains("?"))
                                {
                                    image = image.Substring(0, image.IndexOf("?"));
                                }

                                //image = ~/Images/imagesU/9/99eacc75-4a35-4bf2-8ffe-5d7eac72822f/1g(3).jpg
                                physicalPath = HostingEnvironment.MapPath(image);
                                if (!File.Exists(physicalPath))
                                {
                                    logger.Info("PChome Error Image NotFound " + "<br/>" + "OriImage:" + tempImgHrefs[count].ToString() + "<br/>" + "Image:" + image + "<br/>" + "PhysicalPath:" + physicalPath);
                                    return false;
                                }
                            }
                            else
                            {
                                //非內部圖片
                                logger.Info("PChome Error Image Is Outer " + "<br/>" + "OriImage:" + tempImgHrefs[count].ToString() + "<br/>" + "Image:" + image);
                                physicalPath = (string.IsNullOrEmpty(cp.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : cp.CkFinderBaseDir) + DateTime.Now.Ticks + Path.GetExtension(image);
                                System.Net.WebClient WC = new System.Net.WebClient();
                                System.IO.MemoryStream Ms = new System.IO.MemoryStream(WC.DownloadData(image));
                                Image img = Image.FromStream(Ms);
                                img.Save(physicalPath);
                                tempDelete.Add(physicalPath);
                                if (!File.Exists(physicalPath))
                                {
                                    logger.Info("PChome Error Image NotFound " + "<br/>" + "OriImage:" + tempImgHrefs[count].ToString() + "<br/>" + "Image:" + image + "<br/>" + "PhysicalPath:" + physicalPath);
                                    return false;
                                }
                            }

                            #region 處理PChome圖文最大800,但統一調整700
                            //Image image = System.Drawing.Image.FromStream(requestStream);
                            //physicalPath = D:\pg\Cognac\Main\Source\LunchKingSite\LunchKingSite.Web\Images\imagesU\9\99eacc75-4a35-4bf2-8ffe-5d7eac72822f\1g(3).jpg
                            Image smallImage = System.Drawing.Image.FromFile(physicalPath);
                            int swidth = smallImage.Width;
                            int sheight = smallImage.Height;
                            int changewidth = 700; //統一寬度
                            string smallPath = physicalPath;
                            smallPath = Path.GetDirectoryName(smallPath) + @"\" + Path.GetFileNameWithoutExtension(physicalPath) + "_PC800" + Path.GetExtension(physicalPath);


                            if (swidth != changewidth)
                            {
                                System.Drawing.Image smallresizeimage = ImageFacade.ResizeImage(smallImage, new System.Drawing.Size(changewidth, sheight * changewidth / swidth), true);

                                string contentType = MimeTypes.GetMimeTypeByExtension(Path.GetExtension(physicalPath));
                                var codecInfo = ImageFacade.GetImageCodecInfo(contentType);
                                var encoderParam = ImageFacade.GetEncoderParameters(80);

                                smallImage.Dispose();
                                smallresizeimage.Save(smallPath, codecInfo, encoderParam);
                                smallresizeimage.Dispose();

                                physicalPath = smallPath;
                                tempDelete.Add(smallPath);
                                if (!File.Exists(smallPath))
                                    return false;
                            }
                            #endregion

                            //while單次的圖片量
                            tempFileIntro.Add(new UpdateFile
                            {
                                E7PhysicalPath = physicalPath,
                                E7FileName = Path.GetFileName(physicalPath),
                                PCPath = "",
                                PCUrl = ""
                            });


                        }

                        //所有的圖片量
                        fileIntro.AddRange(tempFileIntro);

                        uploadResult = PChomeChannelAPI.UpdateFile(tempFileIntro);
                        if (!uploadResult)
                        {
                            return false;
                        }
                    }


                    //暫存而已,處理完即刪掉
                    foreach (string de in tempDelete)
                    {
                        File.Delete(de);
                    }
                }
                else
                {
                    uploadResult = true;
                }

                if (uploadResult)
                {
                    for (int i = 0, l = imgTags.Count; i < l; i++)
                    {
                        string imgTag = imgTags[i];
                        int index = description.IndexOf(imgTag);
                        if (index > 0)
                        {
                            //圖前面有文字
                            string intro = description.Substring(0, index);
                            string tempIntro = intro.Replace("<br />", string.Empty).Trim();
                            if (!string.IsNullOrEmpty(tempIntro))
                            {
                                if (intro.Length >= 6 && intro.Substring(0, 6) == "<br />")
                                {
                                    //圖片後多餘的換行拔掉
                                    intro = intro.Substring(6);
                                }

                                //如果只有換行跟空白就不帶了,否則在PChome會太寬
                                paramIntroDetailList.Add(new IntroDetail
                                {
                                    Intro = intro
                                });
                            }

                            paramIntroDetailList.Add(new IntroDetail
                            {
                                Pstn = "M",
                                Pic = fileIntro[i].PCPath
                            });
                            description = description.Substring(index + imgTag.Length);
                        }
                        else if (index == 0)
                        {
                            //圖前面沒有文字
                            paramIntroDetailList.Add(new IntroDetail
                            {
                                Pstn = "M",
                                Pic = fileIntro[i].PCPath
                            });
                            description = description.Substring(imgTag.Length);
                        }
                    }
                    if (!string.IsNullOrEmpty(description))
                    {
                        //剩下的文字
                        paramIntroDetailList.Add(new IntroDetail
                        {
                            Intro = description
                        });
                    }

                    //再加權益說明
                    paramIntroDetailList.Add(new IntroDetail
                    {
                        Intro = "<span style=\"color: blue; \">權益說明</span>" + "<br />" + restrition
                    });

                    //再加票券應記載事項
                    paramIntroDetailList.Add(new IntroDetail
                    {
                        Intro = restrition2
                    });


                    //再加店家資訊
                    string storeDes = "";
                    int count = 0;
                    List<AvailableInformation> storeList = ProviderFactory.Instance().GetSerializer().Deserialize<AvailableInformation[]>(availability).ToList();
                    foreach (var availab in storeList)
                    {
                        if (count == 0)
                        {
                            storeDes += "<span style=\"color: blue; \">店家資訊</span>" + "<br />";
                        }

                        if (!string.IsNullOrEmpty(availab.N))
                            storeDes += "<span style=\"color: red; \">" + availab.N + "</span>" + "<br />";
                        if (!string.IsNullOrEmpty(availab.A))
                            storeDes += "地址：" + availab.A + "<br>";
                        if (!string.IsNullOrEmpty(availab.P))
                            storeDes += "電話：" + availab.P + "<br>";
                        if (!string.IsNullOrEmpty(availab.UT))
                            storeDes += "兌換時間：" + availab.UT.Replace("\n", "<br />") + "<br>";
                        if (!string.IsNullOrEmpty(availab.OT))
                            storeDes += "營業時間：" + availab.OT + "<br>";
                        if (!string.IsNullOrEmpty(availab.CD))
                            storeDes += "公休時間：" + availab.CD + "<br/>";
                        if (!string.IsNullOrEmpty(availab.OV))
                            storeDes += "交通方式：" + availab.OV + "<br/>";
                        if (!string.IsNullOrEmpty(availab.U))
                            storeDes += "官網：<a href='" + availab.U + "' target='_blank'>" + availab.U + "</a><br/>";
                        if (!string.IsNullOrEmpty(availab.FB))
                            storeDes += "Facebook：<a href='" + availab.FB + "' target='_blank'>" + availab.FB + "</a><br/>";
                        if (!string.IsNullOrEmpty(availab.PL))
                            storeDes += "Plurk：<a href='" + availab.PL + "' target='_blank'>" + availab.PL + "</a><br/>";
                        if (!string.IsNullOrEmpty(availab.BL))
                            storeDes += "Blog：<a href='" + availab.BL + "' target='_blank'>" + availab.BL + "</a><br/>";
                        storeDes += "<br/>";

                        count++;

                        //一個區塊內容有限,分批帶入
                        if (count % 5 == 0 || count == storeList.Count)
                        {
                            paramIntroDetailList.Add(new IntroDetail
                            {
                                Intro = storeDes
                            });
                            storeDes = "";
                        }

                    }


                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                logger.Error("PChome UploadIntroFile錯誤,bid=" + bid.ToString() + ",可能是圖片出錯:" + image, ex);
                return false;
            }

        }

        /// <summary>
        /// 取得主商館Id
        /// </summary>
        /// <param name="dealTypeDetail"></param>
        /// <returns></returns>
        public static string GetStoreId(int dealTypeDetail, bool isChannelGift)
        {
            string storeId = "";
            if (isChannelGift)
            {
                storeId = "QFDB0V";
            }
            else if (restaurantList.Contains(dealTypeDetail))
            {
                storeId = "QFDB00";
            }
            else if (restaurantBuffetList.Contains(dealTypeDetail))
            {
                storeId = "QFDB00";
            }
            else if (spaList.Contains(dealTypeDetail))
            {
                storeId = "QFDB01";
            }
            else if (hotelList.Contains(dealTypeDetail))
            {
                storeId = "QFDB02";
            }
            else if (ticketList.Contains(dealTypeDetail))
            {
                storeId = "QFDB02";
            }
            else if (courseList.Contains(dealTypeDetail))
            {
                storeId = "QFDB03";
            }
            else
            {
                storeId = "";
            }


            return storeId;
        }
    }
}

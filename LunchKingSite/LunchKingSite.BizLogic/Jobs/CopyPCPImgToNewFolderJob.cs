﻿using System;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using log4net;
using LunchKingSite.BizLogic;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Interface;
using System.IO;
using System.Linq;
using System.Web.Hosting;

namespace LunchKingSite.BizLogic.Jobs
{
    public class CopyPcpImgToNewFolderJob : IJob
    {
        private static IPCPProvider pcp;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PaymentCharge));

        static CopyPcpImgToNewFolderJob()
        {
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
        }

        public void Execute(JobSetting js)
        {
            CopyPcpImgToNewFolder();
        }

        public static void CopyPcpImgToNewFolder()
        {
            string mediaPath = HostingEnvironment.MapPath("~/Media");
            string pcpFolderUrl = Path.Combine(mediaPath, "PCP");
            string regularsFolderUrl = Path.Combine(mediaPath, "regulars");

            //string virtualPcpFolderUrl = System.AppDomain.CurrentDomain.BaseDirectory.Substring(0, System.AppDomain.CurrentDomain.BaseDirectory.LastIndexOf("\\", System.AppDomain.CurrentDomain.BaseDirectory.Length - 2)) + "\\Media\\PCP";
            //string virtualRegularsFolderUrl = System.AppDomain.CurrentDomain.BaseDirectory.Substring(0, System.AppDomain.CurrentDomain.BaseDirectory.LastIndexOf("\\", System.AppDomain.CurrentDomain.BaseDirectory.Length - 2)) + "\\Media\\regulars";
            //string pcpFolderUrl = System.AppDomain.CurrentDomain.BaseDirectory + "Media\\PCP";
            //string regularsFolderUrl = System.AppDomain.CurrentDomain.BaseDirectory + "Media\\regulars";

            try
            {
                //DirectoryCopy(virtualPcpFolderUrl, virtualRegularsFolderUrl, true);
                //FolderReNmae(virtualRegularsFolderUrl);
                DirectoryCopy(pcpFolderUrl, regularsFolderUrl, true);
                FolderReNmae(regularsFolderUrl);
            }
            catch (Exception ex)
            {
                Logger.Error(ex + "PCP圖片搬移發生異常。");
            }
        }

        private static void FolderReNmae(string dirName)
        {
            var pcpImageDatas = pcp.PcpImageGet().ToList();
            string[] dirs = Directory.GetDirectories(dirName);
            System.Collections.ArrayList dirlist = new System.Collections.ArrayList();

            foreach (var item in dirs)
            {
                dirlist.Add(Path.GetFileNameWithoutExtension(item));
            }

            foreach (var item in dirlist)
            {
                var newDirName = pcpImageDatas.FirstOrDefault(x => x.SellerUserId.ToString() == (string)item);
                var oldDir = dirName + "\\" + item;

                if (newDirName == null || newDirName.GroupId == 0)
                {
                    System.IO.Directory.Delete(oldDir, true);
                }
                else
                {
                    var newDir = dirName + "\\" + newDirName.GroupId;

                    //資料夾已存在則複製資料夾內圖片到既有資料夾中
                    if (Directory.Exists(newDir))
                    {
                        string[] imgs = Directory.GetFiles(oldDir);
                        System.Collections.ArrayList imglist = new System.Collections.ArrayList();

                        foreach (var img in imgs)
                        {
                            imglist.Add(Path.GetFileName(img));
                        }

                        foreach (var img in imglist)
                        {
                            System.IO.Directory.Move(oldDir + "\\" + img, newDir + "\\" + img);
                        }

                        System.IO.Directory.Delete(oldDir, true);   //複製完後刪除資料夾
                    }
                    else
                    {
                        System.IO.Directory.Move(oldDir, newDir);
                    }
                }
            }
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If directory is exist, delete it and recreate
            if (Directory.Exists(destDirName))
            {
                Directory.Delete(destDirName, true);
            }
            Directory.CreateDirectory(destDirName);


            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

    }
}

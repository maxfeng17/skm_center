﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using System;

namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary> Skm Pay 正常退款失敗後重新退款Job </summary>
    public class SkmPayFailedRetryJob : IJob
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(SkmPayFailedRetryJob));
        public void Execute(JobSetting js)
        {
            try
            {
                SkmFacade.SkmPayFailedRetryJob(logger);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

        }
    }
}

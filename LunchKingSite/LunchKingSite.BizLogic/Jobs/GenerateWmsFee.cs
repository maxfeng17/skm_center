﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Hosting;

namespace LunchKingSite.BizLogic.Jobs
{
	public class GenerateWmsFee : IJob
	{
		private static readonly ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static readonly IWmsProvider wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static readonly IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static ILog logger = LogManager.GetLogger("GenerateBalanceSheetWms");

		public void Execute(JobSetting js)
		{
			try
			{
                string message = string.Empty;
                bool isHasData = false;
                message = WmsFacade.DownloadWmsFeeFile(DateTime.Now.AddDays(-1), "all", "sys", out isHasData);

                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                if (isHasData)
                    msg.Subject = string.Format("{0}： [系統]{1}", Environment.MachineName, "GenerateWmsFee");
                else
                    msg.Subject = string.Format("{0}： [系統]{1}", Environment.MachineName, "GenerateWmsFee(無筆數資料請確認)");
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = message;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);

            }
            catch (Exception e)
			{
                logger.ErrorFormat("Job GenerateBalanceSheetWms Error {0}", e);
			}
		}
    }
}

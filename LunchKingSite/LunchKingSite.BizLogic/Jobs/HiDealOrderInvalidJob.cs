﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System;

namespace LunchKingSite.BizLogic.Jobs
{
    public class HiDealOrderInvalidJob : IJob
    {
        private static IHiDealProvider _hiDealProvider;
        private static ILog log = LogManager.GetLogger(typeof(HiDealOrderInvalidJob));
        private static HiDealOrderInvalidJob _theJob;

        static HiDealOrderInvalidJob()
        {
            _theJob = new HiDealOrderInvalidJob();
        }

        public static HiDealOrderInvalidJob Instance()
        {
            return _theJob;
        }

        private HiDealOrderInvalidJob()
        {
            _hiDealProvider = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                //檢查建立時間在30分鐘之前的訂單。
                //2013/06/19 檢查建立時間在15分鐘之前的訂單
                HiDealOrderManager.CancelConfirmHiDealOrderListBeforeDatetime(DateTime.Now.AddMinutes(-15));
            }
            catch (Exception ex)
            {
                log.Error("HiDealOrderInvalid running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
                throw;
            }
        }
    }
}
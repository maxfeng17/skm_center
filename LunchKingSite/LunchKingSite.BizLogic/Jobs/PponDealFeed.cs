﻿using System;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.BizLogic.Facade;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary>
    /// Google, Yahoo,FB product feed.
    /// 參考文件
    /// https://docs.google.com/spreadsheets/d/1iCz-k62ili42PGGVVsgho9aMLPB9_aOsfG2zqniepcs/edit#gid=0
    /// </summary>
    public class PponDealFeed : IJob
    {
        private ILog logger = LogManager.GetLogger(typeof(PponDealFeed));
        private static ISysConfProvider _config = ProviderFactory.Instance().GetConfig();

        public void Execute(JobSetting js)
        {
            try
            {
                BackupGoogleProductFeed();
                DateTime now = DateTime.Now;
                PromotionFacade.CreatePponDealFeed();
                logger.Info("CreatePponDealFeed 完成，費時 " + (DateTime.Now - now).TotalSeconds.ToString("#.##"));

                CheckGoogleProductFeedIntegrity();
            }
            catch (Exception ex)
            {
                logger.Error("PponDealFeed alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

        private void BackupGoogleProductFeed()
        {
            string googleFeedFilePath = Path.Combine(Helper.MapPath(_config.SiteMapCreatePath, true), "GoogleProductFeed.Xml");
            if (File.Exists(googleFeedFilePath) == false)
            {
                return;
            }
            string googleFeedBackupFilePath = Path.Combine(Helper.MapPath(_config.SiteMapCreatePath, true), "GoogleProductFeed.Xml.backup");
            File.Copy(googleFeedFilePath, googleFeedBackupFilePath, true);
        }

        /// <summary>
        /// 檢查xml有沒有完整
        /// 檢查xml是否大於5mb
        /// </summary>
        private void CheckGoogleProductFeedIntegrity()
        {
            string googleFeedFilePath = Path.Combine(Helper.MapPath(_config.SiteMapCreatePath, true), "GoogleProductFeed.Xml");
            string googleFeedBackupFilePath = Path.Combine(Helper.MapPath(_config.SiteMapCreatePath, true), "GoogleProductFeed.Xml.backup");
            string googleFeedFilePathError = Path.Combine(Helper.MapPath(_config.SiteMapCreatePath, true), "GoogleProductFeed.Xml.error");
            string str = File.ReadAllText(googleFeedFilePath);

            bool success;
            string message;
            if (str.EndsWith("</rss>") == false)
            {
                success = false;
                message = "GSA: 驗證有誤，Google Product Feed 匯出的檔案不完整, 先回復到上次備份的版本";
            }
            else if (new FileInfo(googleFeedFilePath).Length < 1024 * 1024 * 5)
            {
                success = false;
                message = "GSA: 驗證有誤，Google Product Feed 匯出的檔案小於5MB, 先回復到上次備份的版本";
            }
            else
            {
                success = true;
                message = "GSA: 匯出筆數: " + GetGoogleProductFeedXmlItemCount() + ", ";
                message += "Yahoo: 匯出筆數: " + GetYahooeProductFeedXmlItemCount();
            }

            var m = new System.Net.Mail.MailMessage
            {
                Subject = string.Format("Goole Product Feed {0}通知", success ? "成功" : "失敗"),
                Body = message
            };
            m.To.Add("itstaff@17life.com");
            m.To.Add("yuki_chiu@17life.com");
            PostMan.Instance().Send(m, SendPriorityType.Immediate);

            if (success == false)
            {
                LogManager.GetLogger(LineAppender._LINE).Info(message);
                File.Copy(googleFeedFilePath, googleFeedFilePathError, true);
                File.Copy(googleFeedBackupFilePath, googleFeedFilePath, true);
            }
        }
        /// <summary>
        /// 從 xml 取得實際匯出多少檔次數目給google
        /// </summary>
        /// <returns></returns>
        private static int GetGoogleProductFeedXmlItemCount()
        {
            string googleFeedFilePath = Path.Combine(Helper.MapPath(_config.SiteMapCreatePath, true), "GoogleProductFeed.Xml");
            var xdoc = XDocument.Load(googleFeedFilePath);
            if (xdoc.Root == null)
            {
                return 0;
            }
            var xElemChannel = xdoc.Root.Elements().FirstOrDefault(t => t.Name == "channel");
            if (xElemChannel == null)
            {
                return 0;
            }
            return xElemChannel.Elements().Count(t => t.Name == "item");
        }

        private static int GetYahooeProductFeedXmlItemCount()
        {
            string googleFeedFilePath = Path.Combine(Helper.MapPath(_config.SiteMapCreatePath, true), "YahooProductFeed.Xml");
            var xdoc = XDocument.Load(googleFeedFilePath);
            if (xdoc.Root == null)
            {
                return 0;
            }
            var xElemChannel = xdoc.Root.Elements().FirstOrDefault(t => t.Name == "channel");
            if (xElemChannel == null)
            {
                return 0;
            }
            return xElemChannel.Elements().Count(t => t.Name == "item");
        }
    }
}

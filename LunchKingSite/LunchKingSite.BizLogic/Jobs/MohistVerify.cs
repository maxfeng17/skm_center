﻿using System;
using System.Net.Mail;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Jobs
{
    public class MohistVerify : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(MohistVerify));
        private static ISysConfProvider _config;
        private static MohistVerify _theJob;

        static MohistVerify()
        {
            _theJob = new MohistVerify();
        }

        public static MohistVerify Instance()
        {
            return _theJob;
        }

        private MohistVerify()
        {
            _config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                DateTime dateStart = DateTime.Now.AddDays(-1).Date;
                DateTime dateEnd = DateTime.Now.Date;

                int productCount = OrderFacade.UploadMohistProduct(dateStart, dateEnd);
                OrderFacade.SaveMohistVerifyInfo(dateStart, dateEnd);
                List<string> unusualCoupon = OrderFacade.UpdateMohistVerifyInfoStatusByPeriod(dateStart, dateEnd);
                int orderCount = OrderFacade.UploadMohistDeals(dateStart, dateEnd);
                int confirmCount = OrderFacade.UploadMohistConfirms(dateStart, dateEnd);

                MailMessage msg = new MailMessage();
                msg.IsBodyHtml = true;
                msg.From = new MailAddress(_config.AdminEmail);
                msg.To.Add(_config.AdminEmail);
                msg.Subject = "[系統] 墨攻核銷紀錄檔次";
                msg.Body = string.Format("新增{0}筆墨攻檔次，{1}筆交易憑證，{2}筆核銷/退貨憑證", productCount, orderCount, confirmCount);
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);

                if (unusualCoupon.Count > 0)
                {
                    msg.To.Add(_config.AdminEmail);
                    msg.Subject = "[系統] 墨攻核銷紀錄-憑證異常!";
                    string msgBody = string.Empty;
                    msgBody += "<br /><span style='color: red;'>注意!!有憑證異常紀錄，請通知墨攻系統相關人員!!<br /></span>";
                    msgBody += "<span style='color: red;'>" + string.Join("<br />", unusualCoupon) + "</span>";
                    msg.Body = msgBody;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(_config.AdminEmail);
                msg.Subject = "MohistVerify Error";
                msg.From = new MailAddress(_config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error("MohistVerify Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
 }

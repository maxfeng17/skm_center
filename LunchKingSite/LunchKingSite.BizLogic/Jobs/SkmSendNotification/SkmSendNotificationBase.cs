﻿using log4net;
using LunchKingSite.BizLogic.Models.Skm;
using System;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Jobs.SkmSendNotification
{
    /// <summary>
    /// 發送簡訊服務基本類別
    /// </summary>
    public abstract class SkmSendNotificationBase
    {
        protected ILog logger;
        /// <summary>
        /// 取得需要發送簡訊的資料清單
        /// </summary>
        /// <returns>需要發送簡訊的資料清單</returns>
        public abstract List<SkmSendNotificationInfo> GetNeedSendInfo();
    }

    /// <summary>
    /// 發送簡訊服務類別範例
    /// </summary>
    public class SkmSendNotificationSample : SkmSendNotificationBase
    {
        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="log">指定要記錄的log元件</param>
        public SkmSendNotificationSample(ILog log)
        {
            logger = log;
        }

        /// <summary>
        /// 建構子(log元件以物件名稱產生)
        /// </summary>
        public SkmSendNotificationSample()
        {
            logger = LogManager.GetLogger(typeof(SkmSendNotificationSample));
        }

        /// <summary>
        /// 取得需要發送簡訊的資料清單
        /// </summary>
        /// <returns>需要發送簡訊的資料清單</returns>
        public override List<SkmSendNotificationInfo> GetNeedSendInfo()
        {
            //在這取得需要發送簡訊的資料&組合成SkmSendNotificationInfo
            base.logger.Info("SkmSendNotificationSample");
            return new List<SkmSendNotificationInfo>();
        }
    }
}

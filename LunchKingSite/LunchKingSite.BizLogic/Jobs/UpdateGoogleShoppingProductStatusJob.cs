﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Xml;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Http;
using Google.Apis.Services;
using Google.Apis.ShoppingContent.v2;
using Google.Apis.ShoppingContent.v2.Data;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SubSonic.Utilities;


namespace LunchKingSite.BizLogic.Jobs
{
    public class UpdateGoogleShoppingProductStatusJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(UpdateGoogleShoppingProductStatusJob));
        public void Execute(JobSetting js)
        {
            try
            {
                List<GoogleShoppingProductResource> items = new GoogleService().GetProducts();

                IPponEntityProvider pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
                pep.GoogleShoppingProductDeleteAll();
                DateTime now = DateTime.Now;
                var gpsList = items.Select(t => new GoogleShoppingProduct
                {
                    Bid = Guid.Parse(t.ProductId),
                    Title = t.Title,
                    ImageLink = t.ImageLink,
                    Description = t.Description,
                    Brand = t.Brand,
                    Gtin = t.Gtin,
                    Mpn = t.Mpn,
                    Link = t.Link,
                    QualityIssue = t.CritalQualityIssue,
                    Approved = t.Approved,
                    CreateTime = now
                }).ToList();
                pep.GoogleShoppingProductSet(gpsList);
                
            }
            catch (Exception ex)
            {
                logger.Error("UpdateGoogleShoppingProductStatusJob", ex);
            }
        }

        public class GoogleShoppingProductResource
        {
            public string Id { get; set; }
            public string ProductId { get; set; }
            public string ImageLink { get; set; }
            public string Link { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string Brand { get; set; }
            public string ProductType { get; set; }
            public string GoogleProductCategory { get; set; }
            public bool? Approved { get; set; }
            public string Gtin { get; set; }
            public string Mpn { get; set; }
            public string CritalQualityIssue { get; set; }
        }

        public class MerchantConfig : BaseConfig
        {
            public override string ConfigDir { get; set; }
            internal override string ConfigFile { get; set; }

            [JsonProperty("merchantId")]
            public ulong? MerchantId { get; set; }

            [JsonProperty("accountSampleUser")]
            public string AccountSampleUser { get; set; }

            [JsonProperty("accountSampleAdWordsCID")]
            public ulong? AccountSampleAdWordsCID { get; set; }

            // The following fields are retrieved via the API after service setup in
            // BaseContentSample#initializeService().
            public bool IsMCA { get; set; }
            public string WebsiteURL { get; set; }

            public static MerchantConfig Load(String configPath)
            {
                MerchantConfig config;
                var contentPath = Path.Combine(configPath, "content");
                if (!Directory.Exists(contentPath))
                {
                    Console.WriteLine("Could not find configuration directory at " + contentPath);
                    throw new FileNotFoundException("Missing configuration directory");
                }

                var contentFile = Path.Combine(contentPath, "merchant-info.json");
                if (!File.Exists(contentFile))
                {
                    Console.WriteLine("No configuration file at " + contentFile);
                    config = new MerchantConfig();
                    config.ConfigDir = contentPath;
                    return config;
                }
                using (StreamReader reader = File.OpenText(contentFile))
                {
                    config = (MerchantConfig)JToken.ReadFrom(new JsonTextReader(reader))
                        .ToObject(typeof(MerchantConfig));
                    config.ConfigDir = contentPath;
                    config.ConfigFile = contentFile;
                }

                return config;
            }

        }

        public abstract class BaseConfig
        {
            public abstract string ConfigDir { get; set; }
            internal abstract string ConfigFile { get; set; }
        }

        public class GoogleService
        {
            MerchantConfig config;

            int maxListPageSize = 250;

            private ShoppingContentService service;
            ILog logger = LogManager.GetLogger(typeof(GoogleService));

            public GoogleService()
            {
                config = MerchantConfig.Load(Helper.MapPath("~/App_Data/Google/"));

                var initializer = Authenticator.Authenticate(config, ShoppingContentService.Scope.Content);
                if (initializer == null)
                {
                    throw new Exception("Failed to authenticate, so exiting.");
                }

                var init = new BaseClientService.Initializer()
                {
                    HttpClientInitializer = initializer,
                    ApplicationName = "17Life Samples",
                };
                service = new ShoppingContentService(init);
            }

            public List<GoogleShoppingProductResource> GetProducts()
            {

                List<GoogleShoppingProductResource> result = new List<GoogleShoppingProductResource>();
                logger.Debug("載入GoogleShopping產品基本資料");
                Dictionary<string, Google.Apis.ShoppingContent.v2.Data.Product> products = GetAllProducts(false);
                logger.Debug("載入GoogleShopping產品狀態資料");
                Dictionary<string, Google.Apis.ShoppingContent.v2.Data.ProductStatus> productStatuses = GetAllProductStatusList(false);


                foreach (var pair in products)
                {
                    if (productStatuses.ContainsKey(pair.Key) == false)
                    {
                        continue;
                    }
                    Google.Apis.ShoppingContent.v2.Data.Product p = pair.Value;
                    Google.Apis.ShoppingContent.v2.Data.ProductStatus ps = productStatuses[pair.Key];

                    GoogleShoppingProductResource gsp = new GoogleShoppingProductResource
                    {
                        Id = p.Id,
                        Title = p.Title,
                        Brand = p.Brand,
                        Description = p.Description,
                        Link = p.Link,
                        ImageLink = p.ImageLink,
                        ProductId = p.OfferId,
                        ProductType = p.ProductType,
                        GoogleProductCategory = p.GoogleProductCategory,
                        Gtin = p.Gtin,
                        Mpn = p.Mpn,
                    };

                    if (ps.DestinationStatuses[0].ApprovalPending.GetValueOrDefault() == false)
                    {
                        gsp.Approved = ps.DestinationStatuses[0].ApprovalStatus == "approved";
                    }
                    if (ps.DataQualityIssues != null)
                    {
                        var qualityIssue = ps.DataQualityIssues.FirstOrDefault(t => t.Severity == "critical");
                        if (qualityIssue != null)
                        {
                            gsp.CritalQualityIssue = qualityIssue.Id;
                        }
                    }
                    result.Add(gsp);
                }
                return result;
            }

            private Dictionary<string, Product> GetAllProducts(bool includeInvalidItems)
            {
                Dictionary<string, Product> result = new Dictionary<string, Product>();
                if (config.MerchantId == null)
                {
                    throw new Exception("無法取得MerchantId");
                }
                ulong merchantId = config.MerchantId.Value;
                // Retrieve account list in pages and display data as we receive it.
                string pageToken = null;
                ProductsListResponse productsResponse = null;

                do
                {
                    ProductsResource.ListRequest accountRequest = service.Products.List(merchantId);
                    accountRequest.IncludeInvalidInsertedItems = includeInvalidItems;
                    accountRequest.MaxResults = maxListPageSize;
                    accountRequest.PageToken = pageToken;

                    productsResponse = accountRequest.Execute();
                    foreach (Product item in productsResponse.Resources)
                    {
                        if (result.ContainsKey(item.OfferId))
                        {
                            throw new Exception("產品重複 " + item.OfferId);
                        }
                        result.Add(item.Id, item);
                    }

                    pageToken = productsResponse.NextPageToken;
                } while (pageToken != null);
                return result;
            }

            private Dictionary<string, ProductStatus> GetAllProductStatusList(bool includeInvalidItems)
            {
                Dictionary<string, ProductStatus> result = new Dictionary<string, ProductStatus>();
                if (config.MerchantId == null)
                {
                    throw new Exception("無法取得MerchantId");
                }
                ulong merchantId = config.MerchantId.Value;
                // Retrieve account list in pages and display data as we receive it.
                string pageToken = null;
                ProductstatusesListResponse productsResponse = null;

                do
                {
                    ProductstatusesResource.ListRequest accountRequest = service.Productstatuses.List(merchantId);
                    accountRequest.IncludeInvalidInsertedItems = includeInvalidItems;
                    accountRequest.MaxResults = maxListPageSize;
                    accountRequest.PageToken = pageToken;

                    productsResponse = accountRequest.Execute();
                    foreach (ProductStatus item in productsResponse.Resources)
                    {
                        if (result.ContainsKey(item.ProductId))
                        {
                            throw new Exception("產品重複 " + item.ProductId);
                        }
                        result.Add(item.ProductId, item);
                    }
                    pageToken = productsResponse.NextPageToken;
                } while (pageToken != null);
                return result;
            }
        }

        internal class Authenticator
        {
            private Authenticator() { }

            private static readonly string TOKEN_FILE = "stored-token.json";

            public static IConfigurableHttpClientInitializer Authenticate(
                BaseConfig config, string scope)
            {
                String[] scopes = new[] { scope };

                try
                {
                    GoogleCredential credential = GoogleCredential.GetApplicationDefaultAsync().Result;
                    logger.Debug("Using Application Default Credentials.");
                    return credential.CreateScoped(scopes);
                }
                catch (AggregateException)
                {
                    // Do nothing, we'll just let it slide and check the others.
                }

                if (config.ConfigDir == null)
                {
                    throw new ArgumentException(
                        "Must use Google Application Default Credentials when running without a "
                        + "configuration directory");
                }

                String oauthFilePath = Path.Combine(config.ConfigDir, "client-secrets.json");
                String serviceAccountPath = Path.Combine(config.ConfigDir, "service-account.json");

                try
                {
                    var test = JsonConvert.DeserializeObject(File.ReadAllText(serviceAccountPath));
                    logger.Info("parse ok");
                }
                catch (Exception ex)
                {
                    logger.Info("parse error", ex);
                }

                if (File.Exists(serviceAccountPath))
                {
                    logger.Debug("Loading service account credentials from " + serviceAccountPath);
                    using (FileStream stream = new FileStream(
                        serviceAccountPath, FileMode.Open, FileAccess.Read))
                    {
                        GoogleCredential credential = GoogleCredential.FromStream(stream);
                        return credential.CreateScoped(scopes);
                    }
                }
                else if (File.Exists(oauthFilePath))
                {
                    logger.Debug("Loading OAuth2 credentials from " + oauthFilePath);
                    using (FileStream oauthFile =
                        File.Open(oauthFilePath, FileMode.Open, FileAccess.Read))
                    {
                        var clientSecrets = GoogleClientSecrets.Load(oauthFile).Secrets;
                        var userId = "unused";
                        TokenResponse token = LoadToken(config, scope);
                        if (token != null)
                        {
                            logger.Debug("Loading old access token.");
                            try
                            {
                                var init = new GoogleAuthorizationCodeFlow.Initializer()
                                {
                                    ClientSecrets = clientSecrets,
                                    Scopes = scopes
                                };
                                var flow = new GoogleAuthorizationCodeFlow(init);
                                UserCredential storedCred = new UserCredential(flow, userId, token);
                                // Want to try and test and make sure we'll actually be able to
                                // use these credentials.
                                if (!storedCred.RefreshTokenAsync(CancellationToken.None).Result)
                                {
                                    throw new InvalidDataException();
                                }
                                return storedCred;
                            }
                            catch (InvalidDataException)
                            {
                                logger.Debug("Failed to load old access token.");
                                // Ignore, we'll just reauthenticate below.
                            }
                        }
                        UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                            clientSecrets, scopes, userId, CancellationToken.None).Result;
                        StoreToken(config, credential.Token);
                        return credential;
                    }
                }
                logger.Debug("Could not find authentication credentials. Checked:");
                logger.Debug(" - Google Application Default Credentials");
                logger.Debug(" - " + serviceAccountPath);
                logger.Debug(" - " + oauthFilePath);
                logger.Debug("Please read the included README for instructions.");
                return null;
            }

            public static TokenResponse LoadToken(BaseConfig config, string scope)
            {
                var tokenFile = Path.Combine(config.ConfigDir, TOKEN_FILE);
                if (!File.Exists(tokenFile))
                {
                    return null;
                }
                using (StreamReader sr = File.OpenText(tokenFile))
                using (JsonTextReader reader = new JsonTextReader(sr))
                {
                    JObject json = (JObject)JToken.ReadFrom(reader);
                    // Some serializations put an array of scopes into the token, but
                    // TokenResponse expects a string, so change appropriately.
                    json["scope"] = scope;
                    return (TokenResponse)json.ToObject(typeof(TokenResponse));
                }
            }

            public static void StoreToken(BaseConfig config, TokenResponse token)
            {
                var tokenFile = Path.Combine(config.ConfigDir, TOKEN_FILE);
                Newtonsoft.Json.JsonSerializer serializer = new Newtonsoft.Json.JsonSerializer();
                using (StreamWriter sw = new StreamWriter(tokenFile))
                using (JsonTextWriter writer = new JsonTextWriter(sw))
                {
                    writer.Formatting = Newtonsoft.Json.Formatting.Indented;
                    writer.IndentChar = ' ';
                    writer.Indentation = 2;
                    serializer.Serialize(writer, token);
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class SkmVerifyReplyJob : IJob
    {
        public void Execute(JobSetting js)
        {
            var start = DateTime.Now.AddHours(-24);
            var end = DateTime.Now;
            SkmFacade.SkmVerifyReplyRetry(start, end);
        }
    }
}

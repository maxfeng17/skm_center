﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Data;
using System.Net.Mail;
using System.IO;

namespace LunchKingSite.BizLogic.Jobs
{
    public class TrustReport : IJob
    {
        protected static ISysConfProvider config;
        protected static IPponProvider pp;
        protected static IMemberProvider mp;

        static TrustReport()
        {
            config = ProviderFactory.Instance().GetConfig();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public void Execute(JobSetting js)
        {
            //本月
            TrustVerificationReport report = mp.TrustVerificationReportGetLatest(TrustProvider.TaiShin, TrustVerificationReportType.CouponAndCash, true, TrustFlag.GenerateFile);

            if (!report.IsLoaded)
            {
                //MailMessage msg = new MailMessage();
                //msg.IsBodyHtml = true;
                //msg.From = new MailAddress(config.AdminEmail);
                //msg.To.Add(config.ItPAD);
                //msg.Subject = Environment.MachineName + ": [系統]本月信託未執行";
                //msg.Body = "請檢查sql排程";
                //PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                return;
            }
            //上月
            TrustVerificationReport report2 = mp.TrustVerificationReportGetLatest(TrustProvider.TaiShin);

            report.ScashIncrease = report.ScashIncrease ?? 0;
            report.ScashDecrease = report.ScashDecrease ?? 0;
            report.UserIncrease = report.UserIncrease ?? 0;
            report.UserDecrease = report.UserDecrease ?? 0;
            
            report.BeginningNonexchangeAmount = report2.TotalTrustAmount - report2.OverOneYearTotal;
            report.OverOneYearTotalOfLastMonth = report2.OverOneYearTotal;

            report.OverOneYearAmount = report.OverOneYearTotal - report.OverOneYearTotalOfLastMonth;

            report.ScashDecrease = report.ScashDecrease.Value + report.OverOneYearAmount;
            report.AmountDecrease = report.ScashDecrease.Value + report.UserDecrease.Value;
            report.ClosingNonexchangeAmount = report.BeginningNonexchangeAmount + (report.AmountIncrease - report.AmountDecrease);

           
            TrustVerificationReport MonthReport = TrustFacade.MonthTrustReport(report);

            DateTime start = report.ReportIntervalStart;
            DateTime end = report.ReportIntervalEnd;
            try
            {
                MailMessage msg = new MailMessage();
                msg.IsBodyHtml = true;
                msg.From = new MailAddress(config.AdminEmail);
                msg.To.Add(config.TrustReportEmail);
                msg.Subject = "[系統]" + start.Year.ToString() + "年" + start.Month.ToString() + "月份信託通知" + "(" + start.ToString("yyyy/MM/dd") + "~" + end.ToString("yyyy/MM/dd") + ")";
                //todo:改成templete?
                msg.Body = "Dear All:<br/><br/>結算區間為:<font color='blue'>" + start.ToString("yyyy/MM/dd HH:mm:ss") + "~" + end.ToString("yyyy/MM/dd HH:mm:ss") + "</font><br/><br/>" +
                    start.Year.ToString() + "年_" + start.Month.ToString() + "月份報表中期末未兌換餘額為:<br/>==================================================<br/><br/><table><tr><td>憑證(未滿一年).csv </td><td>總筆數<font color='blue'>" + MonthReport.ScashLessYearCount.ToString("#,##0") + "</font></td><td>總金額<font color='blue'>" + MonthReport.ScashLessYearAmount.ToString("#,##0") + "</font></td></tr><tr><td>購物金.csv </td><td>總筆數<font color='blue'>" + MonthReport.UserCount.ToString("#,##0") + "</font></td><td>總金額<font color='blue'>" + MonthReport.UserAmount.ToString("#,##0") + "</font></td></tr><tr><td colspan='3'><hr></td><tr/>" +
                    "<tr><td>總計</td><td style='text-align: right;'><font color='red'>" + (MonthReport.ScashLessYearCount + MonthReport.UserCount).ToString("#,##0") + "</font></td><td style='text-align: right;'><font color='red'>" + (MonthReport.ScashLessYearAmount + MonthReport.UserAmount).ToString("#,##0") + "</font></td></tr></table><br/><br/><br/>" +
                    start.Year.ToString() + "年_" + start.Month.ToString() + "月份憑證信託金額為:<br/>==================================================<br/><br/><table><tr><td>憑證(未滿一年).csv  </td><td>總筆數<font color='blue'>" + MonthReport.ScashLessYearCount.ToString("#,##0") + "</font></td><td>總金額<font color='blue'>" + MonthReport.ScashLessYearAmount.ToString("#,##0") + "</font></td></tr><tr><td>憑證(滿一年).csv </td><td>總筆數<font color='blue'>" + MonthReport.ScashOverYearCount.ToString("#,##0") + "</font></td><td>總金額<font color='blue'>" + MonthReport.ScashOverYearAmount.ToString("#,##0") + "</font></td></tr><tr><td colspan='3'><hr></td><tr/>" +
                    "<tr><td>總計</td><td style='text-align: right;'><font color='red'>" + (MonthReport.ScashLessYearCount + MonthReport.ScashOverYearCount).ToString("#,##0") + "</font></td><td style='text-align: right;'><font color='red'>" + (MonthReport.ScashLessYearAmount + MonthReport.ScashOverYearAmount).ToString("#,##0") + "</font></td></tr></table><br/><br/>" +
                    "等同 憑證.csv 總筆數<font color='blue'>" + (MonthReport.ScashLessYearCount + MonthReport.ScashOverYearCount).ToString("#,##0") + "</font>總金額<font color='blue'>" + (MonthReport.ScashLessYearAmount + MonthReport.ScashOverYearAmount).ToString("#,##0") + "</font><br/><br/>" +
                    "相關報表明細如附件";
                Attachment ach = new Attachment(new MemoryStream(MonthReport.ZipFile), start.ToString("yyyyMM") + "月份信託.zip");
                msg.Attachments.Add(ach);
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                report.Flag = (int)TrustFlag.SendReport;
                mp.TrustVerificationReportSet(report);
            }
            catch (Exception e)
            {
                throw (e);
            }
           
            
        }
    }
}

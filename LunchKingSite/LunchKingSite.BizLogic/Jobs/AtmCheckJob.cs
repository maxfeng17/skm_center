using System;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class AtmCheckJob : IJob
    {
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private ILog logger = LogManager.GetLogger(typeof(AtmCheckJob));
        public void Execute(JobSetting js)
        {
            int atmTodayCount = op.CtAtmGetTodayCount(DateTime.Now);
            if (atmTodayCount > 900)
            {
                logger.Warn("ATM超過1000即可能虛擬帳號重複, 目前數字為 " + atmTodayCount);
            }
        }
    }
}
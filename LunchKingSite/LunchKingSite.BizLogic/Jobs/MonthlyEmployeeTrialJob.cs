﻿using System;
using System.Net.Mail;
using LunchKingSite.Core;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System.Data;

namespace LunchKingSite.BizLogic.Jobs
{
    public class MonthlyEmployeeOfficialJob : IJob
    {
        private static IHumanProvider hp;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(DataIntegrityMonitor));

        static MonthlyEmployeeOfficialJob()
        {
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            #region 員工已過試用期狀態更新
            DateTime now = System.DateTime.Today;
            if (now.Day == 25)
            {
                int UpdateEmployeeOfficial = 0;
                try
                {
                    UpdateEmployeeOfficial = hp.updateEmployeeIsOfficial(3, true);
                    MailMessage msg = new MailMessage();
                    msg.To.Add(config.ItPAD);
                    msg.Subject = Environment.MachineName + ": 員工已過試用期狀態更新";
                    msg.From = new MailAddress(config.AdminEmail);
                    msg.IsBodyHtml = true;
                    msg.Body = "員工已過試用期狀態更新:" + UpdateEmployeeOfficial + "筆";
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
                catch (Exception ex)
                {
                    MailMessage msg = new MailMessage();
                    msg.To.Add(config.ItPAD);
                    msg.Subject = Environment.MachineName + ": 員工已過試用期狀態更新發生問題";
                    msg.From = new MailAddress(config.AdminEmail);
                    msg.IsBodyHtml = true;
                    msg.Body = ex.Message + ex.StackTrace;

                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    logger.Error("員工已過試用期狀態更新發生問題<br/>" + ex.StackTrace);
                }
            }
            #endregion
        }
    }
}

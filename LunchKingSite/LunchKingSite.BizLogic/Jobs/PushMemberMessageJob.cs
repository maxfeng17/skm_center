﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary>
    /// 在特訂時間內發送 queued 的會員個人推播訊息，避免深夜擾人。
    /// </summary>
    public class PushMemberMessageJob : IJob
    {
        IPponEntityProvider pep = ProviderFactory.Instance().GetDefaultProvider<IPponEntityProvider>();
        ILog logger = LogManager.GetLogger(typeof(PushMemberMessageJob));
        public void Execute(JobSetting js)
        {
            PushService ps = ProviderFactory.Instance().Resolve<PushService>(PushService._PERSON_PUSH_SERVICE);
            if (ps.IsStartUp == false)
            {
                logger.Warn("個人推播服務初始問題, PushMemberMessageJob 無法執行。");
            }
            var list = pep.GetQueuedMemberPushMessagePendingList();

            DateTime now = DateTime.Now;
            foreach(var item in list)
            {
                try
                {                   
                    var args = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.MessageData);
                    if (now.Hour < item.AcceptableStartHour || now.Hour > item.AcceptableEndHour)
                    {
                        continue;
                    }
                    item.Status = 1;
                    pep.QueuedMemberPushMessageSet(item);
                    ps.PushMemberMessage(item.UserId, item.Title, item.Message, item.MessageType, args, 
                        Facade.PushMethodType.Push | Facade.PushMethodType.Store);
                }
                catch (Exception ex)
                {
                    logger.Warn("推送個人訊息發生問題", ex);
                }

            }
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System;

namespace LunchKingSite.BizLogic.Jobs
{
    public class PponCouponSms :IJob
    {
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(PponCouponSms));

        public void Execute(JobSetting js)
        {
            DateTime now = DateTime.Now;
            //送出狀態為queue的簡訊
            try
            {
                SmsLogCollection smsLogs = pp.SMSLogGetQueue();
                SMS sender = new SMS();
                bool SmsToolate = false;
                foreach (var smsLog in smsLogs)
                {
                    //發送狀態為queue且未跟檔次綁定的簡訊)
                    if (smsLog.BusinessHourGuid == null)
                    {
                        if (smsLog.Type == (int)SmsType.Member)
                        {
                            //發送時間未到，先跳過
                            if (now.Hour < smsLog.AcceptableStartHour || now.Hour > smsLog.AcceptableEndHour)
                            {
                                continue;
                            }
                        }
                        else if ((DateTime.Now - smsLog.CreateTime.GetValueOrDefault()).TotalHours > 1)
                        {
                            SmsToolate = true;
                        }
                        sender.SendQueuedMessage(smsLog);
                    }
                    else
                    {
                        IViewPponDeal pponDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(smsLog.BusinessHourGuid.GetValueOrDefault());
                        if (pponDeal.IsLoaded == false)
                        {
                            continue;
                        }
                        PponDealStage dealStage = pponDeal.GetDealStage();
                        if ((dealStage > PponDealStage.Running) && (dealStage != PponDealStage.ClosedAndFail))
                        {
                            sender.SendQueuedMessage(smsLog);
                        }
                    }
                }
                if (SmsToolate)
                {
                    logger.Warn("queue的簡訊，在1小時內未獲得處理，請檢查");
                }
            }
            catch (Exception ex)
            {
                logger.Error("PponCouponSms Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
}
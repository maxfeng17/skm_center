﻿using log4net;
using LunchKingSite.BizLogic.Jobs.SkmSendNotification;
using LunchKingSite.BizLogic.Models.Skm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary>
    /// 傳送資料給SKM來發送推撥的功能
    /// 若要新增一種發送推撥的資料可以參考<see cref="SkmSendNotificationSample"/>
    /// </summary>
    public class SkmSendNotificationJob : IJob
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(SkmSendNotificationJob));
        private static readonly ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public SkmSendNotificationJob()
        {

        }
        public void Execute(JobSetting js)
        {
            DateTime startDate = DateTime.Today;
            DateTime endDate = DateTime.Today;
            try
            {
                List<SkmSendNotificationInfo> skmSendNotificationInfos;
                //尋找 指定DLL(LunchKingSite.BizLogic) 下的  指定繼承父類別(SkmSendNotificationBase) 的類別
                //並取得需要發送推撥的名單
                Assembly assembly = Assembly.Load("LunchKingSite.BizLogic");
                {
                    assembly.GetTypes().Where(item => item.IsSubclassOf(typeof(SkmSendNotificationBase)))
                        .ForEach(item => {
                            SkmSendNotificationBase skmSendNotification = (SkmSendNotificationBase)Activator.CreateInstance(Type.GetType(item.FullName), new object[] { logger });
                            skmSendNotificationInfos = skmSendNotification.GetNeedSendInfo();
                        });
                }
                //發送資料給SKM讓他們發送推撥
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
    }
}

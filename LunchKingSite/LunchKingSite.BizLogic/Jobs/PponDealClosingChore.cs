﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System.Net.Mail;
using System.Diagnostics;

namespace LunchKingSite.BizLogic.Jobs
{
    public class PponDealClosingChore : IJob
    {
        private static IPponProvider _pponProv;
        private static ISysConfProvider _config;
        private static ILog log = LogManager.GetLogger(typeof(PponDealClosingChore));
        private static PponDealClosingChore _theJob;
        private PponDealClose _dealClose; 

        static PponDealClosingChore()
        {
            _theJob = new PponDealClosingChore();
        }

        public static PponDealClosingChore Instance()
        {
            return _theJob;
        }

        private PponDealClosingChore()
        {
            _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _dealClose = new PponDealClose();
        }

        public void Execute(JobSetting js)
        {

            SendMail("PponDealClosingChore is running.");
            try
            {
                var timer = new Stopwatch();
                timer.Start();


                var now = DateTime.Now;
                // getting deals that ends within 10 minutes
                var deals = _pponProv.ViewPponDealGetListPaging(1, -1, ViewPponDeal.Columns.BusinessHourOrderTimeE,
                    ViewPponDeal.Columns.BusinessHourOrderTimeE + " between " + now.AddMinutes(-10).ToString("yyyy/MM/dd HH:mm") + " and " + now.ToString("yyyy/MM/dd HH:mm"),
                    ViewPponDeal.Columns.Slug + " is null")
                    .ToList();

                //check 是否前一天是否有應結檔而未結檔之檔次須處理
                string mode = js.Parameters["mode"] != null ? js.Parameters["mode"] : "";
                if (mode == "ExpiredDealClose")
                {
                    deals.AddRange(_pponProv.ViewPponDealGetListPaging(1, -1, ViewPponDeal.Columns.BusinessHourOrderTimeE,
                    ViewPponDeal.Columns.BusinessHourOrderTimeE + " between " + DateTime.Today.AddYears(-1).ToString("yyyy/MM/dd HH:mm") + " and " + now.AddMinutes(-10).ToString("yyyy/MM/dd HH:mm"),
                    ViewPponDeal.Columns.Slug + " is null")
                    .ToList());

                }

                //排除skm
                deals = deals.Where(x => !Helper.IsFlagSet(x.GroupOrderStatus ?? 0, GroupOrderStatus.SKMDeal)).ToList();

                // nothihng to do
                if (deals == null || deals.Count <= 0) return;

                List<DealCloseResult> results = _dealClose.CloseDeal(from x in deals.Distinct() where (x.GetDealStage() == PponDealStage.ClosedAndOn || x.GetDealStage() == PponDealStage.ClosedAndFail || x.GetDealStage() == PponDealStage.CouponGenerated) select x, "System");

                timer.Stop();
                SendMail("共執行" + timer.Elapsed.ToString());


            }
            catch (Exception ex)
            {
                log.Error("PponDealClosingChore Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

        private void SendMail(string content)
        {
            try
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(_config.AdminEmail);
                msg.Subject = Environment.MachineName + ": [系統]PponDealClosing Start Running";
                msg.From = new MailAddress(_config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = content;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                log.Error("PponDealClosingChore running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
}

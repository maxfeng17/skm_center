﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

/*
 * 展演/展覽票券，於兌換期後三個工作天，進行「過期核銷」
 * */
namespace LunchKingSite.BizLogic.Jobs
{
    public class ExhibitionDealExpiredVerified : IJob
    {
        private static IOrderProvider op;
        private static IPponProvider pp;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(ExhibitionDealExpiredVerified));
        private StringBuilder builder = new StringBuilder();
        private int effectiveCount = 0;
        static ExhibitionDealExpiredVerified()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                builder.AppendLine(Environment.MachineName + ": 展演/展覽檔次強制核銷資料準備");
                ExpiredOrderExpiredVerified();

                builder.AppendLine("本次處理筆數：" + effectiveCount.ToString());

                #region 寄發處理結果

                if (effectiveCount > 0)
                {
                    MailMessage msg = new MailMessage();
                    msg.To.Add(config.AdminEmail);
                    msg.Subject = Environment.MachineName + ": 展演/展覽檔次強制核銷";
                    msg.From = new MailAddress(config.AdminEmail);
                    msg.Body = builder.ToString();
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
                

                #endregion 寄發處理結果
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = Environment.MachineName + ": ExhibitionDealExpiredVerified Error";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;
                if (builder.Length > 0)
                {
                    msg.Body = string.Format("{0}\r\n\r\n錯誤訊息: {2}{3}",
                        builder,
                        ex.Message,
                        ex.StackTrace);
                }

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error(ex);
            }
            finally
            {
                ExpiredVerifiedLog log = new ExpiredVerifiedLog()
                {
                    ContentLog = builder.ToString(),
                    CreateTime = DateTime.Now
                };
                op.ExpiredVerifiedLogSet(log);
            }

        }

        private void ExpiredOrderExpiredVerified()
        {
            var timer = new Stopwatch();
            timer.Start();

            //處理過期的檔次訂單 轉進退貨單中 (到期日在2015/5/1後的檔次始適用)
            OrderCollection oCol = op.GetExpriedOrderListOverThreeDays();


            var timeStampGetExpiredOrder = timer.Elapsed;
            StringBuilder sb = new StringBuilder();

            foreach (Order o in oCol)
            {
                IEnumerable<CashTrustLog> ctls =
                    OrderFacade.GetExpiredVerifiedCouponCtlogs(o.Guid, BusinessModel.Ppon);

                foreach (CashTrustLog ctl in ctls)
                {
                    var bid = ctl.BusinessHourGuid;
                    if (bid != null && bid != Guid.Empty)
                    {
                        DealProperty dp = pp.DealPropertyGet(bid.Value);
                        /*
                         * 1. 憑證檔次
                         * 2. 展演類型檔次-鎖定功能(逾期自動核銷) 或 展覽類型檔次(逾期自動核銷)  被勾起      
                         * */
                        if (dp.DeliveryType == (int)DeliveryType.ToShop
                            && (dp.IsPromotionDeal || dp.IsExhibitionDeal)
                            )
                        {
                            VerificationStatus s = OrderFacade.VerifyCoupon(ctl.TrustId, false, "sys");
                            effectiveCount++;
                            if(s == VerificationStatus.CouponOk)
                            {
                                //audit(string refId, AuditType type, string msg, string createId, bool commonVisible)
                                string w = "展演憑證";
                                if (dp.IsExhibitionDeal)
                                {
                                    w = "展覽憑證";
                                }
                                CommonFacade.AddAudit(o.Guid, AuditType.Order, w + "逾期未使用，系統逾期核銷/憑證編號:" + ctl.CouponSequenceNumber, "sys", true);

                            }
                            sb.AppendLine("OrderGuid:" + ctl.OrderGuid  + " - TrustId:" + ctl.TrustId + " - CouponSequenceNumber:" + ctl.CouponSequenceNumber + " - VerificationStatus:" + (s == VerificationStatus.CouponOk ? "成功" : "失敗「" + s.ToString() + "」"));
                        }
                        else
                        {
                            sb.AppendLine("OrderGuid:" + ctl.OrderGuid + " - TrustId:" + ctl.TrustId + " - CouponSequenceNumber:" + ctl.CouponSequenceNumber + " - 非展演檔次");
                        }
                    }
                }
            }
            var timeStampCreateReturnForm = timer.Elapsed.Subtract(timeStampGetExpiredOrder);

            timer.Stop();
            builder.AppendLine(string.Format(@"
                                ExpiredOrderExpiredVerified 處理總時間花費: {0:g} ,
                                撈取過期檔次訂單: {1:g} 
                                進行「過期核銷」： {2:g} 
                                核銷資訊：  {3}",
                                timer.Elapsed,
                                timeStampGetExpiredOrder,
                                timeStampCreateReturnForm,
                                sb.ToString()));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    class SkmOrderPushReminderSetInAppJob : IJob
    {

        private static readonly ILog logger = LogManager.GetLogger(typeof(SkmOrderPushReminderSetInAppJob));

        public void Execute(JobSetting js)
        {
            try
            {
                var messages = SkmFacade.PrePushDataInAppMessage();
                SkmFacade.SaveSkmInAppMessageCollection(messages);
            }
            catch (Exception ex)
            {
                logger.Error("SkmOrderPushReminderSetInAppJob Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
}

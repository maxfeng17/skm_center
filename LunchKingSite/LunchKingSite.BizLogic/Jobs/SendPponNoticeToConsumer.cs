﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Mail;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using log4net;

namespace LunchKingSite.BizLogic.Jobs
{
    public class SendPponNoticeToConsumer : IJob
    {
        private static IPponProvider _pponProv;
        private static IMemberProvider _memberProv;
        private static IBookingSystemProvider _bookingProvider;
        private static ISysConfProvider _config;
        private static ILog log = LogManager.GetLogger(typeof(PponDealChore));
        private static readonly EmailAgent agent = new EmailAgent();
        private static SendPponNoticeToConsumer _theJob;

        static SendPponNoticeToConsumer()
        {
            _theJob = new SendPponNoticeToConsumer();
        }

        public static SendPponNoticeToConsumer Instance()
        {
            return _theJob;
        }

        private SendPponNoticeToConsumer()
        {
            _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _memberProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _bookingProvider = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            
        }

        public void Execute(JobSetting js)
        {
            try
            {
                SendToCounsumerMail();
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(_config.AdminEmail);
                msg.Subject = "SendPponSubscribeMail Error";
                msg.From = new MailAddress(_config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        private void SendToCounsumerMail()
        {
            DateTime today = DateTime.Today;
            ViewPponDealCollection vpdc = _pponProv.ViewPponDealGetListByDeliverTimeStart(DateTime.Today);

            //Parallel.ForEach(vpdc, vpd =>
            foreach (ViewPponDeal vpd in vpdc)
            {
                if (vpd.ItemPrice > 0)
                {
                    ViewPponStoreCollection stores = _pponProv.ViewPponStoreGetListByBidWithNoLock(vpd.BusinessHourGuid, VbsRightFlag.VerifyShop);
                    foreach (var sotre in stores)
                    {
                        string changedExpireDate = CouponFacade.GetFinalExpireDateContent(Guid.Empty, vpd.BusinessHourGuid, sotre.StoreGuid);
                        DateTime start = Convert.ToDateTime(vpd.BusinessHourDeliverTimeS);
                        DateTime end = !string.IsNullOrEmpty(changedExpireDate) ? Convert.ToDateTime(changedExpireDate) : Convert.ToDateTime(vpd.BusinessHourDeliverTimeE);
                        TimeSpan ts = new TimeSpan(end.Ticks - start.Ticks);
                        TimeSpan tsWithStart = new TimeSpan(today.Ticks - start.Ticks);
                        TimeSpan tsWithEnd = new TimeSpan(end.Ticks - today.Ticks);
                        //先判斷兌換區間
                        //  1. 超過3個月(91天以上)
                        //  2. 介於六周至3個月之間(43~90天之間)
                        //  3. 六周以內(42天以內)
                        MailInfo mailInfo = new MailInfo();
                        try
                        {
                            if (ts.TotalDays > 90)
                                mailInfo = ActGreaterThenOrEqualTime(vpd, tsWithStart, tsWithEnd, end);
                            else if (ts.TotalDays > 42 && ts.TotalDays <= 90)
                                mailInfo = ActWithinTime(vpd, tsWithStart, tsWithEnd, end);
                            else if (ts.TotalDays <= 42)
                                mailInfo = ActSmallerThanOrEqualTime(vpd, tsWithStart, tsWithEnd, end);
                        }
                        catch (Exception e)
                        {
                            log.Warn("好康使用通知 - 取得 Template Error (EmailFacade.SendToCounsumerMail):" + e);
                        }

                        //根據每一檔取出該檔有下訂單者之記錄，
                        if (mailInfo.HasValue)
                        {
                            try
                            {
                                DataTable dtMembers = _memberProv.GetMemberListByDepartmentAndBusinessHourAndCouponUsageExcludeRefunding(DeliveryType.ToShop, vpd.BusinessHourGuid, sotre.StoreGuid, 0);
                                List<string> invalidEmails = new List<string>();
                                foreach (DataRow dr in dtMembers.Rows)
                                {
                                    string userName = dr["member_email"].ToString();
                                    var email = MemberFacade.GetUserEmail(userName);
                                    if (RegExRules.CheckEmail(email))
                                    {
                                        PostMan.Instance().Send(
                                        agent.GetMailMessage(_config.ServiceEmail
                                                          , email
                                                          , mailInfo.Subject
                                                          , mailInfo.Template
                                                                .Replace("{member_name}", dr["member_name"].ToString())
                                                                .Replace("{order_detail_item_name}", dr["item_name"].ToString())
                                                          , _config.ServiceName
                                            ), SendPriorityType.Immediate);
                                    }
                                    else
                                    {
                                        invalidEmails.Add(email);
                                        continue;
                                    }
                                }
                                if (invalidEmails.Count > 0)
                                {
                                    PostMan.Instance().Send(agent.GetMailMessage(
                                        _config.SystemEmail
                                        , _config.CsManagerEmail
                                        , "[好康使用通知]信件Email地址有誤"
                                        , string.Format("主旨[{0}]<br/>下列Email無法寄送，請聯繫會員:<br>{1}", mailInfo.Subject, string.Join("<br>", invalidEmails))
                                    ), SendPriorityType.Immediate);
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Warn("好康使用通知 - 寄送 Error (EmailFacade.SendToCounsumerMail):" + ex);
                            }
                        }
                    }
                }
            };


            //預約提醒通知信
            var reservationList =  _bookingProvider.ViewBookingSystemReservationListGet(today.AddDays(3).Date);

            Parallel.ForEach(reservationList, reservation =>
                                                {
                                                    if (reservation.BookingType==(int)BookingType.Coupon && !reservation.IsCancel && !reservation.IsCheckin && !string.IsNullOrEmpty(reservation.ContactEmail))
                                                    {
                                                        var coupons = _bookingProvider.BookingSystemCouponGetListByReservationId(reservation.BookingSystemReservationId);
                                                        var couponSequences = new List<string>();
                                                        foreach (var coupon in coupons)
                                                        {
                                                            couponSequences.Add(coupon.Prefix+coupon.CouponSequenceNumber);
                                                        }

                                                        var reservationRemindInfo = new ReservationRemindMailInfomation
                                                        {
                                                            MemberName = reservation.ContactName +" "+((reservation.ContactGender)?"先生":"小姐"),
                                                            ReservationDate = reservation.ReservationDate,
                                                            ReservationTimeSlot = reservation.ReservationDate.SetTime(int.Parse(reservation.TimeSlot.Substring(0, 2)), int.Parse(reservation.TimeSlot.Substring(3, 2)), 0),
                                                            NumberOfPeople = reservation.NumberOfPeople,
                                                            MemberEmail = reservation.ContactEmail,
                                                            ContactName = reservation.ContactName,
                                                            ContactNumber = reservation.ContactNumber,
                                                            Remark = reservation.Remark,
                                                            CouponsSequence = couponSequences,
                                                            CouponInfo = reservation.CouponInfo,
                                                            CouponLink = reservation.CouponLink,
                                                            SellerName = reservation.SellerName,
                                                            StoreName = reservation.StoreName,
                                                            StoreAddress = reservation.AddressString
                                                        };
                                                        BookingSystemFacade.SendReservationRemindMail(reservation.ContactEmail, reservationRemindInfo);
                                                    }
                                                });
        }

        #region 超過三個月(90天)
        private MailInfo ActGreaterThenOrEqualTime(ViewPponDeal vpd, TimeSpan tsStart, TimeSpan tsEnd, DateTime end)
        {
            MailInfo result = new MailInfo();
            if (tsEnd.TotalDays > 30) //至結束前一個月
            {
                if ((int)tsStart.TotalDays % 21 == 0 && tsStart.TotalDays > 20)   //開跑後隔三周(三周、六周、九周依此類推)，取得"使用"提醒樣版
                {
                    result.Subject = string.Format("好康使用提醒通知({0}，將於{1}到期，請儘速使用)", vpd.ItemName, end.ToString("yyyy/MM/dd"));
                    result.Template = GetPponToConsumerWarningMail(vpd, end);
                }
            }
            else if (((int)tsEnd.TotalDays).EqualsAny(7, 30))  //結束前1週、一個月，取得"到期"提醒樣版
            {
                result.Subject = string.Format("好康即將到期通知({0}，將於{1}到期，請儘速使用)", vpd.ItemName, end.ToString("yyyy/MM/dd"));
                result.Template = GetPponToConsumerExpiredMail(vpd, end);
            }
            return result;
        }
        #endregion

        #region 介於六周至三個月之間(43~90天)
        private MailInfo ActWithinTime(ViewPponDeal vpd, TimeSpan tsStart, TimeSpan tsEnd, DateTime end)
        {
            MailInfo result = new MailInfo();

            if ((int)tsStart.TotalDays == 21)    //開始後3週，取得"使用"提醒樣版
            {
                result.Subject = string.Format("好康使用提醒通知({0}，將於{1}到期，請儘速使用)", vpd.ItemName, end.ToString("yyyy/MM/dd"));
                result.Template = GetPponToConsumerWarningMail(vpd, end);
            }

            if (((int)tsEnd.TotalDays).EqualsAny(7, 14))   //結束前1、2週，取得"到期"提醒樣版
            {
                result.Subject = string.Format("好康即將到期通知({0}，將於{1}到期，請儘速使用)", vpd.ItemName, end.ToString("yyyy/MM/dd"));
                result.Template = GetPponToConsumerExpiredMail(vpd, end);
            }
            return result;
        }
        #endregion

        #region 六周以內(42天)
        private MailInfo ActSmallerThanOrEqualTime(ViewPponDeal vpd, TimeSpan tsStart, TimeSpan tsEnd, DateTime end)
        {
            MailInfo rtnDic = new MailInfo();

            if ((int)tsStart.TotalDays == 7)    //開始後1週，取得"使用"提醒樣版
            {
                rtnDic.Subject = string.Format("好康使用提醒通知({0}，將於{1}到期，請儘速使用)", vpd.ItemName, end.ToString("yyyy/MM/dd"));
                rtnDic.Template = GetPponToConsumerWarningMail(vpd, end);
            }

            if ((int)tsEnd.TotalDays == 7)   //結束前1週，取得"到期"提醒樣版
            {
                rtnDic.Subject = string.Format("好康即將到期通知({0}，將於{1}到期，請儘速使用)", vpd.ItemName, end.ToString("yyyy/MM/dd"));
                rtnDic.Template = GetPponToConsumerExpiredMail(vpd, end);
            }
            return rtnDic;
        }
        #endregion

        #region 好康到期提醒
        private string GetPponToConsumerExpiredMail(ViewPponDeal vpd, DateTime end)
        {
            string rtnTemp = string.Empty;
            try
            {
                PponToConsumerExpiredMail template = TemplateFactory.Instance().GetTemplate<PponToConsumerExpiredMail>();
                template.TheMemberName = "{member_name}";
                template.ItemName = "{order_detail_item_name}";
                template.TheDeal = vpd;
                template.PromoDeals = OrderFacade.GetPromoDealsForNoticeMail(vpd);
                template.ServerConfig = _config;
                template.EndTime = end;
                rtnTemp = template.ToString();
            }
            catch (Exception e)
            {
                log.Warn("好康到期提醒通知 Error:" + e);
            }
            return rtnTemp;
        }
        #endregion

        #region 好康使用提醒
        private string GetPponToConsumerWarningMail(ViewPponDeal vpd, DateTime end)
        {
            string rtnTemp = string.Empty;
            try
            {
                PpontoConsumerWarningMail template = TemplateFactory.Instance().GetTemplate<PpontoConsumerWarningMail>();
                template.TheMemberName = "{member_name}";
                template.TheDeal = vpd;
                template.ItemName = vpd.ItemName;
                template.EndTime = end;
                rtnTemp = template.ToString();
            }
            catch (Exception e)
            {
                log.Warn("好康使用提醒通知 Error:" + e);
            }
            return rtnTemp;
        }
        #endregion

        #region 預約提醒通知

        private string GetBookingReservationRemindMail()
        {
            return string.Empty;
        }


        #endregion 預約提醒通知

        internal class MailInfo
        {
            public MailInfo()
            {
                Subject = "";
                Template = "";
            }

            public bool HasValue
            {
                get { return string.IsNullOrEmpty(Subject) == false; }
            }

            public string Subject { get; set; }
            public string Template { get; set; }
        }
    }
}

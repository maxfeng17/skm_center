﻿using System;
using System.Net.Mail;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Jobs
{
    public class GroupOrderNotificationMailer : IJob
    {
        protected static ISysConfProvider config;
        protected static IOrderProvider op;
        protected static ISellerProvider sp;
        protected static IMemberProvider mp;

        static GroupOrderNotificationMailer()
        {
            config = ProviderFactory.Instance().GetConfig();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public void Execute(JobSetting js)
        {
            // 2. (close_time < <% =DateTime.Now%> ) 和 (grouporder_status = 2)  
            GroupOrderCollection orderlist = op.GroupOrderGetList(GroupOrder.Columns.Status + " = 2 AND "
                + GroupOrder.Columns.CloseTime + " < CURRENT_TIMESTAMP AND DATEDIFF(minute,"
                + GroupOrder.Columns.CloseTime + ",CURRENT_TIMESTAMP) > 5 AND "
                + GroupOrder.Columns.MemberUserName + "<>'" + config.ServiceEmail + "'", GroupOrder.Columns.CloseTime);

            foreach (var ord in orderlist)
            {
                // 3. Send mail
                MailMessage msg = new MailMessage();
                GroupOrderNotification template = TemplateFactory.Instance().GetTemplate<GroupOrderNotification>();
                template.User = MemberFacade.GetMember(ord.MemberUserName).DisplayName;
                Seller s = sp.SellerGet(sp.BusinessHourGet(ord.BusinessHourGuid).SellerGuid);
                if (s.Department == (int)DepartmentTypes.Ppon)
                    continue;
                template.SellerName = s.SellerName;
                template.SimpGrpOrdLink = config.SiteUrl + SellerFacade.GetDepartmentPath((DepartmentTypes)sp.SellerGet(sp.BusinessHourGet(ord.BusinessHourGuid).SellerGuid).Department)
                                        + "/menu.aspx?gid=" + ord.Guid;
                template.CheckOutLink = config.SiteUrl + SellerFacade.GetDepartmentPath((DepartmentTypes)sp.SellerGet(sp.BusinessHourGet(ord.BusinessHourGuid).SellerGuid).Department)
                                        + "/buy.aspx?gid=" + ord.Guid;
                template.GOManageLink = config.SiteUrl + "/user/goman.aspx";
                template.CreatTime = ord.CreateTime.ToString("yyyy/MM/dd");
                template.OrderMinimum = Convert.ToInt32(sp.BusinessHourGet(ord.BusinessHourGuid).BusinessHourOrderMinimum);
                template.OrderTotal = Convert.ToInt32(op.OrderGet((Guid)ord.OrderGuid).Total);
                msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                msg.To.Add(ord.MemberUserName);
                msg.Subject = "您的團購已過截止時間";
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);

                // 4. update [grouporder] set order_status=(order_status|0)  
                ord.Status = null;
                op.GroupOrderSet(ord);
            } // end foreach     
        }
    }
}
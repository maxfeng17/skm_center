﻿using System;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Runtime.CompilerServices;

namespace LunchKingSite.BizLogic.Jobs
{
    public class ScheduleRuntimeConfigJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(LineAppender._LINE);

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Execute(JobSetting js)
        {
            DateTime now = DateTime.Now;
            string filePath = Path.Combine(Helper.MapPath("~/share/App_Data/"), "schedule-runtime-config.json");
            List<RuntimeScheduleConfigItem> items = null;
            try
            {
                string json = File.ReadAllText(filePath);
                items = ProviderFactory.Instance().GetSerializer().Deserialize<List<RuntimeScheduleConfigItem>>(json);
            }
            catch (Exception ex)
            {
                logger.InfoFormat("{0} 無法讀取或解析 {1}, 錯誤原因 {2}",
                    Environment.MachineName,
                    filePath, ex.Message);
                return;
            }
            if (items == null)
            {
                return;
            }
            bool anyChanged = false;
            foreach (var item in items)
            {
                if (now.IsBetween(item.StartTime, item.EndTime)
                    && string.IsNullOrEmpty(item.ConfigKey) == false
                    && item.Done.GetValueOrDefault() == false)
                {
                    string resultLog = string.Empty;
                    item.Done = SystemFacade.SetAllServersRuntimeConfig(item.ConfigKey, item.ConfigValue, out resultLog);
                    item.Message = resultLog;
                    anyChanged = true;
                    string message = string.Format("{0}排程更新即時設定: {1} 為 {2}",
                        (bool)item.Done ? string.Empty : "更新失敗，", item.ConfigKey, item.ConfigValue);
                    logger.InfoFormat(message);
                }
            }
            if (anyChanged)
            {
                string json = ProviderFactory.Instance().GetSerializer().Serialize(items, true);
                File.WriteAllText(filePath, json);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public class RuntimeScheduleConfigItem
        {
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("startTime")]
            public DateTime StartTime { get; set; }
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("endTime")]
            public DateTime EndTime { get; set; }
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("configKey")]
            public string ConfigKey { get; set; }
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("configValue")]
            public string ConfigValue { get; set; }

            [JsonProperty("done")]
            public bool? Done { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }
        }
    }
}

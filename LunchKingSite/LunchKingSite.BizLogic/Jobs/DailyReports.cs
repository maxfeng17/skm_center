﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System.Net.Mail;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component.Template;

namespace LunchKingSite.BizLogic.Jobs
{
    public class DailyReports : IJob
    {
        protected static IOrderProvider op;
        protected static ISellerProvider sp;
        protected static IPponProvider pp;
        protected static ISysConfProvider config;

        static DailyReports()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            #region 檢查未填寫出貨回覆日的商品
            int days = 14;
            DataTable dtS = pp.CheckShippingDate();
            if (dtS.Rows.Count > 0 && DateTime.Now.Subtract(Convert.ToDateTime(dtS.Rows[0][3])).Days > days)
            {
                try
                {
                    MailMessage msg = new MailMessage();
                    msg.IsBodyHtml = true;
                    msg.From = new MailAddress(config.SystemEmail);
                    msg.CC.Add(config.FinanceEmail);
                    msg.Subject = Environment.MachineName + ": [系統]出貨回覆日未填超過" + days.ToString() + "天";
                    msg.Body = "共" + dtS.Rows.Count.ToString() + "筆檔次未填寫出貨回覆日。報表連結如下:<br/>http://batman.17life.com/Reports/Pages/Report.aspx?ItemPath=/Operating/shipping_date_unfilled";

                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
                catch (Exception e)
                {
                    throw (e);
                }
            }
            #endregion

            #region 檢查匯款資料完整性

            var deliveryTypes = new List<int> { (int) DeliveryType.ToShop, (int) DeliveryType.ToHouse };

            foreach (var deliveryType in deliveryTypes)
            {
                var dt = op.WeeklyPayMonitorJob(deliveryType);
                var error = string.Empty;
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        if (r["paytocompany"].ToString() == "1")
                        {
                            if (string.IsNullOrEmpty(r["account_no"].ToString()) ||
                                string.IsNullOrEmpty(r["bank_no"].ToString()) ||
                                string.IsNullOrEmpty(r["branch_no"].ToString()) ||
                                string.IsNullOrEmpty(r["account_id"].ToString()))
                            {
                                error += "上檔日期=" + r["stime"].ToString() + ", 結檔日期=" + r["etime"].ToString() + ", BID=" + r["guid"].ToString() +
                                         ", 檔名=" + r["seller_name"].ToString() + ", 地區=" + r["district"].ToString() + ", 業務=" + r["sales_id"].ToString() + " 資料有缺,<br/>";
                            }
                        }
                        else if (r["paytocompany"].ToString() == "0")
                        {
                            var sc = SellerFacade.GetPponStoreSellersByBId(new Guid(r["guid"].ToString()));
                            if (!sc.Any())
                            {
                                error += "上檔日期=" + r["stime"].ToString() + ", 結檔日期=" + r["etime"].ToString() + ", BID=" + r["guid"].ToString() +
                                         ", 檔名=" + r["seller_name"].ToString() + ", 地區=" + r["district"].ToString() + ", 業務=" + r["sales_id"].ToString() + " 無分店,<br/>";
                            }
                            else
                            {
                                var storeGuids = sc.Select(x => x.Guid);
                                var storeAccounts = op.WeeklyPayAccountGetList(new Guid(r["guid"].ToString()))
                                                        .Where(x => x.StoreGuid.HasValue && storeGuids.Contains(x.StoreGuid.Value))
                                                        .ToDictionary(x => x.StoreGuid.Value, x => x);
                                foreach (var s in sc)
                                {
                                    if (s.StoreStatus == (int)StoreStatus.Available && 
                                        (!storeAccounts.Any() ||
                                         !storeAccounts.ContainsKey(s.Guid) ||
                                         string.IsNullOrEmpty(storeAccounts[s.Guid].AccountNo) ||
                                         string.IsNullOrEmpty(storeAccounts[s.Guid].AccountId) ||
                                         string.IsNullOrEmpty(storeAccounts[s.Guid].BankNo) ||
                                         string.IsNullOrEmpty(storeAccounts[s.Guid].BranchNo)))
                                    {
                                        error += "上檔日期=" + r["stime"].ToString() + ", 結檔日期=" + r["etime"].ToString() + ", BID=" + r["guid"].ToString() +
                                                 ", 檔名=" + r["seller_name"].ToString() + ", 地區=" + r["district"].ToString() + ", 業務=" + r["sales_id"].ToString() + ", STORE=" + s.SellerName + " 分店資料有缺,<br/>";
                                    }
                                }
                            }
                        }
                    }
                }

                if (string.IsNullOrEmpty(error)) continue;
                try
                {
                    var msgStart = new MailMessage();
                    msgStart.IsBodyHtml = true;
                    msgStart.From = new MailAddress(config.SystemEmail);
                    msgStart.To.Add(config.FinanceEmail);
                    msgStart.To.Add(config.MhdEmail);
                    msgStart.To.Add(deliveryType == (int)DeliveryType.ToShop 
                                    ? config.SalesSpecialAssistantEmail
                                    : config.SalesSpecialAssistantShippingEmail);
                    msgStart.Subject = string.Format("[系統]{0}檔次匯款資料有缺", deliveryType == (int)DeliveryType.ToShop ? "憑證" : "宅配");
                    msgStart.Body = string.Format(@"在 {0} 時發現了一週內上檔之匯款資料有缺，還請協助處理<br/> 
                                      {1}<br/>提醒您! 以上店家匯款資料輸入不完全，請相關人員注意~ 謝謝!", DateTime.Now, error);
                    PostMan.Instance().Send(msgStart, SendPriorityType.Immediate);
                }
                catch (Exception e)
                {
                    MailMessage msgStart = new MailMessage();
                    msgStart.From = new MailAddress(config.SystemEmail);
                    msgStart.Subject = "Error";
                    msgStart.Body = e.Message + "<br/>" + e.StackTrace.ToString();
                    PostMan.Instance().Send(msgStart, SendPriorityType.Immediate);
                }
            }

            #endregion

            //#region Fami更新
            //try
            //{
            //    int total = OrderFacade.FamiUpdatePrintDate(DateTime.Now);
            //    MailMessage famimsg = new MailMessage();
            //    famimsg.IsBodyHtml = true;
            //    famimsg.From = new MailAddress(config.AdminEmail);
            //    famimsg.To.Add(config.AdminEmail);
            //    famimsg.Subject = Environment.MachineName + ": [系統]Fami更新每日列印";
            //    famimsg.Body = "已更新" + total + "筆";
            //    PostMan.Instance().Send(famimsg, SendPriorityType.Immediate);
            //}
            //catch (Exception errormessage)
            //{
            //    MailMessage famimsg = new MailMessage();
            //    famimsg.IsBodyHtml = true;
            //    famimsg.From = new MailAddress(config.AdminEmail);
            //    famimsg.To.Add(config.AdminEmail);
            //    famimsg.Subject = Environment.MachineName + ": [系統]Fami更新每日列印發生錯誤";
            //    famimsg.Body = errormessage.Message;
            //    PostMan.Instance().Send(famimsg, SendPriorityType.Immediate);
            //}
            //#endregion

            #region 特殊退貨方式-退貨期限截止通知信

            ReturnInformClass returnObj = new ReturnInformClass();
            try
            {
                //不可退貨 (結檔隔日不可退)  
                List<ViewPponDeal> vpdNoRefund = pp.ViewPponDealGetListByNoReturn(DateTime.Now.Date).Where(x => x.GroupOrderStatus != null && Helper.IsFlagSet((int)x.GroupOrderStatus, (int)GroupOrderStatus.NoRefund)).ToList();
                //過期不可退貨 (配送結束日隔日不可退)
                List<ViewPponDeal> vpdExpireNoRefund = pp.ViewPponDealGetListByExpireNoReturn(DateTime.Now.Date).Where(x => x.GroupOrderStatus != null && Helper.IsFlagSet((int)x.GroupOrderStatus, (int)GroupOrderStatus.ExpireNoRefund)).ToList();
                //結檔後 7日的隔日不可退貨
                List<ViewPponDeal> vpdDaysNoRefund = pp.ViewPponDealGetListPaging(0, 0, string.Empty,
                    ViewPponDeal.Columns.BusinessHourOrderTimeE + " >= " + DateTime.Now.AddDays(-config.GoodsAppreciationPeriod - 1).Date.ToShortDateString(),
                    ViewPponDeal.Columns.BusinessHourOrderTimeE + " < " + DateTime.Now.AddDays(-config.GoodsAppreciationPeriod).Date.ToShortDateString()).ToList()
                    .Where(x => x.GroupOrderStatus != null && Helper.IsFlagSet((int)x.GroupOrderStatus, (int)GroupOrderStatus.DaysNoRefund)).ToList();
                //演出時間前十日內 (e.g. 活動商品有效期限開始日 = 10/11, 則 10/1 就已經不能退貨)
                List<ViewPponDeal> vpdNoRefundBeforeDays = pp.ViewPponDealGetListPaging(0, 0, string.Empty,
                    ViewPponDeal.Columns.BusinessHourDeliverTimeS + ">=" + DateTime.Now.AddDays(config.PponNoRefundBeforeDays).Date.ToShortDateString(),
                    ViewPponDeal.Columns.BusinessHourDeliverTimeS + "<" + DateTime.Now.AddDays(config.PponNoRefundBeforeDays + 1).Date.ToShortDateString()).ToList()
                    .Where(x => x.GroupOrderStatus != null && Helper.IsFlagSet((int)x.GroupOrderStatus, (int)GroupOrderStatus.NoRefundBeforeDays)).ToList();
                //聯集起來並且排除全家檔次
                IEnumerable<ViewPponDeal> vpdCols = vpdNoRefund.Union(vpdExpireNoRefund).Union(vpdDaysNoRefund).Union(vpdNoRefundBeforeDays)
                    .Where(x => !Helper.IsFlagSet((int)x.GroupOrderStatus, (int)GroupOrderStatus.FamiDeal)).ToList();
                //因為任何一檔有可能同時滿足多個退貨截止條件, 因此多加這個 "重複清單" , 以免重複列出檔次
                List<string> duplicateUniqueIds = new List<string>();

                if (vpdCols.Any())
                {
                    string mailBody = returnObj.BodyHeader;
                    foreach (ViewPponDeal vpd in vpdCols)
                    {
                        //判斷此檔是否在重複清單中
                        if (!duplicateUniqueIds.Any(x => x == vpd.UniqueId.ToString())) 
                        {
                            //列出此檔不可退貨的原因
                            string returnMethod = vpdNoRefund.Any(x => x.UniqueId == vpd.UniqueId) ? I18N.Phrase.NoRefund + I18N.Phrase.BigDot : string.Empty;
                            returnMethod += vpdExpireNoRefund.Any(x => x.UniqueId == vpd.UniqueId) ? I18N.Phrase.ExpireNoRefund + I18N.Phrase.BigDot : string.Empty;
                            returnMethod += vpdDaysNoRefund.Any(x => x.UniqueId == vpd.UniqueId) ? I18N.Phrase.DaysNoRefund + I18N.Phrase.BigDot : string.Empty;
                            returnMethod += vpdNoRefundBeforeDays.Any(x => x.UniqueId == vpd.UniqueId) ? I18N.Phrase.NoRefundBeforeDays + I18N.Phrase.BigDot : string.Empty;

                            mailBody += string.Format("簽約公司名稱：{0} " + I18N.Phrase.HtmlNewLine 
                                + "檔號：{1} " + I18N.Phrase.HtmlNewLine
                                + "檔名：{2} " + I18N.Phrase.HtmlNewLine
                                + "退貨方式：{3} " + I18N.Phrase.HtmlNewLine + I18N.Phrase.HtmlNewLine
                                , vpd.CompanyName
                                , vpd.UniqueId
                                , vpd.CouponUsage
                                , returnMethod);

                            //已經列過此檔, 因此把UniqueId放入重複清單
                            duplicateUniqueIds.Add(vpd.UniqueId.ToString());
                        }
                    }

                    MailMessage msg = new MailMessage();
                    msg.IsBodyHtml = true;
                    msg.From = new MailAddress(config.SystemEmail);
                    msg.To.Add(config.FinanceEmail);
                    msg.To.Add(config.CsManagerEmail);
                    msg.Subject = returnObj.subject;
                    msg.Body = mailBody;

                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.IsBodyHtml = true;
                msg.From = new MailAddress(config.SystemEmail);
                msg.To.Add(config.AdminEmail);
                msg.Subject = returnObj.subject + ex.Message;
                msg.Body = ex.ToString();

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }

            #endregion

            #region 展期到期核銷通知

            VerifyInformClass verifyObj = new VerifyInformClass();

            try
            {
                //條件: 1.有勾選四種 "不可退貨" 的選項之一 2.今天是寄送截止日的後一天 3.非全家檔次
                DateTime dte = DateTime.Now.Date;
                IEnumerable<ViewPponDeal> vpd4 = pp.ViewPponDealGetListByVerifyInform(dte)
                .Where(x => !Helper.IsFlagSet((int)x.GroupOrderStatus, (int)GroupOrderStatus.FamiDeal));

                if (vpd4.Any())
                {
                    string mailBody = verifyObj.BodyHeader;

                    foreach (ViewPponDeal vpd in vpd4)
                    {
                        #region returnMethodSet

                        string returnMethod = string.Empty;
                        returnMethod += Helper.IsFlagSet((int)vpd.GroupOrderStatus, (int)GroupOrderStatus.NoRefund) ? I18N.Phrase.NoRefund + I18N.Phrase.BigDot : string.Empty;
                        returnMethod += Helper.IsFlagSet((int)vpd.GroupOrderStatus, (int)GroupOrderStatus.ExpireNoRefund) ? I18N.Phrase.ExpireNoRefund + I18N.Phrase.BigDot : string.Empty;
                        returnMethod += Helper.IsFlagSet((int)vpd.GroupOrderStatus, (int)GroupOrderStatus.DaysNoRefund) ? I18N.Phrase.DaysNoRefund + I18N.Phrase.BigDot : string.Empty;
                        returnMethod += Helper.IsFlagSet((int)vpd.GroupOrderStatus, (int)GroupOrderStatus.NoRefundBeforeDays) ? I18N.Phrase.NoRefundBeforeDays + I18N.Phrase.BigDot : string.Empty;

                        #endregion returnMethodSet

                        mailBody += string.Format("簽約公司名稱：{0} " + I18N.Phrase.HtmlNewLine
                            + "檔號：{1} " + I18N.Phrase.HtmlNewLine
                            + "檔名：{2} " + I18N.Phrase.HtmlNewLine
                            + "連結：<a target='_blank' href='{3}/controlroom/ppon/setup.aspx?bid={4}'>點此</a> " + I18N.Phrase.HtmlNewLine
                            + "退貨方式：{5} " + I18N.Phrase.HtmlNewLine + I18N.Phrase.HtmlNewLine
                            , vpd.CompanyName
                            , vpd.UniqueId
                            , vpd.CouponUsage
                            , config.SiteUrl
                            , vpd.BusinessHourGuid
                            , returnMethod);
                    }

                    MailMessage msg = new MailMessage();
                    msg.IsBodyHtml = true;
                    msg.From = new MailAddress(config.SystemEmail);
                    msg.To.Add(config.SalesManagerEmail);
                    msg.Subject = verifyObj.subject;
                    msg.Body = mailBody;

                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.IsBodyHtml = true;
                msg.From = new MailAddress(config.SystemEmail);
                msg.To.Add(config.AdminEmail);
                msg.To.Add(config.SalesSpecialAssistantEmail);
                msg.Subject = verifyObj.subject + ex.Message;
                msg.Body = ex.Message;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }

            #endregion

            #region 提醒商家有新的退貨申請（新版退貨）

            //抓取 已過開始配送時間檔次 且 為24小時內申請的退貨單
            ViewOrderReturnFormListCollection returnFormCol = op.ViewOrderReturnFormListGetListPaging(0, 0, string.Empty,
                ViewOrderReturnFormList.Columns.UseEndTime + " <= " + DateTime.Today,
                ViewOrderReturnFormList.Columns.ReturnApplicationTime + " >= " + DateTime.Now.AddDays(-1).ToShortDateString(),
                ViewOrderReturnFormList.Columns.ProgressStatus + " = " + (int)ProgressStatus.Processing,
                ViewOrderReturnFormList.Columns.DeliveryType + " = " + (int)DeliveryType.ToHouse);

            //以 ProductGuid 做 GroupBy, 並計算每個 ProductGuid 有幾張退貨申請單
            var dealCounts = returnFormCol.Select(x => new
            {
                ProductGuid = x.ProductGuid,
                Quantity = 1,
            })
            .GroupBy(x => new
            {
                ProductGuid = x.ProductGuid
            })
            .Select(x => new
            {
                ProductGuid = x.Key.ProductGuid,
                Quantity = x.Sum(o => o.Quantity)
            });

            foreach (var dealCount in dealCounts) 
            {
                ViewOrderReturnFormList returnForm = returnFormCol.Where(x => x.ProductGuid == dealCount.ProductGuid).FirstOrDefault();

                try
                {
                    RefundNoticeSellerEmail template = TemplateFactory.Instance().GetTemplate<RefundNoticeSellerEmail>();
                    template.Seller_Name = returnForm.SellerName;
                    template.Deal_Name = returnForm.DealName;
                    template.Return_Count = dealCount.Quantity.ToString();

                    MailMessage msg = new MailMessage();
                    msg.IsBodyHtml = true;
                    msg.From = new MailAddress(config.CsReturnEmail);
                    msg.To.Add(returnForm.SellerEmail);
                    msg.To.Add(returnForm.ReturnedPersonEmail);
                    msg.Subject = "您有新的退貨申請_" + DateTime.Now.ToShortDateString();
                    msg.Body = template.ToString();

                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
                catch (Exception) 
                { 
                    
                }
            }

            #endregion

        }

        #region class   
        private class ReturnInformClass
        {
            public string BodyHeader
            {
                get { return "您好：<br/><br/>以下檔次之退貨時間已截止，請客服人員確認退貨數量，以利帳務人員準備出帳。謝謝！<br/><br/>"; }
            }
            public string subject
            {
                get { return "【17Life】特殊檔次退貨時間截止，請確認退貨數量 "; }
            }
        }
        private class VerifyInformClass
        {
            public string BodyHeader
            {
                get { return "您好：<br/><br/>以下檔次的使用期限已經到期，還請盡速至後台核銷。謝謝！<br/><br/>"; }
            }
            public string subject
            {
                get { return "【17Life】檔次使用期限已到期，請盡速核銷 "; }
            }
        }
        #endregion
    }
}

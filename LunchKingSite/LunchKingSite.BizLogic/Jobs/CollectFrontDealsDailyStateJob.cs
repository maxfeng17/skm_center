﻿using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.BizLogic.Jobs
{
    public class CollectFrontDealsDailyStateJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(CollectFrontDealsDailyStateJob));
        
        public void Execute(JobSetting js)
        {
            try
            {
                DateTime today = DateTime.Today;
                DateTime yesterday = today.AddDays(-1);

                List<FrontDealsDailyStat> frontDealsDailyStats = MarketingFacade.GetFrontDealsDailyStateByRange(yesterday, today);
                MarketingFacade.ImportFrontDealsDailyState(yesterday, today, frontDealsDailyStats);
            }
            catch (Exception ex)
            {
                logger.Error("CollectFrontDealsDailyStateJob Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
}

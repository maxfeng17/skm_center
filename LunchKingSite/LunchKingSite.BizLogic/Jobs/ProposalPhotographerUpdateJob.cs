﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using System.Net.Mail;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component.Template;

namespace LunchKingSite.BizLogic.Jobs
{
    public class ProposalPhotographerUpdateJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(ProposalPhotographerUpdateJob));
        protected static ISellerProvider sp;
        private static ISysConfProvider config;

        static ProposalPhotographerUpdateJob()
        {
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                ProposalCollection pc = sp.ProposalGetPhotographer();
                foreach (var p in pc)
                {
                    DateTime PhotoDateTime;
                    Dictionary<ProposalPhotographer, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalPhotographer, string>>(p.SpecialAppointFlagText);
                    if (specialText != null && specialText.Count > 0)
                    {
                        //攝影師資訊
                        DateTime dt = DateTime.MaxValue;
                        PhotoDateTime = DateTime.TryParse(specialText[ProposalPhotographer.PhotographerAppointCheck].Split('|')[0], out dt) ? dt : DateTime.MaxValue;

                        //因為是半夜執行,且考慮假日問題,因此用前5天判斷
                        if (PhotoDateTime != DateTime.MaxValue && PhotoDateTime < DateTime.Now.AddDays(-5))
                        {
                            p.PhotographerAppointFlag = Convert.ToInt32(Helper.SetFlag(true, p.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointClose));
                            ProposalFacade.ProposalLog(p.Id, "變更 攝影狀態 為 " + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppointClose), config.SystemEmail);
                            sp.ProposalSet(p);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex + " 提案單攝影狀態更新錯誤");
            }
        }


    }
}

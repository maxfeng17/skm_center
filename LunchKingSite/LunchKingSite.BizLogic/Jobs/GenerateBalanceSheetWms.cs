﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Hosting;

namespace LunchKingSite.BizLogic.Jobs
{
	public class GenerateBalanceSheetWms : IJob
	{
		private static readonly ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static readonly IWmsProvider wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static readonly IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static ILog logger = LogManager.GetLogger("GenerateBalanceSheetWms");

		public void Execute(JobSetting js)
		{
			try
			{
                FortnightlyToHouseBalanceSheetWms wms = new Component.FortnightlyToHouseBalanceSheetWms();
                
                wms.Wms(js);

            }
            catch (Exception e)
			{
                logger.InfoFormat("Job GenerateBalanceSheetWms Error {0}", e);
			}
		}
    }
}

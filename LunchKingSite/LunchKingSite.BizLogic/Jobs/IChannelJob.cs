﻿using System;
using System.Net.Mail;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Jobs
{
    public class IChannelJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(IChannelJob));
        private static ISysConfProvider config;
        private static IChannelJob theJob;

        static IChannelJob()
        {
            theJob = new IChannelJob();
        }

        public static IChannelJob Instance()
        {
            return theJob;
        }

        private IChannelJob()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                if (config.EnableIChannel)
                {
                    DateTime dateStart = DateTime.Now.AddDays(-1).Date.SetTime(0, 0, 0);
                    DateTime dateEnd = DateTime.Now;

                    OrderFacade.IChannelAPI(dateStart, dateEnd);
                }
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = "IChannelJob Error";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = false;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error("IChannelJob Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
 }

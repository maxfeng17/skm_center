﻿using log4net;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace LunchKingSite.BizLogic.Jobs
{
	public class GenerateBalanceSheet : IJob
	{
		private static readonly ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        private static readonly IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static readonly IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ILog logger = LogManager.GetLogger("GenerateBalanceSheet");

        private BalanceSheetGenerationFrequency GenFreq { get; set; }
		private RemittanceFrequency PayFreq { get; set; }
	    private bool OutOfVerificationTimeRange { get; set; }
	    private DeliveryType DeliveryType { get; set; }

	    private string MailSubject {
			get
			{
				if (GenFreq == BalanceSheetGenerationFrequency.WeekBalanceSheet && 
                    PayFreq == RemittanceFrequency.Weekly)
                {
					return string.Format("[系統] {0}週結週對帳單", DeliveryType == DeliveryType.ToShop ? "憑證" : "商品");
                }
				if (GenFreq == BalanceSheetGenerationFrequency.MonthBalanceSheet && 
                    PayFreq == RemittanceFrequency.Weekly)
                {
                    return string.Format("[系統] {0}週結月對帳單", OutOfVerificationTimeRange ? "補產檔款" : string.Empty);
                }
				if (GenFreq == BalanceSheetGenerationFrequency.MonthBalanceSheet && 
                    PayFreq == RemittanceFrequency.Monthly)
                {
                    return string.Format("[系統] {0}月結對帳單", DeliveryType == DeliveryType.ToShop ? "憑證" : "商品");
                }
                if (GenFreq == BalanceSheetGenerationFrequency.FortnightlyBalanceSheet &&
                    PayFreq == RemittanceFrequency.Fortnightly)
                {
                    return string.Format("[系統] 商品雙周結對帳單");
                }
                if (GenFreq == BalanceSheetGenerationFrequency.LumpSumBalanceSheet &&
                    PayFreq == RemittanceFrequency.LumpSum)
                {
                    return "[系統] 商品暫付及其他付款對帳單";
                }
			    if (GenFreq == BalanceSheetGenerationFrequency.FlexibleBalanceSheet &&
                    PayFreq == RemittanceFrequency.Flexible)
			    {
                    return string.Format("[系統] {0}彈性請款對帳單", DeliveryType == DeliveryType.ToShop ? "憑證" : "商品");
			    }
				return "[系統] 莫名其妙的執行了產對帳單排程";
			}
		}

		public void Execute(JobSetting js)
		{
			try
			{
				Stopwatch timer = new Stopwatch();
				timer.Start();

				InitProps(js);

				DateTime now = DateTime.Now;
				VerificationPolicy verification = new VerificationPolicy();
                IEnumerable<BusinessHour> ppons = verification.GetPponVerifyingDealsForVendorBillingSystem(now, GenFreq, PayFreq, OutOfVerificationTimeRange, DeliveryType).ToList();
				IEnumerable<HiDealProduct> piins = verification.GetHiDealVerifyingProductsForBillingSystem(now, GenFreq, PayFreq).ToList();
                
			    StringBuilder exceptionMailMessage = new StringBuilder();

                logger.InfoFormat(string.Format("{0} 排程開始執行\r\n", MailSubject));
                logger.InfoFormat(string.Format("核銷期間內要產對帳單的檔次:\r\n\t好康共有 {0} 檔\r\n\t品生活共有 {1} 檔\r\n", ppons.Count(), piins.Count()));

                foreach (BusinessHour deal in ppons)
				{
                    try
                    {
                        logger.InfoFormat(GeneratePpon(deal));
                    }
                    catch(Exception e)
                    {
                        logger.InfoFormat(string.Format("產出bid-{0}對帳單時出現錯誤，錯誤訊息：\n{1}", deal.Guid, e.Message));
                        exceptionMailMessage.Append(e.Message);
                    }
				}

				TimeSpan timeStampPponed = timer.Elapsed;

				foreach (HiDealProduct product in piins)
				{
                    try
                    {
                        logger.InfoFormat(GeneratePiin(product));
                    }
                    catch (Exception e)
                    {
                        logger.InfoFormat(string.Format("產出Hideal Product Guid-{0}對帳單時出現錯誤，錯誤訊息：\n{1}", product.Guid, e.Message));
                        exceptionMailMessage.Append(e.Message);
                    }
                }

                #region 週結月對帳單開立通知信

                //只有週結月對帳單要寄發 "對帳單開立通知信"
                if (GenFreq == BalanceSheetGenerationFrequency.MonthBalanceSheet && 
                    PayFreq == RemittanceFrequency.Weekly)
                {
                    MailSellerAndSales(ppons, piins);
                }

                #endregion 週結月對帳單開立通知信

                #region 宅配檔對帳單開立通知信 

                //商品檔 一次性付款對帳單/彈性請款出帳對帳單 
                //寄發對帳單開立通知信給賣家及財務(以賣家為單位通知，若多檔次則顯示母檔資訊)
                else if (DeliveryType == DeliveryType.ToHouse &&
                         (PayFreq == RemittanceFrequency.LumpSum ||
                          PayFreq == RemittanceFrequency.Flexible))
                {
                    //非多檔次
                    List<Guid> bids = ap.ViewBalanceSheetListGetList(
                                            ppons.Where(x => !((BusinessHourStatus)x.BusinessHourStatus).HasFlag(BusinessHourStatus.ComboDealMain) && 
                                                             !((BusinessHourStatus)x.BusinessHourStatus).HasFlag(BusinessHourStatus.ComboDealSub))
                                                 .Select(x => x.Guid))
                                           .Where(x => (x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet ||
                                                        x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet) &&
                                                        x.CreateTime >= DateTime.Today)
                                           .Select(x => x.ProductGuid)
                                           .Distinct()
                                           .ToList();
                    //多檔次子檔
                    List<Guid> subBids = ppons.Where(x => ((BusinessHourStatus) x.BusinessHourStatus).HasFlag(BusinessHourStatus.ComboDealSub))
                                              .Select(x => x.Guid)
                                              .ToList();
                    //以母檔為單位發送通知信 母檔下所有子檔皆產對帳單 方可寄發對帳單開立通知信
                    var comboDealInfos = pp.GetComboDealByBid(subBids)
                                            .Where(x => x.BusinessHourGuid != x.MainBusinessHourGuid)
                                            .GroupBy(x => x.MainBusinessHourGuid)
                                            .ToDictionary(x => x.Key.Value, x => x.Select(b => b.BusinessHourGuid).ToList());
                    var subDealGuids = new List<Guid>();
                    foreach(var subDealGuid in comboDealInfos.Values)
                    {
                        if (!subDealGuid.All(x => subDealGuids.Contains(x)))
                        {
                            subDealGuids.AddRange(subDealGuid);
                        }
                    }
                    var dealInfos = pp.ViewDealPropertyBusinessHourContentGetList(subDealGuids)
                                        .ToDictionary(x => x.BusinessHourGuid, x => x);
                    var bsInfos = ap.ViewBalanceSheetListGetList(subDealGuids)
                                    .Where(x => x.CreateTime >= DateTime.Today)
                                    .GroupBy(x => x.ProductGuid)
                                    .ToDictionary(x => x.Key,
                                                  x => x.Count(b => b.GenerationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet ||
                                                                    b.GenerationFrequency == (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet));
                    int tempSlug;
                    foreach (var comboDeal in comboDealInfos)
                    {
                        var establishedSubDeal = comboDeal.Value.Where(x => dealInfos.ContainsKey(x) &&
                                                                            int.TryParse(dealInfos[x].Slug, out tempSlug) &&
                                                                            tempSlug >= dealInfos[x].BusinessHourOrderMinimum)
                                                                .ToList();

                        if (establishedSubDeal.All(x => bsInfos.ContainsKey(x) &&
                                                        bsInfos[x] > 0))
                        {
                            if (!bids.Contains(comboDeal.Key))
                            {
                                bids.Add(comboDeal.Key);
                            }
                        }
                    }

                    if (PayFreq == RemittanceFrequency.LumpSum)
                    { 
                        BalanceSheetManager.ProductBalanceSheetCreateMailNotify(bids);
                    }
                    else
                    {
                        BalanceSheetManager.FlexibleBalanceSheetSystemCreateNotify(bids);
                    }
                }

                #endregion 宅配檔對帳單開立通知信 
				
                #region 憑證檔彈性請款對帳單系統開立通知

			    if (DeliveryType == DeliveryType.ToShop &&
			        PayFreq == RemittanceFrequency.Flexible)
			    {
                    BalanceSheetManager.FlexibleBalanceSheetSystemCreateNotify(ppons.Select(x => x.Guid));
			    }
                
                #endregion 憑證檔彈性請款對帳單系統開立通知

                TimeSpan timeStampPiined = timer.Elapsed.Subtract(timeStampPponed);
				timer.Stop();
                logger.InfoFormat(
					string.Format("總時間 {0:g} ,\r\n\t 好康: {1:g} \r\n\t品生活: {2:g}\r\n\r\n",
					timer.Elapsed,
					timeStampPponed,
					timeStampPiined));
                
                logger.InfoFormat(string.Format("{0} 排程執行結束\r\n\r\n", MailSubject));

                if (exceptionMailMessage.ToString().Length > 0)
                {
                    AlertResult(exceptionMailMessage.ToString());
                }

                #region 產匯款資料

                //憑證ACH週結對帳單才需自動產Weekly_pay_report檔案
                if (DeliveryType == DeliveryType.ToShop && 
                    GenFreq == BalanceSheetGenerationFrequency.WeekBalanceSheet)
                {
                    var job = new GenAchReport();
                    job.Execute(null);
                }

                #endregion 產匯款資料
            }
			catch (Exception e)
			{
                logger.InfoFormat("Job GenerateBalanceSheet Error", e);
			}
		}

        /// <summary>
        /// 初始對帳單的週期
        /// </summary>
        /// <param name="js"></param>
		private void InitProps(JobSetting js)
		{
            OutOfVerificationTimeRange = false;

			switch(js.Parameters["generationFrequency"])
			{
				case "weekly":
					GenFreq = BalanceSheetGenerationFrequency.WeekBalanceSheet;
			        DeliveryType = DeliveryType.ToShop;
					break;
				case "monthly":
					GenFreq = BalanceSheetGenerationFrequency.MonthBalanceSheet;
			        DeliveryType = DeliveryType.ToShop;
                    break;
                case "lumpSum":
                    GenFreq = BalanceSheetGenerationFrequency.LumpSumBalanceSheet;
			        DeliveryType = DeliveryType.ToHouse;
                    break;
                case "monthly-outOfTimeRange":
			        OutOfVerificationTimeRange = true;
                    GenFreq = BalanceSheetGenerationFrequency.MonthBalanceSheet;
			        DeliveryType = DeliveryType.ToShop;
                    break;
                case "weekly-toHouse":
					GenFreq = BalanceSheetGenerationFrequency.WeekBalanceSheet;
                    DeliveryType = DeliveryType.ToHouse;
                    break;
                case "fortnightly-toHouse":
                    GenFreq = BalanceSheetGenerationFrequency.FortnightlyBalanceSheet;
                    DeliveryType = DeliveryType.ToHouse;
                    break;
                case "monthly-toHouse":
                    GenFreq = BalanceSheetGenerationFrequency.MonthBalanceSheet;
                    DeliveryType = DeliveryType.ToHouse;
                    break;
                case "flexible":
			        GenFreq = BalanceSheetGenerationFrequency.FlexibleBalanceSheet;
			        DeliveryType = DeliveryType.ToShop;
                    break;
                case "flexible-toHouse":
                    GenFreq = BalanceSheetGenerationFrequency.FlexibleBalanceSheet;
                    DeliveryType = DeliveryType.ToHouse;
                    break;
				default:
					throw new InvalidOperationException("Job configuration not recognized!");
			}

			switch (js.Parameters["remittanceFrequency"])
			{
				case "weekly":
					PayFreq = RemittanceFrequency.Weekly;
					break;
                case "fortnightly":
                    PayFreq = RemittanceFrequency.Fortnightly;
                    break;
                case "monthly":
					PayFreq = RemittanceFrequency.Monthly;
                    break;
                case "lumpSum":
                    PayFreq = RemittanceFrequency.LumpSum;
                    break;
                case "flexible":
                    PayFreq = RemittanceFrequency.Flexible;
                    break;
				default:
					throw new InvalidOperationException("Job configuration not recognized!");
			}
		}

        private void MailSellerAndSales(IEnumerable<BusinessHour> ppons, IEnumerable<HiDealProduct> piins)
        {
            try
            {
                #region P好康

                List<Guid> pponBids = new List<Guid>();     //所有的檔次
                List<Guid?> sellers = new List<Guid?>();    //已寄過的Seller名單，避免重複
                int sqlInLimit = 2000;                      //SQL 的 "in" limit = 2100, 設2000以求保險
                int times = 0;                              //檔次數目是2000的幾倍

                foreach (BusinessHour bh in ppons)
                {
                    pponBids.Add(bh.Guid);
                }

                //超過兩千筆以上，要分批抓取ViewDealPropertyBusinessHourContent，以免超過SQL對 In 的數量限制
                while (pponBids.Skip(times * sqlInLimit).Any()) 
                {
                    var inPpons = pponBids.Skip(times * sqlInLimit).Take(sqlInLimit);

                    ViewDealPropertyBusinessHourContentCollection vdpCols = pp.ViewDealPropertyBusinessHourContentGetListPaging(0, 0, string.Empty,
                        ViewDealPropertyBusinessHourContent.Columns.BusinessHourGuid + " in (" + string.Join(",", inPpons) + ") ");

                    foreach (ViewDealPropertyBusinessHourContent vdp in vdpCols)
                    {
                        IEnumerable<BalanceSheet> bals = ap.BalanceSheetGetListByProductGuid(vdp.BusinessHourGuid)
                            .Where(x => x.Year.Equals(DateTime.Today.Year) && x.Month.Equals(DateTime.Today.Month) && x.EstAmount > 0 && !x.IsReceiptReceived);

                        //條件: 1.賣家不能重複 2.ACH每週付款 3.當月對帳單有核銷數且未寄回收據
                        if (!sellers.Contains(vdp.SellerGuid) && vdp.RemittanceType.Equals((int)RemittanceType.AchWeekly) && bals.Any())
                        {
                            #region 商家 + 業務通知信

                            BalanceSheetMade template = TemplateFactory.Instance().GetTemplate<BalanceSheetMade>();
                            template.ServerConfig = _config;
                            template.Seller_Name = vdp.SellerName;

                            try
                            {
                                MailMessage msg = new MailMessage();
                                msg.From = new MailAddress(_config.ServiceEmail, _config.ServiceName);
                                //如果是付給賣家, 則寄信給賣家帳務聯絡人; 如果是付給分店, 則寄給分店帳務聯絡人 + 賣家帳務聯絡人
                                if (vdp.Paytocompany != (int)DealAccountingPayType.PayToCompany)
                                {
                                    ViewPponStoreCollection pponStores = pp.ViewPponStoreGetListByBidWithNoLock(vdp.BusinessHourGuid, VbsRightFlag.Accouting);
                                    foreach (ViewPponStore store in pponStores)
                                    {
                                        if (!string.IsNullOrEmpty(store.CompanyEmail))
                                        {
                                            //賣家的帳務聯絡人Email, 常出現多個Email 並以逗號或分號來分隔的狀態, 故要做此判斷
                                            if (RegExRules.IsEmailContainsSeperator(store.CompanyEmail))
                                            {
                                                msg.To.Add(RegExRules.CheckMultiEmail(store.CompanyEmail));
                                            }
                                            else 
                                            {
                                                if (RegExRules.CheckEmail(store.CompanyEmail))
                                                {
                                                    msg.To.Add(store.CompanyEmail);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(vdp.CompanyEmail))
                                    {
                                        if (RegExRules.IsEmailContainsSeperator(vdp.CompanyEmail))
                                        {
                                            msg.To.Add(RegExRules.CheckMultiEmail(vdp.CompanyEmail));
                                        }
                                        else 
                                        {
                                            if (RegExRules.CheckEmail(vdp.CompanyEmail))
                                            {
                                            msg.To.Add(vdp.CompanyEmail);    
                                        }
                                    }
                                }
                                }

                                //CC給業務
                                if (RegExRules.CheckEmail(vdp.Email))
                                {
                                    msg.CC.Add(vdp.Email);
                                }

                                msg.Subject = vdp.SellerName.Replace('\r', ' ').Replace('\n', ' ') + "－對帳單已開立";
                                msg.Body = template.ToString();
                                msg.IsBodyHtml = true;

                                //必須有收件人才寄
                                if (!string.IsNullOrEmpty(msg.To.ToString()))
                                {
                                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                                }
                            }
                            catch (Exception e) 
                            {
                                MailMessage msg = new MailMessage();
                                msg.To.Add(_config.ItTesterEmail);
                                msg.Subject = "對帳單通知信錯誤";
                                msg.Body = e.Message + Environment.NewLine + e.StackTrace;
                                msg.IsBodyHtml = true;
                                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                            }

                            #endregion

                            if (vdp.SellerGuid != null)
                            {
                                sellers.Add(vdp.SellerGuid);
                            }
                        }

                    }

                    times++;
                }

                #endregion
            }
            catch (Exception e) 
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(_config.ItTesterEmail);
                msg.Subject = "對帳單通知信錯誤";
                msg.Body = e.Message + Environment.NewLine + e.StackTrace;
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

		private void MailResult(string messageBody)
		{
			MailMessage msg = new MailMessage();
            msg.From = new MailAddress(_config.AdminEmail);
            msg.To.Add(_config.AdminEmail);
            msg.Subject = Environment.MachineName + ": " + MailSubject;
			msg.Body = messageBody;

			PostMan.Instance().Send(msg, SendPriorityType.Async);
		}

        private void AlertResult(string messageBody)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(_config.AdminEmail);
            msg.To.Add(_config.AdminEmail);
            msg.Subject = Environment.MachineName + ": " + MailSubject + "-Exception!";
            msg.Body = messageBody;

            PostMan.Instance().Send(msg, SendPriorityType.Normal);
        }

		private string GeneratePpon(BusinessHour deal)
		{
            BalanceSheetSpec spec = new BalanceSheetSpec()
            {
                SheetCategory = GenFreq,
                BizModel = BusinessModel.Ppon,
                BusinessHourGuid = deal.Guid,
                GenOutOfVerificationTimeRange = OutOfVerificationTimeRange
            };
            IEnumerable<BalanceSheetGenerationResult> generationResults = BalanceSheetService.Create(spec);

			StringBuilder dealResultMessage = new StringBuilder();
			dealResultMessage.Append(GetDelimiter());
			dealResultMessage.Append(string.Format("bid = {0}\r\n", deal.Guid));
			dealResultMessage.Append(GenerationResultsToMessage(generationResults));
			dealResultMessage.Append(GetDelimiter());

			return dealResultMessage.ToString();
		}
		
		private string GeneratePiin(HiDealProduct product)
		{
            BalanceSheetSpec spec = new BalanceSheetSpec()
            {
                SheetCategory = GenFreq,
                BizModel = BusinessModel.PiinLife,
                HiDealProductId = product.Id,
                GenOutOfVerificationTimeRange = OutOfVerificationTimeRange
            };
            IEnumerable<BalanceSheetGenerationResult> generationResults = BalanceSheetService.Create(spec);

			StringBuilder productResultMessage = new StringBuilder();
			productResultMessage.Append(GetDelimiter());
			productResultMessage.Append(string.Format("pid = {0}\r\n", product.Id));
			productResultMessage.Append(GenerationResultsToMessage(generationResults));
			productResultMessage.Append(GetDelimiter());

			return productResultMessage.ToString();
		}

		private string GenerationResultsToMessage(IEnumerable<BalanceSheetGenerationResult> results)
		{
			StringBuilder storeMessage = new StringBuilder();
			foreach (BalanceSheetGenerationResult result in results)
			{
				if (result.StoreGuid != null)
				{
					storeMessage.Append(string.Format("分店-{0} : ", result.StoreGuid));
				}

				if (result.IsSuccess)
				{
					storeMessage.Append(string.Format("成功-對帳單 id = {0}", result.BalanceSheetId));
				}
				else
				{
					if (result.Status == BalanceSheetGenerationResultStatus.Exception)
					{
						storeMessage.Append("失敗-");
						storeMessage.Append(result.FailureMessage.TrimToMaxLength(800, "...blah blah blah..."));
					}
					else
					{
						storeMessage.Append("不用產-");
                        storeMessage.Append(result.Status.ToString() + ":");
                        storeMessage.Append(result.FailureMessage);
					}
				}

				storeMessage.Append("\r\n");
			}

			return storeMessage.ToString();
		}

		private string GetDelimiter()
		{
			return "----------------------------------------------------------------------------------------------------------\r\n";
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class PponDealPreviewManagerUpdate : IJob
    {
        public void Execute(JobSetting js)
        {
            PponDealPreviewManager.ReloadManager();
        }
    }
}

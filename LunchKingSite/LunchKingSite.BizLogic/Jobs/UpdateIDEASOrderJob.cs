﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class UpdateIDEASOrderJob : IJob
    {
        public void Execute(JobSetting js)
        {
            ApiLocationDealManager.UpdateIDEASOrder();
        }
    }
}

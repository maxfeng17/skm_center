﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Jobs
{
    public class CollectDealJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(CollectDealJob));
        private static ISysConfProvider _config;
        private static CollectDealJob _theJob;

        static CollectDealJob() 
        {
            _theJob = new CollectDealJob();
        }

        public static CollectDealJob Instance()
        {
            return _theJob;
        }

        private CollectDealJob()
        {
            _config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js) 
        {
            try
            {
                MemberFacade.SendCollectDealExpireMessage(24);
                MemberFacade.SendCollectDealSoldOutMessage();
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("CollectDealJob Message:({0}), {1}", ex.Message, ex.StackTrace);
            }
        }

    }
}

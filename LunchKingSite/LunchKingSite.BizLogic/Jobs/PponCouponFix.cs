﻿using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using System;
using log4net;
using System.Net.Mail;

namespace LunchKingSite.BizLogic.Jobs
{
    public class PponCouponFix : IJob
    {
        private static IPponProvider pp;
        protected static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(PponCouponFix));

        static PponCouponFix()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            try
            { 
                ViewPponCouponCollection vcc = pp.GetNoCouponList();
                if(vcc.Count>0)
                {
                    OrderFacade.GenerateCouponByCouponCol(vcc);
                    #region 憑證補產通知信
                    MailMessage msg = new MailMessage();
                    msg.To.Add(config.AdminEmail);
                    msg.Subject = Environment.MachineName + ": PponCouponFix";
                    msg.From = new MailAddress(config.AdminEmail);
                    msg.IsBodyHtml = true;

                    msg.Body = "補產憑證，OrderDetailGuid，ItemName:<br/>";
                    foreach (var v in vcc)
                    {
                        msg.Body += v.OrderDetailId + "，" + v.ItemName + "<br/>";
                    }
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    #endregion
                }

                string trustFixResult = TrustFacade.FillTrustEmptyCoupon();
                if (!string.IsNullOrEmpty(trustFixResult))
                {
                    #region 信託紀錄補產通知信
                    MailMessage msg2 = new MailMessage();
                    msg2.To.Add(config.AdminEmail);
                    msg2.Subject = Environment.MachineName + ": PponTrustFix";
                    msg2.From = new MailAddress(config.AdminEmail);
                    msg2.IsBodyHtml = true;
                    msg2.Body = trustFixResult;
                    PostMan.Instance().Send(msg2, SendPriorityType.Immediate);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = Environment.MachineName + ": PponCouponFix Error";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = ex.Message + ex.StackTrace;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                logger.Error("PponCouponFix Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }           
        }
    }
}

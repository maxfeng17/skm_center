﻿using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class ReviewInvitationMailer : IJob
    {
        static ReviewInvitationMailer()
        {
        }

        public void Execute(JobSetting js)
        {
            // 只有午餐王使用，信件內容皆為無效連結
        }
    }
}
﻿using log4net;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;

namespace LunchKingSite.BizLogic.Jobs
{
    class PCPJob : IJob
    {
        private static ISysConfProvider config;
        private static IPCPProvider pcp;
        private static IMemberProvider mp;
        private static ILog logger = LogManager.GetLogger(typeof(PCPJob));
        private delegate void JobDelegate(ref List<ExecuteResult> result);
        private static TimeingMethod _timeingMethod;
        static PCPJob()
        {
            config = ProviderFactory.Instance().GetConfig();
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _timeingMethod = new TimeingMethod();
        }

        public void Execute(JobSetting js)
        {
            List<ExecuteResult> resultInfos = new List<ExecuteResult>();

            JobDelegate j = delegate { };
            string mode = js.Parameters["mode"] != null ? js.Parameters["mode"] : "";
            //執行要按照順序
            switch (mode)
            {
                //檢查到期的公關點或熟客點
                //週期 未定
                //case "PointCheck":
                //    j = CheckPointExpirationDate;
                //    break;
                //檢查到期的公關券或熟客券
                //週期 每天
                case "FavorsCheck":
                    j = CheckFavors;
                    j += UserMembershipStatistics;
                    j += DeleteImage;
                    break;
                //更新會員卡的啟用停用、更新會員資料
                //週期 三個月
                case "MemberCardCheck":
                    j = CheckMemberCard;
                    break;
                case "QuickCheck":
                    j = ImageCompressCheck;
                    break;
                //case "TrustPoint":
                //    //熟客點信託
                //    //j = RegularsPointToTrust;
                //    //j += TrustToTrustBank;
                //    //j += TrustToReport;
                //    break;
                default:
                    j = null;
                    break;
            }

            if (j == null)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = "執行JOB參數錯誤",
                    IsSuccess = false,
                    Message = string.Format("JOB參數錯誤：{0}", mode)
                };

                SendResultMail(config.AdminEmail, new List<ExecuteResult> { errMsg }, "PCPCheckExpiration Error " + mode);
                return;
            }

            try
            {
                j(ref resultInfos);
            }
            catch (Exception ex)
            {
                ExecuteResult errMsg = new ExecuteResult
                {
                    title = "執行JOB錯誤",
                    IsSuccess = false,
                    Message = string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace)
                };

                SendResultMail(config.AdminEmail, new List<ExecuteResult> { errMsg }, "PCPCheckExpiration Error " + mode);
                throw;
            }

            SendResultMail(config.AdminEmail, resultInfos);
        }

        private void ImageCompressCheck(ref List<ExecuteResult> resultInfos)
        {
            string purpose = "圖片壓縮檢查";

            _timeingMethod.Start();                       
 
            //var queryTypes = new int[] { 
            //    (int)PcpImageType.Card
            //    , (int)PcpImageType.EnvironmentAroundStore
            //    , (int)PcpImageType.Service };
            //var data = pcp.PcpImageGetUnCompress(queryTypes);

            _timeingMethod.Stop(purpose, "DB running", resultInfos);

            _timeingMethod.Start();

            //const string service = "/ControlRoom/vbs/membership/ImageCompress";
            const string service = "/webservice/pcpservice.asmx/ImageCompress";

            var url = string.Format(@"{0}{1}", config.SiteUrl, service);
            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            if (req == null) { }
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.KeepAlive = false;
            req.ContentLength = 0;
            req.GetResponse();

            _timeingMethod.Stop(purpose, "AP running", resultInfos);
        }

        private void DeleteImage(ref List<ExecuteResult> resultInfos)
        {
            string purpose = "圖片刪除";

            _timeingMethod.Start();

            var queryTime = DateTime.Now.AddDays(-20);
            var deleteData = pcp.PcpImageGetIsDelete(queryTime);

            _timeingMethod.Stop(purpose, "DB running", resultInfos);
            
            _timeingMethod.Start();

            var data = new List<string>();

            foreach (var item in deleteData)
            {
                if (!string.IsNullOrWhiteSpace(item.ImageUrl)) { data.Add(item.ImageUrl); }
                if (!string.IsNullOrWhiteSpace(item.ImageUrlCompressed)) { data.Add(item.ImageUrlCompressed); }
            }

            foreach (var x in data)
            {
                var imageUrl = ImageFacade.GetMediaPath(x, MediaType.PCPImage);

                var relateUrl = string.Join("\\", imageUrl.Split('/').Skip(3).ToArray());
                var path = Path.Combine(HttpRuntime.AppDomainAppPath, relateUrl);

                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }

            pcp.PcpImageDelete(deleteData.Select(x => x.Id).ToArray());
                        
            _timeingMethod.Stop(purpose, "AP running", resultInfos);
        }

        private void RegularsPointToTrust(ref List<ExecuteResult> resultInfos)
        {
            LunchKingSite.BizLogic.Facade.TrustFacade.RegularsPointToTrust(new DateTime(2016, 1, 3));
            LunchKingSite.BizLogic.Facade.TrustFacade.TrustToTrustBank(new DateTime(2016, 1, 3));
            LunchKingSite.BizLogic.Facade.TrustFacade.TrustToReport();
        }

        #region 檢查到期的公關點或熟客點
        private void CheckPointExpirationDate(ref List<ExecuteResult> resultInfos)
        {
            string purpose = "檢查到期的公關點或熟客點";
            
            _timeingMethod.Start();

            var expiredPoint = pcp.ViewPcpPointDepositGetByDatetime(DateTime.Now.AddSeconds(1));

            _timeingMethod.Stop(purpose, "DB running", resultInfos);

            if (expiredPoint.Any())
            {
                foreach (var item in expiredPoint)
                {
                    var withdrawalOverall = new PcpPointWithdrawalOverall
                    {
                        Type = (int)PcpPointWithdrawalOverallType.System,
                        Point = item.Point - item.WithdrawalPoint ?? 0,
                        PointType = item.Type,
                        CreateId = config.SystemUserId,
                        CreateTime = DateTime.Now,
                        UserId = item.UserId
                        
                    };

                    TransactionOptions tranOpts = new TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted
                    };
                    using (var trans = new TransactionScope(TransactionScopeOption.RequiresNew, tranOpts))
                    {
                        pcp.PcpPointWithdrawalOverallSet(withdrawalOverall);

                        if (withdrawalOverall.Id != 0)
                        {
                            var withdrawal = new PcpPointWithdrawal
                            {
                                DepositId = item.Id,
                                OverallId = withdrawalOverall.Id,
                                Type = item.Type,
                                Point = withdrawalOverall.Point,
                                Message = "逾期未使用完畢，系統自動刪除。",
                                CreateId = config.SystemUserId,
                                CreateTime = DateTime.Now,
                                UserId = item.UserId
                            };

                            pcp.PcpPointWithdrawalSet(withdrawal);

                            trans.Complete();

                            resultInfos.Add(new ExecuteResult
                            {
                                title = purpose,
                                IsSuccess = true,
                                Message = string.Format("新增成功。WithdrawalOverallId＝{0}，DepositId＝{1}",
                                withdrawal.OverallId, withdrawal.DepositId)
                            });
                        }
                        else
                        {
                            resultInfos.Add(new ExecuteResult
                            {
                                title = purpose,
                                IsSuccess = false,
                                Message = string.Format("{0}新增WithdrawalOverall失敗。POINT＝{1};TYPE＝{2}", withdrawalOverall.UserId
                                , withdrawalOverall.Point, withdrawalOverall.Type)
                            });
                        }
                    }
                }
            }
        }
        #endregion

        private void CheckFavors(ref List<ExecuteResult> resultInfos)
        {
        }

        private void UserMembershipStatistics(ref List<ExecuteResult> resultInfos)
        {
            string purpose = "統計熟客卡會員";

            _timeingMethod.Start();

            var startDate = DateTime.Now.AddDays(-1).SetTime(new TimeSpan(0, 0, 0));
            var endDate = startDate.SetTime(new TimeSpan(23, 59, 59));

            var checkExist = pcp.UserGetCardDailyReportGet(endDate);

            if (checkExist.Any()) { return; }

            var data = pcp.UserGetCardDailyReportGetList(startDate);

            _timeingMethod.Stop(purpose, "DB running", resultInfos);
            
            _timeingMethod.Start();

            data.ForEach(x => x.ReportDate = endDate);

            if (pcp.UserGetCardDailyReportSet(data) != 0)
            {
                resultInfos.Add(new ExecuteResult
                {
                    title = purpose,
                    IsSuccess = true,
                    Message = string.Format("新增成功。筆數{0}", data.Count())
                });
            }

            _timeingMethod.Stop(purpose, "AP running", resultInfos);
        }   

        private void CheckMemberCard(ref List<ExecuteResult> resultInfos)
        {
            //string purpose = "每季檢查會員的會員卡升級或延長";

            _timeingMethod.Start();
            var dt = DateTime.Now;
            //var dt = new DateTime(2015, 10, 1);
            var qX = Helper.GetQuarterByDate(dt);
            var lQX = Helper.GetQuarterByDate(dt.AddMonths(-3));

            DateTime startDate, endDate, lastStartDate, lastEndDate;
            Helper.GetQuarterDateRange(dt.Year, (Quarter)qX, out startDate, out endDate);
            Helper.GetQuarterDateRange(dt.AddMonths(-3).Year, (Quarter)lQX, out lastStartDate, out lastEndDate);

            //var startDate = dt.AddMonths(-3).SetTime(new TimeSpan(0, 0, 0));
            //var endDate = dt.SetTime(new TimeSpan(0, 0, 0));
            //將會員卡啟用且到期的撈出來
            var data = pcp.ViewMembershipCardGetListByEnableDatetime((int)MembershipCardStatus.Open, lastStartDate, lastEndDate);

            foreach (var memberShipcards in data.GroupBy(x=>x.CardGroupId,x=>x))
            {
                //依照商家檢查有無新卡片
                //依卡片等級更新會員
                var newCard = pcp.ViewMembershipCardGetListByDatetime(memberShipcards.Key, dt.AddMinutes(10)).ToList();
                if (newCard.Any())
                {
                    newCard = newCard.Where(x => x.Level > 0).ToList();
                }

                foreach (var item in memberShipcards)
                {
                    //將受影響的會員撈出來
                    var d = pcp.UserMembershipCardGetByCardId(item.CardId);
                    var lvls = item.Level;
                    d.ForEach(x =>
                    {
                        //有新卡則更新會員卡號
                        var newCardId = x.CardId;
                        if (newCard.Any())
                        {
                            //有商家自定分級卡片時將0級卡片會員自動升等成最低等級會員卡
                            if (lvls == 0)
                            {
                                newCardId = newCard.OrderBy(g => g.Level).First().CardId;
                            }
                            else
                            {
                                //若領取非0級會員卡則將卡片更新為同等級新卡或原等級小於等於新卡等級第一筆卡片
                                var tempCards = newCard.Where(g => g.Level <= lvls).ToList();
                                if (tempCards.Any())
                                {
                                    newCardId = tempCards.OrderByDescending(g => g.Level).First().CardId;
                                }
                            }
                            
                        }

                        x.CardId = newCardId;
                        x.StartTime = startDate;
                        x.EndTime = endDate;
                        x.ModifyTime = dt;
                        x.ModifyId = config.SystemUserId;
                    });

                    pcp.UserMembershipCardSet(d);

                    //無：延長會員卡期限
                    if (!newCard.Any())
                    {
                        var updData = pcp.MembershipCardGroupVersionGet(item.VersionId);
                        updData.OpenTime = startDate;
                        updData.CloseTime = endDate;
                        pcp.MembershipCardGroupVersionSet(updData);

                        var cards = pcp.MembershipCardGetByGroupAndVersion(item.CardGroupId, item.VersionId);
                        cards.ForEach(
                            x =>
                            {
                                x.OpenTime = startDate;
                                x.CloseTime = endDate;
                                x.ModifyTime = dt;
                                x.ModifyId = config.SystemUserId;
                            }
                        );
                        pcp.MembershipCardSet(cards);
                    }
                    else
                    //有：將過季作廢
                    {
                        var cards = pcp.MembershipCardGetByGroupAndVersion(item.CardGroupId, item.VersionId);

                        cards.ForEach(
                            x =>
                            {
                                x.Status = (int)MembershipCardStatus.Cancel;
                                x.ModifyTime = dt;
                                x.ModifyId = config.SystemUserId;
                            }
                        );

                        pcp.MembershipCardSet(cards);
                    }
                }
            }

            //resultInfos.Add(new ExecuteResult
            //{
            //    title = purpose,
            //    IsSuccess = true,
            //    Message = string.Format("新增成功。筆數{0}", data.Count())
            //});
        }

        private void SendResultMail(string mailTo, List<ExecuteResult> resultInfos, string subject = "")
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in resultInfos.OrderBy(x => x.IsSuccess).GroupBy(x => x.IsSuccess))
            {
                sb.Append(string.Format("【{0}】執行結果如下:<br/>", item.Key));
                foreach (var detail in item)
                {
                    sb.Append(string.Format("{0}<br/>", detail.Message));
                }
                sb.Append(string.Format("【{0}】執行結果結束:<br/>", item.Key));
                sb.Append(string.Format("＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br/>"));
            }

            try
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(mailTo);
                msg.Subject = string.Format("{0}：[系統]{1}", Environment.MachineName, "PCPJob Running");
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = sb.ToString();
                //PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                logger.Error("PCPCheck running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
        private class TimeingMethod
        {
            private readonly Stopwatch _stopwatch;

            public TimeingMethod()
            {
                _stopwatch = new Stopwatch();
            }

            internal void Start()
            {
                _stopwatch.Start();
            }

            internal void Stop(string p, string target, List<ExecuteResult> resultInfos)
            {
                resultInfos.Add(new ExecuteResult
                {
                    title = p,
                    IsSuccess = true,
                    Message = string.Format("{0}：{1}", target, _stopwatch.Elapsed)
                });
                _stopwatch.Reset();
            }
        }

        private class ExecuteResult
        {
            public string title { get; set; }
            public bool IsSuccess { get; set; }
            public string Message { get; set; }
        }
    }
}

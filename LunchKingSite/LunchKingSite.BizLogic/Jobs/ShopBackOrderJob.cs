﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class ShopBackOrderJob : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(ShopBackOrderJob));

        private static object thisLock = new object();
        private static bool _isRunning;

        protected static bool IsRunning
        {
            get
            {
                lock (thisLock)
                {
                    return _isRunning;
                }
            }
            set
            {
                lock (thisLock)
                {
                    _isRunning = value;
                }
            }
        }

        public void Execute(JobSetting js)
        {
            if (IsRunning)
            {
                return;
            }
            IsRunning = true;

            string result = string.Empty;

            DateTime endTime = DateTime.Now;
            //若是用atm、超商繳款，只有當日繳完，才是有效訂單。另外有加一天作為buffer，所以共兩天
            DateTime startTime = DateTime.Today.AddDays(-2);
            result = OrderFacade.ShopBackOrderAPI(startTime, endTime);

            logger.Info(string.Format("ShopBackOrderAPI Job =>{0}",result));
            IsRunning = false;
        }
    }
}

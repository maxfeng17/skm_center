﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using log4net;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Jobs
{
    public class DelayAmercement : IJob
    {
        private static ILog logger = LogManager.GetLogger(typeof(PaymentCharge));
        private DateTime deliverTimeS = DateTime.Now;
        protected static IAccountingProvider ap;
        protected static IPponProvider pp;
        protected static ISysConfProvider config;
        protected static IOrderProvider op;
        protected static IMemberProvider mp;

        static DelayAmercement()
        {
            config = ProviderFactory.Instance().GetConfig();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public void Execute(JobSetting js)
        {
            string subject = "";
            DateTime _today = DateTime.Now;
            try
            {
                subject = _today.ToString("yyyy") + "年" + _today.ToString("MM") + "月" + _today.ToString("dd") + "日​商家延遲出貨​系統​扣款​​清單";
                DataTable delayAmercementBid =
                    pp.GetDelayBidAmercements(_today.AddDays(-1).ToString("yyyy/MM/dd"));

                if (delayAmercementBid.Rows.Count > 0)
                {
                    StringBuilder mailContent = new StringBuilder();
                    
                    mailContent.Append("統計日期：" + _today.ToString("yyyy") + "年" + _today.ToString("MM") + "月" + _today.ToString("dd") + "日。<br/>");

                    /*
                     * 延遲出貨扣款
                     * */
                    string content = delayAmercementSet(delayAmercementBid);

                    mailContent.Append(content);

                    SendEmail(config.AdminEmail, new[] { config.CodEmail, config.ServiceEmail }, subject,
                        mailContent.ToString());
                }
            }
            catch (Exception ex)
            {
                SendEmail(config.AdminEmail, new[] { config.AdminEmail }, subject,
                    string.Format("Message： {0} \r\n Trace： {1}", ex.Message, ex.StackTrace), false);
                logger.Error(ex);
            }
        }
        /// <summary>
        /// 寫入延遲付款檔
        /// </summary>
        /// <param name="bids"></param>
        private string delayAmercementSet(DataTable delayAmercementBid)
        {
            //int PaymentChangeId = 0;
            DateTime setDate = DateTime.Now;
            StringBuilder content = new StringBuilder();

            var guids = from x in delayAmercementBid.AsEnumerable()
                        group x by new { Bid = x.Field<Guid>("business_hour_guid").ToString() } into g
                        select new
                        {
                            Bid = g.Key.Bid
                        };

            content.AppendLine("<table border='0'>");

            foreach (var bid in guids)
            {
                Guid obid;
                Guid.TryParse(bid.Bid.ToString(), out obid);

                /*
                 * 取出該檔次所有的訂單資料
                 * */
                var ctlogs = delayAmercementBid.AsEnumerable().Where(x => x.Field<Guid>("business_hour_guid").Equals(obid))
                            .Select(x => new CashTrustLog {
                                TrustId = x.Field<Guid>("trust_id"),
                                Amount = x.Field<int>("amount"),
                                OrderGuid = x.Field<Guid>("order_guid"),
                                BusinessHourGuid = x.Field<Guid>("business_hour_guid")
                            });

                var qctlogs = from q in ctlogs
                            group q by new { q.OrderGuid, q.BusinessHourGuid } into g
                            select new
                            {
                                Amount = g.Sum(q => q.Amount),    //把單筆訂單的金額加總(不用排除運費)
                                OrderGuid = g.Key.OrderGuid,
                                BusinessHourGuid = g.Key.BusinessHourGuid,
                            };

                ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(obid);
                content.AppendLine("<tr><td colspan='3'>");
                content.AppendLine("-----------------------------------------------------------------------------------------");
                content.AppendLine("</td></tr>");
                content.AppendLine("<tr><td>賣家名稱</td><td>檔號</td><td>檔名(短標)</td></tr>");
                content.AppendLine("<tr>");
                content.AppendLine("<td>" + vpd.SellerName + "</td>");
                content.AppendLine("<td>" + vpd.UniqueId + "</td>");
                content.AppendLine("<td>" + vpd.ItemName + "</td>");
                content.AppendLine("</tr>");
                content.AppendLine("<tr><td colspan='3'></td><tr>");

                foreach (var ctlog in qctlogs)
                {
                    /*
                     * 寫入vendor_payment_change
                     * 
                     * 以單筆訂單結帳金額為罰款基准
                     * 500元以下，罰金30元
                     * 500~999元，罰金50元
                     * 1000元以上，罰金100元
                     * */
                    var DebitAmount = 0;
                    if (ctlog.Amount > 0 && ctlog.Amount < 500)
                    {
                        DebitAmount = 30;
                    }
                    else if (ctlog.Amount >= 500 && ctlog.Amount < 1000)
                    {
                        DebitAmount = 50;
                    }
                    else if (ctlog.Amount >= 1000)
                    {
                        DebitAmount = 100;
                    }

                    if (DebitAmount > 0)
                    {
                        var vpc = new VendorPaymentChange
                        {
                            BusinessHourGuid = ctlog.BusinessHourGuid ?? Guid.Empty,
                            Reason = "延遲出貨扣款",
                            Amount = DebitAmount,   //每筆金額(跟賣家扣款)
                            PromotionValue = 0,     //金額(須給消費者)
                            CreateId = "sys",
                            CreateTime = setDate,
                            AllowanceAmount = DebitAmount,   //每筆金額(跟賣家扣款)
                            ModifyId = "sys",
                            ModifyTime = setDate
                        };

                        //先觀察一陣子再寫入資料庫
                        //PaymentChangeId = ap.VendorPaymentChangeSet(vpc);

                        /*
                        * 寫入member_none_deposit(不須給消費者紅利金需寫入此table，主要為記錄order_id)
                        * 如果要給消費者紅利金，則寫入Member_Promotion_Deposit
                        * */
                        //var mnd = new MemberNoneDeposit
                        //{
                        //    OrderGuid = ctlog.OrderGuid,
                        //    VendorPaymentChangeId = PaymentChangeId,
                        //    CreateId = "sys",
                        //    CreateTime = setDate
                        //};
                        
                        //mp.MemberNoneDepositSet(mnd);

                        /*
                         * 更新狀態為延遲出貨
                         * */
                        var ctlogc = mp.CashTrustLogListGetByOrderId(ctlog.OrderGuid);
                        CashTrustStatusLogCollection ctslc = new CashTrustStatusLogCollection();
                        foreach (CashTrustLog _ctlog in ctlogc)
                        {
                            _ctlog.SpecialStatus = (int)Helper.SetFlag(true, _ctlog.SpecialStatus, TrustSpecialStatus.DeliveryDelay);

                            /*
                             * 更新cash_trust_status_log
                             * */
                            var ctsl = new CashTrustStatusLog
                            {
                                TrustId = _ctlog.TrustId,
                                TrustProvider = _ctlog.TrustProvider,
                                Amount = _ctlog.Amount,
                                Status = _ctlog.Status,
                                ModifyTime = setDate,
                                CreateId = "sys",
                                SpecialStatus = _ctlog.SpecialStatus,
                                SpecialOperatedTime = _ctlog.SpecialOperatedTime
                            };
                            ctslc.Add(ctsl);
                        }
                        //cash_trust_log
                        //mp.CashTrustLogCollectionSet(ctlogc);
                        //cash_trust_status_log
                        //mp.CashTrustStatusLogCollectionSet(ctslc);


                        Order pod = op.OrderGet(ctlog.OrderGuid);
                        if (pod != null)
                        {
                            content.AppendLine("<tr><td>扣款訂單編號</td><td>訂單金額</td><td>扣款金額</td></tr>");
                            content.AppendLine("<tr>");
                            content.AppendLine("<td>" + pod.OrderId + "</td>");
                            content.AppendLine("<td>" + ctlog.Amount + "</td>");
                            content.AppendLine("<td>" + DebitAmount + "</td>");
                            content.AppendLine("</tr>");
                        }
                    }
                }
            }
            content.AppendLine("</table>");

            return content.ToString();
        }
        private void SendEmail(string from, IEnumerable<string> to, string subject, string body, bool ishtml = true)
        {
            try
            {
                var msg = new MailMessage();

                foreach (string s in to)
                {
                    msg.To.Add(s);
                }
                msg.Subject = subject;
                msg.From = new MailAddress(from);
                msg.IsBodyHtml = ishtml;
                msg.Body = body;
                PostMan.Instance().Send(msg, SendPriorityType.Normal);
            }
            catch (Exception ex)
            {
                logger.Error("DelayAmercement running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

    }
}

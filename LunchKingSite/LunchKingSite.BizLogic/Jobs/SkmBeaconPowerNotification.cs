﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Jobs
{
    public class SkmBeaconPowerNotification : IJob
    {
        public void Execute(JobSetting js)
        {
            SkmFacade.SkmBeaconLowPowerNotify();
        }
    }
}

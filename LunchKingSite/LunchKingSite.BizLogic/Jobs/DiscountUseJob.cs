﻿using System;
using System.Net.Mail;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.BizLogic.Facade;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Jobs
{
    class DiscountUseJob : IJob
    {
        private ILog log = LogManager.GetLogger(typeof(DiscountUseJob));

        public void Execute(JobSetting js)
        {
            try
            {
                log.Info("DiscountUseJob Start");
                PromotionFacade.UpDateDiscountUseType();
                log.Info("DiscountUseJob End");
            }
            catch (Exception ex)
            {
                log.Error("DiscountUseJob alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using log4net;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.BizLogic.Jobs
{
    /// <summary>
    /// 自動退回超過3天沒人領取的禮物
    /// </summary>
    public class ReturnUnacceptedExpiredGiftJob : IJob
    {
        private static ISysConfProvider _config;
        private static IMGMProvider _mgmProv;
        private static ILog logger = LogManager.GetLogger(typeof(ReturnUnacceptedExpiredGiftJob));
        
        static ReturnUnacceptedExpiredGiftJob()
        {
            _config = ProviderFactory.Instance().GetConfig();
            _mgmProv = ProviderFactory.Instance().GetProvider<IMGMProvider>();
        }

        public void Execute(JobSetting js)
        {
            try
            {
                MGMFacade.ReturnUnacceptedExpiredGiftAndUnlockCoupon();
            }
            catch (Exception ex)
            {
                logger.Error("ReturnUnacceptedExpiredGiftJob running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
        

    }
    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;

namespace LunchKingSite.BizLogic.Jobs
{
    public class FamilyNetPincodeUnlockJob : IJob
    {
        public void Execute(JobSetting js)
        {
            FamiGroupCoupon.ExecuteUnlockPincode();
        }
    }
}

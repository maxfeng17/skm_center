﻿using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace LunchKingSite.BizLogic.Jobs
{
    public class YahooVoucher : IJob
    {
        public void Execute(JobSetting js)
        {
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            int type = (Math.Abs((now - today).TotalMinutes) < 5 || Math.Abs((now - today.AddHours(12)).TotalMinutes) < 5) ? 5 : 6;
            string url = "http://discount.17life.com/api/refreshpromo/70553216/" + type;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";
                request.ContentLength = 0;
                request.Timeout = 180000;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            string responseFromServer = reader.ReadToEnd();
                            if (type == 5)
                            {
                                MailMessage msg = new MailMessage();
                                msg.To.Add("shihling@17life.com");
                                msg.Subject = "Yahoo+資料更新";
                                msg.From = new MailAddress("shihling@17life.com");
                                msg.IsBodyHtml = true;
                                msg.Body = responseFromServer;

                                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                            }
                        }
                    }
                }
            }
            catch 
            { 
            
            }
        }
    }
}

﻿using System;
using System.Net.Mail;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using log4net;

namespace LunchKingSite.BizLogic.Jobs
{
    public class GenerateVerificationAccount : IJob
    {
        private static ISysConfProvider _config;
        private static readonly ILog log = LogManager.GetLogger(typeof(PponDealChore));

        public GenerateVerificationAccount()
        {
            _config = ProviderFactory.Instance().GetConfig();
        }

        public void Execute(JobSetting js)
        {
            var msg = new MailMessage();
            string subject = "[系統]核銷帳號產出執行 - ";
            try
            {
                var vf = new VerificationFacade();
                int result = vf.SetAllInfo(1);
                switch (result)
                {
                    case -1:
                        subject += "有錯！";
                        break;
                    case 0:
                        subject += "本次執行沒有新增的店家/分店帳密(無內文)";
                        break;
                    default:
                        subject += "完成！本次新增"+ result +"筆帳密資料(無內文)";
                        break;
                }

                msg.To.Add(_config.AdminEmail);
                msg.Subject = subject;
                msg.From = new MailAddress(_config.AdminEmail);
                msg.Body = string.Empty;
            }
            catch (Exception ex)
            {
                msg.Body = ex.Message;
                log.Error("核銷帳號產出執行錯誤(GenerateVerificationAccount):" + ex.Message + "<br/>" + ex.StackTrace);
            }finally
            {
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }
    }
}

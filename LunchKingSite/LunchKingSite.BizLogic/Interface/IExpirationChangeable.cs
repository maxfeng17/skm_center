﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Interface
{
    public interface IExpirationChangeable
    {
		/// <summary>
		/// 檔次的開始使用日.
		/// </summary>
		DateTime? DealUseStartDate { get; }

        /// <summary>
        /// 檔次預設的到期日.
        /// </summary>
        DateTime? DealOriginalExpireDate { get; }

        /// <summary>
        /// 檔次變更使用截止日.
        /// </summary>
        DateTime? DealChangedExpireDate { get; }

        /// <summary>
        /// 分店變更使用截止日.
        /// </summary>
        DateTime? StoreChangedExpireDate { get; }

        /// <summary>
        /// 賣家結束營業日.
        /// </summary>
        DateTime? SellerClosedDownDate { get; }

        /// <summary>
        /// 分店結束營業日.
        /// </summary>
        DateTime? StoreClosedDownDate { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Interface
{
    public interface IVendorBillingHelper
    {
        VendorBillingModel VendorBillingModel { get; }

        RemittanceType FinancialStaffPaymentMethod { get; }

        /// <summary>
        /// 是否匯款給總店
        /// </summary>
        /// <returns></returns>
        bool IsPayToCompany { get; }

        /// <summary>
        /// 參予商品販賣的商店 Guid 列表. 若沒有分店設定, 請設定為空集合, 不要 null. 
        /// </summary>
        /// <returns></returns>
        IEnumerable<Guid> ParticipatingStores { get; }

        /// <summary>
        /// 提供成功單的所有信託資料
        /// </summary>
        /// <returns></returns>
        CashTrustLogCollection GetAllSuccessfulOrderTrustList();
        CashTrustLogCollection GetAllSuccessfulOrderTrustList(DateTime genDate);
    }
}

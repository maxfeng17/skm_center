﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.BizLogic.Interface
{
    public interface IdocumentModel
    {
        string Header { get; set; }
        List<string> RowList { set; }
        string csvModelGen(string path, string fileName);
    }
}

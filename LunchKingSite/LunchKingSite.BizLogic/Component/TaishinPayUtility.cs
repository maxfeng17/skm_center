using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class TaishinPayUtility
    {
        private static readonly DateTime UnixEpoch =
            new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(TaishinPayUtility).Name);

        public const int _RTN_CODE_OK = 0;

        static TaishinPayUtility()
        {
            ServicePointManager.ServerCertificateValidationCallback =
                    delegate { return true; };
        }
        
        public static int GetCurrentTimestamp()
        {
            return GetCurrentTimestamp(DateTime.Now);
        }

        public static int GetCurrentTimestamp(DateTime time)
        {
            int totalSeconds = (int)(time.ToUniversalTime() - UnixEpoch).TotalSeconds;
            return totalSeconds;
        }

        public static string EncytSignature(params object[] args)
        {
            StringBuilder sb = new StringBuilder();
            foreach (object arg in args)
            {
                sb.Append(arg);
            }
            return Security.GetSHA1Hash(sb.ToString());
        }

        /// <summary>
        /// 以一次性認證code，請求綁定確認
        /// </summary>
        /// <param name="code"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static TaishinPayBindResult GetTokenByCode(string code, int userId)
        {
            string clientId = config.TaishinThirdPartyPayClientId;
            string clientPw = config.TaishinThirdPartyPayClientPassword;
            int timestamp = GetCurrentTimestamp();
            string signature = EncytSignature(clientId, code, userId.ToString(), clientPw, timestamp);
            string apiUrl = Helper.CombineUrl(config.TaishinThirdPartyPayApiUrl, "api/BindUserResult");

            var request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = WebRequestMethods.Http.Post;
            var jsonText = new JsonSerializer().Serialize(
                new
                {
                    client_id = clientId,
                    auth_code = code,
                    user_id = userId.ToString(),
                    timestamp,
                    signature
                });

            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;
            string responseResult = "";

            try
            {
                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                    requestStream.Flush();
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            return null;
                        }

                        using (var reader = new StreamReader(stream))
                        {
                            var result = reader.ReadToEnd();
                            dynamic json = ProviderFactory.Instance().GetSerializer().DeserializeDynamic(result);
                            if (json.rtnCode == null)
                            {
                                responseResult = result;
                            }

                            if ((int)json.rtnCode == _RTN_CODE_OK)
                            {
                                return new TaishinPayBindResult()
                                    {
                                        AccessToken = (string)json.access_token,
                                        RtnCode = (int)json.rtnCode,
                                        RtnMsg = (string)json.rtnMsg
                                    };
                            }
                            else 
                            {
                                var apiLog = new ThirdPartyPayApiLog()
                                {
                                    UserId = userId,
                                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                                    ApiUrl = apiUrl,
                                    ReturnCode = (int)json.rtnCode,
                                    ReturnMsg = (string)json.rtnMsg,
                                    Memo = Dns.GetHostName(),
                                    SourceIp = Helper.GetClientIP()
                                };
                                op.ThirdPartyPayApiLogSet(apiLog);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var apiLog = new ThirdPartyPayApiLog()
                {
                    UserId = userId,
                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                    ApiUrl = apiUrl,
                    Memo = Dns.GetHostName() + "Exception Error!",
                    SourceIp = Helper.GetClientIP()
                };
                op.ThirdPartyPayApiLogSet(apiLog);
                logger.Error(ex + ", JosonTest=" + jsonText + ", ResponseResult: " + responseResult);
            }

            return null;
        }

        /// <summary>
        /// 解除綁定
        /// </summary>
        /// <param name="token"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool UnbindTaishinMember(string token, int userId)
        {
            string clientId = config.TaishinThirdPartyPayClientId;
            string clientPw = config.TaishinThirdPartyPayClientPassword;
            int timestamp = GetCurrentTimestamp();
            string signature = EncytSignature(token, clientId, clientPw, timestamp);
            string apiUrl = Helper.CombineUrl(config.TaishinThirdPartyPayApiUrl, "api/BindUnbind");
            var request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = WebRequestMethods.Http.Post;
            var jsonText = new JsonSerializer().Serialize(
                new
                {
                    access_token = token,
                    client_id = clientId,
                    timestamp,
                    signature
                });

            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;
            string responseResult = "";

            try
            {
                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                    requestStream.Flush();
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            return false;
                        }

                        using (var reader = new StreamReader(stream))
                        {
                            var result = reader.ReadToEnd();
                            dynamic json = ProviderFactory.Instance().GetSerializer().DeserializeDynamic(result);
                            if (json.rtnCode == null)
                            {
                                responseResult = result;
                            }

                            var apiLog = new ThirdPartyPayApiLog()
                            {
                                UserId = userId,
                                PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                                ApiUrl = apiUrl,
                                ReturnCode = (int)json.rtnCode,
                                ReturnMsg = (string)json.rtnMsg,
                                Memo = Dns.GetHostName(),
                                SourceIp = Helper.GetClientIP()
                            };
                            op.ThirdPartyPayApiLogSet(apiLog);

                            if ((int)json.rtnCode == _RTN_CODE_OK)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var apiLog = new ThirdPartyPayApiLog()
                {
                    UserId = userId,
                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                    ApiUrl = apiUrl,
                    Memo = Dns.GetHostName() + "Exception Error!",
                    SourceIp = Helper.GetClientIP()
                };
                op.ThirdPartyPayApiLogSet(apiLog);
                logger.Error(ex + ", JosonTest=" + jsonText + ", ResponseResult: " + responseResult);
            }

            return false;
        }
        
        /// <summary>
        /// 查詢儲值帳戶
        /// </summary>
        /// <param name="token"></param>
        /// <param name="errMsg"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static TaishinMemberInfo GetTaishinMemberInfo(int userId, string token, out string errMsg)
        {
            errMsg = string.Empty;

            string clientId = config.TaishinThirdPartyPayClientId;
            string clientPw = config.TaishinThirdPartyPayClientPassword;
            int timestamp = GetCurrentTimestamp();
            string signature = EncytSignature(token, clientId, clientPw, timestamp);
            string apiUrl = Helper.CombineUrl(config.TaishinThirdPartyPayApiUrl, "api/BindUserInfo");

            var request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = WebRequestMethods.Http.Post;
            var jsonText = new JsonSerializer().Serialize(
                new
                {
                    access_token = token,
                    client_id = clientId,
                    timestamp,
                    signature
                });

            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;
            string responseResult = "";
            
            try
            {
                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                    requestStream.Flush();
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            return null;
                        }

                        using (var reader = new StreamReader(stream))
                        {
                            var result = reader.ReadToEnd();
                            dynamic json = ProviderFactory.Instance().GetSerializer().DeserializeDynamic(result);
                            if (json.rtnCode == null)
                            {
                                responseResult = result;
                            }

                            if ((int)json.rtnCode == _RTN_CODE_OK)
                            {
                                return new TaishinMemberInfo()
                                {
                                    AccountName = (string)json.account_name,
                                    AccountAmount = (int)json.account_amt
                                };
                            }
                            else
                            {
                                var apiLog = new ThirdPartyPayApiLog()
                                {
                                    UserId = userId,
                                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                                    ApiUrl = apiUrl,
                                    ReturnCode = (int)json.rtnCode,
                                    ReturnMsg = (string)json.rtnMsg,
                                    Memo = Dns.GetHostName(),
                                    SourceIp = Helper.GetClientIP()
                                };
                                op.ThirdPartyPayApiLogSet(apiLog);

                                //非同步綁定:台新沒綁而17Life有綁時，自行解除綁定
                                if ((int)json.rtnCode == (int)TaishinPayReturnCodeAndMsg.BindAccessTokenError)
                                {
                                    mp.MemberTokenDelete(userId, ThirdPartyPayment.TaishinPay);
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var apiLog = new ThirdPartyPayApiLog()
                {
                    UserId = userId,
                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                    ApiUrl = apiUrl,
                    Memo = Dns.GetHostName() + "Exception Error!",
                    SourceIp = Helper.GetClientIP()
                };
                op.ThirdPartyPayApiLogSet(apiLog);
                logger.Error(ex + ", JosonTest=" + jsonText + ", ResponseResult: " + responseResult);
            }
            
            return null;
        }

        /// <summary>
        /// 請求一次性支付Code
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="orderId"></param>
        /// <param name="itemName"></param>
        /// <param name="amount"></param>
        /// <param name="buyerUserId"></param>
        /// <param name="bid"></param>
        /// <param name="transLogId"></param>
        /// <param name="rtnCode"></param>
        /// <returns></returns>
        public static string RequestPayCodeFromTaishin(string accessToken, string orderId, string itemName, decimal amount, int buyerUserId, Guid bid, int transLogId, out int rtnCode)
        {
            string clientId = config.TaishinThirdPartyPayClientId;
            string clientPw = config.TaishinThirdPartyPayClientPassword;
            string orderName = itemName;
            string virtualAcct = config.TaishinThirdPartyPayVirtualAcctStart + transLogId.ToString("000000000");
            string sucessUrl = Helper.CombineUrl(config.SSLSiteUrl, "/mvc/ThirdPartyPay/TaishinPaySuccess");
            string failureUrl = Helper.CombineUrl(config.SSLSiteUrl, "/mvc/ThirdPartyPay/TaishinPayFailure?orderId=" + orderId + "&bid=" + bid);

            int timestamp = GetCurrentTimestamp();
            string signature = EncytSignature(accessToken, orderId, orderName, amount, clientId, virtualAcct, sucessUrl, failureUrl, clientPw, timestamp);
            string apiUrl = Helper.CombineUrl(config.TaishinThirdPartyPayApiUrl, "/api/BindPayReq");
            
            var request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = WebRequestMethods.Http.Post;
            var jsonText = new JsonSerializer().Serialize(
                new
                {
                    access_token = accessToken,
                    order_id = orderId,
                    order_name = orderName,
                    amount,
                    client_id = clientId,
                    virtual_acct = virtualAcct,
                    success_url = sucessUrl,
                    failure_url = failureUrl,
                    timestamp,
                    signature
                });

            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;
            string responseResult = "";
            rtnCode = -1;

            try
            {
                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                    requestStream.Flush();
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            return null;
                        }

                        using (var reader = new StreamReader(stream))
                        {
                            var result = reader.ReadToEnd();
                            dynamic json = ProviderFactory.Instance().GetSerializer().DeserializeDynamic(result);
                            if (json.rtnCode == null)
                            {
                                responseResult = result;
                            }

                            rtnCode = json.rtnCode;
                            if ((int)json.rtnCode == _RTN_CODE_OK)
                            {
                                return json.code;
                            }
                            else
                            {
                                var apiLog = new ThirdPartyPayApiLog()
                                {
                                    UserId = buyerUserId,
                                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                                    ApiUrl = apiUrl,
                                    ReturnCode = (int)json.rtnCode,
                                    ReturnMsg = (string)json.rtnMsg,
                                    Memo = Dns.GetHostName(),
                                    SourceIp = Helper.GetClientIP()
                                };
                                op.ThirdPartyPayApiLogSet(apiLog);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var apiLog = new ThirdPartyPayApiLog()
                {
                    UserId = buyerUserId,
                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                    ApiUrl = apiUrl,
                    Memo = Dns.GetHostName() + "Exception Error!",
                    SourceIp = Helper.GetClientIP()
                };
                op.ThirdPartyPayApiLogSet(apiLog);
                logger.Error(ex + ", JosonTest=" + jsonText + ", ResponseResult: " + responseResult);
            }
            
            return null;
        }

        /// <summary>
        /// 至台新平台請求消費者同意
        /// </summary>
        /// <param name="code"></param>
        /// <param name="orderId"></param>
        public static void RequestPayToTaishin(string code, string orderId)
        {
            string clientId = config.TaishinThirdPartyPayClientId;
            string clientPw = config.TaishinThirdPartyPayClientPassword;
            int timestamp = GetCurrentTimestamp();
            string signature = EncytSignature(clientId, orderId, code, clientPw, timestamp);
            string apiUrl = Helper.CombineUrl(config.TaishinThirdPartyPayUserUrl, "/third_party/Pay");

            var request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = WebRequestMethods.Http.Post;
            var jsonText = new JsonSerializer().Serialize(
                new
                {
                    client_id = clientId,
                    order_id = orderId,
                    code,
                    timestamp,
                    signature
                });

            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;

            try
            {
                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                    requestStream.Flush();
                }
            }
            catch (Exception ex)
            {
                var apiLog = new ThirdPartyPayApiLog()
                {
                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                    ApiUrl = apiUrl,
                    Memo = Dns.GetHostName() + "Exception Error!",
                    SourceIp = Helper.GetClientIP()
                };
                op.ThirdPartyPayApiLogSet(apiLog);
                logger.Error(ex.ToString());
            }
        }

        /// <summary>
        /// 以使用者與台新平臺己確認的code，確認付款動作完成
        /// </summary>
        /// <param name="payCode"></param>
        /// <param name="orderId"></param>
        /// <param name="orderName"></param>
        /// <param name="payAmount"></param>
        /// <param name="rtnCode"></param>
        /// <param name="rtnMsg"></param>
        /// <returns></returns>
        public static bool BindPayStatus(string payCode, string orderId, string orderName, int payAmount, out int rtnCode, out string rtnMsg)
        {
            rtnCode = -1;
            rtnMsg = string.Empty;
            int timestamp = GetCurrentTimestamp();
            string signature = EncytSignature(orderId, orderName, payAmount,
                config.TaishinThirdPartyPayClientId, payCode, config.TaishinThirdPartyPayClientPassword, timestamp);

            string apiUrl = Helper.CombineUrl(config.TaishinThirdPartyPayApiUrl, "/api/BindPayStatus");
            var request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = WebRequestMethods.Http.Post;
            var jsonText = new JsonSerializer().Serialize(
                new
                {
                    order_id = orderId,
                    order_name = orderName,
                    amount = payAmount,
                    client_id = config.TaishinThirdPartyPayClientId,
                    code = payCode,
                    timestamp,
                    signature
                });
            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;
            string responseResult = "";

            try
            {
                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                    requestStream.Flush();
                }
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            string result = reader.ReadToEnd();
                            dynamic json = ProviderFactory.Instance().GetSerializer().DeserializeDynamic(result);
                            if (json.rtnCode == null)
                            {
                                responseResult = result;
                            }

                            rtnCode = (int)json.rtnCode;
                            rtnMsg = (string)json.rtnMsg;

                            if ((int)json.rtnCode == _RTN_CODE_OK)
                            {
                                return true;
                            }
                            else 
                            {
                                var apiLog = new ThirdPartyPayApiLog()
                                {
                                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                                    ApiUrl = apiUrl,
                                    ReturnCode = (int)json.rtnCode,
                                    ReturnMsg = (string)json.rtnMsg,
                                    Memo = Dns.GetHostName(),
                                    SourceIp = Helper.GetClientIP()
                                };
                                op.ThirdPartyPayApiLogSet(apiLog);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var apiLog = new ThirdPartyPayApiLog()
                {
                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                    ApiUrl = apiUrl,
                    Memo = Dns.GetHostName() + "Exception Error!",
                    SourceIp = Helper.GetClientIP()
                };
                op.ThirdPartyPayApiLogSet(apiLog);
                logger.Error(ex + ", JosonTest=" + jsonText + ", ResponseResult: " + responseResult);
            }
            return false;
        }
       
        /// <summary>
        /// 退貨，跟台新退款
        /// 
        /// 一筆訂單只能退一次
        /// amount 是退款金額，需小於當初支付的金額
        /// </summary>
        public static bool Refund(string token, Guid orderGuid, int amount)
        {
            string clientId = config.TaishinThirdPartyPayClientId;
            string clientPw = config.TaishinThirdPartyPayClientPassword;
            int timestamp = GetCurrentTimestamp();
            string signature = EncytSignature(token, clientId, orderGuid, amount, clientPw, timestamp);
            string apiUrl = Helper.CombineUrl(config.TaishinThirdPartyPayApiUrl, "api/BindRefund");
            var request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = WebRequestMethods.Http.Post;
            var jsonText = new JsonSerializer().Serialize(
                new
                {
                    access_token = token,
                    client_id = clientId,
                    order_id = orderGuid,
                    amt = amount,
                    timestamp,
                    signature
                });

            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;

            try
            {
                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                    requestStream.Flush();
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            return false;
                        }

                        using (var reader = new StreamReader(stream))
                        {
                            var result = reader.ReadToEnd();
                            dynamic json = ProviderFactory.Instance().GetSerializer().DeserializeDynamic(result);
                            if ((int)json.rtnCode == _RTN_CODE_OK)
                            {
                                return true;
                            }
                            else 
                            {
                                var apiLog = new ThirdPartyPayApiLog()
                                {
                                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                                    ApiUrl = apiUrl,
                                    ReturnCode = (int)json.rtnCode,
                                    ReturnMsg = (string)json.rtnMsg,
                                    Memo = Dns.GetHostName(),
                                    SourceIp = Helper.GetClientIP()
                                };
                                op.ThirdPartyPayApiLogSet(apiLog);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var apiLog = new ThirdPartyPayApiLog()
                {
                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                    ApiUrl = apiUrl,
                    Memo = Dns.GetHostName() + "Exception Error!",
                    SourceIp = Helper.GetClientIP()
                };
                op.ThirdPartyPayApiLogSet(apiLog);
                logger.Error(ex.ToString());
            }

            return false;
        }

        public static bool Refund(ThirdPartyPayTransLog tpptLog, ThirdPartyPayRefundLog tppRefundLog, string createId)
        {
            MemberToken mem = mp.MemberTokenGet(tpptLog.UserId, ThirdPartyPayment.TaishinPay);
            return false;
        }
    }

    public class TaishinPayBindResult
    {
        public string AccessToken { get; set; }
        public int RtnCode { get; set; }
        public string RtnMsg { get; set; }
    }

    public class TaishinMemberInfo
    {
        public string AccountName { get; set; }
        public int AccountAmount { get; set; }
    }

    public class TaishinThirdPartyPayOrder
    {
        public string Code { get; set; }
        public string OrderId { get; set; }
        public string OrderName { get; set; }
        public decimal Amount { get; set; }
    }

}
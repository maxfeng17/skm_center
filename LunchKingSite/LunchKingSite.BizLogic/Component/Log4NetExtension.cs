﻿using System;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public static class Log4netExtension
    {
        private static ISystemProvider sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        //TODO 可以用ActiveMQ 來做
        public static ILog Hit(this ILog logger, string subName = null)
        {
            try
            {
                HitLog log = new HitLog();
                log.Name = logger.Logger.Name;
                log.SubName = subName;
                log.CreateTime = DateTime.Now;
                sp.HitLogSet(log);
            }
            catch (Exception ex)
            {
                logger.Error("logger hit fail.", ex);
            }
            return logger;
        }
    }
}

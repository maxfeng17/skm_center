﻿using LunchKingSite.DataOrm;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.MGM;
using System;
using System.Linq;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using System.Net.Mail;
using System.Text;
using log4net;
using LunchKingSite.Core.Models.OrderEntities;
using SubSonic;
using System.Data;
using System.Web;
using System.Net;
using LunchKingSite.BizLogic.Component.MemberActions;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// 偵測如果是盜刷者，會做資料替換的處理
    /// </summary>
    public class AntiFraud
    {
        private int uid;
        private Member mem;
        private CashTrustLog ctl;
        IOrderProvider op = ProviderFactory.Instance().GetDefaultProvider<IOrderProvider>();
        IOrderEntityProvider oep = ProviderFactory.Instance().GetDefaultProvider<IOrderEntityProvider>();
        IMemberProvider mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();
        IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        static ILog logger = LogManager.GetLogger(typeof(AntiFraud));
        static HashSet<string> WhiteUsers = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        static AntiFraud()
        {
            WhiteUsers.Add("tmall@17life.com");
        }

        public AntiFraud(int userId)
        {
            mem = MemberFacade.GetMember(userId);
        }
        public AntiFraud(Guid trustId)
        {
            ctl = mp.CashTrustLogGet(trustId);
            mem = MemberFacade.GetMember(ctl.UserId);
        }
        /// <summary>
        /// 回傳空的可送禮物列表
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public List<SendGiftData> GetGiftOrderListByUser(List<SendGiftData> data)
        {
            if (mem.IsFraudSuspect)
            {
                return new List<SendGiftData>();
            }
            return data;
        }
        /// <summary>
        /// 回傳空的購物金交易資料
        /// </summary>
        /// <param name="records_1"></param>
        /// <returns></returns>
        public List<UserScashTransactionInfo> GetUserScashTranLogs(List<UserScashTransactionInfo> records_1)
        {
            if (mem.IsFraudSuspect)
            {
                records_1.Clear();
            }
            return records_1;
        }

        public static int GetVerifyATimeSpanRistScore(TimeSpan couponTimeSpan, DateTime now)
        {
            int veryfyRiskScore = 0;
            if (DateTime.Now.Hour >= 0 && DateTime.Now.Hour <= 6)
            {
                if (couponTimeSpan.TotalMinutes <= 20)
                {
                    veryfyRiskScore = 3;
                }
                else if (couponTimeSpan.TotalMinutes <= 60)
                {
                    veryfyRiskScore = 2;
                }
                else if (couponTimeSpan.TotalMinutes <= 120)
                {
                    veryfyRiskScore = 1;
                }
            }
            else
            {
                if (couponTimeSpan.TotalMinutes <= 20)
                {
                    veryfyRiskScore = 2;
                }
                else if (couponTimeSpan.TotalMinutes <= 60)
                {
                    veryfyRiskScore = 1;
                }
            }
            return veryfyRiskScore;
        }
        /// <summary>
        /// 短時間內重複購買的風險評估
        /// 取得的值，要再參考會員本身的風險值
        /// 譬如老會員就不需在意
        /// </summary>
        /// <param name="dealId"></param>
        /// <param name="failCardScore"></param>
        /// <param name="orderCount"></param>
        /// <returns></returns>
        public int GetBuyTooManyCouponRisk(Guid dealId, int failCardScore, 
            out List<KeyValuePair<string, int>> orderRecords, out int qty, out int turnover)
        {
            qty = 0;
            turnover = 0;
            orderRecords = new List<KeyValuePair<string, int>>();
            try
            {
                return InnerGetBuyTooManyCouponRisk(dealId, failCardScore, out orderRecords, out qty, out turnover);
            }
            catch (Exception ex)
            {
                logger.Warn("GetFoodDealRisk error", ex);
                return 0;
            }
        }
        private int InnerGetBuyTooManyCouponRisk(Guid dealId, int failCardScore, 
            out List<KeyValuePair<string, int>> orderRecords, out int qty, out int turnover)
        {
            qty = 0;
            turnover = 0;
            orderRecords = new List<KeyValuePair<string, int>>();
            if (mem == null || mem.IsLoaded == false)
            {
                return 0;
            }
            int userId = mem.UniqueId;
            if (userId == 0)
            {
                return 0;
            }

            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealId, true);
            if (theDeal.DeliveryType == (int)DeliveryType.ToHouse)
            {
                return 0;
            }
            Guid mainDealId = theDeal.MainBid ?? theDeal.BusinessHourGuid;
            IViewPponDeal mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(mainDealId);

            List<ViewOrderEntity> mainOrders = oep.GetUserSuccessfulOrders(userId, DateTime.Now.AddDays(-1), DateTime.Now);
            turnover = mainOrders.Sum(t => t.Turnover);
            //這邊指24小時內成功的訂單, +1要包括此次購買
            //orderCount = op.SuccessOrderGetCountByUserWithin24hr(userId) + 1;
            int orderCount = 0;
            int mainOrderCount = 0;
            mainOrderCount = mainOrders.Count(t => t.MainDealId == mainDeal.BusinessHourGuid);
            orderCount = mainOrders.Count(t => t.DealId == theDeal.BusinessHourGuid);
            mainOrderCount = mainOrderCount + 1;
            orderCount = orderCount + 1;

            if (mainOrderCount == 1 && failCardScore == 0)
            {
                return 0;
            }

            int riskScore = 0;
            int freakState = 0;
            if (mainDeal.ComboDelas != null && mainDeal.ComboDelas.Count > 0)
            {
                foreach (var comboDeal in mainDeal.ComboDelas)
                {
                    int comboOrderCount = mainOrders.Count(t => t.DealId == comboDeal.BusinessHourGuid);
                    if (comboDeal.BusinessHourGuid == theDeal.BusinessHourGuid)
                    {
                        comboOrderCount = comboOrderCount + 1;
                    }
                    if (comboOrderCount == 0)
                    {
                        continue;
                    }
                    if (qty > (comboDeal.QuantityMultiplier ?? 1))
                    {
                        freakState = 1;
                    }
                    qty += comboOrderCount * (comboDeal.QuantityMultiplier ?? 1);
                    orderRecords.Add(new KeyValuePair<string, int>(comboDeal.Title, comboOrderCount));
                }
            }
            else
            {
                qty += orderCount * (theDeal.QuantityMultiplier ?? 1);
                orderRecords.Add(new KeyValuePair<string, int>(theDeal.ItemName, orderCount));
            }
            int riskScore1 = 0;
            int riskScore2 = 0;
            int riskScore3 = 0;
            if (freakState == 0)
            {
                riskScore1 = mainOrderCount - 4;
                riskScore2 = orderCount - 3;
                riskScore3 = (qty / 5) - 1;
            }
            else
            {
                riskScore1 = mainOrderCount - 2;
                riskScore2 = orderCount - 1;
                riskScore3 = (qty / 5);
            }
            List<int> riskScores = new List<int>();
            riskScores.Add(riskScore1);
            riskScores.Add(riskScore2);
            riskScores.Add(riskScore3);
            riskScore = riskScores.Max(t => t);

            if (riskScore > 0)
            {
                if (failCardScore == 0)
                {
                    riskScore = riskScore - 1;
                }
                if (failCardScore >= 2)
                {
                    riskScore = riskScore + 1;
                }
            }
            if (riskScore < 0 )
            {
                riskScore = 0;
            }
            return riskScore;
        }

        public void VerifyCouponCheck(ref bool isCouponAvailable, ref string errMsg, ref string trustId)
        {
            if (config.AutoBlockCreditcardFraudSuspect == false)
            {
                return;
            }
            if (ctl == null || mem == null)
            {
                return;
            }
            if (WhiteUsers.Contains(mem.UserName))
            {
                return;
            }
            var memProp = mp.MemberPropertyGetByUserId(mem.UniqueId);
            TimeSpan couponTimeSpan = DateTime.Now - ctl.CreateTime;
            int veryfyRiskScore = 0;
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(ctl.BusinessHourGuid.GetValueOrDefault());
            if (deal.IsLoaded && deal.CategoryIds.Contains(90))
            {
                veryfyRiskScore = GetVerifyATimeSpanRistScore(couponTimeSpan, DateTime.Now);
                List<KeyValuePair<string, int>> orderRecords;
                int qty;
                int turnover;
                GetBuyTooManyCouponRisk(deal.BusinessHourGuid, 0, out orderRecords, out qty, out turnover);
                if (veryfyRiskScore > 0 && qty <= 3 && turnover < 2000)
                {
                    veryfyRiskScore = 0;
                }
            }

            int riskScore = memProp.RiskScore + veryfyRiskScore;

            if (riskScore >= 6 && mem.IsFraudSuspect == false)
            {
                //mem.IsFraudSuspect 會變更為 true ，當 mem.Comment == "盜刷嫌疑人
                //設為嫌疑人，目前不設計自動解除的機制。
                mem.Comment = "盜刷嫌疑人";
                mem.IsApproved = false;
                MemberFacade.SaveMember(mem);
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("標註 {0} 盜刷嫌疑人，此帳號已封鎖。\n", mem.DisplayName);
                sb.AppendLine(Helper.CombineUrl(config.SiteUrl, "controlroom/User/users_edit.aspx?username=" + Uri.EscapeDataString(mem.UserName)));
                if (string.IsNullOrEmpty(config.CreditcardFailLineToken) == false)
                {
                    LineAppender.HttpSend(config.CreditcardFailLineToken, string.Empty, sb.ToString()).Start();
                }
                MailMessage mm = new MailMessage();
                mm.IsBodyHtml = true;
                mm.Subject = "標註 " + mem.DisplayName + " 盜刷嫌疑人，此帳號已封鎖。";
                mm.From = new MailAddress("unicorn@17life.com", "防盜小幫手");
                mm.To.Add(new MailAddress("tingwei_chu@17life.com", "曲庭蔚"));
                mm.To.Add(new MailAddress("eva_liu@17life.com", "劉湘琳"));
                mm.To.Add(new MailAddress("mark_yeh@17life.com", "葉德昇"));
                mm.To.Add("unicorn@17life.com");
                mm.Body = Helper.CombineUrl(config.SiteUrl, "controlroom/User/users_edit.aspx?username=" + Uri.EscapeDataString(mem.UserName));
                PostMan.Instance().Send(mm, SendPriorityType.Async);
            }
            if (mem.IsFraudSuspect)
            {
                op.SetUserUnlockCreditcardToLock(mem.UniqueId);
            }
            if (mem.IsFraudSuspect || mem.IsApproved == false)
            {
                isCouponAvailable = false;
                trustId = string.Empty;
                errMsg = "此憑證交易有疑慮，\n無法核銷";
            }
        }
        public static bool IsRiskHighIp(string sourceIp, out List<KeyValuePair<string, bool>> userRecords)
        {
            userRecords = new List<KeyValuePair<string, bool>>();
            try
            {
                return InnerIsRiskHighIp(sourceIp, out userRecords);
            }
            catch (Exception ex)
            {
                logger.Warn("IsRiskHighIp error.", ex);
                return false;
            }
        }
        private static bool InnerIsRiskHighIp(string sourceIp, out List<KeyValuePair<string, bool>> userRecords)
        {
            userRecords = new List<KeyValuePair<string, bool>>();
            string sql = @"
select user_name, is_approved, isnull(mp.risk_score, 0) from member mem with(nolock) 
left join member_property mp with(nolock) on mp.user_id = mem.unique_id
where mem.unique_id in (
select distinct mem.unique_id from member mem with(nolock) 
inner join account_audit aa with(nolock) on aa.user_id = mem.unique_id
where 
	DATEDIFF(day, mem.create_time, getdate()) < 2 and
aa.source_ip = @sourceIp

)";
            QueryCommand qc = new QueryCommand(sql, Member.Schema.Provider.Name);
            qc.Parameters.Add("@sourceIp", sourceIp, System.Data.DbType.AnsiString);
            bool result = false;
            try
            {
                using (IDataReader dr = DataService.GetReader(qc))
                {
                    while (dr.Read())
                    {
                        string userName = dr.GetString(0);
                        bool isApproved = dr.GetBoolean(1);
                        int riskScore = dr.GetInt32(2);
                        bool isRiskHight = isApproved == false || riskScore >= 6;
                        userRecords.Add(new KeyValuePair<string, bool>(userName,isRiskHight));
                        if (isRiskHight)
                        {
                            result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Warn("IsRiskHighIp error", ex);
            }
            return result;
        }


 /*
            +1 購買超過2000或旅遊檔
            +1 購買超過2000且旅遊檔
            +1 註冊不到一天
                +1 凌晨3點到7點購買
            +2 臉書串接註冊但臉書無圖
            +1 特定注意的email domain "awsoo.com","nbzmr.com","nwytg.net"
            +1 填手機但全部只有3個數字
            +  24小時內刷失敗卡號數
            +  24小時內刷遺失卡或標註黑名單卡號數

            -5 註冊超過180天 -3 註冊超過30天 -1 註冊超過7天 
            -1 手機認證
            -1 如果有填過收件地址
            -1 購買<800
            -5 購買宅配
            -5 17員工
        */
        /// <summary>
        /// 計算會員盜刷的可能性
        /// 如超過限制，會註記會員為盜刷嫌疑人
        /// </summary>
        /// <param name="authObject"></param>
        /// <param name="authResult"></param>
        /// <param name="dealId"></param>
        /// <returns></returns>
        private bool InnerAutoBlockCreditcardFraudSuspect(CreditCardAuthObject authObject, CreditCardAuthResult authResult, Guid dealId, 
            out int hazard, out int userId)
        {

            bool authSuccess;
            try
            {
                authSuccess = int.Parse(authResult.ReturnCode) == 0;
            }
            catch
            {
                authSuccess = false;
            }

            userId = 0;
            hazard = 0;
            Member mem = null;
            StringBuilder sbLog = new StringBuilder();
            if (HttpContext.Current != null && HttpContext.Current != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                mem = MemberFacade.GetMember(HttpContext.Current.User.Identity.Name);
                if (mem.IsLoaded == false)
                {
                    return false;
                }
                userId = mem.UniqueId;

                if (WhiteUsers.Contains(mem.UserName))
                {
                    hazard = 0;
                    return false;
                }

                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealId, true);
                //90 是旅遊
                int amount = authObject.Amount;
                if (amount >= 2000 || deal.CategoryIds.Contains(90))
                {
                    hazard++;
                    sbLog.AppendFormat("+1={0} amout>2000 或旅遊\n", hazard);
                }
                if (amount >= 2000 && deal.CategoryIds.Contains(90))
                {
                    hazard++;
                    sbLog.AppendFormat("+1={0} amout>2000 且旅遊\n",  hazard);
                }
                int registedDays = (int)(DateTime.Now - mem.CreateTime).TotalDays;
                if (registedDays <= 3)
                {
                    hazard++;
                    sbLog.AppendFormat("+1={0} 新會員\n", hazard);
                    if (DateTime.Now.Hour >= 2 && DateTime.Now.Hour <= 8)
                    {
                        hazard++;
                        sbLog.AppendFormat("+1={0} 購買時段2-8時\n", hazard);
                    }
                    string userAgnet = HttpContext.Current.Request.UserAgent;
                    if (userAgnet.EndsWith("lat/25.0339031;lon/121.5645098;"))
                    {
                        hazard = hazard + 2;
                        sbLog.AppendFormat("+2={0} 地理位置\n", hazard);
                    }
                    else if (userAgnet.EndsWith("lat/25.0339031;lon/121.5645098;"))
                    {
                        hazard = hazard + 2;
                        sbLog.AppendFormat("+2={0} 地理位置\n", hazard);
                    }
                    else if (userAgnet.EndsWith("lat/25.0211462;lon/121.5243388;"))
                    {
                        hazard = hazard + 2;
                        sbLog.AppendFormat("+2={0} 地理位置\n", hazard);
                    }
                    string userIp = Helper.GetClientIP(true);
                    List<KeyValuePair<string,bool>> userRecords;
                    //if (AntiFraud.IsRiskHighIp(userIp, out userRecords))
                    //{
                    //    int ipRisk = userRecords.Count(t => t.Value);
                    //    if (ipRisk > 2)
                    //    {
                    //        ipRisk = 2;
                    //    }
                    //    hazard = hazard + ipRisk;
                    //}
                }

                MemberLinkCollection mls = mp.MemberLinkGetList(mem.UniqueId);
                string userEmail = mem.UserName;
                if (RegExRules.CheckEmail(mem.UserName) == false)
                {
                    userEmail = mem.UserEmail;
                }
                if (RegExRules.CheckEmail(userEmail))
                {
                    string emailDomain = Helper.GetEmailDomain(userEmail).ToLower();
                    if (MemberUtilityCore.DAEWhiteList.Contains(emailDomain) == false)
                    {
                        if (emailDomain == "163.com")
                        {
                            hazard = hazard + 1;
                        }
                        else if (emailDomain == "awsoo.com" || emailDomain == "nbzmr.com" 
                            || emailDomain == "nwytg.net" || emailDomain == "dayrep.com" 
                            || emailDomain == "jourrapide.com" || emailDomain == "armyspy.com")
                        {
                            hazard = hazard + 4;
                            sbLog.AppendFormat("+4={0} 有疑問的email網域\n", hazard);
                        }
                        else
                        {
                            try
                            {
                                var hostAddresses = Dns.GetHostAddresses(emailDomain);
                                if (hostAddresses.Length == 0 || hostAddresses.FirstOrDefault(t => t.MapToIPv4().ToString().Contains("159.25.16")) != null)
                                {
                                    hazard = hazard + 3;
                                    sbLog.AppendFormat("+3={0} 有疑問的email網域\n", hazard);
                                }
                            }
                            catch (Exception ex)
                            {
                                logger.Warn("Dns.GetHostAddresses error " + emailDomain, ex);
                                hazard = hazard + 2;
                                sbLog.AppendFormat("+2={0} 有疑問的email網域\n", hazard);
                            }
                            //if (mls.Count == 1 && mls[0].ExternalOrg == (int)SingleSignOnSource.ContactDigitalIntegration)
                            //{
                            //    hazard = hazard + 2;
                            //}
                        }
                    }
                }
                if (string.IsNullOrEmpty(mem.Mobile) == false && mem.Mobile.Length >= 6)
                {
                    string tailNumber = mem.Mobile.Substring(mem.Mobile.Length - 6);
                    int distinctCharCount = tailNumber.Select(t => t).Distinct().Count();
                    if (distinctCharCount == 1)
                    {
                        hazard = hazard + 3;
                        sbLog.AppendFormat("+3 = {0} 手機亂打\n", hazard);
                    }
                    else if (distinctCharCount == 2)
                    {
                        hazard = hazard + 2;
                        sbLog.AppendFormat("+2 = {0} 手機亂打\n",  hazard);
                    }
                }
                if (mls.Count == 1 && mls[0].ExternalOrg == (int)SingleSignOnSource.Facebook && registedDays < 5)
                {
                    try
                    {
                        if (Convert.ToDecimal(mls[0].ExternalUserId) > 10217251098255816 && string.IsNullOrEmpty(mem.Pic) == false)
                        {
                            int picSize = new WebClient().DownloadData(mem.Pic).Length;
                            if (picSize == 2550 || picSize == 2638)
                            {
                                hazard = hazard + 2;
                                sbLog.AppendFormat("+2 = {0} 臉書串接無圖\n",  hazard);
                            }
                        }
                    }
                    catch { }
                }

                if (registedDays > 180)
                {
                    hazard = hazard - 5;
                    sbLog.AppendFormat("-5 = {0} 註冊時間180天\n", hazard);
                }
                else if (registedDays >= 30)
                {
                    hazard = hazard - 3;
                    sbLog.AppendFormat("-3 = {0} 註冊時間30天\n",  hazard);
                }
                else if (registedDays >= 7)
                {
                    hazard--;
                    sbLog.AppendFormat("-1 = {0} 註冊時間7天\n", hazard);
                }
                if (mls.FirstOrDefault(t => t.ExternalOrg == (int)SingleSignOnSource.Mobile17Life) != null)
                {
                    hazard--;
                    sbLog.AppendFormat("-1 = {0} 手機註冊\n", hazard);
                }
                if (mp.MemberDeliveryGetCollection(mem.UniqueId).Count > 0)
                {
                    hazard--;
                    sbLog.AppendFormat("-1 = {0} 有收件資料\n", hazard);
                }
                if (amount < 500)
                {
                    hazard--;
                    sbLog.AppendFormat("-1 = {0} 購買金額<500\n",hazard);
                }
                if (deal.DeliveryType == (int)DeliveryType.ToHouse && amount < 800)
                {
                    hazard = hazard - 3;
                    sbLog.AppendFormat("-3 = {0} 宅配 & 購買金額<800\n", hazard);
                }
                //算失敗卡
                int failCardCount = op.GetUserCreditcardFailCardCount(mem.UniqueId, DateTime.Now.AddDays(-1), DateTime.Now);
                hazard = hazard + failCardCount;
                if (failCardCount > 0)
                {
                    sbLog.AppendFormat("+{0} = {1} 失敗卡\n",  failCardCount, hazard);
                }
                //遺失卡再加1
                int stolenCardCount = op.GetUserCreditcardStolenCardCount(mem.UniqueId, DateTime.Now.AddDays(-1), DateTime.Now);
                hazard = hazard + stolenCardCount;
                if (stolenCardCount > 0)
                {
                    sbLog.AppendFormat("+{0} = {1} 遺失卡\n", stolenCardCount, hazard);
                }

                if (hazard >= 6 && mem.IsFraudSuspect == false)
                {
                    //mem.IsFraudSuspect 會變更為 true ，當 mem.Comment == "盜刷嫌疑人
                    //設為嫌疑人，目前不設計自動解除的機制。
                    mem.Comment = "盜刷嫌疑人";
                    MemberFacade.SaveMember(mem);
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat("標註 {0} 盜刷嫌疑人，並對其施展幻術。\n", mem.DisplayName);
                    sb.AppendLine(Helper.CombineUrl(config.SiteUrl, "controlroom/User/users_edit.aspx?username=" + Uri.EscapeDataString(mem.UserName)));
                    if (string.IsNullOrEmpty(config.CreditcardFailLineToken) == false)
                    {
                        LineAppender.HttpSend(config.CreditcardFailLineToken, string.Empty, sb.ToString()).Start();
                    }
                    MailMessage mm = new MailMessage();
                    mm.IsBodyHtml = true;
                    mm.Subject = "標註 " + mem.DisplayName + " 盜刷嫌疑人，並對其施展幻術。";
                    mm.From = new MailAddress("unicorn@17life.com", "防盜小幫手");
                    mm.To.Add(new MailAddress("tingwei_chu@17life.com", "曲庭蔚"));
                    mm.To.Add(new MailAddress("eva_liu@17life.com", "劉湘琳"));
                    mm.To.Add(new MailAddress("mark_yeh@17life.com", "葉德昇"));
                    mm.To.Add("unicorn@17life.com");
                    mm.Body = Helper.CombineUrl(config.SiteUrl, "controlroom/User/users_edit.aspx?username=" + Uri.EscapeDataString(mem.UserName));
                    PostMan.Instance().Send(mm, SendPriorityType.Async);
                }
                if (sbLog.Length > 0) {
                    logger.InfoFormat("風險評估:{0}\n{1}", mem.UniqueId, sbLog);
                }
                if (mem.IsFraudSuspect)
                {
                    op.SetUserUnlockCreditcardToLock(mem.UniqueId);
                }
                MemberFacade.SetMemberRiskScore(mem.UniqueId, hazard);

                if (authSuccess && hazard > 0)
                {
                    int qty;
                    int turnover;
                    List<KeyValuePair<string, int>> orderRecords;
                    int foodDealRiskScore = new AntiFraud(userId).GetBuyTooManyCouponRisk(
                        dealId, failCardCount + stolenCardCount, out orderRecords, out qty, out turnover);
                    if (foodDealRiskScore > 0)
                    {
                        string subject = string.Format("偵測大量購買: {0}", mem.DisplayName);
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine(subject);
                        foreach (var pair in orderRecords)
                        {
                            sb.AppendFormat("{0} * {1}\n", pair.Value, pair.Key);
                        }
                        if (orderRecords.Count > 1)
                        {
                            sb.AppendFormat("共 {0} 單\n", orderRecords.Sum(t=>t.Value));
                        }
                        sb.AppendFormat("風險分數: {0}\n", foodDealRiskScore);
                        sb.AppendLine(Helper.CombineUrl(config.SiteUrl,
                            "controlroom/User/users_edit.aspx?username=" + Uri.EscapeDataString(mem.UserName)));
                        if (string.IsNullOrEmpty(config.CreditcardFailLineToken) == false)
                        {
                            LineAppender.HttpSend(config.CreditcardFailLineToken, string.Empty, sb.ToString()).Start();
                        }
                        MailMessage mm = new MailMessage();
                        mm.IsBodyHtml = false;
                        mm.Subject = subject;
                        mm.Body = sb.ToString();
                        mm.From = new MailAddress("unicorn@17life.com", "防盜小幫手");
                        //mm.To.Add(new MailAddress("tingwei_chu@17life.com", "曲庭蔚"));
                        //mm.To.Add(new MailAddress("eva_liu@17life.com", "劉湘琳"));
                        //mm.To.Add(new MailAddress("mark_yeh@17life.com", "葉德昇"));
                        mm.To.Add("unicorn@17life.com");
                        mm.Body = Helper.CombineUrl(config.SiteUrl, "controlroom/User/users_edit.aspx?username=" + Uri.EscapeDataString(mem.UserName));
                        PostMan.Instance().Send(mm, SendPriorityType.Async);
                    }
                }

                return mem.IsFraudSuspect;
            }
            return false;
        }

        public bool AutoBlockCreditcardFraudSuspect(CreditCardAuthObject authObject, CreditCardAuthResult authResult, Guid dealId, 
            out int hazard)
        {
            hazard = 0;
            if (config.AutoBlockCreditcardFraudSuspect == false)
            {
                return false;
            }
            try
            {
                DateTime now = DateTime.Now;
                int userId;
                bool isFraudSuspect = InnerAutoBlockCreditcardFraudSuspect(authObject, authResult, dealId, out hazard, out userId);
                logger.InfoFormat("會員:{0}, 風險:{1}, 費時{2}秒", userId, hazard, (DateTime.Now - now).TotalSeconds.ToString("0.00"));
                return isFraudSuspect;
            }
            catch (Exception ex)
            {
                logger.Warn("AutoBlockCreditcardFraudSuspect: " + ex);
                return false;
            }
        }

        public void PushToLineBot(CreditCardAuthObject authObject, CreditCardAuthResult authResult, Guid dealId)
        {
            if (string.IsNullOrEmpty(config.CreditcardFailLineToken))
            {
                return;
            }
            int rlt = 0;
            try
            {
                rlt = int.Parse(authResult.ReturnCode);
            }
            catch
            {
                rlt = -9527;
            }
            //成功的卡不記
            if (rlt == 0)
            {
                return;
            }

            string msg = string.Empty;
            try
            {
                StringBuilder sb = new StringBuilder();
                Member mem = null;
                if (HttpContext.Current != null && HttpContext.Current != null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    mem = MemberFacade.GetMember(HttpContext.Current.User.Identity.Name);
                    MobileMember mm = MemberFacade.GetMobileMember(mem.UniqueId);
                    if (mm.IsLoaded)
                    {
                        sb.AppendFormat("{0} {1} 註冊於 {2}\n",
                            mem.DisplayName, mm.MobileNumber, Helper.GetSmartTimeSpanString(DateTime.Now - mem.CreateTime));
                    }
                    else if (mem.IsLoaded)
                    {
                        sb.AppendFormat("{0} {1} 註冊於 {2}\n",
                            mem.DisplayName, mem.UserName, Helper.GetSmartTimeSpanString(DateTime.Now - mem.CreateTime));
                    }
                    string ip = Helper.GetClientIP(true);
                    string deviceType = Helper.GetOrderFromType().ToString();
                    if (string.IsNullOrEmpty(ip) == false)
                    {
                        sb.AppendFormat("來自: {0} {1}\n", ip, deviceType);
                    }
                    List<KeyValuePair<string,bool>> userRecords;
                    if (AntiFraud.IsRiskHighIp(ip, out userRecords))
                    {
                        sb.AppendLine("同IP有其它高危險會員");
                        foreach (var pair in userRecords)
                        {
                            if (pair.Value)
                            {
                                sb.AppendLine(Helper.CombineUrl(config.SiteUrl,
                                    "controlroom/User/users_edit.aspx?username=" + Uri.EscapeDataString(pair.Key)));
                            }
                        }
                    }
                }
                IViewPponDeal deal = null;
                if (dealId != default(Guid))
                {
                    deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealId, true);
                    if (deal.MainBid != null && deal.MainBid != deal.BusinessHourGuid)
                    {
                        deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(deal.MainBid.Value, true);
                    }
                    sb.AppendLine("檔次: " + deal.ItemName);
                }
                if (authObject != null)
                {
                    sb.AppendLine("金額: " + authObject.Amount.ToString());
                }
                else
                {
                    sb.AppendLine("金額: authObject is null");
                }

                if (authResult == null)
                {
                    sb.AppendLine("失敗: authResult is null");
                }
                else
                {
                    sb.AppendLine("卡號: " + authResult.CardNumber);
                    sb.AppendFormat("失敗: ({0}){1}:\n", authResult.ReturnCode, authResult.TransMessage);
                }
                int failCardCount = op.GetUserCreditcardFailCardCount(mem.UniqueId, DateTime.Now.AddDays(-1), DateTime.Now);
                sb.AppendFormat("累計失敗卡數: {0}\n", failCardCount);
                //if (deal != null)
                //{
                //    sb.AppendLine(Helper.CombineUrl(config.SiteUrl, "deal" , deal.BusinessHourGuid.ToString()));
                //}

                if (mem != null && mem.IsLoaded)
                {
                    sb.AppendLine(Helper.CombineUrl(config.SiteUrl, "controlroom/User/users_edit.aspx?username=" + Uri.EscapeDataString(mem.UserName)));
                }
                msg = sb.ToString();
            }
            catch (Exception ex)
            {
                msg = "刷卡失敗: " + ex.ToString() + "- " + Environment.MachineName;
            }

            LineAppender.HttpSend(config.CreditcardFailLineToken, string.Empty, msg).Start();
        }

    }

 
}
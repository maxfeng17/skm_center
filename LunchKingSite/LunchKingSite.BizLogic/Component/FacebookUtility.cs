﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Component
{
    public class FacebookUtility
    {
        public const string _FACEBOOK_API_VER_2_10 = "v2.12/";

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        static ILog logger = LogManager.GetLogger(typeof(FacebookUtility));
        /// <summary>
        /// 自動刷新FB分享網址預覽圖片
        /// </summary>
        /// <param name="contentUrl">網址</param>
        public static void CleanFacebookPictureCache(string contentUrl, string accessToken)
        {
            if (string.IsNullOrEmpty(accessToken))
            {
                accessToken = GetFacebookE7LifeAccessToken();
            }

            string fbUrl = "https://graph.facebook.com/";
            string param = string.Format("id={0}&scrape=true&access_token={1}&appsecret_proof={2}",
                    contentUrl, accessToken, GenerateAppSecretProof(accessToken));
            byte[] bs = Encoding.ASCII.GetBytes(param);

            HttpWebRequest req = WebRequest.Create(fbUrl) as HttpWebRequest;
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = bs.Length;
            using (Stream reqStream = req.GetRequestStream())
            {
                reqStream.Write(bs, 0, bs.Length);
            }
            try
            {
                req.GetResponse();
            }
            catch
            {
            }
        }

        /// <summary>
        /// 取得 17Life在Facebook上的App accesstoken，進行伺服器間的操作
        /// </summary>
        /// <returns></returns>
        public static string GetFacebookE7LifeAccessToken()
        {
            string url = string.Format(
                "https://graph.facebook.com/oauth/access_token?client_id={0}&client_secret={1}&&grant_type=client_credentials"
                , config.FacebookApplicationId, config.FacebookApplicationSecret);

            return GetOAuhToken(url);
        }

        public static string GetOAuhToken(string url)
        {
            Dictionary<string, string> tokens = new Dictionary<string, string>();
            HttpWebRequest request = FacebookUser.CreateRequest(url);

            //request.UserAgent = "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5";
            string retVal = null;
            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response == null)
                    {
                        throw new Exception("response is null");
                    }
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        retVal = reader.ReadToEnd();
                    }
                    if (string.IsNullOrEmpty(retVal))
                    {
                        throw new Exception("retVal is null or empty");
                    }

                    dynamic ret = ProviderFactory.Instance().GetSerializer().DeserializeDynamic(retVal);
                    tokens.Add("access_token", (string)ret.access_token);
                }
            }
            catch (Exception ex)
            {
                logger.InfoFormat("GetOAuhToken {0} fail. ex={1}", url, ex);
            }
            if (tokens.ContainsKey("access_token") == false)
            {
                throw new Exception(string.Format("找不到 access_token, FB出問題了, {0}", retVal));
            }
            return tokens["access_token"];
        }
        /// <summary>
        /// 參數中有 access_token 的，似乎被建議要加上appsecret_proof
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static string GenerateAppSecretProof(string accessToken)
        {
            string appSecret = config.FacebookApplicationSecret;
            byte[] keyBytes = Encoding.UTF8.GetBytes(appSecret);
            byte[] messageBytes = Encoding.UTF8.GetBytes(accessToken);
            byte[] hash;
            using (HMACSHA256 hmacsha256 = new HMACSHA256(keyBytes))
            {
                hash = hmacsha256.ComputeHash(messageBytes);
            }

            StringBuilder sbHash = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sbHash.Append(hash[i].ToString("x2"));
            }
            return sbHash.ToString();
        }

        public static string FbGameAuthUrl(string state)
        {
            string retUrl = Helper.CombineUrl(config.SiteUrl, config.GameFbAuthRedirectUri);
            return string.Format(
                "https://www.facebook.com/{0}dialog/oauth?client_id={1}&redirect_uri={2}&scope={3}&state={4}",
                FacebookUtility._FACEBOOK_API_VER_2_10,
                config.GameFacebookApplicationId, retUrl, "email,public_profile",
                state);
        }
    }
}

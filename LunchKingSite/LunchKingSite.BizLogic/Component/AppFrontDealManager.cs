﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// App API 首頁相關
    /// 執行 APP_top50_ABtesting 計劃用
    /// </summary>
    public class AppFrontDealManager
    {
        private static IPponEntityProvider _pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
        private static ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        private static ILog _logger = LogManager.GetLogger(typeof(AppFrontDealManager));

        public enum TesterGroup
        {
            /// <summary>
            ///  即原本的排序方式，從各頻道取若干，交叉顯示
            /// </summary>
            A = 0,
            /// <summary>
            /// 從過往瀏覽記錄選出推薦檔次，再加上原本排序
            /// </summary>
            B = 1
        }
        /// <summary>
        /// 將會員分群進行 APP首頁檔次的AB測試
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static TesterGroup ClassifysMemberTesterGroup (int userId)
        {
            //if (MemberFacade.IsTesterForBetaFeature(userId))
            //{
            //    return TesterGroup.B;
            //}

            if (_config.EanbleABTestRecommendDealsByViewHistory == false)
            {
                return TesterGroup.A;
            }

            //return userId % 2 == 0 ? TesterGroup.A : TesterGroup.B;
            return userId % 2 == 0 ? TesterGroup.B : TesterGroup.A;
        }

        /// <summary>
        /// 取得針對不同會員的過去瀏覽記錄，取得推薦相關檔次
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="areaId"></param>
        /// <returns></returns>
        public static List<PponDealSynopsisBasic> GetAppFronterDeals(int userId, int areaId)
        {
            List<PponDealSynopsisBasic> result = new List<PponDealSynopsisBasic>();
            if (userId != 0)
            {
                var recommendDeals = GetMemberFronterDealsByViewHistory(userId);

                foreach (IViewPponDeal vpd in recommendDeals)
                {
                    PponDealSynopsis synopsis = PponDealApiManager.PponDealSynopsisGetByDeal(vpd, null, 0, VbsDealType.Ppon, true);
                    PponDealSynopsisBasic basicDeal = PponDealApiManager.PponDealSynopsisToBasic(synopsis);
                    result.Add(basicDeal);
                }
                _logger.InfoFormat("會員 {0} 取得 {1} 推薦檔次", userId, result.Count);
                if (MemberFacade.IsTesterForBetaFeature(userId))
                {
                    string dealsLog = string.Join("\r\n", result.Select(t => t.Bid).ToList());
                    _logger.InfoFormat("會員 {0} 取得 {1}", userId, dealsLog);
                }
            }
            if (_config.IsDefaultPageSettingByCache)
            {
                result.AddRange(PponDealApiManager.GetTodayChoicePponDealSynopsesByCache(areaId));
            }
            else
            {
                result.AddRange(PponDealApiManager.GetTodayChoicePponDealSynopses(areaId));
            }
            return result;
        }

        /// <summary>
        /// 依會員自身瀏覽商品記錄，取得相關推薦檔次
        /// 如條件吻合，也包含訪問過的檔次，
        /// 提供為會員App首頁列表檔次
        /// 
        /// 目前以400為界，將超過均價400元的檔次順序延後
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private static List<IViewPponDeal> GetMemberFronterDealsByViewHistory(int userId)
        {
            List<IViewPponDeal> result = new List<IViewPponDeal>();
            List<Guid> bids = _pep.GetDealPageViewBidsTop10(userId);

            DateTime now = DateTime.Now;
            foreach(Guid bid in bids)
            {
                var recommendDeals = PponDealPreviewManager.GetApiRecommendDeals(bid, 4, _config.RecommendDealsLogicType); 
                result.AddRange(recommendDeals);
                //加上訪問過的檔次
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
                if (deal != null && deal.IsSoldOut == false && now > deal.BusinessHourOrderTimeS && now < deal.BusinessHourOrderTimeE)
                {
                    result.Add(deal);
                }
            }

            //以400塊為分界，均價超過400塊往後移動
            result = result.Distinct().OrderBy(t => t.ItemPrice / 400).Where(
                    t => t.IsSoldOut == false &&
                         t.IsHiddenInRecommendDeal == false &&
                         DateTime.Now > t.BusinessHourOrderTimeS &&
                         DateTime.Now < t.BusinessHourOrderTimeE)
                .ToList();

            return result;
        }

        /// <summary>
        /// 取得APP首頁訪問加總資訊
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        internal List<AppFrontPageView> GetFrontpageView(DateTime dateStart, DateTime dateEnd)
        {
            var frontViewCounts = _pep.GetFrontViewCountByRange(dateStart, dateEnd).ToList();
            List<AppFrontPageView> result = frontViewCounts.GroupBy(gb => new {gb.UserId, gb.Date.Date}).Select(fvc => new AppFrontPageView
            {
                UserId = fvc.Key.UserId,
                Date = fvc.Key.Date,
                DeviceAndroid = fvc.Any(f => f.DeviceAndroid),
                DeviceIos = fvc.Any(f => f.DeviceIos),
                AppFrontpageViewCount = fvc.Max(f => f.AppFrontpageViewCount),
                AppFrontpageViewTotal = fvc.Count(),
            }).ToList();

            return result;
        }

        /// <summary>
        /// 取得APP的訂單
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        internal List<FrontDealOrderInfo> GetFrontDealsOrders(DateTime dateStart, DateTime dateEnd)
        {
            List<FrontDealOrder> frontDealOrders = _pep.GetFrontDealOrderByRange(dateStart, dateEnd);
            var result = frontDealOrders.GroupBy(fdo => new { fdo.UserId, fdo.CreateTime.Date })
                .Select(y => new FrontDealOrderInfo
                {
                    UserId = y.Key.UserId,
                    Date = y.Key.Date,
                    AppOrderCount = y.Count(),
                    AppOrderTurnoverTotal = y.Sum(x => x.Amount - x.DiscountAmount),
                    AppOrderGrossProfitTotal = y.Sum(x => x.Amount - x.DiscountAmount - x.BaseCost),
                });


            return result.ToList();
        }
    }
}

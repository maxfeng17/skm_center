﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Web.Routing;

namespace LunchKingSite.BizLogic.Component
{
    public class QueryStringBuilder
    {
        private string _baseUrl;
        private Dictionary<string, string> args = new Dictionary<string, string>();

        public QueryStringBuilder()
        {
            
        }
        public QueryStringBuilder(string baseUr)
        {
            this._baseUrl = baseUr;
        }

        public void Append(string name, string val)
        {
            if (args.ContainsKey(name) == false)
            {
                args.Add(name, val);
            }            
        }

        public void Remove(string name)
        {
            if (args.ContainsKey(name))
            {
                args.Remove(name);
            }
        }

        public void AppendQuery(string q)
        {
            if (string.IsNullOrEmpty(q))
            {
                return;
            }
            q = q.Trim().TrimStart('?');
            string[] args = q.Split('&');
            foreach(string arg in args)
            {
                string[] pair = arg.Split('=');
                if (pair.Length == 2)
                {
                    Append(pair[0], pair[1]);
                }
            }
        }

        public void AppendRoute(RouteData routeData)
        {
            if (routeData.Route != null)
            {
                foreach(var pair in routeData.Values)
                {
                    Append(pair.Key, pair.Value.ToString());
                }
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, string> pair in args)
            {
                if (sb.Length > 0)
                {
                    sb.Append('&');
                }
                sb.Append(pair.Key);
                sb.Append("=");
                sb.Append(HttpUtility.UrlEncode(pair.Value));
            }
            if (string.IsNullOrEmpty(this._baseUrl) == false)
            {                
                if (this._baseUrl.Contains("?"))
                {
                    sb.Insert(0, "&");
                }
                else
                {
                    sb.Insert(0, "?");
                }
                sb.Insert(0, this._baseUrl);
            }
            return sb.ToString();
        }
    }

    public class PathAndQueryBuilder
    {
        NameValueCollection querys = new NameValueCollection();
        public string Path { get; set; }
        public PathAndQueryBuilder(string pathAndQuery)
        {
            if (string.IsNullOrWhiteSpace(pathAndQuery))
            {
                return;
            }
            int pos = pathAndQuery.IndexOf("?");
            if (pos == -1)
            {
                Path = pathAndQuery;
                return;
            }
            else
            {
                Path = pathAndQuery.Substring(0, pos);
            }
            string query = pathAndQuery.Substring(pos + 1);
                       
            string[] args = query.Split('&');
            foreach (string arg in args)
            {
                string[] pair = arg.Split('=');
                if (pair.Length == 2)
                {
                    querys.Add(pair[0], pair[1]);
                }
            }
        }

        public PathAndQueryBuilder Append(string name, string val)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return this;
            }
            querys[name] = val;
            return this;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Path);
            if (querys.Count > 0)
            {
                sb.Append("?");
            }
            for (int i = 0; i < querys.Keys.Count; i++)
            {
                string key = querys.Keys[i];
                if (i > 0)
                {
                    sb.Append("&");
                }
                sb.Append(key);
                sb.Append("=");
                sb.Append(System.Net.WebUtility.UrlEncode(querys[key]));
            }
            return sb.ToString();
        }

        public static PathAndQueryBuilder Parse(string pathAndQuery)
        {
            return new PathAndQueryBuilder(pathAndQuery);
        }
    }
}

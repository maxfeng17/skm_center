﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class SystemCodeManager
    {
        static ISystemProvider _systemProv;

        private static SystemCodeCollection _systemCodes;
        static ILog logger = LogManager.GetLogger(typeof(SystemCodeManager));
        protected static SystemCodeCollection SystemCodes
        {
            get
            {
                if (_systemCodes.Count == 0)
                {
                    _systemCodes = _systemProv.SystemCodeGetListWithEnabled();
                }
                return _systemCodes;
            }
        }

        private static Dictionary<int, string> DealLabelCollection;

        static SystemCodeManager()
        {
            _systemProv = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _systemCodes = new SystemCodeCollection();
            DealLabelCollection = new Dictionary<int, string>();
        }

        public static void ClearAllSystemCodeCollection()
        {
            _systemCodes = new SystemCodeCollection();
            DealLabelCollection = new Dictionary<int, string>();
        }

        public static void ReloadAllSystemCodeCollection()
        {
            _systemCodes = _systemProv.SystemCodeGetListWithEnabled();
            var deal_labels = SystemCodes.Where(x => x.CodeGroup == "DealLabel");
            if (deal_labels.Count() > 0)
            {
                DealLabelCollection = deal_labels.ToDictionary(x => x.CodeId, x => x.CodeName);
            }
            else
            {
                DealLabelCollection = new Dictionary<int, string>();
            }
        }

        public static string GetDealLabelSystemCodeNameById(int codeId)
        {
            if (_systemCodes.Count == 0)
            {
                var deal_labels = SystemCodes.Where(x => x.CodeGroup == "DealLabel");
                if (deal_labels.Count() > 0)
                {
                    DealLabelCollection = deal_labels.ToDictionary(x => x.CodeId, x => x.CodeName);
                }
                else
                {
                    DealLabelCollection = new Dictionary<int, string>();
                }
            }
            if (DealLabelCollection.ContainsKey(codeId))
            {
                return DealLabelCollection[codeId];
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetSystemCodeNameByGroupAndId(string group, int codeId)
        {
            var rtns = SystemCodes.Where(x => x.CodeGroup == group && x.CodeId == codeId).ToList();
            return rtns.Count > 0 ? rtns[0].CodeName : string.Empty;
        }

        public static SystemCode GetSystemCode(string group, int codeId)
        {
            return SystemCodes.FirstOrDefault(t => t.CodeGroup == group && t.CodeId == codeId);
        }        

        public static SystemCodeCollection GetSystemCodeListByGroup(string groupName)
        {
            var rtn = new SystemCodeCollection();
            rtn.AddRange(_systemCodes.Where(x => x.CodeGroup == groupName).Select(x => x.Clone()).OrderBy(x => x.CodeId));
            return rtn.Count == 0 ? _systemProv.SystemCodeGetListByCodeGroup(groupName) : rtn;
        }
        /// <summary>
        /// 已任一列舉物件取得所有與該物件同Group的systemcode
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static SystemCodeCollection GetSystemCodeListByGroup(Enum value)
        {
            var rtn = new SystemCodeCollection();
            string groupName = GetSystemCodeGroupNameByEnum(value);
            if (string.IsNullOrWhiteSpace(groupName))
            {
                return rtn;
            }
            rtn.AddRange(_systemCodes.Where(x => x.CodeGroup == groupName).Select(x => x.Clone()).OrderBy(x => x.CodeId));
            return rtn.Count == 0 ? _systemProv.SystemCodeGetListByCodeGroup(groupName) : rtn;
        }

        #region SystemCodeAttribute
        public static string GetSystemCodeNameByEnumValue(Enum value)
        {
            var rtn = string.Empty;
            Type enumType = value.GetType();
            var field = enumType.GetField(value.ToString(), BindingFlags.Public | BindingFlags.Static);
            if (field != null && field.GetCustomAttributes(typeof(ExcludeBindingAttribute), false).Length == 0)
            {
                rtn = GetSystemCodeEnum(field);
            }
            return rtn;
        }

        public static string GetSystemCodeGroupNameByEnum(Enum value)
        {
            Type enumType = value.GetType();
            var field = enumType.GetField(value.ToString(), BindingFlags.Public | BindingFlags.Static);
            if (field != null && field.GetCustomAttributes(typeof(ExcludeBindingAttribute), false).Length == 0)
            {
                object[] fobjs = field.GetCustomAttributes(typeof(SystemCodeAttribute), false);
                if (fobjs.Length > 0)
                {
                    var fobj = (SystemCodeAttribute)fobjs[0];
                    return fobj.GroupName;
                }
            }
            return string.Empty;
        }

        public static string GetSystemCodeEnum(MemberInfo info)
        {
            if (info == null) // threw in somehting that's out of range of the enum?
            {
                return string.Empty;
            }

            object[] fobjs = info.GetCustomAttributes(typeof(SystemCodeAttribute), false);
            if (fobjs.Length > 0)
            {
                var fobj = (SystemCodeAttribute)fobjs[0];
                var txt = GetSystemCodeNameByGroupAndId(fobj.GroupName, fobj.Key);
                return txt ?? info.Name;
            }
            return info.Name;
        }



        public static SystemCodeAttribute GetSystemCodeAttributeByEnum(MemberInfo info)
        {
            if (info != null)
            {
                var fobjs = info.GetCustomAttributes(typeof(SystemCodeAttribute), false);
                if (fobjs.Length > 0)
                {
                    var fobj = (SystemCodeAttribute)fobjs[0];
                    return fobj;
                }
            }
            return null;
        }

        #endregion SystemCodeAttribute


        #region HiDealReturnedStatus
        private const string HiDealReturnedStatusGroupName = "HiDealReturnedStatus";
        public static string GetHiDealReturnedStatusCodeName(int codeId)
        {
            return GetSystemCodeNameByGroupAndId(HiDealReturnedStatusGroupName, codeId);
        }
        #endregion HiDealReturnedStatus

        #region HiDealReturnedStatus
        private const string SellerVerifyTypeGroupName = "SellerVerifyType";
        public static SystemCodeCollection GetSellerVerifyType()
        {
            return GetSystemCodeListByGroup(SellerVerifyTypeGroupName); //_systemProv.SystemCodeGetListByCodeGroup("SellerVerifyType");
        }
        #endregion HiDealReturnedStatus

        #region DealTypes

        private const string DealTypeGroupName = "DealType";
        public static SystemCodeCollection GetDealType()
        {
            return GetSystemCodeListByGroup(DealTypeGroupName);
        }

        public static string GetDealTypeName(int codeId)
        {
            return GetSystemCodeNameByGroupAndId(DealTypeGroupName, codeId);
        }

        public static string GetBreadcrumbDealTypeNames(int codeId)
        {
            StringBuilder sb = new StringBuilder();
            int? cid = codeId;
            List<int> idLogs = new List<int>();
            SystemCode sc = null;
            do
            {
                sc = GetSystemCode(DealTypeGroupName, cid.Value);
                if (sc == null)
                {
                    break;
                }
                if (idLogs.Contains(cid.Value))
                {
                    logger.Warn("資料異常，無限迴圈。cids = " + string.Join(",", idLogs));
                    break;
                }
                else
                {
                    idLogs.Add(cid.Value);
                }
                if (sb.Length > 0)
                {
                    sb.Insert(0, " > ");
                }
                sb.Insert(0, sc.CodeName);

                cid = sc.ParentCodeId;
            } while (cid != null);
            return sb.ToString();
        }
        #endregion DealTypes

        #region HiDealRegion
        private const string HiDealRegionGroupName = "HiDealRegion";
        public static SystemCodeCollection GetHiDealRegion()
        {
            return GetSystemCodeListByGroup(HiDealRegionGroupName);
        }
        public static string GetHiDealRegionsName(int codeId)
        {
            return GetSystemCodeNameByGroupAndId(HiDealRegionGroupName, codeId);
        }
        #endregion HiDealRegion

        #region HiDealCatg
        private const string HiDealCatgName = "HiDealCatg";
        public static SystemCodeCollection GetHiDealCatg()
        {
            return GetSystemCodeListByGroup(HiDealCatgName);
        }

        public static string GetHiDealCatgName(int codeId)
        {
            return GetSystemCodeNameByGroupAndId(HiDealCatgName, codeId);
        }
        #endregion HiDealCatg

        #region DealLabel

        private const string DealLabelName = "DealLabel";
        public static string GetDealLabelCodeName(int codeId)
        {
            return GetSystemCodeNameByGroupAndId(DealLabelName, codeId);
        }

        #endregion

        #region SystemData
        //"PersonalDataCollectMatterContent"
        public static SystemData GetSystemDataByName(string systemDataName)
        {
            SystemData systemData = _systemProv.SystemDataGet(systemDataName);
            if (systemData.IsLoaded)
            {
                return systemData;
            }
            else
            {
                var tempSystemData = new SystemData
                {
                    Name = "PersonalDataCollectMatterContent",
                    Data = "",
                    CreateId = "sys@17life.com",
                    CreateTime = DateTime.Now
                };
                systemData = tempSystemData;
            }
            return systemData;
        }

        public static bool SetSystemData(SystemData systemData)
        {
            bool settingResult = default(bool);
            try
            {
                _systemProv.SystemDataSet(systemData);
                settingResult = true;
            }
            catch
            {
                settingResult = false;
            }
            return settingResult;

        }


        #endregion SystemData
    }
}

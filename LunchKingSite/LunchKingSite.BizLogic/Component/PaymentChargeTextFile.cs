﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{

    [AttributeUsage(AttributeTargets.Property)]
    public class AnsiAttribute : Attribute
    {
        public AnsiAttribute()
        {

        }

        public int Index { get; set; }
        public int Size { get; set; }
        public AnsiTextBase.Align Align { get; set; }
        public AnsiTextBase.UnusedChar UnusedChar { get; set; }

        public AnsiAttribute(int index, int size)
        {
            this.Index = index;
            this.Size = size;
        }
    }

    public class AnsiTextSerializer : IDisposable
    {
        readonly BinaryWriter writer;
        public AnsiTextSerializer()
        {
        }
        public AnsiTextSerializer(string filePath)
        {
            FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            writer = new BinaryWriter(fs);
        }
        public AnsiTextSerializer(Stream stream)
        {
            writer = new BinaryWriter(stream);
        }

        public void Write(AnsiTextBase t)
        {
            writer.Write(t.GetBytes());
            writer.Write((byte)10);
        }
        public void Write(List<AnsiTextBase> items)
        {
            foreach (var item in items)
            {
                writer.Write(item.GetBytes());
                writer.Write((byte)10);
            }
        }

        public T ReadItem<T>(byte[] buff,bool isDefault = false) where T : AnsiTextBase
        {
            return AnsiTextBase.Parse<T>(buff, isDefault);
        }

        public void Close()
        {
            if (writer != null)
            {
                writer.Flush();
                writer.Close();
            }
        }

        public void Dispose()
        {
            Close();
        }
    }

    public class AnsiTextBase
    {
        public enum Align
        {
            Left, Right
        }

        public enum UnusedChar
        {
            Space, Number0, Nothing
        }

        private class InnerData
        {
            public AnsiAttribute Attr { get; set; }
            public Type DataType { get; set; }
            public object Value { get; set; }
            public PropertyInfo Property { get; set; }
        }

        private static string TrimStr(string str, UnusedChar value, Align align)
        {
            char removeChar = '\x0';
            if (value == UnusedChar.Number0)
            {
                removeChar = '0';
            }
            else if (value == UnusedChar.Space)
            {
                removeChar = ' ';
            }
            if (align == Align.Left)
            {
                return str.TrimEnd(removeChar);
            }
            else if (align == Align.Right)
            {
                return str.TrimStart(removeChar);
            }
            return str;
        }

        private byte[] GetRepeatBytes(UnusedChar value, int len)
        {
            byte[] result = new byte[len];
            byte defaultUnusendByte;
            if (value == UnusedChar.Space)
            {
                defaultUnusendByte = 32;
            }
            else if (value == UnusedChar.Number0)
            {
                defaultUnusendByte = (byte)'0';
            }
            else
            {
                defaultUnusendByte = 0;
            }
            for (int i = 0; i < len; i++)
            {
                result[i] = defaultUnusendByte;
            }
            return result;
        }

        public byte[] GetBytes()
        {
            PropertyInfo[] props = this.GetType().GetProperties();
            List<InnerData> InnerDatas = new List<InnerData>();
            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    AnsiAttribute fixedLenAttr = attr as AnsiAttribute;

                    if (fixedLenAttr != null)
                    {
                        InnerDatas.Add(
                            new InnerData
                            {
                                Attr = fixedLenAttr,
                                DataType = prop.PropertyType,
                                Value = prop.GetValue(this, null)
                            }
                        );
                    }
                }
            }

            InnerDatas = InnerDatas.OrderBy(t => t.Attr.Index).ToList();
            int bufSize = InnerDatas.Sum(t => t.Attr.Size);
            byte[] buffer = new byte[bufSize];

            int pos = 0;
            foreach (var innerData in InnerDatas)
            {
                byte[] srcBuff;
                if (innerData.Value == null)
                {
                    srcBuff = new byte[0];
                }
                else
                {
                    srcBuff = Encoding.ASCII.GetBytes((string)innerData.Value);
                }

                int mendLen = innerData.Attr.Size - srcBuff.Length;
                byte[] mendBytes = GetRepeatBytes(innerData.Attr.UnusedChar, mendLen);

                if (srcBuff.Length < innerData.Attr.Size)
                {
                    if (innerData.Attr.Align == Align.Left)
                    {
                        Buffer.BlockCopy(srcBuff, 0, buffer, pos, srcBuff.Length);
                        pos += srcBuff.Length;
                        Buffer.BlockCopy(mendBytes, 0, buffer, pos, mendBytes.Length);
                        pos += mendLen;
                    }
                    else if (innerData.Attr.Align == Align.Right)
                    {
                        Buffer.BlockCopy(mendBytes, 0, buffer, pos, mendBytes.Length);
                        pos += mendLen;
                        Buffer.BlockCopy(srcBuff, 0, buffer, pos, srcBuff.Length);
                        pos += srcBuff.Length;
                    }
                }
                else
                {
                    Buffer.BlockCopy(srcBuff, 0, buffer, pos, srcBuff.Length);
                    pos += innerData.Attr.Size;
                }
            }

            return buffer;
        }

        public static T Parse<T>(byte[] buff,bool isDefault = false) where T : AnsiTextBase
        {
            PropertyInfo[] props = typeof(T).GetProperties();
            List<InnerData> innerDatas = new List<InnerData>();
            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    AnsiAttribute fixedLenAttr = attr as AnsiAttribute;

                    if (fixedLenAttr != null)
                    {
                        innerDatas.Add(
                            new InnerData
                            {
                                Attr = fixedLenAttr,
                                DataType = prop.PropertyType,
                                Property = prop
                            }
                        );
                    }
                }
            }


            T result = Activator.CreateInstance<T>();
            innerDatas = innerDatas.OrderBy(t => t.Attr.Index).ToList();
            int pos = 0;
            foreach (var innerData in innerDatas)
            {
                byte[] tmp = new byte[innerData.Attr.Size];
                Buffer.BlockCopy(buff, pos, tmp, 0, tmp.Length);
                pos += tmp.Length;
                string str = Encoding.ASCII.GetString(tmp);
                if (isDefault)
                {
                    str = Encoding.Default.GetString(tmp);
                }
                innerData.Property.SetValue(
                    result, TrimStr(str, innerData.Attr.UnusedChar, innerData.Attr.Align), null);
            }
            return result;
        }

        public override string ToString()
        {
            return Encoding.ASCII.GetString(GetBytes());
        }
    }
}

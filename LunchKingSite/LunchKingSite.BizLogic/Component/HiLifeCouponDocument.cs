﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class HiLifeCouponDocument : CouponDocumentBase
    {
        protected static ISysConfProvider config;

        protected static IHiLifeProvider hiLife;

        /// <summary>
        /// 憑證序號
        /// </summary>
        public string CouponCode { set; get; }

        public CouponCodeType CodeType { set; get; }
        /// <summary>
        /// 短標
        /// </summary>
        public string CouponUsage { set; get; }
        /// <summary>
        /// 黑標
        /// </summary>
        public string EventName { set; get; }
        /// <summary>
        /// 優惠有效日期(起)
        /// </summary>
        public DateTime DeliverTimeStart { set; get; }

        /// <summary>
        /// 優惠有效日期(迄)
        /// </summary>
        public DateTime DeliverTimeEnd { set; get; }
        
        private IViewPponDeal Vpd { set; get; }

        private Dictionary<string, System.Drawing.Image> BarcodeList { get; set; }

        public HiLifeCouponDocument(ViewPponCoupon coupon, CouponCodeType codeType, IViewPponDeal vpd)
        {
            config = ProviderFactory.Instance().GetConfig();
            hiLife = ProviderFactory.Instance().GetProvider<IHiLifeProvider>();
            
            Vpd = vpd;
            CouponCode = coupon.CouponCode;
            CodeType = codeType;
            CouponUsage = coupon.CouponUsage;
            EventName = coupon.EventName;
            DeliverTimeStart = coupon.BusinessHourDeliverTimeS.Value;
            DeliverTimeEnd = coupon.BusinessHourDeliverTimeE.Value;
            BarcodeList = new Dictionary<string, System.Drawing.Image>();

            switch (CodeType)
            {
                case CouponCodeType.HiLifeItemCode://2 Barcode
                  
                    var p = hiLife.GetHiLifePincode(coupon.SequenceNumber);
                    if (p==null)
                    {
                        return;
                    }

                    BarcodeList.Add(config.HiLifeFixedFunctionPincode, new Core.Component.BarCode39(config.HiLifeFixedFunctionPincode, false, false).Paint());
                    BarcodeList.Add(coupon.SequenceNumber.ToUpper(), new Core.Component.BarCode39(coupon.SequenceNumber.ToUpper(), false, false).Paint());
                    break;
                default:
                    PrepareImages(CouponCode, (CouponCodeType)vpd.CouponCodeType);
                    break;
            }
        }

        protected override void RenderPDFContent()
        {
            RenderLogoAndItemName();
            RenderBarcode();
            RenderCouponInfo();
        }

        #region Render PDF

        private void RenderLogoAndItemName()
        {
            PrepareLogoImage();

            //增加上下間的空白間距
            var p = new Paragraph();
            p.SpacingBefore = 2;
            p.SpacingAfter = 3;
            doc.Add(p);

            PdfPTable table = new PdfPTable(3);
            table.SetWidths(new int[] { 175, 220, 200 });
            table.DefaultCell.Border = 1;
            table.WidthPercentage = 95;

            table.AddCell(
                new PdfPCell(pdfImgLogo)
                {
                    FixedHeight = 60f,
                    PaddingTop = 5,
                    PaddingLeft = 3,
                    PaddingRight = 3,
                    Rowspan = 1,
                    Border = 0,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BorderWidthRight = _BREAKLINE_BORDER_WIDTH,
                    BorderColorRight = _BREAKLINE_BORDER_COLOR,
                    BorderWidthBottom = _BREAKLINE_BORDER_WIDTH,
                    BorderColorBottom = _BREAKLINE_BORDER_COLOR,
                });

            Paragraph itemName = new Paragraph();

            itemName.Add(new Phrase(Vpd.CouponUsage, Fonts.Mingliu10Bold));
            itemName.Add("\n\n");
            itemName.Add(new Chunk(string.Format(@"優惠有效期限：{0} ~ {1}"
                , DeliverTimeStart.ToString("yyyy/MM/dd")
                , DeliverTimeEnd.ToString("yyyy/MM/dd")), Fonts.Mingliu10));

            var itemNameCell = new PdfPCell(itemName);
            itemNameCell.PaddingLeft = 10;
            itemNameCell.Border = 0;
            itemNameCell.BorderWidthBottom = _BREAKLINE_BORDER_WIDTH;
            itemNameCell.BorderColorBottom = _BREAKLINE_BORDER_COLOR;
            itemNameCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            itemNameCell.FixedHeight = 60f;
            itemNameCell.Colspan = 2;

            table.AddCell(itemNameCell);
            doc.Add(table);
        }

        private void RenderBarcode()
        {
            PdfPTable barTable = new PdfPTable(1);
            barTable.SetWidths(new int[] { 400 });
            barTable.DefaultCell.Border = 0;
            barTable.WidthPercentage = 50;

            foreach (var barcodeImg in BarcodeList)
            {
                barTable.AddCell(
                    new PdfPCell(iTextSharp.text.Image.GetInstance(barcodeImg.Value, ImageFormat.Png))
                    {
                        FixedHeight = 40f,
                        PaddingTop = (barcodeImg.Value.Height >= 70) ? (barcodeImg.Value.Height - 70) + 5 : 5,
                        PaddingLeft = 0,
                        PaddingRight = 0,
                        PaddingBottom = 1,
                        Border = 0,
                        BorderWidth = 0,
                        VerticalAlignment = Element.ALIGN_BOTTOM,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        BorderWidthRight = 0,
                        BorderWidthBottom = 0
                    });

                barTable.AddCell(
                    new PdfPCell(new Phrase(barcodeImg.Key, Fonts.Arial10))
                    {
                        PaddingTop = 0,
                        VerticalAlignment = Element.ALIGN_TOP,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        BorderWidthTop = 0,
                        Border = 0
                    });
            }
            doc.Add(barTable);
        }

        private void RenderCouponInfo()
        {
            //增加上下間的空白間距
            var space = new Paragraph();
            space.SpacingBefore = 4;
            space.SpacingAfter = 3;
            doc.Add(space);


            var p = new Paragraph();
            p.Leading = 15;
            p.IndentationLeft = 20;
            p.IndentationRight = 10;
            p.Alignment = Element.ALIGN_LEFT;
            p.Add(new Phrase("＊如無法掃描請來電至萊爾富客服，並告知此杯咖啡PIN碼編號，一杯有一組PIN碼編號，" + 
                             "如需兌換多杯則要告知每杯咖啡的PIN碼編號。" + 
                             "萊爾富客服專線：0800-022-118", Fonts.Mingliu8));

            doc.Add(p);
        }

        #endregion
    }
}

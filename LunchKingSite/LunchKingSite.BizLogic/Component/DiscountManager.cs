﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class DiscountManager
    {
        private static ILog _logger = LogManager.GetLogger(typeof(DiscountManager));
        private static ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ICacheProvider _cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
        private static IPponEntityProvider _pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();

        public static CampaignRule GetCampaignRule(int campaignId, bool forceRefresh = false)
        {
            string cacheKey = string.Format("{0}:{1}", typeof(CampaignRule).Name, campaignId);
            CampaignRule rule = null; 
            if (forceRefresh == false)
            {
                rule = _cache.Get<CampaignRule>(cacheKey);
            }
            if (rule == null)
            {
                rule = GetCampaignRuleFromDatabase(campaignId);
                _cache.Set(cacheKey, rule, TimeSpan.FromMinutes(110));
            }
            return rule;
        }

        public static void ClearCampaignRuleCache(List<int> campaignIds)
        {
            if (campaignIds == null)
            {
                return;
            }
            foreach (int campaignId in campaignIds)
            {
                string cacheKey = string.Format("{0}:{1}", typeof(CampaignRule).Name, campaignId);
                _cache.Remove(cacheKey);
            }
        }

        public static Random random = new Random();
        public static DealDiscountTerm GetDealDiscountTerm(Guid bid, bool forceRefresh = false)
        {            
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            if (vpd == null)
            {
                return null;
            }
            TimeSpan expireTime = TimeSpan.FromSeconds(random.Next(1800, 3600)); //1~2小時
            string cacheKey = string.Format("{0}:{1}:{2}", typeof(DealDiscountTerm).Name, bid, vpd.EventModifyTime.Ticks);
            DealDiscountTerm rule = null;
            if (forceRefresh == false)
            {
                rule = MemoryCache2.Get<DealDiscountTerm>(cacheKey);
                if (rule == null)
                {
                    rule = _cache.Get<DealDiscountTerm>(cacheKey);
                    if (rule != null)
                    {
                        MemoryCache2.Set(cacheKey, rule, expireTime);
                    }
                }
            }
            if (rule == null)
            {
                rule = GetDealDiscountTermFromDatabase(bid);
                MemoryCache2.Set(cacheKey, rule, expireTime);
                _cache.Set(cacheKey, rule, expireTime);
            }
            return rule;
        }

        /// <summary>
        /// 取得折後價禁用的檔次名單
        /// </summary>
        public static HashSet<Guid> GetDealDiscountPriceBlackList()
        {
            var dealDiscountPriceBlacklists = _pep.GetAllDealDiscountPriceBlacklist();
            var bids = dealDiscountPriceBlacklists.Select(c => c.Bid).Distinct();

            HashSet<Guid> blackBids = new HashSet<Guid>(bids);
            return blackBids;
        }

        private static CampaignRule GetCampaignRuleFromDatabase(int campaignId)
        {
            DiscountCampaign campaign = _op.DiscountCampaignGet(campaignId);
            
            if (campaign.IsLoaded == false)
            {
                return null;
            }
            var rule = new CampaignRule
            {
                CampaignId = campaignId,
                CampaignName = campaign.Name,
                MinGrossMargin = campaign.MinGrossMargin,
                MinimumAmount = campaign.MinimumAmount,
                Amount = (int)campaign.Amount.GetValueOrDefault(),
                AppUseOnly = Helper.IsFlagSet(campaign.Flag, DiscountCampaignUsedFlags.AppUseOnly)
            };
            

            if (Helper.IsFlagSet(campaign.Flag, DiscountCampaignUsedFlags.CategoryLimit))
            {
                rule.CategoryLimit = true;
                var dcs = _op.DiscountCategoryGetList(campaignId);
                rule.CategoryIds = new HashSet<int>(dcs.Select(t => t.CategoryId).ToList());
            }

            rule.PromotionIds = new HashSet<int>(_op.DiscountEventPromoLinkGetList(campaignId)
                .Where(t => t.EventPromoId != 0)
                .Select(t => t.EventPromoId));
            rule.BrandIds = new HashSet<int>(_op.DiscountBrandLinkGetList(campaignId).Select(t=>t.BrandId));

            return rule;
        }

        private static DealDiscountTerm GetDealDiscountTermFromDatabase(Guid bid)
        {
            Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(bid); //母檔BID
            DealDiscountTerm term = new DealDiscountTerm
            {
                MainBid = mainBid,
                Bid = bid
            };
            //不可使用折價券就不做處理
            term.Banned = _op.DiscountLimitEnabledGetByBid(mainBid);
            term.CategoryIds = new HashSet<int>(_pp.CategoryDealsGetList(mainBid).Select(t=>t.Cid));
            term.BrandIds = new HashSet<int>(_pp.GetViewBrandItemListByBid(mainBid).Select(x => x.MainId));
            term.PromotionIds = new HashSet<int>(_pp.GetViewEventPromoItemListByBid(mainBid).Select(x => x.MainId));
            //term.GrossMargin = PponFacade.GetMinimumGrossMarginFromBids(mainBid, true) * 100;
            term.GrossMargin = PponFacade.GetDealMinimumGrossMargin(mainBid) * 100;
            return term;
        }

        /// <summary>
        /// 檔次的折扣相關的條件
        /// </summary>
        public class DealDiscountTerm
        {
            public DealDiscountTerm()
            {
                CategoryIds = new HashSet<int>();
                BrandIds = new HashSet<int>();
                PromotionIds = new HashSet<int>();
            }
            public Guid MainBid { get; set; }
            public Guid Bid { get; set; }
            public bool Banned { get; set; }
            public decimal GrossMargin { get; set; }
            public HashSet<int> CategoryIds { get; set; }
            public HashSet<int> BrandIds { get; set; }
            public HashSet<int> PromotionIds { get; set; }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(this.Bid);
                if (CategoryIds.Count > 0)
                {
                    sb.Append(", Category: ");
                    sb.Append(CategoryIds.Count);
                }
                if (BrandIds.Count > 0)
                {
                    sb.Append(", Brand: ");
                    sb.Append(BrandIds.Count);
                }
                if (PromotionIds.Count > 0)
                {
                    sb.Append(", Promotion: ");
                    sb.Append(PromotionIds.Count);
                }
                return sb.ToString();
            }
        }

        public class CampaignRule
        {

            public CampaignRule()
            {
                CategoryIds = new HashSet<int>();
                BrandIds = new HashSet<int>();
                PromotionIds = new HashSet<int>();
            }

            public string CampaignName { get; set; }
            public int CampaignId { get; set; }
            /// <summary>
            /// 手機限定
            /// </summary>
            public bool AppUseOnly { get; set; }
            public bool CategoryLimit { get; set; }
            public int? MinimumAmount { get; set; }
            public int? MinGrossMargin { get; set; }
            public HashSet<int> CategoryIds { get; set; }
            public HashSet<int> BrandIds { get; set; }
            public HashSet<int> PromotionIds { get; set; }
            public int Amount { get; set; }

            public bool Match(DealDiscountTerm discountTerm)
            {
                if (discountTerm == null)
                {
                    return false;
                }

                if (discountTerm.Banned)
                {
                    return false;
                }

                if (this.CategoryIds.Count > 0)
                {
                    if (this.CategoryIds.Intersect(discountTerm.CategoryIds).Any() == false)
                    {
                        return false;
                    }
                }

                if (this.BrandIds.Count > 0)
                {
                    if (this.BrandIds.Intersect(discountTerm.BrandIds).Any() == false)
                    {
                        return false;
                    }
                }

                if (this.PromotionIds.Count > 0)
                {
                    if (this.PromotionIds.Intersect(discountTerm.PromotionIds).Any() == false)
                    {
                        return false;
                    }
                }
                //如果活動有設定最低毛利率，且 檔次的毛利率 < 活動最低毛利率 即不符合
                //也就是，檔次毛利率 大於且等於 活動最低毛利率 是符合活動的要件之一。
                if (this.MinGrossMargin.GetValueOrDefault() > 0)
                {
                    if (this.MinGrossMargin > discountTerm.GrossMargin)
                    {
                        return false;
                    }
                }

                return true;
            }



            public override string ToString()
            {
                return this.CampaignName;
            }

            public bool MatchPrice(IViewPponDeal deal, out decimal avgPrice)
            {
                if (deal == null)
                {
                    avgPrice = 0;
                    return false;
                }
                bool result;
                int qty = deal.QuantityMultiplier ?? 1;

                if (deal.ItemPrice >= this.MinimumAmount)
                {
                    result = true;
                    avgPrice = (deal.ItemPrice - this.Amount) / qty;
                }
                else
                {
                    result = false;
                    avgPrice = deal.ItemPrice / qty;
                }
                return result;
            }
        }

        public static List<CampaignRule> GetOnlineCampaignRules(bool forceRefresh = false)
        {
            List<CampaignRule> result = new List<CampaignRule>();
            List<int> campaignIds = _op.DiscountCampaignIdGetListOnline();
            foreach(int campaignId in campaignIds)
            {
                var rule = GetCampaignRule(campaignId, forceRefresh);
                if (rule != null)
                {
                    result.Add(rule);
                }
            }
            return result;
        }

        /// <summary>
        /// 清掉檔次折扣相關條件的暫存，然後進行折後價計算
        /// 
        /// TODO: 哪天實作清除單檔暫存(ViewPponDeal)，需要再回頭調整
        /// 
        /// </summary>
        /// <param name="bid"></param>
        public static void ForceRefreshDealDiscountedPrice(Guid bid)
        {
            List<CampaignRule> campaignRules = GetOnlineCampaignRules();
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
           

            HashSet<Guid> bannedBids = GetDealDiscountPriceBlackList();
            RefreshDistcountedPrice(campaignRules, deal, bannedBids, true);
        }

        /// <summary>
        /// 更新多筆檔次目前適用折扣的折扣價資料
        /// </summary>
        /// <param name="deals"></param>
        public static void RefreshDistcountedPrice(List<IViewPponDeal> deals)
        {
            List<CampaignRule> campaignRules = DiscountManager.GetOnlineCampaignRules();

            DateTime watchNow = DateTime.Now;
            HashSet<Guid> bannedBids = GetDealDiscountPriceBlackList();
            foreach (var deal in deals)
            {
                RefreshDistcountedPrice(campaignRules, deal, bannedBids);
            }
            _pep.DeleteDealDiscountPropertyExpired();

            _logger.InfoFormat("折扣計算 {0} 秒", (DateTime.Now - watchNow).TotalSeconds.ToString("0.##"));
        }

        /// <summary>
        /// 更新檔次目前適用折扣的折扣價資料
        /// </summary>
        /// <param name="campaignRules"></param>
        /// <param name="deal"></param>
        /// <param name="forceRefresh"></param>
        private static void RefreshDistcountedPrice(List<CampaignRule> campaignRules, IViewPponDeal deal, 
            HashSet<Guid> bannedBids,
            bool forceRefresh = false)
        {
            if (campaignRules == null || deal == null)
            {
                throw new Exception("campaignRules or deal is null");
            }

            List<IViewPponDeal> subDeals = new List<IViewPponDeal>();
            foreach (Guid subDealId in deal.ComboDelas.Where(t => t.Slug == null).Select(t => t.BusinessHourGuid))
            {
                IViewPponDeal subDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(subDealId, true);
                if (subDeal != null)
                {
                    subDeals.Add(subDeal);
                }
            }

            if (subDeals.Count == 0)
            {
                subDeals.Add(deal);
            }

            var dealDiscountTerm = DiscountManager.GetDealDiscountTerm(deal.BusinessHourGuid, forceRefresh);

            decimal minAvgPrice;
            int? applicableCampaignId;
            string applicableCampaignName;
            IViewPponDeal applicableSubDeal;
            DateTime now = DateTime.Now;
            if (_config.EnableDiscountPrice && 
                bannedBids.Contains(deal.BusinessHourGuid) == false &&
                DiscountManager.TryGetDealDiscountPriceMatchDiscountCampaign(
                subDeals, dealDiscountTerm, campaignRules,
                out minAvgPrice,
                out applicableCampaignId,
                out applicableCampaignName,
                out applicableSubDeal))
            {
                var dealDistinctProperty = _pep.GetDealDiscountProperty(deal.BusinessHourGuid);
                if (dealDistinctProperty == null)
                {

                    dealDistinctProperty = new DealDiscountProperty
                    {
                        Bid = deal.BusinessHourGuid,
                        DealId = deal.UniqueId.GetValueOrDefault(),
                        ItemName = deal.ItemName,
                        ItemPrice = deal.ItemPrice,
                        CreatedTime = now
                    };
                }
                dealDistinctProperty.ModifiedTime = now;
                dealDistinctProperty.DiscountPrice = Math.Ceiling(minAvgPrice);
                dealDistinctProperty.DiscountCampaignId = applicableCampaignId.GetValueOrDefault();
                dealDistinctProperty.DiscountCampaignName = applicableCampaignName;
                dealDistinctProperty.ApplicableDealBid = applicableSubDeal.BusinessHourGuid;
                dealDistinctProperty.ApplicableDealId = applicableSubDeal.UniqueId.GetValueOrDefault();
                dealDistinctProperty.ApplicableDealItemName = applicableSubDeal.ItemName;
                if (dealDistinctProperty.DiscountPrice > deal.ItemPrice)
                {
                    dealDistinctProperty.DiscountPrice =  deal.ItemPrice;
                    //string errorMessage = string.Format("折後價>售價: https://www.17life.com/deal/{0}", deal.BusinessHourGuid);
                    //LogManager.GetLogger(LineAppender._LINE).Info(errorMessage);
                    //_logger.Warn(errorMessage);
                }
                _pep.SetDealDiscountProperty(dealDistinctProperty);
            }
            else
            {
                _pep.DeleteDealDiscountProperty(deal.BusinessHourGuid);
            }
        }

        private static bool TryGetDealDiscountPriceMatchDiscountCampaign(
            List<IViewPponDeal> subDeals, 
            DealDiscountTerm discountTerm, 
            List<CampaignRule> campaignRules,
            out decimal minAvgPrice, 
            out int? applicableCampaignId, 
            out string applicableCampaignName,
            out IViewPponDeal applicableSubDeal)
        {
            applicableCampaignId = null;
            applicableCampaignName = null;
            applicableSubDeal = null;
            List<CampaignRule> matchRules =
                campaignRules.Where(t => t.Match(discountTerm)).ToList();

            if (matchRules.Count == 0)
            {
                minAvgPrice = 0;
                return false;
            }

            //每個折價券跟每個子檔比對後，算出最低均價
            
            minAvgPrice = subDeals[0].ItemPrice / (subDeals[0].QuantityMultiplier ?? 1);

            foreach (IViewPponDeal subDeal in subDeals)
            {
                foreach (var matchRule in matchRules)
                {
                    decimal avgPrice;
                    if (matchRule.MatchPrice(subDeal, out avgPrice))
                    {
                        if (avgPrice < minAvgPrice)
                        {
                            applicableCampaignId = matchRule.CampaignId;
                            applicableCampaignName = matchRule.CampaignName;
                            applicableSubDeal = subDeal;
                            minAvgPrice = avgPrice;
                        }
                    }
                }
            }
            minAvgPrice = Math.Round(minAvgPrice, 1, MidpointRounding.AwayFromZero);
            bool result = minAvgPrice > 0 && applicableCampaignId != null && applicableCampaignName != null;
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class HiDealCouponSmsFormatter : SmsFormatterBase
    {
        #region properties

        public override string DbTemplateContent { get; set; }

        #region 轉成DB範本用

        public string SmsTitle { private get; set; }
        public string SmsPrefix { private get; set; }
        public string SmsInformation { private get; set; }

        #endregion

        #region 轉簡訊用

        /// <summary>
        /// 轉簡訊時用來取代範本中的憑證前置碼 (需要同時設定 CouponSequence)
        /// </summary>
        public string CouponPrefix { get; set; }

        /// <summary>
        /// 轉簡訊時用來取代範本中的憑證序號 (需要同時設定 CouponPrefix)
        /// </summary>
        public string CouponSequence { get; set; }

        /// <summary>
        /// 轉簡訊時用來取代範本中的憑證代碼
        /// </summary>
        public string CouponCode { get; set; }

        /// <summary>
        /// 轉簡訊時用來取代範本中的開始使用日
        /// </summary>
        public DateTime? BeginDate { get; set; }

        /// <summary>
        /// 轉簡訊時用來取代範本中的使用到期日
        /// </summary>
        public DateTime? ExpireDate { get; set; }

        /// <summary>
        /// 轉簡訊時用來取代範本中的預約專線
        /// </summary>
        public string TelephoneInfo { get; set; }   

        #endregion

        #endregion

        #region public methods

        public override string ToDbFormat()
        {
            return string.Join("|", SmsTitle, SmsPrefix, SmsInformation);
        }

        public override string ToSms()
        {
            if (string.IsNullOrEmpty(DbTemplateContent))
            {
                throw new NullReferenceException("沒有範本內容! DbTemplateContent is null or an empty string.");
            }

            string transformedStr = DbTemplateContent.Replace("|", "");

            if (!string.IsNullOrWhiteSpace(TelephoneInfo))
            {
                transformedStr = transformedStr.Replace("{tel}", TelephoneInfo);
            }

            if (!string.IsNullOrWhiteSpace(CouponPrefix) && !string.IsNullOrWhiteSpace(CouponSequence))
            {
                transformedStr = transformedStr.Replace("{0}", CouponPrefix + CouponSequence);
            }

            if (!string.IsNullOrWhiteSpace(CouponCode))
            {
                transformedStr = transformedStr.Replace("{1}", CouponCode);
            }

            if (BeginDate != null)
            {
                transformedStr = transformedStr.Replace("{2}", BeginDate.Value.ToString("yyyyMMdd"));
            }

            if (ExpireDate != null)
            {
                transformedStr = transformedStr.Replace("{3}", ExpireDate.Value.ToString("yyyyMMdd"));
            }

            return transformedStr;
        }

        public string GetSmsTitleFromTemplateContent()
        {
            if (string.IsNullOrEmpty(DbTemplateContent))
            {
                return null;
            }

            string[] sms = DbTemplateContent.Split('|');
            return sms[0];
        }

        public string GetSmsPrefixFromTemplateContent()
        {
            if (string.IsNullOrEmpty(DbTemplateContent))
            {
                return null;
            }

            string[] sms = DbTemplateContent.Split('|');
            return sms[1];
        }

        public string GetSmsInformationFromTemplateContent()
        {
            if (string.IsNullOrEmpty(DbTemplateContent))
            {
                return null;
            }

            string[] sms = DbTemplateContent.Split('|');
            return sms[2];
        }

        #endregion
        
        #region .ctors

        public HiDealCouponSmsFormatter() { }

        public HiDealCouponSmsFormatter(int productId)
        {
            IHiDealProvider hdProvider = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            HiDealProduct product = hdProvider.HiDealProductGet(productId);
            this.DbTemplateContent = product.Sms;
            this.BeginDate = product.UseStartTime;
            this.ExpireDate = product.UseEndTime;
        }

        public HiDealCouponSmsFormatter(HiDealProduct product)
        {
            this.DbTemplateContent = product.Sms;
            this.BeginDate = product.UseStartTime;
            this.ExpireDate = product.UseEndTime;
        }

        public HiDealCouponSmsFormatter(string smsTemplate)
        {
            this.DbTemplateContent = smsTemplate;
        }

        #endregion
    }
}

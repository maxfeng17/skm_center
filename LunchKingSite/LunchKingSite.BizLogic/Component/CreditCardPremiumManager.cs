﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class CreditCardPremiumManager
    {
        private static IMarketingProvider _markProv;
        private static IOrderProvider _orderProv;
        private static CreditCardPremiumCollection _piinlifeVisaCreditCards;
        private static CreditCardPremiumCollection _taishinCreditCardRules;
        private static CreditCardPremiumCollection _visaCreditCards;
        private static CreditcardBankInfoCollection _bankInfo;
        static CreditCardPremiumManager()
        {
            _markProv = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
            _orderProv = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _piinlifeVisaCreditCards = new CreditCardPremiumCollection();
            _taishinCreditCardRules = new CreditCardPremiumCollection();
            _visaCreditCards = new CreditCardPremiumCollection();
            _bankInfo = new CreditcardBankInfoCollection();
        }


        #region public method

        public static void ClearAllTemporaryData()
        {
            _piinlifeVisaCreditCards = null;
            _taishinCreditCardRules = null;
            _visaCreditCards = null;
            _bankInfo = null;
        }

        /// <summary>
        /// 取得信用卡銀行資訊
        /// </summary>
        /// <param name="cardNum"></param>
        /// <returns></returns>
        public static CreditcardBankInfo GetCreditCardBankInfo(string cardNum)
        {
            if (_bankInfo.Count == 0)
            {
                lock (_bankInfo)
                {
                    if (_bankInfo.Count == 0)
                    {
                        _bankInfo = _orderProv.CreditcardBankInfoGetList();
                    }
                }
            }
            CreditcardBankInfo info = _bankInfo.FirstOrDefault(x => x.Bin == cardNum.PadRight(16, '0').Substring(0, x.Bin.Length));
            if (info != null && info.Id != 0)
            {
                return info;
            }
            else
            {
                return new CreditcardBankInfo();
            }
        }

        /// <summary>
        /// 取得信用卡銀行名稱
        /// </summary>
        /// <param name="bankId"></param>
        /// <returns></returns>
        public static string GetCreditcardBankNameByBankId(int bankId)
        {
            if (bankId == 0)
            {
                return string.Empty;
            }

            if (!_bankInfo.Any())
            {
                _bankInfo = _orderProv.CreditcardBankInfoGetList();
            }
            var info = _bankInfo.FirstOrDefault(x => x.Id == bankId);
            if (info != null && string.IsNullOrEmpty(info.BankName))
            {
                return info.BankName;
            }
            return string.Empty;
        }

        public static string GetCreditcardBankBinsByInstallment()
        {
            List<string> bins= _orderProv.CreditcardBankInstallmentBinsGetList();
            return string.Join(",", bins.ToArray());
        }

        public static string GetCreditcardBankBinsByBankId(int bankId)
        {
            List<string> bins = _orderProv.CreditcardBankBinsGetList(bankId);
            return string.Join(",", bins.ToArray());
        }

        /// <summary>
        /// 取得刷卡銀行代號， 無值回傳null
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        public static int? GetBankIdByCardNumber(string cardNumber)
        {
            if (string.IsNullOrEmpty(cardNumber) || cardNumber.Length <= 6)
            {
                return null;
            }
            string bin = cardNumber.Substring(0, 6);
            CreditcardBankInfo bank = _orderProv.CreditcardBankInfoGetByBin(bin);
            if (bank == null)
            {
                return null;
            }
            return bank.BankId;
        }

        /// <summary>
        /// 檢查是否為visa卡 折價券折抵適用
        /// </summary>
        /// <param name="cardNum"></param>
        /// <returns></returns>
        public static bool IsVisaCardNumber(string cardNum)
        {
            if (_visaCreditCards.Count == 0)
            {
                lock (_visaCreditCards)
                {
                    if (_visaCreditCards.Count == 0)
                    {
                        _visaCreditCards = _markProv.CreditCardPremiumGetListByActivityCodeAndEnabled("VISA_DiscountCode");
                    }
                }
            }
            if (_visaCreditCards.Any(x => x.CardNum == cardNum.PadRight(16, '0').Substring(0, x.CardNum.Length)))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 2013年之前用的判斷Visa卡的程式
        /// 符合條件的只有無限卡與御璽卡
        /// </summary>
        /// <param name="cardNum"></param>
        /// <returns></returns>
        public static bool IsPiinlifeVisaCardNumber(string cardNum)
        {
            if (_piinlifeVisaCreditCards.Count == 0)
            {
                lock (_piinlifeVisaCreditCards)
                {
                    if (_piinlifeVisaCreditCards.Count == 0)
                    {
                        _piinlifeVisaCreditCards =
                            _markProv.CreditCardPremiumGetListByActivityCodeAndEnabled("PiinLife_Visa");
                    }
                }
            }
            if (_piinlifeVisaCreditCards.Any(x => x.CardNum == cardNum.Substring(0, x.CardNum.Length) &&
                (x.CardType == (int)HiDealVisaCardType.Infinite || x.CardType == (int)HiDealVisaCardType.Signature)))
            {
                return true;
            }
            return false;
        }

        public static HiDealVisaCardType GetPiinlifeVisaCardType(string cardNum)
        {
            if (string.IsNullOrEmpty(cardNum))
            {
                return HiDealVisaCardType.Normal;
            }

            if (_piinlifeVisaCreditCards.Count == 0)
            {
                lock (_piinlifeVisaCreditCards)
                {
                    if (_piinlifeVisaCreditCards.Count == 0)
                    {
                        _piinlifeVisaCreditCards =
                            _markProv.CreditCardPremiumGetListByActivityCodeAndEnabled("PiinLife_Visa");
                    }
                }
            }

            var item = _piinlifeVisaCreditCards
                .Where(t => cardNum.Length > t.CardNum.Length &&
                    t.CardNum == cardNum.Substring(0, t.CardNum.Length)).OrderBy(t => t.CardType).FirstOrDefault();

            if (item == null)
            {
                return HiDealVisaCardType.Normal;
            }
            return (HiDealVisaCardType)item.CardType;
        }
        
        public static CreditCardPremiumCollection GetTaishinCreditCardsNumberRules()
        {
            if (_taishinCreditCardRules.Count == 0)
            {
                lock (_taishinCreditCardRules)
                {
                    if (_taishinCreditCardRules.Count == 0)
                    {
                        _taishinCreditCardRules =
                            _markProv.CreditCardPremiumGetListByActivityCodeAndEnabled("SKM_Taishin");
                    }
                }

            }
            return _taishinCreditCardRules;
        }

        #endregion 

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ons;

namespace LunchKingSite.BizLogic.Component.RocketMQTrans
{
    public class SKMMsgListener : MessageListener
    {
        public SKMMsgListener()
        {
        }

        ~SKMMsgListener()
        {
        }

        public override ons.Action consume(Message value, ConsumeContext context)
        {
            //先串通，再來傳送相關資訊及log再補寫
            //byte[] text = Convert.FromBase64String(value.getBody());//中文decode
            //string body = Encoding.UTF8.GetString(text);
            //Byte[] text = Encoding.Default.GetBytes(value.getBody());
            //Console.WriteLine(Encoding.UTF8.GetString(text));
            //Console.WriteLine("Receive message:{0}", value.getMsgID());
            return ons.Action.CommitMessage;
        }
    }
}

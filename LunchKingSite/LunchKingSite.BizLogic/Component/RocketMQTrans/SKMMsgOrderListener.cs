﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ons;

namespace LunchKingSite.BizLogic.Component.RocketMQTrans
{
    public class SKMMsgOrderListener : MessageOrderListener
    {
        public SKMMsgOrderListener()
        {

        }

        ~SKMMsgOrderListener()
        {
        }

        public override ons.OrderAction consume(Message value, ConsumeOrderContext context)
        {
            //先測通，收送訊息的log後補
            //Byte[] text = Encoding.Default.GetBytes(value.getBody());
            //Console.WriteLine(Encoding.UTF8.GetString(text));
            return ons.OrderAction.Success;
        }
    }
}

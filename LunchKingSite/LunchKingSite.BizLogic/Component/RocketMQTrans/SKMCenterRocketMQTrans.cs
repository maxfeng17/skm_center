﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Component.RocketMQEntity;
using ons;
using LunchKingSite.Core;
using System.Data;

namespace LunchKingSite.BizLogic.Component.RocketMQTrans
{
    public class SKMCenterRocketMQTrans
    {
        private static Producer _producer;
        private static PushConsumer _consumer;
        private static OrderConsumer _orderconsumer;
        private static OrderProducer _orderproducer;
        private static SKMMsgListener _listen;
        private static SKMMsgOrderListener _order_listen;
        private static object s_SyncLock = new Object();


        private static void MQSend(InventoriesMessage inventoriesMessage, string tag)
        {
            string transMessage = inventoriesMessage.jsonOut();

            SKMCenterRocketMQTrans.SendMessage(transMessage, tag);
        }

        private static void MQSend(SKMItemEntity sKMItemEntity, string tag)
        {
            string transMessage = sKMItemEntity.jsonOut();

            SKMCenterRocketMQTrans.SendMessage(transMessage, tag);
        }

        /// <summary>
        /// 商品上架完成後，進行訊息回應
        /// </summary>
        /// <param name="sKMChannelOnSaleMessageEntity"></param>
        public static void SKMItemOnSelfComplete(SKMChannelOnSaleMessageEntity sKMChannelOnSaleMessageEntity)
        {
            string tag = "SKM_ITEM_ON_SHELF_APP_COMPLETE";

            string transMessage = sKMChannelOnSaleMessageEntity.jsonOut();

            SKMCenterRocketMQTrans.SendMessage(transMessage, tag);
        }

        /// <summary>
        /// 測試用，當類別建立時使用
        /// </summary>
        /// <param name="skmCenterRocketMQCategoryEntity"></param>
        public static void SKMCenterCategoryModify(SKMCenterRocketMQCategoryEntity skmCenterRocketMQCategoryEntity )
        {
            string tag = "SKM_SHOP_CATEGORY_UPDATE_APP";

            string transMessage = skmCenterRocketMQCategoryEntity.jsonOut();

            SKMCenterRocketMQTrans.SendMessage(transMessage, tag);
        }

        /// <summary>
        /// 上架完成訊息
        /// </summary>
        /// <param name="itemId"></param>
        public static void SKMItemCreateComplete(long itemId)
        {
            SKMItemEntity sKMItemEntity = new SKMItemEntity();

            sKMItemEntity.itemId = itemId;

            MQSend(sKMItemEntity, "SKM_ITEM_CREATE_APP_COMPLETE");
        }

        /// <summary>
        /// 下架完成訊息
        /// </summary>
        /// <param name="itemId"></param>
        public static void SKMItemOffSelfComplete(long itemId)
        {
            SKMItemEntity sKMItemEntity = new SKMItemEntity();

            sKMItemEntity.itemId = itemId;

            MQSend(sKMItemEntity, "SKM_ITEM_OFF_SHELF_APP_COMPLETE");
        }

        /// <summary>
        /// 庫存佔用, 購買
        /// </summary>
        /// <param name="inventoryChangeEntities"></param>
        public static void InventoryOccupy(List<InventoryChangeEntity> inventoryChangeEntities)
        {
            InventoriesMessage inventoriesMessage = new InventoriesMessage();

            InventoryChangeEntity[] inventoryChangeEntitiesArray = inventoryChangeEntities.ToArray();

            inventoriesMessage.paramList = inventoryChangeEntitiesArray;

            //MQSendWithKey(inventoriesMessage, "INVENTORY_OCCUPY", key);
            MQSend(inventoriesMessage, "INVENTORY_OCCUPY");
        }

        /// <summary>
        /// 庫存確認 , 用不到?!
        /// </summary>
        /// <param name="inventoryChangeEntities"></param>
        public static void InventoryConfirm(List<InventoryChangeEntity> inventoryChangeEntities)
        {
            InventoriesMessage inventoriesMessage = new InventoriesMessage();

            InventoryChangeEntity[] inventoryChangeEntitiesArray = inventoryChangeEntities.ToArray();

            inventoriesMessage.paramList = inventoryChangeEntitiesArray;

            //MQSendWithKey(inventoriesMessage, "INVENTORY_OCCUPY", key);
            MQSend(inventoriesMessage, "INVENTORY_CONFIRM");
        }

        /// <summary>
        /// 庫存佔用確認(同時), 核銷
        /// </summary>
        /// <param name="inventoryChangeEntities"></param>
        public static void InventoryOccupyConfirm(List<InventoryChangeEntity> inventoryChangeEntities)
        {
            InventoriesMessage inventoriesMessage = new InventoriesMessage();

            InventoryChangeEntity[] inventoryChangeEntitiesArray = inventoryChangeEntities.ToArray();

            inventoriesMessage.paramList = inventoryChangeEntitiesArray;

            //MQSendWithKey(inventoriesMessage, "INVENTORY_OCCUPY", key);
            MQSend(inventoriesMessage, "INVENTORY_OCCUPY_CONFIRM");
        }

        /// <summary>
        ///庫存取消, 取消核銷/下架
        /// </summary>
        /// <param name="inventoryChangeEntities"></param>
        public static void InventoryCancel(List<InventoryChangeEntity> inventoryChangeEntities)
        {
            InventoriesMessage inventoriesMessage = new InventoriesMessage();

            InventoryChangeEntity[] inventoryChangeEntitiesArray = inventoryChangeEntities.ToArray();

            inventoriesMessage.paramList = inventoryChangeEntitiesArray;

            //MQSendWithKey(inventoriesMessage, "INVENTORY_OCCUPY", key);
            MQSend(inventoriesMessage, "INVENTORY_CANCEL");
        }

        /// <summary>
        /// 庫存增量調整, 建檔上架 / 退貨
        /// </summary>
        /// <param name="inventoryChangeEntities"></param>
        public static void InventoryAddAdjust(List<InventoryChangeEntity> inventoryChangeEntities)
        {
            InventoriesMessage inventoriesMessage = new InventoriesMessage();

            InventoryChangeEntity[] inventoryChangeEntitiesArray = inventoryChangeEntities.ToArray();

            inventoriesMessage.paramList = inventoryChangeEntitiesArray;

            //MQSendWithKey(inventoriesMessage, "INVENTORY_OCCUPY", key);
            MQSend(inventoriesMessage, "INVENTORY_ADD_ADJUST");
        }

        private static void SendMessage(string msgBody, String tag = "RegisterLog")
        {
            var config = ProviderFactory.Instance().GetConfig();
            string Ons_Topic = config.SKMCenterRocketMQTopic;

            Message msg = new Message(Ons_Topic, tag, msgBody);
            msg.setKey(Guid.NewGuid().ToString());
            try
            {
                SendResultONS sendResult = _producer.send(msg);
                Console.WriteLine("send success {0}", sendResult.getMessageId());
            }
            catch (Exception ex)
            {
                Console.WriteLine("send failure{0}", ex.ToString());
            }
        }

        private static void SendOrderMessage(string msgBody, String tag = "RegisterLog", String key = "test")
        {
            var config = ProviderFactory.Instance().GetConfig();
            string Ons_Topic = config.SKMCenterRocketMQTopic;

            Message msg = new Message(Ons_Topic, tag, msgBody);
            msg.setKey(Guid.NewGuid().ToString());
            try
            {
                SendResultONS sendResult = _orderproducer.send(msg, key);
                Console.WriteLine("send success {0}", sendResult.getMessageId());
            }
            catch (Exception ex)
            {
                Console.WriteLine("send failure{0}", ex.ToString());
            }
        }

        public static void StartPushConsumer()
        {
            var config = ProviderFactory.Instance().GetConfig();
            string Ons_Topic = config.SKMCenterRocketMQTopic;

            _listen = new SKMMsgListener();
            _consumer.subscribe(Ons_Topic, "*", _listen);
            _consumer.start();
        }

        public static void StartOrderConsumer()
        {
            var config = ProviderFactory.Instance().GetConfig();
            string Ons_Topic = config.SKMCenterRocketMQTopic;

            _order_listen = new SKMMsgOrderListener();
            _orderconsumer.subscribe(Ons_Topic, "*", _order_listen);
            _orderconsumer.start();
        }

        public static void shutdownPushConsumer()
        {
            _consumer.shutdown();
        }

        public static void shutdownOrderConsumer()
        {
            _orderconsumer.shutdown();
        }

        public static void StartProducer()
        {
            _producer.start();
        }

        public static void ShutdownProducer()
        {
            _producer.shutdown();
        }


        public static void StartOrderProducer()
        {
            _orderproducer.start();
        }

        public static void ShutdownOrderProducer()
        {
            _orderproducer.shutdown();
        }

        private static ONSFactoryProperty getFactoryProperty()
        {
            var config = ProviderFactory.Instance().GetConfig();

            string Ons_AccessKey = config.SKMCenterRocketMQAccessKey;
            string Ons_SecretKey = config.SKMCenterRocketMQSecretKey;
            string Ons_GroupId = config.SKMCenterRocketMQGroupId;
            string Ons_Topic = config.SKMCenterRocketMQTopic;
            string Ons_NameSrv = config.SKMCenterRocketMQNameSrv;

            ONSFactoryProperty factoryInfo = new ONSFactoryProperty();
            factoryInfo.setFactoryProperty(ONSFactoryProperty.AccessKey, Ons_AccessKey);
            factoryInfo.setFactoryProperty(ONSFactoryProperty.SecretKey, Ons_SecretKey);
            factoryInfo.setFactoryProperty(ONSFactoryProperty.ConsumerId, Ons_GroupId);
            factoryInfo.setFactoryProperty(ONSFactoryProperty.ProducerId, Ons_GroupId);
            factoryInfo.setFactoryProperty(ONSFactoryProperty.PublishTopics, Ons_Topic);
            factoryInfo.setFactoryProperty(ONSFactoryProperty.NAMESRV_ADDR, Ons_NameSrv);
            factoryInfo.setFactoryProperty(ONSFactoryProperty.LogPath, "C://log//web");
            return factoryInfo;
        }


        public static void Start()
        {
            _producer = ONSFactory.getInstance().createProducer(getFactoryProperty());
            _producer.start();
        }

        public static void CreatePushConsumer()
        {

            _consumer = ONSFactory.getInstance().createPushConsumer(getFactoryProperty());
        }

        public static void CreateProducer()
        {

            _producer = ONSFactory.getInstance().createProducer(getFactoryProperty());
        }


        public static void CreateOrderConsumer()
        {

            _orderconsumer = ONSFactory.getInstance().createOrderConsumer(getFactoryProperty());
        }

        public static void CreateOrderProducer()
        {

            _orderproducer = ONSFactory.getInstance().createOrderProducer(getFactoryProperty());
        }



        #region 阿里巴巴
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        public enum ordeSatus {
            occupy=1,
            cancel=2,
            confirm=3,
            adjust=4
        }

        /// <summary>
        /// 呼叫阿里巴巴庫存中心
        /// </summary>
        /// <param name="trustId"></param>
        /// <param name="satus"></param>
        public static void AlibabaMQTrans(Guid OrderId, ordeSatus satus)
        {

            var orderInfo = op.SKmOrderGetForAlibaba(OrderId);
            if (orderInfo != null)
            {
                List<BizLogic.Component.RocketMQEntity.InventoryChangeEntity> list = new List<BizLogic.Component.RocketMQEntity.InventoryChangeEntity>();

                foreach (var row in orderInfo.AsEnumerable())
                {
                    list.Add(new BizLogic.Component.RocketMQEntity.InventoryChangeEntity()
                    {
                        bizSrcId = (row["bizSrcId"] == DBNull.Value) ? "" : (string)row["bizSrcId"],
                        bizSrcSubId = null,
                        bizSrcType = (row["bizSrcType"] == DBNull.Value) ? 0 : (int)row["bizSrcType"],
                        code = (row["code"] == DBNull.Value) ? "" : (string)row["code"],
                        entityId = (row["entityId"] == DBNull.Value) ? "" : (string)row["entityId"],
                        entityType = (row["entityType"] == DBNull.Value) ? 0 : (int)row["entityType"],
                        quantity = (row["quantity"] == DBNull.Value) ? 0 : (int)row["quantity"],
                        warehouseCode = (row["warehouseCode"] == DBNull.Value) ? "" : (string)row["warehouseCode"],
                        warehouseType = (row["warehouseType"] == DBNull.Value) ? 0 : (int)row["warehouseType"],
                    });
                }
                BizLogic.Component.RocketMQTrans.SKMCenterRocketMQTrans MQTrans = new BizLogic.Component.RocketMQTrans.SKMCenterRocketMQTrans();
                switch (satus)
                {
                    case ordeSatus.occupy:
                        InventoryOccupy(list.Where(o => !string.IsNullOrEmpty(o.entityId)).ToList());
                        break;
                    case ordeSatus.cancel:
                       InventoryCancel(list.Where(o => !string.IsNullOrEmpty(o.entityId)).ToList());
                        break;
                    case ordeSatus.confirm:
                       InventoryConfirm(list.Where(o => !string.IsNullOrEmpty(o.entityId)).ToList());
                        break;
                    case ordeSatus.adjust:
                       InventoryAddAdjust(list.Where(o => !string.IsNullOrEmpty(o.entityId)).ToList());
                        break;
                }

            }
            else
            {
                throw new ApplicationException("OrderId can't find! trustId=" + OrderId);
            }


        }


#endregion
    }
}

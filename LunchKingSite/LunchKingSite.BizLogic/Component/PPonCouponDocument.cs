﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using NPOI.HPSF;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// 注意:
    ///     請先行確認，登入者有無讀取該憑證的權限
    ///
    /// </summary>
    public class PponCouponDocument : CouponDocumentBase
    {
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public string FirstCode { get; set; }

        public string LastCode { get; set; }

        public string SecurityCode { get; set; }

        public string StoreSequence { get; set; }

        public string ItemName { get; set; }

        public string EventName { get; set; }

        public string MemberName { get; set; }

        public DateTime UsageStartDate { get; set; }

        public DateTime UsageEndDate { get; set; }

        public DateTime? ChangedExpireDate { get; set; }

        public string CouponInfo { get; set; }

        public Guid BusinessHourGuid { get; set; }

        public IList<Seller> StoreInfos { get; set; }

        public string FullCode
        {
            get;
            set;
        }

        public bool IsFami { get; set; }

        public bool IsPEZevent { get; set; }

        public bool IsPponCouponFormat { get; set; }

        public List<int> LabelTags { get; set; }

        public bool IsZeroActivityShowCoupon { get; set; }

        /// <summary>
        /// 是否限制使用分店 通用券檔次為false
        /// </summary>
        public bool IsRestrictedStore { get; set; }

        public bool IsGroupCoupon { get; set; }
        public bool IsEntrepotOrder { get; set; }

        public PponCouponDocument(ViewPponCoupon coupon, IViewPponDeal vpd, bool entrepotOrder = false)
        {
            if (coupon.CouponStoreSequence != null)
            {
                this.StoreSequence = coupon.CouponStoreSequence.Value.ToString("0000");
            }
            this.ItemName = Helper.IsFlagSet(coupon.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? PponFacade.GetDealPdfItemName(coupon.BusinessHourGuid) : OrderFacade.GetOrderDetailItemName(
                OrderDetailItemModel.Create(coupon), new PponItemNameShowOption { ShowPhone = true, StoreInfoInParenthesis = true });
            this.EventName = Helper.IsFlagSet(coupon.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? string.Empty : coupon.EventName;
            this.MemberName = coupon.MemberName;
            this.UsageStartDate = coupon.BusinessHourDeliverTimeS.Value;
            this.UsageEndDate = coupon.BusinessHourDeliverTimeE.Value;
            this.IsEntrepotOrder = entrepotOrder;
            this.ChangedExpireDate = coupon.CouponChangedExpiredDate;
            if (vpd.MainBid != null)
            {
                var mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vpd.MainBid.Value);
                var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(mainDeal);
                this.CouponInfo = contentLite.Introduction;
            } else
            {
                this.CouponInfo =  coupon.Introduction;
            }
            

            this.StoreInfos = new List<Seller>();
            ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            if (coupon.StoreGuid == null)
            {
                this.StoreInfos = SellerFacade.GetPponStoreSellersByBId(coupon.BusinessHourGuid, VbsRightFlag.VerifyShop).ToList();
            }
            else
            {
                var store = sp.SellerGet(coupon.StoreGuid.Value);
                this.StoreInfos.Add(store);
            }
            this.BusinessHourGuid = coupon.BusinessHourGuid;

            //全家檔次
            if ((coupon.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)
            {
                IsFami = true;
                IsPponCouponFormat = false;
                //憑證碼和驗證碼會相同
                this.FirstCode = coupon.SequenceNumber;
                this.LastCode = string.Empty;
                this.SecurityCode = coupon.CouponCode;
                FullCode = coupon.CouponCode;
                IsGroupCoupon = false;
            }
            else if ((coupon.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
            {
                IsFami = false;
                if (Regex.IsMatch(coupon.CouponCode, "[0-9]{4}-[0-9]{4}-[0-9]{6}"))
                {
                    IsPponCouponFormat = true;
                    this.FirstCode = coupon.CouponCode.Substring(0, 4);
                    this.LastCode = coupon.CouponCode.Substring(5, 4);
                    this.SecurityCode = coupon.CouponCode.Substring(10, 6);
                    FullCode = string.Format("{0}-{1}-{2}", FirstCode, LastCode, SecurityCode);
                }
                else
                {
                    IsPponCouponFormat = false;
                    this.FirstCode = coupon.SequenceNumber;
                    this.LastCode = string.Empty;
                    this.SecurityCode = coupon.CouponCode;
                    FullCode = coupon.CouponCode;
                }
                IsGroupCoupon = false;
            }
            else
            {
                IsFami = false;
                IsPponCouponFormat = true;
                string[] parts = coupon.SequenceNumber.Split('-');
                this.FirstCode = parts[0];
                this.LastCode = parts[1];
                this.SecurityCode = coupon.CouponCode;
                FullCode = string.Format("{0}-{1}-{2}", FirstCode, LastCode, SecurityCode);
                IsGroupCoupon = Helper.IsFlagSet(coupon.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon);
            }
            IsPEZevent = (coupon.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0;
            PrepareImages(FullCode, (CouponCodeType)vpd.CouponCodeType, IsEntrepotOrder);

            if (string.IsNullOrEmpty(vpd.LabelTagList) == false)
            {
                LabelTags = vpd.LabelTagList.Split(',').Select(int.Parse).ToList();
            }
            if (vpd.IsZeroActivityShowCoupon.GetValueOrDefault() && coupon.ItemUnitPrice == 0)
            {
                IsZeroActivityShowCoupon = true;
            }

            this.IsRestrictedStore = !Helper.IsFlagSet(coupon.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
        }

        public PponCouponDocument(string sequenceNumber, string couponCode)
        {
        }

        protected override void RenderPDFContent()
        {
            RenderdHeaderWithSerial(this.StoreSequence);
            RenderLogoAndBarcode();
            RenderCouponNumberAndCode(this.FirstCode, this.LastCode, this.SecurityCode);
            RenderMemberrName(this.MemberName);
            if (!IsZeroActivityShowCoupon)
            {
                RenderUsageDate(this.UsageStartDate, this.UsageEndDate, this.ChangedExpireDate);
            }
            RenderItemName(FixItemName(this.ItemName));
            RenderEventName(this.EventName);
            if (IsZeroActivityShowCoupon)
            {
                RenderUsageDate(this.UsageStartDate, this.UsageEndDate, this.ChangedExpireDate);
            }
            RenderDealTags(this.LabelTags);
            RenderCouponInfo(this.CouponInfo);
            if (IsRestrictedStore)
            { 
                RenderStoreInfo(this.StoreInfos, this.BusinessHourGuid);
            }
            Render17ServiceInfo();
        }

        private void RenderdHeaderWithSerial(string storeSequence)
        {
            PdfPTable table = new PdfPTable(1);
            table.DefaultCell.Border = 0;
            table.WidthPercentage = 95;

            Phrase ph = null;
            if (string.IsNullOrEmpty(storeSequence))
            {
                ph = new Phrase();
                ph.Add(new Phrase(" "));
            }
            else
            {
                ph = new Phrase();
                ph.Add(new Chunk("序號(僅排序用): ", Fonts.Mingliu8));
                ph.Add(new Chunk(storeSequence, Fonts.Arial8));
            }

            PdfPCell cell = new PdfPCell(ph);
            cell.Border = 0;
            cell.BorderWidthBottom = _BREAKLINE_BORDER_WIDTH;
            cell.BorderColorBottom = _BREAKLINE_BORDER_COLOR;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.PaddingBottom = 5;

            table.AddCell(cell);
            doc.Add(table);
        }

        private void RenderLogoAndBarcode()
        {
            //增加上下間的空白間距
            var p = new Paragraph();
            p.SpacingBefore = 2;
            p.SpacingAfter = 3;
            doc.Add(p);

            PdfPTable table = new PdfPTable(3);
            table.SetWidths(new int[] { 175, 345, 75 });
            table.DefaultCell.Border = 0;
            table.WidthPercentage = 95;

            table.AddCell(
                new PdfPCell(pdfImgLogo)
                {
                    VerticalAlignment = Element.ALIGN_LEFT,
                    PaddingTop = 5,
                    PaddingLeft = 7,
                    PaddingRight = 7,
                    Rowspan = 2,
                    Border = 0,
                    BorderWidthRight = _BREAKLINE_BORDER_WIDTH,
                    BorderColorRight = _BREAKLINE_BORDER_COLOR
                });

            Phrase phBarcode = new Phrase();
            phBarcode.Add(new Chunk("條碼", Fonts.Mingliu7Bold));
            phBarcode.Add(new Chunk(" & QR Code", Fonts.Arial7Bold));
            table.AddCell(new PdfPCell(phBarcode)
            {
                PaddingLeft = 10,
                PaddingTop = 10,
                Border = 0
            });

            table.AddCell(
                new PdfPCell(pdfImgQrCode)
                {
                    PaddingTop = 5,
                    Rowspan = 2,
                    Border = 0
                }
            );

            table.AddCell(
                new PdfPCell(pdfImgBarcode)
                {
                    PaddingLeft = 25,
                    PaddingTop = 8,
                    Rowspan = 2,
                    Border = 0
                }
             );

            doc.Add(table);
        }

        private void RenderCouponNumberAndCode(string firstCode, string lastCode, string securityCode)
        {
            //增加上下間的空白間距
            var p = new Paragraph();
            p.SpacingBefore = 2;
            p.SpacingAfter = 3;
            doc.Add(p);

            PdfPTable table = new PdfPTable(2);
            table.SetWidths(new int[] { 330, 265 });
            table.WidthPercentage = 95;

            Paragraph para1 = new Paragraph();
            string title = string.Empty;
            string description = string.Empty;
            string description2 = string.Empty;
            if (IsFami)
            {
                title = "全家FamiPort商品兌換PIN碼：";
                description = "※ 請於兌換期限內至全家便利商店門市使用FamiPort依照步驟輸入PIN碼並取得單據後，" +
                    "至店內拿取欲兌換之商品，持繳款單據至櫃台完成結帳動作，即可以優惠價帶走您所購買的商品(恕不提供網路付款)";
            }
            else if (IsPEZevent)
            {
                title = "活動兌換序號：";
            }
            else
            {
                title = "憑證編號：";
                description2 = "※ 每張憑證可換乙份優惠商品或折抵金";
                description = "※ 如非確認預約或使用，請勿隨意提供確認碼予他人，避免影響您的使用權益。";
            }
            para1.Add(new Phrase(title, Fonts.Mingliu12));
            para1.Add(new Phrase(firstCode + (string.IsNullOrEmpty(lastCode) ? string.Empty : ("-" + lastCode)), Fonts.Arial13Bold));
            PdfPCell p1 = new PdfPCell(para1)
            {
                PaddingTop = 3,
                PaddingBottom = 5,
                PaddingLeft = 30,
                BorderColor = _BREAKLINE_BORDER_COLOR,
                BorderWidth = _BREAKLINE_BORDER_WIDTH,
                BorderWidthRight = 0F,
                BorderWidthBottom = 0F
            };

            if (!IsPponCouponFormat)
            {
                p1.Colspan = 2;
            }
            table.AddCell(p1);

            if (IsPponCouponFormat)
            {
                Paragraph para2 = new Paragraph();
                para2.Add(new Phrase("確認碼：", Fonts.Mingliu12));
                para2.Add(new Phrase(securityCode, Fonts.Arial13Bold));
                PdfPCell cell2 = new PdfPCell(para2)
                {
                    PaddingLeft = 25,
                    PaddingTop = 3,
                    PaddingBottom = 5,
                    BorderColor = _BREAKLINE_BORDER_COLOR,
                    BorderWidth = _BREAKLINE_BORDER_WIDTH,
                    BorderWidthLeft = 0F,
                    BorderWidthBottom = 0F
                };
                table.AddCell(cell2);
            }

            Paragraph para4 = new Paragraph();
            para4.Add(new Phrase(description2, Fonts.Mingliu8Bold));
            PdfPCell cell4 = new PdfPCell(para4)
            {
                PaddingLeft = 10,
                PaddingTop = 3,
                PaddingBottom = 5,
                Colspan = 2,
                BorderColor = _BREAKLINE_BORDER_COLOR,
                BorderWidth = _BREAKLINE_BORDER_WIDTH,
                BorderWidthTop = 0F,
                BorderWidthBottom = 0F
            };
            table.AddCell(cell4);

            Paragraph para3 = new Paragraph();
            para3.Add(new Phrase(description, Fonts.Mingliu8));
            PdfPCell cell3 = new PdfPCell(para3)
            {
                PaddingLeft = 10,
                PaddingTop = 3,
                PaddingBottom = 5,
                Colspan = 2,
                BorderColor = _BREAKLINE_BORDER_COLOR,
                BorderWidth = _BREAKLINE_BORDER_WIDTH,
                BorderWidthTop = 0F
            };
            table.AddCell(cell3);

            doc.Add(table);
        }

        private void RenderItemName(string itemName)
        {
            var p = new Paragraph { Leading = 25, IndentationLeft = 10, Alignment = Element.ALIGN_LEFT };
            p.Add(new Phrase(IsZeroActivityShowCoupon ? string.Format("感謝您參與【{0}】活動！", itemName) : itemName, Fonts.Mingliu10));
            doc.Add(p);
        }

        private void RenderEventName(string eventName)
        {
            var p = new Paragraph();
            p.Leading = 15;
            p.IndentationLeft = 10;
            p.IndentationRight = 10;
            p.Alignment = Element.ALIGN_LEFT;
            p.Add(new Phrase(eventName, Fonts.Mingliu10Bold));
            doc.Add(p);
        }
        private void RenderDealTags(List<int> systemCodes)
        {
            if (systemCodes != null && systemCodes.Any())
            {
                List<string> tagName = new List<string>();
                foreach (int codeId in systemCodes)
                {
                    // tagName.Add(SystemCodeManager.GetDealLabelCodeName(codeId));
                }

                var p = new Paragraph();
                p.Leading = 30;
                p.IndentationLeft = 10;
                p.IndentationRight = 10;
                p.Alignment = Element.ALIGN_LEFT;
                p.Add(new Phrase(string.Join("、", tagName), Fonts.Mingliu10Bold));
                doc.Add(p);
            }
        }
        private void RenderMemberrName(string memberName)
        {
            var p = new Paragraph();
            p.Leading = 20;
            p.IndentationLeft = 10;
            p.Alignment = Element.ALIGN_LEFT;
            p.Add(IsZeroActivityShowCoupon
                ? new Phrase(string.Format("親愛的【{0}】您好：", memberName), Fonts.Mingliu10)
                : new Phrase("訂購人：" + memberName, Fonts.Mingliu10));
            doc.Add(p);
        }

        private void RenderUsageDate(DateTime usageStartDate, DateTime usageEndDate, DateTime? changedExpireDate)
        {
            var p = new Paragraph();
            p.Leading = 20;
            p.IndentationLeft = 10;
            p.Alignment = Element.ALIGN_LEFT;
            //p.Add(new Chunk(IsZeroActivityShowCoupon ? "使用期限：" : "優惠有效期間：", Fonts.Mingliu10));
            //p.Add(new Chunk(usageStartDate.ToShortDateString() + " ~ ", Fonts.Arial10));

            if (changedExpireDate != null)
            {
                p.Add(new Chunk(usageEndDate.ToShortDateString(), Fonts.Arial10Strikethru));
                p.Add(new Chunk(IsZeroActivityShowCoupon ? "\n使用期限更改至：" : "\n有效期間更改至：", Fonts.Mingliu10));
                p.Add(new Chunk(changedExpireDate.Value.ToShortDateString(), Fonts.Arial10));
            }
            else
            {
                //p.Add(new Chunk(usageEndDate.ToShortDateString(), Fonts.Arial10));
            }

            doc.Add(p);
        }

        private void RenderCouponInfo(string couponInfo)
        {
            var pTitle = new Paragraph("好康注意事項：", Fonts.Mingliu9);
            pTitle.Leading = 30;
            pTitle.IndentationLeft = 10;
            pTitle.Alignment = Element.ALIGN_LEFT;
            doc.Add(pTitle);

            StringBuilder sb = new StringBuilder();
            sb.Append(couponInfo);
            if (couponInfo.EndsWith("</div>") == false)
            {
                sb.Append("<br />");
            }
            sb.Append("●此憑證由等值購物金兌換之");
            sb.Append("<br />●信託期間：發售日起一年內");
            sb.Append("<br />●本商品(服務)禮券所收取之金額，已存入發行人於台新銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用");
            sb.Append("<br />●本憑證不得與其他優惠合併使用，且恕無法兌換現金及找零");
            sb.Append("<br />●好康憑證不限本人使用，使用時店家將會核對憑證編號，一組編號限用一次");
            if (IsGroupCoupon)
            {
                sb.Append("<br />●成套票券商品為整套/組購入，故僅限於所有套/組憑證均兌換完畢才能享有完整優惠");
                sb.Append("<br />●當有退貨情事發生時，將依其整套/組進貨規則進行剩餘面額價值進行退款");
            }

            StringReader sr = new StringReader(sb.ToString());
            List<IElement> elems = HTMLWorker.ParseToList(sr, null);
            foreach (var elem in elems)
            {
                var paragraph = elem as Paragraph;
                if (paragraph != null)
                {
                    paragraph.Leading = 15;
                }

                foreach (var child in elem.Chunks)
                {
                    child.Font = Fonts.Mingliu9;
                }
            }

            var p = new Paragraph();
            p.Leading = 30;
            p.IndentationLeft = 20;
            p.Alignment = Element.ALIGN_LEFT;
            p.AddRange(elems);
            doc.Add(p);
        }

        private void RenderStoreInfo(IList<Seller> stores, Guid businessHourGuid)
        {
            var pTitle = new Paragraph("商店資訊：", Fonts.Mingliu9);
            pTitle.Leading = 30;
            pTitle.IndentationLeft = 10;
            pTitle.Alignment = Element.ALIGN_LEFT;
            doc.Add(pTitle);

            StringBuilder sb = new StringBuilder();
            sb.Append("<br />");
            foreach (var storeInfo in stores)
            {
                ViewPponStore pponStore = ViewPponStoreManager.DefaultManager.ViewPponStoreGet(businessHourGuid, storeInfo.Guid);
                sb.Append("<h4>");
                sb.Append(storeInfo.SellerName);
                sb.Append("</h4>");
                sb.Append("<ul>");
                GetStoreDetailsAsHtml(sb, storeInfo, pponStore);
                sb.Append("</ul>");
            }
            StringReader sr = new StringReader(sb.ToString());
            List<IElement> elems = HTMLWorker.ParseToList(sr, null);
            foreach (var elem in elems)
            {
                var paragraph = elem as Paragraph;
                if (paragraph != null)
                {
                    paragraph.Leading = 15;
                }
                var elemList = elem as List;
                if (elemList != null)
                {
                    foreach (var listitem in elemList.Items)
                    {
                        var elemParagraph = listitem as Paragraph;
                        if (elemParagraph != null)
                        {
                            elemParagraph.Leading = 15;
                        }
                    }
                }

                foreach (var child in elem.Chunks)
                {
                    child.Font = Fonts.Mingliu9;
                }
            }

            var p = new Paragraph();
            p.IndentationLeft = 20;
            p.Alignment = Element.ALIGN_LEFT;
            p.AddRange(elems);
            doc.Add(p);
        }

        public static string GetStoreDetailsAsHtml(Seller store, ViewPponStore pponStore)
        {
            StringBuilder sb = new StringBuilder();
            GetStoreDetailsAsHtml(sb, store, pponStore);
            return sb.ToString();
        }
        private static void GetStoreDetailsAsHtml(StringBuilder sb, Seller store, ViewPponStore pponStore)
        {
            if (string.IsNullOrEmpty(store.StoreTel) == false)
            {
                sb.Append("<li>");
                sb.Append(I18N.Phrase.Phone + ": " + store.StoreTel);
                sb.Append("</li>");
            }

            if (string.IsNullOrEmpty(store.StoreAddress) == false)
            {
                sb.Append("<li>");
                int townshipId = store.StoreTownshipId == null ? 0 : store.StoreTownshipId.Value;
                sb.Append(I18N.Phrase.Address + ":" + CityManager.CityTownShopStringGet(townshipId) +
                          store.StoreAddress);
                sb.Append("</li>");
            }

            if (pponStore.IsLoaded && !string.IsNullOrWhiteSpace(pponStore.UseTime))
            {
                sb.Append("<li>");
                sb.Append(I18N.Phrase.UseTime + I18N.Phrase.Colon + "<br/>" + pponStore.UseTime);
                sb.Append("</li>");
            }
            else if (string.IsNullOrEmpty(store.OpenTime) == false)
            {
                sb.Append("<li>");
                sb.Append(I18N.Phrase.OpeningTime + I18N.Phrase.Colon + store.OpenTime);
                sb.Append("</li>");
            }

            if (string.IsNullOrEmpty(store.CloseDate) == false)
            {
                sb.Append("<li>");
                sb.Append(I18N.Phrase.CloseDate + I18N.Phrase.Colon + store.CloseDate);
                sb.Append("</li>");
            }

            if (string.IsNullOrEmpty(store.Mrt) == false || string.IsNullOrEmpty(store.Car) == false ||
                string.IsNullOrEmpty(store.Bus) == false || string.IsNullOrEmpty(store.OtherVehicles) == false)
            {
                sb.Append(I18N.Phrase.Vehicles + "<br/>");
                if (string.IsNullOrEmpty(store.Mrt) == false)
                {
                    sb.Append("<li>");
                    sb.Append(I18N.Phrase.Mrt + I18N.Phrase.Colon + store.Mrt);
                    sb.Append("</li>");
                }

                if (string.IsNullOrEmpty(store.Car) == false)
                {
                    sb.Append("<li>");
                    sb.Append(I18N.Phrase.Car + I18N.Phrase.Colon + store.Car);
                    sb.Append("</li>");
                }

                if (string.IsNullOrEmpty(store.Bus) == false)
                {
                    sb.Append("<li>");
                    sb.Append(I18N.Phrase.Bus + I18N.Phrase.Colon + store.Bus);
                    sb.Append("</li>");
                }

                if (string.IsNullOrEmpty(store.OtherVehicles) == false)
                {
                    sb.Append("<li>");
                    sb.Append(I18N.Phrase.OtherVehicle + I18N.Phrase.Colon + store.OtherVehicles);
                    sb.Append("</li>");
                }
            }
        }

        private void Render17ServiceInfo()
        {
            var pTitle = new Paragraph(string.Format("{0}{1} (平日 09:00 ~ 18:00)", IsEntrepotOrder ? "康太數位客服專線：" : "17Life客服專線："
                , config.ServiceTel), Fonts.Mingliu9);
            pTitle.Leading = 30;
            pTitle.IndentationLeft = 10;
            pTitle.Alignment = Element.ALIGN_LEFT;
            doc.Add(pTitle);
        }

        /// <summary>
        /// 如果要表達的是空白，那應該是&nbsp;，但這邊的卻就是 &nbsp(沒分號) ，
        /// 這錯的起因是 OderFacade 那邊塞ShoppingCart塞錯值了，這邊先將錯就錯。
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string FixItemName(string str)
        {
            return str.Replace("&nbsp", " ");
        }
    }

}
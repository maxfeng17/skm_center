﻿using System;
using System.Web;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;

namespace LunchKingSite.WebLib.Component.MemberActions
{
    public class Life17MemberRegister : MemberRegisterBase
    {
        public Life17MemberRegister(string userName, string password, int cityId) :
            base(userName, password, cityId)
        {
        }

        protected override void AfterProcessData(Member mem, MemberAuthInfo mai)
        {
            // sent account confirm mail
            MemberFacade.SendAccountConfirmMail(mem.UserEmail
                , mem.UniqueId.ToString(), mai.AuthKey, mai.AuthCode);
        }

        protected override MemberRegisterReplyType CreateMemberLink()
        {
            try
            {
                MemberFacade.AddMemberLink(
                    _userId, SingleSignOnSource.ContactDigitalIntegration, _userId.ToString(), _userName);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("CreateMemberLink fail. org=({0},exid={1},userid={2}), ex: {3}",
                    SingleSignOnSource.ContactDigitalIntegration, _userId, _userId, ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override MemberRegisterReplyType CreateMemberAuthInfo()
        {
            string lastUrl = "";
            if (HttpContext.Current != null && HttpContext.Current.Session != null &&
                HttpContext.Current.Session[LkSiteSession.NowUrl.ToString()] != null)
            {
                lastUrl = HttpContext.Current.Session[LkSiteSession.NowUrl.ToString()].ToString();
            }
            try
            {
                Member mem = mp.MemberGet(_userName);
                // create member auth info
                string[] authPair = NewMemberUtilityCore.GenEmailAuthCode();
                MemberAuthInfo mai = new MemberAuthInfo
                {
                    UniqueId = mem.UniqueId,
                    Email = _userName,
                    AuthCode = authPair[0],
                    AuthKey = authPair[1],
                    AuthDate = DateTime.Now,
                    LastUrl = lastUrl
                };

                mp.MemberAuthInfoSet(mai);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override void AuditReigsteSuccess()
        {
            MemberFacade.LogAccountAudit(_userId, _userName, AccountAuditAction.LoginUsing17LifeAccount, true, "17Life 建立帳號");
        }

        public override SingleSignOnSource RegisterSourceType
        {
            get { return SingleSignOnSource.ContactDigitalIntegration; }
        }

        protected override MemberRegisterReplyType CheckData()
        {
            if (RegExRules.CheckPassword(this._password) == false)
            {
                return MemberRegisterReplyType.PasswordFormatError;
            }
            return base.CheckData();
        }
    }
}
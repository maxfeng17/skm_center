﻿using System;
using System.Web;
using System.Web.Security;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public class LineBindingMember : IBindingMember
    {
        private readonly string externalId;

        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        protected static ILog logger = LogManager.GetLogger("LineBindingMember");

        public LineBindingMember(string externalId)
        {
            this.externalId = externalId;
        }

        public AccountAvailableStatus GetAccountAvailableStatus()
        {
            MemberLink ml = mp.MemberLinkGetByExternalId(externalId, SingleSignOnSource.Line);
            if (ml.IsLoaded)
            {
                return AccountAvailableStatus.WasUsed;
            }

            return AccountAvailableStatus.Available;
        }

        public override string ToString()
        {
            return "Line";
        }
    }
}

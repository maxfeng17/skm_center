using System;
using System.Web;
using System.Web.Security;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;

namespace LunchKingSite.WebLib.Component.MemberActions
{
    public class FacebookMemberRegister : MemberRegisterBase
    {
        private string _externalUserId;
        private string _userEmail;
        private FacebookUser _fbuser;
        public FacebookMemberRegister(string externalUserId, string userName, string userEmail, int cityId) :
            base(userName, Membership.GeneratePassword(8, 0), cityId)
        {
            if (string.IsNullOrEmpty(externalUserId))
            {
                throw new ArgumentException("未指定從 facebook 的串接 id", "extrenalUserId");
            }
            this._externalUserId = externalUserId;
            this._userEmail = userEmail;
        }

        protected override MemberRegisterReplyType CheckData()
        {
            MemberLink ml = mp.MemberLinkGetByExternalId(GetExternalUserId(), RegisterSourceType);
            if (ml.IsLoaded)
            {
                this.Message = Phrase.RegisterReturnMessageExternalIdExists;
                return MemberRegisterReplyType.ExternalUserIdExists;
            }
            return base.CheckData();
        }

        protected override void BeforeProcessData()
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null)
            {
                return;
            }
            string accessToken = (string)HttpContext.Current.Session[LkSiteSession.AccessToken.ToString()];
            _fbuser = FacebookUser.Get(accessToken);
        }

        protected override void FillMemberProfileEvent(Member mem)
        {
            base.FillMemberProfileEvent(mem);
            if (_fbuser != null)
            {
                mem.FirstName = _fbuser.FirstName;
                mem.LastName = _fbuser.LastName;
                mem.Birthday = _fbuser.Birthday;
                mem.Pic = _fbuser.Pic;
                mem.Gender = _fbuser.Gender;
                mem.UserEmail = this._userEmail;
            }
        }

        protected override MemberRegisterReplyType CreateMemberLink()
        {
            try
            {
                MemberFacade.AddMemberLink(_userId, RegisterSourceType, GetExternalUserId(), _userName);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("CreateMemberLink fail. org=({0},exid={1},userid={2}), ex: {3}",
                    RegisterSourceType, GetExternalUserId(), _userId, ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override void AuditReigsteSuccess()
        {
            MemberFacade.LogAccountAudit(_userId, _userName, AccountAuditAction.LoginUsingFbAccount, true, "經 FB 認證並初次建立 17life 帳號");
        }

        public string GetExternalUserId()
        {
            return _externalUserId;
        }

        public override SingleSignOnSource RegisterSourceType
        {
            get { return SingleSignOnSource.Facebook; }
        }

    }
}
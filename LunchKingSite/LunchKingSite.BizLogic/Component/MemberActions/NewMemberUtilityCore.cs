using System;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public class NewMemberUtilityCore
    {
        /// <summary>
        /// 產生認證信的驗證碼
        /// </summary>
        /// <param name="codeLength"></param>
        /// <param name="keyLength"></param>
        /// <returns></returns>
        public static string[] GenEmailAuthCode(int codeLength = 50, int keyLength = 50)
        {
            int times = (int)Math.Ceiling((double)(codeLength + keyLength) / 32);
            string tempBase = string.Empty;

            for (int x = 0; x < times; x++)
            {
                tempBase += Guid.NewGuid().ToString("N");
            }

            return new string[] { tempBase.Substring(0, codeLength), tempBase.Substring(codeLength, keyLength) };
        }
    }
}
﻿using System;
using System.Web;
using System.Web.Security;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public class FBBindingMemberRegister : MemberRegisterBase
    {
         private string _externalUserId;
        private FacebookUser _FacebookUser;

        public FBBindingMemberRegister(string externalUserId, string userName, string password, int cityId)
            : base(userName, password, cityId)
        {
            if (string.IsNullOrEmpty(externalUserId))
            {
                throw new ArgumentException("未指定從 FaceBook 的串接 id", "extrenalUserId");
            }
            this._externalUserId = externalUserId;
        }
        
        protected override void AfterProcessData(Member mem, MemberAuthInfo mai)
        {
            // sent account confirm mail
            MemberFacade.SendAccountConfirmMail(mem.UserEmail
                , mem.UniqueId.ToString(), mai.AuthKey, mai.AuthCode);
        }

        protected override MemberRegisterReplyType CreateMemberLink()
        {
            try
            {
                //17 life member link
                MemberFacade.AddMemberLink(
                    _userId, SingleSignOnSource.ContactDigitalIntegration, _userId.ToString(), _userName);
                //FB member link
                MemberFacade.AddMemberLink(_userId, RegisterSourceType, GetExternalUserId(), _userName);

                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("CreateMemberLink fail. org=({0},exid={1},userid={2}), ex: {3}",
                    RegisterSourceType, _userId, _userId, ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override MemberRegisterReplyType CreateMemberAuthInfo()
        {
            string lastUrl = "";
            if (HttpContext.Current != null && HttpContext.Current.Session != null &&
                HttpContext.Current.Session[LkSiteSession.NowUrl.ToString()] != null)
            {
                lastUrl = HttpContext.Current.Session[LkSiteSession.NowUrl.ToString()].ToString();
            }
            try
            {
                Member mem = mp.MemberGet(_userName);
                // create member auth info
                string[] authPair = NewMemberUtilityCore.GenEmailAuthCode();
                MemberAuthInfo mai = new MemberAuthInfo
                {
                    UniqueId = mem.UniqueId,
                    Email = _userName,
                    AuthCode = authPair[0],
                    AuthKey = authPair[1],
                    AuthDate = DateTime.Now,
                    LastUrl = lastUrl
                };

                mp.MemberAuthInfoSet(mai);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override void AuditReigsteSuccess()
        {
             MemberFacade.LogAccountAudit(_userId, _userName, AccountAuditAction.LoginUsingFbAccount, true, "經 FB 認證並初次建立 17life 帳號");
        }

        protected override MemberRegisterReplyType CheckData()
        {
            if (RegExRules.CheckPassword(this._password) == false)
            {
                return MemberRegisterReplyType.PasswordFormatError;
            }
            //FB
            MemberLink ml = mp.MemberLinkGetByExternalId(GetExternalUserId(), RegisterSourceType);
            if (ml.IsLoaded)
            {
                this.Message = Phrase.RegisterReturnMessageExternalIdExists;
                return MemberRegisterReplyType.ExternalUserIdExists;
            }
            return base.CheckData();
        }

        public string GetExternalUserId()
        {
            return _externalUserId;
        }

        public override SingleSignOnSource RegisterSourceType
        {
            get { return SingleSignOnSource.Facebook; }
        }

        protected override void BeforeProcessData()
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null)
            {
                return;
            }
            string accessToken = (string)HttpContext.Current.Session[LkSiteSession.AccessToken.ToString()];
            _FacebookUser = FacebookUser.Get(accessToken);
        }

        protected override void FillMemberProfileEvent(Member mem)
        {
            base.FillMemberProfileEvent(mem);
            if (_FacebookUser != null)
            {
                mem.FirstName = _FacebookUser.FirstName;
                mem.LastName = _FacebookUser.LastName;
                mem.Birthday = _FacebookUser.Birthday;
                mem.Pic = _FacebookUser.Pic;
                mem.Gender = _FacebookUser.Gender;
                mem.UserEmail = this.UserName;
            }
        }
    }
}

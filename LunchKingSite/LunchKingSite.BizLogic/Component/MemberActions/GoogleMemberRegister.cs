using System;
using System.Web;
using System.Web.Security;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public class GoogleMemberRegister : MemberRegisterBase
    {
        private string _externalUserId;
        private GoogleUser _guser;
        public GoogleMemberRegister(string externalUserId, string userName, int cityId) :
            base(userName, Membership.GeneratePassword(8, 0), cityId)
        {
            if (string.IsNullOrEmpty(externalUserId))
            {
                throw new ArgumentException("未指定從 google 的串接 id", "extrenalUserId");
            }
            this._externalUserId = externalUserId;
        }

        protected override MemberRegisterReplyType CheckData()
        {
            MemberLink ml = mp.MemberLinkGetByExternalId(GetExternalUserId(), RegisterSourceType);
            if (ml.IsLoaded)
            {
                this.Message = Phrase.RegisterReturnMessageExternalIdExists;
                return MemberRegisterReplyType.ExternalUserIdExists;
            }
            return base.CheckData();
        }

        protected override void BeforeProcessData()
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null)
            {
                return;
            }
            string accessToken = (string)HttpContext.Current.Session[LkSiteSession.AccessToken.ToString()];
            _guser = GoogleUser.Get(accessToken);
        }

        protected override void FillMemberProfileEvent(Member mem)
        {
            base.FillMemberProfileEvent(mem);
            if (_guser != null)
            {
                mem.FirstName = _guser.FirstName;
                mem.LastName = _guser.LastName;
                mem.Birthday = _guser.Birthday;
                mem.Pic = _guser.Pic;
                mem.Gender = _guser.Gender;
            }
        }

        protected override MemberRegisterReplyType CreateMemberLink()
        {
            try
            {
                MemberFacade.AddMemberLink(_userId, RegisterSourceType, GetExternalUserId(), _userName);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("CreateMemberLink fail. org=({0},exid={1},userid={2}), ex: {3}",
                    RegisterSourceType, GetExternalUserId(), _userId, ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override void AuditReigsteSuccess()
        {
            MemberFacade.LogAccountAudit(_userId, _userName, AccountAuditAction.LoginUsingGoogleAccount, true, "經 Google 認證並初次建立 17life 帳號");
        }

        public string GetExternalUserId()
        {
            return _externalUserId;
        }

        public override SingleSignOnSource RegisterSourceType
        {
            get { return SingleSignOnSource.Google; }
        }
    }
}
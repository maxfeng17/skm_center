using System;
using System.Web.Security;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;

namespace LunchKingSite.WebLib.Component.MemberActions
{
    public class PayeasyMemberRegister : MemberRegisterBase
    {
        private string _externalUserId;

        public PayeasyMemberRegister(string externalUserId, string userName, int cityId) :
            base(userName, Membership.GeneratePassword(8, 0), cityId)
        {
            if (string.IsNullOrEmpty(externalUserId))
            {
                throw new ArgumentException("未指定從 payeasy 的串接 id", "extrenalUserId");
            }
            this._externalUserId = externalUserId;
        }

        protected override MemberRegisterReplyType CheckData()
        {
            MemberLink ml = mp.MemberLinkGetByExternalId(GetExternalUserId(), RegisterSourceType);
            if (ml.IsLoaded)
            {
                this.Message = Phrase.RegisterReturnMessageExternalIdExists;
                return MemberRegisterReplyType.ExternalUserIdExists;
            }
            return base.CheckData();
        }

        protected override MemberRegisterReplyType CreateMemberLink()
        {
            try
            {
                MemberFacade.AddMemberLink(_userId, RegisterSourceType, GetExternalUserId(), _userName);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("CreateMemberLink fail. org=({0},exid={1},userid={2}), ex: {3}",
                    RegisterSourceType, GetExternalUserId(), _userId, ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override void AuditReigsteSuccess()
        {
            MemberFacade.LogAccountAudit( _userId, _userName, AccountAuditAction.LoginUsingPayezAccount, true, 
                "經 PayEasy 認證並初次建立 17life 帳號");
        }

        public string GetExternalUserId()
        {
            return _externalUserId;
        }

        public override SingleSignOnSource RegisterSourceType
        {
            get { return SingleSignOnSource.PayEasy; }
        }
    }
}
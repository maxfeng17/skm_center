﻿namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public interface IPponIdentity
    {
        int Id { get; }
        string Name { get; }
        string Email { get; }
        string DisplayName { get; }
        string Pic { get; }
    }
}

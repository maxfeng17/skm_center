﻿using System;
using System.Data.SqlTypes;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public class MobileMemberRegister : MemberRegisterBase
    {
        private string _mobile;
        private string _userEmail;
        /// <summary>
        /// 手機號碼直接註冊時，所使用的帳號名稱
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        private static string GetMobileUserName()
        {            
            return Guid.NewGuid() + "@mobile";
        }

        public MobileMemberRegister(string mobile, string userEmail) : base(GetMobileUserName(), GenerateRandomPassword(), 0)
        {
            this._mobile = mobile;
            this._userEmail = userEmail ?? string.Empty;
        }

        private static string GenerateRandomPassword()
        {
            return Guid.NewGuid().ToString();
        }

        protected override MemberRegisterReplyType CreateMemberLink()
        {
            return MemberRegisterReplyType.RegisterSuccess;
        }

        protected override MemberRegisterReplyType CreateMember()
        {
            MemberRegisterReplyType result = base.CreateMember();

            if (result == MemberRegisterReplyType.RegisterSuccess)
            {
                try
                {
                    MobileMember mm = new MobileMember
                    {
                        UserId = _userId,
                        MobileNumber = _mobile,
                        LastLoginDate = SqlDateTime.MinValue.Value,
                        LastPasswordChangedDate = SqlDateTime.MinValue.Value,
                        IsLockedOut = false,
                        CreateTime = DateTime.Now
                    };
                    mp.MobileMemberSet(mm);
                }
                catch
                {
                    logger.Warn("MobileMemberRegister lost data, _mobile=" + _mobile);
                    throw;
                }
            }

            return result;
        }

        protected override void AuditReigsteSuccess()
        {
            MemberFacade.LogAccountAudit(_userId, _userName, AccountAuditAction.LoginUsingMobile17Life, true, "手機會員 建立帳號");
        }

        protected override MemberRegisterReplyType SubscribeEDM(bool receiveEDM)
        {
            //不訂閱
            return MemberRegisterReplyType.RegisterSuccess;
        }

        protected override MemberRegisterReplyType CheckData()
        {
            AccountAvailableStatus checkStatus = MemberUtilityCore.CheckMobileAvailable(this._mobile);
            if (checkStatus == AccountAvailableStatus.IllegalFormat)
            {
                this.Message = Phrase.RegisterMobileFormatError;
                return MemberRegisterReplyType.MobileFormatError;
            }

            if (checkStatus == AccountAvailableStatus.WasUsed)
            {
                this.Message = Phrase.RegisterReturnMessageEmailIsUser;
                return MemberRegisterReplyType.MailOldMember;
            }
            if (checkStatus == AccountAvailableStatus.Authorizing)
            {
                return MemberRegisterReplyType.RegisterContinue;
            }
            //userEmail 是可以不要設定的，但如果設定，就要符合Email的格式
            if (string.IsNullOrEmpty(this._userEmail) == false && RegExRules.CheckEmail(this._userEmail) == false)
            {
                this.Message = Helper.GetEnumDescription(MemberRegisterReplyType.UserEmailFormatError);
                return MemberRegisterReplyType.UserEmailFormatError;
            }

            return MemberRegisterReplyType.RegisterSuccess;
        }

        public override SingleSignOnSource RegisterSourceType
        {
            get { return SingleSignOnSource.Mobile17Life; }
        }
        
        protected override void ProcessInavlidMember()
        {
            var mm = MemberFacade.GetMobileMember(_mobile);
            if (mm.IsLoaded == false)
            {
                throw new Exception("ProcessInavlidMember error, _mobile=" + _mobile);
            }
            _userId = mm.UserId;
            Member mem = mp.MemberGet(_userId);
            _userName = mem.UserName;
            mem.UserEmail = _userEmail;
            mp.MemberSet(mem);
        }


        protected override void FillMemberProfileEvent(Member mem)
        {
            mem.UserEmail = _userEmail;
            mem.Mobile = _mobile;
        }
    }
}
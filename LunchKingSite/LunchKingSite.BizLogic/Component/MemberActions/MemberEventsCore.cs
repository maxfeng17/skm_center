﻿using System;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public delegate void LoggedInHandler(SingleSignOnSource source, int userId, UserDeviceType userDevice);
    public delegate void MemberCreatedHandler(Member mem, SingleSignOnSource registerSourceType);
    

    public class MemberEventsCore
    {
        public static event MemberCreatedHandler OnMemberCreated;

        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public static void TriggerMemberCreatedEvent(Member mem, SingleSignOnSource registerSourceType)
        {
            MemberProperty memProp = mp.MemberPropertyGetByUserId(mem.UniqueId);
            if (memProp.IsLoaded == false)
            {
                memProp.UserId = mem.UniqueId;
                memProp.RegisterDeviceType = (int)Helper.GetOrderFromType();
                memProp.RegisterSourceType = (int)registerSourceType;
                memProp.RegisterTime = DateTime.Now;
            }
            mp.MemberPropertySet(memProp);

            if (OnMemberCreated != null)
            {
                OnMemberCreated(mem, registerSourceType);
            }
        }

        public static void OnMemberLoggedIn(SingleSignOnSource source, int userId, UserDeviceType userDervice)
        {
            //會員登入後進這裡...

            if (userDervice == UserDeviceType.Web)
            {
                SpecialConsumingDiscountSet(userId);
            }
        }

        private static void SpecialConsumingDiscountSet(int userId)
        {
            ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
            if (conf.IsEnabledSpecialConsumingDiscount)
            {
                DateTime dateStart = DateTime.Parse(conf.SpecialConsumingDiscountPeriod.Split('|')[0]);
                DateTime dateEnd = DateTime.Parse(conf.SpecialConsumingDiscountPeriod.Split('|')[1]);
                if (DateTime.Now > dateStart && DateTime.Now < dateEnd)
                {
                    if (HttpContext.Current.Session[LkSiteSession.SpecialConsumingDiscount.ToString()] == null)
                    {
                        HttpContext.Current.Session[LkSiteSession.SpecialConsumingDiscount.ToString()] =
                            PromotionFacade.GetSpecialConsumingAmount(userId);
                    }
                }
            }
        }
    }

}
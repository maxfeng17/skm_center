﻿using System;
using System.Data.SqlClient;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models.PponEntities;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    /// <summary>
    /// Entrepot 會員
    /// </summary>
    public class EntrepotMemberRegister : MemberRegisterBase
    {
        private static readonly IEntrepotProvider _ep = ProviderFactory.Instance().GetProvider<IEntrepotProvider>();
        private static ISysConfProvider _cp = ProviderFactory.Instance().GetConfig();
        private EntrepotMember _EntrepotMember;
        private const string CombineTag = "@entrepot";

        public EntrepotMemberRegister(EntrepotMember member) :
            base(string.Format("{0}{1}", member.EntrepotToken, CombineTag), new Security(SymmetricCryptoServiceProvider.MD5).Encrypt(string.Format("{0}&{1}", member.EntrepotToken, member.CreateTime.Ticks)), CategoryManager.Default.Taipei.CityId ?? 0)
        {
            _EntrepotMember = member;
        }

        public string GetMemberRegisterName()
        {
            return string.Format("{0}{1}", _EntrepotMember.EntrepotToken, CombineTag);
        }

        protected override MemberRegisterReplyType CreateMemberLink()
        {
            if (_EntrepotMember == null || _EntrepotMember.Id == 0)
            {
                logger.ErrorFormat("CreateMemberLink fail, {0}", "查無會員資訊");
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
            try
            {
                MemberFacade.AddMemberLink(
                    _userId,
                    SingleSignOnSource.Entrepot,
                    _EntrepotMember.Id.ToString(), _userName);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("CreateMemberLink fail. org=({0},exid={1},userid={2}), ex: {3}",
                    SingleSignOnSource.Entrepot, _userId, _userId, ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override MemberRegisterReplyType CreateMember()
        {
            MemberRegisterReplyType registerResult = new MemberRegisterReplyType();
            try
            {
                Member mem = PrepareMember();
                mem.EdmType = 0;
                FillMemberProfileEvent(mem);
                mp.MemberSet(mem);
                _userId = MemberFacade.GetUniqueId(_userName);
                registerResult = MemberRegisterReplyType.RegisterSuccess;
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                {
                    logger.InfoFormat("新增的會員已存在(CreateMember):{0},{1}", _userName, ex.StackTrace);
                    return MemberRegisterReplyType.MemberExisted;
                }
            }
            catch (Exception ex)
            {
                Message = Phrase.RegisterReturnMessageRegisterError;
                logger.Error("新增會員失敗(CreateMember): ", ex);
                return MemberRegisterReplyType.RegisterError;
            }

            if (registerResult == MemberRegisterReplyType.RegisterSuccess)
            {
                registerResult = CreateEntrepotMember();
            }
            return registerResult;
        }

        private MemberRegisterReplyType CreateEntrepotMember()
        {
            try
            {
                var userId = MemberFacade.GetUniqueId(string.Format("{0}{1}", _EntrepotMember.EntrepotToken, CombineTag));
                if (userId == 0)
                {
                    return MemberRegisterReplyType.RegisterError;
                }

                //更新EntrepotMember資料
                _EntrepotMember.UserId = userId;
                return _ep.EntrepotMemberSet(_EntrepotMember)
                    ? MemberRegisterReplyType.RegisterSuccess
                    : MemberRegisterReplyType.RegisterError;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return MemberRegisterReplyType.RegisterError;
            }
        }



        protected override void AuditReigsteSuccess()
        {
            MemberFacade.LogAccountAudit(_userId, _userName, AccountAuditAction.LoginUsingEntrepotMember, true,
                "Entrepot 建立帳號");
        }

        protected override MemberRegisterReplyType SubscribeEDM(bool receiveEDM)
        {
            //不訂閱
            return MemberRegisterReplyType.RegisterSuccess;
        }

        protected override MemberRegisterReplyType CheckData()
        {
            //不檢查
            return MemberRegisterReplyType.RegisterSuccess;
        }

        public override SingleSignOnSource RegisterSourceType
        {
            get { return SingleSignOnSource.Entrepot; }
        }
    }
}
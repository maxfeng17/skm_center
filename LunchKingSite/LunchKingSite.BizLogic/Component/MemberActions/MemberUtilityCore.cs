﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public class MemberUtilityCore
    {
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISystemProvider sys = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static ILog logger = LogManager.GetLogger(typeof(MemberUtilityCore));
        private static string _registerUrl;
        public static string RegisterUrl {
            get
            {
                if (_registerUrl == null)
                {
                    _registerUrl = Helper.CombineUrl(config.SSLSiteUrl, "m/login?state=r");
                }
                return _registerUrl;
            }
        }


        public static Guid DefaultBuildingGuid
        {
            get
            {
                return config.DefaultBuildingGuid;
            }
        }

        public static string GetTrueEmail(string email)
        {
            return EmailUtil.ConvertToTrueEmail(email);
        }

        /// <summary>
        /// 取得 Email 可否被註開使用
        /// </summary>
        /// <param name="email">email</param>
        /// <param name="isCheckDEAfilter">是否需判斷拋棄式email</param>
        /// <returns></returns>
        public static EmailAvailableStatus CheckMailAvailable(string email, bool isCheckDEAfilter = false)
        {
            if (RegExRules.CheckEmail(email) == false)
            {
                return EmailAvailableStatus.IllegalFormat;
            }

            if (isCheckDEAfilter && !CheckDEAfilter(email))
            {
                return EmailAvailableStatus.DisposableEmailAddress;
            }

            Member m = mp.MemberGet(email);
            if (m.IsLoaded)
            {
                if (mp.MemberLinkGet(m.UniqueId, SingleSignOnSource.ContactDigitalIntegration).IsLoaded
                    && Helper.IsFlagSet(m.Status, MemberStatusFlag.Active17Life) == false)
                {
                    return EmailAvailableStatus.Authorizing;
                }
                return EmailAvailableStatus.WasUsed;
            }

            /* for the future: 當哪天想封掉用google無限信箱來洗紅利金的
            else
            {
                string trueEmail = MemberUtility.GetTrueEmail(email);
                m = mp.MemberGetByTrueEmail(trueEmail);
                if (m.IsLoaded)
                {
                    return EmailAvailableStatus.WasUsed;
                }
            }
             */
            return EmailAvailableStatus.Available;
        }

        public static AccountAvailableStatus CheckMobileAvailable(string mobile)
        {
            if (RegExRules.CheckMobile(mobile) == false)
            {
                return AccountAvailableStatus.IllegalFormat;
            }
            MobileMember mm = mp.MobileMemberGet(mobile);
            if (mm.IsLoaded == false)
            {
                return AccountAvailableStatus.Available;
            }
            if (mm.Status == (int)MobileMemberStatusType.Activated)
            {
                return AccountAvailableStatus.WasUsed;
            }
            return AccountAvailableStatus.Authorizing;
        }

        #region 拋棄式信箱白名單 (資料來源為17會員前100大，且經過拋棄式信箱過瀘)

        public static HashSet<string> DAEWhiteList = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "yahoo.com.tw",
            "hotmail.com",
            "gmail.com",
            "yahoo.com",
            "msa.hinet.net",
            "pchome.com.tw",
            "kimo.com",
            "msn.com",
            "hotmail.com.tw",
            "outlook.com",
            "livemail.tw",
            //"mailinator.com",
            //"yopmail.com",
            "seed.net.tw",
            //"mailnesia.com",
            "xuite.net",
            "taishinbank.com.tw",
            "ymail.com",
            "mail2000.com.tw",
            "17life.com",
            "gmail.com.tw",
            "auo.com",
            "qq.com",
            //"sharklasers.com",
            "ntu.edu.tw",
            "so-net.net.tw",
            "opayq.com",
            "126.com",
            "yahoo.com.hk",
            //"dispostable.com",
            "tsmc.com",
            "yam.com",
            "cathaylife.com.tw",
            "cht.com.tw",
            "yahoo.com.te",
            "gamil.com",
            "delta.com.tw",
            "yeah.net",
            "fubon.com",
            "me.com",
            "tp.edu.tw",
            //"lovefood.cc",
            //"sellgroupon.com",
            "nccu.edu.tw",
            "yuntech.edu.tw",
            "skl.com.tw",
            "yahoo.con.tw",
            "nanshanlife.com.tw",
            "mediatek.com",
            "masterlink.com.tw",
            "itri.org.tw",
            "yhaoo.com.tw",
            "playphys.com",
            "facebook.com",
            "umc.com",
            "yaho.com.tw",
            "edirect168.com",
            "sc.com",
            "yuanta.com",
            "yhoo.com.tw",
            //"mailmetrash.com",
            "epistar.com.tw",
            "ms17.hinet.net",
            "umail.hinet.net",
            "ms28.hinet.net",
            "live.com",
            "ms94.url.com.tw",
            "qisda.com",
            "ms96.url.com.tw",
            "ms14.hinet.net",
            "myopera.com",
            "m2k.com.tw",
            "taipower.com.tw",
            "ms21.hinet.net",
            "icloud.com",
            //"coupondemine.com",
            "ms24.hinet.net",
            "mail.tbcnet.net",
            "totalbb.net.tw",
            "mail.ntust.edu.tw",
            "foxconn.com",
            "yahoo.comtw",
            "kimo.com.tw",
            "yaoo.com.tw",
            "ms18.hinet.net",
            "kgi.com.tw",
            "ms23.hinet.net",
            "ms34.hinet.net",
            "pixnet.net",
            "ms16.hinet.net",
            "kbronet.com.tw",
            "ms22.hinet.net",
            "nctu.edu.tw",
            "ms32.hinet.net",
            "ms35.hinet.net",
            "tom.com",
            "ms15.hinet.net",
            "ms25.hinet.net",
            "ms37.hinet.net"
        };
        #endregion

        /// <summary>
        /// stop disposable email addresses
        /// </summary>
        /// <param name="email"></param>
        /// <returns>false is DAE</returns>
        public static bool CheckDEAfilter(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                //這是檢查是否為拋棄式信箱，並非檢查mail的格式是否正確，所以空的mail也回傳true
                return true;
            }

            //白名單內為非拋棄式
            if (CheckDAEWhiteList(email))
            {
                return true;
            }

            // 資料庫紀錄帳號排除
            if (CheckDAEBlackList(email) == false)
            {
                return false;
            }

            //Check EmailDomain data
            int edstatus = GetEmailDomainStatus(email);
            if (edstatus == (int)DEAStatus.OK)
            {
                return true;
            }

            if (edstatus == (int)DEAStatus.Disposable)
            {
                return false;
            }

            EmailDomain email_domain = new EmailDomain();
            email_domain.CreateTime = DateTime.Now;
            email_domain.Domain = Helper.GetEmailDomain(email).ToLower();
            switch (CheckDEARemote(email))
            {
                case DEAStatus.Disposable:
                    email_domain.Status = (int)DEAStatus.Disposable;
                    mp.EmailDomainSet(email_domain);
                    return false;
                case DEAStatus.Invalid:
                    email_domain.Status = (int)DEAStatus.Invalid;
                    mp.EmailDomainSet(email_domain);
                    return false;
                case DEAStatus.OK:
                    email_domain.Status = (int)DEAStatus.OK;
                    mp.EmailDomainSet(email_domain);
                    return true;
            }

            return true;
        }
        private static bool CheckDAEWhiteList(string email)
        {
            string emailDomain = Helper.GetEmailDomain(email);
            return DAEWhiteList.Contains(emailDomain);
        }

        private static bool CheckDAEBlackList(string email)
        {
            SystemCodeCollection deaFilterList = sys.SystemCodeGetListByCodeGroup("DEAfilter");
            string emailDomain = Helper.GetEmailDomain(email);
            if (deaFilterList.Any(x => x.CodeName.Equals(emailDomain, StringComparison.OrdinalIgnoreCase)))
            {
                return false;
            }
            return true;
        }

        private static int GetEmailDomainStatus(string email)
        {
            string emailDomain = Helper.GetEmailDomain(email).ToLower();
            EmailDomain edc = mp.EmailDomainGetListByDomain(emailDomain).FirstOrDefault();

            if (edc != null)
            {
                return edc.Status;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Mashape Block Disposable Email API
        /// https://www.mashape.com/bdea-cc/block-disposable-email#!documentation
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private static DEAStatus CheckDEARemote(string email)
        {
            return DEAStatus.OK;
        }
    }
}

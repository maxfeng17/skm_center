﻿using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public interface IBindingMember
    {
        AccountAvailableStatus GetAccountAvailableStatus();
    }
}
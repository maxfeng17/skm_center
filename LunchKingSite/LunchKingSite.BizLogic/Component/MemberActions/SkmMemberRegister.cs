﻿using System;
using System.Data.SqlClient;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    /// <summary>
    /// 新光 Skm 會員
    /// </summary>
    public class SkmMemberRegister : MemberRegisterBase
    {
        private static readonly ISkmProvider _skmMp = ProviderFactory.Instance().GetProvider<ISkmProvider>();
        private static ISysConfProvider _cp = ProviderFactory.Instance().GetConfig();
        private readonly SkmMember _skmMember;
        private const string CombineTag = "@skm";

        public SkmMemberRegister(SkmMember member) :
            base(string.Format("{0}{1}", member.SkmToken, CombineTag), new Security(SymmetricCryptoServiceProvider.MD5).Encrypt(string.Format("{0}&{1}", member.SkmToken, member.CreateTime.Ticks)), _cp.SkmCityId)
        {
            _skmMember = member;
        }

        public string GetMemberRegisterName()
        {
            return string.Format("{0}{1}", _skmMember.SkmToken, CombineTag);
        }

        protected override MemberRegisterReplyType CreateMemberLink()
        {
            if (_skmMember == null || _skmMember.IsLoaded == false)
            {
                logger.ErrorFormat("CreateMemberLink fail, {0}", "查無新光會員資訊");
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
            try
            {
                MemberFacade.AddMemberLink(
                    _userId,
                    SingleSignOnSource.SkmMember,
                    _skmMember.Id.ToString(), _userName);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("CreateMemberLink fail. org=({0},exid={1},userid={2}), ex: {3}",
                    SingleSignOnSource.SkmMember, _userId, _userId, ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override MemberRegisterReplyType CreateMember()
        {
            MemberRegisterReplyType registerResult = new MemberRegisterReplyType();
            try
            {
                Member mem = PrepareMember();
                mem.EdmType = 0;
                FillMemberProfileEvent(mem);
                mp.MemberSet(mem);
                _userId = MemberFacade.GetUniqueId(_userName);
                registerResult = MemberRegisterReplyType.RegisterSuccess;
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                {
                    logger.InfoFormat("新增的會員已存在(CreateMember):{0},{1}", _userName, ex.StackTrace);
                    return MemberRegisterReplyType.MemberExisted;
                }
            }
            catch (Exception ex)
            {
                Message = Phrase.RegisterReturnMessageRegisterError;
                logger.Error("新增會員失敗(CreateMember): ", ex);
                return MemberRegisterReplyType.RegisterError;
            }

            if (registerResult == MemberRegisterReplyType.RegisterSuccess)
            {
                registerResult = CreateSkmMember();
            }
            return registerResult;
        }

        private MemberRegisterReplyType CreateSkmMember()
        {
            try
            {
                var userId = MemberFacade.GetUniqueId(string.Format("{0}{1}", _skmMember.SkmToken, CombineTag));
                if (userId == 0)
                {
                    return MemberRegisterReplyType.RegisterError;
                }

                //更新skmMember資料
                _skmMember.UserId = userId;
                return _skmMp.MemberSet(_skmMember)
                    ? MemberRegisterReplyType.RegisterSuccess
                    : MemberRegisterReplyType.RegisterError;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return MemberRegisterReplyType.RegisterError;
            }
        }



        protected override void AuditReigsteSuccess()
        {
            MemberFacade.LogAccountAudit(_userId, _userName, AccountAuditAction.LoginUsingSkmMember, true, 
                "新光APP 建立帳號");
        }

        protected override MemberRegisterReplyType SubscribeEDM(bool receiveEDM)
        {
            //不訂閱
            return MemberRegisterReplyType.RegisterSuccess;
        }

        protected override MemberRegisterReplyType CheckData()
        {
            //不檢查
            return MemberRegisterReplyType.RegisterSuccess;
        }

        public override SingleSignOnSource RegisterSourceType
        {
            get { return SingleSignOnSource.SkmMember; }
        }
    }
}
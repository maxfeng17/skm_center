﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public class MembershipProviderCore
    {
        public static string GenerateSalt()
        {
            byte[] numArray = new byte[16];
            new RNGCryptoServiceProvider().GetBytes(numArray);
            return Convert.ToBase64String(numArray);
        }

        public static string EncodePassword(string pass, MembershipPasswordFormat passwordFormat, string salt)
        {
            if (passwordFormat == MembershipPasswordFormat.Clear)
            {
                return pass;
            }
            if (passwordFormat == MembershipPasswordFormat.Encrypted)
            {
                throw new NotSupportedException("MembershipPasswordFormat.Encrypted");
            }

            byte[] bytes = Encoding.Unicode.GetBytes(pass);
            byte[] numArray1 = Convert.FromBase64String(salt);
            byte[] inArray;

            HashAlgorithm hashAlgorithm = GetHashAlgorithm();

            byte[] buffer = new byte[numArray1.Length + bytes.Length];
            Buffer.BlockCopy((Array)numArray1, 0, (Array)buffer, 0, numArray1.Length);
            Buffer.BlockCopy((Array)bytes, 0, (Array)buffer, numArray1.Length, bytes.Length);
            inArray = hashAlgorithm.ComputeHash(buffer);
            return Convert.ToBase64String(inArray);
        }

        private static HashAlgorithm GetHashAlgorithm()
        {
            HashAlgorithm hashAlgorithm = HashAlgorithm.Create("SHA1");
            return hashAlgorithm;
        }
    }
}

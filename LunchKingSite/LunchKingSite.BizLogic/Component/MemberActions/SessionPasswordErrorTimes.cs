﻿using System;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public class PasswordErrorTimesSession
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static PasswordErrorTimesSession _instance = new PasswordErrorTimesSession();
        private PasswordErrorTimesSession()
        {
        }

        public static PasswordErrorTimesSession Current
        {
            get { return _instance; }
        }

        private int Value
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null)
                {
                    throw new Exception("PasswordErrorTimesSession error. context or session is null.");
                }
                if (HttpContext.Current.Session["PasswordErrorTimes"] == null)
                {
                    return 0;
                }
                int errortime;
                if (int.TryParse(HttpContext.Current.Session["PasswordErrorTimes"].ToString(), out errortime))
                {
                    return errortime;
                }
                return 0;
            }
            set {
                if (HttpContext.Current == null || HttpContext.Current.Session == null)
                {
                    throw new Exception("PasswordErrorTimesSession error. context or session is null.");
                }
                HttpContext.Current.Session["PasswordErrorTimes"] = value;
            }
        }

        public void Increment()
        {
            Value++;
        }

        public bool CheckWarning()
        {
            if (config.EnableCaptcha)
            {
                return true;
                //return Value >= 3;    
            }
            return false;
        }

        public void Reset()
        {
            Value = 0;
        }

        public bool CheckCaptchaMatch(string captchaResponse)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null)
            {
                throw new Exception("PasswordErrorTimesSession error. context or session is null.");
            }
            if (HttpContext.Current.Session[LkSiteSession.CaptchaLogin.ToString()] == null ||
                string.Equals(
                    HttpContext.Current.Session[LkSiteSession.CaptchaLogin.ToString()].ToString(),
                    captchaResponse,
                    StringComparison.OrdinalIgnoreCase) == false)
            {
                return false;
            }
            return true;
        }

        public int GetValue()
        {
            return Value;
        }
    }
}

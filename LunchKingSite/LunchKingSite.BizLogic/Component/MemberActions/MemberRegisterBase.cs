﻿using System;
using System.Data.SqlTypes;
using System.Web.Security;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public interface IMemberRegister
    {
        string Message { get; }
        MemberRegisterReplyType Process(bool receiveEDM);
        string UserName { get; }
        bool IsMobile { get; set; }
    }

    public abstract class MemberRegisterBase : IMemberRegister
    {
        protected string _userName;
        protected int _userId;
        protected string _password;
        protected int _cityId;
        protected bool _isMobileMember;
        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        protected static ILog logger = LogManager.GetLogger("register");

        public string Message { get; protected set; }

        public MemberRegisterBase(string userName, string password, int cityId)
        {
            this._userName = userName;
            this._password = password;
            this._cityId = cityId;
        }

        private static Guid RoleRegisterUserGuid;
        static MemberRegisterBase()
        {
            Role role = mp.RoleGet(MemberRoles.RegisteredUser.ToString("g"));
            if (role.IsLoaded == false)
            {
                logger.Fatal("在Role找不到RegisteredUser。");
            }
            RoleRegisterUserGuid = role.Guid;
        }

        public bool IsMobile { get; set; }

        protected virtual MemberRegisterReplyType CheckData()
        {
            var checkMail = MemberUtilityCore.CheckMailAvailable(this._userName, true);
            if (checkMail == EmailAvailableStatus.IllegalFormat)
            {
                this.Message = Phrase.RegisterReturnMessageEmailError;
                return MemberRegisterReplyType.EmailError;
            }
            if (checkMail == EmailAvailableStatus.DisposableEmailAddress)
            {
                this.Message = Phrase.RegisterReturnMessageDisposableEmailAddress;
                return MemberRegisterReplyType.EmailError;
            }
            if (checkMail == EmailAvailableStatus.WasUsed)
            {
                this.Message = Phrase.RegisterReturnMessageEmailIsUser;
                return MemberRegisterReplyType.MailOldMember;
            }
            if (checkMail == EmailAvailableStatus.Authorizing)
            {
                this.Message = Phrase.RegisterReturnMessageRegisterNoOpen;
                return MemberRegisterReplyType.MailRegisterInactive;
            }

            return MemberRegisterReplyType.RegisterSuccess;
        }

        public MemberRegisterReplyType Process(bool receiveEDM)
        {
            var checkMail = CheckData();
            if (checkMail != MemberRegisterReplyType.RegisterSuccess && checkMail != MemberRegisterReplyType.RegisterContinue)
            {
                return checkMail;
            }
            if (checkMail == MemberRegisterReplyType.RegisterContinue)
            {
                ProcessInavlidMember();
                return MemberRegisterReplyType.RegisterSuccess;
            }
            BeforeProcessData();
            using (var trans = TransactionScopeBuilder.CreateReadCommitted())
            {
                MemberRegisterReplyType regType = CreateMember();
                if (regType != MemberRegisterReplyType.RegisterSuccess)
                {
                    return regType;
                }
                if (_userId == 0)
                {
                    _userId = MemberFacade.GetUniqueId(_userName);
                }

                regType = CreateMembership();
                if (regType != MemberRegisterReplyType.RegisterSuccess)
                {
                    return regType;
                }

                regType = CreateMemberAuthInfo();
                if (regType != MemberRegisterReplyType.RegisterSuccess)
                {
                    return regType;
                }

                regType = CreateMemberLink();
                if (regType != MemberRegisterReplyType.RegisterSuccess)
                {
                    return regType;
                }

                SubscribeEDM(receiveEDM);
                AuditReigsteSuccess();
                trans.Complete();
            }
            Member mem = mp.MemberGet(_userId);
            MemberAuthInfo mai = mp.MemberAuthInfoGet(_userId);
            AfterProcessData(mem, mai);
            TriggerMemberCreatedEvent(mem);
            return MemberRegisterReplyType.RegisterSuccess;
        }

        protected virtual void ProcessInavlidMember()
        {
        }

        protected virtual void BeforeProcessData()
        {

        }

        public virtual SingleSignOnSource RegisterSourceType {
            get { return SingleSignOnSource.Undefined; }
        }

        protected virtual void AfterProcessData(Member mem, MemberAuthInfo mai)
        {            
        }

        private void TriggerMemberCreatedEvent(Member mem)
        {
            MemberEventsCore.TriggerMemberCreatedEvent(mem, RegisterSourceType);
        }

        protected virtual MemberRegisterReplyType CreateMembership()
        {
            try
            {
                Guid roleGuiod = RoleRegisterUserGuid;
                if (roleGuiod == Guid.Empty)
                {
                    //如果第一次初始RoleRegisterUserGuid失敗，但後來網站與db的關係修正了
                    //雖然沒有預設值，但這個流程還是要能繼續。
                    Role role = mp.RoleGet(MemberRoles.RegisteredUser.ToString("g"));
                    if (role.IsLoaded == false)
                    {
                        throw new Exception("在Role找不到RegisteredUser");
                    }
                    roleGuiod = role.Guid;
                }
                mp.RoleMemberSet(new RoleMember
                {
                    RoleGuid = roleGuiod,
                    UserId = _userId
                });
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected virtual Member PrepareMember()
        {
            Member mem = new Member
            {
                UserName = _userName,
                PrimaryContactMethod = (int)ContactMethod.Mobile,
                EdmType = (int)MemberEdmType.PponEvent | (int)MemberEdmType.PponStartedSell |
                    (int)MemberEdmType.PiinlifeEvent | (int)MemberEdmType.PiinlifeStartedSell,
                CreateTime = DateTime.Now,
                BuildingGuid = MemberUtilityCore.DefaultBuildingGuid
            };
            mem.TrueEmail = MemberUtilityCore.GetTrueEmail(_userName);
            mem.UserEmail = _userName;

            if (CityManager.CheckExistsCity(_cityId))
            {
                mem.CityId = _cityId;
            }
            if (IsMobile)
            {
                mem.Status = mem.Status | (int)MemberStatusFlag.Mobile;
            }

            mem.FailedPasswordAttemptCount = 0;
            mem.FailedPasswordAttemptWindowStart = SqlDateTime.MinValue.Value;
            mem.LastLoginDate = SqlDateTime.MinValue.Value;
            mem.LastPasswordChangedDate = SqlDateTime.MinValue.Value;
            mem.PasswordSalt = MembershipProviderCore.GenerateSalt();
            mem.PasswordFormat = (int)Membership.Provider.PasswordFormat;
            mem.Password = MembershipProviderCore.EncodePassword(_password, Membership.Provider.PasswordFormat, mem.PasswordSalt);
            mem.IsLockedOut = false;
            mem.IsApproved = true;

            return mem;
        }

        protected virtual MemberRegisterReplyType CreateMember()
        {
            try
            {
                Member mem = PrepareMember();
                if (!RegExRules.CheckEmail(mem.UserEmail))
                {
                    mem.EdmType = 0;
                }
                FillMemberProfileEvent(mem);

                mp.MemberSet(mem);
                _userId = MemberFacade.GetUniqueId(_userName);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                Message = Phrase.RegisterReturnMessageRegisterError;
                logger.Error("新增會員失敗(CreateMember): ", ex);
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected virtual void FillMemberProfileEvent(Member mem)
        {
        }

        protected virtual MemberRegisterReplyType CreateMemberAuthInfo()
        {
            return MemberRegisterReplyType.RegisterSuccess;
        }

        protected abstract MemberRegisterReplyType CreateMemberLink();

        protected abstract void AuditReigsteSuccess();

        protected virtual MemberRegisterReplyType SubscribeEDM(bool receiveEDM)
        {
            try
            {
                if (receiveEDM)
                {
                    MemberFacade.SubscribeForNewMember(_userName, _cityId);
                }
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                Message = Phrase.RegisterReturnMessageRegisterError;
                logger.ErrorFormat("新增會員失敗(SubscribeEDM): {0},{1},{2} ", _userId, _cityId, ex);
                return MemberRegisterReplyType.RegisterError;
            }
        }

        public string UserName
        {
            get { return _userName; }
        }
    }
}

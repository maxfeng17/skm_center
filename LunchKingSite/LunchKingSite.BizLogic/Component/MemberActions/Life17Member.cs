﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.Security;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;

namespace LunchKingSite.BizLogic.Component.MemberActions
{
    public class Life17Member : IBindingMember
    {
        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        protected static ILog logger = LogManager.GetLogger("Life17Member");
        private string emailOrPhoneNum;

        public Life17Member(string emailOrPhoneNum)
        {
            this.emailOrPhoneNum = emailOrPhoneNum;
        }

        public bool IsEmailFormat()
        {
            return RegExRules.CheckEmail(emailOrPhoneNum);
        }

        public bool IsMobileFormat()
        {
            return RegExRules.CheckMobile(emailOrPhoneNum);
        }

        public AccountAvailableStatus GetAccountAvailableStatus()
        {
            var checkMail = MemberUtilityCore.CheckMailAvailable(emailOrPhoneNum, false);
                       
            if (checkMail == EmailAvailableStatus.IllegalFormat)
            {
                return GetMobileRegisterReplyType();
            }
            else
            {
                return GetEmailRegisterReplyType();
            }
        }

        private AccountAvailableStatus GetEmailRegisterReplyType()
        {
            var checkMail = MemberUtilityCore.CheckMailAvailable(emailOrPhoneNum, false);

            switch (checkMail)
            {
                case EmailAvailableStatus.Undefined:
                    return AccountAvailableStatus.Undefined;
                case EmailAvailableStatus.IllegalFormat:
                    return AccountAvailableStatus.IllegalFormat;
                case EmailAvailableStatus.DisposableEmailAddress:
                    return AccountAvailableStatus.Disposable;
                case EmailAvailableStatus.Available:
                    return AccountAvailableStatus.Available;
                case EmailAvailableStatus.WasUsed:
                    return AccountAvailableStatus.WasUsed;
                case EmailAvailableStatus.Authorizing:
                    return AccountAvailableStatus.Authorizing;
                default:
                    throw new InvalidEnumArgumentException();
            }
        }

        private AccountAvailableStatus GetMobileRegisterReplyType()
        {
            var checkMobile = MemberUtilityCore.CheckMobileAvailable(emailOrPhoneNum);
            return checkMobile;
        }
    }
}

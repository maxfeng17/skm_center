﻿using System;
using System.Web;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;

namespace LunchKingSite.WebLib.Component.MemberActions
{
    public class GuestMemberRegister : MemberRegisterBase
    {
        private string displayName;
        private string mobile;

        public GuestMemberRegister(string userName, string displayName, string mobile) :
            base(userName, "000000", 0)
        {
            this.displayName = displayName;
            this.mobile = mobile;
        }

        protected override Member PrepareMember()
        {
            Member mem = base.PrepareMember();
            //訪客會員不需要接收 開賣通知/優惠活動 的相關信件
            mem.EdmType = 0;
            mem.IsGuest = true;
            string givenName;
            string familyName = Helper.GetFamilyName(displayName, out givenName);
            mem.FirstName = givenName;
            mem.LastName = familyName;
            mem.Mobile = mobile;
            return mem;
        }


        protected override MemberRegisterReplyType CreateMemberLink()
        {
            try
            {
                MemberFacade.AddMemberLink(
                    _userId, SingleSignOnSource.ContactDigitalIntegration, _userId.ToString(), _userName);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("CreateMemberLink fail. org=({0},exid={1},userid={2}), ex: {3}",
                    SingleSignOnSource.ContactDigitalIntegration, _userId, _userId, ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override MemberRegisterReplyType CreateMemberAuthInfo()
        {
            string lastUrl = "";
            if (HttpContext.Current != null && HttpContext.Current.Session != null &&
                HttpContext.Current.Session[LkSiteSession.NowUrl.ToString()] != null)
            {
                lastUrl = HttpContext.Current.Session[LkSiteSession.NowUrl.ToString()].ToString();
            }
            try
            {
                Member mem = mp.MemberGet(_userName);
                // create member auth info
                string[] authPair = NewMemberUtilityCore.GenEmailAuthCode();
                MemberAuthInfo mai = new MemberAuthInfo
                {
                    UniqueId = mem.UniqueId,
                    Email = _userName,
                    AuthCode = authPair[0],
                    AuthKey = authPair[1],
                    AuthDate = DateTime.Now,
                    LastUrl = lastUrl
                };

                mp.MemberAuthInfoSet(mai);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override void AuditReigsteSuccess()
        {
            MemberFacade.LogAccountAudit(_userId, _userName, AccountAuditAction.LoginUsing17LifeAccount, true, "訪客會員 建立帳號");
        }

        public override SingleSignOnSource RegisterSourceType
        {
            get { return SingleSignOnSource.ContactDigitalIntegration; }
        }
    }
}
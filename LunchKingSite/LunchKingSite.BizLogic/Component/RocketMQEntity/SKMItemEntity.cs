﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Component.RocketMQEntity
{
    public class SKMItemEntity
    {
        public long itemId { get; set; }

        public SKMItemEntity()
        {
            this.itemId = 0;
        }

        public string jsonOut()
        {
            // Returns JSON string.
            return JsonConvert.SerializeObject(this);
        }
    }
}

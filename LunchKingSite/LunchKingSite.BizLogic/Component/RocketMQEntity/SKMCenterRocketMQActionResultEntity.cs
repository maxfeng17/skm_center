﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Component.RocketMQEntity
{
    public class SKMCenterRocketMQActionResultEntity
    {
        /// <summary>
        /// 處理訊息是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 回應訊息
        /// </summary>
        public string Message { get; set; }


    }
}

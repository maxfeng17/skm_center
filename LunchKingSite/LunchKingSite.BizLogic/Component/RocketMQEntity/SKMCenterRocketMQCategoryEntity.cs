﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Component.RocketMQEntity
{
    public class SKMCenterRocketMQCategoryEntity
    {

        public long id { get; set; }

        public SKMCenterRocketMQCategoryEntity()
        {
            this.id = 0;
        }

        public string jsonOut()
        {
            // Returns JSON string.
            return JsonConvert.SerializeObject(this);
        }
    }
}

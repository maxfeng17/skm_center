﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Component.RocketMQEntity
{
    public class InventoryChangeEntity
    {
        /// <summary>
        /// 實體id(skuId or 物料編碼)-用17life的商品id
        /// </summary>
        public String entityId { get; set; }

        /// <summary>
        /// 實體類型，skuId: 1 或者 物料編碼: 4
        /// </summary>
        public int entityType { get; set; }

        /// <summary>
        /// 物料編碼
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// 倉庫編碼
        /// </summary>
        public String warehouseCode { get; set; }

        /// <summary>
        /// 倉庫類型(默認為100)
        /// </summary>
        public int warehouseType { get; set; }

        /// <summary>
        /// 請求數量 正值為增加，負值為減少
        /// </summary>
        public long quantity { get; set; }

        /// <summary>
        /// 業務id(訂單id)
        /// </summary>
        public String bizSrcId { get; set; }

        /// <summary>
        /// 業務類型， 100：app訂單 200: ec訂單 300: 线下订单
        /// </summary>
        public int bizSrcType { get; set; }

        /// <summary>
        /// 業務子單id(細化到sku單行的id)
        /// </summary>
        public string[] bizSrcSubId { get; set; }

        public InventoryChangeEntity()
        {
            entityId = string.Empty;
            entityType = 1;
            warehouseCode = string.Empty;
            warehouseType = 100;
            quantity = 0;
            bizSrcId = string.Empty;
            bizSrcType = 200;
            bizSrcSubId = new string[] { string.Empty };
        }

        public string jsonOut()
        {
            // Returns JSON string.
            return JsonConvert.SerializeObject(this);
        }
    }
}

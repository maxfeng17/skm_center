﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Component.RocketMQEntity
{
    public class SKMChannelOnSaleMessageEntity
    {
        public long itemId { get; set; }

        public bool success { get; set; }

        public string message { get; set; }

        public SKMChannelOnSaleMessageEntity()
        {
            itemId = 0;
            success = true;
            message = string.Empty;
        }

        public string jsonOut()
        {
            // Returns JSON string.
            return JsonConvert.SerializeObject(this);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Component.RocketMQEntity
{
    public class InventoriesMessage
    {
        public InventoryChangeEntity[] paramList { get; set; }

        public InventoriesMessage()
        {
            InventoryChangeEntity inventoryEntity = new InventoryChangeEntity();

            paramList = new InventoryChangeEntity[] { inventoryEntity };
        }

        public string jsonOut()
        {
            // Returns JSON string.
            return JsonConvert.SerializeObject(this);
        }
    }
}

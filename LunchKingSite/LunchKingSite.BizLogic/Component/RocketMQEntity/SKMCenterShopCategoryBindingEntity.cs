﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Component.RocketMQEntity
{
    public class SKMCenterShopCategoryBindingEntity
    {
        public long itemId { get; set; }

        public long categoryId { get; set; }

        public SKMCenterShopCategoryBindingEntity()
        {
            this.itemId = 0;

            this.categoryId = 0;
        }

        public string jsonOut()
        {
            // Returns JSON string.
            return JsonConvert.SerializeObject(this);
        }
    }
}

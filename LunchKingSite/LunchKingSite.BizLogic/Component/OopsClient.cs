﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace LunchKingSite.BizLogic.Component
{
    public class OopsClient
    {
        static OopsClient _instance = new OopsClient();
        private OopsClient()
        {
        }
        public static OopsClient Instance
        {
            get
            {
                return _instance;
            }
        }

        MqttFactory factory;
        MqttClient mqttClient;
        string topic;

        public async Task Start(string host, string topic)
        {
            factory = new MqttFactory();
            mqttClient = factory.CreateMqttClient() as MqttClient;
            this.topic = topic ?? "Undefined";

            var options = new MqttClientOptionsBuilder()
                .WithClientId(Environment.MachineName)
                .WithTcpServer(host)
                .Build();

            //重連
            mqttClient.UseDisconnectedHandler(async e =>
            {
                //Console.WriteLine("### disconnected from server ###");
                await Task.Delay(TimeSpan.FromSeconds(5));

                try
                {
                    await mqttClient.ConnectAsync(options, CancellationToken.None);
                }
                catch
                {
                    //Console.WriteLine("### reconnecting failed ###");
                }
            });

            //開始連線
            try
            {
                await mqttClient.ConnectAsync(options, CancellationToken.None);
            }
            catch
            {
            }
        }

        public void Write(Exception ex, HttpContext context)
        {
            if (mqttClient != null)
            {
                try
                {
                    Error error = new Error(ex, context);
                    mqttClient.PublishAsync(this.topic, JsonConvert.SerializeObject(error));
                }
                catch { }
            }
        }

        public void Stop()
        {
            if (mqttClient == null)
            {
                return;
            }
            mqttClient.Dispose();
        }
    }


    public class ErrorBase
    {

        public string HostName { get; set; }
        public string TypeName { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public string Detail { get; set; }
        public string User { get; set; }
        public string Ip { get; set; }
        public string RealIp { get; set; }
        public DateTime Time { get; set; }

        public string HttpAgent { get; set; }
        public bool IsHttps { get; set; }
        public string HttpHost { get; set; }
        public string HttpPath { get; set; }
        public string HttpMethod { get; set; }
        public string HttpProtocol { get; set; }
        public int StatusCode { get; set; }

        public List<DetailItem> QueryString { get; set; }
        public List<DetailItem> Form { get; set; }
        public List<DetailItem> Cookies { get; set; }
        public List<DetailItem> Headers { get; set; }


        public class DetailItem
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }
    }

    public class Error : ErrorBase
    {
        public Error(Exception ex, HttpContext context)
        {
            if (ex == null)
                throw new ArgumentNullException("ex");

            int statusCode = 0;
            var httpEx = ex as HttpException;
            if (httpEx != null)
            {
                statusCode = httpEx.GetHttpCode();
            }

            Exception baseException = ex.GetBaseException();

            this.HostName = Environment.MachineName;
            this.TypeName = baseException.GetType().FullName;
            this.Message = baseException.Message;
            this.Source = baseException.Source;
            this.Detail = ex.ToString();

            this.Time = DateTime.Now;
            this.User = string.Empty;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null)
            {
                this.User = Thread.CurrentPrincipal.Identity.Name;
            }

            if (context != null)
            {
                this.User = this.User ?? context.User.Identity.Name;
                this.Ip = GetClientIp(context, false);
                this.RealIp = GetClientIp(context, true);

                this.HttpAgent = context.Request.Headers["User-Agent"];
                if (this.HttpAgent != null && this.HttpAgent.Length > 250)
                {
                    this.HttpAgent = this.HttpAgent.Substring(0, 250);
                }
                this.IsHttps = context.Request.IsSecureConnection;
                this.HttpHost = context.Request.Url.Host;
                this.HttpPath = context.Request.Path;
                this.HttpMethod = context.Request.HttpMethod;
                //this.HttpProtocol = context.Request.h.Protocol;
                if (statusCode != 0)
                {
                    this.StatusCode = statusCode;
                } else
                {
                    this.StatusCode = context.Response.StatusCode;
                }

                var request = context.Request;
                if (request.HttpMethod == "POST")
                {
                    this.Form = CopyCollection(request.Form);
                }
                this.QueryString = CopyCollection(request.QueryString);
                this.Cookies = CopyCollection(request.Cookies);
                this.Headers = CopyCollection(request.Headers);
            }
        }

        private List<DetailItem> CopyCollection(NameValueCollection items)
        {
            List<DetailItem> result = new List<DetailItem>();
            foreach (string key in items)
            {
                string value = items[key];
                result.Add(new DetailItem { Key = key, Value = value });
            }
            return result;
        }

        private List<DetailItem> CopyCollection(HttpCookieCollection items)
        {
            List<DetailItem> result = new List<DetailItem>();
            foreach (string key in items)
            {
                HttpCookie cookie = items[key];
                result.Add(new DetailItem { Key = key, Value = cookie.Value});
            }
            return result;
        }

        private string GetClientIp(HttpContext context, bool forwardIpFirst = true)
        {            
            return Helper.GetClientIP(!forwardIpFirst);
        }
    }
}

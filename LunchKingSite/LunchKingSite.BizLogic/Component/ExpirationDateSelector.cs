﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Component
{
    public class ExpirationDateSelector
    {
        public IExpirationChangeable ExpirationDates { get; set; }

        #region methods

        /// <summary>
        /// 判斷參數 dt 是否超過失效時間
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool IsPastExpirationDate(DateTime dt)
        {
            if (ExpirationDates == null)
            {
                throw new InvalidOperationException(
                    "No expire dates to work with! Please set the property ExpirationDates properly.");
            }
			
            //真正的失效時間
			DateTime effectiveExpireDate = new DateTime(Math.Min(GetCloseDownDate().Ticks, GetExpireDatePlusOne().Ticks));

        	return dt > effectiveExpireDate;
        }

    	/// <summary>
        /// 取得最終失效日期 (顯示用的失效日期). 
        /// 若無失效日期, 回傳 DateTime.MaxValue. 
        /// 如果要判斷一個時間點是否失效, 請用 IsPastExpirationDate().
        /// </summary>
        /// <returns></returns>
        public DateTime GetExpirationDate()
        {
			// 截止日期: 分店優先賣家(檔次)
            if (ExpirationDates == null)
            {
                throw new InvalidOperationException(
                    "No expire dates to work with! Please set the property ExpirationDates properly.");
            }

            //真正的失效時間
			DateTime result = new DateTime(Math.Min(GetCloseDownDate().Ticks, GetExpirationDatePlusZero().Ticks));

			return result;
        }
		
    	/// <summary>
		/// 取得失效日期 (非顯示用的失效日期, 真正可用來比較的日期). 
		/// 若無失效日期, 視為永久有效, 回傳 DateTime.MaxValue. 
		/// </summary>
		public DateTime GetInternalExpirationDate()
		{
			if (ExpirationDates == null)
			{
				throw new InvalidOperationException(
					"No expire dates to work with! Please set the property ExpirationDates properly.");
			}

			DateTime result = new DateTime(Math.Min(GetCloseDownDate().Ticks, GetExpireDatePlusOne().Ticks));
			return result;
		}

        #endregion


		private DateTime GetCloseDownDate()
		{
			// 結束營業日: 分店優先賣家
			return ExpirationDates.StoreClosedDownDate 
				?? ExpirationDates.SellerClosedDownDate 
				?? DateTime.MaxValue;
		}
		private DateTime GetExpireDatePlusOne()
		{
			// 截止日期: 分店優先賣家(檔次)
			DateTime expireDate = ExpirationDates.StoreChangedExpireDate ??
								  ExpirationDates.DealChangedExpireDate ??
								  ExpirationDates.DealOriginalExpireDate ?? DateTime.MaxValue;
			if (expireDate < new DateTime(9999, 12, 31, 0, 0, 0))
			{
				expireDate = expireDate.AddDays(1); // 截止日期是 0點0分, 但在當天結束之前都可使用 
			}
			return expireDate;
		}
		private DateTime GetExpirationDatePlusZero()
		{
			return ExpirationDates.StoreChangedExpireDate 
				?? ExpirationDates.DealChangedExpireDate 
				?? ExpirationDates.DealOriginalExpireDate 
				?? DateTime.MaxValue;
		}


        #region ctors
        
        public ExpirationDateSelector()
        {
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
namespace LunchKingSite.BizLogic.Component
{
    public class PponApiCategoryManager
    {
        private static readonly IPponProvider PponProv;

        private static HamiCategoryCollection _categorys;
        public static HamiCategoryCollection Categorys { 
            get
            {
                if((_categorys==null)||(_categorys.Count==0))
                {
                    ReloadDefaultManager();
                }
                return _categorys;
            } 
        }

        public static void ReloadDefaultManager()
        {
            _categorys = PponProv.HamiCategoryGetList();
        }

        static PponApiCategoryManager()
        {
            PponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            if ((_categorys == null) || (_categorys.Count == 0))
            {
                ReloadDefaultManager();
            }
        }
        /// <summary>
        /// 依據傳入的DealType的id，回傳符合條件的HamiCategory的值，若查無符合條件的資料，回傳null
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static HamiCategory HamiCategoryGetByTypeCode(int code)
        {
            var rtns = Categorys.Where(x => x.DealTypeCodeId == code).ToList();
            HamiCategory rtn = null;
            if(rtns.Count>0)
            {
                rtn = rtns.First();
            }
            return rtn;
        }
        
    }
}

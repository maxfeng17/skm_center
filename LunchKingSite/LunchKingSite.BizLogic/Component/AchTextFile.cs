﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    public class AchTextFile
    {
        #region properties
        
        public StreamReader Reader { get; set; }
        
        #endregion

        public AchBatchTransactionResult Load()
        {
            if (this.Reader == null)
            {
                throw new InvalidOperationException("StreamReader not set!");
            }
            
            string everything = Reader.ReadToEnd();
            string[] allLines = everything.Split(new string[]{ Environment.NewLine } , StringSplitOptions.RemoveEmptyEntries);
            
            int footerIndex = allLines.Length - 1;

            AnsiTextSerializer sw = new AnsiTextSerializer();

            AchResultTextHeader header = sw.ReadItem<AchResultTextHeader>(Encoding.ASCII.GetBytes(allLines[0]));
            AchResultTextFooter footer = sw.ReadItem<AchResultTextFooter>(Encoding.ASCII.GetBytes(allLines[footerIndex]));
            var details = new List<AchResultTextDetail>();
            for (int i = 1; i < footerIndex; i++)
            {
                var detail = sw.ReadItem<AchResultTextDetail>(Encoding.ASCII.GetBytes(allLines[i]));
                details.Add(detail);
            }

            var result = new AchBatchTransactionResult(header, details, footer);
            return result;
        }
    }


    public class AchBatchTransactionResult
    {
        private readonly bool _isCorrupted;
        public bool IsCorrupted 
        {
            get { return _isCorrupted; }
        }

        private readonly DateTime _batchProcessedTime;
        public DateTime BatchProcessedTime
        {
            get
            {
                if (IsCorrupted)
                {
                    throw new Exception("Data is corrupted!");
                }
                return _batchProcessedTime ;
            }
        }
        
        private readonly List<AchTransactionResult> trxResults;
        public List<AchTransactionResult> TransactionResults
        {
            get
            {
                if (IsCorrupted)
                {
                    throw new Exception("Data is corrupted!");
                }
                return trxResults.ToList();
            }
        }

        public AchBatchTransactionResult(AchResultTextHeader header, List<AchResultTextDetail> details, AchResultTextFooter footer)
        {
            DateTime processedTime;
            if (HeaderIsValid(header) 
                && FooterIsValid(footer)
                && TryGetProcessedTime(header, out processedTime))
            {
                _batchProcessedTime = processedTime;
            }
            else
            {
                _isCorrupted = true;
            }

            if(!_isCorrupted)
            {
                trxResults = new List<AchTransactionResult>();
                foreach (AchResultTextDetail detail in details)
                {
                    trxResults.Add(new AchTransactionResult(detail));
                }

                int totalRecordCount;
                if (int.TryParse(footer.TotalRecordCount, out totalRecordCount))
                {
                    if (details.Count != totalRecordCount)
                    {
                        _isCorrupted = true;
                    }
                }
            }
        }

        private bool FooterIsValid(AchResultTextFooter footer)
        {
            return string.Equals("EOF", footer.Lead);
        }

        private bool HeaderIsValid(AchResultTextHeader header)
        {
            return string.Equals("BOF", header.Lead);
        }

        private bool TryGetProcessedTime(AchResultTextHeader header, out DateTime processedTime)
        {
            int taiwanYear;
            int month;
            int day;
            int hour;
            int minute;
            int second;
            
            if(int.TryParse(header.ProcessedDate.Substring(0,4), out taiwanYear)
                && int.TryParse(header.ProcessedDate.Substring(4,2), out month)
                && int.TryParse(header.ProcessedDate.Substring(6, 2), out day)
                && int.TryParse(header.ProcessedTime.Substring(0,2), out hour)
                && int.TryParse(header.ProcessedTime.Substring(2,2), out minute)
                && int.TryParse(header.ProcessedTime.Substring(4,2), out second)
                )
            {
                int westernYear = taiwanYear + 1911;
                processedTime = new DateTime(westernYear, month, day, hour, minute, second);
                return true;
            }
            
            processedTime = new DateTime(1900, 1, 1);
            return false;
        }
    }

    public class AchTransactionResult
    {
        private readonly bool _isCorrupted;
        /// <summary>
        /// 資料是否毀損
        /// </summary>
        public bool IsCorrupted
        {
            get { return _isCorrupted; }
        }

        private readonly int _weeklyPayReportId;
        public int WeeklyPayReportId
        {
            get { return _weeklyPayReportId; }
        }

        private readonly int _dealUniqueId;
        public int DealUniqueId
        {
            get { return _dealUniqueId; }
        }

        private readonly int _amount;
        public int Amount
        {
            get { return _amount; }
        }

        private readonly string _bankNumber;
        /// <summary>
        /// 銀行代號
        /// </summary>
        public string BankNumber
        {
            get { return _bankNumber; }
        }

        private readonly string _branchNumber;
        /// <summary>
        /// 分行代號
        /// </summary>
        public string BranchNumber
        {
            get { return _branchNumber; }
        }

        private readonly string _accountNumber;
        /// <summary>
        /// 收受者帳號
        /// </summary>
        public string AccountNumber
        {
            get { return _accountNumber; }
        }

        private readonly string _companyId;
        /// <summary>
        /// 收受者統一編號/身分證字號
        /// </summary>
        public string CompanyId
        {
            get { return _companyId; }
        }

        public AchResultType Result { get; private set; }

        public AchTransactionResult(AchResultTextDetail detail)
        {
            _bankNumber = detail.ReceiverBankCode.Substring(0, 3);
            _branchNumber = detail.ReceiverBankCode.Substring(3, 4);
            _accountNumber = detail.ReceiverAccount.PadLeft(14, '0');
            _companyId = detail.ReceiverCompanyId;
            this.Result = new AchResultType(detail.RejectionCode);

            int payId;
            int dealId;
            int amount;
            if(int.TryParse(detail.PaymentId.Trim(), out payId)
                && int.TryParse(detail.OriginatorExclusive.Trim(), out dealId)
                && int.TryParse(detail.Amount.Trim(), out amount))
            {
                _weeklyPayReportId = payId;
                _dealUniqueId = dealId;
                _amount = amount;
            }
            else
            {
                _isCorrupted = true;
            }
        }
    }

    public class AchResultType
    {
        private readonly bool _isSuccess;
        public bool IsSuccess
        {
            get { return _isSuccess; }
        }

        private readonly bool _isSuccessButAmountMismatch;
        public bool IsSuccessButAmountMismatch
        {
            get { return _isSuccessButAmountMismatch; }
        }

        private readonly bool _isNull;
        public bool IsNull
        {
            get { return _isNull; }
        }

        private readonly string _code;
        public string Code
        {
            get { return _code; }
        }

        public string Description 
        {
            get
            {
                return GetDescription(this.Code);
            }
        }
        
        public AchResultType(string code)
        {
            switch (code)
            {
                case "00":
                    _code = code;
                    _isSuccess = true;
                    _isNull = false;
                    break;
                case "01":
                case "02":
                case "03":
                case "04":
                case "05":
                case "06":
                case "07":
                case "22":
                case "23":
                case "24":
                case "25":
                case "91":
                case "99":
                    _code = code;
                    _isSuccess = false;
                    _isSuccessButAmountMismatch = false;
                    _isNull = false;
                    break;
                case "-00":
                    _code = code;
                    _isSuccess = true;
                    _isSuccessButAmountMismatch = true;
                    _isNull = false;
                    break;
                case null:
                    _code = "-2";
                    _isSuccess = false;
                    _isSuccessButAmountMismatch = false;
                    _isNull = true;
                    break;
                default:
                    _code = "-1";
                    _isSuccess = false;
                    _isSuccessButAmountMismatch = false;
                    _isNull = false;
                    break;
            }
        }
        

        private static string GetDescription(string code)
        {
            return descriptionLookup[code];
        }
        
        private readonly static Dictionary<string, string> descriptionLookup = new Dictionary<string, string>()
                                                                         {
                                                                             {"00", "付款成功"},
                                                                             {"01", "存款不足"},
                                                                             {"02", "非委託用戶"},
                                                                             {"03", "已終止委託用戶"},
                                                                             {"04", "無此帳號"},
                                                                             {"05", "收受者統編錯誤"},
                                                                             {"06", "無此用戶號碼"},
                                                                             {"07", "用戶號碼不符"},
                                                                             {"22", "帳戶已結清"},
                                                                             {"23", "靜止戶"},
                                                                             {"24", "凍結戶"},
                                                                             {"25", "帳戶存款遭法院強制執行"},
                                                                             {"91", "未交易或匯入失敗資料"},
                                                                             {"99", "其它"},
                                                                             {"-1", "無法辨認"},
                                                                             {"-2", "空資料"},
                                                                             {"-00", "付款成功, 但匯入資料與系統金額不符"}
                                                                         };
    }


    public class AchResultTextHeader : AnsiTextBase
    {
        [Ansi(0, 3)]
        public string Lead { get; set; }
        [Ansi(1, 6)]
        public string DataType { get; set; }
        /// <summary>
        /// 民國年 YYYYMMDD
        /// </summary>
        [Ansi(2, 8)]
        public string ProcessedDate { get; set; }
        /// <summary>
        /// HHMMSS
        /// </summary>
        [Ansi(3, 6)]
        public string ProcessedTime { get; set; }
        [Ansi(4, 7)]
        public string SenderCode { get; set; }
        [Ansi(5, 7)]
        public string TakerCode { get; set; }
        [Ansi(6, 123, UnusedChar = UnusedChar.Space)]
        public string Reserved { get; set; }
    }

    public class AchResultTextDetail : AnsiTextBase
    {
        [Ansi(0, 1)]
        public string TrxType { get; set; }
        [Ansi(1, 2)]
        public string TrxCatg { get; set; }
        [Ansi(2, 3)]
        public string TrxCode { get; set; }
        /// <summary>
        /// 退件行按交易順序編訂序號
        /// </summary>
        [Ansi(3, 6, Align = Align.Right, UnusedChar = UnusedChar.Number0)]
        public string TrxSeqNo { get; set; }
        /// <summary>
        /// 退件行金融機構代號
        /// </summary>
        [Ansi(4, 7)]
        public string ReceiverBankCode { get; set; }
        /// <summary>
        /// 原收受者帳號
        /// </summary>
        [Ansi(5, 14, Align = Align.Right, UnusedChar = UnusedChar.Number0)]
        public string ReceiverAccount { get; set; }
        [Ansi(6, 7)]
        public string OriginatorBankCode { get; set; }
        [Ansi(7, 14, Align = Align.Right, UnusedChar = UnusedChar.Number0)]
        public string OriginatorAccount { get; set; }
        /// <summary>
        /// 金額
        /// </summary>
        [Ansi(8, 10, Align = Align.Right, UnusedChar = UnusedChar.Number0)]
        public string Amount { get; set; }
        /// <summary>
        /// 退件理由代號
        /// </summary>
        [Ansi(9, 2)]
        public string RejectionCode { get; set; }
        [Ansi(10, 1)]
        public string TransferSeq { get; set; }
        [Ansi(11, 10, Align = Align.Left, UnusedChar = UnusedChar.Space)]
        public string OriginatorCompanyId { get; set; }
        /// <summary>
        /// 收受者統一編號
        /// </summary>
        [Ansi(12, 10, Align = Align.Left, UnusedChar = UnusedChar.Space)]
        public string ReceiverCompanyId { get; set; }
        [Ansi(13, 6, Align = Align.Left, UnusedChar = UnusedChar.Space)]
        public string StockSymbol { get; set; }
        [Ansi(14, 8)]
        public string OriginalTrxDate { get; set; }
        [Ansi(15, 6)]
        public string OriginalTrxSeqNo { get; set; }
        [Ansi(16, 1)]
        public string OriginalTransferSeq { get; set; }
        /// <summary>
        /// [weekly_pay_report].[id]
        /// </summary>
        [Ansi(17, 20, Align = Align.Left, UnusedChar = UnusedChar.Space)]
        public string PaymentId { get; set; }
        /// <summary>
        /// 檔號
        /// </summary>
        [Ansi(18, 20, Align = Align.Left, UnusedChar = UnusedChar.Space)]
        public string OriginatorExclusive { get; set; }
        [Ansi(19, 12, UnusedChar = UnusedChar.Space)]
        public string Reserved { get; set; }
    }

    public class AchResultTextFooter : AnsiTextBase
    {
        [Ansi(0, 3)]
        public string Lead { get; set; }
        [Ansi(1, 6)]
        public string DataCodeName { get; set; }
        [Ansi(2, 8)]
        public string ProcessedDate { get; set; }
        [Ansi(3, 7)]
        public string SenderCodeName { get; set; }
        [Ansi(4, 7)]
        public string TakerCodeName { get; set; }
        [Ansi(5, 8)]
        public string TotalRecordCount { get; set; }
        [Ansi(6, 16)]
        public string TotalAmount { get; set; }
        [Ansi(7, 8)]
        public string PreviousWorkday { get; set; }
        [Ansi(8, 97, UnusedChar = UnusedChar.Space)]
        public string Reserved { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class SMS
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("SMS");
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        /// <summary>
        /// 指定taskId 可以
        /// </summary>
        /// <param name="content"></param>
        /// <param name="phoneNumbers"></param>
        /// <param name="taskId"></param>
        public void SendMessage(string content, string[] phoneNumbers, Guid taskId)
        {
            SendMessage(string.Empty, content, string.Empty, phoneNumbers, config.AdminEmail,
                SmsType.System, Guid.Empty, "0", SmsSystem.Undefined, taskId);
        }

        public void SendMessage(string subject, string content, string sendTime, string phoneNumbers, string userName,
            SmsType msgtype = SmsType.System, Guid bid = default(Guid), string couponId = "0")
        {
            SendMessage(subject, content, sendTime, new string[] { phoneNumbers }, userName, msgtype, bid, couponId);
        }

        /// <summary>
        /// Sybase big5編碼，每則簡訊最大是70個字，超過70字分拆成2個Order，但接收者會自動合在同一封簡訊收到
        /// 目前使用 Sybase
        /// </summary>
        /// <param name="subject">(Ev8D)傳送之主旨，主旨內容不會隨簡訊發送出去，用於報表中查詢，方便管理之用</param>
        /// <param name="content">傳送簡訊之內容</param>
        /// <param name="sendTime">(Ev8D)目前無作用，預計傳送時間，帶入空字串時，則立即傳送；若需預約傳送時，帶入格式為yyyyMMddHHmmss，例如:2009/01/01 13:00:00 則傳入20090101130000</param>
        /// <param name="phoneNumbers">要傳送之電話號碼，多筆時以逗點(,)隔開。若為國際電話時，請用+86xxxxxxxx格式</param>
        /// <param name="userName"></param>
        /// <param name="msgtype"></param>
        /// <param name="bid"></param>
        /// <param name="couponId"></param>
        /// <param name="provider"></param>
        /// <param name="taskId"></param>
        public void SendMessage(string subject, string content, string sendTime, string[] phoneNumbers, string userName,
            SmsType msgtype = SmsType.System, Guid bid = default(Guid), string couponId = "0",
            SmsSystem provider = SmsSystem.Undefined, Guid taskId = default(Guid))
        {
            if (config.IsSmsSend == false)
            {
                return;
            }

            if (provider == SmsSystem.Undefined)
            {
                provider = CommunicationFacade.GetSmsSystemByRatio();
            }

            if (taskId == Guid.Empty)
            {
                taskId = Guid.NewGuid();
            }

            if (phoneNumbers.Length > config.SmsPhoneNumberLimit)
            {
                for (int i = 0; i < phoneNumbers.Length; i = i + config.SmsPhoneNumberLimit)
                {
                    string[] partialPhoneNumbers = new List<string>(phoneNumbers)
                        .Skip(i).Take(config.SmsPhoneNumberLimit)
                        .ToArray();

                    SendMessage(subject, content, sendTime, partialPhoneNumbers, userName,
                        msgtype, bid, couponId, provider, taskId);
                }
                return;
            }
            
            try
            {
                SmsSendReply reply;
                ISmsSender sender = null;

                switch (provider)
                {
                    case SmsSystem.Sybase:
                        sender = new SybaseSender();
                        break;
                    case SmsSystem.iTe2:
                        sender = new iTe2Sender();
                        break;
                    default:
                        throw new Exception("sms sender provider is null");
                }

                sender.Send(content, phoneNumbers, new SmsExtraInfo
                {
                }, out reply);

                if (reply.Equals(SmsSendReply.Empty) == false)
                {
                    foreach (string phoneNumber in reply.PhoneNumbers.Select(t => t.OriginalNumber))
                    {
                        AddLog(content, msgtype, userName, phoneNumber, SmsStatus.Sending,
                            SmsFailReason.None, bid, couponId, provider, taskId, reply);
                    }
                    foreach (string phoneNumber in reply.IllegalPhoneNumbers)
                    {
                        AddLog(content, msgtype, userName, phoneNumber, SmsStatus.Fail,
                            SmsFailReason.IllegalFormat, bid, couponId, provider, taskId, reply);
                    }
                }
                else
                {
                    foreach (string phoneNumber in phoneNumbers)
                    {
                        QueueMessage(content, SmsType.System, userName, phoneNumber, bid, couponId, provider, taskId);
                    }
                }
                
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Sms Content: {0} \r\n{1}", content, ex));
                foreach (string phoneNumber in phoneNumbers)
                {
                    QueueMessage(content, SmsType.System, userName, phoneNumber, bid, couponId, provider, taskId);
                }
            }         
        }

        public void QueueMessage(string message, SmsType msgtype, string createId, string phoneNumber,
            Guid bid, string couponID, SmsSystem provider, Guid taskId = default(Guid))
        {
            if (taskId == Guid.Empty)
            {
                taskId = Guid.NewGuid();
            }
            if (RegExRules.CheckMobile(phoneNumber))
            {
                AddLog(message, SmsType.Ppon, createId, phoneNumber, SmsStatus.Queue, SmsFailReason.None, bid, couponID, 
                    provider, taskId, SmsSendReply.Empty);
            }
            else
            {
                AddLog(message, SmsType.Ppon, createId, phoneNumber, SmsStatus.Fail, SmsFailReason.IllegalFormat, bid, couponID,
                    provider, taskId, SmsSendReply.Empty);
            }
        }

        /// <summary>
        /// 也是 queue 一個簡訊，但不發送，等檔次成單的條件成立時，狀態從 ppon 改成 queue ，再由 job 發送
        /// </summary>
        public void QueueMessageForPponNotOnDeal(string message, SmsType msgtype, string createId, string phoneNumber,
            Guid bid, string couponID, SmsSystem provider, Guid taskId = default(Guid))
        {
            if (taskId == Guid.Empty)
            {
                taskId = Guid.NewGuid();
            }
            if (RegExRules.CheckMobile(phoneNumber))
            {
                AddLog(message, SmsType.Ppon, createId, phoneNumber, SmsStatus.Queue, SmsFailReason.None, bid,
                    couponID, provider, taskId, SmsSendReply.Empty);
            }
            else
            {
                AddLog(message, SmsType.Ppon, createId, phoneNumber, SmsStatus.Fail, SmsFailReason.IllegalFormat, bid,
                    couponID, provider, taskId, SmsSendReply.Empty);
            }
        }

        public void QueueMessageForPayByAtm(string message, SmsType msgtype, string createId, string phoneNumber,
            Guid bid, string couponID, SmsSystem provider, Guid taskId = default(Guid))
        {
            if (taskId == Guid.Empty)
            {
                taskId = Guid.NewGuid();
            }
            if (RegExRules.CheckMobile(phoneNumber))
            {
                AddLog(message, SmsType.Ppon, createId, phoneNumber, SmsStatus.Atm, SmsFailReason.None, bid, couponID, provider, taskId,
                    SmsSendReply.Empty);
            }
            else
            {
                AddLog(message, SmsType.Ppon, createId, phoneNumber, SmsStatus.Fail, SmsFailReason.IllegalFormat, bid, couponID, provider, taskId,
                    SmsSendReply.Empty);
            }
        }

        public void QueueMessageWithSendingTime(string message, SmsType msgtype, string createId, string phoneNumber,
            Guid bid, string couponID, SmsSystem provider, Guid taskId = default(Guid))
        {
            if (taskId == Guid.Empty)
            {
                taskId = Guid.NewGuid();
            }
            if (RegExRules.CheckMobile(phoneNumber))
            {
                AddLog(message, SmsType.Ppon, createId, phoneNumber, SmsStatus.Atm, SmsFailReason.None, bid, couponID, provider, taskId,
                    SmsSendReply.Empty);
            }
            else
            {
                AddLog(message, SmsType.Ppon, createId, phoneNumber, SmsStatus.Fail, SmsFailReason.IllegalFormat, bid, couponID, provider, taskId,
                    SmsSendReply.Empty);
            }
        }

        private void AddLog(string message, SmsType msgtype, string createId, string phoneNumber, SmsStatus msgStatus,
            SmsFailReason failReason, Guid bid, string couponID, SmsSystem provider, Guid taskId, SmsSendReply smsReply, 
            int acceptableStartHour = 0, int acceptableEndHour = 24)
        {
            SmsLog slog = new SmsLog();
            slog.Message = message;
            slog.Type = (int)msgtype;
            slog.CreateTime = DateTime.Now;
            slog.CreateId = createId;
            slog.Mobile = phoneNumber;
            slog.Status = (int)msgStatus;
            slog.FailReason = (int)failReason;
            slog.AcceptableStartHour = acceptableStartHour;
            slog.AcceptableEndHour = acceptableEndHour;
            if (smsReply == null || smsReply.Equals(SmsSendReply.Empty))
            {
                slog.SmsOrderCount = 1;
                slog.SmsOrderId = string.Empty;
            }
            else
            {
                slog.SmsOrderCount = smsReply.OrderCount;
                slog.SmsOrderId = smsReply.OrderId;
            }
            if (bid != Guid.Empty)
            {
                slog.BusinessHourGuid = bid;
            }
            slog.CouponId = couponID;
            slog.TaskId = taskId;
            slog.Provider = (int)provider;
            pp.SMSLogSet(slog);
        }


        /// <summary>
        /// 傳送狀態為佇列的簡訊，傳送完會更新狀態
        /// </summary>
        /// <param name="smsLog"></param>
        /// <returns></returns>
        public void SendQueuedMessage(SmsLog smsLog)
        {
            if (config.IsSmsSend == false)
            {
                return;
            }
            if (smsLog.Status != (int)SmsStatus.Queue)
            {
                throw new Exception("只發送狀態為 queue 的簡訊");
            }
            try
            {
                //bool result;
                SmsSendReply reply = SmsSendReply.Empty;
                ISmsSender sender = null;

                if (smsLog.Provider == (int)SmsSystem.Undefined)
                {
                    smsLog.Provider = (int)CommunicationFacade.GetSmsSystemByRatio();
                }
                switch (smsLog.Provider)
                {
                    case (int)SmsSystem.Sybase:
                        sender = new SybaseSender();
                        break;
                    case (int)SmsSystem.iTe2:
                        sender = new iTe2Sender();
                        break;
                }

                sender.Send(smsLog.Message, smsLog.Mobile, new SmsExtraInfo
                {
                }, out reply);

                smsLog.SmsOrderId = reply.OrderId;
                smsLog.SmsOrderCount = reply.OrderCount;
                if (reply.Equals(SmsSendReply.Empty) == false)
                {
                    if (reply.IllegalPhoneNumbers.Contains(smsLog.Mobile))
                    {
                        smsLog.Status = (int)SmsStatus.Fail;
                        smsLog.FailReason = (int)SmsFailReason.IllegalFormat;
                    }
                    else
                    {
                        if (smsLog.Status != (int)SmsStatus.Success && smsLog.Status != (int)SmsStatus.Fail)
                        {
                            smsLog.Status = (int)SmsStatus.Sending;
                        } 
                    } 
                }
                else
                {
                    smsLog.Status = (int)SmsStatus.Queue;
                }
                pp.SMSLogSet(smsLog);
            }
            catch (Exception ex)
            {
                log.Info(ex.Message);
                smsLog.Status = (int)SmsStatus.Fail;
                pp.SMSLogSet(smsLog);
            }            
        }
        /// <summary>
        /// 會員簡訊，需設定會員可接受的時間區段
        /// </summary>
        /// <param name="message"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="createId"></param>
        /// <param name="acceptableStartHour"></param>
        /// <param name="acceptableEndHour"></param>
        public void QueueMessage(string message, string phoneNumber, string createId, int acceptableStartHour, int acceptableEndHour)
        {
            AddLog(message, SmsType.Member, createId, phoneNumber, SmsStatus.Queue, SmsFailReason.None,
                Guid.Empty, string.Empty, SmsSystem.Undefined, Guid.NewGuid(), SmsSendReply.Empty, acceptableStartHour, acceptableEndHour);
        }
    }
}
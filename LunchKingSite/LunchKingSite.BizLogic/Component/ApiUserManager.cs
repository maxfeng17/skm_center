﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class ApiUserManager
    {
        private static ISystemProvider _systemProv;
        private static ApiUserCollection _enabledApiUserList;
        private const int LoginTicketValidityDay = 14;
        private const string _oldIOsUserId = "IOS2012871623";
        private const string _oldAndroidUserId = "AND2013681057";

        public static ApiUserCollection EnabledApiUserList
        {
            get
            {
                if (_enabledApiUserList.Count == 0)
                {
                    _enabledApiUserList = _systemProv.ApiUserGetList(true);
                }
                return _enabledApiUserList;
            }
        }

        static ApiUserManager()
        {
            _systemProv = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _enabledApiUserList = new ApiUserCollection();
        }

        public static void ReloadDefaultManager()
        {
            _enabledApiUserList = _systemProv.ApiUserGetList(true);
        }

        public static void ClearData()
        {
            _enabledApiUserList = new ApiUserCollection();
        }

        /// <summary>
        /// 由暫存檔案中取出ID為傳入參數的ApiUser資料，如果查無資料，回傳null
        /// </summary>
        /// <param name="userId">ApiUser的UserId</param>
        /// <returns></returns>
        public static ApiUser ApiUserGetByUserId(string userId)
        {
            return EnabledApiUserList.FirstOrDefault(x => x.UserId == userId);
        }

        /// <summary>
        /// ApiUsedLog寫入的動作
        /// </summary>
        /// <param name="user">Api使用者物件</param>
        /// <param name="methodName">被呼叫的method名稱</param>
        /// <param name="parameter">傳入參數的json字串</param>
        /// <returns></returns>
        public static bool ApiUsedLogSet(ApiUser user, string methodName, string parameter, string returnValue = "")
        {
            //資料量太大，不適合每筆都計，先不新增紀錄
            return true;
        }



        public static bool IsOldAppUserId(string userId)
        {
            if (_oldAndroidUserId.Equals(userId) || _oldIOsUserId.Equals(userId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
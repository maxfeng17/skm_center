﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    [Serializable]
    public class CpaClientEvent<T> : List<T>
    {
        public int MainId { get; set; }
        public CpaClientEvent(int main_id)
        {
            MainId = main_id;
        }
    }

    [Serializable]
    public class CpaClientDeail
    {
        public string PageName { get; set; }
        public string Rsrc { get; set; }
        public string Content { get; set; }
        public DateTime CreateDate { get; set; }
        public CpaClientDeail(string page_name, string rsrc_code, string content)
        {
            PageName = page_name;
            Rsrc = rsrc_code;
            Content = content;
            CreateDate = DateTime.Now;
        }
    }
}

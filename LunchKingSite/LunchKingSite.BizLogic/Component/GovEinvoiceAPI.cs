﻿using System;
using System.IO;
using System.Net;
using LunchKingSite.BizLogic.Model.EInvoice;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using log4net;

namespace LunchKingSite.BizLogic.Component
{
    public class GovEinvoiceAPI
    {
        private static ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(GovEinvoiceAPI));
        public static GovAPIReply QueryLoveCode(string code)
        {
            //https://www.einvoice.nat.gov.tw/PB2CAPIVAN/loveCodeapp/qryLoveCode?version=0.2&qKey=919&action=qryLoveCode&UUID=0000&appID=EINV9201310315863
            string url = string.Format("{0}/PB2CAPIVAN/loveCodeapp/qryLoveCode?version=0.2&qKey={1}&action=qryLoveCode&UUID=0000&appID={2}",
                conf.EInvoiceApiUrl, code, conf.EInvoiceAppId);

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";

            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    var json = ProviderFactory.Instance().GetSerializer();
                    LoveCodeReply reply = json.Deserialize<LoveCodeReply>(reader.ReadToEnd());
                    return reply;
                }
            }
            catch (Exception ex)
            {
                logger.Error("QueryLoveCode fail, ex: ", ex);
                return new GovAPIReply
                {
                    Code = GovAPIReply.Error,
                    Message = "error"
                };
            }
        }
    }
}

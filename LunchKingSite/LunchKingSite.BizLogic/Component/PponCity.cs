﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon.RDS.Model;

namespace LunchKingSite.BizLogic.Component
{
    public class PponCity
    {
        private int _cityId;
        public int CityId
        {
            get
            {
                return _cityId;
            }
        }
        private string _cityName;
        public string CityName
        {
            get
            {
                return _cityName;
            }
        }
        private string _cityCode;
        public string CityCode
        {
            get
            {
                return _cityCode;
            }
        }
        private int _cityRank;
        public int CityRank
        {
            get
            {
                return _cityRank;
            }
        }
        /// <summary>
        /// 直接對應的CategoryId
        /// </summary>
        private readonly int _categoryId;
        public int CategoryId
        {
            get { return _categoryId; }
        }
        /// <summary>
        /// 紀錄城市對應到的頻道Category Id
        /// </summary>
        private readonly int? _channelId;
        public int? ChannelId
        {
            get { return _channelId; }
        }
        /// <summary>
        /// 記錄城市對應到的某頻道下的區域Category Id
        /// </summary>
        private readonly int? _channelAreaId;
        public int? ChannelAreaId
        {
            get { return _channelAreaId; }
        }

        private string _edmCityName;
        public string EDMCityName
        {
            get
            {
                return _edmCityName;
            }
        }

        private readonly bool _isFoodChannel;
        public bool IsFoodChannel
        {
            get
            {
                return _isFoodChannel;
            }
        }


        public PponCity(int cityId, string cityName, string cityCode, int cityRank, int categoryId, bool isFoodChannel = false)
        {
            _cityId = cityId;
            _cityName = cityName;
            _cityCode = cityCode;
            _cityRank = cityRank;
            _categoryId = categoryId;
            _channelId = null;
            _channelAreaId = null;
            _edmCityName = (isFoodChannel) ? "美食" + cityName : cityName;
            _isFoodChannel = isFoodChannel;
        }

        public PponCity(int cityId,string cityName,string cityCode, int cityRank , int categoryId , int? channelId , int? channelAreaId,bool isFoodChannel =false)
        {
            _cityId = cityId;
            _cityName = cityName;
            _cityCode = cityCode;
            _cityRank = cityRank;
            _categoryId = categoryId;
            _channelId = channelId;
            _channelAreaId = channelAreaId;
            _edmCityName = (isFoodChannel) ? "美食" + cityName : cityName;
        }

        public bool IsMatchChannel(int channelId, int? channelAreaId)
        {
            //未設定頻道，直接回覆不相符
            if (!ChannelId.HasValue)
            {
                return false;
            }
            //當CategoryId 與 ChannnelId相同時，表示原設計的城市 直接 對應到某個新的頻道
            //此狀況只比對channelId，不管AreaId
            if (CategoryId == ChannelId.Value)
            {
                if (ChannelId.Value == channelId)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            //有區域的類型，先比對頻道，之後比對區域
            if (ChannelId.Value == channelId)
            {

                if ((ChannelAreaId.HasValue) && (channelAreaId.HasValue) && (ChannelAreaId.Value == channelAreaId.Value))
                {
                    //有設定區域ID且有傳入要比對的區域ID，且傳入值比對後相同
                    return true;
                }
                else if (!ChannelAreaId.HasValue && !channelAreaId.HasValue)
                {
                    //未設定區域且未傳入需比對的區域參數
                    return true;
                }
                else
                {
                    //會到這邊表示 未設定參數卻需比對區域 或 有設定卻未傳入需比對的區域 或有設定也有傳入參數卻不相等
                    return false;
                }
            }
            else
            {
                //頻道編號不符
                return false;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", this.CityName, this.CityId);
        }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using log4net;
using System.Web.Security;

namespace LunchKingSite.BizLogic.Component
{
    public class FunctionPrivilegeManager
    {
        private static ConcurrentDictionary<string, List<string>> funcPrivileges = new ConcurrentDictionary<string, List<string>>();
        private static ConcurrentDictionary<string, List<FunctionPrivilegeData>> viewPrivileges =
            new ConcurrentDictionary<string, List<FunctionPrivilegeData>>();
        private static ConcurrentDictionary<string, List<FunctionPrivilegeData>> userPrivileges =
            new ConcurrentDictionary<string, List<FunctionPrivilegeData>>();
        private static ConcurrentDictionary<string, List<FunctionPrivilegeData>> orgPrivileges =
            new ConcurrentDictionary<string, List<FunctionPrivilegeData>>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        private static ISystemProvider sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        private static ILog logger = LogManager.GetLogger("privilege");

        public static void ClearManagerData()
        {
            funcPrivileges = new ConcurrentDictionary<string, List<string>>();
            viewPrivileges = new ConcurrentDictionary<string, List<FunctionPrivilegeData>>();
            userPrivileges = new ConcurrentDictionary<string, List<FunctionPrivilegeData>>();
            orgPrivileges = new ConcurrentDictionary<string, List<FunctionPrivilegeData>>();
        }

        public static List<FunctionPrivilegeData> GetFunctionPrivilegeDataById(string email)
        {
            return viewPrivileges.GetOrAdd(email, delegate(string userName)
            {
                List<FunctionPrivilegeData> rtn = GetFunctionPrivilegeData(userName, SystemFunctionType.Read);
                foreach (string orgName in Roles.GetRolesForUser(email))
                {
                    if (orgName == MemberRoles.Administrator.ToString())
                    {
                        rtn = rtn.Union(GetAllFunctionOrgPrivilegeData(SystemFunctionType.Read)).ToList();
                    }
                    else
                    {
                        rtn = rtn.Union(GetFunctionOrgPrivilegeData(orgName, SystemFunctionType.Read)).ToList();
                    }
                }
                return rtn;
            });
        }

        public static List<string> GetPrivilegesByFunctionType(string path, SystemFunctionType type)
        {
            string itemKey = string.Format("{0}:{1}", path, type);
            List<string> rtn =
                funcPrivileges.GetOrAdd(itemKey, delegate(string key)
                {
                    List<string> users = new List<string>();
                    string link = key.Split(':')[0];
                    SystemFunctionType funcType;
                    if (SystemFunctionType.TryParse(key.Split(':')[1], out funcType))
                    {
                        // 取得成員本身賦予權限的成員清單
                        ViewPrivilegeCollection userPris = hp.ViewPrivilegeGetListByFuncType(link, funcType);
                        users = userPris.Select(x => x.UserName).ToList();

                        // 取得群組賦予權限的成員清單
                        SystemFunction func = sp.GetSystemFunction(link, funcType);
                        OrgPrivilegeCollection orgPris = hp.GetOrgPrivilegeListByFuncId(func.Id);
                        foreach (OrgPrivilege orgPri in orgPris)
                        {
                            users = users.Union(Roles.GetUsersInRole(orgPri.OrgName)).ToList();
                        }
                    }
                    return users;
                });
            return rtn;
        }

        public static List<string> GetPrivilegesByFunctionType(SystemFunctionType type, string path)
        {
            return GetPrivilegesByFunctionType(path, type);
        }

        public static List<string> GetPrivilegesByFunctionType(SystemFunctionType type)
        {
            string localPath = GetLocalPath();
            return GetPrivilegesByFunctionType(localPath, type);
        }

        public static bool IsInSystemFunctionPrivilege(string email, SystemFunctionType type)
        {
            return IsInSystemFunctionPrivilege(email, type, GetLocalPath());
        }

        public static bool IsInSystemFunctionPrivilege(string email, string actionName, SystemFunctionType type)
        {
            string localPath = GetLocalPath();
            string checkPath;
            if (actionName.StartsWith("/controlroom", StringComparison.OrdinalIgnoreCase))
            {
                checkPath = actionName;
            }
            else
            {
                checkPath = localPath.ReplaceIgnoreCase("/" + actionName, "");
            }
            if (!IsInSystemFunctionPrivilege(email, type, checkPath))
            {
                return IsInSystemFunctionPrivilege(email, type, localPath);
            }
            return true;
        }

        public static bool IsInSystemFunctionPrivilege(string email, SystemFunctionType type, string path)
        {
            List<FunctionPrivilegeData> rtn =
                userPrivileges.GetOrAdd(email, delegate(string userName)
                {
                    return GetFunctionPrivilegeData(userName, SystemFunctionType.All);
                });
            bool result = rtn.Any(t => t.Link.Equals(path, StringComparison.OrdinalIgnoreCase)
                                && t.FunctionType == type);

            PrivilegeLogged(email, type, path, result);

            return result;
        }

        public static bool IsInSystemFunctionOrgPrivilegeByUserName(string username, SystemFunctionType type)
        {
            foreach (string orgName in Roles.GetRolesForUser(username))
            {
                if (IsInSystemFunctionOrgPrivilege(orgName, type, GetLocalPath()))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsInSystemFunctionOrgPrivilegeByUserName(string username, SystemFunctionType type, string path)
        {
            foreach (string orgName in Roles.GetRolesForUser(username))
            {
                if (IsInSystemFunctionOrgPrivilege(orgName, type, path))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsInSystemFunctionOrgPrivilegeByUserName(string username, string actionName, SystemFunctionType type)
        {
            foreach (string orgName in Roles.GetRolesForUser(username))
            {
                if (IsInSystemFunctionOrgPrivilege(orgName, actionName, type, GetLocalPath()))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsInSystemFunctionOrgPrivilege(string orgName, SystemFunctionType type, string path)
        {
            List<FunctionPrivilegeData> rtn =
                orgPrivileges.GetOrAdd(orgName, delegate(string org)
                {
                    if (org == MemberRoles.Administrator.ToString())
                    {
                        return GetAllFunctionOrgPrivilegeData(type);
                    }
                    else
                    {
                        return GetFunctionOrgPrivilegeData(org, SystemFunctionType.All);
                    }
                });

            bool result = rtn.Any(t => t.Link.Equals(path, StringComparison.OrdinalIgnoreCase)
                                && t.FunctionType == type);

            PrivilegeLogged(orgName, type, path, result);

            return result;
        }

        public static bool IsInSystemFunctionOrgPrivilege(string orgName, string actionName, SystemFunctionType type, string localPath)
        {
            string checkPath;
            if (actionName.StartsWith("/controlroom", StringComparison.OrdinalIgnoreCase))
            {
                checkPath = actionName;
            }
            else
            {
                checkPath = localPath.ReplaceIgnoreCase("/" + actionName, "");
            }
            if (!IsInSystemFunctionOrgPrivilege(orgName, type, checkPath))
            {
                return IsInSystemFunctionOrgPrivilege(orgName, type, localPath);
            }
            return true;
        }

        private static string GetLocalPath()
        {
            string webRoot = VirtualPathUtility.AppendTrailingSlash(HttpContext.Current.Request.ApplicationPath);
            return @"/" + HttpContext.Current.Request.Path.TrimStart(webRoot, StringComparison.OrdinalIgnoreCase);
        }

        private static void PrivilegeLogged(string name, SystemFunctionType type, string localPath, bool result)
        {
            if (config.IsPrivilegeLogged)
            {
                StringBuilder sb = new StringBuilder();
                int idx = 0;
                sb.AppendLine();
                sb.AppendLine(result ? "成功" : "失敗");
                sb.AppendLine(name);
                sb.AppendLine(@"/" + HttpContext.Current.Request.Url.AbsoluteUri.ReplaceIgnoreCase(config.SiteUrl, string.Empty).TrimStart('/'));
                sb.AppendLine(localPath);
                sb.AppendFormat("type: " + type);
                if (userPrivileges.ContainsKey(name))
                {
                    foreach (FunctionPrivilegeData funcData in userPrivileges[name])
                    {
                        idx++;
                        sb.AppendFormat("{0}:{1}=>{2} {3}\r\n", idx, funcData.DisplayName, funcData.FunctionType, funcData.Link);
                    }
                }
                else if (orgPrivileges.ContainsKey(name))
                {
                    foreach (FunctionPrivilegeData funcData in orgPrivileges[name])
                    {
                        idx++;
                        sb.AppendFormat("{0}:{1}=>{2} {3}\r\n", idx, funcData.DisplayName, funcData.FunctionType, funcData.Link);
                    }
                }

                logger.Debug(sb.ToString());
            }
        }

        public static List<FunctionPrivilegeData> GetFunctionPrivilegeData(string userName, SystemFunctionType type)
        {
            List<FunctionPrivilegeData> rtn = new List<FunctionPrivilegeData>();
            //改成 ViewPrivilege
            ViewPrivilegeCollection privilegeList = hp.ViewPrivilegeGetList(userName);
            foreach (ViewPrivilege privilege in privilegeList)
            {
                if (privilege.FuncType != type.ToString() && type != SystemFunctionType.All)
                {
                    continue;
                }
                SystemFunctionType funcType;
                if (SystemFunctionType.TryParse(privilege.FuncType, out funcType))
                {
                    FunctionPrivilegeData fpd = new FunctionPrivilegeData();
                    fpd.DisplayName = privilege.DisplayName;
                    fpd.Link = privilege.Link;
                    fpd.ParentFunction = privilege.ParentFunc;
                    fpd.SortOrder = privilege.SortOrder;
                    fpd.ParentSortOrder = privilege.ParentSortOrder;
                    fpd.Visible = privilege.Visible;
                    fpd.FunctionType = funcType;
                    rtn.Add(fpd);
                }
            }
            return rtn;
        }

        public static List<FunctionPrivilegeData> GetAllFunctionOrgPrivilegeData(SystemFunctionType type)
        {
            List<FunctionPrivilegeData> rtn = new List<FunctionPrivilegeData>();
            SystemFunctionCollection functions = sp.GetSystemFunctionList(1, 999999, SystemFunction.Columns.Id);
            foreach (SystemFunction func in functions)
            {
                SystemFunctionType funcType;
                if (SystemFunctionType.TryParse(func.FuncType, out funcType))
                {
                    if (func.FuncType != type.ToString() && type != SystemFunctionType.All)
                    {
                        continue;
                    }
                    FunctionPrivilegeData fpd = new FunctionPrivilegeData();
                    fpd.DisplayName = func.DisplayName;
                    fpd.Link = func.Link;
                    fpd.ParentFunction = func.ParentFunc;
                    fpd.SortOrder = func.SortOrder;
                    fpd.ParentSortOrder = func.ParentSortOrder;
                    fpd.Visible = func.Visible;
                    fpd.FunctionType = funcType;
                    rtn.Add(fpd);
                }
            }
            return rtn;
        }

        public static List<FunctionPrivilegeData> GetFunctionOrgPrivilegeData(string orgName, SystemFunctionType type)
        {
            List<FunctionPrivilegeData> rtn = new List<FunctionPrivilegeData>();
            OrgPrivilegeCollection privilegeList = hp.GetOrgPrivilegeListByOrgName(orgName);
            foreach (OrgPrivilege privilege in privilegeList)
            {
                SystemFunction func = sp.GetSystemFunction(privilege.FuncId);
                if (func.FuncType != type.ToString() && type != SystemFunctionType.All)
                {
                    continue;
                }
                SystemFunctionType funcType;
                if (SystemFunctionType.TryParse(func.FuncType, out funcType))
                {
                    FunctionPrivilegeData fpd = new FunctionPrivilegeData();
                    fpd.DisplayName = func.DisplayName;
                    fpd.Link = func.Link;
                    fpd.ParentFunction = func.ParentFunc;
                    fpd.SortOrder = func.SortOrder;
                    fpd.ParentSortOrder = func.ParentSortOrder;
                    fpd.Visible = func.Visible;
                    fpd.FunctionType = funcType;
                    rtn.Add(fpd);
                }
            }
            return rtn;
        }

        public static void ClearUserCacheData(string userName)
        {
            List<FunctionPrivilegeData> userPrivilegeData;
            userPrivileges.TryRemove(userName, out userPrivilegeData);
        }
    }

    public class FunctionPrivilegeData
    {
        public string DisplayName { get; set; }
        public string Link { get; set; }
        public int SortOrder { get; set; }
        public string ParentFunction { get; set; }
        public int ParentSortOrder { get; set; }
        public bool Visible { get; set; }
        public SystemFunctionType FunctionType { get; set; }

        public override string ToString()
        {
            return string.Format("{0},{1}", Link, FunctionType);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            FunctionPrivilegeData castObj = obj as FunctionPrivilegeData;
            if (castObj == null)
            {
                return false;
            }
            //return castObj.GetHashCode() == GetHashCode();
            return DisplayName == castObj.DisplayName &&
                Link == castObj.Link &&
                ParentFunction == castObj.ParentFunction &&
                FunctionType == castObj.FunctionType;
        }

        public override int GetHashCode()
        {
            return string.Format("{0}::{1}::{2}::{3}", 
                DisplayName, Link, ParentFunction, FunctionType).GetHashCode();
        }
    }
}

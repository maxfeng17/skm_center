﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Transactions;

namespace LunchKingSite.BizLogic.Component
{
    public class PponDealClose
    {
        private static IOrderProvider _odrProv;
        private static IPponProvider _pponProv;
        private static ISysConfProvider _config;
        private static ILog log = LogManager.GetLogger(typeof(PponDealClose));
        

        public PponDealClose()
        {
            _odrProv = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _config = ProviderFactory.Instance().GetConfig();
        }

        public List<DealCloseResult> CloseDeal(IEnumerable<ViewPponDeal> deals, string userName)
        {
            List<DealCloseResult> results = new List<DealCloseResult>();
            DealCloseResult result = new DealCloseResult();
            string content = string.Empty;
            var now = DateTime.Now;

            if (!deals.Any(d => !Helper.IsFlagSet(d.GroupOrderStatus ?? 0, GroupOrderStatus.PponProcessed) &&
                                 !Helper.IsFlagSet(d.GroupOrderStatus ?? 0, GroupOrderStatus.Completed)))
            {
                foreach (var deal in deals)
                {
                    result = new DealCloseResult
                    {
                        BusinessHourGuid = deal.BusinessHourGuid,
                        Result = DealCloseResultStatus.AlreadyClose
                    };
                    results.Add(result);
                }
            }
            else
            {
                deals = deals.Where(d => !Helper.IsFlagSet(d.GroupOrderStatus ?? 0, GroupOrderStatus.PponProcessed) &&
                                        !Helper.IsFlagSet(d.GroupOrderStatus ?? 0, GroupOrderStatus.Completed));

                //抓取檔次結檔已成立訂單訂購數量(剔除結檔前全部退貨訂單)
                var dealQuantityAmount = _odrProv.OrderCashTrustLogGetQuantityAmount(deals.Select(x => x.BusinessHourGuid).ToList())
                                            .ToLookup(x => x.BusinessHourGuid);

                // closing the running deals
                foreach (ViewPponDeal d in deals)
                {
                    try
                    {
                        // 重算銷售數 避免deal_sales_info quantity與實際銷售數有落差
                        _pponProv.DealSalesInfoRefresh(d.BusinessHourGuid);
                        // 重新撈資料, 確保取得該檔次最新狀態(避免job在很短之間隔時間內重覆執行)
                        ViewPponDeal dd = _pponProv.ViewPponDealGetByBusinessHourGuid(d.BusinessHourGuid);

                        //重新檢查檔次結檔時間 是否符合檢查時間 避免於執行系統結檔時有檔次去異動結檔時間 
                        if (DateTime.Compare(dd.BusinessHourOrderTimeE, now.AddYears(-1)) < 0 ||
                            DateTime.Compare(dd.BusinessHourOrderTimeE, now) > 0)
                        {
                            continue;
                        }

                        // if the deal has completed, log it and leave, this is not supposed to happen
                        if (Helper.IsFlagSet(dd.GroupOrderStatus ?? 0, GroupOrderStatus.Completed) ||
                            Helper.IsFlagSet(dd.GroupOrderStatus ?? 0, GroupOrderStatus.PponProcessed))
                        {
                            result = new DealCloseResult
                            {
                                BusinessHourGuid = d.BusinessHourGuid,
                                Result = DealCloseResultStatus.AlreadyClose
                            };
                            results.Add(result);

                            //txScope.Complete(); 
                            continue;
                        }

                        OrderFacade.CompleteGroupOrder(d.BusinessHourGuid);

                        //update GroupOrderStatus and slug if the deal is soldout
                        int quantity = 0, amount = 0;
                        OrderQuantityAmount info = new OrderQuantityAmount();
                        if (Helper.IsFlagSet(dd.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                        {
                            //multipledeals: the slug of the main deal should sum the orderquantities of subdeals
                            ViewComboDealCollection combodeals = _pponProv.GetViewComboDealByBid(dd.BusinessHourGuid, false);
                            var subDealQuantityAmount = _odrProv.OrderCashTrustLogGetQuantityAmount(combodeals.Select(x => x.BusinessHourGuid).ToList())
                                                        .ToLookup(x => x.BusinessHourGuid);
                            foreach (ViewComboDeal cd in combodeals)
                            {
                                info = subDealQuantityAmount[cd.BusinessHourGuid].FirstOrDefault();
                                if (info != null)
                                {
                                    quantity = quantity + info.Quantity;
                                    amount = amount + info.Amount;               
                                }
                            }
                            OrderFacade.UpdateGroupOrderStatusForClosingDeal(dd.GroupOrderGuid.Value, quantity, amount);
                        }
                        else
                        {
                            info = dealQuantityAmount[dd.BusinessHourGuid].FirstOrDefault();
                            if (info != null)
                            {
                                quantity = info.Quantity;
                                amount = info.Amount;
                            }
                            OrderFacade.UpdateGroupOrderStatusForClosingDeal(dd.GroupOrderGuid.Value, quantity, amount);
                        }

                        if (dd.ItemPrice > 0)
                        {
                            PponFacade.UpdateDealPropertyContinuedQuantity(dd.BusinessHourGuid);
                        }

                        if (dd.GetDealStage() == PponDealStage.ClosedAndOn || dd.GetDealStage() == PponDealStage.CouponGenerated)
                        {
                            //OrderFacade.SendPponDealSuccessMail(d);
                            if (dd.DeliveryType != (int)DeliveryType.ToShop)
                            //if (d.Department != (int)DepartmentTypes.Ppon)    此功能尚未被使用(setup頁)
                            {
                                DealAccounting da = _pponProv.DealAccountingGet(dd.BusinessHourGuid);
                                if (da != null)
                                {
                                    da.MarkOld();
                                    da.Flag |= (int)AccountingFlag.VerificationFinished;
                                    _pponProv.DealAccountingSet(da);
                                }
                            }
                        }
                        else
                        {
                            if (dd.GetDealStage() == PponDealStage.ClosedAndFail)
                            {
                                if (dd.OrderGuid.HasValue && dd.DeliveryType.HasValue)
                                { 
                                    OrderFacade.SendPponDealFailMail(dd);
                                    CancelPayment(dd.OrderGuid.Value, (DeliveryType)dd.DeliveryType.Value);
                                }
                            }
                        }

                        //DEAL結束後，處理邀請紅利部分
                        MemberFacade.MemberPromotionProcessAfterPponDealClosed(dd);

                        // finally we mark this group order as being processed to prevent it from being processing again
                        var go = _odrProv.GroupOrderGet(dd.GroupOrderGuid.Value);
                        go.Status = (int)Helper.SetFlag(true, go.Status ?? 0, GroupOrderStatus.PponProcessed);
                        _odrProv.GroupOrderSet(go);

                        result = new DealCloseResult
                        {
                            BusinessHourGuid = dd.BusinessHourGuid,
                            Result = DealCloseResultStatus.Success
                        };
                        results.Add(result);
                       
                    }
                    catch (Exception e)
                    {
                        result = new DealCloseResult
                        {
                            BusinessHourGuid = d.BusinessHourGuid,
                            Result = DealCloseResultStatus.Fail,
                            Message = string.Format("message: {0} \r\n trace: {1}", e.Message, e.StackTrace)
                        };
                        results.Add(result);
                        log.Error("PponDealClose Error: " + d.BusinessHourGuid, e);
                    }
                }
            }

            if (results.Count > 0)
            {
                content = "由使用者: " + userName + " 觸發結檔<br/>檔次結檔結果依bid紀錄至Audit中，請至Audit以bid查詢確認";
                foreach (DealCloseResult item in results)
                {
                    var resultDesc = string.Empty;
                    switch (item.Result)
                    {
                        case DealCloseResultStatus.AlreadyClose:
                            resultDesc = "無須結檔";
                            break;

                        case DealCloseResultStatus.Success:
                            resultDesc = "結檔成功";
                            break;

                        case DealCloseResultStatus.Fail:
                            resultDesc = "結檔失敗，原因:" + item.Message;
                            break;

                        default:
                            break;
                    }
                    CommonFacade.AddAudit(item.BusinessHourGuid, AuditType.GroupOrder, string.Format("Job:PponDealClose執行結檔，執行結果：{0}", resultDesc), userName, false);
                }
            }
            //發信紀錄結檔結果
            SendMail(content);
            return results;
        }

        private void CancelPayment(Guid pid, DeliveryType deliveryType)
        {
            var returnId = "sys";
            var returnReason = "未達門檻，自動退貨";
            OrderCollection oCol = _odrProv.OrderGetListPaging(0, -1, OrderStatus.Complete, null, Order.Columns.ParentOrderId + "=" + pid);
            foreach (var o in oCol)
            {
                var result = CreateReturnFormResult.ProductsUnreturnable;
                int? returnFormId = null;
                switch (deliveryType)
                {
                    case DeliveryType.ToShop:
                    {
                        IEnumerable<int> refundableCouponIds = ReturnService.GetRefundableCouponCtlogs(o.Guid, BusinessModel.Ppon).Select(x => (int)x.CouponId).ToList();
                        if (refundableCouponIds.Any())
                        {
                            result = ReturnService.CreateCouponReturnForm(o.Guid, refundableCouponIds, false, returnId, returnReason, out returnFormId, true);
                        }
                    }
                        break;
                    case DeliveryType.ToHouse:
                    {
                        IEnumerable<int> orderProductIds = _odrProv.OrderProductGetListByOrderGuid(o.Guid).Where(x => x.IsCurrent && !x.IsReturning && !x.IsReturned && !x.IsExchanging).Select(x => x.Id).ToList();
                        if (orderProductIds.Any())
                        {
                                result = ReturnService.CreateRefundForm(o.Guid, orderProductIds, false, returnId, returnReason, null, null, null, null, out returnFormId, true);
                        }
                    }
                        break;
                }

                if (result == CreateReturnFormResult.Created)
                {
                    string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId.Value.ToString(), returnReason);
                    CommonFacade.AddAudit(o.Guid, AuditType.Refund, auditMessage, returnId, false);

                    ReturnFormEntity returnEntity = ReturnFormRepository.FindById(returnFormId.Value);
                    ReturnService.Refund(returnEntity, returnId, true, false);
                }

                DiscountCode discountcode = _odrProv.DiscountCodeGetByOrderGuid(o.Guid);
                if (discountcode != null)
                {
                    discountcode.MarkOld();
                    discountcode.UseTime = null;
                    discountcode.UseAmount = null;
                    discountcode.UseId = null;
                    discountcode.OrderAmount = null;
                    discountcode.OrderGuid = null;
                    discountcode.OrderCost = null;
                    _odrProv.DiscountCodeSet(discountcode);
                }
            }
        }

        private void SendMail(string content)
        {
            try
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(_config.AdminEmail);
                msg.Subject = Environment.MachineName + ": [系統]PponDealClosing Start Running";
                msg.From = new MailAddress(_config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = content;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                log.Error("PponDealClosingChore running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }
    }

    public class DealCloseResult
    {
        public Guid BusinessHourGuid { get; set; }

        public DealCloseResultStatus Result { get; set; }

        public string Message { get; set; }
    }
}
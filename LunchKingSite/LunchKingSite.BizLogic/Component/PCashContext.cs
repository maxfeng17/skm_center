﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    public class PCashContext
    {
        public static string GetUserRemaining()
        {
            return @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
<PcashDoc xmlns=""http://payment.payeasy.com.tw/pcash/""
xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
xsi:schemaLocation=""http://payment.payeasy.com.tw/pcash/Q004-request.xsd"">
	<DocHead>
	<ServiceCode>Q004</ServiceCode>
	<MerchantId>M0010</MerchantId>
	</DocHead>
	<DocContent>
	<PezMemberNum>{0}</PezMemberNum>
	</DocContent>
</PcashDoc>
";
        }

        public static string GetChargeContext(string transId,string PEZID,decimal cost,string ItemName)
        {
            string context=@"<?xml version=""1.0"" encoding=""UTF-8"" ?>
<PcashDoc xmlns=""http://payment.payeasy.com.tw/pcash/""
xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
xsi:schemaLocation=""http://payment.payeasy.com.tw/pcash/D000-request.xsd"">
	<DocHead>
	<ServiceCode>D000</ServiceCode>
	<MerchantId>M0010</MerchantId>
	<MerchantTrxCode>{0}</MerchantTrxCode>
	</DocHead>
	<DocContent>
	<PezMemberNum>{1}</PezMemberNum>
	<DeductionAmount>{2}</DeductionAmount>
	<ItemList>
	<Item>
		<ProductName>{3}</ProductName>
<Quantity>1</Quantity>
		<UnitPrice>{2}</UnitPrice>
	</Item >
	
	</ItemList>
	</DocContent>
</PcashDoc>
";

            return string.Format(context,transId,PEZID,cost.ToString(),ItemName);
        }

        public static string GetDeChargeContent(string transId,string authCode,decimal cost)
        {
            string context = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
<PcashDoc xmlns=""http://payment.payeasy.com.tw/pcash/""
xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
xsi:schemaLocation=""http://payment.payeasy.com.tw/pcash/C001-request.xsd"">
	<DocHead>
	<ServiceCode>C001</ServiceCode>
	<MerchantId>M0010</MerchantId>
	<MerchantTrxCode>{0}</MerchantTrxCode>
	</DocHead>
	<DocContent>
	<PezAuthCode>{1}</PezAuthCode>
	<CancelAmount>{2}</CancelAmount>
	</DocContent>
</PcashDoc>

";
            return string.Format(context, transId, authCode, cost.ToString("F0"));

        }
    }
}

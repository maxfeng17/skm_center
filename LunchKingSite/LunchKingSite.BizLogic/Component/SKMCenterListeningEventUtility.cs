﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component.RocketMQEntity;
using ons;
using Newtonsoft.Json;
using System.Configuration;
using System.Net;

namespace LunchKingSite.BizLogic.Component
{
    public class SKMCenterListeningEventUtility
    {
        public static SKMCenterRocketMQActionResultEntity ReciveDataAndAction(Message value)
        {
            bool actionSuccess = true;

            string result = string.Empty;

            //取得tag資料
            string tag = value.getTag();


            //取得body
            Byte[] text = Encoding.Default.GetBytes(value.getBody());

            string body = Encoding.UTF8.GetString(text);

            //商品上架
            if (tag.ToUpper().Equals("SKM_ITEM_ON_SHELF_APP"))
            {
                try
                {
                    SKMItemEntity sKMItemEntity = JsonConvert.DeserializeObject<SKMItemEntity>(body);

                    string skmItemOnShelfAppUri = ConfigurationManager.AppSettings["SKMItemOnShelfAppUri"];

                    string address = string.Format("{0}?itemId={1}", skmItemOnShelfAppUri, sKMItemEntity.itemId);

                    using (WebClient wc = new WebClient())
                    {
                        wc.Encoding = Encoding.UTF8;

                        NameValueCollection nvcData = new NameValueCollection();

                        byte[] bResult = wc.UploadValues(address, nvcData);

                        result = Encoding.UTF8.GetString(bResult);
                    }


                }
                catch (Exception ex)
                {
                    actionSuccess = false;

                    result = ex.ToString();
                }
            }

            //商品下架
            if (tag.ToUpper().Equals("SKM_ITEM_OFF_SHELF_APP"))
            {
                try
                {
                    SKMItemEntity sKMItemEntity = JsonConvert.DeserializeObject<SKMItemEntity>(body);

                    string skmItemOffShelfAppUri = ConfigurationManager.AppSettings["SKMItemOffShelfAppUri"];

                    string address = string.Format("{0}?itemId={1}", skmItemOffShelfAppUri, sKMItemEntity.itemId);

                    using (WebClient wc = new WebClient())
                    {
                        wc.Encoding = Encoding.UTF8;

                        NameValueCollection nvcData = new NameValueCollection();

                        byte[] bResult = wc.UploadValues(address, nvcData);

                        result = Encoding.UTF8.GetString(bResult);
                    }

                }
                catch (Exception ex)
                {
                    actionSuccess = false;

                    result = ex.ToString();
                }
            }

            //渠道類目創建或修改
            if (tag.ToUpper().Equals("SKM_SHOP_CATEGORY_CREATE_APP") || tag.ToUpper().Equals("SKM_SHOP_CATEGORY_UPDATE_APP"))
            {
                try
                {
                    SKMCenterRocketMQCategoryEntity skmCenterRocketMQCategoryEntity = JsonConvert.DeserializeObject<SKMCenterRocketMQCategoryEntity>(body);

                    SKMCenterCategoryUtility skmCenterCategoryUtility = new SKMCenterCategoryUtility();

                    result = skmCenterCategoryUtility.CreateOrModifyCategory(skmCenterRocketMQCategoryEntity.id);
                }
                catch (Exception ex)
                {
                    actionSuccess = false;

                    result = ex.ToString();
                }
            }

            //渠道類目刪除
            if (tag.ToUpper().Equals("SKM_SHOP_CATEGORY_DELETE_APP"))
            {
                try
                {
                    SKMCenterRocketMQCategoryEntity skmCenterRocketMQCategoryEntity = JsonConvert.DeserializeObject<SKMCenterRocketMQCategoryEntity>(body);

                    SKMCenterCategoryUtility skmCenterCategoryUtility = new SKMCenterCategoryUtility();

                    result = skmCenterCategoryUtility.DeleteCategory(skmCenterRocketMQCategoryEntity.id);
                }
                catch (Exception ex)
                {
                    actionSuccess = false;

                    result = ex.ToString();
                }
            }

            //渠道類目綁定
            if (tag.ToUpper().Equals("SKM_SHOP_CATEGORY_BINDING_APP"))
            {
                try
                {
                    SKMCenterShopCategoryBindingEntity skmCenterShopCategoryBindingEntity = JsonConvert.DeserializeObject<SKMCenterShopCategoryBindingEntity>(body);

                    SKMCenterCategoryUtility skmCenterCategoryUtility = new SKMCenterCategoryUtility();

                    bool setReturn = skmCenterCategoryUtility.setExternalDealCategory(skmCenterShopCategoryBindingEntity.itemId, skmCenterShopCategoryBindingEntity.categoryId.ToString(), true);

                    if (!setReturn)
                    {
                        throw new Exception("綁定失敗，查無商品資料");
                    }
                }
                catch (Exception ex)
                {
                    actionSuccess = false;

                    result = ex.ToString();
                }
            }

            //渠道類目取消綁定
            if (tag.ToUpper().Equals("SKM_SHOP_CATEGORY_UNBINDING_APP"))
            {
                try
                {
                    SKMCenterShopCategoryBindingEntity skmCenterShopCategoryBindingEntity = JsonConvert.DeserializeObject<SKMCenterShopCategoryBindingEntity>(body);

                    SKMCenterCategoryUtility skmCenterCategoryUtility = new SKMCenterCategoryUtility();

                    bool setReturn = skmCenterCategoryUtility.setExternalDealCategory(skmCenterShopCategoryBindingEntity.itemId, skmCenterShopCategoryBindingEntity.categoryId.ToString(), false);

                    if (!setReturn)
                    {
                        throw new Exception("綁定失敗，查無商品資料");
                    }
                }
                catch (Exception ex)
                {
                    actionSuccess = false;

                    result = ex.ToString();
                }
            }

            SKMCenterRocketMQActionResultEntity skmCenterRocketMQActionResultEntity = new SKMCenterRocketMQActionResultEntity();

            skmCenterRocketMQActionResultEntity.Success = actionSuccess;

            skmCenterRocketMQActionResultEntity.Message = result;

            return skmCenterRocketMQActionResultEntity;

        }

    }
}

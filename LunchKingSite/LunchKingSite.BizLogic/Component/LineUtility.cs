﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Component
{
    public class LineUtility
    {
        public const string _LINE_API_VER = "v2.1/";

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        static ILog logger = LogManager.GetLogger(typeof(FacebookUtility));

        public static string GetGameOAuhToken(string returnCode)
        {
            string redirectUri = Helper.CombineUrl(config.SiteUrl, config.GameLineAuthRedirectUri);
            string applicationId = config.GameLineId;
            string applicationSecret = config.GameLineApplicationSecret;
            return GetOAuhToken(returnCode, redirectUri, applicationId, applicationSecret);
        }

        public static string GetLoginOAuhToken(string returnCode)
        {
            string redirectUri = Helper.CombineUrl(config.SiteUrl, config.LineAuthRedirectUri);
            string applicationId = config.LineApplicationId;
            string applicationSecret = config.LineApplicationSecret;
            return GetOAuhToken(returnCode, redirectUri, applicationId, applicationSecret);
        }

        public static string GetLoginOAuhToken(string returnCode, string redirectUri)
        {
            string applicationId = config.LineApplicationId;
            string applicationSecret = config.LineApplicationSecret;
            return GetOAuhToken(returnCode, redirectUri, applicationId, applicationSecret);
        }


        private static string GetOAuhToken(string returnCode, string redirectUri, string applicationId, string applicationSecret)
        {
            Dictionary<string, string> tokens = new Dictionary<string, string>();
            try
            {
                var request = GetRequest(returnCode, redirectUri, applicationId, applicationSecret);

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    string retVal = null;
                    if (response == null)
                    {
                        throw new Exception("response is null");
                    }
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        retVal = reader.ReadToEnd();
                    }
                    if (string.IsNullOrEmpty(retVal))
                    {
                        throw new Exception("retVal is null or empty");
                    }

                    dynamic ret = ProviderFactory.Instance().GetSerializer().DeserializeDynamic(retVal);
                    tokens.Add("access_token", (string)ret.access_token);
                }
            }
            catch (Exception ex)
            {
                logger.InfoFormat(
                    "GetOAuhToken code:{0} redirectUri:{1} applicationId:{2} applicationSecret:{3} fail. ex={4}"
                    , returnCode, redirectUri, applicationId, applicationSecret, ex);
            }
            return tokens["access_token"];
        }

        public static string LineLoginUrl(string state, string redirectUri)
        {
            string retUrl = Helper.CombineUrl(config.SiteUrl, redirectUri);
            return string.Format(
                "https://access.line.me/oauth2/{0}authorize?response_type=code&client_id={1}&redirect_uri={2}&scope={3}&state={4}",
                LineUtility._LINE_API_VER,
                config.LineApplicationId, retUrl, LineUser.DefaultScope,
                state);
        }

        private static HttpWebRequest GetRequest(string returnCode, string redirectUri, string applicationId,
            string applicationSecret)
        {
            HttpWebRequest request =
                HttpWebRequest.Create("https://api.line.me/oauth2/v2.1/token") as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            NameValueCollection postDatas = System.Web.HttpUtility.ParseQueryString(string.Empty);
            postDatas.Add("grant_type", "authorization_code");
            postDatas.Add("redirect_uri", redirectUri);
            postDatas.Add("client_id", applicationId);
            postDatas.Add("client_secret", applicationSecret);
            postDatas.Add("code", returnCode);
            byte[] byteArray = Encoding.UTF8.GetBytes(postDatas.ToString());
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            return request;
        }
    }
}

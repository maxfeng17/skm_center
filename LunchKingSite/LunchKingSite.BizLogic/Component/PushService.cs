﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.QueueModels;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;

namespace LunchKingSite.BizLogic.Component
{
    public class GcmNotification2 : GcmNotification
    {
        public override string GetJson()
        {
            JObject jobject1 = new JObject();
            if (!string.IsNullOrEmpty(this.CollapseKey))
                jobject1["collapse_key"] = (JToken)this.CollapseKey;

            jobject1["notification"] = (JToken)new JArray((object[])this.RegistrationIds.ToArray());

            jobject1["registration_ids"] = (JToken)new JArray((object[])this.RegistrationIds.ToArray());

            if (!string.IsNullOrEmpty(this.JsonData))
            {
                JObject jobject2 = JObject.Parse(this.JsonData);
                if (jobject2 != null)
                {
                    if (jobject2["title"] == null)
                    {
                        jobject1["notification"] = JObject.Parse(string.Format("{{\"title\":\"\",\"body\":\"{0}\"}}",
                            jobject2["message"]));
                    }
                    else
                    {
                        jobject1["notification"] = JObject.Parse(string.Format("{{\"title\":\"{0}\",\"body\":\"{1}\"}}",
                            jobject2["title"],
                            jobject2["message"]));
                    }


                    jobject1["data"] = (JToken)jobject2;


                }
            }

            return jobject1.ToString();
        }

        //public override string ToString()
        //{
        //    return this.GetJson();
        //}
    }
    /// <summary>
    /// Facade內部推撥類別
    /// </summary>
    public class PushService : IDisposable
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(NotificationFacade));
        private static INotificationProvider np;
        private static IPponProvider pp;
        private static IPponEntityProvider pep;

        private static IMemberProvider mp;
        private static ISysConfProvider config;
        private static ILocationProvider lp;
        private static ISerializer serializer = ProviderFactory.Instance().GetSerializer();
        private static readonly int _androidRegistrationIdRange;

        private const string tickerText = "17Life";
        private const string contentTitle = "17Life";
        public const string PushMGMCardCardType = "cardType";
        public const string PushMGMCardMsgId = "msgId";
        public const string PushMGMCardShowTab = "showTab";

        public const string _PERSON_PUSH_SERVICE = "personPushService";

        public delegate void DeviceSubscriptionExpiredHandler(object sender, string expiredDeviceSubscriptionId, DateTime timestamp, INotification notification);
        public event DeviceSubscriptionExpiredHandler OnDeviceSubscriptionExpired;

        public delegate void DeviceSubscriptionChangedHandler(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification);
        public event DeviceSubscriptionChangedHandler OnDeviceSubscriptionChanged;

        public delegate void NotificationSentHandler(object sender, INotification notification);
        public event NotificationSentHandler OnNotificationSent;

        //public delegate void NotificationFailedHandler(object sender, INotification notification, Exception notificationFailureException);
        //public event NotificationFailedHandler OnNotificationFailed;

        public delegate void ChannelExceptionHandler(object sender, IPushChannel channel, Exception exception);
        public event ChannelExceptionHandler OnChannelException;

        public delegate void ServiceExceptionHandler(object sender, Exception exception);
        public event ServiceExceptionHandler OnServiceException;

        public delegate void ChannelDestroyedHandler(object sender);
        public event ChannelDestroyedHandler OnChannelDestroyed;

        public delegate void ChannelCreatedHandler(object sender, IPushChannel pushChannel);
        public event ChannelCreatedHandler OnChannelCreated;

        PushApp thePush;

        private PushBroker apnsBroker;
        private PushServiceSettings ApnsServiceSettings
        {
            get
            {
                var pushServiceSettings = new PushServiceSettings()
                {
                    IdleTimeout = TimeSpan.FromSeconds(10),
                    NotificationSendTimeout = 10 * 1000,
                    AutoScaleChannels = false,
                    MaxNotificationRequeues = 1, //Request失敗重發次數
                    Channels = config.IOSPushChannelNumber,//NotificationChannel數                    
                };
                return pushServiceSettings;
            }
        }

        private PushBroker gcmBroker;
        private PushServiceSettings GcmServiceSettings
        {
            get
            {
                var pushServiceSettings = new PushServiceSettings()
                {
                    IdleTimeout = TimeSpan.FromSeconds(10),
                    NotificationSendTimeout = 10 * 1000,
                    AutoScaleChannels = false,
                    MaxNotificationRequeues = 1, //Request失敗重發次數                    
                    Channels = config.AndroidPushChannelNumber
                };
                return pushServiceSettings;
            }
        }
        public bool IsStartUp { get; set; }


        static PushService()
        {
            np = ProviderFactory.Instance().GetProvider<INotificationProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
            config = ProviderFactory.Instance().GetConfig();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            _androidRegistrationIdRange = config.AndroidRegistrationIdRange;
        }

        /// <summary>
        /// 建構子
        /// </summary>
        public PushService()
        {
            apnsBroker = new PushBroker();
            apnsBroker.OnNotificationSent += NotificationSent;
            apnsBroker.OnChannelException += ChannelException;
            apnsBroker.OnServiceException += ServiceException;
            apnsBroker.OnNotificationFailed += NotificationFailed;
            apnsBroker.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            apnsBroker.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            apnsBroker.OnChannelCreated += ChannelCreated;
            apnsBroker.OnChannelDestroyed += ChannelDestroyed;

            #region RegisterApplePushChannelSettings

            //apple APNS SERVER Request 相關設定
            /*注意：針對不同iOS app需要有不同的cert*/

            //todo: 不同cert的設定載入
            string iosPushP12FilePath = config.IOSPushP12FilePath;
            string iosPushP12FileName = config.IOSPushP12FileName;
            string iosPushPassword = config.IOSPushP12FilePassword;
            bool iosOfficialApsHost = config.IOSOfficialApsHost;

            logger.Info("IOSPushChannelNumber 設定為: " + config.IOSPushChannelNumber);

            try
            {
                var appleCert = File.ReadAllBytes(Path.Combine(iosPushP12FilePath, iosPushP12FileName));
                logger.Info("register iOS cert: " + Path.Combine(iosPushP12FilePath, iosPushP12FileName));
                apnsBroker.RegisterAppleService(
                    new ApplePushChannelSettings(iosOfficialApsHost, appleCert, iosPushPassword, true), this.ApnsServiceSettings);
            }
            catch (Exception ex)
            {
                string msg = "IOSNotification APNS setting Error:" +
                    ex.Message + "\n\n" + ex.StackTrace + "\n" + GetInnerExceptionMsg(ex);
                logger.Info(msg);
                this.IsStartUp = false;
                return;
            }
            //true :正式 gatewaypush.apple.com  false:測試 gateway.sandbox.push.apple.com

            #endregion RegisterApplePushChannelSettings

            #region RegisterAndroidPushChannelSettings

            gcmBroker = new PushBroker();
            gcmBroker.OnNotificationSent += NotificationSent;
            gcmBroker.OnChannelException += ChannelException;
            gcmBroker.OnServiceException += ServiceException;
            gcmBroker.OnNotificationFailed += NotificationFailed;
            gcmBroker.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            gcmBroker.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            gcmBroker.OnChannelCreated += ChannelCreated;
            gcmBroker.OnChannelDestroyed += ChannelDestroyed;

            string senderId = config.AndroidPushSenderID; //873552557680
            string apiAccessApiKey = config.AndroidPushAPIAccessAPIKey; //AIzaSyDKMTGkX26b54BaCTIfWYIJaUaakPH0oQk
            string androidAppPackageName = config.AndroidAppPackageName; //com.google.android.gcm.demo.app
            try
            {
                logger.InfoFormat("register Android cert: senderId={0}, apiKey={1}, package={2}",
                    senderId, apiAccessApiKey, androidAppPackageName);
                var channelSetting = new GcmPushChannelSettings(
                    senderId, apiAccessApiKey, androidAppPackageName);
                channelSetting.OverrideUrl("https://fcm.googleapis.com/fcm/send");
                gcmBroker.RegisterGcmService(
                   channelSetting, this.GcmServiceSettings);
            }
            catch (Exception ex)
            {
                string msg = "AndroidNotification GCM setting Error: " +
                                            ex.Message + "\n\n" + ex.StackTrace + "\n" + GetInnerExceptionMsg(ex);
                logger.Info(msg);

                this.IsStartUp = false;
                return;
            }

            #endregion RegisterAndroidPushChannelSettings

            this.IsStartUp = true;
        }

        public void Dispose()
        {
            Dispose(true);//waitForQueueToFinish
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool waitForQueueToFinish)
        {
            if (this.apnsBroker != null)
            {
                this.apnsBroker.StopAllServices(waitForQueueToFinish);
            }
            this.apnsBroker = null;
            if (this.gcmBroker != null)
            {
                this.gcmBroker.StopAllServices(waitForQueueToFinish);
            }
            this.gcmBroker = null;
        }

        /// <summary>
        /// 此版本為先寫入訊息中心再推播
        /// 推播時會加上 MarkMessageRead 的 Pokeball，提供 MessageId(pushRecordId),
        /// 供App端呼叫訊息己讀取的Api。
        /// </summary>
        /// <param name="push"></param>
        /// <param name="pushOnly"></param>
        /// <param name="deviceTokenCollection">指定推播，就沒有寫入訊息中心，目前是給測試推播使用</param>
        public void PushMessage2(PushApp push, bool pushOnly,
            DeviceTokenCollection deviceTokenCollection = null)
        {
            if (!this.IsStartUp)
            {
                logger.Error("it's impossible that push message2 at IsStartUp is false.");
                return;
            }
            if (push.ActionEventPushMessageId == null)
            {
                throw new Exception("push 沒有指定 ActionEventPushMessageId，資料不全無法進行推播");
            }
            this.thePush = push;
            //發送通知裝置計數
            int iosPushMessageDevices = push.IosPushCount;
            int androidPushMessageDevices = push.AndriodPushCount;

            DateTime now = DateTime.Now;
            List<Tuple<int, string>> androidTokens;
            List<Tuple<int, string>> iOSTokens;
            #region 處理APP訊息中心
            StringBuilder sbLine = new StringBuilder();
            var aepm = mp.ActionEventPushMessageGetByActionEventPushMessageId(push.ActionEventPushMessageId.Value);
            if (deviceTokenCollection == null)
            {
                List<string> filters = GetPushMessageFilter(push);
                sbLine.AppendLine("寫入訊息中心的過瀘SQL:" + string.Join(" OR ", filters.ToArray()));
                try
                {
                    np.NotificationToDevicePushRecordSet(push.ActionEventPushMessageId.Value, filters.ToArray());
                    androidTokens = np.DevicePushRecordMessageTokenGetDict(
                        push.ActionEventPushMessageId.Value, MobileOsType.Android);
                    iOSTokens = np.DevicePushRecordMessageTokenGetDict(
                        push.ActionEventPushMessageId.Value, MobileOsType.iOS);
                }
                catch (Exception ex)
                {
                    androidTokens = new List<Tuple<int, string>>();
                    iOSTokens = new List<Tuple<int, string>>();
                    logger.WarnFormat("訊息中心寫入失敗。 pushId={0}, AepmId={1}, ex={2}",
                        push.Id, push.ActionEventPushMessageId, ex);
                }
                sbLine.AppendLine(string.Format("建立訊息中心資料，費時{0}",
                    (DateTime.Now - now).TotalSeconds.ToString("0.00")));
            }
            else
            {
                if (pushOnly == false)
                {
                    np.NotificationToDevicePushRecordSet(push.ActionEventPushMessageId.Value, deviceTokenCollection,
                        out androidTokens, out iOSTokens);
                }
                else
                {
                    androidTokens = deviceTokenCollection.Where(t => t.MobileOsType.Equals((int)MobileOsType.Android))
                        .Select(t => new Tuple<int, string>(0, t.Token)).ToList();
                    iOSTokens = deviceTokenCollection.Where(t => t.MobileOsType.Equals((int)MobileOsType.iOS))
                        .Select(t => new Tuple<int, string>(0, t.Token)).ToList();
                }
            }

            #endregion 處理APP訊息中心

            #region 處理Android推播

            now = DateTime.Now;
            sbLine.AppendLine("推播Android開始");

            try
            {
                int deviceMsgCnt = AndroidPushMessage2(androidTokens, push, aepm);
                androidPushMessageDevices += deviceMsgCnt;
            }
            catch (Exception ex)
            {
                logger.Error("NotificationFacade AndroidPushMessage error. : " + ex.Message + "<br/>" + ex.StackTrace);
            }
            sbLine.AppendLine(string.Format("推播Android {0}，{1}({2})則，佇列費時{3}秒",
                push.Id, 
                androidPushMessageDevices, 
                androidPushMessageDevices, 
                (DateTime.Now - now).TotalSeconds.ToString("0.00")));

            #endregion 處理Android推播

            #region 處理IOS推播

            now = DateTime.Now;
            try
            {
                var deviceMsgCnt = IOSPushMessage2(iOSTokens, push, aepm);
                iosPushMessageDevices += deviceMsgCnt;

            }
            catch (Exception ex)
            {
                logger.Error("NotificationFacade IOSPushMessage error. : " + ex.Message + "<br/>" + ex.StackTrace);
            }

            sbLine.AppendLine(string.Format("推播iOS {0}，{1}({2})則，佇列費時{3}秒",
                push.Id, 
                iosPushMessageDevices, 
                iosPushMessageDevices, 
                (DateTime.Now - now).TotalSeconds.ToString("0.00")));

            #endregion 處理IOS推播


            if (pushOnly)
            {
                DateTime apnsCompletedTime;
                DateTime gcmCompletedTime;
                PingStatus(out apnsCompletedTime, out gcmCompletedTime);
            }
            else
            {
                // 紀錄推播數
                push.IosPushCount = iosPushMessageDevices;
                push.AndriodPushCount = androidPushMessageDevices;
                push.SubscribeCount = iosPushMessageDevices + androidPushMessageDevices;
                push.RealPushTime = DateTime.Now;

                np.PushAppSet(push);



                now = DateTime.Now;
                //等待回傳完成推撥時間
                DateTime apnsCompletedTime;
                DateTime gcmCompletedTime;

                PingStatus(out apnsCompletedTime, out gcmCompletedTime);

                sbLine.AppendLine(string.Format("推播iOS {0} 完成，費時{1}秒",
                    push.Id, (apnsCompletedTime - now).TotalSeconds.ToString("0.00")));

                sbLine.AppendLine(string.Format("推播Android {0} 完成，費時{1}秒",
                    push.Id, (gcmCompletedTime - now).TotalSeconds.ToString("0.00")));

                LogManager.GetLogger(LineAppender._LINE).InfoFormat(sbLine.ToString());

                push.CompleteTime = DateTime.Now;

                np.PushAppSet(push);
            }

        }

        /// <summary>
        /// 推播給個人
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="messageType"></param>
        /// <param name="args"></param>
        /// <param name="pushMethod"></param>
        public void PushMemberMessage(int userId, string title, string message, ActionEventType messageType,
            Dictionary<string, object> args, PushMethodType pushMethod)
        {
            if (!this.IsStartUp)
            {
                logger.Error("it's impossible that push member message at IsStartUp is false.");
                return;
            }

            //step 1.先建立 action_event
            ActionEventPushMessage aepm = new ActionEventPushMessage()
            {
                Subject = title,
                Content = message,
                EventType = (int)messageType,
                Status = true,
                CreateTime = DateTime.Now,
                SendStartTime = DateTime.Now,
                SendEndTime = DateTime.Now.AddDays(-1),
                CreateId = userId
            };
            aepm.IsLoaded = true;
            if (messageType == ActionEventType.RemotePushMGMCard)
            {
                aepm.CardType = (int)args[PushMGMCardCardType];
                aepm.MsgId = (string)args[PushMGMCardMsgId];
            }
            else if (messageType == ActionEventType.RemotePushCustomerService)
            {
                aepm.ServiceType = (int)args["serviceType"];
                aepm.ServicePara = (string)args["servicePara"];
            }
            else if (messageType == ActionEventType.ShowOrderDetail)
            {
                if (args["orderGuid"] is string)
                {
                    aepm.OrderGuid = Guid.Parse(args["orderGuid"].ToString());
                }
                else
                {
                    aepm.OrderGuid = (Guid)args["orderGuid"];
                }
            }
            else if (messageType == ActionEventType.RemotePushCustomUrl)
            {
                aepm.CustomUrl = (string)args["customUrl"];
            }
            if (Helper.IsFlagSet(pushMethod, PushMethodType.Store))
            {
                mp.ActionEventPushMessageSet(aepm);
            }

            //不打算新增，只是new出來套用原本機制
            PushApp push = new PushApp
            {
                ActionEventPushMessageId = aepm.Id,
                Description = message
            };

            UserDeviceInfoModel deviceInfo = NotificationFacade.GetDefaultUserDeviceInfoModel(userId);
            if (deviceInfo == null)
            {
                logger.InfoFormat("{0} 沒有任何裝置. 無法推播", userId);
            }
            else
            {
                if (Helper.IsFlagSet(pushMethod, PushMethodType.Push))
                {
                    Dictionary<string, object> messageData = BuildNewMessageData(push, aepm);
                    try
                    {
                        if (deviceInfo.OsType == MobileOsType.Android)
                        {
                            AndroidPushMessage(new List<string>(new[] { deviceInfo.Token }), messageData);
                        }
                        else
                        {
                            IOSPushMessage(new List<string>(new[] { deviceInfo.Token }), messageData, push.Description);
                        }
                        logger.InfoFormat("推播訊息:{0}, userId={1}, os={2}, deviceId={3}, token={4}",
                            message, userId,
                            deviceInfo.OsType, deviceInfo.DeviceId, deviceInfo.Token);
                    }
                    catch (Exception ex)
                    {
                        logger.Info("NotificationFacade AndroidPushMessage error. : " + ex.Message + "<br/>" + ex.StackTrace);
                    }

                }
            }

            //step 2 發送紀錄 device_push_record
            if (deviceInfo != null && Helper.IsFlagSet(pushMethod, PushMethodType.Store))
            {
                DevicePushRecord dpr = new DevicePushRecord
                {
                    ActionId = aepm.Id,
                    MemberId = userId,
                    CreateTime = DateTime.Now,
                    DeviceId = deviceInfo.DeviceId,
                    IdentifierType = 2
                };
                lp.DevicePushRecordSet(dpr);
                logger.InfoFormat("寫入訊息中心:{0}, userId={1}, os={2}, deviceId={3}, token={4}",
                    message, userId,
                    deviceInfo.OsType, deviceInfo.DeviceId, deviceInfo.Token);
            }

        }

        public void PushMemberMessageByMq(int userId, string title, string message, ActionEventType messageType, Dictionary<string, object> args)
        {
            logger.InfoFormat("PushMemberMessageByMq(). userId={0},message={1},messageType={2},args={3}",
                userId, message, messageType.ToString(), JsonConvert.SerializeObject(args));

            if (!this.IsStartUp)
            {
                logger.Error("it's impossible that PushMemberMessageByMq at IsStartUp is false.");
                return;
            }

            #region  step 1.先建立 action_event (不在此set 進入mq才做儲存)

            ActionEventPushMessage aepm = new ActionEventPushMessage()
            {
                Subject = title,
                Content = message,
                EventType = (int)messageType,
                Status = true,
                CreateTime = DateTime.Now,
                SendStartTime = DateTime.Now,
                SendEndTime = DateTime.Now.AddDays(-1),
                CreateId = userId
            };
            aepm.IsLoaded = true;
            if (messageType == ActionEventType.RemotePushMGMCard)
            {
                aepm.CardType = (int)args[PushMGMCardCardType];
                aepm.MsgId = (string)args[PushMGMCardMsgId];
            }
            if (messageType == ActionEventType.RemotePushCustomerService)
            {
                aepm.ServiceType = (int)args["serviceType"];
                aepm.ServicePara = (string)args["servicePara"];
            }
            if (messageType == ActionEventType.ShowOrderDetail)
            {
                aepm.OrderGuid = (Guid)args["orderGuid"];
            }

            #endregion

            //不打算新增，只是new出來套用原本機制
            PushApp push = new PushApp
            {
                ActionEventPushMessageId = aepm.Id,
                Description = message
            };

            UserDeviceInfoModel deviceInfo = NotificationFacade.GetDefaultUserDeviceInfoModel(userId);
            if (deviceInfo == null)
            {
                logger.InfoFormat("{0} 沒有任何裝置.", userId);
            }
            else
            {
                //塞入MQ
                Dictionary<string, object> messageData = BuildNewMessageData(push, aepm);

                IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
                var appPushMq = new AppPushMq
                {
                    UserId = userId,
                    Aepm = aepm,
                    DeviceInfo = deviceInfo,
                    MessageData = messageData,
                    Description = push.Description,
                };
                mqp.Send(AppPushMq._QUEUE_NAME, appPushMq);
                logger.InfoFormat("PushMemberMessageByMq .丟入一則工作時間推播訊息至MQ. para= {0}", JsonConvert.SerializeObject(appPushMq));

            }
        }

        public bool PushMemberMessageByQueued(int userId, string title, string message, ActionEventType messageType, Dictionary<string, object> args)
        {
            UserDeviceInfoModel deviceInfo = NotificationFacade.GetDefaultUserDeviceInfoModel(userId);
            if (deviceInfo == null)
            {
                return false;
            }
            try
            {
                pep.QueuedMemberPushMessageSet(new Core.Models.PponEntities.QueuedMemberPushMessage
                {
                    Title = title,
                    Message = message,
                    MessageType = messageType,
                    MessageData = JsonConvert.SerializeObject(args),
                    Status = 0,
                    UserId = userId,
                    AcceptableStartHour = 8,
                    AcceptableEndHour = 22
                });

                return true;
            }
            catch (Exception ex)
            {
                logger.Warn("PushMemberMessageByQueued error.", ex);
                return false;
            }
        }

        private List<string> GetPushMessageFilter(PushApp push)
        {
            //Get cityIds
            int n;
            int[] cityIds = push.PushArea.Split(',').Select(s => int.TryParse(s, out n) ? n : 0).ToArray();

            //build filter
            List<string> filters = new List<string>();

            string subscription_notice = SubscriptionNotice.Schema.Provider.DelimitDbName(SubscriptionNotice.Schema.TableName);
            string subscription_notice_channel_category_id = string.Format("{0}.{1}",
                subscription_notice, SubscriptionNotice.Schema.Provider.DelimitDbName(SubscriptionNotice.Columns.ChannelCategoryId));
            string subscription_notice_subcategory_id = string.Format("{0}.{1}",
                subscription_notice, SubscriptionNotice.Schema.Provider.DelimitDbName(SubscriptionNotice.Columns.SubcategoryId));

            foreach (int cityId in cityIds)
            {

                PponCity pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId);

                if (pponCity == null)
                {
                    continue;
                }

                string filter = string.Empty;

                filter += (pponCity.ChannelId != null
                            ? string.Format(" {0} = {1} ", subscription_notice_channel_category_id, pponCity.ChannelId)
                            : string.Empty);
                filter += (pponCity.ChannelAreaId != null
                            ? string.Format(" {1} {2} = {0} ", pponCity.ChannelAreaId,
                                string.IsNullOrWhiteSpace(filter)
                                ? string.Empty
                                : "and", subscription_notice_subcategory_id)
                            : string.Empty);

                if (!string.IsNullOrWhiteSpace(filter) && !filters.Contains(filter))
                {
                    filters.Add(string.Format("({0})", filter));
                }
            }

            return filters;
        }

        /// <summary>
        /// 推播PcpAssignment的推播訊息
        /// </summary>
        /// <param name="pcpa"></param>
        public void PushMessage(PcpAssignment pcpa)
        {
            if (pcpa.SendType != (int)AssignmentSendType.Push)
            {
                throw new Exception("PushMessage只處理推播");
            }

            //todo:推播PcpAssignment的推播訊息
            ViewPcpAssignmentMemberDeviceTokenCollection deviceTokens = np.ViewPcpAssignmentMemberDeviceTokenGetList(pcpa.Id);
            //推撥訊息內文
            string message = pcpa.Message.Length > config.NotificationDescriptionLimit ? pcpa.Message.Substring(0, config.NotificationDescriptionLimit) : pcpa.Message;

            foreach (var deviceToken in deviceTokens)
            {
                try
                {
                    this.PushMessage(deviceToken, message);

                    //記錄推撥發送時間
                    np.PcpAssignmentMemberSetSendTime(deviceToken.Id, DateTime.Now);
                }
                catch (Exception ex)
                {
                    logger.Error("NotificationFacade PushMessage error. : " + ex.Message + "<br/>" + ex.StackTrace);
                }
            }

            //記錄推撥完成時間
            //pcpa.CompleteTime = PingStatus();
            np.PcpAssignmentSetStatus(pcpa.Id, AssignmentStatus.Complete);
        }


        /// <summary>
        /// 推撥單則訊息
        /// </summary>
        /// <param name="deviceToken"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool PushMessage(ViewPcpAssignmentMemberDeviceToken deviceToken, string message)
        {
            //todo:message的客製化

            switch ((MobileOsType)deviceToken.MobileOsType)
            {
                case MobileOsType.iOS:
                    if (config.AppIOSNotification)
                    {
                        AppleNotification notification = new AppleNotification().ForDeviceToken(deviceToken.Token)
                                .WithAlert(message)
                                .WithSound("default")
                                .WithBadge(1);
                        //todo:塞入參數
                        //foreach (var data in messageData)
                        //{
                        //    notification.WithCustomItem(data.Key, data.Value);
                        //}
                        this.apnsBroker.QueueNotification(notification);
                    }
                    break;
                case MobileOsType.Android:
                    if (config.AppAndroidNotification)
                    {
                        Dictionary<string, object> messageData = new Dictionary<string, object>();

                        if (!string.IsNullOrWhiteSpace(message))
                        {
                            messageData.Add("message", message);
                        }


                        // convert the object into a string
                        var jsonString = new JsonSerializer().Serialize(messageData);

                        this.gcmBroker.QueueNotification(new GcmNotification().ForDeviceRegistrationId(deviceToken.Token)
                                .WithCollapseKey("NONE")
                                .WithJson(jsonString));
                    }
                    break;

            }

            return true;
        }

        /// <summary>
        /// 檢查推撥結束狀態
        /// </summary>
        /// <returns>推撥結束時間</returns>
        private void PingStatus(out DateTime apnsCompletedTime, out DateTime gcmCompletedTime)
        {
            if (thePush == null)
            {
                logger.Info("Push PingStatus");
            }
            else
            {
                logger.InfoFormat("Push {0} PingStatus", thePush.Id);
            }
            var apnsTask = PingStatusApns();
            var gcmTask = PingStatusGcm();
            Task.WaitAll(apnsTask, gcmTask);
            apnsCompletedTime = apnsTask.Result;
            gcmCompletedTime = gcmTask.Result;
        }

        private Task<DateTime> PingStatusApns()
        {
            return Task.Factory.StartNew(delegate ()
            {
                try
                {
                    int elapsedSecs = 0;
                    bool allDone = false;
                    List<IPushService> svcs = this.apnsBroker.GetAllRegistrations().ToList();
                    long preNum = 0, theNum = 0;
                    while (!allDone)
                    {
                        long queueLength = svcs.Sum(t => ((PushServiceBase)t).QueueLength);
                        allDone = queueLength == 0;
                        Task.Delay(1000).Wait();
                        elapsedSecs++;
                        if (elapsedSecs % 60 == 1)
                        {
                            preNum = queueLength;
                        }
                        if (elapsedSecs % 60 == 0)
                        {
                            theNum = queueLength;
                            if (thePush != null)
                            {
                                logger.InfoFormat("推播 {0} 經過 {1} 分, 還有 {2} 筆未發送, 每分鍾發送 {3} 筆.",
                                    thePush.Id, elapsedSecs / 60, queueLength, preNum - theNum);
                            }
                            else
                            {
                                logger.InfoFormat("推播經過 {0} 分, 還有 {1} 筆未發送, 每分鍾發送 {2} 筆.",
                                    elapsedSecs / 60, queueLength, preNum - theNum);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("NotificationFacade apnsPingStatus error. : " + ex.Message + "<br/>" + ex.StackTrace);
                }

                return DateTime.Now;
            });
        }

        private Task<DateTime> PingStatusGcm()
        {
            return Task.Factory.StartNew(delegate ()
            {
                try
                {
                    bool allDone = false;
                    List<IPushService> svcs = this.gcmBroker.GetAllRegistrations().ToList();

                    while (!allDone)
                    {
                        allDone = svcs.All(t => ((PushServiceBase)t).QueueLength == 0);
                        Task.Delay(1000).Wait();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("NotificationFacade gcmPingStatus error. : " + ex.Message + "<br/>" + ex.StackTrace);
                }

                return DateTime.Now;
            });
        }

        /// <summary>
        /// ios 正式推播
        /// </summary>
        /// <param name="messageTokens"></param>
        /// <param name="push"></param>
        /// <param name="aepm"></param>
        /// <returns></returns>
        public int IOSPushMessage2(List<Tuple<int, string>> messageTokens, PushApp push, ActionEventPushMessage aepm)
        {
            if (!this.IsStartUp)
            {
                return 0;
            }

            if (messageTokens == null && messageTokens.Count == 0)
            {
                return 0;
            }

            //發送通知裝置計數
            int iosPushMessageDevices = 0;

            var messageDataBase = BuildNewMessageDataBase(push, aepm);
            //iOS不傳description
            foreach (Tuple<int, string> pair in messageTokens)
            {
                int devicePushId = pair.Item1;
                string token = pair.Item2;

                //增加判斷 Token 非空or null判定 避免丟空Token問題
                if (!string.IsNullOrEmpty(token))
                {
                    AppleNotification notification = new AppleNotification().ForDeviceToken(token)
                        .WithAlert(messageDataBase.message)
                        .WithSound("default")
                        .WithBadge(1);

                    //塞入參數
                    if (messageDataBase.pushId != 0)
                    {
                        notification.WithCustomItem("pushid", messageDataBase.pushId);
                    }
                    List<dynamic> pokeballs = new List<dynamic>();
                    if (messageDataBase.pokeball != null)
                    {
                        pokeballs.Add(messageDataBase.pokeball);
                    }
                    if (devicePushId > 0)
                    {
                        pokeballs.Add(NotificationFacade.GetPokeballForReadMessage(devicePushId));
                    }
                    if (pokeballs.Count > 0)
                    {
                        notification.WithCustomItem("PokeballList", JToken.FromObject(pokeballs));
                    }

                    this.apnsBroker.QueueNotification(notification);

                    iosPushMessageDevices++;
                }
            }
            return iosPushMessageDevices;
        }

        /// <summary>
        /// ios 正式推播
        /// </summary>
        /// <param name="iosDriverTokenCol"></param>
        /// <param name="messageData"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public int IOSPushMessage(List<string> iosDriverTokens, Dictionary<string, object> messageData, string description)
        {
            if (!this.IsStartUp)
            {
                return 0;
            }

            //發送通知裝置計數
            int iosPushMessageDevices = 0;
            if (iosDriverTokens != null && iosDriverTokens.Count > 0)
            {
                //iOS不傳description
                foreach (string idriverToken in iosDriverTokens)
                {
                    //增加判斷 Token 非空or null判定 避免丟空Token問題
                    if (!string.IsNullOrEmpty(idriverToken))
                    {
                        AppleNotification notification = new AppleNotification().ForDeviceToken(idriverToken)
                            .WithAlert(description)
                            .WithSound("default")
                            .WithBadge(1);

                        //塞入參數
                        foreach (var data in messageData.Where(x => x.Key != "message"))
                        {
                            if (data.Value.GetType().IsValueType || data.Value.GetType() == typeof(string))
                            {
                                notification.WithCustomItem(data.Key, data.Value);
                            }
                            else
                            {
                                notification.WithCustomItem(data.Key, JToken.FromObject(data.Value));
                            }
                        }
                        this.apnsBroker.QueueNotification(notification);

                        iosPushMessageDevices++;
                    }
                }
            }
            return iosPushMessageDevices;
        }

        /// <summary>
        /// ios 正式推播
        /// </summary>
        /// <param name="iosDriverTokenCol"></param>
        /// <param name="messageData"></param>
        /// <param name="description"></param>
        /// <returns></returns>

        public int AndroidPushMessage(List<string> androidDeviceTokens, Dictionary<string, object> messageData)
        {
            if (!this.IsStartUp)
            {
                return 0;
            }

            //發送通知裝置計數
            int androidPushMessageDevices = 0;
            if (androidDeviceTokens.Count > 0)
            {
                messageData.Add("tickerText", tickerText);
                messageData.Add("contentTitle", contentTitle);
                string message = new JsonSerializer().Serialize(messageData);

                //調整成依_androidRegistrationIdRange筆token 一次發送Request
                GcmNotification2 gcmNotification = new GcmNotification2();
                foreach (string deviceToken in androidDeviceTokens)
                {
                    //增加判斷 Token 非空or null判定 避免丟空Token問題
                    if (!string.IsNullOrEmpty(deviceToken))
                    {
                        gcmNotification.RegistrationIds.Add(deviceToken);
                        androidPushMessageDevices++;
                        if ((androidPushMessageDevices % _androidRegistrationIdRange) == 0)
                        {
                            this.gcmBroker.QueueNotification(gcmNotification
                                                            .WithCollapseKey("NONE")
                                                            .WithJson(message));

                            gcmNotification = new GcmNotification2();
                        }
                    }
                }

                if (gcmNotification.RegistrationIds.Count > 0)
                {
                    this.gcmBroker.QueueNotification(gcmNotification.WithJson(message));
                }
            }
            return androidPushMessageDevices;
        }

        /// <summary>
        /// 一次只發送1筆的版本
        /// 此版本因messageId不同，內容也不同，
        /// 因此無法做到一個訊息發送大量裝置
        /// </summary>
        /// <param name="messageTokens"></param>
        /// <param name="push"></param>
        /// <param name="aepm"></param>
        /// <returns></returns>
        public int AndroidPushMessage2(List<Tuple<int, string>> messageTokens, PushApp push, ActionEventPushMessage aepm)
        {
            if (!this.IsStartUp)
            {
                return 0;
            }

            DateTime now = DateTime.Now;
            //發送通知裝置計數
            int androidPushMessageDevices = 0;
            if (messageTokens.Count > 0)
            {
                var messageDataBase = BuildNewMessageDataBase(push, aepm);
                foreach (var pair in messageTokens)
                {
                    string deviceToken = pair.Item2;
                    string message = serializer.Serialize(messageDataBase.CreateMessageData(pushRecordId: pair.Item1));
                    GcmNotification2 gcmNotification = new GcmNotification2();
                    //增加判斷 Token 非空or null判定 避免丟空Token問題
                    if (string.IsNullOrEmpty(deviceToken) == false)
                    {
                        gcmNotification.RegistrationIds.Add(deviceToken);
                        androidPushMessageDevices++;

                        this.gcmBroker.QueueNotification(gcmNotification
                            .WithCollapseKey("NONE")
                            .WithJson(message));
                    }
                }
            }
            logger.Warn("Android 費時: " + (DateTime.Now - now).TotalSeconds.ToString("#.00"));
            return androidPushMessageDevices;
        }

        /// <summary>
        /// android 正式推播
        /// </summary>
        /// <param name="androidDriverTokenCol">發送裝置集合(andriod)</param>
        /// <param name="messageData"></param>
        /// <returns></returns>
        public int AndroidPushMessage(List<DeviceToken> androidDriverTokenCol, Dictionary<string, object> messageData)
        {
            return AndroidPushMessage(androidDriverTokenCol.Select(t => t.Token).ToList(), messageData);
        }

        /// <summary>
        /// 推撥Wrapper方法
        /// </summary>
        /// <typeparam name="TPushNotification"></typeparam>
        /// <param name="notification"></param>
        public void PushNotification<TPushNotification>(TPushNotification notification) where TPushNotification : Notification
        {
            if (this.IsStartUp)
            {
                try
                {
                    if (typeof(TPushNotification) == typeof(AppleNotification))
                    {
                        this.apnsBroker.QueueNotification(notification);
                    }
                    else
                    {
                        this.gcmBroker.QueueNotification(notification);
                    }

                }
                catch (Exception)
                {
                }
            }
        }

        #region Event Handlers
        private void NotificationSent(object sender, INotification notification)
        {
            try
            {
                GcmNotification gcmNotificaiton = notification as GcmNotification;
                if (gcmNotificaiton != null)
                {
                    string tokens = string.Join(",", gcmNotificaiton.RegistrationIds.ToArray());
                    logger.DebugFormat("Android push OK. {0} - {1}", tokens, gcmNotificaiton.JsonData);
                }
                AppleNotification appleNotification = notification as AppleNotification;
                if (appleNotification != null)
                {
                    logger.DebugFormat("iOS push OK. {0} - {1}",
                        appleNotification.DeviceToken,
                        serializer.Serialize(appleNotification.Payload));
                }
                if (this.OnNotificationSent != null)
                {
                    this.OnNotificationSent(sender, notification);
                    return;
                }
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
            }
        }

        private void NotificationFailed(object sender, INotification notification, 
            Exception notificationFailureException)
        {
            try
            {
                GcmNotification gcmNottificaiton = notification as GcmNotification;
                if (gcmNottificaiton != null)
                {
                    string tokens = string.Join(",", gcmNottificaiton.RegistrationIds.ToArray());
                    if (notificationFailureException is ObjectDisposedException)
                    {
                        logger.DebugFormat("Android push fail because {0}. {1} - {2}",
                            notificationFailureException, tokens, gcmNottificaiton.JsonData);
                    }
                    else
                    {
                        logger.InfoFormat("Android push fail because {0}. {1} - {2}",
                            notificationFailureException, tokens, gcmNottificaiton.JsonData);
                    }
                    //不合法的DeviceToken
                    DeviceToken deviceToken = np.DeviceTokenGet(tokens);
                    if (deviceToken.IsLoaded)
                    {
                        deviceToken.TokenStatus = (int)DeviceTokenStatus.Invalid;
                        deviceToken.ModifyTime = DateTime.Now;
                        np.DeviceTokenSet(deviceToken);
                    }
                }
                AppleNotification appleNotification = notification as AppleNotification;
                if (appleNotification != null)
                {
                    NotificationFailureException ex = notificationFailureException as NotificationFailureException;
                    if (ex == null)
                    {
                        logger.InfoFormat("iOS psuh fail because {0}. {1} - {2}",
                            notificationFailureException,
                            appleNotification.DeviceToken,
                            serializer.Serialize(appleNotification.Payload));
                    }
                    else
                    {
                        logger.InfoFormat("iOS psuh fail because {0}. {1} - {2}",
                            ex.ErrorStatusDescription,
                            appleNotification.DeviceToken, serializer.Serialize(appleNotification.Payload));
                    }
                    //過期DeviceToken
                    DeviceToken deviceToken = np.DeviceTokenGet(appleNotification.DeviceToken);
                    if (deviceToken.IsLoaded)
                    {
                        deviceToken.TokenStatus = (int)DeviceTokenStatus.Invalid;
                        deviceToken.ModifyTime = DateTime.Now;
                        np.DeviceTokenSet(deviceToken);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
            }
        }

        private void ChannelException(object sender, IPushChannel channel, Exception exception)
        {
            if (this.OnChannelException != null)
            {
                this.OnChannelException(sender, channel, exception);
                return;
            }
            logger.Info("Channel Exception: " + sender + " -> " + exception.Message +
                "\n\n" + exception.StackTrace + GetInnerExceptionMsg(exception));
            //this.ResetBroker();
        }

        private void ServiceException(object sender, Exception exception)
        {
            if (this.OnServiceException != null)
            {
                this.OnServiceException(sender, exception);
                return;
            }

            logger.InfoFormat(
                "PushService Exception: " + sender + " -> " + exception.Message + "\n\n" +
                exception.StackTrace + GetInnerExceptionMsg(exception));
            //this.ResetBroker();
        }

        private void DeviceSubscriptionExpired(object sender, string expiredDeviceSubscriptionId, DateTime timestamp, INotification notification)
        {
            try
            {
                GcmNotification gcmNottificaiton = notification as GcmNotification;
                if (gcmNottificaiton != null)
                {
                    string tokens = string.Join(",", gcmNottificaiton.RegistrationIds.ToArray());
                    logger.DebugFormat("Android token expired {0}. {1}", tokens, gcmNottificaiton.JsonData);
                }

                AppleNotification appleNotification = notification as AppleNotification;
                if (appleNotification != null)
                {
                    logger.DebugFormat("iOS token expired {0}. {1}",
                        appleNotification.DeviceToken, serializer.Serialize(appleNotification.Payload));
                }

                if (this.OnDeviceSubscriptionExpired != null)
                {
                    OnDeviceSubscriptionExpired(sender, expiredDeviceSubscriptionId, timestamp, notification);
                    return;
                }

                //過期DeviceToken
                DeviceToken deviceToken = np.DeviceTokenGet(expiredDeviceSubscriptionId);
                if (deviceToken.IsLoaded)
                {
                    deviceToken.TokenStatus = (int)DeviceTokenStatus.Expired;
                    deviceToken.ModifyTime = DateTime.Now;
                    np.DeviceTokenSet(deviceToken);
                }
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
            }
        }

        private void ChannelDestroyed(object sender)
        {
            if (this.OnChannelDestroyed != null)
            {
                this.OnChannelDestroyed(sender);
                return;
            }
        }

        private void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            if (this.OnChannelCreated != null)
            {
                this.OnChannelCreated(sender, pushChannel);
                return;
            }
        }

        //v2.0
        private void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            //當發生token的異動時，server端不做任何處理，新版的token更新，一律由app端主動設定。
            if (this.OnDeviceSubscriptionChanged != null)
            {
                OnDeviceSubscriptionChanged(sender, oldSubscriptionId, newSubscriptionId, notification);
                return;
            }
        }
        #endregion

        /// <summary>
        /// [新] 建立推撥訊息字典
        /// </summary>
        /// <param name="push"></param>
        /// <returns></returns>
        public static Dictionary<string, object> BuildNewMessageData(PushApp push, ActionEventPushMessage actionEventPushMsg = null)
        {
            Dictionary<string, object> messageData = new Dictionary<string, object>();

            if (!string.IsNullOrWhiteSpace(push.Description))
            {
                var newDescription = push.Description.Length > config.NotificationDescriptionLimit
                    ? push.Description.Substring(0, config.NotificationDescriptionLimit) : push.Description;

                messageData.Add("message", newDescription);
            }

            if (string.IsNullOrEmpty(actionEventPushMsg.Subject) == false && actionEventPushMsg.Subject != actionEventPushMsg.Content)
            {
                messageData.Add("title", actionEventPushMsg.Subject);
            }

            if (push.Id != 0)
            {
                messageData.Add("pushid", push.Id);
            }

            if (actionEventPushMsg != null || push.ActionEventPushMessageId.HasValue)
            {
                if (actionEventPushMsg == null)
                {
                    actionEventPushMsg = mp.ActionEventPushMessageGetByActionEventPushMessageId(push.ActionEventPushMessageId.Value);
                }
                if (actionEventPushMsg.IsLoaded)
                {
                    var pokeball = NotificationFacade.GetPokeball(actionEventPushMsg);
                    if (pokeball != null)
                    {
                        messageData.Add("PokeballList", new[] { pokeball });
                    }
                }
            }

            return messageData;
        }

        public static MessageDataBase BuildNewMessageDataBase(PushApp push,
            ActionEventPushMessage actionEventPushMsg)
        {
            MessageDataBase messageData = new MessageDataBase();

            if (!string.IsNullOrWhiteSpace(push.Description))
            {
                var newDescription = push.Description.Length > config.NotificationDescriptionLimit
                    ? push.Description.Substring(0, config.NotificationDescriptionLimit) : push.Description;

                messageData.message = newDescription;
            }

            if (push.Id != 0)
            {
                messageData.pushId = push.Id;
            }

            if (actionEventPushMsg != null && actionEventPushMsg.IsLoaded)
            {
                messageData.pokeball = NotificationFacade.GetPokeball(actionEventPushMsg);
            }

            return messageData;
        }

        /// <summary>
        /// 組合推撥訊息
        /// </summary>
        /// <param name="msgs"></param>
        /// <param name="osType"></param>
        /// <param name="extMessageData"></param>
        /// <returns></returns>
        public static string BuildMessage(List<PushMessageViewModel> msgs, MobileOsType osType, out Dictionary<string, object> extMessageData)
        {
            extMessageData = new Dictionary<string, object>();

            if (msgs == null || msgs.Count == 0)
            {
                return string.Empty;
            }

            string singleMsgFormat = config.SubscriptionNoticeSingleMsgFormat;// "您收藏的［{0}］ 即將於 {1} 結束優惠，快把握機會啊！！！";

            //TODO:多檔次收藏推撥格式
            string multiMsgFormat = config.SubscriptionNoticeMultiMsgFormat;// "您收藏的［{0}］ 即將於 {1} 結束優惠，快把握機會啊！！！";

            string message = string.Empty;

            var first = msgs.FirstOrDefault();
            string bid = first.Bid.ToString();

            PushApp push = new PushApp();
            push.BusinessHourGuid = new Guid(bid);
            push.CityId = first.CityId;

            //建立字典
            extMessageData = BuildMessageData(push, msgs.Count);

            //產生訊息
            if (msgs.Count == 1)
            {
                message = string.Format(singleMsgFormat, first.CouponUsage ?? first.AppTitle ?? first.Title ?? first.Name ?? first.SubjectName, first.Bhoe.ToString("MM/dd"));
            }
            else
            {
                message = string.Format(multiMsgFormat, first.CouponUsage ?? first.AppTitle ?? first.Title ?? first.Name ?? first.SubjectName, first.Bhoe.ToString("MM/dd"), msgs.Count - 1);
            }

            switch (osType)
            {
                case MobileOsType.Android:
                    extMessageData.Add("message", message);
                    extMessageData.Add("tickerText", tickerText);
                    extMessageData.Add("contentTitle", contentTitle);
                    return new JsonSerializer().Serialize(extMessageData);
                //return DictionaryToJson(extMessageData);
                case MobileOsType.iOS:
                    return message;
            }
            return string.Empty;
        }

        /// <summary>
        /// [舊] 建立推撥訊息字典
        /// </summary>
        /// <param name="push"></param>
        /// <param name="collectDealCount"></param>
        /// <param name="extraArgs"></param>
        /// <returns></returns>
        public static Dictionary<string, object> BuildMessageData(PushApp push, int collectDealCount = 1,
            Dictionary<string, object> extraArgs = null)
        {
            Dictionary<string, object> messageData;
            if (extraArgs == null)
            {
                messageData = new Dictionary<string, object>();
            }
            else
            {
                messageData = new Dictionary<string, object>(extraArgs);
            }
            if (!string.IsNullOrWhiteSpace(push.Description))
            {
                var newDescription = push.Description.Length > config.NotificationDescriptionLimit
                    ? push.Description.Substring(0, config.NotificationDescriptionLimit) : push.Description;

                messageData.Add("message", newDescription);
            }

            if (push.BusinessHourGuid != null && !push.BusinessHourGuid.Equals(Guid.Empty))
            {
                messageData.Add("bid", push.BusinessHourGuid.Value.ToString());
                messageData.Add("city", GetPushCityName((Guid)push.BusinessHourGuid));
            }
            else if (push.CityId != null)
            {
                messageData.Add("pponcity", push.CityId.Value);
                //城市轉換為頻道的處理
                PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(push.CityId.Value);
                if (city != null)
                {
                    if (city.ChannelId.HasValue)
                    {
                        messageData.Add("channel", city.ChannelId.Value);
                        if (city.ChannelAreaId.HasValue)
                        {
                            messageData.Add("area", city.ChannelAreaId.Value);
                        }
                        else
                        {
                            messageData.Add("area", -1);
                        }
                    }
                }
            }
            else if (push.VourcherId != null)
            {
                messageData.Add("vourcherid", push.VourcherId.Value);
            }
            else if (!string.IsNullOrEmpty(push.SellerId))
            {
                messageData.Add("sellerid", push.SellerId);
            }

            if (push.EventPromoId != null)
            {
                messageData.Add("eventpromoid", push.EventPromoId);
                var promo = pp.GetEventPromo((int)push.EventPromoId);
                if (promo != null)
                {
                    messageData.Add("eventType", promo.EventType);
                }
            }
            else if (push.BrandPromoId.HasValue)
            {
                messageData.Add("brandpromoid", push.BrandPromoId);
                messageData.Add("eventType", (int)EventPromoEventType.CurationTwo);
            }

            if (!string.IsNullOrEmpty(push.CustomUrl))
            {
                messageData.Add("Url", push.CustomUrl);
            }

            if (push.Id != 0)
            {
                messageData.Add("pushid", push.Id);
            }

            //單一檔次到期，使用bid, CityName
            //多檔次到期，使用CollectDealCount參數
            if (collectDealCount > 1)
            {
                messageData.Add("CollectDealCount", collectDealCount);

                if (messageData.ContainsKey("bid"))
                {
                    messageData.Remove("bid");
                }

                if (messageData.ContainsKey("city"))
                {
                    messageData.Remove("city");
                }
            }

            return messageData;
        }

        private static string GetInnerExceptionMsg(Exception e)
        {
            string innerExceptionMsg = string.Empty;
            if (e.InnerException != null)
            {
                innerExceptionMsg = "\n\nInnerException:->" + e.InnerException.Message + "\n\n" + e.InnerException.StackTrace;
            }
            return innerExceptionMsg;
        }

        private static string GetPushCityName(Guid businessHourGuid)
        {
            string cityList = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(businessHourGuid, true).CityList;
            var cityId = new JsonSerializer().Deserialize<List<string>>(cityList).FirstOrDefault();
            int pponCityId = int.TryParse(cityId, out pponCityId) ? pponCityId : PponCityGroup.DefaultPponCityGroup.AllCountry.CityId;
            return PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(pponCityId).CityName;
        }


        public class MessageDataBase
        {
            public int pushId;
            public string message;
            public dynamic pokeball;

            public Dictionary<string, object> CreateMessageData(int pushRecordId)
            {
                var messageData = new Dictionary<string, object>();
                List<dynamic> pokeballs = new List<dynamic>();
                pokeballs.Add(pokeball);
                if (pushRecordId > 0)
                {
                    pokeballs.Add(NotificationFacade.GetPokeballForReadMessage(pushRecordId));
                }
                if (string.IsNullOrEmpty(message) == false)
                {
                    messageData.Add("message", this.message);
                }
                if (pushId > 0)
                {
                    messageData.Add("pushid", pushId);
                }
                messageData.Add("PokeballList", pokeballs.ToArray());
                messageData.Add("tickerText", tickerText);
                messageData.Add("contentTitle", contentTitle);
                return messageData;
            }
        }
    }

    public class PushMessageViewModel
    {
        public Guid Bid { get; set; }
        public DateTime Bhoe { get; set; }
        public int CityId { get; set; }
        public string AppTitle { get; set; }
        public string CouponUsage { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string SubjectName { get; set; }
    }
}

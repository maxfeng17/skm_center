﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.Web;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.BizLogic.Component
{
    public delegate void OrderCreatedHandler(Guid bid, int userId, Guid orderGuid, Core.PaymentType payType);
    public delegate void OrderedHandler(Guid bid, int count, decimal total, Guid orderGuid, string userName);
    public delegate void OnVerifyHandler(Guid trustId);

    /// <summary>    
    /// 因為pay的程式有點四散，atm、ISP超取付款 交易的完成的狀態，也是在繳費後而非訂單建立。
    /// 於是有這隻程式，希望訂單完成要額外處理的code，有統一放置的地方。
    /// 
    /// 要加 events 寫在 RegisterEvents
    /// 
    /// </summary>
    public class PayEvents
    {
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(PayEvents));
        private static ILog flogger = LogManager.GetLogger("DealSalesInfo");

        public static event OrderCreatedHandler OnOrderCreatedEvents;
        private static event OrderedHandler OnOrderedEvents;
        private static event OrderedHandler OnAtmOrderCreatedEvents;
        private static event OrderedHandler OnIspOrderCreatedEvents;
        private static event OnVerifyHandler OnVerifyEvents;

        internal class UpdateItem
        {
            public Guid bid;
            public int OrderedQuantity;
            public decimal OrderedTotal;
            public bool Refresh;

            public UpdateItem DeepCopy()
            {
                return new UpdateItem
                {
                    bid = this.bid,
                    OrderedQuantity = this.OrderedQuantity,
                    OrderedTotal = this.OrderedTotal,
                    Refresh = this.Refresh
                };
            }
        }

        internal class UpdateItemDict : Dictionary<Guid, UpdateItem>
        {
            public UpdateItemDict DeepCopy()
            {
                UpdateItemDict result = new UpdateItemDict();
                foreach (KeyValuePair<Guid, UpdateItem> pair in this)
                {
                    result.Add(pair.Key, pair.Value.DeepCopy());
                }
                return result;
            }
        }

        private static object thisLock = new object();
        private static UpdateItemDict storage = new UpdateItemDict();

        private static Timer timer;

        public static void Register()
        {
            RegisterEvents();
            timer = new Timer(3000);
            timer.AutoReset = false;
            timer.Elapsed += delegate(object sender, ElapsedEventArgs e)
            {
                UpdateItemDict instance;
                try
                {
                    lock (thisLock)
                    {
                        instance = storage.DeepCopy();
                        storage.Clear();
                    }

                    foreach (KeyValuePair<Guid, UpdateItem> pair in instance)
                    {
                        if (pair.Value.Refresh)
                        {
                            flogger.InfoFormat("{0} 執行PayEvents.OnRefresh程序:開始更新", pair.Key);

                            pp.DealSalesInfoRefresh(pair.Key);

                            flogger.InfoFormat("{0} 執行PayEvents.OnRefresh程序:更新完成", pair.Key);
                        }
                        else if (pair.Value.OrderedQuantity > 0)
                        {
                            int quantity = pair.Value.OrderedQuantity;
                            decimal total = pair.Value.OrderedTotal;

                            flogger.InfoFormat("{0} 執行PayEvents.OrderedQuantity更新程序:處理OrderedQuantity > 0... 開始", pair.Key);

                            pp.DealSalesInfoAddTotalAndQuantity(pair.Key, quantity, total);

                            flogger.InfoFormat("{0} 執行PayEvents.OrderedQuantity更新程序:處理OrderedQuantity > 0... deal_sales_info 更新完成", pair.Key);

                            pp.DealSalesInfoDailyAddTotalAndQuantity(pair.Key, quantity, (int)total);

                            flogger.InfoFormat("{0} 執行PayEvents.OrderedQuantity更新程序:處理OrderedQuantity > 0... deal_sales_info_daily 更新完成", pair.Key);

                            flogger.InfoFormat("{0} 執行PayEvents.OrderedQuantity更新程序:處理OrderedQuantity > 0... 完成", pair.Key);
                        }
                        //目前沒在用，退貨都用 Refresh 
                        else if (pair.Value.OrderedQuantity < 0)
                        {
                            int quantity = Math.Abs(pair.Value.OrderedQuantity);
                            decimal total = Math.Abs(pair.Value.OrderedTotal);
                            pp.DealSalesInfoMinusTotalAndQuantity(pair.Key, quantity, total);
                            //Daily 退貨暫不計

                            flogger.InfoFormat("{0} 執行PayEvents.OrderedQuantity更新程序:處理OrderedQuantity < 0... 更新完成", pair.Key);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Warn("DealSalesInfo refresh error.", ex);
                }
                finally
                {
                    timer.Start();
                }
            };
            timer.Start();
        }

        public static void Stop()
        {
            timer.Stop();
            pp.DealSalesInfoRefresh();
            lock (thisLock)
            {
                storage.Clear();
            }
        }

        public static void Start()
        {
            timer.Start();
        }



        private static void RegisterEvents()
        {
            OnOrderedEvents += delegate(Guid bid, int count, decimal total, Guid orderGuid, string userName)
            {
                if (config.PersonalPushMessageEnabled)
                {
                    Member mem = MemberFacade.GetMember(userName);
                    if (mem.UniqueId == 0 || mem.IsFraudSuspect)
                    {
                        return;
                    }
                    Order o = op.OrderGet(orderGuid);
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid,  true);
                    string msg = string.Format("您已成功購得17Life{0}(訂單：{1})。",
                        deal.DeliveryType.GetValueOrDefault() == (int)DeliveryType.ToShop ? "電子票券" : "商品",
                        o.OrderId);
                    if (o != null && o.Guid != Guid.Empty && mem.UniqueId != 0 && !Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.SKMDeal))
                    {
                        NotificationFacade.PushMemberOrderMessage(mem.UniqueId, msg, o.Guid);
                    }
                }
            };

            OnAtmOrderCreatedEvents += delegate (Guid bid, int count, decimal total, Guid orderGuid, string userName)
            {
                if (config.PersonalPushMessageEnabled)
                {
                    int userId = MemberFacade.GetUniqueId(userName);
                    Order o = op.OrderGet(orderGuid);
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                    string msg = string.Format("請於下單日 23:59:59 前使用ATM或網路ATM完成付款，才算交易成功喔。感謝您訂購17Life{0}(訂單：{1})",
                        deal.DeliveryType.GetValueOrDefault() == (int)DeliveryType.ToShop ? "電子票券" : "商品",
                        o.OrderId);
                    if (o != null && o.Guid != Guid.Empty && userId != 0)
                    {
                        NotificationFacade.PushMemberOrderMessage(userId, msg, o.Guid);
                    }
                }
            };

            OnIspOrderCreatedEvents += delegate (Guid bid, int count, decimal total, Guid orderGuid, string userName)
            {
                if (config.PersonalPushMessageEnabled)
                {
                    int userId = MemberFacade.GetUniqueId(userName);
                    Order o = op.OrderGet(orderGuid);
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);

                    string msg = string.Format("您已成功購得17Life{0}(訂單：{1})。",
                        deal.DeliveryType.GetValueOrDefault() == (int)DeliveryType.ToShop ? "電子票券" : "商品",
                        o.OrderId);

                    if (o != null && o.Guid != Guid.Empty && userId != 0)
                    {
                        NotificationFacade.PushMemberOrderMessage(userId, msg, o.Guid);
                    }
                }
            };
        }

 

        public static void OnOrdered(Guid bid, int count, decimal total, Guid orderGuid)
        {
            if (OnOrderedEvents != null)
            {
                var receivers = OnOrderedEvents.GetInvocationList();
                foreach (OrderedHandler receiver in receivers)
                {
                    try
                    {
                        string userName = string.Empty;
                        if (HttpContext.Current != null && HttpContext.Current.User != null)
                        {
                            userName = HttpContext.Current.User.Identity.Name;
                        }
                        receiver.Invoke(bid, count, total, orderGuid, userName);
                    }
                    catch (Exception ex)
                    {
                        logger.Warn("OnOrdered raise event error.", ex);
                    }
                }
            }
            if (count < 0 || total < 0)
            {
                throw new ArgumentException("count or total can't less than 0");
            }
            lock (thisLock)
            {
                if (storage.ContainsKey(bid) == false)
                {
                    storage[bid] = new UpdateItem { bid = bid };
                }
                storage[bid].OrderedQuantity += count;
                storage[bid].OrderedTotal += total;

                flogger.InfoFormat("{0} 執行PayEvents.OnOrdered程序:Set OrderedQuantity = {1}, OrderedTotal = {2}", bid, storage[bid].OrderedQuantity, storage[bid].OrderedTotal);
            }
        }

        /// <summary>
        /// ATM的訂單"成立"即觸發 OnAtmOrderCreatedEvents
        /// ATM的單如果繳費完成，會再觸發 OnOrderedEvents
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="count"></param>
        /// <param name="total"></param>
        /// <param name="orderGuid"></param>
        public static void OnAtmOrderCreated(Guid bid, int count, decimal total, Guid orderGuid)
        {
            if (OnAtmOrderCreatedEvents != null)
            {
                var receivers = OnAtmOrderCreatedEvents.GetInvocationList();
                foreach (OrderedHandler receiver in receivers)
                {
                    try
                    {
                        receiver.Invoke(bid, count, total, orderGuid, HttpContext.Current.User.Identity.Name);
                    }
                    catch (Exception ex)
                    {
                        logger.Warn("OnAtmOrderCreated raise event error.", ex);
                    }
                }
            }
            if (count < 0 || total < 0)
            {
                throw new ArgumentException("OnAtmOrderCreated count or total can't less than 0");
            }

        }

        public static void OnIspOrderCreated(Guid bid, int count, decimal total, Guid orderGuid)
        {
            if (OnIspOrderCreatedEvents != null)
            {
                var receivers = OnIspOrderCreatedEvents.GetInvocationList();
                foreach (var @delegate in receivers)
                {
                    var receiver = (OrderedHandler) @delegate;
                    try
                    {
                        receiver.Invoke(bid, count, total, orderGuid, HttpContext.Current.User.Identity.Name);
                    }
                    catch (Exception ex)
                    {
                        logger.Warn("OnIspOrderCreated raise event error.", ex);
                    }
                }
            }
            if (count < 0 || total < 0)
            {
                throw new ArgumentException("OnIspOrderCreated count or total can't less than 0");
            }

        }

        /// <summary>
        /// 訂單建立觸發的事件。包含ATM初建立，超取初建立、或信用卡刷卡建立(即完成)
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="orderGuid"></param>
        /// <param name="userId"></param>
        public static void OnOrderCreated(Guid bid, int userId, Guid orderGuid, Core.PaymentType payType)
        {
            if (OnOrderCreatedEvents != null)
            {
                var receivers = OnOrderCreatedEvents.GetInvocationList();
                foreach (OrderCreatedHandler receiver in receivers)
                {
                    try
                    {
                        receiver.Invoke(bid, userId, orderGuid, payType);
                    }
                    catch (Exception ex)
                    {
                        logger.Warn("OnOrdered raise event error.", ex);
                    }
                }
            }
        }

        public static void OnVerify(Guid trustId)
        {
            if (OnVerifyEvents != null)
            {
                var receivers = OnVerifyEvents.GetInvocationList();
                foreach (OnVerifyHandler receiver in receivers)
                {
                    try
                    {
                        receiver.Invoke(trustId);
                    }
                    catch (Exception ex)
                    {
                        logger.Warn("OnOrdered raise event error.", ex);
                    }
                }
            }

        }

        public static void OnRefresh(Guid bid)
        {
            if (thisLock == null)
            {
                flogger.InfoFormat("{0} 執行PayEvents.OnRefresh程序出現異常:Object thisLock = null", bid);
            }

            lock (thisLock)
            {
                if (storage.ContainsKey(bid) == false)
                {
                    storage[bid] = new UpdateItem { bid = bid };
                }
                storage[bid].Refresh = true;
                
                flogger.InfoFormat("{0} 執行PayEvents.OnRefresh程序:Set Refresh = true", bid);
            }
        }

        public static void OnRefund(Guid bid)
        {                
            //主要想觀察Atm退款完成訂單 更新deal_sales_info過程是否有異常 之後若觀察無異常可拔除
            flogger.InfoFormat("{0} PayEvents.OnRefund準備呼叫PayEvents.OnRefresh程序", bid);

            OnRefresh(bid);
                        
            flogger.InfoFormat("{0} PayEvents.OnRefund呼叫PayEvents.OnRefresh程序執行完成", bid);
        }

        /* 希望的版本，但在退貨的部份無法掌握，暫時先用 refresh 的方式
        public static void OnRefund(Guid bid, int count, decimal total)
        {
            if (count < 0 || total < 0)
            {
                throw new ArgumentException("count or total can't less than 0");
            }
            lock (thisLock)
            {
                if (storage.ContainsKey(bid) == false)
                {
                    storage[bid] = new UpdateItem { bid = bid };
                }
                storage[bid].OrderedQuantity -= (int)count;
                storage[bid].OrderedTotal -= total;
            }            
        }
        */
    }
}

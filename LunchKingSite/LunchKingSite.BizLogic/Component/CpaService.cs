﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class CpaService
    {
        private IMarketingProvider mkp = null;

        public CpaService()
        {
        }
        
        public CpaService(IMarketingProvider marketing)
        {
            mkp = marketing;
        }


        public string ComposeReferenceCode(string referrerCode, string campaignCode)
        {
            return referrerCode + "_" + campaignCode;
        }

        public bool DecomposeReferenceCode(string referenceCode, out string referrerCode, out string campaignCode)
        {
            referrerCode = campaignCode = null;
            string[] tokens = null;
            if (string.IsNullOrWhiteSpace(referenceCode))
            {
                return false;
            }

            tokens = referenceCode.Split('_');
            if (tokens == null || tokens.Length < 2)
            {
                return false;
            }

            referrerCode = tokens[0];
            campaignCode = tokens[1];

            return true;
        }

        public bool DecomposeReferenceCode(string referenceCode, out MemberReferral referrer, out ReferralCampaign campaign)
        {
            string referrerCode, campaignCode;
            referrer = null;
            campaign = null;
            if (!DecomposeReferenceCode(referenceCode, out referrerCode, out campaignCode))
            {
                return false;
            }
            
            referrer = mkp.MemberReferralGetList().FirstOrDefault(x => x.Code.ToLower() == referrerCode.ToLower());
            if (referrer == null)
            {
                return false;
            }
            
            campaign = mkp.ReferralCampaignGetByRsrc(referenceCode);
            if (campaign == null)
            {
                return false;
            }

            return true;
        }

        #region Referrer 
        //不再使用Mango，改回DB取值
        public ReferralCampaign GetCampaign(string referenceCode)
        {
            MemberReferral r;
            ReferralCampaign c;
            if (!DecomposeReferenceCode(referenceCode, out r, out c))
            {
                return null;
            }

            return c;
        }

        public string GetReferrerSourceId(string referenceCode)
        {
            MemberReferral r;
            ReferralCampaign c;
            if (!DecomposeReferenceCode(referenceCode, out r, out c))
            {
                return null;
            }

            return ComposeReferenceCode(r.Code, c.Code);
        }
        #endregion Referrer

        #region ReferredAction

        public bool SaveAction(ReferralAction action)
        {
            mkp.ReferredActionSet(action);
            return true;
        }

        public bool RecordOrderByReferrer(string srcId, Order ord, IViewPponDeal deal, DealProperty dealProperty)
        {
            ReferralAction action = new ReferralAction();
            action.Guid = "";
            action.ReferrerGuid = mkp.ReferralCampaignGetByRsrc(srcId).Guid;
            action.ReferrerSourceId = srcId;
            action.ActionType = Convert.ToInt32(ReferrerActionType.MakeOrder);
            action.ReferenceId = ord.OrderId;
            action.CreateTime = DateTime.Now;
            action.Department = deal.Department.ToString();
            action.City = ord.CityId.ToString();
            action.ItemName = deal.ItemName;
            action.Price = deal.ItemPrice.ToString();
            action.Total = ord.Total.ToString();
            action.SubTotal = ord.Subtotal.ToString();
            action.BusinessModel = (int)BusinessModel.Ppon;

            //DealType、AccBusinessGroupId先給預設值
            action.DealType = -1;
            action.AccBusinessGroupId = -1;

            if (dealProperty != null && dealProperty.IsLoaded)
            {
                if (dealProperty.DealType != null)
                {
                    action.DealType = dealProperty.DealType.Value;
                }

                if (dealProperty.DealAccBusinessGroupId != null)
                {
                    action.AccBusinessGroupId = dealProperty.DealAccBusinessGroupId.Value;
                }
            }
            return RecordReferredAction(srcId, action);
        }

        /// <summary>
        /// 品生活 訂單成立的 CPA 紀錄
        /// </summary>
        /// <param name="srcId"></param>
        /// <param name="ord"></param>
        /// <param name="deal"></param>
        /// <param name="dealProduct"></param>
        /// <returns></returns>
        public bool RecordOrderByReferrer(string srcId, HiDealOrder ord, HiDealDeal deal, HiDealProduct dealProduct)
        {
            ReferralAction action = new ReferralAction();
            action.Guid = "";
            action.ReferrerGuid = mkp.ReferralCampaignGetByRsrc(srcId).Guid;
            action.ReferrerSourceId = srcId;
            action.ActionType = Convert.ToInt32(ReferrerActionType.MakeOrder);
            action.ReferenceId = ord.OrderId;
            action.CreateTime = DateTime.Now;
            action.ItemName = deal.Name;
            action.Price = dealProduct.Price.ToString();
            action.Total = ord.TotalAmount.ToString();
            action.SubTotal = ord.TotalAmount.ToString();
            action.DealType = deal.DealType == null ? 0 : deal.DealType.Value;
            action.AccBusinessGroupId = deal.SalesGroupId == null ? 0 : deal.SalesGroupId.Value;
            action.BusinessModel = (int)BusinessModel.PiinLife;

            return RecordReferredAction(srcId, action);
        }

        public bool RecordRegistrationByReferrer(string srcId, Member member, BusinessModel businessModel)
        {
            ReferralAction action = new ReferralAction();
            action.Guid = "";
            action.ReferrerGuid = mkp.ReferralCampaignGetByRsrc(srcId).Guid;
            action.ReferrerSourceId = srcId;
            action.ActionType = Convert.ToInt32(ReferrerActionType.UserRegistration);
            action.ReferenceId = member.UserName;
            action.CreateTime = DateTime.Now;
            action.DealType = -1;
            action.AccBusinessGroupId = -1;
            action.BusinessModel = (int)businessModel;

            return RecordReferredAction(srcId, action);
        }

        public bool RecordSubscriptionByReferrer(string srcId, Subscription sub)
        {
            ReferralAction action = new ReferralAction();
            action.Guid = "";
            action.ReferrerGuid = mkp.ReferralCampaignGetByRsrc(srcId).Guid;
            action.ReferrerSourceId = srcId;
            action.ActionType = Convert.ToInt32(ReferrerActionType.UserSubscription);
            action.ReferenceId = sub.Email;
            action.City = sub.CategoryId.ToString();
            action.CreateTime = DateTime.Now;
            action.DealType = -1;
            action.AccBusinessGroupId = -1;
            action.BusinessModel = (int)BusinessModel.Ppon;

            return RecordReferredAction(srcId, action);
        }

        /// <summary>
        /// 品生活 訂閱 的cpa紀錄
        /// </summary>
        /// <param name="srcId"></param>
        /// <param name="sub"></param>
        /// <returns></returns>
        public bool RecordSubscriptionByReferrer(string srcId, HiDealSubscription sub)
        {
            ReferralAction action = new ReferralAction();
            action.Guid = "";
            action.ReferrerGuid = mkp.ReferralCampaignGetByRsrc(srcId).Guid;
            action.ReferrerSourceId = srcId;
            action.ActionType = Convert.ToInt32(ReferrerActionType.UserSubscription);
            action.ReferenceId = sub.Email;
            action.CreateTime = DateTime.Now;
            action.BusinessModel = (int)BusinessModel.PiinLife;
            action.DealType = -1;
            action.AccBusinessGroupId = -1;

            return RecordReferredAction(srcId, action);
        }

        public List<ReferralAction> GetActionsByReferenceSource(string srcId)
        {
            return mkp.ReferralActionGetListByReferenceSourceId(srcId).ToList();
        }

        public List<ReferralAction> GetActionsByReferenceSource(string srcId, ReferrerActionType atype)
        {
            return mkp.ReferralActionGetListByReferenceSourceAndActionType(srcId, atype).ToList();
        }

        public List<ReferralAction> GetActionsByReferenceSourceAndTime(
            ReferrerActionType? actionType, string srcId, DateTime starttime, DateTime endtime)
        {
            return mkp.ReferralActionGetListByReferenceSourceAndTime(
                actionType, srcId, starttime, endtime).ToList();
        }

        private bool RecordReferredAction(string referrerSourceId, ReferralAction action)
        {
            if (string.IsNullOrWhiteSpace(referrerSourceId))
            {
                return false;
            }

            return SaveAction(action);
        }

        #endregion ReferredAction
    }
}
﻿using System;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Component
{
    public class OrderedQuantityHelper
    {
        private static readonly int thousandSelling;
        private static readonly DateTime stopRefundQuantityCalculateDate;
        static OrderedQuantityHelper()
        {
            thousandSelling = ProviderFactory.Instance().GetConfig().ThousandSelling;
            stopRefundQuantityCalculateDate = ProviderFactory.Instance().GetConfig().StopRefundQuantityCalculateDate;
        }

        public enum ShowType
        {
            InPortal,
            TodayDeals,
            RecentDeals,
            MainDealAtComboShow,
            MainDealRunning,
            MainDealRunningAndOn,
            MainDealRunningAndFull,
            MainDealClosedAndOn,
            /*M版網頁*/
            MStylePortal,
            MStyleDeal,
            RelateDeal,
            BetaInPortal,
            InPortal201804
        }

        /// <summary>
        /// 銷售量顯式方式，當QuantityMultiplier 不等於 null & IsQuantiytMultiplier，銷售量以購買份數顯示
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="showType"></param>
        /// <returns></returns>
        public static string Show(IViewPponDeal deal, ShowType showType)
        {
            var adj = deal.GetAdjustedOrderedQuantity();
            /*企劃確認購買人(份)數不做退貨完成後數量處理, 但要在此修正上線前的檔次都要照原邏輯執行*/
            int quantity = (deal.BusinessHourOrderTimeS  >= stopRefundQuantityCalculateDate) ? adj.IncludeRefundQuantity : adj.Quantity;
            bool IsAdjusted = adj.IsAdjusted;
            int diffp = (int)deal.BusinessHourOrderMinimum - quantity;
            if (diffp < 0 && showType == ShowType.MainDealRunning)
            {
                showType = ShowType.MainDealRunningAndOn;
            }

            ShowAction act = null;
            if (deal.GroupOrderStatus.HasValue && (deal.GroupOrderStatus.Value & (int)GroupOrderStatus.KindDeal) > 0)
            {
                act = new PponKindDealShowAction();
            }
            else
            {
                act = new PponNormalDealShowAction();
            }
            return act.ToString(IsAdjusted, showType, deal, quantity, diffp);


        }

        public static Dictionary<int, string> ShowList(ViewPponDeal deal)
        {
            var adj = deal.GetAdjustedOrderedQuantity();
            int quantity = adj.Quantity;
            bool IsAdjusted = adj.IsAdjusted;
            int diffp = (int)deal.BusinessHourOrderMinimum - quantity;

            ShowAction act = null;
            if (deal.GroupOrderStatus.HasValue && (deal.GroupOrderStatus.Value & (int)GroupOrderStatus.KindDeal) > 0)
            {
                act = new PponKindDealShowAction();
            }
            else
            {
                act = new PponNormalDealShowAction();
            }
            return act.ToShowList(IsAdjusted, deal, quantity, diffp);
        }

        /// <summary>
        /// 顯示多檔次的子檔的銷售數量
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="showType"></param>
        /// <returns></returns>
        public static string Show(ViewComboDeal deal, ShowType showType)
        {
            int quantity = deal.OrderedQuantity;
            if (showType == ShowType.InPortal)
            {
                //return string.Format("{0}人已購買", quantity);
                return string.Empty;
            }
            throw new NotSupportedException(string.Format("{0} not supported", showType));
        }

        public static bool OverThousand(IViewPponDeal deal)
        {
            //多檔次顯示銷售數調整
            return deal.GetAdjustedOrderedQuantity().Quantity >= thousandSelling;
        }

        public interface ShowAction
        {
            string ToString(bool isAdjusted, ShowType showType, IViewPponDeal deal, int quantity, int diffp);
            Dictionary<int, string> ToShowList(bool isAdjusted, IViewPponDeal deal, int quantity, int diffp);
        }

        public class PponNormalDealShowAction : ShowAction
        {
            public Dictionary<int, string> ToShowList(bool isAdjusted, IViewPponDeal deal, int quantity, int diffp)
            {
                Dictionary<int, string> show_list = new Dictionary<int, string>();
                if (isAdjusted == false)
                {
                    show_list.Add((int)ShowType.BetaInPortal, string.Format("<span class=\"tag_buycounter\"><span class=\"num_people\">{0}</span>人已購買</span>", quantity));
                    show_list.Add((int)ShowType.InPortal, string.Format("<span class=\"people\">{0}</span>人已購買", quantity));
                    show_list.Add((int)ShowType.TodayDeals, string.Format("{0}<span class=\"text\">人已購買</span>", quantity));
                    show_list.Add((int)ShowType.RecentDeals, string.Format("<span class='PastPricePeopleBuy'>購買人數</span><br />{0}人", deal.GetAdjustedSlug().Quantity));
                    show_list.Add((int)ShowType.MainDealRunning, string.Format("還差<span class=\"people\">{0}</span>人達到團購門檻<br />", diffp));
                    show_list.Add((int)ShowType.MainDealRunningAndOn, string.Format("<span class=\"people\">{0}</span>人已購買", quantity));
                    show_list.Add((int)ShowType.MainDealRunningAndFull, string.Format("共<span class=\"people\">{0}</span>人購買", quantity));
                    show_list.Add((int)ShowType.MainDealClosedAndOn, string.Format("共<span class=\"people\">{0}</span>人購買", deal.GetAdjustedSlug().Quantity));
                    show_list.Add((int)ShowType.MStylePortal, string.Format("{0}人已購買", quantity));
                    show_list.Add((int)ShowType.MStyleDeal, string.Format("{0}人已購買", quantity));
                    show_list.Add((int)ShowType.RelateDeal, string.Format("<span class=\"icon-user\"></span><span class='people'>{0}</span>人<span class='rd-t-Disap'>已</span>購買", quantity));
                }
                else
                {
                    show_list.Add((int)ShowType.BetaInPortal, string.Format("<span class=\"tag_buycounter\">已售出<span class=\"num_people\">{0}</span>份</span>", quantity));
                    show_list.Add((int)ShowType.InPortal, string.Format("已售出<span class=\"people\">{0}</span>份", quantity));
                    show_list.Add((int)ShowType.TodayDeals, string.Format("<span class=\"text\">已售出</span>{0}<span class=\"text\">份</span>", quantity));
                    show_list.Add((int)ShowType.RecentDeals, string.Format("<span class='PastPricePeopleBuy'>共售出</span><br />{0}份", deal.GetAdjustedSlug().Quantity));
                    show_list.Add((int)ShowType.MainDealAtComboShow, string.Format("合計共售出<span class=\"people\">{0}</span>份", quantity));
                    var insufficiency = deal.GetAdjustedInsufficientQuantity();
                    show_list.Add((int)ShowType.MainDealRunning, string.Format("還差<span class=\"people\">{0}</span>份達到團購門檻<br />", insufficiency.Quantity));
                    show_list.Add((int)ShowType.MainDealRunningAndOn, string.Format("已售出<span class=\"people\">{0}</span>份", quantity));
                    show_list.Add((int)ShowType.MainDealRunningAndFull, string.Format("共售出<span class=\"people\">{0}</span>份", quantity));
                    show_list.Add((int)ShowType.MainDealClosedAndOn, string.Format("共售出<span class=\"people\">{0}</span>份", deal.GetAdjustedSlug().Quantity));
                    show_list.Add((int)ShowType.MStylePortal, string.Format("已售出{0}份", quantity));
                    show_list.Add((int)ShowType.MStyleDeal, string.Format("已售出{0}份", quantity));
                    show_list.Add((int)ShowType.RelateDeal, string.Format("<span class=\"icon-user\"></span><span class='people'>{0}</span>人<span class='rd-t-Disap'>已</span>購買", quantity));
                }
                return show_list;
            }

            public string ToString(bool isAdjusted, ShowType showType, IViewPponDeal deal, int quantity, int diffp)
            {
                if (isAdjusted == false)
                {
                    switch (showType)
                    {
                        case ShowType.BetaInPortal:
                            return string.Format("<span class=\"tag_buycounter\"><span class=\"num_people\">{0}</span>人已購買</span>", quantity);
                        case ShowType.InPortal:
                            return string.Format("<span class=\"people\">{0}</span>人已購買", quantity);
                        case ShowType.TodayDeals:
                            return string.Format("{0}<span class=\"text\">人已購買</span>", quantity);
                        case ShowType.RecentDeals:
                            return string.Format("<span class='PastPricePeopleBuy'>購買人數</span><br />{0}人",
                               deal.GetAdjustedSlug().Quantity);
                        case ShowType.MainDealRunning:
                            return string.Format("還差<span class=\"people\">{0}</span>人達到團購門檻<br />", diffp);
                        case ShowType.MainDealRunningAndOn:
                            return string.Format("<span class=\"people\">{0}</span>人已購買", quantity);
                        case ShowType.MainDealRunningAndFull:
                            return string.Format("共<span class=\"people\">{0}</span>人購買", quantity);
                        case ShowType.MainDealClosedAndOn:
                            return string.Format("共<span class=\"people\">{0}</span>人購買",
                               deal.GetAdjustedSlug().Quantity);
                        case ShowType.MStylePortal:
                        case ShowType.MStyleDeal:
                            return string.Format("{0}人已購買", quantity);
                        case ShowType.RelateDeal:
                            return string.Format("<span class=\"icon-user\"></span><span class='people'>{0}</span>人<span class='rd-t-Disap'>已</span>購買", quantity);
                        case ShowType.InPortal201804:
                            return string.Format("<span class='timer_num'>{0}</span><span class='timer_text'>人已購買</span>", quantity);
                        default:
                            throw new NotSupportedException(string.Format("{0} not supported", showType));
                    }
                }
                else
                {
                    switch (showType)
                    {
                        case ShowType.BetaInPortal:
                            return string.Format("<span class=\"tag_buycounter\">已售出<span class=\"num_people\">{0}</span>份</span>", quantity);
                        case ShowType.InPortal:
                            return string.Format("已售出<span class=\"people\">{0}</span>份", quantity);
                        case ShowType.TodayDeals:
                            return string.Format("<span class=\"text\">已售出</span>{0}<span class=\"text\">份</span>", quantity);
                        case ShowType.RecentDeals:
                            return string.Format("<span class='PastPricePeopleBuy'>共售出</span><br />{0}份",
                                deal.GetAdjustedSlug().Quantity);
                        case ShowType.MainDealAtComboShow:
                            return string.Format("合計共售出<span class=\"people\">{0}</span>份", quantity);
                        case ShowType.MainDealRunning:
                            var insufficiency = deal.GetAdjustedInsufficientQuantity();
                            return string.Format("還差<span class=\"people\">{0}</span>份達到團購門檻<br />",
                                insufficiency.Quantity);
                        case ShowType.MainDealRunningAndOn:
                            return string.Format("已售出<span class=\"people\">{0}</span>份", quantity);
                        case ShowType.MainDealRunningAndFull:
                            return string.Format("共售出<span class=\"people\">{0}</span>份", quantity);
                        case ShowType.MainDealClosedAndOn:
                            return string.Format("共售出<span class=\"people\">{0}</span>份",
                                deal.GetAdjustedSlug().Quantity);
                        case ShowType.MStylePortal:
                        case ShowType.MStyleDeal:
                            return string.Format("已售出{0}份", quantity);
                        case ShowType.RelateDeal:
                            return string.Format("<span class=\"icon-user\"></span><span class='people'>{0}</span>人<span class='rd-t-Disap'>已</span>購買", quantity);
                        case ShowType.InPortal201804:
                            return string.Format("<span class='timer_text'>已出售</span><span class='timer_num'>{0}</span><span class='timer_text'>份</span>", quantity);
                        default:
                            throw new NotSupportedException(string.Format("{0} not supported", showType));
                    }
                }
            }
        }

        public class PponKindDealShowAction : ShowAction
        {
            public Dictionary<int, string> ToShowList(bool isAdjusted, IViewPponDeal deal, int quantity, int diffp)
            {
                Dictionary<int, string> show_list = new Dictionary<int, string>();
                if (isAdjusted == false)
                {
                    show_list.Add((int)ShowType.BetaInPortal, string.Format("<span class=\"tag_buycounter\"><span class=\"num_people\">{0}</span>人已捐款</span>", quantity));
                    show_list.Add((int)ShowType.InPortal, string.Format("目前已有<span class=\"people\">{0}</span>人捐款", quantity));
                    show_list.Add((int)ShowType.TodayDeals, string.Format("{0}<span class=\"text\">人已捐款</span>", quantity));
                    show_list.Add((int)ShowType.RecentDeals, string.Format("<span class='PastPricePeopleBuy'>捐款人數</span><br />{0}人", deal.GetAdjustedSlug().Quantity));
                    show_list.Add((int)ShowType.MainDealRunning, string.Format("邀請您獻上第一份愛心"));
                    show_list.Add((int)ShowType.MainDealRunningAndOn, string.Format("目前已有<span class=\"people\">{0}</span>人捐款", quantity));
                    show_list.Add((int)ShowType.MainDealRunningAndFull, string.Format("共<span class=\"people\">{0}</span>人捐款", quantity));
                    show_list.Add((int)ShowType.MainDealClosedAndOn, string.Format("共<span class=\"people\">{0}</span>人捐款", deal.GetAdjustedSlug().Quantity));
                    show_list.Add((int)ShowType.MStylePortal, string.Format("{0}人已捐款", quantity));
                    show_list.Add((int)ShowType.MStyleDeal, string.Format("{0}人已捐款", quantity));
                    show_list.Add((int)ShowType.RelateDeal, string.Format("<span class=\"icon-user\"></span><span class='people'>{0}</span>人<span class='rd-t-Disap'>已</span>捐款", quantity));
                }
                else
                {
                    show_list.Add((int)ShowType.BetaInPortal, string.Format("<span class=\"tag_buycounter\">已有<span class=\"num_people\">{0}</span>筆捐款</span>", quantity));
                    show_list.Add((int)ShowType.InPortal, string.Format("目前已有<span class=\"people\">{0}</span>筆捐款", quantity));
                    show_list.Add((int)ShowType.TodayDeals, string.Format("<span class=\"text\">目前已有</span>{0}<span class=\"text\">筆捐款</span>", quantity));
                    show_list.Add((int)ShowType.RecentDeals, string.Format("<span class='PastPricePeopleBuy'>共有</span><br />{0}筆捐款", deal.GetAdjustedSlug().Quantity));
                    show_list.Add((int)ShowType.MainDealAtComboShow, string.Format("合計共有<span class=\"people\">{0}</span>筆捐款", quantity));
                    show_list.Add((int)ShowType.MainDealRunning, string.Format("邀請您獻上第一份愛心"));
                    show_list.Add((int)ShowType.MainDealRunningAndOn, string.Format("目前已有<span class=\"people\">{0}</span>筆捐款", quantity));
                    show_list.Add((int)ShowType.MainDealRunningAndFull, string.Format("共有<span class=\"people\">{0}</span>筆捐款", quantity));
                    show_list.Add((int)ShowType.MainDealClosedAndOn, string.Format("共有<span class=\"people\">{0}</span>筆捐款", deal.GetAdjustedSlug().Quantity));
                    show_list.Add((int)ShowType.MStylePortal, string.Format("已有{0}筆捐款", quantity));
                    show_list.Add((int)ShowType.MStyleDeal, string.Format("已有{0}筆捐款", quantity));
                    show_list.Add((int)ShowType.RelateDeal, string.Format("<span class=\"icon-user\"></span><span class='people'>{0}</span>人<span class='rd-t-Disap'>已</span>捐款", quantity));
                }
                return show_list;
            }

            public string ToString(bool isAdjusted, ShowType showType, IViewPponDeal deal, int quantity, int diffp)
            {
                if (isAdjusted == false)
                {
                    switch (showType)
                    {
                        case ShowType.BetaInPortal:
                            return string.Format("<span class=\"tag_buycounter\"><span class=\"num_people\">{0}</span>人已捐款</span>", quantity);
                        case ShowType.InPortal:
                            return string.Format("目前已有<span class=\"people\">{0}</span>人捐款", quantity);
                        case ShowType.TodayDeals:
                            return string.Format("{0}<span class=\"text\">人已捐款</span>", quantity);
                        case ShowType.RecentDeals:
                            return string.Format("<span class='PastPricePeopleBuy'>捐款人數</span><br />{0}人",
                               deal.GetAdjustedSlug().Quantity);
                        case ShowType.MainDealRunning:
                            return string.Format("邀請您獻上第一份愛心");
                        case ShowType.MainDealRunningAndOn:
                            return string.Format("目前已有<span class=\"people\">{0}</span>人捐款", quantity);
                        case ShowType.MainDealRunningAndFull:
                            return string.Format("共<span class=\"people\">{0}</span>人捐款", quantity);
                        case ShowType.MainDealClosedAndOn:
                            return string.Format("共<span class=\"people\">{0}</span>人捐款",
                               deal.GetAdjustedSlug().Quantity);
                        case ShowType.MStylePortal:
                        case ShowType.MStyleDeal:
                            return string.Format("{0}人已捐款", quantity);
                        case ShowType.RelateDeal:
                            return string.Format("<span class=\"icon-user\"></span><span class='people'>{0}</span>人<span class='rd-t-Disap'>已</span>捐款", quantity);
                        case ShowType.InPortal201804:
                            return string.Format("<span class='timer_num'>{0}</span><span class='timer_text'>人已捐款</span>", quantity);
                        default:
                            throw new NotSupportedException(string.Format("{0} not supported", showType));
                    }
                }
                else
                {
                    switch (showType)
                    {
                        case ShowType.BetaInPortal:
                            return string.Format("<span class=\"tag_buycounter\">已有<span class=\"num_people\">{0}</span>筆捐款</span>", quantity);
                        case ShowType.InPortal:
                            return string.Format("目前已有<span class=\"people\">{0}</span>筆捐款", quantity);
                        case ShowType.TodayDeals:
                            return string.Format("<span class=\"text\">目前已有</span>{0}<span class=\"text\">筆捐款</span>", quantity);
                        case ShowType.RecentDeals:
                            return string.Format("<span class='PastPricePeopleBuy'>共有</span><br />{0}筆捐款",
                                deal.GetAdjustedSlug().Quantity);
                        case ShowType.MainDealAtComboShow:
                            return string.Format("合計共有<span class=\"people\">{0}</span>筆捐款", quantity);
                        case ShowType.MainDealRunning:
                            return string.Format("邀請您獻上第一份愛心");
                        case ShowType.MainDealRunningAndOn:
                            return string.Format("目前已有<span class=\"people\">{0}</span>筆捐款", quantity);
                        case ShowType.MainDealRunningAndFull:
                            return string.Format("共有<span class=\"people\">{0}</span>筆捐款", quantity);
                        case ShowType.MainDealClosedAndOn:
                            return string.Format("共有<span class=\"people\">{0}</span>筆捐款",
                                deal.GetAdjustedSlug().Quantity);
                        case ShowType.MStylePortal:
                        case ShowType.MStyleDeal:
                            return string.Format("已有{0}筆捐款", quantity);
                        case ShowType.RelateDeal:
                            return string.Format("<span class=\"icon-user\"></span><span class='people'>{0}</span>人<span class='rd-t-Disap'>已</span>捐款", quantity);
                        case ShowType.InPortal201804:
                            return string.Format("<span class='timer_num'>{0}</span><span class='timer_text'>人已捐款</span>", quantity);
                        default:
                            throw new NotSupportedException(string.Format("{0} not supported", showType));
                    }
                }
            }

        }
    }

    public class QuantityAdjustment
    {
        public int Quantity { get; set; }
        public int IncludeRefundQuantity { get; set; }
        public bool IsAdjusted { get; set; }
    }

    public static class ViewPponDealExtension
    {
        /// <summary>
        /// 未達門檻的數量顯示  
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static QuantityAdjustment GetAdjustedInsufficientQuantity(this IViewPponDeal deal)
        {
            if (deal.OrderedQuantity >= deal.BusinessHourOrderMinimum)
            {
                if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                {
                    var combodeals = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(deal.BusinessHourGuid);
                    return new QuantityAdjustment { IsAdjusted = combodeals.Any(t => t.IsQuantityMultiplier && t.QuantityMultiplier != null), Quantity = 0 };
                }

                return new QuantityAdjustment { IsAdjusted = ((deal.IsQuantityMultiplier.HasValue && deal.IsQuantityMultiplier.Value) && deal.QuantityMultiplier != null), Quantity = 0 };
            }
            QuantityAdjustment result = new QuantityAdjustment();
            if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
            {
                var combodeals = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(deal.BusinessHourGuid);
                int minMultiplier =
                    (from t in combodeals where t.IsQuantityMultiplier && t.QuantityMultiplier != null select t.QuantityMultiplier).Min() ?? 1;
                result.Quantity = minMultiplier * ((int)deal.BusinessHourOrderMinimum - deal.OrderedQuantity ?? 0);
                result.IsAdjusted = combodeals.Any(t => t.IsQuantityMultiplier && t.QuantityMultiplier != null);
            }
            else if ((deal.IsQuantityMultiplier.HasValue && deal.IsQuantityMultiplier.Value) && deal.QuantityMultiplier != null)
            {
                result.IsAdjusted = true;
                result.Quantity = deal.QuantityMultiplier.Value *
                                  ((int)deal.BusinessHourOrderMinimum - deal.OrderedQuantity ?? 0);
            }
            else
            {
                result.IsAdjusted = false;
                result.Quantity = ((int)deal.BusinessHourOrderMinimum - deal.OrderedQuantity ?? 0);
            }
            return result;
        }

        /// <summary>
        /// 銷售/購買/兌換 數量
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="isIncludePreviousOrderedQuantity"></param>
        /// <returns></returns>
        public static QuantityAdjustment GetAdjustedOrderedQuantity(this IViewPponDeal deal, bool isIncludePreviousOrderedQuantity = true)
        {
            QuantityAdjustment result;
            if (ViewPponDealManager.DefaultManager.TryGetAdjustedOrderedQuantity(deal.BusinessHourGuid, isIncludePreviousOrderedQuantity, null, out result))
            {
                return result;
            }
            else
            {
                result = new QuantityAdjustment();
                if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                {
                    var combodeals = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(deal.BusinessHourGuid);
                    int orderedQty = 0;
                    int orderedQtyIncluedRefund = 0;
                    foreach (var combodeal in combodeals)
                    {
                        int multiplier = 1;
                        if (combodeal.IsQuantityMultiplier && combodeal.QuantityMultiplier.HasValue)
                        {
                            multiplier = combodeal.QuantityMultiplier.Value;
                            result.IsAdjusted = isIncludePreviousOrderedQuantity;
                        }
                        orderedQty += combodeal.OrderedQuantity * multiplier;
                        orderedQtyIncluedRefund += combodeal.OrderedIncludeRefundQuantity * multiplier;
                    }
                    result.Quantity = orderedQty + (deal.ContinuedQuantity ?? 0);
                    result.IncludeRefundQuantity = orderedQtyIncluedRefund + (deal.ContinuedQuantity ?? 0);
                }
                else if ((deal.IsQuantityMultiplier.HasValue && deal.IsQuantityMultiplier.Value) && deal.QuantityMultiplier != null)
                {
                    result.IsAdjusted = isIncludePreviousOrderedQuantity;
                    if (isIncludePreviousOrderedQuantity && (deal.IsContinuedQuantity ?? false))
                    {
                        result.Quantity = ((deal.OrderedQuantity ?? 0) * deal.QuantityMultiplier.Value) + (deal.ContinuedQuantity ?? 0);
                        result.IncludeRefundQuantity = ((deal.OrderedIncludeRefundQuantity ?? 0) * deal.QuantityMultiplier.Value) + (deal.ContinuedQuantity ?? 0);
                    }
                    else
                    {
                        result.Quantity = (deal.OrderedQuantity ?? 0) * deal.QuantityMultiplier.Value;
                        result.IncludeRefundQuantity = (deal.OrderedIncludeRefundQuantity ?? 0) * deal.QuantityMultiplier.Value;
                    }
                }
                else
                {
                    if (isIncludePreviousOrderedQuantity && (deal.IsContinuedQuantity ?? false))
                    {
                        result.Quantity = ((deal.OrderedQuantity ?? 0) + (deal.ContinuedQuantity ?? 0));
                        result.IncludeRefundQuantity = ((deal.OrderedIncludeRefundQuantity ?? 0) + (deal.ContinuedQuantity ?? 0));
                    }
                    else
                    {
                        result.Quantity = deal.OrderedQuantity ?? 0;
                        result.IncludeRefundQuantity = deal.OrderedIncludeRefundQuantity ?? 0;
                    }
                }
                ViewPponDealManager.DefaultManager.TryGetAdjustedOrderedQuantity(deal.BusinessHourGuid, isIncludePreviousOrderedQuantity, result, out result);
                return result;
            }

        }

        /// <summary>
        /// 結檔時的銷售數字(不含後續退貨)
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static QuantityAdjustment GetAdjustedSlug(this IViewPponDeal deal)
        {
            if (deal.Slug == null)
            {
                return GetAdjustedOrderedQuantity(deal);
            }

            QuantityAdjustment adjust = new QuantityAdjustment();
            int slug_quantity = 0;
            if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
            {
                var combodeals = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(deal.BusinessHourGuid);
                foreach (var combodeal in combodeals)
                {
                    int slug = 0;
                    if (string.IsNullOrEmpty(combodeal.Slug) || !int.TryParse(combodeal.Slug, out slug))
                    {
                        slug = combodeal.OrderedQuantity;
                    }

                    int multiplier = 1;
                    if (combodeal.IsQuantityMultiplier && combodeal.QuantityMultiplier.HasValue)
                    {
                        multiplier = combodeal.QuantityMultiplier.Value;
                        adjust.IsAdjusted = true;
                    }
                    slug_quantity += slug * multiplier;
                }
                adjust.Quantity = adjust.IncludeRefundQuantity = slug_quantity + (deal.ContinuedQuantity ?? 0);
            }
            else
            {
                if (string.IsNullOrEmpty(deal.Slug) || !int.TryParse(deal.Slug, out slug_quantity))
                {
                    slug_quantity = deal.OrderedQuantity ?? 0;
                }
                if ((deal.IsQuantityMultiplier.HasValue && deal.IsQuantityMultiplier.Value) && deal.QuantityMultiplier != null)
                {
                    adjust.IsAdjusted = true;
                    if (deal.IsContinuedQuantity ?? false)
                    {
                        adjust.Quantity = adjust.IncludeRefundQuantity = (deal.QuantityMultiplier.Value * slug_quantity) + (deal.ContinuedQuantity ?? 0);
                    }
                    else
                    {
                        adjust.Quantity = adjust.IncludeRefundQuantity = deal.QuantityMultiplier.Value * slug_quantity;
                    }
                }
                else
                {
                    adjust.IsAdjusted = false;
                    if (deal.IsContinuedQuantity ?? false)
                    {
                        adjust.Quantity = adjust.IncludeRefundQuantity = slug_quantity + (deal.ContinuedQuantity ?? 0);
                    }
                    else
                    {
                        adjust.Quantity = adjust.IncludeRefundQuantity = slug_quantity;
                    }
                }
            }
            return adjust;
        }
    }
}
﻿using System;
using System.IO;
using System.Net;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Component
{
    public static class WebUtility2
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private static string SHORTURL_SITE = config.SiteUrl + "/tinyurl/";


        public static string RequestShortUrl(string realUrl)
        {
            DateTime now = DateTime.Now;
            string tinyUrl;
            try
            {
                var req = (HttpWebRequest)WebRequest.Create(SHORTURL_SITE + "register/?realUrl=" + Uri.EscapeDataString(realUrl));
                using (var res = (HttpWebResponse)req.GetResponse())
                {
                    using (var resSrm = new StreamReader(res.GetResponseStream()))
                    {
                        tinyUrl = resSrm.ReadToEnd();
                        LogManager.GetLogger("tinyurl").InfoFormat("{0} -> {1} -> {2}",
                            tinyUrl,
                            (DateTime.Now - now).TotalSeconds.ToString("0.###"),
                            realUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger("tinyurl").InfoFormat("{0} -> {1} -> {2}",
                    ex.Message,
                    (DateTime.Now - now).TotalSeconds.ToString("0.###"),
                    realUrl);

                tinyUrl = null;
            }

            return string.IsNullOrEmpty(tinyUrl) ? null : SHORTURL_SITE + tinyUrl;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using log4net;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// SimpleFtpClient
    /// </summary>
    public class SimpleFtpClient
    {
        private static ILog logger = LogManager.GetLogger(typeof(SimpleFtpClient));
        public class FtpConfig
        {
            public string UserId { get; set; }
            public string Password { get; set; }
            public string LocalPath { get; set; }
            public string Host { get; set; }
        }

        public FtpConfig Config { get; set; }

        public SimpleFtpClient(Uri uri)
        {
            Config = new FtpConfig
            {
                Host = uri.DnsSafeHost
            };
            if (string.IsNullOrEmpty(uri.UserInfo) == false)
            {
                if (uri.UserInfo.Contains(':'))
                {
                    Config.UserId = uri.UserInfo.Split(':')[0];
                    Config.Password = uri.UserInfo.Split(':')[1];
                }
                else
                {
                    Config.UserId = uri.UserInfo;
                }
            }
        }

        public List<string> GetDirectoryList(string path)
        {

            var result = new List<string>();
            FtpWebRequest request = null;
            request = (FtpWebRequest)FtpWebRequest.Create("ftp://" + Config.Host + "/" + path);
            /* Log in to the FTP Server with the User Name and Password Provided */
            request.Credentials = new NetworkCredential(Config.UserId, Config.Password);

            request.Method = WebRequestMethods.Ftp.ListDirectory;
            try
            {
                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    Stream responseStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream);

                    while (!reader.EndOfStream)
                    {
                        var data = reader.ReadLine();
                        result.Add(data);
                    }
                }
            }
            catch
            {
            }

            /* Resource Cleanup */
            finally
            {
                request = null;
            }

            return result;

        }

        public void SimpleDownload(string fileName, string folderName)
        {
            FtpWebRequest request = null;
            try
            {
                request = (FtpWebRequest)WebRequest.Create("ftp://" + Config.Host + "/" + folderName);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(Config.UserId, Config.Password);

                FtpWebResponse downloadResponse = (FtpWebResponse)request.GetResponse();
                Stream responseStream = downloadResponse.GetResponseStream();
                byte[] buffer = new byte[1024];
                int read = 0;
                MemoryStream ms = new MemoryStream();
                while ((read = responseStream.Read(buffer, 0, 1024)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                //ms.Flush();
                //System.Web.HttpContext.Current.Response.Clear();
                //// 設定強制下載標頭。
                //System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=" + fileName));
                //// 輸出檔案。
                //System.Web.HttpContext.Current.Response.BinaryWrite(ms.ToArray());
                responseStream.Close();

                System.Web.HttpContext.Current.Response.Buffer = false;
                System.Web.HttpContext.Current.Response.ClearHeaders();
                System.Web.HttpContext.Current.Response.ClearContent();
                System.Web.HttpContext.Current.Response.ContentType = "application/pdf";
                System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + System.Web.HttpUtility.UrlEncode(fileName));
                System.Web.HttpContext.Current.Response.AddHeader("Content-Length", ms.Length.ToString());
                System.Web.HttpContext.Current.Response.BinaryWrite(ms.ToArray());
                System.Web.HttpContext.Current.Response.Flush();
            }
            catch (System.Web.HttpException)
            {

            }
            catch (System.Threading.ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                logger.Warn("SimpleDownload Error, fileName=" + fileName, ex);
                throw;
            }
            finally
            {
                try
                {
                    request = null;
                }
                catch { }
            }
        }

        public void SimpleDownloadToFolder(string fileName, string folderName, string localFolder)
        {
            FtpWebRequest request = null;
            try
            {
                request = (FtpWebRequest)WebRequest.Create("ftp://" + Config.Host + "/" + folderName);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(Config.UserId, Config.Password);

                FtpWebResponse downloadResponse = (FtpWebResponse)request.GetResponse();
                Stream responseStream = downloadResponse.GetResponseStream();
                byte[] buffer = new byte[1024];
                int read = 0;
                FileStream ms = new FileStream(localFolder, FileMode.Create);
                while ((read = responseStream.Read(buffer, 0, 1024)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                //ms.Flush();
                //System.Web.HttpContext.Current.Response.Clear();
                //// 設定強制下載標頭。
                //System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=" + fileName));
                //// 輸出檔案。
                //System.Web.HttpContext.Current.Response.BinaryWrite(ms.ToArray());
                ms.Close();
                responseStream.Close();

            }
            catch (Exception ex)
            {
                logger.Warn("SimpleDownloadToFolder Error, fileName=" + fileName, ex);
                throw;
            }
            finally
            {
                try
                {
                    request = null;
                }
                catch { }
            }
        }

        public void TaishinDownload(string fileName, string folderName)
        {
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            FtpWebRequest request = null;
            try
            {
                request = (FtpWebRequest)WebRequest.Create("ftp://" + Config.Host + "/" + fileName);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(Config.UserId, Config.Password);
                request.UsePassive = false;
                request.EnableSsl = false;

                using (Stream reader = request.GetResponse().GetResponseStream())
                {
                    using (BinaryWriter writer = new BinaryWriter(File.Open(Path.Combine(folderName, fileName), FileMode.CreateNew)))
                    {
                        while (true)
                        {
                            bytesRead = reader.Read(buffer, 0, buffer.Length);

                            if (bytesRead == 0)
                            {
                                break;
                            }

                            writer.Write(buffer, 0, bytesRead);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Warn("TaishinDownload Error, fileName=" + fileName, ex);
                throw;
            }
            finally
            {
                try
                {
                    request = null;
                }
                catch { }
            }
        }



        public void SimpleUpload(System.Web.HttpPostedFileBase file, string DirName, string fileName)
        {
            FtpWebRequest request = null;
            try
            {
                request = (FtpWebRequest)WebRequest.Create("ftp://" + Config.Host + "/" + DirName + "/" + fileName);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(Config.UserId, Config.Password);

                Stream requestStream = request.GetRequestStream();
                StreamReader fileStream = new StreamReader(file.InputStream);
                byte[] buffer = new byte[1024];
                int bytesRead;
                while (true)
                {
                    bytesRead = file.InputStream.Read(buffer, 0, buffer.Length);
                    if (bytesRead == 0)
                    {
                        break;
                    }

                    requestStream.Write(buffer, 0, bytesRead);
                }
                requestStream.Close();
            }
            catch (Exception ex)
            {
                logger.Warn("SimpleUpload Error, fileName=" + fileName, ex);
                throw;
            }
            finally
            {
                try
                {
                    request = null;
                }
                catch { }
            }
        }

        public bool CheckDirectoryExist(string folder)
        {
            bool flag = false;
            FtpWebRequest request = null;
            request = (FtpWebRequest)FtpWebRequest.Create("ftp://" + Config.Host + "/" + folder);
            /* Log in to the FTP Server with the User Name and Password Provided */
            request.Credentials = new NetworkCredential(Config.UserId, Config.Password);

            request.Method = WebRequestMethods.Ftp.ListDirectory;
            try
            {
                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    Stream responseStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream);
                    string files = reader.ReadToEnd();
                    if (!string.IsNullOrEmpty(files))
                    {
                        flag = true;
                    }
                }
            }
            catch
            {
                flag = false;
            }

            /* Resource Cleanup */
            finally
            {
                request = null;
            }

            return flag;
        }

        public void MakeDirectory(string folder)
        {
            FtpWebRequest ftpRequest = null;
            try
            {
                ftpRequest = (FtpWebRequest)WebRequest.Create("ftp://" + Config.Host + "/" + folder);
                ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                ftpRequest.Credentials = new NetworkCredential(Config.UserId, Config.Password);
                using (var resp = (FtpWebResponse)ftpRequest.GetResponse())
                {
                    Console.WriteLine(resp.StatusCode);
                }
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    response.Close();
                }
                else
                {
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Warn("MakeDirectory Error, folder=" + folder, ex);
                //throw;
            }
            finally
            {
                try
                {
                    ftpRequest = null;
                }
                catch { }
            }
        }

        public void TaishinUpload(string filePath, string folder, string fileName)
        {
            try
            {
                FtpWebRequest ftpRequest = null;
                try
                {
                    ftpRequest = (FtpWebRequest)WebRequest.Create("ftp://" + Config.Host + "/" + folder + "/" + fileName);
                    ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                    ftpRequest.Credentials = new NetworkCredential(Config.UserId, Config.Password);
                    ftpRequest.UsePassive = false;
                    ftpRequest.EnableSsl = false;

                    using (FileStream fs = File.OpenRead(filePath))
                    {
                        byte[] buffer = new byte[fs.Length];
                        fs.Read(buffer, 0, buffer.Length);
                        fs.Close();
                        Stream requestStream = ftpRequest.GetRequestStream();
                        requestStream.Write(buffer, 0, buffer.Length);
                        requestStream.Flush();
                        requestStream.Close();
                    }
                }
                catch (Exception ex)
                {
                    logger.Warn("TaishinUpload Error, fileName=" + fileName, ex);
                    throw;
                }
                finally
                {
                    try
                    {
                        ftpRequest = null;
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool TaishinCheckFileExist(string fileName)
        {
            bool falg = false;
            FtpWebRequest request = null;
            try
            {
                request = (FtpWebRequest)WebRequest.Create("ftp://" + Config.Host + "/" + fileName);
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(Config.UserId, Config.Password);
                request.UsePassive = false;
                request.EnableSsl = false;

                WebResponse response = request.GetResponse();
                falg = true;

                response.Close();
                response = null;

            }
            catch
            {
                falg = false;
            }
            finally
            {
                try
                {
                    request = null;
                }
                catch { }
            }
            return falg;
        }
        public bool SimpleCheckFileExist(string DirName, string fileName)
        {
            bool falg = false;
            FtpWebRequest request = null;
            try
            {
                request = (FtpWebRequest)WebRequest.Create("ftp://" + Config.Host + "/" + DirName + "/" + fileName);
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(Config.UserId, Config.Password);

                WebResponse response = request.GetResponse();
                falg = true;

                response.Close();
                response = null;

            }
            catch
            {
                falg = false;
            }
            finally
            {
                try
                {
                    request = null;
                }
                catch { }
            }
            return falg;
        }

    }
}



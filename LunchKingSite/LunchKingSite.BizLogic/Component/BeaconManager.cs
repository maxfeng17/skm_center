﻿using log4net;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class BeaconManager
    {
        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static ISkmProvider skmMp = ProviderFactory.Instance().GetProvider<ISkmProvider>();
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        protected static IBeaconProvider beaconMp = ProviderFactory.Instance().GetProvider<IBeaconProvider>();
        protected static IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        private static ILog logger = LogManager.GetLogger("beacon");

        /// <summary>
        /// 簡單工廠模式:由一個工廠類根據傳入的參量決定創建出哪一種?品類的實例
        /// </summary>
        public class BeaconFatory
        {
            /// <summary>
            /// 透過參數去決定用哪個Qrcode類別實現
            /// </summary>
            /// <param name="beaconModel">beacon相關資訊</param>
            /// <param name="userId">身分驗證</param>
            /// <param name="appDeviceId">裝置驗證</param>
            /// <param name="accessToken">OauthToken</param>
            /// <returns></returns>
            public IBeaconBase CreateFatory(BeaconTriggerModel beaconModel, int userId, string appDeviceId, string accessToken)
            {
                try
                {
                    //此時的major/minor可能為電量算換用, 故無法直接判斷場域問題, 所以不在此mapping場域
                    OauthToken oauthToken = CommonFacade.OauthTokenGet(accessToken);
                    return CreateFatory(beaconModel, oauthToken, userId, appDeviceId);
                }
                catch (Exception e)
                {
                    var item = new PromoItem()
                    {
                        Code = "SkmBeaconExceptionLog",
                        UseTime = DateTime.Now,
                        Memo = string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4},log:{5}", accessToken, userId, beaconModel.uuId, beaconModel.major, beaconModel.minor, "CreateFatory1(appDeviceId="+ appDeviceId + ").Exception=" + e.Message),
                        UserId = userId
                    };
                    SystemFacade.SetApiLog("BeaconManager.BeaconFatory.CreateFatory1", userId.ToString(), beaconModel, item);
                    
                    return null;
                }
            }

            /// <summary>
            /// 透過參數去決定用哪個Qrcode類別實現
            /// </summary>
            /// <param name="beaconModel">beacon相關資訊</param>
            /// <param name="userId">身分驗證</param>
            /// <param name="appDeviceId">裝置驗證</param>
            /// <returns></returns>
            public IBeaconBase CreateFatory(BeaconTriggerModel beaconModel, int userId, string appDeviceId)
            {
                //此時的major/minor可能為電量算換用, 故無法直接判斷場域問題, 所以不在此mapping場域
                OauthToken oauthToken = CommonFacade.OauthTokenGet(userId);
                return CreateFatory(beaconModel, oauthToken, userId, appDeviceId);
            }

            public IBeaconBase CreateFatory(BeaconTriggerModel beaconModel, OauthToken oauthToken, int userId, string appDeviceId)
            {
                try
                {
                    BeaconTriggerApp triggerApp = beaconMp.GetBeaconTriggerApp(oauthToken.AppId);
                    if (triggerApp.TriggerAppId != 0)
                    {
                        switch (triggerApp.TriggerAppId)
                        {
                            case (int)BeaconTriggerAppType.Life17Beacon:
                                return new Life17Beacon(beaconModel, triggerApp, userId, appDeviceId, oauthToken.AccessToken);
                            case (int)BeaconTriggerAppType.SkmBeaconBeacon:
                                return new SkmBeacon(beaconModel, triggerApp, userId, appDeviceId, oauthToken.AccessToken);
                            default:
                                return null;
                        }
                    }
                }
                catch (Exception e)
                {
                    var item = new PromoItem()
                    {
                        Code = "SkmBeaconExceptionLog",
                        UseTime = DateTime.Now,
                        Memo = string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4},log:{5}", oauthToken.AccessToken, userId, beaconModel.uuId, beaconModel.major, beaconModel.minor, "CreateFatory3(appDeviceId=" + appDeviceId + ").Exception=" + e.Message),
                        UserId = userId
                    };
                    SystemFacade.SetApiLog("BeaconManager.BeaconFatory.CreateFatory3", userId.ToString(), beaconModel, item);
                    
                    return null;
                }

                return null;
            }
            
            /// <summary>
            /// 指定17LifeBeacon (非會員可使用)
            /// </summary>
            /// <param name="beaconModel"></param>
            /// <param name="userId"></param>
            /// <param name="appDeviceId"></param>
            /// <returns></returns>
            public IBeaconBase CreateFatoryBy17Life(BeaconTriggerModel beaconModel, int userId, string appDeviceId)
            {
                BeaconTriggerApp triggerApp = beaconMp.GetBeaconTriggerApp((int)BeaconTriggerAppFunctionName.life17);
                return new Life17Beacon(beaconModel, triggerApp, userId, appDeviceId, string.Empty);
            }
        }

        /// <summary>
        /// Beacon介面
        /// </summary>
        public interface IBeaconBase
        {
            /// <summary>
            /// BeaconUUIDType
            /// </summary>
            BeaconUuidType UuidType { set; get; }
            /// <summary>
            /// App接收的資料模式
            /// </summary>
            object PokeballList { get; set; }
            /// <summary>
            /// API錯誤訊息
            /// </summary>
            string ErrorMessage { get; set; }
        }

        /// <summary>
        /// Beacon基本架構
        /// </summary>
        public abstract class BeaconBase : IBeaconBase
        {
            #region prop
            
            /// <summary>
            /// BeaconUUIDType
            /// </summary>
            public BeaconUuidType UuidType { set; get; }
            /// <summary>
            /// App接收的資料模式
            /// </summary>
            public object PokeballList { set; get; }
            /// <summary>
            /// API錯誤訊息
            /// </summary>
            public string ErrorMessage { get; set; }
            /// <summary>
            /// 個人訊息中心紀錄
            /// </summary>
            protected List<BeaconLog> UserBeaconLogs { get; set; }
            /// <summary>
            /// 主推播Beacon訊息
            /// </summary>
            protected BeaconEvent TargetBeaconEvent { get; set; }
            /// <summary>
            /// 主推播App
            /// </summary>
            protected BeaconTriggerApp TriggerApp { get; set; }
            /// <summary>
            /// Beacon裝置編號
            /// </summary>
            protected BeaconDevice BeaconDevice { get; set; }
            /// <summary>
            /// 會員唯一碼
            /// </summary>
            protected int UserId { get; set;}
            /// <summary>
            /// 裝置唯一碼
            /// </summary>
            protected int appDeviceId { get; set; }
            /// <summary>
            /// Oauth.AccessToken
            /// </summary>
            protected string AccessToken { get; set; }
            /// <summary>
            /// 傳入Model
            /// </summary>
            protected BeaconTriggerModel BeaconTriggerModel { get; set; }
            /// <summary>
            /// beacon_group.id
            /// </summary>
            protected int GroupId { get; set; }
            /// <summary>
            /// deviceRecord.Id
            /// </summary>
            protected int DeviceRecordId { get; set; }

            protected string Floor { get;set;}

            #endregion

            /// <summary>
            /// 新架構
            /// </summary>
            /// <param name="beaconModel"></param>
            /// <param name="triggerApp"></param>
            /// <param name="userId"></param>
            /// <param name="IdentifierCode"></param>
            /// <param name="accessToken"></param>
            protected BeaconBase(BeaconTriggerModel beaconModel, BeaconTriggerApp triggerApp, int userId, string IdentifierCode, string accessToken)
            {
                ErrorMessage = string.Empty;
                TargetBeaconEvent = new BeaconEvent();
                BeaconTriggerModel = beaconModel;
                TriggerApp = triggerApp;
                UserId = userId;
                AccessToken = accessToken;
                appDeviceId = ConfirmAppDevice(IdentifierCode, userId);//取得DeviceIdentyfierInfo.Id

                //電量
                if (string.Equals(beaconModel.uuId, triggerApp.ElectricUuid.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    UuidType = BeaconUuidType.ElectricUuid;
                    SetElectric();
                   
                }
                else if (string.Equals(beaconModel.uuId, triggerApp.SellerUuid.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    UuidType = BeaconUuidType.SellerUuid;
                    SetSeller();
                }
                else
                {
                    SystemFacade.SetApiLog("BeaconManager.BeaconBase", userId.ToString(), beaconModel, "UUID不在範圍內");
                }
            }
            
            /// <summary>
            /// 確認裝置資訊
            /// </summary>
            /// <param name="identifierCode">裝置唯一碼</param>
            /// <param name="memberUserId"></param>
            public int ConfirmAppDevice(string identifierCode, int memberUserId)
            {
                if (string.IsNullOrEmpty(identifierCode))
                {
                    return 0;
                }

                try
                {
                    #region  確認裝置資訊

                    DeviceIdentyfierInfo device;

                    var deviceCols = lp.DeviceIdentyfierInfoListGet(identifierCode);
                    if (deviceCols.Any())
                    {
                        device = deviceCols.ToList().OrderByDescending(x => x.Id).First();
                        device.ModifyTime = DateTime.Now;
                    }
                    else
                    {
                        device = new DeviceIdentyfierInfo();
                        device.IdentifierCode = identifierCode;
                        device.CreateTime = DateTime.Now;
                        device.ActionTrigger = "BeaconEvent";
                    }

                    if (memberUserId > 0)
                    {
                        device.MemberUniqueId = memberUserId;
                    }

                    lp.DeviceIdentyfierInfoSet(device);

                    #endregion  確認裝置資訊

                    return device.Id;
                }
                catch (Exception e)
                {
                    var memo =  "ConfirmAppDevice Exception=" + e;
                    SystemFacade.SetApiLog("BeaconManager.BeaconBase.ConfirmAppDevice", memberUserId.ToString(), identifierCode, memo);
                }

                return 0;
            }

            /// <summary>
            /// 電量處理
            /// </summary>
            /// <returns></returns>
            public void SetElectric()
            {
                try
                {
                    //電量資訊
                    if (BeaconTriggerModel.major > 0 && BeaconTriggerModel.minor > 0)
                    {
                        var beaconBatteryCapacity = ApiActionFilterManager.DecodeBatteryCapacityOfBeacon(BeaconTriggerModel.minor);
                        var beaconMacAddress1 = ApiActionFilterManager.DecodeMacAddressOfBeacon(BeaconTriggerModel.major, BeaconTriggerModel.minor);
                        var beaconMacAddress2 = ApiActionFilterManager.DecodeMacAddressOfBeacon2(BeaconTriggerModel.major, BeaconTriggerModel.minor);

                        BeaconDevice = beaconMp.GetBeaconDeviceByMacAddress(beaconMacAddress1, beaconMacAddress2);

                        //一天只會有24/6=4次 update and add PromoItem 資料
                        if (BeaconDevice.DeviceId != 0 
                            && BeaconDevice.LastUpdateTime.AddHours(6) <= DateTime.Now)
                        {
                            BeaconDevice.ElectricPower = beaconBatteryCapacity;
                            BeaconDevice.LastUpdateTime = DateTime.Now;
                            //更新電量
                            beaconMp.InsertOrUpdateBeaconDevice(BeaconDevice);
                        }
                    }
                }
                catch (Exception e)
                {
                    SystemFacade.SetApiLog("BeaconManager.BeaconBase.SetElectric", UserId.ToString(), BeaconTriggerModel, "SetElectric().Exception: " + e);
                }

                SetElectricBeaconTriggerLog("");
                PokeballList = new List<Pokeball>();
            }

            /// <summary>
            /// SELLER處理
            /// </summary>
            /// <returns></returns>
            public void SetSeller()
            {
                bool isNeedLog = true;
                try
                {
                    BeaconDevice = beaconMp.GetBeaconDevice(BeaconTriggerModel.major, BeaconTriggerModel.minor, TriggerApp.TriggerAppId);

                    // 合法場域下的beaconDevice
                    if (BeaconDevice.DeviceId != 0)
                    {
                        //取得個人資料紀錄
                        UserBeaconLogs = BeaconFacade.GetUserBeaconLog(TriggerApp.TriggerAppId, UserId, appDeviceId);

                        // 此Method由子類別去判斷訊息限制  (類似則數上限/間隔)
                        if (CheckIsLimited() == false)
                        {
                            //取得有效時間內的EventIds 
                            List<int> effectEventIds = beaconMp.GetEffectiveBeaconEventIds(DateTime.Now, TriggerApp.TriggerAppId);

                            if (effectEventIds.Any())
                            {
                                //取訊息紀錄與有效eventids差集(收過不再收)
                                effectEventIds = effectEventIds.Except(UserBeaconLogs.Select(p => p.EventId).ToList()).ToList();
                                if (effectEventIds.Any())
                                {
                                    if (GetBeaconEvent(effectEventIds, out isNeedLog)) //有取到訊息才有往下做判斷的必要
                                    {
                                        if (SaveDevicePushRecord())//儲存訊息至訊息中心+beacon_log
                                        {
                                            PokeballList = SetPokeBallList();//組pokeball json
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessage = "今日相關事件已觸發完畢";
                                }
                            }
                            else
                            {
                                isNeedLog = false;  //此事件量太大，不再記入DB
                                ErrorMessage = "今日無相關觸發事件(BeaconEvent)";
                            }
                        }
                        else
                        {
                            ErrorMessage = "受限於冷卻或則數限制";
                        }
                    }
                    else
                    {
                        ErrorMessage = "不為合法場域下的beacon";
                    }

                }
                catch (Exception e)
                {
                    SystemFacade.SetApiLog("BeaconManager.BeaconBase.SetSeller", UserId.ToString(), BeaconTriggerModel, "SetSeller().Exception: " + e);
                }

                if (TargetBeaconEvent.EventType != (int)EventType.SubEvent && isNeedLog)
                {
                    //記錄log供BI分析
                    SetSellerBeaconTriggerLog(ErrorMessage);//LOG
                }
            }
            
            /// <summary>
            /// 取得欲推播(Alert)的訊息
            /// </summary>
            /// <returns></returns>
            public virtual bool GetBeaconEvent(List<int> effectEventIds, out bool isNeedLog)
            {
                isNeedLog = true;

                List<BeaconGroupDeviceLink> beaconGroupDeviceLinks = beaconMp.GetBeaconGroupDeviceLinkList(TriggerApp.TriggerAppId, BeaconDevice.DeviceId);
                if (beaconGroupDeviceLinks.Any())
                {
                    Floor = beaconGroupDeviceLinks.Where(p => !string.IsNullOrEmpty(p.Floor)).Select(p => p.Floor).FirstOrDefault();

                    var beaconGroupIds = beaconGroupDeviceLinks.Select(p => p.GroupId).ToList();
              
                    //2.TimeSlot判斷時間內
                    List<BeaconEventTimeSlot> toDayEventTimeSlots = beaconMp.GetEventTimeSlotByToDay(effectEventIds, beaconGroupIds, DateTime.Now, Floor);
                    
                    List<int> toDayEventTimeSlotsEventIds = toDayEventTimeSlots.Select(p => p.EventId).ToList();

                    if (!string.IsNullOrEmpty(AccessToken))//台新要可非會員使用
                    {
                        //3.客群判斷是否符合
                        toDayEventTimeSlotsEventIds = BeaconFacade.GetEventIdInTargets(toDayEventTimeSlotsEventIds, AccessToken);
                    }

                    if (toDayEventTimeSlotsEventIds.Any())
                    {
                        int eventId = toDayEventTimeSlotsEventIds.FirstOrDefault();

                        //回塞beacon.log的group_id
                        GroupId = toDayEventTimeSlots.Where(p => p.EventId == eventId).Select(p => p.GroupId).FirstOrDefault();
                        
                        //4.取交集且排序
                        TargetBeaconEvent = beaconMp.GetBeaconEvent(eventId);
                    }
                    else
                    {
                        ErrorMessage = "不符合指定客群限制";
                    }
                }
                else
                {
                    ErrorMessage = "無beacon所屬群組或事件";
                }
                return TargetBeaconEvent.EventId > 0;
            }

            /// <summary>
            /// 此Method由子類別去判斷訊息限制  (類似上限/間隔/有效時間)
            /// </summary>
            public virtual bool CheckIsLimited()
            {
                if (UserBeaconLogs.Any())
                {
                    //每日上線則數判斷
                    if (UserBeaconLogs.Count() > TriggerApp.DailyMaxReceivedCount)
                    {
                        return true;
                    }

                    //cc時間判斷
                    var lastRecordDateTime = UserBeaconLogs.OrderByDescending(p => p.CreateDate).FirstOrDefault().CreateDate;
                    if (lastRecordDateTime.AddSeconds(TriggerApp.CdSec) > DateTime.Now)
                    {
                        return true;
                    }
                }
                return false;
            }
            
            /// <summary>
            /// 儲存訊息至訊息中心 (注意子訊息)
            /// </summary>
            /// <returns></returns>
            public bool SaveDevicePushRecord()
            {
                try
                {
                    var beaconLog = new BeaconLog()
                    {
                        TriggerAppId = TriggerApp.TriggerAppId,
                        UserId = UserId,
                        EventId = TargetBeaconEvent.EventId,
                        BeaconDeviceId = BeaconDevice.DeviceId,
                        GroupId = GroupId,
                        AppDeviceId = appDeviceId,
                        CreateDate =  DateTime.Now,
                    };

                    if (beaconMp.InsertOrUpdateBeaconLog(beaconLog))
                    {
                        #region Save DevicePushRecord

                        //判斷是否有子訊息, 決定存一筆或多筆
                        if (TargetBeaconEvent.EventType == (int)EventType.SubEvent)
                        {
                            List<int> beaconSubEventIds = BeaconFacade.GetBeaconSubeventIds(TargetBeaconEvent.EventId);
                            foreach (int subId in beaconSubEventIds)
                            {
                                DataOrm.DevicePushRecord deviceRecord = new DataOrm.DevicePushRecord();
                                deviceRecord.ActionId = subId;
                                deviceRecord.DeviceId = appDeviceId; // DeviceIdentyfierInfo.Id
                                deviceRecord.CreateTime = DateTime.Now;
                                if (UserId > 0)
                                {
                                    deviceRecord.MemberId = UserId;
                                    deviceRecord.IdentifierType = 2;
                                }
                                else
                                {
                                    deviceRecord.IdentifierType = 1; //IdentifierType 1 為廣告 2 為個人
                                }
                                lp.DevicePushRecordSet(deviceRecord);

                                var item = new PromoItem()
                                {
                                    Code = "SkmBeaconTriggerLog",
                                    UseTime = DateTime.Now,
                                    Memo = string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4},SkmPushActionEventPushMsgId={5},recordId={6}",
                                    AccessToken, UserId, BeaconTriggerModel.uuId, BeaconTriggerModel.major, BeaconTriggerModel.minor, TargetBeaconEvent.EventId, deviceRecord.Id),
                                    UserId = UserId
                                };
                                ep.PromoItemSet(item);
                                if (DeviceRecordId == 0)
                                {
                                    DeviceRecordId = deviceRecord.Id;
                                }
                            }
                        }
                        else
                        {
                            DataOrm.DevicePushRecord deviceRecord = new DataOrm.DevicePushRecord();
                            deviceRecord.ActionId = TargetBeaconEvent.EventId;
                            deviceRecord.DeviceId = appDeviceId;
                            deviceRecord.CreateTime = DateTime.Now;
                            if (UserId > 0)
                            {
                                deviceRecord.MemberId = UserId;
                                deviceRecord.IdentifierType = 2;
                            }
                            else
                            {
                                deviceRecord.IdentifierType = 1; //IdentifierType 1 為廣告 2 為個人
                            }
                            lp.DevicePushRecordSet(deviceRecord);
                            DeviceRecordId = deviceRecord.Id;
                        }

                        #endregion
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    SetSellerBeaconExceptionLog("SaveDevicePushRecord()儲存至訊息中心發生問題  Exception:" + e.Message);
                    return false;
                }
                return true;
            }

            /// <summary>
            /// 組推播Alert資訊 
            /// </summary>
            public virtual object SetPokeBallList()
            {
               return new List<Pokeball>();
            }
            
            public abstract void SetElectricBeaconTriggerLog(string log);

            public abstract void SetSellerBeaconTriggerLog(string log);

            public virtual void SetSellerBeaconExceptionLog(string errorMsg)
            {
                //需要再打開 紀錄SkmBeaconExceptionLog
                //logger.Info("SkmBeaconExceptionLog: "+ errorMsg);
            }
        }

        /// <summary>
        /// 新光
        /// </summary>
        public class SkmBeacon : BeaconBase
        {
            public SkmBeacon(BeaconTriggerModel beaconModel, BeaconTriggerApp triggerApp, int memberUinqueId, string appDeviceId, string accessToken) 
                : base(beaconModel, triggerApp, memberUinqueId, appDeviceId, accessToken) { }
            
            /// <summary>
            /// 取得欲推播(Alert)的訊息
            /// </summary>
            /// <returns></returns>
            public override bool GetBeaconEvent(List<int> effectEventIds, out bool isNeedLog)
            {
                isNeedLog = true;

                //1.Beacon所屬的群組
                List<BeaconGroupDeviceLink> beaconGroupDeviceLinks = beaconMp.GetBeaconGroupDeviceLinkList(TriggerApp.TriggerAppId, BeaconDevice.DeviceId);

                if (beaconGroupDeviceLinks.Any())
                {
                    Floor = beaconGroupDeviceLinks.Where(p => !string.IsNullOrEmpty(p.Floor)).Select(p => p.Floor).FirstOrDefault();

                    var beaconGroupIds = beaconGroupDeviceLinks.Select(p => p.GroupId).ToList();

                    //2.TimeSlot判斷時間內(已有Distinct)
                    List<BeaconEventTimeSlot> effectEventTimeSlots = beaconMp.GetEventTimeSlotByToDay(effectEventIds, beaconGroupIds, DateTime.Now, Floor);
                    
                    if (effectEventTimeSlots.Any())
                    {
                        //3.客群判斷是否符合
                        List<int> effectEventTimeSlotsEventIds = BeaconFacade.GetEventIdInTargets(effectEventTimeSlots.Select(p => p.EventId).ToList(), AccessToken);

                        if (effectEventTimeSlotsEventIds.Any())
                        {
                            int eventId = effectEventTimeSlotsEventIds.FirstOrDefault();

                            //回塞beacon.log的group_id
                            GroupId = effectEventTimeSlots.Where(p => p.EventId == eventId).Select(p => p.GroupId).FirstOrDefault();
                            
                            //4.取交集且排序
                            TargetBeaconEvent = beaconMp.GetBeaconEvent(eventId);
                        }
                        else
                        {
                            ErrorMessage = "不符合指定客群限制 toDayEventTimeSlots :" + string.Join(",", effectEventTimeSlots) + " / accessToken:"+ AccessToken;
                        }
                    }
                    else
                    {
                        isNeedLog = false; //資料量太大不記入DB
                        ErrorMessage = "不在TimeSlot判斷時間內 effectEventIds :" + string.Join(",", effectEventIds) + " / beaconGroupIds :" + string.Join(",", beaconGroupIds);
                    }
                }
                else
                {
                    ErrorMessage = "無beacon所屬群組或事件 effectEventIds :" + string.Join(",", effectEventIds);
                }
                return TargetBeaconEvent.EventId > 0;
            }

            /// <summary>
            /// 此Method由子類別去判斷訊息限制  (類似上限/間隔/有效時間)
            /// </summary>
            public override bool CheckIsLimited()
            {
                try
                {
                    if (UserBeaconLogs.Any())
                    {
                        //cd時間判斷
                        DateTime lastRecordDateTime = UserBeaconLogs.OrderByDescending(p => p.CreateDate).FirstOrDefault().CreateDate;
                        if (lastRecordDateTime.AddSeconds(TriggerApp.CdSec) > DateTime.Now)
                        {
                            return true;
                        }

                        //每日則數上限判斷  //不為指定才需要考慮則數上限
                        //3. 若主訊息擁有9筆仔訊息(類似上次舉例的九間銀行信用卡msg), 那則數算9筆還1筆?  => 1筆 
                        if (UserBeaconLogs.Count() > TriggerApp.DailyMaxReceivedCount)
                        {
                            return true;
                        }
                    }
                }
                catch(Exception e)
                {
                    ErrorMessage = "CheckIsLimited(UserBeaconLogs.Count()=" + UserBeaconLogs.Count + ").Exception=" + e.Message;
                }
                return false;
            }

            /// <summary>
            /// 組推播Alert資訊 
            /// </summary>
            public override object SetPokeBallList()
            {
                var pokeballList = new List<Pokeball>();

                try
                {
                    var pokeball = new Pokeball()
                    {
                        Message = TargetBeaconEvent.Content,//推播顯示內文, 非標題
                        Type = PokeballType.localPush.ToString()
                    };

                    var actionEvent = BeaconFacade.GetPokeBallActionParameter(TargetBeaconEvent);
                    pokeball.ActionList.Add(actionEvent);
                
                    if (TargetBeaconEvent.EventType!= (int)EventType.SubEvent)//只有子訊息類型的eventID不做已讀
                    {
                        pokeball.ActionList.Add(new Core.ModelCustom.Action
                        {
                            ActionType = Helper.GetEnumDescription(PokeballActionType.MarkMessageRead),
                            MessageId = TargetBeaconEvent.EventId.ToString(),
                            TriggerType = PokeballTriggerType.BeaconMessageRead
                        });
                    }
                    pokeballList.Add(pokeball);
                }
                catch (Exception e)
                {
                    SetSellerBeaconExceptionLog("SetPokeBallList().Exception=" + e.Message);
                }
                
                return pokeballList;// SET
            }

            /// <summary>
            /// SKM 觸發更新電量Log
            /// </summary>
            /// <param name="log"></param>
            public override void SetElectricBeaconTriggerLog(string log)
            {
                if (string.IsNullOrEmpty(log))
                {
                    log = "更新電量記錄";
                }

                var item = new PromoItem()
                {
                    Code = "SkmBeaconElectricLog",
                    UseTime = DateTime.Now,
                    Memo = string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4}, log:{5}", AccessToken, UserId, BeaconTriggerModel.uuId, BeaconTriggerModel.major, BeaconTriggerModel.minor, log),
                    UserId = UserId
                };

                SystemFacade.SetApiLog("BeaconManager.BeaconFatory.SetElectricBeaconTriggerLog", UserId.ToString(), "", item);
            }

            /// <summary>
            /// SKM 觸發Beacan訊息Log
            /// </summary>
            /// <param name="log"></param>
            public override void SetSellerBeaconTriggerLog(string log)
            {
                string memo;

                //沒訊息
                if (TargetBeaconEvent.EventId == 0 && DeviceRecordId == 0)
                {
                    //沒有Event就不再記log了
                    //memo = string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4},SkmPushActionEventPushMsgId={5},log={6}",
                    //       AccessToken, UserId, BeaconTriggerModel.uuId, BeaconTriggerModel.major, BeaconTriggerModel.minor, -1, log);
                    return;
                }

                //正常有訊息
                {
                    memo = string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4},SkmPushActionEventPushMsgId={5},recordId={6}",
                           AccessToken, UserId, BeaconTriggerModel.uuId, BeaconTriggerModel.major, BeaconTriggerModel.minor, TargetBeaconEvent.EventId, DeviceRecordId);
                }

                var item = new PromoItem()
                {
                    Code = "SkmBeaconTriggerLog",
                    UseTime = DateTime.Now,
                    Memo = memo,
                    UserId = UserId
                };

                //記錄log供BI分析
                ep.PromoItemSet(item);
            }

            #region 舊架構寫法

            ///// <summary>
            ///// GetBeaconMsg
            ///// </summary>
            ///// <param name="beaconModel"></param>
            ///// <param name="memberUinqueId"></param>
            //public void _GetBeaconEvent(BeaconTriggerModel beaconModel, int memberUinqueId)
            //{
            ////    //seller的major與minor是正常的   電量的major與minor會被設計用來儲存電量資訊, 用此major與minor會找不到beacon
            ////    var triggerBeaconDevice = mp.ActionEventDeviceInfoByMajorMinorGet(activityId, beaconModel.major, beaconModel.minor);
            ////    if (triggerBeaconDevice == null)
            ////    {
            ////        notificationMessage = new SkmBeaconMessage();
            ////    }

            ////    SkmBeaconMessage sortBeaconMessage = new SkmBeaconMessage();

            ////    var pushedActionIdList = myDevicePushRecords.Select(x => x.ActionId).ToList();//UserPushRecord

            ////    List<SkmBeaconMessage> skmBeaconMessageList = skmMp.SkmBeaconMessageGetListByTimeByBeaconID(
            ////                                                    config.SkmBeaconChecked, //測試期間不綁定特定Beacon
            ////                                                    DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"),
            ////                                                    DateTime.Now.ToString("HH:mm"),
            ////                                                    triggerBeaconDevice.Major.ToString() + triggerBeaconDevice.Minor.ToString()
            ////                                                  ).ToList();

            ////    var skmBeaconTimeSlotCol = skmMp.SkmBeaconMessageTimeSlotGetList("", SkmBeaconMessageTimeSlot.Columns.EffectiveStart + " <= " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), SkmBeaconMessageTimeSlot.Columns.EffectiveEnd + " > " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            ////    var skmMember = skmMp.MemberGetByUserId(memberUinqueId);

            ////    // ReSort順序 (誰先誰後的順序還會再改~~~) 
            ////    foreach (var skmBeaconTimeSlot in skmBeaconTimeSlotCol.Where(x => x.Status == (int)DealTimeSlotStatus.Default).OrderBy(x => x.BeaconType).ThenBy(x => x.Sequence))
            ////    {
            ////        var message = skmBeaconMessageList.FirstOrDefault(x => x.BusinessHourGuid == skmBeaconTimeSlot.BusinessHourGuid);
            ////        if ((message != null && message.Id > 0) && (!pushedActionIdList.Contains(message.Id))) // 大家都有的判斷
            ////        {
            ////            List<string> skmBeaconGroupMemberIdList = new List<string>();
            ////            switch (skmBeaconTimeSlot.BeaconType)
            ////            {
            ////                case (int)SkmBeaconType.Top://取置頂排序
            ////                    sortBeaconMessage = message;
            ////                    break;
            ////                case (int)SkmBeaconType.Deal://目前因為switch寫法會讓商品順序往上升, 是錯誤的, 但猶豫要重構了所以沒差 暫放
            ////                    var memberCollectDealBids = ApiMemberManager.GetApiMemberCollectDealList(memberUinqueId, MemberCollectDealType.Coupon).Select(x => x.Bid).ToList();
            ////                    if (memberCollectDealBids.Any() && memberCollectDealBids.Contains(message.BusinessHourGuid)) //取得商品+個人收藏訊息
            ////                    {
            ////                        sortBeaconMessage = message;
            ////                    }
            ////                    else if (message.Status != (int)SkmBeaconStatus.Deal) //商品 不為收藏通知設定才得進入
            ////                    {
            ////                        sortBeaconMessage = message;
            ////                    }
            ////                    break;
            ////                case (int)SkmBeaconType.Project://取得專案 + 匯入資訊(全館活動)
            ////                    skmBeaconGroupMemberIdList = skmMp.SkmBeaconGroupGetByGuid(message.BusinessHourGuid).Select(x => x.MemberId).ToList();
            ////                    if (skmBeaconGroupMemberIdList.Any()) //有匯入才比較
            ////                    {
            ////                        if (IsContainSkmBeaconGroup(skmBeaconGroupMemberIdList, skmMember))
            ////                        {
            ////                            sortBeaconMessage = message;
            ////                        }
            ////                    }
            ////                    else
            ////                    {
            ////                        sortBeaconMessage = message;
            ////                    }
            ////                    break;
            ////                case (int)SkmBeaconType.Event_Manual://取行銷訊息_手動排序
            ////                    sortBeaconMessage = message;
            ////                    break;
            ////                case (int)SkmBeaconType.Event_Count://取行銷訊息_計算排序
            ////                    skmBeaconGroupMemberIdList = skmMp.SkmBeaconGroupGetByGuid(message.BusinessHourGuid).Select(x => x.MemberId).ToList();
            ////                    if (IsContainSkmBeaconGroup(skmBeaconGroupMemberIdList, skmMember))
            ////                    {
            ////                        sortBeaconMessage = message;
            ////                    }
            ////                    break;
            ////                default:
            ////                    break;
            ////            }
            ////        }
            ////    }

            ////    notificationMessage = sortBeaconMessage;
            ////    return notificationMessage.Id != 0;
            //}

            ///// <summary>
            ///// 判斷訊息限制  
            ///// config.SkmBeaconNotificationDailyLimit 新光 Beacon推播訊息-每日上限
            ///// config.SkmBeaconMsgBoxDailyLimit 新光 Beacon非推播訊息(messagebox)
            ///// config.SkmBeaconNotificationTimeSpanMinutes 新光 Beacon推播訊息-觸發時間間隔
            ///// config.SkmBeaconMsgBoxTimeSpanMinutes 新光 Beacon非推播訊息(messagebox)-觸發時間間隔
            ///// </summary>
            ///// <returns></returns>
            //public bool _CheckLimit()
            //{

            //    bool isInTimeSpan = false;

            //    //取得今日推播訊息中心上限
            //    todayMsgBoxCount = myDevicePushRecords.Where(x => x.IsDeal == false).Count(x => x.CreateTime.Date.Equals(DateTime.Now.Date));

            //    //推播及非推播每日總上限
            //    bool isLessThanDailyLimit = todayMsgBoxCount < (config.SkmBeaconNotificationDailyLimit + config.SkmBeaconMsgBoxDailyLimit);

            //    var lastPushRecord = myDevicePushRecords.ToList().OrderByDescending(x => x.CreateTime).FirstOrDefault();

            //    if (lastPushRecord != null && lastPushRecord.Id > 0)
            //    {
            //        if (todayMsgBoxCount < config.SkmBeaconNotificationDailyLimit)
            //        {
            //            isInTimeSpan = (DateTime.Now < lastPushRecord.CreateTime.AddMinutes(config.SkmBeaconNotificationTimeSpanMinutes));
            //        }
            //        else
            //        {
            //            isInTimeSpan = (DateTime.Now < lastPushRecord.CreateTime.AddMinutes(config.SkmBeaconMsgBoxTimeSpanMinutes));
            //        }
            //    }

            //    if (isLessThanDailyLimit && !isInTimeSpan)
            //    {
            //        isVaild = true;
            //    }
            //    else
            //    {
            //        isVaild = false;
            //    }

            //    return isVaild;
            //}

            ///// <summary>
            ///// SetPokeBall
            ///// </summary>
            ///// <param name="beaconModel"></param>
            ///// <param name="memberUinqueId"></param>
            ///// <param name="deviceId"></param>
            //public List<Pokeball> _SetPokeBall(BeaconTriggerModel beaconModel, int memberUinqueId, int deviceId)
            //{
            //    Pokeball pokeball = new Pokeball();
            //    BeaconPushShowType showType = BeaconPushShowType.NoAlert;
            //    bool isDealCollection = false;
            //    if (notificationMessage.Status.Equals((int)SkmBeaconStatus.Deal))
            //    {
            //        showType = BeaconPushShowType.ShowAlert;

            //        #region 收藏檔次推播
            //        //收藏檔次推播
            //        var deviceRecord = new DevicePushRecord();
            //        deviceRecord.ActionId = notificationMessage.Id;
            //        deviceRecord.DeviceId = default(int);
            //        deviceRecord.CreateTime = DateTime.Now;
            //        deviceRecord.MemberId = memberUinqueId;
            //        deviceRecord.IdentifierType = 2;
            //        //deviceRecord.IsRemove = true;
            //        deviceRecord.IsDeal = true;
            //        deviceRecord.RemoveTime = DateTime.Now;
            //        lp.DevicePushRecordSet(deviceRecord);

            //        pokeball.Type = "localPush";
            //        pokeball.Message = notificationMessage.Subject;

            //        if (notificationMessage.BeaconType == (int)SkmBeaconType.Deal)
            //        {
            //            Action actin_event = new Action { ActionType = "showDealDetail", Bid = notificationMessage.BusinessHourGuid.ToString() };
            //            pokeball.ActionList.Add(actin_event);
            //        }

            //        #endregion 收藏檔次推播

            //        #region Beacon_Log 

            //        //SkmBeaconTriggerLog("SkmBeaconTriggerLog", string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4},SkmPushActionEventPushMsgId={5},recordId={6}", accessToken, memberUinqueId, model.uuId, model.major, model.minor, notificationMessage.Id, deviceRecord.Id));

            //        #endregion Beacon_Log
            //    }
            //    else if (isVaild && notificationMessage.Status.Equals((int)SkmBeaconStatus.Normal))
            //    {
            //        #region 其他類型推播
            //        //其他類型推播
            //        var deviceRecord = new DevicePushRecord();
            //        deviceRecord.ActionId = notificationMessage.Id;
            //        deviceRecord.DeviceId = default(int);
            //        deviceRecord.CreateTime = DateTime.Now;
            //        deviceRecord.MemberId = memberUinqueId;
            //        deviceRecord.IdentifierType = 2;
            //        lp.DevicePushRecordSet(deviceRecord);

            //        if (todayMsgBoxCount < config.SkmBeaconNotificationDailyLimit)
            //        {
            //            showType = BeaconPushShowType.ShowAlert;
            //            pokeball.Type = "localPush";
            //            pokeball.Message = notificationMessage.Subject;

            //            if (notificationMessage.BeaconType == (int)SkmBeaconType.Deal)
            //            {
            //                Action actin_event = new Action { ActionType = "showDealDetail", Bid = notificationMessage.BusinessHourGuid.ToString() };
            //                pokeball.ActionList.Add(actin_event);
            //            }

            //            Action action_message = new Action { ActionType = "markMessageRead", MessageId = deviceRecord.Id.ToString(), TriggerType = "1" };

            //            pokeball.ActionList.Add(action_message);

            //        }
            //        else
            //        {
            //            showType = BeaconPushShowType.NoAlert;
            //        }

            //        #endregion 其他類型推播

            //        #region Beacon_Log 

            //        //SkmBeaconTriggerLog("SkmBeaconTriggerLog", string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4},SkmPushActionEventPushMsgId={5},recordId={6}", accessToken, memberUinqueId, model.uuId, model.major, model.minor, notificationMessage.Id, deviceRecord.Id));

            //        #endregion Beacon_Log

            //    }
            //    //紀錄beacon msg 以便config.limit判斷
            //    //skmMp.SkmBeaconMessageRelatedSet(new SkmBeaconMessageRelated() { ShowType = (byte)showType, MemberUniqueId = memberUinqueId, SkmBeaconMessageId = notificationMessage.Guid.Value, IsDealCollection = isDealCollection });
            //    PokeballList = new List<Pokeball>();
            //    PokeballList.Add(pokeball);

            //    return PokeballList;
            //}

            ///// <summary>
            ///// 判斷是否存在指定客群  skmbeacongroup.[member_id] == skm_member.[skm_token]
            ///// </summary>
            ///// <param name="skmBeaconGroupMemberIDList">指定群眾的關係表</param>
            ///// <param name="skmMember">新光會員是以亂數token形式與17life綁定會員關係</param>
            ///// <returns></returns>
            //private bool IsContainSkmBeaconGroup(List<string> skmBeaconGroupMemberIDList, SkmMember skmMember)
            //{
            //    if (skmBeaconGroupMemberIDList.Any() && skmMember.Id > 0)
            //    {
            //        return skmBeaconGroupMemberIDList.Contains(skmMember.SkmToken);
            //    }
            //    return false;
            //}

            #endregion
        }

        /// <summary>
        /// 台新
        /// </summary>
        public class Life17Beacon : BeaconBase
        {
            public Life17Beacon(BeaconTriggerModel beaconModel, BeaconTriggerApp triggerApp, int memberUinqueId, string appDeviceId, string accessToken) 
                : base(beaconModel, triggerApp, memberUinqueId, appDeviceId, accessToken) { }

            public override object SetPokeBallList()
            {
              #region 原始json
              //{
              //    "Code": 0,
              //    "Data": {
              //          "PokeballList": [
              //          {
              //              "Type": "LocalPush",
              //              "Message": "888專案貴賓您好，雞年納福迎新春!即日起至1/26限量贈送「雞年發財金」乙組，快到台新銀行索取。(限量1,000組，送完為止)",
              //               "PokeballList": [
              //               {
              //                  "Type": "FullscreenWebView",
              //                  "Url": "https://www.taishinbank.com.tw/TS/TS02/TS0299/TS029901/TS02990115/TSB0558065"
              //                },
              //                {
              //                  "Type": "MarkMessageRead",
              //                  "MessageId": 155517052,
              //                  "TriggerType": 1
              //                }
              //          ]
              //        }
              //      ]
              //    },
              //    "Message": ""
              //  }
                #endregion
                
                return new
                {
                    Type = PokeballType.localPush.ToString(),
                    Message = TargetBeaconEvent.Subject,
                    PokeballList = new List<object>() {
                            new {
                                Type = PokeballType.FullscreenWebView.ToString(),
                                Url = TargetBeaconEvent.ActionUrl
                            },
                            new {
                                Type = PokeballType.MarkMessageRead.ToString(),
                                Url = DeviceRecordId.ToString(),
                                TriggerType = PokeballTriggerType.BeaconMessageRead
                            }
                        }
                };
            }

            public override void SetElectricBeaconTriggerLog(string log)
            {
                var item = new PromoItem()
                {
                    Code = "TaiShinBeaconElectricLog",
                    UseTime = DateTime.Now,
                    Memo = string.Format("deviceId={0},uuId={1}, major={2}, minor={3},electricMacAddress={4},electricPower={5},log={6}", BeaconDevice.DeviceId, BeaconTriggerModel.uuId, BeaconTriggerModel.major, BeaconTriggerModel.minor, BeaconDevice.ElectricMacAddress, BeaconDevice.ElectricPower, log),
                    UserId = UserId
                };
                ep.PromoItemSet(item);
            }

            public override void SetSellerBeaconTriggerLog(string errorMsg)
            {
                string memo = string.Empty;

                if (TargetBeaconEvent.EventId == 0 && DeviceRecordId == 0)
                {
                    memo = string.Format("deviceId={0},uuId={1}, major={2}, minor={3},pushActionEventPushMsgId={4},log={5}",
                                    BeaconDevice.DeviceId, BeaconTriggerModel.uuId, BeaconTriggerModel.major, BeaconTriggerModel.minor, -1, errorMsg);
                }
                else
                {
                    memo = string.Format("deviceId={0},uuId={1}, major={2}, minor={3},pushActionEventPushMsgId={4},recordId={5}",
                                    BeaconDevice.DeviceId, BeaconTriggerModel.uuId, BeaconTriggerModel.major, BeaconTriggerModel.minor, TargetBeaconEvent.EventId, DeviceRecordId);
                }

                var item = new PromoItem()
                {
                    Code = "TaiShinBeaconTriggerLog",
                    UseTime = DateTime.Now,
                    Memo = memo,
                    UserId = UserId
                };
                ep.PromoItemSet(item);
            }
        }
    } 


}

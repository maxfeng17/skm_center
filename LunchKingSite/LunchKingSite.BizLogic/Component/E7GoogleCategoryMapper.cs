﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// 取得 17Life 跟 GoogleProductCategory的分類對映
    /// </summary>
    public class E7GoogleCategoryMapper
    {
        const string E7GoogleCategoryMappingFile = "E7GoogleCategoryMappingFile.txt";
        const string E7GoogleCategoryMappingErrorLogFile = "E7GoogleCategoryMappingFile.log";
        Dictionary<int, int> _e7GoogleCategoryMappings;

        public E7GoogleCategoryMapper(string path)
        {
            Init(path);
        }

        private void Init(string path)
        {
            StringBuilder sblog = new StringBuilder();

            string logFilePath = Path.Combine(path, E7GoogleCategoryMappingErrorLogFile);
            string mappingFilePath = Path.Combine(path, E7GoogleCategoryMappingFile);
            if (File.Exists(mappingFilePath) == false)
            {
                sblog.AppendLine(mappingFilePath + " 檔案找不到");
            }
            else
            {
                _e7GoogleCategoryMappings = new Dictionary<int, int>();
                var lines = File.ReadAllLines(Path.Combine(path, E7GoogleCategoryMappingFile));
                foreach (string line in lines)
                {
                    var matches = System.Text.RegularExpressions.Regex.Matches(line, @"\d+");
                    if (matches.Count != 2)
                    {
                        sblog.AppendFormat("{0}{1}",
                            line,
                            Environment.NewLine);
                    }
                    else
                    {
                        int e7Category = int.Parse(matches[0].Value);
                        int googleCategory = int.Parse(matches[1].Value);

                        if (_e7GoogleCategoryMappings.ContainsKey(e7Category) == false)
                        {
                            _e7GoogleCategoryMappings.Add(e7Category, googleCategory);
                        }
                    }
                }
                sblog.AppendLine("成功 " + _e7GoogleCategoryMappings.Count + " 個分類對映");
            }
            if (sblog.Length > 0)
            {
                File.WriteAllText(logFilePath, sblog.ToString());
            }
        }

        public int? ConvertToGoogleCategory(params int[] cids)
        {
            if (_e7GoogleCategoryMappings == null || _e7GoogleCategoryMappings.Count == 0)
            {
                return null;
            }

            List<int> result = new List<int>();
            foreach (int cid in cids)
            {
                if (_e7GoogleCategoryMappings.ContainsKey(cid))
                {
                    result.Add(_e7GoogleCategoryMappings[cid]);
                }
            }
            if (result.Count == 0)
            {
                return null;
            }
            //google分類裏 精確的子分類的數字，會比其上層大
            //也就是儘量取最底層分類的意思
            return result[0];
        }
    }
}

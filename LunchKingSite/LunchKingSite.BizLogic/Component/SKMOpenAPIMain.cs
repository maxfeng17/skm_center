﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.Component;
using Newtonsoft.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Component
{
    public class SKMOpenAPIMain
    {
        private string _appKey = string.Empty;
        private string _appSecret = string.Empty;
        private string _apiUrl = string.Empty;
        private SKMOpenAPIGeneralExchange _generalExchange;

        public SKMOpenAPIMain(Core.ISysConfProvider config = null)
        {
            if (config == null)
            {
                config = ProviderFactory.Instance().GetConfig();
            }

            this._apiUrl = config.SKMCenterApiUrl;
            this._appSecret = config.SKMCenterAppSecret;
            this._appKey = config.SKMCenterAppKey;
            this._generalExchange = new SKMOpenAPIGeneralExchange(_appKey, _appSecret, _apiUrl);
        }


        public string GetItemInfo(long item)
        {
            string pampasCall = "full.item.info";

            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add("itemId", item.ToString());

            string result = _generalExchange.ExchangeInfo(data, pampasCall);

            return result;
        }

        public string GetSkuTemplateInfo(long skuTemplateId)
        {
            string pampasCall = "skuTemplate.info";

            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add("skuTemplateId", skuTemplateId.ToString());

            string result = _generalExchange.ExchangeInfo(data, pampasCall);

            return result;
        }

        public string GetChannelCategoryInfo(long categoryId)
        {
            string pampasCall = "channel.category.info";

            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add("categoryId", categoryId.ToString());

            string result = _generalExchange.ExchangeInfo(data, pampasCall);

            return result;
        }

        public string GetChannelCategoryBindingInfo(long categoryId)
        {
            string pampasCall = "channel.category.binding.info";

            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add("categoryId", categoryId.ToString());

            string result = _generalExchange.ExchangeInfo(data, pampasCall);

            return result;
        }

        public string GetFullSpuInfo(long skuId)
        {
            string pampasCall = "full.spu.info";

            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add("spuId", skuId.ToString());

            string result = _generalExchange.ExchangeInfo(data, pampasCall);

            return result;
        }

        public string GetUserAuth(string account)
        {
            string pampasCall = "user.auth";

            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add("account", account);

            string result = _generalExchange.ExchangeInfo(data, pampasCall);

            return result;

        }
    }
}

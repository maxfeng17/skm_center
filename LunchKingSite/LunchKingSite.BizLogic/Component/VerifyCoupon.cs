﻿using LunchKingSite.BizLogic.Component.Verification;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Verification;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class VerifyCoupon
    {
        private IMemberProvider _mp;
        private IOrderProvider _op;
        private IPponProvider _pp;
        private ISellerProvider _sp;
        private IVerificationProvider _vp;
        private IHiDealProvider _hp;
        private VerifyAccount _va = new VerifyAccount();
        private string _msg_ori = "{\"msg\":\"$msg\", \"mt\":\"$time\"}";

        private string _deal_info = " {\"id\":\"$bid\", \"ina\":\"$item_name\", \"isT\":\"$is_deliver_time\" " +
                                  ", \"dts\":\"$deliver_time_s\", \"dte\":\"$deliver_time_e\" " +
                                  ", \"m\":\"$member\", \"cs\":\"$sequence_no\", \"cc\":\"$coupon_code\"}";

        //建構式
        public VerifyCoupon()
        {
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        }

        public string GetCouponInfo(string e, string s, string seg, string stg, string cs, string cc, string ih)
        {//e: IMEI, s:IMSI, seg: seller guid, stg: store guid, cs: sequence_code(last 4 word), cc: coupon_code
            string rtnInfo = "";
            ViewPponCoupon vpc = (ih.ToLower() == "false") ? _va.GetCouponInfo(cs, cc, seg, stg) : null;
            ViewHiDealVerificationInfo vhvi = (ih.ToLower() == "true") ? _va.GetHiDealCouponInfo(cs, cc, seg, stg) : null;
            //vpc為空，表此時間範圍內無此憑證
            if (vpc == null && vhvi == null)
            {
                rtnInfo = "{\"msg\":\"" + (int)VerificationStatus.CouponNone +
                         "\",\"mt\":\"" + string.Format("{0:yyyy/MM/dd HH:mm:ss}", DateTime.Now) + "\"}";
            }
            else if (vpc != null)
            {
                rtnInfo = GetPponCouponInfo(vpc, e, s, seg, stg, cs, cc);
            }
            else
            {
                rtnInfo = GetHiDealCouponInfo(vhvi, e, s, seg, stg, cs, cc);
            }
            return rtnInfo;
        }

        public string GetPponCouponInfo(ViewPponCoupon vpc, string e, string s, string seg, string stg, string cs, string cc)
        {
            string rtnMsg = "";
            string rtnInfo = "";
            //從 table: cash_turst_log 的 status 判斷此張憑證的現行狀態
            Dictionary<string, string> dicVerStatus = _va.GetCashTrustLogStatus(vpc, e, s);
            VerificationStatus status = (VerificationStatus)(int.Parse(dicVerStatus["flag"]));//GetVerificationStatus(int.Parse(dicVerStatus["flag"]));

            #region 判斷是否在可用期限內

            if (status == VerificationStatus.CouponInit)
            {
                var expirationDates = new CouponUsageExpiration(vpc);
                var couponUsageDateSelector = new CouponUsageDateSelector { ExpirationDates = expirationDates };
                status = couponUsageDateSelector.ChkCouponUsageStatus(status);
            }

            #endregion 判斷是否在可用期限內

            if (!(VerificationStatus.CouponInit == status || VerificationStatus.DuInTime == status))
            {
                dicVerStatus["flag"] = ((int)status).ToString();
                rtnMsg = _msg_ori.Replace("$msg", dicVerStatus["flag"]).Replace("$time", dicVerStatus["time"]);
            }
            else
            {//若狀態值為初始狀態，即代表此張憑證仍是可合法核銷之憑證
                rtnInfo = _deal_info.Replace("$bid", vpc.BusinessHourGuid.ToString())
                            .Replace("$item_name", vpc.ItemName).Replace("$is_deliver_time", "0")
                            .Replace("$deliver_time_s", string.Format("{0:yyyy/MM/dd}", vpc.BusinessHourDeliverTimeS))
                            .Replace("$deliver_time_e", string.Format("{0:yyyy/MM/dd}", vpc.BusinessHourDeliverTimeE))
                            .Replace("$member", vpc.MemberName)
                            .Replace("$sequence_no", vpc.SequenceNumber).Replace("$coupon_code", vpc.CouponCode);
            }
            return (rtnMsg != "") ? rtnMsg : rtnInfo;
        }

        public string GetHiDealCouponInfo(ViewHiDealVerificationInfo vhvi, string e, string s, string seg, string stg, string cs, string cc)
        {
            string rtnMsg = string.Empty;
            string rtnInfo = string.Empty;

            VerificationStatus status = GetVerificationStatus(vhvi.Status.GetValueOrDefault());

            #region 判斷是否在可用期限內

            if (status == VerificationStatus.CouponInit)
            {
                var expirationDates = new CouponUsageExpiration(vhvi);
                var couponUsageDateSelector = new CouponUsageDateSelector { ExpirationDates = expirationDates };
                status = couponUsageDateSelector.ChkCouponUsageStatus((VerificationStatus)vhvi.Status.GetValueOrDefault());
            }

            #endregion 判斷是否在可用期限內

            if (!(VerificationStatus.CouponInit == status || VerificationStatus.DuInTime == status))
            {
                if (status == VerificationStatus.CouponUsed)
                {
                    vhvi.Status = (int)VerificationStatus.CouponUsed;
                }
                //將本次動作記進DB - VerificationLog中
                _va.SetVerificationLog(vhvi.SellerGuid.GetValueOrDefault(new Guid()),
                                       (int?)vhvi.Cid, vhvi.Sequence, vhvi.Code,
                                       vhvi.Status.GetValueOrDefault((int)VerificationStatus.CouponError),
                                       true, DateTime.Now, "IMEI:" + e + "& IMSI: " + s);
                rtnMsg = _msg_ori
                    .Replace("$msg", vhvi.Status.GetValueOrDefault((int)VerificationStatus.CouponError).ToString())
                    .Replace("$time", string.Format("{0:yyyy/MM/dd HH:mm:ss}", vhvi.ModifyTime));
            }
            else
            {//若狀態值為初始狀態，即代表此張憑證仍是可合法核銷之憑證
                rtnInfo = _deal_info.Replace("$bid", vhvi.Cid.ToString())
                            .Replace("$item_name", vhvi.ItemName).Replace("$is_deliver_time", "0")
                            .Replace("$deliver_time_s", string.Format("{0:yyyy/MM/dd}", vhvi.UseStartTime))
                            .Replace("$deliver_time_e", string.Format("{0:yyyy/MM/dd}", vhvi.UseEndTime))
                            .Replace("$member", vhvi.UserName)
                            .Replace("$sequence_no", vhvi.Sequence).Replace("$coupon_code", vhvi.Code);
            }
            return (rtnMsg != "") ? rtnMsg : rtnInfo;
        }

        public List<VerificationListInfo> GetVerificationList(OrderClassification enumType, string strBid, string strDid, string strPid,
            DateTime dtStart, DateTime dtEnd,
            string strSeGuid, string strStGuid, int iStatus)
        {
            var queryData = new VerificationQueryData(enumType);
            var list = new List<VerificationListInfo>();
            var status = (VerificationQueryStatus)iStatus;
            queryData.SetQueryCondition(strBid, strDid, strPid, dtStart, dtEnd, strSeGuid, strStGuid, iStatus);
            if (queryData.IsSetCondition)
            {
                var tblList = new DataTable();
                switch (enumType)
                {
                    case OrderClassification.LkSite:
                        tblList = _vp.GetVerifiedListWithPpon(queryData.IsStore, queryData.Bid,
                                                              queryData.SellerGuid, queryData.StoreGuid,
                                                              queryData.StartQueryTime, queryData.EndQueryTime, status);
                        break;

                    case OrderClassification.HiDeal:
                        tblList = _vp.GetVerifiedListWithPiinlife(queryData.IsStore, queryData.DealId, queryData.ProductId,
                                                              queryData.SellerGuid, queryData.StoreGuid,
                                                              queryData.StartQueryTime, queryData.EndQueryTime, status);
                        break;
                }

                foreach (DataRow row in tblList.Rows)
                {
                    var info = new VerificationListInfo(status);
                    info.SetValue(
                        row["coupon_sequence_number"].ToString(),
                        (row["verified_time"] == DBNull.Value) ? new DateTime() : (DateTime)row["verified_time"],
                        (row["modify_time"] == DBNull.Value) ? new DateTime() : (DateTime)row["modify_time"],
                        (TrustStatus)row["status"]);
                    list.Add(info);
                }
            }
            else
            {
                return new List<VerificationListInfo>();
            }
            return list;
        }

        /// <summary>
        /// 將 table 中的 status(int) 轉為 VerificationStatus
        /// 因為 table 的 status 記錄為 TrustStatus，跟 APP 溝通的所得狀態不一樣
        /// </summary>
        /// <param name="iStatus"> TrustStatus </param>
        /// <returns></returns>
        private VerificationStatus GetVerificationStatus(int iStatus)
        {
            VerificationStatus status = VerificationStatus.CouponInit;
            switch ((TrustStatus)iStatus)
            {
                #region 憑證是可用狀態

                case TrustStatus.Initial:
                case TrustStatus.Trusted:
                case TrustStatus.ATM:
                    status = VerificationStatus.CouponInit;
                    break;

                #endregion 憑證是可用狀態

                #region 已被核銷

                case TrustStatus.Verified:
                    status = VerificationStatus.CouponUsed;
                    break;

                #endregion 已被核銷

                #region 退貨

                case TrustStatus.Refunded:
                case TrustStatus.Returned:
                    status = VerificationStatus.CouponReted;
                    break;

                #endregion 退貨

                default:
                    status = VerificationStatus.CouponError;
                    break;
            }
            return status;
        }

        public VerificationStatus GetChkVerificationStatusWithDueInTime()
        {
            VerificationStatus chkStatus = VerificationStatus.CouponOk;

            return chkStatus;
        }

        public VerificationStatus CheckVerifyCouponStatus(int couponId)
        {
            ViewPponCoupon coupon = _pp.ViewPponCouponGet(couponId);
            CouponUsageExpiration expirationDates = null;
            if (coupon.IsLoaded && coupon.CouponId != null)
            {
                expirationDates = new CouponUsageExpiration(coupon);
            }
            else
            {
                ViewHiDealCoupon hidealCoupon = _hp.ViewHiDealCouponGet(couponId);
                if (hidealCoupon.IsLoaded && hidealCoupon.CouponId != null)
                {
                    expirationDates = new CouponUsageExpiration(hidealCoupon);
                }
            }
            var couponUsageDateSelector = new CouponUsageDateSelector { ExpirationDates = expirationDates };
            return couponUsageDateSelector.ChkCouponUsageStatus();
        }

        public VerificationStatus CheckVerifyCouponStatus(string sequenceNumber, string orderid)
        {
            ViewPponCoupon coupon = _pp.ViewPponCouponGetList(ViewPponCoupon.Columns.OrderId, orderid).FirstOrDefault(x => x.SequenceNumber == sequenceNumber);
            
            CouponUsageExpiration expirationDates = null;
            if (coupon.IsLoaded && coupon.CouponId != null)
            {
                expirationDates = new CouponUsageExpiration(coupon);
            }
           
            var couponUsageDateSelector = new CouponUsageDateSelector { ExpirationDates = expirationDates };
            return couponUsageDateSelector.ChkCouponUsageStatus();
        }
    }
}
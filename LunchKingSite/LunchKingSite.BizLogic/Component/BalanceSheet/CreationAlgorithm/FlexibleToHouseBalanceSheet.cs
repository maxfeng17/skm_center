﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;

namespace LunchKingSite.BizLogic.Component
{
    public class FlexibleToHouseBalanceSheet : SystemToHouseBsCreationTemplate
    {
        #region SystemFinancialBalanceSheet

        protected sealed override DateTime GetCashTrustLogModifyTimeThreshold()
        {
            DateTime genTime = GetGenerationTime();
            DateTime result = new DateTime(
                genTime.Year,
                genTime.Month,
                genTime.Day
                );

            return result;
        }

        protected sealed override bool FinancialStaffPaymentSettingCheck()
        {
            return VendorBillingHelper.FinancialStaffPaymentMethod == RemittanceType.Flexible;
        }

        protected override bool GenerationTimeRangeCheck()
        {
            //彈性請款對帳單 過對帳區間仍可產
            return true;
        }

        protected sealed override BalanceSheet PrepareBalanceSheet()
        {
            return new BalanceSheet
            {
                ProductGuid = this.MerchandiseGuid,
                ProductType = (int)this.MerchandiseType,
                StoreGuid = null,
                GenerationFrequency = (int)this.GetGenerationFrequency(),
                IsDetailFixed = true,
                IsConfirmedReadyToPay = false,
                IsReceiptReceived = false,
                IsManual = false,
                Year = GetCashTrustLogModifyTimeThreshold().Year,
                Month = GetCashTrustLogModifyTimeThreshold().Month,
                IntervalStart = GetDeliverStartTime().Value,
                IntervalEnd = GetCashTrustLogModifyTimeThreshold(),
                CreateId = this.TriggerUser,
                CreateTime = DateTime.Now,
                BalanceSheetType = (int)this.BalanceSheetType,
                ProductId = this.MerchandiseId
            };
        }

        #endregion SystemFinancialBalanceSheet

        public FlexibleToHouseBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId, string triggerUser)
        {
            this.MerchandiseType = merchandiseType;
            this.MerchandiseKey = merchandiseKey;
            this.BalanceSheetType = balanceSheetType;
            this.MerchandiseId = merchandiseId;
            this.TriggerUser = triggerUser;
            TrySetMerchandiseGuid();
        }
    }
}

﻿using System;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using System.Diagnostics;
using System.Text;
using log4net;
using System.Web.Hosting;
using System.Net;
using System.Transactions;
using System.Linq;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

namespace LunchKingSite.BizLogic.Component
{
    public class FortnightlyToHouseBalanceSheetWms
    {
        private static readonly ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static readonly IWmsProvider wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static readonly IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static ILog logger = LogManager.GetLogger("GenerateBalanceSheetWms");

        private BalanceSheetGenerationFrequency GenFreq { get; set; }
		private RemittanceFrequency PayFreq { get; set; }
        private BalanceSheetType BalanceSheetType { get; set; }
        private DeliveryType DeliveryType { get; set; }
        public DateTime generateTime { get; set; }
        private Guid sellerGuid { get; set; }
        private BalanceSheetWmsGenerationResult GenerationResult { get; set; }
        private WmsPurchaseServFeeCollection purchaseServ { get; set; }
        private WmsStockFeeCollection stock { get; set; }
        private WmsReturnServFeeCollection returnServ { get; set; }
        private WmsReturnShipFeeCollection returnShip { get; set; }
        private WmsReturnPackFeeCollection returnPack { get; set; }


        public class BalanceSheetWmsGenerationResult
        {
            public bool IsSuccess { get; set; }
            public int BalanceSheetId { get; set; }
            public Guid SellerGuid { get; set; }
            public BalanceSheetType BalanceSheetType { get; set; }
            public BalanceSheetGenerationResultStatus Status { get; set; }
            public string FailureMessage { get; set; }

            public override string ToString()
            {
                var result = new StringBuilder();


                result.Append(this.SellerGuid.ToString());

                if (this.IsSuccess)
                {
                    result.Append(string.Format("成功-對帳單 id = {0}", this.BalanceSheetId));
                }
                else
                {
                    if (this.Status == BalanceSheetGenerationResultStatus.Exception)
                    {
                        result.Append("失敗-");
                        result.Append(this.FailureMessage.TrimToMaxLength(800, "...blah blah blah..."));
                    }
                    else
                    {
                        result.Append("不用產-");
                        result.Append(this.Status.ToString() + ":");
                        result.Append(this.FailureMessage);
                    }
                }

                result.Append(Environment.NewLine);
                return result.ToString();
            }
        }


        private string MailSubject {
			get
			{
                //if (GenFreq == BalanceSheetGenerationFrequency.FortnightlyBalanceSheet &&
                //    PayFreq == RemittanceFrequency.Fortnightly)
                //{
                //    return string.Format("[系統] PCHOME商品雙周結對帳單");
                //}
                if (GenFreq == BalanceSheetGenerationFrequency.FortnightlyBalanceSheet)
                {
                    return string.Format("[系統] PCHOME商品雙周結對帳單");
                }
                return "[系統] 莫名其妙的執行了產對帳單排程";
			}
		}
        public void Wms(JobSetting js)
        {
            try
            {

                Stopwatch timer = new Stopwatch();
                timer.Start();

                InitProps(js);
                List<Guid> sellers = wp.GetWmsProductItemForBalanceSheetSeller();
                StringBuilder exceptionMailMessage = new StringBuilder();
                logger.InfoFormat(string.Format("{0} 排程開始執行\r\n", MailSubject));
                logger.InfoFormat(string.Format("要產PCHOME產品對帳單的商家:共有 {0} \r\n", sellers.Count()));

                foreach (Guid sellerGuid in sellers)
                {
                    try
                    {
                        logger.InfoFormat(GenerateWms(sellerGuid));
                    }
                    catch (Exception e)
                    {
                        logger.InfoFormat(string.Format("產出seller_guid-{0}PCHOME產品對帳單時出現錯誤，錯誤訊息：\n{1}", sellerGuid, e.Message));
                        exceptionMailMessage.Append(e.Message);
                    }
                }


                timer.Stop();
                logger.InfoFormat(
                    string.Format("總時間 {0:g} ,\r\n\t",
                    timer.Elapsed));

                logger.InfoFormat(string.Format("{0} 排程執行結束\r\n\r\n", MailSubject));

                if (exceptionMailMessage.ToString().Length > 0)
                {
                    AlertResult(exceptionMailMessage.ToString());
                }


            }
            catch (Exception e)
            {
                logger.InfoFormat("Job GenerateBalanceSheetWms Error {0}", e);
            }
        }

        private void InitProps(JobSetting js)
        {
            switch (js.Parameters["generationFrequency"])
            {

                case "fortnightly-toHouse":
                    GenFreq = BalanceSheetGenerationFrequency.FortnightlyBalanceSheet;
                    DeliveryType = DeliveryType.ToHouse;
                    break;
                default:
                    throw new InvalidOperationException("Job configuration not recognized!");
            }
        }

        protected DateTime GetGenerationTime()
        {
            if (generateTime == null)
            {
                generateTime = DateTime.Now;
            }

            return generateTime;
        }

        protected DateTime GetInterval()
        {
            DateTime genTime = GetGenerationTime();
            DateTime result = new DateTime();
            if (genTime.Day == 1)
            {
                //上個月16號~上個月底
                genTime = genTime.GetFirstDayOfMonth();
                result = new DateTime(
                genTime.Year,
                genTime.Month,
                genTime.Day
                );

            }
            else if (genTime.Day == 16)
            {
                //本月1號~本月15號
                genTime = genTime.GetFirstDayOfMonth().AddDays(15);
                result = new DateTime(
                genTime.Year,
                genTime.Month,
                genTime.Day
                );


            }

            return result;

        }

        protected BalanceSheetGenerationFrequency GetGenerationFrequency()
        {
            switch (this.BalanceSheetType)
            {
                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                    return BalanceSheetGenerationFrequency.FortnightlyBalanceSheet;
                default:
                    return BalanceSheetGenerationFrequency.Unknown;
            }
        }

        protected BalanceSheetWm PrepareBalanceSheetWms()
        {
            return new BalanceSheetWm
            {
                SellerGuid = this.sellerGuid,
                Year = this.GetGenerationTime().Year,
                Month = this.GetGenerationTime().Month,
                GenerationFrequency = (int)this.GetGenerationFrequency(),
                IntervalStart = GetInterval().Day == 1 ? GetInterval().AddMonths(-1).AddDays(15) : GetInterval().GetFirstDayOfMonth(),
                IntervalEnd = GetInterval(),
                IsConfirmedReadyToPay = false,
                IsReceiptReceived = false,
                BalanceSheetType = (int)this.BalanceSheetType,
                CreateId = "sys",
                CreateTime = this.GetGenerationTime(),
                

            };
        }

        public void GetCreationAlgorithm(BalanceSheetGenerationFrequency GenFreq, Guid sellerGuid)
        {
            BalanceSheetType sheetType;
            sheetType = GetBalanceSheetType(GenFreq, sellerGuid);

            this.sellerGuid = (Guid)sellerGuid;
            this.BalanceSheetType = sheetType;
        }

        private static BalanceSheetType GetBalanceSheetType(BalanceSheetGenerationFrequency sheetCategory, Guid sellerGuid)
        {
            if (sellerGuid == null)
            {
                return BalanceSheetType.Unknown;
            }

            if (sheetCategory == BalanceSheetGenerationFrequency.FortnightlyBalanceSheet)
            {
                return BalanceSheetType.FortnightlyToHouseBalanceSheet;
            }

            return BalanceSheetType.Unknown;
        }

        private string GenerateWms(Guid sellerGuid)
        {

            BalanceSheetWmsGenerationResult generationResults = Create(GenFreq, sellerGuid);

            StringBuilder dealResultMessage = new StringBuilder();
            dealResultMessage.Append(GetDelimiter());
            dealResultMessage.Append(string.Format("sellerGuid = {0}\r\n", sellerGuid));
            dealResultMessage.Append(GenerationResultsToMessage(generationResults));
            dealResultMessage.Append(GetDelimiter());

            return dealResultMessage.ToString();
        }

        public BalanceSheetWmsGenerationResult Create(BalanceSheetGenerationFrequency GenFreq, Guid sellerGuid)
        {
            GetCreationAlgorithm(GenFreq, sellerGuid);
            this.generateTime = DateTime.Now;
            return Generate(sellerGuid);
        }

        public BalanceSheetWmsGenerationResult SimulateCreate(BalanceSheetGenerationFrequency GenFreq, Guid sellerGuid, DateTime simTime)
        {
            GetCreationAlgorithm(GenFreq, sellerGuid);
            this.generateTime = simTime;
            return Generate(sellerGuid);
        }
        public BalanceSheetWmsGenerationResult Generate(Guid sellerGuid)
        {

            DataSetup(sellerGuid);

            try
            {
                BalanceSheetGenerationResultStatus generationStatus;
                int balanceSheetId = GenerateBalanceSheet(out generationStatus);
                UpdateGenerationResults(generationStatus, balanceSheetId);
            }
            catch (Exception ex)
            {
                GenerationResult = GetGenerationExceptionResult(ex);
                return GenerationResult;
            }

            if (GenerationResult != null)
            {
                //寫入對帳單產出結果紀錄

                var resultDesc = string.Format("對帳單產出結果:{0}-{1} 對帳單類型:{2} 對帳單Id:{3}",
                    GenerationResult.IsSuccess ? "成功" : "失敗", GenerationResult.Status, GenerationResult.BalanceSheetType, GenerationResult.BalanceSheetId);
                CommonFacade.AddAudit(GenerationResult.SellerGuid, AuditType.DealAccounting, resultDesc, "sys", true);
            }

            return GenerationResult;
        }

        // <summary>
        /// 取得相關資料
        /// </summary>
        protected virtual void DataSetup(Guid sellerGuid)
        {
            //撈賣家底下的24產品且有費用
            ProductItemCollection vpic = wp.GetViewProductItemBySellerProdId(sellerGuid);

            purchaseServ = wp.GetWmsPurchaseServFeeByNoBalanceSheet(vpic.Select(x => x.PchomeProdId).ToList(), GetInterval());
            stock = wp.GetWmsStockFeeByNoBalanceSheet(vpic.Select(x => x.PchomeProdId).ToList(), GetInterval());
            returnServ = wp.GetWmsReturnServFeeByNoBalanceSheet(vpic.Select(x => x.PchomeProdId).ToList(), GetInterval());
            returnShip = wp.GetWmsReturnShipFeeByNoBalanceSheet(vpic.Select(x => x.PchomeProdId).ToList(), GetInterval());
            returnPack = wp.GetWmsReturnPackFeeByNoBalanceSheet(vpic.Select(x => x.PchomeProdId).ToList(), GetInterval());



            //還有其他
        }


        private int GenerateBalanceSheet(out BalanceSheetGenerationResultStatus status)
        {
            BalanceSheetWm newBSW = PrepareBalanceSheetWms();
            if (newBSW == null)
            {
                status = BalanceSheetGenerationResultStatus.BalanceSheetDataError;
                return 0;
            }
            if (!DuplicationCheck(newBSW))
            {
                status = BalanceSheetGenerationResultStatus.Duplication;
                return 0;
            }

            int id = PersistNewBalanceSheet(purchaseServ, stock, returnServ, returnShip, returnPack, newBSW);
            status = BalanceSheetGenerationResultStatus.Success;
            return id;
        }

        /// <summary>
        /// 確認有無產過對帳單
        /// </summary>
        /// <param name="unpersistedBS"></param>
        /// <returns></returns>
        protected virtual bool DuplicationCheck(BalanceSheetWm unpersistedBSW)
        {
            int count = ap.BalanceSheetWmsDuplicationCountCheck(unpersistedBSW.SellerGuid, unpersistedBSW.IntervalStart, unpersistedBSW.IntervalEnd, this.BalanceSheetType);
            return count == 0;
        }

        private int PersistNewBalanceSheet(WmsPurchaseServFeeCollection purchaseServ, WmsStockFeeCollection stockFee,
                                           WmsReturnServFeeCollection returnServFee, WmsReturnShipFeeCollection returnShipFee,
                                           WmsReturnPackFeeCollection returnPackFee, BalanceSheetWm newUnpersistedBSW)
        {
            int balanceSheetId;

            using (TransactionScope txScope = new TransactionScope())
            {
                #region 對帳單應付金額

                int purchaseServTotal = purchaseServ.Sum(x => x.Fee) ?? 0;
                int stockFeeTotal = stockFee.Sum(x => x.Fee) ?? 0;
                int returnServFeeTotal = returnServFee.Sum(x => x.Fee) ?? 0;
                int returnShipFeeTotal = returnShipFee.Sum(x => x.Fee) ?? 0;
                int returnPackFeeTotal = returnPackFee.Sum(x => x.Fee) ?? 0;
                newUnpersistedBSW.EstAmount = -(purchaseServTotal + stockFeeTotal + returnServFeeTotal + returnShipFeeTotal + returnPackFeeTotal);

                balanceSheetId = ap.BalanceSheetWmsSet(newUnpersistedBSW);
                #endregion

                #region 0 元對帳單 & 負對帳單的確認

                /*
                * 0元 對帳單不需要管理單據, 所以要直接確認掉, 跳過營管部門這道關卡, 並進入財會部門的程序.
                * 0元不需要付錢 => pay_report_id = 0
                * 負的需要收錢 => pay_report_id = null, 等收到錢後會填入對應的 id.
                */

                if (newUnpersistedBSW.EstAmount == 0)
                {
                    newUnpersistedBSW.PayReportId = 0;
                    newUnpersistedBSW.IsConfirmedReadyToPay = true;
                    newUnpersistedBSW.ConfirmedUserName = "sys";
                    newUnpersistedBSW.ConfirmedTime = GetGenerationTime();
                    newUnpersistedBSW.IsReceiptReceived = true;
                }

                #endregion

                ap.BalanceSheetWmsSet(newUnpersistedBSW);

                #region 更新balancesheetID

                purchaseServ.ForEach(x =>
                {
                    x.BalanceSheetWmsId = balanceSheetId;
                });
                wp.WmsPurchaseServFeeSet(purchaseServ);

                stockFee.ForEach(x =>
                {
                    x.BalanceSheetWmsId = balanceSheetId;
                });
                wp.WmsStockFeeSet(stockFee);

                returnServFee.ForEach(x =>
                {
                    x.BalanceSheetWmsId = balanceSheetId;
                });
                wp.WmsReturnServFeeSet(returnServFee);

                returnShipFee.ForEach(x =>
                {
                    x.BalanceSheetWmsId = balanceSheetId;
                });
                wp.WmsReturnShipFeeSet(returnShipFee);

                returnPackFee.ForEach(x =>
                {
                    x.BalanceSheetWmsId = balanceSheetId;
                });
                wp.WmsReturnPackFeeSet(returnPackFee);

                #endregion

                txScope.Complete();
            }

            return balanceSheetId;
        }

        private string GetDelimiter()
        {
            return "----------------------------------------------------------------------------------------------------------\r\n";
        }

        private string GenerationResultsToMessage(BalanceSheetWmsGenerationResult result)
        {
            StringBuilder storeMessage = new StringBuilder();

            if (result.IsSuccess)
            {
                storeMessage.Append(string.Format("成功-對帳單 id = {0}", result.BalanceSheetId));
            }
            else
            {
                if (result.Status == BalanceSheetGenerationResultStatus.Exception)
                {
                    storeMessage.Append("失敗-");
                    storeMessage.Append(result.FailureMessage.TrimToMaxLength(800, "...blah blah blah..."));
                }
                else
                {
                    storeMessage.Append("不用產-");
                    storeMessage.Append(result.Status.ToString() + ":");
                    storeMessage.Append(result.FailureMessage);
                }
            }

            storeMessage.Append("\r\n");

            return storeMessage.ToString();
        }



        private void UpdateGenerationResults(BalanceSheetGenerationResultStatus generationStatus, int balanceSheetId)
        {
            if (generationStatus == BalanceSheetGenerationResultStatus.Success)
            {
                GenerationResult = GetGenerationSuccessResult(balanceSheetId);
            }
            else
            {
                GenerationResult = GetGenerationFailResult(generationStatus);
            }
        }

        private BalanceSheetWmsGenerationResult GetGenerationSuccessResult(int balanceSheetId)
        {
            return new BalanceSheetWmsGenerationResult
            {
                IsSuccess = true,
                BalanceSheetId = balanceSheetId,
                SellerGuid = this.sellerGuid,
                BalanceSheetType = this.BalanceSheetType,
                Status = BalanceSheetGenerationResultStatus.Success,
                FailureMessage = string.Empty
            };
        }

        private BalanceSheetWmsGenerationResult GetGenerationFailResult(BalanceSheetGenerationResultStatus generationStatus)
        {
            return new BalanceSheetWmsGenerationResult
            {
                IsSuccess = false,
                BalanceSheetId = 0,
                SellerGuid = this.sellerGuid,
                BalanceSheetType = this.BalanceSheetType,
                Status = generationStatus,
                FailureMessage = string.Empty
            };
        }

        private BalanceSheetWmsGenerationResult GetGenerationExceptionResult(Exception exception)
        {
            return new BalanceSheetWmsGenerationResult
            {
                IsSuccess = false,
                BalanceSheetId = 0,
                SellerGuid = this.sellerGuid,
                BalanceSheetType = this.BalanceSheetType,
                Status = BalanceSheetGenerationResultStatus.Exception,
                FailureMessage = string.Format("message: {0} \r\n trace: {1}", exception.Message, exception.StackTrace)
            };
        }

        private void AlertResult(string messageBody)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(config.AdminEmail);
            msg.To.Add(config.AdminEmail);
            msg.Subject = Environment.MachineName + ": " + MailSubject + "-Exception!";
            msg.Body = messageBody;

            PostMan.Instance().Send(msg, SendPriorityType.Normal);
        }
    }
}

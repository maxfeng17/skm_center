﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public abstract class MonthlyPayBalanceSheet : SystemBsCreationTemplate
    {
        #region SystemFinancialBalanceSheet

        protected sealed override DateTime GetCashTrustLogModifyTimeThreshold()
        {
			DateTime genTime = GetGenerationTime().GetFirstDayOfMonth();
			DateTime result = new DateTime(
				genTime.Year,
				genTime.Month,
				genTime.Day
				);  //GetFirstDayOfMonth() only changes the date information of GetGenerationTime(), not the time

			return result;
        }
        
        protected sealed override bool StabilizePreviousBalanceSheetContent(Guid? storeGuid)
        {
            BalanceSheet unfixedBS = ap.BalanceSheetGetDetailNotFixed(this.MerchandiseGuid, this.MerchandiseType, storeGuid);
            if (unfixedBS == null)
            {
                return true;
            }
            unfixedBS.IsDetailFixed = true;
            ap.BalanceSheetSet(unfixedBS);
            return true;
        }

        #endregion
        
        protected MonthlyPayBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId)
        {
            this.MerchandiseType = merchandiseType;
            this.MerchandiseKey = merchandiseKey;
            this.BalanceSheetType = balanceSheetType;
            this.MerchandiseId = merchandiseId;
            TrySetMerchandiseGuid();
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class AccountingMonthlyPayBalanceSheet : MonthlyPayBalanceSheet
    {
        protected sealed override bool FinancialStaffPaymentSettingCheck()
        {
            return VendorBillingHelper.FinancialStaffPaymentMethod == RemittanceType.ManualMonthly;
        }

        protected sealed override BalanceSheet PrepareBalanceSheet(Guid? storeGuid)
        {
            return new BalanceSheet
            {
                ProductGuid = this.MerchandiseGuid,
                ProductType = (int)this.MerchandiseType,
                StoreGuid = storeGuid,
                GenerationFrequency = (int)BalanceSheetGenerationFrequency.MonthBalanceSheet,
                IsDetailFixed = true,
                IsConfirmedReadyToPay = false,
                IsReceiptReceived = false,
                IsManual = true,
                Year = GetGenerationTime().AddMonths(-1).Year,
                Month = GetGenerationTime().AddMonths(-1).Month,
                IntervalStart = GetGenerationTime().AddMonths(-1).GetFirstDayOfMonth(),
                IntervalEnd = GetGenerationTime().GetFirstDayOfMonth(),
                CreateId = "sys",
                CreateTime = this.GetGenerationTime(),
                BalanceSheetType = (int)this.BalanceSheetType,
                ProductId = this.MerchandiseId
            };
        }

        public AccountingMonthlyPayBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId)
            : base(merchandiseType, merchandiseKey, balanceSheetType, merchandiseId)
        {
        }
    }
}

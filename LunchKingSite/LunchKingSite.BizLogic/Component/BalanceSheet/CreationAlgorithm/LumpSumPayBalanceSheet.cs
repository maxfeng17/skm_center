﻿using System;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class LumpSumPayBalanceSheet : SystemProductBsCreationTemplate
    {
        #region SystemFinancialBalanceSheet

        protected sealed override DateTime GetCashTrustLogModifyTimeThreshold()
        {
			DateTime genTime = GetGenerationTime();
			DateTime result = new DateTime(
				genTime.Year,
				genTime.Month,
				genTime.Day
				);  

			return result;
        }

        protected sealed override bool BalanceSheetTypeSpecificCheck()
        {
            //檔次是否成檔檢查
            if (!IsDealEstablished())
            {
                return false;
            }
            //退換貨處理完成日檢查
            if (!GetFinalBalanceSheetDate().HasValue)
            {
                return false;
            }

            var balanceSheetCreateTime = GetBalanceSheetCreateTime();
            //有填寫客服備註之檔次 需檢查是否已人工回填可開立對帳單日期 方可產對帳單
            if (HasVendorPaymentChange())
            {
                return balanceSheetCreateTime.HasValue;
            }

            return !balanceSheetCreateTime.HasValue;
        }

        protected sealed override DateTime GetBalanceSheetIntervalStart()
        {
            return GetDeliverStartTime().Value;
        }

        protected sealed override DateTime GetBalanceSheetIntervalEnd()
        {
            return GetDeliverEndTime().Value;
        }

        protected sealed override bool FinancialStaffPaymentSettingCheck()
        {
            return VendorBillingHelper.FinancialStaffPaymentMethod == RemittanceType.Others ||
                   VendorBillingHelper.FinancialStaffPaymentMethod == RemittanceType.ManualPartially;
        }

        protected sealed override BalanceSheet PrepareBalanceSheet()
        {
            return new BalanceSheet
            {
                ProductGuid = this.MerchandiseGuid,
                ProductType = (int)this.MerchandiseType,
                StoreGuid = null,
                GenerationFrequency = (int)this.GetGenerationFrequency(),
                IsDetailFixed = false,
                IsConfirmedReadyToPay = false,
                IsReceiptReceived = false,
                IsManual = true,
                Year = this.GetBalanceSheetIntervalEnd().Year,
                Month = this.GetBalanceSheetIntervalEnd().Month,
                IntervalStart = this.GetBalanceSheetIntervalStart(),
                IntervalEnd = this.GetBalanceSheetIntervalEnd(),
                CreateId = "sys",
                CreateTime = this.GetGenerationTime(),
                BalanceSheetType = (int)this.BalanceSheetType,
                ProductId = this.MerchandiseId
            };
        }

        #endregion SystemFinancialBalanceSheet

        public LumpSumPayBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId)
        {
            this.MerchandiseType = merchandiseType;
            this.MerchandiseKey = merchandiseKey;
            this.BalanceSheetType = balanceSheetType;
            this.MerchandiseId = merchandiseId;
            TrySetMerchandiseGuid();
        }
    }
}

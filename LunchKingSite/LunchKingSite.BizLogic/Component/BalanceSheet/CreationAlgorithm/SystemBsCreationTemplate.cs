﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// 系統對帳單. "系統" 指的是可以自動並有規則的產出對帳單的對帳單類型.
    /// </summary>
    public abstract class SystemBsCreationTemplate : BsCreationBase
    {
        #region props

        public object MerchandiseKey { get; set; }
        public string TriggerUser { get; set; }
        public bool GenOutOfVerificationTimeRange { get; set; }

        #endregion

        #region protected props
        
        protected bool IsHelperSet { get; set; }
        protected bool IsDetailsNeeded { get; set; }
        protected bool IsMerchandiseGuidSet { get; set; }
        protected CashTrustLogCollection AllCashTrustLogs { get; set; }
        protected Dictionary<Guid, BalanceSheetDetailCollection> AllOldBalanceSheetDetails { get; set; }

        protected virtual bool NeedDeductionCheck
        {
            get
            {
                if (this.BalanceSheetType == BalanceSheetType.MonthlyPayBalanceSheet
                || this.BalanceSheetType == BalanceSheetType.FlexiblePayBalanceSheet
                || this.BalanceSheetType == BalanceSheetType.WeeklyPayBalanceSheet)
                { 
                    //新出帳方式過兌換區間檔次 即可開始產出負額對帳單
                    return !IsOverCouponUsageTime();
                }

                return true;
            }
        }
        
        

        #endregion

        /// <summary>
        /// 設定產對帳單的模擬時間.
        /// </summary>
        /// <param name="simulationTime"></param>
        public override void SetGenerationTime(DateTime simulationTime)
        {
            this.generationTime = simulationTime;
        }
        
        /// <summary>
        /// 產對帳單 ***template method for generating balance sheet***
        /// </summary>
        /// <returns></returns>
        public override ReadOnlyCollection<BalanceSheetGenerationResult> Generate()
        {
            GenerationSetup();
            
            if (!BalanceSheetTypeCheck() || !IsHelperSet || !PreExecutionCheck())
            {
                string failureMessage = string.Empty;
                
                if(!BalanceSheetTypeCheck())
                {
                    failureMessage = "balance sheet type check failed.";
                }
                if (!IsHelperSet)
                {
                    failureMessage = "VendorBillingHelper set failed.";
                }
                if (!PreExecutionCheck())
                {
                    failureMessage = "preexecution check failed.";
                }

                GenerationResults.Add(GetPreconditionFailResult(failureMessage));
                return GenerationResults.AsReadOnly();
            }
			
            DataSetup();
            foreach (Guid? storeGuid in GetBalanceSheetsStoreGuid())
            {
                try
                {
                    //補產非於核銷區間週結月對帳單(目前只有補產檔款週結月對帳單) 不需檢查核銷區間
                    if (!GenOutOfVerificationTimeRange)
                    {
                        if (!VerificationTimeRangeCheck(storeGuid))
                        {
                            GenerationResults.Add(GetGenerationFailResult(storeGuid, BalanceSheetGenerationResultStatus.OutOfVerificationTimeRange));
                            continue;
                        }
                    }

                    if (!StabilizePreviousBalanceSheetContent(storeGuid))
                    {
                        GenerationResults.Add(GetGenerationFailResult(storeGuid, BalanceSheetGenerationResultStatus.StablizingCheckFailed));
                        continue;
                    }

                    BalanceSheetGenerationResultStatus generationStatus;
                    int balanceSheetId = GenerateBalanceSheet(storeGuid, out generationStatus);
                    UpdateGenerationResults(storeGuid, generationStatus, balanceSheetId);
                }
                catch (Exception ex)
                {
                    GenerationResults.Add(GetGenerationExceptionResult(storeGuid, ex));
                    continue;
                }
            }

            if (GenerationResults.Any())
            {
                //寫入對帳單產出結果紀錄
                foreach (var result in GenerationResults)
                {
                    var resultDesc = string.Format("Store_Guid:{0} 對帳單產出結果:{1}-{2} 對帳單類型:{3} 對帳單Id:{4}", 
                        result.StoreGuid, result.IsSuccess ? "成功" : "失敗", result.Status, result.BalanceSheetType, result.BalanceSheetId);
                    CommonFacade.AddAudit(result.MerchandiseGuid, AuditType.DealAccounting, resultDesc, string.IsNullOrEmpty(TriggerUser) ? "sys" : TriggerUser, true);
                }
            }

            return GenerationResults.AsReadOnly();
        }

        /// <summary>
        /// 對帳單是否符合核銷區間的檢查
        /// </summary>
        protected virtual bool VerificationTimeRangeCheck(Guid? storeGuid)
        {
            VerificationPolicy verificationPolicy = new VerificationPolicy();
            if (!verificationPolicy.IsInVerifyTimeRange(this.MerchandiseType, this.MerchandiseGuid, storeGuid, this.GetGenerationTime()))
            {
                //產生時間已經超出核銷區間, 要確認是否還有最後一份對帳單要產
                if(!IsLastBalanceSheet(storeGuid, verificationPolicy))
                {
                    //如果不是最後一張對帳單, 就真的不屬於產對帳單的期限內, 這個 check 就需要fail掉
                    return false; 
                }
            }
            return true;
        }

        protected virtual bool IsLastBalanceSheet(Guid? storeGuid, VerificationPolicy verificationPolicy)
        {
            BalanceSheet latestBS = GetLatestBalanceSheet(storeGuid);
            if (latestBS == null
                || (!verificationPolicy.IsInVerifyTimeRange(this.MerchandiseType, this.MerchandiseGuid, storeGuid, latestBS.IntervalEnd)))
            {
                return false;
            }
            return true;
        }
		
        
        #region protected members

        #region abstract members

        /// <summary>
        /// cash_trust_log 的 modify time 臨界值 (對於該對帳單有效的 cash_trust_log)
        /// </summary>
        /// <returns></returns>
        protected abstract DateTime GetCashTrustLogModifyTimeThreshold();

        /// <summary>
        /// 財會的匯款方式檢查
        /// </summary>
        protected abstract bool FinancialStaffPaymentSettingCheck();

        /// <summary>
        /// 把要產的對帳單資料準備好.
        /// </summary>
        /// <param name="storeGuid"></param>
        /// <returns></returns>
        protected abstract BalanceSheet PrepareBalanceSheet(Guid? storeGuid);

        /// <summary>
        /// 把以前的對帳單明細固定住. 沒有固定時, "反核銷" 的動作會調整對帳單明細.
        /// </summary>
        /// <param name="storeGuid"></param>
        /// <returns></returns>
        protected abstract bool StabilizePreviousBalanceSheetContent(Guid? storeGuid);

		#endregion


        #region overridable

        protected virtual void GenerationSetup()
        {
            IsMerchandiseGuidSet = TrySetMerchandiseGuid();
            IsHelperSet = TrySetBillingHelper();
            IsDetailsNeeded = true;
            GenerationResults = new List<BalanceSheetGenerationResult>();
        }

        protected virtual bool PreExecutionCheck()
        {
			if (BillingSystemCheck() 
				&& FinancialStaffPaymentSettingCheck()
				&& PponSpecificCheck())
            {
                return true;
            }

            return false;
        }
        
        protected virtual void DataSetup()
        {
            AllCashTrustLogs = VendorBillingHelper.GetAllSuccessfulOrderTrustList();
            AllOldBalanceSheetDetails = GetOldBalanceSheetDetails(GetBalanceSheetsStoreGuid());
        }

        protected List<Guid?> balanceSheetStoreGuids;
        /// <summary>
        /// 檔次若有設定分店, 就要為每家分店產對帳單. 沒分店就產一份 "store_guid = null" 的對帳單
        /// </summary>
        /// <returns></returns>
        protected virtual List<Guid?> GetBalanceSheetsStoreGuid()
        {
            if (balanceSheetStoreGuids != null)
            {
                return balanceSheetStoreGuids;
            }

            balanceSheetStoreGuids = new List<Guid?>();

            if (int.Equals(0, VendorBillingHelper.ParticipatingStores.Count()))
            {
                balanceSheetStoreGuids.Add(null);
            }
            else
            {
                foreach (Guid storeGuid in VendorBillingHelper.ParticipatingStores)
                {
                    balanceSheetStoreGuids.Add(storeGuid);
                }
            }

            return balanceSheetStoreGuids;
        }

        protected DateTime? generationTime;
        protected DateTime GetGenerationTime()
        {
            if (generationTime == null)
            {
                generationTime = DateTime.Now;
            }

            return generationTime.Value;
        }

		protected virtual BalanceSheet GetLatestBalanceSheet(Guid? storeGuid)
		{
			return ap.BalanceSheetGetLatest(this.MerchandiseType, this.MerchandiseGuid, storeGuid, GetGenerationFrequency());
		}

        protected virtual VerificationStatisticsLog CalculateStatistics(CashTrustLogCollection allTrustData, Guid? storeGuid)
        {
            IEnumerable<CashTrustLog> frieghtlessData = allTrustData.Where(log => !Helper.IsFlagSet((TrustSpecialStatus)log.SpecialStatus, TrustSpecialStatus.Freight)
                                                                               && log.VerifiedStoreGuid == storeGuid).ToList();
            IEnumerable<CashTrustLog> beforeThresholdTimeData = frieghtlessData.Where(log => log.UsageVerifiedTime < this.GetCashTrustLogModifyTimeThreshold()).ToList();
            IEnumerable<CashTrustLog> afterThresholdTimeData = frieghtlessData.Where(log => log.UsageVerifiedTime >= this.GetCashTrustLogModifyTimeThreshold()).ToList();

            VerificationStatisticsLog stats = new VerificationStatisticsLog();
            /* 
             * This default implementation does not calculate the exact statistics at the time of GetCashTrustLogModifyTimeThreshold() !!
             * If exact statistics is needed, use cash_trust_status_log.
             * 
             * When [cash_trust_log].[modify_time] is after generation time (), 
             * restore to previous state for statistic calculation. 
             * **other extreme cases are not considered.              * 
             * previous state <==> current state
             *      unverified                 verified                       
             *      verified                      force returned                                           
             *      returned                    force verified
             *      verified                      unverified
             */
            stats.Unverified = beforeThresholdTimeData.Where(x => x.Status == (int)TrustStatus.Initial || x.Status == (int)TrustStatus.Trusted).Count()
                + afterThresholdTimeData.Where(x => x.Status == (int)TrustStatus.Initial || x.Status == (int)TrustStatus.Trusted || x.Status == (int)TrustStatus.Verified).Count();
            stats.Verified = beforeThresholdTimeData.Where(x => x.Status == (int)TrustStatus.Verified).Count()
                + afterThresholdTimeData.Where(x => Helper.IsFlagSet((TrustSpecialStatus)x.SpecialStatus, TrustSpecialStatus.ReturnForced)).Count();
            stats.Returned = beforeThresholdTimeData.Where(x => x.Status == (int)TrustStatus.Returned || x.Status == (int)TrustStatus.Refunded).Count()
                + afterThresholdTimeData.Where(x => Helper.IsFlagSet((TrustSpecialStatus)x.SpecialStatus, TrustSpecialStatus.VerificationForced)).Count();
            stats.ForcedVerified = beforeThresholdTimeData.Where(x => Helper.IsFlagSet((TrustSpecialStatus)x.SpecialStatus, TrustSpecialStatus.VerificationForced)).Count();
            stats.ForcedReturned = beforeThresholdTimeData.Where(x => Helper.IsFlagSet((TrustSpecialStatus)x.SpecialStatus, TrustSpecialStatus.ReturnForced)).Count();
            stats.Lost = beforeThresholdTimeData.Where(x => Helper.IsFlagSet((TrustSpecialStatus)x.SpecialStatus, TrustSpecialStatus.VerificationLost)).Count();
            stats.Adjustment = 0;
            stats.Status = (int)VerificationStatisticsLogStatus.BalanceSheet;

            return stats;
        }

        protected virtual bool DuplicationCheck(BalanceSheet unpersistedBS)
        {
            int count = ap.BalanceSheetDuplicationCountCheck(unpersistedBS.ProductGuid,
                                                this.MerchandiseType,
                                                unpersistedBS.StoreGuid, unpersistedBS.IntervalStart, unpersistedBS.IntervalEnd,
                                                this.BalanceSheetType);
            return count == 0;
        }

        protected virtual BalanceSheet FinalizeBalanceSheet(BalanceSheet newBS, IDictionary<Guid, int> payList, IEnumerable<BalanceSheetDetail> deductList)
        {
            return newBS;
        }

        protected virtual bool ItemsCheck(BalanceSheet newBS, IDictionary<Guid, int> payList, IEnumerable<BalanceSheetDetail> deductList)
        {
            return true;
        }

        #endregion


        #region non-overridable

        protected IHiDealProvider hp;
        protected IPponProvider pp;
        protected IAccountingProvider ap;
    	protected IMemberProvider mp;
        protected ISysConfProvider config;
        protected void InitializeProviders()
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        	mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        protected bool TrySetMerchandiseGuid()
        {
            if (MerchandiseKey == null)
            {
                return false;
            }

            int key;
            if (int.TryParse(MerchandiseKey.ToString(), out key) && MerchandiseType == BusinessModel.PiinLife)
            {
                HiDealProduct product = hp.HiDealProductGet(key);
                if (product != null && product.IsLoaded)
                {
                    hiDealProductId = product.Id;
                    this.MerchandiseGuid = product.Guid;
                    return true;
                }
            }

            Guid guidKey;
            if (Guid.TryParse(MerchandiseKey.ToString(), out guidKey))
            {
                this.MerchandiseGuid = guidKey;
                return true;
            }

            return false;
        }

        /// <summary>
        /// 檢查是否用對帳單系統的對帳方式
        /// </summary>
        /// <returns></returns>
        protected bool BillingSystemCheck()
        {
            return VendorBillingHelper.VendorBillingModel == VendorBillingModel.BalanceSheetSystem;
        }

		protected bool PponSpecificCheck()
		{
			if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return true;
            }
			
			//多檔次主檔不可產對帳單
			var result = pp.GetViewComboDealByBid(this.MerchandiseGuid);
			return result.Count() == 0;
		}

    	protected Dictionary<Guid, BalanceSheetDetailCollection> GetOldBalanceSheetDetails(List<Guid?> balanceSheetStoreGuids)
        {
            Dictionary<Guid, BalanceSheetDetailCollection> allOldDetails = new Dictionary<Guid, BalanceSheetDetailCollection>();
            foreach (Guid? storeGuid in balanceSheetStoreGuids)
            {
                BalanceSheetDetailCollection details = ap.BalanceSheetDetailGetListByProductGuidAndStoreGuid(this.MerchandiseGuid, storeGuid);
                allOldDetails.Add(storeGuid ?? Guid.Empty, details);
            }
            return allOldDetails;
        }

        #endregion


        #endregion

        #region private members

        private bool BalanceSheetTypeCheck()
        {
            return this.BalanceSheetType == BalanceSheetType.ManualMonthlyPayBalanceSheet
                   || this.BalanceSheetType == BalanceSheetType.ManualWeeklyPayMonthBalanceSheet
                   || this.BalanceSheetType == BalanceSheetType.ManualWeeklyPayWeekBalanceSheet
                   || this.BalanceSheetType == BalanceSheetType.MonthlyPayBalanceSheet
                   || this.BalanceSheetType == BalanceSheetType.WeeklyPayMonthBalanceSheet
                   || this.BalanceSheetType == BalanceSheetType.WeeklyPayWeekBalanceSheet
                   || this.BalanceSheetType == BalanceSheetType.ManualTriggered
                   || this.BalanceSheetType == BalanceSheetType.FlexiblePayBalanceSheet
                   || this.BalanceSheetType == BalanceSheetType.WeeklyPayBalanceSheet;
        }
        
        private List<BalanceSheetGenerationResult> GenerationResults { get; set; }

        private int hiDealProductId = 0;
        private bool TrySetBillingHelper()
        {
            if (this.MerchandiseType == BusinessModel.Ppon)
            {
                this.VendorBillingHelper = new PponVendorBillingHelper(this.MerchandiseGuid);
                return true;
            }

            if (this.MerchandiseType == BusinessModel.PiinLife)
            {
                this.VendorBillingHelper = new HiDealVendorBillingHelper(hiDealProductId);
                return true;
            }

            return false;
        }

		private BalanceSheetGenerationFrequency GetGenerationFrequency()
		{
			switch (this.BalanceSheetType)
			{
				case BalanceSheetType.ManualMonthlyPayBalanceSheet:
				case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
				case BalanceSheetType.MonthlyPayBalanceSheet:
				case BalanceSheetType.WeeklyPayMonthBalanceSheet:
					return BalanceSheetGenerationFrequency.MonthBalanceSheet;
				case BalanceSheetType.ManualWeeklyPayWeekBalanceSheet:
				case BalanceSheetType.WeeklyPayWeekBalanceSheet:
                case BalanceSheetType.WeeklyPayBalanceSheet:
					return BalanceSheetGenerationFrequency.WeekBalanceSheet;
				case BalanceSheetType.ManualTriggered:
					return BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet;
                case BalanceSheetType.FlexiblePayBalanceSheet:
                    return BalanceSheetGenerationFrequency.FlexibleBalanceSheet;
				default:
					return BalanceSheetGenerationFrequency.Unknown;
			}
		}

        private int GenerateBalanceSheet(Guid? storeGuid, out BalanceSheetGenerationResultStatus status)
        {
            BalanceSheet newBS = PrepareBalanceSheet(storeGuid);
            if (newBS == null)
            {
                status = BalanceSheetGenerationResultStatus.BalanceSheetDataError;
                return 0;
            }
            if (!DuplicationCheck(newBS))
            {
                status = BalanceSheetGenerationResultStatus.Duplication;
                return 0;
            }

        	IDictionary<Guid, int> payList;	//Guid: [trust_id], int: [cash_trust_status_log].[id]
			IEnumerable<BalanceSheetDetail> deductList;	
            if (IsDetailsNeeded)
            {
				payList = GetPayListForNewBalanceSheet(AllCashTrustLogs, AllOldBalanceSheetDetails[storeGuid ?? Guid.Empty], storeGuid);
				deductList = GetDeductListForNewBalanceSheet(AllOldBalanceSheetDetails[storeGuid ?? Guid.Empty]).ToList();
                int slottingFeeQuantity = GetSlottingFeeQuantity();
                var slottingFeeCount = GetPreviousBsSlottingFeeCount();
                if (NeedDeductionCheck)
                {
                    var payCount = payList.Count() - (slottingFeeQuantity > slottingFeeCount ? slottingFeeQuantity - slottingFeeCount : 0);
                    if (payCount < deductList.Count())
                    {
                        //沒東西可扣錢就留到以後再扣
                        deductList = GetDeductListForNewBalanceSheet(AllOldBalanceSheetDetails[storeGuid ?? Guid.Empty])
                            .OrderBy(detail => detail.BalanceSheetId).ThenBy(detail => detail.UndoTime)
                            .Take(payCount);
                    }
                }
            }
            else
            {
            	payList = new Dictionary<Guid, int>();
				deductList = new List<BalanceSheetDetail>();
            }

            if (!ItemsCheck(newBS, payList, deductList))
            {
                status = BalanceSheetGenerationResultStatus.ItemsCheckFailed;
                return 0;
            }

            BalanceSheet finalizedNewBS = FinalizeBalanceSheet(newBS, payList, deductList);
			
			VerificationStatisticsLog currentPerStoreStats = CalculateStatistics(AllCashTrustLogs, storeGuid);

            if (currentPerStoreStats == null)
            {
                status = BalanceSheetGenerationResultStatus.StopPayment;
                return 0;
            }

            int id = PersistNewBalanceSheet(payList, deductList, currentPerStoreStats, finalizedNewBS);
            status = BalanceSheetGenerationResultStatus.Success;
            return id;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <returns>dictionary key: trust_id; dictionary value: [cash_trust_status_log].[id]</returns>
		private IDictionary<Guid, int> GetPayListForNewBalanceSheet(CashTrustLogCollection allCashTrustLogs, BalanceSheetDetailCollection allDetails, Guid? storeGuid)
		{
			IEnumerable<Guid> allVerifiedTrustList = GetAllVerifiedTrustList(allCashTrustLogs, storeGuid);
			IEnumerable<Guid> allBalanceSheetTrustList = GetAllBalanceSheetDetailListExceptUndos(allDetails);
			IEnumerable<Guid> payList = allVerifiedTrustList.Where(trustId => !allBalanceSheetTrustList.Contains(trustId)).ToList();
			CashTrustStatusLogCollection statusLogs = mp.CashTrustStatusLogGetList(payList);
			
			Dictionary<Guid, int> results = new Dictionary<Guid, int>();
			foreach(Guid trustId in payList)
			{
				Guid trustGuid = trustId;
				CashTrustStatusLog latestLog = 
							statusLogs
							.Where(log => 
								log.TrustId == trustGuid 
								&& log.Status == (int)TrustStatus.Verified
								&& log.ModifyTime.AddSeconds(-2) < GetCashTrustLogModifyTimeThreshold())  //改 cash_trust_log 時, 歷程資料可能沒有填 cash_trust_log 的時間 => 兩個都填DateTime.Now 造成的時間差問題
							.OrderByDescending(log => log.ModifyTime).FirstOrDefault();

				if(latestLog != null)
				{
					results.Add(trustId, latestLog.Id);
				}
				else
				{
					//cash_trust_status_log must be missing information
					results.Add(trustId, 0);
				}
			}

			return results;
		}

		private IEnumerable<BalanceSheetDetail> GetDeductListForNewBalanceSheet(BalanceSheetDetailCollection allDetails)
		{
			List<BalanceSheetDetail> results = new List<BalanceSheetDetail>();

            IEnumerable<BalanceSheetDetail> allUndos = allDetails.Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Undo).ToList();
			IEnumerable<BalanceSheetDetail> allDeducted = allDetails.Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Deduction).ToList();

			foreach(var detail in allUndos)
			{
				BalanceSheetDetail currentDetail = detail;
				int undoCount = allUndos.Where(x => currentDetail.TrustId == x.TrustId).Count();
				int deductCount = allDeducted.Where(x => currentDetail.TrustId == x.TrustId).Count();
				if(undoCount != deductCount)
				{
					results.Add(currentDetail);
				}
			}

			return results;
		}

		private IEnumerable<Guid> GetAllBalanceSheetDetailListExceptUndos(BalanceSheetDetailCollection allDetails)
        {
            return allDetails.Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Normal || 
                                              detail.Status == (int)BalanceSheetDetailStatus.NoPay)
                             .Select(detail => detail.TrustId);
        }

        private IEnumerable<Guid> GetAllVerifiedTrustList(CashTrustLogCollection allTrustList, Guid? storeGuid)
        {
            if (allTrustList != null)
            {
				IEnumerable<CashTrustLog> resultLogs = 
                allTrustList
                    .Where(log => log.SpecialStatus != (int)TrustSpecialStatus.Freight
                                            && log.VerifiedStoreGuid == storeGuid
                                            && log.UsageVerifiedTime < GetCashTrustLogModifyTimeThreshold()
                                            && (log.Status == (int)TrustStatus.Verified
                                            || (log.Status == (int)TrustStatus.Returned && Helper.IsFlagSet(log.SpecialStatus, TrustSpecialStatus.ReturnForced))
                                            || (log.Status == (int)TrustStatus.Refunded && Helper.IsFlagSet(log.SpecialStatus, TrustSpecialStatus.ReturnForced)))
                                  );
				IEnumerable<Guid> results = resultLogs
                    .Select(pponCtLog => pponCtLog.TrustId);

				return results;
            }
            return new List<Guid>();
        }

        private int PersistNewBalanceSheet(
            IDictionary<Guid, int> payList,
			IEnumerable<BalanceSheetDetail> deductList,
            VerificationStatisticsLog unpersistedStatistics,
            BalanceSheet newUnpersistedBS)
        {
            int balanceSheetId;

            using (TransactionScope txScope = new TransactionScope())
            {
                #region persist statistics

                int statId = pp.VerificationStatisticsLogInsert(unpersistedStatistics, this.MerchandiseGuid, "sys");
                newUnpersistedBS.VerificationStatisticsLogId = statId;

                #endregion

                #region persist balance sheet

                balanceSheetId = ap.BalanceSheetSet(newUnpersistedBS);

                #endregion

                #region persist balance sheet details

                BalanceSheetDetailCollection details = new BalanceSheetDetailCollection();

                var slottingFeeCount = GetPreviousBsSlottingFeeCount();
                var slottingFeeQuantity = GetSlottingFeeQuantity();

                foreach (KeyValuePair<Guid, int> trust in payList)
                {
                    var detailStatus = (int) BalanceSheetDetailStatus.Normal;
                    if (slottingFeeQuantity > slottingFeeCount)
                    {
                        detailStatus = (int) BalanceSheetDetailStatus.NoPay;
                        slottingFeeCount++;
                    }

                    BalanceSheetDetail detail = new BalanceSheetDetail();
                    detail.BalanceSheetId = balanceSheetId;
					detail.TrustId = trust.Key;
                    detail.Status = detailStatus;
                    
                	detail.CashTrustStatusLogId = trust.Value;
                    details.Add(detail);
                }

				foreach (BalanceSheetDetail undoDetail in deductList)
                {
                    BalanceSheetDetail detail = new BalanceSheetDetail();
                    detail.BalanceSheetId = balanceSheetId;
					detail.TrustId = undoDetail.TrustId;
                    detail.Status = (int)BalanceSheetDetailStatus.Deduction;
                	detail.UndoTime = undoDetail.UndoTime;
                    detail.UndoId = undoDetail.UndoId;
                	detail.CashTrustStatusLogId = undoDetail.CashTrustStatusLogId;
                    detail.DeductedBalanceSheetId = undoDetail.BalanceSheetId;
                    details.Add(detail);
                }

                if (details.Count() > 0)
                {
                    ap.BalanceSheetDetailSetList(details);
                }

                #endregion
                
                
                var bsModel = new BalanceSheetModel(balanceSheetId);

                #region 週對帳單的確認

                if (bsModel.Type == BalanceSheetType.WeeklyPayWeekBalanceSheet
                    || bsModel.Type == BalanceSheetType.ManualWeeklyPayWeekBalanceSheet)
                {
                    BalanceSheetCollection balanceSheets =
                        ap.BalanceSheetGetListByProductGuidStoreGuid(this.MerchandiseGuid, bsModel.StoreGuid, null);

                    DateTime thresholdDate;
                    if (TryGetConfirmationThresholdDate(balanceSheets, out thresholdDate))
                    {
                        //有未確認的月對帳單, 週對帳單日期要小於擋款日才可確認
                        if (newUnpersistedBS.IntervalEnd <= thresholdDate)
                        {
                            newUnpersistedBS.IsConfirmedReadyToPay = true;
                            newUnpersistedBS.ConfirmedUserName = "sys";
                            newUnpersistedBS.ConfirmedTime = GetGenerationTime();
                        }
                    }
                    else
                    {
                        //沒有未確認的月對帳單, 週對帳單可直接確認
                        newUnpersistedBS.IsConfirmedReadyToPay = true;
                        newUnpersistedBS.ConfirmedUserName = "sys";
                        newUnpersistedBS.ConfirmedTime = GetGenerationTime();
                    }
                }

                #endregion

                #region 對帳單應付金額  

                int amount; 

                if (bsModel.Type == BalanceSheetType.WeeklyPayMonthBalanceSheet
                    || bsModel.Type == BalanceSheetType.ManualWeeklyPayMonthBalanceSheet)
                {
                    //週結已先付款, 所以月對帳單的應付 = 實付
                    amount = bsModel.TransferInfo.AccountsPaid.GetValueOrDefault();
                }
                else
                {
                    amount = (int)bsModel.GetAccountsPayable().TotalAmount;
                }
                
                newUnpersistedBS.EstAmount = amount;

                #endregion 對帳單應付金額

                #region 0 元對帳單 & 負對帳單的確認

                /*
                 * 0元 和 金額為負(金額為負的規則2016/3/29拿掉了)的對帳單不需要管理單據, 
                 * 所以要直接確認掉, 跳過營管部門這道關卡, 並進入財會部門的程序.
                 * 0元不需要付錢 => pay_report_id = 0
                 * 負的需要收錢 => pay_report_id = null, 等收到錢後會填入對應的 id.
                 */

                if (amount == 0) 
                {
                    newUnpersistedBS.PayReportId = 0;
                    newUnpersistedBS.IsConfirmedReadyToPay = true;
                    newUnpersistedBS.ConfirmedUserName = "sys";
                    newUnpersistedBS.ConfirmedTime = GetGenerationTime();
                    newUnpersistedBS.IsDetailFixed = true;

                    if(newUnpersistedBS.BalanceSheetType != (int)BalanceSheetType.WeeklyPayWeekBalanceSheet &&
                       newUnpersistedBS.BalanceSheetType != (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet)
                    {
                        newUnpersistedBS.IsReceiptReceived = true;
                    }

                    if(!newUnpersistedBS.Year.HasValue) //週對帳單才會發生
                    {
                        DateTime date = newUnpersistedBS.IntervalEnd.AddDays(-1);
                        newUnpersistedBS.Year = date.Year;
                        newUnpersistedBS.Month = date.Month;
                    }
                }

                #endregion

                ap.BalanceSheetSet(newUnpersistedBS);

                txScope.Complete();
            }

            return balanceSheetId;
        }

        private bool TryGetConfirmationThresholdDate(BalanceSheetCollection balanceSheets, out DateTime thresholdDate)
        {
            List<BalanceSheet> unconfirmedMbs = balanceSheets.Where(sheet =>
                       sheet.GenerationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet
                       && sheet.IsConfirmedReadyToPay == false)
                       .OrderBy(x => x.IntervalStart).ToList();

            if (unconfirmedMbs.Count > 0)
            {
                int receiptThresholdDay = ProviderFactory.Instance().GetConfig().ReceiptReceivedThresholdDay;

                BalanceSheet mbs = unconfirmedMbs.First();
                thresholdDate = (new DateTime(mbs.Year.Value, mbs.Month.Value, receiptThresholdDay)).AddMonths(1);
                return true;
            }

            thresholdDate = DateTime.MinValue;
            return false;
        }

        private int GetSlottingFeeQuantity()
        {
            DealCostCollection dcc = pp.DealCostGetList(this.MerchandiseGuid);
            return dcc.Count > 1 ? dcc.Min(x => x.CumulativeQuantity).Value : 0; 
        }

        private int GetPreviousBsSlottingFeeCount()
        {
            //統計多分店已列入之上架費份數 需重新撈取details以利計算已列入對帳單明細之上架費份數
            return GetOldBalanceSheetDetails(GetBalanceSheetsStoreGuid()).Values.Sum(detail => detail.Count(x => x.Status == (int)BalanceSheetDetailStatus.NoPay));
        }

        private void UpdateGenerationResults(Guid? storeGuid, BalanceSheetGenerationResultStatus generationStatus, int balanceSheetId)
        {
            if (generationStatus == BalanceSheetGenerationResultStatus.Success)
            {
                GenerationResults.Add(GetGenerationSuccessResult(storeGuid, balanceSheetId));
            }
            else
            {
                GenerationResults.Add(GetGenerationFailResult(storeGuid, generationStatus));
            }
        }

        private BalanceSheetGenerationResult GetGenerationFailResult(Guid? storeGuid, BalanceSheetGenerationResultStatus generationStatus)
        {
            return new BalanceSheetGenerationResult
            {
                IsSuccess = false,
                BalanceSheetId = 0,
                MerchandiseGuid = this.MerchandiseGuid,
                MerchandiseType = this.MerchandiseType,
                BalanceSheetType = this.BalanceSheetType,
                StoreGuid = storeGuid,
                Status = generationStatus,
                FailureMessage = string.Empty
            };
        }

        private BalanceSheetGenerationResult GetGenerationSuccessResult(Guid? storeGuid, int balanceSheetId)
        {
            return new BalanceSheetGenerationResult
            {
                IsSuccess = true,
                BalanceSheetId = balanceSheetId,
                MerchandiseGuid = this.MerchandiseGuid,
                MerchandiseType = this.MerchandiseType,
                BalanceSheetType = this.BalanceSheetType,
                StoreGuid = storeGuid,
                Status = BalanceSheetGenerationResultStatus.Success,
                FailureMessage = string.Empty
            };
        }

        private BalanceSheetGenerationResult GetGenerationExceptionResult(Guid? storeGuid, Exception exception)
        {
            return new BalanceSheetGenerationResult
            {
                IsSuccess = false,
                BalanceSheetId = 0,
                MerchandiseGuid = this.MerchandiseGuid,
                MerchandiseType = this.MerchandiseType,
                BalanceSheetType = this.BalanceSheetType,
                StoreGuid = storeGuid,
                Status = BalanceSheetGenerationResultStatus.Exception,
                FailureMessage = string.Format("message: {0} \r\n trace: {1}", exception.Message, exception.StackTrace)
            };
        }

		private BalanceSheetGenerationResult GetPreconditionFailResult(string failureMessage)
		{
			return new BalanceSheetGenerationResult
			{
				IsSuccess = false,
				BalanceSheetId = 0,
				MerchandiseGuid = this.MerchandiseGuid,
				MerchandiseType = this.MerchandiseType,
				BalanceSheetType = this.BalanceSheetType,
				StoreGuid = null,
				Status = BalanceSheetGenerationResultStatus.PreconditionCheckFailed,
                FailureMessage = failureMessage
			};

		}

        private bool IsOverCouponUsageTime()
        {
            var usageEndTime = pp.BusinessHourGet(this.MerchandiseGuid).BusinessHourDeliverTimeE;
            if (!usageEndTime.HasValue)
            {
                return false;
            }

            return usageEndTime.Value.AddDays(config.VerificationBuffer) < GetCashTrustLogModifyTimeThreshold();
    }

        #endregion


        #region .ctors

        protected SystemBsCreationTemplate()
        {
            InitializeProviders();
        }

        #endregion


        
    }
}

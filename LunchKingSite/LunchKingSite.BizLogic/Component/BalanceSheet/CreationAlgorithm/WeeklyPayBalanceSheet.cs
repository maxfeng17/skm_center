﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;

namespace LunchKingSite.BizLogic.Component
{
    public class WeeklyPayBalanceSheet : SystemBsCreationTemplate
    {
        protected sealed override DateTime GetCashTrustLogModifyTimeThreshold()
        {
            var genTime = GetGenerationTime().GetFirstDayOfWeek().AddDays(1);  // first day of week is sunday, we need monday through sunday
            var result = new DateTime(
                genTime.Year,
                genTime.Month,
                genTime.Day
                );	//GetFirstDayOfWeek only changes the date information of GetGenerationTime(), not the time

            return result;
        }

        protected sealed override bool FinancialStaffPaymentSettingCheck()
        {
            return VendorBillingHelper.FinancialStaffPaymentMethod == RemittanceType.Weekly;
        }

        protected sealed override bool StabilizePreviousBalanceSheetContent(Guid? storeGuid)
        {
            BalanceSheet unfixedBS = ap.BalanceSheetGetDetailNotFixed(this.MerchandiseGuid, this.MerchandiseType, storeGuid);
            if (unfixedBS == null)
            {
                return true;
            }
            unfixedBS.IsDetailFixed = true;
            ap.BalanceSheetSet(unfixedBS);
            return true;
        }

        protected sealed override BalanceSheet PrepareBalanceSheet(Guid? storeGuid)
        {
            var bs = new BalanceSheet
            {
                ProductGuid = this.MerchandiseGuid,
                ProductType = (int)this.MerchandiseType,
                StoreGuid = storeGuid,
                GenerationFrequency = (int)BalanceSheetGenerationFrequency.WeekBalanceSheet,
                IsDetailFixed = true,
                IsConfirmedReadyToPay = false,
                ConfirmedUserName = null,
                IsReceiptReceived = false,
                IsManual = false,
                Year = GetCashTrustLogModifyTimeThreshold().Year,
                Month = GetCashTrustLogModifyTimeThreshold().Month,
                IntervalStart = GetCashTrustLogModifyTimeThreshold().AddDays(-7),
                IntervalEnd = GetCashTrustLogModifyTimeThreshold(),
                CreateId = "sys",
                CreateTime = this.GetGenerationTime(),
                BalanceSheetType = (int)this.BalanceSheetType,
                ProductId = this.MerchandiseId
            };

            return bs;
        }

        internal WeeklyPayBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId)
        {
            this.MerchandiseType = merchandiseType;
            this.MerchandiseKey = merchandiseKey;
            this.BalanceSheetType = balanceSheetType;
            this.MerchandiseId = merchandiseId;
            TrySetMerchandiseGuid();
        }
    }
}

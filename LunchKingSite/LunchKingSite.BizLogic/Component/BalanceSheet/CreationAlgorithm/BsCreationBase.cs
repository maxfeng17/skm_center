﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Interface;

namespace LunchKingSite.BizLogic.Component
{
    public abstract class BsCreationBase
    {
        public BalanceSheetType BalanceSheetType { get; protected set; }
        public BusinessModel MerchandiseType { get; protected set; }
        public virtual Guid MerchandiseGuid { get; protected set; }
        public int MerchandiseId { get; protected set; }

        protected IVendorBillingHelper VendorBillingHelper { get; set; }        
     
        public abstract ReadOnlyCollection<BalanceSheetGenerationResult> Generate();
        public abstract void SetGenerationTime(DateTime simulationTime);	
    }

    public class BalanceSheetGenerationResult
    {
        public bool IsSuccess { get; set; }
        public int BalanceSheetId { get; set; }
        public Guid MerchandiseGuid { get; set; }
        public BusinessModel MerchandiseType { get; set; }
        public BalanceSheetType BalanceSheetType { get; set; }
        public Guid? StoreGuid { get; set; }
        public BalanceSheetGenerationResultStatus Status { get; set; }
        public string FailureMessage { get; set; }

        public override string ToString()
        {
            var result = new StringBuilder();

            string bizType;
            switch(MerchandiseType)
            {
                case BusinessModel.Ppon:
                    bizType = "好康";
                    break;
                case BusinessModel.PiinLife:
                    bizType = "品生活";
                    break;
                default:
                    bizType = "不知道的商業模式";
                    break;
            }

            if (this.StoreGuid.HasValue)
            {
                result.Append(string.Format("{0}({1}) -分店({2}) : ", bizType, this.MerchandiseGuid.ToString(), this.StoreGuid));
            }
            else
            {
                result.Append(string.Format("{0}({1}) : ", bizType, this.MerchandiseGuid.ToString()));
            }

            if (this.IsSuccess)
            {
                result.Append(string.Format("成功-對帳單 id = {0}", this.BalanceSheetId));
            }
            else
            {
                if (this.Status == BalanceSheetGenerationResultStatus.Exception)
                {
                    result.Append("失敗-");
                    result.Append(this.FailureMessage.TrimToMaxLength(800, "...blah blah blah..."));
                }
                else
                {
                    result.Append("不用產-");
                    result.Append(this.Status.ToString() + ":");
                    result.Append(this.FailureMessage);
                }
            }

            result.Append(Environment.NewLine);
            return result.ToString();
        }
    }

    public class AmountDetail
    {
        public decimal NetAmount { get; set; }
        public decimal PayAmount { get; set; }
        public decimal DeductAmount { get; set; }
    }
}

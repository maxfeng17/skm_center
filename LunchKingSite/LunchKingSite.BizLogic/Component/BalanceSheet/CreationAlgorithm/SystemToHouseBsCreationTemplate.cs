﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// 系統對帳單. "系統" 指的是可以自動並有規則的產出對帳單的對帳單類型.
    /// </summary>
    public abstract class SystemToHouseBsCreationTemplate : BsCreationBase
    {
        #region props

        public object MerchandiseKey { get; set; }
        public string TriggerUser { get; set; }

        #endregion props

        #region protected props

        protected bool IsHelperSet { get; set; }
        protected bool IsDetailsNeeded { get; set; }
        protected bool IsMerchandiseGuidSet { get; set; }
        protected CashTrustLogCollection AllCashTrustLogs { get; set; }
        protected BalanceSheetDetailCollection AllOldBalanceSheetDetails { get; set; }

        #endregion protected props

        /// <summary>
        /// 設定產對帳單的模擬時間.
        /// </summary>
        /// <param name="simulationTime"></param>
        public override void SetGenerationTime(DateTime simulationTime)
        {
            this.generationTime = simulationTime;
        }

        /// <summary>
        /// 產對帳單 ***template method for generating balance sheet***
        /// </summary>
        /// <returns></returns>
        public override ReadOnlyCollection<BalanceSheetGenerationResult> Generate()
        {
            GenerationSetup();

            if (!BalanceSheetTypeCheck() || !IsHelperSet || !PreExecutionCheck())
            {
                string failureMessage = string.Empty;

                if (!BalanceSheetTypeCheck())
                {
                    failureMessage = "balance sheet type check failed.";
                }
                if (!IsHelperSet)
                {
                    failureMessage = "VendorBillingHelper set failed.";
                }
                if (!PreExecutionCheck())
                {
                    failureMessage = "preexecution check failed.";
                }

                GenerationResults.Add(GetPreconditionFailResult(failureMessage));
                return GenerationResults.AsReadOnly();
            }

            
            DataSetup();

            try
            {
                if (!GenerationTimeRangeCheck())
                {
                    GenerationResults.Add(GetGenerationFailResult(BalanceSheetGenerationResultStatus.OutOfVerificationTimeRange));
                    return GenerationResults.AsReadOnly();
                }

                if (!StabilizePreviousBalanceSheetContent())
                {
                    GenerationResults.Add(GetGenerationFailResult(BalanceSheetGenerationResultStatus.StablizingCheckFailed));
                    return GenerationResults.AsReadOnly();
                }

                BalanceSheetGenerationResultStatus generationStatus;
                int balanceSheetId = GenerateBalanceSheet(out generationStatus);
                UpdateGenerationResults(generationStatus, balanceSheetId);
            }
            catch (Exception ex)
            {
                GenerationResults.Add(GetGenerationExceptionResult(ex));
                return GenerationResults.AsReadOnly();
            }

            if (GenerationResults.Any())
            {
                //寫入對帳單產出結果紀錄
                foreach (var result in GenerationResults)
                {
                    var resultDesc = string.Format("對帳單產出結果:{0}-{1} 對帳單類型:{2} 對帳單Id:{3}",
                        result.IsSuccess ? "成功" : "失敗", result.Status, result.BalanceSheetType, result.BalanceSheetId);
                    CommonFacade.AddAudit(result.MerchandiseGuid, AuditType.DealAccounting, resultDesc, string.IsNullOrEmpty(TriggerUser) ? "sys" : TriggerUser, true);
                }
            }

            return GenerationResults.AsReadOnly();
        }
        
        #region protected members

        #region abstract members

        /// <summary>
        /// cash_trust_log 的 modify time 臨界值 (對於該對帳單有效的 cash_trust_log)
        /// </summary>
        /// <returns></returns>
        protected abstract DateTime GetCashTrustLogModifyTimeThreshold();

        /// <summary>
        /// 財會的匯款方式檢查
        /// </summary>
        protected abstract bool FinancialStaffPaymentSettingCheck();


        /// <summary>
        /// 把要產的對帳單資料準備好.
        /// </summary>
        /// <returns></returns>
        protected abstract BalanceSheet PrepareBalanceSheet();

        #endregion


        #region overridable

        protected virtual void GenerationSetup()
        {
            IsMerchandiseGuidSet = TrySetMerchandiseGuid();
            IsHelperSet = TrySetBillingHelper();
            IsDetailsNeeded = true;
            GenerationResults = new List<BalanceSheetGenerationResult>();
        }

        protected virtual bool PreExecutionCheck()
        {
            if (!GetDeliverStartTime().HasValue ||
                !GetDeliverEndTime().HasValue)
            {
                return false;
            }

            if (BillingSystemCheck() &&
                FinancialStaffPaymentSettingCheck() &&
                PponSpecificCheck())
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 取得相關資料
        /// </summary>
        protected virtual void DataSetup()
        {
            AllCashTrustLogs = GetAllCashTrustLogs();
            AllOldBalanceSheetDetails = GetOldBalanceSheetDetails();
        }

        protected DateTime? generationTime;
        protected DateTime GetGenerationTime()
        {
            if (generationTime == null)
            {
                generationTime = DateTime.Now;
            }

            return generationTime.Value;
        }

        protected virtual VerificationStatisticsLog CalculateStatistics(CashTrustLogCollection allTrustData)
        {
            //取這一期的資料
            IEnumerable<CashTrustLog> frieghtlessData = allTrustData.ToList();
            IEnumerable<CashTrustLog> thresholdTimeData = frieghtlessData.Where(log => log.ModifyTime <= this.GetCashTrustLogModifyTimeThreshold()).ToList();

            var stats = new VerificationStatisticsLog
            {
                Unverified = thresholdTimeData.Count(x => x.Status == (int)TrustStatus.Initial || x.Status == (int)TrustStatus.Trusted),
                Verified = thresholdTimeData.Count(x => x.Status == (int)TrustStatus.Verified),
                Returned = thresholdTimeData.Count(x => x.Status == (int)TrustStatus.Returned || x.Status == (int)TrustStatus.Refunded),
                Adjustment = 0,
                Status = (int)VerificationStatisticsLogStatus.BalanceSheet
            };

            return stats;
        }

        /// <summary>
        /// 取時間內有進倉日的超取訂單，計算物流處理費使用
        /// </summary>
        /// <param name="MerchandiseGuid"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        protected virtual IDictionary<Guid,int> GetISPOrder(Guid MerchandiseGuid, DateTime startTime, DateTime endTime)
        {
            //若是彈性出帳需要額外取起始時間(測試用)
            //var balanceSheetStartTime = ap.BalanceSheetGetListByProductGuid(MerchandiseGuid).Where(x => x.IntervalEnd < endTime);
            //if (balanceSheetStartTime.Count() > 0)
            //    startTime = balanceSheetStartTime.Max(x => x.IntervalEnd);


            //超取付款尚未成立訂單也要撈取
            //超取費用只抓B2C
            //過去沒算物流的重抓檢查
            Dictionary<Guid, int> ispOrderList = new Dictionary<Guid, int>();

            //所有有進倉時間的訂單
            var StockOrderList = op.ViewOrderShipListGetListByProductGuid(MerchandiseGuid, OrderClassification.LkSite)
                .Where(x => x.ShipToStockTime.HasValue && x.ShipToStockTime < endTime 
                         && x.IspOrderType == (int)IspOrderType.B2C);

            //已納入對帳單的訂單
            IEnumerable<Guid> OldIspDetail = GetOldBalanceSheetIspDetails(MerchandiseGuid).Select(x => x.OrderGuid);

            //排除已納入
            IEnumerable<ViewOrderShipList> orderList = StockOrderList.Where(x => !OldIspDetail.Contains(x.OrderGuid)).ToList();

            foreach (var order in orderList)
            {
                if (order.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
                {
                    ispOrderList.Add(order.OrderGuid, (int)ProductDeliveryType.FamilyPickup);
                }
                else if (order.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
                {
                    ispOrderList.Add(order.OrderGuid, (int)ProductDeliveryType.SevenPickup);
                }
            }           


            return ispOrderList;


        }

        /// <summary>
        /// 取時間內有PCHOME物流處理費
        /// </summary>
        /// <param name="MerchandiseGuid"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        protected virtual void GeWmsPOrder(Guid MerchandiseGuid, DateTime endTime,out WmsOrderServFeeCollection WmsOrderServFeeList,
                                          out WmsShipFeeCollection WmsShipFeeList,out WmsPackFeeCollection WmsPackFeeList,
                                           out WmsRefundShipFeeCollection WmsRefundShipFeeList)
        {

            Dictionary<Guid, int> wmsOrderList = new Dictionary<Guid, int>();


            WmsOrderServFeeList = wp.WmsOrderServFeeGetByOrder(MerchandiseGuid, endTime);
            WmsShipFeeList = wp.WmsShipFeeGetListGetByOrder(MerchandiseGuid, endTime);
            WmsPackFeeList = wp.WmsPackFeeGetByOrder(MerchandiseGuid, endTime);
            WmsRefundShipFeeList = wp.WmsRefundShipFeeGetByOrder(MerchandiseGuid, endTime);

        }

        /// <summary>
        /// 確認有無產過對帳單
        /// </summary>
        /// <param name="unpersistedBS"></param>
        /// <returns></returns>
        protected virtual bool DuplicationCheck(BalanceSheet unpersistedBS)
        {
            int count = ap.BalanceSheetDuplicationCountCheck(unpersistedBS.ProductGuid,
                                                this.MerchandiseType,
                                                unpersistedBS.StoreGuid, unpersistedBS.IntervalStart, unpersistedBS.IntervalEnd,
                                                this.BalanceSheetType);
            return count == 0;
        }

        protected virtual BalanceSheet FinalizeBalanceSheet(BalanceSheet newBS, IDictionary<Guid, int> payList)
        {
            return newBS;
        }

        protected virtual bool ItemsCheck(BalanceSheet newBS, IDictionary<Guid, int> payList)
        {
            return true;
        }

        /// <summary>
        /// 商品對帳單是否符合產生對帳單區間的檢查
        /// </summary>
        protected virtual bool GenerationTimeRangeCheck()
        {
            //先收單據後付款:週結、月結檔次退換貨完成日+緩衝時間內
            if (!BalanceSheetManager.IsInBalanceTimeRange(this.MerchandiseGuid, this.GetGenerationTime()))
            {
                //產生時間已經超出核銷區間, 要確認是否還有最後一份對帳單要產
                if (!IsLastBalanceSheet() && !IsVendorPaymentNoBalanceSheet())
                {
                    //如果不是最後一張對帳單, 就真的不屬於產對帳單的期限內, 這個 check 就需要fail掉
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 最後一張是否在區間內，若是則欲產對帳單為最後一張對帳單
        /// </summary>
        /// <returns></returns>
        protected virtual bool IsLastBalanceSheet()
        {
            var latestBS = GetLatestBalanceSheet();
            if (latestBS == null
                || (!BalanceSheetManager.IsInBalanceTimeRange(this.MerchandiseGuid, latestBS.IntervalEnd)))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 是否還有需補扣款及異動金額要對帳
        /// </summary>
        /// <returns></returns>
        protected virtual bool IsVendorPaymentNoBalanceSheet()
        {
            var vpoc = ap.VendorPaymentOverdueGetListByBid(this.MerchandiseGuid)
                .Where(x => !x.BalanceSheetId.HasValue).Any();

            var vpcs = ap.VendorPaymentChangeGetList(this.MerchandiseGuid)
                .Where(x => !x.BalanceSheetId.HasValue).Any();

            return vpoc | vpcs;
        }

        /// <summary>
        /// 把以前的對帳單明細固定住. 沒有固定時, 退貨份數完成退款 的動作會調整對帳單明細.
        /// </summary>
        protected virtual bool StabilizePreviousBalanceSheetContent()
        {
            var unfixedBS = ap.BalanceSheetGetDetailNotFixed(this.MerchandiseGuid, this.MerchandiseType);
            if (unfixedBS == null)
            {
                return true;
            }
            unfixedBS.IsDetailFixed = true;
            ap.BalanceSheetSet(unfixedBS);
            return true;
        }

        #endregion


        #region non-overridable

        protected IAccountingProvider ap;
        protected IHiDealProvider hp;
        protected IMemberProvider mp;
        protected IOrderProvider op;
        protected IPponProvider pp;
        protected IWmsProvider wp;
        protected ISysConfProvider config;
        protected void InitializeProviders()
        {
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        protected bool TrySetMerchandiseGuid()
        {
            if (MerchandiseKey == null)
            {
                return false;
            }

            Guid guidKey;
            if (MerchandiseType == BusinessModel.Ppon &&
                Guid.TryParse(MerchandiseKey.ToString(), out guidKey))
            {
                this.MerchandiseGuid = guidKey;
                return true;
            }

            return false;
        }

        /// <summary>
        /// 檢查是否用對帳單系統的對帳方式
        /// </summary>
        /// <returns></returns>
        protected bool BillingSystemCheck()
        {
            return VendorBillingHelper.VendorBillingModel == VendorBillingModel.BalanceSheetSystem;
        }

        protected bool PponSpecificCheck()
        {
            if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return false;
            }

            //過結檔時間 未成檔檔次無須再繼續產對帳單
            if (!IsDealEstablished() && !IsVendorPaymentNoBalanceSheet())
            {
                return false;
            }

            //多檔次主檔不可產對帳單
            var result = pp.GetViewComboDealByBid(this.MerchandiseGuid);
            return result.Count() == 0;
        }

        protected BalanceSheetGenerationFrequency GetGenerationFrequency()
        {
            switch (this.BalanceSheetType)
            {
                case BalanceSheetType.WeeklyToHouseBalanceSheet:
                    return BalanceSheetGenerationFrequency.WeekBalanceSheet;
                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                    return BalanceSheetGenerationFrequency.FortnightlyBalanceSheet;
                case BalanceSheetType.MonthlyToHouseBalanceSheet:
                    return BalanceSheetGenerationFrequency.MonthBalanceSheet;
                case BalanceSheetType.FlexibleToHouseBalanceSheet:
                    return BalanceSheetGenerationFrequency.FlexibleBalanceSheet;
                default:
                    return BalanceSheetGenerationFrequency.Unknown;
            }
        }

        protected DateTime? GetDeliverStartTime()
        {
            if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return null;
            }

            return pp.BusinessHourGet(this.MerchandiseGuid).BusinessHourDeliverTimeS;
        }

        protected DateTime? GetDeliverEndTime()
        {
            if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return null;
            }

            return pp.BusinessHourGet(this.MerchandiseGuid).BusinessHourDeliverTimeE;
        }


        /// <summary>
        /// 取得cash_trust_log該檔次排除(退貨中暫壓已退貨)
        /// </summary>
        /// <returns></returns>
        protected CashTrustLogCollection GetAllCashTrustLogs()
        {
            //所有成功訂單
            var cashTrustLogs = VendorBillingHelper.GetAllSuccessfulOrderTrustList();
            //抓取之trust_id 須將 退貨處理中(無論商家是否標註退貨完成) 退貨單 之 refund trust_id 視為已退貨(剔除出貨份數)
            cashTrustLogs = BalanceSheetManager.GetTrustListWithReturnningToReturned(cashTrustLogs, false);

            //**寄倉功能開啟方啟用寄倉檔次出帳規則**
            //寄倉檔次運費將另外計算 故於對帳單明細中排除運費份數
            //待日後確認運費計算規則(使用進貨運費架構 購物車結帳結構尚無進貨運費規劃)後 再進行調整修正
            if (config.IsConsignment && IsConsignmentDeal())
            {
                var cashTrustLogsWithOutFreight = new CashTrustLogCollection();
                cashTrustLogsWithOutFreight.AddRange(cashTrustLogs.Where(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)));
                return cashTrustLogsWithOutFreight;
            }

            return cashTrustLogs;
        }

        /// <summary>
        /// 取得該檔次已出現過的所有對帳單明細
        /// </summary>
        /// <returns></returns>
        protected BalanceSheetDetailCollection GetOldBalanceSheetDetails()
        {
            return ap.BalanceSheetDetailGetListByProductGuidAndStoreGuid(this.MerchandiseGuid, null);
        }

        protected bool IsDealFreeze()
        {
            return Helper.IsFlagSet(pp.DealAccountingGet(this.MerchandiseGuid).Flag, (int) AccountingFlag.Freeze);
        }

        protected BalanceSheet GetLatestBalanceSheet()
        {
            return ap.BalanceSheetGetLatest(this.MerchandiseType, this.MerchandiseGuid, null, GetGenerationFrequency());
        }

        protected bool IsDealEstablished()
        {
            int tempSlug;
            var deal = pp.ViewPponDealGetByBusinessHourGuid(this.MerchandiseGuid);

            //未過結檔時間 跳過是否成檔的檢查
            if (deal != null && deal.BusinessHourOrderTimeE > GetGenerationTime())
            {
                return true;
            }

            return deal != null &&
                   int.TryParse(deal.Slug, out tempSlug) &&
                   tempSlug >= deal.BusinessHourOrderMinimum;
        }

        //檢查是否為寄倉檔次
        protected bool IsConsignmentDeal()
        {
            return pp.DealPropertyGet(this.MerchandiseGuid).Consignment;
        }

        protected BalanceSheetIspDetailCollection GetOldBalanceSheetIspDetails(Guid MerchandiseGuid)
        {
            return ap.BalanceSheetIspDetailGetListByBid(MerchandiseGuid);
        }

        #endregion


        #endregion

        #region private members

        /// <summary>
        /// 確認對帳單類型
        /// </summary>
        /// <returns></returns>
        private bool BalanceSheetTypeCheck()
        {
            return this.MerchandiseType == BusinessModel.Ppon &&
                   (this.BalanceSheetType == BalanceSheetType.WeeklyToHouseBalanceSheet ||
                    this.BalanceSheetType == BalanceSheetType.FortnightlyToHouseBalanceSheet || 
                    this.BalanceSheetType == BalanceSheetType.MonthlyToHouseBalanceSheet ||
                    this.BalanceSheetType == BalanceSheetType.FlexibleToHouseBalanceSheet);
        }

        private List<BalanceSheetGenerationResult> GenerationResults { get; set; }

        private bool TrySetBillingHelper()
        {
            if (this.MerchandiseType == BusinessModel.Ppon)
            {
                this.VendorBillingHelper = new PponVendorBillingHelper(this.MerchandiseGuid);
                return true;
            }

            return false;
        }

        private int GenerateBalanceSheet(out BalanceSheetGenerationResultStatus status)
        {
            BalanceSheet newBS = PrepareBalanceSheet();
            if (newBS == null)
            {
                status = BalanceSheetGenerationResultStatus.BalanceSheetDataError;
                return 0;
            }
            if (!DuplicationCheck(newBS))
            {
                status = BalanceSheetGenerationResultStatus.Duplication;
                return 0;
            }

            IDictionary<Guid, int> payList;	//Guid: [trust_id], int: [cash_trust_status_log].[id]
            IEnumerable<BalanceSheetDetail> deductList;	
            if (IsDetailsNeeded)
            {
                //本期應付
                payList = GetPayListForNewBalanceSheet(AllCashTrustLogs, AllOldBalanceSheetDetails);
                //過去反核消須扣回
                deductList = GetDeductListForNewBalanceSheet(AllOldBalanceSheetDetails).ToList();
            }
            else
            {
                payList = new Dictionary<Guid, int>();
                deductList = new List<BalanceSheetDetail>();
            }

            if (!ItemsCheck(newBS, payList))
            {
                status = BalanceSheetGenerationResultStatus.ItemsCheckFailed;
                return 0;
            }

            BalanceSheet finalizedNewBS = FinalizeBalanceSheet(newBS, payList);

            VerificationStatisticsLog currentStatics = CalculateStatistics(AllCashTrustLogs);

            IDictionary<Guid, int> ispOrderList = GetISPOrder(newBS.ProductGuid, newBS.IntervalStart, newBS.IntervalEnd);

            WmsOrderServFeeCollection wosfc = new WmsOrderServFeeCollection();
            WmsShipFeeCollection wshfc = new WmsShipFeeCollection();
            WmsPackFeeCollection wpfc = new WmsPackFeeCollection();
            WmsRefundShipFeeCollection wrsfc = new WmsRefundShipFeeCollection();
            GeWmsPOrder(newBS.ProductGuid, newBS.IntervalEnd, out wosfc, out wshfc, out wpfc, out wrsfc);

            if (currentStatics == null)
            {
                status = BalanceSheetGenerationResultStatus.StopPayment;
                return 0;
            }

            int id = PersistNewBalanceSheet(payList, deductList, currentStatics, finalizedNewBS, ispOrderList, wosfc, wshfc, wpfc, wrsfc);
            status = BalanceSheetGenerationResultStatus.Success;
            return id;
        }

        /// <summary>
        /// 本期的cash_trust_log資料
        /// </summary>
        /// <param name="allCashTrustLogs">成功訂單排除退貨所有資料</param>
        /// <param name="allDetails">已出現過對帳單明細所有資料</param>
        /// <returns>dictionary key: trust_id; dictionary value: [cash_trust_status_log].[id]</returns>
        private IDictionary<Guid, int> GetPayListForNewBalanceSheet(CashTrustLogCollection allCashTrustLogs, BalanceSheetDetailCollection allDetails)
        {
            //已過鑑賞期的所有核銷明細
            IEnumerable<Guid> allVerifiedTrustList = GetAllVerifiedTrustList(allCashTrustLogs);
            //已產生過對帳單且付款狀態的所有核銷明細
            IEnumerable<Guid> allBalanceSheetTrustList = GetAllBalanceSheetDetailListExceptUndos(allDetails);
            //剔除已存在於其他對帳單之出貨份數
            IEnumerable<Guid> payList = allVerifiedTrustList.Where(trustId => !allBalanceSheetTrustList.Contains(trustId)).ToList();
            CashTrustStatusLogCollection statusLogs = mp.CashTrustStatusLogGetList(payList);

            Dictionary<Guid, int> results = new Dictionary<Guid, int>();
            foreach (Guid trustId in payList)
            {
                Guid trustGuid = trustId;
                CashTrustStatusLog latestLog =
                            statusLogs
                            .Where(log =>
                                log.TrustId == trustGuid
                                && log.Status == (int)TrustStatus.Verified
                                && log.ModifyTime.Date <= GetCashTrustLogModifyTimeThreshold())  //改 cash_trust_log 時, 歷程資料可能沒有填 cash_trust_log 的時間 => 兩個都填DateTime.Now 造成的時間差問題
                            .OrderByDescending(log => log.ModifyTime).FirstOrDefault();

                if (latestLog != null)
                {
                    results.Add(trustId, latestLog.Id);
                }
                else
                {
                    //cash_trust_status_log must be missing information
                    results.Add(trustId, 0);
                }
            }

            return results;
        }

        /// <summary>
        /// 過去有反核銷的對帳單
        /// </summary>
        /// <param name="allDetails"></param>
        /// <returns></returns>
        private IEnumerable<BalanceSheetDetail> GetDeductListForNewBalanceSheet(BalanceSheetDetailCollection allDetails)
        {
            List<BalanceSheetDetail> results = new List<BalanceSheetDetail>();

            IEnumerable<BalanceSheetDetail> allUndos = allDetails.Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Undo).ToList();
            IEnumerable<BalanceSheetDetail> allDeducted = allDetails.Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Deduction).ToList();

            foreach (var detail in allUndos)
            {
                BalanceSheetDetail currentDetail = detail;
                int undoCount = allUndos.Where(x => currentDetail.TrustId == x.TrustId).Count();
                int deductCount = allDeducted.Where(x => currentDetail.TrustId == x.TrustId).Count();
                if (undoCount != deductCount)//一樣就互相抵銷
                {
                    results.Add(currentDetail);
                }
            }

            return results;
        }

        private IEnumerable<Guid> GetAllBalanceSheetDetailListExceptUndos(BalanceSheetDetailCollection allDetails)
        {
            return allDetails.Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Normal).Select(detail => detail.TrustId);
        }

        private IEnumerable<Guid> GetAllVerifiedTrustList(CashTrustLogCollection allTrustList)
        {
            if (allTrustList != null)
            {
                //過10天鑑賞期之已出貨份數(每次換貨刷新)、排除換貨中
                var shippedOrders = BalanceSheetManager.GetOverTrialPeriodOrderGuid(this.MerchandiseGuid, GetCashTrustLogModifyTimeThreshold());

                //剔除標註凍結請款狀態之出貨份數
                IEnumerable<CashTrustLog> resultLogs =
                allTrustList
                    .Where(log => shippedOrders.Contains(log.OrderGuid) &&
                                  log.Status == (int)TrustStatus.Verified &&
                                  !Helper.IsFlagSet(log.SpecialStatus, (int)TrustSpecialStatus.Freeze));
                IEnumerable<Guid> results = resultLogs
                    .Select(pponCtLog => pponCtLog.TrustId);

                return results;
            }

            return new List<Guid>();
        }
        
        private VendorPaymentChangeCollection GetVendorPaymentChangeList()
        {
            var vendorPaymentChanges = new VendorPaymentChangeCollection();
            if (this.BalanceSheetType != BalanceSheetType.WeeklyToHouseBalanceSheet &&
                this.BalanceSheetType != BalanceSheetType.MonthlyToHouseBalanceSheet &&
                this.BalanceSheetType != BalanceSheetType.FlexibleToHouseBalanceSheet &&
                this.BalanceSheetType != BalanceSheetType.FortnightlyToHouseBalanceSheet)

            {
                return vendorPaymentChanges;
            }

            var vpcs = ap.VendorPaymentChangeGetList(this.MerchandiseGuid)
                .Where(x => !x.BalanceSheetId.HasValue);
            foreach (var vpc in vpcs)
            {
                vendorPaymentChanges.Add(vpc);
            }

            return vendorPaymentChanges;
        }

        private VendorPaymentOverdueCollection GetVendorPaymentOverdueList()
        {
            var vendorPaymentOverdues = new VendorPaymentOverdueCollection();
            if (this.BalanceSheetType != BalanceSheetType.WeeklyToHouseBalanceSheet &&
                this.BalanceSheetType != BalanceSheetType.MonthlyToHouseBalanceSheet &&
                this.BalanceSheetType != BalanceSheetType.FlexibleToHouseBalanceSheet &&
                this.BalanceSheetType != BalanceSheetType.FortnightlyToHouseBalanceSheet)

            {
                return vendorPaymentOverdues;
            }

            var vpoc = ap.VendorPaymentOverdueGetListByBid(this.MerchandiseGuid)
                .Where(x => !x.BalanceSheetId.HasValue);
            foreach (var vpo in vpoc)
            {
                vendorPaymentOverdues.Add(vpo);
            }

            return vendorPaymentOverdues;
        }


        /// <summary>
        /// 更新對帳單/對帳單明細/核銷明細相關資料
        /// </summary>
        /// <param name="payList">本期應付</param>
        /// <param name="deductList">//過去反核消須扣回</param>
        /// <param name="unpersistedStatistics"></param>
        /// <param name="newUnpersistedBS"></param>
        /// /// <param name="ispOrderList">超取訂單</param>
        /// <returns></returns>
        private int PersistNewBalanceSheet(
            IDictionary<Guid, int> payList,
            IEnumerable<BalanceSheetDetail> deductList,
            VerificationStatisticsLog unpersistedStatistics,
            BalanceSheet newUnpersistedBS,
            IDictionary<Guid, int> ispOrderList,
            WmsOrderServFeeCollection wosfc,
            WmsShipFeeCollection wshfc,
            WmsPackFeeCollection wpfc,
            WmsRefundShipFeeCollection wrsfc)
        {



            int balanceSheetId;

            using (TransactionScope txScope = new TransactionScope())
            {
                #region persist statistics

                int statId = pp.VerificationStatisticsLogInsert(unpersistedStatistics, this.MerchandiseGuid, "sys");
                newUnpersistedBS.VerificationStatisticsLogId = statId;

                #endregion

                #region persist balance sheet

                balanceSheetId = ap.BalanceSheetSet(newUnpersistedBS);

                #endregion

                #region persist balance sheet details

                var details = new BalanceSheetDetailCollection();

                foreach (KeyValuePair<Guid, int> trust in payList)
                {
                    var detail = new BalanceSheetDetail
                    {
                        BalanceSheetId = balanceSheetId,
                        TrustId = trust.Key,
                        Status = (int)BalanceSheetDetailStatus.Normal,
                        CashTrustStatusLogId = trust.Value
                    };
                    details.Add(detail);
                }

                foreach (BalanceSheetDetail undoDetail in deductList)
                {
                    var detail = new BalanceSheetDetail
                    {
                        BalanceSheetId = balanceSheetId,
                        TrustId = undoDetail.TrustId,
                        Status = (int)BalanceSheetDetailStatus.Deduction,
                        UndoTime = undoDetail.UndoTime,
                        UndoId = undoDetail.UndoId,
                        CashTrustStatusLogId = undoDetail.CashTrustStatusLogId,
                        DeductedBalanceSheetId = undoDetail.BalanceSheetId
                    };
                    details.Add(detail);
                }

                if (details.Any())
                {
                    ap.BalanceSheetDetailSetList(details);
                }

                #endregion

                #region persist vendor_payment_change

                var paymentChangeList = GetVendorPaymentChangeList();

                if (paymentChangeList.Any())
                {
                    foreach (var paymentChange in paymentChangeList)
                    {
                        //將異動金額對應bsid
                        paymentChange.BalanceSheetId = balanceSheetId;
                    }

                    ap.VendorPaymentChangeSetList(paymentChangeList);
                }

                #endregion persist vendor_payment_change

                #region persist vendor_payment_overdue

                var paymentOverdueList = GetVendorPaymentOverdueList();

                if (paymentOverdueList.Any())
                {
                    foreach (var paymentOverdue in paymentOverdueList)
                    {
                        //將異動金額對應bsid
                        paymentOverdue.BalanceSheetId = balanceSheetId;
                    }

                    ap.VendorPaymentOverdueSetList(paymentOverdueList);
                }

                #endregion persist vendor_payment_overdue

                #region persist balance_sheet_isp_detail
                var ispDetails = new BalanceSheetIspDetailCollection();
                foreach (KeyValuePair<Guid, int> ispOrder in ispOrderList)
                {
                    var ispDetail = new BalanceSheetIspDetail
                    {
                        BalanceSheetId = balanceSheetId,
                        OrderGuid = ispOrder.Key,
                        ProductDeliveryType = ispOrder.Value
                    };
                    ispDetails.Add(ispDetail);
                }

                if (ispDetails.Any())
                {
                    ap.BalanceSheetIspDetailSetList(ispDetails);
                }

                #endregion persist balance_sheet_isp_detail


                #region wms
                if (wosfc.Any())
                {
                    wosfc.ForEach(x => x.BalanceSheetId = balanceSheetId);
                    wp.WmsOrderServFeeSet(wosfc);
                }

                if (wshfc.Any())
                {
                    wshfc.ForEach(x => x.BalanceSheetId = balanceSheetId);
                    wp.WmsShipFeeSet(wshfc);
                }

                if (wpfc.Any())
                {
                    wpfc.ForEach(x => x.BalanceSheetId = balanceSheetId);
                    wp.WmsPackFeeSet(wpfc);
                }

                if (wrsfc.Any())
                {
                    wrsfc.ForEach(x => x.BalanceSheetId = balanceSheetId);
                    wp.WmsRefundShipFeeSet(wrsfc);
                } 
                #endregion

                var bsModel = new BalanceSheetModel(balanceSheetId);

                #region 對帳單應付金額

                TotalAmountDetail totalAmountDetail = bsModel.GetAccountsPayable();
                int amount = (int)totalAmountDetail.TotalAmount;
                int ispFamilyAmount = (int)totalAmountDetail.ISPFamilyAmount;
                int ispSeventAmount = (int)totalAmountDetail.ISPSevenAmount;
                int overdueAmount = (int)totalAmountDetail.OverdueAmount;
                int vendorPositivePaymentOverdueAmount = (int)totalAmountDetail.VendorPositivePaymentOverdueAmount;
                int vendorNegativePaymentOverdueAmount = (int)totalAmountDetail.VendorNegativePaymentOverdueAmount;
                int orderWmsAmount = (int)totalAmountDetail.WmsOrderAmount;

                newUnpersistedBS.EstAmount = amount;//最後金額
                newUnpersistedBS.IspFamilyAmount = ispFamilyAmount;//全家物流費用
                newUnpersistedBS.IspSevenAmount = ispSeventAmount;//seven物流費用
                newUnpersistedBS.OverdueAmount = overdueAmount;//逾期出貨
                newUnpersistedBS.WmsOrderAmount = orderWmsAmount;//PCHOME物流費

                #endregion 對帳單應付金額

                #region 0 元對帳單 & 負對帳單的確認

                /*
                * 原本0元的不需確認對帳單
                * 因增加對開發票相關項目
                * 只要有逾期付款 || 超取物流 || PC物流皆須確認對帳單
                */

                if (amount == 0
                    && vendorPositivePaymentOverdueAmount == 0
                    && vendorNegativePaymentOverdueAmount == 0
                    && ispFamilyAmount == 0
                    && ispSeventAmount == 0
                    && orderWmsAmount == 0)
                {
                    newUnpersistedBS.PayReportId = 0;
                    newUnpersistedBS.IsConfirmedReadyToPay = true;
                    newUnpersistedBS.ConfirmedUserName = "sys";
                    newUnpersistedBS.ConfirmedTime = GetGenerationTime();
                    newUnpersistedBS.IsReceiptReceived = true;
                    newUnpersistedBS.IsDetailFixed = true;
                }

                #endregion

                ap.BalanceSheetSet(newUnpersistedBS);

                txScope.Complete();
            }

            return balanceSheetId;
        }

        private void UpdateGenerationResults(BalanceSheetGenerationResultStatus generationStatus, int balanceSheetId)
        {
            if (generationStatus == BalanceSheetGenerationResultStatus.Success)
            {
                GenerationResults.Add(GetGenerationSuccessResult(balanceSheetId));
            }
            else
            {
                GenerationResults.Add(GetGenerationFailResult(generationStatus));
            }
        }

        private BalanceSheetGenerationResult GetGenerationFailResult(BalanceSheetGenerationResultStatus generationStatus)
        {
            return new BalanceSheetGenerationResult
            {
                IsSuccess = false,
                BalanceSheetId = 0,
                MerchandiseGuid = this.MerchandiseGuid,
                MerchandiseType = this.MerchandiseType,
                BalanceSheetType = this.BalanceSheetType,
                StoreGuid = null,
                Status = generationStatus,
                FailureMessage = string.Empty
            };
        }

        private BalanceSheetGenerationResult GetGenerationSuccessResult(int balanceSheetId)
        {
            return new BalanceSheetGenerationResult
            {
                IsSuccess = true,
                BalanceSheetId = balanceSheetId,
                MerchandiseGuid = this.MerchandiseGuid,
                MerchandiseType = this.MerchandiseType,
                BalanceSheetType = this.BalanceSheetType,
                StoreGuid = null,
                Status = BalanceSheetGenerationResultStatus.Success,
                FailureMessage = string.Empty
            };
        }

        private BalanceSheetGenerationResult GetGenerationExceptionResult(Exception exception)
        {
            return new BalanceSheetGenerationResult
            {
                IsSuccess = false,
                BalanceSheetId = 0,
                MerchandiseGuid = this.MerchandiseGuid,
                MerchandiseType = this.MerchandiseType,
                BalanceSheetType = this.BalanceSheetType,
                StoreGuid = null,
                Status = BalanceSheetGenerationResultStatus.Exception,
                FailureMessage = string.Format("message: {0} \r\n trace: {1}", exception.Message, exception.StackTrace)
            };
        }

        private BalanceSheetGenerationResult GetPreconditionFailResult(string failureMessage)
        {
            return new BalanceSheetGenerationResult
            {
                IsSuccess = false,
                BalanceSheetId = 0,
                MerchandiseGuid = this.MerchandiseGuid,
                MerchandiseType = this.MerchandiseType,
                BalanceSheetType = this.BalanceSheetType,
                StoreGuid = null,
                Status = BalanceSheetGenerationResultStatus.PreconditionCheckFailed,
                FailureMessage = failureMessage
            };

        }
        #endregion


        #region .ctors

        protected SystemToHouseBsCreationTemplate()
        {
            InitializeProviders();
        }

        #endregion



    }
}


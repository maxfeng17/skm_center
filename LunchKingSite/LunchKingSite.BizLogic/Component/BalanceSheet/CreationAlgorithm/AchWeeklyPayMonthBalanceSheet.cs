﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class AchWeeklyPayMonthBalanceSheet : WeeklyPayMonthBalanceSheet
    {
        protected sealed override bool FinancialStaffPaymentSettingCheck()
        {
            return VendorBillingHelper.FinancialStaffPaymentMethod == RemittanceType.AchWeekly;
        }

        protected sealed override BalanceSheet PrepareBalanceSheet(Guid? storeGuid)
        {
            DateTime intervalStart = GetMinIntervalStartByMonth(storeGuid);
            DateTime intervalEnd = GetMaxIntervalEndByMonth(storeGuid);

            int year;
            int month;
            if (!TryGetYearAndMonthForBalanceSheet(out year, out month))
            {
                return null;
            }

            BalanceSheet bs = new BalanceSheet
            {
                ProductGuid = this.MerchandiseGuid,
                ProductType = (int)this.MerchandiseType,
                StoreGuid = storeGuid,
                GenerationFrequency = (int)BalanceSheetGenerationFrequency.MonthBalanceSheet,
                IsDetailFixed = true,
                IsConfirmedReadyToPay = false,
                IsReceiptReceived = false,
                IsManual = false,
                IntervalStart = intervalStart,
                IntervalEnd = intervalEnd,
                Year = year,
                Month = month,
                PayReportId = 0,    //週結的付款資料在週對帳單內
                CreateId = this.TriggerUser,
                CreateTime = this.GetGenerationTime(),
                BalanceSheetType = (int)this.BalanceSheetType,
                ProductId = this.MerchandiseId
            };

            return bs;
        }

        public AchWeeklyPayMonthBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId, string triggerUser, bool genOutOfVerificationTimeRange)
            : base(merchandiseType, merchandiseKey, balanceSheetType, merchandiseId, triggerUser, genOutOfVerificationTimeRange)
        {
        }
    }
}

﻿using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LunchKingSite.BizLogic.Component
{
    public class ManualTriggeredBalanceSheet : SystemBsCreationTemplate
    {
        private Guid? TriggeredStoreGuid { get; set; }
        
        protected sealed override bool NeedDeductionCheck
        {
            get
            {
                return false;
            }
        }

        protected override DateTime GetCashTrustLogModifyTimeThreshold()
        {
            return GetGenerationTime();
        }

        protected override bool FinancialStaffPaymentSettingCheck()
        {
            //meaningless check for this type of balance sheet
            return true;
        }

        protected override BalanceSheet PrepareBalanceSheet(Guid? storeGuid)
        {
            return new BalanceSheet
            {
                ProductGuid = this.MerchandiseGuid,
                ProductType = (int)this.MerchandiseType,
                StoreGuid = storeGuid,
                GenerationFrequency = (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet,
                IsDetailFixed = true,
                IsConfirmedReadyToPay = false,
                IsReceiptReceived = false,
                IsManual = true,
                Year = GetGenerationTime().Year,
                Month = GetGenerationTime().Month,
                IntervalStart = GetLatestBalanceSheet(storeGuid).IntervalEnd,
                IntervalEnd = GetGenerationTime(),
                CreateId = HttpContext.Current.User.Identity.Name,
                CreateTime = this.GetGenerationTime(),
                BalanceSheetType = (int)this.BalanceSheetType,
                ProductId = this.MerchandiseId
            };
        }

        protected override bool StabilizePreviousBalanceSheetContent(Guid? storeGuid)
        {
            BalanceSheet unfixedBS = ap.BalanceSheetGetDetailNotFixed(this.MerchandiseGuid, this.MerchandiseType, storeGuid);
            if (unfixedBS == null)
            {
                return true;
            }
            unfixedBS.IsDetailFixed = true;
            unfixedBS.ConfirmedTime = DateTime.Now;
            unfixedBS.ConfirmedUserName = "sys";
            ap.BalanceSheetSet(unfixedBS);
            return true;
        }

        protected override bool DuplicationCheck(BalanceSheet unpersistedBS)
        {
            //一天只能產一次
            
            if (unpersistedBS.IntervalStart == null || unpersistedBS.IntervalEnd == null)
            {
                return false;
            }

            BalanceSheetCollection allTriggered = ap.BalanceSheetGetListByProductGuidStoreGuid(unpersistedBS.ProductGuid, unpersistedBS.StoreGuid,
                                                         BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet);
            if(allTriggered.Count == 0)
            {
                return true;
            }
            else
            {
                var latestTriggered = allTriggered.OrderByDescending(x => x.IntervalEnd).First();
                return latestTriggered.IntervalEnd.Date != unpersistedBS.IntervalEnd.Date;
            }
        }

        protected override List<Guid?> GetBalanceSheetsStoreGuid()
        {
            if (balanceSheetStoreGuids != null)
            {
                return balanceSheetStoreGuids;
            }

            balanceSheetStoreGuids = new List<Guid?>();
            
            if(VendorBillingHelper.ParticipatingStores.Count() > 0)
            {
                if(this.TriggeredStoreGuid.HasValue
                    && VendorBillingHelper.ParticipatingStores.Contains(this.TriggeredStoreGuid.Value))
                {
                    balanceSheetStoreGuids.Add(this.TriggeredStoreGuid.Value);
                }
            }
            else
            {
                if(!this.TriggeredStoreGuid.HasValue)
                {
                    balanceSheetStoreGuids.Add(this.TriggeredStoreGuid);
                }
            }

            return balanceSheetStoreGuids;
        }

        /// <summary>
        /// 過了自動產對帳單的區間後, 才可以產手動觸發對帳單
        /// </summary>
        protected sealed override bool VerificationTimeRangeCheck(Guid? storeGuid)
        {
            return !base.VerificationTimeRangeCheck(storeGuid);
        }

        protected override bool IsLastBalanceSheet(Guid? storeGuid, VerificationPolicy verificationPolicy)
        {
            BalanceSheet latestBS = GetLatestBalanceSheet(storeGuid);
            if (latestBS == null
                || (!verificationPolicy.IsInVerifyTimeRange(this.MerchandiseType, this.MerchandiseGuid, storeGuid, latestBS.IntervalEnd, true)))
            {
                return false;
            }
            return true;
        }

        public ManualTriggeredBalanceSheet(BusinessModel bizModel, object merchandiseKey, Guid? storeGuid, int merchandiseId)
        {
            this.MerchandiseType = bizModel;
            this.MerchandiseKey = merchandiseKey;
            this.BalanceSheetType = BalanceSheetType.ManualTriggered;
            this.MerchandiseId = merchandiseId;
            this.TriggeredStoreGuid = storeGuid;
            TrySetMerchandiseGuid();
        }

        protected sealed override bool ItemsCheck(BalanceSheet newBS, IDictionary<Guid, int> payList, IEnumerable<BalanceSheetDetail> deductList)
        {
            if(int.Equals(0, payList.Count + deductList.Count()))
            {
                return false;
            }
            return true;
        }
    }
}

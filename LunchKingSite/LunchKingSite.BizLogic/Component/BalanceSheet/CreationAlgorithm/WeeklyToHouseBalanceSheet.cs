﻿using System;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class WeeklyToHouseBalanceSheet : SystemToHouseBsCreationTemplate
    {
        #region SystemFinancialBalanceSheet

        protected sealed override DateTime GetCashTrustLogModifyTimeThreshold()
        {
            DateTime genTime = GetGenerationTime().GetFirstDayOfWeek().AddDays(1);  // first day of week is sunday, we need monday through sunday
            DateTime result = new DateTime(
                genTime.Year,
                genTime.Month,
                genTime.Day
                );

            return result;
        }
        
        protected sealed override bool FinancialStaffPaymentSettingCheck()
        {
            return VendorBillingHelper.FinancialStaffPaymentMethod == RemittanceType.Weekly;
        }

        /// <summary>
        /// 產生一個新的對帳單
        /// </summary>
        /// <returns></returns>
        protected sealed override BalanceSheet PrepareBalanceSheet()
        {
            return new BalanceSheet
            {
                ProductGuid = this.MerchandiseGuid,
                ProductType = (int)this.MerchandiseType,
                StoreGuid = null,
                GenerationFrequency = (int)this.GetGenerationFrequency(),
                IsDetailFixed = true,
                IsConfirmedReadyToPay = false,
                IsReceiptReceived = false,
                IsManual = false,
                Year = GetCashTrustLogModifyTimeThreshold().Year,
                Month = GetCashTrustLogModifyTimeThreshold().Month,
                IntervalStart = GetCashTrustLogModifyTimeThreshold().AddDays(-7),
                IntervalEnd = GetCashTrustLogModifyTimeThreshold(),
                CreateId = "sys",
                CreateTime = this.GetGenerationTime(),
                BalanceSheetType = (int)this.BalanceSheetType,
                ProductId = this.MerchandiseId
            };
        }

        #endregion SystemFinancialBalanceSheet

        public WeeklyToHouseBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId)
        {
            this.MerchandiseType = merchandiseType;
            this.MerchandiseKey = merchandiseKey;
            this.BalanceSheetType = balanceSheetType;
            this.MerchandiseId = merchandiseId;
            TrySetMerchandiseGuid();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class AccountingWeeklyPayWeekBalanceSheet : WeeklyPayWeekBalanceSheet
    {
        protected sealed override bool FinancialStaffPaymentSettingCheck()
        {
            return VendorBillingHelper.FinancialStaffPaymentMethod == RemittanceType.ManualWeekly;
        }

        protected sealed override BalanceSheet PrepareBalanceSheet(Guid? storeGuid)
        {
            BalanceSheet bs = new BalanceSheet
            {
                ProductGuid = this.MerchandiseGuid,
                ProductType = (int)this.MerchandiseType,
                StoreGuid = storeGuid,
                GenerationFrequency = (int)BalanceSheetGenerationFrequency.WeekBalanceSheet,
                IsDetailFixed = true,
                IsConfirmedReadyToPay = false,
                ConfirmedUserName = null,
                IsReceiptReceived = false,
                IsManual = true,
                IntervalStart = GetCashTrustLogModifyTimeThreshold().AddDays(-7),
                IntervalEnd = GetCashTrustLogModifyTimeThreshold(),
                CreateId = "sys",
                CreateTime = this.GetGenerationTime(),
                BalanceSheetType = (int)this.BalanceSheetType,
                ProductId =  this.MerchandiseId
            };

            return bs;
        }

        public AccountingWeeklyPayWeekBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId)
            : base(merchandiseType, merchandiseKey, balanceSheetType, merchandiseId)
        {
        }
    }
}

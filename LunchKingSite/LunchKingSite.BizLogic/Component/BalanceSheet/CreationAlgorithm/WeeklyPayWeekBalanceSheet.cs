﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public abstract class WeeklyPayWeekBalanceSheet : SystemBsCreationTemplate
    {
        protected sealed override DateTime GetCashTrustLogModifyTimeThreshold()
        {
			DateTime genTime = GetGenerationTime().GetFirstDayOfWeek().AddDays(1);  // first day of week is sunday, we need monday through sunday
			DateTime result = new DateTime(
				genTime.Year,
				genTime.Month,
				genTime.Day
				);	//GetFirstDayOfWeek only changes the date information of GetGenerationTime(), not the time

        	return result;
        }
        
        protected sealed override bool StabilizePreviousBalanceSheetContent(Guid? storeGuid)
        {
            //週對帳單內容是固定的
            return true;
        }
        
        
        protected WeeklyPayWeekBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId)
        {
            this.MerchandiseType = merchandiseType;
            this.MerchandiseKey = merchandiseKey;
            this.BalanceSheetType = balanceSheetType;
            this.MerchandiseId = merchandiseId;
            TrySetMerchandiseGuid();
        }
    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;

namespace LunchKingSite.BizLogic.Component
{
    public class MonthlyToHouseBalanceSheet : SystemToHouseBsCreationTemplate
    {
        #region SystemFinancialBalanceSheet

        protected sealed override DateTime GetCashTrustLogModifyTimeThreshold()
        {
            DateTime genTime = GetGenerationTime().GetFirstDayOfMonth();
            DateTime result = new DateTime(
                genTime.Year,
                genTime.Month,
                genTime.Day
                );

            return result;
        }

        protected sealed override bool FinancialStaffPaymentSettingCheck()
        {
            return VendorBillingHelper.FinancialStaffPaymentMethod == RemittanceType.Monthly;
        }

        /// <summary>
        /// 產生一個新的對帳單
        /// </summary>
        /// <returns></returns>
        protected sealed override BalanceSheet PrepareBalanceSheet()
        {
            return new BalanceSheet
            {
                ProductGuid = this.MerchandiseGuid,
                ProductType = (int)this.MerchandiseType,
                StoreGuid = null,
                GenerationFrequency = (int)this.GetGenerationFrequency(),
                IsDetailFixed = true,
                IsConfirmedReadyToPay = false,
                IsReceiptReceived = false,
                IsManual = false,
                Year = GetCashTrustLogModifyTimeThreshold().AddMonths(-1).Year,
                Month = GetCashTrustLogModifyTimeThreshold().AddMonths(-1).Month,
                IntervalStart = GetCashTrustLogModifyTimeThreshold().AddMonths(-1),
                IntervalEnd = GetCashTrustLogModifyTimeThreshold(),
                CreateId = "sys",
                CreateTime = this.GetGenerationTime(),
                BalanceSheetType = (int)this.BalanceSheetType,
                ProductId = this.MerchandiseId
            };
        }

        #endregion SystemFinancialBalanceSheet

        public MonthlyToHouseBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId)
        {
            this.MerchandiseType = merchandiseType;
            this.MerchandiseKey = merchandiseKey;
            this.BalanceSheetType = balanceSheetType;
            this.MerchandiseId = merchandiseId;
            TrySetMerchandiseGuid();
        }
    }
}

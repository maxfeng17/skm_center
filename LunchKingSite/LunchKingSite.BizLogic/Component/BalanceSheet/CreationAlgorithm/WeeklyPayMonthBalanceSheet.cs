﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public abstract class WeeklyPayMonthBalanceSheet : SystemBsCreationTemplate
    {
        #region SystemFinancialBalanceSheet

        protected Dictionary<Guid, List<BalanceSheet>> WeekBalanceSheets { get; set; }
        protected override void GenerationSetup()
        {
            base.GenerationSetup();
            IsDetailsNeeded = false; //週結月對帳單是用來卡付款程序的, 不需要 balance_sheet_detail
            WeekBalanceSheetsSetup();
        }

        protected override bool PreExecutionCheck()
        {
			return base.PreExecutionCheck() && CountWeekBalanceSheets() > 0;
		}

        /// <summary>
        /// 非系統觸發產週結月對帳單 不需檢查核銷區間
        /// </summary>
        protected sealed override bool VerificationTimeRangeCheck(Guid? storeGuid)
        {
            if (!this.GenOutOfVerificationTimeRange)
            {
                return base.VerificationTimeRangeCheck(storeGuid);
            }
            
            return true;
        }

		private int CountWeekBalanceSheets()
		{
			// has to be counted separately because of conditions such as changing expire time ...

			int result = 0;
			
			foreach(KeyValuePair<Guid,List<BalanceSheet>> pair in WeekBalanceSheets)
			{
				result += pair.Value.Count();
			}

			return result;
		}

        private void WeekBalanceSheetsSetup()
        {
            if (!IsMerchandiseGuidSet)
            {
                WeekBalanceSheets = new Dictionary<Guid, List<BalanceSheet>>();
                return;
            }

            Dictionary<Guid, List<BalanceSheet>> balanceSheetsByStore = new Dictionary<Guid, List<BalanceSheet>>();
            BalanceSheetCollection allPreviousMonthWeekBalanceSheets = new BalanceSheetCollection();

            if (!this.GenOutOfVerificationTimeRange)
            {
                allPreviousMonthWeekBalanceSheets = ap.BalanceSheetGetWeekBalanceSheetsByMonth(this.MerchandiseGuid,
                                                            this.MerchandiseType,
                                                            GetGenerationTime().AddMonths(-1).Year,
                                                            GetGenerationTime().AddMonths(-1).Month);
            }
            else //補產檔款週結月對帳單
            {
                allPreviousMonthWeekBalanceSheets = ap.GetWeekBalanceSheetHasNoMonthBalanceSheet(this.MerchandiseGuid,
                                                            this.MerchandiseType,
                                                            GetBalanceSheetsStoreGuid(),
                                                            GetGenerationTime().AddMonths(-1).Year,
                                                            GetGenerationTime().AddMonths(-1).Month);
            }

            foreach (Guid? storeGuid in GetBalanceSheetsStoreGuid())
            {
                if (storeGuid == null)
                {
                    balanceSheetsByStore.Add(Guid.Empty, allPreviousMonthWeekBalanceSheets.ToList());
                }
                else
                {
                    List<BalanceSheet> currentStoreWeekBalanceSheets = allPreviousMonthWeekBalanceSheets
                        .Where(sheet => sheet.StoreGuid == storeGuid).ToList<BalanceSheet>();
                    balanceSheetsByStore.Add(storeGuid.Value, currentStoreWeekBalanceSheets);
                }
            }
            WeekBalanceSheets = balanceSheetsByStore;
        }

        //不應該用到這個方法 ( 週結月對帳單不需要分析 cash_trust_log )
        protected sealed override DateTime GetCashTrustLogModifyTimeThreshold()
        {
            throw new InvalidOperationException();
        }
        
        //只要還有週對帳單就要產.
		protected sealed override bool IsLastBalanceSheet(Guid? storeGuid, Model.VerificationPolicy verificationPolicy)
        {
            List<BalanceSheet> sheets = WeekBalanceSheets[storeGuid ?? Guid.Empty];
            if (sheets.Count > 0)
            {
                return true;
            }
            return false;
        }
        
        protected sealed override bool StabilizePreviousBalanceSheetContent(Guid? storeGuid)
        {
            //週結內容是固定的.
            return true;
        }
		
        protected sealed override VerificationStatisticsLog CalculateStatistics(CashTrustLogCollection allTrustData, Guid? storeGuid)
        {
            //統計資料 = 該月最後一份週對帳單時統計的資料

            Guid dictionaryStoreGuid = storeGuid.HasValue ? storeGuid.Value : Guid.Empty;

            //找不到該月份的週對帳單, 很可能是擋款造成的, 因此不需要產月對帳單
            if (WeekBalanceSheets[dictionaryStoreGuid].Count == 0)
            {
                return null;
            }

            int lastStatisticsId = WeekBalanceSheets[dictionaryStoreGuid]
                .Select(sheet => sheet.VerificationStatisticsLogId)
                .Max();

            VerificationStatisticsLog statisticsLog = pp.VerificationStatisticsLogGetById(lastStatisticsId);

            VerificationStatisticsLog newStats = new VerificationStatisticsLog();
            newStats.Unverified = statisticsLog.Unverified;
            newStats.Verified = statisticsLog.Verified;
            newStats.Returned = statisticsLog.Returned;
            newStats.ForcedReturned = statisticsLog.ForcedReturned;
            newStats.ForcedVerified = statisticsLog.ForcedVerified;
            newStats.Lost = 0;
            newStats.Adjustment = 0;
            newStats.Status = (int)VerificationStatisticsLogStatus.BalanceSheet;

            return newStats;
        }
        
		protected bool TryGetYearAndMonthForBalanceSheet(out int year, out int month)
		{
			// must make sure there is a week balance sheet, 
			// because conditions such as changing expire time might cause nothing generated

			foreach(KeyValuePair<Guid,List<BalanceSheet>> pair in WeekBalanceSheets)
			{
				if(pair.Value.Count() > 0)
				{
					BalanceSheet bs = pair.Value.FirstOrDefault();
					if (bs != null && bs.Year.HasValue && bs.Month.HasValue)
					{
						year = bs.Year.Value;
						month = bs.Month.Value;
						return true;
					}
				}
			}
			year = 1900;
			month = 1;
			return false;
		}
        protected DateTime GetMinIntervalStartByMonth(Guid? storeGuid)
        {
            Guid dictionaryStoreGuid = storeGuid.HasValue ? storeGuid.Value : Guid.Empty;
            List<BalanceSheet> weekBalanceSheets = WeekBalanceSheets[dictionaryStoreGuid];
            if (weekBalanceSheets.Count > 0)
            {
                return weekBalanceSheets.OrderBy(sheet => sheet.Id).First().IntervalStart;
            }
            return DateTime.MinValue;
        }
        protected DateTime GetMaxIntervalEndByMonth(Guid? storeGuid)
        {
            Guid dictionaryStoreGuid = storeGuid.HasValue ? storeGuid.Value : Guid.Empty;
            List<BalanceSheet> weekBalanceSheets = WeekBalanceSheets[dictionaryStoreGuid];
            if (weekBalanceSheets.Count > 0)
            {
                return weekBalanceSheets.OrderByDescending(sheet => sheet.Id).First().IntervalEnd;
            }
            return DateTime.MinValue;
        }

		protected sealed override bool DuplicationCheck(BalanceSheet unpersistedBS)
		{
			if (unpersistedBS.Year == null || unpersistedBS.Month == null)
            {
                return false;
            }

            int count = ap.BalanceSheetDuplicationCountCheck(unpersistedBS.ProductGuid,
												this.MerchandiseType,
												unpersistedBS.StoreGuid, unpersistedBS.Year.Value, unpersistedBS.Month.Value,
												this.BalanceSheetType);
			return count == 0;
		}

        #endregion

        protected WeeklyPayMonthBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId, string triggerUser, bool genOutOfVerificationTimeRange)
        {
            this.MerchandiseType = merchandiseType;
            this.MerchandiseKey = merchandiseKey;
            this.BalanceSheetType = balanceSheetType;
            this.MerchandiseId = merchandiseId;
            this.TriggerUser = triggerUser;
            this.GenOutOfVerificationTimeRange = genOutOfVerificationTimeRange;
            TrySetMerchandiseGuid();           
        }
    }
}

﻿using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Transactions;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// 系統對帳單. "系統" 指的是可以自動並有規則的產出對帳單的對帳單類型.
    /// </summary>
    public abstract class SystemProductBsCreationTemplate : BsCreationBase
    {
        #region props

        public object MerchandiseKey { get; set; }
        public string TriggerUser { get; set; }

        #endregion props

        #region protected props

        protected bool IsHelperSet { get; set; }
        protected bool IsDetailsNeeded { get; set; }
        protected bool IsMerchandiseGuidSet { get; set; }
        protected CashTrustLogCollection AllCashTrustLogs { get; set; }


        #endregion protected props

        /// <summary>
        /// 設定產對帳單的模擬時間.
        /// </summary>
        /// <param name="simulationTime"></param>
        public override void SetGenerationTime(DateTime simulationTime)
        {
            this.generationTime = simulationTime;
        }

        /// <summary>
        /// 產對帳單 ***template method for generating balance sheet***
        /// </summary>
        /// <returns></returns>
        public override ReadOnlyCollection<BalanceSheetGenerationResult> Generate()
        {
            GenerationSetup();

            if (!BalanceSheetTypeCheck() || !IsHelperSet || !PreExecutionCheck())
            {
                string failureMessage = string.Empty;

                if (!BalanceSheetTypeCheck())
                {
                    failureMessage = "balance sheet type check failed.";
                }
                if (!IsHelperSet)
                {
                    failureMessage = "VendorBillingHelper set failed.";
                }
                if (!PreExecutionCheck())
                {
                    failureMessage = "preexecution check failed.";
                }

                GenerationResults.Add(GetPreconditionFailResult(failureMessage));
                return GenerationResults.AsReadOnly();
            }

            DataSetup();

            try
            {
                if (!GenerationTimeRangeCheck())
                {
                    GenerationResults.Add(GetGenerationFailResult(BalanceSheetGenerationResultStatus.OutOfVerificationTimeRange));
                    return GenerationResults.AsReadOnly();
                }

                BalanceSheetGenerationResultStatus generationStatus;
                int balanceSheetId = GenerateBalanceSheet(out generationStatus);
                UpdateGenerationResults(generationStatus, balanceSheetId);
            }
            catch (Exception ex)
            {
                GenerationResults.Add(GetGenerationExceptionResult(ex));
                return GenerationResults.AsReadOnly();
            }

            if (GenerationResults.Any())
            {
                //寫入對帳單產出結果紀錄
                foreach (var result in GenerationResults)
                {
                    var resultDesc = string.Format("對帳單產出結果:{0}-{1} 對帳單類型:{2} 對帳單Id:{3}",
                        result.IsSuccess ? "成功" : "失敗", result.Status, result.BalanceSheetType, result.BalanceSheetId);
                    CommonFacade.AddAudit(result.MerchandiseGuid, AuditType.DealAccounting, resultDesc, "sys", true);
                }
            }

            return GenerationResults.AsReadOnly();
        }

        /// <summary>
        /// 商品對帳單是否符合產生對帳單區間的檢查
        /// </summary>
        protected virtual bool GenerationTimeRangeCheck()
        {
            return true;
        }
        
        #region protected members

        #region abstract members

        /// <summary>
        /// cash_trust_log 的 modify time 臨界值 (對於該對帳單有效的 cash_trust_log)
        /// </summary>
        /// <returns></returns>
        protected abstract DateTime GetCashTrustLogModifyTimeThreshold();

        /// <summary>
        /// 財會的匯款方式檢查
        /// </summary>
        protected abstract bool FinancialStaffPaymentSettingCheck();

        /// <summary>
        /// 特殊條件檢查
        /// </summary>
        protected abstract bool BalanceSheetTypeSpecificCheck();

        /// <summary>
        /// 把要產的對帳單資料準備好.
        /// </summary>
        /// <returns></returns>
        protected abstract BalanceSheet PrepareBalanceSheet();

        /// <summary>
        /// 取得對帳單區間起
        /// </summary>
        /// <returns></returns>
        protected abstract DateTime GetBalanceSheetIntervalStart();

        /// <summary>
        /// 對帳單區間迄
        /// </summary>
        /// <returns></returns>
        protected abstract DateTime GetBalanceSheetIntervalEnd();

        #endregion


        #region overridable

        protected virtual void GenerationSetup()
        {
            IsMerchandiseGuidSet = TrySetMerchandiseGuid();
            IsHelperSet = TrySetBillingHelper();
            IsDetailsNeeded = true;
            GenerationResults = new List<BalanceSheetGenerationResult>();
        }

        protected virtual bool PreExecutionCheck()
        {
            if (!GetDeliverStartTime().HasValue ||
                !GetDeliverEndTime().HasValue)
            {
                return false;
            }

            if (BillingSystemCheck() &&
                FinancialStaffPaymentSettingCheck() && 
                PponSpecificCheck() &&
                BalanceSheetTypeSpecificCheck())
            {
                return true;
            }

            return false;
        }

        protected virtual void DataSetup()
        {
            var cashTrustLogs = VendorBillingHelper.GetAllSuccessfulOrderTrustList();
            //抓取之trust_id 須將 未退款完成但廠商已標註退款完成 退貨單 之 refund trust_id 視為已退貨
            cashTrustLogs = BalanceSheetManager.GetTrustListWithReturnningToReturned(cashTrustLogs, true);

            AllCashTrustLogs = cashTrustLogs;
        }
        
        protected DateTime? generationTime;
        protected DateTime GetGenerationTime()
        {
            if (generationTime == null)
            {
                generationTime = DateTime.Now;
            }

            return generationTime.Value;
        }

        protected virtual VerificationStatisticsLog CalculateStatistics(CashTrustLogCollection allTrustData)
        {
            IEnumerable<CashTrustLog> frieghtlessData = allTrustData.ToList();
            IEnumerable<CashTrustLog> thresholdTimeData = frieghtlessData.Where(log => log.ModifyTime <= this.GetCashTrustLogModifyTimeThreshold()).ToList();

            var stats = new VerificationStatisticsLog
            {
                Unverified = thresholdTimeData.Count(x => x.Status == (int)TrustStatus.Initial || x.Status == (int)TrustStatus.Trusted),
                Verified = thresholdTimeData.Count(x => x.Status == (int)TrustStatus.Verified),
                Returned = thresholdTimeData.Count(x => x.Status == (int)TrustStatus.Returned || x.Status == (int)TrustStatus.Refunded),
                Adjustment = 0,
                Status = (int)VerificationStatisticsLogStatus.BalanceSheet
            };

            return stats;
        }

        protected virtual bool DuplicationCheck(BalanceSheet unpersistedBS)
        {
            //暫付檔次 一檔只產一張對帳單
            int count = ap.BalanceSheetGetListByProductGuid(unpersistedBS.ProductGuid)
                            .Count(x => x.GenerationFrequency == unpersistedBS.GenerationFrequency);
            return count == 0;
        }

        protected virtual BalanceSheet FinalizeBalanceSheet(BalanceSheet newBS, IDictionary<Guid, int> payList)
        {
            return newBS;
        }

        protected virtual bool ItemsCheck(BalanceSheet newBS, IDictionary<Guid, int> payList)
        {
            return true;
        }

        #endregion


        #region non-overridable

        protected IAccountingProvider ap;
        protected IHiDealProvider hp;
        protected IMemberProvider mp;
        protected IOrderProvider op;
        protected IPponProvider pp;
        protected void InitializeProviders()
        {
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        protected bool TrySetMerchandiseGuid()
        {
            if (MerchandiseKey == null)
            {
                return false;
            }

            int key;
            if (int.TryParse(MerchandiseKey.ToString(), out key) && MerchandiseType == BusinessModel.PiinLife)
            {
                HiDealProduct product = hp.HiDealProductGet(key);
                if (product != null && product.IsLoaded)
                {
                    hiDealProductId = product.Id;
                    this.MerchandiseGuid = product.Guid;
                    return true;
                }
            }

            Guid guidKey;
            if (Guid.TryParse(MerchandiseKey.ToString(), out guidKey))
            {
                this.MerchandiseGuid = guidKey;
                return true;
            }

            return false;
        }

        /// <summary>
        /// 檢查是否用對帳單系統的對帳方式
        /// </summary>
        /// <returns></returns>
        protected bool BillingSystemCheck()
        {
            return VendorBillingHelper.VendorBillingModel == VendorBillingModel.BalanceSheetSystem;
        }

        protected bool PponSpecificCheck()
        {
            if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return true;
            }

            //多檔次主檔不可產對帳單
            var result = pp.GetViewComboDealByBid(this.MerchandiseGuid);
            return result.Count() == 0;
        }

        protected BalanceSheetGenerationFrequency GetGenerationFrequency()
        {
            switch (this.BalanceSheetType)
            {
                case BalanceSheetType.ManualLumpSumPayBalanceSheet:
                    return BalanceSheetGenerationFrequency.LumpSumBalanceSheet;
                default:
                    return BalanceSheetGenerationFrequency.Unknown;
            }
        }

        protected DateTime? GetDeliverStartTime()
        {
            if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return null;
            }

            return pp.BusinessHourGet(this.MerchandiseGuid).BusinessHourDeliverTimeS;
        }

        protected DateTime? GetDeliverEndTime()
        {
            if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return null;
            }

            return pp.BusinessHourGet(this.MerchandiseGuid).BusinessHourDeliverTimeE;
        }

        protected DateTime? GetFinalBalanceSheetDate()
        {
            if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return null;
            }

            return pp.DealAccountingGet(this.MerchandiseGuid).FinalBalanceSheetDate;
        }

        protected DateTime? GetBalanceSheetCreateTime()
        {
            if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return null;
            }

            return pp.DealAccountingGet(this.MerchandiseGuid).BalanceSheetCreateDate;
        }

        protected bool HasVendorPaymentChange()
        {
            if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return false;
            }

            return ap.VendorPaymentChangeGetList(this.MerchandiseGuid).Any();
        }

        protected bool IsDealEstablished()
        {
            if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return false;
            }
            int tempSlug;
            var deal = pp.ViewPponDealGetByBusinessHourGuid(this.MerchandiseGuid);

            return deal != null &&
                   int.TryParse(deal.Slug, out tempSlug) &&
                   tempSlug >= deal.BusinessHourOrderMinimum;
        }

        #endregion


        #endregion

        #region private members

        private bool BalanceSheetTypeCheck()
        {
            return this.MerchandiseType == BusinessModel.Ppon && 
                   this.BalanceSheetType == BalanceSheetType.ManualLumpSumPayBalanceSheet;
        }

        private List<BalanceSheetGenerationResult> GenerationResults { get; set; }

        private int hiDealProductId = 0;
        private bool TrySetBillingHelper()
        {
            if (this.MerchandiseType == BusinessModel.Ppon)
            {
                this.VendorBillingHelper = new PponVendorBillingHelper(this.MerchandiseGuid);
                return true;
            }

            if (this.MerchandiseType == BusinessModel.PiinLife)
            {
                this.VendorBillingHelper = new HiDealVendorBillingHelper(hiDealProductId);
                return true;
            }

            return false;
        }
        
        private int GenerateBalanceSheet(out BalanceSheetGenerationResultStatus status)
        {
            BalanceSheet newBS = PrepareBalanceSheet();
            if (newBS == null)
            {
                status = BalanceSheetGenerationResultStatus.BalanceSheetDataError;
                return 0;
            }
            if (!DuplicationCheck(newBS))
            {
                status = BalanceSheetGenerationResultStatus.Duplication;
                return 0;
            }

            IDictionary<Guid, int> payList;	//Guid: [trust_id], int: [cash_trust_status_log].[id]
            if (IsDetailsNeeded)
            {
                payList = GetPayListForNewBalanceSheet(AllCashTrustLogs);
            }
            else
            {
                payList = new Dictionary<Guid, int>();
            }

            if (!ItemsCheck(newBS, payList))
            {
                status = BalanceSheetGenerationResultStatus.ItemsCheckFailed;
                return 0;
            }

            BalanceSheet finalizedNewBS = FinalizeBalanceSheet(newBS, payList);

            VerificationStatisticsLog currentStatics = CalculateStatistics(AllCashTrustLogs);

            if (currentStatics == null)
            {
                status = BalanceSheetGenerationResultStatus.StopPayment;
                return 0;
            }

            int id = PersistNewBalanceSheet(payList, currentStatics, finalizedNewBS);
            status = BalanceSheetGenerationResultStatus.Success;
            return id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>dictionary key: trust_id; dictionary value: [cash_trust_status_log].[id]</returns>
        private IDictionary<Guid, int> GetPayListForNewBalanceSheet(CashTrustLogCollection allCashTrustLogs)
        {
            IEnumerable<Guid> payList = GetAllVerifiedTrustList(allCashTrustLogs).ToList();
            CashTrustStatusLogCollection statusLogs = mp.CashTrustStatusLogGetList(payList);

            Dictionary<Guid, int> results = new Dictionary<Guid, int>();
            foreach (Guid trustId in payList)
            {
                Guid trustGuid = trustId;
                CashTrustStatusLog latestLog =
                            statusLogs
                            .Where(log =>
                                log.TrustId == trustGuid
                                && log.Status == (int)TrustStatus.Verified
                                && log.ModifyTime.AddSeconds(-2) < GetCashTrustLogModifyTimeThreshold())  //改 cash_trust_log 時, 歷程資料可能沒有填 cash_trust_log 的時間 => 兩個都填DateTime.Now 造成的時間差問題
                            .OrderByDescending(log => log.ModifyTime).FirstOrDefault();

                if (latestLog != null)
                {
                    results.Add(trustId, latestLog.Id);
                }
                else
                {
                    //cash_trust_status_log must be missing information
                    results.Add(trustId, 0);
                }
            }

            return results;
        }

        private IEnumerable<Guid> GetAllVerifiedTrustList(CashTrustLogCollection allTrustList)
        {
            if (allTrustList != null)
            {
                IEnumerable<CashTrustLog> resultLogs =
                allTrustList
                    .Where(log => log.ModifyTime <= GetCashTrustLogModifyTimeThreshold() && 
                                  log.Status == (int)TrustStatus.Verified);
                IEnumerable<Guid> results = resultLogs
                    .Select(pponCtLog => pponCtLog.TrustId);

                return results;
            }

            return new List<Guid>();
        }

        private VendorPaymentChangeCollection GetVendorPaymentChangeList()
        {
            if (this.BalanceSheetType != BalanceSheetType.ManualLumpSumPayBalanceSheet)
            {
                return new VendorPaymentChangeCollection();
            }

            return ap.VendorPaymentChangeGetList(this.MerchandiseGuid);
        }

        private int PersistNewBalanceSheet(
            IDictionary<Guid, int> payList,
            VerificationStatisticsLog unpersistedStatistics,
            BalanceSheet newUnpersistedBS)
        {
            int balanceSheetId;

            using (TransactionScope txScope = new TransactionScope())
            {
                #region persist statistics

                int statId = pp.VerificationStatisticsLogInsert(unpersistedStatistics, this.MerchandiseGuid, "sys");
                newUnpersistedBS.VerificationStatisticsLogId = statId;

                #endregion

                #region persist balance sheet

                balanceSheetId = ap.BalanceSheetSet(newUnpersistedBS);

                #endregion

                #region persist balance sheet details

                BalanceSheetDetailCollection details = new BalanceSheetDetailCollection();

                foreach (KeyValuePair<Guid, int> trust in payList)
                {
                    var detail = new BalanceSheetDetail
                    {
                        BalanceSheetId = balanceSheetId,
                        TrustId = trust.Key,
                        Status = (int) BalanceSheetDetailStatus.Normal,
                        CashTrustStatusLogId = trust.Value
                    };
                    details.Add(detail);
                }
                
                if (details.Any())
                {
                    ap.BalanceSheetDetailSetList(details);
                }

                #endregion

                #region persist vendor_payment_change

                var paymentChangeList = GetVendorPaymentChangeList();

                if (paymentChangeList.Any())
                { 
                    foreach(var paymentChange in paymentChangeList)
                    {
                        paymentChange.BalanceSheetId = balanceSheetId;
                    }

                    ap.VendorPaymentChangeSetList(paymentChangeList);
                }

                #endregion persist vendor_payment_change


                var bsModel = new BalanceSheetModel(balanceSheetId);

                #region 對帳單應付金額

                int amount = (int)bsModel.GetAccountsPayable().TotalAmount;

                newUnpersistedBS.EstAmount = amount;

                #endregion 對帳單應付金額

                #region 0 元對帳單 & 負對帳單的確認

                /*
                * 0元 和 金額為負的對帳單不需要管理單據, 
                * 所以要直接確認掉, 跳過營管部門這道關卡, 並進入財會部門的程序.
                * 0元不需要付錢 => pay_report_id = 0
                * 負的需要收錢 => pay_report_id = null, 等收到錢後會填入對應的 id.
                */

                if (amount <= 0)
                {
                    if (amount == 0)
                    {
                        newUnpersistedBS.PayReportId = 0;
                    }

                    newUnpersistedBS.IsConfirmedReadyToPay = true;
                    newUnpersistedBS.ConfirmedUserName = "sys";
                    newUnpersistedBS.ConfirmedTime = GetGenerationTime();
                    newUnpersistedBS.IsReceiptReceived = true;
                    newUnpersistedBS.IsDetailFixed = true;
                }

                #endregion

                ap.BalanceSheetSet(newUnpersistedBS);

                txScope.Complete();
            }

            return balanceSheetId;
        }

        private void SetBalanceSheetCreateTime()
        {
            //更新對帳單開立日(系統產生對帳單只寫日期 不須時間 以利和人工更新區別)
            DealAccounting da = pp.DealAccountingGet(this.MerchandiseGuid);
            da.BalanceSheetCreateDate = GetGenerationTime().Date;
            pp.DealAccountingSet(da);
        }
        
        private void UpdateGenerationResults(BalanceSheetGenerationResultStatus generationStatus, int balanceSheetId)
        {
            if (generationStatus == BalanceSheetGenerationResultStatus.Success)
            {
                if (this.BalanceSheetType == BalanceSheetType.ManualLumpSumPayBalanceSheet)
                {
                    SetBalanceSheetCreateTime();
                }

                GenerationResults.Add(GetGenerationSuccessResult(balanceSheetId));
            }
            else
            {
                GenerationResults.Add(GetGenerationFailResult(generationStatus));
            }
        }

        private BalanceSheetGenerationResult GetGenerationFailResult(BalanceSheetGenerationResultStatus generationStatus)
        {
            return new BalanceSheetGenerationResult
            {
                IsSuccess = false,
                BalanceSheetId = 0,
                MerchandiseGuid = this.MerchandiseGuid,
                MerchandiseType = this.MerchandiseType,
                BalanceSheetType = this.BalanceSheetType,
                StoreGuid = null,
                Status = generationStatus,
                FailureMessage = string.Empty
            };
        }

        private BalanceSheetGenerationResult GetGenerationSuccessResult(int balanceSheetId)
        {
            return new BalanceSheetGenerationResult
            {
                IsSuccess = true,
                BalanceSheetId = balanceSheetId,
                MerchandiseGuid = this.MerchandiseGuid,
                MerchandiseType = this.MerchandiseType,
                BalanceSheetType = this.BalanceSheetType,
                StoreGuid = null,
                Status = BalanceSheetGenerationResultStatus.Success,
                FailureMessage = string.Empty
            };
        }

        private BalanceSheetGenerationResult GetGenerationExceptionResult(Exception exception)
        {
            return new BalanceSheetGenerationResult
            {
                IsSuccess = false,
                BalanceSheetId = 0,
                MerchandiseGuid = this.MerchandiseGuid,
                MerchandiseType = this.MerchandiseType,
                BalanceSheetType = this.BalanceSheetType,
                StoreGuid = null,
                Status = BalanceSheetGenerationResultStatus.Exception,
                FailureMessage = string.Format("message: {0} \r\n trace: {1}", exception.Message, exception.StackTrace)
            };
        }

        private BalanceSheetGenerationResult GetPreconditionFailResult(string failureMessage)
        {
            return new BalanceSheetGenerationResult
            {
                IsSuccess = false,
                BalanceSheetId = 0,
                MerchandiseGuid = this.MerchandiseGuid,
                MerchandiseType = this.MerchandiseType,
                BalanceSheetType = this.BalanceSheetType,
                StoreGuid = null,
                Status = BalanceSheetGenerationResultStatus.PreconditionCheckFailed,
                FailureMessage = failureMessage
            };

        }
        #endregion


        #region .ctors

        protected SystemProductBsCreationTemplate()
        {
            InitializeProviders();
        }

        #endregion



    }
}

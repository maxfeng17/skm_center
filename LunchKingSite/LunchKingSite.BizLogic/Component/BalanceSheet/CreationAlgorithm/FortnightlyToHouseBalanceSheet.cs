﻿using System;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class FortnightlyToHouseBalanceSheet : SystemToHouseBsCreationTemplate
    {
        #region SystemFinancialBalanceSheet

        protected sealed override DateTime GetCashTrustLogModifyTimeThreshold()
        {
            DateTime genTime = GetGenerationTime();
            DateTime result = new DateTime();
            if (genTime.Day == 1)
            {
                //上個月16號~上個月底
                genTime = genTime.GetFirstDayOfMonth();
                result = new DateTime(
                genTime.Year,
                genTime.Month,
                genTime.Day
                );

            }
            else if (genTime.Day == 16)
            {
                //本月1號~本月15號
                genTime = genTime.GetFirstDayOfMonth().AddDays(15);
                result = new DateTime(
                genTime.Year,
                genTime.Month,
                genTime.Day
                );

                
            }

            return result;

        }
        
        protected sealed override bool FinancialStaffPaymentSettingCheck()
        {
            return VendorBillingHelper.FinancialStaffPaymentMethod == RemittanceType.Fortnightly;
        }

        /// <summary>
        /// 產生一個新的對帳單
        /// </summary>
        /// <returns></returns>
        protected sealed override BalanceSheet PrepareBalanceSheet()
        {
            return new BalanceSheet
            {
                ProductGuid = this.MerchandiseGuid,
                ProductType = (int)this.MerchandiseType,
                StoreGuid = null,
                GenerationFrequency = (int)this.GetGenerationFrequency(),
                IsDetailFixed = true,
                IsConfirmedReadyToPay = false,
                IsReceiptReceived = false,
                IsManual = false,
                Year = GetCashTrustLogModifyTimeThreshold().Day == 1 ? GetCashTrustLogModifyTimeThreshold().AddMonths(-1).Year : GetCashTrustLogModifyTimeThreshold().Year,
                Month = GetCashTrustLogModifyTimeThreshold().Day == 1 ? GetCashTrustLogModifyTimeThreshold().AddMonths(-1).Month : GetCashTrustLogModifyTimeThreshold().Month,
                IntervalStart = GetCashTrustLogModifyTimeThreshold().Day == 1 ? GetCashTrustLogModifyTimeThreshold().AddMonths(-1).AddDays(15) : GetCashTrustLogModifyTimeThreshold().GetFirstDayOfMonth(),
                IntervalEnd = GetCashTrustLogModifyTimeThreshold(),
                CreateId = "sys",
                CreateTime = this.GetGenerationTime(),
                BalanceSheetType = (int)this.BalanceSheetType,
                ProductId = this.MerchandiseId
            };
        }

        #endregion SystemFinancialBalanceSheet

        public FortnightlyToHouseBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, BalanceSheetType balanceSheetType, int merchandiseId)
        {
            this.MerchandiseType = merchandiseType;
            this.MerchandiseKey = merchandiseKey;
            this.BalanceSheetType = balanceSheetType;
            this.MerchandiseId = merchandiseId;
            TrySetMerchandiseGuid();
        }
    }
}

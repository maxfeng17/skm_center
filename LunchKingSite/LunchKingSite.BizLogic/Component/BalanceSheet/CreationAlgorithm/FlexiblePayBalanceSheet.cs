﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class FlexiblePayBalanceSheet : SystemBsCreationTemplate
    {
        private Guid? TriggeredStoreGuid { get; set; }
        
        private DateTime? GetDeliverStartTime()
        {
            if (this.MerchandiseType != BusinessModel.Ppon)
            {
                return null;
            }

            return pp.BusinessHourGet(this.MerchandiseGuid).BusinessHourDeliverTimeS;
        }

        protected sealed override DateTime GetCashTrustLogModifyTimeThreshold()
        {
            var genTime = GetGenerationTime();
            var result = new DateTime(
                genTime.Year,
                genTime.Month,
                genTime.Day
                );

            return result;
        }

        protected sealed override bool FinancialStaffPaymentSettingCheck()
        {
            return VendorBillingHelper.FinancialStaffPaymentMethod == RemittanceType.Flexible;
        }

        protected override void DataSetup()
        {
            AllCashTrustLogs = VendorBillingHelper.GetAllSuccessfulOrderTrustList(GetGenerationTime());
            var storeGuids = new List<Guid?>();
            foreach (var storeGuid in VendorBillingHelper.ParticipatingStores)
            {
                storeGuids.Add(storeGuid);
            }
            AllOldBalanceSheetDetails = GetOldBalanceSheetDetails(storeGuids);
        }

        protected override List<Guid?> GetBalanceSheetsStoreGuid()
        {
            if (balanceSheetStoreGuids != null)
            {
                return balanceSheetStoreGuids;
            }

            balanceSheetStoreGuids = new List<Guid?>();

            if (VendorBillingHelper.ParticipatingStores.Any())
            {
                if (this.TriggeredStoreGuid.HasValue
                    && VendorBillingHelper.ParticipatingStores.Contains(this.TriggeredStoreGuid.Value))
                {
                    balanceSheetStoreGuids.Add(this.TriggeredStoreGuid.Value);
                }
                if (!this.TriggeredStoreGuid.HasValue)
                {
                    foreach (var storeGuid in VendorBillingHelper.ParticipatingStores)
                    {
                        balanceSheetStoreGuids.Add(storeGuid);
                    }
                }
            }
            else
            {
                if (!this.TriggeredStoreGuid.HasValue)
                {
                    balanceSheetStoreGuids.Add(this.TriggeredStoreGuid);
                }
            }

            return balanceSheetStoreGuids;
        }

        /// <summary>
        /// 彈性請款對帳單 過對帳區間仍可產
        /// </summary>
        protected sealed override bool VerificationTimeRangeCheck(Guid? storeGuid)
        {
            return true;
        }

        protected sealed override bool StabilizePreviousBalanceSheetContent(Guid? storeGuid)
        {
            BalanceSheet unfixedBS = ap.BalanceSheetGetDetailNotFixed(this.MerchandiseGuid, this.MerchandiseType, storeGuid);
            if (unfixedBS == null)
            {
                return true;
            }
            unfixedBS.IsDetailFixed = true;
            ap.BalanceSheetSet(unfixedBS);
            return true;
        }

        protected sealed override BalanceSheet PrepareBalanceSheet(Guid? storeGuid)
        {
            var lastBs = GetLatestBalanceSheet(storeGuid);
            var bs = new BalanceSheet
            {
                ProductGuid = this.MerchandiseGuid,
                ProductType = (int)this.MerchandiseType,
                StoreGuid = storeGuid,
                GenerationFrequency = (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet,
                IsDetailFixed = true,
                IsConfirmedReadyToPay = false,
                ConfirmedUserName = null,
                IsReceiptReceived = false,
                IsManual = false,
                Year = GetCashTrustLogModifyTimeThreshold().Year,
                Month = GetCashTrustLogModifyTimeThreshold().Month,
                IntervalStart = GetDeliverStartTime().Value,
                IntervalEnd = GetCashTrustLogModifyTimeThreshold(),
                CreateId = this.TriggerUser,
                CreateTime = DateTime.Now,
                BalanceSheetType = (int)this.BalanceSheetType,
                ProductId = this.MerchandiseId
            };

            return bs;
        }

        internal FlexiblePayBalanceSheet(BusinessModel merchandiseType, object merchandiseKey, Guid? storeGuid, BalanceSheetType balanceSheetType, int merchandiseId, string triggerUser)
        {
            this.MerchandiseType = merchandiseType;
            this.MerchandiseKey = merchandiseKey;
            this.TriggeredStoreGuid = storeGuid;
            this.BalanceSheetType = balanceSheetType;
            this.MerchandiseId = merchandiseId;
            this.TriggerUser = triggerUser;
            TrySetMerchandiseGuid();
        }
    }
}

﻿using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    public class BalanceSheetManager
    {
        private static IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        /// <summary>
        /// 計算是否還在對帳區間內
        /// </summary>
        /// <param name="productGuid"></param>
        /// <param name="genTime"></param>
        /// <param name="checkNextGenTime"></param>
        /// <returns></returns>
        public static bool IsInBalanceTimeRange(Guid productGuid, DateTime genTime, bool checkNextGenTime = false)
        {
            var deal = pp.ViewPponDealGetByBusinessHourGuid(productGuid);
            if (deal == null)
            {
                return false;
            }
            if (deal.DeliveryType == (int) DeliveryType.ToShop)
            {
                return false;
            }
            var deliveryStartDate = deal.BusinessHourDeliverTimeS;
            if (!deliveryStartDate.HasValue)
            {
                return false;
            }

            var accountInfo = pp.DealAccountingGet(productGuid);
            if (accountInfo == null)
            {
                return false;
            }

            if (accountInfo.VendorBillingModel != (int)VendorBillingModel.BalanceSheetSystem)
            {
                return false;
            }

            //抓取下次產出對帳單時間
            var nextGenTime = genTime;
            if (checkNextGenTime)
            {
                switch (accountInfo.RemittanceType)
                {
                    case (int)RemittanceType.Weekly:
                        nextGenTime = genTime.AddDays(7);
                        break;

                    case (int)RemittanceType.Monthly:
                    case (int)RemittanceType.Flexible:
                        nextGenTime = genTime.AddMonths(1).GetFirstDayOfMonth();
                        break;
                    case (int)RemittanceType.Fortnightly:
                        if (genTime.Day == 1)
                        {
                            nextGenTime = genTime.AddDays(15);
                        }
                        else //16
                        {
                            nextGenTime = genTime.AddMonths(1).GetFirstDayOfMonth();
                        }

                        break;

                    //非先收單據後付款檔次 無需考慮對帳區間
                    case (int)RemittanceType.ManualPartially:
                    case (int)RemittanceType.Others:
                    default:
                        return false;
                }
            }       

            var expirationDate = accountInfo.FinalBalanceSheetDate;
            if (expirationDate.HasValue)
            {
                //20160513 先前產出對帳單之對帳區間只以30天計算，未多保留緩衝天數，導致部分彈性請款檔次之對帳單未產出最後一張對帳單 導致檢查對帳狀態有誤 
                //因這些檔次大多已無對帳明細可產 故不打算由系統補產最後一張對帳單來解決問題 
                //而以調整完成的進版日期為基準 調整檢查區間規則
                int endBuffer = CalculateEndBuffer((RemittanceType)accountInfo.RemittanceType, genTime);
                if (accountInfo.RemittanceType == (int)RemittanceType.Flexible)
                {
                    endBuffer = DateTime.Compare(genTime, new DateTime(2016, 5, 16, 0, 0, 0)) > 0
                                ? endBuffer
                                : 0;
                }
                else
                {
                    endBuffer = DateTime.Compare(genTime, new DateTime(2016, 3, 8, 0, 0, 0)) > 0
                                ? endBuffer
                                : 0;
                }
                //20160920增加檢查下個月產出對帳單時間 也須符合對帳區間(避免這個月產出時間未過對帳區間 但下個月產出時間已過對帳區間 因不會產出下期對帳單而導致無法顯示對帳完成狀態問題)
                //觀察一陣子 若確認無誤 可同步改至憑證對帳區間規則
                //Todo:想取消加上除對帳緩衝區間以外另加緩衝區間的規則 考慮是否以延長壓記退換貨完成日規則 來解決目前過退換貨完成日+對帳區間後後仍有退貨數須出帳問題
                return genTime < expirationDate.Value.AddDays(config.GenToHouseBalanceSheetBuffer + endBuffer)
                    && nextGenTime < expirationDate.Value.AddDays(config.GenToHouseBalanceSheetBuffer + endBuffer)
                    && genTime > deliveryStartDate.Value;
            }

            return genTime > deliveryStartDate.Value;
        }


        /// <summary>
        /// 取得已過鑑賞期之訂單
        /// </summary>
        /// <param name="productGuid"></param>
        /// <param name="getDate"></param>
        /// <param name="orderClassification"></param>
        /// <returns></returns>
        public static IEnumerable<Guid> GetOverTrialPeriodOrderGuid(Guid productGuid, DateTime getDate,
            OrderClassification orderClassification = OrderClassification.LkSite)
        {
            //非超取訂單(出貨日)
            var orderList = op.ViewOrderShipListGetListByProductGuid(productGuid, orderClassification)
            .Where(x => x.ShipTime.HasValue &&
                        DateTime.Compare(x.ShipTime.Value.AddDays(config.ShippingBuffer).Date, getDate) < 0 &&
                        x.ProductDeliveryType == (int)ProductDeliveryType.Normal).Select(x => x.OrderGuid);

            //全家超取訂單(取貨日+10)
            var ispFamilyOrderList = op.ViewOrderShipListGetListByProductGuid(productGuid, orderClassification)
            .Where(x => x.ReceiptTime.HasValue &&
                        DateTime.Compare(x.ReceiptTime.Value.AddDays(config.ShippingBuffer).Date, getDate) < 0 &&
                        (x.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)).Select(x => x.OrderGuid);

            //7-11超取訂單(取貨日+15)
            var ispSevenOrderList = op.ViewOrderShipListGetListByProductGuid(productGuid, orderClassification)
            .Where(x => x.ReceiptTime.HasValue &&
                        DateTime.Compare(x.ReceiptTime.Value.AddDays(config.SevenShippingBuffer).Date, getDate) < 0 &&
                        (x.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)).Select(x => x.OrderGuid);

            //PCHOME(取貨日+10)
            var wmsOrderList = op.ViewOrderShipListGetListByProductGuid(productGuid, orderClassification)
            .Where(x => x.ReceiptTime.HasValue &&
                        DateTime.Compare(x.ReceiptTime.Value.AddDays(config.ShippingBuffer).Date, getDate) < 0 &&
                        (x.ProductDeliveryType == (int)ProductDeliveryType.Wms)).Select(x => x.OrderGuid);


            //一般+全家+7-11+pchome
            orderList = orderList.Union(ispFamilyOrderList).Union(ispSevenOrderList).Union(wmsOrderList);

            
            


            //換貨的訂單已客服確認時間為主
            var newList = new List<Guid>();
            foreach (var oguid in orderList)
            {
                var orlc = op.OrderReturnListGetListByType(oguid, (int)OrderReturnType.Exchange);
                if (orlc.Count > 0)
                {
                    int deliveryType = op.OrderProductDeliveryGetByOrderGuid(oguid).ProductDeliveryType;
                    if (orlc.Any(x => x.Status == (int)OrderReturnStatus.ExchangeProcessing))
                    {
                        continue;
                    }
                    if (orlc.Any(x => x.Status == (int)OrderReturnStatus.ExchangeSuccess && 
                                ((deliveryType == (int)ProductDeliveryType.FamilyPickup && x.ModifyTime.Value.AddDays(config.ShippingBuffer).Date > getDate.Date) 
                                || (deliveryType == (int)ProductDeliveryType.SevenPickup && x.ModifyTime.Value.AddDays(config.SevenShippingBuffer).Date > getDate.Date))))
                    {
                        continue;
                    }
                }
                newList.Add(oguid);
            }

            return newList;
        }

        /// <summary>
        /// 將退貨中先視為已退貨
        /// </summary>
        /// <param name="cashTrustLogs"></param>
        /// <param name="vendorProgressComplete"></param>
        /// <returns></returns>
        public static CashTrustLogCollection GetTrustListWithReturnningToReturned(CashTrustLogCollection cashTrustLogs, bool vendorProgressComplete)
        {
            var shippedOrderGuids = cashTrustLogs.Where(x => x.Status == (int)TrustStatus.Verified)
                                        .Select(x => x.OrderGuid);
            //抓取未退款完成 但廠商已標註退貨處理完成之trust_id
            var returnForm = op.ViewOrderReturnFormListGetListByOrderGuid(shippedOrderGuids)
                                .Where(x => x.ProgressStatus == (int)ProgressStatus.Processing ||
                                            x.ProgressStatus == (int)ProgressStatus.AtmQueueing ||
                                            x.ProgressStatus == (int)ProgressStatus.AtmQueueSucceeded ||
                                            x.ProgressStatus == (int)ProgressStatus.AtmFailed ||
                                            (x.ProgressStatus == (int)ProgressStatus.Unreturnable && 
                                            (x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndRetrievied || x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndUnShip)));
            if (vendorProgressComplete)
            {
                returnForm = returnForm.Where(x => x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndNoRetrieving ||
                                                   x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndRetrievied ||
                                                   x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndUnShip ||
                                                   x.VendorProgressStatus == (int)VendorProgressStatus.CompletedByCustomerService);
            }
            var returnedReturFormIds = returnForm.Select(x => x.ReturnFormId);
            var returnedTrustIds = op.ReturnFormRefundGetList(returnedReturFormIds)
                                        .Where(x => !x.VendorNoRefund)
                                        .Select(x => x.TrustId);
            if (returnedTrustIds.Any())
            {
                //抓取之trust_id 須將 未退款完成但廠商已標註退款完成 退貨單 之 refund trust_id 視為已退貨
                foreach (var ctl in cashTrustLogs.Where(ctl => ctl.Status == (int)TrustStatus.Verified &&
                                                                returnedTrustIds.Contains(ctl.TrustId)))
                {
                    ctl.Status = (int)TrustStatus.Returned;
                }
            }

            return cashTrustLogs;
        }

        public static void ProductBalanceSheetCreateMailNotify(IEnumerable<Guid> bids)
        {
            var dealInfos = pp.ViewDealPropertyBusinessHourContentGetList(bids)
                                .GroupBy(x => x.SellerGuid)
                                .ToDictionary(x => x.Key, x => x.ToList());
            foreach (var dealInfo in dealInfos)
            {
                var template = TemplateFactory.Instance().GetTemplate<ProductBalanceSheetCreate>();
                var sellerInfo = dealInfo.Value.First();
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;
                template.SellerName = sellerInfo.SellerName;
                template.DealInfo = string.Join("<br/>",
                                                dealInfo.Value.Select(
                                                    x => string.Format("({0}){1}", x.UniqueId, x.CouponUsage)));

                try
                {
                    var msg = new MailMessage { From = new MailAddress(config.ServiceEmail, config.ServiceName) };

                    if (!string.IsNullOrEmpty(sellerInfo.CompanyEmail))
                    {
                        if (RegExRules.IsEmailContainsSeperator(sellerInfo.CompanyEmail))
                        {
                            msg.To.Add(RegExRules.CheckMultiEmail(sellerInfo.CompanyEmail));
                        }
                        else
                        {
                            if (RegExRules.CheckEmail(sellerInfo.CompanyEmail))
                            {
                                msg.To.Add(sellerInfo.CompanyEmail);
                            }
                        }
                    }
                    //CC給財務
                    msg.CC.Add(config.FinanceAccountsEmail);

                    msg.Subject = string.Format("17Life對帳單開立通知({0})", sellerInfo.SellerName);
                    msg.Body = template.ToString();
                    msg.IsBodyHtml = true;

                    //必須有收件人才寄
                    if (!string.IsNullOrEmpty(msg.To.ToString()))
                    {
                        PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    }
                }
                catch (Exception e)
                {
                    var msg = new MailMessage();
                    msg.To.Add(config.ItTesterEmail);
                    msg.Subject = Environment.MachineName + ": " + "對帳單通知信錯誤";
                    msg.Body = e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine + "檢查SellerEmail格式是否有誤: " + sellerInfo.CompanyEmail + ";SellerGuid:" + sellerInfo.SellerGuid.ToString();
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
            }
        }

        public static void ProductBalanceSheetWithNegativeAmountMailNotify(DataTable bsData)
        {
            var bsInfos = bsData.AsEnumerable()
                                .Select(x => new
                                {
                                    ProductGuid = x.Field<Guid>("product_guid"),
                                    EstAmount = x.Field<int>("est_amount"),
                                    IntervalStart = x.Field<DateTime>("interval_start"),
                                    IntervalEnd = x.Field<DateTime>("interval_end")
                                })
                                .ToLookup(x => x.ProductGuid);
            var dealInfos = pp.ViewDealPropertyBusinessHourContentGetList(bsInfos.Select(x => x.Key))
                                .GroupBy(x => x.SellerGuid)
                                .ToDictionary(x => x.Key, x => x.ToList());
            foreach (var deal in dealInfos)
            {
                var body = new StringBuilder("親愛的財務部同仁，您好：<br/>本對帳單因為出現應付金額為負向，故通知貴部同仁注意此檔收付款情況，並進行相關的協助與處理。謝謝。<br/><br/>");
                var sellerName = deal.Value.First().SellerName;
                body.Append(string.Format("賣家名稱：{0}<br/>對帳單付款資訊如下：<br/>", sellerName));
                body.Append(string.Join("<br/>",
                                                deal.Value.Select(
                                                    x => string.Format("[{0}]{1}<br/>結帳方式：{2}<br/>本次應付總額：{3}<br/>結算日期：{4}", 
                                                                x.UniqueId, 
                                                                x.CouponUsage,
                                                                Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)x.RemittanceType),
                                                                bsInfos.Contains(x.BusinessHourGuid) ? bsInfos[x.BusinessHourGuid].First().EstAmount : 0,
                                                                bsInfos.Contains(x.BusinessHourGuid) 
                                                                ? string.Format("{0:yyyy/MM/dd}", x.RemittanceType == (int)RemittanceType.Flexible
                                                                                                    ? bsInfos[x.BusinessHourGuid].First().IntervalEnd
                                                                                                    : bsInfos[x.BusinessHourGuid].First().IntervalEnd.AddDays(-1)) 
                                                                : string.Empty))));

                try
                {
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(config.SystemEmail);
                    msg.To.Add(config.FinanceAccountsEmail);

                    msg.Subject = string.Format("[{0}]對帳單出現負數通知", sellerName);
                    msg.Body = body.ToString();
                    msg.IsBodyHtml = true;

                    //必須有收件人才寄
                    if (!string.IsNullOrEmpty(msg.To.ToString()))
                    {
                        PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    }
                }
                catch (Exception e)
                {
                    MailMessage msg = new MailMessage();
                    msg.To.Add(config.ItTesterEmail);
                    msg.Subject = Environment.MachineName + ": " + "對帳單出現負數通知信錯誤";
                    msg.Body = e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine;
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
            }

        }

        public static void FlexibleBalanceSheetSystemCreateNotify(IEnumerable<Guid> bids)
        {
            var dealInfos = pp.ViewDealPropertyBusinessHourContentGetList(bids)
                                .GroupBy(x => x.SellerGuid)
                                .ToDictionary(x => x.Key, x => x.ToList());
            foreach (var dealInfo in dealInfos)
            {
                var template = TemplateFactory.Instance().GetTemplate<FlexibleBalanceSheetSystemCreate>();
                var sellerInfo = dealInfo.Value.First();
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;
                template.SellerName = sellerInfo.SellerName;
                template.DealInfo = string.Join("<br/>",
                                                dealInfo.Value.Select(
                                                    x => string.Format("({0}){1}", x.UniqueId, x.CouponUsage)));

                try
                {
                    var msg = new MailMessage { From = new MailAddress(config.ServiceEmail, config.ServiceName) };

                    if (!string.IsNullOrEmpty(sellerInfo.CompanyEmail))
                    {
                        if (RegExRules.IsEmailContainsSeperator(sellerInfo.CompanyEmail))
                        {
                            msg.To.Add(RegExRules.CheckMultiEmail(sellerInfo.CompanyEmail));
                        }
                        else
                        {
                            if (RegExRules.CheckEmail(sellerInfo.CompanyEmail))
                            {
                                msg.To.Add(sellerInfo.CompanyEmail);
                            }
                        }
                    }
                    //CC給財務
                    msg.CC.Add(config.FinanceAccountsEmail);

                    msg.Subject = string.Format("17Life對帳單開立通知({0})", sellerInfo.SellerName);
                    msg.Body = template.ToString();
                    msg.IsBodyHtml = true;

                    //必須有收件人才寄
                    if (!string.IsNullOrEmpty(msg.To.ToString()))
                    {
                        PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    }
                }
                catch (Exception e)
                {
                    var msg = new MailMessage();
                    msg.To.Add(config.ItTesterEmail);
                    msg.Subject = Environment.MachineName + ": " + "對帳單通知信錯誤";
                    msg.Body = e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine + "檢查SellerEmail格式是否有誤: " + sellerInfo.CompanyEmail + ";SellerGuid:" + sellerInfo.SellerGuid.ToString();
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
            }
        }

        //抓取超過每月限制匯款次數之次數
        /* 暫時取消收取匯費機制 待之後確定進行會再有新定義計算規則
         * 原規劃:於verification_statistics_log增加 exceed_remittance_time_limit欄位 以紀錄已超出匯款限制次數 用來計算匯費
           目前已開發完成的部分 先回復程式碼及資料庫新增欄位 只保留View Model定義及匯款次數計算規則 
           視未來是否仍需收取匯費機制 再決定是否繼續保留or 直接清除  
           search "ExceedRemittanceTimeLimit" 即可 搜尋目前保留之部分*/
        public static int GetLastMonthExceedRemittanceTimeLimit(Guid productGuid, Guid? storeGuid, DateTime checkDate)
        {
            var exceedTime = 0;
            var remittanceInfo = pp.DealAccountingGet(productGuid);
            if (remittanceInfo == null)
            {
                return exceedTime;
            }

            if (remittanceInfo.RemittanceType != (int) RemittanceType.Flexible &&
                remittanceInfo.RemittanceType != (int) RemittanceType.Weekly &&
                remittanceInfo.RemittanceType != (int) RemittanceType.Monthly)
            {
                return exceedTime;
            }

            var accountInfo = op.WeeklyPayAccountGetList(productGuid);
            if (!accountInfo.Any())
            {
                return exceedTime;
            }

            WeeklyPayAccount account = null;
            
            if (remittanceInfo.Paytocompany == (int)DealAccountingPayType.PayToCompany)
            {
                var sellerGuid = pp.BusinessHourGet(productGuid).SellerGuid;
                account = accountInfo.FirstOrDefault(x => x.StoreGuid == sellerGuid);
            }
            else
            {
                if (!storeGuid.HasValue)
                {
                    return exceedTime;
                }
                account = accountInfo.FirstOrDefault(x => x.StoreGuid == storeGuid);
            }                
            if (account == null)
            {
                return exceedTime;
            }
            var bankCode = account.BankNo;
            var branchCode = account.BranchNo;
            var accountNo = account.AccountNo;

            if (remittanceInfo.RemittanceType == (int) RemittanceType.Weekly)
            {
                checkDate = checkDate.AddDays(-1);
            }

            //抓取上月先收單據後付款檔次匯款成功筆數
            var startDate = checkDate.AddMonths(-1).GetFirstDayOfMonth();
            var endDate = checkDate.AddMonths(-1).GetLastDayOfMonth();
            var payCount = ap.ViewBalanceSheetFundsTransferPponGetList(startDate, endDate, true)
                                .Where(x => (x.BalanceSheetType == (int)BalanceSheetType.FlexiblePayBalanceSheet
                                         || x.BalanceSheetType == (int)BalanceSheetType.FlexibleToHouseBalanceSheet
                                         || x.BalanceSheetType == (int)BalanceSheetType.MonthlyPayBalanceSheet
                                         || x.BalanceSheetType == (int)BalanceSheetType.MonthlyToHouseBalanceSheet
                                         || x.BalanceSheetType == (int)BalanceSheetType.WeeklyPayBalanceSheet
                                         || x.BalanceSheetType == (int)BalanceSheetType.WeeklyToHouseBalanceSheet)
                                         && x.ReceiverAccountNo == accountNo 
                                         && x.ReceiverBranchNo == branchCode 
                                         && x.ReceiverBankNo == bankCode)
                                .GroupBy(x => x.ResponseTime)
                                .Count();

            //抓取本月對帳區間關聯對帳單對應之verification_statistics_log並加總已超出次數
            //須扣除已紀錄超出次數 仍有超出次數 再進行標註
            /* 目前無紀錄超出次數欄位 先隱藏 */
            //var exceedLimitCount = ap.ViewBalanceSheetFundsTransferPponGetListByIntervalEnd(checkDate.GetFirstDayOfMonth(), checkDate.GetLastDayOfMonth(), true)
            //                            .Where(x => (x.BalanceSheetType == (int)BalanceSheetType.FlexiblePayBalanceSheet
            //                                     || x.BalanceSheetType == (int)BalanceSheetType.FlexibleToHouseBalanceSheet
            //                                     || x.BalanceSheetType == (int)BalanceSheetType.MonthlyPayBalanceSheet
            //                                     || x.BalanceSheetType == (int)BalanceSheetType.MonthlyToHouseBalanceSheet
            //                                     || x.BalanceSheetType == (int)BalanceSheetType.WeeklyPayBalanceSheet
            //                                     || x.BalanceSheetType == (int)BalanceSheetType.WeeklyToHouseBalanceSheet)
            //                                     && x.ReceiverAccountNo == accountNo
            //                                     && x.ReceiverBranchNo == branchCode
            //                                     && x.ReceiverBankNo == bankCode)
            //                            .Sum(x => x.ExceedRemittanceTimeLimit);
            
            //payCount = payCount - exceedLimitCount;
            //var limitTime = config.AchRemittanceTimeLimit;
            //if (payCount > limitTime)
            //{
            //    exceedTime = payCount - limitTime;
            //}

            return exceedTime;
        }

        #region Private

        //目前只提供宅配對帳單產出使用
        //只有新出帳方式(宅配)適用此緩衝期間計算
        private static int CalculateEndBuffer(RemittanceType remittanceType, DateTime genTime)
        {
            int result;
            
            switch (remittanceType)
            {
                case RemittanceType.Weekly:
                    result = 7;
                    break;
                case RemittanceType.Monthly:
                case RemittanceType.Flexible:
                    result = genTime.AddMonths(-1).GetCountDaysOfMonth();
                    break;
                case RemittanceType.Fortnightly:
                    result = 16;
                    break;
                default:
                    result = 0;
                    break;
            }

            return result;
        }

        #endregion Private

    }
}

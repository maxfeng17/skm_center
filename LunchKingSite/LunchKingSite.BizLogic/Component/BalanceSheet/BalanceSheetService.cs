﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;

namespace LunchKingSite.BizLogic.Component
{
    public class BalanceSheetService
    {
        private static IAccountingProvider _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IHiDealProvider _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static ISysConfProvider _cp = ProviderFactory.Instance().GetConfig();


        /// <summary>
        /// job執行產生對帳單的
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        public static ReadOnlyCollection<BalanceSheetGenerationResult> Create(BalanceSheetSpec spec)
        {
            BsCreationBase algorithm = GetCreationAlgorithm(spec);
            return algorithm.Generate();
        }

        /// <summary>
        /// 模擬/彈性執行產生對帳單
        /// </summary>
        /// <param name="spec"></param>
        /// <param name="simulationTime"></param>
        /// <returns></returns>
        public static ReadOnlyCollection<BalanceSheetGenerationResult> SimulateCreate(BalanceSheetSpec spec, DateTime simulationTime)
        {
            BsCreationBase algorithm = GetCreationAlgorithm(spec);
            algorithm.SetGenerationTime(simulationTime);
            return algorithm.Generate();
        }

        /// <summary>
        /// 反核銷在對帳單系統的資料異動
        /// </summary>
        /// <returns>是否成功執行</returns>
        public static bool UndoDetail(Guid trustId, int memberUniqueId, UndoType undoType)
        {
            IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            BalanceSheetDetail detail = ap.BalanceSheetDetailGetByTrustIdForUndo(trustId);
            if (detail == null || detail.IsNew)
            {
                return true;
            }
            string message;
            switch (undoType)
            {
                case UndoType.UnVerify:
                    message = "取消核銷";
                    break;
                case UndoType.Return:
                    message = "商品退貨";
                    break;
                default:
                    message = "不明原因";
                    break;
            }

            BalanceModificationLog log = new BalanceModificationLog
            {
                BalanceSheetId = detail.BalanceSheetId,
                TrustId = detail.TrustId,
                DetailStatus = detail.Status,
                DetailStatusLogId = detail.CashTrustStatusLogId,
                Message = message,
                CreateId = memberUniqueId,
                CreateTime = DateTime.Now
            };

            BalanceSheetModel bsModel = new BalanceSheetModel(detail.BalanceSheetId);
            
            BalanceSheet balanceSheet = bsModel.GetDbBalanceSheet();
            if (balanceSheet.IsDetailFixed)
            {
                //暫付對帳單 因只會有一張對帳單 不會產生對應扣項 所以不做undo
                if (balanceSheet.GenerationFrequency == (int) BalanceSheetGenerationFrequency.LumpSumBalanceSheet)
                {
                    return true;
                }

                detail.Status = (detail.Status == (int)BalanceSheetDetailStatus.Normal) ? (int)BalanceSheetDetailStatus.Undo : (int)BalanceSheetDetailStatus.UndoNoPay;
                detail.UndoTime = DateTime.Now;
                detail.UndoId = memberUniqueId;

                log.LogStatus = (int)BalanceModificationLogStatus.Undo;
                int count = 0;

                using (TransactionScope scope = new TransactionScope())
                {
                    ap.BalanceModificationLogSet(log);
                    detail.ModLogId = log.Id;
                    count = ap.BalanceSheetDetailUndoSet(detail);
                    scope.Complete();
                }
                return count == 1;
            }
            else
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    log.LogStatus = (int)BalanceModificationLogStatus.UndoDelete;
                    ap.BalanceModificationLogSet(log);
                    detail.ModLogId = log.Id;
                    ap.BalanceSheetDetailDelete(detail);
                    balanceSheet.EstAmount = (int)bsModel.GetAccountsPayable().TotalAmount; //更新應付金額
                    var vendorPositivePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorPositivePaymentOverdueAmount;
                    var vendorNegativePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorNegativePaymentOverdueAmount;

                    if (balanceSheet.DirtyColumns.Any())
                    {
                        ap.BalanceSheetEstAmountSet(balanceSheet, vendorPositivePaymentOverdueAmount, vendorNegativePaymentOverdueAmount);
                    }
                    scope.Complete();
                }

                return true;
            }
        }

        /// <summary>
        /// 把未列入對帳單的反核銷資料加到對帳單內扣款
        /// </summary>
        public static bool AddDeductDetailToBs(Guid trustId, string reason, int unfixedBsId, int memberUniqueId)
        {
            // In order to add a "deduct detail",
            // (1) an "undo detail" must exist in a previous balance sheet.
            // (2) balance sheet must be unfixed.
            // (3) balance sheet must have enough amount to deduct

            BalanceSheetModel bsModel = new BalanceSheetModel(unfixedBsId);
            BalanceSheet bs = bsModel.GetDbBalanceSheet();
            if (bs.IsDetailFixed)
            {
                return false;
            }

            if ((BalanceSheetGenerationFrequency)bs.GenerationFrequency != BalanceSheetGenerationFrequency.MonthBalanceSheet)
            {
                return false;
            }

            BalanceSheetDetail undoDetail;
            if (!ExistsUndoDetail(trustId, out undoDetail))
            {
                return false;
            }

            if (!IsAmountEnough(bsModel, trustId))
            {
                return false;
            }

            BalanceModificationLog modLog = new BalanceModificationLog()
            {
                BalanceSheetId = bsModel.Id,
                TrustId = trustId,
                DetailStatus = (int)BalanceSheetDetailStatus.Deduction,
                LogStatus = (int)BalanceModificationLogStatus.AdditionalDeduction,
                Message = reason,
                CreateId = memberUniqueId,
                CreateTime = DateTime.Now,
                DetailStatusLogId = undoDetail.CashTrustStatusLogId
            };

            BalanceSheetDetail deduct = new BalanceSheetDetail()
            {
                BalanceSheetId = bsModel.Id,
                TrustId = trustId,
                Status = (int)BalanceSheetDetailStatus.Deduction,
                UndoTime = undoDetail.UndoTime,
                UndoId = undoDetail.UndoId,
                CashTrustStatusLogId = undoDetail.CashTrustStatusLogId,
                DeductedBalanceSheetId = undoDetail.BalanceSheetId
            };

            try
            {
                using (TransactionScope trxScope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    _ap.BalanceModificationLogSet(modLog);
                    deduct.ModLogId = modLog.Id;
                    _ap.BalanceSheetDetailSet(deduct);
                    bs.EstAmount = -1;  // if the value of EstAmount is not changed (if cost = 0 or something), saving it will fail (dirty columns = 0)
                    bs.EstAmount = (int)bsModel.GetAccountsPayable().TotalAmount;
                    var vendorPositivePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorPositivePaymentOverdueAmount;
                    var vendorNegativePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorNegativePaymentOverdueAmount;
                    _ap.BalanceSheetEstAmountSet(bs, vendorPositivePaymentOverdueAmount, vendorNegativePaymentOverdueAmount);
                    trxScope.Complete();
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 把未列入對帳單的核銷資料加到對帳單內付款
        /// </summary>
        public static bool AddVerifiedDetailToBs(Guid trustId, string reason, int unfixedBsId, int memberUniqueId)
        {
            BalanceSheetModel bsModel = new BalanceSheetModel(unfixedBsId);
            BalanceSheet bs = bsModel.GetDbBalanceSheet();
            if (bs.IsDetailFixed)
            {
                return false;
            }

            if ((BalanceSheetGenerationFrequency)bs.GenerationFrequency != BalanceSheetGenerationFrequency.MonthBalanceSheet)
            {
                return false;
            }

            CashTrustStatusLogCollection statusLogs = _mp.CashTrustStatusLogGetList(trustId);

            int latestLogId = statusLogs
                .Where(log => log.TrustId == trustId && log.Status == (int)TrustStatus.Verified)
                .OrderByDescending(log => log.ModifyTime)
                .Select(log => log.Id).FirstOrDefault();

            BalanceModificationLog modLog = new BalanceModificationLog()
            {
                BalanceSheetId = bs.Id,
                TrustId = trustId,
                DetailStatus = (int)BalanceSheetDetailStatus.Normal,
                LogStatus = (int)BalanceModificationLogStatus.AdditionalPay,
                Message = reason,
                CreateId = memberUniqueId,
                CreateTime = DateTime.Now,
                DetailStatusLogId = latestLogId
            };

            BalanceSheetDetail detail = new BalanceSheetDetail()
            {
                BalanceSheetId = bs.Id,
                TrustId = trustId,
                Status = (int)BalanceSheetDetailStatus.Normal,
                CashTrustStatusLogId = latestLogId
            };

            try
            {
                using (TransactionScope trxScope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    _ap.BalanceModificationLogSet(modLog);
                    detail.ModLogId = modLog.Id;
                    _ap.BalanceSheetDetailSet(detail);
                    bs.EstAmount = -1;  // if the value of EstAmount is not changed (if cost = 0 or something), saving it will fail (dirty columns = 0)
                    bs.EstAmount = (int)bsModel.GetAccountsPayable().TotalAmount;
                    var vendorPositivePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorPositivePaymentOverdueAmount;
                    var vendorNegativePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorNegativePaymentOverdueAmount;
                    _ap.BalanceSheetEstAmountSet(bs, vendorPositivePaymentOverdueAmount, vendorNegativePaymentOverdueAmount);
                    trxScope.Complete();
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 週對帳單的月份要從匯款結果來判定
        /// </summary>
        public static void CompleteWeekBalanceSheetMonthInfo()
        {
            IAccountingProvider _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            _ap.CompleteWeekBalanceSheetMonthInfo();
        }
        
        public static void RecomputeBalanceSheetEstAmount(int balanceSheetId)
        {
            BalanceSheetModel bsModel = new BalanceSheetModel(balanceSheetId);
            BalanceSheet bs = bsModel.GetDbBalanceSheet();
            bs.EstAmount = -1;  // if the value of EstAmount is not changed (if cost = 0 or something), saving it will fail (dirty columns = 0)
            bs.EstAmount = (int)bsModel.GetAccountsPayable().TotalAmount;
            _ap.BalanceSheetSet(bs);
        }

        public static DateTime? GetStopPaymentTime(Guid productGuid, Guid? storeGuid)
        {
            BalanceSheetCollection mbs = _ap.BalanceSheetGetListByProductGuidStoreGuid(productGuid, storeGuid,
                                                          BalanceSheetGenerationFrequency.MonthBalanceSheet);

            if (mbs == null || mbs.Count == 0)
            {
                return null;
            }

            List<BalanceSheet> unconfirmedMbs = mbs.Where(bs => !bs.IsConfirmedReadyToPay)
                                 .OrderBy(bs => bs.IntervalStart).ToList();

            if (unconfirmedMbs.Count == 0)
            {
                return null;
            }

            BalanceSheet earliestUnconfirmedMbs = unconfirmedMbs.First();

            return new DateTime(earliestUnconfirmedMbs.Year.Value
                , earliestUnconfirmedMbs.Month.Value
                , _cp.ReceiptReceivedThresholdDay)
                .AddMonths(1);
        }

        public static bool RecoverUndoDetail(Guid trustId, int memberUniqueId, string reason)
        {
            var ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            var details = ap.BalanceSheetDetailGetListByTrustId(trustId);
            if (!details.Any())
            {
                return true;
            }
            var undos = details.Where(x => x.Status == (int) BalanceSheetDetailStatus.Undo).ToList();
            var deducts = details.Where(x => x.Status == (int)BalanceSheetDetailStatus.Deduction).ToList();
            if (undos.Count == deducts.Count)
            {
                return true;
            }
            var detail = undos.OrderByDescending(x => x.UndoTime).FirstOrDefault();
            if (detail == null)
            {
                return true;
            }

            var log = new BalanceModificationLog
            {
                BalanceSheetId = detail.BalanceSheetId,
                TrustId = detail.TrustId,
                DetailStatus = detail.Status,
                DetailStatusLogId = detail.CashTrustStatusLogId,
                Message = reason,
                CreateId = memberUniqueId,
                CreateTime = DateTime.Now
            };

            var bsModel = new BalanceSheetModel(detail.BalanceSheetId);

            var balanceSheet = bsModel.GetDbBalanceSheet();
            if (balanceSheet.IsDetailFixed)
            {
                detail.Status = (int)BalanceSheetDetailStatus.Normal;
                detail.UndoTime = null;
                detail.UndoId = null;

                log.LogStatus = (int)BalanceModificationLogStatus.UndoRecover;
                int count;

                using (var scope = new TransactionScope())
                {
                    ap.BalanceModificationLogSet(log);
                    detail.ModLogId = log.Id;
                    count = ap.BalanceSheetDetailSet(detail);
                    scope.Complete();
                }
                return count == 1;
            }

            return true;
        }

        /// <summary>
        /// 刪除對帳單
        /// </summary>
        /// <param name="bsId"></param>
        /// <param name="vslId"></param>
        /// <returns></returns>
        public static bool DeleteBalanceSheeet(int bsId, int vslId, Guid productGuid, string userName)
        {
            try
            {
                BalanceSheet bs = _ap.BalanceSheetGet(bsId);
                _ap.BalanceSheetDetailDelete(bsId);
                _ap.BalanceSheetIspDetailDelete(bsId);

                //異動金額
                VendorPaymentChangeCollection vpcc = _ap.VendorPaymentChangeGetList(bsId);
                foreach (VendorPaymentChange vpc in vpcc)
                {
                    vpc.BalanceSheetId = null;
                }
                if (vpcc.Any())
                {
                    _ap.VendorPaymentChangeSetList(vpcc);
                }

                //逾期
                VendorPaymentOverdueCollection vpoc = _ap.VendorPaymentOverdueGetListByBsid(bsId);
                foreach (VendorPaymentOverdue vpo in vpoc)
                {
                    vpo.BalanceSheetId = null;
                }
                if (vpoc.Any())
                {
                    _ap.VendorPaymentOverdueSetList(vpoc);
                }

                //物流費用
                WmsOrderServFeeCollection order = _wp.GetWmsOrderServFeeByBalanceSheetId(bsId);
                if (order.Any())
                {
                    order.ForEach(x => x.BalanceSheetId = null);
                    _wp.WmsOrderServFeeSet(order);
                }
                

                WmsShipFeeCollection ship = _wp.GetWmsShipFeeByBalanceSheetId(bsId);
                if (ship.Any())
                {
                    ship.ForEach(x => x.BalanceSheetId = null);
                    _wp.WmsShipFeeSet(ship);
                }
                

                WmsPackFeeCollection pack = _wp.GetWmsPackFeeByBalanceSheetId(bsId);
                if (pack.Any())
                {
                    pack.ForEach(x => x.BalanceSheetId = null);
                    _wp.WmsPackFeeSet(pack);
                }
                

                WmsRefundShipFeeCollection refund = _wp.GetWmsRefundShipFeeByBalanceSheetId(bsId);
                if (refund.Any())
                {
                    refund.ForEach(x => x.BalanceSheetId = null);
                    _wp.WmsRefundShipFeeSet(refund);
                }
                


                _ap.BalanceSheetDelete(bsId);
                _pp.VerificationStatisticsLogDelete(vslId);

                CommonFacade.AddAudit(productGuid.ToString(), AuditType.DealAccounting, "刪除對帳單：bsId=" + bsId + ",vslId=" + vslId + ",interval=" + bs.IntervalStart.ToString("yyyy /MM/dd") + "~" + bs.IntervalEnd.ToString("yyyy/MM/dd"), userName, true);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 刪除對帳單
        /// </summary>
        /// <param name="bsId"></param>
        /// <param name="vslId"></param>
        /// <returns></returns>
        public static bool DeleteBalanceSheeetWms(int bswId, Guid sellerGuid, string userName)
        {
            try
            {
                BalanceSheetWm bsw = _ap.BalanceSheetWmsGet(bswId);


                //物流費用
                WmsPurchaseServFeeCollection purchase = _wp.GetWmsPurchaseServFeeByBalanceSheetWmsId(bswId);
                if (purchase.Any())
                {
                    purchase.ForEach(x => x.BalanceSheetWmsId = null);
                    _wp.WmsPurchaseServFeeSet(purchase);
                }

                WmsStockFeeCollection stock = _wp.GetWmsStockFeeByBalanceSheetWmsId(bswId);
                if (stock.Any())
                {
                    stock.ForEach(x => x.BalanceSheetWmsId = null);
                    _wp.WmsStockFeeSet(stock);
                }

                WmsReturnServFeeCollection returnServ = _wp.GetWmsReturnServFeeByBalanceSheetWmsId(bswId);
                if (returnServ.Any())
                {
                    returnServ.ForEach(x => x.BalanceSheetWmsId = null);
                    _wp.WmsReturnServFeeSet(returnServ);
                }


                WmsReturnShipFeeCollection returnShip = _wp.GetWmsReturnShipFeeByBalanceSheetWmsId(bswId);
                if (returnShip.Any())
                {
                    returnShip.ForEach(x => x.BalanceSheetWmsId = null);
                    _wp.WmsReturnShipFeeSet(returnShip);
                }

                WmsReturnPackFeeCollection returnPack = _wp.GetWmsReturnPackFeeByBalanceSheetWmsId(bswId);
                if (returnPack.Any())
                {
                    returnPack.ForEach(x => x.BalanceSheetWmsId = null);
                    _wp.WmsReturnPackFeeSet(returnPack);
                }


                _ap.BalanceSheetWmsDelete(bswId);

                CommonFacade.AddAudit(sellerGuid.ToString(), AuditType.DealAccounting, "刪除倉儲對帳單：bswId=" + bswId + ",interval=" + bsw.IntervalStart.ToString("yyyy /MM/dd") + "~" + bsw.IntervalEnd.ToString("yyyy/MM/dd"), userName, true);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static List<DateTime> GetIspFreightsChangeDateList(ProductDeliveryType type)
        {
            List<DateTime> list = new List<DateTime>();

            //超取開始時間
            list.Add(Convert.ToDateTime("2017/08/01"));
            if (type == ProductDeliveryType.FamilyPickup)
            {
                if (_cp.ISPFamilyFreightsChangeDate != string.Empty)
                    list.AddRange(_cp.ISPFamilyFreightsChangeDate.Split(",").ToList().Select(date => DateTime.Parse(date)).ToList());
            }
            else if (type == ProductDeliveryType.SevenPickup)
            {
                if (_cp.ISPSevenFreightsChangeDate != string.Empty)
                    list.AddRange(_cp.ISPSevenFreightsChangeDate.Split(",").ToList().Select(date => DateTime.Parse(date)).ToList());
            }
            return list;
        }

        #region private members

        private static BsCreationBase GetCreationAlgorithm(BalanceSheetSpec spec)
        {
            BalanceSheetType sheetType;
            object merchandiseKey;

            //todo: remove merchandiseKey, add merchandiseGuid
            //or...... see if [balance_sheet].[product_guid] can be removed
            int merchandiseId;
            var triggerUser = string.IsNullOrEmpty(spec.TriggerUser)
                                    ? "sys"
                                    : spec.TriggerUser;
            var genOutOfVerificationTimeRange = spec.GenOutOfVerificationTimeRange.HasValue && spec.GenOutOfVerificationTimeRange.Value;

            switch (spec.BizModel)
            {
                case BusinessModel.Ppon:
                    sheetType = GetBalanceSheetType(spec.SheetCategory, spec.BizModel, spec.BusinessHourGuid);
                    merchandiseKey = spec.BusinessHourGuid;
                    var dealProperty = _pp.DealPropertyGet(spec.BusinessHourGuid);
                    if (dealProperty == null || !dealProperty.IsLoaded)
                    {
                        throw new Exception("Unable to find ppon unique id");
                    }
                    merchandiseId = dealProperty.UniqueId;
                    break;
                case BusinessModel.PiinLife:
                    sheetType = GetBalanceSheetType(spec.SheetCategory, spec.BizModel, spec.HiDealProductId);
                    merchandiseKey = spec.HiDealProductId;
                    merchandiseId = spec.HiDealProductId.Value;
                    break;
                default:
                    sheetType = BalanceSheetType.Unknown;
                    merchandiseKey = null;
                    merchandiseId = 0;
                    break;
            }

            switch (sheetType)
            {
                case BalanceSheetType.WeeklyPayWeekBalanceSheet:
                    return new AchWeeklyPayWeekBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId);

                case BalanceSheetType.ManualWeeklyPayWeekBalanceSheet:
                    return new AccountingWeeklyPayWeekBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId);

                case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                    return new AchWeeklyPayMonthBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId, triggerUser, genOutOfVerificationTimeRange);

                case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
                    return new AccountingWeeklyPayMonthBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId, triggerUser, genOutOfVerificationTimeRange);

                case BalanceSheetType.MonthlyPayBalanceSheet:
                    return new AchMonthlyPayBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId);

                case BalanceSheetType.ManualMonthlyPayBalanceSheet:
                    return new AccountingMonthlyPayBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId);

                case BalanceSheetType.ManualTriggered:
                    return new ManualTriggeredBalanceSheet(spec.BizModel, merchandiseKey, spec.ManualTriggeredStoreGuid, merchandiseId);

                case BalanceSheetType.ManualLumpSumPayBalanceSheet:
                    return new LumpSumPayBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId);

                case BalanceSheetType.WeeklyToHouseBalanceSheet:
                    return new WeeklyToHouseBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId);

                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                    return new FortnightlyToHouseBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId);

                case BalanceSheetType.MonthlyToHouseBalanceSheet:
                    return new MonthlyToHouseBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId);

                case BalanceSheetType.FlexibleToHouseBalanceSheet:
                    return new FlexibleToHouseBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId, triggerUser);

                case BalanceSheetType.WeeklyPayBalanceSheet:
                    return new WeeklyPayBalanceSheet(spec.BizModel, merchandiseKey, sheetType, merchandiseId);

                case BalanceSheetType.FlexiblePayBalanceSheet:
                    return new FlexiblePayBalanceSheet(spec.BizModel, merchandiseKey, spec.ManualTriggeredStoreGuid, sheetType, merchandiseId, triggerUser);
            }
            string balanceSheetErrorMessage=string.Format(
                @"Cannot create a FinanceBalanceSheet type:{0}
                The arguments are: sheetCategory: {1},\r\nbizModel: {2},\r\nmerchandiseKey={3},\r\nmanualTriggeredStoreGuid={4}"
                                    , sheetType.ToString()
                                    , spec.SheetCategory.ToString()
                                    , spec.BizModel.ToString()
                                    , spec.BizModel == BusinessModel.Ppon ? spec.BusinessHourGuid.ToString() : spec.HiDealProductId.ToString()
                                    , spec.ManualTriggeredStoreGuid.GetValueOrDefault(Guid.Empty));
            log4net.LogManager.GetLogger(typeof(BalanceSheetService)).Warn(balanceSheetErrorMessage);
            throw new InvalidOperationException(balanceSheetErrorMessage);
        }

        private static BalanceSheetType GetBalanceSheetType(BalanceSheetGenerationFrequency sheetCategory, BusinessModel businessType, object merchandiseKey)
        {
            if (merchandiseKey == null)
            {
                return BalanceSheetType.Unknown;
            }

            if (sheetCategory == BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet)
            {
                return BalanceSheetType.ManualTriggered;
            }
            
            int key;
            if (businessType == BusinessModel.PiinLife && int.TryParse(merchandiseKey.ToString(), out key))
            {
                HiDealVendorBillingHelper piinHelper = new HiDealVendorBillingHelper(key);

                if (!piinHelper.DeliveryType.HasValue || piinHelper.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    return BalanceSheetType.Unknown;
                }

                if (sheetCategory == BalanceSheetGenerationFrequency.WeekBalanceSheet)
                {
                    if (piinHelper.FinancialStaffPaymentMethod == RemittanceType.AchWeekly)
                    {
                        return BalanceSheetType.WeeklyPayWeekBalanceSheet;
                    }

                    if (piinHelper.FinancialStaffPaymentMethod == RemittanceType.ManualWeekly)
                    {
                        return BalanceSheetType.ManualWeeklyPayWeekBalanceSheet;
                    }
                }

                if (sheetCategory == BalanceSheetGenerationFrequency.MonthBalanceSheet)
                {
                    if (piinHelper.FinancialStaffPaymentMethod == RemittanceType.AchMonthly)
                    {
                        return BalanceSheetType.MonthlyPayBalanceSheet;
                    }

                    if (piinHelper.FinancialStaffPaymentMethod == RemittanceType.ManualMonthly)
                    {
                        return BalanceSheetType.ManualMonthlyPayBalanceSheet;
                    }
                }

                if (sheetCategory == BalanceSheetGenerationFrequency.MonthBalanceSheet)
                {
                    if (piinHelper.FinancialStaffPaymentMethod == RemittanceType.AchWeekly)
                    {
                        return BalanceSheetType.WeeklyPayMonthBalanceSheet;
                    }

                    if (piinHelper.FinancialStaffPaymentMethod == RemittanceType.ManualWeekly)
                    {
                        return BalanceSheetType.ManualWeeklyPayMonthBalanceSheet;
                    }
                }
            }

            Guid guidKey;
            if (businessType == BusinessModel.Ppon && Guid.TryParse(merchandiseKey.ToString(), out guidKey))
            {
                PponVendorBillingHelper pponhelper = new PponVendorBillingHelper(guidKey);

                if (!pponhelper.DeliveryType.HasValue)
                {
                    return BalanceSheetType.Unknown;
                }

                if (pponhelper.DeliveryType == (int)DeliveryType.ToShop)
                {
                    if (sheetCategory == BalanceSheetGenerationFrequency.WeekBalanceSheet)
                    {
                        if (pponhelper.FinancialStaffPaymentMethod == RemittanceType.AchWeekly)
                        {
                            return BalanceSheetType.WeeklyPayWeekBalanceSheet;
                        }

                        if (pponhelper.FinancialStaffPaymentMethod == RemittanceType.ManualWeekly)
                        {
                            return BalanceSheetType.ManualWeeklyPayWeekBalanceSheet;
                        }

                        if (pponhelper.FinancialStaffPaymentMethod == RemittanceType.Weekly)
                        {
                            return BalanceSheetType.WeeklyPayBalanceSheet;
                        }
                    }

                    if (sheetCategory == BalanceSheetGenerationFrequency.MonthBalanceSheet)
                    {
                        if (pponhelper.FinancialStaffPaymentMethod == RemittanceType.AchMonthly ||
                            pponhelper.FinancialStaffPaymentMethod == RemittanceType.Monthly)
                        {
                            return BalanceSheetType.MonthlyPayBalanceSheet;
                        }

                        if (pponhelper.FinancialStaffPaymentMethod == RemittanceType.ManualMonthly)
                        {
                            return BalanceSheetType.ManualMonthlyPayBalanceSheet;
                        }

                        if (pponhelper.FinancialStaffPaymentMethod == RemittanceType.AchWeekly)
                        {
                            return BalanceSheetType.WeeklyPayMonthBalanceSheet;
                        }

                        if (pponhelper.FinancialStaffPaymentMethod == RemittanceType.ManualWeekly)
                        {
                            return BalanceSheetType.ManualWeeklyPayMonthBalanceSheet;
                        }
                    }

                    if (sheetCategory == BalanceSheetGenerationFrequency.FlexibleBalanceSheet)
                    {
                        return BalanceSheetType.FlexiblePayBalanceSheet;
                    }
                }

                if (pponhelper.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    //目前宅配皆為人工付款 無ACH付款
                    if (sheetCategory == BalanceSheetGenerationFrequency.LumpSumBalanceSheet)
                    { 
                        if (pponhelper.FinancialStaffPaymentMethod == RemittanceType.Others ||
                            pponhelper.FinancialStaffPaymentMethod == RemittanceType.ManualPartially)
                        {
                            return BalanceSheetType.ManualLumpSumPayBalanceSheet;
                        }
                    }

                    #region 先收單據後付款 
                    
                    if (sheetCategory == BalanceSheetGenerationFrequency.WeekBalanceSheet &&
                        pponhelper.FinancialStaffPaymentMethod == RemittanceType.Weekly)
                    {
                        return BalanceSheetType.WeeklyToHouseBalanceSheet;
                    }

                    if (sheetCategory == BalanceSheetGenerationFrequency.FortnightlyBalanceSheet &&
                        pponhelper.FinancialStaffPaymentMethod == RemittanceType.Fortnightly)
                    {
                        return BalanceSheetType.FortnightlyToHouseBalanceSheet;
                    }

                    if (sheetCategory == BalanceSheetGenerationFrequency.MonthBalanceSheet &&
                        pponhelper.FinancialStaffPaymentMethod == RemittanceType.Monthly)
                    {
                        return BalanceSheetType.MonthlyToHouseBalanceSheet;
                    }

                    if (sheetCategory == BalanceSheetGenerationFrequency.FlexibleBalanceSheet &&
                        pponhelper.FinancialStaffPaymentMethod == RemittanceType.Flexible)
                    {
                        return BalanceSheetType.FlexibleToHouseBalanceSheet;
                    }

                    #endregion 先收單據後付款
                }
            }

            return BalanceSheetType.Unknown;
        }

        private static bool ExistsUndoDetail(Guid trustId, out BalanceSheetDetail undoDetail)
        {
            ViewBalanceSheetUndeducted undeductedDetail = _ap.ViewBalanceSheetUndeductedGetByTrustId(trustId);

            if (undeductedDetail != null && undeductedDetail.IsLoaded)
            {
                //                      X                            V
                // verify => undo => verify => undo
                BalanceSheetDetailCollection details = _ap.BalanceSheetDetailGetListByTrustId(trustId);
                undoDetail = details.Where(x => x.BalanceSheetId == undeductedDetail.BalanceSheetId).First();
                return true;
            }

            undoDetail = null;
            return false;
        }

        private static bool IsAmountEnough(BalanceSheetModel deductBsModel, Guid trustId)
        {
            decimal amount = deductBsModel.GetAccountsPayable().TotalAmount;

            decimal cost;
            switch (deductBsModel.BizModel)
            {
                case BusinessModel.Ppon:
                    cost = GetFirstCost(deductBsModel.MerchandiseGuid);
                    break;

                case BusinessModel.PiinLife:
                    cost = GetPiinCost(trustId);
                    break;

                default:
                    cost = decimal.MaxValue;
                    break;
            }

            return amount >= cost;
        }

        private static decimal GetFirstCost(Guid bid)
        {
            var costs = _pp.DealCostGetList(bid) ?? new DealCostCollection();
            DealProperty dp = _pp.DealPropertyGet(bid);
            if (costs.Any())
            {
                decimal cost = costs.Max(x => x.Cost) ?? 0;
                return cost > 0 && dp.SaleMultipleBase > 0 ? cost / dp.SaleMultipleBase.Value : cost;
            }

            return decimal.MaxValue;
        }

        private static decimal GetPiinCost(Guid trustId)
        {
            CashTrustLog ctlog = _mp.CashTrustLogGet(trustId);
            if (ctlog.CouponId.HasValue)
            {
                HiDealCoupon coupon = _hp.HiDealCouponGet(ctlog.CouponId.Value);
                if (coupon != null && coupon.IsLoaded)
                {
                    return coupon.Cost;
                }
            }

            return decimal.MaxValue;
        }

        #endregion private members
    }

    public class BalanceSheetSpec
    {
        public BalanceSheetGenerationFrequency SheetCategory { get; set; }

        public BusinessModel BizModel { get; set; }
        
        public int? HiDealProductId { get; set; }

        public Guid BusinessHourGuid { get; set; }

        public Guid? ManualTriggeredStoreGuid { get; set; }

        public string TriggerUser { get; set; }

        public bool? GenOutOfVerificationTimeRange { get; set; }
    }
}
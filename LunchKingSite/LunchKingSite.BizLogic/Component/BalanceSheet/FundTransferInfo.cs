﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.BizLogic.Vbs.BS
{
    /// <summary>
    /// 對帳匯款資訊
    /// </summary>
    public class FundTransferInfo
    {
        private static IAccountingProvider _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();

        #region props

        private readonly int _balanceSheetId;
        public int BalanceSheetId
        {
            get { return _balanceSheetId; }
        }

        private readonly int _accountsPayable;
        /// <summary>
        /// 應付總額
        /// </summary>
        public int AccountsPayable 
        { 
            get { return _accountsPayable; }
        }

        private readonly int? _accountsPaid;
        /// <summary>
        /// 實付總額
        /// </summary>
        public int? AccountsPaid
        {
            get { return _accountsPaid; }
        }

        private readonly string _transferStatusDescription;
        /// <summary>
        /// 匯款狀態的描述 (請款中; 已完成; 尚有前期未回發票或收據 ...) 
        /// </summary>
        public string TransferStatusDescription
        {
            get { return _transferStatusDescription; }
        }

        private readonly string _transferCompletionDescription;
        /// <summary>
        /// 完成付款的描述 (example: 2012/06/22; 暫停付款; 尚未付款)
        /// </summary>
        public string TransferCompletionDescription
        {
            get { return _transferCompletionDescription; }
        }

        private readonly string _invoiceNumberAndStatus;
        /// <summary>
        /// 對應的單據發票&處理狀態 (ex: 發票處理中,完成的話沒有狀態，所以只有一種狀態)
        /// </summary>
        public string InvoiceNumberAndStatus
        {
            get { return _invoiceNumberAndStatus; }
        }

        private readonly string _defaultPaymentTime;
        /// <summary>
        /// 預計付款日期 
        /// </summary>
        public string DefaultPaymentTime
        {
            get { return _defaultPaymentTime; }
        }

        private string _memo;
        public string Memo
        {
            get { return _memo; }
            set { _memo = value; }
        }

        private readonly string _receiverTitle;
        /// <summary>
        /// 受款戶名
        /// </summary>
        public string ReceiverTitle
        {
            get { return _receiverTitle; }
        }

        private readonly string _receiverCompanyId;
        /// <summary>
        /// 受款ID (統編/身分證字號)
        /// </summary>
        public string ReceiverCompanyId
        {
            get { return _receiverCompanyId; }
        }

        private readonly string _receiverAccountBankNo;
        /// <summary>
        /// 受款帳戶銀行代號
        /// </summary>
        public string ReceiverAccountBankNo
        {
            get { return _receiverAccountBankNo; }
        }

        private readonly string _receiverAccountBranchNo;
        /// <summary>
        /// 受款帳戶分行代號
        /// </summary>
        public string ReceiverAccountBranchNo
        {
            get { return _receiverAccountBranchNo; }
        }

        private readonly string _receiverAccountNo;
        /// <summary>
        /// 受款帳號
        /// </summary>
        public string ReceiverAccountNo
        {
            get { return _receiverAccountNo; }
        }

        #endregion

        #region .ctor
                
        public FundTransferInfo(int balanceSheetId, Func<TotalAmountDetail> amountPayableCallback, BalanceSheetType bsType)
        {
            _balanceSheetId = balanceSheetId;
            bool useDefaultValue = false;

            if (int.Equals(0, balanceSheetId))
            {
                useDefaultValue = true;
            }
            BalanceSheetBillRelationshipCollection bsBills =_ap.BalanceSheetBillRelationshipGetListByBalanceSheetId(balanceSheetId);
            if (bsBills.Count > 0)
            {
                var result = bsBills.OrderByDescending(x => x.CreateTime).FirstOrDefault();
                Bill bill = _ap.BillGet(result.BillId);
                _invoiceNumberAndStatus = bill.BillNumber;
            }
            else 
            {
                _invoiceNumberAndStatus = "";
            }
            if (IsAch(bsType))
            {
                #region ACH  2013/07/10 delete this block after [weekly_pay_report] has been integrated into [fund_transfer]
                
                ViewBalanceSheetFundsTransferPpon bsFundInfo = _ap.ViewBalanceSheetFundsTransferPponGetByBalanceSheetId(balanceSheetId);
                if (bsFundInfo == null
                    || !bsFundInfo.IsLoaded)
                {
                    useDefaultValue = true;
                }

                if (!useDefaultValue)
                {
                    _defaultPaymentTime = (bsFundInfo.DefaultPaymentTime == null ? "" : Convert.ToDateTime(bsFundInfo.DefaultPaymentTime).Date.ToShortDateString());       
                    switch (bsType)
                    {
                        case BalanceSheetType.ManualMonthlyPayBalanceSheet:
                        case BalanceSheetType.MonthlyPayBalanceSheet:
                        case BalanceSheetType.ManualWeeklyPayWeekBalanceSheet:
                        case BalanceSheetType.WeeklyPayWeekBalanceSheet:
                        case BalanceSheetType.FlexibleToHouseBalanceSheet:
                        case BalanceSheetType.MonthlyToHouseBalanceSheet:
                        case BalanceSheetType.WeeklyToHouseBalanceSheet:
                        case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                        case BalanceSheetType.FlexiblePayBalanceSheet:
                        case BalanceSheetType.WeeklyPayBalanceSheet:
                            _accountsPayable = GetAccountsPayable(bsFundInfo, amountPayableCallback);
                            _accountsPaid = GetAccountsPaid(bsFundInfo);
                            _transferStatusDescription =
                                GetTransferStatusDescription((BalanceSheetType) bsFundInfo.BalanceSheetType,
                                                             bsFundInfo.IsPastConfirmedReadyToPay,
                                                             bsFundInfo.IsConfirmedReadyToPay,
                                                             bsFundInfo.IsTransferComplete);
                            _transferCompletionDescription =
                                GetTransferCompletionDescription((BalanceSheetType) bsFundInfo.BalanceSheetType,
                                                                 bsFundInfo.IsPastConfirmedReadyToPay,
                                                                 bsFundInfo.IsTransferComplete, bsFundInfo.ResponseTime,
                                                                 bsFundInfo.BsIntervalEnd,
                                                                 bsFundInfo.DealGuid, bsFundInfo.StoreGuid);
                            _memo = bsFundInfo.Memo;
                            if (int.Equals(0, _accountsPayable))
                            {
                                _receiverTitle = string.Empty;
                                _receiverCompanyId = string.Empty;
                                _receiverAccountBankNo = string.Empty;
                                _receiverAccountBranchNo = string.Empty;
                                _receiverAccountNo = string.Empty;
                            }
                            else
                            {
                                if (bsFundInfo.IsTransferComplete.GetValueOrDefault(false))
                                {
                                    _receiverTitle = bsFundInfo.ReceiverTitle;
                                    _receiverCompanyId = bsFundInfo.ReceiverCompanyId;
                                    _receiverAccountBankNo = bsFundInfo.ReceiverBankNo;
                                    _receiverAccountBranchNo = bsFundInfo.ReceiverBranchNo;
                                    _receiverAccountNo = bsFundInfo.ReceiverAccountNo;
                                }
                                else
                                {
                                    _receiverTitle = bsFundInfo.AccountTitle;
                                    _receiverCompanyId = bsFundInfo.CompanyId;
                                    _receiverAccountBankNo = bsFundInfo.BankNo;
                                    _receiverAccountBranchNo = bsFundInfo.BranchNo;
                                    _receiverAccountNo = bsFundInfo.AccountNo;
                                }
                            }
                            break;
                        case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                        case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
                            ViewBalanceSheetFundsTransferPponCollection weekBsTransfers = GetWbsFundTransfers(bsFundInfo);
                            ViewBalanceSheetFundsTransferPpon representingWbs = GetRepresentingWbs(bsFundInfo);
                            _accountsPayable = GetAccountsPayable(weekBsTransfers);
                            _accountsPaid = GetAccountsPaid(weekBsTransfers);
                            _transferStatusDescription =
                                GetTransferStatusDescription((BalanceSheetType) bsFundInfo.BalanceSheetType,
                                                             bsFundInfo.IsPastConfirmedReadyToPay,
                                                             bsFundInfo.IsConfirmedReadyToPay,
                                                             bsFundInfo.IsTransferComplete);
                            _transferCompletionDescription =
                                GetTransferCompletionDescription((BalanceSheetType) bsFundInfo.BalanceSheetType,
                                                                 bsFundInfo.IsPastConfirmedReadyToPay,
                                                                 bsFundInfo.IsTransferComplete, bsFundInfo.ResponseTime,
                                                                 bsFundInfo.BsIntervalEnd,
                                                                 bsFundInfo.DealGuid, bsFundInfo.StoreGuid);
                            _memo = bsFundInfo.Memo;
                            if (int.Equals(0, _accountsPayable))
                            {
                                _receiverTitle = string.Empty;
                                _receiverCompanyId = string.Empty;
                                _receiverAccountBankNo = string.Empty;
                                _receiverAccountBranchNo = string.Empty;
                                _receiverAccountNo = string.Empty;
                            }
                            else
                            {
                                if (representingWbs.IsTransferComplete.GetValueOrDefault(false))
                                {
                                    _receiverTitle = representingWbs.ReceiverTitle;
                                    _receiverCompanyId = representingWbs.ReceiverCompanyId;
                                    _receiverAccountBankNo = representingWbs.ReceiverBankNo;
                                    _receiverAccountBranchNo = representingWbs.ReceiverBranchNo;
                                    _receiverAccountNo = representingWbs.ReceiverAccountNo;
                                }
                                else
                                {
                                    _receiverTitle = representingWbs.AccountTitle;
                                    _receiverCompanyId = representingWbs.CompanyId;
                                    _receiverAccountBankNo = representingWbs.BankNo;
                                    _receiverAccountBranchNo = representingWbs.BranchNo;
                                    _receiverAccountNo = representingWbs.AccountNo;
                                }
                            }
                            break;
                        default:
                            useDefaultValue = true;
                            break;
                    }
                }

                #endregion
            }
            else
            {
                ViewBalanceSheetFundTransfer bsFundInfo = _ap.ViewBalanceSheetFundTransferGetByBalanceSheetId(balanceSheetId);
                if (bsFundInfo == null
                    || !bsFundInfo.IsLoaded)
                {
                    useDefaultValue = true;
                }

                if (!useDefaultValue)
                {
                    _defaultPaymentTime = (bsFundInfo.DefaultPaymentTime == null ? "" : Convert.ToDateTime(bsFundInfo.DefaultPaymentTime).Date.ToShortDateString());
                    switch (bsType)
                    {
                        case BalanceSheetType.ManualMonthlyPayBalanceSheet:
                        case BalanceSheetType.ManualWeeklyPayWeekBalanceSheet:
                        case BalanceSheetType.ManualTriggered:
                        case BalanceSheetType.ManualLumpSumPayBalanceSheet:
                            _accountsPayable = bsFundInfo.EstAmount.Value;
                            _accountsPaid = GetAccountsPaid(bsFundInfo);
                            if (bsType != BalanceSheetType.ManualTriggered)
                            {
                                _transferStatusDescription = GetTransferStatusDescription((BalanceSheetType)bsFundInfo.BalanceSheetType, bsFundInfo.IsPastConfirmedReadyToPay, bsFundInfo.IsConfirmedReadyToPay, bsFundInfo.IsTransferComplete);
                            }
                            else
                            {
                                _transferStatusDescription = string.Empty;
                            }
                            _transferCompletionDescription =
                                GetTransferCompletionDescription((BalanceSheetType) bsFundInfo.BalanceSheetType,
                                                                 bsFundInfo.IsPastConfirmedReadyToPay,
                                                                 bsFundInfo.IsTransferComplete, bsFundInfo.TransferTime,
                                                                 bsFundInfo.IntervalEnd, 
                                                                 bsFundInfo.ProductGuid, bsFundInfo.StoreGuid);
                            _memo = bsFundInfo.FundMemo;
                            if (int.Equals(0, bsFundInfo.EstAmount))
                            {
                                _receiverTitle = string.Empty;
                                _receiverCompanyId = string.Empty;
                                _receiverAccountBankNo = string.Empty;
                                _receiverAccountBranchNo = string.Empty;
                                _receiverAccountNo = string.Empty;
                            }
                            else
                            {
                                if (bsFundInfo.IsTransferComplete.GetValueOrDefault((false)))
                                {
                                    _receiverTitle = bsFundInfo.ReceiverAccountTitle;
                                    _receiverCompanyId = bsFundInfo.ReceiverCompanyId;
                                    _receiverAccountBankNo = bsFundInfo.ReceiverBankNo;
                                    _receiverAccountBranchNo = bsFundInfo.ReceiverBranchNo;
                                    _receiverAccountNo = bsFundInfo.ReceiverAccountNo;
                                }
                                else
                                {
                                    _receiverTitle = bsFundInfo.DealAccountTitle;
                                    _receiverCompanyId = bsFundInfo.DealCompanyId;
                                    _receiverAccountBankNo = bsFundInfo.DealBankNo;
                                    _receiverAccountBranchNo = bsFundInfo.DealBranchNo;
                                    _receiverAccountNo = bsFundInfo.DealAccountNo;
                                }
                            }
                            break;
                        case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
                            ViewBalanceSheetFundTransferCollection weekBsTransfers = GetWbsFundTransfers(bsFundInfo);
                            ViewBalanceSheetFundTransfer representingWbs = GetRepresentingWbs(bsFundInfo);
                            _accountsPayable = GetAccountsPayable(weekBsTransfers);
                            _accountsPaid = GetAccountsPaid(weekBsTransfers);
                            _transferStatusDescription = GetTransferStatusDescription((BalanceSheetType) bsFundInfo.BalanceSheetType,
                                                             bsFundInfo.IsPastConfirmedReadyToPay,
                                                             bsFundInfo.IsConfirmedReadyToPay,
                                                             bsFundInfo.IsTransferComplete);
                            _transferCompletionDescription =
                                GetTransferCompletionDescription((BalanceSheetType) bsFundInfo.BalanceSheetType,
                                                                 bsFundInfo.IsPastConfirmedReadyToPay,
                                                                 bsFundInfo.IsTransferComplete, bsFundInfo.TransferTime,
                                                                 bsFundInfo.IntervalEnd,
                                                                 bsFundInfo.ProductGuid, bsFundInfo.StoreGuid);
                            _memo = bsFundInfo.FundMemo;
                            if (int.Equals(0, _accountsPayable))
                            {
                                _receiverTitle = string.Empty;
                                _receiverCompanyId = string.Empty;
                                _receiverAccountBankNo = string.Empty;
                                _receiverAccountBranchNo = string.Empty;
                                _receiverAccountNo = string.Empty;
                            }
                            else
                            {
                                if (representingWbs.IsTransferComplete.GetValueOrDefault(false))
                                {
                                    _receiverTitle = representingWbs.ReceiverAccountTitle;
                                    _receiverCompanyId = representingWbs.ReceiverCompanyId;
                                    _receiverAccountBankNo = representingWbs.ReceiverBankNo;
                                    _receiverAccountBranchNo = representingWbs.ReceiverBranchNo;
                                    _receiverAccountNo = representingWbs.ReceiverAccountNo;
                                }
                                else
                                {
                                    _receiverTitle = representingWbs.DealAccountTitle;
                                    _receiverCompanyId = representingWbs.DealCompanyId;
                                    _receiverAccountBankNo = representingWbs.DealBankNo;
                                    _receiverAccountBranchNo = representingWbs.DealBranchNo;
                                    _receiverAccountNo = representingWbs.DealAccountNo;
                                }
                            }
                            break;
                        default:
                            throw new InvalidOperationException();
                    }
                }
            }

            if (useDefaultValue)
            {
                _accountsPayable = 0;
                _accountsPaid = null;
                _transferStatusDescription = string.Empty;
                _transferCompletionDescription = string.Empty;
                _receiverTitle = string.Empty;
                _receiverCompanyId = string.Empty;
                _receiverAccountBankNo = string.Empty;
                _receiverAccountBranchNo = string.Empty;
                _receiverAccountNo = string.Empty;
            }
        }

        /// <summary>
        /// 抓取Demo檔次匯款資料使用
        /// </summary>
        public FundTransferInfo(BalanceSheetType bsType, bool demo = true)
        {
            if (!demo)
            {
                return;
            }

            _accountsPayable = 600;
            _receiverTitle = "17Life";
            _receiverCompanyId = "12345678";
            _receiverAccountBankNo = "012";
            _receiverAccountBranchNo = "0123";
            _receiverAccountNo = "12345678901234";
            switch (bsType)
            {
                case BalanceSheetType.WeeklyPayWeekBalanceSheet:
                    _transferStatusDescription = "請款中";
                    _transferCompletionDescription = "週結已付款";
                    break;
                case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                    _transferStatusDescription = string.Empty;
                    _transferCompletionDescription = DateTime.Today.AddDays(-2).ToString("yyyy/MM/dd");
                    break;
            }
        }

        private int? GetAccountsPaid(ViewBalanceSheetFundTransferCollection weekBsTransfers)
        {
            var paidWbs = weekBsTransfers
                .Where(x => x.IsTransferComplete.GetValueOrDefault(false)).ToList();

            if (paidWbs.Count > 0)
            {
                return paidWbs.Sum(x => x.TransferAmount);
            }

            return null;
        }

        private int GetAccountsPayable(ViewBalanceSheetFundTransferCollection weekBsTransfers)
        {
            return weekBsTransfers.Sum(x => x.EstAmount).Value;
        }
        

        private ViewBalanceSheetFundTransfer GetRepresentingWbs(ViewBalanceSheetFundTransfer bsFundInfo)
        {
            ViewBalanceSheetFundTransferCollection wbs = GetWbsFundTransfers(bsFundInfo);

            var result = wbs
                .Where(x => x.IsTransferComplete.GetValueOrDefault(false))
                .OrderByDescending(x => x.BalanceSheetId)
                .FirstOrDefault();

            if (result == null && _wbsFundInfos.Count > 0)
            {
                result = wbs.OrderByDescending(x => x.BalanceSheetId).First();
            }

            return result;
        }

        private ViewBalanceSheetFundTransferCollection _wbsFundInfos;
        private ViewBalanceSheetFundTransferCollection GetWbsFundTransfers(ViewBalanceSheetFundTransfer bsFundInfo)
        {
            if (_wbsFundInfos == null)
            {
                BalanceSheetCollection wbs = _ap.BalanceSheetGetWeekBalanceSheetsByMonth(bsFundInfo.ProductGuid,
                                                                                         (BusinessModel)
                                                                                         bsFundInfo.ProductType,
                                                                                         bsFundInfo.Year.Value,
                                                                                         bsFundInfo.Month.Value);
                List<int> wbsIds = wbs.Where(x => x.StoreGuid == bsFundInfo.StoreGuid).Select(x => x.Id).ToList();
                _wbsFundInfos = _ap.ViewBalanceSheetFundTransferGetListByBalanceSheetIds(wbsIds) ?? new ViewBalanceSheetFundTransferCollection();
            }
            return _wbsFundInfos;
        }

        private bool IsAch(BalanceSheetType bsType)
        {
            switch (bsType)
            {
                case BalanceSheetType.MonthlyPayBalanceSheet:
                case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                case BalanceSheetType.WeeklyPayWeekBalanceSheet:
                case BalanceSheetType.FlexibleToHouseBalanceSheet:
                case BalanceSheetType.MonthlyToHouseBalanceSheet:
                case BalanceSheetType.WeeklyToHouseBalanceSheet:
                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                case BalanceSheetType.FlexiblePayBalanceSheet:
                case BalanceSheetType.WeeklyPayBalanceSheet:
                    return true;
                case BalanceSheetType.Unknown:
                    throw new InvalidOperationException();
                default:
                    return false;
            }
        }

        #endregion

        #region private members
        private decimal? _balanceSheetAccountsPayable;
        private int GetAccountsPayable(ViewBalanceSheetFundsTransferPpon bsFundInfo, Func<TotalAmountDetail> amountPayableCallback)
        {
            if (bsFundInfo.TotalSum.HasValue)
            {
                return decimal.ToInt32(bsFundInfo.TotalSum.Value);
            }

            if (!_balanceSheetAccountsPayable.HasValue)
            {
                _balanceSheetAccountsPayable = amountPayableCallback().ReceiptAmount;
            }

            return decimal.ToInt32(_balanceSheetAccountsPayable.Value);
        }

        private int? GetAccountsPaid(ViewBalanceSheetFundsTransferPpon bsFundInfo)
        {
            if(!bsFundInfo.IsTransferComplete.GetValueOrDefault(false))
            {
                //匯款前 TransferAmount 也可能有值
                return null;
            }
           
            if (bsFundInfo.TransferAmount.HasValue)
            {
                return decimal.ToInt32(bsFundInfo.TransferAmount.Value);
            }
            return null;
        }

        private int? GetAccountsPaid(ViewBalanceSheetFundTransfer bsFundInfo)
        {
            if (!bsFundInfo.IsTransferComplete.GetValueOrDefault(false))
            {                
                return null;
            }
            
            if (bsFundInfo.TransferAmount.HasValue)
            {
                return decimal.ToInt32(bsFundInfo.TransferAmount.Value);
            }
            return null;
        }
        
        private ViewBalanceSheetFundsTransferPpon GetRepresentingWbs(ViewBalanceSheetFundsTransferPpon mbs)
        {
            if(_wbsInfos == null)
            {
                LoadWbsInfos(mbs);
            }

            var result = _wbsInfos
                .Where(x => x.IsTransferComplete.GetValueOrDefault(false))
                .OrderByDescending(x => x.BsId)
                .FirstOrDefault();
            if (result == null)
            {
                result = _wbsInfos.OrderByDescending(x => x.BsId).First();
            }
            return result;
        }
        
        private ViewBalanceSheetFundsTransferPponCollection GetWbsFundTransfers(ViewBalanceSheetFundsTransferPpon mbs)
        {
            if (_wbsInfos == null)
            {
                LoadWbsInfos(mbs);
            }
            return _wbsInfos;
        }

        private int? GetAccountsPaid(ViewBalanceSheetFundsTransferPponCollection wbsInfos)
        {
            var paidWbs = wbsInfos
                .Where(x => x.IsTransferComplete.GetValueOrDefault(false)).ToList();

            if (paidWbs.Count > 0)
            {
                return decimal.ToInt32(paidWbs.Sum(x => x.TransferAmount).Value);
            }

            return null;
        }

        private int GetAccountsPayable(ViewBalanceSheetFundsTransferPponCollection wbsInfos)
        {
            return decimal.ToInt32(wbsInfos.Sum(x => x.TotalSum).Value);
        }

        /// <summary>
        /// 取得 "匯款狀態" 的描述. 
        /// </summary>
        private string GetTransferStatusDescription(BalanceSheetType bsType, bool? isPastConfirmedReadyToPay, bool isConfirmedReadyToPay, bool? isTransferComplete)
        {
            string result;   //匯款狀態
            
            if (bsType == BalanceSheetType.ManualWeeklyPayMonthBalanceSheet
                || bsType == BalanceSheetType.WeeklyPayMonthBalanceSheet)
            {
                #region 週結月對帳單

                if (this.AccountsPaid == 0)
                {
                    result = string.Empty;
                }
                else
                {
                    if (isPastConfirmedReadyToPay.GetValueOrDefault(true))
                    {
                        if (isConfirmedReadyToPay)
                        {
                            result = "已完成";
                        }
                        else
                        {
                            result = "請盡速將發票或收據寄回，以免影響後續請款。";
                        }
                    }
                    else
                    {
                        result = "尚有前期未回發票或收據。";
                    }
                }
                
                #endregion
            }
            else if (bsType == BalanceSheetType.ManualMonthlyPayBalanceSheet)
            {
                #region 月結月對帳單

                if (this.AccountsPayable > 0)
                {
                    if (!isPastConfirmedReadyToPay.GetValueOrDefault(true))
                    {
                        result = "尚有前期未回發票或收據。";
                    }
                    else
                    {
                        if (isConfirmedReadyToPay)
                        {
                            if (isTransferComplete.GetValueOrDefault(false))
                            {
                                result = "已完成";
                            }
                            else
                            {
                                result = "請款中";
                            }
                        }
                        else
                        {
                            result = "請盡速將發票或收據寄回，以免影響後續請款。";
                        }
                    }
                }
                else
                {
                    result = string.Empty;
                }

                #endregion
            }
            else if (bsType == BalanceSheetType.ManualWeeklyPayWeekBalanceSheet
                || bsType == BalanceSheetType.WeeklyPayWeekBalanceSheet)
            {
                #region 週對帳單

                //todo: 可以考慮改成 "預估付款時間" 
                result = string.Empty;

                #endregion
            }
            else if (bsType == BalanceSheetType.FlexibleToHouseBalanceSheet
                || bsType == BalanceSheetType.WeeklyToHouseBalanceSheet
                || bsType == BalanceSheetType.FortnightlyToHouseBalanceSheet
                || bsType == BalanceSheetType.MonthlyToHouseBalanceSheet
                || bsType == BalanceSheetType.MonthlyPayBalanceSheet
                || bsType == BalanceSheetType.FlexiblePayBalanceSheet
                || bsType == BalanceSheetType.WeeklyPayBalanceSheet)
            {
                #region 先收單據後付款:週/月/彈性出帳對帳單

                if (this.AccountsPayable > 0)
                {
                    if (isConfirmedReadyToPay)
                    {
                        if (isTransferComplete.GetValueOrDefault(false))
                        {
                            result = "已完成";
                        }
                        else
                        {
                            result = "請款中";
                        }
                    }
                    else
                    {
                        result = "請將發票或收據寄回，以利請款。";
                    }
                }
                else
                {
                    result = string.Empty;
                }

                #endregion
            }
            else
            {
                result = string.Empty;
            }

            return result;
        }

        /// <summary>
        /// 取得 "完成付款時間" 的描述. 此描述會出現在前端 "對帳查詢" 裡的"匯款資訊" 區域內
        /// </summary>
        private string GetTransferCompletionDescription(BalanceSheetType bsType, bool? isPastConfirmedReadyToPay, bool? isTransferComplete, DateTime? responseTime, DateTime? bsIntervalEnd, Guid productGuid, Guid? storeGuid)
        {
            string result;
            
            if (bsType == BalanceSheetType.ManualWeeklyPayMonthBalanceSheet
                || bsType == BalanceSheetType.WeeklyPayMonthBalanceSheet)
            {
                #region 週結月對帳單
                
                result = this.AccountsPayable > 0 ? "週結已付款" : "無須付款";
                
                #endregion
            }
            else if (bsType == BalanceSheetType.ManualMonthlyPayBalanceSheet)
            {
                #region 月結月對帳單

                if(this.AccountsPayable > 0)
                {
                    if (!isPastConfirmedReadyToPay.GetValueOrDefault(true))
                    {
                        result = "暫停付款";
                    }
                    else
                    {
                        if (isTransferComplete.GetValueOrDefault(false))
                        {
                            result = responseTime.HasValue
                                ? responseTime.Value.ToString("yyyy/MM/dd")
                                : string.Empty;
                        }
                        else
                        {
                            result = "尚未付款";
                        }
                    }
                }
                else
                {
                    result = "無須付款";
                }

                #endregion
            }
            else if (bsType == BalanceSheetType.ManualWeeklyPayWeekBalanceSheet
                || bsType == BalanceSheetType.WeeklyPayWeekBalanceSheet)
            {
                #region 週對帳單

                if(int.Equals(0, this.AccountsPayable))
                {
                    result = "無須付款";
                }
                else
                {
                    if (isTransferComplete.GetValueOrDefault(false))
                    {
                        result = responseTime.HasValue
                            ? responseTime.Value.ToString("yyyy/MM/dd")
                            : string.Empty;
                    }
                    else
                    {
                        DateTime? stopPayTime = BalanceSheetService.GetStopPaymentTime(productGuid, storeGuid);
                        if (stopPayTime.HasValue
                            && bsIntervalEnd > stopPayTime)
                        {
                            result = "暫停付款"; 
                        }
                        else
                        {
                            result = "尚未付款";
                        }
                    }
                }

                #endregion
            }
            else if (bsType == BalanceSheetType.ManualTriggered)
            {
                #region 人工觸發對帳單

                if (int.Equals(0, this.AccountsPayable))
                {
                    result = "無須付款";
                }
                else
                {
                    if (isTransferComplete.GetValueOrDefault(false))
                    {
                        result = responseTime.HasValue
                            ? responseTime.Value.ToString("yyyy/MM/dd")
                            : string.Empty;
                    }
                    else
                    {
                        result = "尚未付款";
                    }
                }

                #endregion 人工觸發對帳單
            }
            else if (bsType == BalanceSheetType.FlexibleToHouseBalanceSheet
                || bsType == BalanceSheetType.WeeklyToHouseBalanceSheet
                || bsType == BalanceSheetType.FortnightlyToHouseBalanceSheet
                || bsType == BalanceSheetType.MonthlyToHouseBalanceSheet
                || bsType == BalanceSheetType.MonthlyPayBalanceSheet
                || bsType == BalanceSheetType.FlexiblePayBalanceSheet
                || bsType == BalanceSheetType.WeeklyPayBalanceSheet)
            {
                #region 先收單據後付款:週/月/彈性出帳對帳單

                if (int.Equals(0, this.AccountsPayable))
                {
                    result = "無須付款";
                }
                else
                {
                    if (isTransferComplete.GetValueOrDefault(false))
                    {
                        result = responseTime.HasValue
                            ? responseTime.Value.ToString("yyyy/MM/dd")
                            : string.Empty;
                    }
                    else
                    {
                        result = "尚未付款";
                    }
                }

                #endregion
            }
            else
            {
                result = string.Empty;
            }

            return result;
        }
        
        private ViewBalanceSheetFundsTransferPponCollection _wbsInfos;
        private void LoadWbsInfos(ViewBalanceSheetFundsTransferPpon mbs)
        {
            if (_wbsInfos == null)
            {
                BalanceSheetCollection wbs = _ap.BalanceSheetGetWeekBalanceSheetsByMonth(mbs.DealGuid, (BusinessModel)mbs.ProductType, mbs.BsYear.Value, mbs.BsMonth.Value);
                List<int> wbsIds = wbs.Where(x => x.StoreGuid == mbs.StoreGuid).Select(x => x.Id).ToList();
                _wbsInfos = _ap.ViewBalanceSheetFundsTransferPponGetListByBalanceSheetIds(wbsIds);
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using log4net;
using log4net.Core;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// 處理與整頓好康檔次的各頻道、分類及排序等資料
    /// </summary>
    public class PponDealPreviewManager
    {
        private const int _LAST_DAT_CATEGORY_ID = 318; //檔次屬於"最後一天"的Cid
        public const int _INSTANT_BUY_CATEGORY_ID = 100001; //即買即用
        public const int _LAST_DAY_CATEGORY_ID = 100002; //最後一天
        public const int _MAIN_THEME_CATEGORY_ID = 3001; //虛擬-主題活動

        private static IPponProvider pp;
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public static DateTime StartTime = DateTime.Now;


        private static ILog logger = LogManager.GetLogger(typeof(PponDealPreviewManager));
        private static object _pponDealPreviewDatasLock = new object();
        private static object _reloadManagerLock = new object();
        private static List<PponDealPreviewData> _pponDealPreviewDatas;
        private static List<PponDealPreviewData> PponDealPreviewDatas
        {
            get
            {
                lock (_pponDealPreviewDatasLock)
                {
                    return _pponDealPreviewDatas;
                }
            }
            set
            {
                lock (_pponDealPreviewDatasLock)
                {
                    _pponDealPreviewDatas = value;
                }
            }
        }

        /// <summary>
        /// 依據日期保存每天 各分類的檔次數量
        /// </summary>
        private static object _dailyDealCountNodesLock = new object();
        private static List<DailyCategoryDealCountNode> _dailyDealCountNodes;
        private static List<DailyCategoryDealCountNode> DailyDealCountNodes
        {
            get
            {
                lock (_dailyDealCountNodesLock)
                {
                    return _dailyDealCountNodes;
                }
            }
            set
            {
                lock (_dailyDealCountNodesLock)
                {
                    _dailyDealCountNodes = value;
                }
            }
        }

        /// <summary>
        /// 依據日期保存每天 各分類的檔次數量
        /// </summary>
        private static object _dailySpecialCategoryCountNodesLock = new object();
        private static List<DailyCategoryDealCountNode> _dailySpecialCategoryCountNodes;
        private static List<DailyCategoryDealCountNode> DailySpecialCategoryCountNodes
        {
            get
            {
                lock (_dailySpecialCategoryCountNodesLock)
                {
                    return _dailySpecialCategoryCountNodes;
                }
            }
            set
            {
                lock (_dailySpecialCategoryCountNodesLock)
                {
                    _dailySpecialCategoryCountNodes = value;
                }
            }
        }

        private static object _dailyPponChannelCategoryTreesLock = new object();
        private static ApiCategoryTypeNode _dailyPponChannelCategoryTrees;
        private static ApiCategoryTypeNode DailyPponChannelCategoryTrees
        {
            get
            {
                lock (_dailyPponChannelCategoryTreesLock)
                {
                    return _dailyPponChannelCategoryTrees;
                }
            }
            set
            {
                lock (_dailyPponChannelCategoryTreesLock)
                {
                    _dailyPponChannelCategoryTrees = value;
                }
            }
        }

        public static ApiCategoryTypeNode GetPponChannelCategoryTree()
        {
            if (DailyPponChannelCategoryTrees == null || DailyPponChannelCategoryTrees.CategoryNodes.Count == 0)
            {
                //發現資料庫讀取失敗，造成頻道分類無資料，這邊做一個事後修正。
                lock (_reloadManagerLock)
                {
                    if (DailyPponChannelCategoryTrees == null || DailyPponChannelCategoryTrees.CategoryNodes.Count == 0)
                    {
                        logger.Warn("發現資料庫讀取失敗，造成頻道分類無資料，這邊做一個事後修正。");
                        ReloadManager();
                    }
                }
            }

            ApiCategoryTypeNode result = new ApiCategoryTypeNode(CategoryType.PponChannel);
            int seq = 0;
            foreach (int channelIdNow in CategoryManager.AppChannelIdNew)
            {
                var categoryNode = DailyPponChannelCategoryTrees.CategoryNodes.FirstOrDefault(t => t.CategoryId == channelIdNow);
                if (categoryNode == null)
                {
                    continue;
                }
                if (categoryNode.Seq != seq)
                {
                    categoryNode.Seq = seq;
                }
                result.CategoryNodes.Add(categoryNode);
                seq++;
            }
            return result;
        }

        static PponDealPreviewManager()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            ReloadManager();
        }
        /// <summary>
        /// 異動PponDealPreviewData資料，並傳入異動時間，只有當異動時間不相同時，才會執行資料的更新
        /// </summary>
        public static void ReloadManager()
        {
            DateTime baseDate = DateTime.ParseExact(DateTime.Today.ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
            DateTime startDate = baseDate.AddDays(-1);
            DateTime endDate = baseDate.AddDays(1);

            using (StopwatchLog log = new StopwatchLog("PponDealPreviewManager", "費時 {0} 秒")
                .SetLog(Level.Warn, t => (DateTime.Now - StartTime).TotalMinutes < 10))
            {
                LoadPponDealPreviewData(startDate, endDate);
                log.Log("LoadPponDealPreviewData");
                LoadChannelDealCountNodes(startDate, endDate);
                log.Log("LoadChannelDealCountNodes");
                LoadChannelSpecialCategoryCountNodes(startDate, endDate);
                log.Log("LoadChannelSpecialCategoryCountNodes");
                LoadPponChannelCategoryTree();
                log.Log("LoadPponChannelCategoryTree");
            }
        }

        #region private static method

        private static void LoadPponDealPreviewData(DateTime startDate, DateTime endDate)
        {
            //先建立暫時存放查詢結果的物件
            List<PponDealPreviewData> tempPponDealPreviewDatas = new List<PponDealPreviewData>();
            //預設查詢昨天與今天的檔次資料                        

            #region 由ViewPponDealManager檔次資料為基準產生PponDealPreviewData的資料
            //由ViewPponDealManager檔次資料為基準產生PponDealPreviewData的資料
            foreach (var pponDeal in ViewPponDealManager.DefaultManager.ViewPponDealGetList(excludeSubDeals: true))
            {
                PponDealPreviewData previewData = new PponDealPreviewData();
                previewData.DealId = pponDeal.UniqueId.GetValueOrDefault();
                previewData.BusinessHourGuid = pponDeal.BusinessHourGuid;
                previewData.BusinessOrderTimeS = pponDeal.BusinessHourOrderTimeS;
                previewData.BusinessOrderTimeE = pponDeal.BusinessHourOrderTimeE;
                previewData.Price = pponDeal.ItemPrice;
                previewData.IsHiddenInRecommendDeal = pponDeal.IsHiddenInRecommendDeal;
                previewData.IsSoldOut = pponDeal.IsSoldOut;
                previewData.HasCategoryIdList = new HashSet<int>(pponDeal.CategoryIds);
                previewData.HasIconList = new HashSet<DealLabelSystemCode>(pponDeal.LabelIcons);

                //當檔次符合"最後一天"的條件，將"最後一天"的Cid加進List裡，讓Web版首頁可以判斷篩選
                if (DateTime.Now >= previewData.BusinessOrderTimeE.AddDays(-1) && previewData.BusinessOrderTimeE > DateTime.Now)
                {
                    previewData.HasCategoryIdList.Add(_LAST_DAT_CATEGORY_ID);
                }
                tempPponDealPreviewDatas.Add(previewData);
            }
            #endregion 由ViewPponDealManager檔次資料為基準產生PponDealPreviewData的資料

            PponDealPreviewDatas = tempPponDealPreviewDatas;
        }

        private static void AdujstPiinlifeVisaDeals(ViewCategoryDealBaseCollection categoryDeals)
        {
            int visaCategoryId = 158;
            List<ViewCategoryDealBase> visaDeals = categoryDeals.Where(t => t.Cid == visaCategoryId).ToList();
            for (int i = 0; i < visaDeals.Count; i++)
            {
                ViewCategoryDealBase visaDeal = visaDeals[i];
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(visaDeal.Bid, true);
                if (string.IsNullOrEmpty(vpd.LabelIconList) == false)
                {
                    string visaIcon = ((int)DealLabelSystemCode.Visa).ToString();
                    string[] icons = vpd.LabelIconList.Split(',').Where(t => t != visaIcon).ToArray();
                    vpd.LabelIconList = string.Join(",", icons);
                }
                if ((DateTime.Now - vpd.BusinessHourOrderTimeS).Days < 3)
                {
                    //3天內  要刪掉該檔在其他分類的資料
                    List<ViewCategoryDealBase> filterDeals = categoryDeals.Where(
                        t => t.Bid == visaDeal.Bid &&
                            t.Cid != visaCategoryId &&
                            t.Cid != CategoryManager.Default.Piinlife.CategoryId)
                        .ToList();
                    foreach (ViewCategoryDealBase filterDeal in filterDeals)
                    {
                        categoryDeals.Remove(filterDeal);
                    }
                    if (string.IsNullOrEmpty(vpd.LabelIconList))
                    {
                        vpd.LabelIconList = ((int)DealLabelSystemCode.Visa).ToString();
                    }
                    else
                    {
                        vpd.LabelIconList = string.Format("{0},{1}",
                            vpd.LabelIconList, ((int)DealLabelSystemCode.Visa).ToString());
                    }
                    //vpd.IsVisaNow = true;
                }
                else
                {
                    //3天後 要刪掉自己這個 visa 分類的資料
                    categoryDeals.Remove(visaDeal);
                    //vpd.IsVisaNow = false;
                }
            }
        }

        private static void LoadChannelDealCountNodes(DateTime startDate, DateTime endDate)
        {
            DailyDealCountNodes = LoadDailyDealCountNodesBy(CategoryType.DealCategory, startDate, endDate);
        }

        private static void LoadChannelSpecialCategoryCountNodes(DateTime startDate, DateTime endDate)
        {
            DailySpecialCategoryCountNodes = LoadDailyDealCountNodesBy(CategoryType.DealSpecialCategory, startDate,
                                                                        endDate);
        }
        /// <summary>
        /// 設定app使用的PponChannelCategoryTree，只有可顯示於前台部分的資料
        /// </summary>
        private static void LoadPponChannelCategoryTree()
        {
            ApiCategoryTypeNode baseTree = ApiCategoryTypeNode.CreateApiCategoryTypeNode(
                CategoryManager.PponChannelCategoryTree, true);

            //判斷各分類是否有檔次資料
            foreach (var categoryNode in baseTree.CategoryNodes)
            {
                SetApiCategoryNodeExistsField(categoryNode, PponDealPreviewDatas);
            }
            //填入全部的項目
            InsertAllNodesToCategoryNodes(baseTree);
            //強制過瀘實體的即買即用分類，避免跟虛擬即買即用重複
            baseTree.CategoryNodes =
                baseTree.CategoryNodes.Where(x => x.CategoryId != _INSTANT_BUY_CATEGORY_ID).ToList();

            //即買即用
            ApiCategoryNode instantBuyChannel = CreateInstantBuyVirutalChannel(baseTree);
            DailyPponChannelCategoryTrees = baseTree.Copy();
            DailyPponChannelCategoryTrees.CategoryNodes.Add(instantBuyChannel);

            //主題活動虛擬頻道
            ApiCategoryNode mainTheme3001Channel = CreateMainTheme3001VirutalChannel();
            
            DailyPponChannelCategoryTrees.CategoryNodes.Add(mainTheme3001Channel);            
        }

        private static ApiCategoryNode CreateInstantBuyVirutalChannel(ApiCategoryTypeNode baseTree)
        {
            //即買即用是從美食頻道挑出來
            var pponCategoryNode =
                baseTree.Copy().CategoryNodes.First(x => x.CategoryId == CategoryManager.Default.PponDeal.CategoryId);
            //判斷各分類是否有檔次資料 //DealTags:4, Categories:137 即買即用
            List<PponDealPreviewData> instantBuyDeals = PponDealPreviewDatas.Where(
                x => x.HasCategoryIdList.Contains(4) || x.HasCategoryIdList.Contains(137)).ToList();
            SetApiCategoryNodeExistsField(pponCategoryNode, instantBuyDeals);

            //即買即用--虛擬頻道  id:100001            
            ApiCategoryNode instantBuyChannel = new ApiCategoryNode(new Category
            {
                Id = _INSTANT_BUY_CATEGORY_ID,
                Name = "即買即用",
            });


            //只顯示NodeType CategoryType.PponChannelArea:地區
            instantBuyChannel.NodeDatas = pponCategoryNode.NodeDatas.Where(x => x.NodeType.Equals(CategoryType.PponChannelArea)).ToList();
            instantBuyChannel.ExistsDeal = instantBuyDeals.Count > 0;
            return instantBuyChannel;
        }

        private static ApiCategoryNode CreateMainTheme3001VirutalChannel()
        {
            //主題活動--虛擬頻道  id:3001            
            ApiCategoryNode result = new ApiCategoryNode(new Category
            {
                Id = _MAIN_THEME_CATEGORY_ID,
                Name = "主題活動"
            });

            result.NodeDatas = new List<ApiCategoryTypeNode>();
            result.ExistsDeal = false;
            return result;
        }

        private static void SetApiCategoryNodeExistsField(ApiCategoryNode node, List<PponDealPreviewData> pponList)
        {
            DateTime dealDate = DateTime.Now;
            if (node.IsFinal)
            {
                //於查詢日期，商品處於銷售狀態，且目前的categoryId包含在所需的區間內
                // 因為 cache 會維持一段時間，  所以從cache更新的時間開始，1天內會上檔的，都應該顯示分類
                node.ExistsDeal = pponList.Any(x => x.BusinessOrderTimeS <= dealDate.AddDays(1) && dealDate < x.BusinessOrderTimeE
                                                                && x.HasCategoryIdList.Contains(node.CategoryId));
            }
            else
            {
                List<PponDealPreviewData> subPponList = pponList.Where(x => x.HasCategoryIdList.Contains(node.CategoryId)).ToList();
                if (subPponList.Count > 0)
                {
                    node.ExistsDeal = true;
                    //到這邊pponList還有符合的資料，且目前不是最底層的類別，則設定ExistsDeal為true，取逐筆遞迴往下處理子類別
                    foreach (var typeNode in node.NodeDatas)
                    {
                        foreach (var categoryNode in typeNode.CategoryNodes)
                        {
                            SetApiCategoryNodeExistsField(categoryNode, subPponList);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 感覺第一個版本有問題，試著調整
        /// </summary>
        /// <param name="node"></param>
        /// <param name="pponList"></param>
        private static void SetApiCategoryNodeExistsField2(ApiCategoryNode node, List<PponDealPreviewData> pponList)
        {
            DateTime dealDate = DateTime.Now;

            List<PponDealPreviewData> subPponList = pponList.Where(x => x.BusinessOrderTimeS <= dealDate.AddDays(1) && dealDate < x.BusinessOrderTimeE
                                                            && x.HasCategoryIdList.Contains(node.CategoryId)).ToList();

            //於查詢日期，商品處於銷售狀態，且目前的categoryId包含在所需的區間內
            // 因為 cache 會維持一段時間，  所以從cache更新的時間開始，1天內會上檔的，都應該顯示分類
            node.ExistsDeal = subPponList.Count > 0;

            //到這邊pponList還有符合的資料，且目前不是最底層的類別，則設定ExistsDeal為true，取逐筆遞迴往下處理子類別
            foreach (var typeNode in node.NodeDatas)
            {
                foreach (var categoryNode in typeNode.CategoryNodes)
                {
                    SetApiCategoryNodeExistsField2(categoryNode, subPponList);
                }
            }
        }

        private static void InsertAllNodesToCategoryNodes(ApiCategoryTypeNode typeNode)
        {
            foreach (ApiCategoryNode categoryNode in typeNode.CategoryNodes)
            {
                if (categoryNode.IsFinal)
                {
                    continue;
                }
                foreach (var subTypeNodes in categoryNode.NodeDatas)
                {
                    //確認是否需要加上全部選項
                    List<CategoryAllNodeSetting> settingList =
                    CategoryManager.AllNodeSettings.Where(x => x.Id == categoryNode.CategoryId && x.NodeType == subTypeNodes.NodeType)
                                    .ToList();
                    if (settingList.Count > 0)
                    {
                        CategoryAllNodeSetting setting = settingList.First();
                        ApiCategoryNode allNode = ApiCategoryNode.GetDefaultCategory(setting.InsertNode.Name, setting.NodeType,
                                                                               setting.InsertNode.ShortName, categoryNode.CategoryId);
                        //ExistsDeal依據subTypeNodes內的ExistsDeal為準
                        allNode.ExistsDeal = subTypeNodes.CategoryNodes.Any(x => x.ExistsDeal);
                        subTypeNodes.CategoryNodes.Insert(0, allNode);
                    }

                    InsertAllNodesToCategoryNodes(subTypeNodes);
                }
            }
        }

        private static List<DailyCategoryDealCountNode> LoadDailyDealCountNodesBy(CategoryType categoryType, DateTime startDate, DateTime endDate)
        {
            List<DailyCategoryDealCountNode> tempDailyDealCountNodes = new List<DailyCategoryDealCountNode>();

            DateTime tempDate = startDate;
            while (tempDate <= endDate)
            {
                DailyCategoryDealCountNode tempDailyNode = new DailyCategoryDealCountNode();
                tempDailyNode.Date = tempDate;
                tempDailyNode.DealCountNodes = new List<CategoryDealCountNode>();

                #region Level 1 頻道(美食、宅配、...)
                foreach (CategoryNode channelNode in CategoryManager.PponChannelCategoryTree.CategoryNodes)
                {
                    //處理頻道的各分類數量
                    CategoryDealCountNode channelSummary = new CategoryDealCountNode(channelNode.CategoryId);
                    //取得檔次category的資料
                    List<CategoryNode> categoryNodes = channelNode.GetSubCategoryTypeNode(categoryType);
                    //依據每個category取出商品數量
                    foreach (var categoryNode in categoryNodes.Where(x => x.IsShowFrontEnd))
                    {
                        channelSummary.DealCountList.Add(new CategoryDealCount(categoryNode.CategoryId,
                                                                               categoryNode.CategoryName));
                    }

                    #region Level 2 區域(台北、桃園、...)
                    //取出區域的類別
                    List<CategoryNode> areaNodes = channelNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                    //每個地區篩選
                    foreach (var areaNode in areaNodes)
                    {
                        //產生區域的各分類數量物件
                        CategoryDealCountNode areaSummary = new CategoryDealCountNode(areaNode.CategoryId);
                        foreach (var categoryNode in categoryNodes.Where(x => x.IsShowFrontEnd))
                        {
                            areaSummary.DealCountList.Add(new CategoryDealCount(categoryNode.CategoryId,
                                                                                categoryNode.CategoryName));
                        }

                        #region Level 3 區域分類(商圈、行政區、捷運站沿線)
                        //取出子區域-行政區的類別
                        List<CategoryNode> subRegionNodes = areaNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                        foreach (var subRegionNode in subRegionNodes)
                        {
                            //產生子區域的各分類數量物件
                            CategoryDealCountNode subRegionSummary = new CategoryDealCountNode(subRegionNode.CategoryId);
                            foreach (var categoryNode in categoryNodes.Where(x => x.IsShowFrontEnd))
                            {
                                subRegionSummary.DealCountList.Add(new CategoryDealCount(categoryNode.CategoryId,
                                                                                         categoryNode.CategoryName));
                            }

                            #region Level 4 商圈(東區、西門町、...)、行政區(台北行政區、...)、捷運站沿線(文湖線、...)
                            List<CategoryNode> standardRegionNodes = subRegionNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                            foreach (var standardRegionNode in standardRegionNodes)
                            {
                                CategoryDealCountNode standardRegionSummary = new CategoryDealCountNode(standardRegionNode.CategoryId);
                                foreach (var categoryNode in categoryNodes.Where(x => x.IsShowFrontEnd))
                                {
                                    standardRegionSummary.DealCountList.Add(new CategoryDealCount(categoryNode.CategoryId,
                                                                                             categoryNode.CategoryName));
                                }

                                #region Level 5 捷運站沿線 內容(台北車站、善導寺、...)
                                List<CategoryNode> subStandardRegionNodes = standardRegionNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                                foreach (var subStandardRegionNode in subStandardRegionNodes)
                                {
                                    CategoryDealCountNode subStandardRegionSummary = new CategoryDealCountNode(subStandardRegionNode.CategoryId);
                                    foreach (var categoryNode in categoryNodes.Where(x => x.IsShowFrontEnd))
                                    {
                                        subStandardRegionSummary.DealCountList.Add(new CategoryDealCount(categoryNode.CategoryId,
                                                                                                        categoryNode.CategoryName));
                                    }
                                    standardRegionSummary.SubNodes.Add(subStandardRegionSummary);
                                }
                                subRegionSummary.SubNodes.Add(standardRegionSummary);
                                #endregion
                            }
                            areaSummary.SubNodes.Add(subRegionSummary);
                            #endregion
                        }
                        channelSummary.SubNodes.Add(areaSummary);
                        #endregion
                    }
                    tempDailyNode.DealCountNodes.Add(channelSummary);
                    #endregion
                }

                tempDailyDealCountNodes.Add(tempDailyNode);
                #endregion

                //增加作業日期一天
                tempDate = tempDate.AddDays(1);
            }

            foreach (var dailyNode in tempDailyDealCountNodes)
            {
                DailyCategoryDealCountNode node = dailyNode;
                foreach (var pponDealPreviewData in PponDealPreviewDatas.Where(x => x.BusinessOrderTimeS <= node.Date && x.BusinessOrderTimeE > node.Date))
                {
                    var tempChannelDealCountNodes = node.DealCountNodes;

                    foreach (var channelDealContNode in tempChannelDealCountNodes)
                    {
                        TotalCountInCategory(pponDealPreviewData, categoryType, channelDealContNode);
                    }
                }
            }

            return tempDailyDealCountNodes;
        }

        /// <summary>
        /// 計算各分類內的檔次數 (抽出相同程式碼，改用遞迴)
        /// </summary>
        /// <param name="previewData">當日檔次清單</param>
        /// <param name="categoryType">分類類別</param>
        /// <param name="dealCountNode">該層分類節點</param>
        /// <remarks>
        /// 頻道依舊用foreach，channel(頻道)以下才用這個function
        /// </remarks>
        private static void TotalCountInCategory(PponDealPreviewData previewData, CategoryType categoryType, CategoryDealCountNode dealCountNode)
        {
            //計算CategoryDealCountNode.TotalCount
            if (previewData.HasCategoryIdList.Contains(dealCountNode.CategoryId))
            {
                //累計子節點數
                dealCountNode.TotalCount += 1;

                //計算分類裡面的檔次數
                foreach (var dealCount in dealCountNode.DealCountList)
                {
                    DealCountInNode(previewData, categoryType, dealCount);
                }

                //如果底下還有節點，繼續向下
                foreach (var node in dealCountNode.SubNodes)
                {
                    TotalCountInCategory(previewData, categoryType, node);
                }
            }
        }
        /// <summary>
        /// 計算CategoryDealCount.DealCount
        /// </summary>
        /// <param name="previewData"></param>
        /// <param name="categoryType"></param>
        /// <param name="dealCount"></param>
        private static void DealCountInNode(PponDealPreviewData previewData, CategoryType categoryType, CategoryDealCount dealCount)
        {
            if (previewData.HasCategoryIdList.Contains(dealCount.CategoryId))
            {
                dealCount.DealCount += 1;
            }
            else if (dealCount.CategoryId == 317 && categoryType == CategoryType.DealSpecialCategory)
            {
                TimeSpan soldEndDays = new TimeSpan(previewData.BusinessOrderTimeE.Ticks - DateTime.Now.Ticks);
                if (1 <= soldEndDays.TotalDays && soldEndDays.TotalDays < 3)
                {
                    //檢查 三天內結檔=>即將售完
                    dealCount.DealCount += 1;
                }
            }
        }

        #endregion private static method

        #region public static method

        /// <summary>
        /// 輸入一組 int的陣列，查詢檔次設定category包含傳入的所有id的資料，並回傳PponDealPreviewData資料。
        /// </summary>
        /// <param name="categoryCriteria"></param>
        /// <returns></returns>
        public static List<PponDealPreviewData> GetDealPreviewDataListWithCategory(List<int> categoryCriteria)
        {
            //檢查每一個PponDealPreviewDatas中的物件，判斷物件的HasCategoryIdList中包含傳入參數categoryCriteria中的每個元素
            List<PponDealPreviewData> rtn = PponDealPreviewDatas.Where(x => categoryCriteria.All(y => x.HasCategoryIdList.Contains(y)))
                .Select(x => x.Copy()).ToList();

            return rtn;
        }

        /// <summary>
        /// 回傳檔次所有 category id
        /// </summary>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public static List<int> GetDealCategoryIdList(Guid businessHourGuid)
        {
            var pponDealPreviewData = PponDealPreviewDatas.FirstOrDefault(x => x.BusinessHourGuid == businessHourGuid);
            if (pponDealPreviewData != null)
            {
                return new List<int>(pponDealPreviewData.HasCategoryIdList);
            }
            else
            {
                return new List<int>();
            }
        }

        public static List<PponDealPreviewData> GetTodayDealPreviewDataListWithCategory(DateTime date,
                                                                                      List<int> categoryCriteria)
        {
            List<PponDealPreviewData> rtn = PponDealPreviewDatas.Where(x =>
                x.BusinessOrderTimeS >= date &&
                x.BusinessOrderTimeS < date.AddDays(1) && x.BusinessOrderTimeS < DateTime.Now &&
                categoryCriteria.All(y => x.HasCategoryIdList.Contains(y)))
    .Select(x => x.Copy()).ToList();
            return rtn;
        }
        public static List<PponDealPreviewData> GetTodayDealPreviewDataList(DateTime date)
        {
            List<PponDealPreviewData> rtn = PponDealPreviewDatas.Where(x =>
                x.BusinessOrderTimeS >= date &&
                x.BusinessOrderTimeS < date.AddDays(1) && x.BusinessOrderTimeS < DateTime.Now)
                .Select(x => x.Copy()).ToList();
            return rtn;
        }
        /// <summary>
        ///  取得最後一日的檔次，且符合篩選的Category規則
        /// </summary>
        /// <param name="categoryCriteria"></param>
        /// <returns></returns>
        public static List<PponDealPreviewData> GetLastDayDealPreviewDataListWithCategory(List<int> categoryCriteria)
        {
            List<PponDealPreviewData> rtn = PponDealPreviewDatas.Where(x =>
                x.HasCategoryIdList.Contains(_LAST_DAT_CATEGORY_ID) && DateTime.Now < x.BusinessOrderTimeE &&
                categoryCriteria.All(y => x.HasCategoryIdList.Contains(y)))
                .ToList();
            return rtn;
        }

        /// <summary>
        ///  取得24小時到貨檔次
        /// </summary>
        /// <param name="categoryCriteria"></param>
        /// <returns></returns>
        public static List<PponDealPreviewData> GetArrival24hCategory(List<int> categoryCriteria)
        {
            return new List<PponDealPreviewData>();
        }

        public static List<MultipleMainDealPreview> GetArrival24hCategoryMMDeals(List<int> categoryCriteria)
        {
            List<PponDealPreviewData> matchPreviewDataList = PponDealPreviewManager.GetArrival24hCategory(categoryCriteria);

            //Dictionary<Guid, IDealTimeSlot> slots = ViewPponDealManager.DefaultManager.DealTimeSlotGetByCityAndTime(CategoryManager.Default.Delivery.CityId.Value, DateTime.Now).ToDictionary
            //    (t => t.BusinessHourGuid, t => t);
            DateTime now = DateTime.Now;
            List<MultipleMainDealPreview> mmdeals = new List<MultipleMainDealPreview>();
            ///取得在宅配的順序
            foreach (PponDealPreviewData _data in matchPreviewDataList)
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(_data.BusinessHourGuid, true);
                var slot = ViewPponDealManager.DefaultManager.DealTimeSlotGetByBidCityAndTimeNotFoundReturnNull(
                    deal.BusinessHourGuid, CategoryManager.Default.Delivery.CityId.Value, now);
                MultipleMainDealPreview mainDealPreview;
                if (slot == null)
                {
                    mainDealPreview = new MultipleMainDealPreview(0, int.MaxValue, deal, deal.CategoryIds, DealTimeSlotStatus.Default, 0);
                }
                else
                {
                    mainDealPreview = new MultipleMainDealPreview(0, slot.Sequence,
                        deal, deal.CategoryIds, (DealTimeSlotStatus)slot.Status, 0);
                }
                mmdeals.Add(mainDealPreview);
            }
            mmdeals = mmdeals.OrderBy(p => p.PponDeal.IsSoldOut).ThenBy(t => t.Sequence).ToList();
            return mmdeals;
        }
        /// <summary>
        /// 取得美食、玩美、旅遊檔次，過瀘售完、隱藏、限憑證類，並依照上檔日期排序(最新上檔在前)
        /// </summary>
        /// <returns></returns>
        public static List<MultipleMainDealPreview> GetFoodBeautyTravelCategoryMMDeals()
        {
            var root = HxRootCategory.Create();
            
            var deals = new List<MultipleMainDealPreview>();
            var ch87 = root.Tops.FirstOrDefault(t => t.Id == 87);
            var ch89 = root.Tops.FirstOrDefault(t => t.Id == 89);
            var ch90 = root.Tops.FirstOrDefault(t => t.Id == 90);

            if (ch87 != null)
            {
                deals.AddRange(ch87.Deals.Where(
                        t => t.PponDeal.DeliveryType == (int)DeliveryType.ToShop &&
                        t.PponDeal.IsSoldOut == false &&
                        t.PponDeal.IsHiddenInRecommendDeal == false));
            }
            if (ch89 != null)
            {
                deals.AddRange(ch89.Deals.Where(
                        t => t.PponDeal.DeliveryType == (int)DeliveryType.ToShop &&
                        t.PponDeal.IsSoldOut == false &&
                        t.PponDeal.IsHiddenInRecommendDeal == false));
            }
            if (ch90 != null)
            {
                deals.AddRange(ch90.Deals.Where(
                        t => t.PponDeal.DeliveryType == (int)DeliveryType.ToShop &&
                        t.PponDeal.IsSoldOut == false &&
                        t.PponDeal.IsHiddenInRecommendDeal == false));
            }
            deals = deals.OrderByDescending(t => t.PponDeal.BusinessHourOrderTimeS).ToList();
            return deals;
        }


        public static ApiCategoryNode GetArrival24hCategoryNode()
        {
            ApiCategoryNode result = DailyPponChannelCategoryTrees.CategoryNodes.FirstOrDefault(t => t.CategoryId == _MAIN_THEME_CATEGORY_ID);
            return result;
        }

        /// <summary>
        /// 回傳DailyDealCountNodes
        /// </summary>
        /// <returns></returns>
        public static List<DailyCategoryDealCountNode> GetDailyChannelDealCountNodes()
        {
            return DailyDealCountNodes;
        }

        /// <summary>
        /// 依據條件取出某個頻道、區域的類別數量
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="channelAreaId"></param>
        /// <param name="withAllDealCount">是否要補上全部類別數量的項目，若為true會補上一筆id為0，name為全部的紀錄</param>
        /// <param name="defaultCountName">可設定全部項目的名稱</param>
        /// <returns></returns>
        public static List<CategoryDealCount> GetCategoryDealCountListByChannel(int channelId, int? channelAreaId,
                                                                                bool withAllDealCount,
                                                                                string defaultCountName)
        {
            List<CategoryDealCount> tempDealCount = new List<CategoryDealCount>();
            channelAreaId = (channelAreaId == 0) ? null : channelAreaId;
            string defaultName = string.IsNullOrWhiteSpace(defaultCountName) ? "全部" : defaultCountName;
            CategoryDealCount allDealCount = new CategoryDealCount(CategoryManager.GetDefaultCategoryNode().CategoryId, defaultName);

            //取得目前系統時間
            DateTime workDate = DateTime.Now;
            if (workDate.Hour < 12)
            {
                workDate = DateTime.ParseExact(workDate.AddDays(-1).ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
            }
            else
            {
                workDate = DateTime.ParseExact(workDate.ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
            }
            //依據目前系統時間，取出目前時間保存各分類數量的物件
            List<DailyCategoryDealCountNode> dailyNodes = DailyDealCountNodes.Where(x => x.Date.Equals(workDate)).ToList();
            //判斷是否有資料
            if (dailyNodes.Count > 0)
            {
                //有資料，取出目前時間的分類數量陣列
                var tempChannelCountNodes = dailyNodes.First().DealCountNodes;
                //取出傳入的頻道ID之類別數量陣列
                List<CategoryDealCountNode> channelCountNodes = tempChannelCountNodes.Where(x => x.CategoryId == channelId).ToList();
                if (channelCountNodes.Count > 0)
                {
                    CategoryDealCountNode channelCountNode = channelCountNodes.First();
                    //判斷是否需要查詢 區域
                    if (channelAreaId.HasValue)
                    {
                        //檢查目前分類數量記錄的物件，看看此物件是否有子物件儲存區域的資料
                        CategoryDealCountNode areaCountNode = channelCountNode.GetSubNode(channelAreaId.Value);
                        if (areaCountNode != null)
                        {
                            //有取得資料回傳分類數量
                            tempDealCount = areaCountNode.DealCountList;
                            allDealCount.DealCount = areaCountNode.TotalCount;
                        }
                    }
                    else
                    {
                        //不須查詢地區，直接回傳頻道的分類數量
                        tempDealCount = channelCountNodes.First().DealCountList;
                        allDealCount.DealCount = channelCountNodes.First().TotalCount;
                    }
                }
            }

            //無資料，回傳空的物件
            if (tempDealCount.Count <= 1)
            {
                return new List<CategoryDealCount>();
            }

            List<CategoryDealCount> rtnDealCount = new List<CategoryDealCount>();
            rtnDealCount.AddRange(tempDealCount);

            if (withAllDealCount && rtnDealCount.Count > 0)
            {
                rtnDealCount.Insert(0, allDealCount);
            }

            return rtnDealCount;
        }


        /// <summary>
        /// 依據條件取出某個頻道、區域的特殊類別數量
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="channelAreaId"></param>
        /// <returns></returns>
        public static List<CategoryDealCount> GetSpecialDealCountListByChannel(int channelId, int? channelAreaId)
        {
            List<CategoryDealCount> tempDealCount = new List<CategoryDealCount>();
            channelAreaId = (channelAreaId == 0) ? null : channelAreaId;

            //取得目前系統時間
            DateTime workDate = DateTime.Now;
            if (workDate.Hour < 12)
            {
                workDate = DateTime.ParseExact(workDate.AddDays(-1).ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
            }
            else
            {
                workDate = DateTime.ParseExact(workDate.ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
            }
            //依據目前系統時間，取出目前時間保存各分類數量的物件
            List<DailyCategoryDealCountNode> dailyNodes = DailySpecialCategoryCountNodes.Where(x => x.Date.Equals(workDate)).ToList();
            //判斷是否有資料
            if (dailyNodes.Count > 0)
            {
                //有資料，取出目前時間的分類數量陣列
                var tempChannelCountNodes = dailyNodes.First().DealCountNodes;
                //取出傳入的頻道ID之類別數量陣列
                List<CategoryDealCountNode> channelCountNodes = tempChannelCountNodes.Where(x => x.CategoryId == channelId).ToList();
                if (channelCountNodes.Count > 0)
                {
                    CategoryDealCountNode channelCountNode = channelCountNodes.First();
                    //判斷是否需要查詢 區域
                    if (channelAreaId.HasValue)
                    {
                        //檢查目前分類數量記錄的物件，看看此物件是否有子物件儲存區域的資料
                        CategoryDealCountNode areaCountNode = channelCountNode.GetSubNode(channelAreaId.Value);
                        if (areaCountNode != null)
                        {
                            //有取得資料回傳分類數量
                            tempDealCount = areaCountNode.DealCountList;
                        }
                    }
                    else
                    {
                        //不須查詢地區，直接回傳頻道的分類數量
                        tempDealCount = channelCountNodes.First().DealCountList;
                    }
                }
            }

            List<CategoryDealCount> rtnDealCount = new List<CategoryDealCount>();
            rtnDealCount.AddRange(tempDealCount);

            return rtnDealCount;
        }

        /// <summary>
        /// 依據條件取出某個頻道、區域的類別數量的物件
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="channelAreaId"></param>
        /// <param name="subRegionId"></param>
        /// <returns></returns>
        public static CategoryDealCountNode GetCategoryDealCountNodeByChannel(int channelId, int? channelAreaId, int? subRegionId = null)
        {
            CategoryDealCountNode rtnNode = new CategoryDealCountNode(channelId);
            //int = 0也當作null
            channelAreaId = (channelAreaId == 0) ? null : channelAreaId;
            subRegionId = (subRegionId == 0) ? null : subRegionId;

            //取得目前系統時間
            DateTime workDate = DateTime.Now;
            if (workDate.Hour < 12)
            {
                workDate = DateTime.ParseExact(workDate.AddDays(-1).ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
            }
            else
            {
                workDate = DateTime.ParseExact(workDate.ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
            }

            //依據目前系統時間，取出目前時間保存各分類數量的物件
            List<DailyCategoryDealCountNode> dailyNodes = DailyDealCountNodes.Where(x => x.Date.Equals(workDate)).ToList();
            //判斷是否有資料
            if (dailyNodes.Count > 0)
            {
                //有資料，取出目前時間的分類數量陣列
                var tempChannelCountNodes = dailyNodes.First().DealCountNodes;
                //取出傳入的頻道ID之類別數量陣列
                List<CategoryDealCountNode> channelCountNodes = tempChannelCountNodes.Where(x => x.CategoryId == channelId).ToList();
                if (channelCountNodes.Count > 0)
                {
                    CategoryDealCountNode channelCountNode = channelCountNodes.First();
                    //判斷是否需要查詢 區域
                    if (channelAreaId.HasValue)
                    {
                        //檢查目前分類數量記錄的物件，看看此物件是否有子物件儲存區域的資料
                        CategoryDealCountNode areaCountNode = channelCountNode.GetSubNode(channelAreaId.Value);
                        if (areaCountNode != null)
                        {
                            //有取得資料回傳分類數量物件
                            rtnNode = areaCountNode;

                            if (subRegionId.HasValue)
                            {
                                //檢查目前分類數量記錄的物件，看看此物件是否有子物件儲存行政區或商圈的資料
                                CategoryDealCountNode subRegionCountNode = areaCountNode.GetSubNode(subRegionId.Value);
                                if (subRegionCountNode != null)
                                {
                                    rtnNode = subRegionCountNode;
                                }
                                else
                                {
                                    //可能更深一層 行政區或商圈
                                    foreach (var childSubNode in areaCountNode.SubNodes)
                                    {
                                        CategoryDealCountNode childSubRegionCountNode = childSubNode.GetSubNode(subRegionId.Value);
                                        if (childSubRegionCountNode != null)
                                        {
                                            rtnNode = childSubRegionCountNode;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //不須查詢地區，直接回傳頻道的分類數量物件
                        rtnNode = channelCountNodes.First();
                    }
                }
            }

            return rtnNode.Copy();
        }

        #endregion public static method

        #region 推薦檔次
        /// <summary>
        /// 
        /// 推薦檔次
        /// 
        /// 關於 recommendLogicType
        /// 0.是原本的頻道下以亂數取
        /// 1.分類相關性，取前20後再亂數取
        /// 2.關鍵字相關性，取前20後再亂數取
        /// 3.混合分類與關鍵字評分，取前20後再亂數取
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="dealCount"></param>
        /// <param name="recommendLogicType"></param>
        /// <returns></returns>
        public static List<IViewPponDeal> GetApiRecommendDeals(Guid bid, int dealCount, int recommendLogicType)
        {
            List<Tuple<Guid, decimal>> scores = null;
            if (recommendLogicType == 0)
            {
                return OrderFacade.GetRandomDeal(bid, dealCount, dealCount);
            }
            else if (recommendLogicType == 1)
            {
                scores = GetApiRecommendDealScoresByCategories(bid);
            }
            else if (recommendLogicType == 2)
            {
                scores = GetApiRecommendDealScoresByTags(bid);
            }
            else if (recommendLogicType == 3)
            {
                string cacheKey = string.Format("ApiRecommendDealScore.{0}.{1:yyyyMMddHH}", bid, DateTime.Now);
                scores = MemoryCache.Default.Get(cacheKey) as List<Tuple<Guid, decimal>>;
                if (scores == null)
                {
                    scores = GetApiRecommendDealScoresByBotTagsAndCategories(bid, dealCount + 8);
                    MemoryCache.Default.Set(cacheKey, scores, DateTime.MaxValue);
                }
            }
            if (scores == null)
            {
                return new List<IViewPponDeal>();
            }
            List<IViewPponDeal> result = new List<IViewPponDeal>();
            Random rd = new Random();
            scores = scores.OrderBy(t => rd.Next(100)).Take(dealCount).OrderByDescending(t => t.Item2).ToList();
            foreach (var score in scores)
            {
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(score.Item1, true);
                result.Add(vpd);
            }
            return result;
        }

        /// <summary>
        /// 合併PponDealPreviewData Categories分數及ViewPponDeal Tags分數
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        private static List<Tuple<Guid, decimal>> GetApiRecommendDealScoresByBotTagsAndCategories(Guid bid, int takeSize)
        {
            List<Tuple<Guid, decimal>> categorieScores = GetApiRecommendDealScoresByCategories(bid, takeSize);
            List<Tuple<Guid, decimal>> tagScores = GetApiRecommendDealScoresByTags(bid, takeSize);
            Dictionary<Guid, decimal> scoresDict = new Dictionary<Guid, decimal>();

            foreach (var item in categorieScores)
            {
                if (item.Item2 == 0)
                {
                    continue;
                }
                if (scoresDict.ContainsKey(item.Item1) == false)
                {
                    scoresDict.Add(item.Item1, item.Item2 * (decimal)0.5);
                }
                else
                {
                    scoresDict[item.Item1] += item.Item2 * (decimal)0.5;
                }
            }

            foreach (var item in tagScores)
            {
                if (item.Item2 == 0)
                {
                    continue;
                }
                if (scoresDict.ContainsKey(item.Item1) == false)
                {
                    scoresDict.Add(item.Item1, item.Item2 * (decimal)0.5);
                }
                else
                {
                    scoresDict[item.Item1] += item.Item2 * (decimal)0.5;
                }
            }

            List<Tuple<Guid, decimal>> scores = scoresDict.Select(
                t => new Tuple<Guid, decimal>(t.Key, t.Value)).OrderByDescending(t => t.Item2).Take(takeSize).ToList();
            return scores;
        }

        /// <summary>
        /// 以快取PponDealPreviewData的Categories計算Scores
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        private static List<Tuple<Guid, decimal>> GetApiRecommendDealScoresByCategories(Guid bid, int takeSize = 10)
        {
            var target = PponDealPreviewDatas.FirstOrDefault(t => t.BusinessHourGuid == bid);
            if (target == null)
            {
                return new List<Tuple<Guid, decimal>>(); //非在近日上檔檔次可能找不到
            }

            List<Tuple<Guid, decimal>> scores = new List<Tuple<Guid, decimal>>();
            foreach (PponDealPreviewData relationDeal in PponDealPreviewDatas)
            {
                decimal score;
                decimal similarity;
                if (bid == relationDeal.BusinessHourGuid)
                {
                    continue;
                }
                if (DateTime.Now < relationDeal.BusinessOrderTimeS || DateTime.Now > relationDeal.BusinessOrderTimeE)
                {
                    continue;
                }
                if (relationDeal.HasCategoryIdList == null || relationDeal.HasCategoryIdList.Count == 0 ||
                    target.HasCategoryIdList == null || target.HasCategoryIdList.Count == 0)
                {
                    continue;
                }
                if (relationDeal.IsHiddenInRecommendDeal)
                {
                    continue;
                }
                if (relationDeal.IsSoldOut)
                {
                    continue;
                }
                similarity = target.HasCategoryIdList.Intersect(relationDeal.HasCategoryIdList).Count();

                score =
                    (similarity / target.HasCategoryIdList.Count) *
                    (similarity / relationDeal.HasCategoryIdList.Count);

                scores.Add(new Tuple<Guid, decimal>(relationDeal.BusinessHourGuid, score));
            }

            var recommendDealScores = scores.OrderByDescending(t => t.Item2).Take(takeSize).ToList();
            return recommendDealScores;
        }

        /// <summary>
        /// 以快取ViewPponDeal的Tags計算Scores
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        private static List<Tuple<Guid, decimal>> GetApiRecommendDealScoresByTags(Guid bid, int takeSize = 10)
        {
            var target = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            if (target == null)
            {
                return new List<Tuple<Guid, decimal>>();
            }
            List<Tuple<Guid, decimal>> scores = new List<Tuple<Guid, decimal>>();
            HashSet<string> targetTags = target.Tags;

            var relationDeals = ViewPponDealManager.DefaultManager.ViewPponDealGetList(excludeSubDeals: true, onDeal: true);
            foreach (IViewPponDeal relationDeal in relationDeals)
            {
                if (bid == relationDeal.BusinessHourGuid)
                {
                    continue;
                }
                if (relationDeal.Tags == null || relationDeal.Tags.Count == 0 || targetTags.Count == 0)
                {
                    continue;
                }
                if (relationDeal.IsHiddenInRecommendDeal)
                {
                    continue;
                }
                if (relationDeal.IsSoldOut)
                {
                    continue;
                }

                decimal score;
                decimal similarity;

                similarity = relationDeal.Tags.Intersect(targetTags).Count();

                score =
                    (similarity / targetTags.Count) *
                    (similarity / relationDeal.Tags.Count);

                scores.Add(new Tuple<Guid, decimal>(relationDeal.BusinessHourGuid, score));
            }

            var recommendDealScores = scores.OrderByDescending(t => t.Item2).Take(takeSize).ToList();
            return recommendDealScores;
        }

        #endregion
    }

    #region class
    public class DailyCategoryDealCountNode
    {
        public DateTime Date { get; set; }
        public List<CategoryDealCountNode> DealCountNodes { get; set; }
    }

    public class CategoryDealCountNode
    {
        public int CategoryId { get; set; }
        public List<CategoryDealCountNode> SubNodes;
        public List<CategoryDealCount> DealCountList { get; set; }
        public int TotalCount { get; set; }

        public CategoryDealCountNode(int categoryId)
        {
            CategoryId = categoryId;
            DealCountList = new List<CategoryDealCount>();
            SubNodes = new List<CategoryDealCountNode>();
        }

        /// <summary>
        /// 回傳 SubNodes，若查無資料，回傳NULL
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public CategoryDealCountNode GetSubNode(int categoryId)
        {
            List<CategoryDealCountNode> nodes = SubNodes.Where(x => x.CategoryId == categoryId).ToList();
            if (nodes.Count > 0)
            {
                return nodes.First();
            }
            else
            {
                return null;
            }
        }

        public CategoryDealCountNode Copy()
        {
            CategoryDealCountNode rtnObject = new CategoryDealCountNode(CategoryId);
            foreach (var categoryDealCountNode in this.SubNodes)
            {
                rtnObject.SubNodes.Add(categoryDealCountNode.Copy());
            }

            foreach (var dealCount in this.DealCountList)
            {
                rtnObject.DealCountList.Add(dealCount.Copy());
            }

            rtnObject.TotalCount = TotalCount;

            return rtnObject;
        }
    }

    public class CategoryDealCount
    {
        [JsonProperty("Id")]
        public int CategoryId { get; private set; }
        [JsonProperty("Name")]
        public string CategoryName { get; private set; }
        [JsonProperty("Count")]
        public int DealCount { get; set; }
        public CategoryDealCount(int categoryId, string categoryName)
        {
            CategoryId = categoryId;
            CategoryName = categoryName;
            DealCount = 0;
        }

        public CategoryDealCount Copy()
        {
            CategoryDealCount rtnObject = new CategoryDealCount(CategoryId, CategoryName);
            rtnObject.DealCount = DealCount;

            return rtnObject;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} - {2}", this.CategoryId, this.CategoryName, this.DealCount);
        }
    }
    #endregion class
}


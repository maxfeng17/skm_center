﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using log4net;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.BizLogic.Component
{
    public class CartPaymentTransaction
    {
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        public string MainTransId { get; set; }
        public decimal Amount { get; set; }
        public List<PaymentTransaction> PaymentTransactions { get; set; }
        public int ApiProvider { get; set; }

        public PayTransType TransType
        {
            get
            {
                var pt = PaymentTransactions.FirstOrDefault();
                if (pt == null)
                {
                    throw new Exception("取TransType時，PaymentTransactions沒有資料.");
                }
                if (pt.TransType == null)
                {
                    throw new Exception("取TransType時，第1筆PaymentTransaction沒有設定TransType.");
                }
                return (PayTransType)pt.TransType.GetValueOrDefault();
            }
        }

        public bool IsValid
        {
            get { return Amount > 0 && PaymentTransactions != null && PaymentTransactions.Count > 0; }
        }

        public static List<CartPaymentTransaction> ConvertFrom(List<PaymentTransaction> pts)
        {
            List<CartPaymentTransaction> result = new List<CartPaymentTransaction>();

            var ptWithCart = pts.Where(t => string.IsNullOrEmpty(t.CartTransId) == false).ToList();
            foreach (var grp in ptWithCart.GroupBy(t => t.CartTransId))
            {
                bool isAuthoriztion = grp.All(t => t.TransType.GetValueOrDefault() == (int)PayTransType.Authorization);
                bool isRefund = grp.All(t => t.TransType.GetValueOrDefault() == (int)PayTransType.Refund);
                if (isAuthoriztion == false && isRefund == false)
                {
                    throw new Exception("CartPaymentTransaction 的 ConvertFrom 不接受處理混合請款與退款的交易資料");
                }
                CartPaymentTransaction item = new CartPaymentTransaction();
                item.MainTransId = grp.Key;
                item.Amount = grp.Sum(t => t.Amount);
                item.PaymentTransactions = new List<PaymentTransaction>();
                //請退款的資料有做不同的檢查
                if (isAuthoriztion)
                {
                    foreach (var pt in grp)
                    {
                        if (Helper.GetPaymentTransactionPhase(pt.Status) == PayTransPhase.Successful && HasChargingPaymentTransaction(pt) == false)
                        {
                            item.PaymentTransactions.Add(pt);
                        }
                    }
                }
                else
                {
                    foreach (var pt in grp)
                    {
                        if (Helper.GetPaymentTransactionPhase(pt.Status) == PayTransPhase.Requested)
                        {
                            item.PaymentTransactions.Add(pt);
                        }
                    }
                }
                item.ApiProvider = grp.First().ApiProvider.GetValueOrDefault();
                result.Add(item);
            }

            var ptWithoutCart = pts.Where(t => string.IsNullOrEmpty(t.CartTransId)).ToList();
            foreach (var pt in ptWithoutCart)
            {
                CartPaymentTransaction item = new CartPaymentTransaction();
                item.MainTransId = pt.TransId;
                item.Amount = pt.Amount;
                item.PaymentTransactions = new List<PaymentTransaction>();
                if (pt.TransType.GetValueOrDefault() == (int)PayTransType.Authorization)
                {
                    if (Helper.GetPaymentTransactionPhase(pt.Status) == PayTransPhase.Successful && HasChargingPaymentTransaction(pt) == false)
                    {
                        item.PaymentTransactions.Add(pt);
                    }
                }
                else if (pt.TransType.GetValueOrDefault() == (int)PayTransType.Refund)
                {
                    if (Helper.GetPaymentTransactionPhase(pt.Status) == PayTransPhase.Requested)
                    {
                        item.PaymentTransactions.Add(pt);
                    }
                }
                item.ApiProvider = pt.ApiProvider.GetValueOrDefault();
                result.Add(item);
            }

            return result;
        }

        private static bool HasChargingPaymentTransaction(PaymentTransaction pt)
        {
            PaymentTransactionCollection chargingPtCol = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                PaymentTransaction.Columns.TransId + " = " + pt.TransId,
                PaymentTransaction.Columns.PaymentType + " = " + (int)PaymentType.Creditcard,
                PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Charging);
            return chargingPtCol.Count > 0;
        }
    }

    public class CreditcardProcessor
    {
        private static IOrderProvider op;
        private static ISysConfProvider config;

        private static readonly Dictionary<PaymentAPIProvider, string> MerchantIds = new Dictionary<PaymentAPIProvider, string>();
        private static ILog logger = LogManager.GetLogger(typeof(CreditcardProcessor));

        static CreditcardProcessor()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            config = ProviderFactory.Instance().GetConfig();

            //要加新特店，要調整 MerchantIds, PaymentChargeTextFiles 
            MerchantIds.Add(PaymentAPIProvider.HiTrust, config.HiTrustMerchantID);
            MerchantIds.Add(PaymentAPIProvider.HiTrustPiinLife, config.HiTrustPiinLifeMerchantID);
            MerchantIds.Add(PaymentAPIProvider.HiTrustPayeasyTravel, config.HiTrustPayeasyTravelMerchantID);
            MerchantIds.Add(PaymentAPIProvider.HiTrustContactWithCVV2, config.HiTrustContactWithCVV2MerchantID);
            MerchantIds.Add(PaymentAPIProvider.HiTrustContactWithoutCVV2, config.HiTrustContactWithoutCVV2MerchantID);
        }

        #region Old NCCC

        public static bool OldNCCCApply(PaymentTransactionCollection ptCol)
        {
            if (ptCol.Count > 0)
            {
                #region Create NCCC Content

                StreamWriter sw = File.AppendText(config.CardPath + "\\" + config.MerchantID + ".dat");
                string text = CreateNCCCCreditCardContent(ptCol);
                //只有表頭沒有內容，直接結束
                if (text.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries).Length == 1)
                {
                    return true;
                }
                sw.WriteLine(text);
                sw.Close();
                sw.Dispose();

                try
                {
                    System.Diagnostics.Process myprocess = new System.Diagnostics.Process();
                    myprocess.StartInfo.WorkingDirectory = config.CardPath + "\\";
                    myprocess.StartInfo.FileName = config.NcccFileName + "UP.bat";
                    myprocess.StartInfo.Arguments = config.MerchantID;
                    myprocess.Start();
                    Thread.Sleep(5000);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                File.Move(config.CardPath + "\\" + config.MerchantID + ".dat", config.CardPath
                          + "\\Record\\" + config.MerchantID + "_" + DateTime.Now.ToString("yyMMddHHmmss") + ".dat");

                #endregion Create NCCC Content

                return true;
            }
            else
            {
                return false;
            }
        }

        public static void OldNCCCResult()
        {
            try
            {
                System.Diagnostics.Process myprocess = new System.Diagnostics.Process();
                myprocess.StartInfo.WorkingDirectory = config.CardPath + "\\";
                myprocess.StartInfo.FileName = config.NcccFileName + "DL.bat";
                myprocess.StartInfo.Arguments = config.MerchantID;
                myprocess.Start();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            Thread.Sleep(5000);

            DirectoryInfo di = new DirectoryInfo(config.CardPath);
            FileInfo[] fis = di.GetFiles("*.rsp");
            if (fis.Count() > 0)
            {
                StreamReader sr = new StreamReader(fis[0].FullName);
                string strResult = string.Empty;
                while ((strResult = sr.ReadLine()) != null)
                {
                    string strTransId = strResult.Substring(18, 40).Trim();
                    string strResponseCode = strResult.Substring(165, 3).Trim();
                    PaymentTransaction pt = op.PaymentTransactionGet(strTransId, PaymentType.Creditcard, PayTransType.Charging);
                    if (pt == null)
                    {
                        pt = op.PaymentTransactionGet(strTransId, PaymentType.Creditcard, PayTransType.Refund);
                    }

                    if (pt != null)
                    {
                        if (Helper.GetPaymentTransactionPhase(pt.Status) == PayTransPhase.Requested)
                        {
                            if (strResponseCode == "00")
                            {
                                pt.Status = Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Successful);
                            }
                            else
                            {
                                pt.Status = Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Failed);
                                OrderFacade.SendPponChargingFailMail(pt);
                            }
                            pt.Result = Convert.ToInt32(strResponseCode);
                        }
                        op.PaymentTransactionSet(pt);
                    }
                }
                sr.Close();

                File.Move(fis[0].FullName, config.CardPath + "\\Record\\" + fis[0].Name);
            }
        }

        public static string CreateNCCCCreditCardContent(PaymentTransactionCollection ptCol)
        {
            string strContentH = string.Empty;
            StringBuilder strContentD = new StringBuilder();

            string strSCode = config.MerchantID;
            string strTCode = config.TerminalID;
            decimal totalAmount = 0;
            int ptCount = 0;

            foreach (PaymentTransaction p in ptCol.Where(x => Enum.Equals(PaymentAPIProvider.NCCC, Helper.GetPaymentAPIProvider(x.ApiProvider))))
            {
                strContentD.Append(strSCode.PadRight(10, ' '));
                strContentD.Append(strTCode.PadRight(8, ' '));
                strContentD.Append(p.TransId.PadRight(40, ' '));
                strContentD.Append(" ".PadRight(19, ' '));
                strContentD.Append(p.Amount.ToString("f0").PadLeft(8, '0'));
                strContentD.Append(p.AuthCode.PadRight(8, ' '));
                if (p.TransType == (int)PayTransType.Authorization || p.TransType == (int)PayTransType.Charging)
                {
                    strContentD.Append("02");
                }

                else if (p.TransType == (int)PayTransType.Refund)
                {
                    strContentD.Append("01");
                }

                strContentD.Append(p.TransTime.Value.ToString("yyyMMdd").PadRight(8, ' '));
                strContentD.Append(" ".PadLeft(167, ' '));
                strContentD.Append("\r\n");

                ptCount++;
                if (p.TransType == (int)PayTransType.Refund)
                {
                    totalAmount -= p.Amount;
                }
                else
                {
                    totalAmount += p.Amount;
                }

                PaymentTransactionCollection ptc = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                    PaymentTransaction.Columns.TransId + " = " + p.TransId,
                                    PaymentTransaction.Columns.PaymentType + " = " + (int)PaymentType.Creditcard,
                                    PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Charging);

                DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(p.Status);
                if (Helper.GetPaymentTransactionPhase(p.Status) == PayTransPhase.Successful && ptc.Count == 0)
                {
                    PaymentFacade.NewTransaction(p.TransId, new Guid(p.OrderGuid.ToString()), PaymentType.Creditcard, p.Amount, p.AuthCode,
                        PayTransType.Charging, DateTime.Now, "sys", null,
                        Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Requested), 
                        PayTransResponseType.Initial, (OrderClassification)p.OrderClassification.Value, null, (PaymentAPIProvider)p.ApiProvider.Value);
                }
                else if (Helper.GetPaymentTransactionPhase(p.Status) == PayTransPhase.Requested)
                {
                    PaymentTransaction pt = op.PaymentTransactionGet(p.Id);
                    pt.TransTime = DateTime.Now;
                    pt.Status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                    pt.ApiProvider = p.ApiProvider;
                    op.PaymentTransactionSet(pt);
                }
            }

            strContentH += "H";
            strContentH += strSCode.PadRight(10, ' ');
            strContentH += DateTime.Today.ToString("yyyyMMdd").PadRight(8, ' ');
            strContentH += DateTime.Now.ToString("yyMMddHHmm").PadRight(10, ' ');
            strContentH += ptCount.ToString().PadLeft(12, '0');
            if (totalAmount < 0)
            {
                strContentH += "-";
            }
            else
            {
                strContentH += "+";
            }
            strContentH += Math.Abs(totalAmount).ToString("f0").PadLeft(12, '0');
            strContentH += " ".PadLeft(216, ' ');
            strContentH += "\r\n";

            return strContentH + strContentD.ToString();
        }

        #endregion Old NCCC     

        /// <summary>
        // 2019年1月15日，網際威信的退款，信用相關的API，於是改用手動退款
        /// 退款的payment_transaction加到清單
        /// </summary>
        /// <param name="pt"></param>
        /// <param name="manual"></param>
        private static bool ProcessHitrustRefund(PaymentTransaction pt, ref bool manual)
        {
            try
            {
                PaymentAPIProvider apiProvider = Helper.GetPaymentAPIProvider(pt.ApiProvider);
                if (HitrustApiProviders.Contains(apiProvider))
                {
                    PaymentTransaction ptAuth = op.PaymentTransactionGet(pt.TransId, PaymentType.Creditcard, PayTransType.Authorization);
                    PaymentTransaction ptCharging = op.PaymentTransactionGet(pt.TransId, PaymentType.Creditcard, PayTransType.Charging);
                    PaymentTransaction ptScash = op.PaymentTransactionGet(pt.TransId, PaymentType.SCash, PayTransType.Authorization);


                    //特店代號：
                    //網威51422 →000812770010705
                    //網威51423 →000812770010706


                    string merchantId = string.Empty;
                    if (apiProvider == PaymentAPIProvider.HiTrustContactWithCVV2)
                    {
                        merchantId = "000812770010705";
                    }
                    else if (apiProvider == PaymentAPIProvider.HiTrustContactWithoutCVV2)
                    {
                        merchantId = "000812770010706";
                    }

                    CreditcardOrder creditcardOrder = op.CreditcardOrderGetByOrderGuid(ptScash.OrderGuid.GetValueOrDefault());
                    string cardNumber = CreditCardUtility.DecryptCardNumber(creditcardOrder.CreditcardInfo);
                    DateTime transTime = ptAuth.TransTime.GetValueOrDefault();
                    decimal transAmount = ptCharging.Amount;
                    decimal refundAmount = pt.Amount;
                    decimal remainAmount = transAmount - refundAmount;
                    string authCode = pt.AuthCode;
                    int installment = ptAuth.Installment.GetValueOrDefault();
                    DateTime createTime = DateTime.Now;

                    //特店代號	
                    //卡號
                    //交易日	(年月日)
                    //原刷金額	
                    //退刷金額	
                    //正確金額 (原刷-退刷	)
                    //授權號碼
                    //分期


                    //pt.HitrustRefundRecord

                    HitrustRefundRecord record = new HitrustRefundRecord
                    {
                        TransId = pt.TransId,
                        TheDate = DateTime.Today,
                        MerchantId = merchantId,
                        CardNumber = cardNumber,
                        TransTime = transTime,
                        TransAmount = transAmount,
                        RefundAmount = refundAmount,
                        RemainAmount = remainAmount,
                        AuthCode = authCode,
                        Installment = installment,
                        CreateTime = DateTime.Now,
                        ApiProvider = (int)apiProvider
                    };

                    op.HitrustRefundRecordSet(record);
                    manual = true;
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.WarnFormat("ProcessHitrustRefund general fail, trans_id={0}, ex={1}", pt.TransId, ex);
                return false;
            }
        }

        static HashSet<PaymentAPIProvider> HitrustApiProviders = new HashSet<PaymentAPIProvider>(new[]            {
            PaymentAPIProvider.HiTrust,
            PaymentAPIProvider.HiTrustPiinLife,
            PaymentAPIProvider.HiTrustPayeasyTravel,
            PaymentAPIProvider.HiTrustUnionPay,
            PaymentAPIProvider.HiTrustContactWithCVV2,
            PaymentAPIProvider.HiTrustContactWithoutCVV2 });
        public static bool SequentialApply(PaymentTransactionCollection ptCol,bool debug = false, bool manual = false)
        {
            int resendTimes = 10;
            bool defaultManual = manual;
            foreach (PaymentTransaction p in ptCol)
            {
                manual = defaultManual;
                PaymentAPIProvider apiProvider = Helper.GetPaymentAPIProvider(p.ApiProvider);

                if (IsByPass(apiProvider))
                {
                    continue;
                }

                //2019年1月15日，網際威信的退款，信用相關的API，於是改用手動退款
                if (ProcessHitrustRefund(p, ref manual)==false)
                {
                    continue;
                }

                DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(p.Status);
                bool success;
                if (p.TransType == (int)PayTransType.Refund && Helper.GetPaymentTransactionPhase(p.Status) == PayTransPhase.Requested)
                {
                    if (debug == false && manual == false)
                    {
                        success = CreditCardUtility.Refund(p.TransId, (int)p.Amount, p.AuthCode);
                    }
                    else
                    {
                        success = true;
                    }

                    logger.DebugFormat("[退款][{0}]授權Id={1}, 金額={2}, apiProvider={3}",
                        success ? "成功" : "失敗", p.TransId, (int)p.Amount, p.ApiProvider);

                    PayTransPhase ptp = success ? PayTransPhase.Successful : PayTransPhase.Failed;
                    PaymentTransaction pt = op.PaymentTransactionGet(p.Id);
                    pt.TransTime = DateTime.Now;
                    pt.Status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), ptp);
                    pt.ApiProvider = (int)apiProvider;
                    pt.Message = manual ? "人工退款" : null;
                    if (debug == false)
                    {
                        op.PaymentTransactionSet(pt);
                    }
                }
                else if (p.TransType == (int)PayTransType.Authorization
                         && Helper.GetPaymentTransactionPhase(p.Status) == PayTransPhase.Successful)
                {
                    PaymentTransactionCollection chargingPtCol = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                        PaymentTransaction.Columns.TransId + " = " + p.TransId,
                        PaymentTransaction.Columns.PaymentType + " = " + (int)PaymentType.Creditcard,
                        PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Charging);
                    if (chargingPtCol.Count > 0)
                    {
                        logger.Debug("提醒unicorn, 的確有 chargingPtCol.Count > 0 的情況, transId=" + p.TransId);
                        continue;
                    }
                    if (debug == false && manual == false)
                    {
                        int times = 0;
                        do
                        {
                            times++;
                            success = CreditCardUtility.Capture(p.TransId, (int)p.Amount, p.AuthCode);
                        } while (times < resendTimes && !success);
                    }
                    else
                    {
                        success = true;
                    }

                    logger.DebugFormat("[請款][{0}]授權Id={1}, 金額={2}, apiProvider={3}",
                        success ? "成功" : "失敗", p.TransId, (int)p.Amount, p.ApiProvider);

                    PayTransPhase ptp = success ? PayTransPhase.Successful : PayTransPhase.Failed;
                    if (debug == false)
                    {
                        PaymentFacade.NewTransaction(p.TransId, new Guid(p.OrderGuid.ToString()), PaymentType.Creditcard, p.Amount, p.AuthCode,
                            PayTransType.Charging, DateTime.Now, "sys", manual ? "人工請款" : null,
                            Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), ptp),
                            PayTransResponseType.Initial, (OrderClassification)p.OrderClassification.Value, null,
                            (PaymentAPIProvider)p.ApiProvider.Value);
                    }
                }
            }
            return true;
        }



        /// <summary>
        /// 檢查是否要執行請款退款
        /// </summary>
        /// <param name="apiProvider"></param>
        /// <returns></returns>
        private static bool IsByPass(PaymentAPIProvider apiProvider)
        {
            if (Enum.Equals(PaymentAPIProvider.NCCC, apiProvider) ||
                 Enum.Equals(PaymentAPIProvider.ForgionCard, apiProvider) ||
                 Enum.Equals(PaymentAPIProvider.PezWebService, apiProvider) ||
                 Enum.Equals(PaymentAPIProvider.Mock, apiProvider) ||
                 Enum.Equals(PaymentAPIProvider.HiTrustComTest, apiProvider) ||
                 Enum.Equals(PaymentAPIProvider.HiTrustDotNetTest, apiProvider) ||
                 Enum.Equals(PaymentAPIProvider.NewebTest, apiProvider))
            {
                return true;
            }

            return false;
        }

        #region Method

        #region 取得異常原因

        /// <summary>
        /// 取得異常原因
        /// </summary>
        /// <param name="iRetCode">回傳碼</param>
        /// <returns></returns>
        public string GetResultName(int iRetCode)
        {
            return GetResultName((CreditcardResult)iRetCode);
        }

        /// <summary>
        /// 取得異常原因
        /// </summary>
        /// <param name="enumRetCode">CreditcardResult 來自 Enumeration/OrderConfig</param>
        /// <returns></returns>
        public string GetResultName(CreditcardResult enumRetCode)
        {
            string resultName;
            switch (enumRetCode)
            {
                case CreditcardResult.Approve:
                    resultName = "授權成功";
                    break;

                case CreditcardResult.UnauththorizedUsage:
                    resultName = "超額/管制/效期錯";
                    break;

                case CreditcardResult.PickupCard:
                    resultName = "掛失卡";
                    break;

                case CreditcardResult.InvalidTranscation:
                    resultName = "3D 驗證失敗";
                    break;

                case CreditcardResult.InvalidCardNumber:
                    resultName = "卡號錯誤";
                    break;

                case CreditcardResult.LossCard:
                    resultName = "遺失卡";
                    break;

                case CreditcardResult.StolenCard:
                    resultName = "被竊卡";
                    break;

                case CreditcardResult.NotSufficientFund:
                    resultName = "超額婉拒交易";
                    break;

                case CreditcardResult.ExpiredCard:
                    resultName = "過期卡/有效日期錯誤";
                    break;

                case CreditcardResult.OverTransaction:
                    resultName = "超額";
                    break;

                case CreditcardResult.OverQuantity:
                    resultName = "單筆消費超出上限";
                    break;

                case CreditcardResult.ReferToCardIssue:
                case CreditcardResult.OverQuantityWithin1Hr:
                    resultName = "短期間連續消費";
                    break;

                case CreditcardResult.IsForeignCard:
                    resultName = "國外卡";
                    break;

                case CreditcardResult.IsBlacklist:
                    resultName = "17Life - 設定黑名單";
                    break;

                case CreditcardResult.OverQuantityWithin24Hr:
                    resultName = "單卡累積消費超出上限";
                    break;

                case CreditcardResult.DenyTranscation:
                    resultName = "否認交易";
                    break;

                case CreditcardResult.SpecialCustomer:
                    resultName = "特殊客戶";
                    break;
                case CreditcardResult.CreditcardFraudIp:
                    resultName = "風險IP";
                    break;
                case CreditcardResult.CreditcardFraudAddress:
                    resultName = "風險地址";
                    break;
                case CreditcardResult.CreditcardFraudAccount:
                    resultName = "風險帳號";
                    break;
                case CreditcardResult.CreditcardFraudOrder:
                    resultName = "風險檔次";
                    break;
                default:
                    resultName = "未定義狀態";
                    break;
            }
            return resultName;
        }

        public List<ObjectCreditcardResult> GetResultList()
        {
            List<ObjectCreditcardResult> rtnList = new List<ObjectCreditcardResult>();
            foreach (CreditcardResult result in Enum.GetValues(typeof(CreditcardResult)))
            {
                ObjectCreditcardResult cr = new ObjectCreditcardResult();
                cr.Id = (int)result;
                cr.Name = result.ToString();
                cr.Title = GetResultName(cr.Id);
                if (cr.Title != GetResultName(999999))
                {
                    rtnList.Add(cr);
                }
            }

            return (rtnList.Count > 0) ? rtnList : null;
        }

        #endregion 取得異常原因

        #endregion Method
    }
}
﻿using LunchKingSite.BizLogic.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Xml;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using MasterCard.SDK;
using Com.MasterCard.Masterpass.Merchant.Api;
using Com.MasterCard.Masterpass.Merchant.Model;
using Com.MasterCard.Masterpass.Merchant.Client;
using Com.MasterCard.Sdk.Core;
using System.Xml.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class MasterPassExpressCheckout
    {
        private readonly IOrderProvider _op;
        private readonly ISysConfProvider _config;
        string CheckoutIdentifier { get; set; }
        string OriginUrl { set; get; }
        string ConsumerKey { get; set; }
        bool Auth_Level_Basic { get { return false; } }
        string CallbackDomain { get; set; }
        string CallbackPath { get; set; }
        /// <summary>
        /// 是否有信用卡紅利相關程式
        /// </summary>
        bool RewardsProgram { get { return false; } }
        string AcceptedCards { get { return "master,visa,jcb"; } }
        /// <summary>
        /// 是否包括運送資料
        /// </summary>
        bool ShippingSuppression { get { return true; } }
        public MasterPassExpressCheckout()
        {
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            CheckoutIdentifier = _config.CheckoutIdentifier;
            OriginUrl = HttpContext.Current.Request.Url.ToString();
            ConsumerKey = _config.ConsumerKey;
            if (HttpContext.Current.Request.Url.Query.Length > 0)
            {
                OriginUrl = OriginUrl.Replace(HttpContext.Current.Request.Url.Query, string.Empty);
            }
            CallbackDomain = _config.SSLSiteUrl;
            CallbackPath = _config.CallbackPath;

            string hostUrl = OriginUrl;
            string KeystorePath = HttpContext.Current.Server.MapPath(_config.CertPath);
            string KeystorePassword = _config.CertPassword;
            var privateKey = new X509Certificate2(KeystorePath, KeystorePassword).PrivateKey;

            //MasterCardApiConfiguration.Sandbox = Convert.ToBoolean(IsSandbox);
            MasterCardApiConfiguration.ConsumerKey = ConsumerKey;
            MasterCardApiConfiguration.PrivateKey = privateKey;
            MasterCardApiConfiguration.Sandbox = true;
        }

        public string precheckouttest()
        {
            try
            {
                PrecheckoutDataRequest precheckoutDataRequest
                                        = new PrecheckoutDataRequest().WithPairingDataTypes(new PairingDataTypes()
                                                .WithPairingDataType(new PairingDataType()
                                                    .WithType("CARD"))
                                                .WithPairingDataType(new PairingDataType()
                                                    .WithType("ADDRESS"))
                                                .WithPairingDataType(new PairingDataType()
                                                    .WithType("PROFILE"))
                                                .WithPairingDataType(new PairingDataType()
                                                    .WithType("REWARD_PROGRAM"))
                                        );

                //Call the PrecheckoutDataApi with required params
                PrecheckoutDataResponse response = PrecheckoutDataApi.Create("longAccessToken", precheckoutDataRequest);
            }
            catch (Exception e)
            {
                string errormsg = string.Empty;
                if (e.InnerException != null)
                {
                    errormsg = e.Message + " / " + e.InnerException;
                }
                else
                {
                    errormsg = e.Message;
                }
                return errormsg;
            }

            return "ok";
        }
        /// <summary>
        /// step1-4 取得卡號與取貨資訊列表
        /// </summary>
        /// <param name="token">MasterpassPreCheckoutToken db_table</param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MasterPassPreCheckoutData PreCheckout(MasterpassPreCheckoutToken token, int userId)
        {
            var result = new MasterPassPreCheckoutData
            {
                IsCheckoutPass = false
            };

            try
            {
                DateTime requestStartTime = DateTime.Now;
                string longAccessToken = token.AccessToken;

                //Create an instance of PrecheckoutDataRequest
                PrecheckoutDataRequest precheckoutDataRequest
                                        = new PrecheckoutDataRequest().WithPairingDataTypes(new PairingDataTypes()
                                                .WithPairingDataType(new PairingDataType()
                                                    .WithType("CARD"))
                                                .WithPairingDataType(new PairingDataType()
                                                    .WithType("ADDRESS"))
                                                .WithPairingDataType(new PairingDataType()
                                                    .WithType("PROFILE"))
                                                .WithPairingDataType(new PairingDataType()
                                                    .WithType("REWARD_PROGRAM"))
                                        );

                //Call the PrecheckoutDataApi with required params
                PrecheckoutDataResponse response = PrecheckoutDataApi.Create(longAccessToken, precheckoutDataRequest);
                DateTime requestEndTime = DateTime.Now;
                TimeSpan ts = new TimeSpan(requestEndTime.Ticks - requestStartTime.Ticks);

                //更新 token 已用過
                //_op.MasterPassPreCheckoutTokenUpdateUsed(userId);
                //更新新的 LongAccessToken
                //_op.MasterPassPreCheckoutTokenUpdateAccessToken(token.UserId, response.LongAccessToken, ts.Seconds);

                result.MasterpassLogoUrl = response.MasterpassLogoUrl;
                result.WalletPartnerLogoUrl = response.WalletPartnerLogoUrl;
                result.ConsumerWalletId = response.PrecheckoutData.ConsumerWalletId;
                result.PrecheckoutTransactionId = response.PrecheckoutData.PrecheckoutTransactionId;
                result.WalletName = response.PrecheckoutData.WalletName;
                result.Cards = new List<MasterPassCard>();

                //是不是要加上地址 response.PrecheckoutData.ShippingAddresses.ShippingAddress

                var accpetedCardsList = AcceptedCards.Split(",").ToList();
                foreach (var card in response.PrecheckoutData.Cards.Card)
                {
                    //card.BillingAddress ?
                    if (!accpetedCardsList.Contains(card.BrandId))
                    {
                        continue;
                    }

                    result.Cards.Add(new MasterPassCard
                    {
                        BrandName = card.BrandName,
                        CardHolderName = card.CardHolderName,
                        CardAlias = card.CardAlias,
                        CardId = card.CardId,
                        LastFour = card.LastFour,
                        SelectedAsDefault = card.SelectedAsDefault.HasValue ? card.SelectedAsDefault.Value : false,
                        ExpiryMonth = card.ExpiryMonth.HasValue ? card.ExpiryMonth.Value : 0,
                        ExpiryYear = card.ExpiryYear.HasValue ? card.ExpiryYear.Value : 0
                    });
                }

                if (result.Cards.Any())
                {
                    result.IsCheckoutPass = true;
                }
                else
                {
                    //若無卡片資料等於綁定失敗
                    result.CheckoutFailReason = "無卡片資料";
                    _op.MasterPassPreCheckoutTokenUpdateUsed(userId);
                }

            }
            catch (Exception ex)
            {
                result.CheckoutFailReason = ex.Message;
                _op.MasterPassPreCheckoutTokenUpdateFailReason(userId, ex.Message);
            }

            return result;
        }

        /// <summary>
        /// step5-7 快速結帳
        /// </summary>
        /// <param name="longAccessToken"></param>
        /// <param name="precheckoutTransactionId"></param>
        /// <param name="selectedCardId"></param>
        /// <param name="selectedShippingAddressId"></param>
        /// <param name="amout"></param>
        /// <returns></returns>
        public CreditCardInfo ExpressCheckout(string longAccessToken, string precheckoutTransactionId, string selectedCardId, string selectedShippingAddressId, int amout)
        {
            try
            {
                ExpressCheckoutRequest expressCheckoutRequest = new ExpressCheckoutRequest
                {
                    PrecheckoutTransactionId = precheckoutTransactionId,  // from precheckout data
                    MerchantCheckoutId = CheckoutIdentifier,
                    OriginUrl = OriginUrl,
                    CurrencyCode = "TWD",
                    AdvancedCheckoutOverride = false, // set to true to disable 3-DS authentication
                    OrderAmount = amout,
                    DigitalGoods = false,
                    CardId = selectedCardId,
                    ShippingAddressId = selectedShippingAddressId,//尚未得知
                };

                ExpressCheckoutResponse response = ExpressCheckoutApi.Create(longAccessToken, expressCheckoutRequest);

                Checkout checkout = response.Checkout;

                Card card = checkout.Card;
                Address billingAddress = card.BillingAddress;
                Contact contact = checkout.Contact;
                AuthenticationOptions authOptions = checkout.AuthenticationOptions;
                ShippingAddress shippingAddress = checkout.ShippingAddress;
                string reCheckoutTransactionId = checkout.PreCheckoutTransactionId;
                string transactionId = checkout.TransactionId;
                string walletId = checkout.WalletID;

                CreditCardInfo cardInfo = new CreditCardInfo()
                {
                    CardNumber = checkout.Card.AccountNumber,
                    ExpiryMonth = checkout.Card.ExpiryMonth.ToString(),
                    ExpiryYear = checkout.Card.ExpiryYear.ToString(),
                    MasterPassTransId = checkout.TransactionId,
                    PayPassWalletIndicator = checkout.WalletID,
                    //OauthToken = oauth_token,
                    CardBrandId = checkout.Card.BrandId,
                    CardBrandName = checkout.Card.BrandName,
                    CardHolder = checkout.Card.CardHolderName
                };
                return cardInfo; //這些資訊已經足夠結帳
            }
            catch (Exception e)
            {
                return null;
            }
        }
        /// <summary>
        /// 1-4 配對中結帳
        /// </summary>
        public MasterPassData GetMasterPassData(int subtotal, string ticketid, bool isApp)
        {
            MasterPassData data = new MasterPassData();
            data.OriginUrl = OriginUrl;
            data.CallbackUrl = isApp ? string.Format("{0}/mvc/MasterPass/CheckoutCallBack", CallbackDomain) :
                string.Format("{0}{1}?TicketId={2}", CallbackDomain, CallbackPath, ticketid);
            data.AcceptedCards = AcceptedCards;
            data.AuthLevelBasic = Auth_Level_Basic;
            data.RewardsProgram = RewardsProgram;
            data.ShippingSuppression = ShippingSuppression;

            string pairingRequestToken = string.Empty;
            string requestToken = string.Empty;
            data.CheckoutIdentifier = CheckoutIdentifier;

            try
            {

                #region  1. Call Request Token Service (2 times)
                //RequestTokenResponse requestTokenResponse = RequestTokenApi.Create(callbackUrl);
                //pairingRequestToken = requestTokenResponse.OauthToken;
                //RequestTokenResponse requestTokenResponse = RequestTokenApi.Create(callbackUrl);
                //requestToken = requestTokenResponse.OauthToken;
                data.RequestToken = requestToken;
                data.PairingToken = pairingRequestToken;
                //data.PairingDataTypes = PairingDataTypeList;
                #endregion

                #region 2. ShoppingCartRequest

                ShoppingCartRequest shoppingCartRequest = new ShoppingCartRequest()
                    .WithShoppingCart(new Com.MasterCard.Masterpass.Merchant.Model.ShoppingCart()
                        .WithSubtotal((long)subtotal * 100)
                        .WithCurrencyCode("TWD")
                        .WithShoppingCartItem(new ShoppingCartItem()
                            //.WithImageURL("https://somemerchant.com/images/xbox.jpg")
                            //.WithValue((long)29999)
                            //.WithDescription("XBox 360")
                            //.WithQuantity(1)
                            )).WithOAuthToken(requestToken);

                //Call the ShoppingCartApi  with required params
                ShoppingCartResponse shoppingCartResponse = ShoppingCartApi.Create(shoppingCartRequest);
                data.ShoppingCartResponse = XElement.Parse(Serializer<ShoppingCartResponse>.Serialize(shoppingCartResponse).OuterXml).ToString();
                #endregion

                #region  3. MerchantInitializationRequest
                //Create an instance of MerchantInitializationRequest
                MerchantInitializationRequest merchantInitializationRequest = new MerchantInitializationRequest()
                        .WithOriginUrl(OriginUrl)
                        .WithOAuthToken(pairingRequestToken);
                //Call the MerchantInitializationApi Service with required params
                MerchantInitializationResponse merchantInitializationResponse = MerchantInitializationApi.Create(merchantInitializationRequest);

                data.MerchantInitRequest = Serializer<MerchantInitializationRequest>.Serialize(merchantInitializationRequest).OuterXml;

                #endregion

                //4. go to view , open the lightbox
                data.IsLoaded = true;

            }
            catch (Exception e)
            {
                data.IsLoaded = false;
            }
            return data;
        }
        
        /// <summary>
        /// 6-7 取得結帳資料
        /// </summary>
        public CreditCardInfo MasterPassCardInfo(int userId, string checkoutResourceUrl, string oauth_token, string oauth_verifier, string pairing_token, string pairing_verifier)
        {
            if (_config.IsMasterPassLocalTest && oauth_token == "test")
            {
                return new CreditCardInfo
                {
                    CardBrandId = "HiTrust",
                    CardBrandName = "HiTrust",
                    CardHolder = "測試者",
                    CardNumber = "8888880000000001",
                    ExpiryMonth = "12",
                    ExpiryYear = "2018",
                    MasterPassTransId = "test",
                    OauthToken = "test",
                    PayPassWalletIndicator = "test"
                };
            }

            #region 6. Call Access Token Service (2 times)

            string requestToken = oauth_token;
            string verifierToken = oauth_verifier;
            string accessToken = string.Empty;
            //AccessTokenResponse accessTokenResponse = AccessTokenApi.Create(requestToken, verifierToken);
            //accessToken = accessTokenResponse.OauthToken;

            
            string pairingRequestToken = pairing_token;
            string pairingVerifierToken = pairing_verifier;
            string longAccessToken = string.Empty;
            //AccessTokenResponse accessTokenResponse = AccessTokenApi.Create(pairingRequestToken, pairingVerifierToken);
            //longAccessToken = accessTokenResponse.OauthToken; // store for future requests
        

            MasterpassPreCheckoutToken longToken = new MasterpassPreCheckoutToken();
            longToken.UserId = userId;
            longToken.AccessToken = longAccessToken;
            longToken.OauthSecret = "";
            longToken.ModifyTime = DateTime.Now;
            longToken.IsUsed = false;
            _op.MasterPassPreCheckoutTokenSet(longToken);
            

            #endregion

            string checkoutId = checkoutResourceUrl.IndexOf('?') != -1 ? checkoutResourceUrl.Substring(checkoutResourceUrl.LastIndexOf('/') + 1).Split('?')[0] : checkoutResourceUrl.Substring(checkoutResourceUrl.LastIndexOf('/') + 1);
            Checkout checkout = CheckoutApi.Show(checkoutId, accessToken);
            Card card = checkout.Card;
            Address billingAddress = card.BillingAddress;
            Contact contact = checkout.Contact;
            AuthenticationOptions authOptions = checkout.AuthenticationOptions;
            string preCheckoutTransactionId = checkout.PreCheckoutTransactionId;
            ShippingAddress shippingAddress = checkout.ShippingAddress;
            string transactionId = checkout.TransactionId;
            string walletId = checkout.WalletID;

            CreditCardInfo cardInfo = new CreditCardInfo()
            {
                CardNumber = checkout.Card.AccountNumber,
                ExpiryMonth = checkout.Card.ExpiryMonth.ToString(),
                ExpiryYear = checkout.Card.ExpiryYear.ToString(),
                MasterPassTransId = checkout.TransactionId,
                PayPassWalletIndicator = checkout.WalletID,
                //OauthToken = oauth_token,
                CardBrandId = checkout.Card.BrandId,
                CardBrandName = checkout.Card.BrandName,
                CardHolder = checkout.Card.CardHolderName
            };
            return cardInfo;
        }
        
        /// <summary>
        /// step8 最後LOG回傳至masterpass後也要儲存
        /// </summary>
        /// <param name="masterpass_transid"></param>
        /// <param name="masterpass_indicator"></param>
        /// <param name="subtotal"></param>
        /// <param name="return_code"></param>
        /// <param name="resultcode"></param>
        /// <param name="orderGuid"></param>
        /// <param name="userid"></param>
        /// <param name="oauthToken"></param>
        /// <param name="cardBrandId"></param>
        /// <param name="cardBrandName"></param>
        /// <param name="cardHolder"></param>
        /// <param name="cardNumber"></param>
        public void MasterPassTransLog(string masterpass_transid, string masterpass_indicator, int subtotal
         , string return_code, int resultcode, Guid orderGuid, int userid, string oauthToken
         , string cardBrandId, string cardBrandName, string cardHolder, string cardNumber)
        {
            //Create an instance of MerchantTransactions
            MerchantTransactions merchantTransactions = new MerchantTransactions()
                    .With_MerchantTransactions(new MerchantTransaction()
                        .WithTransactionId(masterpass_transid)
                        .WithPurchaseDate(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))
                        .WithExpressCheckoutIndicator(false)
                        .WithApprovalCode(string.IsNullOrEmpty(return_code) ? "000000" : return_code.PadLeft(6, '0'))
                        .WithTransactionStatus("Success")
                        .WithOrderAmount((long)subtotal * 100)
                        .WithCurrency("TWD")
                        .WithConsumerKey(ConsumerKey));

            //Call the PostbackService with required params
            MerchantTransactions result = PostbackApi.Create(merchantTransactions);

            XmlDocument responseXML = Serializer<MerchantTransactions>.Serialize(result);
            _op.MasterPassAdd(new MasterpassLog()
            {
                OrderGuid = orderGuid,
                Indicator = masterpass_indicator,
                ResponseContent = responseXML.OuterXml,
                CreateTime = DateTime.Now,
                Uniqueid = userid,
                MasterpassTransid = masterpass_transid,
                Subtotal = subtotal,
                OauthToken = oauthToken,
                CardBrandId = cardBrandId,
                CardBrandName = cardBrandName,
                CardHolder = cardHolder,
                AccountNumber = MemberFacade.EncryptCreditCardInfo(cardNumber, orderGuid.ToString().Substring(0, 16)),
                InfoNumber = cardNumber.Length > 5 ? cardNumber.Substring(0, 6) : string.Empty
            });
        }

        public void MasterPassTransErrorLog(Guid order_guid, int userid, string errormessage)
        {
            _op.MasterPassAdd(new MasterpassLog() { OrderGuid = order_guid, Indicator = "error", ResponseContent = errormessage, CreateTime = DateTime.Now, Uniqueid = userid });
        }
    }
}

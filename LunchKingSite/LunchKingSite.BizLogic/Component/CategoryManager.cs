﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;
using LunchKingSite.BizLogic.Models;
using log4net;

namespace LunchKingSite.BizLogic.Component
{
    public class CategoryManager
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(CategoryManager));

        #region const
        /// <summary>
        /// 到店的category id
        /// </summary>
        private const int ToShopCategoryId = 135;
        /// <summary>
        /// 宅配的category id
        /// </summary>
        private const int ToHouseCategoryId = 136;
        /// <summary>
        /// 即買即用的category id
        /// </summary>
        private const int CanBeUsedImmediatelyId = 137;        

        /// <summary>
        /// 分類版本設定於DB ExtendedProperty 中的名稱
        /// </summary>
        private const string CategoryVersionExtendedPropertyName = "category_version";
        /// <summary>
        /// 分類版本設定於DB ExtendedProperty 中的名稱
        /// </summary>
        private const string MembershipCardCategoryVersionExtendedPropertyName = "membership_card_category_version";
        #endregion const
        private static ISellerProvider _sellerProvider;
        private static ISystemProvider _systemProvider;
        private static readonly log4net.ILog _log;

        private static CategoryCollection _categories;
        private static ViewCategoryDependencyCollection _categoryDependencies;

        #region 解決「city.id、category.id」Hard Code要用的object

        public static PponCityId pponCityId { get; private set; }
        public static PponChannelId pponChannelId { get; private set; }
        public static PponFoodRegionId pponFoodRegionId { get; private set; }

        #endregion

        #region 產生前台區域選單會用到的 Dictionary  (only CategoryType in (4, 5))
        private static Dictionary<int, Category> _categoriesDic;
        private static Dictionary<int, List<ViewCategoryDependency>> _categoryDependenciesDic;
        private static Dictionary<int, CategoryRegionDisplayRule> _categoryRegionDisplayRuleDic;

        /// <summary>
        /// 以頻道為最上層節點的地區樹
        /// </summary>
        public static List<CategoryTreeNode> _channelCategoryTree { get; private set; }
        #endregion

        public static Dictionary<int, string> CategoriesName { get; private set; }

        private static readonly List<int> _appChannelId = new List<int>() { 87, 88, 89, 90, 91};

        public static List<int> AppChannelId
        {
            get { return _appChannelId; }
        }

        public static List<int> AppChannelIdNew
        {
            get
            {
                return ViewPponDealManager.AppChannelIdNew;
            }
        }

        private static List<int> _SKMDealTypes;
        public static List<int> SKMDealTypes
        {
            get
            {
                if ((_SKMDealTypes == null) == true)
                {
                    _SKMDealTypes = new List<int>();
                    foreach (var item in config.MobileChannelSKMDefaultDealTypes.Split(',').ToList())
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            _SKMDealTypes.Add(Convert.ToInt32(item));
                        }
                    }
                }
                return _SKMDealTypes;
            }
        }

        private static int _categoryVersion;
        public static int CategoryVersion
        {
            get { return _categoryVersion; }
        }


        public static string ApiVersion{ get;set;}


        /// <summary>
        /// 取得目前PponChannelCategory相關資料的版本設定，供前端判定是否資料有更新
        /// </summary>
        public static ApiVersionItem PponChannelCategoryVersion
        {
            get
            {
                return new ApiVersionItem("PponChannelCategory", _categoryVersion.ToString());
            }
        }

        private static int _membershipCardCategoryVersion;

        /// <summary>
        /// 取得目前熟客卡使用的分類資料的版本設定，供前端判定是否資料有更新
        /// </summary>
        public static ApiVersionItem MembershipCardCategoryVersion
        {
            get
            {
                return new ApiVersionItem("MembershipCardCategoryVersion", _membershipCardCategoryVersion.ToString());
            }
        }

        /// <summary>
        /// 檔次頻道CategoryTree
        /// </summary>
        private static CategoryTypeNode _pponChannelCategoryTree;
        /// <summary>
        /// 檔次頻道CategoryTree
        /// </summary>
        public static CategoryTypeNode PponChannelCategoryTree
        {
            get { return _pponChannelCategoryTree.Copy(); }
        }

        /// <summary>
        /// 送貨方式的CategoryTree
        /// </summary>
        private static CategoryTypeNode _deliveryTypeCategoryTree;
        /// <summary>
        /// 送貨方式的CategoryTree
        /// </summary>
        public static CategoryTypeNode DeliveryTypeCategoryTree
        {
            get { return _deliveryTypeCategoryTree; }
        }


        /// <summary>
        /// 宅配的類型
        /// </summary>
        private static Category _toHouseCategory;
        /// <summary>
        /// 到店的類型
        /// </summary>
        private static Category _toShopCategory;

        /// <summary>
        /// 即買即用的類型
        /// </summary>
        public static Category CanBeUsedImmediately { get; private set; }

        /// <summary>
        /// DealLabelSystemCode 轉換使用category的過渡期，需要 categoryId 對應的 DealLabelSystemCode 資料
        /// </summary>
        private static List<KeyValuePair<int, DealLabelSystemCode>> _dealLabelSystemCodeMap;

        /// <summary>
        /// travel 對應 pponChannelArea 的category資料
        /// </summary>
        private static List<KeyValuePair<Category, Category>> _travelWithPponChannelArea;

        private static List<CategoryNode> _categoryNodesForSubscription = new List<CategoryNode>();

        private static List<CategoryAllNodeSetting> _allNodeSettings;
        /// <summary>
        /// CategoryType需要於server端增加全部項目的相關設定資料
        /// </summary>
        public static List<CategoryAllNodeSetting> AllNodeSettings
        {
            get { return _allNodeSettings; }
        }

        /// <summary>
        /// 舊區域分類樹的節點
        /// </summary>
        public class DefaultCategoryNode
        {
            private Dictionary<int, CategoryNode> _nodes = new Dictionary<int, CategoryNode>();

            public CategoryNode PponDeal { get; set; }

            public CategoryNode Taipei { get; set; }
            public CategoryNode Taoyuan { get; set; }
            public CategoryNode Hsinchu { get; set; }
            public CategoryNode Taichung { get; set; }
            public CategoryNode Tainan { get; set; }
            public CategoryNode Kaohsiung { get; set; }

            public CategoryNode Delivery { get; set; }
            public CategoryNode Beauty { get; set; }
            public CategoryNode Travel { get; set; }

            public CategoryNode Skm { get; set; }
            public CategoryNode DepositCoffee { get; set; }
            public CategoryNode Family { get; set; }

            public CategoryNode AppLimitedEdition { get; set; }

            public CategoryNode TravelNorth { get; set; }
            public CategoryNode TravelEast { get; set; }
            public CategoryNode TravelSouth { set; get; }
            public CategoryNode TravelMidland { get; set; }
            public CategoryNode TravelOutlyingIslands { set; get; }

            public CategoryNode FemaleTaipei { get; set; }
            public CategoryNode FemaleTaoyuan { get; set; }
            public CategoryNode FemaleHsinchu { get; set; }
            public CategoryNode FemaleTaichung { get; set; }
            public CategoryNode FemaleTainan { get; set; }
            public CategoryNode FemaleKaohsiung { get; set; }
            public CategoryNode Piinlife { get; set; }
            public CategoryNode PponSelect { get; set; }

            public CategoryNode LastDay { get; set; }

            public CategoryNode Regulars { get; set; }
            public CategoryNode Vourcher { get; set; }

            public void Ready()
            {
                _nodes.Clear();
                _nodes.Add(Taipei.CategoryId, Taipei);
                _nodes.Add(Taoyuan.CategoryId, Taoyuan);
                _nodes.Add(Hsinchu.CategoryId, Hsinchu);
                _nodes.Add(Taichung.CategoryId, Taichung);
                _nodes.Add(Tainan.CategoryId, Tainan);
                _nodes.Add(Kaohsiung.CategoryId, Kaohsiung);
                _nodes.Add(Delivery.CategoryId, Delivery);
                _nodes.Add(Beauty.CategoryId, Beauty);
                _nodes.Add(Travel.CategoryId, Travel);

                _nodes.Add(Skm.CategoryId, Skm);
                _nodes.Add(DepositCoffee.CategoryId, DepositCoffee);
                _nodes.Add(Family.CategoryId, Family);
                _nodes.Add(AppLimitedEdition.CategoryId, AppLimitedEdition);

                _nodes.Add(TravelNorth.CategoryId, TravelNorth);
                _nodes.Add(TravelEast.CategoryId, TravelEast);
                _nodes.Add(TravelSouth.CategoryId, TravelSouth);
                _nodes.Add(TravelMidland.CategoryId, TravelMidland);
                _nodes.Add(TravelOutlyingIslands.CategoryId, TravelOutlyingIslands);

                _nodes.Add(FemaleTaipei.CategoryId, FemaleTaipei);
                _nodes.Add(FemaleTaoyuan.CategoryId, FemaleTaoyuan);
                _nodes.Add(FemaleHsinchu.CategoryId, FemaleHsinchu);
                _nodes.Add(FemaleTaichung.CategoryId, FemaleTaichung);
                _nodes.Add(FemaleTainan.CategoryId, FemaleTainan);
                _nodes.Add(FemaleKaohsiung.CategoryId, FemaleKaohsiung);
                _nodes.Add(Piinlife.CategoryId, Piinlife);
                _nodes.Add(PponSelect.CategoryId, PponSelect);

                _nodes.Add(LastDay.CategoryId, LastDay);

                _nodes.Add(Regulars.CategoryId, Regulars);
                _nodes.Add(Vourcher.CategoryId, Vourcher);
            }

            public CategoryNode Find(int categoryId)
            {
                if (_nodes.ContainsKey(categoryId))
                {
                    return _nodes[categoryId];
                }
                return null;
            }


            /*
<select name="ctl00$ctl00$ML$MC$ddlcity" id="ctl00_ctl00_ML_MC_ddlcity">
	<option value="請選擇">請選擇</option>
	<option value="1">基隆市</option>
	<option value="199">台北市</option>
	<option value="294">新北市</option>
	<option value="356">桃園縣</option>
	<option value="338">新竹市</option>
	<option value="342">新竹縣</option>
	<option value="370">苗栗縣</option>
	<option value="389">台中市</option>
	<option value="31">彰化縣</option>
	<option value="58">南投縣</option>
	<option value="95">雲林縣</option>
	<option value="72">嘉義市</option>
	<option value="75">嘉義縣</option>
	<option value="116">台南市</option>
	<option value="155">高雄市</option>
	<option value="226">屏東縣</option>
	<option value="262">台東縣</option>
	<option value="280">花蓮縣</option>
	<option value="324">宜蘭縣</option>
	<option value="212">澎湖縣</option>
	<option value="219">金門縣</option>
	<option value="478">連江縣</option>
</select>
             */
            //北市、新北市 歸在北市，看正式站資料直接寫死
            /// <summary>
            /// 從 cityId 查找 Category，注意 北市、新北市、基隆都查找到北市這個分類，找不到的回傳 null
            /// </summary>
            /// <param name="cityId"></param>
            /// <returns></returns>
            public CategoryNode FindByCity(int cityId)
            {
                switch (cityId)
                {
                    case 1:
                    case 199:
                    case 294:
                        return Default.Taipei;
                    case 356:
                        return Default.Taoyuan;
                    case 338:
                    case 342:
                    case 370:
                        return Default.Hsinchu;
                    case 389:
                    case 31:
                    case 58:
                        return Default.Taichung;
                    case 95:
                    case 72:
                    case 75:
                    case 116:
                        return Default.Tainan;
                    case 155:
                    case 226:
                        return Default.Kaohsiung;

                    default:
                        return Default.Taipei;
                }
            }
        }

        public static DefaultCategoryNode Default;

        static CategoryManager()
        {
            _sellerProvider = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _systemProvider = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _log = log4net.LogManager.GetLogger(typeof(CategoryManager));

            if ((_categories == null) || (_categories.Count == 0) || (_categoryDependencies == null) || (_categoryDependencies.Count == 0))
            {
                ReloadManager();
            }

            _dealLabelSystemCodeMap = new List<KeyValuePair<int, DealLabelSystemCode>>();

            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(137, DealLabelSystemCode.CanBeUsedImmediately));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(138, DealLabelSystemCode.ArriveIn24Hrs));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(139, DealLabelSystemCode.ArriveIn72Hrs));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(140, DealLabelSystemCode.FiveStarHotel));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(141, DealLabelSystemCode.FourStarHotel));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(149, DealLabelSystemCode.PiinlifeEvent));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(158, DealLabelSystemCode.Visa));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(307, DealLabelSystemCode.AppLimitedEdition));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(314, DealLabelSystemCode.PiinlifeValentinesEvent));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(315, DealLabelSystemCode.PiinlifeFatherEvent));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(316, DealLabelSystemCode.PiinlifeMoonEvent));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(319, DealLabelSystemCode.PiinlifeMajiEvent));
            _dealLabelSystemCodeMap.Add(new KeyValuePair<int, DealLabelSystemCode>(322, DealLabelSystemCode.PiinlifeRecommend));

        }

        #region public

        /// <summary>
        /// 不論原本有無查詢過，重新查詢需要的暫存資料
        /// </summary>
        public static void ReloadManager()
        {
            DateTime now = System.DateTime.Now;
            CategoryCollection tempCategories =
                _sellerProvider.CategoryGetList(0, 0,
                                                string.Format("{0} , {1}", Category.Columns.Type, Category.Columns.Rank),
                                                Category.Columns.Status + " = " + (int)CategoryStatus.Enabled);

            ViewCategoryDependencyCollection tempCategoryDependencies =
                _sellerProvider.ViewCategoryDependencyGetAll(string.Empty);

            _categories = tempCategories;
            _categoryDependencies = tempCategoryDependencies;
            CategoriesName = _sellerProvider.CategoriesNameGet();

            #region 長分類樹用的cache

            #region 將cityId用成class模擬列舉
            pponCityId = new PponCityId();
            pponChannelId = new PponChannelId();
            pponFoodRegionId = new PponFoodRegionId();
            #endregion

            _categoriesDic = tempCategories.Where(x => x.Type == (int)CategoryType.PponChannelArea ||
                                                       x.Type == (int)CategoryType.PponChannel)
                                           .GroupBy(x => x.Id)
                                           .OrderBy(x => x.Key)
                                           .ToDictionary(x => x.Key, x => x.FirstOrDefault());
            _categoryDependenciesDic = tempCategoryDependencies.Where(x => x.CategoryType == (int)CategoryType.PponChannelArea ||
                                                                           x.CategoryType == (int)CategoryType.PponChannel)
                                                               .Where(x => x.ParentId != x.CategoryId)
                                                               .GroupBy(x => x.ParentId)
                                                               .OrderBy(x => x.Key)
                                                               .ToDictionary(x => x.Key, x => x.ToList());

            _categoryRegionDisplayRuleDic = _sellerProvider.CategoryRegionDisplayRuleGetList().GroupBy(x => x.CategoryId)
                                                                                              .ToDictionary(x => x.Key, x => x.FirstOrDefault());


            var hxRoot = HxRootCategory.Create();            
            _channelCategoryTree = RenderCategoryTree(hxRoot);
            #endregion

            CategoryTypeNode tempPponChannelCategoryTree = CategoryTypeNodeGetByType(CategoryType.PponChannel, null);
            _pponChannelCategoryTree = tempPponChannelCategoryTree;
            SetupCategoryParentNode(tempPponChannelCategoryTree);
            
            CategoryTypeNode tempDeliveryTypeCategoryTree = CategoryTypeNodeGetByType(CategoryType.DeliveryType, null);
            _deliveryTypeCategoryTree = tempDeliveryTypeCategoryTree;

            _toShopCategory = _categories.FirstOrDefault(x => x.Id == ToShopCategoryId);
            _toHouseCategory = _categories.FirstOrDefault(x => x.Id == ToHouseCategoryId);
            CanBeUsedImmediately = _categories.FirstOrDefault(x => x.Id == CanBeUsedImmediatelyId);

            #region 新版分類與舊版分類切換時期，舊版旅遊商品區域與新版的對應
            _travelWithPponChannelArea = new List<KeyValuePair<Category, Category>>();
            _travelWithPponChannelArea.Add(new KeyValuePair<Category, Category>(_categories.FirstOrDefault(x => x.Id == 67), _categories.FirstOrDefault(x => x.Id == 101)));
            _travelWithPponChannelArea.Add(new KeyValuePair<Category, Category>(_categories.FirstOrDefault(x => x.Id == 68), _categories.FirstOrDefault(x => x.Id == 102)));
            _travelWithPponChannelArea.Add(new KeyValuePair<Category, Category>(_categories.FirstOrDefault(x => x.Id == 69), _categories.FirstOrDefault(x => x.Id == 103)));
            _travelWithPponChannelArea.Add(new KeyValuePair<Category, Category>(_categories.FirstOrDefault(x => x.Id == 70), _categories.FirstOrDefault(x => x.Id == 104)));
            _travelWithPponChannelArea.Add(new KeyValuePair<Category, Category>(_categories.FirstOrDefault(x => x.Id == 71), _categories.FirstOrDefault(x => x.Id == 105)));
            #endregion 新版分類與舊版分類切換時期，舊版旅遊商品區域與新版的對應


            CategoryNode deliveryNode = tempPponChannelCategoryTree.CategoryNodes.First(t => t.CategoryId == 88);
            deliveryNode.CityId = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId;
            CategoryNode beautyNode = tempPponChannelCategoryTree.CategoryNodes.First(t => t.CategoryId == 89);
            beautyNode.CityId = PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId;

            List<CategoryNode> pponSpaNodes = beautyNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
            CategoryNode femaleTaipei = pponSpaNodes.First(t => t.CategoryId == 142);
            femaleTaipei.CityId = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
            CategoryNode femaleTaoyuan = pponSpaNodes.First(t => t.CategoryId == 143);
            femaleTaoyuan.CityId = PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId;
            CategoryNode femaleHsinchu = pponSpaNodes.First(t => t.CategoryId == 144);
            femaleHsinchu.CityId = PponCityGroup.DefaultPponCityGroup.Hsinchu.CityId;
            CategoryNode femaleTaichung = pponSpaNodes.First(t => t.CategoryId == 145);
            femaleTaichung.CityId = PponCityGroup.DefaultPponCityGroup.Taichung.CityId;
            CategoryNode femaleTainan = pponSpaNodes.First(t => t.CategoryId == 146);
            femaleTainan.CityId = PponCityGroup.DefaultPponCityGroup.Tainan.CityId;
            CategoryNode femaleKaohsiung = pponSpaNodes.First(t => t.CategoryId == 147);
            femaleKaohsiung.CityId = PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId;

            CategoryNode piinlife = tempPponChannelCategoryTree.CategoryNodes.First(t => t.CategoryId == 148);
            piinlife.CityId = PponCityGroup.DefaultPponCityGroup.Piinlife.CityId;
            
            CategoryNode travelNode = tempPponChannelCategoryTree.CategoryNodes.First(t => t.CategoryId == 90);
            travelNode.CityId = PponCityGroup.DefaultPponCityGroup.Travel.CityId;

            CategoryNode pponSelect = CategoryTypeNodeGetByType(CategoryType.None, null).CategoryNodes.First(t => t.CategoryId == 184);
            pponSelect.CityId = PponCityGroup.DefaultPponCityGroup.PponSelect.CityId;
            CategoryNode appLimitedEdition = CategoryTypeNodeGetByType(CategoryType.None, null).CategoryNodes.First(t => t.CategoryId == 307);

            CategoryNode pponNode = tempPponChannelCategoryTree.CategoryNodes.First(t => t.CategoryId == 87);

            pponNode.SetSubCategoryTypeNodeCity(CategoryType.PponChannelArea);
            List<CategoryNode> pponAreaNodes = pponNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
            CategoryNode pponTP = pponAreaNodes.First(t => t.CategoryId == 94);
            CategoryNode pponTY = pponAreaNodes.First(t => t.CategoryId == 95);
            CategoryNode pponXZ = pponAreaNodes.First(t => t.CategoryId == 96);
            CategoryNode pponTC = pponAreaNodes.First(t => t.CategoryId == 97);
            CategoryNode pponTN = pponAreaNodes.First(t => t.CategoryId == 98);
            CategoryNode pponGX = pponAreaNodes.First(t => t.CategoryId == 99);

            CategoryNode skmNode = tempPponChannelCategoryTree.CategoryNodes.First(t => t.CategoryId == 185);
            skmNode.CityId = PponCityGroup.DefaultPponCityGroup.Skm.CityId;

            CategoryNode depositCoffeeNode = tempPponChannelCategoryTree.CategoryNodes.First(t => t.CategoryId == 186);
            depositCoffeeNode.CityId = PponCityGroup.DefaultPponCityGroup.DepositCoffee.CityId;

            CategoryNode familyNode = tempPponChannelCategoryTree.CategoryNodes.First(t => t.CategoryId == 91);
            familyNode.CityId = PponCityGroup.DefaultPponCityGroup.Family.CityId;

            CategoryTypeNode specialCategory = CategoryTypeNodeGetByType(CategoryType.DealSpecialCategory, null);

            CategoryNode travelCategoryNode = tempPponChannelCategoryTree.CategoryNodes.First(t => t.CategoryId == 90);

            List<CategoryNode> pponTravelNodes = travelCategoryNode.NodeDatas[0].CategoryNodes;
            CategoryNode travelNorth = pponTravelNodes.First(t => t.CategoryId == 101);
            travelNorth.CityId = PponCityGroup.DefaultPponCityGroup.Travel.CityId;
            CategoryNode travelMidland = pponTravelNodes.First(t => t.CategoryId == 102);
            travelMidland.CityId = PponCityGroup.DefaultPponCityGroup.Travel.CityId;
            CategoryNode travelSouth = pponTravelNodes.First(t => t.CategoryId == 103);
            travelSouth.CityId = PponCityGroup.DefaultPponCityGroup.Travel.CityId;
            CategoryNode travelEast = pponTravelNodes.First(t => t.CategoryId == 104);
            travelEast.CityId = PponCityGroup.DefaultPponCityGroup.Travel.CityId;
            CategoryNode travelOutlyingIslands = pponTravelNodes.First(t => t.CategoryId == 105);
            travelOutlyingIslands.CityId = PponCityGroup.DefaultPponCityGroup.Travel.CityId;

            CategoryNode lastDay = specialCategory.CategoryNodes.First(t => t.CategoryId == 318);

            CategoryNode regulars = tempPponChannelCategoryTree.CategoryNodes.First(t => t.CategoryId == 2160);
            CategoryNode vourcher = tempPponChannelCategoryTree.CategoryNodes.First(t => t.CategoryId == 2161);
            _categoryNodesForSubscription.Clear();

            _categoryNodesForSubscription.Add(pponTP);
            _categoryNodesForSubscription.Add(pponTY);
            _categoryNodesForSubscription.Add(pponXZ);
            _categoryNodesForSubscription.Add(pponTC);
            _categoryNodesForSubscription.Add(pponTN);
            _categoryNodesForSubscription.Add(pponGX);
            _categoryNodesForSubscription.Add(beautyNode);
            _categoryNodesForSubscription.Add(travelNode);
            _categoryNodesForSubscription.Add(deliveryNode);
            _categoryNodesForSubscription.Add(pponSelect);
            _categoryNodesForSubscription.Add(piinlife);

            Default = new DefaultCategoryNode
            {
                PponDeal = pponNode,
                Taipei = pponTP,
                Taoyuan = pponTY,
                Hsinchu = pponXZ,
                Taichung = pponTC,
                Tainan = pponTN,
                Kaohsiung = pponGX,
                Delivery = deliveryNode,
                Beauty = beautyNode,
                Travel = travelNode,
                Skm = skmNode,
                DepositCoffee = depositCoffeeNode,
                Family = familyNode,
                TravelNorth = travelNorth,
                TravelMidland = travelMidland,
                TravelSouth = travelSouth,
                TravelEast = travelEast,
                TravelOutlyingIslands = travelOutlyingIslands,
                FemaleTaipei = femaleTaipei,
                FemaleTaoyuan = femaleTaoyuan,
                FemaleHsinchu = femaleHsinchu,
                FemaleTaichung = femaleTaichung,
                FemaleTainan = femaleTainan,
                FemaleKaohsiung = femaleKaohsiung,
                Piinlife = piinlife,
                PponSelect = pponSelect,
                AppLimitedEdition = appLimitedEdition,
                LastDay = lastDay,
                Regulars = regulars,
                Vourcher = vourcher
            };
            Default.Ready();

            #region CategoryVersion

            LoadCategoryVersion();

            #endregion CategoryVersion

            #region MembershipCardCategoryVersion
            //查詢MembershipCardCategoryVersion資料設定的版本，供前端作業判斷資料是否已異動
            string membershipCardCategoryVersion = _systemProvider.DBExtendedPropertiesGet(MembershipCardCategoryVersionExtendedPropertyName);
            //若查無資料或取出的值不為int，預設為0
            if (!int.TryParse(membershipCardCategoryVersion, out _membershipCardCategoryVersion))
            {
                _membershipCardCategoryVersion = 0;
            }
            #endregion MembershipCardCategoryVersion

            LoadCategoryAllNodeSetting();


            logger.InfoFormat("CategoryManager's spent {0} secs", (DateTime.Now - now).TotalSeconds.ToString("#.##"));
        }

        public static CategoryTreeNode Find(int channelId, int areaId)
        {
            List<CategoryTreeNode> categoryTree = CategoryManager._channelCategoryTree;
            var channel = categoryTree.FirstOrDefault(t => t.CategoryId == channelId);
            if (channel == null)
            {
                return null;
            }
            foreach (var child in channel.Children)
            {
                if (child.CategoryId == areaId)
                {
                    return child;
                }
                else
                {
                    foreach (var grandChild in child.Children)
                    {
                        if (grandChild.CategoryId == areaId)
                        {
                            return grandChild;
                        }
                        else
                        {
                            foreach (var grandGrandChild in grandChild.Children)
                            {
                                if (grandGrandChild.CategoryId == areaId)
                                {
                                    return grandGrandChild;
                                }
                                else
                                {
                                    foreach (var grandGrandGrandChild in grandGrandChild.Children)
                                    {
                                        if (grandGrandGrandChild.CategoryId == areaId)
                                        {
                                            return grandGrandGrandChild;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        private static void SetupCategoryParentNode(CategoryTypeNode tempPponChannelCategoryTree, CategoryNode parent = null)
        {
            tempPponChannelCategoryTree.Parent = parent;
            foreach (var node in tempPponChannelCategoryTree.CategoryNodes)
            {
                node.Parent = tempPponChannelCategoryTree;
                foreach (var typeNode in node.NodeDatas)
                {
                    typeNode.Parent = node;
                    SetupCategoryParentNode(typeNode, node);
                }
            }
        }

        public static ViewCategoryDependencyCollection CategoryDependencyGetAll()
        {
            return _categoryDependencies;
        }
        public static List<Category> CategoryGetAllList()
        {
            var result = _categories.ToList();
            return result;
        }
        public static List<Category> CategoryGetListByType(CategoryType type)
        {
            var result = _categories.Where(x => x.Type != null && (CategoryType)x.Type == type).ToList();
            return result;
        }
        /// <summary>
        /// 將DeliveryType也當作Category來處理，此function依據deliveryType回傳對應的category
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Category DeliveryCategeryGetByDeliveryType(DeliveryType type)
        {
            switch (type)
            {
                case DeliveryType.ToHouse:
                    return _toHouseCategory;
                case DeliveryType.ToShop:
                    return _toShopCategory;
                default:
                    return null;
            }
        }

        public static Category CategoryGetById(int categoryId)
        {
            var result = _categories.Where(x => x.Id == categoryId).FirstOrDefault();
            return result;
        }

        public static CategoryIconType GetCategoryIconTypeById(int categoryId)
        {
            var result = _categories.Where(x => x.Id == categoryId).FirstOrDefault();
            if (result != null)
            {
                return (CategoryIconType)result.IconType;
            }
            return CategoryIconType.None;
        }

        /// <summary>
        /// DealLabelSystemCode 轉換使用category的過渡期，需要categoryId對應的DealLabelSystemCode
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static DealLabelSystemCode? GetDealLabelSystemCodeByCategoryId(int categoryId)
        {
            List<KeyValuePair<int, DealLabelSystemCode>> dataList = _dealLabelSystemCodeMap.Where(x => x.Key == categoryId).ToList();
            if (dataList.Count > 0)
            {
                return dataList.First().Value;
            }
            return null;
        }
        /// <summary>
        /// DealLabelSystemCode 轉換使用category的過渡期，需要DealLabelSystemCode對應的categoryId
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static int? GetCategoryIdByDealLabelSystemCode(DealLabelSystemCode code)
        {
            List<KeyValuePair<int, DealLabelSystemCode>> dataList = _dealLabelSystemCodeMap.Where(x => x.Value == code).ToList();
            if (dataList.Count > 0)
            {
                return dataList.First().Key;
            }
            return null;
        }
        /// <summary>
        /// 依據 CategoryType為Travel的值，取得對應到的新版分類的Category物件，此函式為過渡期轉換用，無對應值回傳null
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static Category GetPponChannelAreaByTravel(int categoryId)
        {
            var travelList = _travelWithPponChannelArea.Where(x => x.Key.Id == categoryId).ToList();
            if (travelList.Count > 0)
            {
                return travelList.First().Value;
            }
            return null;
        }
        /// <summary>
        /// 依據 CategoryType為PponChannelArea的值，取得對應到的新版分類的Category物件，此函式為過渡期轉換用，無對應值回傳null
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static Category GetTravelByPponChannelArea(int categoryId)
        {
            var travelList = _travelWithPponChannelArea.Where(x => x.Value.Id == categoryId).ToList();
            if (travelList.Count > 0)
            {
                return travelList.First().Key;
            }
            return null;
        }
        /// <summary>
        /// 依據傳入的CategoryId回傳頻道的CategoryNode物件，查無物件回傳null
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static CategoryNode GetChannelNodeByCategoryId(int categoryId)
        {
            List<CategoryNode> nodes = PponChannelCategoryTree.CategoryNodes.Where(x => x.CategoryId == categoryId).ToList();
            if (nodes.Count > 0)
            {
                return nodes.First().Copy();
            }
            return null;
        }

        public static CategoryNode GetCategoryNodeByCategoryId(int categoryId)
        {
            CategoryNode findNode = null;
            GetCategoryNodeByCategoryId(PponChannelCategoryTree, categoryId, ref findNode);
            return findNode;
        }

        public static void GetCategoryNodeByCategoryId(
            CategoryTypeNode typeNode, int categoryId, ref CategoryNode findNode)
        {
            foreach (var node in typeNode.CategoryNodes)
            {
                if (node.CategoryId == categoryId)
                {
                    findNode = node;
                    break;
                }
                foreach (var childTypeNode in node.NodeDatas)
                {
                    GetCategoryNodeByCategoryId(childTypeNode, categoryId, ref findNode);
                    if (findNode != null)
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// 取得從最上層一直到自己的 CategoryNode
        /// 
        /// top->parent->self
        /// </summary>
        /// <param name="categoryNode"></param>
        /// <param name="withSelf"></param>
        /// <returns></returns>
        public static CategoryNode[] GetCategoryPaths(CategoryNode categoryNode, bool withSelf = true)
        {
            List<CategoryNode> result = new List<CategoryNode>();
            if (categoryNode == null)
            {
                return result.ToArray();
            }
            if (withSelf)
            {
                result.Add(categoryNode);
            }
            CategoryNode currentNode = categoryNode;
            do
            {
                CategoryTypeNode parentTypeNode = currentNode.Parent;
                if (parentTypeNode == null)
                {
                    break;
                }
                CategoryNode parentNode = parentTypeNode.Parent;
                if (parentNode == null)
                {
                    break;
                }
                result.Add(parentNode);
                currentNode = parentNode;
            } while (true);
            result.Reverse();
            return result.ToArray();
        }

        public static CategoryNode GetDefaultCategoryNode()
        {
            return CategoryNode.GetDefaultCategory();
        }

        public static List<CategoryNode> CategoryNodesForSubscription
        {
            get { return _categoryNodesForSubscription; }
        }


        /// <summary>
        /// 取得PponChannelCategoryTree的副本，並依據設定包含了全部的Node
        /// </summary>
        /// <param name="inFrontEnd">只需要可顯示於前台的部分</param>
        /// <returns></returns>
        public static CategoryTypeNode GetPponChannelCategoryTreeWithAllNode(bool inFrontEnd)
        {
            //填入回傳直
            CategoryTypeNode typeNode = PponChannelCategoryTree;
            InsertAllNodesToCategoryNodes(typeNode, inFrontEnd);
            return typeNode;
        }

        private static void InsertAllNodesToCategoryNodes(CategoryTypeNode typeNode, bool inFrontEnd)
        {

            if (inFrontEnd)
            {
                typeNode.CategoryNodes = typeNode.CategoryNodes.Where(x => x.IsShowFrontEnd).ToList();
            }
            foreach (CategoryNode categoryNode in typeNode.CategoryNodes)
            {
                if (categoryNode.IsFinal)
                {
                    continue;
                }
                foreach (var subTypeNodes in categoryNode.NodeDatas)
                {
                    //確認是否需要加上全部選項
                    List<CategoryAllNodeSetting> settingList =
                    _allNodeSettings.Where(x => x.Id == categoryNode.CategoryId && x.NodeType == subTypeNodes.NodeType)
                                    .ToList();
                    if (settingList.Count > 0)
                    {
                        CategoryAllNodeSetting setting = settingList.First();
                        CategoryNode allNode = CategoryNode.GetDefaultCategory(setting.InsertNode.Name, setting.NodeType,
                                                                               setting.InsertNode.ShortName, categoryNode.CategoryId);
                        subTypeNodes.CategoryNodes.Insert(0, allNode);
                    }

                    InsertAllNodesToCategoryNodes(subTypeNodes, inFrontEnd);
                }
            }
        }

        public static void LoadCategoryVersion()
        {
            //查詢cityGroup資料設定的版本，供前端作業判斷資料是否已異動
            string categoryVersionData = _systemProvider.DBExtendedPropertiesGet(CategoryVersionExtendedPropertyName);
            //若查無資料或取出的值不為int，預設為0
            if (!int.TryParse(categoryVersionData, out _categoryVersion))
            {
                _categoryVersion = 0;
            }
        }

        public static void UpdateCategoryDbExtendedProperty()
        {
            LoadCategoryVersion();
            if (_categoryVersion == 0)
            {
                _systemProvider.DbExtendedPropertyAdd(CategoryVersionExtendedPropertyName, Convert.ToString(_categoryVersion + 1));
            }
            else
            {
                _systemProvider.DbExtendedPropertyUpdate(CategoryVersionExtendedPropertyName, Convert.ToString(_categoryVersion + 1));
            }
        }

        #region 與前台區域選單相關

        /// <summary>
        /// 產生分類樹
        /// </summary>
        public static List<CategoryTreeNode> RenderCategoryTree(HxRootCategory hxRoot)
        {
            List<CategoryTreeNode> tree = new List<CategoryTreeNode>();

            List<int> headCids = new List<int>()
            {
                CategoryManager.pponChannelId.Food,
                CategoryManager.pponChannelId.Delivery,
                CategoryManager.pponChannelId.Travel,
                CategoryManager.pponChannelId.Beauty,
                CategoryManager.pponChannelId.Skm
            };

            foreach (var cid in headCids)
            {
                int defaultDepth = 0;
                tree.Add(new CategoryTreeNode(cid, defaultDepth, null, hxRoot));
            }

            return tree;
        }

        /// <summary>
        /// 用categoryId取得暫存中的Category
        /// </summary>
        public static Category GetCategoryByCategoryId(int categoryId)
        {
            Category category = null;
            _categoriesDic.TryGetValue(categoryId, out category);
            return category;
        }

        /// <summary>
        /// 用categoryCode取得暫存中的categoryId
        /// </summary>
        public static int GetCategoryIdByCategoryCode(int categoryCode)
        {
            int categoryId = 0;
            Category category = null;
            category = _categories.FirstOrDefault(x => x.Code == categoryCode);
            if (category != null)
            {
                categoryId = category.Id;
            }
            return categoryId;
        }

        /// <summary>
        /// 用傳入的category_id當作parent_id，從暫存中的Category_dependency取得所有與子節點的對應關係
        /// </summary>
        /// <param name="parent_id"></param>
        /// <returns></returns>
        public static List<ViewCategoryDependency> GetCategoryDependencyInCacheByPid(int parentId)
        {
            List<ViewCategoryDependency> dependencies = new List<ViewCategoryDependency>();
            _categoryDependenciesDic.TryGetValue(parentId, out dependencies);
            return dependencies;
        }

        public static CategoryRegionDisplayRule GetRegionDisplayRuleByCid(int categoryId)
        {
            CategoryRegionDisplayRule displayRule;
            _categoryRegionDisplayRuleDic.TryGetValue(categoryId, out displayRule);
            return displayRule;
        }

        /// <summary>
        /// 查詢cityId對應的channelId
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static int GetChannelIdByCityId(int cityId)
        {
            int channelId = 0;

            #region 說明
            //city -----> channel
            //city <----- channel+category(region)
            //[city]
            //id    city_name   code                 |  channelId    *categoryId(HardCode以正式站為主)
            //1	    基隆市	    JL                   |               
            //58	南投縣	    NT                   |               
            //95	雲林縣	    YL                   |               
            //116	台南市	    TN  v                |    87          98 嘉南
            //155	高雄市	    GX  v                |    87          99 高屏
            //199	台北市	    TP  v                |    87          94 台北
            //212	澎湖縣	    PH                   |               
            //280	花蓮縣	    HL                   |               
            //324	宜蘭縣	    IL                   |               
            //338	新竹市	    XZ  v                |    87          96 竹苗
            //356	桃園市	    TY  v                |    87          95 桃園
            //389	台中市	    TC  v                |    87          97 中彰
            //  ※ v為有使用在分類選單上的city         |
            //  -------------------------------------|------------------------------
            //475	玩美女人	    PBU                  |    89          89
            //476	旅遊渡假	    TRA                  |    90          90
            //477	全國宅配	    ALL                  |    88          88
            //490	全家專區	    FAM                  |    91          91
            //492	72H到貨	    A72                  |    88          88
            //494	品生活	    PIN                  |    148         148
            //496	新光三越	    SKM                  |    185         185
            #endregion

            if (cityId == pponCityId.Taipei || cityId == pponCityId.Taoyuan || cityId == pponCityId.Hsinchu || 
                cityId == pponCityId.Taichung || cityId == pponCityId.Tainan || cityId == pponCityId.Kaohsiung)
            {
                channelId = pponChannelId.Food;
            }
            else if (cityId == pponCityId.Beauty)
            {
                channelId = pponChannelId.Beauty;
            }
            else if (cityId == pponCityId.Travel)
            {
                channelId = pponChannelId.Travel;
            }
            else if (cityId == pponCityId.Delivery || cityId == pponCityId.Hour72)
            {
                channelId = pponChannelId.Delivery;
            }
            else if (cityId == pponCityId.DepositCoffee)
            {
                channelId = pponChannelId.DepositCoffee;
            }
            else if (cityId == pponCityId.FamilyMart)
            {
                channelId = pponChannelId.FamilyMart;
            }
            else if (cityId == pponCityId.PiinLife)
            {
                channelId = pponChannelId.PiinLife;
            }
            else if (cityId == pponCityId.Skm)
            {
                channelId = pponChannelId.Skm;
            }

            return channelId;
        }

        /// <summary>
        /// 查詢cityId對應的channelId
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static int GetCityIdByChannelId(int channelId)
        {
            int cityId = 0;

            if (channelId == pponChannelId.Food)
            {
                cityId = pponCityId.Taipei;
            }
            else if (channelId == pponChannelId.Beauty)
            {
                cityId = pponCityId.Beauty;
            }
            else if (channelId == pponChannelId.Travel)
            {
                cityId = pponCityId.Travel;
            }
            else if (channelId == pponChannelId.Delivery)
            {
                cityId = pponCityId.Delivery;
            }
            else if (channelId == pponChannelId.FamilyMart)
            {
                cityId = pponCityId.FamilyMart;
            }
            else if (channelId == pponChannelId.PiinLife)
            {
                cityId = pponCityId.PiinLife;
            }
            else if (channelId == pponChannelId.Skm)
            {
                cityId = pponCityId.Skm;
            }
            else if (channelId == pponChannelId.Immediately)
            {
                cityId = 199;
            }

            return cityId;
        }


        /// <summary>
        /// 根據channelId與categoryId取得cityId
        /// </summary>
        /// <param name="channelId">頻道id</param>
        /// <param name="categoryId">分類id</param>
        /// <returns></returns>
        public static int GetCityIdByChannelIdAndCategoryId(int channelId, int categoryId)
        {
            //參照GetChannelIdByCityId(int cityId)的 #region 說明
            int cityId = 0;
            if (channelId == pponChannelId.Food)
            {
                if (categoryId == pponFoodRegionId.Taipei)
                {
                    cityId = pponCityId.Taipei;
                }
                else if (categoryId == pponFoodRegionId.Taoyuan)
                {
                    cityId = pponCityId.Taoyuan;
                }
                else if (categoryId == pponFoodRegionId.Hsinchu)
                {
                    cityId = pponCityId.Hsinchu;
                }
                else if (categoryId == pponFoodRegionId.Taichung)
                {
                    cityId = pponCityId.Taichung;
                }
                else if (categoryId == pponFoodRegionId.Tainan)
                {
                    cityId = pponCityId.Tainan;
                }
                else if (categoryId == pponFoodRegionId.Kaohsiung)
                {
                    cityId = pponCityId.Kaohsiung;
                }
            }
            else
            {
                if (channelId == pponChannelId.Beauty)
                {
                    cityId = pponCityId.Beauty;
                }
                else if (channelId == pponChannelId.Travel)
                {
                    cityId = pponCityId.Travel;
                }
                else if (channelId == pponChannelId.Delivery)
                {
                    cityId = pponCityId.Delivery;
                }
                else if (channelId == pponChannelId.DepositCoffee)
                {
                    cityId = pponCityId.DepositCoffee;
                }
                else if (channelId == pponChannelId.FamilyMart)
                {
                    cityId = pponCityId.FamilyMart;
                }
                else if (channelId == pponChannelId.PiinLife)
                {
                    cityId = pponCityId.PiinLife;
                }
                else if (channelId == pponChannelId.Skm)
                {
                    cityId = pponCityId.Skm;
                }
            }

            return cityId;
        }

        /// <summary>
        /// 取得各頻道預設的地區Id
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public static int GetDefaultRegionIdByChannelId(int channelId)
        {
            int regionId = 0;
            if (CategoryManager._channelCategoryTree.Count() > 0)
            {
                CategoryTreeNode channelNode = CategoryManager._channelCategoryTree.Where(x => x.CategoryId == channelId).FirstOrDefault();
                if (channelNode != null)
                {
                    CategoryTreeNode regionNode = channelNode.Children.FirstOrDefault();
                    if (regionNode != null)
                    {
                        //區域如果不用顯示加總項目 才要取regionId，否則預設為0
                        if (!Helper.IsFlagSet(channelNode.SpecialRuleFlag, CategoryDisplayRuleFlag.DisplaySummaryItem))
                        {
                            regionId = regionNode.CategoryId;
                        }
                    }
                }
            }
            return regionId;
        }

        #endregion

        

        #endregion public

        #region private
        /// <summary>
        /// 傳入某一CategoryType，以此類型為條件，查詢此類型的category，與其下個子項目的資料。
        /// </summary>
        /// <param name="type">要查詢的CategoryType</param>
        /// <param name="parentId">資料的父類型ID，傳入null則查詢type的全部category</param>
        /// <returns></returns>
        private static CategoryTypeNode CategoryTypeNodeGetByType(CategoryType type, int? parentId)
        {
            CategoryTypeNode typeNode = new CategoryTypeNode(type);
            if (parentId.HasValue)
            {
                //有指定父類型參數，依據父類型的值查詢子項目
                List<ViewCategoryDependency> subCategoryDependencies =
                    _categoryDependencies.Where(
                        x =>
                        x.ParentId == parentId.Value && x.CategoryType.HasValue && (CategoryType)x.CategoryType == type)
                                         .OrderBy(x => x.Seq)
                                         .ToList();
                foreach (var viewCategoryDependency in subCategoryDependencies)
                {
                    List<Category> subCategories = _categories.Where(x => x.Id == viewCategoryDependency.CategoryId).ToList();
                    if (subCategories.Count > 0)
                    {
                        CategoryNode node = CategoryNodeGet(subCategories.First());
                        if (node != null)
                        {
                            typeNode.CategoryNodes.Add(node);
                        }
                    }
                }
            }
            else
            {
                //沒有指定父類型參數，查詢所有此type的category資料
                List<Category> typeCategories =
                    _categories.Where(x => x.Type.HasValue && (CategoryType)x.Type == type).OrderBy(x => x.Rank).ToList();

                foreach (var typeCategory in typeCategories)
                {
                    CategoryNode node = CategoryNodeGet(typeCategory);
                    if (node != null)
                    {
                        typeNode.CategoryNodes.Add(node);
                    }
                }
            }
            return typeNode;
        }

        /// <summary>
        /// 傳入category物件，以此物件建立CategoryNode物件，並包含其下個子類別的內容。
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        private static CategoryNode CategoryNodeGet(Category category)
        {
            //傳入參數有錯誤，回傳空物件
            if (category == null || !category.IsLoaded)
            {
                return null;
            }

            CategoryNode node = new CategoryNode(category);
            //最底層，不再往下查，直接回傳已建立物件
            if (category.IsFinal)
            {
                return node;
            }
            //不是最底層，看看他有甚麼子項目
            List<ViewCategoryDependency> dependencies = _categoryDependencies.Where(x => x.ParentId == category.Id).ToList();
            foreach (var dependency in dependencies.GroupBy(x => x.CategoryType))
            {
                if (dependency.Key != null)
                {
                    //已據此類型產生下一層資料
                    CategoryTypeNode typeNode = CategoryTypeNodeGetByType((CategoryType)dependency.Key, category.Id);
                    if (typeNode != null)
                    {
                        node.NodeDatas.Add(typeNode);
                    }
                }
            }
            return node;
        }

        /// <summary>
        /// 讀取分類各區塊全部項目的設定。
        /// </summary>
        private static void LoadCategoryAllNodeSetting()
        {
            string dataName = "CategoryAllNodeSetting";
            SystemData data = _systemProvider.SystemDataGet(dataName);
            if (!data.IsLoaded)
            {
                _log.Error("CategoryManager.LoadCategoryAllNodeSetting 未設定CategoryAllNodeSetting的SystemData資料");
                return;
            }
            try
            {
                _allNodeSettings = new JsonSerializer().Deserialize<List<CategoryAllNodeSetting>>(data.Data);
            }
            catch (Exception ex)
            {
                _allNodeSettings = new List<CategoryAllNodeSetting>();
                _log.Error(string.Format("CategoryManager.LoadCategoryAllNodeSetting {0}的SystemData資料", dataName), ex);
            }
        }
        #endregion private
    }


    #region 分類的結構
    /// <summary>
    /// 每個分類類別的資料內容物件
    /// </summary>
    public class CategoryTypeNode
    {
        public CategoryTypeNode(CategoryType nodeType)
        {
            _nodeType = nodeType;
            CategoryNodes = new List<CategoryNode>();
        }

        private readonly CategoryType _nodeType;
        /// <summary>
        /// 分類類別
        /// </summary>
        public CategoryType NodeType
        {
            get { return _nodeType; }
        }
        /// <summary>
        /// 此分類類別之中擁有的分類物件與物件的子類別項目資料
        /// </summary>
        public List<CategoryNode> CategoryNodes { get; set; }

        [JsonIgnore]
        public CategoryNode Parent { get; set; }

        public CategoryTypeNode Copy()
        {
            CategoryTypeNode rtn = new CategoryTypeNode(_nodeType);
            rtn.Parent = this.Parent;
            foreach (var categoryNode in CategoryNodes)
            {
                rtn.CategoryNodes.Add(categoryNode.Copy());
            }
            return rtn;
        }

        /// <summary>
        /// 傳入某個category，檢查此CategoryTypeNode，回傳找到此Id資料經過的每個CategoryId的陣列
        /// 如有多個符合條件的資料，只會回傳第一個符合的
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public List<int> GetCategoryPath(int categoryId)
        {
            foreach (var categoryNode in CategoryNodes)
            {
                List<int> paths = categoryNode.GetCategoryPath(categoryId);
                if (paths.Count > 0)
                {
                    return paths;
                }
            }
            return new List<int>();
        }
    }

    /// <summary>
    /// Category物件及其下的子分類之相關物件
    /// </summary>
    public class CategoryNode
    {
        private CategoryNode(string name, CategoryType type, string shortName, int id)
        {
            _mainCategory = new Category();
            _mainCategory.Id = id;
            _mainCategory.Name = name;
            _mainCategory.Type = (int)type;
            _mainCategory.NameInConsole = name;
            _mainCategory.IsFinal = false;
            _mainCategory.IsShowFrontEnd = true;
            _shortName = shortName;
            //排序序號預設為-1卻表依據序號排序時預設的CategoryNode會排在第一位
            _seq = -1;

            NodeDatas = new List<CategoryTypeNode>();
        }

        public CategoryNode(Category mainCategory)
        {
            _mainCategory = mainCategory;
            NodeDatas = new List<CategoryTypeNode>();
        }

        #region 參數
        private readonly Category _mainCategory;
        /// <summary>
        /// 此節點代表的Category之ID
        /// </summary>
        [JsonProperty("Id")]
        public int CategoryId
        {
            get { return _mainCategory.Id; }
        }
        /// <summary>
        /// 此節點代表的Category之名稱
        /// </summary>
        [JsonProperty("Name")]
        public string CategoryName
        {
            get { return _mainCategory.Name; }
            set { _mainCategory.Name = value; }
        }
        /// <summary>
        /// 此節點代表的Category之類型
        /// </summary>
        [JsonProperty("Type")]
        public CategoryType CategoryType
        {
            get
            {
                if (_mainCategory.Type == null)
                {
                    return CategoryType.None;
                }
                return (CategoryType)_mainCategory.Type;
            }
        }
        [JsonIgnore]
        public CategoryTypeNode Parent { get; set; }
        /// <summary>
        /// 此節點代表的Category之後臺顯示名稱
        /// </summary>
        public string NameInConsole
        {
            get { return _mainCategory.NameInConsole; }
        }
        /// <summary>
        /// 此節點代表的Category為leaf (最後一個)
        /// </summary>
        public bool IsFinal
        {
            get { return _mainCategory.IsFinal; }
        }
        /// <summary>
        /// 此節點代表的Category是否顯示於前台
        /// </summary>
        public bool IsShowFrontEnd
        {
            get { return _mainCategory.IsShowFrontEnd; }
        }
        /// <summary>
        /// 此節點代表的Category是否顯示於後台
        /// </summary>
        public bool IsShowBackEnd
        {
            get { return _mainCategory.IsShowBackEnd; }
        }

        private string _shortName = string.Empty;
        /// <summary>
        /// short欄位，提供短的 Category名稱，供顯示區塊空間不夠時使用
        /// </summary>
        public string ShortName
        {
            get
            {
                return string.IsNullOrWhiteSpace(_shortName) ? _mainCategory.Name : _shortName;
            }
        }

        private int? _seq;
        /// <summary>
        /// 此節點代表的Category之後設定的序號，若程式端為特別指定，以Category之Rank欄位為預設值
        /// </summary>
        public int Seq
        {
            get
            {
                if (_seq.HasValue)
                {
                    return _seq.Value;
                }
                if (_mainCategory.Rank.HasValue)
                {

                    return _mainCategory.Rank.Value;
                }
                return int.MaxValue;
            }
            set { _seq = value; }
        }
        /// <summary>
        /// 此分類項目下之子分類
        /// </summary>
        public List<CategoryTypeNode> NodeDatas { get; set; }

        [JsonIgnore]
        public int? CityId { get; set; }

        #endregion 參數

        /// <summary>
        /// 取得此節點代表之Category的物件，為建構元傳入之Category的Clone。
        /// </summary>
        /// <returns></returns>
        public Category GetMainCategory()
        {
            return _mainCategory.Clone();
        }

        public CategoryNode Copy()
        {
            CategoryNode rtn = new CategoryNode(_mainCategory.Clone());
            rtn.CityId = CityId;
            rtn.Parent = Parent;
            foreach (var categoryTypeNode in NodeDatas)
            {
                rtn.NodeDatas.Add(categoryTypeNode.Copy());
            }
            return rtn;
        }

        /// <summary>
        /// 由子項目中取出某個CategoryType的CategoryNode List，若無此type或此type無資料，回傳空的CategoryNode List
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<CategoryNode> GetSubCategoryTypeNode(CategoryType type)
        {
            List<CategoryTypeNode> subTypeNodes = NodeDatas.Where(x => x.NodeType == type).ToList();
            if (subTypeNodes.Count > 0)
            {
                CategoryTypeNode subNode = subTypeNodes.First();
                return subNode.CategoryNodes.Select(categoryNode => categoryNode.Copy()).ToList();
            }
            return new List<CategoryNode>();
        }

        public void SetSubCategoryTypeNodeCity(CategoryType type)
        {
            List<CategoryTypeNode> subTypeNodes = NodeDatas.Where(x => x.NodeType == type).ToList();
            if (subTypeNodes.Count > 0)
            {
                CategoryTypeNode subNode = subTypeNodes.First();
                var n = subNode.CategoryNodes.Select(categoryNode => categoryNode).ToList();
                foreach (var item in n)
                {
                    item.CityId = PponCityGroup.GetPponCityByCategoryId(item.CategoryId).CityId;
                }
            }
        }

        /// <summary>
        /// 根據子分類查找CategoryNode
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public CategoryNode GetSubCategoryNodeByCategoryId(int categoryId)
        {
            List<CategoryNode> subNodes = NodeDatas.SelectMany(x => x.CategoryNodes).ToList();
            return subNodes.Where(x => x.CategoryId == categoryId).FirstOrDefault();
        }

        public List<CategoryNode> GetSubCategoryByCategoryId(int categoryId)
        {
            List<CategoryNode> subNodes = NodeDatas.SelectMany(x => x.CategoryNodes).ToList();
            return subNodes;
        }

        public static CategoryNode GetDefaultCategory(string name = "全部", CategoryType type = CategoryType.None, string shortName = "全部", int id = 0)
        {
            return new CategoryNode(name, type, shortName, id);
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", this.CategoryId, this.CategoryName);
        }
        /// <summary>
        /// 傳入某個category，檢查此CategoryNode，回傳找到此Id資料經過的每個CategoryId的陣列
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public List<int> GetCategoryPath(int categoryId)
        {
            if (categoryId == CategoryId)
            {
                List<int> rtnList = new List<int>();
                rtnList.Add(categoryId);
                return rtnList;
            }
            else
            {
                foreach (var categoryTypeNode in NodeDatas)
                {
                    List<int> subData = categoryTypeNode.GetCategoryPath(categoryId);
                    if (subData.Count > 0)
                    {
                        //子項目有符合條件的資料，將本身的CategoryId，插入回傳陣列的第一筆，因為依據路徑來看，越上層的要越上面
                        subData.Insert(0, CategoryId);
                        return subData;
                    }
                }
            }
            return new List<int>();
        }
    }


    public class CategoryAllNodeSetting
    {

        public int Id { get; set; }
        public CategoryType NodeType { get; set; }
        public string Name { get; set; }
        public CateogryNodeSetting InsertNode { get; set; }
    }

    public class CateogryNodeSetting
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
    }


    #endregion 分類的結構
}

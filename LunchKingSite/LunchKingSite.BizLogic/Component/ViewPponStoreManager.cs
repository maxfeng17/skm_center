﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class ViewPponStoreManager
    {
        #region const
        public const string ManagerNameForJob = "ViewPponStoreManager";
        #endregion const

        #region static

        private static readonly ILog logger = LogManager.GetLogger(typeof(ViewPponStoreManager));
        private static ViewPponStoreManager _manager;
        public static ViewPponStoreManager DefaultManager
        {
            get { return _manager ?? (_manager = new ViewPponStoreManager()); }
        }


        /// <summary>
        /// 產生新的DefaultPponDealManager資料將會同步更新
        /// </summary>
        public static void ReloadDefaultManager()
        {
            DateTime now = DateTime.Now;
            var newOne = new ViewPponStoreManager();
            newOne.LoadTodayBaseDealStore();
            _manager = newOne;
            logger.WarnFormat("ViewPponStoreManager: 費時 {0} 秒", (DateTime.Now - now).TotalSeconds.ToString("#.##"));
        }
        /// <summary>
        /// 將DefaultManager清空
        /// </summary>
        public static void ClearDefaultManager()
        {
            var newOne = new ViewPponStoreManager();
            _manager = newOne;
        }
        #endregion static

        protected IPponProvider PponProv;
        private ViewPponStoreCollection _collection;
        private DateTime dealTimeSlotStart;
        private DateTime dealTimeSlotEnd;

        private Dictionary<Guid, List<ViewPponStore>> _indexedDealStores;

        private ViewPponStoreManager()
        {
            PponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _collection = new ViewPponStoreCollection();
            DateTime baseDate = DateTime.ParseExact(DateTime.Today.ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
            dealTimeSlotStart = baseDate.AddDays(-1);
            dealTimeSlotEnd = baseDate.AddDays(1);
            _indexedDealStores = new Dictionary<Guid, List<ViewPponStore>>();
        }

        /// <summary>
        /// 將系統日當天與前一天的好康資料寫入暫存的collection中
        /// </summary>
        protected void LoadTodayBaseDealStore()
        {
            var vpsc = PponProv.ViewPponStoreGetListByDayWithNoLock(dealTimeSlotStart, dealTimeSlotEnd);
            _collection = vpsc;

            InitIndex(vpsc);
        }

        private void InitIndex(ViewPponStoreCollection vpsCol)
        {
            List<Guid> bids = PponProv.ViewPponDealGetBidListByDay(dealTimeSlotStart, dealTimeSlotEnd);
            foreach (Guid bid in bids)
            {
                if (_indexedDealStores.ContainsKey(bid) == false)
                {
                    _indexedDealStores.Add(bid, new List<ViewPponStore>());
                }
            }
            foreach (ViewPponStore vps in vpsCol)
            {
                if (_indexedDealStores.ContainsKey(vps.BusinessHourGuid) == false)
                {
                    _indexedDealStores.Add(vps.BusinessHourGuid, new List<ViewPponStore>());
                }
                _indexedDealStores[vps.BusinessHourGuid].Add(vps);
            }
        }

        /// <summary>
        /// 改讀靜態物件判斷是否為通用卷
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        private bool IsRestrictedStoreDeal(IViewPponDeal deal)
        {
            if (!deal.IsLoaded)
            {
                return false;
            }

            return !Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
        }

        #region public method

        public void ClearViewPponStoreByStore(Guid businessHourGuid, Guid storeGuid)
        {
            var rtns = _collection.Where(x => x.BusinessHourGuid == businessHourGuid && x.StoreGuid == storeGuid).ToList();
            if (rtns.Any())
            {
                foreach (ViewPponStore viewstorestore in rtns)
                {
                    _collection.Remove(viewstorestore);
                }
            }
            ViewPponStore viewPponStore = PponProv.ViewPponStoreGetByBidAndStoreGuid(businessHourGuid, storeGuid);
            if (viewPponStore.IsLoaded)
            {
                _collection.Add(viewPponStore);
            }
        }

        public ViewPponStoreCollection ViewPponStoreGetListByStoreGuid(Guid storeGuid)
        {
            var stores = _collection.Where(x => x.StoreGuid == storeGuid).OrderBy(x => x.CityId).ToList();
            if (stores.Count > 0)
            {
                var rtn = new ViewPponStoreCollection();
                rtn.AddRange(stores);
                return rtn;
            }
            return PponProv.ViewPponStoreGetListWithNoLock(ViewPponStore.Columns.StoreGuid + " = " + storeGuid);
        }

        public ViewPponStore ViewPponStoreGet(Guid businessHourGuid, Guid storeGuid)
        {
            var rtns = _collection.Where(x => x.BusinessHourGuid == businessHourGuid && x.StoreGuid == storeGuid).ToList();
            return rtns.Count > 0 ? rtns.First() : PponProv.ViewPponStoreGetByBidAndStoreGuid(businessHourGuid, storeGuid);
        }

        public ViewPponStoreCollection ViewPponStoreGetList(Guid businessHourGuid)
        {
            List<ViewPponStore> stores;
            if (_indexedDealStores.TryGetValue(businessHourGuid, out stores) == false)
            {
                return PponProv.ViewPponStoreGetListByBidWithNoLock(businessHourGuid);
            }
            var rtn = new ViewPponStoreCollection();
            if (stores != null && stores.Count > 0)
            {
                rtn.AddRange(stores);
            }
            return rtn;
        }

        public ViewPponStoreCollection ViewPponStoreGetList(Guid businessHourGuid, VbsRightFlag vbsRight)
        {
            List<ViewPponStore> stores;
            if (_indexedDealStores.TryGetValue(businessHourGuid, out stores) == false)
            {
                return PponProv.ViewPponStoreGetListByBidWithNoLock(businessHourGuid, vbsRight);
            }
            var rtn = new ViewPponStoreCollection();
            if (stores != null && stores.Count > 0)
            {
                if (vbsRight == VbsRightFlag.VerifyShop)
                {
                    rtn.AddRange(stores.Where(x => Helper.IsFlagSet(x.VbsRight, vbsRight) &&
                                                   !Helper.IsFlagSet(x.VbsRight, VbsRightFlag.HideFromVerifyShop)));
                }
                else
                {
                    rtn.AddRange(stores.Where(x => Helper.IsFlagSet(x.VbsRight, vbsRight)));
                }
            }
            return rtn;
        }

        #endregion public method


        /// <summary>
        /// 取得多分店時，簡短版的分店地址
        /// 譬如 分店1:台北市中山區xx路... 分店2:台北市中正區xx路...
        /// 會回傳中山區、中正區 
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        /// 
        public string GetPponStoresShortDesc(Guid bid)
        {
            HashSet<string> citytowns = new HashSet<string>();

            ViewPponStoreCollection vpsCol = ViewPponStoreGetList(bid, VbsRightFlag.VerifyShop);
            if (vpsCol.Count <= 1)
            {
                return string.Empty;
            }
            int cityCount = vpsCol.Select(t => t.CityName).Distinct().Count();
            foreach (ViewPponStore vps in vpsCol)
            {
                string text;
                if (cityCount > 1)
                {
                    text = vps.CityName + vps.TownshipName;
                }
                else
                {
                    text = vps.TownshipName;
                }
                if (citytowns.Contains(text) == false)
                {
                    citytowns.Add(text);
                }
            }
            string result = string.Join("、", citytowns.ToArray());
            if (result.Length > 36)
            {
                result = result.TrimToMaxLength(36, "...");
            }
            return result;
        }

        public string GetPponStoresShortDesc(Guid bid, out bool is_multiplestores)
        {
            HashSet<string> citytowns = new HashSet<string>();

            ViewPponStoreCollection vpsCol = ViewPponStoreGetList(bid, VbsRightFlag.VerifyShop);
            if (vpsCol.Count == 1)
            {
                is_multiplestores = false;
                return vpsCol[0].CityName + vpsCol[0].TownshipName;
            }
            else if (vpsCol.Count == 0)
            {
                is_multiplestores = false;
                return string.Empty;
            }
            int cityCount = vpsCol.Select(t => t.CityName).Distinct().Count();
            is_multiplestores = vpsCol.Count > 1;
            foreach (ViewPponStore vps in vpsCol)
            {
                string text;
                if (cityCount > 1)
                {
                    text = vps.CityName + vps.TownshipName;
                }
                else
                {
                    text = vps.TownshipName;
                }
                if (citytowns.Contains(text) == false)
                {
                    citytowns.Add(text);
                }
            }
            string result = string.Join("、", citytowns.ToArray());
            if (result.Length > 36)
            {
                result = result.TrimToMaxLength(36, "...");
            }
            return result;
        }

        /// <summary>
        /// 取得檔次的地區名稱，當有多個分店時，顯示"多分店"
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public string GetPponStoreTownshipName(Guid bid)
        {
            var vpsCol = ViewPponStoreGetList(bid, VbsRightFlag.VerifyShop);
            
            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);

            var isRestrictedStore = IsRestrictedStoreDeal(vpd);


            if (vpsCol.Count == 0 && isRestrictedStore)
            {
                return string.Empty;
            }
            if (vpsCol.Count > 1 || !isRestrictedStore)
            {
                return "多分店";
            }
            return vpsCol[0].TownshipName;
        }
        public string GetPponStoreTownshipName(Guid bid, out bool ismultiple)
        {
            ismultiple = false;
            var vpsCol = ViewPponStoreGetList(bid, VbsRightFlag.VerifyShop);
            if (vpsCol.Count == 1)
            {
                ismultiple = true;
                return vpsCol[0].TownshipName;
            }
            else if (vpsCol.Count > 1)
            {
                ismultiple = true;
                return string.Join("、", vpsCol.Select(x => x.TownshipName).Distinct());
            }
            return string.Empty;
        }
        public string GetPponStoreCityTownshipName(Guid bid)
        {
            var vpsCol = ViewPponStoreGetList(bid, VbsRightFlag.VerifyShop);
            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            var isRestrictedStore = IsRestrictedStoreDeal(vpd);
            if (vpsCol.Count == 0 && isRestrictedStore)
            {
                return string.Empty;
            }
            if (vpsCol.Count > 1 || !isRestrictedStore)
            {
                return "多分店";
            }
            return vpsCol[0].CityShortName + vpsCol[0].TownshipName;
        }

    }
}

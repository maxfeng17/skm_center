﻿using log4net;
using LunchKingSite.BizLogic.Models.PchomeWarehouse;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.Wms;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;
using System.IO;

namespace LunchKingSite.BizLogic.Component
{
    public class PchomeWmsAPI
    {

        static PchomeWmsAPI()
        {
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback =
                delegate { return true; };

            logger.InfoFormat("VendorId: {0}", VendorId);
            logger.InfoFormat("ApiBaseUrl: {0}", ApiBaseUrl);
            logger.InfoFormat("IsDevelopment: {0}", IsDevelopment);
        }

        static ILog logger = LogManager.GetLogger(typeof(PchomeWmsAPI));
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public static bool IsDevelopment
        {
            get
            {
                return config.EnableWms == false;
            }
        }

        public static string ApiBaseUrl
        {
            get
            {
                return "https://pfcapi.pchome.com.tw/index.php";
            }
        }

        public static string VendorId
        {
            get
            {
                return config.WmsVendorId;
            }
        }

        public static bool AddProd(WmsProd prod)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod", ApiBaseUrl, "wms_fake", VendorId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod", ApiBaseUrl, "wms", VendorId);
                    }

                    var postData = JObject.FromObject(prod).ToString();

                    HttpResponseMessage response = client.PostAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var loc = response.Headers.Location;
                        logger.InfoFormat("AddProd: {0} {1}, prod={2}", response.StatusCode, response.Headers.Location, JsonConvert.SerializeObject(prod));
                        prod.Id = loc.OriginalString.Split("/").LastOrDefault();
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("AddProd: {0} {1}, prod={2}", response.StatusCode, content, JsonConvert.SerializeObject(prod));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("新增Prod錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(prod), ex.ToString());
            }
            return false;
        }
        /// <summary>
        /// 取得商品資料 (1.2.4)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static WmsProd GetProd(string id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod/{3}", ApiBaseUrl, "wms_fake", VendorId, id);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod/{3}", ApiBaseUrl, "wms", VendorId, id);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("GetProd: {0} {1}", response.StatusCode, content);
                        if (content.Contains("\\\""))
                        {
                            //P1905000280商品多雙引號導致解析錯誤
                            content = content.Replace("\\\"", "");
                        }
                        logger.InfoFormat("GetProd: {0} {1}", response.StatusCode, content);
                        content = Regex.Unescape(content);
                        logger.InfoFormat("GetProd: {0} {1}", response.StatusCode, content);
                        WmsProd prod = JsonConvert.DeserializeObject<WmsProd>(content);
                        return prod;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("GetProd: {0} {1}", response.StatusCode, content);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得Prod錯誤, id={0}, ex={1}", id, ex.ToString());
            }
            return null;
        }

        /// <summary>
        /// 取得商品資料，依群組id (1.2.5)
        /// </summary>
        /// <param name="groupIds"></param>
        /// <returns></returns>
        public static List<WmsProd> GetProdsByGroupIds(params string[] groupIds)
        {
            try
            {
                string matchGroupId = string.Join(",", groupIds);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod?groupid={3}", ApiBaseUrl, "wms_fake", VendorId, matchGroupId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod?groupid={3}", ApiBaseUrl, "wms", VendorId, matchGroupId);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        List<WmsProd> prods = JsonConvert.DeserializeObject<List<WmsProd>>(content);
                        return prods ?? new List<WmsProd>();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得Prod錯誤, groupIds={0}, ex={1}", string.Join(",", groupIds), ex.ToString());
            }
            return new List<WmsProd>();
        }
        /// <summary>
        /// 1.2.6
        /// </summary>
        /// <returns>回傳id</returns>
        public static bool AddSpec(WmsProdSpec spec)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod/spec?groupid={3}", ApiBaseUrl, "wms_fake", VendorId, spec.GroupId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod/spec?groupid={3}", ApiBaseUrl, "wms", VendorId, spec.GroupId);
                    }

                    var postData = JObject.FromObject(spec).ToString();

                    HttpResponseMessage response = client.PostAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var loc = response.Headers.Location;
                        spec.Id = loc.OriginalString.Split("/").LastOrDefault();
                        return true;
                    }
                    else
                    {
                        var statusCode = response.StatusCode;
                        string content = response.Content.ReadAsStringAsync().Result;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("AddSpec錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(spec), ex.ToString());
            }
            return false;
        }
        /// <summary>
        /// 取得商品資料，依venderpids廠商料號來查詢 (1.2.7)
        /// </summary>
        /// <param name="verderpids"></param>
        /// <returns></returns>
        public static List<WmsProd> GetProdsByVendorPIds(params string[] verderPids)
        {
            try
            {
                string matchVerderPids = string.Join(",", verderPids);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod?vendorpid={3}", ApiBaseUrl, "wms_fake", VendorId, matchVerderPids);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod?vendorpid={3}", ApiBaseUrl, "wms", VendorId, matchVerderPids);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        List<WmsProd> prods = JsonConvert.DeserializeObject<List<WmsProd>>(content);
                        return prods ?? new List<WmsProd>();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得Prod錯誤, verderPids={0}, ex={1}", string.Join(",", verderPids), ex.ToString());
            }
            return new List<WmsProd>();
        }
        /// <summary>
        /// 取得商品的庫存狀況 (1.3.3)
        /// </summary>
        /// <param name="prodIds"></param>
        /// <returns></returns>
        public static List<WmsInventory> GetInventories(params string[] prodIds)
        {
            string matchIdsStr = string.Join(",", prodIds);
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod/inventory?prodid={3}",
                            ApiBaseUrl, "wms_fake", VendorId, matchIdsStr);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v1/vendor/{2}/prod/inventory?prodid={3}",
                            ApiBaseUrl, "wms", VendorId, matchIdsStr);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {

                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("GetInventories: {0} {1}", response.StatusCode, content);
                        if (content.Contains("\\\""))
                        {
                            //P1905000280商品多雙引號導致解析錯誤
                            content = content.Replace("\\\"", "");
                        }
                        logger.InfoFormat("GetInventories: {0} {1}", response.StatusCode, content);
                        content = Regex.Unescape(content);
                        logger.InfoFormat("GetInventories: {0} {1}", response.StatusCode, content);
                        List<WmsInventory> inventories = JsonConvert.DeserializeObject<List<WmsInventory>>(content);
                        return inventories ?? new List<WmsInventory>();
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("GetInventories: {0} {1}", response.StatusCode, content);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得Inventories錯誤, verderPids={0}, ex={1}", string.Join(",", matchIdsStr), ex.ToString());
            }
            return new List<WmsInventory>();
        }
        /// <summary>
        /// 新增進倉單 (2.2.4)
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static bool AddPurchaseOrder(WmsPurchaseOrder order)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;

                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/purchase/v1/vendor/{2}/purchaseorder", ApiBaseUrl, "wms_fake", VendorId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/purchase/v1/vendor/{2}/purchaseorder", ApiBaseUrl, "wms", VendorId);
                    }

                    var postData = JObject.FromObject(order).ToString();

                    HttpResponseMessage response = client.PostAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var loc = response.Headers.Location;
                        logger.InfoFormat("AddPurchaseOrder: {0} {1} {2}, purchaseOrder={3}", response.IsSuccessStatusCode.ToString(), response.StatusCode, loc, JsonConvert.SerializeObject(order));
                        order.Id = loc.OriginalString.Split("/").LastOrDefault();
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("AddPurchaseOrder: {0} {1} {2}, purchaseOrder={3}", response.IsSuccessStatusCode.ToString(), response.StatusCode, content, JsonConvert.SerializeObject(order));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("新增PurchaseOrder錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(order), ex.ToString());
            }
            return false;
        }
        /// <summary>
        /// 查詢進倉單資訊:用進倉單編號 (2.2.5)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static WmsPurchaseOrder GetPurchaseOrder(string id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;

                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/purchase/v1/vendor/{2}/purchaseorder/{3}", ApiBaseUrl, "wms_fake", VendorId, id);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/purchase/v1/vendor/{2}/purchaseorder/{3}", ApiBaseUrl, "wms", VendorId, id);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        logger.InfoFormat("GetPurchaseOrder: {0} {1}", response.StatusCode, content);
                        WmsPurchaseOrder prod = JsonConvert.DeserializeObject<WmsPurchaseOrder>(content);
                        return prod;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("GetPurchaseOrder: {0} {1}", response.StatusCode, content);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得PurchaseOrder錯誤, id={0}, ex={1}", id, ex.ToString());
            }
            return null;
        }
        /// <summary>
        /// 取得 PChome倉儲進倉單麥頭連結網址 (2.2.6)
        /// </summary>
        /// <param name="purchaseOrderIds"></param>
        /// <returns></returns>
        public static string GetPurchaseOrderMarkWithReceiptUrl(string[] purchaseOrderIds)
        {
            string matchIdsStr = string.Join(",", purchaseOrderIds);
            string url;
            if (IsDevelopment)
            {
                url = string.Format("{0}/{1}/purchase/v1/vendor/{2}/purchaseorder/markwithreceipt/report.htm?id={3}",
                    ApiBaseUrl, "wms_fake", VendorId, matchIdsStr);
            }
            else
            {
                url = string.Format("{0}/{1}/purchase/v1/vendor/{2}/purchaseorder/markwithreceipt/report.htm?id={3}",
                    ApiBaseUrl, "wms", VendorId, matchIdsStr);
            }
            logger.InfoFormat("GetPurchaseOrderMarkWithReceiptUrl: {0}", url);
            return url;
        }
        /// <summary>
        /// 利用進倉單編號,查詢進倉單進度資料 (2.3.2)
        /// </summary>
        /// <param name="purchaseOrderId"></param>
        /// <returns></returns>
        public static List<PurchaseOrderProgressStatus> GetPurchaseOrderProgressStatuses(string purchaseOrderId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/purchase/v1/vendor/{2}/purchaseorder/{3}/progressstatus",
                            ApiBaseUrl, "wms_fake", VendorId, purchaseOrderId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/purchase/v1/vendor/{2}/purchaseorder/{3}/progressstatus",
                            ApiBaseUrl, "wms", VendorId, purchaseOrderId);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        List<PurchaseOrderProgressStatus> items = JsonConvert.DeserializeObject<List<PurchaseOrderProgressStatus>>(content);
                        return items ?? new List<PurchaseOrderProgressStatus>();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得PurchaseOrder錯誤, purchaseOrderId={0}, ex={1}", string.Join(",", purchaseOrderId), ex.ToString());
            }
            return new List<PurchaseOrderProgressStatus>();
        }
        /// <summary>
        /// 新增訂單 (3.2.4)
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static bool AddOrder(WmsOrder order, out string content)
        {
            order.ReceiveContact = WmsHelper.Encrypt(order.ReceiveContact);
            content = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order", ApiBaseUrl, "wms_fake", VendorId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order", ApiBaseUrl, "wms", VendorId);
                    }

                    var postData = JObject.FromObject(order).ToString();

                    HttpResponseMessage response = client.PostAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var loc = response.Headers.Location;
                        logger.InfoFormat("AddOrder: {0} {1}", response.StatusCode, loc);                        
                        order.Id = loc.OriginalString.Split("/").LastOrDefault();
                        CommonFacade.AddAudit(Guid.Parse(order.VendorOrderId), AuditType.WmsOrder, 
                            JObject.FromObject(order).ToString(), "sys", false);
                        return true;
                    }
                    else
                    {
                        content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("AddOrder: {0} {1}, order={2}", response.StatusCode, content, JsonConvert.SerializeObject(order));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("AddOrder錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(order), ex.ToString());
            }
            return false;
        }
        /// <summary>
        /// 查詢訂單 (3.2.5)
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public static WmsOrder GetOrder(string orderId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order/{3}", ApiBaseUrl, "wms_fake", VendorId, orderId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order/{3}", ApiBaseUrl, "wms", VendorId, orderId);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        WmsOrder order = JsonConvert.DeserializeObject<WmsOrder>(content);
                        try
                        {
                            order.ReceiveContact = WmsHelper.Decrypt(order.ReceiveContact);
                        }
                        catch (Exception ex)
                        {
                            logger.WarnFormat("解密失敗, order.ReceiveContact={0}", order.ReceiveContact);
                        }

                        return order;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得Order錯誤, id={0}, ex={1}", orderId, ex.ToString());
            }
            return null;
        }
        /// <summary>
        /// 查詢訂單狀態，依多筆貨主訂單編號 (3.2.6)
        /// </summary>
        /// <param name="vendorOrderIds"></param>
        /// <returns></returns>
        public static List<WmsOrder> GetOrdersByVendorOrderId(params string[] vendorOrderIds)
        {
            string matchIdsStr = string.Join(",", vendorOrderIds);
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order?vendororderid={3}",
                            ApiBaseUrl, "wms_fake", VendorId, matchIdsStr);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order?vendororderid={3}",
                            ApiBaseUrl, "wms", VendorId, matchIdsStr);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        List<WmsOrder> orders = JsonConvert.DeserializeObject<List<WmsOrder>>(content);
                        return orders;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得Order錯誤, id={0}, ex={1}", matchIdsStr, ex.ToString());
            }
            return new List<WmsOrder>();
        }
        /// <summary>
        /// 查詢出貨進度 (3.3.3)
        /// </summary>
        /// <param name="orderIds"></param>
        /// <returns></returns>
        public static List<WmsOrderProgressStatus> GetOrderProgressStatus(params string[] orderIds)
        {
            string matchIdsStr = string.Join(",", orderIds);
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;

                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order/progressstatus?orderid={3}",
                            ApiBaseUrl, "wms_fake", VendorId, matchIdsStr);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order/progressstatus?orderid={3}",
                            ApiBaseUrl, "wms", VendorId, matchIdsStr);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        List<WmsOrderProgressStatus> result = JsonConvert.DeserializeObject<List<WmsOrderProgressStatus>>(content);
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得OrderProgressStatus錯誤, id={0}, ex={1}", matchIdsStr, ex.ToString());
            }
            return new List<WmsOrderProgressStatus>();
        }
        /// <summary>
        /// 查詢出貨物流資訊 (3.4.2)
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static List<WmsLogistic> GetLogistics(string orderId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order/{3}/logistic",
                            ApiBaseUrl, "wms_fake", VendorId, orderId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order/{3}/logistic",
                            ApiBaseUrl, "wms", VendorId, orderId);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        List<WmsLogistic> result = JsonConvert.DeserializeObject<List<WmsLogistic>>(content);
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得Logistic錯誤, id={0}, ex={1}", orderId, ex.ToString());
            }
            return new List<WmsLogistic>();
        }
        /// <summary>
        /// 查訂單配送狀態 (3.5.4)
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public static WmsLogisticTrace GetLogisticTrace(string orderId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order/{3}/logistictrace",
                            ApiBaseUrl, "wms_fake", VendorId, orderId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/order/v1/ship/vendor/{2}/order/{3}/logistictrace",
                            ApiBaseUrl, "wms", VendorId, orderId);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        WmsLogisticTrace result = JsonConvert.DeserializeObject<WmsLogisticTrace>(content);
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得LogisticTrace錯誤, id={0}, ex={1}", orderId, ex.ToString());
            }
            return null;
        }
        /// <summary>
        /// 新增退貨單 (4.2.4)
        /// </summary>
        public static bool AddRefund(WmsRefund refund)
        {
            refund.RefundContact = WmsHelper.Encrypt(refund.RefundContact);
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;

                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/refund/v1/ship/vendor/{2}/refund", ApiBaseUrl, "wms_fake", VendorId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/refund/v1/ship/vendor/{2}/refund", ApiBaseUrl, "wms", VendorId);
                    }

                    var postData = JObject.FromObject(refund).ToString();

                    HttpResponseMessage response = client.PostAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var loc = response.Headers.Location;
                        logger.InfoFormat("AddRefund: {0} {1}, refund={2}", response.StatusCode, response.Headers.Location, JsonConvert.SerializeObject(refund));
                        refund.Id = loc.OriginalString.Split("/").LastOrDefault();
                        if (IsDevelopment)
                        {
                            //測試API亂吐,自己定義
                            refund.Id = "RF" + DateTime.Now.ToString("yyyyMMddHHmmss");
                        }
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("AddRefund錯誤: {0} {1}, refund={2}", response.StatusCode, content, JsonConvert.SerializeObject(refund));
                    } 
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("AddRefund錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(refund), ex);
            }
            return false;
        }
        /// <summary>
        /// 查退貨單資訊:用退貨單編號 (4.2.5)
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public static WmsRefund GetRefund(string refundId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/refund/v1/ship/vendor/{2}/refund/{3}", ApiBaseUrl, "wms_fake", VendorId, refundId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/refund/v1/ship/vendor/{2}/refund/{3}", ApiBaseUrl, "wms", VendorId, refundId);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        WmsRefund refund = JsonConvert.DeserializeObject<WmsRefund>(content);
                        refund.RefundContact = WmsHelper.Decrypt(refund.RefundContact);
                        return refund;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得Refund錯誤, id={0}, ex={1}", refundId, ex.ToString());
            }
            return null;
        }
        /// <summary>
        /// 查詢退貨逆物流配送狀態 (4.3.4)
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public static WmsRefundTrace GetRefundTrace(string refundId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;

                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/refund/v1/ship/vendor/{2}/refund/{3}/logistic", ApiBaseUrl, "wms_fake", VendorId, refundId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/refund/v1/ship/vendor/{2}/refund/{3}/logistic", ApiBaseUrl, "wms", VendorId, refundId);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        WmsRefundTrace refund = JsonConvert.DeserializeObject<WmsRefundTrace>(content);
                        return refund;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得RefundTrace錯誤, id={0}, ex={1}", refundId, ex.ToString());
            }
            return null;
        }
        /// <summary>
        /// 新增還貨單 (5.2.4)
        /// </summary>
        /// <param name="prod"></param>
        /// <returns></returns>
        public static bool AddReturn(WmsAddReturn item)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/return/v1/vendor/{2}/return",
                            ApiBaseUrl, "wms_fake", VendorId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/return/v1/vendor/{2}/return",
                            ApiBaseUrl, "wms", VendorId);
                    }

                    var postData = JObject.FromObject(item, new JsonSerializer { NullValueHandling = NullValueHandling.Ignore }).ToString();

                    HttpResponseMessage response = client.PostAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var loc = response.Headers.Location;
                        item.Id = loc.OriginalString.Split("/").LastOrDefault();
                        return true;
                    }
                    else
                    {
                        var statusCode = response.StatusCode;
                        item.Error=response.Content.ReadAsStringAsync().Result;
                        string content = response.Content.ReadAsStringAsync().Result;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("新增Return錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(item), ex.ToString());
            }
            return false;
        }
        /// <summary>
        /// 查詢還貨單資訊 (5.2.5)
        /// </summary>
        /// <param name="returnIds"></param>
        /// <returns></returns>
        public static List<WmsReturn> GetReturns(params string[] returnIds)
        {
            try
            {
                string matchId = string.Join(",", returnIds);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/return/v1/vendor/{2}/return?id={3}",
                            ApiBaseUrl, "wms_fake", VendorId, matchId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/return/v1/vendor/{2}/return?id={3}",
                            ApiBaseUrl, "wms", VendorId, matchId);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        List<WmsReturn> items = JsonConvert.DeserializeObject<List<WmsReturn>>(content);
                        return items ?? new List<WmsReturn>();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得Return錯誤, groupIds={0}, ex={1}", String.Join(",", returnIds), ex.ToString());
            }
            return new List<WmsReturn>();
        }
        /// <summary>
        /// 查詢還貨清單:用還貨申請日期區間 (5.2.6)
        /// </summary>
        /// <param name="returnIds"></param>
        /// <returns></returns>
        public static WmsReturnQueryList GetWmsReturnQueryList(DateTime startApplyDate, DateTime endApplyDate, int offset = 1, int limit = 20)
        {
            if (startApplyDate == default(DateTime) || endApplyDate == default(DateTime) || endApplyDate <= startApplyDate)
            {
                throw new Exception("輸入日期不正確");
            }
            try
            {
                string queryStr = string.Format("applydate={0}-{1}&offset={2}&limit={3}",
                    startApplyDate.ToString("yyyy/MM/dd"), endApplyDate.ToString("yyyy/MM/dd"), offset, limit);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;

                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/return/v1/vendor/{2}/return?{3}",
                            ApiBaseUrl, "wms_fake", VendorId, queryStr);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/return/v1/vendor/{2}/return?{3}",
                            ApiBaseUrl, "wms", VendorId, queryStr);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        WmsReturnQueryList item = JsonConvert.DeserializeObject<WmsReturnQueryList>(content);
                        return item;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得Return錯誤, groupIds={0}, ex={1}",
                    string.Join(",", new object[] { startApplyDate, endApplyDate, offset, limit }), ex.ToString());
            }
            return null;
        }

        /// <summary>
        /// 下載帳務資料
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetWmsFee(string feeName,DateTime billingDate)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;

                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/account/next/v1/ship/vendor/{2}/{3}/export?accountingdate={4}", ApiBaseUrl, "wms_fake", VendorId, feeName, billingDate.ToString("yyyy/MM/dd"));
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/account/next/v1/ship/vendor/{2}/{3}/export?accountingdate={4}", ApiBaseUrl, "wms", VendorId, feeName, billingDate.ToString("yyyy/MM/dd"));
                    }

                    

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                    string jsonData = JsonConvert.SerializeObject(response);

                    if (response.IsSuccessStatusCode)
                    {
                        logger.InfoFormat("GetWmsFee: {0} - {1}", response.StatusCode, jsonData);
                    }
                    else
                    {
                        logger.InfoFormat("GetWmsFee: {0} - {1}", response.StatusCode, jsonData);
                    }
                    return apiUrl;
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("檔案{0} - {1}", feeName, ex.ToString());
                logger.Warn("檔案" + feeName, ex);
                return "";
            }
        }
    }
}

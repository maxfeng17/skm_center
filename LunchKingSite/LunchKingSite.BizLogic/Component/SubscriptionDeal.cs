﻿using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class SubscriptionDeal
    {
        private int _order;
        public int Order
        {
            get
            {
                return _order;
            }
        }

        private ViewPponDeal _vpd;
        public ViewPponDeal ViewPponDeal
        {
            get
            {
                return _vpd;
            }
        }

        private string _cityName;
        public string CityName
        {
            get
            {
                return _cityName;
            }
        }

        private bool _isTeriminal;
        public bool IsTeriminal
        {
            get
            {
                return _isTeriminal;
            }
        }

        public SubscriptionDeal(int order, ViewPponDeal vpd, string cityName, bool isTeriminal)
        {
            _order = order;
            _vpd = vpd;
            _cityName = cityName;
            _isTeriminal = isTeriminal;
        }
    }
}

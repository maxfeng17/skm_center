﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.FamiPort;
using LunchKingSite.BizLogic.Models.QueueModels;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.DataOrm;
using Famiport = LunchKingSite.DataOrm.Famiport;
using Peztemp = LunchKingSite.Core.Models.Entities.Peztemp;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class FamiGroupCoupon
    {
        protected static ISysConfProvider Config = ProviderFactory.Instance().GetConfig();
        protected static IFamiportProvider Fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
        protected static IOrderProvider Op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected static IPponProvider Pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected static IMemberProvider Mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ILog Logger = LogManager.GetLogger(typeof(FamiGroupCoupon).Name);

        #region Famiport Pincode AES

        public static string FamiportAesKey
        {
            get { return Config.FamiportAesKey; }
        }

        public static string FamiportAesIv
        {
            get
            {
                string md5 = new Security(SymmetricCryptoServiceProvider.DefaultMD5).Encrypt(FamiportAesKey);
                byte[] hexByte = new byte[md5.Length/2];
                for (int i = 0; i < md5.Length; i = i + 2)
                {
                    hexByte[i/2] = Convert.ToByte(md5.Substring(i, 2), 16);
                }
                return Convert.ToBase64String(hexByte);
            }
        }

        public static string EncryptPincode(string code)
        {
            byte[] iv = Convert.FromBase64String(FamiportAesIv);
            Security s = new Security(SymmetricCryptoServiceProvider.AES, Encoding.UTF8.GetBytes(FamiportAesKey),
                iv);
            return s.Encrypt(code);
        }

        public static string DecryptPincode(string code)
        {
            byte[] iv = Convert.FromBase64String(FamiportAesIv);
            Security s = new Security(SymmetricCryptoServiceProvider.AES, Encoding.UTF8.GetBytes(FamiportAesKey),
                iv);
            return s.Decrypt(code);
        }

        #endregion Famiport Pincode AES

        #region Famiport API

        private static bool FamiportPincodePinOrder(int famiportPincodeTransId, FamilyNetEvent famiEvent)
        {
            var trans = Fami.GetFamilyNetPincodeTrans(famiportPincodeTransId);
            if (trans == null)
            {
                return false;
            }

            var order = new PinOrderInput
            {
                OrderNo = trans.OrderTicket.ToString(),
                ActCode = famiEvent.ActCode,
                Qty = trans.Qty.ToString()
            };
            order.SetOrderDate(trans.CreateTime);
            order.SetTransStatus(false);

            var js = new JsonSerializer();
            var formData = string.Format("json_info={0}", js.Serialize(order));
            var formBtyes = Encoding.UTF8.GetBytes(formData);
            var famiApiUrl = string.Format(GetFamiportApiUrl((FamilyBarcodeVersion)famiEvent.Version), "PinOrder.aspx");
            var requestResult = FamiWebRequest(formBtyes, famiApiUrl);
            var r = js.Deserialize<PinOrderResult>(requestResult);
            Logger.InfoFormat("FamiGroupCoupon -> {0}", r);
            trans.ReplyCode = r.ReplyCode ?? string.Empty;
            trans.ReplyDesc = r.ReplyDesc ?? string.Empty;
            trans.Pincode = r.PinCode ?? string.Empty;
            trans.PinExpiredate = r.PinExpiredate ?? string.Empty;
            var pinCount = string.IsNullOrEmpty(r.PinCode) ? 0 : r.PinCode.Split(',').Count();

            if (r.GetReplyCode() == FamilyNetReplyCode.Code00 && trans.Qty == pinCount)
            {

                trans.Status = (int) FamilyNetPincodeStatus.Completed;
            }
            else
            {
                trans.Status = (int)FamilyNetPincodeStatus.Fail;
            }

            if (Fami.UpdateFamilyNetPincodeTran(trans) && r.GetReplyCode() == FamilyNetReplyCode.Code00)
            {
                return true;
            }

            return false;
        }

        private static string GetFamiportApiUrl(FamilyBarcodeVersion version)
        {
            switch (version)
            {
                case FamilyBarcodeVersion.FamiSingleBarcode:
                    return Config.FamiportSingleBarcodeApiUrl;
                case FamilyBarcodeVersion.Pincode:
                default:
                    return Config.FamiportApiUrl;
            }
        }

        public static string FamiWebRequest(byte[] dataBytes, string url)
        {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
            request.Method = WebRequestMethods.Http.Post;
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = dataBytes.Length;
            request.Timeout = 60000;
            
            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(dataBytes, 0, dataBytes.Length);
                requestStream.Flush();
            }

            string result;

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        result = reader.ReadToEnd();
                    }
                }
            }
            
            return result;
        }

        public static void FamiportPincodeOrderExg(Peztemp pez, Guid orderGuid, int couponId)
        {
            var famiEvent = Fami.GetFamilyNetEvent(pez.Bid ?? Guid.Empty);
            if (famiEvent == null || string.IsNullOrEmpty(famiEvent.ActCode))
            {
                return;
            }

            FamilyNetPincode pincode = Fami.GetFamilyNetPincode(pez.Id, famiEvent.ActCode, orderGuid);
            if (pincode == null)
            {
                pincode = Fami.AddFamilyNetPincode(pez, famiEvent, orderGuid, couponId);
            }
            else if (pincode.Status == (byte) FamilyNetPincodeStatus.Completed)
            {
                return;
            }
            else
            {
                pincode.FailRetry++;
            }

            if (pincode == null)
            {
                return;
            }

            var input = new PincodeOrderExgInput
            {
                OrderNo = pincode.OrderTicket.ToString(),
                ActCode = pincode.ActCode,
                Pincode = HttpUtility.UrlEncode(EncryptPincode(pez.PezCode))
            };

            input.SetOrderDate(pincode.OrderDate);
            input.SetTransStatus(pincode.Status == (byte) FamilyNetPincodeStatus.Completed || pincode.ReplyCode == Convert.ToString((int)FamilyNetReplyCode.Code23)
                ? FamilyNetTransStatus.Code22
                : FamilyNetTransStatus.Code02);

            input.Detail.Add(new OrderExgInputDetail
            {
                SerialNo = "01",
                ItemCode = famiEvent.ItemCode
            });

            var js = new JsonSerializer();
            var formData = string.Format("json_info={0}", js.Serialize(input));
            var jsonBtyes = Encoding.UTF8.GetBytes(formData);
            var famiApiUrl = string.Format(GetFamiportApiUrl((FamilyBarcodeVersion)famiEvent.Version), "OrderExg.aspx");
            var requestResult = FamiWebRequest(jsonBtyes, famiApiUrl);
            var r = js.Deserialize<OrderExgResult>(requestResult);
            pincode.ReplyCode = r.ReplyCode;
            pincode.ReplyDesc = r.ReplyDesc;
            pincode.ModifyDate = DateTime.Now;
            List<FamilyNetPincodeDetail> detailCol = new List<FamilyNetPincodeDetail>();

            if (r.GetReplyCode() != FamilyNetReplyCode.Code00)
            {
                pincode.Status = (byte)FamilyNetPincodeStatus.Fail;
            }
            else
            {
                pez.IsUsed = true;
                pez.UsedTime = DateTime.Now;
                pincode.Status = (byte)FamilyNetPincodeStatus.Completed;
                pincode.TranNo = r.TranNo;
                pincode.PinExpiredate = r.PinExpiredate;
                pincode.ActMemo1 = r.ActMemo1;
                pincode.ActMemo2 = r.ActMemo2;
                pincode.ActMemo3 = r.ActMemo3;
                pincode.ActMemo4 = r.ActMemo4;
                pincode.FnTranNo = r.FnTranNo;

                foreach (var d in r.Detail)
                {
                    detailCol.Add(new FamilyNetPincodeDetail
                    {
                        PincodeOrderNo = pincode.OrderNo,
                        SerialNo = int.Parse(d.SerialNo),
                        ItemCode = d.ItemCode,
                        ProdName = d.ProdName,
                        Barcode1 = d.Barcode1,
                        Barcode2 = d.Barcode2,
                        Barcode3 = d.Barcode3,
                        Barcode4 = d.Barcode4,
                        Barcode5 = d.Barcode5,
                        ExpireDate = d.ExpireDate,
                        ExgKind = d.ExgKind,
                        ExgBonus = int.Parse(d.ExgBonus),
                        DisAmt = int.Parse(d.DisAmt),
                        ProdMemo1 = d.ProdMemo1,
                        ProdMemo2 = d.ProdMemo2,
                        ProdMemo3 = d.ProdMemo3,
                        ProdMemo4 = d.ProdMemo4,
                        CreateTime = DateTime.Now
                    });
                }
            }

            if (r.GetReplyCode() == FamilyNetReplyCode.Code23)
            {
                var checkPin = Fami.GetFamilyNetPincode(pincode.OrderNo);
                if (checkPin.Status == (byte) FamilyNetPincodeStatus.Completed)
                {
                    Fami.SetFamilyNetOrderExgGateExpired(pez.Id);
                    return;
                }
            }

            if (Fami.UpdatePeztemp(pez))
            {
                if (Fami.UpdateFamilyNetPincode(pincode))
                {
                    if (detailCol.Any())
                    {
                        Fami.AddFamilyNetPincodeDetail(detailCol);
                    }
                }
            }

            Fami.SetFamilyNetOrderExgGateExpired(pez.Id);
        }

        #endregion

        #region 交易

        public static bool MakeFamiportPincodeTrans(IViewPponDeal vpd, Order o, int qty, int userId)
        {
            bool result = false;
            try
            {
                var famiEvent = Fami.GetFamilyNetEvent(vpd.BusinessHourGuid);
                if (famiEvent == null)
                {
                    return false;
                }
                FamilyNetPincodeTran entity = new FamilyNetPincodeTran
                {
                    OrderGuid = o.Guid,
                    Qty = qty,
                    FamilyNetEventId = famiEvent.Id,
                    UserId = userId
                };

                var transId = Fami.AddFamilyNetPincodeTran(entity);
                result = FamiportPincodePinOrder(transId, famiEvent);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("FamiGroupCoupon:{0}, {1}", ex.Message, ex.StackTrace);
            }
            return result;
        }

        public static List<string> GetFamiportPincodeByOrderGuid(Guid orderGuid)
        {
            var trans = Fami.GetFamilyNetPincodeTrans(orderGuid);
            if (trans.Pincode == null)
            {
                return new List<string>();
            }
            else
            {
                return trans.Pincode.Split(',').ToList();
            }
        }

        /// <summary>
        /// 重做取Pin
        /// </summary>
        /// <param name="orderGuid"></param>
        public static void RemakePincodeTrans(Guid orderGuid)
        {
            var orderDetail = Op.OrderDetailGetList(orderGuid).FirstOrDefault();
            if (orderDetail == null)
            {
                return;
            }

            var couponCol = Pp.CouponGetListWithNolock(orderDetail.Guid);

            foreach (var c in couponCol)
            {
                Peztemp p = Fami.GetPeztemp(c.SequenceNumber);
                if (p == null)
                {
                    continue;
                }

                FamiportPincodeOrderExg(p, orderGuid, c.Id);
            }
        }

        #endregion

        #region 退貨

        /// <summary>
        /// 新版多筆退貨
        /// </summary>
        /// <param name="input"></param>
        /// <param name="orderGuid"></param>
        /// <param name="couponId"></param>
        /// <param name="returnType"></param>
        /// <returns></returns>
        public static PincodeReturnResult2 VoidFamilyNetPincode2(PincodeReturnInput2 input, List<CashTrustLog> ctlc, Guid orderGuid
            , Guid? bid, FamilyNetReturnType returnType = FamilyNetReturnType.UserReturn)
        {
            DateTime requestTime = DateTime.Now;
            List<FamilyNetReturnLog> list = new List<FamilyNetReturnLog>();
            foreach (var d in input.Detail)
            {
                FamilyNetReturnLog rl = new FamilyNetReturnLog
                {
                    Status = input.Status,
                    OrderDate = input.OrderDate,
                    ActCode = d.ActCode,
                    CreateTime = requestTime,
                    OrderGuid = orderGuid,
                    CouponId = d.CouponId,
                    ReturnType = (byte)returnType
                };
                list.Add(rl);
            }

            var version = FamilyBarcodeVersion.Pincode;
            if (bid.HasValue)
            {
                var famiEvent = Fami.GetFamilyNetEvent(bid.Value);
                version = (FamilyBarcodeVersion)famiEvent.Version;
            }
            PincodeReturnResult2 result = new PincodeReturnResult2();

            try
            {
                //丟出時要拔掉couponId...之後看有無更好的做法
                var js = new JsonSerializer();
                var jObj = JObject.Parse(js.Serialize(input));
                jObj.Descendants().OfType<JProperty>().Where(x => x.Name == "CouponId").ToList().ForEach(s => s.Remove());
                var inputObj = jObj.ToString();

                var formData = string.Format("json_info={0}", inputObj);
                var formBtyes = System.Text.Encoding.UTF8.GetBytes(formData);
                var famiApiUrl = string.Format(GetFamiportApiUrl(version), "PinReturn.aspx");

                var requestResult = FamiWebRequest(formBtyes, famiApiUrl);
                result = js.Deserialize<PincodeReturnResult2>(requestResult);
                DateTime responseTime = DateTime.Now;
                foreach (var d in list)
                {
                    var ctl = ctlc.FirstOrDefault(c=>c.CouponId == d.CouponId);
                    if (ctl.IsLoaded)
                    {
                        string pinCode = EncryptPincode(ctl.CouponSequenceNumber);
                        d.ReplyCode = result.Detail.FirstOrDefault(x => x.PinCode == pinCode).ReplyCode;
                        d.ReplyTime = responseTime;
                        d.ReplyDesc = result.Detail.FirstOrDefault(x => x.PinCode == pinCode).ReplyDesc;
                        d.CouponId = result.Detail.FirstOrDefault(x => x.PinCode == pinCode).CouponId = d.CouponId ?? 0;
                    }
                }
            }
            catch (WebException e)
            {
                //出錯不要緊 會有job再補處理
                Logger.Error(e.Message);
            }
            Fami.AddFamilyNetReturnLog(list);
            return result;
        }
        #endregion

        #region Job

        public static void ExecuteVerifyRetryForView()
        {
            var col = Fami.GetViewFamilyNetVerifyRetry();
            foreach (var c in col)
            {
                if (c.PincodeOrderNo == null)
                {
                    continue;
                }

                var q = new FamiVerifyPincodeMq
                {
                    VerifyLogId = c.VerifyLogId,
                    FamilyNetPincodeOrderNo = c.PincodeOrderNo ?? 0,
                    PeztempId = c.PeztempId
                };

                IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
                mqp.Send(FamiVerifyPincodeMq._QUEUE_NAME, q);
            }
        }

        public static void ExecuteVerifyRetry()
        {
            var col = Fami.GetFamilyNetVerifyLogRetryData(Config.FamiportPinVerifyRetryPerDay, Config.FamiportPinVerifyRetryTimes, Config.FamiportPinVerifyRetryTimeSpan);
            List<FamilyNetVerifyLog> updateCol = new List<FamilyNetVerifyLog>();
            foreach (var r in col)
            {
                if (r.PincodeOrderNo == null)
                {
                    string code = string.Empty;
                    try
                    {
                        code = FamiGroupCoupon.DecryptPincode(r.CdPinCode);
                    }
                    catch (Exception ex)
                    {
                        r.RetryCount ++;
                        r.Remark = string.Format("Pincode Decode Error:{0}", ex.Message);
                        updateCol.Add(r);
                        continue;
                    }

                    var pinTemp = Fami.GetFamilyNetPincodeByPezCode(code);
                    if (pinTemp == null)
                    {
                        r.RetryCount++;
                        r.Remark = "pincode is not found 1.5";
                        updateCol.Add(r);
                        continue;
                    }

                    r.PincodeOrderNo = pinTemp.OrderNo;
                    r.Remark = string.Empty;
                    updateCol.Add(r);
                    continue;
                }

                var pin = Fami.GetFamilyNetPincode(r.PincodeOrderNo ?? 0);
                var q = new FamiVerifyPincodeMq
                {
                    VerifyLogId = r.Id,
                    FamilyNetPincodeOrderNo = pin.OrderNo,
                    PeztempId = pin.PeztempId
                };

                IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
                mqp.Send(FamiVerifyPincodeMq._QUEUE_NAME, q);
            }

            if (updateCol.Any())
            {
                Fami.UpdateFamilyNetVerifyLog(updateCol);
            }
        }

        public static void ExecutePincodeOrderRetry()
        {
            var retryCol = Fami.GetFamilyNetPincodeRetryData(Config.FamiportPinOrderExgRetryPerDay, Config.FamiportPinOrderExgRetryTimes, Config.FamiportPinOrderExgRetryTimeSpan);
            foreach (var r in retryCol)
            {
                var pez = Fami.GetPeztemp(r.PeztempId);
                if (pez != null && pez.Id > 0 && Config.ActiveMQEnable)
                {
                    IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
                    var famiMq = new FamiGroupCouponMq
                    {
                        PeztempCode = FamiGroupCoupon.EncryptPincode(pez.PezCode),
                        OrderGuid = r.OrderGuid
                    };
                    mqp.Send(FamiGroupCouponMq._QUEUE_NAME, famiMq);
                }
                else
                {
                    r.FailRetry = (byte)Config.FamiportPinOrderExgRetryTimes;
                    Fami.UpdateFamilyNetPincode(r);
                }
            }
        }

        public static void ExecuteUnlockPincode()
        {
            Fami.SetFamilyNetPincodeUnlock(Config.FamilyNetUnlockPincodeSec);
        }

        public static void ExecutePincodeTransRetry()
        {
            var data = Fami.GetFamilyNetPincodeTransRetry();
            foreach (var d in data)
            {
                RemakePincodeTrans(d.OrderGuid);
            }
        }

        public static void ExecutePincodeReturnRetry()
        {
            //取出modify_time 一小時以前且退貨中的pincode 再做一次退貨
            DateTime returnTime = DateTime.Now.AddHours(-1);
            var pincodes = Fami.GetFamilyNetPincodeReturning(returnTime);
            string errMsg = "";
            if (pincodes.Count > 0)
            {
                var returnOrderList = pincodes.GroupBy(x => x.OrderGuid).Select(s => s.Key).ToList();
                foreach(var orderGuid in returnOrderList)
                {
                    ReturnForm rf = Op.ReturnFormGetListByOrderGuid(orderGuid).FirstOrDefault(x => x.ProgressStatus == (int)ProgressStatus.Processing);
                    //正在處理中的退貨單上
                    if (rf != null)
                    {
                        Order o = Op.OrderGet(orderGuid);
                        CashTrustLogCollection allCtlogs = Mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
                        List<CashTrustLog> ctlc = new List<CashTrustLog>();
                        foreach (CashTrustLog ctlog in allCtlogs)
                        {
                            if (ctlog.Status == (int)TrustStatus.Initial ||
                                 ctlog.Status == (int)TrustStatus.Trusted)
                            {
                                ctlc.Add(ctlog);
                            }
                        }

                        if (ctlc.Count > 0)
                        {
                            OrderFacade.FamilyNetPincodeListReturn(ctlc, pincodes, o, null, pincodes.FirstOrDefault(x => x.OrderGuid == o.Guid).ActCode, out errMsg, FamilyNetReturnType.JobRetry);
                        }
                    }
                }
            }
        }


        #endregion

        #region Provider

        public static FamilyNetPincodeTran GetFamilyNetPincodeTrans(Guid orderGuid)
        {
            return Fami.GetFamilyNetPincodeTrans(orderGuid);
        }

        #endregion

        #region Common Method

        public static string GetFamiportAuthId()
        {
            var code = Config.Famiport17LifeCode;
            return string.Format(Config.FamiportAuthFormat, code[0], code[1], code[2], code[3], code[4], code[5]);
        }

        /// <summary>
        /// 確認兌換及鎖定
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="couponIdList"></param>
        /// <returns></returns>
        public static int ConfirmExchangeAndLock(Guid orderGuid, List<int> couponIdList)
        {
            return Fami.SetFamilyNetPincodeLock(orderGuid, couponIdList);
        }

        /// <summary>
        /// 取得全家鎖定憑證數量
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static int GetFamiCouponLockedCount(ViewCouponListMain main)
        {
            var lockedCount = 0;
            var couponType = PponFacade.GetDealCouponType(main);
            if (couponType == DealCouponType.FamilyNetPincode)
            {
                lockedCount = Fami.GetFamiCouponLockedCount(main.Guid);
            }
            return lockedCount;
        }

        /// <summary>
        /// 取得全家寄杯憑證Barcode相關資訊
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static Dictionary<int, FamiUserCouponBarcodeInfo> GetUserCouponBarcodeInfoListByOrderGuid(Guid orderGuid)
        {
            var famiBarcodeInfoDic = new Dictionary<int, FamiUserCouponBarcodeInfo>();
            var userOrderPincodeBarcodeList = Fami.GetUserCouponBarcodeInfoListByOrderGuid(orderGuid);
            if (userOrderPincodeBarcodeList.Any())
            {
                famiBarcodeInfoDic = userOrderPincodeBarcodeList.ToDictionary(pb => pb.CouponId);
            }
            return famiBarcodeInfoDic;
        }

        #endregion
    }
}

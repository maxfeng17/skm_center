﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using log4net;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Component
{
    public class CpaUtility
    {
        private static ILog logger = LogManager.GetLogger(typeof (CpaUtility).Name);
        private static IMarketingProvider mkp = ProviderFactory.Instance().GetProvider<IMarketingProvider>();

        public static void RecordCpaOrderByReferrer(string cpaKey, Order order)
        {
            if (order.Guid == Guid.Empty)
            {
                logger.WarnFormat("RecordCpaOrderByReferrer error. cpaKey={0}", cpaKey);
                return;
            }
            try
            {
                //第一筆與最後一筆
                List<CpaTracking> trackings = mkp.CpaTrackingGetHeadAndTail(cpaKey);
                //最後的外部導購商
                CpaTracking lastExternal = mkp.CpaTrackingGetListExternalByCpaKey(cpaKey);
                if (trackings.Count == 0)
                {
                    throw new Exception();
                }

                string lastExternalRsrc = lastExternal == null ? null : lastExternal.Rsrc;
                mkp.CpaOrderSet(new CpaOrder
                {
                    CpaKey = cpaKey,
                    CreateTime = DateTime.Now,
                    OrderGuid = order.Guid,
                    FirstRsrc = trackings[0].Rsrc,
                    LastRsrc = trackings[1].Rsrc, 
                    LastExternalRsrc = lastExternalRsrc,
                });
            }
            catch (Exception ex)
            {
                logger.WarnFormat("RecordCpaOrderByReferrer:CpaOrderSet error. cpaKey={0}, orderGuid={1}, ex={2}",
                    cpaKey, order.Guid, ex);
            }
        }

        public static string MergeCpaKeysToOne(string[] cpaList)
        {
            if (cpaList == null || cpaList.Length == 0)
            {
                return string.Empty;
            }
            if (cpaList.Length == 1)
            {
                return cpaList[0];
            }
            List<CpaTracking> allTrackings = ProviderFactory.Instance().GetProvider<IMarketingProvider>()
                .CpaTrackingGetListByCpaKey(cpaList).OrderBy(t => t.CreateTime).ToList();
            List<CpaTracking> newTrackings = new List<CpaTracking>();
            string newCpaKey = Guid.NewGuid().ToString();
            foreach (CpaTracking t in allTrackings)
            {
                CpaTracking newTracking = new CpaTracking
                {
                    CpaKey = newCpaKey,
                    CreateTime = t.CreateTime,
                    Rsrc = t.Rsrc,
                    UserId = t.UserId,
                    Url = t.Url
                };
                if (newTrackings.Count == 0 ||
                    (newTrackings.Count > 0 && string.Equals(t.Rsrc, newTrackings.Last().Rsrc) == false))
                {
                    newTrackings.Add(newTracking);
                }
            }
            foreach(CpaTracking newTracking in newTrackings)
            {
                mkp.CpaTrackingSet(newTracking);
            }
            if (newTrackings.Count > 0)
            {
                return newCpaKey;
            }
            return string.Empty;
        }

        public static bool RecordOrderByReferrer(string referenceId, Order order, IViewPponDeal deal ,DealProperty dealProperty)
        {
            if (string.IsNullOrWhiteSpace(referenceId) || order == null)
            {
                return false;
            }

            try
            {
                var service = ServiceLocator.Container.Resolve<CpaService>();
                service.RecordOrderByReferrer(referenceId, order, deal, dealProperty);
                ClearCookieIfOneShot(service, referenceId);
            }
            catch (Exception e)
            {
                // CPA failure shouldn't stop the whole process but we still log the error
                //WebUtility.LogExceptionAnyway(e);
                logger.Error(e);
            }

            return true;
        }

        public static bool RecordRegistrationByReferrer(string srcId, Member member , BusinessModel businessModel)
        {
            if (string.IsNullOrWhiteSpace(srcId) || member == null)
            {
                return false;
            }

            try
            {
                var service = ServiceLocator.Container.Resolve<CpaService>();
                service.RecordRegistrationByReferrer(srcId, member, businessModel);
                ClearCookieIfOneShot(service, srcId);
            }
            catch (Exception e)
            {
                // CPA failure shouldn't stop the whole process but we still log the error
                //WebUtility.LogExceptionAnyway(e);
                logger.Error(e);
            }
            return true;
        }

        public static bool RecordSubscriptionByReferrer(string srcId, Subscription sub)
        {
            if (string.IsNullOrWhiteSpace(srcId) || sub == null)
            {
                return false;
            }

            try
            {
                var service = ServiceLocator.Container.Resolve<CpaService>();
                service.RecordSubscriptionByReferrer(srcId, sub);
                ClearCookieIfOneShot(service, srcId);
            }
            catch (Exception e)
            {
                // CPA failure shouldn't stop the whole process but we still log the error
                //WebUtility.LogExceptionAnyway(e);
                logger.Error(e);
            }
            return true;
        }

        public static bool RecordSubscriptionByReferrer(string srcId, HiDealSubscription sub)
        {
            LogManager.GetLogger(LineAppender._LINE).Info("RecordSubscriptionByReferrer[HiDeal] trigger.");
            if (string.IsNullOrWhiteSpace(srcId) || sub == null)
            {
                return false;
            }

            try
            {
                var service = ServiceLocator.Container.Resolve<CpaService>();
                service.RecordSubscriptionByReferrer(srcId, sub);
                ClearCookieIfOneShot(service, srcId);
            }
            catch (Exception e)
            {
                // CPA failure shouldn't stop the whole process but we still log the error
                //WebUtility.LogExceptionAnyway(e);
                logger.Error(e);
            }
            return true;
        }
        
        private static bool ClearCookieIfOneShot(CpaService service, string referenceId)
        {
            if (HttpContext.Current == null || HttpContext.Current.Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()] == null)
            {
                return false;
            }

            var cmp = service.GetCampaign(referenceId);
            if (cmp == null)
            {
                return false;
            }
            if (cmp.OneShotAction)
            {
                HttpContext.Current.Response.Cookies[LkSiteCookie.ReferrerSourceId.ToString()].Expires = DateTime.Now.AddSeconds(-1);
            }
            return true;
        }
    }
}

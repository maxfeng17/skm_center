﻿using System;
using System.Data.SqlTypes;
using System.IO;
using System.Net;
using System.Text;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.I18N;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class ExternalUser
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public DateTime? Birthday { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Pic { get; set; }
        public int? Gender { get; set; }
    }

    public class FacebookUser : ExternalUser
    {
        public const string DefaultScope = "email,public_profile,user_friends";
        private static ILog logger = LogManager.GetLogger(typeof(FacebookUser).Name);

        public static HttpWebRequest CreateRequest(string url)
        {
            return WebRequest.Create(url) as HttpWebRequest;
        }

        public static FacebookUser Get(string token, bool hasAppSecretProof=true)
        {
            FacebookUser fbuser = null;
            string appSecretProof = string.Empty;
            if (!string.IsNullOrWhiteSpace(token))
            {
                string url = string.Format(
                    "https://graph.facebook.com/{0}me?fields=first_name,last_name,birthday,email,picture.type(large),gender&access_token={1}",
                    FacebookUtility._FACEBOOK_API_VER_2_10, token);

                if (hasAppSecretProof)
                {
                    appSecretProof = FacebookUtility.GenerateAppSecretProof(token);
                    url += "&appsecret_proof=" + appSecretProof;
                }

                HttpWebRequest request = CreateRequest(url);

                request.Method = "GET";
                try
                {
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        StreamReader reader = new StreamReader(response.GetResponseStream());

                        dynamic user = JObject.Parse(reader.ReadToEnd());
                        fbuser = new FacebookUser();
                        fbuser.Id = user.id;
                        fbuser.Email = user.email;
                        fbuser.FirstName = user.first_name;
                        fbuser.LastName = user.last_name;

                        if (user.birthday != null)
                        {
                            DateTime birthday;
                            if (DateTime.TryParse((string)user.birthday, out birthday) &&
                                birthday >= SqlDateTime.MinValue && birthday <= SqlDateTime.MaxValue)
                            {
                                fbuser.Birthday = birthday;
                            }
                        }
                       
                        if (user.picture != null && user.picture.data != null && user.picture.data.url != null)
                        {
                            fbuser.Pic = (string)user.picture.data.url;
                        }
                        else
                        {
                            fbuser.Pic = string.Format("http://graph.facebook.com/{0}/picture?type=large", fbuser.Id);
                        }

                        if (user.gender != null)
                        {
                            if (user.gender == "male")
                            {
                                fbuser.Gender = 1;
                            }
                            else if (user.gender == "female")
                            {
                                fbuser.Gender = 0;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Info("get facebookuser error. url:" + url, ex);
                }
            }

            return fbuser;
        }
    }

    public class PayeasyUser : ExternalUser
    {
        private static ILog logger = LogManager.GetLogger(typeof(FacebookUser).Name);
        private static ISysConfProvider conf = ProviderFactory.Instance().GetConfig();

        public static HttpWebRequest CreateRequest(string url)
        {
            return WebRequest.Create(url) as HttpWebRequest;
        }

        public static PayeasyUser Get(string memId, string password, out string errorMessage)
        {
            errorMessage = string.Empty;
            if (string.IsNullOrWhiteSpace(memId) || string.IsNullOrWhiteSpace(password))
            {
                errorMessage = "參數不正確";
                return null;
            }
            HttpWebRequest request = WebRequest.Create(conf.PayeasyIdCheckWebServiceUrl) as HttpWebRequest;

            string param = string.Format("memId={0}&memPwd={1}&tryCount={2}&clientIp={3}&appName={4}",
                memId, password, "1", Helper.GetClientIP(), "23");

            byte[] bs = Encoding.ASCII.GetBytes(param);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = bs.Length;

            try
            {
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                }

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());

                    JObject user = JObject.Parse(reader.ReadToEnd());
                    string code = user.Value<string>("Code");
                    if (string.IsNullOrWhiteSpace(code))
                    {
                        errorMessage = Phrase.SignInReplyOtherError;
                        return null;
                    }

                    if (code.Equals("OK"))
                    {
                        //登入成功
                        string message = user.Value<string>("Message");
                        string[] idList = message.Split(';');
                        if (idList.Length <= 2)
                        {
                            string externalId = idList[1];
                            return new PayeasyUser
                            {
                                Id = externalId
                            };
                        }
                    }
                    else if (code.Equals("FAIL"))
                    {
                        //登入失敗，帳號密碼錯誤
                        errorMessage = Phrase.SignInReplyAccountNotExist;
                        return null;
                    }
                    else if (code.Equals("EXCEPTION"))
                    {
                        errorMessage = "您的帳號無法登入，請與PayEasy客服聯絡，謝謝。";
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info("PayeasyUser get fail.", ex);
            }
            errorMessage = Phrase.SignInReplyAccountNotExist;
            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
	public class HiDealUsageExpiration: IExpirationChangeable
	{
		#region prop
		
		public DateTime? DealUseStartDate { get; private set; }

		public DateTime? DealOriginalExpireDate { get; private set; }

		public DateTime? DealChangedExpireDate { get; private set; }

		public DateTime? StoreChangedExpireDate { get; private set; }

		public DateTime? SellerClosedDownDate { get; private set; }

		public DateTime? StoreClosedDownDate { get; private set; }

		#endregion

		#region .ctor
		
		private HiDealUsageExpiration(){ }
		
		#endregion

		private static IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
		public static HiDealUsageExpiration HiDealUsageExpirationCreator(Guid productGuid, Guid? storeGuid)
		{
			ViewHiDealExpirationCollection expirations = hp.ViewHiDealExpirationGetList(productGuid);
			HiDealUsageExpiration result = new HiDealUsageExpiration();

			if (storeGuid == null)
			{
				ViewHiDealExpiration ex = expirations.FirstOrDefault();
				if (ex != null)
				{
					result.DealUseStartDate = ex.UseStartTime;
					result.DealOriginalExpireDate = ex.UseEndTime;
					result.DealChangedExpireDate = ex.ProductChangedExpireTime;
					result.StoreChangedExpireDate = null;
					result.SellerClosedDownDate = ex.SellerCloseDownDate;
					result.StoreClosedDownDate = null;
				}
			}
			else
			{
				ViewHiDealExpiration ex = expirations.Where(exp => exp.StoreGuid == storeGuid).FirstOrDefault();
				if (ex != null)
				{
					result.DealUseStartDate = ex.UseStartTime;
					result.DealOriginalExpireDate = ex.UseEndTime;
					result.DealChangedExpireDate = ex.ProductChangedExpireTime;
					result.StoreChangedExpireDate = null;	//ToDo: 等品生活加入分店截止時間的功能後再做
					result.SellerClosedDownDate = ex.SellerCloseDownDate;
					result.StoreClosedDownDate = ex.StoreCloseDownDate;
				}
			}

			return result;
		}
	}
}

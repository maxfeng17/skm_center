﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class PponVendorBillingHelper: IVendorBillingHelper
    {
        public Guid Bid { get; set; }

        private DealAccounting da = null;
        public VendorBillingModel VendorBillingModel
        {
            get
            {
                if (da == null)
                {
                    da = pp.DealAccountingGet(Bid);
                }
                return (VendorBillingModel)da.VendorBillingModel;
            }
        }

        public RemittanceType FinancialStaffPaymentMethod
        {
            get
            {
                if (da == null)
                {
                    da = pp.DealAccountingGet(Bid);
                }
                return (RemittanceType)da.RemittanceType;
            }
        }

        public bool IsPayToCompany
        {
            get
            {
                if (da == null)
                {
                    da = pp.DealAccountingGet(Bid);
                }
                return int.Equals(1, da.Paytocompany);
            }
        }

        public int? DeliveryType
        {
            get
            {
                return pp.DealPropertyGet(Bid).DeliveryType;
            }
        }

        private IEnumerable<PponStore> pponStores;
        public IEnumerable<Guid> ParticipatingStores
        {
            get
            {
                if (pponStores == null)
                {
                    //限定分店使用憑證檔次 對帳單產出依銷售分店產出
                    //通用券檔次 對帳單產出依可核銷分店產出
                    var isRestrictedStore = true;
                    var bh = pp.BusinessHourGet(Bid);
                    if (bh != null)
                    {
                        isRestrictedStore = !Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
                    }
                    pponStores = pp.PponStoreGetListByBusinessHourGuid(Bid)
                                    .Where(x => isRestrictedStore
                                                ? Helper.IsFlagSet(x.VbsRight, VbsRightFlag.VerifyShop) 
                                                : Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Verify));
                }

                if(pponStores == null) //避免 Subsonic 作怪
                {
                    return new List<Guid>();
                }

                return pponStores.Select(pponStore => pponStore.StoreGuid);
            }
        }

        public CashTrustLogCollection GetAllSuccessfulOrderTrustList()
        {
            return mp.CashTrustLogGetListByBidWhereOrderIsSuccessful(Bid);
        }

        public CashTrustLogCollection GetAllSuccessfulOrderTrustList(DateTime genDate)
        {
            return mp.CashTrustLogGetListByBidWhereOrderIsSuccessful(Bid, genDate);
        }

        private IPponProvider pp;
        private IMemberProvider mp;

        #region .ctors
        
        public PponVendorBillingHelper(Guid bid)
        {
            Bid = bid;
            InitializeProviders();
        }

        #endregion

        private void InitializeProviders()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }





        
    }
}

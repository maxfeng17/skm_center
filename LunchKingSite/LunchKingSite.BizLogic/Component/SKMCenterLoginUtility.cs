﻿using System;
using System.Collections.Specialized;
using LunchKingSite.Core.Component;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;

namespace LunchKingSite.BizLogic.Component
{
    public class SKMCenterLoginUtility
    {
        public long CheckStartTimeStamp { get; set; }
        public long CheckEndTimeStamp { get; set; }

        public long CurrentTimeStamp { get; set; }

        private string _appKey = string.Empty;
        private int _checkSec = 10;


        public SKMCenterLoginUtility(int checkSec = 10)
        {
            //取得key值
            var config = ProviderFactory.Instance().GetConfig();
            //this._appKey = config.SKMCenterAppKey;
            this._appKey = "app";

            //計算起迄的timestamp
            this._checkSec = checkSec;
            DateTime startDateTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));

            this.CheckStartTimeStamp = (long)(DateTime.Now.AddSeconds((0 - this._checkSec)) - startDateTime).TotalSeconds;
            this.CurrentTimeStamp = (long)(DateTime.Now - startDateTime).TotalSeconds;
            this.CheckEndTimeStamp = (long)(DateTime.Now.AddSeconds((0 + this._checkSec)) - startDateTime).TotalSeconds;

        }

        static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }


        private static string HMACSHA256(string message, string key)
        {
            var encoding = new UTF8Encoding();
            byte[] keyByte = encoding.GetBytes(key);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacSHA256 = new HMACSHA256(keyByte))
            {
                byte[] hashMessage = hmacSHA256.ComputeHash(messageBytes);
                return BitConverter.ToString(hashMessage).Replace("-", "").ToLower();
            }
        }

        public bool CheckSign(string pageUrl, NameValueCollection queryString)
        {
            bool result = true;

            if (queryString["sign"] == null || queryString["sign"] == string.Empty)
            {
                result = false;
            }
            else
            {
                string innerSign = SignInfo(pageUrl, queryString);

                if (!innerSign.Equals(queryString["sign"]))
                {
                    result = false;
                }
            }

            return result;
        }

        public string SignInfo(string pageUrl, NameValueCollection queryString)
        {
            string time = (queryString["time"] == null) ? string.Empty : queryString["time"];

            string account = (queryString["account"] == null) ? string.Empty : queryString["account"];

            Dictionary<string, string> orgData = new Dictionary<string, string>();

            foreach (string eachKey in queryString.Keys)
            {
                if (!eachKey.Equals("sign"))
                {
                    orgData.Add(eachKey, queryString[eachKey]);
                }
               
            }

            Dictionary<string, string> dataAsc = orgData.OrderBy(o => o.Key).ToDictionary(o => o.Key, p => p.Value);

            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> k in dataAsc)
            {
                sb.Append(string.Format("&{0}={1}", k.Key, k.Value));
            }

            string sortString = sb.ToString().TrimStart('&');

            string urlBeforeSign = string.Format("{0}?{1}", pageUrl, sortString);

            string signKey = string.Format("{0}_{1}_{2}", account, this._appKey, time);

            //string countSign = HMACSHA256(urlBeforeSign, signKey);

            string countSign = ComputeSha256Hash(signKey);

            return countSign;
        }

        public string GetDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

    }
}

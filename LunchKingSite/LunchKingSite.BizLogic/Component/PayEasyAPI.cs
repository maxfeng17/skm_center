﻿using log4net;
using System;
using System.Net;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.IO;
using LunchKingSite.BizLogic.Model;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Component
{
    public class PayEasyAPI
    {

        static PayEasyAPI()
        {
            
        }

        static ILog logger = LogManager.GetLogger(typeof(PayEasyAPI));
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public static string ApiBaseUrl
        {
            get
            {
                return "https://eip.payeasy.com.tw/SCMWebService/";
            }
        }
 

        /// <summary>
        /// 取得pez token
        /// </summary>
        /// <param name="jsonData"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetPezToken(SCMWebServiceLogin para)
        {
            string token = "";
            try
            {
                string jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(para);
                byte[] dataBytes = Encoding.UTF8.GetBytes(jsonData);


                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(ApiBaseUrl + "auth/login");
                request.ContentType = "application/json";
                request.ContentLength = dataBytes.Length;
                request.Method = WebRequestMethods.Http.Post;

                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(dataBytes, 0, dataBytes.Length);
                    requestStream.Flush();
                }

                string result = "";
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                            SCMWebServiceResult pezResult = Newtonsoft.Json.JsonConvert.DeserializeObject<SCMWebServiceResult>(result);
                            if (pezResult.status == (int)HttpStatusCode.OK)
                            {
                                token = response.Headers["x-auth-token"].ToString();
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error("GetPezToken Error" + ex.Message + ex.StackTrace + "\r\n data:" + Newtonsoft.Json.JsonConvert.SerializeObject(para) + "\r\n token:" + token);
                token = "";
            }

            return token;
        }

        /// <summary>
        /// pez退貨
        /// </summary>
        /// <param name="jsonData"></param>
        /// <param name="url"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public static bool PezReturnGoods(SCMWebServiceReturnGoods para, string token)
        {
            bool returnResult = false;
            try
            {
                string jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(para);
                byte[] dataBytes = Encoding.UTF8.GetBytes(jsonData);


                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(ApiBaseUrl + "scmsupplier/returnGoods");
                request.ContentType = "application/json";
                request.Headers.Add("X-AUTH-TOKEN", token);
                request.ContentLength = dataBytes.Length;
                request.Method = WebRequestMethods.Http.Post;

                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(dataBytes, 0, dataBytes.Length);
                    requestStream.Flush();
                }

                string result = "";
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                            SCMWebServiceResult pezResult = Newtonsoft.Json.JsonConvert.DeserializeObject<SCMWebServiceResult>(result);
                            returnResult = pezResult.success;

                            logger.InfoFormat("PezReturnGoods: para:{0} response{1}", JsonConvert.SerializeObject(para), JsonConvert.SerializeObject(pezResult));
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error("PezReturnGoods Error" + ex.Message + ex.StackTrace + "\r\n data:" + Newtonsoft.Json.JsonConvert.SerializeObject(para) + "\r\n token:" + token);
                returnResult = false;
            }

            return returnResult;


        }

        /// <summary>
        /// 訂單查詢
        /// </summary>
        /// <param name="jsonData"></param>
        /// <param name="url"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public static SCMWebServiceSupplierResult GetSupplierByCondition(SCMWebServiceSupplier para, string token)
        {
            SCMWebServiceSupplierResult pezResult = null;
            try
            {
                string jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(para);
                byte[] dataBytes = Encoding.UTF8.GetBytes(jsonData);


                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(ApiBaseUrl + "scmsupplier/getSupplierByCondition");
                request.ContentType = "application/json";
                request.Headers.Add("X-AUTH-TOKEN", token);
                request.ContentLength = dataBytes.Length;
                request.Method = WebRequestMethods.Http.Post;

                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(dataBytes, 0, dataBytes.Length);
                    requestStream.Flush();
                }

                string result = "";
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                            pezResult = Newtonsoft.Json.JsonConvert.DeserializeObject<SCMWebServiceSupplierResult>(result);

                            logger.InfoFormat("GetSupplierByCondition: para:{0} response{1}", JsonConvert.SerializeObject(para), JsonConvert.SerializeObject(pezResult));
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error("GetSupplierByCondition Error \r\n " + ex.Message + ex.StackTrace + "\r\n data:" + Newtonsoft.Json.JsonConvert.SerializeObject(para) + "\r\n token:" + token);
            }

            return pezResult;

        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using System.Web;
using LunchKingSite.DataOrm;
using System.Xml.Linq;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Model;
using System.Xml;
using LunchKingSite.BizLogic.Facade;
using MasterCard.SDK;
using MasterCard.WalletSDK;
using MasterCard.WalletSDK.Models.All;
using MasterCard.WalletSDK.Models.Switch;
using Checkout = MasterCard.WalletSDK.Models.All.Checkout;
using PairingDataType = MasterCard.WalletSDK.Models.All.PairingDataType;
using PairingDataTypes = MasterCard.WalletSDK.Models.All.PairingDataTypes;
using PairingDataTypeType = MasterCard.WalletSDK.Models.All.PairingDataTypeType;

namespace LunchKingSite.BizLogic.Component
{
    public class MasterPass
    {
        private readonly IOrderProvider _op;
        private static readonly ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region property

        string ConsumerKey { get; set; }
        string CheckoutIdentifier { get; set; }

        string CertPath { get; set; }
        string CertPassword { get; set; }
        string PhysicalCertPath { get; set; }

        string CallbackDomain { get; set; }
        string CallbackPath { get; set; }

        string ApiPrefix { get; set; }

        string XmlVersion { get { return "v5"; } }

        //是否包括運送資料
        bool ShippingSuppression { get { return true; } }
        //預設MasterPass儲存配送地址區域
        string ShippingProfile { get { return string.Empty; } }
        //是否有3ds認證
        bool Auth_Level_Basic { get { return false; } }
        //是否有信用卡紅利相關程式
        bool RewardsProgram { get { return false; } }
        string AcceptedCards { get { return "master,visa,jcb"; } }
        private List<string> PairingDataTypeList = new List<string> { "CARD", "PROFILE" };

        string RequestUrl { get; set; }
        string AccessUrl { get; set; }
        string PostbackUrl { get; set; }
        string ShoppingCartUrl { get; set; }
        string MerchantInitUrl { set; get; }
        string PreCheckoutUrl { set; get; }
        string OriginUrl { set; get; }
        MasterPassService service;

        #endregion
        
        public MasterPass()
        {
            var config = ProviderFactory.Instance().GetConfig();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

            ConsumerKey = config.ConsumerKey;
            CheckoutIdentifier = config.CheckoutIdentifier;

            CertPath = config.CertPath;
            PhysicalCertPath = HttpContext.Current.Server.MapPath(CertPath);
            CertPassword = config.CertPassword;

            CallbackDomain = config.SSLSiteUrl;
            CallbackPath = config.CallbackPath;            
            OriginUrl = HttpContext.Current.Request.Url.ToString();
            if (HttpContext.Current.Request.Url.Query.Length > 0)
            {
                OriginUrl = OriginUrl.Replace(HttpContext.Current.Request.Url.Query, string.Empty);
            }
            ApiPrefix = config.ApiPrefix;

            RequestUrl = string.Format("https://{0}api.mastercard.com/oauth/consumer/v1/request_token", ApiPrefix);
            AccessUrl = string.Format("https://{0}api.mastercard.com/oauth/consumer/v1/access_token", ApiPrefix);
            ShoppingCartUrl = string.Format("https://{0}api.mastercard.com/masterpass/v6/shopping-cart", ApiPrefix);
            PostbackUrl = string.Format("https://{0}api.mastercard.com/masterpass/v6/transaction", ApiPrefix);
            MerchantInitUrl = string.Format("https://{0}api.mastercard.com/masterpass/v6/merchant-initialization", ApiPrefix);
            PreCheckoutUrl = string.Format("https://{0}api.mastercard.com/masterpass/v6/precheckout", ApiPrefix);
            service = MasterPassService.FactoryMethod(ConsumerKey, PhysicalCertPath, CertPassword, OriginUrl, true);
        }

        public MasterPassData GetRequestToken(int subtotal)
        {
            return GetMasterPassData(subtotal, string.Empty, true);
        }

        public MasterPassData GetRequestToken(int subtotal, string ticketid)
        {
            return GetMasterPassData(subtotal, ticketid, false);
        }

        /// <summary>
        /// only binding
        /// </summary>
        /// <returns></returns>
        public MasterPassData GetRequestTokenByBinding()
        {
            MasterPassData data = new MasterPassData();
            data.OriginUrl = OriginUrl;
            data.CallbackUrl = string.Format("{0}/mvc/MasterPass/BindingCallBack", CallbackDomain);
            data.AcceptedCards = AcceptedCards;
            data.AuthLevelBasic = Auth_Level_Basic;
            data.RewardsProgram = RewardsProgram;
            data.ShippingSuppression = ShippingSuppression;

            #region RequestToken

            var requestTokenResponse = service.GetRequestToken(RequestUrl, data.CallbackUrl);
            data.RequestToken = requestTokenResponse.RequestToken;

            #endregion RequestToken

            #region Pairing Token

            var pairingTokenResponse = service.GetRequestToken(RequestUrl, data.CallbackUrl);
            data.PairingToken = pairingTokenResponse.RequestToken;
            data.PairingDataTypes = PairingDataTypeList;

            #endregion Pairing Token

            #region MerchantInit

            MerchantInitializationResponse merchantInitResponse;
            MerchantInitializationRequest merchantInitRequest;

            merchantInitRequest = ParseMerchantInitFile(data.PairingToken, data.OriginUrl);
            merchantInitResponse = service.PostMerchantInitData(MerchantInitUrl, merchantInitRequest);
            data.MerchantInitRequest = Serializer<MerchantInitializationRequest>.Serialize(merchantInitRequest).OuterXml;
            data.MerchantInitResponse = Serializer<MerchantInitializationResponse>.Serialize(merchantInitResponse).OuterXml;
            data.CheckoutIdentifier = CheckoutIdentifier;

            #endregion MerchantInit

            data.IsLoaded = true;
            return data;
        }

        /// <summary>
        /// Get GetMasterPassData
        /// </summary>
        /// <param name="subtotal"></param>
        /// <param name="ticketid"></param>
        /// <param name="isApp"></param>
        /// <returns></returns>
        private MasterPassData GetMasterPassData(int subtotal, string ticketid, bool isApp)
        {
            MasterPassData data = new MasterPassData();
            data.OriginUrl = OriginUrl;
            data.CallbackUrl = isApp ? string.Format("{0}/mvc/MasterPass/CheckoutCallBack", CallbackDomain) :
                string.Format("{0}{1}?TicketId={2}", CallbackDomain, CallbackPath, ticketid);
            data.AcceptedCards = AcceptedCards;
            data.AuthLevelBasic = Auth_Level_Basic;
            data.RewardsProgram = RewardsProgram;
            data.ShippingSuppression = ShippingSuppression;

            #region RequestToken

            var requestTokenResponse = service.GetRequestToken(RequestUrl, data.CallbackUrl);
            data.RequestToken = requestTokenResponse.RequestToken;

            #endregion RequestToken

            #region Pairing Token

            var pairingTokenResponse = service.GetRequestToken(RequestUrl, data.CallbackUrl);
            data.PairingToken = pairingTokenResponse.RequestToken;
            data.PairingDataTypes = PairingDataTypeList;

            #endregion Pairing Token

            #region MerchantInit

            MerchantInitializationResponse merchantInitResponse;
            MerchantInitializationRequest merchantInitRequest;

            merchantInitRequest = ParseMerchantInitFile(data.PairingToken, data.OriginUrl);
            merchantInitResponse = service.PostMerchantInitData(MerchantInitUrl, merchantInitRequest);
            data.MerchantInitRequest = Serializer<MerchantInitializationRequest>.Serialize(merchantInitRequest).OuterXml;
            data.MerchantInitResponse = Serializer<MerchantInitializationResponse>.Serialize(merchantInitResponse).OuterXml;
            data.CheckoutIdentifier = CheckoutIdentifier;

            #endregion MerchantInit

            #region ShoppingCartRequest

            ShoppingCartRequest shoppingCartRequest = Serializer<ShoppingCartRequest>.Deserialize(ShoppingCartXml(subtotal));
            shoppingCartRequest.OAuthToken = data.RequestToken;
            shoppingCartRequest.OriginUrl = data.OriginUrl;
            foreach (var item in shoppingCartRequest.ShoppingCart.ShoppingCartItem)
            {
                item.Description = TrimDescription(item.Description);
            }
            ShoppingCartResponse shoppingCartResponse = service.PostShoppingCartData(ShoppingCartUrl, shoppingCartRequest);
            data.ShoppingCartResponse = XElement.Parse(Serializer<ShoppingCartResponse>.Serialize(shoppingCartResponse).OuterXml).ToString();

            #endregion ShoppingCartRequest
            data.IsLoaded = true;
            return data;
        }

        public void GetPairingToken(string pairingToken, string pairingVerifier, int userId)
        {
            var longAccessTokenResponse = service.GetAccessToken(AccessUrl, pairingToken, pairingVerifier);
            var longToken = _op.MasterPassPreCheckoutTokenGet(userId);
            longToken.UserId = userId;
            longToken.AccessToken = longAccessTokenResponse.AccessToken;
            longToken.OauthSecret = longAccessTokenResponse.OAuthSecret;
            longToken.IsUsed = false;
            _op.MasterPassPreCheckoutTokenSet(longToken);
        }

        public CreditCardInfo MasterPassCardInfo(string oauth_token, string oauth_verifier, string checkout_resource_url)
        {
            if (config.IsMasterPassLocalTest && oauth_token == "test")
            {
                return new CreditCardInfo
                {
                    CardBrandId = "HiTrust",
                    CardBrandName = "HiTrust",
                    CardHolder = "測試者",
                    CardNumber = "8888880000000001",
                    ExpiryMonth = "12",
                    ExpiryYear = "2018",
                    MasterPassTransId = "test",
                    OauthToken = "test",
                    PayPassWalletIndicator = "test"
                };
            }

            var accessTokenResponse = service.GetAccessToken(AccessUrl, oauth_token, oauth_verifier);
            Checkout checkoutResult = service.GetPaymentShippingResource(checkout_resource_url, accessTokenResponse.AccessToken);

            CreditCardInfo cardInfo = new CreditCardInfo()
            {
                CardNumber = checkoutResult.Card.AccountNumber,
                ExpiryMonth = checkoutResult.Card.ExpiryMonth.ToString(),
                ExpiryYear = checkoutResult.Card.ExpiryYear.ToString(),
                MasterPassTransId = checkoutResult.TransactionId,
                PayPassWalletIndicator = checkoutResult.WalletID,
                OauthToken = oauth_token,
                CardBrandId = checkoutResult.Card.BrandId,
                CardBrandName = checkoutResult.Card.BrandName,
                CardHolder = checkoutResult.Card.CardHolderName
            };
            return cardInfo;
        }

        public MasterPassPreCheckoutData PreCheckout(MasterpassPreCheckoutToken token, int userId)
        {
            var result = new MasterPassPreCheckoutData
            {
                IsCheckoutPass = false
            };

            try
            {
                DateTime requestStartTime = DateTime.Now;
                PrecheckoutDataRequest request = GetPrecheckoutDataRequest(PairingDataTypeList);
                PrecheckoutDataResponse response = service.GetPrecheckoutData(PreCheckoutUrl, token.AccessToken, request);
                DateTime requestEndTime = DateTime.Now;
                TimeSpan ts = new TimeSpan(requestEndTime.Ticks - requestStartTime.Ticks);
                
                //更新 token 已用過
                _op.MasterPassPreCheckoutTokenUpdateUsed(userId);
                //更新新的 LongAccessToken
                _op.MasterPassPreCheckoutTokenUpdateAccessToken(token.UserId, response.LongAccessToken, ts.Seconds);

                result.MasterpassLogoUrl = response.MasterpassLogoUrl;
                result.WalletPartnerLogoUrl = response.WalletPartnerLogoUrl;
                result.ConsumerWalletId = response.PrecheckoutData.ConsumerWalletId;
                result.PrecheckoutTransactionId = response.PrecheckoutData.PrecheckoutTransactionId;
                result.WalletName = response.PrecheckoutData.WalletName;
                result.Cards = new List<MasterPassCard>();

                var accpetedCardsList = AcceptedCards.Split(",").ToList();
                foreach (var card in response.PrecheckoutData.Cards)
                {
                    if (!accpetedCardsList.Contains(card.BrandId))
                    {
                        continue;
                    }

                    result.Cards.Add(new MasterPassCard
                    {
                        BrandName = card.BrandName,
                        CardHolderName = card.CardHolderName,
                        CardAlias = card.CardAlias,
                        CardId = card.CardId,
                        LastFour = card.LastFour,
                        SelectedAsDefault = card.SelectedAsDefault,
                        ExpiryMonth = card.ExpiryMonth,
                        ExpiryYear = card.ExpiryYear
                    });
                }

                if (result.Cards.Any())
                {
                    result.IsCheckoutPass = true;
                }
                else
                {
                    //若無卡片資料等於綁定失敗
                    result.CheckoutFailReason = "無卡片資料";
                    _op.MasterPassPreCheckoutTokenUpdateUsed(userId);
                }
                
            }
            catch (Exception ex)
            {
                result.CheckoutFailReason = ex.Message;
                _op.MasterPassPreCheckoutTokenUpdateFailReason(userId, ex.Message);
            }

            return result;
        }

        public void MasterPassTransLog(string masterpass_transid, string masterpass_indicator, int subtotal
            , string return_code, int resultcode, Guid orderGuid, int userid, string oauthToken
            , string cardBrandId, string cardBrandName, string cardHolder, string cardNumber)
        {
            MerchantTransactions merchantTransactions = new MerchantTransactions();
            merchantTransactions.MerchantTransactions1 = new List<MerchantTransaction>() { 
            new MerchantTransaction(){
             TransactionId = masterpass_transid,
                ConsumerKey = ConsumerKey,
                Currency = "TWD",
                OrderAmount = (long)subtotal*100,
                PurchaseDate = DateTime.Now,
             TransactionStatus =(resultcode==0? TransactionStatus.Success:TransactionStatus.Failure),
                ApprovalCode = string.IsNullOrEmpty(return_code)?"000000":return_code.PadLeft(6,'0')
            }
            };
            var result = service.PostCheckoutTransaction(PostbackUrl, merchantTransactions);
            XmlDocument responseXML = Serializer<MerchantTransactions>.Serialize(result);
            _op.MasterPassAdd(new MasterpassLog()
            {
                OrderGuid = orderGuid,
                Indicator = masterpass_indicator,
                ResponseContent = responseXML.OuterXml,
                CreateTime = DateTime.Now,
                Uniqueid = userid,
                MasterpassTransid = masterpass_transid,
                Subtotal = subtotal,
                OauthToken = oauthToken,
                CardBrandId = cardBrandId,
                CardBrandName = cardBrandName,
                CardHolder = cardHolder,
                AccountNumber = MemberFacade.EncryptCreditCardInfo(cardNumber, orderGuid.ToString().Substring(0, 16)),
                InfoNumber = cardNumber.Length > 5 ? cardNumber.Substring(0, 6) : string.Empty
            });
        }

        public void MasterPassTransErrorLog(Guid order_guid, int userid, string errormessage)
        {
            _op.MasterPassAdd(new MasterpassLog() { OrderGuid = order_guid, Indicator = "error", ResponseContent = errormessage, CreateTime = DateTime.Now, Uniqueid = userid });
        }

        public MasterpassCardInfoLog GetCardInfoLog(CreditCardInfo info, int memberUniqueId)
        {
            Guid newGuid = Guid.NewGuid();

            var plainTextBytes = Encoding.UTF8.GetBytes(newGuid.ToString());
            var token = Convert.ToBase64String(plainTextBytes);
            var cardInfoLog = new MasterpassCardInfoLog
            {
                AuthToken = token,
                CardNumber = MemberFacade.EncryptCreditCardInfo(info.CardNumber, token.Substring(0, 16)),
                ExpiryMonth = info.ExpiryMonth,
                ExpiryYear = info.ExpiryYear,
                TransId = info.MasterPassTransId,
                Indicator = info.PayPassWalletIndicator,
                CardBrandId = info.CardBrandId,
                CardBrandName = info.CardBrandName,
                CardHolder = info.CardHolder,
                OauthToken = info.OauthToken,
                CreateUserId = memberUniqueId,
                IsUsed = false
            };
            _op.MasterpassCardInfoLogNew(cardInfoLog);
            return cardInfoLog;
        }

        #region private method

        private PrecheckoutDataRequest GetPrecheckoutDataRequest(List<string> pairingTypes)
        {
            PrecheckoutDataRequest preCheckoutDataRequest = new PrecheckoutDataRequest();
            PairingDataTypes types = new PairingDataTypes();
            foreach (string pairingDataType in pairingTypes)
            {
                PairingDataType type = new PairingDataType();
                if (pairingDataType.Equals("CARD"))
                {
                    type.Type = PairingDataTypeType.CARD;
                }
                else if (pairingDataType.Equals("PROFILE"))
                {
                    type.Type = PairingDataTypeType.PROFILE;
                }
                else if (pairingDataType.Equals("ADDRESS"))
                {
                    type.Type = PairingDataTypeType.ADDRESS;
                }
                else if (pairingDataType.Equals("REWARD_PROGRAM"))
                {
                    type.Type = PairingDataTypeType.REWARD_PROGRAM;
                }

                types.PairingDataType.Add(type);
            }
            preCheckoutDataRequest.PairingDataTypes = types.PairingDataType;
            return preCheckoutDataRequest;
        }

        private string ShoppingCartXml(ShoppingCartCollection items, int subtotal)
        {
            XDocument doc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));
            XElement xitem = new XElement("ShoppingCart", new XElement("CurrencyCode", "TWD"), new XElement("Subtotal", subtotal * 100));

            foreach (var item in items)
            {
                xitem.Add(new XElement("ShoppingCartItem",
                    new XElement("Description", item.ItemName),
                    new XElement("Quantity", item.ItemQuantity),
                    new XElement("Value", ((int)(item.ItemUnitPrice)) * 100),
                    new XElement("ImageURL")));
            }

            doc.Add(
                new XElement("ShoppingCartRequest",
                new XElement("OAuthToken"),
                    xitem));
            return doc.ToString();
        }

        private string ShoppingCartXml(int subtotal)
        {
            XDocument doc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));
            XElement xitem = new XElement("ShoppingCart", new XElement("CurrencyCode", "TWD"), new XElement("Subtotal", subtotal * 100));

            //xitem.Add(new XElement("ShoppingCartItem",
            //    new XElement("Description", "17Life Item"),
            //    new XElement("Quantity", 1),
            //    new XElement("Value", subtotal * 100),
            //    new XElement("ImageURL")));

            doc.Add(
                new XElement("ShoppingCartRequest",
                new XElement("OAuthToken"),
                    xitem));
            return doc.ToString();
        }

        private string TrimDescription(string str)
        {
            if (str.Length >= 95)
            {
                string str1 = str.Substring(0, 95);
                string str2 = str.Substring(95, str.Length - 95);
                str2 = str2.Replace("&amp;", "").Replace("&", "");
                str = str1 + str2;
                str = str.Substring(0, 100);
            }
            return str;
        }

        private MerchantInitializationRequest ParseMerchantInitFile(string pairingToken, string originUrl)
        {
            MerchantInitializationRequest request = new MerchantInitializationRequest();
            request.OAuthToken = pairingToken;
            request.OriginUrl = originUrl;
            return request;
        }

        #endregion private method
    }
}

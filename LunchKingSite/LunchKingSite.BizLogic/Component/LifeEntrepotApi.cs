﻿using log4net;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.Channel;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;

namespace LunchKingSite.BizLogic.Component
{
    public class LifeEntrepotApi
    {
        private static ISysConfProvider _config;
        private static ILog logger = LogManager.GetLogger("LifeEntrepotApi");

        static LifeEntrepotApi()
        {
            _config = ProviderFactory.Instance().GetConfig();
        }

        public static ApiResult VerifyNotifity(int coupontId, string seq, string code, NotifyVoucherStatus status, Guid? verifiedStoreGuid, string verifiedStoreId) 
        {
            var model = new EntrepotVerifyModel 
            { 
                CouponId = coupontId,
                SequenceNumber = seq,
                Code = code,
                VoucherStatus = (int)status,
                VerifiedStoreGuid = verifiedStoreGuid == null ? Guid.Empty.ToString() : verifiedStoreGuid.ToString(),
                VerifiedStoreId = verifiedStoreId
            };
            logger.Info("[VerifyNotifity][model]" + new JsonSerializer().Serialize(model));
            string url = string.Format("{0}/{1}", _config.LifeEntrepotSiteUrl, "api/Suppiler/verify");
            logger.Info("[VerifyNotifity]" + url);
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = WebRequestMethods.Http.Post;
            var jsonText = new JsonSerializer().Serialize(model);
            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;

            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                requestStream.Flush();
            }
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return new ApiResult
                        {
                            Code = ApiResultCode.Error,
                            Message = "no response."
                        };
                    }

                    using (var reader = new StreamReader(stream))
                    {
                        var result = reader.ReadToEnd();
                        logger.Info(new JsonSerializer().Deserialize<ApiResult>(result));
                        return new JsonSerializer().Deserialize<ApiResult>(result);
                    }
                }
            }
        }

        public static ApiResult CustomerServiceReply(string serviceNo, string content, string imageUrl, string createId, DateTime createTime)
        {
            var model = new EntrepotCustomerServiceReplyModel
            {
                ServiceNo = serviceNo,
                Content = content,
                ImageUrl = imageUrl,
                CreateId = createId,
                ReplyTime = createTime
            };

            var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}/{1}", _config.LifeEntrepotSiteUrl, "api/Suppiler/CustomerServiceReply"));
            request.Method = WebRequestMethods.Http.Post;
            var jsonText = new JsonSerializer().Serialize(model);
            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;

            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                requestStream.Flush();
            }
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return new ApiResult
                        {
                            Code = ApiResultCode.Error,
                            Message = "no response."
                        };
                    }

                    using (var reader = new StreamReader(stream))
                    {
                        var result = reader.ReadToEnd();
                        return new JsonSerializer().Deserialize<ApiResult>(result);
                    }
                }
            }
        }

        public static ApiResult CustomerServiceStatus(string serviceNo, int? subCategory, int status, string createId)
        {
            var model = new EntrepotCustomerServiceStatusModel
            {
                ServiceNo = serviceNo,
                SubCategory = subCategory,
                Status = status,
                CreateId = createId
            };

            var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}/{1}", _config.LifeEntrepotSiteUrl, "api/Suppiler/CustomerServiceStatus"));
            request.Method = WebRequestMethods.Http.Post;
            var jsonText = new JsonSerializer().Serialize(model);
            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;

            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                requestStream.Flush();
            }
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return new ApiResult
                        {
                            Code = ApiResultCode.Error,
                            Message = "no response."
                        };
                    }

                    using (var reader = new StreamReader(stream))
                    {
                        var result = reader.ReadToEnd();
                        return new JsonSerializer().Deserialize<ApiResult>(result);
                    }
                }
            }
        }
    }
}

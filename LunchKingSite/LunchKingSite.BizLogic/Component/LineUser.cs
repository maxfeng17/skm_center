﻿using System;
using System.Data.SqlTypes;
using System.IO;
using System.Net;
using log4net;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class LineUser : ExternalUser
    {
        public const string DefaultScope = "profile";

        private static ILog logger = LogManager.GetLogger(typeof(LineUser));

        public static HttpWebRequest CreateRequest(string url)
        {
            return WebRequest.Create(url) as HttpWebRequest;
        }

        public static LineUser Get(string token)
        {
            LineUser lineUser = null;
            if (!string.IsNullOrWhiteSpace(token))
            {
                string url = "https://api.line.me/v2/profile";

                HttpWebRequest request = CreateRequest(url);
                request.Method = "GET";
                request.Headers.Add("Authorization", "Bearer " + token);

                try
                {
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        StreamReader reader = new StreamReader(response.GetResponseStream());

                        dynamic user = JObject.Parse(reader.ReadToEnd());
                        lineUser = new LineUser();
                        lineUser.Id = user.userId;
                        lineUser.Email = user.email;
                        lineUser.FirstName = user.displayName;
                       
                        if (user.pictureUrl != null)
                        {
                            lineUser.Pic = (string)user.pictureUrl;
                        }
                       
                    }
                }
                catch (Exception ex)
                {
                    logger.Info("get line error. url:" + url, ex);
                }
            }

            return lineUser;
        }
    }
}

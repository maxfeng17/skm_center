﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Web;

namespace LunchKingSite.BizLogic.Component
{
    public class PponExpandEndTime
    {
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(PponExpandEndTime));
        private static PponDealClose _dealClose = new PponDealClose();

        #region 延長檔次
        public bool Check(Guid bid, DateTime newEndTime, bool subDealCheck, out string errorMessage)
        {
            GroupOrder gpo = op.GroupOrderGetByBid(bid);
            BusinessHour bh = pp.BusinessHourGet(bid);
            DealProperty dp = pp.DealPropertyGet(bid);
            DealAccounting da = pp.DealAccountingGet(bid);
            DealTimeSlotCollection dts = PponFacade.GetDealTimeSlotList(bid);
            return Check(bh, gpo, dp, da, dts, newEndTime, subDealCheck, out errorMessage);
        }

        /// <summary>
        /// 延長檔次check
        /// </summary>
        /// <param name="bh"></param>
        /// <param name="gpo"></param>
        /// <param name="dp"></param>
        /// <param name="da"></param>
        /// <param name="dts"></param>
        /// <param name="newEndTime"></param>
        /// <param name="subDealCheck"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private bool Check(BusinessHour bh, GroupOrder gpo, DealProperty dp, DealAccounting da,
            DealTimeSlotCollection dts, DateTime newEndTime, bool subDealCheck, out string errorMessage)
        {
            errorMessage = string.Empty;

            if (gpo.IsLoaded == false || bh.IsLoaded == false || dp.IsLoaded == false || da.IsLoaded == false)
            {
                errorMessage = "資料不完整(1)";
                return false;
            }
            if (bh.BusinessHourOrderTimeS > DateTime.Now)
            {
                errorMessage = "還沒開檔不能用此功能延長";
                return false;
            }
            if (dp.DeliveryType == (int)DeliveryType.ToShop)
            {
                errorMessage = "憑證檔次不處理";
                return false;
            }

            if (subDealCheck && Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
            {
                errorMessage = "子檔不處理";
                return false;
            }

            //母檔要檢查 DealTimeSlot
            if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealSub) == false && dts.Count == 0)
            {
                errorMessage = "資料不完整(dts)";
                return false;
            }

            if ((DateTime.Now - bh.BusinessHourOrderTimeE).TotalDays > 3)
            {
                errorMessage = "超過結檔後可復原的期限";
                return false;
            }

            if (bh.BusinessHourOrderTimeE >= newEndTime)
            {
                errorMessage = "只能延長期限";
                return false;
            }

            if (dp.ShipType == (int)DealShipType.Other && newEndTime > bh.BusinessHourDeliverTimeE)
            {
                errorMessage = "延長的日期不得大於等於[最晚出貨日]";
                return false;
            }

            DealShipType shipType = (DealShipType)dp.ShipType.GetValueOrDefault();
            switch (shipType)
            {
                case DealShipType.Normal:
                    if (dp.ShippingdateType == (int)DealShippingDateType.Special)
                    {
                        if (dp.ProductUseDateEndSet == null)
                        {
                            errorMessage = "資料不完整(6)";
                            return false;
                        }
                    }
                    else if (dp.ShippingdateType == (int)DealShippingDateType.Normal)
                    {
                        if (dp.Shippingdate == null)
                        {
                            errorMessage = "資料不完整(7)";
                            return false;
                        }
                    }
                    else
                    {
                        errorMessage = "不明錯誤";
                        return false;
                    }
                    break;
                case DealShipType.LastShipment:
                case DealShipType.LastDelivery:
                    errorMessage = "出貨設定為" + shipType + ", 不允許調整";
                    return false;
            }

            //如果是母檔，同步子檔
            if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
            {
                var subDealBids = pp.GetComboDealByBid(bh.Guid, false).Select(t => t.BusinessHourGuid);
                string subErrorMessage;
                foreach (var subDealBid in subDealBids)
                {
                    if (Check(subDealBid, newEndTime, false, out subErrorMessage) == false)
                    {
                        errorMessage += string.Format("子檔 {0} {1}", subDealBid, subErrorMessage);
                        return false;
                    }
                }
            }

            return true;
        }

        public bool Execute(Guid bid, DateTime newEndTime, string userName, StringBuilder sbResult, out string errorMessage, bool subDealCheck = true)
        {
            GroupOrder gpo = op.GroupOrderGetByBid(bid);
            BusinessHour bh = pp.BusinessHourGet(bid);
            DealProperty dp = pp.DealPropertyGet(bid);
            DealAccounting da = pp.DealAccountingGet(bid);
            DealTimeSlotCollection dts = PponFacade.GetDealTimeSlotList(bid);
            VacationCollection recentHolidays = pp.GetVacationListByPeriod(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(14));

            if (Check(bh, gpo, dp, da, dts, newEndTime, subDealCheck, out errorMessage) == false)
            {
                return false;
            }

            if (!dp.IsExpiredDeal)
            {
                //調整BusinessHourOrderTimeE, BusinessHourDeliverTimeE
                DateTime oldEndTime = bh.BusinessHourOrderTimeE;
                bh.BusinessHourOrderTimeE = newEndTime;
                DealShipType shipType = (DealShipType)dp.ShipType.GetValueOrDefault();
                switch (shipType)
                {
                    case DealShipType.Normal:
                        // 一般出貨
                        //bh.BusinessHourDeliverTimeE = newEndTime.AddDays(7);
                        //統一出貨日
                        if (dp.ShippingdateType == (int)DealShippingDateType.Special)
                        {
                            bh.BusinessHourDeliverTimeE = IncrDaysExceptVocations(newEndTime, dp.ProductUseDateEndSet.GetValueOrDefault(), recentHolidays);
                            //bh.BusinessHourDeliverTimeE = newEndTime.AddDays(dp.ProductUseDateEndSet.GetValueOrDefault());
                        }
                        else if (dp.ShippingdateType == (int)DealShippingDateType.Normal)
                        {
                            bh.BusinessHourDeliverTimeE = IncrDaysExceptVocations(newEndTime, dp.Shippingdate.GetValueOrDefault(), recentHolidays);
                            //bh.BusinessHourDeliverTimeE = newEndTime.AddDays(dp.Shippingdate.GetValueOrDefault());
                        }
                        break;
                    case DealShipType.Ship72Hrs:
                        //bh.BusinessHourDeliverTimeE = newEndTime.AddDays(1);
                        bh.BusinessHourDeliverTimeE = IncrDaysExceptVocations(newEndTime, 1, recentHolidays);
                        //24小時出貨
                        break;
                }

                //開始進行延長
                bool isDealRecovered = false;
                if (Helper.IsFlagSet(gpo.Status.GetValueOrDefault(), GroupOrderStatus.Completed)
                    || gpo.Slug != null)
                {
                    isDealRecovered = true;
                    //己結檔，復原結檔
                    da.FinalBalanceSheetDate = null;
                    da.ShippedDate = null;
                    gpo.Status = (int)Helper.SetFlag(false, gpo.Status.GetValueOrDefault(), (int)GroupOrderStatus.Completed);
                    gpo.Status = (int)Helper.SetFlag(false, gpo.Status.GetValueOrDefault(), (int)GroupOrderStatus.PponProcessed);
                    gpo.Slug = null;
                }

                DealTimeSlot origEndDts;
                DealTimeSlotCollection incrEndDts = GenerateExpandDealTimeSlots(bh, dts, newEndTime, out origEndDts);

                string auditBhMsg = string.Format("延長檔次時間 {0:yyyy/MM/dd HH:mm:ss}=>{1::yyyy/MM/dd HH:mm:ss}",
                    oldEndTime, newEndTime);

                Proposal pro = sp.ProposalGet(bh.Guid);

                try
                {
                    StringBuilder sbLog = new StringBuilder();
                    using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                    {
                        pp.BusinessHourSet(bh);

                        if (pro.IsLoaded)
                        {
                            pro.OrderTimeE = bh.BusinessHourOrderTimeE;
                            sp.ProposalSet(pro);
                            ProposalFacade.ProposalLog(pro.Id, "[後台延長檔次結檔時間]" + string.Format("{0}~{1}", pro.OrderTimeS, pro.OrderTimeE), userName);
                        }
                        CommonFacade.AddAudit(bh.Guid, AuditType.GroupOrder, auditBhMsg, userName, true);
                        if (isDealRecovered)
                        {
                            op.GroupOrderSet(gpo);
                            pp.DealAccountingSet(da);
                            string auditDaMsg = "清空DealAccounting 出貨回覆日,退換貨完成日";
                            CommonFacade.AddAudit(da.BusinessHourGuid, AuditType.DealAccounting, auditDaMsg, userName, true);
                        }
                        //非子檔才來動 deal_time_slog
                        if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealSub) == false)
                        {
                            if (origEndDts.IsLoaded)
                            {
                                pp.DealTimeSlotSet(origEndDts);
                            }
                            DB.BulkInsert(incrEndDts);
                            sbLog.AppendLine("M:" + bh.Guid.ToString());
                        }

                        //如果是母檔，同步子檔
                        if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                        {
                            var subDealBids = pp.GetComboDealByBid(bh.Guid, false).Select(t => t.BusinessHourGuid);
                            string subErrorMessage;
                            foreach (Guid subDealBid in subDealBids)
                            {
                                if (Execute(subDealBid, newEndTime, userName, sbResult, out subErrorMessage, subDealCheck: false) == false)
                                {
                                    errorMessage += string.Format("子檔 {0} {1}", subDealBid, subErrorMessage);
                                    logger.WarnFormat("批次延長結檔時間 發生錯誤，{0}", errorMessage);
                                    return false;
                                }
                                sbResult.AppendLine("S:" + bh.Guid.ToString());
                            }
                        }
                        tx.Complete();
                    }
                    sbResult.Append(sbLog.ToString());
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    errorMessage = "失敗，資料庫問題請洽IT";
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 從baseTime增加incrDays天，中間如碰到週末與假日，則遞延
        /// </summary>
        /// <param name="baseTime"></param>
        /// <param name="incrDays"></param>
        /// <param name="recentHolidays"></param>
        /// <returns></returns>
        public static DateTime IncrDaysExceptVocations(DateTime baseTime, int incrDays, VacationCollection recentHolidays)
        {
            if (incrDays < 0)
            {
                throw new ArgumentException("incrDays不能小於或等於0");
            }
            HashSet<DateTime> holidays = new HashSet<DateTime>(
                recentHolidays.Select(t => t.Holiday).Distinct().ToArray());
            DateTime baseDate = baseTime.Date;

            int workingDays = 0;
            int days = 0;
            do
            {
                days++;
                DateTime d = baseDate.AddDays(days);
                if (d.DayOfWeek != DayOfWeek.Saturday && d.DayOfWeek != DayOfWeek.Sunday && holidays.Contains(d) == false)
                {
                    workingDays++;
                }
            } while (workingDays < incrDays);
            return baseDate.AddDays(days);
        }

        private DealTimeSlotCollection GenerateExpandDealTimeSlots(
            BusinessHour bh, DealTimeSlotCollection dts, DateTime newEndTime, out DealTimeSlot origEndDts)
        {
            Guid bid = bh.Guid;
            DealTimeSlotCollection result = new DealTimeSlotCollection();
            if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
            {
                origEndDts = new DealTimeSlot();
                return result;
            }
            List<int> cityIds = dts.Select(t => t.CityId).Distinct().ToList();
            origEndDts = dts.OrderByDescending(t => t.EffectiveStart).First();


            DateTime newEffectStart = origEndDts.EffectiveStart;
            bool done = false;

            //1.處理接續的第1筆
            if ((newEndTime - newEffectStart).TotalDays < 1)
            {
                //延長不超過1天
                origEndDts.EffectiveEnd = origEndDts.EffectiveStart.Add(newEndTime - newEffectStart);
                return result;
            }
            origEndDts.EffectiveEnd = origEndDts.EffectiveStart.AddDays(1);

            //2.處理接續中間的部份與的最後1筆
            do
            {
                newEffectStart = newEffectStart.AddDays(1);
                int lastIncrHours = (int)(newEndTime - newEffectStart).TotalHours;
                DateTime newEffectEnd;
                if (lastIncrHours < 24)
                {
                    newEffectEnd = newEffectStart.Add(newEndTime - newEffectStart);
                    done = true;
                }
                else
                {
                    newEffectEnd = newEffectStart.AddDays(1);
                }
                DealTimeSlotStatus timeSlotStatus = DealTimeSlotStatus.Default;
                foreach (int cityId in cityIds)
                {
                    if (timeSlotStatus == DealTimeSlotStatus.Default)
                    {
                        var prev = dts.Where(x => x.CityId == cityId).OrderBy(y => y.EffectiveEnd).LastOrDefault();
                        if (prev != null)
                        {
                            if (prev.Status == (int)DealTimeSlotStatus.NotShowInPponDefault)
                            {
                                timeSlotStatus = DealTimeSlotStatus.NotShowInPponDefault;
                            }
                        }
                    }

                    result.Add(new DealTimeSlot
                    {
                        BusinessHourGuid = bid,
                        CityId = cityId,
                        EffectiveStart = newEffectStart,
                        EffectiveEnd = newEffectEnd,
                        Sequence = 999999,
                        IsLockSeq = false,
                        IsInTurn = false,
                        IsHotDeal = false,
                        Status = (int)timeSlotStatus
                    });
                }
            } while (done == false);

            return result;
        }
        #endregion

        #region 批次結檔
        public bool CheckClose(Guid bid, bool subDealCheck, out string errorMessage)
        {
            GroupOrder gpo = op.GroupOrderGetByBid(bid);
            BusinessHour bh = pp.BusinessHourGet(bid);
            DealProperty dp = pp.DealPropertyGet(bid);
            DealAccounting da = pp.DealAccountingGet(bid);
            DealTimeSlotCollection dts = PponFacade.GetDealTimeSlotList(bid);

            return CheckClose(bh, gpo, dp, da, dts, subDealCheck, out errorMessage);


        }

        /// <summary>
        /// 批次結檔檢核check
        /// </summary>
        /// <param name="bh"></param>
        /// <param name="gpo"></param>
        /// <param name="dp"></param>
        /// <param name="da"></param>
        /// <param name="dts"></param>
        /// <param name="newEndTime"></param>
        /// <param name="subDealCheck"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private bool CheckClose(BusinessHour bh, GroupOrder gpo, DealProperty dp, DealAccounting da,
            DealTimeSlotCollection dts, bool subDealCheck, out string errorMessage)
        {
            errorMessage = string.Empty;

            if (gpo.IsLoaded == false || bh.IsLoaded == false || dp.IsLoaded == false || da.IsLoaded == false)
            {
                errorMessage = "資料不完整(1)";
                return false;
            }
            if (bh.BusinessHourOrderTimeS > DateTime.Now)
            {
                errorMessage = "還沒開檔不能用此功能結檔";
                return false;
            }

            if (subDealCheck && Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
            {
                errorMessage = "子檔不處理";
                return false;
            }

            //母檔要檢查 DealTimeSlot
            if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealSub) == false && dts.Count == 0)
            {
                errorMessage = "資料不完整(dts)";
                return false;
            }

            if (gpo.Slug != null)
            {
                errorMessage = "已結檔";
                return false;
            }


            //如果是母檔，同步子檔
            if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
            {
                var subDealBids = pp.GetComboDealByBid(bh.Guid, false).Select(t => t.BusinessHourGuid);
                string subErrorMessage;
                foreach (var subDealBid in subDealBids)
                {
                    if (CheckClose(subDealBid, false, out subErrorMessage) == false)
                    {
                        errorMessage += string.Format("子檔 {0} {1}", subDealBid, subErrorMessage);
                        return false;
                    }
                }
            }

            return true;
        }



        public bool ExecuteClose(Guid bid, DateTime newEndTime, string userName, StringBuilder sbResult, out string errorMessage, bool subDealCheck = true)
        {
            GroupOrder gpo = op.GroupOrderGetByBid(bid);
            BusinessHour bh = pp.BusinessHourGet(bid);
            DealProperty dp = pp.DealPropertyGet(bid);
            DealAccounting da = pp.DealAccountingGet(bid);
            DealTimeSlotCollection dts = PponFacade.GetDealTimeSlotList(bid);

            if (CheckClose(bh, gpo, dp, da, dts, subDealCheck, out errorMessage) == false)
            {
                return false;
            }
            
            var dealGuids = new List<Guid> { Guid.Parse(bid.ToString()) };
            var combodeals = pp.GetViewComboDealByBid(Guid.Parse(bid.ToString()), false);
            dealGuids.AddRange(combodeals.Select(cd => cd.BusinessHourGuid));
            var deals = pp.ViewPponDealGetByBusinessHourGuidList(dealGuids).ToList();

            if (deals.Any())
            {
                //要先改結檔時間才能結檔
                foreach (var deal in dealGuids)
                {
                    BusinessHour bhDeal = pp.BusinessHourGet(deal);
                    bhDeal.BusinessHourOrderTimeE = newEndTime;
                    pp.BusinessHourSet(bhDeal);
                }
                

                //母檔才需要改dealtimeslot
                if (!Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                {
                    pp.DealTimeSlotDeleteAfterNewEndTime(bh.Guid, newEndTime);

                    var newDealTimeSlotCollection= PponFacade.GetDealTimeSlotList(bid).Where(x => x.EffectiveEnd > newEndTime);
                    foreach (var newDealTimeSlot in newDealTimeSlotCollection)
                    {
                        newDealTimeSlot.EffectiveEnd = newEndTime;
                        pp.DealTimeSlotSet(newDealTimeSlot);
                    }
                }

                List<DealCloseResult> results = _dealClose.CloseDeal(deals, HttpContext.Current.User.Identity.Name);

                if (results.Any())
                {
                    foreach (var item in results)
                    {
                        var deal = deals.FirstOrDefault(x => x.BusinessHourGuid == item.BusinessHourGuid);
                        if (deal == null)
                        {
                            errorMessage = string.Format("檔號[{0}]結檔失敗，請聯絡系統管理員\\n", deal.UniqueId);
                            return false;
                        }

                        if (item.Result == DealCloseResultStatus.AlreadyClose)
                        {
                            errorMessage = string.Format("檔號[{0}]無須結檔\\n", deal.UniqueId);
                            return false;
                        }
                        else if (item.Result == DealCloseResultStatus.Success)
                        {
                            Proposal pro = sp.ProposalGet(bh.Guid);
                            if (pro.IsLoaded)
                            {
                                pro.OrderTimeE = newEndTime;
                                sp.ProposalSet(pro);
                                ProposalFacade.ProposalLog(pro.Id, "[後台手動結檔]" + string.Format("{0}", newEndTime), userName);
                            }

                            sbResult.Append(string.Join("、",deals.Select(x=>x.BusinessHourGuid)));
                            errorMessage = "";
                            return true;
                        }
                        else if (item.Result == DealCloseResultStatus.Fail)
                        {
                            errorMessage = string.Format("檔號[{0}]結檔失敗，請聯絡系統管理員\\n", deal.UniqueId);
                            return false;
                        }
                        else
                        {
                            errorMessage = string.Format("檔號[{0}]系統錯誤，請聯絡系統管理員\\n", deal.UniqueId);
                            return false;
                        }
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        #endregion


    }
}

﻿using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Component
{
    public interface IPagerList
    {
        int CurrentPage { get; }
        int MinPage { get; }
        int MaxPage { get; }
        int RangeMinPage { get; }
        int RangeMaxPage { get; }
        int TotalItemCount { get; }
        bool HasPreviousPage { get; }
        bool HasNextPage { get; }
        void SetPagerBarRange(int prevLinksNum, int nextLinksNum);
    }

    public class PagerList<T> : List<T>, IPagerList
    {
        public static PagerList<T> Empty
        {
            get { return new PagerList<T>(null, new PagerOption(1, 0), 0, 0); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="items"></param>
        /// <param name="opt"></param>
        /// <param name="pageIndex">start from 0</param>
        /// <param name="dataCount"></param>
        public PagerList(IEnumerable<T> items, PagerOption opt, int pageIndex, int dataCount)
        {

            this.PageSize = opt.PageSize;
            this.TotalItemCount = dataCount;
            this.CurrentPage = pageIndex + 1;

            HasPreviousPage = (pageIndex > 0);
            HasNextPage = CurrentPage < MaxPage;
            IsFirstPage = CurrentPage == MinPage;
            IsLastPage = CurrentPage == MaxPage;

            this.SortColumn = opt.SortColumn;
            this.SortDescending = opt.SortDescending;
            if (items != null)
            {
                this.AddRange(items);
            }
        }

        public int PageSize { get; private set; }
        public int TotalItemCount { get; private set; }

        public bool HasPreviousPage { get; private set; }
        public bool HasNextPage { get; private set; }
        public bool IsFirstPage { get; private set; }
        public bool IsLastPage { get; private set; }

        public int CurrentPage { get; private set; }
        public int MaxPage
        {
            get
            {
                int result = TotalItemCount / PageSize;
                if (TotalItemCount % PageSize > 0)
                {
                    result++;
                }

                return result;
            }
        }
        public int MinPage
        {
            get
            {
                return TotalItemCount > 0 ? 1 : 0;
            }
        }

        public int RangeMinPage
        {
            get
            {
                if (_prevLinksNum == 0)
                {
                    return MinPage;
                }
                if (CurrentPage - MinPage > _prevLinksNum)
                {
                    return CurrentPage - _prevLinksNum;
                }
                return MinPage;
            }
        }

        public int RangeMaxPage
        {
            get
            {
                if (_nextLinksNum == 0)
                {
                    return MaxPage;
                }
                if (MaxPage - CurrentPage > _nextLinksNum)
                {
                    return CurrentPage + _nextLinksNum;
                }
                return MaxPage;
            }
        }

        public string SortColumn { get; private set; }
        public bool SortDescending { get; private set; }

        private int _prevLinksNum;
        private int _nextLinksNum;
        public void SetPagerBarRange(int prevLinksNum, int nextLinksNum)
        {
            this._prevLinksNum = prevLinksNum;
            this._nextLinksNum = nextLinksNum;
        }
    }

    public class PagerOption
    {
        public PagerOption(int pageSize, int dataIndex, string sortColumn, bool sortDescending)
        {
            this.PageSize = pageSize;
            this.DataIndex = dataIndex;
            this.SortColumn = sortColumn;
            this.SortDescending = sortDescending;
        }

        public PagerOption(int pageSize, int dataIndex)
        {
            this.PageSize = pageSize;
            this.DataIndex = dataIndex;
        }

        public int PageSize { get; private set; }

        public int DataIndex { get; private set; }

        public string SortColumn { get; private set; }

        public bool SortDescending { get; private set; }
    }
}

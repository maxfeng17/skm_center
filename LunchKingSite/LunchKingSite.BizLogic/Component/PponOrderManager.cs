﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Component
{
    public class PponOrderManager
    {
        protected static ISysConfProvider config;
        protected static IOrderProvider op;
        protected static IPponProvider pp;

        static PponOrderManager()
        {
            config = ProviderFactory.Instance().GetConfig();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        #region 出貨相關

        #region P好康:檢查該訂單商品是否已過鑑賞期
        /// <summary>
        /// 檢查該訂單商品是否已過鑑賞期
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static bool CheckIsOverTrialPeriod(Guid orderGuid)
        {
            //抓取檔次bid
            ViewPponOrder vpo = pp.ViewPponOrderGet(orderGuid);
            if (vpo == null || !vpo.IsLoaded)
            {
                throw new InvalidOperationException(string.Format("order not found! order_guid: {0}", orderGuid.ToString()));
            }
            Guid bid = vpo.BusinessHourGuid;

            //取得配送結束日期
            BusinessHour bh = pp.BusinessHourGet(bid);
            if (bh == null || !bh.IsLoaded)
            {
                throw new InvalidOperationException(string.Format("deal not found in business_hour! business_hour_guid: {0}", bid.ToString()));
            }
            DateTime? deliverEndTime = bh.BusinessHourDeliverTimeE;

            //取得檔次選擇的對帳方式
            DealAccounting da = pp.DealAccountingGet(bid);
            if (da == null || !da.IsLoaded)
            {
                throw new InvalidOperationException(string.Format("deal not found in deal_accounting! business_hour_guid: {0}", bid.ToString()));
            }
            int billingModel = da.VendorBillingModel;

            //商品檔鑑賞期判斷需區別新/舊對帳方式而有所不同
            //舊對帳方式
            if (billingModel.Equals((int)VendorBillingModel.None))
            {
                if (deliverEndTime.HasValue && deliverEndTime.Value.AddDays(config.GoodsAppreciationPeriod).Date < DateTime.Now.Date)
                {
                    return true;
                }
            }
            //新對帳方式:過鑑賞期判斷- 出貨日期+出貨緩衝時間 < 今天
            else if (billingModel.Equals((int)VendorBillingModel.BalanceSheetSystem))
            {
                //訂單最後出貨日期
                //若有換貨 則以最後換貨完成的出貨日期進行鑑賞期判斷
                //如有取貨日 優先以取貨日判斷
                DateTime? shipTime = GetLastShippedBufferTime(orderGuid);

                if (shipTime.HasValue && shipTime.Value.Date < DateTime.Now.Date)
                {
                    return true;
                }
            }

            return false;
        }

        public static DateTime? GetLastShippedBufferTime(Guid orderGuid)
        {
            var exchangeShipId = op.OrderReturnListGetListByType(orderGuid, (int)OrderReturnType.Exchange)
                                    .Where(x => x.Status == (int)OrderReturnStatus.ExchangeSuccess && x.OrderShipId.HasValue)
                                    .Select(x => x.OrderShipId.Value);
            var osInfos = op.OrderShipGetListByOrderGuid(orderGuid);

            if (osInfos.Any())
            {
                if (exchangeShipId.Any())
                {
                    var tempOsExInfos = osInfos.Where(x => exchangeShipId.Contains(x.Id) && (x.ShipTime.HasValue || x.ReceiptTime.HasValue));
                    if (tempOsExInfos.Any())
                    {
                        var maxShipTime = tempOsExInfos.Max(x => x.ShipTime) ?? new DateTime();
                        var maxReceiptTime = tempOsExInfos.Max(x => x.ReceiptTime) ?? new DateTime();
                        var tempOsExInfo = maxShipTime > maxReceiptTime ? tempOsExInfos.OrderByDescending(x => x.ShipTime).First() : tempOsExInfos.OrderByDescending(x => x.ReceiptTime).First();
                        return tempOsExInfo.ReceiptTime.HasValue ? tempOsExInfo.ReceiptTime.Value.AddDays(config.GoodsAppreciationPeriod) : tempOsExInfo.ShipTime.Value.AddDays(config.ShippingBuffer);
                    }
                }

                var tempOsInfos = osInfos.First(x => x.Type == (int)OrderShipType.Normal);

                return tempOsInfos.ReceiptTime.HasValue ? tempOsInfos.ReceiptTime.Value.AddDays(config.GoodsAppreciationPeriod) : (tempOsInfos.ShipTime.HasValue ? (DateTime?)tempOsInfos.ShipTime.Value.AddDays(config.ShippingBuffer) : null);
            }

            return null;
        }

        #endregion P好康:檢查該訂單商品是否已過鑑賞期

        #region 前台消費者檢視訂單狀態使用:檢查該筆訂單是否已出貨

        public static bool CheckIsShipped(Guid orderGuid, OrderClassification orderClassification)
        {
            var os = op.OrderShipGetListByOrderGuid(orderGuid).FirstOrDefault(x => x.OrderClassification == (int)orderClassification && x.Type == (int)OrderShipType.Normal);
            if (os != null)
            {
                DateTime? shipTime = os.ShipTime;
                if (shipTime.HasValue && shipTime.Value.Date <= DateTime.Now.Date)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 取得廠商的出貨資訊
        /// </summary>
        /// <param name="shipInfo"></param>
        /// <param name="seperator"></param>
        /// <returns></returns>
        public static string GetShipInfo(ShipInfo shipInfo, string seperator, ViewCouponListMain main)
        {
            if (shipInfo == null)
            {
                return string.Empty;
            }

            //未出貨:顯示最後出貨日期(檔次選擇[舊對帳方式] 或 檔次選擇[新對帳方式]且 廠商未填出貨資訊 或 出貨日期大於今天日期)
            StringBuilder sb = new StringBuilder();
            
            if (string.IsNullOrEmpty(shipInfo.ShipNo))
            {
                sb.AppendFormat("<p id='shipping-history'>出貨進度{0}{1}</p>", seperator, shipInfo.ShipStatusDesc);
            }
            else
            {

                if (main.ProductDeliveryType == (int)ProductDeliveryType.Normal)
                {
                    sb.AppendFormat("<p id='shipping-history'>出貨進度{0}{1}</p>", seperator, shipInfo.ShipStatusDesc);
                }
                else
                {
                    sb.AppendFormat("<p id='shipping-history'>出貨進度{0}{1}&nbsp;&nbsp;<span class='cta-2019-blue'>查詢歷程</span></p>", seperator, shipInfo.ShipStatusDesc);

                    //製作歷程資料
                    if (main.ProductDeliveryType!=(int)ProductDeliveryType.Wms)
                    {
                        //取得一般歷程
                        var sellerHistory = ISPFacade.GetIspOrderHistory(main.Guid).Where(p => p.HistoryType == (int)IspHistoryType.DeliveryStatus).OrderBy(p => p.SerialNo).ToList();
                        if (sellerHistory.Count() > 0)
                        {
                            sb.AppendFormat("<div class='history-hover-modal'></div>");
                            sb.AppendFormat("<div class='history-hover'><div class='history-hover-content'><ul>");
                            foreach (var item in sellerHistory.OrderBy(p => p.DeliveryTime))
                            {
                                sb.AppendFormat("<li><div class='title'>{0}</div><div class='date'>{1}</div></li>", item.DelvieryContent, item.DeliveryTime);
                            }
                            sb.AppendFormat("</ul></div></div>");
                        }
                    }
                    else
                    {
                        //取得24小歷程
                        var wmsHistory = WmsFacade.GetWmsOrderHistory(main.Guid, shipInfo.ShipNo);
                        if (wmsHistory.Count() > 0)
                        {
                            sb.AppendFormat("<div class='history-hover-modal'></div>");
                            sb.AppendFormat("<div class='history-hover'><div class='history-hover-content'><ul>");
                            foreach (var item in wmsHistory.OrderBy(p => p.DeliveryTime))
                            {
                                sb.AppendFormat("<li><div class='title'>{0}</div><div class='date'>{1}</div></li>", item.DelvieryDesc, item.DeliveryTime);
                            }
                            sb.AppendFormat("</ul></div></div>");
                        }
                    }
                }
            }  


            if (shipInfo.ShipStatus == ShippingStatus.ReadyToShip)
            {
                if (main.ProductDeliveryType != (int)ProductDeliveryType.Wms)
                {
                    sb.AppendFormat("<p>最後出貨日{0}{1}</p>", seperator, string.IsNullOrEmpty(shipInfo.LastShippingDate) ? "照會中" : shipInfo.LastShippingDate);
                }
                else
                {
                    string shipDate = OrderFacade.GetShipDate(main.CreateTime);
                    //sb.AppendFormat("<p>最後出貨日{0}{1}</p>", seperator, "24H到貨不適用");
                    sb.AppendFormat("<p><span>預計到貨{0}{1}</span><span class='note-text-wrap' href='#'><img src='../Themes/default/images/17Life/G2/safely_code_icon.png' width='21' height='21' /><span class='note-text-box-description'>24H到貨配送時程對照表(*依下單且付款完成時間計算)<br />►周一～周五 10:00～21:59，預計以24H為配送到貨時間<br />►周一～周四、周日<br />&nbsp;&nbsp;&nbsp;&nbsp;22:00～23:59，預計後天12:00前到貨<br />&nbsp;&nbsp;&nbsp;&nbsp;00:00～10:00，預計隔天12:00前到貨<br />►周五 00:00～10:00，預計以24H為配送到貨時間<br />►周五 22:00～周日 21:59，預計周一配送</span></span></p>", seperator, shipDate);
                }

            }
            //已出貨:顯示廠商出貨資訊(檔次選擇[新對帳方式]且廠商已填出貨資訊且出貨日期小於等於今天日期)
            else
            {
                sb.AppendFormat("<p>出貨時間{0}{1}</p>", seperator, shipInfo.ActualShippingDate);
                if (string.IsNullOrEmpty(shipInfo.ShipCompanyName) == false && string.IsNullOrEmpty(shipInfo.ShipWebsite) == false)
                {
                    sb.AppendFormat("<p>物流公司{0}<a target='_blank' style='cursor:pointer' onclick='gotoShipUrl(this)'>{1}</a>",
                        seperator, shipInfo.ShipCompanyName);
                    sb.AppendFormat("<span id='shipUrl' style='display:none'>{0}</span><span id='shipNo' style='display:none'>{1}</span></p>",
                        shipInfo.ShipWebsite, shipInfo.ShipNo);
                }
                else if (string.IsNullOrEmpty(shipInfo.ShipCompanyName) == false && string.IsNullOrEmpty(shipInfo.ShipWebsite))
                {
                    sb.AppendFormat("<p>物流公司{0}{1}</p>", seperator, shipInfo.ShipCompanyName);
                }
                if (string.IsNullOrEmpty(shipInfo.ShipNo) == false)
                {
                    sb.AppendFormat("<p>運單編號{0}{1}</p>", seperator, shipInfo.ShipNo);
                }
            }
            return sb.ToString();
        }
        //ShipInfo shipInfo

        #endregion 前台消費者檢視訂單狀態使用:檢查該筆訂單是否已出貨

        #region 檢查訂單是否有可出貨商品

        public static bool CheckIsShipable(Guid orderGuid)
        {
            //於order_product抓取非退貨完成狀態商品數 為 可出貨商品數
            return op.OrderProductGetListByOrderGuid(orderGuid)
                        .Any(x => x.IsCurrent && !x.IsReturned);
        }

        #endregion 檢查訂單是否有可出貨商品

        #endregion 出貨相關

        #region 換貨相關

        //20161007訂單是否有換貨紀錄的判定:是否可異動初始出貨資訊判定使用
        //無關處理進度 只要有成立過換貨單皆視為有換貨紀錄(較為合理) 
        //上線後觀察是否有欲排除換貨取消及換貨失敗等 標記無換貨完成等結案進度 之需求

        public static bool CheckEverExchange(Guid orderGuid)
        {
            if (!config.EnabledExchangeProcess)
            {
                return false;
            }
            //有換貨紀錄之訂單 不得異動初始出貨資訊
            return op.OrderReturnListGetListByType(orderGuid, (int)OrderReturnType.Exchange)
                        //.Any(x => x.Status != (int)OrderReturnStatus.ExchangeCancel)
                        .Any();
        }

        public static ILookup<Guid, ViewPponOrderReturnList> GetHasExchangeLogOrderInfos(IEnumerable<Guid> dealGuids)
        {
            if (!config.EnabledExchangeProcess)
            {
                return (new Dictionary<Guid, ViewPponOrderReturnList>()).ToLookup(t => t.Key, t => t.Value);
            }
            return op.ViewPponOrderReturnListGetList(dealGuids, OrderReturnType.Exchange)
                        //.Where(x => x.ReturnStatus != (int)OrderReturnStatus.ExchangeCancel)
                        .ToLookup(x => x.OrderGuid);
        }

        #endregion 換貨相關


    }
}

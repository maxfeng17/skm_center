﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class PcpAssignmentUtility
    {
        #region properties

        private static readonly IPCPProvider pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
        private static readonly IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static readonly INotificationProvider np = ProviderFactory.Instance().GetProvider<INotificationProvider>();
        private static readonly IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static readonly ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private PcpAssignment pcpAssignmentObj { set; get; }
        private PcpAssignmentFilterCollection filterCol { set; get; }
        
        public AssignmentType PcpAssignmentType { set; get; }
        public AssignmentMemberType MemberType { set; get; }
        public int SendCount { set; get; }
        public int SellerUserId { set; get; }
        public Guid? StoreGuid { set; get; }

        public bool IsValid
        {
            get
            {
                if (!pcpAssignmentObj.IsLoaded)
                {
                    return false;
                }

                if (MemberType == AssignmentMemberType.WebSiteMember)
                {
                    return filterCol.Any(x => x.FilterType == (int) AssignmentFilterType.DealCategory)
                           || filterCol.Any(x => x.FilterType == (int) AssignmentFilterType.City)
                           || filterCol.Any(x => x.FilterType == (int) AssignmentFilterType.District)
                           || PcpAssignmentType == AssignmentType.PushMsg2Km;
                }
                return true;
            }
        }

        #endregion

        public PcpAssignmentUtility(int pcpAssignmentId)
        {
            pcpAssignmentObj = np.PcpAssignmentGet(pcpAssignmentId);
            filterCol = np.PcpAssignmentFilterGetList(pcpAssignmentId);
            
            if (pcpAssignmentObj.IsLoaded)
            {
                SellerUserId = pcpAssignmentObj.UserId;
                PcpAssignmentType = (AssignmentType)pcpAssignmentObj.AssignmentType;
                MemberType = (AssignmentMemberType)pcpAssignmentObj.MemberType;
                SendCount = pcpAssignmentObj.SendCount;
                StoreGuid = pcpAssignmentObj.StoreGuid;
            }
        }

        public bool InsertPcpAssignmentMember()
        {
            //至少要有一個條件
            if (filterCol.Count == 0)
            {
                return false;
            }
            //非新任務
            if (pcpAssignmentObj.Status != (int)AssignmentStatus.NewAssign)
            {
                return false;
            }
            //已產過 campaign 不可再產
            if (pcpAssignmentObj.DiscountCampaignId != null)
            {
                return false;
            }
            //沒有 template id 無法產生抵用券
            if (pcpAssignmentObj.DiscountTemplateId == null)
            {
                return false;
            }
            //目前不開放發 17life 會員 (member) 簡訊
            if (MemberType == AssignmentMemberType.WebSiteMember && PcpAssignmentType == AssignmentType.Sms)
            {
                return false;
            }

            //取得要發送的名單
            var sendList = GetUserIdList();

            using (var trans = TransactionScopeBuilder.CreateReadCommitted())
            {
                //產生 campaign
                var discountCampaignId = BonusFacade.DiscountCampaignCloneTemplate(pcpAssignmentObj);
                //產生 discount_code
                BonusFacade.DiscountCodeCreate(discountCampaignId, sendList, MemberType);
                var codes = op.DiscountCodeGetListByCampaign(discountCampaignId);

                foreach (var userId in sendList)
                {
                    var assignmentMember = new PcpAssignmentMember
                    {
                        AssignmentId = pcpAssignmentObj.Id,
                        MemberId = userId,
                        MemberType = (int)MemberType,
                    };

                    var uniqueId = userId;
                    int? discountCodeId = null;

                    if (MemberType == AssignmentMemberType.SellerMember)
                    {
                        uniqueId = 0;
                        var sellerMember = sp.ViewVbsSellerMemberGet(null, userId, SellerUserId);
                        if (sellerMember.IsLoaded && sellerMember.UserId != null)
                        {
                            uniqueId = (int)sellerMember.UserId;
                        }
                    }

                    var firstOrDefault = codes.FirstOrDefault(x => x.Owner == uniqueId);
                    if (firstOrDefault != null)
                    {
                        discountCodeId = firstOrDefault.Id;
                    }

                    assignmentMember.DiscountCodeId = discountCodeId;
                    np.PcpAssignmentMemberSet(assignmentMember);
                }

                pcpAssignmentObj.FilterCount = sendList.Count;
                pcpAssignmentObj.DiscountCampaignId = discountCampaignId;
                pcpAssignmentObj.Status = (int)AssignmentStatus.Ready;
                np.PcpAssignmentSet(pcpAssignmentObj);

                //新增會員訊息
                List<PcpAssignmentMember> assMems = np.PcpAssignmentMemberGetList(pcpAssignmentObj.Id);
                DateTime now = DateTime.Now;
                foreach (PcpAssignmentMember assMem in assMems)
                {
                    mp.MemberMessageSet(new MemberMessage
                    {
                        Message = pcpAssignmentObj.Message,
                        SenderId = assMem.MemberId,
                        IsRead = false,
                        ReadTime = null,
                        //AssignmentMemberId = assMem.Id,
                        SendTime = now
                        
                    });
                }
                trans.Complete();
            }
            return true;
        }

        #region Private

        private List<int> GetUserIdList()
        {
            Random rand = new Random();
            List<int> memberIds = (MemberType == AssignmentMemberType.SellerMember) ? SellerMemberFilter() : MemberFilter();
            if (memberIds.Count > SendCount)
            {
                memberIds = memberIds.OrderBy(x => rand.Next()).Take(SendCount).ToList();
            }
            return memberIds;
        }

        private List<int> MemberFilter()
        {
            var filterCategory = filterCol.Any(x => x.FilterType == (int)AssignmentFilterType.DealCategory);
            var filterCity = (filterCol.Any(x => x.FilterType == (int)AssignmentFilterType.City) ||
                              filterCol.Any(x => x.FilterType == (int)AssignmentFilterType.District));
            var is17LifeMember = MemberType == AssignmentMemberType.WebSiteMember;

            var isGiveDiscountCodeToMember =
                filterCol.Any(x => x.FilterType == (int) AssignmentFilterType.UserMembershipCardId);

            if (isGiveDiscountCodeToMember)
            {
                filterCategory = filterCity = is17LifeMember = false;
            }

            var sql = string.Format("select distinct m.* from {0} m with(nolock) {1} {2} {3} where 1=1 {4} "
                , (is17LifeMember) ? "member" : "view_user_membership_card"
                , (filterCategory) ?
                    string.Format(" inner join member_order_category_log mo with(nolock) On m.{0} = mo.user_id", is17LifeMember ? "unique_id" : "user_id") : string.Empty
                , (filterCity) ?
                    string.Format(" inner join view_member_building_city vm with(nolock) On m.{0} = vm.unique_id", is17LifeMember ? "unique_id" : "user_id") : string.Empty
                , (is17LifeMember) ? " left join member_property mp On m.unique_id = mp.user_id" : string.Empty
                , (!is17LifeMember) ? string.Format(" and seller_user_id = {0}", SellerUserId) : string.Empty);

            //類別條件
            if (filterCategory)
            {
                sql += string.Format(" and mo.cid in ({0})"
                    ,
                    string.Join(",",
                        filterCol.Where(x => x.FilterType == (int)AssignmentFilterType.DealCategory)
                            .Select(x => x.ParameterFirst)));
            }

            //地區條件
            if (filterCity)
            {
                if (filterCol.Any(x => x.FilterType == (int)AssignmentFilterType.City))
                {
                    sql += string.Format(" and vm.city_parent_id in ({0}) and DateDiff(mm, mo.order_time, getdate()) < 7"
                        ,
                        string.Join(",",
                            filterCol.Where(x => x.FilterType == (int)AssignmentFilterType.City)
                                .Select(x => x.ParameterFirst)));
                }

                if (filterCol.Any(x => x.FilterType == (int)AssignmentFilterType.District))
                {
                    sql += string.Format(" and vm.city_id in ({0})"
                        , string.Join(",", filterCol.Where(x => x.FilterType == (int)AssignmentFilterType.District)
                            .Select(x => x.ParameterFirst)));
                }
            }

            //卡片等級
            if (!is17LifeMember && filterCol.Any(x => x.FilterType == (int)AssignmentFilterType.MembershipCardLevel))
            {
                sql += string.Format(" and m.level = {0}",
                    filterCol.First(x => x.FilterType == (int)AssignmentFilterType.MembershipCardLevel).ParameterFirst);
            }

            //會員卡直發
            if (isGiveDiscountCodeToMember)
            {
                sql += string.Format(" and m.id = {0}",
                    filterCol.First(x => x.FilterType == (int) AssignmentFilterType.UserMembershipCardId).ParameterFirst);
            }

            //推播條件
            if ((new List<AssignmentType> {AssignmentType.PushMsg17Life, AssignmentType.PushMsg2Km}).Contains(PcpAssignmentType))
            {
                //推播必需有 device token
                sql += string.Format(@" and exists (
			            select * from  [member_collect_notice] mcn with(nolock)  
			                where mcn.user_id = m.{0})", is17LifeMember ? "unique_id" : "user_id");

                //推播2km
                if (PcpAssignmentType == AssignmentType.PushMsg2Km)
                {
                    sql +=
                        string.Format(" and coordinate.STDistance(select coordinate from store where Guid = '{0}') < 2000",
                            StoreGuid);
                }
            }

            return is17LifeMember ? mp.MemberGetListBySql(sql).Select(x => x.UniqueId).ToList()
                : pcp.ViewUserMembershipCardGet(sql).Select(x => x.UserId).ToList();
        }

        private List<int> SellerMemberFilter()
        {
            var filterPara = new List<string> { string.Format("{0}={1}", SellerMember.Columns.SellerUserId, SellerUserId) };
            foreach (var f in filterCol)
            {
                switch ((AssignmentFilterType)f.FilterType)
                {
                    case AssignmentFilterType.Age:
                        filterPara.Add(string.Format("Datediff(yy, {0}, getdate()) {1}"
                            , SellerMember.Columns.Birthday
                            , GetFilterString((Operators)f.OperatorX, f.ParameterFirst, f.ParameterSecond)));
                        break;
                    case AssignmentFilterType.BirthdayByMonth:
                        filterPara.Add(string.Format("Month({0}) {1}"
                            , SellerMember.Columns.Birthday
                            , GetFilterString((Operators)f.OperatorX, f.ParameterFirst, f.ParameterSecond)));
                        break;
                }
            }

            return pcp.SellerMemberGet(filterPara.ToArray()).Select(x => x.Id).ToList();
        }

        private string GetFilterString(Operators op, string para1, string para2)
        {
            switch (op)
            {
                case Operators.IsEqualTo:
                    return string.Format("={0}", para1);
                case Operators.GreaterThan:
                    return string.Format(">{0}", para1);
                case Operators.GreaterThanEqual:
                    return string.Format(">={0}", para1);
                case Operators.LessThan:
                    return string.Format("<{0}", para1);
                case Operators.LessThanEqual:
                    return string.Format("<={0}", para1);
                case Operators.BetweenAnd:
                    return string.Format(" {0} and {1}", para1, para2);
                default:
                    return string.Empty;
            }
        }

        #endregion Private
    }
}

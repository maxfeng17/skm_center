﻿using EnterpriseDT.Util.Debug;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Component
{
    public class FuliAdapter
    {
        static ILog logger = LogManager.GetLogger(typeof(FuliAdapter));
        public static string QueryCodes(string code, bool cancel)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string url = string.Format("http://192.168.170.15:1770/check?code={0}", code);
                    if (cancel)
                    {
                        url += "&cancel=1";
                    }
                        
                    HttpResponseMessage response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    logger.Warn("FuliAdapter query fail", ex);
                }
            }

            return string.Empty;
        }

        public class CodeResult
        {
            public string Code { get; set; }
            public string Status { get; set; }
        }
    }
}

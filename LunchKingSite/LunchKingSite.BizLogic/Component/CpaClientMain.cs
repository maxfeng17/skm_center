﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    [Serializable]
    public class CpaClientMain<T> : List<T>
    {
        public string SessionId { get; set; }
        public string Ip { get; set; }
        public string UserName { get; set; }

        public CpaClientMain()
        {
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.Ppon;
using LunchKingSite.BizLogic.Models.Skm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Component
{
    public class SystemDataManager
    {
        private static ISystemProvider ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(SystemDataManager));
        public const string _BUY_PROMOTION_TEXT = "BuyPromotionText";
        public const string _SKM_OAUTH_KEY = "SkmSiteOauthKey";

        public static T GetFromCache<T>(string name) where T : class, new()
        {
            string cacheKey = string.Format("SystemDataManager.{0}.{1}", name, DateTime.Now.ToString("yyyyMMddHH"));

            T t = MemoryCache.Default.Get(cacheKey) as T;
            if (t != null)
            {
                return t;
            }           
            var sd = ss.SystemDataGet(name);
            if (sd.IsLoaded)
            {
                try
                {
                    t = ProviderFactory.Instance().GetSerializer().Deserialize<T>(sd.Data);
                    MemoryCache.Default.Set(cacheKey, t, DateTime.MaxValue);
                    return t;
                }
                catch (Exception ex)
                {
                    logger.Error("GetFromCache error, name=" + name, ex);
                }
            }
            return null;
        }

        public static void ClearBuyPromotionTextCache()
        {
            SystemFacade.ClearAllServersCache<List<BuyPromotionText>>(_BUY_PROMOTION_TEXT);
        }

        public static void ClearSkmSiteOauthKeyCache()
        {
            SystemFacade.ClearAllServersCache<List<SkmSiteOauthKeyModel>>(_SKM_OAUTH_KEY);
        }
    }
}

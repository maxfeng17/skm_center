﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class HiDealFreightManager
    {
        protected static IHiDealProvider HiDealProv;
        protected static ISysConfProvider SysProv;
        protected static ISellerProvider SellerProv;

        private static HiDealProduct _freightProduct;

        static HiDealFreightManager()
        {
            HiDealProv = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            SysProv = ProviderFactory.Instance().GetConfig();
            SellerProv = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        }

        public static HiDealProduct GetFreightProduct()
        {
            if (_freightProduct == null)
            {
                ReloadDefaultManager();
            }
            return _freightProduct;
        }

        public static void ReloadDefaultManager()
        {
            var tmpObject = new object();
            lock (tmpObject)
            {
                var fProducts = HiDealProv.HiDealProductGetList(0, 0, HiDealProduct.Columns.Id,
                                                                HiDealProduct.Columns.Type + " = " +
                                                                (int)HiDealProductType.Freight);
                if (fProducts.Count > 0)
                {
                    _freightProduct = fProducts.First();
                }
                else
                {
                    _freightProduct = CreateFreightProduct();
                }
            }
        }

        public static HiDealProduct CreateFreightProduct()
        {
            var seller = SellerProv.SellerGet(Seller.Columns.SellerName, "HiDeal官方系統商家");
            //無系統商家，建立系統商家
            if (!seller.IsLoaded)
            {
                string sellerId = string.Empty;

                //取台北市中山區為代表城市
                var tmps = CityManager.Citys.Where(x => x.Code == "TP" && x.ParentId == null).ToList();
                var taipeiCity = CityManager.Citys.Where(x => x.Code != "SYS").First();
                if (tmps.Count > 0)
                {
                    taipeiCity = tmps.First();
                }

                seller.Guid = Helper.GetNewGuid(seller);
                //賣家編號產生不再依據 城市-地區  產出
                sellerId = SellerFacade.GetNewSellerID();
                seller.SellerId = sellerId;
                seller.SellerName = "HiDeal官方系統商家";
                seller.Department = (int)DepartmentTypes.HiDeal;
                seller.PostCkoutAction = (int)PostCheckoutActionType.NothingToDo;
                seller.Coordinate = LocationFacade.GetGeographyWKT("0", "0");
                seller.CityId = taipeiCity.Id;
                seller.SellerInvoice = ((int)SellerInvoiceType.NoInvoiceReceipt).ToString();
                seller.CreateId = SysProv.SystemEmail;
                seller.CreateTime = DateTime.Now;
                SellerProv.SellerSet(seller);
            }
            //查詢系統檔次
            var deals = HiDealProv.HiDealProductGetList(0, 0, HiDealDeal.Columns.Id,
                                                             HiDealDeal.Columns.SellerGuid + "=" + seller.Guid);
            //查無資料，建立系統檔次
            int dealId = 0;
            if (deals.Count == 0)
            {
                var deal = new HiDealDeal();
                deal.SellerGuid = seller.Guid;
                deal.HiDealGuid = Helper.GetNewGuid(deal);
                deal.Name = "HiDeal官方系統檔次";
                deal.CreateId = SysProv.SystemEmail;
                deal.CreateTime = DateTime.Now;

                HiDealProv.HiDealDealSet(deal);
                dealId = deal.Id;
            }
            else
            {
                dealId = deals.First().Id;
            }
            var products = HiDealProv.HiDealProductGetList(0, 0, HiDealProduct.Columns.Id,
                                                            HiDealProduct.Columns.DealId + " = " + dealId);
            //減少時間差造成的問題
            var testProducts = products.Where(x => x.Type == (int)HiDealProductType.Freight).ToList();
            if (testProducts.Count > 0)
            {
                return testProducts.First();
            }
            //建立product
            var product = new HiDealProduct();
            product.Guid = Helper.GetNewGuid(product);
            product.DealId = dealId;
            product.Seq = products.Count + 1;
            product.SellerGuid = seller.Guid;
            product.Name = "運費";
            product.Description = null;
            product.IsOnline = true;
            product.UseStartTime = DateTime.Now;
            product.UseEndTime = DateTime.MaxValue;
            product.Type = (int)HiDealProductType.Freight;
            product.CreateId = SysProv.SystemEmail;
            product.CreateTime = DateTime.Now;
            HiDealProv.HiDealProductSet(product);
            return product;
        }
    }
}
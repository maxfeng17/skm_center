﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.EInvoice;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Component
{
    public class LoveCodeManager
    {
        private static LoveCodeManager _instance = new LoveCodeManager();
        private LoveCodeManager()
        {
        }
        public static LoveCodeManager Instance
        {
            get
            {
                return _instance;
            }
        }

        private CodeName[] _loveCodes = null;

        public CodeName[] LoveCodes
        {
            get
            {
                if (_loveCodes == null)
                {
                    LoadAllLoveCode();
                }
                return _loveCodes ?? new CodeName[] { };
            }
        }

        private ISystemProvider sys = ProviderFactory.Instance().GetProvider<ISystemProvider>();

        public void ReloadDefaultManager()
        {
            _instance.LoadAllLoveCode();
        }

        protected void LoadAllLoveCode()
        {
            string data = sys.SystemDataGet("LoveCode").Data;
            _loveCodes = ProviderFactory.Instance().GetSerializer().Deserialize<CodeName[]>(data);
        }

        public List<CodeName> QueryLoveCodes(string code)
        {
            if (_instance._loveCodes == null)
            {
                LoadAllLoveCode();
            }
            return _instance._loveCodes.Where(t => t.Code.StartsWith(code)).ToList();
        }

        public CodeName QueryLoveCode(string code)
        {
            if (_instance._loveCodes == null)
            {
                LoadAllLoveCode();
            }
            return _instance._loveCodes.FirstOrDefault(t => t.Code == code);
        }


        public string QueryLoveCodeString(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return string.Empty;
            }
            CodeName cn = QueryLoveCode(code);
            if (cn == null)
            {
                return string.Empty;
            }
            return string.Format("{0} {1}", cn.Code, cn.Name);
        }

        /// <summary>
        /// 從政府的web service取最新的愛心碼資料
        /// </summary>
        /// <returns></returns>
        public static List<LoveCode> GetAllLoveCodesFromGovService()
        {
            HashSet<LoveCode> loveCodes = new HashSet<LoveCode>();

            GovAPIReply reply = GovEinvoiceAPI.QueryLoveCode("%");
            if (reply.Code == GovAPIReply.Error)
            {
                throw new Exception("QueryLoveCode fail.");
            }
            LoveCodeReply loveCodeReply = reply as LoveCodeReply;
            if (loveCodeReply != null)
            {
                foreach (var item in loveCodeReply.Details)
                {
                    if (loveCodes.Contains(item) == false)
                    {
                        loveCodes.Add(item);
                    }
                }
            }
            return loveCodes.OrderBy(t => t.Code).ToList();
        }

        /// <summary>
        /// 將 web service 來的愛心碼集合，轉成 json 字串
        /// </summary>
        /// <param name="loveCodes"></param>
        /// <returns></returns>
        public static string LoveCodesToJson(List<LoveCode> loveCodes)
        {
            ISerializer json = ProviderFactory.Instance().GetSerializer();
            string result = json.Serialize(
                (from t in loveCodes
                 select new
                 {
                     code = t.Code,
                     name = t.Name
                 }).ToList());
            return result;
        }

    }
}
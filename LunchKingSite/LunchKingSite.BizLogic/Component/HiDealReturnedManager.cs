﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.BizLogic.Component
{
    public class HiDealReturnedManager
    {
        public const string ManagerNameForJob = "HiDealReturnedManager";
        private static IHiDealProvider hp;
        private static IMemberProvider mp;
        protected static ISysConfProvider config;
        protected static IOrderProvider op;
        protected static IAccountingProvider ap;
        private static readonly log4net.ILog log;

        static HiDealReturnedManager()
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            config = ProviderFactory.Instance().GetConfig();
            log = log4net.LogManager.GetLogger(typeof(HiDealReturnedManager));
        }

        #region 退貨資料建立與作業

        /// <summary>
        /// 依據傳入的訂單物件，建立退貨單，退貨商品為此張訂單尚未使用的全部商品
        /// 到店取件-尚未核銷的全部退回
        /// 宅配商品-尚未退件的商品全退，若退貨數量與購買數量相同，則運費也退貨。
        /// </summary>
        /// <param name="order"></param>
        /// <param name="cashBack">刷卡與ATM是否要刷退</param>
        /// <param name="userName">產生退貨單的使用者/操作者，可能前臺呼叫因此記會員的UserName，但考量修改成本與效益，暫時不替換成UserId</param>
        /// <param name="reason">退貨原因</param>
        /// <param name="returnedOrder">回傳建立的退貨單</param>
        /// <returns></returns>
        public static bool HiDealReturnedSetByOrderWithAllReturn(HiDealOrder order, bool cashBack, string userName, string reason, out HiDealReturned returnedOrder)
        {
            returnedOrder = new HiDealReturned();
            var returnedCoupons = new HiDealReturnedCouponCollection();
            //建立退貨單
            decimal totalAmount = 0, pcash = 0, scash = 0, bcash = 0, creditCardAmt = 0, atmAmount = 0, discountAmount = 0;
            var returned = new HiDealReturned();
            returned.OrderPk = order.Pk;
            returned.OrderId = order.OrderId;
            returned.UserId = order.UserId;
            returned.Status = (int)HiDealReturnedStatus.Create;
            returned.ApplicationTime = DateTime.Now;
            returned.CreateId = userName;
            returned.CreateTime = DateTime.Now;
            returned.CashBack = cashBack;//刷卡與ATM是否退回現金-申請狀態
            returned.ReturnCashBack = cashBack;//刷卡與ATM是否退回現金-實際處理判斷
            returned.ReturnReason = reason;

            //建立退貨明細
            //查詢orderDetail資料
            var orderDetails = hp.HiDealOrderDetailGetListByOrderGuid(order.Guid);
            var returnedDetails = new HiDealReturnedDetailCollection();
            //取出目前 已申請、已完成 的退貨單明細資料
            var oldReturnStatus = new List<HiDealReturnedStatus> { HiDealReturnedStatus.Create, HiDealReturnedStatus.Completed };
            var oldHiDealReturneds = hp.ViewHiDealReturnedGetList(order.Pk, oldReturnStatus);
            //已退貨 或 已申請退貨 商品總數(不包含運費等商品類型)
            var returnedGoodsQuant = oldHiDealReturneds.Where(x => x.ProductType == (int)HiDealProductType.Product).Sum(x => x.ItemQuantity);
            //訂單商品總數(不包含運費等商品類型)
            var orderItemQuant = 0;
            //本次退貨商品總數
            var newReturnItemQuant = 0;

            #region 處理所以一般商品(非運費等)

            var returnedCashTrustLog = new List<RefundCashTrustLogTmpData>();
            foreach (var detail in orderDetails.Where(x => x.ProductType == (int)HiDealProductType.Product))
            {
                orderItemQuant += detail.ItemQuantity;
                //檢查明細所屬憑證核銷狀況(cash_trust_log)
                var trustLogs = mp.CashTrustLogGetListByOrderDetailGuid(detail.Guid, GetTrustStatusWithAllowReturns((HiDealDeliveryType)detail.DeliveryType), OrderClassification.HiDeal);
                //有商品供退貨
                if (trustLogs.Count > 0)
                {
                    returnedCashTrustLog.Add(new RefundCashTrustLogTmpData() { CashTrustLogs = trustLogs, OrderDetailGuid = detail.Guid });
                    //退貨單明細部分
                    var rtnDetail = new HiDealReturnedDetail();
                    rtnDetail.OrderDetailGuid = detail.Guid;
                    rtnDetail.HiDealId = detail.HiDealId;
                    rtnDetail.SellerGuid = detail.SellerGuid;
                    rtnDetail.SellerName = detail.SellerName;
                    rtnDetail.ProductId = detail.ProductId;
                    rtnDetail.ProductName = detail.ProductName;
                    rtnDetail.ItemQuantity = trustLogs.Count;
                    rtnDetail.UnitPrice = detail.UnitPrice;
                    rtnDetail.DetailTotalAmt = detail.UnitPrice * trustLogs.Count;
                    rtnDetail.Category1 = detail.Category1;
                    rtnDetail.Option1 = detail.Option1;
                    rtnDetail.OptionName1 = detail.OptionName1;
                    rtnDetail.Category2 = detail.Category2;
                    rtnDetail.Option2 = detail.Option2;
                    rtnDetail.OptionName2 = detail.OptionName2;
                    rtnDetail.Category3 = detail.Category3;
                    rtnDetail.Option3 = detail.Option3;
                    rtnDetail.OptionName3 = detail.OptionName3;
                    rtnDetail.Category4 = detail.Category4;
                    rtnDetail.Option4 = detail.Option4;
                    rtnDetail.OptionName4 = detail.OptionName4;
                    rtnDetail.Category5 = detail.Category5;
                    rtnDetail.Option5 = detail.Option5;
                    rtnDetail.OptionName5 = detail.OptionName5;
                    rtnDetail.StoreGuid = detail.StoreGuid;
                    rtnDetail.DeliveryType = detail.DeliveryType;
                    rtnDetail.CreateId = userName;
                    rtnDetail.CreateTime = DateTime.Now;
                    rtnDetail.ProductType = detail.ProductType;
                    //金額加總部分
                    pcash += trustLogs.Sum(x => x.Pcash);
                    scash += trustLogs.Sum(x => x.Scash);
                    bcash += trustLogs.Sum(x => x.Bcash);
                    creditCardAmt += trustLogs.Sum(x => x.CreditCard);
                    atmAmount += trustLogs.Sum(x => x.Atm);
                    discountAmount += trustLogs.Sum(x => x.DiscountAmount);

                    newReturnItemQuant += detail.ItemQuantity;

                    returnedDetails.Add(rtnDetail);
                }
            }

            #endregion 處理所以一般商品(非運費等)

            #region 處理運費資料 --若 已完成或申請退貨商品數量 + 本次退貨數量 = 訂單商品總數 則進行運費的退貨處理。

            //處理運費資料 --若 已完成或申請退貨商品數量 + 本次退貨數量 = 訂單商品總數 則進行運費的退貨處理。
            if (returnedGoodsQuant + newReturnItemQuant == orderItemQuant)
            {
                foreach (var detail in orderDetails.Where(x => x.ProductType == (int)HiDealProductType.Freight))
                {
                    //檢查明細所屬憑證核銷狀況(cash_trust_log)
                    var trustLogs = mp.CashTrustLogGetListByOrderDetailGuid(detail.Guid, GetTrustStatusWithAllowReturns((HiDealDeliveryType)detail.DeliveryType), OrderClassification.HiDeal);
                    //有商品供退貨
                    if (trustLogs.Count > 0)
                    {
                        var rtnDetail = new HiDealReturnedDetail();
                        rtnDetail.OrderDetailGuid = detail.Guid;
                        rtnDetail.HiDealId = detail.HiDealId;
                        rtnDetail.SellerGuid = detail.SellerGuid;
                        rtnDetail.SellerName = detail.SellerName;
                        rtnDetail.ProductId = detail.ProductId;
                        rtnDetail.ProductName = detail.ProductName;
                        rtnDetail.ItemQuantity = trustLogs.Count;
                        rtnDetail.UnitPrice = detail.UnitPrice;
                        rtnDetail.DetailTotalAmt = detail.UnitPrice * trustLogs.Count;
                        rtnDetail.Category1 = detail.Category1;
                        rtnDetail.Option1 = detail.Option1;
                        rtnDetail.OptionName1 = detail.OptionName1;
                        rtnDetail.Category2 = detail.Category2;
                        rtnDetail.Option2 = detail.Option2;
                        rtnDetail.OptionName2 = detail.OptionName2;
                        rtnDetail.Category3 = detail.Category3;
                        rtnDetail.Option3 = detail.Option3;
                        rtnDetail.OptionName3 = detail.OptionName3;
                        rtnDetail.Category4 = detail.Category4;
                        rtnDetail.Option4 = detail.Option4;
                        rtnDetail.OptionName4 = detail.OptionName4;
                        rtnDetail.Category5 = detail.Category5;
                        rtnDetail.Option5 = detail.Option5;
                        rtnDetail.OptionName5 = detail.OptionName5;
                        rtnDetail.StoreGuid = detail.StoreGuid;
                        rtnDetail.DeliveryType = detail.DeliveryType;
                        rtnDetail.CreateId = userName;
                        rtnDetail.CreateTime = DateTime.Now;
                        rtnDetail.ProductType = detail.ProductType;

                        pcash += trustLogs.Sum(x => x.Pcash);
                        scash += trustLogs.Sum(x => x.Scash);
                        bcash += trustLogs.Sum(x => x.Bcash);
                        creditCardAmt += trustLogs.Sum(x => x.CreditCard);
                        atmAmount += trustLogs.Sum(x => x.Atm);
                        discountAmount += trustLogs.Sum(x => x.DiscountAmount);
                        returnedDetails.Add(rtnDetail);
                    }
                }
            }

            #endregion 處理運費資料 --若 已完成或申請退貨商品數量 + 本次退貨數量 = 訂單商品總數 則進行運費的退貨處理。

            //退貨金額
            totalAmount += (pcash + scash + bcash + creditCardAmt + atmAmount + discountAmount);

            returned.TotalAmount = totalAmount;
            returned.Pcash = pcash;
            returned.Scash = scash;
            returned.Bcash = bcash;
            returned.CreditCardAmt = creditCardAmt;
            returned.DiscountAmount = discountAmount;
            returned.AtmAmount = atmAmount;

            using (var transScope = new TransactionScope())
            {
                try
                {
                    //儲存退貨單
                    hp.HiDealReturnedSet(returned);
                    //取得退貨單號後，填入退貨單號
                    foreach (var returnedDetail in returnedDetails)
                    {
                        returnedDetail.ReturnedId = returned.Id;
                    }
                    hp.HiDealReturnedDetailSetList(returnedDetails);

                    #region 處理憑證退貨紀錄

                    foreach (var returnedTmpData in returnedCashTrustLog)
                    {
                        var returnedDetail = returnedDetails.Where(x => x.OrderDetailGuid == returnedTmpData.OrderDetailGuid).First();
                        foreach (var trustLog in returnedTmpData.CashTrustLogs)
                        {
                            if (trustLog.CouponId != null)
                            {
                                var returnedCoupon = new HiDealReturnedCoupon();
                                returnedCoupon.ReturnedId = returned.Id;
                                returnedCoupon.ReturnedDetailId = returnedDetail.Id;
                                returnedCoupon.CouponId = (long)trustLog.CouponId;

                                returnedCoupons.Add(returnedCoupon);
                            }
                        }
                    }

                    #endregion 處理憑證退貨紀錄

                    hp.HiDealReturnedCouponSetList(returnedCoupons);
                    transScope.Complete();
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message, ex);
                    return false;
                }
            }
            returnedOrder = returned;

            #region 重新整理 HiDealCouponManager ProductCouponData

            if ((orderDetails.Where(x => x.ProductType == (int)HiDealProductType.Product)).Any())
            {
                HiDealCouponManager.ClearProductCouponData(orderDetails.First().ProductId);
            }

            #endregion 重新整理 HiDealCouponManager ProductCouponData

            return true;
        }

        /// <summary>
        /// 取得可退貨的trustStatus狀態
        /// </summary>
        /// <param name="deliveryType"></param>
        /// <returns></returns>
        public static List<TrustStatus> GetTrustStatusWithAllowReturns(HiDealDeliveryType deliveryType)
        {
            var trustStatusList = new List<TrustStatus>();
            switch (deliveryType)
            {
                case HiDealDeliveryType.Other:

                    #region 非商品類，如運費等 資料為信託準備中、已信託、ATM、核銷 可退貨

                    trustStatusList = new List<TrustStatus>
                                                    {
                                                        TrustStatus.Initial,
                                                        TrustStatus.Trusted,
                                                        TrustStatus.ATM,
                                                        TrustStatus.Verified
                                                    };

                    #endregion 非商品類，如運費等 資料為信託準備中、已信託、ATM、核銷 可退貨

                    break;

                case HiDealDeliveryType.ToShop:

                    #region 到店-憑證為信託準備中、已信託、ATM 才可退貨

                    trustStatusList = new List<TrustStatus> { TrustStatus.Initial, TrustStatus.Trusted, TrustStatus.ATM };

                    #endregion 到店-憑證為信託準備中、已信託、ATM 才可退貨

                    break;

                case HiDealDeliveryType.ToHouse:

                    #region 到府-憑證為信託準備中、已信託、ATM、核銷 可退貨

                    //到府-憑證為信託準備中、已信託、ATM、核銷 可退貨
                    trustStatusList = new List<TrustStatus>
                                                    {
                                                        TrustStatus.Initial,
                                                        TrustStatus.Trusted,
                                                        TrustStatus.ATM,
                                                        TrustStatus.Verified
                                                    };

                    #endregion 到府-憑證為信託準備中、已信託、ATM、核銷 可退貨

                    break;
            }
            return trustStatusList;
        }

        /// <summary>
        /// 檢查傳入的order與orderDetail是否符合全部退貨條件(全部退貨-有任一尚未退貨的商品不合規定則回覆false)
        /// 2012-09-25備註，強制退貨目前只針對宅配商品超過鑑賞期部分做設定，未來到店商品有強制退貨需求再調整。
        /// </summary>
        /// <param name="order"></param>
        /// <param name="isEnforce">是否為強制退貨狀態，若為true，則只檢查訂單狀態，與目前已退貨的狀態。</param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool CheckOrderDataForAllReturn(HiDealOrder order, bool isEnforce, out string message)
        {
            HiDealOrderDetailCollection rtnDetails;
            return CheckOrderDataForAllReturn(order, isEnforce, out message, out rtnDetails);
        }

        /// <summary>
        /// 檢查傳入的order與orderDetail是否符合全部退貨條件(全部退貨-有任一尚未退貨的商品不合規定則回覆false)
        /// 如果回傳true，則透過rtnDetails回傳可退貨的HiDetailOrderDetail資料
        /// 2012-09-25備註，強制退貨目前只針對宅配商品超過鑑賞期部分做設定，未來到店商品有強制退貨需求再調整。
        /// </summary>
        /// <param name="order"></param>
        /// <param name="isEnforce">是否為強制退貨狀態，若為true，則只檢查訂單狀態，與目前已退貨的狀態。</param>
        /// <param name="message"></param>
        /// <param name="rtnDetails">可退貨的HiDetailOrderDetail資料</param>
        /// <returns></returns>
        public static bool CheckOrderDataForAllReturn(HiDealOrder order, bool isEnforce, out string message, out HiDealOrderDetailCollection rtnDetails)
        {
            message = string.Empty;

            rtnDetails = new HiDealOrderDetailCollection();

            #region 檢查訂單是否存在。

            if (order == null || !order.IsLoaded)
            {
                //錯誤-訂單不存在
                message = "查無此訂單，無法申請。";
                return false;
            }

            #endregion 檢查訂單是否存在。

            #region 檢查訂單是否為完成單

            switch ((HiDealOrderStatus)order.OrderStatus)
            {
                case HiDealOrderStatus.Create:
                    message = "未完成付款，無法申請。";
                    break;

                case HiDealOrderStatus.Confirm:
                    message = "未完成付款，無法申請。";
                    break;

                case HiDealOrderStatus.Completed:
                    break;

                case HiDealOrderStatus.Cancel:
                    message = "訂單已取消，無法申請。";
                    break;

                default:
                    message = "訂單尚未成立，無法申請。";
                    break;
            }

            if (!string.IsNullOrWhiteSpace(message))
            {
                return false;
            }

            #endregion 檢查訂單是否為完成單

            #region 檢查有無商品可退貨

            //訂單所包含的商品明細
            var orderDetails = hp.HiDealOrderDetailGetListByOrderGuid(order.Guid);
            if (orderDetails.Count == 0)
            {
                message = "查無此訂單，無法申請。";
                return false;
            }
            //取出已退貨或已申請退貨的資料
            var oldReturnStatus = new List<HiDealReturnedStatus> { HiDealReturnedStatus.Create, HiDealReturnedStatus.Completed };
            var oldHiDealReturneds = hp.ViewHiDealReturnedGetList(order.Pk, oldReturnStatus);

            foreach (var orderDetail in orderDetails.Where(x => x.ProductType == (int)HiDealProductType.Product))
            {
                //已退或已申請的商品數量
                var returnedQuant = oldHiDealReturneds.Where(x => x.OrderDetailGuid == orderDetail.Guid).Sum(x => x.ItemQuantity);

                //確認商品憑證是否有已核銷的部分 for 憑證
                int verifiedItemQuantity = 0;

                if (orderDetail.DeliveryType == (int)HiDealDeliveryType.ToShop)
                {
                    var coupons = hp.ViewHiDealCouponTrustStatusGetList(0, 0, ViewHiDealCouponTrustStatus.Columns.Sequence,
                                                     ViewHiDealCouponTrustStatus.Columns.OrderPk + "=" + order.Pk
                                                    , ViewHiDealCouponTrustStatus.Columns.ProductId + "=" + orderDetail.ProductId
                                                    );
                    verifiedItemQuantity = coupons.Where(x => x.LogSataus == (int)TrustStatus.Verified).Count();

                    //憑證已使用完
                    if (verifiedItemQuantity == orderDetail.ItemQuantity)
                    {
                        message = "訂單憑證已使用完，無法申請。";
                        return false;
                    }

                }

                //已退或已申請退貨的商品數量 不等於 購買數量，表示還有商品可以退貨
                if ((returnedQuant + verifiedItemQuantity) != orderDetail.ItemQuantity)
                {
                    //取得商品資料
                    var product = hp.HiDealProductGet(orderDetail.ProductId);

                    #region 宅配商品檢查是否在可退貨日期，強制退貨則跳過此檢查。

                    if (((HiDealDeliveryType)orderDetail.DeliveryType == HiDealDeliveryType.ToHouse) && (!isEnforce))
                    {
                        //最後退貨期限
                        if (CheckIsOverTrialPeriod(order.Guid))
                        {
                            //全退的判定，只要有任一商品不能退貨就回覆錯誤訊息
                            message = "商品已過鑑賞期，無法申請。";
                            return false;
                        }
                    }

                    #endregion 宅配商品檢查是否在可退貨日期，強制退貨則跳過此檢查。

                    #region 到店商品檢查是否在可退貨日期

                    if (product.IsDaysNoRefund)
                    {
                        //最後退貨期限
                        HiDealDeal deal = hp.HiDealDealGet(order.HiDealId);
                        DateTime lastReturnTime = deal.DealEndTime.Value.AddDays(config.ProductRefundDays);
                        if (lastReturnTime < DateTime.Now)
                        {
                            message = "已過可退貨時間，無法退貨。";
                            return false;
                        }
                    }

                    #endregion 到店商品檢查是否在可退貨日期

                    rtnDetails.Add(orderDetail);
                }
            }

            if (rtnDetails.Count == 0)
            {
                //沒有商品可退貨，且退貨單都為完成單，表示訂單已完全退貨，顯示訂單已取消
                if (oldHiDealReturneds.Where(x => x.ReturnedStatus == (int)HiDealReturnedStatus.Completed).Count() == oldHiDealReturneds.Count)
                {
                    message = "已取消的訂單，無法申請。";
                }
                else
                {
                    message = "訂單正在退貨處理中，無法申請。";
                }
                return false;
            }

            #endregion 檢查有無商品可退貨

            return true;
        }

        #region 檢查該訂單商品是否已過鑑賞期

        public static bool CheckIsOverTrialPeriod(Guid orderGuid)
        {
            //取得product id
            HiDealOrderDetail hdod = hp.HiDealOrderDetailGetListByOrderGuid(orderGuid).FirstOrDefault();
            if (hdod == null || !hdod.IsLoaded)
            {
                throw new InvalidOperationException(string.Format("order not found! order_guid: {0}", orderGuid.ToString()));
            }
            int productId = hdod.ProductId;

            //取得商品資料
            HiDealProduct product = hp.HiDealProductGet(productId);
            if (product == null || !product.IsLoaded)
            {
                throw new InvalidOperationException(string.Format("product not found in hi_deal_product! product_id: {0}", productId.ToString()));
            }
            //檔次配送結束時間
            DateTime? deliverEndTime = product.UseEndTime;
            //檔次選擇的對帳方式
            int billingModel = product.VendorBillingModel;

            //商品檔鑑賞期判斷需區別新/舊對帳方式而有所不同
            //舊對帳方式
            if (billingModel.Equals((int)VendorBillingModel.None))
            {
                if (deliverEndTime != null && deliverEndTime.Value.AddDays(config.GoodsAppreciationPeriod).Date < DateTime.Now.Date)
                {
                    return true;
                }
            }
            //新對帳方式:過鑑賞期判斷- 出貨日期+出貨緩衝時間 < 今天
            else if (billingModel.Equals((int)VendorBillingModel.BalanceSheetSystem))
            {
                //訂單出貨日期
                DateTime? shipTime;
                //透過order_guid抓取最後一筆出貨紀錄 (**若日後換貨之出貨若開發完成，出貨日期判斷須一併更新**)
                OrderShip os = op.OrderShipGetListByOrderGuid(orderGuid).FirstOrDefault(x => x.Type == (int)OrderShipType.Normal);
                int shippingBuffer = config.ShippingBuffer;
                if (os != null)
                {
                    shipTime = os.ReceiptTime.HasValue ? os.ReceiptTime : os.ShipTime;
                    shippingBuffer = os.ReceiptTime.HasValue ? config.GoodsAppreciationPeriod : shippingBuffer;
                }
                else
                {
                    shipTime = null;
                }

                if (shipTime != null && shipTime.Value.AddDays(shippingBuffer).Date < DateTime.Now.Date)
                {
                    return true;
                }
            }

            return false;
        }

        #endregion 檢查該訂單商品是否已過鑑賞期

        /// <summary>
        /// 取消某張訂單的所有退貨。
        /// </summary>
        /// <param name="order">要取消退貨的訂單</param>
        /// <param name="userName">執行此項作業的人員(不一定是會員)</param>
        /// <param name="cancelReason">取消原因</param>
        /// <param name="message">回復訊息</param>
        /// <returns></returns>
        public static bool ReturnedCancelWork(HiDealOrder order, string userName, string cancelReason, out string message)
        {
            message = string.Empty;
            //檢查有無新申請的退貨單。
            HiDealReturnedCollection returneds = hp.HiDealReturnedGetList(0, 0, HiDealReturned.Columns.ApplicationTime,
                                     HiDealReturned.Columns.OrderPk + " = " + order.Pk, HiDealReturned.Columns.Status + " = " + (int)HiDealReturnedStatus.Create);
            //沒有，傳回失敗，顯示錯誤訊息。
            if (returneds.Count == 0)
            {
                message = "沒有可取消的退貨單。";
                return false;
            }

            foreach (var returned in returneds)
            {
                returned.Status = (int)HiDealReturnedStatus.Cancel;
                returned.CancelReason = cancelReason;
                returned.CancelTime = DateTime.Now;
                returned.CancelUserName = userName;

                //如果是取消憑證退貨單，如有選項須扣回數字
                var returnedDetailList = hp.HiDealReturnedDetailGetListByReturnedId(returned.Id);
                foreach (var returnedDetail in returnedDetailList.Where(x => x.ProductType == (int)HiDealProductType.Product && x.DeliveryType == (int)HiDealDeliveryType.ToShop))
                {
                    var returnedOptionIds = ReturnedDetailGetReturnedOptionIds(returnedDetail);
                    //有選項資料
                    if (returnedOptionIds.Count > 0)
                    {
                        hp.HiDealProductOptionItemUpdateSellQuantity(returnedOptionIds, returnedDetail.ItemQuantity);
                    }
                }
            }




            hp.HiDealReturnedSetList(returneds);
            //orderShow顯示狀況，修改為正常
            HiDealOrderFacade.HiDealOrderShowUpdate(order.Pk, HiDealOrderShowStatus.Create, true);
            return true;
        }

        /// <summary>
        /// 新成立退貨單，退購物金改為刷退
        /// </summary>
        /// <returns></returns>
        public static bool NewReturnedOrderChangeToReturnCash(int returnedId, string userName, out string message)
        {
            message = string.Empty;

            #region 資料檢查

            var returned = hp.HiDealReturnedGet(returnedId);
            //判斷查無資料
            if (!returned.IsLoaded)
            {
                message = "查無退貨單。";
                return false;
            }

            //判斷目前是否為刷退，若是甚麼也不做
            if (returned.ReturnCashBack)
            {
                message = "已刷退。";
                return false;
            }

            if ((HiDealReturnedStatus)returned.Status != HiDealReturnedStatus.Create)
            {
                message = "新建立退貨單才可轉換。";
                return false;
            }

            #endregion 資料檢查

            #region 修改申請紀錄為刷退

            returned.CashBack = true;
            returned.ReturnCashBack = true;
            returned.ModifyId = userName;
            returned.ModifyTime = DateTime.Now;
            hp.HiDealReturnedSet(returned);

            #endregion 修改申請紀錄為刷退

            return true;
        }

        /// <summary>
        /// 退貨作業，退貨完成之退貨單轉刷退。
        /// </summary>
        /// <returns></returns>
        public static bool RefundWorkReturnPointToReturnCash(int returnedId, string userName, out string message)
        {
            message = string.Empty;

            #region 資料檢查

            var returned = hp.HiDealReturnedGet(returnedId);
            //判斷查無資料
            if (!returned.IsLoaded)
            {
                message = "查無退貨單。";
                return false;
            }

            //判斷目前是否為刷退，若是甚麼也不做
            if (returned.ReturnCashBack)
            {
                message = "已刷退。";
                return false;
            }

            if ((HiDealReturnedStatus)returned.Status != HiDealReturnedStatus.Completed)
            {
                message = "需退貨完成才可進行轉換";
                return false;
            }

            #endregion 資料檢查

            #region 開始退款作業

            //尚未刷退，將狀態改為刷退，並進行刷退作業
            //要進行刷退的資料邏輯為:有申請要退貨的coupon，且目前CashTrustLog紀錄為退貨的訂單
            //查詢登記要退貨的coupon資料
            var returnedCoupons = hp.HiDealReturnedCouponGetList(returnedId);
            var returnCouponList = returnedCoupons.Select(coupons => coupons.CouponId).ToList();
            //查詢訂單資料
            var hidealOrder = hp.HiDealOrderGet(returned.OrderPk);
            //先取出此訂單所有的CashTrustLog，減少分次查詢的壓力
            var cashTrustLogs = mp.CashTrustLogGetListByOrderGuid(hidealOrder.Guid, OrderClassification.HiDeal);
            //逐筆處理將cashTrustLog刷退
            decimal returnCreditCard = 0, returnAtm = 0;
            var returnTrustLog = cashTrustLogs.Where(x => x.CouponId != null && (returnCouponList.Contains(x.CouponId.Value) && x.Status == (int)TrustStatus.Returned)).ToList();
            //預定刷退的信用卡金額
            var creditCardAmount = returnTrustLog.Sum(x => x.CreditCard);
            //預定刷退的信用卡金額
            var atmAmount = returnTrustLog.Sum(x => x.Atm);

            #region 檢查是否有足夠的購物金

            var userScash = OrderFacade.GetSCashSum(returned.UserId);
            if (userScash < creditCardAmount + atmAmount)
            {
                message = "17life購物金不足，無法退貨。";
                return false;
            }

            #endregion 檢查是否有足夠的購物金

            #region 購物金轉刷退，須新增應收應付

            //金額未定所以都先設為0
            var receivable = new Receivable();
            receivable.ClassificationId = returned.Id;
            receivable.AccountsClassification = (int)AccountsClassification.HiDealReturn;
            receivable.TotalAmount = creditCardAmount + atmAmount;
            receivable.Pcash = 0;
            receivable.Scash = creditCardAmount + atmAmount;
            receivable.Bcash = 0;
            receivable.CreditCardAmt = 0;
            receivable.DiscountAmount = 0;
            receivable.AtmAmount = 0;
            receivable.ReceivableStatus = (int)ReceivableStatus.NotReceived;
            receivable.InvoiceId = null;
            receivable.CreateId = userName;
            receivable.CreateTime = DateTime.Now;

            var payable = new Payable();
            payable.ClassificationId = returned.Id;
            payable.AccountsClassification = (int)AccountsClassification.HiDealReturn;
            payable.TotalAmount = creditCardAmount + atmAmount;
            payable.Pcash = 0;
            payable.Scash = 0;
            payable.Bcash = 0;
            payable.CreditCardAmt = creditCardAmount;
            payable.DiscountAmount = 0;
            payable.AtmAmount = atmAmount;
            payable.PayableStatus = (int)PayableStatus.NotPaid;
            payable.InvoiceId = null;
            payable.CreateId = userName;
            payable.CreateTime = DateTime.Now;

            #endregion 購物金轉刷退，須新增應收應付

            #region 逐筆進行退貨作業

            foreach (var cashTrustLog in returnTrustLog)
            {
                if (RefundCreditcardByCouponForTrust(cashTrustLog, returned.UserId, userName))
                {
                    returnCreditCard += cashTrustLog.CreditCard;
                    returnAtm += cashTrustLog.Atm;

                    #region 收購物金 退現金

                    // 現金退款成功，紀錄應收
                    receivable.CompleteTotalAmount += cashTrustLog.CreditCard + cashTrustLog.Atm;
                    receivable.CompleteScash += cashTrustLog.CreditCard + cashTrustLog.Atm;
                    // 現金退款成功，紀錄應收
                    payable.CompleteTotalAmount += cashTrustLog.CreditCard + cashTrustLog.Atm;
                    payable.CompleteCreditCardAmt += cashTrustLog.CreditCard;
                    payable.CompleteAtmAmount += cashTrustLog.Atm;

                    #endregion 收購物金 退現金
                }
            }

            #endregion 逐筆進行退貨作業

            #region 退貨完成，處理退貨單資料

            returned.CashBack = true;
            returned.ReturnCashBack = true;
            returned.Status = (int)HiDealReturnedStatus.Completed;
            returned.ReturnScash = returned.ReturnScash - receivable.CompleteScash;
            returned.ReturnCreditCardAmt = returnCreditCard;
            returned.ReturnAtmAmount = returnAtm;
            returned.ModifyId = userName;
            returned.ModifyTime = DateTime.Now;
            returned.CashBackTime = DateTime.Now;
            hp.HiDealReturnedSet(returned);

            #endregion 退貨完成，處理退貨單資料

            #region 退款完成，儲存應收應付帳款資料

            if (payable.TotalAmount == payable.CompleteTotalAmount)
            {
                payable.PayableStatus = (int)PayableStatus.CompletePaid;
            }
            else if (payable.CompleteTotalAmount > 0)
            {
                payable.PayableStatus = (int)PayableStatus.PartialPaid;
            }

            if (receivable.TotalAmount == receivable.CompleteTotalAmount)
            {
                receivable.ReceivableStatus = (int)ReceivableStatus.CompleteReceived;
            }
            else if (receivable.CompleteTotalAmount > 0)
            {
                receivable.ReceivableStatus = (int)ReceivableStatus.PartialReceived;
            }
            ap.ReceivableSet(receivable);
            ap.PayableSet(payable);

            #endregion 退款完成，儲存應收應付帳款資料

            #endregion 開始退款作業

            return true;
        }

        /// <summary>
        /// 退款作業-依據退貨單編號與現實狀況，開始進行退款作業。
        /// </summary>
        /// <param name="returnedId">退貨單編號</param>
        /// <param name="userName">使用者名稱</param>
        /// <param name="allpass">是否檢查單據已繳回，true 都不檢查，false 則要檢查</param>
        /// <param name="onlyVirtualCurrencyBack">若為true，不論申請是否要退回現金，一律只允許退回購物金。</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns></returns>
        public static bool RefundWork(int returnedId, string userName, bool allpass, bool onlyVirtualCurrencyBack, out string message)
        {
            message = string.Empty;
            //查詢退貨單
            var returned = hp.HiDealReturnedGet(returnedId);
            if (!returned.IsLoaded)
            {
                message = "查無退貨單。";
                return false;
            }

            //檢查是否全紅利金(bcash)購買，無發票檔產生
            bool isBcashPayTotal = (returned.TotalAmount == returned.Bcash);

            //查詢退貨明細
            var returnedDetails = hp.HiDealReturnedDetailGetListByReturnedId(returnedId);
            if (returnedDetails.Count == 0)
            {
                message = "查無退貨單明細。";
                return false;
            }

            //檔次名稱，發通知信用的，由於目前訂單只會有一個商品，所以也只列一個檔次名稱。
            string dealName = string.Empty;
            string productName = string.Empty;
            foreach (var returnedDetail in returnedDetails)
            {
                var deal = hp.HiDealDealGet(returnedDetail.HiDealId);
                if (returnedDetail.ProductType == (int)HiDealProductType.Product)
                {
                    dealName = deal.Name;
                    productName = returnedDetail.ProductName;
                }
            }
            //查詢訂單資料
            var hidealOrder = hp.HiDealOrderGet(returned.OrderPk);
            //查詢登記要退貨的coupon資料
            var returnedCoupons = hp.HiDealReturnedCouponGetList(returnedId);

            //判斷是否有收回折讓單，直接退貨
            if (!allpass && !isBcashPayTotal)
            {
                EinvoiceMainCollection einvoices = op.EinvoiceMainGetListByOrderGuid(hidealOrder.Guid);

                foreach (var einvoice in einvoices)
                {
                    if (einvoice.CouponId.HasValue)
                    {
                        var cashtrustlog = mp.CashTrustLogGetByCouponId(einvoice.CouponId.Value,
                                                                        OrderClassification.HiDeal);

                        if (cashtrustlog.Status >= (int)TrustStatus.Verified)
                        {
                            continue;
                        }
                    }

                    if (einvoice.InvoiceStatus == (int)EinvoiceType.Initial && string.IsNullOrEmpty(einvoice.InvoiceNumber))
                    {
                        //未開立發票，InvoiceNmuber還未寫入，直接進入退款流程
                    }
                    else if (einvoice.InvoiceStatus == (int)EinvoiceType.C0401)
                    {
                        //已開立發票
                        if (einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate)
                        {
                            //三聯式發票
                            if ((einvoice.InvoiceMailbackAllowance == null) || (!einvoice.InvoiceMailbackAllowance.Value))
                            {
                                //折讓單未繳回
                                message = "折讓單未繳回";
                                return false;
                            }
                        }
                        else
                        {
                            //二聯式&捐贈
                            //判斷是否跨期 或有開紙本發票
                            DateTime dt = (einvoice.InvoiceNumberTime != null) ? (DateTime)einvoice.InvoiceNumberTime : einvoice.InvoiceNumberTime.GetValueOrDefault();
                            if (IsInvoiceExceedPeriod(dt) || einvoice.InvoicePapered)
                            {
                                if ((einvoice.InvoiceMailbackAllowance == null) || (!einvoice.InvoiceMailbackAllowance.Value))
                                {
                                    //折讓單未繳回
                                    message = "折讓單未繳回";
                                    return false;
                                }
                            }
                        }
                    }
                    else
                    {
                        message = "查無發票資料。";
                        return false;
                    }
                }
            }


            //開始退款作業
            //先取出此訂單所有的CashTrustLog，減少分次查詢的壓力
            var cashTrustLogs = mp.CashTrustLogGetListByOrderGuid(hidealOrder.Guid, OrderClassification.HiDeal);
            //如果傳入參數是限定退購物金，則 returnCashBack的判斷設為false(退購物金)，否則依據退貨單預設的ReturnCashBack欄位值為準。
            bool returnCashBack = onlyVirtualCurrencyBack ? false : returned.ReturnCashBack;

            #region 整理所以要處理的CashTrustLog

            //可以進行退貨的所有cashtrustLog
            var cashTrustLogsForReturn = new List<CashTrustLog>();
            foreach (var returnedDetail in returnedDetails)
            {
                //運費另外處理
                if (returnedDetail.ProductType == (int)HiDealProductType.Freight)
                {
                    continue;
                }
                var allowTrustStatus = GetTrustStatusWithAllowReturns((HiDealDeliveryType)returnedDetail.DeliveryType);
                var returnCouponIds = (from returnedCoupon in returnedCoupons
                                       where returnedCoupon.ReturnedDetailId == returnedDetail.Id
                                       select returnedCoupon.CouponId).ToList();
                var trustLogs =
                    cashTrustLogs.Where(
                        x =>
                        x.CouponId != null &&
                        (allowTrustStatus.Contains((TrustStatus)x.Status) &&
                         returnCouponIds.Contains((long)x.CouponId))).ToList();
                cashTrustLogsForReturn.AddRange(trustLogs);
            }
            //檢查是否要退運費
            //有運費的退貨申請紀錄，且 目前要退的商品數量與該訂單商品數量一樣多
            if ((returnedDetails.Where(x => x.ProductType == (int)HiDealProductType.Freight).Count() > 0) &&
                (cashTrustLogs.Where(x => x.SpecialStatus != (int)TrustSpecialStatus.Freight).Count() ==
                 cashTrustLogsForReturn.Count))
            {
                cashTrustLogsForReturn.AddRange(cashTrustLogs.Where(x => x.SpecialStatus == (int)TrustSpecialStatus.Freight));
            }

            //處理商品選項已販售數目回補，憑證已在退貨申請成功時已補入。
            foreach (var returnedDetail in returnedDetails)
            {
                var returnedOptionIds = ReturnedDetailGetReturnedOptionIds(returnedDetail);
                if (returnedDetail.ProductType == (int)HiDealProductType.Product && returnedOptionIds.Count > 0 && returnedDetail.StoreGuid == null)
                {
                    hp.HiDealProductOptionItemUpdateSellQuantity(returnedOptionIds, (-returnedDetail.ItemQuantity));
                }
            }

            #endregion 整理所以要處理的CashTrustLog

            #region 統計要退的商品產生應付帳款

            var pcash = cashTrustLogsForReturn.Sum(x => x.Pcash);
            var scash = cashTrustLogsForReturn.Sum(x => x.Scash);
            var bcash = cashTrustLogsForReturn.Sum(x => x.Bcash);
            var creditCardAmt = cashTrustLogsForReturn.Sum(x => x.CreditCard);
            var atmAmount = cashTrustLogsForReturn.Sum(x => x.Atm);
            var total = pcash + scash + bcash + creditCardAmt + atmAmount;
            var completeTotalAmount = 0;
            var completePCash = 0;
            var completeSCash = 0;
            var completeBCash = 0;
            var completeCreditCard = 0;
            var completeAtm = 0;
            //退購物金的應付款紀錄
            var payableSCash = new Payable();
            payableSCash.ClassificationId = returned.Id;
            payableSCash.AccountsClassification = (int)AccountsClassification.HiDealReturn;
            payableSCash.TotalAmount = total;
            payableSCash.Pcash = pcash;
            payableSCash.Scash = scash + creditCardAmt + atmAmount; //現金支付的部分一律退回購物金
            payableSCash.Bcash = bcash;
            payableSCash.CreditCardAmt = 0;
            payableSCash.DiscountAmount = 0;
            payableSCash.AtmAmount = 0;
            payableSCash.PayableStatus = (int)PayableStatus.NotPaid;
            payableSCash.InvoiceId = null;
            payableSCash.CreateId = userName;
            payableSCash.CreateTime = DateTime.Now;
            //判斷是否退還現金
            var payableCashBack = new Payable();
            var receivableCashBack = new Receivable();
            if (returnCashBack)
            {
                //收購物金退回現金

                receivableCashBack.ClassificationId = returned.Id;
                receivableCashBack.AccountsClassification = (int)AccountsClassification.HiDealReturn;
                receivableCashBack.TotalAmount = creditCardAmt + atmAmount;
                receivableCashBack.Pcash = 0;
                receivableCashBack.Scash = creditCardAmt + atmAmount;
                receivableCashBack.Bcash = 0;
                receivableCashBack.CreditCardAmt = 0;
                receivableCashBack.DiscountAmount = 0;
                receivableCashBack.AtmAmount = 0;
                receivableCashBack.ReceivableStatus = (int)ReceivableStatus.NotReceived;
                receivableCashBack.InvoiceId = null;
                receivableCashBack.CreateId = userName;
                receivableCashBack.CreateTime = DateTime.Now;

                payableCashBack.ClassificationId = returned.Id;
                payableCashBack.AccountsClassification = (int)AccountsClassification.HiDealReturn;
                payableCashBack.TotalAmount = creditCardAmt + atmAmount;
                payableCashBack.Pcash = 0;
                payableCashBack.Scash = 0;
                payableCashBack.Bcash = 0;
                payableCashBack.CreditCardAmt = creditCardAmt;
                payableCashBack.DiscountAmount = 0;
                payableCashBack.AtmAmount = atmAmount;
                payableCashBack.PayableStatus = (int)PayableStatus.NotPaid;
                payableCashBack.InvoiceId = null;
                payableCashBack.CreateId = userName;
                payableCashBack.CreateTime = DateTime.Now;
            }

            #endregion 統計要退的商品產生應付帳款

            #region 逐筆處理每個憑證退貨

            if (cashTrustLogsForReturn.Count == 0)
            {
                message = "查無可退貨憑證。";
                returned.FailReason = message;
                returned.FailTime = DateTime.Now;
                returned.Status = (int)HiDealReturnedStatus.Fail;
                hp.HiDealReturnedSet(returned);

                return false;
            }

            foreach (var cashTrustLog in cashTrustLogsForReturn)
            {
                if (CancelPaymentTransactionByCouponForTrust(cashTrustLog, returned.UserId, userName))
                {
                    //退款成功，處理應付帳款
                    payableSCash.CompleteTotalAmount += cashTrustLog.Pcash + cashTrustLog.Scash +
                                                        cashTrustLog.CreditCard + cashTrustLog.Atm +
                                                        cashTrustLog.Bcash;
                    payableSCash.CompletePcash += cashTrustLog.Pcash;
                    payableSCash.CompleteScash += cashTrustLog.Scash + cashTrustLog.CreditCard + cashTrustLog.Atm;
                    payableSCash.CompleteBcash += cashTrustLog.Bcash;
                    payableSCash.CompleteCreditCardAmt = 0;
                    payableSCash.CompleteAtmAmount = 0;

                    completeTotalAmount += cashTrustLog.Pcash + cashTrustLog.Scash + cashTrustLog.CreditCard +
                                           cashTrustLog.Atm + cashTrustLog.Bcash;
                    completePCash += cashTrustLog.Pcash;
                    completeSCash += cashTrustLog.Scash + cashTrustLog.CreditCard + cashTrustLog.Atm;
                    completeBCash += cashTrustLog.Bcash;
                    //退回現金的作業。
                    if (returnCashBack)
                    {
                        if (RefundCreditcardByCouponForTrust(cashTrustLog, returned.UserId, userName))
                        {
                            #region 收購物金 退現金

                            // 現金退款成功，紀錄應收
                            receivableCashBack.CompleteTotalAmount += cashTrustLog.CreditCard + cashTrustLog.Atm;
                            receivableCashBack.CompleteScash += cashTrustLog.CreditCard + cashTrustLog.Atm;
                            // 現金退款成功，紀錄應收
                            payableCashBack.CompleteTotalAmount += cashTrustLog.CreditCard + cashTrustLog.Atm;
                            payableCashBack.CompleteCreditCardAmt += cashTrustLog.CreditCard;
                            payableCashBack.CompleteAtmAmount += cashTrustLog.Atm;

                            #endregion 收購物金 退現金

                            completeSCash -= (cashTrustLog.CreditCard + cashTrustLog.Atm);
                            completeCreditCard += cashTrustLog.CreditCard;
                            completeAtm += cashTrustLog.Atm;
                        }
                    }
                    //退貨單明細的修改
                    var returnedDetail =
                        returnedDetails.Where(x => x.OrderDetailGuid == cashTrustLog.OrderDetailGuid).First();
                    returnedDetail.ReturnItemQuantity += 1;
                    returnedDetail.ReturnDetailTotalAmt += cashTrustLog.Pcash + cashTrustLog.Scash +
                                                           cashTrustLog.CreditCard + cashTrustLog.Atm +
                                                           cashTrustLog.Bcash;

                    //憑證作廢
                    if (cashTrustLog.CouponId != null)
                    {
                        HiDealCouponManager.HiDealOneCouponChangeNewOne((long)cashTrustLog.CouponId);
                    }
                }
            }

            #endregion 逐筆處理每個憑證退貨

            #region 退貨完成，處理退貨單資料

            returned.Status = (int)HiDealReturnedStatus.Completed;
            returned.ReturnCashBack = returnCashBack; //紀錄最後實際退現金否的狀態
            returned.ReturnTotalAmount = completeTotalAmount;
            returned.ReturnPcash = completePCash;
            returned.ReturnScash = completeSCash;
            returned.ReturnBcash = completeBCash;
            returned.ReturnCreditCardAmt = completeCreditCard;
            returned.ReturnDiscountAmount = 0;
            returned.ReturnAtmAmount = completeAtm;
            returned.ModifyId = userName;
            returned.ModifyTime = DateTime.Now;
            if (returnCashBack)
            {
                returned.CashBackTime = DateTime.Now;
            }
            returned.ReviewUserName = userName;
            returned.CompletedTime = DateTime.Now;

            hp.HiDealReturnedSet(returned);

            foreach (var returnedDetail in returnedDetails)
            {
                returnedDetail.ModifyId = userName;
                returnedDetail.ModifyTime = DateTime.Now;
            }
            hp.HiDealReturnedDetailSetList(returnedDetails);

            #endregion 退貨完成，處理退貨單資料

            #region 退款完成，儲存應收應付帳款資料

            if (payableSCash.TotalAmount == payableSCash.CompleteTotalAmount)
            {
                payableSCash.PayableStatus = (int)PayableStatus.CompletePaid;
            }
            else if (payableSCash.CompleteTotalAmount > 0)
            {
                payableSCash.PayableStatus = (int)PayableStatus.PartialPaid;
            }

            if (payableCashBack.TotalAmount == payableCashBack.CompleteTotalAmount)
            {
                payableCashBack.PayableStatus = (int)PayableStatus.CompletePaid;
            }
            else if (payableCashBack.CompleteTotalAmount > 0)
            {
                payableCashBack.PayableStatus = (int)PayableStatus.PartialPaid;
            }

            if (receivableCashBack.TotalAmount == receivableCashBack.CompleteTotalAmount)
            {
                receivableCashBack.ReceivableStatus = (int)ReceivableStatus.CompleteReceived;
            }
            else if (receivableCashBack.CompleteTotalAmount > 0)
            {
                receivableCashBack.ReceivableStatus = (int)ReceivableStatus.PartialReceived;
            }

            var payables = new PayableCollection();
            payables.Add(payableSCash);
            payables.Add(payableCashBack);
            ap.PayableSetList(payables);
            ap.ReceivableSet(receivableCashBack);

            #endregion 退款完成，儲存應收應付帳款資料

            #region 退貨完成，寄發退貨成功通知

            Member member = mp.MemberGet(returned.UserId);
            var refundSuccessNotificationInformation = new RefundSuccessNotificationInformation
            {
                AtmCharged = completeAtm,
                BCash = completeBCash,
                BuyerName = member.DisplayName,
                CreditCardCharged = completeCreditCard,
                OrderItemsCount =
                    returnedDetails.Where(
                        x =>
                        x.ProductType ==
                        (int)HiDealProductType.Product)
                                   .Sum(x => x.ReturnItemQuantity),
                PCash = completePCash,
                DCash = 0,
                RefundDetailUrl =
                    config.SiteUrl +
                    "/piinlife/member/CouponList.aspx?opk=" +
                    returned.OrderPk,
                RefundItemName =
                    string.Format("{0} {1}", dealName, productName),
                RefundItemsCount =
                    returnedDetails.Where(
                        x =>
                        x.ProductType ==
                        (int)HiDealProductType.Product)
                                   .Sum(x => x.ReturnItemQuantity),
                SCash = completeSCash,
                TotalRefundAmount = completeTotalAmount,
                HDmailHeaderUrl =
                    config.SiteUrl +
                    "/Themes/HighDeal/images/mail/HDmail_Header.png",
                HDmailFooterUrl =
                    config.SiteUrl +
                    "/Themes/HighDeal/images/mail/HDmail_Footer.png"
            };
            string userEmail = MemberFacade.GetUserEmail(returned.UserId);
            HiDealMailFacade.SendRefundSuccessNotification(userEmail, refundSuccessNotificationInformation);

            #endregion 退貨完成，寄發退貨成功通知

            return true;
        }

        /// <summary>
        /// 自動退貨作業，由於退貨資料會累積於系統中，
        /// 針對所有退貨單(不論發票與退貨申請書是否寄回)，於一段時間後，全部自動完成退貨，或檢查錯誤後設定為退貨失敗。
        /// 20130624 品生活退貨規則調整  Sam
        /// 1.商品檔不適用自動退貨機制，除非客服使用自動退貨
        /// 2.退購物金的訂單(無發票)，直接進入退貨。
        /// 3.刷退的訂單(未開發票)，直接進入退貨。
        /// 4.刷退的訂單(開發票)，需檢查折讓單是否已繳回，是否有索取紙本發票，有的話需繳回，才退貨，無紙本發票即進行退貨。
        /// </summary>
        /// <returns>退貨失敗筆數。</returns>
        public static int AutoRefundWork()
        {
            var returneds = hp.HiDealReturnedGetList(0, 0, HiDealReturned.Columns.CreateTime,
                                     HiDealReturned.Columns.Status + " = " + (int)HiDealReturnedStatus.Create
                                     , HiDealReturned.Columns.ApplicationTime + " < " + DateTime.Now.Date.AddDays(-1 * config.ReturnWaitingDays));

            int failCount = 0;
            foreach (var returned in returneds)
            {
                var returnedDetails = hp.HiDealReturnedDetailGetListByReturnedId(returned.Id);
                HiDealReturnedDetail returnedDetail = returnedDetails.Where(x => x.ProductType == (int)HiDealProductType.Product).First();

                //憑證檔可自動退，前台商品檔退貨一律不自動退，除非該商品檔退貨單設定為自動退貨(後台客服產生)，create_id = user_id=>前台申請、create_id != user_id=>後台客服協助產生"
                if (returnedDetail.DeliveryType == (int)HiDealDeliveryType.ToShop || (returnedDetail.DeliveryType == (int)HiDealDeliveryType.ToHouse && string.Compare(returned.CreateId, (mp.MemberGetbyUniqueId(returned.UserId)).UserName, true) != 0))
                {
                    if (returned.CashBack)
                    {
                        #region 刷退動作

                        string message;
                        if (!RefundWork(returned.Id, config.SystemEmail, false, false, out message))
                        {
                            if (string.Compare(message, "查無發票資料。", true) == 0 || string.Compare(message, "折讓單未繳回", true) == 0 || string.Compare(message, "發票未繳回", true) == 0)
                            {
                                //調整為Application 增加14天等待時間，等待相關文件補齊，不增加failCount。
                                var failReturned = hp.HiDealReturnedGet(returned.Id);
                                failReturned.ApplicationTime = DateTime.Now.Date; //改為現在日期，14天後再行判定。
                                hp.HiDealReturnedSet(failReturned);
                            }
                            else
                            {
                                //修改OrderShow退款失敗
                                HiDealOrderFacade.HiDealOrderShowUpdate(returned.OrderPk, HiDealOrderShowStatus.RefundFail, true);
                                failCount++;
                            }
                        }
                        else
                        {
                            //修改OrderShow退貨完成。
                            HiDealOrderFacade.HiDealOrderShowUpdate(returned.OrderPk, HiDealOrderShowStatus.RefundSuccess, true);
                        }

                        #endregion 刷退動作
                    }
                    else
                    {
                        #region 退購物金動作

                        string message;
                        //退貨作業，一律退回購物金。
                        if (!RefundWork(returned.Id, config.SystemEmail, false, true, out message))
                        {
                            //修改OrderShow退款失敗
                            HiDealOrderFacade.HiDealOrderShowUpdate(returned.OrderPk, HiDealOrderShowStatus.RefundFail, true);
                            failCount++;
                        }
                        else
                        {
                            //修改OrderShow退貨完成。
                            HiDealOrderFacade.HiDealOrderShowUpdate(returned.OrderPk, HiDealOrderShowStatus.RefundSuccess, true);
                        }

                        #endregion 退購物金動作
                    }

                }
            }
            return failCount;
        }

        /// <summary>
        /// 處理所有退款，退回17life購物金
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="memberName"></param>
        /// <param name="createId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        private static bool CancelPaymentTransactionByCouponForTrust(CashTrustLog ct, int userId, string createId)
        {
            PaymentTransactionCollection ptc = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                                PaymentTransaction.Columns.OrderGuid + " = " + ct.OrderGuid,
                                                PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Authorization,
                                                PaymentTransaction.Columns.OrderClassification + " = " + (int)OrderClassification.HiDeal);
            DepartmentTypes dType = (ptc.Count > 0) ? Helper.GetPaymentTransactionDepartmentTypes(ptc.First().Status) : DepartmentTypes.Ppon;
            if (ptc.Count() > 0 && ct.Amount > 0)
            {
                #region 處理購物金退回

                //ATM付款也要退回購物金，先不做

                //包含刷卡 與 單純購物金付款的部分
                if (ct.Scash + ct.CreditCard > 0)
                {
                    var status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                    PaymentFacade.NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.SCash,
                                                 ct.Scash + ct.CreditCard, null, PayTransType.Refund, DateTime.Now,
                                                 createId, "以購物金退貨", status, PayTransResponseType.OK, (OrderClassification)ct.OrderClassification);

                    #region New Scash

                    OrderFacade.ScashReturned(ct.Scash + ct.CreditCard, userId, ct.OrderGuid, "以購物金退貨:" + ct.ItemName, createId, OrderClassification.HiDeal, ct.UninvoicedAmount, IsUnivoicedAmountInvoiced(ct.CouponId));

                    #endregion New Scash

                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退購物金計NT$" + (ct.Scash + ct.CreditCard).ToString("F0"), createId, true);
                }

                #endregion 處理購物金退回

                #region 處理Pcash退回

                IEnumerable<PaymentTransaction> ptPcashCol = from i in ptc where (i.PaymentType == (int)PaymentType.PCash) select i;
                PaymentTransaction ptPcash = (ptPcashCol.Count() > 0) ? ptPcashCol.First() : null;
                if (ptPcash != null && ct.Pcash > 0)
                {
                    string pezResult = PCashWorker.DeCheckOut(ptc.First().TransId, ptPcash.AuthCode, ct.Pcash);
                    PayTransResponseType responseType = PayTransResponseType.OK;
                    PayTransPhase transPhase = PayTransPhase.Created;
                    if (pezResult == "0000")
                    {
                        transPhase = PayTransPhase.Successful;
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退PCash計NT$" + ct.Pcash.ToString("F0"), createId, true);
                    }
                    else
                    {
                        transPhase = PayTransPhase.Failed;
                        responseType = PayTransResponseType.GenericError;
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退PCash失敗" + pezResult, createId, true);
                    }
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), transPhase);
                    PaymentFacade.NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.PCash, ct.Pcash, ptPcash.AuthCode, PayTransType.Refund, DateTime.Now, createId, pezResult, status, responseType, (OrderClassification)ct.OrderClassification);
                }

                #endregion 處理Pcash退回

                #region 處理紅利退回

                if (ct.Bcash > 0)
                {
                    int exchangeRate = 10;
                    MemberPromotionDeposit addBonus = new MemberPromotionDeposit();
                    addBonus.OrderGuid = ct.OrderGuid;
                    addBonus.StartTime = DateTime.Now;
                    addBonus.ExpireTime = DateTime.Now.AddYears(2);
                    addBonus.PromotionValue = (double)ct.Bcash * exchangeRate;
                    addBonus.CreateTime = DateTime.Now;
                    addBonus.UserId = userId;
                    addBonus.Action = "品生活-取消訂單:" + ct.ItemName.TrimToMaxLength(20, "...");
                    addBonus.CreateId = "sys";
                    mp.MemberPromotionDepositSet(addBonus);

                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                    PaymentFacade.NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.BonusPoint, ct.Bcash, null,
                                                    PayTransType.Refund, DateTime.Now, createId, "以紅利退貨", status, PayTransResponseType.OK, (OrderClassification)ct.OrderClassification);
                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退紅利計NT$" + ct.Bcash.ToString("F0"), createId, true);
                }

                #endregion 處理紅利退回

                #region 信託紀錄

                #region CashTrustLog

                ct.MarkOld();
                ct.ModifyTime = DateTime.Now;
                ct.ReturnedTime = ct.ModifyTime;
                ct.Status = (int)TrustStatus.Returned;
                mp.CashTrustLogSet(ct);

                #endregion CashTrustLog

                #region CashTrustStatusLog

                CashTrustStatusLog ctStatus = new CashTrustStatusLog();
                ctStatus.TrustId = ct.TrustId;
                ctStatus.TrustProvider = ct.TrustProvider;
                ctStatus.Amount = ct.CreditCard + ct.Atm + ct.Scash;
                ctStatus.Status = (int)TrustStatus.Returned;
                ctStatus.ModifyTime = DateTime.Now;
                ctStatus.CreateId = createId;
                ctStatus.MarkNew();
                mp.CashTrustStatusLogSet(ctStatus);

                #endregion CashTrustStatusLog

                if (ct.CreditCard > 0)
                {
                    #region UserCashTrustLog

                    UserCashTrustLog ctUser = new UserCashTrustLog();
                    ctUser.UserId = ct.UserId;
                    ctUser.Amount = ct.CreditCard;
                    ctUser.BankStatus = (int)TrustBankStatus.Initial;
                    ctUser.CreateTime = DateTime.Now;
                    ctUser.Message = "以購物金代替刷卡金退回";
                    ctUser.TrustProvider = ct.TrustProvider;
                    ctUser.TrustId = ct.TrustId;
                    //ctUser.CouponId = ct.CouponId;
                    //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                    ctUser.Type = (int)UserTrustType.Return;
                    ctUser.MarkNew();
                    mp.UserCashTrustLogSet(ctUser);

                    #endregion UserCashTrustLog
                }

                //ATM部分~先預留。
                if (ct.Atm > 0)
                {
                    #region UserCashTrustLog

                    #endregion UserCashTrustLog
                }

                if (ct.Scash > 0)
                {
                    #region UserCashTrustLog

                    UserCashTrustLog ctUser = new UserCashTrustLog();
                    ctUser.UserId = ct.UserId;
                    ctUser.Amount = ct.Scash;
                    ctUser.BankStatus = (int)TrustBankStatus.Initial;
                    ctUser.CreateTime = DateTime.Now;
                    ctUser.Message = "退回既有購物金";
                    ctUser.TrustProvider = ct.TrustProvider;
                    ctUser.TrustId = ct.TrustId;
                    //ctUser.CouponId = ct.CouponId;
                    //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                    ctUser.Type = (int)UserTrustType.Return;
                    ctUser.MarkNew();
                    mp.UserCashTrustLogSet(ctUser);

                    #endregion UserCashTrustLog
                }

                #endregion 信託紀錄

                return true;
            }
            else
            {
                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "查無付款紀錄或付款總額為零，無法退貨", createId, true);
            }

            return false;
        }

        /// <summary>
        /// 信用卡刷卡金額刷退
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="userId"></param>
        /// <param name="createId"></param>
        /// <returns></returns>
        private static bool RefundCreditcardByCouponForTrust(CashTrustLog ct, int userId, string createId)
        {
            PaymentTransactionCollection ptScash = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                                    PaymentTransaction.Columns.OrderGuid + " = " + ct.OrderGuid,
                                                    PaymentTransaction.Columns.PaymentType + " = " + (int)PaymentType.SCash,
                                                    PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Refund,
                                                    PaymentTransaction.Columns.OrderClassification + " = " + (int)OrderClassification.HiDeal);

            if (ct.Status == (int)TrustStatus.Returned)
            {
                if (ptScash.Count() > 0 && ct.CreditCard > 0)
                {
                    if (OrderFacade.GetSCashSum(userId) >= ct.CreditCard)
                    {
                        DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(ptScash.First().Status);
                        PaymentTransactionCollection ptCreditcards = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                        PaymentTransaction.Columns.TransId + " = " + ptScash.First().TransId,
                                        PaymentTransaction.Columns.PaymentType + " = " + (int)PaymentType.Creditcard);
                        var ptCreditcard = new PaymentTransaction();
                        if (ptCreditcards.Count > 0)
                        {
                            ptCreditcard = ptCreditcards.First();
                        }
                        else
                        {
                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "查無信用卡的PaymentTransaction紀錄。", createId, true);
                            return false;
                        }

                        var ptRefundTodayCol = ptCreditcards.Where(x => x.TransType == (int)PayTransType.Refund && Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Created).ToList();

                        decimal creditAmount = ptCreditcards.Where(x => x.TransType == (int)PayTransType.Authorization && Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Successful).Sum(x => x.Amount)
                                             - ptCreditcards.Where(x => x.TransType == (int)PayTransType.Refund).Sum(x => x.Amount);

                        if (creditAmount > 0)
                        {
                            DateTime authTime = ptCreditcards.Where(x => x.TransType == (int)PayTransType.Authorization && (Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Successful)).First().TransTime.Value;
                            
                            OrderFacade.ScashUsed(ct.CreditCard, userId, ct.OrderGuid, "轉刷退取回購物金:" + ct.ItemName, createId, OrderClassification.HiDeal);

                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "轉刷退取回購物金NT$" + ct.CreditCard.ToString("F0"), createId, true);

                            if (ptRefundTodayCol.Count() > 0)
                            {
                                //同一天已有退貨申請，直接異動。
                                PaymentFacade.UpdateTransaction(ptRefundTodayCol.First().TransId, ptRefundTodayCol.First().OrderGuid.Value, PaymentType.Creditcard,
                                   ct.CreditCard + ptRefundTodayCol.First().Amount, ptRefundTodayCol.First().AuthCode, PayTransType.Refund,
                                   authTime, ptRefundTodayCol.First().Message, ptRefundTodayCol.First().Status, (int)PayTransResponseType.Initial);
                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "購物金轉刷退總計NT$" + (ct.CreditCard + ptRefundTodayCol.First().Amount).ToString("F0"), createId, true);
                            }
                            else
                            {
                                PaymentFacade.NewTransaction(ptCreditcard.TransId, ptCreditcard.OrderGuid.Value, PaymentType.Creditcard, ct.CreditCard,
                                    ptCreditcard.AuthCode, PayTransType.Refund, authTime, createId, "購物金轉刷退",
                                    Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Created),
                                    PayTransResponseType.Initial, (OrderClassification)ptCreditcard.OrderClassification.Value, null, (PaymentAPIProvider)ptCreditcard.ApiProvider.Value);
                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "購物金轉刷退計NT$" + ct.CreditCard.ToString("F0"), createId, true);
                            }

                            #region 信託紀錄

                            #region CashTrustLog

                            ct.MarkOld();
                            ct.Status = (int)TrustStatus.Refunded;
                            ct.ModifyTime = DateTime.Now;

                            //檢查退款方式為刷退 非購物金轉刷退 才更新returned_time
                            var orderPk = hp.HiDealOrderGet(ct.OrderGuid).Pk;
                            var hrf =
                                hp.HiDealReturnedGetListByOrderPk(orderPk)
                                    .FirstOrDefault(
                                        x => x.CashBack &&
                                            (x.Status == (int) HiDealReturnedStatus.Create ||
                                             x.Status == (int) HiDealReturnedStatus.Completed));
                            if (hrf != null)
                            {
                                ct.ReturnedTime = ct.ModifyTime;
                            }

                            mp.CashTrustLogSet(ct);

                            #endregion CashTrustLog

                            #region CashTrustStatusLog

                            CashTrustStatusLog ctStatus = new CashTrustStatusLog();
                            ctStatus.TrustId = ct.TrustId;
                            ctStatus.TrustProvider = ct.TrustProvider;
                            ctStatus.Amount = ct.CreditCard;
                            ctStatus.Status = (int)TrustStatus.Refunded;
                            ctStatus.ModifyTime = DateTime.Now;
                            ctStatus.CreateId = createId;
                            ctStatus.MarkNew();
                            mp.CashTrustStatusLogSet(ctStatus);

                            #endregion CashTrustStatusLog

                            if (ct.CreditCard > 0)
                            {
                                #region UserCashTrustLog

                                UserCashTrustLog ctUser = new UserCashTrustLog();
                                ctUser.UserId = ct.UserId;
                                ctUser.Amount = (-1) * ct.CreditCard;
                                ctUser.BankStatus = (int)TrustBankStatus.Initial;
                                ctUser.CreateTime = DateTime.Now;
                                ctUser.Message = "將購物金以刷卡金退回";
                                ctUser.TrustProvider = ct.TrustProvider;
                                ctUser.TrustId = ct.TrustId;
                                //ctUser.CouponId = ct.CouponId;
                                //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                                ctUser.Type = (int)UserTrustType.Refund;
                                ctUser.MarkNew();
                                mp.UserCashTrustLogSet(ctUser);

                                #endregion UserCashTrustLog
                            }

                            #endregion 信託紀錄

                            return true;
                        }
                        else
                        {
                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "購物金並非由此張訂單刷卡購得，無法刷退", createId, true);
                        }
                    }
                    else
                    {
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "購物金不足，無法刷退", createId, true);
                    }
                }
                else
                {
                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "此訂單無退貨購物金供刷退", createId, true);
                }
            }
            else
            {
                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "憑證編號#" + ct.CouponSequenceNumber + "狀態無法退款", createId, true);
            }

            return false;
        }

        private static bool IsUnivoicedAmountInvoiced(int? couponId)
        {
            if (couponId != null)
            {
                EinvoiceMain invoice = op.EinvoiceMainGetByCouponid(couponId.Value, OrderClassification.HiDeal);
                if (invoice != null && invoice.Id > 0 && invoice.VerifiedTime == null)
                {
                    return false;
                }
            }
            return true;
        }

        public static List<int> ReturnedDetailGetReturnedOptionIds(HiDealReturnedDetail returnedDetail)
        {
            var optionIds = new List<int>();
            if (returnedDetail.Option1 != null)
            {
                optionIds.Add(returnedDetail.Option1.Value);
            }

            if (returnedDetail.Option2 != null)
            {
                optionIds.Add(returnedDetail.Option2.Value);
            }

            if (returnedDetail.Option3 != null)
            {
                optionIds.Add(returnedDetail.Option3.Value);
            }

            if (returnedDetail.Option4 != null)
            {
                optionIds.Add(returnedDetail.Option4.Value);
            }

            if (returnedDetail.Option5 != null)
            {
                optionIds.Add(returnedDetail.Option5.Value);
            }

            return optionIds;
        }

        /// <summary>
        /// 退貨作業中，用以處理資料用的物件
        /// </summary>
        private class RefundCashTrustLogTmpData
        {
            public Guid OrderDetailGuid { get; set; }

            public CashTrustLogCollection CashTrustLogs { get; set; }
        }

        /// <summary>
        /// 判斷發票是否跨期退貨
        /// </summary>
        /// <param name="invDate">發票開立日期</param>
        /// <returns>發票是否跨期退貨</returns>
        private static bool IsInvoiceExceedPeriod(DateTime invDate)
        {
            DateTime now = DateTime.Now;
            if (invDate.Year != now.Year)
            {
                //不同年份的話, 必定跨期了
                return true;
            }
            else
            {
                if (
                    (invDate.Month.EqualsAny(1, 2) && now.Month.EqualsAny(1, 2)) ||
                    (invDate.Month.EqualsAny(3, 4) && now.Month.EqualsAny(3, 4)) ||
                    (invDate.Month.EqualsAny(5, 6) && now.Month.EqualsAny(5, 6)) ||
                    (invDate.Month.EqualsAny(7, 8) && now.Month.EqualsAny(7, 8)) ||
                    (invDate.Month.EqualsAny(9, 10) && now.Month.EqualsAny(9, 10)) ||
                    (invDate.Month.EqualsAny(11, 12) && now.Month.EqualsAny(11, 12))
                    )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        #endregion 退貨資料建立與作業

        #region 退貨申請書相關

        /// <summary>
        /// 取得 退貨申請書 的QueryString id的值
        /// </summary>
        /// <returns></returns>
        public static string GetRefundFormPageId(int returnedId, int orderPk, string userName)
        {
            return string.Format("{0}_{1}_{2}", returnedId.ToString(), orderPk.ToString(), userName.Replace("_", ","));
        }

        /// <summary>
        /// 檢查傳入字串，是否符合編碼規則
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        public static bool IsRefundFormPageId(string pageId)
        {
            var idList = pageId.Split("_");
            int tmp;
            if ((idList.Count() == 3) && (int.TryParse(idList[0], out tmp)) && (int.TryParse(idList[1], out tmp)))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 依據pageId的編碼規則，回傳OrderPk，如果解碼錯誤，就回傳null
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        public static int? GetOrderPkFromRefundFormPageId(string pageId)
        {
            var idList = pageId.Split("_");
            if (idList.Count() == 3)
            {
                int orderPk;
                if (int.TryParse(idList[1], out orderPk))
                {
                    return orderPk;
                }
            }
            return null;
        }

        /// <summary>
        /// 依據pageId的編碼規則，回傳ReturnedId，如果解碼錯誤，就回傳null
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        public static int? GetReturnedIdFromRefundFormPageId(string pageId)
        {
            var idList = pageId.Split("_");
            if (idList.Count() == 3)
            {
                int returnedId;
                if (int.TryParse(idList[0], out returnedId))
                {
                    return returnedId;
                }
            }
            return null;
        }

        /// <summary>
        /// 依據pageId的編碼規則，回傳UserName，如果解碼錯誤，就回傳 空字串
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        public static string GetUserNameFromRefundFormPageId(string pageId)
        {
            var idList = pageId.Split("_");
            if (idList.Count() == 3)
            {
                return idList[2].Replace(",", "_");
            }
            return null;
        }

        #endregion 退貨申請書相關
    }
}
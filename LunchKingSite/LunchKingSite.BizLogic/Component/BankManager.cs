﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class BankManager
    {
        private static ISystemProvider _systemProvider;

        private static IOrderProvider _orderProvider;

        /// <summary>
        /// 取得目前BankNoVersion相關資料的版本設定，供前端判定是否資料有更新
        /// </summary>
        public static ApiVersionItem BankNoDataVersion
        {
            get
            {
                return new ApiVersionItem("BankNoVersion", BankNoDataVersionNumber.ToString(CultureInfo.InvariantCulture));
            }
        }

        private const string BankNoVersionExtendedPropertyName = "bank_no_version";
        private static int? _bankNoDataVersionNumber;
        public static int BankNoDataVersionNumber
        {
            get
            {
                //借助_citys判斷資料是否已查詢
                if (_bankNoDataVersionNumber == null)
                {
                    ReloadDefaultManager();
                }
                return _bankNoDataVersionNumber.HasValue ? _bankNoDataVersionNumber.Value : 0;
            }
        }

        private static List<ApiBankNo> _bankNoList;

        public static List<ApiBankNo> BankNoList
        {
            get
            {
                if (_bankNoList == null)
                {
                    ReloadDefaultManager();
                }
                return _bankNoList;
            }
        }

        static BankManager()
        {
            _bankNoDataVersionNumber = null;
            _systemProvider = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _orderProvider = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        public static void ReloadDefaultManager()
        {
            //查詢目前Bank資料設定的版本，供前端判斷是否資料已異動
            string bankNoDataVersion = _systemProvider.DBExtendedPropertiesGet(BankNoVersionExtendedPropertyName);
            int tempVersion;
            //若查無資料或取出的值不為int，預設為0
            if (int.TryParse(bankNoDataVersion, out tempVersion))
            {
                _bankNoDataVersionNumber = tempVersion;
            }
            else
            {
                _bankNoDataVersionNumber = 0;
            }
            ReloadBankNoList();
        }

        public static void ReloadBankNoList()
        {
            BankInfoCollection bankInfoCollection = _orderProvider.BankInfoGetList();
            List<ApiBankNo> tempList = new List<ApiBankNo>();

            var bankGroupList = bankInfoCollection.GroupBy(x => x.BankNo).OrderBy(x=>x.Key);


            foreach (var bankGroup in bankGroupList)
            {
                BankInfo firstInfo = bankGroup.First();

                ApiBankNo bankNo = new ApiBankNo();
                bankNo.BankNo = firstInfo.BankNo;
                bankNo.BankName = firstInfo.BankName;

                foreach (var branch in bankGroup.OrderBy(x=>x.BranchNo))
                {
                    if (!string.IsNullOrWhiteSpace(branch.BranchNo))
                    {
                        ApiBranchNo branchNo = new ApiBranchNo();
                        branchNo.BranchNo = branch.BranchNo;
                        branchNo.BranchName = branch.BranchName;
                        bankNo.Branchs.Add(branchNo);
                    }
                }
                tempList.Add(bankNo);
            }

            _bankNoList = tempList;
        }
    }
}

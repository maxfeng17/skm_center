﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data.SqlTypes;
using System.Text;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.BizLogic.Component.API
{
    public class ApiPromoEventManager
    {

        public const string ManagerNameForJob = "ApiPromoEventManager";
        private const string DefaultPponImage = "/images/skmapp450.jpg";
        private const string DefaultVourcherImage = "/images/skmapp330.jpg";

        private static readonly IPponProvider PponProvider;
        private static readonly ISellerProvider SellerProvider;
        private static readonly IEventProvider EventProvider;
        private static readonly ISysConfProvider config;
        private static readonly log4net.ILog log;
        private static object reloadDataLock = new object();

        /// <summary>
        /// 商品主題活動event的暫存資料
        /// </summary>
        private static List<ApiPromoEventStaticData> _promoEventStaticDataList;
        private static List<ApiPromoEventStaticData> PromoEventStaticDataList
        {
            get
            {
                if (_promoEventStaticDataList == null || _promoEventStaticDataList.Count == 0)
                {
                    LoadDataManager();
                }
                return _promoEventStaticDataList;
            }
        }
        /// <summary>
        /// 暫存資料已讀取過，原始設計為檢察是否有查到資料來判斷是否有進行讀取
        /// 由於商品主題活動可能有某些時段無資料，所以不適合以此做為判斷條件，改新增此屬性
        /// </summary>
        private static bool _isLoaded;

        static ApiPromoEventManager()
        {
            _isLoaded = false;
            PponProvider = ProviderFactory.Instance().GetProvider<IPponProvider>();
            SellerProvider = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            EventProvider = ProviderFactory.Instance().GetProvider<IEventProvider>();
            config = ProviderFactory.Instance().GetConfig();
            log = log4net.LogManager.GetLogger(typeof(ApiPromoEventManager));
            _promoEventStaticDataList = new List<ApiPromoEventStaticData>();
        }



        public static void ReloadDataManager()
        {
            LoadDataManager(true);
        }

        /// <summary>
        /// 讀取暫存資料
        /// 如果reload為true，則不論有無資料，一定會重新讀取
        /// 如果reload為false，則有資料時不進行重讀的動作
        /// </summary>
        /// <param name="reload">是否重新讀取</param>
        public static void LoadDataManager(bool reload = false)
        {
            lock (reloadDataLock)
            {
                //不為重新讀取
                if (!reload)
                {
                    //如果目前已查詢過資料
                    if (_isLoaded)
                    {
                        return;
                    }
                }

                //開始讀取資料，不論原本暫存資料是否有值，一律取代
                List<ApiPromoEventStaticData> tmpDataList = new List<ApiPromoEventStaticData>();

                EventPromoCollection promoCollection = PromotionFacade.EventPromoGetMainListInProgress(true);
                foreach (var promo in promoCollection)
                {
                    IEnumerable<ApiPromoEventStaticData> staticDatas = ApiPromoEventStaticDataGetListByEventPromo(promo);
                    tmpDataList.AddRange(staticDatas);
                }

                _promoEventStaticDataList = tmpDataList;
                //註記已完成讀取作業
                _isLoaded = true;
            }
        }

        /// <summary>
        /// 查詢所有目前正舉辦中的商品主題活動
        /// </summary>
        /// <returns></returns>
        public static List<ApiPromoEvent> ApiPromoEventGetList(bool isAllowNoDeal = true)
        {
            if (isAllowNoDeal) //允許撈出無檔次資料
            {
                return PromoEventStaticDataList.Where(x => x.IsParent && x.PromoEventData.Status == true)
                    .Select(y => y.PromoEvent).OrderByDescending(x => x.EventId).ToList();
            }
            else
            {
                return PromoEventStaticDataList
                    .Where(x => x.IsParent && x.PromoEventData.Status == true && x.PromoEventData.CouponDeals.Count > 0)
                    .Select(y => y.PromoEvent)
                    .OrderByDescending(x => x.EventId).ToList();
            }
        }

        /// <summary>
        /// 查詢eventId 此商品主題活動參加的團購及優惠券資料
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public static ApiPromoEventData ApiPromoEventDataGet(int eventId)
        {
            List<ApiPromoEventData> rtnList = PromoEventStaticDataList.Where(x => x.EventId == eventId && x.PromoEventData.Status == true).Select(y => y.PromoEventData).ToList();
            if (rtnList.Count > 0)
            {
                return rtnList.First();
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 依據傳入的商品活動ID，回傳地圖模式顯示的團購與優惠券資訊
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="coordinates"></param>
        /// <returns></returns>
        public static ApiPromoEventMapData ApiPromoEventMapDataGet(int eventId, ApiCoordinates coordinates)
        {
            List<ApiPromoEventMapData> rtnList = PromoEventStaticDataList.Where(x => x.EventId == eventId).Select(y => y.PromoEventMapData).ToList();
            if (rtnList.Count > 0)
            {
                ApiPromoEventMapData mapData = rtnList.First().Clone() as ApiPromoEventMapData;
                //如果有傳入地理資訊的資料，依據傳入資料進行距離計算
                if (coordinates != null)
                {
                    //取出使用者地理座標資訊
                    SqlGeography userGeo = LocationFacade.GetGeographicPoint(coordinates);
                    #region 行銷活動主題店鋪的處理
                    foreach (var mainStore in mapData.MainStores)
                    {
                        //沒有店鋪的經緯度
                        if (mainStore.Coordinates == null)
                        {
                            mainStore.Distance = 0;
                            mainStore.DistanceDesc = string.Empty;
                        }
                        else
                        {
                            SqlGeography storeGeo = mainStore.Coordinates.ToSqlGeography();
                            double distance = LocationFacade.GetDistance(userGeo, storeGeo);
                            mainStore.Distance = distance;
                            mainStore.DistanceDesc = LocationFacade.GetDistanceDesc(distance);
                        }
                    }
                    #endregion 行銷活動主題店鋪的處理

                    #region 團購檔次資料
                    foreach (var coupon in mapData.CouponDeals)
                    {
                        //沒有店鋪的經緯度
                        if (coupon.Coordinates == null)
                        {
                            coupon.Distance = 0;
                            coupon.DistanceDesc = string.Empty;
                        }
                        else
                        {
                            SqlGeography storeGeo = coupon.Coordinates.ToSqlGeography();
                            double distance = LocationFacade.GetDistance(userGeo, storeGeo);
                            coupon.Distance = distance;
                            coupon.DistanceDesc = LocationFacade.GetDistanceDesc(distance);
                        }
                    }
                    #endregion 團購檔次資料

                    #region 優惠券部分
                    foreach (var vourcher in mapData.VourcherStores)
                    {
                        //沒有店鋪的經緯度
                        if (vourcher.Coordinates == null)
                        {
                            vourcher.Distance = 0;
                            vourcher.DistanceDesc = string.Empty;
                        }
                        else
                        {
                            SqlGeography storeGeo = vourcher.Coordinates.ToSqlGeography();
                            double distance = LocationFacade.GetDistance(userGeo, storeGeo);
                            vourcher.Distance = distance;
                            vourcher.DistanceDesc = LocationFacade.GetDistanceDesc(distance);
                        }
                    }
                    #endregion 優惠券部分
                }

                return mapData;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 依據傳入的eventId回傳參加活動的團購檔次資訊
        /// </summary>
        /// <param name="eventId">商品主題活動編號</param>
        /// <returns></returns>
        public static List<PponDealSynopsis> PponDealSynopsisGetListByEventId(int eventId)
        {
            List<ApiPromoEventStaticData> rtnList = PromoEventStaticDataList.Where(x => x.EventId == eventId).ToList();
            if (rtnList.Count > 0)
            {
                return rtnList.First().PromoEventData.CouponDeals;
            }
            else
            {
                return new List<PponDealSynopsis>();
            }
        }
        /// <summary>
        /// 依據傳入的eventId回傳參加活動的優惠券檔次資訊
        /// </summary>
        /// <param name="eventId">商品主題活動編號</param>
        /// <returns></returns>
        public static List<ApiVourcherStore> ApiVourcherStoreGetListByEventId(int eventId)
        {
            List<ApiPromoEventStaticData> rtnList = PromoEventStaticDataList.Where(x => x.EventId == eventId).ToList();
            if (rtnList.Count > 0)
            {
                return rtnList.First().PromoEventData.VourcherStores;
            }
            else
            {
                return new List<ApiVourcherStore>();
            }
        }

        #region private
        /// <summary>
        /// 依據傳入的EventPromo物件，取出商品主題活動中，要強調的店舖資訊與位置
        /// 目前只有新光商圈活動有相關的資料。
        /// </summary>
        /// <param name="eventPromo"></param>
        /// <param name="coordinates"></param>
        /// <returns></returns>
        private static List<ApiPromoEventMainStore> ApiPromoEventMainStoreGetByEventId(EventPromo eventPromo, ApiCoordinates coordinates)
        {
            //原為新光活動紀錄百貨公司的座標，現保留此作法，之後活動有主題店鋪時可使用，現行一律回傳空陣列
            return new List<ApiPromoEventMainStore>();
        }

        /// <summary>
        /// 依據傳入的EventPromo回傳所有靜態物件用的資料，有子項目則一併產生ApiPromoEventStaticData物件
        /// </summary>
        /// <param name="eventPromo"></param>
        /// <returns></returns>
        private static IEnumerable<ApiPromoEventStaticData> ApiPromoEventStaticDataGetListByEventPromo(EventPromo eventPromo)
        {
            //傳入參數錯誤，回傳空的LIST物件
            if (eventPromo == null)
            {
                return new List<ApiPromoEventStaticData>();
            }
            //建立回傳參數
            List<ApiPromoEventStaticData> rtnList = new List<ApiPromoEventStaticData>();

            ApiPromoEvent apiEvent = new ApiPromoEvent();
            apiEvent.EventTitle = eventPromo.Title;
            apiEvent.EventId = eventPromo.Id;
            string[] bannerImagePaths = ImageFacade.GetMediaPathsFromRawData(eventPromo.AppBannerImage, MediaType.EventPromo);
            apiEvent.PromoBannerImage = (bannerImagePaths == null || bannerImagePaths.Length == 0)
                                            ? string.Empty
                                            : bannerImagePaths[0];
            string[] imagePaths = ImageFacade.GetMediaPathsFromRawData(eventPromo.AppPromoImage, MediaType.EventPromo);
            apiEvent.PromoImage = (imagePaths == null || imagePaths.Length == 0)
                                            ? string.Empty
                                            : imagePaths[0];
            apiEvent.Description = eventPromo.Description;
            apiEvent.DescriptionImage = string.Empty;
            apiEvent.StartDate = ApiSystemManager.DateTimeToDateTimeString(eventPromo.StartDate);
            apiEvent.EndDate = ApiSystemManager.DateTimeToDateTimeString(eventPromo.EndDate);
            //目前只有新光的活動需要輸入發票資訊，新光活動已結束，後續一律設為false
            apiEvent.NeedInvInput = false;
            apiEvent.TemplateType = (EventPromoTemplateType)eventPromo.TemplateType;
            apiEvent.BackgroundImage = string.Empty;
            apiEvent.EventType = eventPromo.EventType;

            if (string.IsNullOrWhiteSpace(eventPromo.Coordinate))
            {
                apiEvent.Coordinates = null;
            }
            else
            {
                SqlGeography geo = LocationFacade.GetGeographyByCoordinate(eventPromo.Coordinate);
                apiEvent.Coordinates = new ApiCoordinates(geo.Long.Value, geo.Lat.Value);
            }
            apiEvent.SubEventList = new List<ApiSubPromoEvent>();
            //取得子項目
            EventPromoCollection subPromos = PromotionFacade.EventPromoGetSubListInProgress(eventPromo.Id, true);

            foreach (var subPromo in subPromos)
            {
                ApiSubPromoEvent subEvent = new ApiSubPromoEvent();
                subEvent.EventTitle = subPromo.Title;
                subEvent.EventId = subPromo.Id;
                subEvent.BackgroundImage = string.Empty;
                //預定由DB提供
                if (string.IsNullOrWhiteSpace(subPromo.Coordinate))
                {
                    subEvent.Coordinates = null;
                }
                else
                {
                    SqlGeography geo = LocationFacade.GetGeographyByCoordinate(subPromo.Coordinate);
                    subEvent.Coordinates = new ApiCoordinates(geo.Long.Value, geo.Lat.Value);
                }

                apiEvent.SubEventList.Add(subEvent);
            }

            ApiPromoEventStaticData mainStaticData = new ApiPromoEventStaticData();
            mainStaticData.EventId = apiEvent.EventId;
            mainStaticData.PromoEvent = apiEvent;
            mainStaticData.PromoEventData = ApiPromoEventDataForStaticData(eventPromo);
            mainStaticData.PromoEventMapData = ApiPromoEventMapDataForStaticData(eventPromo, null);
            //無parentId項目，表示為主要活動，反之為子活動
            mainStaticData.IsParent = eventPromo.ParentId == null;
            rtnList.Add(mainStaticData);

            foreach (var subPromo in subPromos)
            {
                //遞迴呼叫自己產生子項目的物件
                IEnumerable<ApiPromoEventStaticData> subStaticList = ApiPromoEventStaticDataGetListByEventPromo(subPromo);
                rtnList.AddRange(subStaticList);
            }

            return rtnList;
        }
        /// <summary>
        /// 供建立靜態物件時使用，由DB讀取參加商品主題活動頁的團購與優惠券資料
        /// </summary>
        /// <param name="eventPromo"></param>
        /// <returns></returns>
        private static ApiPromoEventData ApiPromoEventDataForStaticData(EventPromo eventPromo)
        {
            DateTime theMoment = DateTime.Now;
            //若查無資料，表示此ID違法或狀態有異常等，回傳null
            if ((eventPromo == null) || (!eventPromo.IsLoaded))
            {
                return null;
            }

            ApiPromoEventData apiEvent = new ApiPromoEventData();
            apiEvent.EventTitle = eventPromo.Title;
            apiEvent.EventId = eventPromo.Id;
            apiEvent.StartDate = ApiSystemManager.DateTimeToDateTimeString(eventPromo.StartDate);
            apiEvent.EndDate = ApiSystemManager.DateTimeToDateTimeString(eventPromo.EndDate);
            apiEvent.TemplateType = (EventPromoTemplateType)eventPromo.TemplateType;
            apiEvent.Status = eventPromo.Status;

            #region 查詢此商品活動的團購檔次
            //查詢此商品活動的團購檔次
            ViewEventPromoCollection promoItem = PromotionFacade.ViewEventPromoGetList(eventPromo);
            apiEvent.CouponDeals = new List<PponDealSynopsis>();
            foreach (var pponDeal in promoItem.Where(x => x.Status == true).OrderBy(x => x.EventId).ThenBy(x => x.Seq))
            {
                //確認檔次處於上檔時間
                if (pponDeal.BusinessHourOrderTimeS > theMoment || pponDeal.BusinessHourOrderTimeE < theMoment)
                {
                    //尚未開賣或已結檔，直接處理下一筆
                    continue;
                }
                //剩下資料應該已經上檔，由靜態物件取出檔次資料
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(pponDeal.BusinessHourGuid);

                PponDealSynopsis dealSynopsis = PponDealApiManager.PponDealSynopsisGetByDeal(deal,null, pponDeal.Seq);
                if (dealSynopsis != null)
                {
                    //商品主題活動的檔次名稱，改為行銷維護的標題
                    dealSynopsis.DealName = pponDeal.Title;

                    if (string.IsNullOrEmpty(dealSynopsis.PromoImage))
                    {
                        dealSynopsis.PromoImage = config.SiteUrl + DefaultPponImage;
                    }
                    apiEvent.CouponDeals.Add(dealSynopsis);
                }
            }

            #endregion 查詢此商品活動的團購檔次

            #region 查詢參加活動的優惠券資料
            //查詢參加活動的優惠券資料
            ViewVourcherStoreFacadeCollection storeList = EventProvider.ViewVourcherStoreFacadeGetListByEventPromoId(eventPromo.Id);
            apiEvent.VourcherStores = new List<ApiVourcherStore>();
            foreach (
                var groupData in
                    storeList.GroupBy(
                        x => new { x.SellerGuid, x.SellerId, x.SellerName, x.SellerLogoimgPath, x.SellerCategory }))
            {
                ApiVourcherStore store = new ApiVourcherStore();
                ViewVourcherStoreFacade storeFacade = groupData.FirstOrDefault();

                store.SellerGuid = groupData.Key.SellerGuid;
                store.SellerId = groupData.Key.SellerId;
                store.SellerName = groupData.Key.SellerName;

                var eventGroup = groupData.GroupBy(y => new { y.VourcherEventId, y.VourcherPageCount }).ToList();

                string eventPicUrl = string.Empty;
                if (storeFacade != null)
                {
                    eventPicUrl = storeFacade.EventPicUrl;
                }
                store.SellerLogoimgPath = GetPromoEventVourcherEventImageUrl(groupData.Key.SellerLogoimgPath,
                                                                               eventPicUrl);

                store.SellerCategory = (SellerSampleCategory)groupData.Key.SellerCategory;
                store.AttentionCount =
                    eventGroup.Sum(x => x.Key.VourcherPageCount == null ? 0 : x.Key.VourcherPageCount.Value);
                store.DistanceDesc = string.Empty;

                if (storeFacade != null)
                {
                    store.VourcherContent = storeFacade.Contents;
                    store.EventId = storeFacade.VourcherEventId;
                }
                else
                {
                    store.VourcherContent = string.Empty;
                    store.EventId = 0;
                }

                DateTime startTime = DateTime.Now;
                if ((storeFacade != null) && (storeFacade.StartDate != null))
                {
                    startTime = storeFacade.StartDate.Value;
                }
                store.StartDate = ApiSystemManager.DateTimeToDateTimeString(startTime);
                store.VourcherCount = eventGroup.Count();

                apiEvent.VourcherStores.Add(store);
            }

            #endregion 查詢參加活動的優惠券資料

            return apiEvent;
        }
        /// <summary>
        /// 供建立靜態物件時使用，由DB讀取地圖模式的資料
        /// </summary>
        /// <param name="eventPromo"></param>
        /// <param name="coordinates"></param>
        /// <returns></returns>
        private static ApiPromoEventMapData ApiPromoEventMapDataForStaticData(EventPromo eventPromo, ApiCoordinates coordinates)
        {
            DateTime theMoment = DateTime.Now;
            //以eventId查詢進行中的活動資料
            //若查無資料，表示此ID違法或狀態有異常等，回傳null
            if ((eventPromo == null) || (!eventPromo.IsLoaded))
            {
                return null;
            }

            ApiPromoEventMapData apiEvent = new ApiPromoEventMapData();
            apiEvent.EventTitle = eventPromo.Title;
            apiEvent.EventId = eventPromo.Id;
            apiEvent.StartDate = ApiSystemManager.DateTimeToDateTimeString(eventPromo.StartDate);
            apiEvent.EndDate = ApiSystemManager.DateTimeToDateTimeString(eventPromo.EndDate);
            apiEvent.TemplateType = (EventPromoTemplateType)eventPromo.TemplateType;

            #region 主題活動店家部分
            apiEvent.MainStores = ApiPromoEventMainStoreGetByEventId(eventPromo, coordinates);
            #endregion 主題活動店家部分


            #region 團購檔次部分
            //查詢此商品活動的團購檔次
            ViewEventPromoCollection promoItem = PromotionFacade.ViewEventPromoGetList(eventPromo);
            apiEvent.CouponDeals = new List<ApiPponDealMapData>();
            foreach (var pponDeal in promoItem)
            {
                #region 檢查檔次資料是否要回傳
                //確認檔次處於上檔時間
                if (pponDeal.BusinessHourOrderTimeS > theMoment || pponDeal.BusinessHourOrderTimeE < theMoment)
                {
                    //尚未開賣或已結檔，直接處理下一筆
                    continue;
                }
                //剩下資料應該已經上檔，由靜態物件取出檔次資料
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(pponDeal.BusinessHourGuid);
                if (deal == null || !deal.IsLoaded)
                {
                    //查無檔次資料，直接處理下一筆
                    continue;
                }

                //是否為宅配商品
                if (deal.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    //如果是宅配商品，不列於地圖模式上，直接處理下一筆
                    continue;
                }

                #endregion 檢查檔次資料是否要回傳

                //取得分店資料
                ViewPponStoreCollection stores = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(deal.BusinessHourGuid);
                //依據每間分店產出API STORE的資訊
                foreach (var store in stores)
                {
                    ApiPponDealMapData mapData = PponDealApiManager.ApiPponDealMapDataGetByDeal(deal, store, coordinates);
                    if (mapData != null)
                    {
                        //商品主題活動的檔次名稱，改為行銷維護的標題
                        mapData.DealName = pponDeal.Title;

                        apiEvent.CouponDeals.Add(mapData);
                    }
                }
            }
            #endregion 團購檔次部分

            #region 優惠券部分
            //查詢參加活動的優惠券資料
            ViewVourcherStoreFacadeCollection storeList = EventProvider.ViewVourcherStoreFacadeGetListByEventPromoId(eventPromo.Id);
            apiEvent.VourcherStores = new List<ApiVourcherStore>();
            foreach (
                var groupData in
                    storeList.GroupBy(
                        x => new { x.StoreGuid }))
            {
                ApiVourcherStore store = new ApiVourcherStore();
                ViewVourcherStoreFacade storeFacade = groupData.FirstOrDefault();

                store.SellerGuid = storeFacade.SellerGuid;
                store.SellerId = storeFacade.SellerId;
                store.SellerName = storeFacade.SellerName;

                var eventGroup = groupData.GroupBy(y => new { y.VourcherEventId, y.VourcherPageCount }).ToList();

                string eventPicUrl = string.Empty;
                if (storeFacade != null)
                {
                    eventPicUrl = storeFacade.EventPicUrl;
                }
                store.SellerLogoimgPath = GetPromoEventVourcherEventImageUrl(storeFacade.SellerLogoimgPath,
                                                                               eventPicUrl);

                store.SellerCategory = (SellerSampleCategory)storeFacade.SellerCategory;
                store.AttentionCount =
                    eventGroup.Sum(x => x.Key.VourcherPageCount == null ? 0 : x.Key.VourcherPageCount.Value);
                store.DistanceDesc = string.Empty;

                store.VourcherContent = storeFacade.Contents;
                store.EventId = storeFacade.VourcherEventId;

                DateTime startTime = DateTime.Now;
                if (storeFacade.StartDate != null)
                {
                    startTime = storeFacade.StartDate.Value;
                }
                //取得店鋪的座標
                SqlGeography storeGeo = LocationFacade.GetGeographyByCoordinate(storeFacade.Coordinate);
                if (storeGeo != null)
                {
                    store.Latitude = (double)storeGeo.Lat;
                    store.Longitude = (double)storeGeo.Long;

                    store.Coordinates = new ApiCoordinates((double)storeGeo.Long, (double)storeGeo.Lat);
                }

                store.StartDate = ApiSystemManager.DateTimeToDateTimeString(startTime);
                store.VourcherCount = eventGroup.Count();

                apiEvent.VourcherStores.Add(store);
            }
            #endregion 優惠券部分

            return apiEvent;
        }
        /// <summary>
        /// 取得優惠券的圖片
        /// </summary>
        /// <param name="sellerLogoPathString"></param>
        /// <param name="eventPicUrlString"></param>
        /// <returns></returns>
        private static string GetPromoEventVourcherEventImageUrl(string sellerLogoPathString, string eventPicUrlString)
        {
            string[] sellerImagePaths = ImageFacade.GetMediaPathsFromRawData(sellerLogoPathString, MediaType.SellerPhotoLarge);
            string[] eventImagePaths = ImageFacade.GetMediaPathsFromRawData(eventPicUrlString, MediaType.PponDealPhoto);
            string rtnImagePath = string.Empty;

            if (eventImagePaths.Length > 0)
            {
                //有優惠券圖片，依據順位取排第二的圖片，如沒有第二的，取第一的
                if ((eventImagePaths.Length > 1) && (!string.IsNullOrWhiteSpace(eventImagePaths[1])))
                {
                    rtnImagePath = eventImagePaths[1];
                }
                else if (!string.IsNullOrWhiteSpace(eventImagePaths[0]))
                {
                    rtnImagePath = eventImagePaths[0];
                }
            }
            //依然沒有圖片，檢查商家的LOGO圖
            if (string.IsNullOrEmpty(rtnImagePath))
            {
                //沒有優惠券圖片，以商家圖片為主
                foreach (string path in sellerImagePaths)
                {
                    if (!string.IsNullOrWhiteSpace(path))
                    {
                        rtnImagePath = path;
                        break;
                    }
                }
            }
            //優惠券與商家都沒有圖，設定為預設圖
            if (string.IsNullOrEmpty(rtnImagePath))
            {
                rtnImagePath = config.SiteUrl + DefaultVourcherImage;
            }
            return rtnImagePath;
        }

        #endregion private
    }

    /// <summary>
    /// 供ApiPromoEventManager暫存商品主題頁資訊的物件
    /// </summary>
    class ApiPromoEventStaticData
    {
        /// <summary>
        /// 是否為主活動
        /// </summary>
        public bool IsParent { get; set; }
        /// <summary>
        /// 商品主題活動之代號
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 回傳用，描述此商品主題活動的物件
        /// </summary>
        public ApiPromoEvent PromoEvent { get; set; }
        /// <summary>
        /// 此商品主題活動內容包含的檔次與優惠券資料
        /// </summary>
        public ApiPromoEventData PromoEventData { get; set; }
        /// <summary>
        /// 供地圖模式使用的商品主題活動資訊
        /// </summary>
        public ApiPromoEventMapData PromoEventMapData { get; set; }
    }
}

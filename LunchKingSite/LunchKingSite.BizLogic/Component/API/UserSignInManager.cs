﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Net;
using log4net;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Component.API
{
    public class UserSignInManager
    {
        private static IMemberProvider _memberProv;
        private static ISysConfProvider _config;
        private static ILog logger = LogManager.GetLogger(typeof (UserSignInManager).Name);
        public const int LoginTicketValidityDay = 60;

        static UserSignInManager()
        {
            _memberProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _config = ProviderFactory.Instance().GetConfig();
        }

        public static ApiSignInReplyData ApiSignInReplyDataGet(SignInReplyData data, string clientIp, string driverId, string ticket, ApiUserDeviceType deviceType)
        {
            ApiSignInReplyData replyData = new ApiSignInReplyData();
            replyData.ReplyType = data.Reply;

            if (!string.IsNullOrWhiteSpace(data.ReplyMessage))
            {
                replyData.ReplyMessage = data.ReplyMessage;
            }
            if (data.Reply == SignInReply.Success)
            {
                LoginTicket loginTicket = CreateOrUpdateLoginTicket(UserSignInMember.Create(data.SignInMember), 
                    ticket, clientIp, driverId, data.SignOnSource, LoginTicketType.AppLogin);
                replyData.LoginTicket = loginTicket.TicketId;
                //取得pez的id
                string pezId = string.Empty;
                MemberLink pezLink = _memberProv.MemberLinkGet(data.SignInMember.UniqueId, SingleSignOnSource.PayEasy);
                if (pezLink.IsLoaded)
                {
                    pezId = pezLink.ExternalUserId;
                }

                //取得fb的id
                string fbId = string.Empty;
                MemberLink fbLink = _memberProv.MemberLinkGet(data.SignInMember.UniqueId, SingleSignOnSource.Facebook);
                if (fbLink.IsLoaded)
                {
                    fbId = fbLink.ExternalUserId;
                }

                Member mem = _memberProv.MemberGet(data.SignInMember.UniqueId);
                if (mem.IsLoaded)
                {
                    if (!string.IsNullOrEmpty(fbId) && string.IsNullOrEmpty(mem.Pic))
                    {
                        try
                        {
                            //之後使用者可以自己更換大頭照，如果原有圖片不是fb，就不再自動替換
                            List<string> fbKeyWord = new List<string> { "facebook", "fbcdn" };
                            if (string.IsNullOrEmpty(mem.Pic) || fbKeyWord.Any(k => mem.Pic.Contains(k, StringComparison.OrdinalIgnoreCase)))
                            {
                                
                                WebRequest request = WebRequest.Create(string.Format("http://graph.facebook.com/{0}{1}/picture?type=large", 
                                    FacebookUtility._FACEBOOK_API_VER_2_10, fbId));
                                mem.Pic = request.GetResponse().ResponseUri.ToString();
                                _memberProv.MemberPicSet(mem.UniqueId, mem.Pic);
                            }
                        }
                        catch
                        {
                            log4net.LogManager.GetLogger("LogToDb").Error(fbId + " can't get fb pic");
                        }
                    }
                }
                ApiMember memberApi = new ApiMember()
                {
                    Id = data.SignInMember.UniqueId,
                    FirstName = data.SignInMember.FirstName,
                    LastName = data.SignInMember.LastName,
                    UserName = data.SignInMember.UserName,
                    UserEmail = data.SignInMember.UserEmail,
                    PezId = pezId,
                    FbId = fbId,
                    Mobile = data.SignInMember.Mobile,
                    FbPic = MemberFacade.GetMemberPicUrl(mem)
                };
                if (memberApi.UserName.EndsWith("@mobile", StringComparison.OrdinalIgnoreCase))
                {
                    memberApi.UserName = string.Empty;
                }
                replyData.SignInMember = memberApi;
                replyData.ExtId = data.ExtId;
                replyData.ReplyMessage = data.ReplyMessage;

                #region OauthToken

                string oauthClientId = string.Empty;
                                
                switch (deviceType)
                {
                    case ApiUserDeviceType.IOS:
                        oauthClientId = _config.iOS17LifeAppOAuthClientId;
                        break;
                    case ApiUserDeviceType.Android:
                        oauthClientId = _config.Android17LifeAppOauthClientId;
                        break;
                }

                if (!string.IsNullOrEmpty(oauthClientId))
                {
                    var tr = OAuthFacade.CreateMemberToken(oauthClientId, data.SignInMember.UniqueId);
                    replyData.AccessToken = tr.AccessToken;
                    replyData.AccessTokenExpiredSec = tr.ExpiresIn;
                }

                #endregion
            }
            else if (data.Reply == SignInReply.MemberLinkNotExist)
            {
                replyData.ExtId = data.ExtId;
            }
            return replyData;
        }

        public static LoginTicket CreateOrUpdateLoginTicket(UserSignInMember member,string ticket, string clientIp, string driverId,
            SingleSignOnSource signOnSource, LoginTicketType logintickettype)
        {
            if (member == null)
            {
                logger.Warn("CreateOrUpdateLoginTicket's member is null, loginticket type=" + logintickettype);
                return new LoginTicket();
            }
            LoginTicket loginTicket;
            if (string.IsNullOrWhiteSpace(ticket))
            {
                loginTicket = CreateLoginTicket(member, clientIp, driverId, signOnSource, logintickettype);
            }
            else
            {
                //已有ticket，取出原有的loginTicket進行更新
                loginTicket = _memberProv.LoginTicketGetExpiration(ticket, member.UserId, driverId, logintickettype);
                if (loginTicket.IsLoaded)
                {
                    loginTicket = UpdateLoginTicket(loginTicket, clientIp);
                }
                else
                {
                    loginTicket = CreateLoginTicket(member, clientIp, driverId, signOnSource, logintickettype);
                }
            }
            return loginTicket;
        }

        private static LoginTicket CreateLoginTicket(UserSignInMember member, string clientIp, string driverId,
            SingleSignOnSource signOnSource, LoginTicketType logintickettype)
        {
            LoginTicket loginTicket = new LoginTicket();
            loginTicket.TicketId = GetTicketHash(member.UserName, clientIp, driverId);
            loginTicket.PrevTicketId = loginTicket.TicketId;
            loginTicket.UserId = member.UserId;
            loginTicket.ClientIp = clientIp;
            loginTicket.UserName = member.UserName;
            loginTicket.ExternalOrg = (int)signOnSource;
            loginTicket.LoginTime = DateTime.Now;
            loginTicket.ExpirationTime = DateTime.Now.AddDays(LoginTicketValidityDay);
            loginTicket.DriverId = driverId;
            loginTicket.LoginTicketType = (int)logintickettype;
            _memberProv.LoginTicketSet(loginTicket);
            return loginTicket;
        }


        private static LoginTicket UpdateLoginTicket(LoginTicket loginTicket, string clientIp)
        {
            //檢查登入紀錄是否存在
            if (loginTicket != null && loginTicket.IsLoaded)
            {
                loginTicket.PrevTicketId = loginTicket.TicketId;
                //產生新的ticket
                loginTicket.TicketId = GetTicketHash(loginTicket.UserName, clientIp, loginTicket.DriverId); ;
                loginTicket.ClientIp = clientIp;
                //延長使用期限
                loginTicket.ExpirationTime = DateTime.Now.AddDays(LoginTicketValidityDay);
                _memberProv.LoginTicketSet(loginTicket);
                return loginTicket;
            }

            return new LoginTicket();
        }

        private static string GetTicketHash(string userName, string clientIp, string driverId)
        {
            string stringData = userName + clientIp + driverId + DateTime.Now.ToString("yyyyMMddHHmmss");
            byte[] data = ASCIIEncoding.ASCII.GetBytes(stringData);
            using (SHA256CryptoServiceProvider cryptoProvider = new SHA256CryptoServiceProvider())
            {
                data = cryptoProvider.ComputeHash(data);
            }
            string hash = "";
            for (int i = 0; i < data.Length; i++)
            {
                hash += data[i].ToString("x2", CultureInfo.InvariantCulture);
            }
            return hash;
        }

        #region  Vbs
        public static VbsApiSignInReplyData VbsApiSignInReplyDataGet(VbsSignInReplyData data, string clientIp, string driverId, string ticket)
        {
            VbsApiSignInReplyData replyData = new VbsApiSignInReplyData();
            replyData.ReplyType = data.Reply;
            if (!string.IsNullOrWhiteSpace(data.ReplyMessage))
            {
                replyData.ReplyMessage = data.ReplyMessage;
            }
            if (data.Reply == SignInReply.Success)
            {
                LoginTicket loginTicket = CreateOrUpdateLoginTicket(UserSignInMember.Create(data.VbsSignInMembership),
                    ticket, clientIp, driverId, data.SignOnSource, LoginTicketType.VbsLogin);
                replyData.LoginTicket = loginTicket.TicketId;

                VbsApiMember vbsApiMember = new VbsApiMember()
                {
                    Id = data.VbsSignInMembership.Id,
                    AccountId = data.VbsSignInMembership.AccountId,
                    AccountType = data.VbsSignInMembership.AccountType,
                    Name = data.VbsSignInMembership.Name,
                    Email = data.VbsSignInMembership.Email
                };
                replyData.IsFirstLogin = data.VbsSignInMembership.IsFirstLogin;
                replyData.SignInMember = vbsApiMember;
                replyData.ExtId = data.ExtId;
                replyData.ReplyMessage = data.ReplyMessage;
            }
            return replyData;
        }

        #endregion

        public class UserSignInMember
        {
            public int UserId { get; set; }
            public string UserName { get; set; }

            public static UserSignInMember Create(VbsMembership mem)
            {
                if (mem == null)
                {
                    return null;
                }
                return new UserSignInMember
                {
                    UserId = mem.Id,
                    UserName = mem.AccountId
                };
            }

            public static UserSignInMember Create(Member mem)
            {
                if (mem == null || mem.IsLoaded == false)
                {
                    return null;
                }
                return new UserSignInMember
                {
                    UserId = mem.UniqueId,
                    UserName = mem.UserName
                };
            }
        }
    }
}

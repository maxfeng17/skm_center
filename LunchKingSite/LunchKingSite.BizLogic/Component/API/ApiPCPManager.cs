﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NPOI.HSSF.Record.Formula.Udf;
using System.Drawing;
using com.google.zxing;
using com.google.zxing.qrcode.decoder;
using com.google.zxing.qrcode;
using System.Collections;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;
using LunchKingSite.BizLogic.Component.PCP;
using LunchKingSite.BizLogic.Model;


namespace LunchKingSite.BizLogic.Component.API
{
    public class ApiPCPManager
    {
        private static readonly IPCPProvider _pcp;
        private static readonly IMemberProvider _mp;
        private static readonly ISellerProvider _sp;
        private static readonly IPponProvider _pp;
        private static readonly ISystemProvider _sysp;
        private static readonly ISysConfProvider _config;
        private const int MaxMemberShipCardCount = 20;
        private static string remark0 = I18N.Message.MembershipCardFixRemark0;
        private static string remark1 = I18N.Message.MembershipCardFixRemark1;
        private static string remark2 = I18N.Message.MembershipCardFixRemark2;
        private static string remark3 = string.Format(I18N.Message.MembershipCardFixRemark3, DateTime.Now.Year, DateTime.Now.Year + 1);
        static ApiPCPManager()
        {
            _pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sysp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _config = ProviderFactory.Instance().GetConfig();
        }

        /// <summary>
        /// 透過userId與cardGroupId開啟熟客卡服務
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cardGroupId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IPcpService CreatePcpService(int userId, int cardGroupId, PcpType type)
        {
            switch (type)
            {
                case PcpType.App:
                    return new PcpAppService(userId, cardGroupId);
                case PcpType.POS:
                    return new PcpPosService(userId, cardGroupId);
                default:
                    return null;
            }
        }

        /// <summary>
        /// 依據熱門狀況撈取熟客卡群組
        /// </summary>
        /// <returns></returns>
        public static List<UserMembershipCardListModel> GetMembershipCardGroupListForHotCardV2(AppImageSize imageSize,
            double longitude, double latitude)
        {
            var data = _pcp.ViewMembershipCardStoreGetByHotTenGroup();
            Regex r = new Regex(@"(?<lo>\d+\.?\d*)\s(?<la>\d+\.?\d*)");
            var imageData = VBSFacade.GetSellerImage(data.Select(x => x.CardGroupId).ToList<int>());
            var userGeo =
                LocationFacade.GetGeographyByCoordinate(string.Format("POINT ({0} {1})", longitude, latitude));
            SqlGeography geo = LocationFacade.GetGeographicPoint(latitude.ToString(), longitude.ToString());
            var cardAvailableService = _pcp.PcpViewMembershipCardAvailableServiceCollectionGet();
            var result = (from g in data
                          group g by g.CardGroupId
                          into items
                          let item = items.First()
                          let images = imageData.Where(x => x.GroupId == item.CardGroupId && x.Seq == 1).ToList()
                          let level1Item = items.FirstOrDefault(x => x.CardLevel == (int)MembershipCardLevel.Level1)
                          let availableService = cardAvailableService.FirstOrDefault(x => x.GroupId == item.CardGroupId)
                          select new UserMembershipCardListModel()
                          {
                              GroupId = item.CardGroupId,
                              SellerName = item.SellerName,
                              SellerFullCardThumbnailPath =
                                        Helper.GetPropValue(VBSFacade.GetImagePathByType(images, PcpImageType.CorporateImage, true), "result").ToString(),
                              AvailableService = availableService == null ? "" :
                                        MembershipCardServerDesc(availableService.DepositService > 0 ? "可寄杯" : "", availableService.PointService > 0 ? "可集點" : ""),
                              OfferDescription = (level1Item == null) ?
                                    "" : MembershipCardGroupOfferDescription(level1Item.PaymentPercent, level1Item.Others, level1Item.PaymentPercent, level1Item.Others),
                              IconImagePath = GetImageUrl(item.IconImageValue, (AppImageSize)imageSize),
                              BackgroundColor = item.BackgroundColorValue,
                              BackgroundImagePath = GetImageUrl(item.BackgroundImageValue, (AppImageSize)imageSize),

                              IsApply = false,
                              PaymentPercent = item.PaymentPercent,
                              Distance = items.Min(s => LocationFacade.GetDistance(geo,
                                                 LocationFacade.GetGeographyByCoordinate(s.Coordinate))),
                              UserSeq = 0,
                              Stores = (from store in items.Where(x => x.CardLevel == (int)MembershipCardLevel.Level0)
                                        let storeGeo = LocationFacade.GetGeographyByCoordinate(store.Coordinate)
                                        let distance = LocationFacade.GetDistance(userGeo, storeGeo)
                                        orderby distance ascending
                                        select new StoreInfo()
                                        {
                                            StoreName = store.StoreName,
                                            Coordinates = new Coordinate
                                            {
                                                Longitude =
                                                     (store.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                                         ? Convert.ToDouble(r.Match(store.Coordinate ?? "POINT (0 0)").Groups["lo"].Value) : 0,
                                                Latitude =
                                                     (store.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                                         ? Convert.ToDouble(r.Match(store.Coordinate ?? "POINT (0 0)").Groups["la"].Value) : 0
                                            },
                                            DistanceDesc =
                                                (longitude.Equals(0) && latitude.Equals(0))
                                                    ? string.Empty
                                                    : LocationFacade.GetDistanceDesc(distance)
                                        }
                                        ).ToList(),
                              CreateTime = item.CardCreateTime
                          }
                ).ToList();


            return result;
        }

        /// <summary>
        /// 依據熱門狀況撈取熟客卡群組
        /// </summary>
        /// <returns></returns>
        public static List<MembershipCardGroupListResultModel> GetMembershipCardGroupListForHotCard(
            AppImageSize imageSize)
        {
            var data = _pcp.ViewMembershipCardStoreGetByHotTenGroup();

            Regex r = new Regex(@"(?<lo>\d+\.?\d*)\s(?<la>\d+\.?\d*)");
            var imageData = VBSFacade.GetSellerImage(data.Select(x => x.CardGroupId).ToList<int>());
            var cardAvailableService = _pcp.PcpViewMembershipCardAvailableServiceCollectionGet();
            var result = (from g in data
                          group g by g.VersionId
                          into items
                          let item = items.First()
                          let availableService = cardAvailableService.FirstOrDefault(x => x.GroupId == item.CardGroupId)
                          select new MembershipCardGroupListResultModel()
                          {
                              GroupId = item.CardGroupId,
                              SellerName = item.SellerName,
                              //SellerImagePath = ((dynamic)VBSFacade.GetImagePathByType(imageData, item.SellerUserId, PcpImageType.CorporateImage)).result,
                              //SellerFullCardPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, item.SellerUserId, PcpImageType.Card))
                              //.result,
                              //SellerFullCardThumbnailPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, item.SellerUserId, PcpImageType.Card, true)).result,
                              SellerImagePath = Helper.GetPropValue(
                                  VBSFacade.GetImagePathByType(imageData, item.CardGroupId, PcpImageType.CorporateImage),
                                  "result"
                                  ).ToString(),
                              SellerFullCardPath = Helper.GetPropValue(
                                  VBSFacade.GetImagePathByType(imageData, item.CardGroupId, PcpImageType.Card), "result"
                                  ).ToString(),
                              SellerFullCardThumbnailPath = Helper.GetPropValue(
                                  VBSFacade.GetImagePathByType(imageData, item.CardGroupId, PcpImageType.Card, true), "result"
                                  ).ToString(),
                              SellerCardType = item.CardType,
                              OfferDescription =
                                  MembershipCardGroupOfferDescription(items.FirstOrDefault(), items.LastOrDefault()),
                              IconImagePath = GetImageUrl(item.IconImageValue, imageSize),
                              BackgroundColor = item.BackgroundColorValue,
                              BackgroundImagePath = GetImageUrl(item.BackgroundImageValue, imageSize),
                              StoreCount = item.StoreCount ?? 0,
                              Stores =
                                  data.Where(
                                      x =>
                                          x.CardGroupId == item.CardGroupId && x.VersionId == item.VersionId &&
                                          x.CardLevel == (int)MembershipCardLevel.Level1).Select(x => new StoreDetailReduced
                                          {
                                              StoreName = x.StoreName,
                                              Coordinates = new Coordinate
                                              {
                                                  Longitude =
                                                     (x.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                                         ? Convert.ToDouble(r.Match(x.Coordinate).Groups["lo"].Value) : 0,
                                                  Latitude =
                                                     (x.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                                         ? Convert.ToDouble(r.Match(x.Coordinate).Groups["la"].Value) : 0
                                              }
                                          }).ToList(),
                              IsApply = false,
                              UserCardId = 0,
                              ReleaseTime = ApiSystemManager.DateTimeToDateTimeString(item.OpenTime),
                              CompanyName = string.IsNullOrWhiteSpace(item.InContractWith) ? string.Empty : item.InContractWith,
                              Email = string.IsNullOrWhiteSpace(item.SellerEmail) ? string.Empty : item.SellerEmail,
                              Phone = string.IsNullOrWhiteSpace(item.SellerMobile) ? string.Empty : item.SellerMobile,
                              AvailableService = availableService == null ? "" :
                                        MembershipCardServerDesc(availableService.DepositService > 0 ? "可寄杯" : "", availableService.PointService > 0 ? "可集點" : ""),
                          }
                ).ToList();
            return result;
        }

        public static List<MembershipCardGroupListResultModel> GetMembershipCardGroupList(
            MembershipCardGroupListQueryModel q)
        {
            var data = _pcp.ViewMembershipCardStoreCollectionGet(q.RegionId, q.Category, q.Latitude, q.Longitude,
                q.GetOrderByEnum);

            int userId = 0;
            if (!string.IsNullOrWhiteSpace(q.UserName))
            {
                var mem = _mp.MemberGet(q.UserName);
                userId = mem.IsLoaded ? mem.UniqueId : 0;
            }

            var checkUserOwner = _pcp.UserMembershipCardGetByUserId(userId);
            var imageData = VBSFacade.GetSellerImage(data.Select(x => x.CardGroupId).ToList<int>());
            Regex r = new Regex(@"(?<lo>\d+\.?\d*)\s(?<la>\d+\.?\d*)");

            var result = (from g in data
                          group g by g.VersionId
                into items
                          let item = items.First()
                          select new MembershipCardGroupListResultModel()
                          {
                              GroupId = item.CardGroupId,
                              SellerName = item.SellerName,
                              //SellerImagePath = ((dynamic)VBSFacade.GetImagePathByType(imageData, item.SellerUserId, PcpImageType.CorporateImage)).result,
                              //SellerFullCardPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, item.SellerUserId, PcpImageType.Card))
                              //.result,
                              //SellerFullCardThumbnailPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, item.SellerUserId, PcpImageType.Card, true)).result,
                              SellerImagePath =
                                  Helper.GetPropValue(
                                      VBSFacade.GetImagePathByType(imageData, item.CardGroupId, PcpImageType.CorporateImage),
                                      "result").ToString(),
                              SellerFullCardPath = Helper.GetPropValue(
                                  VBSFacade.GetImagePathByType(imageData, item.CardGroupId, PcpImageType.Card), "result"
                                  ).ToString(),
                              SellerFullCardThumbnailPath = Helper.GetPropValue(
                                  VBSFacade.GetImagePathByType(imageData, item.CardGroupId, PcpImageType.Card, true), "result"
                                  ).ToString(),
                              SellerCardType = item.CardType,
                              OfferDescription =
                                  MembershipCardGroupOfferDescription(items.FirstOrDefault(), items.LastOrDefault()),
                              IconImagePath = GetImageUrl(item.IconImageValue, q.GetAppImageSizeEnum),
                              BackgroundColor = item.BackgroundColorValue,
                              BackgroundImagePath = GetImageUrl(item.BackgroundImageValue, q.GetAppImageSizeEnum),
                              StoreCount = item.StoreCount ?? 0,
                              Stores =
                                  data.Where(
                                      x =>
                                          x.CardGroupId == item.CardGroupId && x.VersionId == item.VersionId &&
                                          x.CardLevel == (int)MembershipCardLevel.Level1).Select(x => new StoreDetailReduced
                                          {
                                              StoreName = x.StoreName,
                                              Coordinates = new Coordinate
                                              {
                                                  Longitude =
                                                     (x.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                                         ? Convert.ToDouble(r.Match(x.Coordinate).Groups["lo"].Value)
                                                         : 0,
                                                  Latitude =
                                                     (x.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                                         ? Convert.ToDouble(r.Match(x.Coordinate).Groups["la"].Value)
                                                         : 0
                                              }
                                          }).ToList(),
                              IsApply = checkUserOwner.Any(x => x.CardGroupId == item.CardGroupId
                                                                && x.Enabled == true
                                                                && DateTime.Today.IsBetween(x.StartTime, x.EndTime)
                                  ),
                              UserCardId = checkUserOwner.Where(x => x.CardGroupId == item.CardGroupId
                                                                     && x.Enabled == true
                                                                     && DateTime.Today.IsBetween(x.StartTime, x.EndTime)
                                  ).Select(x => x.Id).DefaultIfEmpty(0).FirstOrDefault(),
                              ReleaseTime = ApiSystemManager.DateTimeToDateTimeString(item.ReleaseTime ?? item.OpenTime),
                              CompanyName = string.IsNullOrWhiteSpace(item.InContractWith) ? string.Empty : item.InContractWith,
                              Email = string.IsNullOrWhiteSpace(item.SellerEmail) ? string.Empty : item.SellerEmail,
                              Phone = string.IsNullOrWhiteSpace(item.SellerMobile) ? string.Empty : item.SellerMobile,
                              HotPoint = item.HotPoint
                          }
                ).ToList();

            result.RemoveAll(x => x.IsApply);

            return result;
        }

        public static MembershipCardGroupListModel GetMembershipCardGroupListV2(MembershipCardGroupListQueryModel q)
        {
            var data = _pcp.ViewMembershipCardStoreCollectionGet(q.RegionId, q.Category, q.Latitude, q.Longitude,
                q.GetOrderByEnum).Where(y => y.Status == (byte) MembershipCardStatus.Open).Select(x => new
                {
                    CardGroupId = x.CardGroupId,
                    CategoryList = x.CategoryList,
                    CardLevel = x.CardLevel,
                    SellerName = x.SellerName,
                    IconImageValue = x.IconImageValue,
                    StoreName = x.StoreName,
                    BackgroundImageValue = x.BackgroundImageValue,
                    BackgroundColorValue = x.BackgroundColorValue,
                    PaymentPercent = x.PaymentPercent,
                    Others = x.Others,
                    Coordinate = x.Coordinate
                });

            int userId = 0;
            if (!string.IsNullOrWhiteSpace(q.UserName))
            {
                var mem = _mp.MemberGet(q.UserName);
                userId = mem.IsLoaded ? mem.UniqueId : 0;
            }

            var checkUserOwner = _pcp.UserMembershipCardGetByUserId(userId);
            var imageData = VBSFacade.GetSellerImage(data.Select(x => x.CardGroupId).ToList<int>());
            var cardAvailableService = _pcp.PcpViewMembershipCardAvailableServiceCollectionGet();
            Regex r = new Regex(@"(?<lo>\d+\.?\d*)\s(?<la>\d+\.?\d*)");
            var userGeo =
                LocationFacade.GetGeographyByCoordinate(string.Format("POINT ({0} {1})", q.Longitude, q.Latitude));
            var membershipCards = (from g in data
                                   group g by g.CardGroupId
                                   into items
                                   let item = items.First()
                                   let images = imageData.Where(x => x.GroupId == item.CardGroupId && x.Seq == 1).ToList()
                                   let level1Item = items.FirstOrDefault(x => x.CardLevel == (int)MembershipCardLevel.Level1)
                                   let level1ItemPercent = (level1Item == null) ? 0 : (double)level1Item.PaymentPercent
                                   let level1ItemOther = (level1Item == null) ? "" : level1Item.Others
                                   let availableService = cardAvailableService.FirstOrDefault(x => x.GroupId == item.CardGroupId)
                                   select new MembershipCardInfo()
                                   {
                                       GroupId = item.CardGroupId,
                                       SellerName = item.SellerName,
                                       SellerFullCardThumbnailPath =
                                          Helper.GetPropValue(VBSFacade.GetImagePathByType(images, PcpImageType.CorporateImage), "result").ToString(),
                                       AvailableService = availableService == null ? "" :
                                        MembershipCardServerDesc(availableService.DepositService > 0 ? "可寄杯" : "", availableService.PointService > 0 ? "可集點" : ""),
                                       OfferDescription =
                                           MembershipCardGroupOfferDescription(level1ItemPercent, level1ItemOther, level1ItemPercent, level1ItemOther),
                                       IconImagePath = GetImageUrl(item.IconImageValue, q.GetAppImageSizeEnum),
                                       BackgroundColor = item.BackgroundColorValue,
                                       BackgroundImagePath = GetImageUrl(item.BackgroundImageValue, q.GetAppImageSizeEnum),

                                       IsApply = checkUserOwner.Any(x => x.CardGroupId == item.CardGroupId
                                                                         && x.Enabled == true
                                                                         && DateTime.Today.IsBetween(x.StartTime, x.EndTime)
                                           ),
                                       Stores = (from store in items.Where(x => x.CardLevel == (int)MembershipCardLevel.Level0) //預設都有一張零級卡片
                                                 let storeGeo = LocationFacade.GetGeographyByCoordinate(store.Coordinate)
                                                 let distance = LocationFacade.GetDistance(userGeo, storeGeo)
                                                 orderby distance ascending
                                                 select new StoreInfo()
                                                 {
                                                     StoreName = store.StoreName,
                                                     Coordinates = new Coordinate
                                                     {
                                                         Longitude =
                                                     (store.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                                         ? Convert.ToDouble(r.Match(store.Coordinate).Groups["lo"].Value)
                                                         : 0,
                                                         Latitude =
                                                     (store.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                                         ? Convert.ToDouble(r.Match(store.Coordinate).Groups["la"].Value)
                                                         : 0
                                                     },
                                                     DistanceDesc =
                                                         (q.Longitude.Equals(0) && q.Latitude.Equals(0))
                                                             ? string.Empty
                                                             : LocationFacade.GetDistanceDesc(distance)
                                                 }
                                                 ).ToList()
                                   }
                ).ToList();

            var result = new MembershipCardGroupListModel();
            var totalCount = membershipCards.Count;
            //分頁
            if (q.GetAll == false)
            {
                if (q.StartIndex <= 0)
                {
                    result.MembershipCardList = membershipCards.Take(MaxMemberShipCardCount).ToList();
                }
                else if (totalCount >= q.StartIndex)
                {
                    result.MembershipCardList = membershipCards.Skip(q.StartIndex).Take(MaxMemberShipCardCount).ToList();
                }
                else
                {
                    result.MembershipCardList = new List<MembershipCardInfo>();
                }
            }
            else
            {
                result.MembershipCardList = membershipCards;
            }

            return result;
        }

        public static List<MembershipCardCategoryListModel> GetMembershipRecommendList(
            MembershipCardGroupListQueryModel q)
        {
            //撈出分類全部資料
            var data = _pcp.ViewMembershipCardStoreCollectionGet(q.RegionId, 0, q.Latitude, q.Longitude,
                q.GetOrderByEnum).Where(x => !string.IsNullOrWhiteSpace(x.CategoryList) && x.Status == (byte)MembershipCardStatus.Open)
                .Select(x => new
                {
                    CardGroupId = x.CardGroupId,
                    CategoryList = x.CategoryList,
                    CardLevel = x.CardLevel,
                    SellerName = x.SellerName,
                    IconImageValue = x.IconImageValue,
                    BackgroundColorValue = x.BackgroundColorValue,
                    BackgroundImageValue = x.BackgroundImageValue,
                    PaymentPercent = x.PaymentPercent,
                    Others = x.Others
                });

            int userId = 0;
            if (!string.IsNullOrWhiteSpace(q.UserName))
            {
                var mem = _mp.MemberGet(q.UserName);
                userId = mem.IsLoaded ? mem.UniqueId : 0;
            }

            var checkUserOwner = _pcp.UserMembershipCardGetByUserId(userId);
            var imageData = VBSFacade.GetSellerImage(data.Select(x => x.CardGroupId).ToList<int>());

            //分類的部分
            List<Category> categories = CategoryManager.CategoryGetListByType(CategoryType.MembershipCard);
            var pcpImgDic = _pcp.PcpCategoryImageCollectionGet().ToDictionary(x => x.CategoryId, x => x.ImageUrl);
            var cardAvailableService = _pcp.PcpViewMembershipCardAvailableServiceCollectionGet();
            var result = new List<MembershipCardCategoryListModel>();
            foreach (var category in categories)
            {
                if (category.Name.Contains("品牌連鎖")) //hardCode 濾掉品牌連鎖，改由MembershipCardBrandList
                {
                    continue;
                }

                var cards = (from g in data
                             group g by g.CardGroupId
                             into items
                             let item = items.First()
                             let images = imageData.Where(x => x.GroupId == item.CardGroupId && x.Seq == 1).ToList()
                             let level1Item = items.FirstOrDefault(x => x.CardLevel == (int)MembershipCardLevel.Level1)
                             let level1ItemPercent = (level1Item == null) ? 0 : (double)level1Item.PaymentPercent
                             let level1ItemOther = (level1Item == null) ? "" : level1Item.Others
                             let availableService = cardAvailableService.FirstOrDefault(x => x.GroupId == item.CardGroupId)
                             where CategoryListToIntArray(item.CategoryList).Contains(category.Id)
                             select new MembershipCardInfo()
                             {
                                 GroupId = item.CardGroupId,
                                 SellerName = item.SellerName,
                                 SellerFullCardThumbnailPath =
                                     Helper.GetPropValue(VBSFacade.GetImagePathByType(images, PcpImageType.CorporateImage, true), "result").ToString(),
                                 AvailableService = availableService == null ? "" :
                                        MembershipCardServerDesc(availableService.DepositService > 0 ? "可寄杯" : "", availableService.PointService > 0 ? "可集點" : ""),
                                 OfferDescription =
                                 MembershipCardGroupOfferDescription(level1ItemPercent, level1ItemOther, level1ItemPercent, level1ItemOther),
                                 IconImagePath = GetImageUrl(item.IconImageValue, q.GetAppImageSizeEnum),
                                 BackgroundColor = item.BackgroundColorValue,
                                 BackgroundImagePath = GetImageUrl(item.BackgroundImageValue, q.GetAppImageSizeEnum),
                                 IsApply = checkUserOwner.Any(c => c.CardGroupId == item.CardGroupId
                                                                   && c.Enabled == true
                                                                   && DateTime.Today.IsBetween(c.StartTime, c.EndTime)),
                             }
                    ).Take(10).ToList();

                if (cards.Any())
                {
                    string categoryImage;
                    pcpImgDic.TryGetValue(category.Id, out categoryImage);

                    result.Add(new MembershipCardCategoryListModel()
                    {
                        Id = category.Id,
                        Name = category.Name,
                        DefaultImagePath = _config.SiteUrl + "/Themes/pcp/category/" + categoryImage ?? "", //預設類別圖片
                        MembershipCardList = cards
                    });
                }
            }

            return result;
        }

        private static int[] CategoryListToIntArray(string str)
        {
            if (!String.IsNullOrWhiteSpace(str))
            {
                string[] strArray = str.Split(",");
                int[] cids = new int[strArray.Length];

                for (int index = 0; index < strArray.Count(); index++)
                {
                    int cid;
                    int.TryParse(strArray[index], out cid);
                    cids[index] = cid;
                }
                return cids;
            }

            return new int[0];
        }

        public static MembershipCardAboutGroupResultModel GetMembershipCardGroup(MembershipCardGroupListQueryModel q,
            bool isNewPrototype = false)
        {
            var datas = _pcp.ViewMembershipCardStoreGetByLastGroupId(q.GroupId);
            if (!datas.Any())
            {
                return new MembershipCardAboutGroupResultModel(false);
            }

            int userId = 0;
            if (!string.IsNullOrWhiteSpace(q.UserName))
            {
                var mem = _mp.MemberGet(q.UserName);
                userId = mem.IsLoaded ? mem.UniqueId : 0;
            }

            var checkUserOwner = _pcp.UserMembershipCardGetByUserId(userId);
            var imageData = VBSFacade.GetSellerImage(datas.Select(x => x.CardGroupId).ToList<int>());

            Regex r = new Regex(@"(?<lo>\d+\.?\d*)\s(?<la>\d+\.?\d*)");

            var data = datas.FirstOrDefault();

            var convert2CardDetail = (from item in _pcp.ViewMembershipCardGetListByDatetime(q.GroupId, DateTime.Now)
                .Where(x => x.Status == (int)MembershipCardStatus.Open && x.Enabled)
                                      select new CardDetail
                                      {
                                          CardId = item.CardId,
                                          Level = item.Level,
                                          Enabled = item.Enabled,
                                          PaymentPercent = item.PaymentPercent,
                                          Instruction = item.Instruction ?? "",
                                          OtherPremiums = item.Others ?? "",
                                          AvailableDateType = item.AvailableDateType,
                                          AvailableDateDesc = Helper.GetEnumDescription(((AvailableDateType)item.AvailableDateType)),
                                          OrderNeeded = item.OrderNeeded ?? 0,
                                          AmountNeeded = Convert.ToDouble((item.AmountNeeded ?? 0)),
                                          ConditionalLogic = item.ConditionalLogic,
                                          OpenTime = ApiSystemManager.DateTimeToDateTimeString(item.OpenTime),
                                          CloseTime = ApiSystemManager.DateTimeToDateTimeString(item.CloseTime),
                                          CombineUse = item.CombineUse
                                      }).ToList();


            var userGeo =
                LocationFacade.GetGeographyByCoordinate(string.Format("POINT ({0} {1})", q.Longitude, q.Latitude));
            var convert2StroeDetail =
                (from item in _sp.SellerGetListBySellerGuidList(datas.Select(x => x.StoreGuid).ToList())
                 let matchCoor = r.Match(item.Coordinate ?? "")
                 let storeGeo = LocationFacade.GetGeographyByCoordinate(item.Coordinate ?? "")
                 let distance = LocationFacade.GetDistance(userGeo, storeGeo)
                 orderby distance ascending
                 select new StoreDetail
                 {
                     StoreGuid = item.Guid,
                     StoreName = item.SellerName,
                     Phone = item.StoreTel,
                     Address =
                         item.StoreTownshipId == null
                             ? ""
                             : CityManager.CityTownShopStringGet(item.StoreTownshipId.Value) + item.StoreAddress,
                     OpenTime = item.OpenTime,
                     CloseDate = item.CloseDate,
                     Remarks = item.SellerRemark,
                     Mrt = item.Mrt,
                     Car = item.Car,
                     Bus = item.Bus,
                     OtherVehicles = item.OtherVehicles,
                     WebUrl = item.WebUrl,
                     FbUrl = item.FacebookUrl,
                     //PlurkUrl = item.PlurkUrl, SellerTree合併後沒有PlurkUrl
                     BlogUrl = item.BlogUrl,
                     OtherUrl = item.OtherUrl,
                     CreditcardAvailable = item.CreditcardAvailable,
                     Coordinates = new Coordinate
                     {
                         Longitude = (item.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                    ? Convert.ToDouble(r.Match(item.Coordinate).Groups["lo"].Value) : 0,
                         Latitude = (item.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                   ? Convert.ToDouble(r.Match(item.Coordinate).Groups["la"].Value) : 0
                     },
                     DistanceDesc =
                         (q.Longitude.Equals(0) && q.Latitude.Equals(0))
                             ? string.Empty
                             : LocationFacade.GetDistanceDesc(distance)
                 }
                    ).ToList();

            CardDetail firstCard = convert2CardDetail.FirstOrDefault();
            CardDetail lastCard = convert2CardDetail.LastOrDefault();
            double firstPercent = 0, lastPercent = 0;
            string firstOtherOffer = string.Empty, lastOtherOffer = string.Empty;
            if (firstCard != null)
            {
                firstPercent = firstCard.PaymentPercent;
                firstOtherOffer = firstCard.OtherPremiums ?? "";
            }
            if (lastCard != null)
            {
                lastPercent = lastCard.PaymentPercent;
                lastOtherOffer = lastCard.OtherPremiums ?? "";
            }

            var storeCol = _sp.VbsSellerGuidGet(data.SellerUserId).FirstOrDefault();
            var dealList = new List<PponDealSynopsis>();
            if (storeCol != null)
            {
                var deals = _pp.ViewPponDealGetBySellerGuid(storeCol.Guid);

                var queryDeals = deals
                    .Where(x =>
                        x.BusinessHourOrderTimeS <= DateTime.Now
                        && DateTime.Now <= x.BusinessHourOrderTimeE
                        && !Helper.IsFlagSet(x.BusinessHourStatus, (int)BusinessHourStatus.ComboDealSub)
                    )
                    .Select(x => x.BusinessHourGuid).ToList();

                dealList = PponDealApiManager.PponDealSynopsesGetListByBids(queryDeals, isNewPrototype);

                if (isNewPrototype == true)
                {
                    foreach (var temp in dealList)
                    {
                        temp.OriginalPrice = temp.OriPrice;
                    }
                }
            }

            var result = new MembershipCardAboutGroupResultModel
            {
                IsLoaded = true,
                GroupId = data.CardGroupId,
                SellerName = data.SellerName,
                SellerImagePath =
                    ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.CorporateImage)).result,
                SellerFullCardPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.Card)).result,
                SellerFullCardThumbnailPath =
                    ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.Card, true)).result,
                //SellerImagePath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.CorporateImage), "result").ToString(),
                //SellerFullCardPath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.Card), "result").ToString(),
                //SellerFullCardThumbnailPath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.Card, true), "result").ToString(),
                SellerCardType = data.CardType,
                SellerIntro =
                    _pcp.PcpIntroGet(PcpIntroType.Seller, data.CardGroupId).Select(x => x.IntroContent).ToArray(),
                SellerServiceImgPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.Service)).result,
                SellerServiceImgThumbnailPath =
                    ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.Service, true)).result,
                SellerSrndgImgPath =
                    ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.EnvironmentAroundStore)).result,
                SellerSrndgThumbnailPath =
                    ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.EnvironmentAroundStore, true))
                        .result,
                OfferDescription =
                    MembershipCardGroupOfferDescription(firstPercent, firstOtherOffer, lastPercent, lastOtherOffer),
                IconImagePath = GetImageUrl(data.IconImageValue, q.GetAppImageSizeEnum),
                BackgroundImagePath = GetImageUrl(data.BackgroundImageValue, q.GetAppImageSizeEnum),
                BackgroundColor = data.BackgroundColorValue,
                StoreCount = data.StoreCount ?? 0,
                IsApply = checkUserOwner.Any(x => x.CardGroupId == data.CardGroupId
                                                  && x.Enabled == true
                                                  && DateTime.Today.IsBetween(x.StartTime, x.EndTime)
                    ),
                UserCardId = checkUserOwner.Where(x => x.CardGroupId == data.CardGroupId
                                                       && x.Enabled == true
                                                       && DateTime.Today.IsBetween(x.StartTime, x.EndTime)
                    ).Select(x => x.Id).DefaultIfEmpty(0).FirstOrDefault(),
                ReleaseTime = ApiSystemManager.DateTimeToDateTimeString(data.OpenTime),
                Cards = convert2CardDetail,
                Stores = convert2StroeDetail,
                StoreName = convert2StroeDetail.Count > 0 ? convert2StroeDetail.First().StoreName : string.Empty,
                CompanyName = string.IsNullOrWhiteSpace(data.InContractWith) ? string.Empty : data.InContractWith,
                Email = string.IsNullOrWhiteSpace(data.SellerEmail) ? string.Empty : data.SellerEmail,
                Phone = string.IsNullOrWhiteSpace(data.SellerMobile) ? string.Empty : data.SellerMobile,
                HotPoint = data.HotPoint,
                CardValidDate =
                    data.ContractValidDate.HasValue
                        ? ApiSystemManager.DateTimeToDateTimeString(data.ContractValidDate.Value)
                        : ApiSystemManager.DateTimeToDateTimeString(DateTime.Now.AddYears(1)),
                LastModifyTime =
                    data.CardModifyTime.HasValue
                        ? ApiSystemManager.DateTimeToDateTimeString(data.CardModifyTime.Value)
                        : ApiSystemManager.DateTimeToDateTimeString(data.CardCreateTime),
                DealList = dealList
            };

            return result;
        }

        public static List<MembershipCardGroupListResultModel> GetPromoCardsList(string userName = "")
        {
            var data = _pcp.ViewMembershipCardStoreCollectionGetByPromo();
            int userId = 0;
            if (!string.IsNullOrWhiteSpace(userName))
            {
                var mem = _mp.MemberGet(userName);
                userId = mem.IsLoaded ? mem.UniqueId : 0;
            }
            UserMembershipCardCollection checkUserOwner = new UserMembershipCardCollection();
            var userMembershipCardCol = new ViewUserMembershipCardCollection();

            if (userId != 0)
            {
                checkUserOwner = _pcp.UserMembershipCardGetByUserId(userId);

                var nowTime = DateTime.Now;
                userMembershipCardCol = _pcp.ViewUserMembershipCardGetList(ViewUserMembershipCard.Columns.UserSeq,
                    ViewUserMembershipCard.Columns.UserId + " = " + userId,
                    ViewUserMembershipCard.Columns.Enabled + " = " + true,
                    ViewUserMembershipCard.Columns.StartTime + " <= " + nowTime,
                    ViewUserMembershipCard.Columns.EndTime + " >= " + nowTime,
                    ViewUserMembershipCard.Columns.Status + " = " + (byte)MembershipCardStatus.Open);
            }
            var imageData = VBSFacade.GetSellerImage(data.Select(x => x.CardGroupId).ToList<int>());

            var result = (from g in data
                          group g by g.VersionId
                into items
                          let item = items.First()
                          let users = userMembershipCardCol.Where(x => x.CardGroupId == item.CardGroupId).FirstOrDefault()
                          select new MembershipCardGroupListResultModel()
                          {
                              GroupId = item.CardGroupId,
                              SellerName = item.SellerName,
                              SellerFullCardPath = Helper.GetPropValue(
                                  VBSFacade.GetImagePathByType(imageData, item.CardGroupId, PcpImageType.Card), "result"
                                  ).ToString(),
                              SellerFullCardThumbnailPath = Helper.GetPropValue(
                                  VBSFacade.GetImagePathByType(imageData, item.CardGroupId, PcpImageType.Card, true), "result"
                                  ).ToString(),
                              SellerCardType = item.CardType,
                              IsApply = checkUserOwner.Any(x => x.CardGroupId == item.CardGroupId
                                                                && x.Enabled == true
                                                                && DateTime.Today.IsBetween(x.StartTime, x.EndTime)
                                  ),
                              UserCardId = users == null ? 0 : users.Id,
                          }
                ).ToList();
            return result;
        }

        public static PromoGroupUserMembershipCardModel GetMembershipCardByGroup(MembershipCardGroupListQueryModel q)
        {
            var datas = _pcp.ViewMembershipCardStoreGetByLastGroupId(q.GroupId);
            if (!datas.Any())
            {
                return null;
            }

            var data = datas.FirstOrDefault();

            var result = new PromoGroupUserMembershipCardModel
            {
                GroupId = data.CardGroupId,
                SellerName = data.SellerName,
                Percent = string.Format("{0:0.#}", data.PaymentPercent * 100).Replace("0", ""),
                OfferDescription = data.Others,
                IconImagePath = GetImageUrl(data.IconImageValue, q.GetAppImageSizeEnum),
                BackgroundImagePath = GetImageUrl(data.BackgroundImageValue, q.GetAppImageSizeEnum),
                BackgroundColor = data.BackgroundColorValue,
                StoreName = data.StoreName,
                OpenTime = data.OpenTime.ToShortDateString(),
                CloseTime = data.CloseTime.ToShortDateString()
            };

            return result;
        }

        /// <summary>
        /// 單張熟客卡領取
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userName"></param>
        /// <param name="rtnObject"></param>
        /// <param name="refereeId"></param>
        /// <returns></returns>
        public static bool ApplyCardByGroupId(int groupId, string userName, out ApiReturnObject rtnObject,
            string refereeId = "")
        {
            rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var m = _mp.MemberGet(userName);

            //領取熟客卡
            var pcpApp = CreatePcpService(m.UniqueId, groupId, PcpType.App);
            if (!pcpApp.ApplyCard())
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = "尚未領取成功";
                return false;
            }

            //設定回傳資料
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Data = new { GroupId = groupId };
            return true;

        }

        /// <summary>
        /// 熟客卡多張領取
        /// </summary>
        /// <param name="groupIds"></param>
        /// <param name="userName"></param>
        /// <param name="rtnObject"></param>
        /// <param name="refereeId"></param>
        /// <returns></returns>
        public static bool ApplyCardByGroupIdV2(List<int> groupIds, string userName, out ApiResult rtnObject,
            string refereeId = "")
        {
            rtnObject = new ApiResult() { Code = ApiResultCode.Success };

            var m = _mp.MemberGet(userName);
            //領取熟客卡
            bool isComplete = true;
            using (var tran = TransactionScopeBuilder.CreateReadCommitted())
            {
                foreach (var id in groupIds)
                {
                    var pcpApp = CreatePcpService(m.UniqueId, id, PcpType.App);
                    if (!pcpApp.ApplyCard())
                    {
                        isComplete = false;
                        break;
                    }
                }

                if (isComplete)
                {
                    tran.Complete();
                }
            }

            if (isComplete == false)
            {
                //已索取指回傳訊息供判斷，依然已true表示運作正常
                rtnObject.Code = ApiResultCode.PcpApplyCardFail;
                rtnObject.Data = new { GroupId = groupIds };
                rtnObject.Message = "尚未領取成功";
                return true;
            }

            //設定回傳資料
            rtnObject.Data = new { GroupId = groupIds };
            return true;

        }

        /// <summary>
        /// 確認是否同意熟客卡一次性條款
        /// </summary>
        /// <returns></returns>
        public static bool CheckOnceAgreement(string userName)
        {
            //確認是否有同意條款
            var m = _mp.MemberGet(userName);
            var paus = PcpFacade.GetPcpUserAgreeStatusById(m.UniqueId);
            if (!paus.IsLoaded)
            {
                return false;
            }

            return true;
        }

        public static bool AgreeMembershipCard(string userName, bool isAgree)
        {
            var m = _mp.MemberGet(userName);
            if (!m.IsLoaded)
            {
                return false;
            }

            if (isAgree)
            {
                PcpUserAgreeStatus puas = new PcpUserAgreeStatus()
                {
                    UserId = m.UniqueId,
                    AgreeTime = DateTime.Now,
                    SystemDataName = "PersonalMembershipCardAgree"
                };
                _pcp.PcpUserAgreeStatusSet(puas);
            }
            else //未來如果有取消可擴充
            {

            }

            return true;
        }

        private static string GetImageUrl(string imageValue, AppImageSize imageSize)
        {
            if (string.IsNullOrWhiteSpace(imageValue))
            {
                return "";
            }
            string imageName;
            switch (imageSize)
            {
                case AppImageSize.iOS2x:
                    imageName = imageValue.Replace(".", "@2x.");
                    break;
                case AppImageSize.iOS3x:
                    imageName = imageValue.Replace(".", "@3x.");
                    break;
                default:
                    imageName = imageValue;
                    break;
            }
            var combineDpi = string.Format("{0},{1}", Helper.GetEnumDescription(imageSize), imageName);
            return ImageFacade.GetMediaPathsFromRawData(combineDpi, MediaType.PCPImage).FirstOrDefault() ?? "";
        }

        public static bool SetMembershipCardListSeq(int memberUserId, List<int> userMembershipCards)
        {
            int cardSeqIndex = 1;

            UserMembershipCardCollection userMembershipCardCol = _pcp.UserMembershipCardGetByUserId(memberUserId);

            userMembershipCardCol.ForEach(delegate (UserMembershipCard card)
            {
                card.UserSeq = 0;
            });

            foreach (int cardId in userMembershipCards)
            {
                if (userMembershipCardCol.Any(x => x.Id == cardId))
                {
                    userMembershipCardCol.First(x => x.Id == cardId).UserSeq = cardSeqIndex++;
                }
            }

            foreach (var card in userMembershipCardCol.Where(x => x.UserSeq == 0).OrderBy(x => x.CreateTime))
            {
                card.UserSeq = cardSeqIndex++;
            }

            var result = _pcp.UserMembershipCardSet(userMembershipCardCol);

            return true;
        }


        public static bool RemoveMembershipCardList(int memberUserId, List<int> userCardIdList)
        {
            try
            {
                _pcp.UserMembershipCardRemove(userCardIdList, memberUserId);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static string CreateIdentityCodeUrl(int userMembershipCardId, int discountCodeId, int memberUserId)
        {
            ViewIdentityCode identityCode;

            //取得SellerMemberId

            var viewUserMembershipCardCol =
                _pcp.ViewUserMembershipCardGet("select * from view_user_membership_card where id='" +
                                               userMembershipCardId + "' ");

            var viewUserMembershipCard = viewUserMembershipCardCol.Any()
                ? viewUserMembershipCardCol.FirstOrDefault()
                : new ViewUserMembershipCard();
            if (viewUserMembershipCard == null)
            {
                return "";
            }

            if (discountCodeId == 0)
            {
                //不指定熟客券、公關券編號情況
                identityCode = BonusFacade.GenIdentityCode(viewUserMembershipCard.CardId,
                    viewUserMembershipCard.CardGroupId, DateTime.Now, memberUserId, 100);
            }
            else
            {
                //指定熟客券、公關券編號情況
                identityCode = BonusFacade.GenIdentityCode(viewUserMembershipCard.CardId,
                    viewUserMembershipCard.CardGroupId, DateTime.Now, memberUserId, 100, discountCodeId);
            }

            return _config.SiteUrl + "/vbs_r/verifycode?code=" + identityCode.Code;
        }

        public static MultipleUserMemberCardListModel GetUserMembershipCardListV2(int orderBy, double longitude,
            double latitude, int imageSize, string userName)
        {
            DateTime nowTime = DateTime.Now;
            List<UserMembershipCardListModel> userCardList = new List<UserMembershipCardListModel>();

            //有使用者的資訊
            if (!string.IsNullOrWhiteSpace(userName))
            {
                int userId = GetMemberUserId(userName);
                var checkUserOwner = _pcp.UserMembershipCardGetByUserId(userId);
                Regex r = new Regex(@"(?<lo>\d+\.?\d*)\s(?<la>\d+\.?\d*)");
                var userGeo =
                    LocationFacade.GetGeographyByCoordinate(string.Format("POINT ({0} {1})", longitude, latitude));

                ViewUserMembershipCardStoreCollection userCardStoreList =
                    _pcp.ViewUserMembershipCardStoreGetList(null,
                        ViewUserMembershipCardStore.Columns.UserId + " = " + userId,
                        ViewUserMembershipCardStore.Columns.Enabled + " = " + true,
                        ViewUserMembershipCardStore.Columns.StartTime + " <= " + nowTime,
                        ViewUserMembershipCardStore.Columns.EndTime + " >= " + nowTime,
                        ViewUserMembershipCardStore.Columns.Status + " = " + (byte)MembershipCardStatus.Open);

                SqlGeography geo = LocationFacade.GetGeographicPoint(latitude.ToString(), longitude.ToString());
                var imageData = VBSFacade.GetSellerImage(userCardStoreList.Select(x => x.CardGroupId).ToList<int>());
                var cardAvailableService = _pcp.PcpViewMembershipCardAvailableServiceCollectionGet();
                userCardList = (from g in userCardStoreList
                                group g by g.CardGroupId
                                       into items
                                let item = items.First()
                                let level1Item = items.FirstOrDefault(x => x.Level == (int)MembershipCardLevel.Level1)
                                let images = imageData.Where(x => x.GroupId == item.CardGroupId && x.Seq == 1).ToList()
                                let availableService = cardAvailableService.FirstOrDefault(x => x.GroupId == item.CardGroupId)
                                select new UserMembershipCardListModel()
                                {
                                    GroupId = item.CardGroupId,
                                    SellerName = item.SellerName,
                                    SellerFullCardThumbnailPath =
                                        Helper.GetPropValue(VBSFacade.GetImagePathByType(images, PcpImageType.CorporateImage, true), "result").ToString(),
                                    AvailableService = availableService == null ? "" :
                                        MembershipCardServerDesc(availableService.DepositService > 0 ? "可寄杯" : "", availableService.PointService > 0 ? "可集點" : ""),
                                    OfferDescription = (level1Item == null) ?
                                    "" : MembershipCardGroupOfferDescription(level1Item.PaymentPercent, level1Item.Others, level1Item.PaymentPercent, level1Item.Others),
                                    IconImagePath = GetImageUrl(item.IconImageValue, (AppImageSize)imageSize),
                                    BackgroundColor = item.BackgroundColorValue,
                                    BackgroundImagePath = GetImageUrl(item.BackgroundImageValue, (AppImageSize)imageSize),

                                    IsApply = checkUserOwner.Any(x => x.CardGroupId == item.CardGroupId
                                                                      && x.Enabled == true
                                                                      && DateTime.Today.IsBetween(x.StartTime, x.EndTime)
                                        ),
                                    PaymentPercent = item.PaymentPercent,
                                    Distance = items.Min(s => LocationFacade.GetDistance(geo,
                                                LocationFacade.GetGeographyByCoordinate(s.Coordinate))),
                                    UserSeq = item.UserSeq,
                                    Stores = (from store in items
                                              let storeGeo = LocationFacade.GetGeographyByCoordinate(store.Coordinate)
                                              let distance = LocationFacade.GetDistance(userGeo, storeGeo)
                                              orderby distance ascending
                                              select new StoreInfo()
                                              {
                                                  StoreName = store.StoreName,
                                                  Coordinates = new Coordinate
                                                  {
                                                      Longitude =
                                                     (store.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                                         ? Convert.ToDouble(r.Match(store.Coordinate).Groups["lo"].Value)
                                                         : 0,
                                                      Latitude =
                                                     (store.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                                         ? Convert.ToDouble(r.Match(store.Coordinate).Groups["la"].Value)
                                                         : 0
                                                  },
                                                  DistanceDesc =
                                                      (longitude.Equals(0) && latitude.Equals(0))
                                                          ? string.Empty
                                                          : LocationFacade.GetDistanceDesc(distance)
                                              }
                                        ).ToList(),
                                    UserCardId = item.Id,
                                    CreateTime = item.CreateTime
                                }).ToList();

            }

            //排序
            switch ((MembershipCardGroupOrderby)orderBy)
            {
                case MembershipCardGroupOrderby.Close: //離我最近
                    userCardList = userCardList.OrderBy(x => x.Distance).ToList();
                    break;
                case MembershipCardGroupOrderby.RecentlyUsed: //最近領取
                    userCardList = userCardList.OrderByDescending(x => x.CreateTime).ToList();
                    break;
                case MembershipCardGroupOrderby.CustomSeq:
                    if (userCardList.Any(x => x.UserSeq != 0)) //表示有排序過
                    {
                        userCardList = userCardList.OrderBy(x => x.UserSeq).ToList();
                    }
                    else
                    {
                        userCardList = userCardList.OrderByDescending(x => x.CreateTime).ToList();
                    }
                    break;
                default:
                    userCardList = userCardList.OrderByDescending(x => x.UserCardId).ToList();
                    break;
            }

            var resultModel = new MultipleUserMemberCardListModel();
            resultModel.UserCardList = userCardList;

            //會員尚未領取任何熟客卡
            if (userCardList.Any() == false)
            {
                resultModel.PromoGroupList =
                    GetMembershipCardGroupListForHotCardV2((AppImageSize) imageSize, longitude, latitude);
            }

            return resultModel;
        }

        public static MultipleUserMembershipCardModel GetUserMembershipCardList(int orderBy, double longitude,
            double latitude, int imageSize, string userName)
        {
            DateTime nowTime = DateTime.Now;
            var resultModel = new MultipleUserMembershipCardModel();
            resultModel.IsLogin = false;

            //有使用者的資訊
            if (!string.IsNullOrWhiteSpace(userName))
            {
                int userId = GetMemberUserId(userName);

                resultModel.IsLogin = true;

                #region UserCardList

                List<UserMembershipCardModel> userCardList = new List<UserMembershipCardModel>();
                Regex r = new Regex(@"(?<lo>\d+\.?\d*)\s(?<la>\d+\.?\d*)");
                var userGeo =
                    LocationFacade.GetGeographyByCoordinate(string.Format("POINT ({0} {1})", longitude, latitude));
                if (orderBy == (int)MembershipCardGroupOrderby.Close && longitude > 0 && latitude > 0)
                {
                    ViewUserMembershipCardStoreCollection userCardStoreList =
                        _pcp.ViewUserMembershipCardStoreGetList(null,
                            ViewUserMembershipCardStore.Columns.UserId + " = " + userId,
                            ViewUserMembershipCardStore.Columns.Enabled + " = " + true,
                            ViewUserMembershipCardStore.Columns.StartTime + " <= " + nowTime,
                            ViewUserMembershipCardStore.Columns.EndTime + " >= " + nowTime);

                    SqlGeography geo = LocationFacade.GetGeographicPoint(latitude.ToString(), longitude.ToString());
                    var imageData = VBSFacade.GetSellerImage(userCardStoreList.Select(x => x.CardGroupId).ToList<int>());

                    resultModel.UserCardList = new List<UserMembershipCardModel>();
                    foreach (var groupData in userCardStoreList.GroupBy(x => new { x.SellerName }))
                    {
                        ViewUserMembershipCardStore userCardStore = groupData.FirstOrDefault();
                        UserMembershipCardModel userCardModel = new UserMembershipCardModel
                        {
                            SellerName = userCardStore.SellerName,
                            //SellerImagePath = ((dynamic)VBSFacade.GetImagePathByType(imageData, userCardStore.SellerUserId, PcpImageType.CorporateImage)).result,
                            //SellerFullCardPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, userCardStore.SellerUserId, PcpImageType.Card))
                            //.result,
                            //SellerFullCardThumbnailPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, userCardStore.SellerUserId, PcpImageType.Card, true)).result,
                            SellerImagePath = Helper.GetPropValue(
                                VBSFacade.GetImagePathByType(imageData, userCardStore.CardGroupId,
                                    PcpImageType.CorporateImage), "result"
                                ).ToString(),
                            SellerFullCardPath = Helper.GetPropValue(
                                VBSFacade.GetImagePathByType(imageData, userCardStore.CardGroupId, PcpImageType.Card),
                                "result"
                                ).ToString(),
                            SellerFullCardThumbnailPath = Helper.GetPropValue(
                                VBSFacade.GetImagePathByType(imageData, userCardStore.CardGroupId, PcpImageType.Card,
                                    true), "result"
                                ).ToString(),
                            SellerCardType = userCardStore.CardType,
                            UserCardId = userCardStore.Id,
                            MembershipCardId = userCardStore.CardId,
                            Level = userCardStore.Level,
                            Enabled = userCardStore.Enabled,
                            PaymentPercent = userCardStore.PaymentPercent,
                            Instruction = userCardStore.Instruction,
                            OtherPremiums = userCardStore.Others,
                            OpenTime = ApiSystemManager.DateTimeToDateTimeString(userCardStore.OpenTime),
                            CloseTime = ApiSystemManager.DateTimeToDateTimeString(userCardStore.CloseTime),
                            HaveRegularsCode = false,
                            HaveFavorCode = false,
                            UserSeq = userCardStore.UserSeq,
                            IconImagePath = GetImageUrl(userCardStore.IconImageValue, (AppImageSize)imageSize),
                            BackgroundImage = GetImageUrl(userCardStore.BackgroundImageValue, (AppImageSize)imageSize),
                            BackgroundColor = userCardStore.BackgroundColorValue,
                            Distance =
                                groupData.Min(
                                    s =>
                                        LocationFacade.GetDistance(geo,
                                            LocationFacade.GetGeographyByCoordinate(s.Coordinate))),
                            OfferDescription =
                                           MembershipCardGroupOfferDescription(userCardStore.PaymentPercent, userCardStore.Others,
                                           userCardStore.PaymentPercent, userCardStore.Others),
                            Stores = (from store in userCardStoreList
                                      let storeGeo = LocationFacade.GetGeographyByCoordinate(store.Coordinate)
                                      let distance = LocationFacade.GetDistance(userGeo, storeGeo)
                                      orderby distance ascending
                                      select new StoreInfo()
                                      {
                                          StoreName = store.StoreName,
                                          Coordinates = new Coordinate
                                          {
                                              Longitude =
                                                     (store.Coordinate != null && !string.IsNullOrWhiteSpace(store.Coordinate))
                                                         ? Convert.ToDouble(r.Match(store.Coordinate).Groups["lo"].Value)
                                                         : 0,
                                              Latitude =
                                                     (store.Coordinate != null && !string.IsNullOrWhiteSpace(store.Coordinate))
                                                         ? Convert.ToDouble(r.Match(store.Coordinate).Groups["la"].Value)
                                                         : 0
                                          },
                                          DistanceDesc =
                                              (longitude.Equals(0) && latitude.Equals(0))
                                                  ? string.Empty
                                                  : LocationFacade.GetDistanceDesc(distance)
                                      }
                                  ).ToList()
                        };
                        userCardList.Add(userCardModel);
                    }
                }
                else
                {
                    var userMembershipCardCol =
                        _pcp.ViewUserMembershipCardGetList(ViewUserMembershipCard.Columns.UserSeq,
                            ViewUserMembershipCard.Columns.UserId + " = " + userId,
                            ViewUserMembershipCard.Columns.Enabled + " = " + true,
                            ViewUserMembershipCardStore.Columns.StartTime + " <= " + nowTime,
                            ViewUserMembershipCardStore.Columns.EndTime + " >= " + nowTime);
                    var imageData =
                        VBSFacade.GetSellerImage(userMembershipCardCol.Select(x => x.CardGroupId).ToList<int>());
                    userCardList = (from item in userMembershipCardCol
                                    select new UserMembershipCardModel
                                    {
                                        SellerName = item.SellerName,
                                        //SellerImagePath = ((dynamic)VBSFacade.GetImagePathByType(imageData, item.SellerUserId, PcpImageType.CorporateImage)).result,
                                        //SellerFullCardPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, item.SellerUserId, PcpImageType.Card))
                                        //.result,
                                        //SellerFullCardThumbnailPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, item.SellerUserId, PcpImageType.Card, true)).result,
                                        SellerImagePath = Helper.GetPropValue(
                                            VBSFacade.GetImagePathByType(imageData, item.CardGroupId, PcpImageType.CorporateImage),
                                            "result"
                                            ).ToString(),
                                        SellerFullCardPath = Helper.GetPropValue(
                                            VBSFacade.GetImagePathByType(imageData, item.CardGroupId, PcpImageType.Card), "result"
                                            ).ToString(),
                                        SellerFullCardThumbnailPath = Helper.GetPropValue(
                                            VBSFacade.GetImagePathByType(imageData, item.CardGroupId, PcpImageType.Card, true),
                                            "result"
                                            ).ToString(),
                                        SellerCardType = item.CardType,
                                        UserCardId = item.Id,
                                        MembershipCardId = item.CardId,
                                        Level = item.Level,
                                        Enabled = item.Enabled,
                                        PaymentPercent = item.PaymentPercent,
                                        Instruction = item.Instruction,
                                        OtherPremiums = item.Others,
                                        OpenTime = ApiSystemManager.DateTimeToDateTimeString(item.OpenTime),
                                        CloseTime = ApiSystemManager.DateTimeToDateTimeString(item.CloseTime),
                                        HaveRegularsCode = false,
                                        HaveFavorCode = false,
                                        UserSeq = item.UserSeq,
                                        IconImagePath = GetImageUrl(item.IconImageValue, (AppImageSize)imageSize),
                                        BackgroundImage = GetImageUrl(item.BackgroundImageValue, (AppImageSize)imageSize),
                                        BackgroundColor = item.BackgroundColorValue
                                    }).ToList();
                }
                //排序
                switch ((MembershipCardGroupOrderby)orderBy)
                {
                    case MembershipCardGroupOrderby.Hot: //尚未定義熱門條件
                        userCardList = userCardList.OrderByDescending(x => x.UserCardId).ToList();
                        break;
                    case MembershipCardGroupOrderby.Close:
                        userCardList = userCardList.OrderBy(x => x.Distance).ToList();
                        break;
                    case MembershipCardGroupOrderby.Newest:
                        userCardList = userCardList.OrderByDescending(x => x.UserCardId).ToList();
                        break;
                    case MembershipCardGroupOrderby.CustomSeq:
                        userCardList = userCardList.OrderBy(x => x.UserSeq).ToList();
                        break;
                    case MembershipCardGroupOrderby.DiscountPercent:
                        userCardList = userCardList.OrderBy(x => x.PaymentPercent).ToList();
                        break;
                    default:
                        userCardList = userCardList.OrderByDescending(x => x.UserCardId).ToList();
                        break;
                }
                resultModel.UserCardList.AddRange(userCardList);

                #endregion UserCardList
            }

            //會員尚未領取任何熟客卡或使用者未登入
            resultModel.PromoGroupList = GetMembershipCardGroupListForHotCard((AppImageSize)imageSize);
            return resultModel;
        }

        public static SingleUserMembershipCardModel GetUserMembershipCardModel(int userCardId, double longitude,
            double latitude, int imageSize, int memberUserId)
        {
            var userMembershipCardCol =
                _pcp.ViewUserMembershipCardGet(string.Format(
                    "select * from view_user_membership_card where [id] ='{0}'", userCardId));

            var userMembershipCard = userMembershipCardCol.Any()
                ? userMembershipCardCol.FirstOrDefault()
                : new ViewUserMembershipCard();

            if (userMembershipCard == null)
            {
                return new SingleUserMembershipCardModel();
            }
            var vbsCompany = _pcp.VbsCompanyDetailGet(userMembershipCard.SellerGuid);
            //取得MembershipCard的級距
            var viewMembershipCard = _pcp.ViewMembershipCardGet(userMembershipCard.CardId);

            if (viewMembershipCard == null)
            {
                return new SingleUserMembershipCardModel();
            }

            var imageData = VBSFacade.GetSellerImage(new List<int> { userMembershipCard.CardGroupId });

            var storeCol = _sp.VbsSellerGuidGet(userMembershipCard.SellerUserId).FirstOrDefault();
            var dealList = new List<PponDealSynopsis>();
            if (storeCol != null)
            {
                var deals = _pp.ViewPponDealGetBySellerGuid(storeCol.Guid);

                var queryDeals = deals
                    .Where(x =>
                        x.BusinessHourOrderTimeS <= DateTime.Now
                        && DateTime.Now <= x.BusinessHourOrderTimeE
                        && !Helper.IsFlagSet(x.BusinessHourStatus, (int)BusinessHourStatus.ComboDealSub)
                    )
                    .Select(x => x.BusinessHourGuid).ToList();

                dealList = PponDealApiManager.PponDealSynopsesGetListByBids(queryDeals);
            }

            var userMembershipCardModel = new SingleUserMembershipCardModel()
            {
                SellerName = userMembershipCard.SellerName,
                SellerImagePath =
                    ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.CorporateImage)).result,
                SellerFullCardPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.Card)).result,
                SellerFullCardThumbnailPath =
                    ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.Card, true)).result,
                //SellerImagePath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.CorporateImage), "result").ToString(),
                //SellerFullCardPath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.Card), "result").ToString(),
                //SellerFullCardThumbnailPath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.Card, true), "result").ToString(),
                //ccmark:undo
                SellerIntro = _pcp.PcpIntroGet(PcpIntroType.Seller, userMembershipCard.CardGroupId).OrderBy(x => x.Id)
                    .Select(x => x.IntroContent).ToArray(),
                SellerServiceImgPath = ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.Service)).result,
                SellerServiceImgThumbnailPath =
                    ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.Service, true)).result,
                SellerSrndgImgPath =
                    ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.EnvironmentAroundStore)).result,
                SellerSrndgThumbnailPath =
                    ((dynamic)VBSFacade.GetImagePathByType(imageData, PcpImageType.EnvironmentAroundStore, true))
                        .result,
                SellerCardType = userMembershipCard.CardType,
                GroupId = userMembershipCard.CardGroupId,
                UserCardId = userMembershipCard.CardId,
                UserCardNo = userMembershipCard.CardNo,
                Level = userMembershipCard.Level,
                Enabled = userMembershipCard.Enabled,
                PaymentPercent = userMembershipCard.PaymentPercent,
                Instruction = userMembershipCard.Instruction,
                OtherPremiums = userMembershipCard.Others,
                OpenTime = ApiSystemManager.DateTimeToDateTimeString(userMembershipCard.OpenTime),
                CloseTime = ApiSystemManager.DateTimeToDateTimeString(userMembershipCard.OpenTime),
                UserSeq = userMembershipCard.UserSeq,
                OrderCount = userMembershipCard.OrderCount,
                AmountTotal = (double)userMembershipCard.AmountTotal,
                OrderNeeded =
                    (userMembershipCard.Level <= (int)MembershipCardLevel.Level1)
                        ? 0
                        : (viewMembershipCard.OrderNeeded.HasValue ? viewMembershipCard.OrderNeeded.Value : 0),
                AmountNeeded =
                    (userMembershipCard.Level <= (int)MembershipCardLevel.Level1)
                        ? 0
                        : (viewMembershipCard.AmountNeeded.HasValue ? (double)viewMembershipCard.AmountNeeded.Value : 0),
                ConditionalLogic = userMembershipCard.ConditionalLogic,
                IconImagePath = GetImageUrl(userMembershipCard.IconImageValue, (AppImageSize)imageSize),
                BackgroundImage = GetImageUrl(userMembershipCard.BackgroundImageValue, (AppImageSize)imageSize),
                BackgroundColor = userMembershipCard.BackgroundColorValue,
                CardQRCodeUrl = CreateIdentityCodeUrl(userMembershipCard.Id, 0, memberUserId),
                CardValidDate =
                    vbsCompany.ContractValidDate.HasValue
                        ? ApiSystemManager.DateTimeToDateTimeString(vbsCompany.ContractValidDate.Value)
                        : ApiSystemManager.DateTimeToDateTimeString(DateTime.Now.AddYears(1)),
                LastModifyTime =
                    userMembershipCard.ModifyTime.HasValue
                        ? ApiSystemManager.DateTimeToDateTimeString(userMembershipCard.ModifyTime.Value)
                        : ApiSystemManager.DateTimeToDateTimeString(userMembershipCard.CreateTime),
                DealList = dealList
            };
            //Cards
            userMembershipCardModel.Cards =
                (from item in _pcp.ViewMembershipCardGetListByDatetime(userMembershipCard.CardGroupId, DateTime.Now)
                    .Where(x => x.Status == (int)MembershipCardStatus.Open && x.Enabled)
                 select new CardDetail
                 {
                     CardId = item.CardId,
                     Level = item.Level,
                     Enabled = item.Enabled,
                     PaymentPercent = item.PaymentPercent,
                     Instruction = item.Instruction,
                     OtherPremiums = item.Others,
                     AvailableDateType = item.AvailableDateType,
                     AvailableDateDesc = Helper.GetEnumDescription(((AvailableDateType)item.AvailableDateType)),
                     OrderNeeded = item.OrderNeeded ?? 0,
                     AmountNeeded = Convert.ToDouble((item.AmountNeeded ?? 0)),
                     ConditionalLogic = item.ConditionalLogic,
                     OpenTime = ApiSystemManager.DateTimeToDateTimeString(item.OpenTime),
                     CloseTime = ApiSystemManager.DateTimeToDateTimeString(item.CloseTime),
                     CombineUse = item.CombineUse
                 }).ToList();
            CardDetail firstCard = userMembershipCardModel.Cards.FirstOrDefault();
            CardDetail lastCard = userMembershipCardModel.Cards.LastOrDefault();
            double firstPercent = 0, lastPercent = 0;
            string firstOtherOffer = string.Empty, lastOtherOffer = string.Empty;
            if (firstCard != null)
            {
                firstPercent = firstCard.PaymentPercent;
                firstOtherOffer = firstCard.OtherPremiums ?? "";
            }
            if (lastCard != null)
            {
                lastPercent = lastCard.PaymentPercent;
                lastOtherOffer = lastCard.OtherPremiums ?? "";
            }

            userMembershipCardModel.OfferDescription = MembershipCardGroupOfferDescription
                (firstPercent, firstOtherOffer, lastPercent, lastOtherOffer);

            //Stores
            var stores = _pcp.MembershipCardGroupStoreGet(viewMembershipCard.CardGroupId);

            var userGeo = LocationFacade.GetGeographyByCoordinate(string.Format("POINT ({0} {1})", longitude, latitude));
            Regex r = new Regex(@"(?<lo>\d+\.?\d*)\s(?<la>\d+\.?\d*)");
            userMembershipCardModel.Stores =
                (from item in _sp.SellerGetListBySellerGuidList(stores.Select(x => x.StoreGuid).ToList())
                 let matchCoor = r.Match(item.Coordinate ?? "")
                 let storeGeo = LocationFacade.GetGeographyByCoordinate(item.Coordinate ?? "")
                 let distance = LocationFacade.GetDistance(userGeo, storeGeo)
                 orderby distance ascending
                 select new UserMembershipCardStore
                 {
                     StoreGuid = item.Guid,
                     StoreName = item.SellerName,
                     Phone = item.StoreTel,
                     Address =
                         item.StoreTownshipId == null
                             ? ""
                             : CityManager.CityTownShopStringGet(item.StoreTownshipId.Value) + item.StoreAddress,
                     OpenTime = item.OpenTime,
                     CloseDate = item.CloseDate,
                     Remarks = item.SellerRemark,
                     Mrt = item.Mrt,
                     Car = item.Car,
                     Bus = item.Bus,
                     OtherVehicles = item.OtherVehicles,
                     WebUrl = item.WebUrl,
                     FbUrl = item.FacebookUrl,
                     //PlurkUrl = item.PlurkUrl, SellerTree合併後沒有PlurkUrl
                     BlogUrl = item.BlogUrl,
                     OtherUrl = item.OtherUrl,
                     CreditcardAvailable = item.CreditcardAvailable,
                     Coordinates = new Coordinate
                     {
                         Longitude = (item.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                    ? Convert.ToDouble(r.Match(item.Coordinate).Groups["lo"].Value) : 0,
                         Latitude = (item.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                   ? Convert.ToDouble(r.Match(item.Coordinate).Groups["la"].Value) : 0
                     },
                     DistanceDesc =
                         (longitude.Equals(0) && latitude.Equals(0))
                             ? string.Empty
                             : LocationFacade.GetDistanceDesc(LocationFacade.GetDistance(userGeo, storeGeo))
                 }
                    ).ToList();
            //Logs
            //userMembershipCardModel.Logs=new List<UserMembershipCardEventLog>();
            var logDatas = new List<UserMembershipCardEventLog>();
            var logList = _mp.ViewMembershipCardLogGetListByUserCardId(userMembershipCard.Id,
                MembershipCardLogType.System);

            logDatas.AddRange((from item in logList
                               select new UserMembershipCardEventLog
                               {
                                   Memo = item.Memo,
                                   LogTime = ApiSystemManager.DateTimeToDateTimeString(item.CreateTime)
                               }).ToList()
                );
            userMembershipCardModel.Logs = logDatas;
            return userMembershipCardModel;
        }

        public static int GetMemberUserId(string userName)
        {
            int memberUserId = MemberFacade.GetUniqueId(userName);
            return memberUserId;
        }

        public static string MembershipCardGroupOfferDescription(ViewMembershipCardStore firstData,
            ViewMembershipCardStore lastData)
        {
            return MembershipCardGroupOfferDescription(firstData.PaymentPercent, firstData.Others ?? "",
                lastData.PaymentPercent, lastData.Others ?? "");
        }

        public static string MembershipCardGroupOfferDescription(double firstPercent, string firstOtherOffer,
            double lastPercent, string lastOtherOffer)
        {
            string offer = string.Empty;

            //只有一張卡片
            if (firstPercent.Equals(lastPercent) && firstOtherOffer.Equals(lastOtherOffer))
            {
                if (firstPercent > 0 && firstPercent < 1)
                {
                    offer = string.Format("憑熟客卡{0:0.#} 折", firstPercent * 100).Replace("0", "");
                }
                else
                {
                    offer = string.Format("{0}", firstOtherOffer);
                }
            }
            else
            {
                if (firstPercent > 0 && firstPercent < 1 &&
                    lastPercent > 0 && lastPercent < 1)
                {
                    offer = string.Format("憑熟客卡{0:0.#} ～ {1:0.#} 折", firstPercent * 100,
                        lastPercent * 100).Replace("0", "");
                }
                else if (firstPercent > 0 && firstPercent < 1)
                {
                    offer = string.Format("憑熟客卡{0:0.#} 折", firstPercent * 100).Replace("0", "");
                }
                else
                {
                    offer = string.Format("{0}", firstOtherOffer);
                }
            }
            return offer;
        }

        private static string MembershipCardServerDesc(params string[] serviceNameArray)
        {

            string[] list = serviceNameArray.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            return string.Join("/", list);
        }

        private const string PersonalDataCollectMatterFormatVersionExtendedPropertyName =
            "personal_data_collect_matter_version";

        private static int _personalDataCollectMatterFormatVersion;

        public static ApiVersionItem PersonalDataCollectMatterFormatVersion
        {
            get
            {
                string cityGroupVersionData =
                    _sysp.DBExtendedPropertiesGet(PersonalDataCollectMatterFormatVersionExtendedPropertyName);
                //若查無資料或取出的值不為int，預設為0
                if (!int.TryParse(cityGroupVersionData, out _personalDataCollectMatterFormatVersion))
                {
                    _personalDataCollectMatterFormatVersion = 0;
                }


                return new ApiVersionItem("PersonalDataCollectMatterFormatVersion",
                    _personalDataCollectMatterFormatVersion.ToString());
            }
        }

        private static Bitmap GetQRCdoeBitmap(string content)
        {
            //string content = string.Format(@"https://www.17life.com/Openapp.aspx?groupid={0}&userid={1}", GroupId, GroupId);
            var hints = new Hashtable();
            hints.Add(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            //hints.Add(EncodeHintType.CHARACTER_SET, "UTF-8");

            Writer writer = new MultiFormatWriter();
            var matrix = writer.encode(content, BarcodeFormat.QR_CODE, 480, 480, hints);

            int width = matrix.Width;
            int height = matrix.Height;
            Bitmap result;
            using (Bitmap bmap = new Bitmap(width, height, PixelFormat.Format32bppArgb))
            {
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        bmap.SetPixel(x, y,
                            matrix.get_Renamed(x, y) != -1
                                ? ColorTranslator.FromHtml("Black")
                                : ColorTranslator.FromHtml("White"));
                    }
                }
                Rectangle cloneRect = new Rectangle(0, 0, 480, 480);
                PixelFormat format = bmap.PixelFormat;
                //Bitmap cloneBitmap = bmap.Clone(cloneRect, format);
                result = bmap.Clone(cloneRect, format);
            }
            return result;
        }

        public static byte[] GetMembershipCardGroupQRCdoe(string content)
        {
            var tmp = GetQRCdoeBitmap(content);
            return GetImageBytes(tmp);
        }


        public static byte[] GetMembershipCardGroup(string groupId, string sellerName)
        {
            //var content = string.Format("{0}/App/MembershipCard?groupId={1}", _config.SiteUrl,groupId);
            var content = string.Format("https://www.17life.com/App/MembershipCard?groupId={0}", groupId);
            var bmap = GetQRCdoeBitmap(content);

            RectangleF rectf = new RectangleF(25, 0, 430, 100);

            Graphics g = Graphics.FromImage(bmap);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.DrawString(sellerName, new Font("Comic Sans MS", 25), Brushes.Black, rectf);

            g.Flush();

            return GetImageBytes(bmap);

        }

        private static byte[] GetImageBytes(Image image)
        {
            ImageCodecInfo codec = null;
            foreach (ImageCodecInfo e in ImageCodecInfo.GetImageEncoders())
            {
                if (e.MimeType == "image/png")
                {
                    codec = e;
                    break;
                }
            }

            using (EncoderParameters ep = new EncoderParameters())
            {
                ep.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, codec, ep);
                    return ms.ToArray();
                }
            }
        }

        public static List<SellerModel> GetMembershipCardGroupForSal(string sellerName)
        {
            var result = new List<SellerModel>();

            var accountTypes = new List<VbsMembershipAccountType> { VbsMembershipAccountType.VendorAccount };
            var members = _mp.VbsMembershipGetListByAccountName(sellerName, accountTypes, true);
            if (members.FirstOrDefault() == null)
            {
                return null;
            }

            var userIds = members.Select(x => x.UserId.Value).ToList();
            var cards = _pcp.ViewMembershipCardGetListBySellerUserId(userIds);

            foreach (var item in members)
            {
                var seller = new SellerModel(item.Name);
                var card = cards.Where(x => x.SellerUserId == item.UserId);

                if (card.Any())
                {
                    seller.GroupId = card.First().CardGroupId.ToString();
                    var lvls = card.Select(x => Helper.GetEnumDescription((MembershipCardLevel)x.Level)).ToArray();
                    seller.MembershipCardLevels = string.Join(",", lvls);
                }
                result.Add(seller);
            }

            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name="isAll">true:全部資料，false :10筆資料</param>
        /// <returns></returns>
        public static List<MembershipCardBrandModel> GetMembershipCardBrandList(bool isAll)
        {
            var data = _pcp.ViewMembershipCardStoreCollectionGetByPromo();
            //TODO: Group by 歐克老特例，分店有自己的熟客卡
            var imageData = VBSFacade.GetSellerImage(data.Select(x => x.CardGroupId).ToList<int>());
            var result = (from g in data
                          group g by g.CardGroupId
                          into items
                          let item = items.First()
                          let images = imageData.Where(x => x.GroupId == item.CardGroupId && x.Seq == 1).ToList()
                          let pcpIntro = _pcp.PcpIntroGetByCardGroupId(PcpIntroType.Seller, item.CardGroupId).FirstOrDefault()
                          select new MembershipCardBrandModel()
                          {
                              GroupId = item.CardGroupId,
                              BrandName = item.SellerName,
                              BrandInfo = (pcpIntro == null) ? "" : pcpIntro.IntroContent,
                              BrandImagePath =
                                  Helper.GetPropValue(VBSFacade.GetImagePathByType(images, PcpImageType.CorporateImage, true), "result").ToString(),
                              LogoImagePath = Helper.GetPropValue(VBSFacade.GetImagePathByType(images, PcpImageType.Logo, true), "result").ToString(),
                              StoreCount = items.Count(x => x.CardLevel == (int)MembershipCardLevel.Level0)
                                           + FindOtherGroupIdByStore(item.StoreGuid).Sum(id => _pcp.MembershipCardGroupStoreGet(id).Count)
                          }
                );

            if (isAll == false)
            {
                return result.Take(10).ToList();
            }

            return result.ToList();
        }


        /// <summary>
        /// 取得其他相關店家GroupId
        /// </summary>
        /// <param name="storeGuid"></param>
        /// <returns></returns>
        private static List<int> FindOtherGroupIdByStore(Guid storeGuid)
        {
            SellerTree sellerTree = _sp.SellerTreeGetListBySellerGuid(storeGuid).FirstOrDefault();
            var originalGroupId = _pcp.MembershipCardGroupStoreGetBySellerGuid(storeGuid);
            var groupIds = new List<int>();
            //groupId 找到 seller_guid_root，關聯membership_card_group，取出所有 groupId
            if (sellerTree != null && sellerTree.IsLoaded)
            {
                var sellerGuids =
                    SellerFacade.GetSellerTreeCol(sellerTree.RootSellerGuid).Select(x => x.SellerGuid).ToList();
                var gIds = _pcp.MembershipCardGroupGetBySellerGuids(sellerGuids)
                               .Where(x => x.Status == (int)MembershipCardStatus.Open).Select(x => x.Id).ToList();
                groupIds.AddRange(gIds);
                groupIds.Remove(originalGroupId.CardGroupId); //移除原有的重複的
            }

            return groupIds;
        }

        public static List<MembershipCardInfo> GetMembershipCardStoreInfoList(MembershipCardGroupListQueryModel q)
        {
            int userId = 0;
            if (!string.IsNullOrWhiteSpace(q.UserName))
            {
                var mem = _mp.MemberGet(q.UserName);
                userId = mem.IsLoaded ? mem.UniqueId : 0;
            }

            var groupIds = new List<int> { q.GroupId };
            var membershipCardGroup = _pcp.MembershipCardGroupStoreGet(q.GroupId);
            groupIds.AddRange(FindOtherGroupIdByStore(membershipCardGroup.First().StoreGuid));
            groupIds = groupIds.Distinct().ToList();

            var checkUserOwner = _pcp.UserMembershipCardGetByUserId(userId);
            Regex r = new Regex(@"(?<lo>\d+\.?\d*)\s(?<la>\d+\.?\d*)");
            var userGeo =
                LocationFacade.GetGeographyByCoordinate(string.Format("POINT ({0} {1})", q.Longitude, q.Latitude));
            var imageData = VBSFacade.GetSellerImage(groupIds);
            var cardAvailableService = _pcp.PcpViewMembershipCardAvailableServiceCollectionGet();
            var result = (from id in groupIds
                          let data = _pcp.ViewMembershipCardStoreGetByLastGroupId(id)
                          let card = data.FirstOrDefault()
                          let images = imageData.Where(x => x.GroupId == id && x.Seq == 1).ToList()
                          let level1Item = data.FirstOrDefault(x => x.CardLevel == (int)MembershipCardLevel.Level1)
                          let availableService = (card == null) ? null : cardAvailableService.FirstOrDefault(x => x.GroupId == card.CardGroupId)
                          where card != null && card.IsLoaded
                          select new MembershipCardInfo()
                          {
                              GroupId = id,
                              SellerName = card.SellerName,
                              SellerFullCardThumbnailPath =
                                 Helper.GetPropValue(VBSFacade.GetImagePathByType(images, PcpImageType.CorporateImage, true), "result").ToString(),
                              AvailableService = availableService == null ? "" :
                                        MembershipCardServerDesc(availableService.DepositService > 0 ? "可寄杯" : "", availableService.PointService > 0 ? "可集點" : ""),
                              OfferDescription =
                                 MembershipCardGroupOfferDescription(level1Item ?? new ViewMembershipCardStore(), level1Item ?? new ViewMembershipCardStore()),
                              IconImagePath = GetImageUrl(card.IconImageValue, (AppImageSize)q.GetAppImageSizeEnum),
                              BackgroundColor = card.BackgroundColorValue,
                              BackgroundImagePath = GetImageUrl(card.BackgroundImageValue, (AppImageSize)q.GetAppImageSizeEnum),
                              IsApply =
                                  checkUserOwner.Any(
                                      x =>
                                          x.CardGroupId == card.CardGroupId && x.Enabled == true &&
                                          DateTime.Today.IsBetween(x.StartTime, x.EndTime)),
                              Stores = (from store in data.Where(x => x.CardLevel == (int)MembershipCardLevel.Level0)
                                        let storeGeo = LocationFacade.GetGeographyByCoordinate(store.Coordinate)
                                        let distance = LocationFacade.GetDistance(userGeo, storeGeo)
                                        orderby distance ascending
                                        select new StoreInfo()
                                        {
                                            StoreName = store.StoreName,
                                            Coordinates = new Coordinate
                                            {
                                                Longitude =
                                                     (store.Coordinate != null && !string.IsNullOrWhiteSpace(store.Coordinate))
                                                         ? Convert.ToDouble(r.Match(store.Coordinate).Groups["lo"].Value)
                                                         : 0,
                                                Latitude =
                                                     (store.Coordinate != null && !string.IsNullOrWhiteSpace(store.Coordinate))
                                                         ? Convert.ToDouble(r.Match(store.Coordinate).Groups["la"].Value)
                                                         : 0
                                            },
                                            DistanceDesc =
                                                (q.Longitude.Equals(0) && q.Latitude.Equals(0))
                                                    ? string.Empty
                                                    : LocationFacade.GetDistanceDesc(distance)
                                        }
                                  ).ToList()

                          }).ToList();

            return result;
        }

        public static MembershipCardDetail GetMembershipCardDetail(MembershipCardGroupListQueryModel q, bool isNewPrototype)
        {

            int userId = 0;
            var result = new MembershipCardDetail();

            if (!string.IsNullOrWhiteSpace(q.UserName))
            {
                var mem = _mp.MemberGet(q.UserName);
                userId = mem.IsLoaded ? mem.UniqueId : 0;
            }

            result.GroupId = q.GroupId;
            var cardGroup = _pcp.MembershipCardGroupGet(q.GroupId);

            #region 商家功能模組(會員優惠、點數功能、寄杯功能)有無開啟的檢查
            MembershipCardGroupPermissionCollection permissions = _pcp.MembershipCardGroupPermissionGetList(q.GroupId);
            result.IsVipOn = PcpFacade.IsRegularsServiceOn(q.GroupId, permissions, MembershipService.Vip);
            result.IsPointOn = PcpFacade.IsRegularsServiceOn(q.GroupId, permissions, MembershipService.Point);
            result.IsDepositOn = PcpFacade.IsRegularsServiceOn(q.GroupId, permissions, MembershipService.Deposit);
            #endregion

            #region 熟客系統基本設定
            var imageData = VBSFacade.GetSellerImage(q.GroupId);
            //KA卡圖片，固定吃形象圖第一張
            result.SellerFullCardPath =
                Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.CorporateImage), "result").ToString();
            //KA卡圖片(縮)
            result.SellerFullCardThumbnailPath =
                Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.CorporateImage, true), "result").ToString();
            //形象圖片
            result.SellerImagePath =
                 (from item in imageData.Where(x => x.ImgType == PcpImageType.CorporateImage)
                  orderby item.Seq ascending
                  let path = ImageFacade.GetMediaPath(item.ImageCompressedPathUrl, MediaType.PCPImage)
                  select path).ToList();
            //商家介紹
            result.SellerIntro =
                _pcp.PcpIntroGetByCardGroupId(PcpIntroType.Seller, q.GroupId)
                    .OrderBy(x => x.Id)
                    .Select(x => x.IntroContent)
                    .ToList();
            //行銷跑馬燈
            result.MarqueePrmo = _pcp.PcpMarqueeGetListByCardGroupId(q.GroupId).Select(x => x.Marquee).ToList();
            //商家所販賣內容圖片
            result.SellerServiceImgPath =
                 (from item in imageData.Where(x => x.ImgType == PcpImageType.Service)
                  orderby item.Seq ascending
                  let path = ImageFacade.GetMediaPath(item.ImagePathUrl, MediaType.PCPImage)
                  select path).ToList();

            result.SellerServiceImgThumbnailPath =
                (from item in imageData.Where(x => x.ImgType == PcpImageType.Service)
                 orderby item.Seq ascending
                 let path = ImageFacade.GetMediaPath(item.ImageCompressedPathUrl, MediaType.PCPImage)
                 select path).ToList();
            //商家環境
            result.SellerSrndgImgPath =
                (from item in imageData.Where(x => x.ImgType == PcpImageType.EnvironmentAroundStore)
                 orderby item.Seq ascending
                 let path = ImageFacade.GetMediaPath(item.ImagePathUrl, MediaType.PCPImage)
                 select path).ToList();
            result.SellerSrndgThumbnailPath =
                 (from item in imageData.Where(x => x.ImgType == PcpImageType.EnvironmentAroundStore)
                  orderby item.Seq ascending
                  let path = ImageFacade.GetMediaPath(item.ImageCompressedPathUrl, MediaType.PCPImage)
                  select path).ToList();

            #region 舊的卡片樣式 (熟客系統改版後店家一定會有形象圖，沒有卡片樣式，也無從設定卡片樣式) (此區塊早晚要移除)
            var membershipCardStores = _pcp.ViewMembershipCardStoreGetByLastGroupId(q.GroupId);
            var firstStore = membershipCardStores.FirstOrDefault();
            if (firstStore != null)
            {
                result.BackgroundImagePath = GetImageUrl(firstStore.BackgroundImageValue, q.GetAppImageSizeEnum);
                result.BackgroundColor = firstStore.BackgroundColorValue;
                result.IconImagePath = GetImageUrl(firstStore.IconImageValue, q.GetAppImageSizeEnum);
            }
            #endregion

            #endregion


            if (result.IsVipOn)
            {
                #region 會員優惠

                var viewMembershipCardList = _pcp.ViewMembershipCardGetListByDatetime(q.GroupId, DateTime.Now).Where(x => x.Status == (int)MembershipCardStatus.Open && x.Enabled).ToList();
                ViewMembershipCard level1Card;
                CardDetail level1CardDetail;

                if (viewMembershipCardList != null && viewMembershipCardList.Count > 0)
                {
                    //優惠一律顯示普卡資訊!
                    level1Card = viewMembershipCardList.FirstOrDefault(x => x.Level == (int)MembershipCardLevel.Level1);

                    if (level1Card != null)
                    {
                        //「會員優惠」區塊是否要顯示的判斷
                        result.IsUseOffer = true;

                        if (!string.IsNullOrWhiteSpace(level1Card.Instruction))
                        {
                            result.Remark.Add(level1Card.Instruction);
                        }

                        result.Remark.Add(remark0);
                        result.Remark.Add(remark2);

                        level1CardDetail = new CardDetail()
                        {
                            CardId = level1Card.CardId,
                            Level = level1Card.Level,
                            Enabled = level1Card.Enabled,
                            PaymentPercent = level1Card.PaymentPercent,
                            Instruction = level1Card.Instruction,
                            OtherPremiums = level1Card.Others,
                            AvailableDateType = level1Card.AvailableDateType,
                            AvailableDateDesc = Helper.GetEnumDescription(((AvailableDateType)level1Card.AvailableDateType)),
                            OrderNeeded = level1Card.OrderNeeded ?? 0,
                            AmountNeeded = Convert.ToDouble((level1Card.AmountNeeded ?? 0)),
                            ConditionalLogic = level1Card.ConditionalLogic,
                            OpenTime = ApiSystemManager.DateTimeToDateTimeString(level1Card.OpenTime),
                            CloseTime = ApiSystemManager.DateTimeToDateTimeString(level1Card.CloseTime),
                            CombineUse = level1Card.CombineUse
                        };

                        #region 組優惠折數字串    
                        double level1CardPercent = 0;
                        string level1CardOtherOffer = string.Empty;

                        level1CardPercent = level1CardDetail.PaymentPercent;
                        level1CardOtherOffer = level1CardDetail.OtherPremiums ?? "";
                        result.OfferDescription = MembershipCardGroupOfferDescription(level1CardPercent, level1CardOtherOffer, level1CardPercent, level1CardOtherOffer);
                        #endregion

                        result.Card = level1CardDetail;

                        var card = _pcp.MembershipCardGet(level1Card.CardId);
                        result.LastModifyTime = card.ModifyTime.HasValue
                                ? ApiSystemManager.DateTimeToDateTimeString(card.ModifyTime.Value)
                                : ApiSystemManager.DateTimeToDateTimeString(card.CreateTime);
                    }
                }


                //預設取user Level1 熟客卡
                var cardUserOwner = _pcp.ViewUserMembershipCardGet(userId, q.GroupId)
                                         .Where(x => x.CardGroupId == q.GroupId &&
                                                     x.Enabled == true &&
                                                     DateTime.Today.IsBetween(x.StartTime, x.EndTime));

                result.IsApply = cardUserOwner.Any();
                result.CurrentLevel = result.IsApply ? cardUserOwner.LastOrDefault().Level : (int)MembershipCardLevel.Level0;

                //熟客卡其他相關欄位 (vbs_company_detail)
                var companyDetail = _pcp.VbsCompanyDetailGet(cardGroup.SellerGuid);
                if (companyDetail.IsLoaded)
                {
                    result.SellerName = companyDetail.SellerName;
                    result.CardValidDate = companyDetail.ContractValidDate.HasValue
                        ? ApiSystemManager.DateTimeToDateTimeString(companyDetail.ContractValidDate.Value)
                        : ApiSystemManager.DateTimeToDateTimeString(DateTime.Now.AddYears(1));
                }
                #endregion
            }

            if (result.IsPointOn)
            {
                #region 點數
                var collectRule = _pcp.PcpPointCollectRuleGetListByCardGroupId(q.GroupId).FirstOrDefault(x => x.Enabled == true);
                var exchangeRule = _pcp.PcpPointExchangeRuleGetListByCardGroupId(q.GroupId);

                if (exchangeRule.Any())
                {
                    //「點數功能」區塊是否要顯示的判斷
                    result.IsUsePoint = true;

                    //●集點規則
                    if (collectRule != null)
                    {
                        result.PointActiveContent.CollectRule = "到店消費金額" + collectRule.CollectValue + "元，可獲得 1 點";
                    }
                    //●兌點規則
                    if (exchangeRule.Any())
                    {
                        foreach (var e in exchangeRule)
                        {
                            result.PointActiveContent.ExchangeRule.Add("累積" + e.Point + "點兌換" + e.ItemName);
                        }
                    }
                    //●點數注意事項
                    var pointRemarks = _pcp.PcpPointRemarkGetListByCardGroupId(q.GroupId);
                    foreach (var remark in pointRemarks)
                    {
                        result.PointActiveContent.Remark.Add(remark.Remark);
                    }
                    result.PointActiveContent.Remark.Add(remark1);
                    result.PointActiveContent.Remark.Add(remark2);
                    result.PointActiveContent.Remark.Add(remark3);

                    //使用者的餘點
                    var userPoint = PcpFacade.GetUserRemainPoint(q.GroupId, userId); //_pcp.PcpUserRemainPointListGetByGroupIdAndUserId(q.GroupId, userId);
                    result.UserPoint = userPoint;
                }
                #endregion
            }

            if (result.IsDepositOn)
            {
                #region 寄杯
                //●寄杯活動列表
                var menuItemCollection = _pcp.ViewPcpSellerMenuItemCollectionGetByCardGroupIdInEffective(q.GroupId)
                                             .Select(s => new
                                             {
                                                 Title = s.Title,
                                                 ItemName = s.ItemName,
                                             });

                result.DepositActiveContent.Active = menuItemCollection.GroupBy(x => x.Title)
                                                                       .Select(s => new
                                                                       {
                                                                           Result = s.Key + ":" + string.Join("/", s.Select(x => x.ItemName))
                                                                       }).Select(x => x.Result).ToList();

                result.DepositActiveContent.Remark.Add(remark1);
                result.DepositActiveContent.Remark.Add(remark2);

                //使用者剩餘寄杯數量
                result.UserDeposit = PcpFacade.GetViewPcpUserDepositListByUserIdAndCardGroupId(userId, q.GroupId)
                                              .Sum(x => x.RemainAmount);

                //「寄杯功能」區塊是否要顯示的判斷
                if (menuItemCollection.Any())
                {
                    result.IsUseDeposit = true;
                }
                #endregion
            }


            //result.AvailableService = MembershipCardServerDesc(
            //    result.DepositActiveContent.Active.Count > 0 ? "可寄杯" : "", (collectRule != null) ? "可集點" : "");
            string pointService = (result.IsPointOn && result.IsUsePoint) ? "可寄杯" : string.Empty;
            string depositService = (result.IsDepositOn && result.IsUseDeposit) ? "可寄杯" : string.Empty;
            result.AvailableService = MembershipCardServerDesc(pointService, depositService);


            #region 適用店家

            var storeGuids = _pcp.MembershipCardGroupStoreGet(q.GroupId).Select(x => x.StoreGuid).ToList();
            Regex r = new Regex(@"(?<lo>\d+\.?\d*)\s(?<la>\d+\.?\d*)");
            var userGeo =
                LocationFacade.GetGeographyByCoordinate(string.Format("POINT ({0} {1})", q.Longitude, q.Latitude));
            result.Stores =
                (from item in _sp.SellerGetListBySellerGuidList(storeGuids).Where(x => x.StoreStatus == (int)StoreStatus.Available)
                 let matchCoor = r.Match(item.Coordinate ?? "")
                 let storeGeo = LocationFacade.GetGeographyByCoordinate(item.Coordinate ?? "")
                 let distance = LocationFacade.GetDistance(userGeo, storeGeo)
                 orderby distance ascending
                 select new StoreDetail
                 {
                     StoreGuid = item.Guid,
                     StoreName = item.SellerName,
                     Phone = item.StoreTel,
                     Address = CityManager.CityTownShopStringGet(item.StoreTownshipId.GetValueOrDefault()) + item.StoreAddress,
                     OpenTime = item.OpenTime,
                     CloseDate = item.CloseDate,
                     Remarks = item.SellerRemark,
                     Mrt = item.Mrt,
                     Car = item.Car,
                     Bus = item.Bus,
                     OtherVehicles = item.OtherVehicles,
                     WebUrl = item.WebUrl,
                     FbUrl = item.FacebookUrl,
                     BlogUrl = item.BlogUrl,
                     OtherUrl = item.OtherUrl,
                     CreditcardAvailable = item.CreditcardAvailable,
                     Coordinates = new Coordinate
                     {
                         Longitude = (item.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                    ? Convert.ToDouble(matchCoor.Groups["lo"].Value) : 0,
                         Latitude = (item.Coordinate != null && !string.IsNullOrWhiteSpace(item.Coordinate))
                                   ? Convert.ToDouble(matchCoor.Groups["la"].Value) : 0
                     },
                     DistanceDesc =
                         (q.Longitude.Equals(0) && q.Latitude.Equals(0))
                             ? string.Empty
                             : LocationFacade.GetDistanceDesc(distance)
                 }
                    ).ToList();

            #endregion

            #region 相關檔次

            if (firstStore != null)
            {
                var storeCol = _sp.VbsSellerGuidGet(firstStore.SellerUserId).FirstOrDefault();
                var dealList = new List<PponDealSynopsis>();
                if (storeCol != null)
                {
                    var deals = _pp.ViewPponDealGetBySellerGuid(storeCol.Guid);

                    var queryDeals = deals
                        .Where(x =>
                            x.BusinessHourOrderTimeS <= DateTime.Now
                            && DateTime.Now <= x.BusinessHourOrderTimeE
                            && !Helper.IsFlagSet(x.BusinessHourStatus, (int)BusinessHourStatus.ComboDealSub)
                        )
                        .Select(x => x.BusinessHourGuid).ToList();

                    dealList = PponDealApiManager.PponDealSynopsesGetListByBids(queryDeals, isNewPrototype);


                    if (isNewPrototype)
                    {
                        foreach (var temp in dealList)
                        {
                            temp.OriginalPrice = temp.OriPrice;
                        }
                    }
                }
                result.DealList = dealList;
            }

            #endregion

            return result;
        }

        public static MembershipCardOfferModel GetMembershipCardOffer(string userName, int groupId)
        {
            int userId = 0;
            if (!string.IsNullOrWhiteSpace(userName))
            {
                var mem = _mp.MemberGet(userName);
                userId = mem.IsLoaded ? mem.UniqueId : 0;
            }

            //all cards
            var membershipCards = _pcp.ViewMembershipCardGetListByDatetime(groupId, DateTime.Now)
                .Where(x => x.Status == (int)MembershipCardStatus.Open && x.Enabled && x.Level != (int)MembershipCardLevel.Level0);
            var userMembershipCard = _pcp.ViewUserMembershipCardGet(userId, groupId).FirstOrDefault();

            var result = new MembershipCardOfferModel();
            if (userMembershipCard != null)
            {
                //取得MembershipCard的級距
                var viewMembershipCard = membershipCards.FirstOrDefault(x => x.CardId == userMembershipCard.CardId);

                //個人熟客優惠
                if (userMembershipCard.IsLoaded && viewMembershipCard.IsLoaded)
                {
                    int orderNeeded = 0;
                    double amountNeeded = 0;

                    //只有一張普卡 or 無下一張升級卡別
                    if (membershipCards.Count() == 1 || membershipCards.Max(x => x.Level).Equals(viewMembershipCard.Level))
                    {
                        orderNeeded = 0;
                        amountNeeded = 0;
                    }
                    else //取下一級卡片
                    {
                        var nextLevelCard = membershipCards.First(x => x.Level == viewMembershipCard.Level + 1);
                        orderNeeded = nextLevelCard.OrderNeeded.HasValue ? nextLevelCard.OrderNeeded.Value - userMembershipCard.OrderCount : 0;
                        amountNeeded = nextLevelCard.AmountNeeded.HasValue ? (double)nextLevelCard.AmountNeeded.Value - (double)userMembershipCard.AmountTotal : 0;
                    }

                    result.OrderCount = userMembershipCard.OrderCount;
                    result.AmountTotal = (double)userMembershipCard.AmountTotal;
                    result.OrderNeeded = orderNeeded;
                    result.AmountNeeded = amountNeeded;
                    result.ConditionalLogic = userMembershipCard.ConditionalLogic;
                    result.CurrentLevel = userMembershipCard.Level;
                    result.CardValidDate =
                        viewMembershipCard.ContractValidDate.HasValue
                            ? ApiSystemManager.DateTimeToDateTimeString(viewMembershipCard.ContractValidDate.Value)
                            : ApiSystemManager.DateTimeToDateTimeString(DateTime.Now.AddYears(1));
                }
            }

            //Cards
            result.Cards =
                (from item in membershipCards
                 select new CardDetail
                 {
                     CardId = item.CardId,
                     Level = item.Level,
                     Enabled = item.Enabled,
                     PaymentPercent = item.PaymentPercent,
                     Instruction = item.Instruction,
                     OtherPremiums = item.Others,
                     AvailableDateType = item.AvailableDateType,
                     AvailableDateDesc = Helper.GetEnumDescription(((AvailableDateType)item.AvailableDateType)),
                     OrderNeeded = item.OrderNeeded ?? 0,
                     AmountNeeded = Convert.ToDouble((item.AmountNeeded ?? 0)),
                     ConditionalLogic = item.ConditionalLogic,
                     OpenTime = ApiSystemManager.DateTimeToDateTimeString(item.OpenTime),
                     CloseTime = ApiSystemManager.DateTimeToDateTimeString(item.CloseTime),
                     CombineUse = item.CombineUse
                 }).ToList();

            var level1Card = membershipCards.FirstOrDefault(x => x.Level == (int)MembershipCardLevel.Level1);
            if (level1Card != null && !string.IsNullOrWhiteSpace(level1Card.Instruction))
            {
                result.Remark.Add(level1Card.Instruction);
            }
            result.Remark.Add(remark0);
            result.Remark.Add(remark2);

            return result;
        }

        public static MembershipCardDepositModel GetMembershipCardDeposit(string userName, int groupId)
        {
            int userId = 0;
            if (!string.IsNullOrWhiteSpace(userName))
            {
                var mem = _mp.MemberGet(userName);
                userId = mem.IsLoaded ? mem.UniqueId : 0;
            }

            var result = new MembershipCardDepositModel();

            //寄杯服務
            var menuItemCollection = _pcp.ViewPcpSellerMenuItemCollectionGetByCardGroupIdInEffective(groupId)
                 .Select(s => new { Title = s.Title, ItemName = s.ItemName, });

            result.DepositActiveContent.Active = menuItemCollection.GroupBy(x => x.Title)
                .Select(s => new
                {
                    Result = s.Key + ":" + string.Join("/", s.Select(x => x.ItemName))
                }).Select(x => x.Result).ToList();

            var userDepositItems = PcpFacade.GetViewPcpUserDepositListByUserIdAndCardGroupId(userId, groupId);
            var userDepositLog =
                _pcp.ViewPcpUserDepositLogCollectionGetByUserIdAndCardGroupId(userId, groupId).OrderByDescending(x => x.CreateTime);

            foreach (var udl in userDepositItems)
            {
                DepositDetail detail = new DepositDetail();
                var transLogs = userDepositLog.Where(x => x.DepositId == udl.UserDepositId).ToList();

                detail.MenuId = (int)udl.MenuId;
                detail.MenuName = udl.Title;
                detail.TotalAmount = udl.TotalAmount;
                detail.RemainAmount = udl.RemainAmount;
                detail.CreatedDate = ApiSystemManager.DateTimeToDateTimeString(
                    transLogs.FirstOrDefault(x => x.DepositType == (int)PcpUserDepositLogType.Add).CreateTime);

                if (transLogs.Any())
                {
                    detail.IsNew = transLogs
                        .FirstOrDefault(x => x.CreateTime > DateTime.Now.AddDays(-7)) != null;
                }

                //新增移到最下面
                var firstTrans = transLogs.First(x => x.DepositType == (int)PcpUserDepositLogType.Add);
                transLogs.Remove(firstTrans);
                transLogs.Add(firstTrans);

                detail.TransHistory = transLogs.Select(x => new TransData()
                {
                    Type = x.DepositType,
                    TransDate = ApiSystemManager.DateTimeToDateTimeString(x.CreateTime),
                    TransAmount = x.Amount,
                    TransItem = x.ItemName,
                    TransDesc = x.Description,
                    TransStoreName = ""
                }).ToList();



                result.DepositList.Add(detail);
            }


            result.DepositActiveContent.Remark.Add(remark1);
            result.DepositActiveContent.Remark.Add(remark2);
            result.TotalDeposit = PcpFacade.GetViewPcpUserDepositListByUserIdAndCardGroupId(userId, groupId)
                                           .Sum(x => x.RemainAmount);
            return result;
        }


        public static MembershipCardPointModel GetMembershipCardPoint(string userName, int groupId)
        {
            int userId = 0;
            if (!string.IsNullOrWhiteSpace(userName))
            {
                var mem = _mp.MemberGet(userName);
                userId = mem.IsLoaded ? mem.UniqueId : 0;
            }

            var result = new MembershipCardPointModel();
            var userPointLog = _pcp.ViewPcpUserPointCollectionGet(userId, groupId).OrderByDescending(x => x.UserPointId);

            //集點內容
            var collectRule = _pcp.PcpPointCollectRuleGetListByCardGroupId(groupId).FirstOrDefault();
            if (collectRule != null)
            {
                result.PointActiveContent.CollectRule = "到店消費金額" + collectRule.CollectValue + "，可獲得 1 點";
            }

            var exchangeRule = _pcp.PcpPointExchangeRuleGetListByCardGroupId(groupId);
            if (exchangeRule.Any())
            {
                foreach (var e in exchangeRule)
                {
                    result.PointActiveContent.ExchangeRule.Add("累積" + e.Point + "點兌換" + e.ItemName);
                }
            }

            if (userPointLog.Any())
            {
                result.IsNew = userPointLog.FirstOrDefault(x => x.CreateTime > DateTime.Now.AddDays(-7)) != null;
            }

            //集點歷程
            foreach (var userPoint in userPointLog)
            {
                string transDesc = string.Empty;
                if (userPoint.SetType == (int)PcpUserPointSetType.Exchange)
                {
                    var exRule = exchangeRule.First(x => x.Id == userPoint.PointRuleId);
                    transDesc = string.Format("兌換 {0}x{1}", exRule.ItemName, userPoint.Count);
                }
                else if (userPoint.SetType == (int)PcpUserPointSetType.Correct)
                {
                    transDesc = userPoint.ItemName;
                }

                result.TransHistory.Add(new TransData()
                {
                    Type = userPoint.SetType,
                    TransDate = ApiSystemManager.DateTimeToDateTimeString(userPoint.CreateTime),
                    TransAmount = userPoint.TotalPoint,
                    TransItem = userPoint.ItemName,
                    TransDesc = transDesc,
                    TransStoreName = userPoint.StoreName,
                    TransPrice = Decimal.ToInt32(userPoint.Price ?? 0)
                });
            }

            var pointRemarks = _pcp.PcpPointRemarkGetListByCardGroupId(groupId);
            foreach (var remark in pointRemarks)
            {
                result.PointActiveContent.Remark.Add(remark.Remark);
            }

            result.PointActiveContent.Remark.Add(remark1);
            result.PointActiveContent.Remark.Add(remark2);
            result.PointActiveContent.Remark.Add(remark3);
            result.TotalPoint = userPointLog.Sum(x => x.RemainPoint);

            return result;
        }

    }
}

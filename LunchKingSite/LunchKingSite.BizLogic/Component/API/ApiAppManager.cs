﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Component.API
{
    public class ApiAppManager
    {
        private static readonly IPCPProvider pcp;
        private static readonly IMemberProvider mp;
        private static readonly ISellerProvider sp;
        private static readonly IPponProvider pp;
        private static readonly ISystemProvider sysp;
        private static readonly ISysConfProvider config;

        static ApiAppManager()
        {
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            sysp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public static List<HotSearchKeyModel> GetSearchKeyList(int useMode)
        {
            var result = new List<HotSearchKeyModel>();

            var data = pp.PromoSearchKeyGetColl((PromoSearchKeyUseMode)useMode);

            foreach (var item in data)
            {
                result.Add(new HotSearchKeyModel
                {
                    KeyId = item.Id,
                    KeyWord = item.KeyWord
                });
            }

            return result;
        }

        public static void SetSearchKeyRecord(int key,string user,string memo = "")
        {
            int userId;
            if (!int.TryParse(user, out userId))
            {
                var m = MemberFacade.GetMember(user);
                if (m.IsLoaded) { userId = m.UniqueId; }
            }
            

            pp.PromoSearchKeyRecordSet(new PromoSearchKeyRecord
            {
                SearchKeyId = key,
                MemberUserId = userId,
                CreateTime = DateTime.Now,
                Memo = memo,
            });
        }

        public static List<string> GetKWPromptList(string word)
        {
            var promptLis = pp.KeywordGetPromptList(word)
                .Where(x => x.Word.Length > 1).OrderBy(x => x.Word.Length)
                .Select(x => x.Word).Take(5).ToList();

            return promptLis;
        }
    }
}

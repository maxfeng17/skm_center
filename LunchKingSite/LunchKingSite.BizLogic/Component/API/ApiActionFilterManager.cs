﻿using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace LunchKingSite.BizLogic.Component.API
{
    public class ApiActionFilterManager
    {
        private static ISysConfProvider config;
        private static ILog Logger = LogManager.GetLogger("ApiActionFilterManager");

         static ApiActionFilterManager()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        public static double DecodeBatteryCapacityOfBeacon(int minor)
        {
            //int major = 33190;
            int _minor = minor;

            int oneInDigit = default (int);
            int decimalInDigit = default(int);

            //取得個位數
            oneInDigit = _minor % 4096 % 256 / 10;
            //取得小數位數
            decimalInDigit = _minor % 4096 % 256 % 10;

            //combinBatteryCapacity
            double combinBatteryCapacity = default(double);

            if (double.TryParse(oneInDigit + "." + decimalInDigit, out combinBatteryCapacity))
            {
            }
            else
            {
                Logger.Error(string.Format("Beacon電量解析錯誤，minor:{0}", _minor));
            }

            return combinBatteryCapacity;
        }

        public static string DecodeMacAddressOfBeacon( int major, int minor )
        {
            //macAddress rule1 => a:ab:bc
            string bClassOneDigitOfMacAddress = string.Empty;
            string cClassTenDigitOfMacAddress = string.Empty;
            string cClassOneDigitOfMacAddress = string.Empty;
            string dClassTenDigitOfMacAddress = string.Empty;
            string dClassOneDigitOfMacAddress = string.Empty;

            int _major = major;
            int _minor = minor;

            bClassOneDigitOfMacAddress = Convert.ToString(_major / 4096, 16);
            cClassTenDigitOfMacAddress = Convert.ToString((_major % 4096) / 256, 16);
            cClassOneDigitOfMacAddress = Convert.ToString((_major % 4096) % 256 / 16, 16);
            dClassTenDigitOfMacAddress = Convert.ToString((_major % 4096) % 256 % 16, 16);
            dClassOneDigitOfMacAddress = Convert.ToString(_minor / 4096, 16);

            string partialBeaconAddress = string.Format("{0}:{1}{2}:{3}{4}"
                , bClassOneDigitOfMacAddress
                , cClassTenDigitOfMacAddress
                , cClassOneDigitOfMacAddress
                , dClassTenDigitOfMacAddress
                , dClassOneDigitOfMacAddress
                );

            return partialBeaconAddress;
        }

        public static string DecodeMacAddressOfBeacon2(int major, int minor)
        {
            //macAddress rule2 => aa:bb:cc
            string major0X = string.Empty;
            string minor0X = string.Empty;

            int _major = major;
            int _minor = minor;

            major0X = Convert.ToString(major, 16);
            minor0X = Convert.ToString(minor, 16);

            //不足四位數補0, 因為2byte顯示關係
            if (minor0X.Length < 4)
            {
                minor0X = minor0X.PadLeft(4, '0');
            }

            string partialBeaconAddress = string.Empty;

            int doubleNumberIndex = 0;
            foreach(char c in major0X)
            {
                doubleNumberIndex++;
                partialBeaconAddress += c;
                if (doubleNumberIndex % 2 == 0)
                {
                    partialBeaconAddress += ":";
                }
            }
            doubleNumberIndex = 0;

            foreach (char c in minor0X)
            {
                doubleNumberIndex++;
                partialBeaconAddress += c;
                if (partialBeaconAddress.Length == 8)
                {
                    break;
                }

                if (doubleNumberIndex % 2 == 0)
                {
                    partialBeaconAddress += ":";
                }
            }

            return partialBeaconAddress;
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Microsoft.SqlServer.Types;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.BizLogic.Component.API
{
    /// <summary>
    /// 供API呼叫，相關區域限定類型的檔次或優惠券
    /// </summary>
    public class ApiLocationDealManager
    {
        private static readonly IPponProvider PponProvider;
        private static readonly ISellerProvider SellerProvider;
        private static readonly IItemProvider ItemProvider;
        private static readonly IOrderProvider OrderProvider;
        private static readonly IMemberProvider MemberProvider;
        private static readonly ISysConfProvider config;
        private static readonly log4net.ILog log;


        private static Member _IDEASMember;
        public static Member IDEASMember
        {
            get
            {
                if (_IDEASMember == null)
                {
                    lock (typeof(ApiLocationDealManager))
                    {
                        //再檢查一次
                        if (_IDEASMember == null)
                        {
                            _IDEASMember = MemberProvider.MemberGet(config.IDEASUserEmail);
                        }
                    }

                }
                return _IDEASMember;
            }
        }

        static ApiLocationDealManager()
        {
            PponProvider = ProviderFactory.Instance().GetProvider<IPponProvider>();
            SellerProvider = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            ItemProvider = ProviderFactory.Instance().GetProvider<IItemProvider>();
            OrderProvider = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            MemberProvider = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            config = ProviderFactory.Instance().GetConfig();
            log = log4net.LogManager.GetLogger(typeof(ApiPromoEventManager));
        }

        /// <summary>
        /// 回傳所有提供給 雙北APP之檔次與優惠券資料
        /// </summary>
        /// <returns></returns>
        public static ApiIDEASEventBaseData ApiIDEASEventBaseDataGet(DateTime startDate, DateTime endDate)
        {
            //依次撈取的資料區間
            DateTime itemStartDate = startDate; //DateTime.Now.Date;
            DateTime itemEndDate = endDate;// itemStartDate.AddDays(2);

            ApiIDEASEventBaseData rtnData = new ApiIDEASEventBaseData();

            List<IDEASStationData> stationDatas = IDEASStationDataGetList();
            //整理所有要查詢的EventUrl
            List<string> eventUrls = new List<string>();
            foreach (var ideasStationData in stationDatas)
            {
                if (!eventUrls.Contains(ideasStationData.CouponEventUrl))
                {
                    eventUrls.Add(ideasStationData.CouponEventUrl);
                }
                if (!eventUrls.Contains(ideasStationData.DeliveryEventUrl))
                {
                    eventUrls.Add(ideasStationData.DeliveryEventUrl);
                }
                if (!eventUrls.Contains(ideasStationData.SpecialEventUrl))
                {
                    eventUrls.Add(ideasStationData.SpecialEventUrl);
                }
                if (!eventUrls.Contains(ideasStationData.VourcherEventUrl))
                {
                    eventUrls.Add(ideasStationData.VourcherEventUrl);
                }
            }
            //撈取展示時間與查詢時間有重疊的所有檔次
            EventPromoItemCollection items = PponProvider.EventPromoItemGetListByMainUrls(eventUrls, itemStartDate, itemEndDate);
            foreach (var item in items)
            {
                switch ((EventPromoItemType)item.ItemType)
                {
                    case EventPromoItemType.Ppon:
                        if (rtnData.products.Count(x => x.dealId == item.ItemId) != 0)
                        {
                            continue;
                        }
                        ApiIDEASDeal deal = ApiIDEASDealGetByDealId(item.ItemId);
                        if (deal != null)
                        {
                            rtnData.products.Add(deal);
                            //多檔次主檔，須查詢子檔並新增到列表中
                            if (deal.multiDealType == ApiMultiDealType.MainDeal)
                            {
                                foreach (ApiIDEASSubDeal subDeal in deal.subDealData)
                                {
                                    if (rtnData.products.Count(x => x.dealId == subDeal.dealId) != 0)
                                    {
                                        continue;
                                    }
                                    ApiIDEASDeal subdeal = ApiIDEASDealGetByDealId(subDeal.dealId);
                                    if (subdeal != null)
                                    {
                                        //子檔次的圖片與母檔次相同
                                        subdeal.photoUrls = deal.photoUrls;
                                        rtnData.products.Add(subdeal);
                                    }
                                }
                            }
                        }
                        break;
                    case EventPromoItemType.Vourcher:
                        if (rtnData.coupons.Count(x => x.eventId == item.ItemId) == 0)
                        {
                            ApiIDEASVourcher vourcher = ApiIDEASVourcherGetByEventId(item.ItemId);
                            if (vourcher != null)
                            {
                                rtnData.coupons.Add(vourcher);
                            }
                        }
                        break;
                    default:
                        //其他不管
                        break;
                }
            }

            return rtnData;
        }
        /// <summary>
        /// 回傳目前還在銷售中的所有雙北APP檔次與優惠券的剩餘數量
        /// </summary>
        /// <returns></returns>
        public static ApiIDEASSurplusQuantityData ApiIDEASSurplusQuantityDataGet()
        {
            ApiIDEASSurplusQuantityData rtnData = new ApiIDEASSurplusQuantityData();

            List<IDEASStationData> stationDatas = IDEASStationDataGetList();
            //整理所有要查詢的EventUrl
            List<string> eventUrls = new List<string>();
            foreach (var ideasStationData in stationDatas)
            {
                if (!eventUrls.Contains(ideasStationData.CouponEventUrl))
                {
                    eventUrls.Add(ideasStationData.CouponEventUrl);
                }
                if (!eventUrls.Contains(ideasStationData.DeliveryEventUrl))
                {
                    eventUrls.Add(ideasStationData.DeliveryEventUrl);
                }
                if (!eventUrls.Contains(ideasStationData.SpecialEventUrl))
                {
                    eventUrls.Add(ideasStationData.SpecialEventUrl);
                }
                if (!eventUrls.Contains(ideasStationData.VourcherEventUrl))
                {
                    eventUrls.Add(ideasStationData.VourcherEventUrl);
                }
            }

            DateTime date = DateTime.Now;
            ViewEventPromoCollection promoPpon = PponProvider.ViewEventPromoGetList(eventUrls, true);
            foreach (var ppon in promoPpon)
            {
                //還未結檔才取得數量
                if (ppon.BusinessHourOrderTimeS <= date && ppon.BusinessHourOrderTimeE > date)
                {
                    if (rtnData.products.Count(x => x.dealId == ppon.ItemId) != 0)
                    {
                        continue;
                    }

                    ApiIDEASDealSurplusQuantity product = ApiIDEASDealSurplusQuantityGetByDealId(ppon.ItemId);
                    if (product != null)
                    {
                        rtnData.products.Add(product);
                    }
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(ppon.ItemId);
                    if (deal == null)
                    {
                        continue;
                    }

                    //判斷是否為多檔次
                    if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                    {
                        //查詢子檔次資料
                        ViewComboDealCollection comboDeals = PponProvider.GetViewComboDealByBid(deal.BusinessHourGuid,
                                                                                                false);
                        foreach (var comboDeal in comboDeals)
                        {
                            if (rtnData.products.Count(x => x.dealId == comboDeal.UniqueId) != 0)
                            {
                                continue;
                            }

                            ApiIDEASDealSurplusQuantity subDeal = ApiIDEASDealSurplusQuantityGetByDealId(comboDeal.UniqueId);
                            if (subDeal != null)
                            {
                                rtnData.products.Add(subDeal);
                            }
                        }
                    }
                }
            }

            ViewEventPromoVourcherCollection promoVourcher = PponProvider.ViewEventPromoVourcherGetList(eventUrls, true);
            foreach (var vourcher in promoVourcher)
            {
                //優惠券的有效期限並未設定到時分秒，紀錄的endData整天都還算是有消期限，判斷時要注意。
                if (vourcher.VourcherStartDate <= date && vourcher.VourcherEndDate >= date.Date)
                {
                    if (rtnData.coupons.Count(x => x.eventId == vourcher.VourcherEventId) != 0)
                    {
                        continue;
                    }
                    ApiIDEASVourcherSurplusQuantity coupon = ApiIDEASVourcherSurplusQuantityGetByEventId(vourcher.VourcherEventId);
                    if (coupon != null)
                    {
                        rtnData.coupons.Add(coupon);
                    }
                }
            }
            return rtnData;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static List<ApiIDEASEventData> ApiIDEASEventDataGetList(DateTime date)
        {
            List<ApiIDEASEventData> rtnList = new List<ApiIDEASEventData>();

            List<IDEASStationData> stationDatas = IDEASStationDataGetList();

            foreach (var ideasStationData in stationDatas)
            {
                List<string> specialBroadcastTimes = new List<string>();
                foreach (var broadcast in ideasStationData.SpecialDealBroadcastTime)
                {
                    DateTime sTime = DateTime.ParseExact((date.ToString(date.ToString(broadcast.StartTimeFormat))), "yyyyMMddHHmmss", null);
                    DateTime eTime = DateTime.ParseExact((date.ToString(date.ToString(broadcast.EndTimeFormat))), "yyyyMMddHHmmss", null);

                    string specialBroadcastTime = string.Format("{0}~{1}", ApiSystemManager.DateTimeToDateTimeString(sTime), ApiSystemManager.DateTimeToDateTimeString(eTime));
                    specialBroadcastTimes.Add(specialBroadcastTime);
                }
                ApiIDEASEventData eventData = new ApiIDEASEventData();
                eventData.id = ideasStationData.Id;
                eventData.title = ideasStationData.Title;
                eventData.broadcastDate = ApiSystemManager.DateTimeToDateString(date);
                eventData.specialDealBroadcastTime = specialBroadcastTimes;
                eventData.couponDealBroadcastMinutes = ideasStationData.CouponDealBroadcastMinutes;
                eventData.deliveryDealBroadcastMinutes = ideasStationData.DeliveryDealBroadcastMinutes;
                eventData.vourcherStoreBroadcastMinutes = ideasStationData.VourcherStoreBroadcastMinutes;


                ViewEventPromoCollection specialItems = PromotionFacade.GetViewEventPromoListByUrlAndItemDate(ideasStationData.SpecialEventUrl, date, true);
                foreach (var item in specialItems)
                {
                    if (item.BusinessHourOrderTimeS <= date && item.BusinessHourOrderTimeE > date)
                    {
                        eventData.specialDeals.Add(new ApiIDEASDealBroadcastData() { dealId = item.ItemId });
                    }
                }

                ViewEventPromoCollection couponDeals = PromotionFacade.GetViewEventPromoListByUrlAndItemDate(ideasStationData.CouponEventUrl, date, true);
                foreach (var item in couponDeals)
                {
                    if (item.BusinessHourOrderTimeS <= date && item.BusinessHourOrderTimeE > date)
                    {
                        eventData.couponDeals.Add(new ApiIDEASDealBroadcastData() { dealId = item.ItemId });
                    }
                }

                ViewEventPromoCollection deliveryDeals = PromotionFacade.GetViewEventPromoListByUrlAndItemDate(ideasStationData.DeliveryEventUrl, date, true);
                foreach (var item in deliveryDeals)
                {
                    if (item.BusinessHourOrderTimeS <= date && item.BusinessHourOrderTimeE > date)
                    {
                        eventData.deliveryDeals.Add(new ApiIDEASDealBroadcastData() { dealId = item.ItemId });
                    }
                }

                ViewEventPromoVourcherCollection vourcherStores = PromotionFacade.ViewEventPromoVourcherGetListByUrlAndItemDate(ideasStationData.VourcherEventUrl, date, true);
                foreach (var item in vourcherStores)
                {
                    //優惠券的有效期限並未設定到時分秒，紀錄的endData整天都還算是有消期限，判斷時要注意。
                    if (item.VourcherStartDate <= date && item.VourcherEndDate >= date.Date)
                    {
                        eventData.vourcherStores.Add(new ApiIDEASVourcherBroadcastData()
                        {
                            eventId = item.VourcherEventId,
                        });
                    }
                }

                rtnList.Add(eventData);
            }

            return rtnList;
        }

        #region 訂單狀況異動至IDEAS端相關

        public static bool UpdateIDEASOrder()
        {
            List<ApiIDEASOrderProperty> updateDatas = new List<ApiIDEASOrderProperty>();
            List<Guid> orderGuidList = new List<Guid>();
            //查詢需要回傳資料給廠商的訂單
            CompanyUserOrderCollection companyUserOrderList =
                OrderProvider.CompanyUserOrderGetList(CompanyUserOrder.Columns.WaitUpdate + " = " + true);
            //逐筆整理資料預備回傳
            foreach (var companyOrder in companyUserOrderList)
            {
                //紀錄異動的訂單編號
                orderGuidList.Add(companyOrder.OrderGuid);
                //查詢訂單
                Order order = OrderProvider.OrderGet(companyOrder.OrderGuid);

                //先處理憑證狀態
                CashTrustLogCollection ctlc = MemberProvider.CashTrustLogGetListByOrderGuid(order.Guid,
                                                                                            OrderClassification.LkSite);
                //剔除運費
                List<CashTrustLog> cashTrustLogs =
                    ctlc.Where(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).ToList();
                //以第一筆cashTrustLog查詢此訂單需要的所有資料
                if (cashTrustLogs.Count <= 0)
                {
                    //沒有cashTrustLog資料，訂單資料不全，跳過這一筆，處理下一筆訂單
                    continue;
                }

                CashTrustLog firstLog = cashTrustLogs.First();
                //異常資料，跳過這一筆，處理下一筆訂單
                if (firstLog.BusinessHourGuid == null)
                {
                    continue;
                }
                Guid bid = firstLog.BusinessHourGuid.Value;
                DealProperty dealProperty = PponProvider.DealPropertyGet(firstLog.BusinessHourGuid.Value);
                BusinessHour businessHour = PponProvider.BusinessHourGet(bid);

                OrderReturnList orToRefund = OrderProvider.OrderReturnListGetbyType(order.Guid,
                                                                                    (int)OrderReturnType.Refund);

                //判斷此訂單是否退貨處理中
                bool isInRefund = false;
                if (orToRefund != null && orToRefund.Id > 0)
                {
                    //退貨處理中
                    if ((OrderReturnStatus)orToRefund.Status == OrderReturnStatus.Processing)
                    {
                        isInRefund = true;
                    }
                }
                //判斷時間上是否檔次已超過使用期限
                bool isExpired = PponFacade.IsPastFinalExpireDate(companyOrder.OrderGuid);
                //先查詢發票預備後續處理
                EinvoiceMainCollection mainCollection =
                    OrderProvider.EinvoiceMainGetListByOrderGuid(companyOrder.OrderGuid);

                if ((!dealProperty.IsLoaded) || dealProperty.DeliveryType == null)
                {
                    //沒有dealProperty或無法區別宅配或憑證，錯誤資料，跳過這一筆，處理下一筆訂單
                    continue;
                }
                //異動時間的字串
                string modifyTimeString = ApiSystemManager.DateTimeToDateTimeString(DateTime.Now);
                //預設為未付款
                ApiIDEASOrderStatus orderStatus = ApiIDEASOrderStatus.Unpaid;

                ApiIDEASOrderProperty ideasOrder = new ApiIDEASOrderProperty();
                ideasOrder.orderId = order.OrderId;
                ideasOrder.couponIds = new List<ApiIDEASCouponProperty>();
                //判斷是否為宅配商品
                if ((DeliveryType)dealProperty.DeliveryType == DeliveryType.ToHouse)
                {
                    //宅配檔次沒有憑證資料，建立單一商品資料

                    #region 判斷發票狀態

                    //判斷發票狀態
                    //先預設為未開立
                    ideasOrder.invoiceOfferType = ApiInvoiceOfferType.None;
                    if (mainCollection.Count > 0)
                    {
                        EinvoiceMain einvoice = mainCollection.First();
                        if (!string.IsNullOrWhiteSpace(einvoice.InvoiceNumber))
                        {
                            ideasOrder.goodsInvoiceNumber = einvoice.InvoiceNumber;
                            //有發票編號，判斷有無申請紙本
                            if (einvoice.InvoiceRequestTime != null)
                            {
                                ideasOrder.invoiceOfferType = ApiInvoiceOfferType.ApplyPaper;
                            }
                            else
                            {
                                ideasOrder.invoiceOfferType = ApiInvoiceOfferType.Create;
                            }
                        }
                    }

                    #endregion 判斷發票狀態

                    if (cashTrustLogs.Count == 0)
                    {
                        log.WarnFormat("methodName:{0} 宅配檔次至少須有一筆cashTrustLog紀錄orderGuid:{1}",
                                       "ApiLocationDealManager.UpdateIDEASOrder", companyOrder.OrderGuid);
                        continue;
                    }

                    #region 依據cashTrustLog的status判斷orderStatus的值

                    //預設為宅配
                    orderStatus = ApiIDEASOrderStatus.ToHouse;
                    //沒有退貨資料
                    if (orToRefund != null && orToRefund.IsLoaded && (OrderReturnStatus)orToRefund.Status == OrderReturnStatus.Processing)
                    {
                        //退貨處理中
                        orderStatus = ApiIDEASOrderStatus.InRefund;
                    }
                    else if (orToRefund != null && orToRefund.IsLoaded && (OrderReturnStatus)orToRefund.Status == OrderReturnStatus.Success)
                    {
                        //退貨完成。
                        orderStatus = ApiIDEASOrderStatus.RefundComplete;
                    }
                    else
                    {
                        //未退貨，判斷商品是否已出貨或已過鑑賞期
                        if (PponOrderManager.CheckIsOverTrialPeriod(companyOrder.OrderGuid))
                        {
                            //已過鑑賞期
                            orderStatus = ApiIDEASOrderStatus.GoodsExpired;
                        }
                        else
                        {
                            //檢查是否已出貨
                            if (PponOrderManager.CheckIsShipped(companyOrder.OrderGuid, OrderClassification.LkSite))
                            {
                                //已出貨
                                orderStatus = ApiIDEASOrderStatus.GoodsShipped;
                            }
                        }
                    }

                    #endregion
                }
                else
                {
                    //憑證商品
                    foreach (var cashTrustLog in cashTrustLogs)
                    {
                        if (cashTrustLog.CouponId == null)
                        {
                            //cashTrustLog無憑證資料，資料異常，跳過這一筆。
                            log.WarnFormat("methodName:{0} 憑證檔次的cashTrustLog必須要有紀錄憑證編號 cashTrustLog trustId:{1}",
                                           "ApiLocationDealManager.UpdateIDEASOrder", cashTrustLog.TrustId);
                            continue;
                        }
                        ApiIDEASCouponProperty ideasCoupon = new ApiIDEASCouponProperty();
                        ideasCoupon.couponId = cashTrustLog.CouponId.Value;
                        ideasCoupon.status = GetApiIDEASCouponStatus(cashTrustLog, businessHour, isInRefund, isExpired);
                        ideasCoupon.modifyTime = modifyTimeString;

                        List<EinvoiceMain> invoiceList =
                            mainCollection.Where(x => x.CouponId == cashTrustLog.CouponId).ToList();

                        #region 判斷發票狀態

                        //預設狀態為未開立發票
                        ideasCoupon.invoiceOfferType = ApiInvoiceOfferType.None;
                        if (invoiceList.Count > 0)
                        {
                            EinvoiceMain invoice = invoiceList.First();
                            //是否開立發票
                            if (!string.IsNullOrWhiteSpace(invoice.InvoiceNumber))
                            {
                                ideasCoupon.invoiceNumber = invoice.InvoiceNumber;
                                //有發票編號，檢查是否開立紙本
                                if (invoice.InvoiceRequestTime != null)
                                {
                                    //有申請時間
                                    ideasCoupon.invoiceOfferType = ApiInvoiceOfferType.ApplyPaper;
                                }
                                else
                                {
                                    //未申請紙本，回傳已開立發票
                                    ideasCoupon.invoiceOfferType = ApiInvoiceOfferType.Create;
                                }
                            }
                        }

                        #endregion 判斷發票狀態

                        ideasOrder.couponIds.Add(ideasCoupon);
                    }

                    #region 判斷憑證檔次訂單狀態

                    if (ideasOrder.couponIds.Any(x => x.status == ApiIDEASCouponStatus.NotUsed))
                    {
                        //有任何未使用憑證
                        orderStatus = ApiIDEASOrderStatus.NotUsed;
                    }
                    else if (ideasOrder.couponIds.Any(x => x.status == ApiIDEASCouponStatus.InRefund))
                    {
                        //已任一憑證申請退貨
                        orderStatus = ApiIDEASOrderStatus.InRefund;
                    }
                    else if (ideasOrder.couponIds.Any(x => x.status == ApiIDEASCouponStatus.Expired))
                    {
                        //任一憑證為已過期，表示有憑證未使用
                        orderStatus = ApiIDEASOrderStatus.NotUsed;
                    }
                    else if (ideasOrder.couponIds.Any(x => x.status == ApiIDEASCouponStatus.Used))
                    {
                        //排除之前所有狀況，有憑證狀態為已使用
                        orderStatus = ApiIDEASOrderStatus.AllUsed;
                    }
                    else if (ideasOrder.couponIds.Any(x => x.status == ApiIDEASCouponStatus.RefundComplete))
                    {
                        //有退貨完成
                        orderStatus = ApiIDEASOrderStatus.RefundComplete;
                    }

                    #endregion 判斷憑證檔次訂單狀態
                }
                ideasOrder.orderStatus = orderStatus;

                updateDatas.Add(ideasOrder);

                companyOrder.WaitUpdate = false;
            }

            if (updateDatas.Count > 0)
            {
                if (UpdateIDEASOrder(updateDatas))
                {
                    OrderProvider.CompanyUserOrderUpdateWaitUpdate(orderGuidList, false);
                    return true;
                }
            }
            return true;
        }

        public static bool UpdateIDEASOrder(List<ApiIDEASOrderProperty> orderProperties)
        {
            if (orderProperties == null || orderProperties.Count <= 0)
            {
                return false;
            }

            return IDEASServiceCall("voucher", "update_status", new { orders = orderProperties });
        }

        private static ApiIDEASCouponStatus GetApiIDEASCouponStatus(CashTrustLog cashTrustLog, BusinessHour businessHour, bool isInRefund, bool isExpired)
        {
            if (cashTrustLog.BusinessHourGuid != businessHour.Guid)
            {
                //簡單的檢查一下，避免有人不小心丟錯了。
                log.WarnFormat("methodName:{0} BID不相符 trustId:{1}", "ApiLocationDealManager.GetApiIDEASCouponStatus", cashTrustLog.TrustId);
                return ApiIDEASCouponStatus.Used;
            }
            //預設為已使用。
            ApiIDEASCouponStatus rtn = ApiIDEASCouponStatus.Used;
            switch ((TrustStatus)cashTrustLog.Status)
            {
                case TrustStatus.Initial:
                case TrustStatus.Trusted:
                    //信託準備中與已信託，皆為未使用
                    //未使用，需進一步檢查是否申請退貨或超過使用期限
                    //先檢查是否正在申請退貨。
                    if (isInRefund)
                    {
                        rtn = ApiIDEASCouponStatus.InRefund;
                    }
                    //檢查是否已過期
                    else if (isExpired)
                    {

                        rtn = ApiIDEASCouponStatus.Expired;
                    }
                    else
                    {
                        rtn = ApiIDEASCouponStatus.NotUsed;
                    }
                    break;
                case TrustStatus.Verified:
                    rtn = ApiIDEASCouponStatus.Used;
                    break;
                case TrustStatus.Returned:
                    rtn = ApiIDEASCouponStatus.RefundComplete;
                    break;
                case TrustStatus.Refunded:
                    rtn = ApiIDEASCouponStatus.RefundComplete;
                    break;
                case TrustStatus.ATM:
                    //ATM時訂單未成立，資策會預設不開放ATM功能，屬於異常狀態
                    rtn = ApiIDEASCouponStatus.Used;
                    log.WarnFormat("methodName:{0} 資策會訂單不可以ATM付款 trustId:{1}", "ApiLocationDealManager.GetApiIDEASCouponStatus", cashTrustLog.TrustId);
                    break;
            }

            return rtn;
        }
        /// <summary>
        /// 依據資策會webService規範，將呼叫的args包到符合規範的物件中。
        /// </summary>
        /// <param name="args">出叫api需傳入的參數</param>
        /// <returns></returns>
        private static object GetIDEASRequestObject(object args)
        {
            return new { service_token = config.IDEASServiceToken, args = args };
        }
        #endregion 訂單狀況異動至IDEAS端相關


        #region 緊急異動檔次資料
        public static bool UpdateIDEASDeal(int dealId)
        {
            ApiIDEASDeal deal = ApiIDEASDealGetByDealId(dealId);
            if (deal != null)
            {
                return UpdateIDEASDeal(deal);
            }
            return false;
        }

        public static bool UpdateIDEASDeal(ApiIDEASDeal dealData)
        {
            if (dealData == null)
            {
                return false;
            }

            return IDEASServiceCall("product", "update", new { product = dealData });
        }
        #endregion 緊急異動檔次資料

        #region 緊急異動優惠券資料
        public static bool UpdateIDEASVourcher(int eventId)
        {
            ApiIDEASVourcher vourcher = ApiIDEASVourcherGetByEventId(eventId);
            if (vourcher != null)
            {
                return UpdateIDEASVourcher(vourcher);
            }
            return false;
        }

        public static bool UpdateIDEASVourcher(ApiIDEASVourcher vourcher)
        {
            if (vourcher == null)
            {
                return false;
            }

            return IDEASServiceCall("coupon", "update", new { coupon = vourcher });
        }
        #endregion 緊急異動優惠券資料


        public static bool IDEASServiceCall(string resource, string method, object dataObject)
        {
            if (dataObject == null)
            {
                return false;
            }
            //SSL一律忽略憑證驗證
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            JsonSerializer json = new JsonSerializer();
            var requestData = GetIDEASRequestObject(dataObject);
            string data = json.Serialize(requestData);
            string url = string.Format("{0}/{1}/{2}/", config.IDEASServiceUrl, resource, method);
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.ContentType = "application/json";
            request.Method = "POST";
            try
            {
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)request.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        JObject resultDictionary = json.Deserialize<JObject>(result);
                        //如果異動失敗
                        if ((int)resultDictionary["result_content"]["resStatus"] == 0)
                        {
                            log.WarnFormat("ApiLocationDealManager.IDEASServiceCall resource:{0},method:{1} msg:{2}",
                                resource, method, resultDictionary["result_content"]["msg"]);
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("ApiLocationDealManager.IDEASServiceCall 呼叫失敗 resource:{0},method:{1} ex:{2}", resource, method, ex);
                return false;
            }
            return true;
        }



        /// <summary>
        /// 資策會的索取紙本發票功能
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="invoiceNumber"></param>
        /// <param name="writeName"></param>
        /// <param name="writeAddress"></param>
        /// <returns></returns>
        public static ApiIDEASInvoiceRequestReply EinvoiceRequestPaper(string orderId, string invoiceNumber,
                                                                       string writeName, string writeAddress)
        {
            ApiIDEASInvoiceRequestReply rtn = new ApiIDEASInvoiceRequestReply();
            //檢查訂單是否為資策會的訂單
            Order order = OrderProvider.OrderGet(Order.Columns.OrderId, orderId);
            //查無訂單
            if (!order.IsLoaded)
            {
                rtn.Status = ApiIDEASInvoiceRequestStatus.NoOrderOrInvoice;
                rtn.Message = "查無訂單資料";
                return rtn;
            }
            //資策會帳號資料錯誤或訂單不為資策會訂單
            if (IDEASMember == null || IDEASMember.UniqueId != order.UserId)
            {
                rtn.Status = ApiIDEASInvoiceRequestStatus.DataMismatch;
                rtn.Message = "訂單或發票編號不正確。";
                return rtn;
            }
            //檢查發票
            invoiceNumber = invoiceNumber.ToUpper(); //先轉成大寫
            EinvoiceMain invoice = OrderProvider.EinvoiceMainGet(invoiceNumber);
            if (!invoice.IsLoaded)
            {
                //查無發票
                rtn.Status = ApiIDEASInvoiceRequestStatus.NoOrderOrInvoice;
                rtn.Message = "查無發票。";
                return rtn;
            }
            if (!invoice.OrderId.Equals(orderId.ToUpper()))
            {
                rtn.Status = ApiIDEASInvoiceRequestStatus.DataMismatch;
                rtn.Message = "訂單或發票編號不正確。";
                return rtn;
            }
            if (invoice.InvoiceRequestTime != null)
            {
                rtn.Status = ApiIDEASInvoiceRequestStatus.AlreadyRequest;
                rtn.Message = "已經申請過了";
                return rtn;
            }


            //申請紙本發票
            OrderProvider.EinvoiceRequestPaper(writeName + "申請紙本", invoiceNumber, writeName, writeAddress);
            rtn.Status = ApiIDEASInvoiceRequestStatus.Success;
            return rtn;
        }

        /// <summary>
        /// 依據EventPromo的Url欄位資訊，查詢所以該EventPromo的好康活動，並整理成 ApiPponDeal的list 後回傳
        /// </summary>
        /// <param name="eventPromo"></param>
        /// <returns></returns>
        private List<ApiPponDeal> ApiPponDealGetByEventPromo(EventPromo eventPromo)
        {
            EventPromoItemCollection itemCollection = PromotionFacade.EvnetPromoItemGetByMainId(eventPromo.Id, EventPromoItemType.Ppon);
            List<ApiPponDeal> rtnList = new List<ApiPponDeal>();
            foreach (var item in itemCollection)
            {
                ApiPponDeal apiPponDeal = PponDealApiManager.ApiPponDealGetByDealId(item.ItemId, ApiImageSize.M);
                if (apiPponDeal != null)
                {
                    rtnList.Add(apiPponDeal);
                }
            }
            return rtnList;
        }

        /// <summary>
        /// 取得目前雙北APP個點資料
        /// </summary>
        /// <returns></returns>
        public static List<IDEASStationData> IDEASStationDataGetList()
        {
            //現階段雙北APP上檔後台為上線，先以商品主題活動功能代替，透過此文件檔取得各數位看板所需要抓取的商品主題活動頁URL參數
            //string fileName = "IDEASStationData.txt";
            //string filePath = HttpContext.Current.Server.MapPath(@"~/template/" + fileName);
            //string stationJSon = File.ReadAllText(filePath);

            //List<IDEASStationData> rtnList = new List<IDEASStationData>();
            //try
            //{
            //    rtnList = new JsonSerializer().Deserialize<List<IDEASStationData>>(stationJSon);
            //}
            //catch (Exception ex)
            //{
            //    log.Error(string.Format("ApiLocationDealManager.ApiIDEASEventDataGetListByTime 檔案{0} JSON錯誤", filePath), ex);
            //}
            //return rtnList;

            IdeasStationCollection items = PponProvider.GetIdeasStationList();
            List<IDEASStationData> rtnList = new List<IDEASStationData>();

            foreach (var item in items)
            {
                IDEASStationData d = new IDEASStationData()
                {
                    Id = item.Id,
                    Title = item.Title,
                    CouponDealBroadcastMinutes = item.CouponDealBroadcastMinutes ?? (int)item.CouponDealBroadcastMinutes,
                    CouponEventUrl = item.CouponEventUrl,
                    DeliveryDealBroadcastMinutes = item.DeliveryDealBroadcastMinutes ?? (int)item.DeliveryDealBroadcastMinutes,
                    DeliveryEventUrl = item.DeliveryEventUrl,
                    SpecialDealBroadcastTime = new List<IDEASStationData.BroadcastFormat>(),
                    SpecialEventUrl = item.SpecialEventUrl,
                    VourcherEventUrl = item.VourcherEventUrl,
                    VourcherStoreBroadcastMinutes = item.VourcherStoreBroadcastMinutes ?? (int)item.VourcherStoreBroadcastMinutes
                };

                if (!string.IsNullOrWhiteSpace(item.SpecialDealBroadcastTimeStartTimeFormat1) && !string.IsNullOrWhiteSpace(item.SpecialDealBroadcastTimeEndTimeFormat1))
                {
                    d.SpecialDealBroadcastTime.Add(
                        new IDEASStationData.BroadcastFormat()
                        {
                            StartTimeFormat = item.SpecialDealBroadcastTimeStartTimeFormat1
                            ,
                            EndTimeFormat = item.SpecialDealBroadcastTimeEndTimeFormat1
                        });

                }


                if (!string.IsNullOrWhiteSpace(item.SpecialDealBroadcastTimeStartTimeFormat2) && !string.IsNullOrWhiteSpace(item.SpecialDealBroadcastTimeEndTimeFormat2))
                {
                    d.SpecialDealBroadcastTime.Add(
                        new IDEASStationData.BroadcastFormat()
                        {
                            StartTimeFormat = item.SpecialDealBroadcastTimeStartTimeFormat2
                            ,
                            EndTimeFormat = item.SpecialDealBroadcastTimeEndTimeFormat2
                        });

                }


                rtnList.Add(d);
            }


            return rtnList;
        }

        /// <summary>
        /// ApiIDEASDeal 依據檔次編號取得物件
        /// </summary>
        /// <param name="dealId"></param>
        /// <returns></returns>
        private static ApiIDEASDeal ApiIDEASDealGetByDealId(int dealId)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(dealId);
            //檢查檔次是否已到期

            #region 檔次圖片
            string[] imagePaths;
            if (deal.ItemPrice == 0)
            {
                imagePaths = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where(
                    (x, i) => i == 0).ToArray();
            }
            else
            {
                imagePaths = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where(
                    (x, i) => i != 1 && i != 2).ToArray();
            }
            #endregion 檔次圖片

            #region 購物車判斷
            bool shoppingCart = false;
            if (deal.ShoppingCart.HasValue)
            {
                shoppingCart = deal.ShoppingCart.Value;
            }
            #endregion 購物車判斷

            #region 判斷是否為多檔次
            ApiMultiDealType multiDealType = ApiMultiDealType.NotMultiDeal;
            List<ApiIDEASSubDeal> subDeals = new List<ApiIDEASSubDeal>();
            if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
            {
                multiDealType = ApiMultiDealType.MainDeal;
                //查詢子檔次資料
                ViewComboDealCollection comboDeals = PponProvider.GetViewComboDealByBid(deal.BusinessHourGuid, false);
                foreach (var comboDeal in comboDeals)
                {
                    ApiIDEASSubDeal subDeal = new ApiIDEASSubDeal()
                    {
                        dealId = comboDeal.UniqueId,
                        title = comboDeal.Title
                    };
                    subDeals.Add(subDeal);
                }
            }
            else if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
            {
                multiDealType = ApiMultiDealType.SubDeal;
            }
            #endregion 判斷是否為多檔次

            #region 免運費資訊
            // 階梯式運費
            List<ApiIDEASFreight> apiFreights = new List<ApiIDEASFreight>();
            CouponFreightCollection freights = PponProvider.CouponFreightGetList(deal.BusinessHourGuid, CouponFreightType.Income);
            foreach (CouponFreight couponFreight in freights.OrderBy(x => x.StartAmount))
            {
                var apiFreight = new ApiIDEASFreight();
                apiFreight.startAmount = couponFreight.StartAmount;
                apiFreight.endAmount = couponFreight.EndAmount;
                apiFreight.freightAmount = couponFreight.FreightAmount;

                apiFreights.Add(apiFreight);
            }

            #endregion 免運費資訊

            #region 商品選項

            var categories = new List<ApiIDEASPponOptionCategory>();
            AccessoryGroupCollection viagc = ItemProvider.AccessoryGroupGetListByItem(deal.ItemGuid);
            foreach (AccessoryGroup ag in viagc)
            {
                var category = new ApiIDEASPponOptionCategory();
                category.categoryGuid = ag.Guid;
                category.options = new List<ApiIDEASPponOption>();
                category.categoryName = ag.AccessoryGroupName;

                ViewItemAccessoryGroupCollection vigc = ItemProvider.ViewItemAccessoryGroupGetList(deal.ItemGuid, ag.Guid);
                foreach (ViewItemAccessoryGroup vig in vigc)
                {
                    var option = new ApiIDEASPponOption();
                    option.optionGuid = vig.AccessoryGroupMemberGuid;
                    option.optionName = vig.AccessoryName;
                    option.quantity = vig.Quantity == null ? ApiPponOption.DefaultQuantity : vig.Quantity.Value;
                    category.options.Add(option);
                }
                categories.Add(category);
            }
            #endregion 商品選項

            #region 搜尋分店資料
            List<ApiIDEASPponStore> storeOptions = new List<ApiIDEASPponStore>();
            ViewPponStoreCollection stores = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(deal.BusinessHourGuid);
            foreach (ViewPponStore viewPponStore in stores.OrderBy(x => x.SortOrder))
            {
                ApiIDEASPponStore storeOption = new ApiIDEASPponStore();
                storeOption.guid = viewPponStore.StoreGuid;
                storeOption.storeName = viewPponStore.StoreName;
                storeOption.phone = viewPponStore.Phone;
                storeOption.address = viewPponStore.CityName + viewPponStore.TownshipName + viewPponStore.AddressString;
                storeOption.openTime = viewPponStore.OpenTime;
                storeOption.closeDate = viewPponStore.CloseDate;
                storeOption.remarks = viewPponStore.Remarks;
                storeOption.mrt = viewPponStore.Mrt;
                storeOption.car = viewPponStore.Car;
                storeOption.bus = viewPponStore.Bus;
                storeOption.otherVehicles = viewPponStore.OtherVehicles;
                storeOption.webUrl = viewPponStore.WebUrl;
                storeOption.fbUrl = viewPponStore.FacebookUrl;
                storeOption.plurkUrl = viewPponStore.PlurkUrl;
                storeOption.blogUrl = viewPponStore.OtherUrl;
                storeOption.creditcardAvailable = viewPponStore.CreditcardAvailable;

                //取出分店座標
                SqlGeography storeGeo = LocationFacade.GetGeographyByCoordinate(viewPponStore.Coordinate);
                ApiCoordinates Coordinates = ApiCoordinates.FromSqlGeography(storeGeo);
                storeOption.longitude = Coordinates.Longitude;
                storeOption.latitude = Coordinates.Latitude;

                storeOptions.Add(storeOption);
            }

            #endregion 搜尋分店資料

            #region 產生回傳的資料
            ApiIDEASDeal rtnData = new ApiIDEASDeal();
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
            rtnData.dealId = dealId;
            rtnData.title = deal.EventTitle;
            rtnData.kanbanName = deal.EventName;
            rtnData.dealName = deal.CouponUsage;
            rtnData.description = contentLite.Description;
            rtnData.photoUrls = imagePaths.ToList();
            rtnData.introduction = PponDealApiManager.NoHTML(contentLite.Introduction);
            rtnData.isShoppingCart = shoppingCart;
            rtnData.pponDeliveryType = (DeliveryType)deal.DeliveryType;
            rtnData.originPrice = deal.ItemOrigPrice;
            rtnData.sellPrice = deal.ItemPrice;
            rtnData.discount = ViewPponDealManager.GetDealDiscountString(deal);
            rtnData.total = deal.OrderTotalLimit.Value;
            rtnData.orderedQuantity = deal.OrderedQuantity.Value;
            rtnData.quantityMin = deal.BusinessHourOrderMinimum;
            //成套販售美套數量，若未設定，一律為1  本周不能上線
            //rtnData.comboPackCount = deal.ComboPackCount.HasValue ? deal.ComboPackCount.Value : 1;
            rtnData.validStartTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourDeliverTimeS.Value);
            rtnData.validEndTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourDeliverTimeE.Value);
            rtnData.dealStartTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourOrderTimeS);
            rtnData.dealEndTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourOrderTimeE);
            rtnData.restriction = PponDealApiManager.NoHTML(contentLite.Restrictions) + "\r\n" + PponDealApiManager.GetPponBaseIntroduction(deal);
            rtnData.multiDealType = multiDealType;
            rtnData.subDealData = subDeals;
            rtnData.notDeliveryIslands = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands);
            rtnData.freightList = apiFreights;
            rtnData.optionCategories = categories;
            rtnData.stores = storeOptions;
            #endregion 產生回傳的資料

            return rtnData;
        }

        private static ApiIDEASDealSurplusQuantity ApiIDEASDealSurplusQuantityGetByDealId(int dealId)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(dealId);

            #region 商品選項

            var categories = new List<ApiIDEASDealOptionCategory>();
            AccessoryGroupCollection viagc = ItemProvider.AccessoryGroupGetListByItem(deal.ItemGuid);
            foreach (AccessoryGroup ag in viagc)
            {
                var category = new ApiIDEASDealOptionCategory();
                category.categoryGuid = ag.Guid;
                category.options = new List<ApiIDEASDealOption>();
                category.categoryName = ag.AccessoryGroupName;

                ViewItemAccessoryGroupCollection vigc = ItemProvider.ViewItemAccessoryGroupGetList(deal.ItemGuid, ag.Guid);
                foreach (ViewItemAccessoryGroup vig in vigc)
                {
                    var option = new ApiIDEASDealOption();
                    option.optionGuid = vig.AccessoryGroupMemberGuid;
                    option.optionName = vig.AccessoryName;
                    option.surplusQuantity = vig.Quantity == null ? ApiPponOption.DefaultQuantity : vig.Quantity.Value;
                    category.options.Add(option);
                }
                categories.Add(category);
            }
            #endregion 商品選項

            #region 搜尋分店資料
            ViewPponStoreCollection stores = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(deal.BusinessHourGuid);
            #endregion 搜尋分店資料

            #region 產生回傳的資料
            int orderedQuantity = deal.OrderedQuantity == null ? 0 : deal.OrderedQuantity.Value;
            decimal orderTotalLimit = deal.OrderTotalLimit == null ? 0 : deal.OrderTotalLimit.Value;
            int surplusQuantity = (int)orderTotalLimit - orderedQuantity;

            ApiIDEASDealSurplusQuantity rtnData = new ApiIDEASDealSurplusQuantity();
            rtnData.dealId = dealId;
            rtnData.orderedQuantity = orderedQuantity;
            rtnData.surplusQuantity = surplusQuantity;
            rtnData.optionCategories = categories;
            List<ApiIDEASDealStoreSurplusQuantity> storeSurplus = new List<ApiIDEASDealStoreSurplusQuantity>();
            foreach (var store in stores)
            {
                int storeQuantity = surplusQuantity;
                if (store.TotalQuantity != null)
                {
                    storeQuantity = store.TotalQuantity.Value - store.OrderedQuantity == null ? 0 : store.OrderedQuantity.Value;
                }

                ApiIDEASDealStoreSurplusQuantity quantity = new ApiIDEASDealStoreSurplusQuantity()
                {
                    storeGuid = store.StoreGuid,
                    surplusQuantity = storeQuantity
                };
                storeSurplus.Add(quantity);
            }
            rtnData.stores = storeSurplus;
            #endregion 產生回傳的資料

            return rtnData;
        }


        private static ApiIDEASVourcher ApiIDEASVourcherGetByEventId(int eventId)
        {
            ViewVourcherSeller vourcherSeller = VourcherFacade.ViewVourcherSellerGetEnabledById(eventId);
            if (vourcherSeller == null)
            {
                return null;
            }
            //商家說明
            Seller seller = SellerProvider.SellerGet(vourcherSeller.SellerGuid);
            string sellerDescription = string.Empty;
            if (seller.IsLoaded)
            {
                sellerDescription = seller.SellerDescription;
            }
            List<string> imgList = ImageFacade.GetMediaPathsFromRawData(vourcherSeller.PicUrl, MediaType.PponDealPhoto).ToList();
            ApiIDEASVourcher rtnData = new ApiIDEASVourcher();
            rtnData.eventId = eventId;
            rtnData.sellerName = vourcherSeller.SellerName;
            rtnData.sellerLogoimgPath = VourcherFacade.GetSellerLogoImageUrl(seller.SellerLogoimgPath, vourcherSeller.PicUrl);
            rtnData.sellerDescription = seller.SellerDescription;
            rtnData.vourcherContent = vourcherSeller.Contents;

            rtnData.instruction = vourcherSeller.Instruction;
            rtnData.restriction = vourcherSeller.Restriction;
            rtnData.validStartTime = ApiSystemManager.DateTimeToDateTimeString(vourcherSeller.StartDate.Value);
            rtnData.validEndTime = ApiSystemManager.DateTimeToDateTimeString(vourcherSeller.EndDate.Value);
            rtnData.others = vourcherSeller.Others;
            rtnData.mode = (VourcherEventMode)vourcherSeller.Mode;
            rtnData.maxQuantity = vourcherSeller.MaxQuantity;
            rtnData.picUrl = imgList;
            rtnData.attentionCount = vourcherSeller.PageCount;

            rtnData.sellerDescription = sellerDescription;
            rtnData.consumptionAvg = (SellerConsumptionAvg?)vourcherSeller.SellerConsumptionAvg; //平均店消費
            //分店
            ViewVourcherStoreCollection stores = VourcherFacade.ViewVourcherStoreCollectionGetByEventId(0, 0, vourcherSeller.Id, string.Empty);
            rtnData.eventStores = new List<ApiIDEASVourcherEventStore>();
            foreach (ViewVourcherStore store in stores)
            {
                ApiIDEASVourcherEventStore apiStore = new ApiIDEASVourcherEventStore();
                apiStore.guid = store.StoreGuid;
                apiStore.id = store.Id;
                apiStore.storeName = store.StoreName;
                apiStore.address = store.City + store.Town + store.AddressString;
                apiStore.phone = store.Phone;
                apiStore.openTime = store.OpenTime;
                apiStore.closeDate = store.CloseDate;
                apiStore.remarks = store.Remarks;
                apiStore.mrt = store.Mrt;
                apiStore.car = store.Car;
                apiStore.bus = store.Bus;
                apiStore.otherVehicles = store.OtherVehicles;
                apiStore.webUrl = store.WebUrl;
                apiStore.fbUrl = store.FacebookUrl;
                apiStore.plurkUrl = store.PlurkUrl;
                apiStore.blogUrl = store.BlogUrl;
                apiStore.otherUrl = store.OtherUrl;
                apiStore.creditcardAvailable = store.CreditcardAvailable;
                SqlGeography storeGeo = LocationFacade.GetGeographyByCoordinate(store.Coordinate);

                ApiCoordinates Coordinates = ApiCoordinates.FromSqlGeography(storeGeo);
                apiStore.longitude = Coordinates.Longitude;
                apiStore.latitude = Coordinates.Latitude;

                apiStore.distance = 0;
                apiStore.distanceDesc = string.Empty;

                rtnData.eventStores.Add(apiStore);
            }
            return rtnData;
        }

        private static ApiIDEASVourcherSurplusQuantity ApiIDEASVourcherSurplusQuantityGetByEventId(int eventId)
        {
            ViewVourcherSeller vourcherSeller = VourcherFacade.ViewVourcherSellerGetEnabledById(eventId);
            if (vourcherSeller == null)
            {
                return null;
            }

            int surplusQuantity = -1;
            switch ((VourcherEventMode)vourcherSeller.Mode)
            {
                case VourcherEventMode.None:
                    surplusQuantity = -1;
                    break;
                case VourcherEventMode.NoEventLimitQuantyOnlyOneTime:
                    surplusQuantity = -1;
                    break;
                case VourcherEventMode.EventLimitQuantityOnlyOneTime:
                    {
                        int quantity = vourcherSeller.MaxQuantity - vourcherSeller.CurrentQuantity;
                        surplusQuantity = quantity < 0 ? 0 : quantity;
                    }
                    break;
                case VourcherEventMode.EventLimitQuantityNoUsingTimes:
                    {
                        int quantity = vourcherSeller.MaxQuantity - vourcherSeller.CurrentQuantity;
                        surplusQuantity = quantity < 0 ? 0 : quantity;
                    }
                    break;
            }

            ApiIDEASVourcherSurplusQuantity rtnData = new ApiIDEASVourcherSurplusQuantity()
            {
                eventId = eventId,
                surplusQuantity = surplusQuantity
            };
            return rtnData;
        }
    }
}

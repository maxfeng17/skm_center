﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component.API
{
    public class ApiSystemManager
    {
        private static readonly ISysConfProvider _sysConfProvider;
        private static ISystemProvider _systemProvider;
        private static AppVersionCollection _appVersionCollection;
        private static AppVersionCollection AppVersionCollection
        {
            get
            {
                //檢查靜態物件
                if (_appVersionCollection == null)
                {
                    lock (typeof(ApiSystemManager))
                    {
                        //lock中，再檢查一次
                        if (_appVersionCollection == null)
                        {
                            _appVersionCollection = _systemProvider.AppVersionGetList();
                        }
                    }
                }

                return _appVersionCollection;
            }
        }


        static ApiSystemManager()
        {
            _sysConfProvider = ProviderFactory.Instance().GetConfig();
            _systemProvider = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        }

        /// <summary>
        /// 重新讀取暫存資料
        /// </summary>
        public static void ReloadManagerData()
        {

            lock (typeof(ApiSystemManager))
            {
                _appVersionCollection = _systemProvider.AppVersionGetList();
            }
        }

        public static string GetFamilyMarkOpenTimeString()
        {
            string opentimeSettingString = _sysConfProvider.FamilyMariOpenDatetime;
            DateTime openTime;
            IFormatProvider ifp = new CultureInfo("zh-TW", true);
            if (DateTime.TryParseExact(opentimeSettingString, "yyyy-MM-dd HH:mm:ss", ifp, DateTimeStyles.None, out openTime))
            {
                return DateTimeToDateTimeString(openTime);
            }
            return string.Empty;
        }

        public static string DateTimeToDateTimeString(DateTime date)
        {
            return date.ToString("yyyyMMdd HHmmss zz00");
        }

        public static DateTime DateTimeFromDateTimeString(string str)
        {
            DateTime dt;
            if (DateTime.TryParseExact(str, @"yyyyMMdd HHmmss zz00", null, DateTimeStyles.None, out dt) == false)
            {
                return DateTime.MinValue;
            }
            return dt;
        }

        static readonly string[] dateFormats = { 
            // 17 format
            "yyyyMMdd HHmmss zz00",
            // Basic formats
            "yyyyMMddTHHmmsszzz",
            "yyyyMMddTHHmmsszz",
            "yyyyMMddTHHmmssZ",
            // Extended formats
            "yyyy-MM-ddTHH:mm:sszzz",
            "yyyy-MM-ddTHH:mm:sszz",
            "yyyy-MM-ddTHH:mm:ssZ",
            "yyyy/MM/ddTHH:mm:sszzz",
            "yyyy/MM/ddTHH:mm:sszz",
            "yyyy/MM/ddTHH:mm:ssZ",
            // All of the above with reduced accuracy
            "yyyyMMddTHHmmzzz",
            "yyyyMMddTHHmmzz",
            "yyyyMMddTHHmmZ",
            "yyyy-MM-ddTHH:mmzzz",
            "yyyy-MM-ddTHH:mmzz",
            "yyyy-MM-ddTHH:mmZ",
            "yyyy/MM/ddTHH:mmzzz",
            "yyyy/MM/ddTHH:mmzz",
            "yyyy/MM/ddTHH:mmZ",
            // Accuracy reduced to hours
            "yyyyMMddTHHzzz",
            "yyyyMMddTHHzz",
            "yyyyMMddTHHZ",
            "yyyy-MM-ddTHHzzz",
            "yyyy-MM-ddTHHzz",
            "yyyy-MM-ddTHHZ",
            "yyyy/MM/ddTHHzzz",
            "yyyy/MM/ddTHHzz",
            "yyyy/MM/ddTHHZ"
        };
        /// <summary>
        /// 一個既相容17Life也相容ISO8601的日期時間格式
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static DateTime DateTimeFromDateTimeString2(string str)
        {
            DateTime dt;
            if (DateTime.TryParseExact(str, dateFormats, null, DateTimeStyles.None, out dt) == false)
            {
                return DateTime.MinValue;
            }
            return dt;
        }

        public static string DateTimeToDateString(DateTime date)
        {
            return date.ToString("yyyyMMddzz00");
        }

        public static bool TryDateStringToDateTime(string dateString, out DateTime date)
        {
            return DateTime.TryParseExact(dateString, "yyyyMMddzz00", null, DateTimeStyles.None, out date);
        }

        public static bool TryDateTimeStringToDateTime(string datetimeString, out DateTime date)
        {
            return DateTime.TryParseExact(datetimeString, "yyyyMMdd HHmmss zz00", null, DateTimeStyles.None, out date);
        }

        /// <summary>
        /// 檢查特定APP的特定版本是否需要更新
        /// </summary>
        /// <param name="appName">特定app的版本編號</param>
        /// <param name="checkVersion">要檢查的版本</param>
        /// <returns></returns>
        public static CheckAppVersionReply CheckAppVersion(string appName, Version checkVersion)
        {
            //因App需求將絕對相等改為起始相等
            List<AppVersion> appVersions = AppVersionCollection.Where(x => appName.StartsWith(x.AppName)).ToList();
            if (appVersions.Count > 0)
            {
                AppVersion appVersion = appVersions.First();
                //有此APP的紀錄
                //宣告最小支援版本、最新版本、輸入進行檢查的版本 三個參數
                Version minVersion, latestVersion;
                //產生需要的Version物件，如果失敗回傳Error
                if ((!Version.TryParse(appVersion.MinimumVersion, out minVersion)) ||
                    (!Version.TryParse(appVersion.LatestVersion, out latestVersion)))
                {
                    return Core.CheckAppVersionReply.Error;
                }
                //比較如果輸入版號小於最小支援版號，回傳強制更新
                if (checkVersion < minVersion)
                {
                    return Core.CheckAppVersionReply.ForceUpgrade;
                }
                //如果輸入版號大於等於最小支援版號，小於最新版號，則建議更新
                if (checkVersion >= minVersion && checkVersion < latestVersion)
                {
                    return Core.CheckAppVersionReply.RecommendedUpgrade;
                }
                //其他狀況回傳版本正常
                return Core.CheckAppVersionReply.Pass;
            }
            //其他狀況一律回傳檢查異常
            return Core.CheckAppVersionReply.Error;
        }

        public static CheckAppVersionReply CheckAppVersion(string appName, Version checkVersion, Version checkOsVersion)
        {
            //因App需求將絕對相等改為起始相等
            List<AppVersion> appVersions = AppVersionCollection.Where(x => appName.StartsWith(x.AppName)).ToList();
            if (appVersions.Count > 0)
            {
                AppVersion appVersion = appVersions.First();
                //有此APP的紀錄
                //宣告最小支援版本、最新版本、輸入進行檢查的版本 三個參數
                Version minVersion, latestVersion, osVersion, os_minVersion;
                //產生需要的Version物件，如果失敗回傳Error
                if ((!Version.TryParse(appVersion.MinimumVersion, out minVersion)) ||
                    (!Version.TryParse(appVersion.LatestVersion, out latestVersion)) ||
                    (!Version.TryParse(appVersion.OsVersion, out osVersion)) ||
                    (!Version.TryParse(appVersion.OsMinimumVersion, out os_minVersion)))
                {
                    return Core.CheckAppVersionReply.Error;
                }

                if (checkOsVersion < os_minVersion)
                {
                    return Core.CheckAppVersionReply.RedirectToWebUpdate;
                }
                else if (checkVersion < minVersion)
                {
                    return Core.CheckAppVersionReply.ForceUpgrade;
                }
                else if (checkOsVersion < osVersion)
                {
                    return Core.CheckAppVersionReply.InformRecommendUpdate;
                }
                else if (checkVersion < latestVersion)
                {
                    return Core.CheckAppVersionReply.RecommendedUpgrade;
                }
                else
                {
                    return Core.CheckAppVersionReply.Pass;
                }
            }
            //其他狀況一律回傳檢查異常
            return Core.CheckAppVersionReply.Error;
        }

        public static AppVersionCollection GetAppVersionCollection()
        {
            return AppVersionCollection;
        }

        public static bool IsNewAppVersion(string nowVersion, string iosVersion, string androidVersion, string userId)
        {
            if (string.IsNullOrEmpty(nowVersion) || string.IsNullOrEmpty(userId) || 
                string.IsNullOrEmpty(iosVersion) || string.IsNullOrEmpty(androidVersion))
            {
                return false;
            }
                
            Version vApp = new Version(nowVersion);
            Version vConfig;

            switch (userId.Substring(0, 3).ToUpper())
            {
                case "AND":
                    vConfig = new Version(androidVersion);
                    break;
                case "IOS":
                    vConfig = new Version(iosVersion);
                    break;
                default:
                    return false;
            }
            return vApp.CompareTo(vConfig) > 0;
        }
    }
}

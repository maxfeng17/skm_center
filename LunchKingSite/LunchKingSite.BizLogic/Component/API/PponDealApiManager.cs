﻿using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.SqlServer.Types;
using System.Reflection;
using System.Text;
using log4net;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Models.API;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.BizLogic.Models.PponOrder;
using LunchKingSite.Core.Models.PponEntities;
using System.Runtime.Caching;

namespace LunchKingSite.BizLogic.Component.API
{
    public class PponDealApiManager
    {
        private const int MaxLPerView = 50;

        private static readonly IPponProvider _pponProvider;
        private static readonly IMemberProvider _memberProvider;
        private static readonly ISellerProvider _sellerProvider;
        private static readonly IItemProvider _itemProvider;
        private static readonly ISysConfProvider _sysConfProvider;
        private static readonly ILocationProvider _locationProvider;
        private static readonly IOrderProvider _orderProvider;
        private static readonly Core.IServiceProvider _csProvider;
        private static readonly ISysConfProvider _config;
        private static readonly IFamiportProvider _famiProvider;
        private static IMGMProvider _mgmp;
        private static ISellerProvider _sp;
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static readonly IPponEntityProvider _pponEntityProvider;
        private static ILog logger = LogManager.GetLogger("speed");
        private static readonly IOrderEntityProvider _oep = ProviderFactory.Instance().GetProvider<IOrderEntityProvider>();

        private const string PponPromoPageUrl = "/ppon/PponReferralRedirection.aspx";
        private const string PponDealPageUrl = "/ppon/Default.aspx";
        private const int FamiBeaconLockCategoryId = 320;

        static PponDealApiManager()
        {
            _pponProvider = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _memberProvider = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _sellerProvider = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _itemProvider = ProviderFactory.Instance().GetProvider<IItemProvider>();
            _sysConfProvider = ProviderFactory.Instance().GetConfig();
            _locationProvider = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            _orderProvider = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _csProvider = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _famiProvider = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
            _mgmp = ProviderFactory.Instance().GetProvider<IMGMProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _pponEntityProvider = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
        }

        #region 檔次相關

        /// <summary>
        /// 頻道檔次列表
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="channelAreaId"></param>
        /// <param name="categoryList"></param>
        /// <param name="sortType"></param>
        /// <param name="filterList"></param>
        /// <param name="totalCount"></param>
        /// <param name="isNewPrototype"></param>
        /// <param name="getAll"></param>
        /// <param name="startIndex"></param>
        /// <param name="IsExcludeFamily"></param>
        /// <returns></returns>
        public static List<PponDealSynopsis> PponDealSynopsesGetList(int channelId, int? channelAreaId, List<int> categoryList, CategorySortType sortType
            , List<int> filterList, ref int totalCount, bool isNewPrototype = false, bool getAll = true, int startIndex = 0, bool IsExcludeFamily = false)
        {

            PponCity city = PponCityGroup.GetPponCityByChannel(channelId, channelAreaId);

            List<MultipleMainDealPreview> dealPreviewList =
                ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(channelId, channelAreaId, categoryList, city);

            List<PponDealSynopsis> rtnList = new List<PponDealSynopsis>();

            if (filterList != null && filterList.Count > 0)
            {
                dealPreviewList = dealPreviewList.Where(x => x.DealCategoryIdList.Any(filterList.Contains)).ToList();
            }
            //處理排序，如果需要的排序條件為預設值，則不進行排序
            if (sortType != CategorySortType.Default)
            {
                dealPreviewList = ViewPponDealManager.DefaultManager.SortMultipleMainDealPreview(dealPreviewList, sortType).ToList();
            }
            ////排除全家鎖定檔檢查
            if (IsExcludeFamily)
            {
                dealPreviewList = dealPreviewList.Where(x => !x.DealCategoryIdList.Contains(FamiBeaconLockCategoryId)).ToList();
            }

            //逐筆將資料轉換為PponDealSynopsis
            foreach (var preview in dealPreviewList)
            {
                if (HiddenInApp(preview.PponDeal.BusinessHourGuid, city.CityId))
                {
                    //不顯示於APP
                    continue;
                }

                if (isNewPrototype)
                {
                    var cityList = new JsonSerializer().Deserialize<List<int>>(preview.PponDeal.CityList);
                    if (cityList.Contains(CategoryManager.Default.Skm.CityId ?? 0))
                    {
                        #region 新光只撈優惠部分 ExternalDealTagType 優惠活動:0  熱賣商品:1  注目商品:2

                        ViewExternalDeal ved = null;
                        if (_config.NotShowSkmDealsInWeb == false)
                        {
                            ved = ExternalDealFacade.GetViewExternalDeals(preview.PponDeal.BusinessHourGuid).FirstOrDefault();
                        }

                        //是否有提品
                        if (ved == null)
                        {
                            continue;
                        }

                        if ((ved.Status == (int)SKMDealStatus.Confirmed) == false)
                        {
                            continue;
                        }

                        List<int> externalDealTagType = new JsonSerializer().Deserialize<List<int>>(ved.TagList);
                        if (CategoryManager.SKMDealTypes.Any())
                        {
                            if (!externalDealTagType.Any(x => CategoryManager.SKMDealTypes.Contains(x)))
                            {
                                continue;
                            }
                        }

                        #endregion
                    }
                }

                rtnList.Add(preview.AppPponDeal);
            }

            if (isNewPrototype)
            {
                rtnList = PponDealSynopsisSkipSelect(rtnList, getAll, startIndex, out totalCount);
            }
            return rtnList;
        }

        /// <summary>
        /// APP首頁-每日精選檔次列表 (cache版)
        /// </summary>
        /// <param name="workAreaId"></param>
        /// <returns></returns>
        public static List<PponDealSynopsisBasic> GetTodayChoicePponDealSynopsesByCache(int workAreaId)
        {
            //緩存30分鍾，但20~30分鍾取資料會觸發更新
            const int minutes = 30;
            const int extendExpireMinutes = 20;
            CacheDataStatus dataStatus;
            List<PponDealSynopsisBasic> basicDeals =
                MemoryCache2.Get<List<PponDealSynopsisBasic>>(workAreaId, out dataStatus);
            if (dataStatus == CacheDataStatus.None)
            {
                basicDeals = GetTodayChoicePponDealSynopses(workAreaId);
                MemoryCache2.Set(workAreaId, basicDeals, minutes, extendExpireMinutes);
            }
            else if (dataStatus == CacheDataStatus.Expired && _config.MemoryCache2Mode == 2)
            {
                Func<List<PponDealSynopsisBasic>> func = delegate { return GetTodayChoicePponDealSynopses(workAreaId); };
                MemoryCache2.SetAsync(workAreaId, func, minutes, extendExpireMinutes);
            }

            return basicDeals;
        }
        /// <summary>
        /// APP首頁-每日精選檔次列表
        /// </summary>
        /// <param name="workAreaId"></param>
        /// <returns></returns>
        public static List<PponDealSynopsisBasic> GetTodayChoicePponDealSynopses(int workAreaId)
        {
            List<PponDealSynopsisBasic> basicDeals = new List<PponDealSynopsisBasic>();
            List<MultipleMainDealPreview> previewDeals = PponFacade.GetTodayChoicePponDealSynopses(workAreaId);
            foreach (var previewDeal in previewDeals)
            {
                basicDeals.Add(MultipleMainDealPreviewToBasic(previewDeal));
            }
            return basicDeals;
        }
        /// <summary>
        /// 取得24到貨熱銷檔次，條件為近6小時銷售最多
        /// </summary>
        /// <returns></returns>
        public static List<PponDealSynopsis> GetFoodBeautyTravelHotDealSynopses()
        {
            DateTime now = DateTime.Now;
            string cacheKey = string.Format("FoodBeautyTravelHotDealSynopses:{0}", now.ToString("yyyyMMddHH"));
            List<PponDealSynopsis> result = MemoryCache.Default.Get(cacheKey) as List<PponDealSynopsis>;
            if (result != null)
            {
                return result;
            }
            result = new List<PponDealSynopsis>();
            List<Guid> dealGuids = _pponProvider.GetFoodBeautyTravelHotDealsGuidList();
            foreach (Guid guid in dealGuids)
            {
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(guid, true);
                if (vpd.IsSoldOut)
                {
                    continue;
                }
                if (vpd.IsHiddenInRecommendDeal)
                {
                    continue;
                }
                PponDealSynopsis synopsis = PponDealSynopsisGetByDeal(vpd, vpd.CategoryIds, 0, VbsDealType.Ppon,
                    true);
                result.Add(synopsis);
            }
            //實際資料的有效性，由cacheKey決定，每1小時就會換cacheKey
            MemoryCache.Default.Set(cacheKey, result, new DateTimeOffset(DateTime.Now.AddDays(1)));
            return result;
        }

        public static List<PponDealSynopsis> Get24hDealSynopses(List<int> categories, bool getAll, int startIndex, ref int totalCount)
        {
            List<MultipleMainDealPreview> mmdeals = PponDealPreviewManager.GetArrival24hCategoryMMDeals(categories);
            List<PponDealSynopsis> rtnList = new List<PponDealSynopsis>();

            foreach (var dealPreview in mmdeals)
            {
                if (HiddenInApp(dealPreview))
                {
                    //不顯示於APP
                    continue;
                }
                if (dealPreview.DealCategoryIdList.Contains(FamiBeaconLockCategoryId))
                {
                    //不顯示全家Beacon鎖定檔
                    continue;
                }

                //產生APP用的檔次說明物件
                PponDealSynopsis synopsis = PponDealSynopsisGetByDeal(dealPreview.PponDeal, dealPreview.DealCategoryIdList, dealPreview.Sequence, VbsDealType.Ppon, true);

                //已存在檔次不寫入，行銷檔全頻道都有勾選
                if (!rtnList.Any(x => x.Bid == synopsis.Bid))
                {
                    rtnList.Add(synopsis);
                }
            }
            rtnList = PponDealSynopsisSkipSelect(rtnList, getAll, startIndex, out totalCount);

            return rtnList;
        }
        /// <summary>
        /// 取得美食、玩美、旅遊憑證，並特意移除咖啡寄杯，依照上檔日期排序(最新上檔在前)
        /// 過瀘不顯示
        /// </summary>
        /// <param name="topNum"></param>
        /// <returns></returns>
        public static List<PponDealSynopsis> GetFoodBeautyTravelCouponDealSynopses(int topNum) 
        {
            List<MultipleMainDealPreview> mmdeals = PponDealPreviewManager.GetFoodBeautyTravelCategoryMMDeals();
            List<PponDealSynopsis> rtnList = new List<PponDealSynopsis>();

            foreach (var dealPreview in mmdeals)
            {
                if (HiddenInApp(dealPreview))
                {
                    //不顯示於APP
                    continue;
                }
                if (dealPreview.DealCategoryIdList.Contains(FamiBeaconLockCategoryId))
                {
                    //不顯示全家Beacon鎖定檔
                    continue;
                }

                if (dealPreview.DealCategoryIdList.Contains(CategoryManager.Default.DepositCoffee.CategoryId))
                {
                    //不顯示咖啡寄杯
                    continue;
                }
                if (dealPreview.PponDeal.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    continue;
                }

                //產生APP用的檔次說明物件
                PponDealSynopsis synopsis = PponDealSynopsisGetByDeal(dealPreview.PponDeal, dealPreview.DealCategoryIdList, dealPreview.Sequence, VbsDealType.Ppon, true);

                //已存在檔次不寫入，行銷檔全頻道都有勾選
                if (!rtnList.Any(x => x.Bid == synopsis.Bid))
                {
                    rtnList.Add(synopsis);
                }
                if (rtnList.Count >= topNum)
                {
                    break;
                }
            }            
            return rtnList;
        }

        /// <summary>
        /// 以BID查詢符合的檔次列表
        /// </summary>
        /// <returns></returns>
        public static List<PponDealSynopsis> PponDealSynopsesGetListByBids(
            List<Guid> bids, bool isNewPrototype = false, bool sort = false)
        {
            var dealList = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bids);
            return PponDealSynopsesGetListByBids(dealList, isNewPrototype, sort);
        }

        public static List<PponDealSynopsis> PponDealSynopsesGetListByBids(
            List<IViewPponDeal> dealList, bool isNewPrototype = false, bool sort = false)
        {
            if (sort)
            {
                //針對銷量排一次
                var soldOut = dealList.Where(x => x.OrderTotalLimit.GetValueOrDefault(0) == 0);
                dealList = dealList.Where(x => x.OrderTotalLimit.GetValueOrDefault(0) > 0)
                    .OrderByDescending(x => x.IsWms)
                    .ThenByDescending(x => x.GetAdjustedOrderedQuantity().Quantity).ToList();

                dealList.AddRange(soldOut);
            }

            var dealCategory = _pponProvider.CategoryDealsGetList(
                dealList.Select(t => t.BusinessHourGuid).ToList()).AsEnumerable();

            List<PponDealSynopsis> rtnList = new List<PponDealSynopsis>();
            int counter = 1;
            foreach (var item in dealList)
            {
                if (HiddenInApp(item.BusinessHourGuid))
                {
                    //不顯示於APP
                    continue;
                }

                //產生APP用的檔次說明物件
                PponDealSynopsis synopsis = PponDealSynopsisGetByDeal(item
                    , dealCategory.Where(x => x.Bid == item.BusinessHourGuid).Select(x => x.Cid).ToList()
                    , counter++
                    , VbsDealType.Ppon
                    , isNewPrototype);

                //320Beacon
                //323Beacon
                var filerCategory = new List<int> { 320, 323 };

                if (synopsis.Categories.Intersect(filerCategory).Any())
                {
                    continue;
                }

                //已存在檔次不寫入，行銷檔全頻道都有勾選
                if (!rtnList.Any(x => x.Bid == synopsis.Bid))
                {
                    rtnList.Add(synopsis);
                }
            }

            return rtnList;
        }

        /// <summary>
        /// 以BID查詢檔次相關資料，無資料或有錯誤回傳null。
        /// 相關資料由暫存資料中組出，非即時資訊
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="imageSize"></param>
        /// <param name="isNewPrototype"></param>
        /// <returns></returns>
        public static ApiPponDeal PponDealForApiGet(Guid guid, ApiImageSize imageSize, bool isNewPrototype = false)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(guid, true);
            return (deal.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0
                ? null //SkmDealForApiGet(deal, imageSize, isNewPrototype) 
                : ApiPponDealGetByViewPponDeal(deal, imageSize, isNewPrototype);
        }

        /// <summary>
        /// 查詢檔次相關資料，無資料或有錯誤回傳null。
        /// 相關資料由暫存資料中組出，非即時資訊
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="imageSize"></param>
        /// <param name="isNewPrototype"></param>
        /// <returns></returns>
        public static ApiPponDeal SkmDealForApiGet(IViewPponDeal deal, ApiImageSize imageSize, bool isNewPrototype = false)
        {
            if (!((deal.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0))
            {
                return null;
            }

            ViewPponStoreSkmCollection pponStoresSkm = _sp.ViewPponStoreSkmGetByBid(deal.BusinessHourGuid);
            var result = ApiPponDealGetByViewPponDeal(deal, imageSize, isNewPrototype);
            if (deal.CreateTime < Convert.ToDateTime(_config.SkmShoppeDisplayNameCheckDate) &&
                deal.EventModifyTime < Convert.ToDateTime(_config.SkmShoppeDisplayNameCheckDate))
            {
                foreach (var availab in result.Availabilities)
                {
                    var storeGuid = pponStoresSkm.Where(x => x.StoreName == availab.StoreName)
                        .Select(x => x.StoreGuid).FirstOrDefault();

                    availab.StoreName = SkmFacade.GetSkmShoppeDisplayName(storeGuid);
                }
            }

            foreach (var availab in result.Availabilities)
            {
                availab.Address = availab.Mrt = string.Empty;
            }

            SkmPponDeal skmDeal = CreateSkmOrderPponDeal(deal);
            result.DiscountType = skmDeal.DiscountType;
            result.Discount = skmDeal.Discount;
            result.ExchangeDealCount = skmDeal.ExchangeDealCount;

            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
            result.Introduction = NoHTML(ReplaceLineFeedBrTag(contentLite.Introduction));
            result.Restrictions = NoHTML(ReplaceLineFeedBrTag(contentLite.Restrictions));
            result.SoldOut = deal.IsSoldOut;

            return result;
        }

        public static string ReplaceLineFeedBrTag(string content)
        {
            content = content.Replace("<br>", "\r\n");
            content = content.Replace("<br/>", "\r\n");
            content = content.Replace("<br />", "\r\n");
            return content;
        }

        /// <summary>
        /// 以檔次的uniqueId 查詢檔次相關資料，無資料或有錯誤回傳null。
        /// 相關資料由暫存資料中組出，非即時資訊
        /// </summary>
        /// <param name="id"></param>
        /// <param name="imageSize"></param>
        /// <param name="isNewPrototype"></param>
        /// <returns></returns>
        public static ApiPponDeal ApiPponDealGetByDealId(int id, ApiImageSize imageSize, bool isNewPrototype = false)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(id);
            return ApiPponDealGetByViewPponDeal(deal, imageSize, isNewPrototype);
        }

        /// <summary>
        /// 以ViewPponDeal資料產出API用的檔次相關資料，參數錯誤或有異常回傳null。
        /// 相關資料由暫存資料中組出，非即時資訊
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="imageSize"></param>
        /// <param name="isNewPrototype"></param>
        /// <returns></returns>
        private static ApiPponDeal ApiPponDealGetByViewPponDeal(IViewPponDeal deal, ApiImageSize imageSize, bool isNewPrototype = false)
        {
            //傳入資料為空或併非由資料庫取出
            if (deal == null || !deal.IsLoaded)
            {
                return null;
            }
            if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0 && deal.MainBid != null)
            {
                //判斷如果是子檔次，改搜尋主檔
                deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(deal.MainBid.Value);
            }

            if (deal.IsLoaded)
            {
                var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
                if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
                {
                    return null;
                }

                QuantityAdjustment qa = (deal.BusinessHourOrderTimeE < DateTime.Now) ? deal.GetAdjustedSlug() : deal.GetAdjustedOrderedQuantity();

                #region 可用店家

                //好康哪裡找
                List<ApiPponStore> stores = new List<ApiPponStore>();
                foreach (var availab in ProviderFactory.Instance().GetSerializer()
                    .Deserialize<AvailableInformation[]>(contentLite.Availability).ToList())
                {
                    double longitude = 0, latitude = 0;
                    double.TryParse(availab.Longitude, out longitude);
                    double.TryParse(availab.Latitude, out latitude);

                    var store = new ApiPponStore
                    {
                        StoreName = availab.N,
                        Phone = NoHTML(availab.P),
                        Address = availab.A,
                        OpenTime = (!string.IsNullOrWhiteSpace(availab.UT) ? availab.UT : availab.OT) ?? string.Empty,
                        CloseDate = availab.CD ?? string.Empty,
                        Remarks = availab.R ?? string.Empty,
                        Mrt = availab.MR ?? string.Empty,
                        Car = availab.CA ?? string.Empty,
                        Bus = availab.BU ?? string.Empty,
                        OtherVehicles = availab.OV ?? string.Empty,
                        WebUrl = availab.U ?? string.Empty,
                        FbUrl = availab.FB ?? string.Empty,
                        PlurkUrl = availab.PL ?? string.Empty,
                        BlogUrl = availab.BL ?? string.Empty,
                        OtherUrl = availab.OL ?? string.Empty,
                        Longitude = longitude,
                        Latitude = latitude
                    };
                    stores.Add(store);
                }
                #endregion

                #region 圖

                string[] imagePaths;
                if (deal.ItemPrice == 0)
                {
                    imagePaths = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where(
                            (x, i) => i == 0).ToArray();
                }
                else
                {
                    imagePaths = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where(
                            (x, i) => i != 1 && i != 2).ToArray();
                }

                if (imagePaths.Count() == 0 || Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.SKMDeal))//代表列表只上一圖 imagePaths(商品主圖) AppDealPic(列表方圖) 都使用同一圖
                {
                    imagePaths = new string[1];
                    imagePaths[0] = !string.IsNullOrEmpty(deal.AppDealPic) ? ImageFacade.GetMediaPathsFromRawData(deal.AppDealPic, MediaType.EventPromoAppMainImage)[0] : string.Empty;
                }

                #endregion

                //判斷是否為多檔次
                bool multiDeal = (deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0;
                bool shoppingCart = false;
                if (deal.ShoppingCart.HasValue)
                {
                    shoppingCart = deal.ShoppingCart.Value;
                }

                List<int> cityList = new JsonSerializer().Deserialize<List<int>>(deal.CityList);

                string defaultActivityName = I18N.Phrase.PponDealApiManagerDefaultActivityName;
                if (cityList.Count > 0)
                {
                    PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityList.First());
                    defaultActivityName = city.CityName;
                }
                //處理是否需顯示旅遊地點
                string travelPlace = string.Empty;
                if (cityList.Contains(PponCityGroup.DefaultPponCityGroup.Travel.CityId))
                {
                    travelPlace = deal.TravelPlace;
                }

                List<string> introductionTags = new List<string>();
                if (!string.IsNullOrEmpty(deal.LabelTagList))
                {
                    introductionTags =
                        deal.LabelTagList.Split(',')
                            .Select(int.Parse)
                            .ToList()
                            .Select(codeId => SystemCodeManager.GetDealLabelCodeName(codeId)).ToList();
                }

                //檢查會員是否登入，若有登入取得收藏相關的資訊
                bool isCollected = false;
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    int memberUnique = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
                    isCollected = MemberFacade.MemberCollectDealIsCollected(memberUnique, deal.BusinessHourGuid);
                }

                //**企劃確認購買人(份)數不做退貨完成後數量處理, 但要在此修正上線前的檔次都要照原邏輯執行**
                string orderedQuantity = (deal.BusinessHourOrderTimeS >= _config.StopRefundQuantityCalculateDate) ? deal.GetAdjustedOrderedQuantity().IncludeRefundQuantity.ToString("#,#") : deal.GetAdjustedOrderedQuantity().Quantity.ToString("#,#");
                string collectDealCount = _memberProvider.MemberCollectDealCountByBidStatusType(
                            deal.BusinessHourGuid, MemberCollectDealStatus.Collected, MemberCollectDealType.Experience).ToString("#,#");

                var discountStringModel = new DealDiscountStringModel();
                if (isNewPrototype == false)
                {
                    discountStringModel.DiscountString = ViewPponDealManager.GetDealDiscountString(deal);
                }
                else
                {
                    discountStringModel = ViewPponDealManager.GetDealDiscountStringByIViewPponDeal(deal);

                    //1.有設定旅遊地點，則顯示旅遊地點的資訊 
                    //2.判斷是否為0元顯示優惠、全家顯示全家、宅配顯示宅配 
                    //3.憑證檔 多個分店顯示多分店，單一分店顯示市區
                    travelPlace = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(null, deal.BusinessHourGuid);
                }

                ApiPponDeal rtnDeal = null;
                if (ProposalFacade.IsVbsProposalNewVersion() && deal.IsHouseDealNewVersion())
                {
                    decimal orderTotalLimit = deal.OrderTotalLimit ?? 0;
                    ProposalMultiDeal pmd = _sellerProvider.ProposalMultiDealGetByBid(deal.BusinessHourGuid);
                    if (pmd.IsLoaded)
                    {
                        bool isMulti = ProposalFacade.IsProposalMultiSpec(pmd.Options);
                        if (isMulti)
                        {
                            orderTotalLimit = (deal.OrderTotalLimit ?? 0);
                        }
                        else
                        {
                            int stock = 0;
                            //ProposalMultiOptionSpecCollection dbSpecs = _sellerProvider.ProposalMultiOptionSpecGetByBid(deal.BusinessHourGuid);
                            List<Guid> itemGuids = _sellerProvider.ProposalMultiOptionSpecGetItemGuidByBid(deal.BusinessHourGuid);
                            ProductItemCollection item = _pponProvider.ProductItemGetList(itemGuids);
                            stock += item.Select(x => x.Stock).Sum();
                            orderTotalLimit = stock;
                            if (orderTotalLimit < deal.OrderTotalLimit)
                            {
                                orderTotalLimit = (deal.OrderTotalLimit ?? 0);
                            }
                        }
                    }
                    rtnDeal = new ApiPponDeal
                    {
                        Bid = deal.BusinessHourGuid,
                        DealName = GetAppDealName(deal),
                        ImagePaths = imagePaths,
                        AppDealPic = !string.IsNullOrEmpty(deal.AppDealPic) ? ImageFacade.GetMediaPathsFromRawData(deal.AppDealPic, MediaType.EventPromoAppMainImage)[0] : string.Empty,
                        CouponEventName = deal.EventName,
                        CouponEventTitle = deal.EventTitle,
                        Introduction = NoHTML(contentLite.Introduction) + "\r\n" + GetPponBaseIntroduction(deal),
                        IntroductionTags = introductionTags,
                        Restrictions = NoHTML(contentLite.Restrictions) + "\r\n" + GetPponBaseIntroduction(deal),
                        Description = ViewPponDealManager.DefaultManager.GetDealAppDescription(deal),
                        Availabilities = stores,
                        SoldNum = (deal.BusinessHourOrderTimeS >= _config.StopRefundQuantityCalculateDate) ? qa.IncludeRefundQuantity : qa.Quantity,
                        SoldNumShow = (deal.BusinessHourOrderTimeS >= _config.StopRefundQuantityCalculateDate) ? qa.IncludeRefundQuantity : qa.Quantity,
                        ShowUnit = qa.IsAdjusted ? @"已售出\d份" : @"\d人已購買",
                        OriPrice = deal.ItemOrigPrice,
                        Price = deal.ItemPrice,
                        DealEndTime = DateTimeToDateTimeString(deal.BusinessHourOrderTimeE),
                        DealStartTime = DateTimeToDateTimeString(deal.BusinessHourOrderTimeS),
                        SoldOut = deal.OrderedQuantity >= orderTotalLimit,//訂單總銷售數量大於上限設為已售完
                        Remark = string.IsNullOrEmpty(contentLite.Remark) == true ? string.Empty : NoHTML(contentLite.Remark.Replace("●", "• ").Split(new[] { "||" }, StringSplitOptions.None)[2]),
                        MultiDeal = multiDeal,
                        ShoppingCart = shoppingCart,
                        DefaultActivityName = defaultActivityName,
                        TravelPlace = travelPlace,
                        DealTags = GetDealTags(deal, 2),
                        //是否顯示全家兌換價的處理
                        IsFami = ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0),

                        ShowExchangePrice = ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0),
                        ExchangePrice = deal.ExchangePrice.HasValue ? deal.ExchangePrice.Value : 0,
                        IsCollected = isCollected,
                        DiscountCodeUsed = PromotionFacade.GetDiscountUseType(deal.BusinessHourGuid),
                        AtmAvailable = PaymentFacade.CtAtmCheck(deal),
                        PiinlifeMenuTabs = ApiPiinlifeMenuTabForApiGet(deal, isNewPrototype),
                        PiinlifeRightContent = GetPiinlifeRightContent(deal),
                        IsExperience = deal.IsExperience,
                        ExchangeDealCount = string.Format("{0}人 已兌換", string.IsNullOrEmpty(orderedQuantity) ? "0" : orderedQuantity),
                        ExperienceDealCollectCount = string.Format("{0}人 已收藏", string.IsNullOrEmpty(collectDealCount) ? "0" : collectDealCount),
                        DiscountType = (int)DiscountType.None,
                        Discount = 0,

                        DisplayPrice = discountStringModel.DisplayPrice,
                        DiscountString = discountStringModel.DiscountString,
                        DiscountDisplayType = discountStringModel.DiscountDisplayType,

                        BehaviorType = (int)GetDealBehaviorType(deal)
                    };
                }
                else
                {
                    rtnDeal = new ApiPponDeal
                    {
                        Bid = deal.BusinessHourGuid,
                        DealName = GetAppDealName(deal),
                        ImagePaths = imagePaths,
                        AppDealPic = !string.IsNullOrEmpty(deal.AppDealPic) ? ImageFacade.GetMediaPathsFromRawData(deal.AppDealPic, MediaType.EventPromoAppMainImage)[0] : string.Empty,
                        CouponEventName = deal.EventName,
                        CouponEventTitle = deal.EventTitle,
                        Introduction = NoHTML(contentLite.Introduction) + "\r\n" + GetPponBaseIntroduction(deal),
                        IntroductionTags = introductionTags,
                        Restrictions = NoHTML(contentLite.Restrictions) + "\r\n" + GetPponBaseIntroduction(deal),
                        Description = ViewPponDealManager.DefaultManager.GetDealAppDescription(deal),
                        Availabilities = stores,
                        SoldNum = (deal.BusinessHourOrderTimeS >= _config.StopRefundQuantityCalculateDate) ? qa.IncludeRefundQuantity : qa.Quantity,
                        SoldNumShow = (deal.BusinessHourOrderTimeS >= _config.StopRefundQuantityCalculateDate) ? qa.IncludeRefundQuantity : qa.Quantity,
                        ShowUnit = qa.IsAdjusted ? @"已售出\d份" : @"\d人已購買",
                        OriPrice = deal.ItemOrigPrice,
                        Price = deal.ItemPrice,

                        DealEndTime = DateTimeToDateTimeString(deal.BusinessHourOrderTimeE),
                        DealStartTime = DateTimeToDateTimeString(deal.BusinessHourOrderTimeS),
                        SoldOut = deal.OrderedQuantity >= deal.OrderTotalLimit, //訂單總銷售數量大於上限設為已售完
                        Remark = string.IsNullOrEmpty(contentLite.Remark) == true ? string.Empty : NoHTML(contentLite.Remark.Replace("●", "• ").Split(new[] { "||" }, StringSplitOptions.None)[2]),
                        MultiDeal = multiDeal,
                        ShoppingCart = shoppingCart,
                        DefaultActivityName = defaultActivityName,
                        TravelPlace = travelPlace,
                        DealTags = GetDealTags(deal, 2),
                        //是否顯示全家兌換價的處理
                        IsFami = ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0),

                        ShowExchangePrice = ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0),
                        ExchangePrice = deal.ExchangePrice.HasValue ? deal.ExchangePrice.Value : 0,
                        IsCollected = isCollected,
                        DiscountCodeUsed = PromotionFacade.GetDiscountUseType(deal.BusinessHourGuid),
                        AtmAvailable = PaymentFacade.CtAtmCheck(deal),
                        PiinlifeMenuTabs = ApiPiinlifeMenuTabForApiGet(deal, isNewPrototype),
                        PiinlifeRightContent = GetPiinlifeRightContent(deal),
                        IsExperience = deal.IsExperience,
                        ExchangeDealCount = string.Format("{0}人 已兌換", string.IsNullOrEmpty(orderedQuantity) ? "0" : orderedQuantity),
                        ExperienceDealCollectCount = string.Format("{0}人 已收藏", string.IsNullOrEmpty(collectDealCount) ? "0" : collectDealCount),
                        DiscountType = (int)DiscountType.None,
                        Discount = 0,

                        DisplayPrice = discountStringModel.DisplayPrice,
                        DiscountString = discountStringModel.DiscountString,
                        DiscountDisplayType = discountStringModel.DiscountDisplayType,

                        BehaviorType = (int)GetDealBehaviorType(deal)
                    };
                }

                rtnDeal.DeliveryType = deal.DeliveryType ?? 0;
                rtnDeal.FreightAmount = deal.FreightAmount;
                rtnDeal.EnableShipByIsp = deal.EnableIsp;
                rtnDeal.RelatedTags = deal.Tags.ToList();
                rtnDeal.DiscountPrice = (_config.EnableDiscountPrice == false || deal.DiscountPrice == null)
                    ? (decimal?)null : decimal.Truncate(deal.DiscountPrice.Value);
                rtnDeal.Categories = deal.CategoryIds;

                return rtnDeal;
            }
            return null;
        }

        /// <summary>
        /// 判斷檔次是否隱藏於APP
        /// </summary>
        /// <param name="bGuid"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static bool HiddenInApp(Guid bGuid, int? cityId = null)
        {
            return PponFacade.IsHiddenDeal(bGuid, cityId);
        }

        /// <summary>
        /// 是否隱藏於APP
        /// </summary>
        /// <param name="dealPreviewData"></param>
        /// <returns></returns>
        public static bool HiddenInApp(MultipleMainDealPreview dealPreviewData)
        {
            //dealtimeslot紀錄顯示為隱藏
            if (dealPreviewData.TimeSlotStatus == DealTimeSlotStatus.NotShowInPponDefault)
            {
                return true;
            }
            if (_config.EnableKindDealInApp == false &&
                (EntrustSellType)dealPreviewData.PponDeal.EntrustSell.GetValueOrDefault() == EntrustSellType.KindReceipt)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 是否顯示平均價
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        private static bool IsShowAveragePriceInfo(ViewComboDeal deal)
        {
            return (deal.IsAveragePrice && deal.QuantityMultiplier != null && deal.QuantityMultiplier >= 1);
        }

        /// <summary>
        /// 計算平均價
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        private static decimal CheckZeroPriceToShowAveragePrice(ViewComboDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                return deal.ItemPrice;
            }
            else
            {
                if (IsShowAveragePriceInfo(deal))
                {
                    return Math.Ceiling(deal.ItemPrice / deal.QuantityMultiplier.Value);
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
        }

        /// <summary>
        /// 回傳多檔次子檔的資料
        /// </summary>
        /// <param name="bid">主檔BID</param>
        /// <returns></returns>
        public static List<ApiComboDeal> ApiComboDealForApiGet(Guid bid)
        {
            var combodeals = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(bid);
            List<ApiComboDeal> rtnDeals = new List<ApiComboDeal>();

            foreach (ViewComboDeal combodeal in combodeals)
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(combodeal.BusinessHourGuid);
                if (deal.IsExpiredDeal.GetValueOrDefault(false))
                {
                    continue;
                }
                var couponDescription = deal.DeliveryType == (int)DeliveryType.ToShop ? (deal.EventName ?? string.Empty) : string.Empty;
                ApiComboDeal rtn = new ApiComboDeal();
                rtn.Bid = combodeal.BusinessHourGuid;
                rtn.Title = combodeal.Title;
                rtn.CouponDescription = couponDescription;
                rtn.SoldNum = combodeal.OrderedQuantity;
                rtn.IsGroupCoupon = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
                rtn.ItemOrigPrice = combodeal.ItemOrigPrice;
                rtn.Price = combodeal.ItemPrice;
                rtn.AveragePrice = CheckZeroPriceToShowAveragePrice(combodeal);
                rtn.DiscountString = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(combodeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), combodeal.ItemPrice, combodeal.ItemOrigPrice);
                rtn.DealEndTime = DateTimeToDateTimeString(combodeal.BusinessHourOrderTimeE);
                rtn.SoldOut = (combodeal.BusinessHourOrderTimeE < DateTime.Now) || combodeal.OrderedQuantity >= combodeal.OrderTotalLimit;
                rtn.ShoppingCart = deal.ShoppingCart != null && deal.ShoppingCart.Value;
                rtn.PiinlifeRightContent = GetPiinlifeRightContent(deal);
                rtn.IsShowAveragePrice = deal.IsAveragePrice ?? false;
                rtn.EnableShipByIsp = deal.EnableIsp;
                rtn.PponDeliveryType = deal.DeliveryType ?? 0;
                rtn.FreightAmount = deal.FreightAmount;
                rtnDeals.Add(rtn);
            }
            return rtnDeals;
        }

        /// <summary>
        /// 依據多檔次主檔或子檔BID，回傳包含主檔銷售份數或人數等資訊與子檔列表的物件(ApiComboDealMain)
        /// </summary>
        /// <param name="bid">主檔或子檔BID</param>
        /// <returns></returns>
        public static ApiComboDealMain ApiComboDealMainGetByMainDealGuid(Guid bid)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            if (!deal.IsLoaded)
            {
                return null;
            }

            if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.ComboDealSub) && deal.MainBid != null)
            {
                deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)deal.MainBid);
                if (!deal.IsLoaded)
                {
                    return null;
                }
            }

            QuantityAdjustment qa = (deal.BusinessHourOrderTimeE < DateTime.Now) ? deal.GetAdjustedSlug() : deal.GetAdjustedOrderedQuantity();

            ApiComboDealMain dealMain = new ApiComboDealMain();
            dealMain.MainDealGuid = deal.BusinessHourGuid;
            dealMain.IsAdjusted = qa.IsAdjusted;
            dealMain.SoldNumShow = (deal.BusinessHourOrderTimeS >= _config.StopRefundQuantityCalculateDate) ? qa.IncludeRefundQuantity : qa.Quantity;
            dealMain.ShowUnit = qa.IsAdjusted ? @"已售出\d份" : @"\d人已購買";
            dealMain.IsGroupCoupon = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
            dealMain.ComboDeals = ApiComboDealForApiGet(deal.BusinessHourGuid);

            return dealMain;
        }

        /// <summary>
        /// 取得結帳需要的商品資料
        /// </summary>
        /// <param name="bid">主檔BID</param>
        /// <param name="userName"></param>
        /// <param name="memberInfo"></param>
        /// <param name="message"></param>
        /// <param name="withBuilding"></param>
        /// <returns></returns>
        public static ApiPponDealCheckout ApiPponDealCheckoutDataGet(Guid bid, string userName,
            ExternalMemberInfo memberInfo, bool isForcePass, out string message, bool withBuilding)
        {
            var apiPponDealCheckout = new ApiPponDealCheckout();
            message = string.Empty;
            bool isTmall = userName == _sysConfProvider.TmallDefaultUserName;

            #region 會員資料與檔次資料基本驗證

            Member mem = _memberProvider.MemberGet(userName);
            if (!mem.IsLoaded)
            {
                message = "會員不存在。";
                return null;
            }

            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            //是否為公益檔，公益檔會關掉 紅利金、購物金、折扣券、ATM等付款選項
            bool isKindDeal = theDeal.GroupOrderStatus != null && Helper.IsFlagSet(theDeal.GroupOrderStatus.Value, GroupOrderStatus.KindDeal);
            apiPponDealCheckout.IsKindDeal = isKindDeal;

            //是否為銀行限定檔，同公益檔會關掉 紅利金、購物金、折扣券、ATM等付款選項
            bool isLimitBank = (theDeal.BankId ?? 0) > 0;

            apiPponDealCheckout.Bid = theDeal.BusinessHourGuid;
            apiPponDealCheckout.ShoppingCart = false;
            if (theDeal.ShoppingCart.HasValue)
            {
                apiPponDealCheckout.ShoppingCart = theDeal.ShoppingCart.Value;
            }
            PponDealStage pponDealStage = theDeal.GetDealStage();

            if (pponDealStage == PponDealStage.RunningAndFull)
            {
                message = "此檔好康已銷售一空喔。";
                return null;
            }
            else if (pponDealStage != PponDealStage.Running && pponDealStage != PponDealStage.RunningAndOn)
            {
                message = "此檔無法購買。";
                return null;
            }
            else if ((theDeal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
            {
                message = "代表檔無法購買。";
                return null;
            }

            if (Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb)
                && GameFacade.HasGameDealViewPermission(mem.UniqueId, theDeal.BusinessHourGuid) == false
                && !isForcePass)
            {
                message = "此檔無法購買。";
                return null;
            }

            #endregion 會員資料與檔次資料基本驗證

            #region 購物車

            if (theDeal.ShoppingCart.HasValue && theDeal.ShoppingCart.Value)
            {
                apiPponDealCheckout.ShoppingCart = true;
                apiPponDealCheckout.ComboPackCount = theDeal.ComboPackCount == null ? 1 : theDeal.ComboPackCount.Value;
            }
            else
            {
                apiPponDealCheckout.ShoppingCart = false;
                //非購物車，預設為1
                apiPponDealCheckout.ComboPackCount = 1;
            }

            #endregion 購物車

            #region 全家活動

            apiPponDealCheckout.IsFami = (theDeal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0;

            #endregion 全家活動

            #region 會員基本資料

            apiPponDealCheckout.FirstName = mem.FirstName;
            apiPponDealCheckout.LastName = mem.LastName;
            apiPponDealCheckout.Mobile = mem.Mobile;
            apiPponDealCheckout.UserEmail = mem.UserEmail;
            //記憶信用卡列表
            apiPponDealCheckout.MemberCreditCardList = MemberFacade.GetMemberCreditCardList(mem.UniqueId);

            #endregion 會員基本資料

            #region 檢查是否無法配送離島

            apiPponDealCheckout.NotDeliveryIslands = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands);

            #endregion 檢查是否無法配送離島

            #region 取得會員payCash金額

            //取得會員payCash金額
            if (memberInfo != null && !string.IsNullOrEmpty(memberInfo[SingleSignOnSource.PayEasy]))
            {
                apiPponDealCheckout.UserPcash = PCashWorker.CheckUserRemaining(memberInfo[SingleSignOnSource.PayEasy]);
            }
            else
            {
                // the user has not signed-in using pez account
                MemberLink l = _memberProvider.MemberLinkGet(MemberFacade.GetUniqueId(userName), SingleSignOnSource.PayEasy);
                if (l != null && l.IsLoaded)
                {
                    apiPponDealCheckout.UserPcash = PCashWorker.CheckUserRemaining(l.ExternalUserId);
                }
            }

            #endregion 取得會員payCash金額

            #region 商品選項

            var categories = new List<ApiPponOptionCategory>();
            if (ProposalFacade.IsVbsProposalNewVersion() && theDeal.IsHouseDealNewVersion())
            {
                AccessoryGroupCollection viagc = PponFacade.AccessoryGroupGetByHouseDeal(theDeal);
                foreach (AccessoryGroup ag in viagc)
                {
                    var category = new ApiPponOptionCategory();
                    category.CategoryGuid = ag.Guid;
                    category.Options = new List<ApiPponOption>();
                    category.CategoryName = ag.AccessoryGroupName;

                    foreach (ViewItemAccessoryGroup vig in ag.members)
                    {
                        var option = new ApiPponOption();
                        option.OptionGuid = vig.AccessoryGroupMemberGuid;
                        option.OptionName = vig.AccessoryName;
                        option.Quantity = vig.Quantity == null ? ApiPponOption.DefaultQuantity : ((vig.Quantity.Value < 0) ? 0 : vig.Quantity.Value);
                        category.Options.Add(option);
                    }
                    categories.Add(category);
                }
            }
            else
            {
                AccessoryGroupCollection viagc = _itemProvider.AccessoryGroupGetListByItem(theDeal.ItemGuid);
                foreach (AccessoryGroup ag in viagc)
                {
                    var category = new ApiPponOptionCategory();
                    category.CategoryGuid = ag.Guid;
                    category.Options = new List<ApiPponOption>();
                    category.CategoryName = ag.AccessoryGroupName;

                    ViewItemAccessoryGroupCollection vigc = _itemProvider.ViewItemAccessoryGroupGetList(theDeal.ItemGuid, ag.Guid);
                    foreach (ViewItemAccessoryGroup vig in vigc)
                    {
                        var option = new ApiPponOption();
                        option.OptionGuid = vig.AccessoryGroupMemberGuid;
                        option.OptionName = vig.AccessoryName;
                        option.Quantity = vig.Quantity == null ? ApiPponOption.DefaultQuantity : ((vig.Quantity.Value < 0) ? 0 : vig.Quantity.Value);
                        category.Options.Add(option);
                    }
                    categories.Add(category);
                }
            }
            apiPponDealCheckout.OptionCategories = categories;

            #endregion 商品選項

            #region 免運費資訊

            // 階梯式運費
            CouponFreightCollection freights = _pponProvider.CouponFreightGetList(theDeal.BusinessHourGuid, CouponFreightType.Income);
            foreach (CouponFreight couponFreight in freights.OrderBy(x => x.StartAmount))
            {
                var apiFreight = new ApiFreight();
                apiFreight.StartAmount = couponFreight.StartAmount;
                apiFreight.EndAmount = couponFreight.EndAmount;
                apiFreight.FreightAmount = couponFreight.FreightAmount;

                apiPponDealCheckout.FreightList.Add(apiFreight);
            }

            #endregion 免運費資訊

            #region 出貨選項

            apiPponDealCheckout.DeliveryOptions = GetDeliveryOption(theDeal);

            #endregion

            #region ATM

            if (isKindDeal || isLimitBank)
            {
                apiPponDealCheckout.ATMAvailable = false;
            }
            else
            {
                apiPponDealCheckout.ATMAvailable = PaymentFacade.CtAtmCheck(bid);
            }

            #endregion ATM

            #region 商品券成套販售

            apiPponDealCheckout.IsGroupCoupon = _sysConfProvider.IsGroupCouponOn && Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
            apiPponDealCheckout.GroupCouponInfo = PponFacade.GetApiGroupCouponInfoV2(theDeal);

            #endregion

            #region MasterPass

            if (PaymentFacade.CheckUsingMasterPass(theDeal))
            {
                var preCheckout = _orderProvider.MasterPassPreCheckoutTokenGet(mem.UniqueId);
                if (preCheckout.IsLoaded && preCheckout.IsUsed == false)
                {
                    apiPponDealCheckout.IsBindingMasterPass = true;
                }
            }

            #endregion MasterPass

            #region 會員的17life紅利、購物金等

            if (isKindDeal || isLimitBank || isTmall)
            {
                apiPponDealCheckout.UserBcash = 0;
                apiPponDealCheckout.UserScash = 0;
            }
            else
            {
                double userBcash = MemberFacade.GetAvailableMemberPromotionValue(userName);
                double userBcashAmt = userBcash / 10; //紅利與現金的轉換 10紅利:1新台幣
                if (userBcashAmt < 1)
                {
                    userBcashAmt = 0;
                }
                apiPponDealCheckout.UserBcash = userBcashAmt;

                decimal userScash;
                decimal userPscash;
                apiPponDealCheckout.UserScash = OrderFacade.GetSCashSum2(mem.UniqueId, out userScash, out userPscash);
            }

            #endregion

            #region 搜尋分店資料

            //通用券檔次無須選擇分店 故不組分店資料
            if (!Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore))
            {
                var stores = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(bid, VbsRightFlag.VerifyShop)
                                                    .OrderBy(x => x.SortOrder);
                foreach (ViewPponStore viewPponStore in stores)
                {
                    ApiPponStoreOption storeOption = new ApiPponStoreOption();
                    storeOption.StoreGuid = viewPponStore.StoreGuid;
                    storeOption.StoreName = viewPponStore.StoreName;
                    storeOption.CityId = viewPponStore.CityId == null ? 0 : viewPponStore.CityId.Value;
                    storeOption.TownshipId = viewPponStore.TownshipId == null ? 0 : viewPponStore.TownshipId.Value;
                    storeOption.AddressDesc = viewPponStore.AddressString;
                    storeOption.CompleteAddress = viewPponStore.CityName + viewPponStore.TownshipName + viewPponStore.AddressString;
                    storeOption.TotalQuantity = viewPponStore.TotalQuantity == null ? 0 : viewPponStore.TotalQuantity.Value;
                    storeOption.OrderedQuantity = viewPponStore.OrderedQuantity == null ? 0 : ((viewPponStore.OrderedQuantity.Value < 0) ? 0 : viewPponStore.OrderedQuantity.Value);
                    storeOption.HaveLimit = viewPponStore.TotalQuantity != null;//有設定上限值表示有控制銷售數量
                    //取出分店座標
                    SqlGeography storeGeo = LocationFacade.GetGeographyByCoordinate(viewPponStore.Coordinate);
                    storeOption.Coordinates = ApiCoordinates.FromSqlGeography(storeGeo);

                    apiPponDealCheckout.Stores.Add(storeOption);
                }
            }

            #endregion 搜尋分店資料

            #region 購買人資料

            apiPponDealCheckout.LastMemberDelivery = GetLastMemberDelivery(mem.UniqueId);

            if (mem.BuildingGuid.HasValue && !Guid.Equals(MemberUtilityCore.DefaultBuildingGuid, mem.BuildingGuid.Value))
            {
                Building bu = _locationProvider.BuildingGet(mem.BuildingGuid.Value);
                City area = CityManager.TownShipGetById(bu.CityId);
                City city = CityManager.CityGetById(area.ParentId.Value);
                string address = string.Empty;
                if (!string.IsNullOrEmpty(mem.CompanyAddress))
                {
                    address = mem.CompanyAddress.Replace(city.CityName, string.Empty).Replace(area.CityName, string.Empty);
                }

                if (!string.IsNullOrEmpty(bu.BuildingStreetName))
                {
                    address = address.Replace(bu.BuildingStreetName, string.Empty);
                }

                apiPponDealCheckout.MemberAddress.CityId = city.Id;
                apiPponDealCheckout.MemberAddress.TownshipId = area.Id;
                apiPponDealCheckout.MemberAddress.BuildingGuid = mem.BuildingGuid.Value;
                apiPponDealCheckout.MemberAddress.AddressDesc = address;
            }

            #endregion 購買人資料

            apiPponDealCheckout.MultipleBranch = (bool)theDeal.MultipleBranch;

            //是否顯示DiscountCode的顯示畫面
            apiPponDealCheckout.DiscountCodeUsed = !isTmall && PromotionFacade.GetDiscountUseTypeLite(theDeal.BusinessHourGuid);
            apiPponDealCheckout.InvoiceVersion = InvoiceVersion.CarrierEra;

            #region 計算使用者可購買最大數量

            int qty = theDeal.MaxItemCount ?? 10;
            string canNotBuyMessage = "";

            if (!isTmall)
            {
                //此會員本檔次已購買數。
                int alreadyboughtcount = _pponProvider.ViewPponOrderDetailGetCountByUser(userName, bid,
                    (bool)theDeal.IsDailyRestriction);

                if (theDeal.ItemDefaultDailyAmount != null)
                {
                    if (alreadyboughtcount < theDeal.ItemDefaultDailyAmount.Value)
                    {
                        int stillcanbuycount = theDeal.ItemDefaultDailyAmount.Value - alreadyboughtcount;
                        qty = qty >= stillcanbuycount ? stillcanbuycount : qty;
                    }
                    else
                    {
                        qty = 0;
                        canNotBuyMessage =
                            string.Format(
                                (bool)theDeal.IsDailyRestriction
                                    ? I18N.Message.DailyRestrictionLimitForApp
                                    : I18N.Message.AlreadyboughtcountForApp, alreadyboughtcount);
                    }
                }
                #region test code
                int qty2 = 0;
                string canNotBuyMessage2 = string.Empty;
                PponBuyFacade.CheckAlreadyBoughtRestriction(mem.UniqueId, theDeal, out qty2, out canNotBuyMessage2);
                if (qty != qty2 || canNotBuyMessage != canNotBuyMessage2)
                {
                    logger.WarnFormat("提醒 unicorn ，CheckAlreadyBoughtRestriction 不符合預期 {0},{1},{2},{3}",
                        qty, qty2, canNotBuyMessage, canNotBuyMessage2);
                }
                #endregion
            }
            apiPponDealCheckout.DealUserCanBuy = qty;
            apiPponDealCheckout.CanNotBuyMessage = canNotBuyMessage;

            #endregion 計算使用者可購買最大數量

            apiPponDealCheckout.PponDepartment = (DepartmentTypes)theDeal.Department;
            apiPponDealCheckout.PponDeliveryType = theDeal.DeliveryType == null ? DeliveryType.ToShop : (DeliveryType)theDeal.DeliveryType.Value;

            #region 宅配檔次查詢聯絡方式

            if (apiPponDealCheckout.PponDeliveryType == DeliveryType.ToHouse)
            {
                string msg;
                var memberDeliveries = ApiMemberDeliveryGetByAddressee(userName, out msg, withBuilding);
                if (memberDeliveries.Any())
                {
                    apiPponDealCheckout.MemberDeliveries.AddRange(memberDeliveries);
                }
                /*ViewMemberDeliveryBuildingCityCollection Receivers = _memberProvider.ViewMemberDeliveryBuildingCityGetCollection(userName);
                foreach (var memberDelivery in Receivers)
                {
                    if (memberDelivery.ParentId != null)
                    {
                        ApiMemberDelivery apiMemberDelivery = new ApiMemberDelivery();
                        apiMemberDelivery.DeliveryId = memberDelivery.Id;
                        apiMemberDelivery.AddressAlias = memberDelivery.AddressAlias ?? string.Empty;
                        apiMemberDelivery.ContactName = memberDelivery.ContactName;
                        apiMemberDelivery.Mobile = memberDelivery.Mobile;

                        if (!string.IsNullOrEmpty(memberDelivery.Address))
                        {
                            apiMemberDelivery.Address.CityId = memberDelivery.ParentId.Value;
                            apiMemberDelivery.Address.TownshipId = memberDelivery.CityId;
                            apiMemberDelivery.Address.BuildingGuid = memberDelivery.BuildingGuid;
                            if (withBuilding)
                            {
                                apiMemberDelivery.Address.AddressDesc =
                                    memberDelivery.Address.Replace(
                                        CityManager.CityTownShopBuildingStringGet(memberDelivery.BuildingGuid), string.Empty);
                            }
                            else
                            {
                                apiMemberDelivery.Address.AddressDesc = memberDelivery.Address.Replace(
                                    CityManager.CityTownShopStringGet(memberDelivery.CityId), string.Empty);
                            }
                        }

                        apiPponDealCheckout.MemberDeliveries.Add(apiMemberDelivery);
                    }
                }*/
            }

            #endregion

            apiPponDealCheckout.DealName = GetAppDealName(theDeal);
            apiPponDealCheckout.Price = theDeal.ItemPrice;
            apiPponDealCheckout.NoInvoiceCreate = false;

            if (isKindDeal || isLimitBank || isTmall)
            {
                return apiPponDealCheckout;
            }

            //DateTime now = DateTime.Now;

            #region 會員擁有的折價券資料

            var dc = GetCheckoutDataApiDiscountCodes2(bid, mem.UniqueId, mem.UserName);
            apiPponDealCheckout.DiscountCodes = dc;

            #endregion

            //LogManager.GetLogger(LineAppender._LINE)
            //    .InfoFormat("會員擁有的折價券資料 {0}", (DateTime.Now - now).TotalSeconds.ToString("0.00"));

            return apiPponDealCheckout;
        }
        /// <summary>
        /// 取得會員歸戶且可使用的折價券，並轉換格式給App呼叫使用
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static List<ApiDiscountCode> GetCheckoutDataApiDiscountCodes2(Guid bid, int userId, string userName)
        {
            List<ViewDiscountDetail> dcData =
                PromotionFacade.GetMemberDiscountList(userId, bid);

            var dc = new List<ApiDiscountCode>();

            foreach (var item in dcData)
            {
                if (item.Code == null)
                {
                    continue;
                }

                var isAppOnly = Helper.IsFlagSet(item.Flag, DiscountCampaignUsedFlags.AppUseOnly);
                var desc = PromotionFacade.GetDiscountCategoryString(item.Id, true, isAppOnly);

                var tmp = new ApiDiscountCode
                {
                    Amount = item.Amount ?? 0.0m,
                    StartTime = DateTimeToDateTimeString(item.StartTime.Value),
                    EndTime = DateTimeToDateTimeString(item.EndTime.Value),
                    Code = item.Code,
                    Used = item.UseTime != null,
                    Type = DiscountCampaignType.PponDiscountTicket,
                    MinimumAmount = item.MinimumAmount ?? 0,
                    ChannelRestrict = desc
                };

                dc.Add(tmp);
            }

            return dc.OrderBy(x => x.MinimumAmount).ToList();

        }

        public static List<ApiDiscountCode> GetCheckoutDataApiDiscountCodes(Guid bid, int userId, string userName)
        {
            //母檔BID
            Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(bid);

            //不可使用折價券就不做處理
            if (_op.DiscountLimitEnabledGetByBid(mainBid))
            {
                return new List<ApiDiscountCode>();
            }
            decimal grossMargin = PponFacade.GetMinimumGrossMarginFromBids(bid, true) * 100;
            //取得會員可用的折價券
            List<ViewDiscountDetail> dcData = _orderProvider.ViewDiscountDetailGetUnUsedList(
                userId, DiscountCampaignType.PponDiscountTicket).ToList();
            dcData = dcData.Where(x => grossMargin > x.MinGrossMargin || x.MinGrossMargin == null).ToList();

            var dc = new List<ApiDiscountCode>();

            foreach (var item in dcData)
            {
                if (item.Code == null)
                {
                    continue;
                }

                int flag;
                string msg;
                KeyValuePair<decimal, decimal> kv =
                    PromotionFacade.GetAmountCheckDepFromDiscountCode(item.Code.ToUpper(), userName
                        , (int)DiscountCampaignUsedFlags.Ppon, bid.ToString(), out flag, out msg);

                //負值為折價券狀態異常
                if (kv.Key < 0)
                {
                    continue;
                }

                var isAppOnly = Helper.IsFlagSet(item.Flag, DiscountCampaignUsedFlags.AppUseOnly);
                var desc = PromotionFacade.GetDiscountCategoryString(item.Id, true, isAppOnly);

                var tmp = new ApiDiscountCode
                {
                    Amount = item.Amount ?? 0.0m,
                    StartTime = DateTimeToDateTimeString(item.StartTime.Value),
                    EndTime = DateTimeToDateTimeString(item.EndTime.Value),
                    Code = item.Code,
                    Used = item.UseTime != null,
                    Type = DiscountCampaignType.PponDiscountTicket,
                    MinimumAmount = item.MinimumAmount ?? 0,
                    ChannelRestrict = desc
                };

                dc.Add(tmp);
            }

            return dc.OrderBy(x => x.MinimumAmount).ToList();

        }



        /// <summary>
        /// 舊版列表Model
        /// 依據 ViewPponDeal 與 DealProperty 產生APP使用的 PponDealSynopsis
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="categoryList"></param>
        /// <param name="seq"></param>
        /// <param name="dealType"></param>
        /// <param name="isNewPrototype"></param>
        /// <param name="ved"></param>
        /// <returns></returns>
        public static PponDealSynopsis PponDealSynopsisGetByDeal(IViewPponDeal deal, List<int> categoryList, int seq,
            VbsDealType dealType = VbsDealType.Ppon, bool isNewPrototype = false, ViewExternalDeal ved = null)
        {
            //計算顯示的已售份數資訊
            QuantityAdjustment qa = (deal.BusinessHourOrderTimeE < DateTime.Now) ? deal.GetAdjustedSlug() : deal.GetAdjustedOrderedQuantity();
            //處理顯示的檔次名稱
            string couponUsage = GetAppDealName(deal);
            string pureCouponUsage = couponUsage;
            //1.有設定旅遊地點，則顯示旅遊地點的資訊 
            //2.判斷是否為0元顯示優惠、全家顯示全家、宅配顯示宅配 
            //3.憑證檔 多個分店顯示多分店，單一分店顯示市區
            string travelPlace = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(null, deal.BusinessHourGuid);
            if (string.IsNullOrWhiteSpace(travelPlace) == false && _config.AppDealNameIncludeLocation)
            {
                couponUsage = string.Format("【{0}】{1}", travelPlace, couponUsage);
            }

            string[] spImgPaths = ImageFacade.GetMediaPathsFromRawData(deal.EventSpecialImagePath, MediaType.PponDealPhoto);

            #region ExternalDealTagType

            if (ved == null && _config.NotShowSkmDealsInWeb == false)
            {
                ved = ExternalDealFacade.GetViewExternalDeals(deal.BusinessHourGuid).FirstOrDefault();
            }

            List<int> externalDealTagType = new List<int>();
            string store = string.Empty;
            string department = string.Empty;
            if (ved != null)
            {
                externalDealTagType = new JsonSerializer().Deserialize<List<int>>(ved.TagList);
                store = ved.StoreName;
                department = ved.ShopName;
            }

            #endregion

            int behaviorType = (int)GetDealBehaviorType(deal);

            var discountStringModel = new DealDiscountStringModel();

            if (isNewPrototype == false)
            {
                discountStringModel.DiscountString = ViewPponDealManager.GetDealDiscountString(deal);
            }
            else
            {
                discountStringModel = ViewPponDealManager.GetDealDiscountStringByIViewPponDeal(deal);
            }

            //判斷是否為多檔次
            bool multiDeal = false;
            if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
            {
                multiDeal = true;
            }

            PponDealSynopsis synopses = new PponDealSynopsis()
            {
                Bid = deal.BusinessHourGuid,
                DealName = couponUsage,
                MiniDealName = pureCouponUsage,
                DealEventName = deal.EventName,
                DealStartTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourOrderTimeS),
                DealEndTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourOrderTimeE),
                ImagePath = PponFacade.GetPponDealFirstImagePath(deal.EventImagePath),
                SquareImagePath = (string.IsNullOrEmpty(deal.AppDealPic)) ? PponFacade.GetPponDealFirstImagePath(deal.EventImagePath) : PponFacade.GetPponDealFirstImagePath(deal.AppDealPic),
                Price = deal.ItemPrice,
                OriPrice = deal.ItemOrigPrice,
                SoldNum = (deal.BusinessHourOrderTimeS >= _config.StopRefundQuantityCalculateDate) ? qa.IncludeRefundQuantity : qa.Quantity,
                SoldNumShow = (deal.BusinessHourOrderTimeS >= _config.StopRefundQuantityCalculateDate) ? qa.IncludeRefundQuantity : qa.Quantity,
                ShowUnit = qa.IsAdjusted ? @"已售出\d份" : @"\d人已購買",
                SoldOut = deal.OrderedQuantity >= deal.OrderTotalLimit,
                TravelPlace = travelPlace,
                PromoImage = spImgPaths != null && spImgPaths.Length > 0 ? spImgPaths[0] : "",
                DealTags = GetDealTags(deal, 2, dealType),
                //是否顯示全家兌換價的處理
                IsFami = ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0),
                BehaviorType = behaviorType,
                ShowExchangePrice = ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0),
                ExchangePrice = deal.ExchangePrice.HasValue ? deal.ExchangePrice.Value : 0,
                Categories = categoryList ?? new List<int>(),
                Seq = seq,
                ExternalDealTagType = externalDealTagType,
                DiscountType = deal.DiscountType.GetValueOrDefault(),
                Discount = deal.DiscountValue.GetValueOrDefault(),
                Store = store,
                Department = department,
                IsMultiDeal = multiDeal,
                OriginalPrice = deal.ItemOrigPrice,
                DisplayPrice = discountStringModel.DisplayPrice,
                DiscountString = discountStringModel.DiscountString,
                DiscountDisplayType = discountStringModel.DiscountDisplayType,
                DeliveryType = deal.DeliveryType ?? 0,
                FreightAmount = deal.FreightAmount,
                EnableShipByIsp = deal.EnableIsp,
                DiscountPrice = (_config.EnableDiscountPrice == false || deal.DiscountPrice == null)
                    ? (decimal?)null : decimal.Truncate(deal.DiscountPrice.Value)
            };

            return synopses;
        }

        /// <summary>
        /// 新版列表Model
        /// PponDealSynopsis 轉 Basic
        /// </summary>
        /// <param name="pponDealSynopsis"></param>
        /// <returns></returns>
        public static PponDealSynopsisBasic PponDealSynopsisToBasic(PponDealSynopsis pponDealSynopsis)
        {
            PponDealSynopsisBasic basic = new PponDealSynopsisBasic();

            basic.Bid = pponDealSynopsis.Bid;
            basic.Location = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(null, pponDealSynopsis.Bid) ?? string.Empty;
            basic.DealName = _config.AppDealNameIncludeLocation ? pponDealSynopsis.DealName.Replace("【宅配】", "") : pponDealSynopsis.DealName;
            basic.ImagePath = pponDealSynopsis.ImagePath;
            basic.SquareImagePath = pponDealSynopsis.SquareImagePath;
            basic.SoldNum = pponDealSynopsis.SoldNum;
            basic.ShowUnit = pponDealSynopsis.ShowUnit;
            basic.DisplayPrice = pponDealSynopsis.DisplayPrice;
            basic.Price = pponDealSynopsis.Price;
            basic.OriginalPrice = pponDealSynopsis.OriPrice;
            basic.DiscountDisplayType = pponDealSynopsis.DiscountDisplayType;
            basic.DiscountString = pponDealSynopsis.DiscountString;
            basic.DealStartTime = pponDealSynopsis.DealStartTime;
            basic.DealEndTime = pponDealSynopsis.DealEndTime;
            basic.SoldOut = pponDealSynopsis.SoldOut;
            basic.DealTags = pponDealSynopsis.DealTags;
            basic.BehaviorType = pponDealSynopsis.BehaviorType;
            basic.Categories = pponDealSynopsis.Categories;
            basic.Seq = pponDealSynopsis.Seq;
            basic.IsMultiDeal = pponDealSynopsis.IsMultiDeal;
            basic.EveryDayNewDeal = _config.IsEveryDayNewDeals
                ? ViewPponDealManager.DefaultManager.GetEveryDayNewDealByBid(pponDealSynopsis.Bid) : EveryDayNewDeal.None;

            //轉換成新版列表的來源有很多，沒有評價資訊的再處理
            if (basic.DealEvaluate == null)
            {
                var evaluateStar = ViewPponDealManager.DefaultManager.GetPponDealEvaluateStarList(pponDealSynopsis.Bid);
                if (evaluateStar != null)
                {
                    basic.DealEvaluate = new DealEvaluate
                    {
                        EvaluateNumber = evaluateStar.Cnt,
                        EvaluateStar = evaluateStar.Star
                    };
                }
            }

            //轉換成新版列表的來源有很多，有的來源已處理優惠價有的未處理，無法清查完畢，所以再加一判斷
            if (pponDealSynopsis.DiscountDisplayType == 0 && string.IsNullOrEmpty(basic.DiscountString))
            {
                var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(pponDealSynopsis.Bid);
                var discountStringModel = ViewPponDealManager.GetDealDiscountStringByIViewPponDeal(deal);
                basic.DisplayPrice = discountStringModel.DisplayPrice;
                basic.DiscountString = discountStringModel.DiscountString;
                basic.DiscountDisplayType = discountStringModel.DiscountDisplayType;
            }

            basic.DeliveryType = pponDealSynopsis.DeliveryType;
            basic.FreightAmount = pponDealSynopsis.FreightAmount;
            basic.EnableShipByIsp = pponDealSynopsis.EnableShipByIsp;

            basic.DiscountPrice = pponDealSynopsis.DiscountPrice;

            return basic;
        }

        /// <summary>
        /// 新版列表Model
        /// MultipleMainDealPreview 轉 Basic
        /// </summary>
        /// <param name="previewDeal"></param>
        /// <param name="dealType"></param>
        /// <returns></returns>
        public static PponDealSynopsisBasic MultipleMainDealPreviewToBasic(MultipleMainDealPreview previewDeal, VbsDealType dealType = VbsDealType.Ppon)
        {
            var deal = previewDeal.PponDeal;

            //計算顯示的已售份數資訊
            QuantityAdjustment qa = (deal.BusinessHourOrderTimeE < DateTime.Now) ? deal.GetAdjustedSlug() : deal.GetAdjustedOrderedQuantity();

            //處理顯示的檔次名稱
            string couponUsage = GetAppDealName(deal);

            string travelPlace = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(null, deal.BusinessHourGuid);
            if (string.IsNullOrWhiteSpace(travelPlace) == false && _config.AppDealNameIncludeLocation)
            {
                couponUsage = string.Format("【{0}】{1}", travelPlace, couponUsage);
            }

            int behaviorType = (int)GetDealBehaviorType(deal);

            PponDealSynopsisBasic basic = new PponDealSynopsisBasic();
            basic.Bid = deal.BusinessHourGuid;
            basic.Location = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(null, deal.BusinessHourGuid) ?? string.Empty;
            basic.DealName = _config.AppDealNameIncludeLocation ? couponUsage.Replace("【宅配】", "") : couponUsage;
            basic.ImagePath = PponFacade.GetPponDealFirstImagePath(deal.EventImagePath);
            basic.SquareImagePath = string.IsNullOrEmpty(deal.AppDealPic)
                                    ? PponFacade.GetPponDealFirstImagePath(deal.EventImagePath)
                                    : PponFacade.GetPponDealFirstImagePath(deal.AppDealPic);
            basic.SoldNum = (deal.BusinessHourOrderTimeS >= _config.StopRefundQuantityCalculateDate) ? qa.IncludeRefundQuantity : qa.Quantity;
            basic.ShowUnit = qa.IsAdjusted ? @"已售出\d份" : @"\d人已購買";
            basic.Price = deal.ItemPrice;
            basic.OriginalPrice = deal.ItemOrigPrice;
            basic.DealStartTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourOrderTimeS);
            basic.DealEndTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourOrderTimeE);
            basic.SoldOut = deal.OrderedQuantity >= deal.OrderTotalLimit;
            basic.DealTags = GetDealTags(deal, 2, dealType);
            basic.BehaviorType = behaviorType;
            basic.Categories = previewDeal.DealCategoryIdList ?? new List<int>();
            basic.Seq = previewDeal.Sequence;
            basic.IsMultiDeal = (deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0;

            var discountStringModel = ViewPponDealManager.GetDealDiscountStringByIViewPponDeal(deal);
            basic.DisplayPrice = discountStringModel.DisplayPrice;
            basic.DiscountString = discountStringModel.DiscountString;
            basic.DiscountDisplayType = discountStringModel.DiscountDisplayType;

            basic.DeliveryType = deal.DeliveryType ?? 0;
            basic.FreightAmount = deal.FreightAmount;
            basic.EnableShipByIsp = deal.EnableIsp;
            basic.EveryDayNewDeal = _config.IsEveryDayNewDeals
                ? ViewPponDealManager.DefaultManager.GetEveryDayNewDealByBid(deal.BusinessHourGuid) : EveryDayNewDeal.None;

            var evaluateStar = ViewPponDealManager.DefaultManager.GetPponDealEvaluateStarList(deal.BusinessHourGuid);
            if (evaluateStar != null)
            {
                basic.DealEvaluate = new DealEvaluate
                {
                    EvaluateNumber = evaluateStar.Cnt,
                    EvaluateStar = evaluateStar.Star
                };
            }

            basic.DiscountPrice = (_config.EnableDiscountPrice == false || deal.DiscountPrice == null)
                    ? (decimal?)null : decimal.Truncate(deal.DiscountPrice.Value);

            return basic;
        }

        /// <summary>
        /// 取列表分頁
        /// </summary>
        /// <param name="list"></param>
        /// <param name="getAll"></param>
        /// <param name="startIndex">從0開始</param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public static List<T> PponDealSynopsisSkipSelect<T>(List<T> list, bool getAll, int startIndex, out int totalCount)
        {
            totalCount = list.Count;

            //getAll true=回傳全部筆數，false則從startIndex開始回傳，一次回傳的數量由server決定
            if (getAll == false) // && startIndex.Equals(0) == false
            {
                if (startIndex <= 0)
                {
                    list = list.Take(MaxLPerView).ToList();
                }
                else if (totalCount >= startIndex)
                {
                    list = list.Skip(startIndex).Take(MaxLPerView).ToList();
                }
                else
                {
                    list = new List<T>();
                }
            }
            return list;
        }
        public static List<PponDealSynopsisBasic> PponDealSynopsisBasicSkipSelect(List<PponDealSynopsisBasic> list, bool getAll, int startIndex)
        {
            int totalCount = 0;
            var result = PponDealSynopsisSkipSelect(list, getAll, startIndex, out totalCount);
            if (result == null)
            {
                result = new List<PponDealSynopsisBasic>();
            }
            return result;
        }

        /// <summary>
        /// 單檔明細
        /// </summary>
        /// <param name="apiPponDeal"></param>
        /// <returns></returns>
        public static ApiPponDealBasic ApiPponDealToApiPponDealSynopsisDetail(ApiPponDeal apiPponDeal)
        {
            if (apiPponDeal == null)
            {
                return null;
            }

            ApiPponDealBasic apiPponDealSynopsisDetail = new ApiPponDealBasic();

            #region DealListInfo
            apiPponDealSynopsisDetail.DealListInfo = new PponDealSynopsisBasic();
            apiPponDealSynopsisDetail.DealListInfo.Bid = apiPponDeal.Bid;
            apiPponDealSynopsisDetail.DealListInfo.DealName = _config.AppDealNameIncludeLocation ? apiPponDeal.DealName.Replace("【宅配】", "") : apiPponDeal.DealName;
            apiPponDealSynopsisDetail.DealListInfo.ImagePath = apiPponDeal.ImagePaths.FirstOrDefault();
            apiPponDealSynopsisDetail.DealListInfo.SquareImagePath = string.IsNullOrEmpty(apiPponDeal.AppDealPic) == true ? apiPponDeal.ImagePaths.FirstOrDefault() : apiPponDeal.AppDealPic;
            apiPponDealSynopsisDetail.DealListInfo.SoldNum = apiPponDeal.SoldNum;
            apiPponDealSynopsisDetail.DealListInfo.ShowUnit = apiPponDeal.ShowUnit;
            apiPponDealSynopsisDetail.DealListInfo.DisplayPrice = apiPponDeal.DisplayPrice;
            apiPponDealSynopsisDetail.DealListInfo.Price = apiPponDeal.Price;
            apiPponDealSynopsisDetail.DealListInfo.OriginalPrice = apiPponDeal.OriPrice;
            apiPponDealSynopsisDetail.DealListInfo.DiscountPrice = apiPponDeal.DiscountPrice;
            apiPponDealSynopsisDetail.DealListInfo.DiscountDisplayType = apiPponDeal.DiscountDisplayType;
            apiPponDealSynopsisDetail.DealListInfo.DiscountString = apiPponDeal.DiscountString;
            apiPponDealSynopsisDetail.DealListInfo.DealStartTime = apiPponDeal.DealStartTime;
            apiPponDealSynopsisDetail.DealListInfo.DealEndTime = apiPponDeal.DealEndTime;
            apiPponDealSynopsisDetail.DealListInfo.SoldOut = apiPponDeal.SoldOut;
            apiPponDealSynopsisDetail.DealListInfo.DealTags = apiPponDeal.DealTags;
            apiPponDealSynopsisDetail.DealListInfo.BehaviorType = apiPponDeal.BehaviorType;
            //apiPponDealSynopsisDetail.DealListInfo.Categories = apiPponDeal;
            apiPponDealSynopsisDetail.DealListInfo.IsMultiDeal = apiPponDeal.MultiDeal;

            apiPponDealSynopsisDetail.DealListInfo.DeliveryType = apiPponDeal.DeliveryType;
            apiPponDealSynopsisDetail.DealListInfo.FreightAmount = apiPponDeal.FreightAmount;
            apiPponDealSynopsisDetail.DealListInfo.EnableShipByIsp = apiPponDeal.EnableShipByIsp;
            apiPponDealSynopsisDetail.DealListInfo.Categories = apiPponDeal.Categories;

            #endregion

            apiPponDealSynopsisDetail.ImagePaths = apiPponDeal.ImagePaths;

            apiPponDealSynopsisDetail.CityName = apiPponDeal.TravelPlace;
            apiPponDealSynopsisDetail.CouponEventName = apiPponDeal.CouponEventTitle; //後來以橘標EventTitle取代
            var couponDescription = apiPponDeal.DeliveryType == (int)DeliveryType.ToShop ? apiPponDeal.CouponEventName : string.Empty; ;
            apiPponDealSynopsisDetail.CouponDescription = couponDescription; //黑標 EventName
            apiPponDealSynopsisDetail.IntroductionTags = apiPponDeal.IntroductionTags;

            string uri = HttpContext.Current.Request.Url.Authority;
            string action = _sysConfProvider.PponDescriptionTestEnabled ? "PponDescriptionAsync" : "PponDescription";

            apiPponDealSynopsisDetail.Introduction = string.Format("https://{0}/mvc/ppon/{1}?bid={2}&type={3}", uri, action, apiPponDeal.Bid, 2);
            apiPponDealSynopsisDetail.Description = string.Format("https://{0}/mvc/ppon/{1}?bid={2}&type={3}", uri, action, apiPponDeal.Bid, 1);
            apiPponDealSynopsisDetail.DetailDescription = string.Format(
                "https://{0}/mvc/ppon/{1}?bid={2}&type={3}&section={4}", uri, action, apiPponDeal.Bid, 1, (int)AppDescriptionSection.DetailDescription);

            CouponEventContent cec = _pponProvider.CouponEventContentGetByBid(apiPponDeal.Bid);
            if (!cec.IsLoaded || string.IsNullOrEmpty(cec.ProductSpec))
            {
                apiPponDealSynopsisDetail.ProductSpec = string.Empty;
            }
            else
            {
                apiPponDealSynopsisDetail.ProductSpec = string.Format(
                    "https://{0}/mvc/ppon/{1}?bid={2}&type={3}&section={4}", uri, action, apiPponDeal.Bid, 1, (int)AppDescriptionSection.ProductSpec);
            }

            apiPponDealSynopsisDetail.Remark = apiPponDeal.Remark;
            apiPponDealSynopsisDetail.IsShoppingCart = apiPponDeal.ShoppingCart;
            apiPponDealSynopsisDetail.IsCollected = apiPponDeal.IsCollected;
            apiPponDealSynopsisDetail.CanUseDiscountCode = apiPponDeal.DiscountCodeUsed;
            apiPponDealSynopsisDetail.CityName = apiPponDeal.TravelPlace;
            apiPponDealSynopsisDetail.PiinlifeMenuTabs = apiPponDeal.PiinlifeMenuTabs;
            apiPponDealSynopsisDetail.PiinlifeRightContent = apiPponDeal.PiinlifeRightContent;

            if (apiPponDeal.BehaviorType.Equals((int)BehaviorType.Skm) == true)
            {
                foreach (var item in apiPponDealSynopsisDetail.Availabilities)
                {
                    item.StoreName = SkmFacade.PrefixStoreName(item.StoreName);
                }
            }
            else
            {
                apiPponDealSynopsisDetail.Availabilities = apiPponDeal.Availabilities;
            }

            var evaluateStar = ViewPponDealManager.DefaultManager.GetPponDealEvaluateStarList(apiPponDeal.Bid);
            if (evaluateStar != null)
            {
                apiPponDealSynopsisDetail.DealListInfo.DealEvaluate = new DealEvaluate
                {
                    EvaluateNumber = evaluateStar.Cnt,
                    EvaluateStar = evaluateStar.Star
                };
            }

            #region 關聯檔次
            Proposal pro = _sellerProvider.ProposalGet(apiPponDeal.Bid);
            List<RelatedDealEntity> relatedDeals = new List<RelatedDealEntity>();
            if (pro.IsLoaded)
            {
                var prdList = ProposalFacade.GetRelatedDeal(pro.Id).Where(x => x.IsShow);
                relatedDeals.AddRange(prdList.Select(x => new RelatedDealEntity { Bid = x.BusinessHourGuid, DealName = ProposalFacade.GetProposalRelatedItemName(x) }));
            }

            apiPponDealSynopsisDetail.RelatedDeals = relatedDeals;

            #endregion 關聯檔次

            apiPponDealSynopsisDetail.RelatedTags = apiPponDeal.RelatedTags;

            return apiPponDealSynopsisDetail;
        }

        /// <summary>
        /// 依Bid取得策展編號及圖示
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static List<DealRelatedCuration> GetCurationRelayPageByBid(Guid bid)
        {
            var dealCurations = new List<DealRelatedCuration>();
            var brandIdList = _pponProvider.GetViewBrandItemListByBid(bid).Select(x => x.MainId).ToList();
            if (brandIdList.Any())
            {
                foreach (var brandId in brandIdList)
                {
                    var eventInfo = MarketingFacade.GetApiEventPromoOrBrandByIdFromCache(brandId, (int)EventPromoEventType.CurationTwo);
                    if (eventInfo != null && DateTime.Now >= eventInfo.StartDate && DateTime.Now <= eventInfo.EndDate
                        && eventInfo.DiscountList.Any())  //檔次有參與策展活動且有策展折價券才加入相關策展物件
                    {
                        var dc = new DealRelatedCuration();
                        dc.Type = EventPromoEventType.CurationTwo;
                        dc.EventId = eventInfo.EventId;
                        dc.ToRelayPageImageUrl = eventInfo.RelayImageUrl;
                        dc.RelatedCurationImageUrl = eventInfo.MobileMainPicUrl;
                        dc.RelatedDealCount = eventInfo.TotalCount;
                        dealCurations.Add(dc);
                    }
                }
            }
            return dealCurations;
        }

        /// <summary>
        /// App檔次內頁說明顯示
        /// </summary>
        /// <param name="bid">檔次編號</param>
        /// <param name="type">1:Description  2:Introduction  3:piinLife</param>
        /// <param name="tab"></param>
        /// <param name="section">1:優惠方案 2: 詳細介紹 4: 商品規格 7: 全部</param> 
        /// <returns></returns>
        public static string PponDealDescription(Guid bid, int type, string tab, int section = 7)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
            string value = "";

            if (deal.BusinessHourGuid == bid)
            {
                switch (type)
                {
                    case 1:
                        string html = ProposalFacade.GetAppDescription(deal, deal.BusinessHourGuid, section);
                        value = ViewPponDealManager.RemoveSpecialHtmlForApp(html);
                        break;
                    case 2:
                        value = contentLite.Introduction +
                        ((deal.GroupOrderStatus.Value & (int)GroupOrderStatus.SKMDeal) > 0 ? string.Empty : "<br />" + GetPponBaseIntroduction(deal, true));
                        break;
                    case 3:
                        // 各項頁籤說明
                        string tag = string.Format("||{0}##", HttpUtility.UrlDecode(tab));

                        if (contentLite.Remark.Contains(tag))
                        {
                            value = contentLite.Remark.Substring(contentLite.Remark.IndexOf(tag) + tag.Length);
                            if (value.IndexOf("||") > -1)
                            {
                                value = value.Remove(value.IndexOf("||"));
                            }
                        }
                        break;
                }
            }

            return value;
        }

        /// <summary>
        /// 取得WEB交易成功明細底下，推薦檔次列表，取6筆
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="dealCount"></param>
        /// <param name="logicType"></param>
        /// <returns></returns>
        public static List<IViewPponDeal> GetPponDealRecommendDeal(Guid bid, int dealCount)//, out int logicType)
        {
            List<IViewPponDeal> recommendDeals = null;

            //logicType = _config.RecommendDealsLogicType;
            recommendDeals = PponDealPreviewManager.GetApiRecommendDeals(bid, dealCount, _config.RecommendDealsLogicType);

            //底下是意外處理
            if (recommendDeals.Count < dealCount)
            {
                Random rd = new Random();
                //推測發生的可能，就只有頻道下的檔次太少，底下只是防止真的有這樣的意外
                recommendDeals.AddRange(
                    ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true)
                        .OrderBy(t => rd.Next(100))
                        .Take(dealCount - recommendDeals.Count));
                for (int i = 0; i < dealCount - recommendDeals.Count; i++)
                {
                    //填自己當做推薦
                    recommendDeals.Add(ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true));
                }
            }


            return recommendDeals;
        }

        /// <summary>
        /// 取得APP檔次明細底下，推薦檔次列表，取4筆
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="dealCount"></param>
        /// <param name="logicType"></param>
        /// <returns></returns>
        public static List<PponDealSynopsisBasic> GetPponDealSynopsisBasicRecommendDeal(Guid bid, int dealCount, out int logicType)
        {
            List<IViewPponDeal> recommendDeals = null;

            logicType = _config.RecommendDealsLogicType;
            recommendDeals = PponDealPreviewManager.GetApiRecommendDeals(bid, dealCount, _config.RecommendDealsLogicType);

            //底下是意外處理
            if (recommendDeals.Count < dealCount)
            {
                Random rd = new Random();
                //推測發生的可能，就只有頻道下的檔次太少，底下只是防止真的有這樣的意外
                recommendDeals.AddRange(
                    ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true)
                        .OrderBy(t => rd.Next(100))
                        .Take(dealCount - recommendDeals.Count));
                for (int i = 0; i < dealCount - recommendDeals.Count; i++)
                {
                    //填自己當做推薦
                    recommendDeals.Add(ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true));
                }
            }

            List<PponDealSynopsisBasic> pponDealSynopsisBasicrecommendDeals = new List<PponDealSynopsisBasic>();
            foreach (IViewPponDeal vpd in recommendDeals)
            {
                PponDealSynopsis synopsis = PponDealSynopsisGetByDeal(vpd, null, 0, VbsDealType.Ppon, true);
                PponDealSynopsisBasic basicDeal = PponDealSynopsisToBasic(synopsis);
                pponDealSynopsisBasicrecommendDeals.Add(basicDeal);
            }

            return pponDealSynopsisBasicrecommendDeals;
        }

        /// <summary>
        /// 依據檔次內容與分店內容，產生
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="store"></param>
        /// <param name="coordinates"></param>
        /// <returns></returns>
        public static ApiPponDealMapData ApiPponDealMapDataGetByDeal(IViewPponDeal deal, ViewPponStore store, ApiCoordinates coordinates)
        {
            //處理顯示的檔次名稱
            string couponUsage = GetAppDealName(deal);
            //有設定旅遊地點，則顯示旅遊地點的資訊於檔次名稱
            if (_config.AppDealNameIncludeLocation)
            {
                if (!string.IsNullOrWhiteSpace(deal.TravelPlace))
                {
                    couponUsage = string.Format("【{0}】{1}", deal.TravelPlace, couponUsage);
                }
            }

            //取出分店座標
            SqlGeography storeGeo = LocationFacade.GetGeographyByCoordinate(store.Coordinate);
            if (storeGeo == null || storeGeo.IsNull || storeGeo.Lat.IsNull || storeGeo.Long.IsNull)
            {
                //無分店座標,回傳null
                return null;
            }
            #region 計算距離

            double distance = 0;
            string distanceDesc = string.Empty;
            //檢查有無傳入地理定位資訊
            SqlGeography userGeo = LocationFacade.GetGeographicPoint(coordinates);
            if (userGeo != null)
            {
                distance = LocationFacade.GetDistance(userGeo, storeGeo);
                distanceDesc = LocationFacade.GetDistanceDesc(distance);
            }
            #endregion 計算距離

            string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto);


            ApiPponDealMapData mapData = new ApiPponDealMapData()
            {
                Bid = deal.BusinessHourGuid,
                DealName = couponUsage,
                DealEventName = deal.EventName,
                DealEndTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourOrderTimeE),
                DiscountString = ViewPponDealManager.GetDealDiscountString(deal),
                ImagePath = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : "",
                Price = deal.ItemPrice,
                SoldOut = deal.OrderedQuantity >= deal.OrderTotalLimit,
                StoreName = store.StoreName,
                DistanceDesc = distanceDesc,
                Distance = distance,
                Coordinates = new ApiCoordinates((double)storeGeo.Long, (double)storeGeo.Lat)
            };
            return mapData;
        }

        /// <summary>
        /// 取得商家檔次列表
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static List<PponDealSynopsisBasic> GetAppDealsBySellerGuid(Guid sellerGuid)
        {
            List<PponDealSynopsisBasic> sellerDeals = new List<PponDealSynopsisBasic>();

            var bids = _pponProvider.BusinessHourGetBidsBySeller(sellerGuid);
            foreach (var bid in bids)
            {
                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                if (vpd != null && vpd.MainBid == vpd.BusinessHourGuid) //只取母檔
                {
                    PponDealSynopsis synopsis = PponDealSynopsisGetByDeal(vpd, null, 0, VbsDealType.Ppon, true);
                    PponDealSynopsisBasic basicDeal = PponDealSynopsisToBasic(synopsis);
                    sellerDeals.Add(basicDeal);
                }
            }

            return sellerDeals;
        }

        #endregion 檔次相關

        #region 會員訂單資料相關

        /// <summary>
        /// 新光訂單
        /// </summary>
        /// <param name="vpd"></param>
        /// <param name="dealProperty"></param>
        /// <returns></returns>
        public static SkmPponDeal CreateSkmOrderPponDeal(IViewPponDeal vpd, DealProperty dealProperty = null)
        {
            SkmPponDeal spd = new SkmPponDeal();
            spd.MainBid = vpd.MainBid ?? vpd.BusinessHourGuid;
            spd.BusinessHourGuid = vpd.BusinessHourGuid;
            spd.ApiDealName = GetAppDealName(vpd);

            //圖片
            var picUrl = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).FirstOrDefault();
            spd.PicUrl = string.IsNullOrEmpty(picUrl) ? (_config.SiteUrl + "/Themes/default/images/17Life/G2/skm/ppon-M1_pic_skm.png") : picUrl;
            spd.AppPicUrl = string.IsNullOrEmpty(vpd.AppDealPic)
                ? spd.PicUrl : ImageFacade.GetMediaPathsFromRawData(vpd.AppDealPic, MediaType.PponDealPhoto).FirstOrDefault();
            spd.ApiDealName = GetAppDealName(vpd);
            spd.AppPicUrl = string.IsNullOrEmpty(vpd.AppDealPic)
                ? spd.PicUrl : ImageFacade.GetMediaPathsFromRawData(vpd.AppDealPic, MediaType.PponDealPhoto).FirstOrDefault();

            //優惠類型
            spd.DiscountType = 0;
            spd.Discount = 0;
            spd.DealMemberCollectCount = 0;
            dealProperty = dealProperty ?? _pponProvider.DealPropertyGet(vpd.BusinessHourGuid);

            if (dealProperty != null && dealProperty.IsLoaded)
            {
                spd.DiscountType = dealProperty.DiscountType;
                spd.Discount = dealProperty.Discount;
                MemberCollectDealType dealType = dealProperty.IsExperience ?? false
                    ? MemberCollectDealType.Experience
                    : MemberCollectDealType.Coupon;

                spd.DealMemberCollectCount =
                    _memberProvider.MemberCollectDealCountByBidStatusType(
                        vpd.BusinessHourGuid, MemberCollectDealStatus.Collected, dealType);
            }

            //顯示金額
            spd.ItemOrigPrice = vpd.ItemOrigPrice;
            spd.ItemPrice = vpd.ItemPrice;
            spd.ItemDefaultDailyAmount = vpd.ItemDefaultDailyAmount;

            //體驗商品收藏次數
            var collectCount = spd.DealMemberCollectCount.ToString("#,#");
            spd.ExperienceDealCollectCount = string.Format("{0}人 已收藏", string.IsNullOrEmpty(collectCount) ? "0" : collectCount);
            spd.IsExperience = vpd.IsExperience;

            //已兌換人數
            string orderQuantity = vpd.GetAdjustedOrderedQuantity().Quantity.ToString("#,#");
            spd.ExchangeDealCount = string.Format("{0}人 已兌換", (string.IsNullOrEmpty(orderQuantity) ? "0" : orderQuantity));

            //可核銷櫃位
            spd.SkmAvailabilities = SkmCacheFacade.GetSkmPponStoreListFromCache(vpd.BusinessHourGuid);

            //清算檔
            spd.IsSettlementDeal = false;
            if (vpd.SettlementTime != null)
            {
                spd.IsSettlementDeal = true;
                spd.NextSettlementTime = DateTimeToDateTimeString((DateTime)vpd.SettlementTime);
            }

            return spd;
        }

        /// <summary>
        /// [v2.0] 取得某依訂單的資料
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="uniqueId"></param>
        /// <param name="newGroupCouponApi"></param>
        /// <param name="forSKMOrderAPI">新光用的版本</param>
        /// <returns></returns>
        public static ApiUserOrder ApiUserOrderGetByOrderGuid(Guid orderGuid, int uniqueId, bool newGroupCouponApi = true, bool forSKMOrderAPI = false)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("1" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            Order order = _orderProvider.OrderGet(orderGuid);

            //憑證資料
            ViewCouponListSequenceCollection couponList = _memberProvider.GetCouponListSequenceListByOid(orderGuid);
            builder.AppendLine("2" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            ViewCouponListMainCollection couponListMainList = _memberProvider.GetCouponListMainListByOrderID(uniqueId, order.OrderId);
            builder.AppendLine("3" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            //查無資料
            if (couponListMainList.Count == 0)
            {
                return null;
            }
            ViewCouponListMain couponListMain = couponListMainList.First();
            builder.AppendLine("4" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            OrderDetailCollection orderDetailList = _orderProvider.OrderDetailGetList(orderGuid);
            builder.AppendLine("5" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            CashTrustLogCollection cashTrustLogList = _memberProvider.CashTrustLogGetListByOrderGuid(orderGuid);
            builder.AppendLine("6" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            var classification = cashTrustLogList.First().OrderClassification;

            Guid bid = Guid.Empty;
            if (cashTrustLogList.Count > 0)
            {
                var firstCoupon = cashTrustLogList.First();
                if (firstCoupon.BusinessHourGuid == null)
                {
                    return null;
                }
                else
                {
                    bid = firstCoupon.BusinessHourGuid.Value;
                }
            }
            else
            {
                //沒有資料，回傳錯誤。
                return null;
            }
            builder.AppendLine("7" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            var exchangeInfo = _orderProvider.OrderReturnListGetListByType(orderGuid, (int)OrderReturnType.Exchange).ToList();
            builder.AppendLine("8" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            CtAtmRefund ctatmrefund = _orderProvider.CtAtmRefundGetLatest(orderGuid);
            builder.AppendLine("9" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            ApiUserOrder userOrder = new ApiUserOrder();

            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            if (!vpd.IsLoaded)
            {
                return null;
            }

            //是否符合Line回饋資格
            userOrder.LinePoint = new ApiLinePointsData()
            {
                IsShow = _oep.GetViewLineshopOrderinfoByOrderGuid(orderGuid)
                    .Any(p => p.LastExternalRsrc == _config.LineRsrc && Helper.IsFlagSet(p.OrderStatus,OrderStatus.Complete))
            };

            userOrder.OrderGuid = order.Guid;
            userOrder.OrderId = order.OrderId;
            userOrder.BusinessHourGuid = vpd.BusinessHourGuid;
            userOrder.MainBid = vpd.MainBid ?? vpd.BusinessHourGuid;

            if (classification == (int)OrderClassification.Skm)
            {
                userOrder.SkmPponDeal = CreateSkmOrderPponDeal(vpd);
            }

            userOrder.UseStartDate = DateTimeToDateTimeString(vpd.BusinessHourDeliverTimeS.Value);
            if (order.OrderSettlementTime == null)
            {
                if (vpd.ChangedExpireDate == null)
                {
                    userOrder.UseEndDate = DateTimeToDateTimeString(vpd.BusinessHourDeliverTimeE.Value);
                }
                else
                {
                    userOrder.UseEndDate = DateTimeToDateTimeString(vpd.ChangedExpireDate.Value);
                }
            }
            else
            {   //清算檔次使用期限記在Order.OrderSettlementTime
                userOrder.UseEndDate = DateTimeToDateTimeString((DateTime)order.OrderSettlementTime);
            }
            builder.AppendLine("10" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            userOrder.BuyDate = OrderFacade.IsIspPaymentOrder(order.OrderStatus) && order.DeliveryTime != null ? DateTimeToDateTimeString((DateTime)order.DeliveryTime)
                : DateTimeToDateTimeString(order.CreateTime);
            builder.AppendLine("11" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            userOrder.OrderDetailList = new List<ApiUserOrderDetail>();
            userOrder.Freight = 0; //運費先設為0

            Guid selectStoreGuid = Guid.Empty;
            foreach (var orderDetail in orderDetailList)
            {
                if (orderDetail.Status == (int)OrderDetailStatus.SystemEntry)
                {
                    //運費
                    if (orderDetail.Total != null)
                    {
                        userOrder.Freight += orderDetail.Total.Value;
                    }
                }
                else
                {
                    //有選擇分店
                    if ((selectStoreGuid == Guid.Empty) && (orderDetail.StoreGuid != null))
                    {
                        selectStoreGuid = orderDetail.StoreGuid.Value;
                    }
                    ApiUserOrderDetail userOrderDetail = new ApiUserOrderDetail();
                    userOrderDetail.OrderDetailGuid = orderDetail.Guid;
                    userOrderDetail.ItemName = OrderFacade.GetOrderDetailItemName(OrderDetailItemModel.Create(orderDetail, vpd.BusinessHourGuid)).Replace("&nbsp", "");
                    userOrderDetail.Price = newGroupCouponApi && (vpd.GroupCouponDealType ?? 0) == (int)GroupCouponDealType.AvgAssign ? orderDetail.ItemUnitPrice / (vpd.SaleMultipleBase ?? 1) : orderDetail.ItemUnitPrice;
                    userOrderDetail.Quantity = newGroupCouponApi && (vpd.GroupCouponDealType ?? 0) == (int)GroupCouponDealType.AvgAssign ? orderDetail.ItemQuantity * (vpd.SaleMultipleBase ?? 1) : orderDetail.ItemQuantity;

                    userOrder.OrderDetailList.Add(userOrderDetail);
                }
            }
            builder.AppendLine("12" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            bool isCompleted = !MemberFacade.ViewCouponListMainIsFilterType(couponListMain, CouponListFilterType.NotCompleted);//已完成付款
            builder.AppendLine("13" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            decimal totalAmount = 0, pcash = 0, scash = 0, pscash = 0, bcash = 0, creditCard = 0, atm = 0, discountAmount = 0, linePay = 0, taishinPay = 0;
            decimal familyIspAmount = 0, sevenIspAmount = 0;
            foreach (var ctl in cashTrustLogList)
            {
                if (ctl.ThirdPartyPayment == (int)ThirdPartyPayment.LinePay)
                {
                    linePay += ctl.Tcash;
                }
                else if (ctl.ThirdPartyPayment == (int)ThirdPartyPayment.TaishinPay)
                {
                    taishinPay += ctl.Tcash;
                }
                totalAmount += ctl.Amount;
                pcash += ctl.Pcash;
                scash += ctl.Scash;
                pscash += ctl.Pscash;
                bcash += ctl.Bcash;
                creditCard += ctl.CreditCard;
                atm += ctl.Atm;
                familyIspAmount += ctl.FamilyIsp;
                sevenIspAmount += ctl.SevenIsp;
                discountAmount += ctl.DiscountAmount;
            }
            builder.AppendLine("14" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            userOrder.TotalAmount = totalAmount;
            userOrder.PCashPay = pcash;
            userOrder.SCashPay = scash;
            userOrder.PSCashPay = pscash;
            userOrder.BCashPay = bcash;
            userOrder.CreditCardPay = creditCard;
            userOrder.ATMPay = atm;
            userOrder.FamilyIspAmount = familyIspAmount;
            userOrder.SevenIspAmount = sevenIspAmount;
            userOrder.DiscountCodePay = discountAmount;
            userOrder.IsCompleted = isCompleted;

            //超取付款訂單，有壓完成單就算有付款
            if (familyIspAmount + sevenIspAmount > 0)
            {
                userOrder.IsCompleted = Helper.IsFlagSet(order.OrderStatus, OrderStatus.Complete);
            }

            var famip = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
            builder.AppendLine("15" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            if (forSKMOrderAPI)
            {
                userOrder.CouponType = DealCouponType.SkmQrCode;
            }
            else
            {
                var ed = famip.GetFamilyNetEvent(couponListMain.BusinessHourGuid);
                builder.AppendLine("16" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
                if (ed != null && ed.Version == (int)FamilyBarcodeVersion.FamiSingleBarcode)
                {
                    userOrder.CouponType = DealCouponType.FamiSingleBarcode;  //全家一段條碼
                }
                else
                {
                    userOrder.CouponType = PponFacade.GetDealCouponType(couponListMain);
                }
            }
            builder.AppendLine("17" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            #region 全家 Pincode Barcode

            var famiBarcodeInfoDic = new Dictionary<int, FamiUserCouponBarcodeInfo>();
            if (userOrder.CouponType == DealCouponType.FamilyNetPincode || userOrder.CouponType == DealCouponType.FamiSingleBarcode)
            {
                famiBarcodeInfoDic = FamiGroupCoupon.GetUserCouponBarcodeInfoListByOrderGuid(order.Guid);
            }

            #endregion
            builder.AppendLine("18" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));

            #region atm處理

            //付款方式為ATM
            userOrder.AtmData = null;
            if ((order.OrderStatus & (int)LunchKingSite.Core.OrderStatus.ATMOrder) > 0)
            {
                ATMRemittanceData remittanceData = PaymentFacade.ATMRemittanceDataGetByOrderGuid(order.Guid);
                if (remittanceData != null)
                {
                    ApiOrderAtmData atmData = new ApiOrderAtmData();
                    atmData.BankName = remittanceData.BankName;
                    atmData.BankCode = remittanceData.BankCode;
                    atmData.BankAccount = remittanceData.BankAccount;
                    atmData.DeadlineDate = DateTimeToDateTimeString(remittanceData.RemittanceDeadline);
                    atmData.Amount = remittanceData.Amount;

                    userOrder.AtmData = atmData;
                }
            }

            #endregion atm處理
            builder.AppendLine("19" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));

            #region 付款資訊

            var payInfoList = new List<ApiPayInfo>();

            if (pcash != 0)
            {
                payInfoList.Add(new ApiPayInfo(I18N.Message.PCash, pcash));
            }
            if (scash != 0 || pscash != 0)
            {
                payInfoList.Add(new ApiPayInfo("購物金", 0, "購物金"));
            }
            if (scash != 0)
            {
                payInfoList.Add(new ApiPayInfo("17Life", scash, "購物金"));
            }
            if (pscash != 0)
            {
                payInfoList.Add(new ApiPayInfo(I18N.Message.PScash, pscash, "購物金"));
            }
            if (bcash != 0)
            {
                payInfoList.Add(new ApiPayInfo(I18N.Message.BonusPoint, bcash));
            }
            if (creditCard != 0)
            {
                payInfoList.Add(new ApiPayInfo(I18N.Message.CreditCard, creditCard));
            }
            if (atm != 0)
            {
                payInfoList.Add(new ApiPayInfo(I18N.Message.ATM, atm));
            }
            if (familyIspAmount != 0)
            {
                payInfoList.Add(new ApiPayInfo(I18N.Message.FamilyIsp, familyIspAmount));
            }
            if (sevenIspAmount != 0)
            {
                payInfoList.Add(new ApiPayInfo(I18N.Message.SevenIsp, sevenIspAmount));
            }
            if (linePay != 0)
            {
                payInfoList.Add(new ApiPayInfo(Helper.GetEnumDescription(ThirdPartyPayment.LinePay), linePay));
            }
            if (taishinPay != 0)
            {
                payInfoList.Add(new ApiPayInfo(Helper.GetEnumDescription(ThirdPartyPayment.TaishinPay), taishinPay));
            }
            if (discountAmount != 0)
            {
                payInfoList.Add(new ApiPayInfo(I18N.Message.Discount, discountAmount));
            }

            userOrder.PayInfoList = payInfoList;

            #endregion
            builder.AppendLine("10" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));

            #region 退貨明細

            //退貨狀況
            userOrder.RefundLogList = new List<ApiRefundLog>();

            IList<ReturnFormEntity> returnForms = ReturnFormRepository.FindAllByOrder(orderGuid);
            if (returnForms.Any())
            {
                userOrder.RefundLogList = GetRefundLogList(cashTrustLogList, returnForms, ctatmrefund);
            }

            #endregion 退貨明細
            builder.AppendLine("21" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));

            #region 換貨明細

            userOrder.ExchangeLogList = new List<ApiExchangeLog>();
            if (exchangeInfo.Any())
            {
                userOrder.ExchangeLogList = GetExchangeLogList(exchangeInfo);
            }

            #endregion 換貨明細
            builder.AppendLine("22" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));

            #region 出貨明細

            if (couponListMain.DeliveryType == (int)DeliveryType.ToHouse)
            {
                var shipInfo = OrderFacade.GetOrderShipInfo(couponListMain);
                var shipInfoList = OrderFacade.GetOrderShipInfoList(couponListMain);
                userOrder.LastShippingDate = shipInfo.LastShippingDate;
                if (!string.IsNullOrWhiteSpace(vpd.LabelIconList))
                {
                    string[] ss = vpd.LabelIconList.Split(',');
                    //24H到貨
                    if (ss.Contains(((int)DealLabelSystemCode.TwentyFourHoursArrival).ToString()))
                    {
                        userOrder.LastShippingDate = "24H到貨不適用";
                    }
                }
                userOrder.ActualShippingDate = shipInfo.ActualShippingDate;

                //ATM 未成單不顯示最後出貨日及出貨狀態
                if (Helper.IsFlagSet(order.OrderStatus, OrderStatus.ATMOrder) &&
                !Helper.IsFlagSet(order.OrderStatus, OrderStatus.Complete))
                {
                    userOrder.LastShippingDate = string.Empty;
                    shipInfo = null;
                }

                userOrder.ShipInfo = shipInfo;
                userOrder.ShipInfoList = shipInfoList;
            }

            #endregion
            builder.AppendLine("23" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));

            #region 分店

            //全家寄杯/萊爾富第一階段不顯示可使用分店
            if ((userOrder.CouponType != DealCouponType.FamilyNetPincode && userOrder.CouponType != DealCouponType.FamiSingleBarcode)
                && (_config.EnableHiLifeDealSetup && userOrder.CouponType != DealCouponType.HiLifePincode))
            {
                ViewPponStoreCollection pponStores;
                if (selectStoreGuid != null && selectStoreGuid != Guid.Empty)
                {
                    pponStores =
                        _pponProvider.ViewPponStoreGetListWithNoLock(ViewPponStore.Columns.BusinessHourGuid + " = " + bid,
                                                                     ViewPponStore.Columns.StoreGuid + " = " +
                                                                     selectStoreGuid);
                }
                else
                {
                    pponStores = _pponProvider.ViewPponStoreGetListByBidWithNoLock(bid, VbsRightFlag.Location);
                }

                //TODO: 訂單分店資訊加入經緯度
                //var availabilities = ProviderFactory.Instance().GetDefaultSerializer().Deserialize<AvailableInformation[]>(vpd.Availability).ToList();

                List<ApiPponStore> stores = new List<ApiPponStore>();
                foreach (var pponStore in pponStores)
                {
                    //LocationFacade.GetGeographyByCoordinate(x.Coordinate).Lat
                    //LocationFacade.GetGeographyByCoordinate(x.Coordinate).Long
                    //LocationFacade.GetLatLongitudeByGeography(string coordinate)

                    var store = new ApiPponStore
                    {
                        StoreName = pponStore.StoreName,
                        Phone = NoHTML(pponStore.Phone),
                        Address = pponStore.TownshipId == null ? "" : CityManager.CityTownShopStringGet(pponStore.TownshipId.Value) + pponStore.AddressString,
                        OpenTime = pponStore.OpenTime,
                        CloseDate = pponStore.CloseDate,
                        Remarks = pponStore.Remarks,
                        Mrt = pponStore.Mrt,
                        Car = pponStore.Car,
                        Bus = pponStore.Bus,
                        OtherVehicles = pponStore.OtherVehicles,
                        WebUrl = pponStore.WebUrl,
                        FbUrl = pponStore.FacebookUrl,
                        PlurkUrl = pponStore.PlurkUrl,
                        BlogUrl = pponStore.BlogUrl,
                        OtherUrl = pponStore.OtherUrl
                    };
                    stores.Add(store);
                }

                userOrder.Availabilities = stores;

            }

            #endregion 分店
            builder.AppendLine("24" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));

            userOrder.PponDeliveryType = vpd.DeliveryType == null
                                             ? DeliveryType.ToShop
                                             : (DeliveryType)vpd.DeliveryType;

            userOrder.DisableSMS = false;
            if (((vpd.GroupOrderStatus & (int)GroupOrderStatus.DisableSMS) > 0))
            {
                userOrder.DisableSMS = true;
            }

            //收件人資訊
            if (vpd.DeliveryType == (int)DeliveryType.ToHouse)
            {
                userOrder.AddresseeName = order.MemberName;
                userOrder.AddresseePhone = order.MobileNumber;
                userOrder.AddresseeAddress = order.DeliveryAddress;
            }

            //判斷檔次是否為0元好康
            userOrder.IsFreeDeal = totalAmount == 0;

            //是否全家活動
            userOrder.IsFami = (vpd.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0;

            userOrder.BehaviorType = (int)GetDealBehaviorType(vpd);
            builder.AppendLine("25" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            //是否為廠商提供序號要下載紙本之檔次
            userOrder.IsPaperDownload = (vpd.GroupOrderStatus & (int)GroupOrderStatus.PEZeventCouponDownload) > 0;

            #region 整理憑證資料

            //查看是否有送禮歷程
            var gift = _mgmp.GiftGetByOid(orderGuid).Where(x => x.IsActive == true).ToList();
            userOrder.CouponList = new List<ApiCouponData>();
            userOrder.SMSCount = 0;
            List<int> couponIdList = new List<int>();
            //付款完成才傳遞付款完成的資料
            builder.AppendLine("26" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            if (isCompleted)
            {
                int slug;
                if (!int.TryParse(vpd.Slug, out slug))
                {
                    slug = 0;
                }
                //取得退貨憑證
                CashTrustLogCollection returnFormCtlogs = _memberProvider.PponRefundingCashTrustLogGetListByOrderGuid(orderGuid);
                builder.AppendLine("27" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
                #region 處理 CouponList

                foreach (ViewCouponListSequence coupon in couponList)
                {

                    ApiCouponData apiCoupon = new ApiCouponData();

                    DateTime now = DateTime.Now;
                    apiCoupon.CouponId = coupon.Id;
                    apiCoupon.ItemName = coupon.ItemName.Replace("&nbsp", string.Empty);
                    apiCoupon.SequenceNumber = coupon.SequenceNumber;
                    apiCoupon.CouponCode = coupon.Code;
                    if (apiCoupon.SequenceNumber == coupon.Code && (vpd.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) == 0)
                    {
                        apiCoupon.CouponCode = string.Empty;
                        apiCoupon.CouponSeparateCode = string.Empty;


                        //是否為自訂簡訊格式  
                        if (vpd != null && vpd.CouponSeparateDigits > 0)
                        {
                            apiCoupon.CouponSeparateCode = CouponFacade.GetCouponSeparateCode(coupon.SequenceNumber, (int)vpd.CouponSeparateDigits);
                        }
                    }
                    if (classification == (int)OrderClassification.Skm)
                    {
                        ViewExternalDeal ved = _pponProvider.ViewExternalDealGetByBid(vpd.BusinessHourGuid);
                        if (ved.IsLoaded)
                        {
                            apiCoupon.ProductCode = ved.ProductCode;
                            apiCoupon.SpecialItemNo = ved.SpecialItemNo;
                        }
                    }

                    #region 三段式條碼

                    //三段式條碼須設定特別的憑證編號
                    if (userOrder.CouponType == DealCouponType.ThreeStageBarcode)
                    {
                        //全家檔次，一定只有一筆orderDetail
                        OrderDetail orderDetail = orderDetailList.First();
                        FamiposBarcode famipos = OrderFacade.FamiposBarcodeGetCreate(orderDetail.Guid, bid, vpd);
                        apiCoupon.ThreeStageCode.Barcode1 = famipos.Barcode1;
                        apiCoupon.ThreeStageCode.Barcode2 = famipos.Barcode2;
                        apiCoupon.ThreeStageCode.Barcode3 = famipos.Barcode3;
                    }

                    if (coupon.StoreSequence == null)
                    {
                        apiCoupon.StoreSeq = string.Empty;
                    }
                    else
                    {
                        apiCoupon.StoreSeq = coupon.StoreSequence.Value.ToString();
                    }

                    #endregion

                    #region 憑證狀態

                    if (coupon.BankStatus == 0)
                    {
                        apiCoupon.BankStatusDesc = "信託準備中";
                    }
                    else if (coupon.BankStatus == 1)
                    {
                        apiCoupon.BankStatusDesc = "已信託";
                    }
                    else if (coupon.BankStatus == 3)
                    {
                        apiCoupon.BankStatusDesc = "信託已核銷";
                    }

                    if (coupon.Status < 0)//no trust data
                    {
                        //沒有 cashTrustLog資料
                        if ((order.OrderStatus & (int)Core.OrderStatus.Cancel) > 0)
                        {
                            apiCoupon.CouponUsedType = ApiCouponUsedType.CouponRefund;
                            apiCoupon.CouponUsedTypeDesc = "已退貨";
                        }
                        else if (vpd.BusinessHourOrderTimeE < now)
                        {
                            //已結檔
                            if (slug >= vpd.BusinessHourOrderMinimum)
                            {
                                if (PponFacade.IsPastFinalExpireDate(coupon))
                                {
                                    apiCoupon.CouponUsedType = ApiCouponUsedType.CouponExpired;
                                    apiCoupon.CouponUsedTypeDesc = "憑證過期";
                                }
                                else
                                {
                                    apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                                    apiCoupon.CouponUsedTypeDesc = "未使用";
                                }
                            }
                            else
                            {
                                apiCoupon.CouponUsedType = ApiCouponUsedType.FailDeal;
                                apiCoupon.CouponUsedTypeDesc = "未達門檻";
                            }
                        }
                        //檔次未結束
                        else if ((vpd.BusinessHourOrderTimeS <= now) &&
                                 (vpd.BusinessHourOrderTimeE > now))
                        {
                            //目前銷售數量
                            int currentQuantity = vpd.OrderedQuantity == null ? 0 : vpd.OrderedQuantity.Value;

                            if (currentQuantity < vpd.BusinessHourOrderMinimum && currentQuantity != 0)
                            {
                                //未達門檻，販賣中
                                apiCoupon.CouponUsedType = ApiCouponUsedType.WaitToPass;
                                apiCoupon.CouponUsedTypeDesc = "";
                            }
                            else
                            {
                                apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                                apiCoupon.CouponUsedTypeDesc = "未使用";
                            }
                        }
                        else
                        {
                            apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                            apiCoupon.CouponUsedTypeDesc = "未使用";
                        }

                    }
                    else //coupon.Status > 0
                    {
                        if (Helper.IsFlagSet((long)coupon.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                        {
                            apiCoupon.CouponUsedType = ApiCouponUsedType.ATMRefund;
                            apiCoupon.CouponUsedTypeDesc = "ATM退款中";
                        }
                        else
                        {
                            //憑證未使用
                            if (CouponIsNotUsed((TrustStatus)coupon.Status))
                            {
                                //已結檔
                                if (vpd.BusinessHourOrderTimeE < now)
                                {
                                    if (slug >= vpd.BusinessHourOrderMinimum)
                                    {
                                        if (PponFacade.IsPastFinalExpireDate(coupon))
                                        {
                                            apiCoupon.CouponUsedType = ApiCouponUsedType.CouponExpired;
                                            apiCoupon.CouponUsedTypeDesc = "憑證過期";
                                        }
                                        else
                                        {
                                            apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                                            apiCoupon.CouponUsedTypeDesc = "未使用";
                                        }
                                    }
                                    else
                                    {
                                        apiCoupon.CouponUsedType = ApiCouponUsedType.FailDeal;
                                        apiCoupon.CouponUsedTypeDesc = "未達門檻";
                                    }
                                }
                                //檔次未結束
                                else if ((vpd.BusinessHourOrderTimeS <= now) &&
                                         (vpd.BusinessHourOrderTimeE > now))
                                {
                                    //檔次進行中，可以使用ViewPponDealManager來取得資料
                                    //ViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                                    //目前銷售數量
                                    int currentQuantity = vpd.OrderedQuantity == null
                                                              ? 0
                                                              : vpd.OrderedQuantity.Value;

                                    if (currentQuantity < vpd.BusinessHourOrderMinimum &&
                                        currentQuantity != 0)
                                    {
                                        //為達門檻
                                        apiCoupon.CouponUsedType = ApiCouponUsedType.WaitToPass;
                                        apiCoupon.CouponUsedTypeDesc = "";
                                    }
                                    else
                                    {
                                        apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                                        apiCoupon.CouponUsedTypeDesc = "未使用";
                                    }
                                }
                                else
                                {
                                    apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                                    apiCoupon.CouponUsedTypeDesc = "未使用";
                                }

                            }
                            else if ((TrustStatus)coupon.Status == TrustStatus.Verified)
                            {
                                apiCoupon.UsageVerifiedTime =
                                    coupon.UsageVerifiedTime != null ? ApiSystemManager.DateTimeToDateTimeString((DateTime)coupon.UsageVerifiedTime)
                                    : string.Empty;
                                apiCoupon.CouponUsedType = ApiCouponUsedType.Used;
                                apiCoupon.CouponUsedTypeDesc = "已使用";
                            }
                            else
                            {
                                //憑證cashTrustLog狀態不為 已核銷 或 未使用 類型的其他狀態
                                if ((order.OrderStatus & (int)Core.OrderStatus.Cancel) > 0)
                                {
                                    apiCoupon.CouponUsedType = ApiCouponUsedType.CouponRefund;
                                    apiCoupon.CouponUsedTypeDesc = "已退貨";
                                }
                                else if ((TrustStatus)coupon.Status != TrustStatus.ATM)
                                {
                                    apiCoupon.CouponUsedType = ApiCouponUsedType.WaitForRefund;
                                    apiCoupon.CouponUsedTypeDesc = "退貨中";
                                }
                            }
                        }
                    }

                    //處理coupon.Status=0但是已經辦理退貨中
                    CashTrustLog returningCtlog = returnFormCtlogs.FirstOrDefault(x => x.CouponId == coupon.Id);
                    if (returningCtlog != null && apiCoupon.CouponUsedType == ApiCouponUsedType.CanUsed)
                    {
                        apiCoupon.CouponUsedType = ApiCouponUsedType.WaitForRefund;
                        apiCoupon.CouponUsedTypeDesc = "退貨中";
                    }

                    //全家四段式條碼及鎖定狀態
                    var userCouponBarcodeInfo = new FamiUserCouponBarcodeInfo();
                    if (userOrder.CouponType == DealCouponType.FamilyNetPincode
                        || userOrder.CouponType == DealCouponType.FamiSingleBarcode)
                    {
                        if (famiBarcodeInfoDic.Any(x => x.Key == coupon.Id))
                        {
                            userCouponBarcodeInfo = famiBarcodeInfoDic[coupon.Id];
                        }
                    }

                    //全家寄杯憑證鎖定中
                    if (userCouponBarcodeInfo != null && userCouponBarcodeInfo.IsLock)
                    {
                        apiCoupon.CouponUsedType = ApiCouponUsedType.FamiLocked;
                        apiCoupon.CouponUsedTypeDesc = "資料處理中";
                    }

                    #endregion

                    #region 全家四段式條碼

                    //全家四段式條碼 (在已使用或憑證鎖定中時才顯示)
                    if (userCouponBarcodeInfo != null && userOrder.CouponType == DealCouponType.FamilyNetPincode
                        && (apiCoupon.CouponUsedType == ApiCouponUsedType.Used || apiCoupon.CouponUsedType == ApiCouponUsedType.FamiLocked))
                    {
                        if (apiCoupon.CouponUsedType == ApiCouponUsedType.FamiLocked && userCouponBarcodeInfo.LockTime != null)
                        {
                            apiCoupon.UsageVerifiedTime = ApiSystemManager.DateTimeToDateTimeString((DateTime)userCouponBarcodeInfo.LockTime);
                        }

                        var familyNetPincode = new FamilyNetPincode();
                        familyNetPincode.Pincode = userCouponBarcodeInfo.Pincode;
                        familyNetPincode.Barcode.Add(userCouponBarcodeInfo.Barcode1);
                        familyNetPincode.Barcode.Add(userCouponBarcodeInfo.Barcode2);
                        familyNetPincode.Barcode.Add(userCouponBarcodeInfo.Barcode3);
                        familyNetPincode.Barcode.Add(userCouponBarcodeInfo.Barcode4);
                        familyNetPincode.UsageStoreName = userCouponBarcodeInfo.StoreName;
                        familyNetPincode.UsageStoreAddress = userCouponBarcodeInfo.StoreAddress;
                        apiCoupon.FamilyNetPincode = familyNetPincode;
                    }

                    #region 全家一段式條碼

                    if (userCouponBarcodeInfo != null && userOrder.CouponType == DealCouponType.FamiSingleBarcode)
                    {
                        if (apiCoupon.CouponUsedType == ApiCouponUsedType.FamiLocked && userCouponBarcodeInfo.LockTime != null)
                        {
                            apiCoupon.UsageVerifiedTime = ApiSystemManager.DateTimeToDateTimeString((DateTime)userCouponBarcodeInfo.LockTime);
                        }

                        var familyNetPincode = new FamilyNetPincode();
                        familyNetPincode.Pincode = userCouponBarcodeInfo.Pincode;
                        familyNetPincode.Barcode.Add(userCouponBarcodeInfo.Barcode2);
                        familyNetPincode.UsageStoreName = userCouponBarcodeInfo.StoreName;
                        familyNetPincode.UsageStoreAddress = userCouponBarcodeInfo.StoreAddress;
                        apiCoupon.FamilyNetPincode = familyNetPincode;
                    }

                    #endregion

                    #endregion

                    # region 萊爾富兩段式條碼
                    if (_config.EnableHiLifeDealSetup && userOrder.CouponType == DealCouponType.HiLifePincode && apiCoupon.CouponUsedType == ApiCouponUsedType.Used)
                    {
                        apiCoupon.HiLifePincode = new HiLifePincode()
                        {
                            PincodeList = new List<string>()
                            {
                                _config.HiLifeFixedFunctionPincode,//第一段固定功能條碼
                                apiCoupon.SequenceNumber//peztemp.pezcode廠商匯入序號為第二段條碼
                            },
                            UsageStoreAddress = string.Empty,
                            UsageStoreName = string.Empty
                        };
                    }

                    #endregion

                    //成套商品贈品
                    apiCoupon.IsGiveaway = false;

                    //送禮狀態
                    var giftCoupon = gift.FirstOrDefault(x => x.CouponId == coupon.Id);
                    if (giftCoupon != null)
                    {
                        apiCoupon.AcceptGiftType = giftCoupon.Accept;

                        //因為快速領取等同一般領取，前端邏輯接口可以不改變
                        if (apiCoupon.AcceptGiftType == (int)MgmAcceptGiftType.Quick)
                        {
                            apiCoupon.AcceptGiftType = (int)MgmAcceptGiftType.Accept;
                        }
                    }
                    else
                    {
                        apiCoupon.AcceptGiftType = (int)MgmAcceptGiftType.None;
                    }

                    if (apiCoupon.CouponUsedType == ApiCouponUsedType.CanUsed || apiCoupon.CouponUsedType == ApiCouponUsedType.WaitToPass)
                    {
                        couponIdList.Add(coupon.Id);
                    }

                    //萊爾富咖啡不評價
                    if (_config.EnableHiLifeDealSetup && userOrder.CouponType == DealCouponType.HiLifePincode)
                    {
                        apiCoupon.IsToEvaluate = false;
                    }
                    else
                    {
                        //是否可評價
                        apiCoupon.IsToEvaluate =
                        coupon.Status == (int)TrustStatus.Verified && //已核銷
                        _memberProvider.EvaluateGetMainID(_memberProvider.CashTrustLogGetByCouponId(coupon.Id).TrustId) <= 0 && //未評價
                        !(coupon.UsageVerifiedTime == null && coupon.UsageVerifiedTime.Value.AddDays(7) < DateTime.Now); //未逾期    

                    }

                    userOrder.CouponList.Add(apiCoupon);
                }

                #endregion
            }
            builder.AppendLine("28" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            if (couponIdList.Count > 0)
            {
                userOrder.SMSCount = _pponProvider.SMSLogGetcountByCouponIdList(couponIdList);
            }

            //與列表相同的尚有憑證數 (會包含逾期未退貨的數量)
            //新光三越尚有憑證數，不需要計算mgm_gift與全家相關
            if (forSKMOrderAPI)
            {                
                userOrder.PponCouponCount = (int)couponListMain.RemainCount;
            } 
            else
            {
                userOrder.PponCouponCount = CouponFacade.GetCouponCanUsedCount(couponListMain);
            }

            builder.AppendLine("29" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            //真正剩下可以使用的憑證數
            List<ApiCouponUsedType> canUseCoupon = new List<ApiCouponUsedType>()
                {
                    ApiCouponUsedType.WaitToPass,
                    ApiCouponUsedType.CanUsed,
                    ApiCouponUsedType.SpecialCoupon
                };
            userOrder.CanUseCouponCount = userOrder.CouponList.Count(x => canUseCoupon.IndexOf(x.CouponUsedType) != -1);
            builder.AppendLine("30" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            userOrder.OrderStatus = ConfirmOrderStatus(userOrder, couponListMain);
            builder.AppendLine("31" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            #endregion 整理憑證資料

            #region 整理發票資訊
            EinvoiceMainCollection einvoices = _orderProvider.EinvoiceMainGetListByOrderGuid(orderGuid);

            List<EinvoiceMain> viewableInvoice;
            //依據憑證檔或宅配檔，取得可顯示的發票資料(成套以宅配方式撈取)
            if (userOrder.PponDeliveryType == DeliveryType.ToShop && !Helper.IsFlagSet(vpd.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon))
            {
                // 取得 退貨/刷退 以外的 coupon id                
                List<int> nonReturnBackCouponIds =
                    cashTrustLogList.Where(
                        x => x.Status != (int)TrustStatus.Refunded && x.Status != (int)TrustStatus.Returned)
                                    .Select(x => x.CouponId ?? 0).ToList();
                //排除未開立的與已退貨的商品 (C0501 為作廢發票)
                viewableInvoice = einvoices.Where(x => !string.IsNullOrWhiteSpace(x.InvoiceNumber)
                                                                       && x.InvoiceStatus != (int)EinvoiceType.C0501
                                                                       && x.CouponId.HasValue &&
                                                                       nonReturnBackCouponIds.Contains(x.CouponId.Value)).ToList();
            }
            else
            {
                viewableInvoice = einvoices.Where(x => !string.IsNullOrWhiteSpace(x.InvoiceNumber)
                                                       && x.InvoiceStatus != (int)EinvoiceType.C0501).ToList();
                //退貨訂單
                if (order.OrderStatus == (int)OrderStatus.Cancel)
                {
                    //訂單標記為已取消，檢查折讓單是否已處理，若還有餘額則需顯示發票 (部分退貨訂單狀態也會是已取消)
                    var allowances = _orderProvider.EinvoiceAllowanceGetList(viewableInvoice.Select(x => x.Id).ToList());
                    viewableInvoice =
                        viewableInvoice.Where(
                            x => x.OrderAmount != allowances.Where(y => y.Id == x.Id).Sum(y => y.Amount)).ToList();

                }

            }


            foreach (var einvoice in viewableInvoice)
            {
                if (!einvoice.InvoiceNumberTime.HasValue)
                {
                    continue;
                }
                //捐贈發票
                if (einvoice.IsDonateMark)
                {
                    //todo:捐贈發票需另外處理，規則未定。
                }
                else
                {
                    ApiInvoiceData apiInvoice = new ApiInvoiceData();
                    apiInvoice.InvoiceTime = DateTimeToDateTimeString(einvoice.InvoiceNumberTime.Value);
                    apiInvoice.Number = einvoice.InvoiceNumber;
                    apiInvoice.Amount = einvoice.OrderAmount;
                    userOrder.Invoices.Add(apiInvoice);
                }
            }
            #endregion 整理發票資訊
            builder.AppendLine("32" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));

            #region 成套商品券

            userOrder.TotalCouponCount = couponListMain.TotalCount ?? 0;
            userOrder.GroupProductUnitPrice = couponListMain.GroupProductUnitPrice ?? 0;

            #endregion

            userOrder.CouponName = couponListMain.CouponUsage;

            #region App 需求 辨識訂單型態 目前只分 商品券 與 好康
            List<NewCouponListFilterType> filterTypes = MemberFacade.GetViewCouponListMainGetCountNewFilterTypes(couponListMain);
            userOrder.GroupCouponType = GetGroupCouponType(filterTypes);
            #endregion
            builder.AppendLine("33" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));

            #region 檔次圖示
            string imagePathData = couponListMain.ImagePath;
            //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
            if ((couponListMain.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
            {
                //多檔次子檔訂單需取得主檔的圖片
                CouponEventContent mainDealContent = _pponProvider.CouponEventContentGetBySubDealBid(couponListMain.BusinessHourGuid);
                if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                {
                    imagePathData = mainDealContent.ImagePath;
                }
            }
            string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);
            userOrder.MainImagePath = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
            #endregion
            builder.AppendLine("34" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));

            userOrder.GroupCouponAppChannel = couponListMain.GroupCouponAppStyle; //成套商品App頻道
            userOrder.GroupCouponDealType = vpd.GroupCouponDealType ?? 0; //成套票券類型 舊版不支援B型

            userOrder.EvaluateCount = new EvaluateFacade().GetEvaluateCount(order.Guid);
            builder.AppendLine("35" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            if (_config.EnableAppEvaluateSystem && userOrder.EvaluateCount > 0 && userOrder.CouponType != DealCouponType.HiLifePincode)
            {
                userOrder.OrderTypeDesc = "待評價: " + userOrder.EvaluateCount;
            }
            else
            {
                userOrder.OrderTypeDesc = GetOrderTypeDesc(couponListMain);
            }
            builder.AppendLine("36" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            //宅配檔次 方可使用換貨申請功能
            userOrder.ExchangeEnable = userOrder.PponDeliveryType == DeliveryType.ToHouse
                                    && _config.EnabledExchangeProcess;

            userOrder.VerifyType = vpd.VerifyActionType ?? (int)VerifyActionType.None;
            userOrder.CsMessageCount = _csProvider.GetCustomerServiceOutsideLogCountByorderGuid(uniqueId, orderGuid);
            builder.AppendLine("37" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            userOrder.UnUsed17PayMsg = _config.UnUsed17PayMsg;
            userOrder.Used17PayMsg = _config.Used17PayMsg;

            userOrder.Is24H = vpd.CategoryIds != null
                && vpd.CategoryIds.Contains(PponDealPreviewManager._MAIN_THEME_CATEGORY_ID);
            builder.AppendLine("38" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));

            if (userOrder.TotalAmount != 0)
                ProposalFacade.ProposalPerformanceLogSet("CheckDeliveryStatus", orderGuid.ToString() + " " + builder.ToString(), "sys");
            return userOrder;
        }


        /// <summary>
        /// [v2.0] 取得某依訂單的資料
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="uniqueId"></param>
        /// <param name="newGroupCouponApi"></param>
        /// <returns></returns>
        public static ApiUserOrder ApiUserOrderGetDeliveryByOrderGuid(Guid orderGuid, int uniqueId, bool newGroupCouponApi = true)
        {
            ApiUserOrder userOrder = new ApiUserOrder();
            Order order = _orderProvider.OrderGet(orderGuid);
            Guid bid = Guid.Empty;
            CashTrustLogCollection cashTrustLogList = _memberProvider.CashTrustLogGetListByOrderGuid(orderGuid);
            var classification = cashTrustLogList.First().OrderClassification;
            if (cashTrustLogList.Count > 0)
            {
                var firstCoupon = cashTrustLogList.First();
                if (firstCoupon.BusinessHourGuid == null)
                {
                    return null;
                }
                else
                {
                    bid = firstCoupon.BusinessHourGuid.Value;
                }
            }
            else
            {
                //沒有資料，回傳錯誤。
                return null;
            }
            CtAtmRefund ctatmrefund = _orderProvider.CtAtmRefundGetLatest(orderGuid);
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            if (!vpd.IsLoaded)
            {
                return null;
            }

            ViewCouponListMainCollection couponListMainList = _memberProvider.GetCouponListMainListByOrderID(uniqueId, order.OrderId);
            //查無資料
            if (couponListMainList.Count == 0)
            {
                return null;
            }

            ViewCouponListMain couponListMain = couponListMainList.First();

            userOrder.OrderGuid = order.Guid;
            userOrder.BusinessHourGuid = vpd.BusinessHourGuid;
            userOrder.CouponName = couponListMain.CouponUsage;

            userOrder.UseStartDate = DateTimeToDateTimeString(vpd.BusinessHourDeliverTimeS.Value);
            if (order.OrderSettlementTime == null)
            {
                if (vpd.ChangedExpireDate == null)
                {
                    userOrder.UseEndDate = DateTimeToDateTimeString(vpd.BusinessHourDeliverTimeE.Value);
                }
                else
                {
                    userOrder.UseEndDate = DateTimeToDateTimeString(vpd.ChangedExpireDate.Value);
                }
            }
            else
            {   //清算檔次使用期限記在Order.OrderSettlementTime
                userOrder.UseEndDate = DateTimeToDateTimeString((DateTime)order.OrderSettlementTime);
            }

            OrderDetailCollection orderDetailList = _orderProvider.OrderDetailGetList(orderGuid);
            userOrder.BuyDate = OrderFacade.IsIspPaymentOrder(order.OrderStatus) && order.DeliveryTime != null ? DateTimeToDateTimeString((DateTime)order.DeliveryTime)
                : DateTimeToDateTimeString(order.CreateTime);
            userOrder.OrderDetailList = new List<ApiUserOrderDetail>();
            userOrder.Freight = 0; //運費先設為0

            Guid selectStoreGuid = Guid.Empty;
            foreach (var orderDetail in orderDetailList)
            {
                if (orderDetail.Status == (int)OrderDetailStatus.SystemEntry)
                {
                    //運費
                    if (orderDetail.Total != null)
                    {
                        userOrder.Freight += orderDetail.Total.Value;
                    }
                }
                else
                {
                    //有選擇分店
                    if ((selectStoreGuid == Guid.Empty) && (orderDetail.StoreGuid != null))
                    {
                        selectStoreGuid = orderDetail.StoreGuid.Value;
                    }
                    ApiUserOrderDetail userOrderDetail = new ApiUserOrderDetail();
                    userOrderDetail.OrderDetailGuid = orderDetail.Guid;
                    userOrderDetail.ItemName = OrderFacade.GetOrderDetailItemName(OrderDetailItemModel.Create(orderDetail, vpd.BusinessHourGuid)).Replace("&nbsp", "");
                    userOrderDetail.Price = newGroupCouponApi && (vpd.GroupCouponDealType ?? 0) == (int)GroupCouponDealType.AvgAssign ? orderDetail.ItemUnitPrice / (vpd.SaleMultipleBase ?? 1) : orderDetail.ItemUnitPrice;
                    userOrderDetail.Quantity = newGroupCouponApi && (vpd.GroupCouponDealType ?? 0) == (int)GroupCouponDealType.AvgAssign ? orderDetail.ItemQuantity * (vpd.SaleMultipleBase ?? 1) : orderDetail.ItemQuantity;

                    userOrder.OrderDetailList.Add(userOrderDetail);
                }
            }

            bool isCompleted = !MemberFacade.ViewCouponListMainIsFilterType(couponListMain, CouponListFilterType.NotCompleted);//已完成付款
            decimal totalAmount = 0;
            decimal familyIspAmount = 0, sevenIspAmount = 0;
            foreach (var ctl in cashTrustLogList)
            {
                familyIspAmount += ctl.FamilyIsp;
                sevenIspAmount += ctl.SevenIsp;
                totalAmount += ctl.Amount;

            }
            userOrder.TotalAmount = totalAmount;
            userOrder.IsCompleted = isCompleted;

            //超取付款訂單，有壓完成單就算有付款
            if (familyIspAmount + sevenIspAmount > 0)
            {
                userOrder.IsCompleted = Helper.IsFlagSet(order.OrderStatus, OrderStatus.Complete);
            }

            userOrder.PponDeliveryType = vpd.DeliveryType == null
                                            ? DeliveryType.ToShop
                                            : (DeliveryType)vpd.DeliveryType;

            //收件人資訊
            if (vpd.DeliveryType == (int)DeliveryType.ToHouse)
            {
                userOrder.AddresseeName = order.MemberName;
                userOrder.AddresseePhone = order.MobileNumber;
                userOrder.AddresseeAddress = order.DeliveryAddress;
            }



            if (_config.EnableAppEvaluateSystem && userOrder.EvaluateCount > 0 && userOrder.CouponType != DealCouponType.HiLifePincode)
            {
                userOrder.OrderTypeDesc = "待評價: " + userOrder.EvaluateCount;
            }
            else
            {
                userOrder.OrderTypeDesc = GetOrderTypeDesc(couponListMain);
            }

            #region atm處理

            //付款方式為ATM
            userOrder.AtmData = null;
            if ((order.OrderStatus & (int)LunchKingSite.Core.OrderStatus.ATMOrder) > 0)
            {
                ATMRemittanceData remittanceData = PaymentFacade.ATMRemittanceDataGetByOrderGuid(order.Guid);
                if (remittanceData != null)
                {
                    ApiOrderAtmData atmData = new ApiOrderAtmData();
                    atmData.BankName = remittanceData.BankName;
                    atmData.BankCode = remittanceData.BankCode;
                    atmData.BankAccount = remittanceData.BankAccount;
                    atmData.DeadlineDate = DateTimeToDateTimeString(remittanceData.RemittanceDeadline);
                    atmData.Amount = remittanceData.Amount;

                    userOrder.AtmData = atmData;
                }
            }

            #endregion atm處理

            #region 退貨明細

            //退貨狀況
            userOrder.RefundLogList = new List<ApiRefundLog>();

            IList<ReturnFormEntity> returnForms = ReturnFormRepository.FindAllByOrder(orderGuid);
            if (returnForms.Any())
            {
                userOrder.RefundLogList = GetRefundLogList(cashTrustLogList, returnForms, ctatmrefund);
            }

            #endregion 退貨明細

            #region 出貨明細

            if (couponListMain.DeliveryType == (int)DeliveryType.ToHouse)
            {
                var shipInfoList = OrderFacade.GetOrderShipInfoList(couponListMain);
                userOrder.ShipInfoList = shipInfoList;
            }

            #endregion

            userOrder.OrderStatus = ConfirmOrderStatus(userOrder, couponListMain);
            #region 檔次圖示
            string imagePathData = couponListMain.ImagePath;
            //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
            if ((couponListMain.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
            {
                //多檔次子檔訂單需取得主檔的圖片
                CouponEventContent mainDealContent = _pponProvider.CouponEventContentGetBySubDealBid(couponListMain.BusinessHourGuid);
                if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                {
                    imagePathData = mainDealContent.ImagePath;
                }
            }
            string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);
            userOrder.MainImagePath = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
            #endregion
            return userOrder;
        }
        /// <summary>
        /// 依據order的狀態，確認其orderStatus該為何值
        /// </summary>
        /// <param name="order"></param>
        /// <param name="couponListMain"></param>
        /// <returns></returns>
        public static ApiOrderStatus ConfirmOrderStatus(ApiUserOrder order, ViewCouponListMain couponListMain)
        {
            DateTime theMoment = DateTime.Now;
            //訂單未付款完成且有ATM資料
            if (!order.IsCompleted)
            {
                if (order.AtmData != null)
                {
                    //有ATM匯款資料，是否已超過匯款期限
                    DateTime deadLineDate;
                    if (ApiSystemManager.TryDateTimeStringToDateTime(order.AtmData.DeadlineDate, out deadLineDate))
                    {
                        //目前系統時間大於付款期限
                        if (deadLineDate < theMoment)
                        {
                            //ATM已超過付款期限
                            return ApiOrderStatus.AtmOverdue;
                        }
                        else
                        {
                            //ATM未付款
                            return ApiOrderStatus.AtmUnpaid;
                        }
                    }
                }
                if (couponListMain.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup ||
                    couponListMain.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
                {
                    if (Helper.IsFlagSet(couponListMain.OrderStatus, (int)OrderStatus.Cancel))
                    {
                        return ApiOrderStatus.RefundComplete;
                    }
                    return ApiOrderStatus.ToHouse;
                }

                //訂單未完成，卻沒有ATM資料或ATM資料不齊全，皆為
                return ApiOrderStatus.None;
            }
            //若為宅配訂單
            if (order.PponDeliveryType == DeliveryType.ToHouse)
            {
                if (couponListMain.IsLoaded)
                {
                    var returnModifyTime = couponListMain.ReturnModifyTime ?? DateTime.MinValue;
                    var exchangeModifyTime = couponListMain.ExchangeModifyTime ?? DateTime.MinValue;

                    if ((returnModifyTime == DateTime.MinValue || couponListMain.ReturnStatus == (int)ProgressStatus.Canceled ) && exchangeModifyTime == DateTime.MinValue)
                    {
                        //未退貨，判斷商品是否已出貨或已過鑑賞期
                        if (PponOrderManager.CheckIsOverTrialPeriod(order.OrderGuid))
                        {
                            //已過鑑賞期
                            return ApiOrderStatus.GoodsExpired;
                        }
                        else
                        {
                            //檢查是否已出貨
                            if (PponOrderManager.CheckIsShipped(order.OrderGuid, OrderClassification.LkSite))
                            {
                                //已出貨
                                return ApiOrderStatus.GoodsShipped;
                            }
                            else
                            {
                                //尚未出貨出貨
                                return ApiOrderStatus.NotYetShipped;
                            }
                        }
                    }
                    //依最新一筆退換貨單紀錄之異動時間 抓取最新一筆 顯示訂單退換貨狀態
                    else if (exchangeModifyTime > returnModifyTime)
                    {
                        //換貨
                        if (couponListMain.ExchangeStatus == (int)OrderLogStatus.SendToSeller ||
                            couponListMain.ExchangeStatus == (int)OrderLogStatus.ExchangeProcessing)
                        {
                            return ApiOrderStatus.ExchangeProcessing;
                        }
                        if (couponListMain.ExchangeStatus == (int)OrderLogStatus.ExchangeSuccess)
                        {
                            return ApiOrderStatus.ExchangeSuccess;
                        }
                        if (couponListMain.ExchangeStatus == (int)OrderLogStatus.ExchangeFailure)
                        {
                            return ApiOrderStatus.ExchangeFailure;
                        }
                        if (couponListMain.ExchangeStatus == (int)OrderLogStatus.ExchangeCancel)
                        {
                            return ApiOrderStatus.ExchangeCancel;
                        }
                    }
                    else
                    {
                        //退貨
                        if (couponListMain.ReturnStatus == (int)ProgressStatus.Processing ||
                            couponListMain.ReturnStatus == (int)ProgressStatus.AtmQueueing ||
                            couponListMain.ReturnStatus == (int)ProgressStatus.AtmQueueSucceeded ||
                            couponListMain.ReturnStatus == (int)ProgressStatus.AtmFailed ||
                            couponListMain.ReturnStatus == (int)ProgressStatus.Retrieving ||
                            couponListMain.ReturnStatus == (int)ProgressStatus.ConfirmedForCS ||
                            couponListMain.ReturnStatus == (int)ProgressStatus.RetrieveToPC ||
                            couponListMain.ReturnStatus == (int)ProgressStatus.RetrieveToCustomer ||
                            couponListMain.ReturnStatus == (int)ProgressStatus.ConfirmedForVendor ||
                            couponListMain.ReturnStatus == (int)ProgressStatus.ConfirmedForUnArrival)
                        {
                            return ApiOrderStatus.WaitForRefund;
                        }
                        if (couponListMain.ReturnStatus == (int)ProgressStatus.Completed ||
                            couponListMain.ReturnStatus == (int)ProgressStatus.CompletedWithCreditCardQueued)
                        {
                            return ApiOrderStatus.RefundComplete;
                        }
                        if (couponListMain.ReturnStatus == (int)ProgressStatus.Unreturnable)
                        {
                            return ApiOrderStatus.Unreturnable;
                        }
                    }
                }

                //其他狀況一律回傳宅配類型
                return ApiOrderStatus.ToHouse;
            }
            else
            {
                //任何憑證是銷售中未達門檻，回傳尚未開始使用
                if (order.CouponList.Any(x => x.CouponUsedType == ApiCouponUsedType.WaitToPass))
                {
                    return ApiOrderStatus.NotYetStarted;
                }
                //任何憑證是未達門檻，回傳尚未開始使用
                if (order.CouponList.Any(x => x.CouponUsedType == ApiCouponUsedType.FailDeal))
                {
                    return ApiOrderStatus.FailDeal;
                }

                List<ApiCouponUsedType> canUseList = new List<ApiCouponUsedType>
                {
                    ApiCouponUsedType.CanUsed,
                    ApiCouponUsedType.SpecialCoupon,
                    ApiCouponUsedType.CouponExpired
                };
                //有任何憑證未使用
                if (order.CouponList.Any(x => canUseList.Contains(x.CouponUsedType)))
                {
                    //檢查是否已過期
                    DateTime useStartDate, useEndDate;
                    if ((ApiSystemManager.TryDateTimeStringToDateTime(order.UseStartDate, out useStartDate)) &&
                        (ApiSystemManager.TryDateTimeStringToDateTime(order.UseEndDate, out useEndDate)))
                    {
                        //尚未開始，以日期判斷不考慮時分秒
                        if (useStartDate.Date > theMoment.Date)
                        {
                            return ApiOrderStatus.NotYetStarted;
                        }
                        //已截止，以日期判斷不考慮時分秒
                        if (useEndDate.Date < theMoment.Date)
                        {
                            //已截止，憑證未使用完畢。
                            return ApiOrderStatus.CouponOverdue;
                        }
                        return ApiOrderStatus.Unused;
                    }
                    //錯誤的狀態
                    return ApiOrderStatus.None;
                }
                //到這邊是沒有未使用的憑證

                //檢查是否有退貨處理中
                if (order.CouponList.Any(x => x.CouponUsedType == ApiCouponUsedType.WaitForRefund) ||
                    order.CouponList.Any(x => x.CouponUsedType == ApiCouponUsedType.ATMRefund))
                {
                    return ApiOrderStatus.WaitForRefund;
                }
                if (order.CouponList.Any(x => x.CouponUsedType == ApiCouponUsedType.CouponRefund))
                {
                    return ApiOrderStatus.RefundComplete;
                }

                //其他狀態為已使用
                return ApiOrderStatus.CouponAllUsed;
            }
        }

        /// <summary>
        /// [v2.0] 依據會員帳號，回傳會員各類別訂單的數量
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isLatest"></param>
        /// <param name="groupingCoupon"></param>
        /// <returns></returns>
        public static List<dynamic> ApiCouponListMainGroupGetListByUser(int userId, bool isLatest, bool groupingCoupon = false)
        {
            List<dynamic> rtn = new List<dynamic>();
            //查詢使用者訂單(不排除品生活)
            var orders = MemberFacade.ViewCouponListMainGetListByUser(0,
                                                                          0,
                                                                          userId,
                                                                          NewCouponListFilterType.None,
                                                                          Guid.Empty,
                                                                          isLatest, true);


            //依據顯示順序產生物件
            //禮券成套販售是否上線設定
            if (_sysConfProvider.IsGroupCouponOn && groupingCoupon)
            {
                #region 支援成套販售

                int couponCount = 0, groupCouponCount = 0;

                #region 組FilterType

                //成套販售 順序 -> 成套販售 > 一般憑證
                rtn.Add(new
                {
                    GroupType = (int)CouponListGroupType.GroupCoupon,
                    CouponList = new List<dynamic>
                {
                    new { Count = 0, FilterType = (int)NewCouponListFilterType.GroupCouponReady, Text = string.Empty }
                    , new { Count = 0, FilterType = (int)NewCouponListFilterType.GroupCouponNotUsed, Text = string.Empty }
                    , new { Count = 0, FilterType = (int)NewCouponListFilterType.GroupCoupon, Text = string.Empty }
                }
                });

                //一般憑證

                //暫時判斷,待SKMfilter正式上線後就可以刪了
                if (_config.IsShowSKMFilter == true)
                {
                    rtn.Add(new
                    {
                        GroupType = (int)CouponListGroupType.Coupon,
                        CouponList = new List<dynamic>
                {
                    new { Count = 0, FilterType = (int)NewCouponListFilterType.NotUsed, Text = string.Empty }
                    , new { Count = 0, FilterType = (int)NewCouponListFilterType.OverdueNotUsed, Text = string.Empty }
                    , new { Count = 0, FilterType = (int)NewCouponListFilterType.NotShipped, Text = string.Empty }
                    , new { Count = 0, FilterType = (int)NewCouponListFilterType.Fami, Text = string.Empty }
                    , new { Count = 0, FilterType = (int)NewCouponListFilterType.CouponOnly, Text = string.Empty }
                    , new { Count = 0, FilterType = (int)NewCouponListFilterType.Product, Text = string.Empty }
                    , new { Count = 0, FilterType = (int)NewCouponListFilterType.Event, Text = string.Empty }
                    , new { Count = 0, FilterType = (int)NewCouponListFilterType.Cancel, Text = string.Empty }
                    , new { Count = 0, FilterType = (int)NewCouponListFilterType.SKM, Text = string.Empty }
                }
                    });
                }
                else
                {
                    rtn.Add(new
                    {
                        GroupType = (int)CouponListGroupType.Coupon,
                        CouponList = new List<dynamic>
                        {
                            new { Count = 0, FilterType = (int)NewCouponListFilterType.NotUsed, Text = string.Empty }
                            , new { Count = 0, FilterType = (int)NewCouponListFilterType.OverdueNotUsed, Text = string.Empty }
                            , new { Count = 0, FilterType = (int)NewCouponListFilterType.NotShipped, Text = string.Empty }
                            , new { Count = 0, FilterType = (int)NewCouponListFilterType.Fami, Text = string.Empty }
                            , new { Count = 0, FilterType = (int)NewCouponListFilterType.CouponOnly, Text = string.Empty }
                            , new { Count = 0, FilterType = (int)NewCouponListFilterType.Product, Text = string.Empty }
                            , new { Count = 0, FilterType = (int)NewCouponListFilterType.Event, Text = string.Empty }
                            , new { Count = 0, FilterType = (int)NewCouponListFilterType.Cancel, Text = string.Empty }
                        }
                    });
                }
                #endregion 組Filter

                List<string> couponOrderIds = new List<string>();
                List<string> couponGroupOrderIds = new List<string>();
                List<Guid> groupCouponOrderGuid = new List<Guid>(); //若order已是成套商品將不再放入一般憑證數字

                foreach (var order in orders)
                {
                    foreach (var groupType in rtn)
                    {
                        foreach (var apiCouponListMainGroup in groupType.CouponList)
                        {
                            if (MemberFacade.ViewCouponListMainIsFilterType(order, (NewCouponListFilterType)apiCouponListMainGroup.FilterType))
                            {
                                if (groupType.GroupType == (int)CouponListGroupType.GroupCoupon)
                                {
                                    groupCouponOrderGuid.Add(order.Guid);
                                }
                                else //過濾成套商品
                                {
                                    if (groupCouponOrderGuid.Contains(order.Guid))
                                    {
                                        continue;
                                    }
                                }

                                string fieldName = "<Count>i__Field";

                                // Get the type
                                Type t = apiCouponListMainGroup.GetType();
                                // Get the field information using reflection
                                FieldInfo fi = t.GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);
                                // Set the field using reflection
                                int temp = apiCouponListMainGroup.Count + 1;
                                fi.SetValue(apiCouponListMainGroup, temp);

                                switch ((CouponListGroupType)groupType.GroupType)
                                {
                                    case CouponListGroupType.Coupon:
                                        if (!couponOrderIds.Contains(order.OrderId))
                                        {
                                            couponCount++;
                                            couponOrderIds.Add(order.OrderId);
                                        }
                                        break;
                                    case CouponListGroupType.GroupCoupon:
                                        if (!couponGroupOrderIds.Contains(order.OrderId))
                                        {
                                            groupCouponCount++;
                                            couponGroupOrderIds.Add(order.OrderId);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }

                //全部的類別最後再加進去
                foreach (var groupType in rtn)
                {
                    switch ((CouponListGroupType)groupType.GroupType)
                    {
                        case CouponListGroupType.Coupon:
                            groupType.CouponList.Add(new { Count = couponCount, FilterType = (int)NewCouponListFilterType.None, Text = string.Empty });
                            break;
                        case CouponListGroupType.GroupCoupon:
                            //成套禮券不需要 All
                            break;
                    }
                }
                #endregion
            }
            else
            {
                #region 不支援成套販售

                rtn.Add(new { Count = 0, FilterType = (int)NewCouponListFilterType.NotUsed, Text = string.Empty });
                rtn.Add(new { Count = 0, FilterType = (int)NewCouponListFilterType.OverdueNotUsed, Text = string.Empty });
                rtn.Add(new { Count = 0, FilterType = (int)NewCouponListFilterType.NotShipped, Text = string.Empty });
                rtn.Add(new { Count = 0, FilterType = (int)NewCouponListFilterType.Fami, Text = string.Empty });
                rtn.Add(new { Count = 0, FilterType = (int)NewCouponListFilterType.CouponOnly, Text = string.Empty });
                rtn.Add(new { Count = 0, FilterType = (int)NewCouponListFilterType.Product, Text = string.Empty });
                rtn.Add(new { Count = 0, FilterType = (int)NewCouponListFilterType.Event, Text = string.Empty });
                rtn.Add(new { Count = 0, FilterType = (int)NewCouponListFilterType.Cancel, Text = string.Empty });

                if (_config.IsShowSKMFilter == true)
                {
                    rtn.Add(new { Count = 0, FilterType = (int)NewCouponListFilterType.SKM, Text = string.Empty });
                }

                foreach (var order in orders)
                {
                    foreach (var apiCouponListMainGroup in rtn)
                    {
                        //todo: 邏輯有點問題，導致數字也不太對。check again
                        if (MemberFacade.ViewCouponListMainIsFilterType(order, (NewCouponListFilterType)apiCouponListMainGroup.FilterType))
                        {
                            string fieldName = "<Count>i__Field";

                            // Get the type
                            Type t = apiCouponListMainGroup.GetType();
                            // Get the field information using reflection
                            FieldInfo fi = t.GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);
                            // Set the field using reflection
                            int temp = apiCouponListMainGroup.Count + 1;
                            fi.SetValue(apiCouponListMainGroup, temp);
                        }
                    }
                }
                //全部的類別最後再加進去
                rtn.Add(new { Count = orders.Count, FilterType = NewCouponListFilterType.None, Text = string.Empty });

                #endregion
            }

            return rtn.ToList();//只回傳有資料的部分
        }

        /// <summary>
        /// [v2.0] 取得APP訂單目錄(Group)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isLatest"></param>
        /// <param name="groupingCoupon"></param>
        /// <returns></returns>
        public static List<ApiOrderGrouptMain> GetApiOrderGroupListMain(int userId, bool isLatest, bool groupingCoupon = false)
        {
            List<ApiOrderGrouptMain> ogmList = GetApiOrderGroupStrucate();
            ViewCouponListMainCollection vCouponListMainCol =
                MemberFacade.ViewCouponListMainGetListByUser(0, 0, userId, NewCouponListFilterType.None, Guid.Empty, isLatest, true);

            #region 處理各GROUP訂單數

            int couponCount = 0; //統計一般憑證總數
            List<string> couponOrderIds = new List<string>();
            List<string> couponGroupOrderIds = new List<string>();
            List<string> famiGroupOrderIds = new List<string>();
            List<Guid> groupCouponOrderGuid = new List<Guid>(); //若order已是成套商品將不再放入一般憑證數字

            foreach (var vCouponListMain in vCouponListMainCol)
            {
                foreach (ApiOrderGrouptMain groupType in ogmList) //憑證類型:3筆
                {
                    foreach (var apiOrderGroup in groupType.CouponList) //憑證群組:固定值 (目前約十幾個)
                    {
                        //依Filter分群
                        if (MemberFacade.ViewCouponListMainIsFilterType(vCouponListMain, apiOrderGroup.FilterType))
                        {
                            //成套
                            if (groupType.GroupType == (int)CouponListGroupType.GroupCoupon && !vCouponListMain.IsDepositCoffee)
                            {
                                groupCouponOrderGuid.Add(vCouponListMain.Guid);
                            }
                            //非成套
                            else
                            {
                                if (groupCouponOrderGuid.Contains(vCouponListMain.Guid))
                                {
                                    continue;
                                }
                            }

                            switch ((CouponListGroupType)groupType.GroupType)
                            {
                                case CouponListGroupType.Coupon:
                                    if (!PponFacade.IsFamilyNetPincode(vCouponListMain.BusinessHourStatus, vCouponListMain.GroupOrderStatus ?? 0))
                                    {
                                        if (!couponOrderIds.Contains(vCouponListMain.OrderId))
                                        {
                                            couponCount++;
                                            couponOrderIds.Add(vCouponListMain.OrderId);
                                        }
                                        apiOrderGroup.Count += 1;
                                    }
                                    break;
                                case CouponListGroupType.GroupCoupon:
                                    if (!couponGroupOrderIds.Contains(vCouponListMain.OrderId))
                                    {
                                        couponGroupOrderIds.Add(vCouponListMain.OrderId);
                                    }
                                    apiOrderGroup.Count += 1;
                                    break;
                                case CouponListGroupType.CoffeeGroup:
                                    if (!famiGroupOrderIds.Contains(vCouponListMain.OrderId))
                                    {
                                        famiGroupOrderIds.Add(vCouponListMain.OrderId);
                                    }

                                    //是否在退貨處理中
                                    var isReturnProcess = _memberProvider.PponRefundingCashTrustLogGetListByOrderGuid(vCouponListMain.Guid).Any();
                                    var isPastFinalExpireDate = IsPastFinalExpireDate(vCouponListMain);

                                    int slug;
                                    DateTime sysDate = DateTime.Now;
                                    if (Helper.IsFlagSet(vCouponListMain.OrderStatus, OrderStatus.Complete) &&
                                        !Helper.IsFlagSet(vCouponListMain.OrderStatus, OrderStatus.Cancel) &&
                                        !Helper.IsFlagSet(vCouponListMain.OrderStatus, OrderStatus.Locked) &&
                                        CouponFacade.GetCouponCanUsedCount(vCouponListMain) > 0 && //憑證未使用完畢
                                        !isPastFinalExpireDate && //憑證未過期
                                        !isReturnProcess && //非退貨處理中
                                        !(vCouponListMain.BusinessHourOrderTimeE <= sysDate &&
                                            int.TryParse(vCouponListMain.Slug, out slug) &&
                                            slug < vCouponListMain.BusinessHourOrderMinimum)) //排除結檔未達門檻
                                    {
                                        apiOrderGroup.Count += 1;
                                    }
                                    break;
                            }
                        }
                    }
                }
            }

            //全部的類別最後再加進去
            foreach (var groupType in ogmList)
            {
                switch ((CouponListGroupType)groupType.GroupType)
                {
                    case CouponListGroupType.Coupon:
                        var apiOrderGroup = new ApiOrderGroup();
                        apiOrderGroup.Count = couponCount;
                        apiOrderGroup.FilterType = NewCouponListFilterType.None;
                        apiOrderGroup.Text = "全部";
                        groupType.CouponList.Add(apiOrderGroup);
                        break;
                    case CouponListGroupType.GroupCoupon:
                        //成套禮券不需要 All
                        break;
                }
            }

            #endregion

            return ogmList;
        }

        /// <summary>
        /// [v2.1] 建立APP訂單目錄(Group) 
        /// </summary>
        /// <returns></returns>
        private static List<ApiOrderGrouptMain> GetApiOrderGroupStrucate()
        {
            #region 整理訂單群組

            var groupFilterDic = new Dictionary<int, List<NewCouponListFilterType>>();

            //成套憑證
            var groupFilters = new List<NewCouponListFilterType>();
            groupFilters.Add(NewCouponListFilterType.GroupCouponReady);
            groupFilters.Add(NewCouponListFilterType.GroupCouponNotUsed);
            if (_config.EnableAppEvaluateSystem)
            {
                groupFilters.Add(NewCouponListFilterType.GroupCouponToEvaluate);
            }

            groupFilters.Add(NewCouponListFilterType.GroupCoupon);
            groupFilterDic.Add((int)CouponListGroupType.GroupCoupon, groupFilters);

            //寄杯
            var coffeeFilters = new List<NewCouponListFilterType>();
            if (_config.EnableDepositCoffee)
            {
                coffeeFilters.Add(NewCouponListFilterType.DepositCoffee);
            }

            if (_config.EnableFamiCoffee)
            {
                coffeeFilters.Add(NewCouponListFilterType.FamiCoffee);
            }

            if (coffeeFilters.Any())
            {
                groupFilterDic.Add((int)CouponListGroupType.CoffeeGroup, coffeeFilters);
            }

            //一般憑證
            var gneralFilters = new List<NewCouponListFilterType>();
            gneralFilters.Add(NewCouponListFilterType.NotUsed);
            gneralFilters.Add(NewCouponListFilterType.OverdueNotUsed);
            if (_config.EnableAppEvaluateSystem)
            {
                gneralFilters.Add(NewCouponListFilterType.CouponToEvaluate);
            }

            gneralFilters.Add(NewCouponListFilterType.NotShipped);
            gneralFilters.Add(NewCouponListFilterType.Fami);
            gneralFilters.Add(NewCouponListFilterType.CouponOnly);
            gneralFilters.Add(NewCouponListFilterType.Product);
            gneralFilters.Add(NewCouponListFilterType.Event);
            gneralFilters.Add(NewCouponListFilterType.Cancel);
            if (_config.IsShowSKMFilter)
            {
                gneralFilters.Add(NewCouponListFilterType.SKM);
            }

            if (_config.EnabledExchangeProcess)
            {
                gneralFilters.Add(NewCouponListFilterType.Exchanging);
            }

            groupFilterDic.Add((int)CouponListGroupType.Coupon, gneralFilters);

            #endregion

            var ogmList = new List<ApiOrderGrouptMain>();
            foreach (var groupFilter in groupFilterDic)
            {
                var ogm = new ApiOrderGrouptMain();
                ogm.GroupType = groupFilter.Key;
                ogm.CouponList = new List<ApiOrderGroup>();
                foreach (var filterType in groupFilter.Value)
                {
                    var og = new ApiOrderGroup();
                    og.FilterType = filterType;
                    og.Text = Helper.GetLocalizedEnum(filterType);
                    ogm.CouponList.Add(og);
                }
                ogmList.Add(ogm);
            }

            return ogmList;
        }

        /// <summary>
        /// [v2.0] 訂單列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="isLatest"></param>
        /// <returns></returns>
        public static List<ApiPponOrder> ApiPponOrderListItemGetListByFilterType(int userId, NewCouponListFilterType type, bool isLatest)
        {
            //查詢訂單資料(不排除品生活)
            var orderList = MemberFacade.ViewCouponListMainGetListByUser(0, 0, userId, type, Guid.Empty, isLatest, true);
            return GetApiPponOrderList(orderList, type);
        }

        /// <summary>
        /// [v2.0] 訂單列表
        /// 將訂單ViewCouponListMainCol轉成ApiPponOrder列表
        /// </summary>
        /// <param name="viewCouponListMainCol"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<ApiPponOrder> GetApiPponOrderList(ViewCouponListMainCollection viewCouponListMainCol,
                NewCouponListFilterType type = NewCouponListFilterType.None)
        {
            List<ApiPponOrder> apiPponOrderList = new List<ApiPponOrder>();

            foreach (var order in viewCouponListMainCol.OrderByDescending(x => x.CreateTime))
            {
                List<NewCouponListFilterType> filterTypes = MemberFacade.GetViewCouponListMainGetCountNewFilterTypes(order);

                #region 排除項目

                //非成套商品Type濾除含成套商品Type order
                if (!_groupCouponType.Contains(type) && filterTypes.Intersect(_groupCouponType).Any())
                {
                    continue;
                }

                if (!_depositCoffee.Contains(type) && filterTypes.Intersect(_depositCoffee).Any())
                {
                    continue;
                }

                #endregion

                DateTime useStartDate = order.BusinessHourDeliverTimeS == null
                                            ? DateTime.MinValue
                                            : order.BusinessHourDeliverTimeS.Value;
                DateTime useEndDate = order.BusinessHourDeliverTimeE == null
                                            ? useStartDate
                                            : order.BusinessHourDeliverTimeE.Value;
                useEndDate = order.ChangedExpireDate == null ? useEndDate : order.ChangedExpireDate.Value;

                bool isSettlementDeal = false;

                #region SKM

                if (type.Equals(NewCouponListFilterType.SKM) == true)
                {
                    Order tempOrder = _orderProvider.OrderGet(order.Guid);
                    IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(order.BusinessHourGuid);
                    if (tempOrder.OrderSettlementTime == null)
                    {
                        if (vpd.ChangedExpireDate == null)
                        {
                            useEndDate = vpd.BusinessHourDeliverTimeE == null
                                            ? DateTime.MinValue
                                            : vpd.BusinessHourDeliverTimeS.Value;
                        }
                        else
                        {
                            useEndDate = vpd.ChangedExpireDate.Value;
                        }
                    }
                    else
                    {   //清算檔次使用期限記在Order.OrderSettlementTime
                        useEndDate = (DateTime)tempOrder.OrderSettlementTime;
                    }

                    SkmPponDeal skmPponDeal = CreateSkmOrderPponDeal(vpd);
                    isSettlementDeal = skmPponDeal.IsSettlementDeal;
                }

                #endregion

                int couponCount = CouponFacade.GetCouponCanUsedCount(order);
                //成套禮券濾除過期商品
                if (_groupCouponType.Contains(type) && useEndDate < DateTime.Now && couponCount == 0)
                {
                    continue;
                }

                string imagePathData = order.ImagePath;
                //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                if ((order.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                {
                    //多檔次子檔訂單需取得主檔的圖片
                    CouponEventContent mainDealContent = _pponProvider.CouponEventContentGetBySubDealBid(order.BusinessHourGuid);
                    if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                    {
                        imagePathData = mainDealContent.ImagePath;
                    }
                }
                string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);
                NewCouponListFilterType countType = filterTypes.Count > 0
                    ? (filterTypes.Contains(type)
                        ? type : filterTypes.First())
                    : NewCouponListFilterType.None;

                var evaluateCount = new EvaluateFacade().GetEvaluateCount(order.Guid);

                #region 出貨時間
                var actualShippingDate = string.Empty;
                var lastShippingDate = string.Empty;

                if (order.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    var shipCols = _orderProvider.ViewOrderShipListGetListByOrderGuid(order.Guid).OrderByDescending(x => x.ShipTime);
                    ViewOrderShipList orderShip = shipCols.FirstOrDefault();
                    if (orderShip != null && orderShip.ShipTime != null && orderShip.ShipTime < DateTime.Now.AddDays(1).Date)
                    {
                        actualShippingDate = ((DateTime)orderShip.ShipTime).ToShortDateString();
                    }

                    if (order.LastShipDate != null)
                    {
                        lastShippingDate = ((DateTime)order.LastShipDate).ToShortDateString();
                    }

                    //ATM 未成單不顯示最後出貨日及出貨狀態
                    if (Helper.IsFlagSet(order.OrderStatus, OrderStatus.ATMOrder) &&
                    !Helper.IsFlagSet(order.OrderStatus, OrderStatus.Complete))
                    {
                        lastShippingDate = string.Empty;
                    }
                }

                #endregion

                var apiPponOrder = new ApiPponOrder();
                apiPponOrder.OrderGuid = order.Guid;
                apiPponOrder.CountType = countType;
                apiPponOrder.CouponName = order.CouponUsage;
                apiPponOrder.UseStartDate = DateTimeToDateTimeString(useStartDate);
                apiPponOrder.UseEndDate = DateTimeToDateTimeString(useEndDate);
                apiPponOrder.BuyDate = DateTimeToDateTimeString(order.CreateTime);
                apiPponOrder.CouponCount = couponCount;
                apiPponOrder.MainImagePath = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : "";
                apiPponOrder.DisableSMS = ((order.Status & (int)GroupOrderStatus.DisableSMS) > 0);
                apiPponOrder.OrderTypeDesc = _config.EnableAppEvaluateSystem && (evaluateCount > 0)
                    ? "待評價: " + evaluateCount
                    : GetOrderTypeDesc(order);
                apiPponOrder.CouponType = PponFacade.GetDealCouponType(order);
                apiPponOrder.TotalCouponCount = order.TotalCount ?? 0;
                apiPponOrder.UnitPrice = order.GroupProductUnitPrice ?? 0;
                apiPponOrder.GroupCouponAppChannel = order.GroupCouponAppStyle;
                apiPponOrder.GroupCouponType = GetGroupCouponType(filterTypes);
                apiPponOrder.IsSettlementDeal = isSettlementDeal;
                apiPponOrder.EvaluateCount = evaluateCount;
                apiPponOrder.ActualShippingDate = actualShippingDate;
                apiPponOrder.LastShippingDate = lastShippingDate;
                apiPponOrderList.Add(apiPponOrder);
            }
            return apiPponOrderList;
        }

        /// <summary>
        /// 依據會員帳號，回傳會員各類別訂單的數量
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static List<ApiCouponListMainGroup> ApiCouponListMainGroupGetListByUser(int userId)
        {
            List<ApiCouponListMainGroup> rtn = new List<ApiCouponListMainGroup>();
            //查詢訂單，現階段APP不處理品生活訂單
            var orders = MemberFacade.ViewCouponListMainGetListByUser(0, 0, userId, CouponListFilterType.None,
                                                                             Guid.Empty, true, false);
            //依據顯示順序產生物件
            rtn.Add(new ApiCouponListMainGroup() { Count = 0, FilterType = CouponListFilterType.NotCompleted });
            rtn.Add(new ApiCouponListMainGroup() { Count = 0, FilterType = CouponListFilterType.NotUsed });
            rtn.Add(new ApiCouponListMainGroup() { Count = 0, FilterType = CouponListFilterType.Used });
            rtn.Add(new ApiCouponListMainGroup() { Count = 0, FilterType = CouponListFilterType.Product });
            rtn.Add(new ApiCouponListMainGroup() { Count = 0, FilterType = CouponListFilterType.Event });
            rtn.Add(new ApiCouponListMainGroup() { Count = 0, FilterType = CouponListFilterType.Cancel });

            foreach (var order in orders)
            {
                foreach (var apiCouponListMainGroup in rtn)
                {
                    if (MemberFacade.ViewCouponListMainIsFilterType(order, apiCouponListMainGroup.FilterType))
                    {
                        apiCouponListMainGroup.Count++;
                    }
                }
            }
            //全部的類別最後再加進去
            rtn.Add(new ApiCouponListMainGroup() { Count = orders.Count, FilterType = CouponListFilterType.None });

            return rtn.ToList();//只回傳有資料的部分
        }

        /// <summary>
        /// [v1.0] 訂單列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<ApiCouponListMain> ApiPponOrderListItemGetListByFilterType(int userId, CouponListFilterType type)
        {
            //查詢訂單，現階段一律排除品生活的檔次
            var orderList = MemberFacade.ViewCouponListMainGetListByUser(0, 0, userId, type, Guid.Empty, true, false);

            List<ApiCouponListMain> rtn = new List<ApiCouponListMain>();

            foreach (var order in orderList.OrderByDescending(x => x.CreateTime))
            {
                DateTime useStartDate = order.BusinessHourDeliverTimeS == null
                                            ? DateTime.MinValue
                                            : order.BusinessHourDeliverTimeS.Value;
                DateTime useEndDate = order.BusinessHourDeliverTimeE == null
                                            ? useStartDate
                                            : order.BusinessHourDeliverTimeE.Value;
                useEndDate = order.ChangedExpireDate == null ? useEndDate : order.ChangedExpireDate.Value;

                ApiCouponListMain item = new ApiCouponListMain();
                item.OrderGuid = order.Guid;
                List<CouponListFilterType> filterTypes = MemberFacade.GetViewCouponListMainGetCountFilterTypes(order);
                item.CountType = filterTypes.Count > 0 ? filterTypes.First() : CouponListFilterType.None;
                item.CouponName = order.CouponUsage;
                item.UseStartDate = DateTimeToDateTimeString(useStartDate);
                item.UseEndDate = DateTimeToDateTimeString(useEndDate);
                item.BuyDate = DateTimeToDateTimeString(order.CreateTime);
                //item.CouponCount = order.TotalCount == null ? 0 : order.TotalCount.Value;
                item.CouponCount = order.RemainCount == null ? 0 : order.RemainCount.Value;


                string imagePathData = order.ImagePath;
                //判斷訂單是否為多檔次子檔的訂單，且子檔未設定圖片
                if (((order.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0) && (string.IsNullOrWhiteSpace(imagePathData)))
                {
                    //多檔次子檔訂單需取得主檔的圖片
                    CouponEventContent mainDealContent = _pponProvider.CouponEventContentGetBySubDealBid(order.BusinessHourGuid);
                    if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                    {
                        imagePathData = mainDealContent.ImagePath;
                    }
                }
                string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);
                item.MainImagePath = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : "";
                item.DisableSMS = false;
                if (((order.Status & (int)GroupOrderStatus.DisableSMS) > 0))
                {
                    item.DisableSMS = true;
                }
                //訂單狀況的內容
                item.OrderTypeDesc = getOrderTypeDesc(order, item.CountType);

                item.CouponType = PponFacade.GetDealCouponType(order);

                rtn.Add(item);
            }
            return rtn;
        }

        /// <summary>
        ///[反向核銷] By商家查詢可核銷訂單
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static List<ApiOrderByVerifyStore> GetMemberOrdersBySellerGuid(int userId, Guid sellerGuid)
        {
            var apiOrderList = new List<ApiOrderByVerifyStore>();

            var orderList = MemberFacade.ViewCouponListMainGetListBySeller(0, 0, userId, sellerGuid);
            foreach (var order in orderList)
            {
                string imagePathData = order.ImagePath;
                //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                if ((order.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                {
                    //多檔次子檔訂單需取得主檔的圖片
                    CouponEventContent mainDealContent = _pponProvider.CouponEventContentGetBySubDealBid(order.BusinessHourGuid);
                    if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                    {
                        imagePathData = mainDealContent.ImagePath;
                    }
                }
                string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);

                DateTime useStartDate = order.BusinessHourDeliverTimeS == null
                                        ? DateTime.MinValue
                                        : order.BusinessHourDeliverTimeS.Value;
                DateTime useEndDate = order.BusinessHourDeliverTimeE == null
                                        ? useStartDate
                                        : order.BusinessHourDeliverTimeE.Value;
                useEndDate = order.ChangedExpireDate == null ? useEndDate : order.ChangedExpireDate.Value;

                var apiOrder = new ApiOrderByVerifyStore();
                apiOrder.OrderGuidOrAccessCode = order.Guid;
                apiOrder.ItemName = order.ItemName;
                apiOrder.MainImagePath = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : "";
                apiOrder.CouponCount = CouponFacade.GetCouponCanUsedCount(order);
                apiOrder.TotalCouponCount = order.TotalCount ?? 0;
                apiOrder.UseEndDate = DateTimeToDateTimeString(useEndDate);
                apiOrder.GroupCouponAppChannel = order.GroupCouponAppStyle;
                apiOrderList.Add(apiOrder);
            }

            return apiOrderList;
        }

        #endregion 會員訂單資料相關

        #region 整理退貨資料

        /// <summary>
        /// 整理退貨資料
        /// </summary>
        /// <param name="os"></param>
        /// <param name="orToRefund"></param>
        /// <param name="orToExchange"></param>
        /// <param name="ctatmrefund"></param>
        /// <returns></returns>
        protected static List<ApiRefundLog> GetRefundLogList(CashTrustLogCollection os, OrderReturnList orToRefund, OrderReturnList orToExchange, CtAtmRefund ctatmrefund)
        {
            List<ApiRefundLog> log = new List<ApiRefundLog>();

            #region orToRefund

            if (orToRefund.OrderGuid != Guid.Empty)
            {
                switch ((OrderReturnStatus)orToRefund.Status)
                {
                    case OrderReturnStatus.Processing:
                        string refundtype = string.Empty;
                        if (ctatmrefund.Si > 0)
                        {
                            refundtype = "- 指定帳號退款" + GetReturnAccountData(ctatmrefund);
                        }
                        else
                        {
                            refundtype = (Helper.IsFlagSet(orToRefund.Flag, OrderReturnFlags.RefundScashOnly))
                                             ? "(申請退購物金)"
                                             : "(申請刷卡退款)";
                        }
                        log.Add(new ApiRefundLog(orToRefund.CreateTime.ToString("yyyy/MM/dd"), string.Empty, 0, 0, 0, 0, 0,
                                              "等待處理中" + refundtype, false));
                        break;

                    case OrderReturnStatus.Success:
                        foreach (var item in os)
                        {
                            string isFreight = string.Empty;

                            if (Helper.IsFlagSet(item.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                            {
                                isFreight = (item.SpecialStatus == (int)TrustSpecialStatus.Freight)
                                                ? ("(運費" + "-ATM退款)")
                                                : "(ATM退款)";
                                log.Add(new ApiRefundLog(item.CreateTime.ToString("yyyy/MM/dd"), item.CouponSequenceNumber,
                                                      item.Pcash, item.Bcash, item.Scash, item.CreditCard, item.Atm,
                                                      item.ModifyTime.ToString("yyyy/MM/dd 退貨處理中") + isFreight +
                                                      GetReturnAccountData(ctatmrefund), true));
                            }
                            else
                            {
                                if (item.Status == (int)TrustStatus.Returned)
                                {
                                    isFreight = (item.SpecialStatus == (int)TrustSpecialStatus.Freight)
                                                    ? ("(運費" + "-退購物金)")
                                                    : "(退購物金)";
                                    log.Add(new ApiRefundLog(item.CreateTime.ToString("yyyy/MM/dd"), item.CouponSequenceNumber,
                                                          item.Pcash, item.Bcash, item.Scash + item.CreditCard + item.Atm, 0,
                                                          0,
                                                          item.ModifyTime.ToString("yyyy/MM/dd 退貨完成" + isFreight), true));
                                }
                                else if (item.Status == (int)TrustStatus.Refunded)
                                {
                                    isFreight = (item.SpecialStatus == (int)TrustSpecialStatus.Freight)
                                                    ? ("(運費" + "-現金退款)")
                                                    : "(現金退款)";
                                    log.Add(new ApiRefundLog(item.CreateTime.ToString("yyyy/MM/dd"), item.CouponSequenceNumber,
                                                          item.Pcash, item.Bcash, item.Scash, item.CreditCard, item.Atm,
                                                          item.ModifyTime.ToString("yyyy/MM/dd 退貨完成" + isFreight) +
                                                          GetReturnAccountData(ctatmrefund), true));
                                }
                            }
                        }
                        break;

                    case OrderReturnStatus.Failure:
                        log.Add(new ApiRefundLog(orToRefund.CreateTime.ToString("yyyy/MM/dd"), string.Empty, 0, 0, 0, 0, 0,
                                              "退貨失敗:" +
                                              ((ctatmrefund.Si == 0) ? string.Empty : (";" + ctatmrefund.FailReason)) +
                                              GetReturnAccountData(ctatmrefund), false));
                        break;
                }
            }

            #endregion orToRefund

            #region orToExchange

            if (orToExchange.OrderGuid != Guid.Empty)
            {
                switch ((OrderReturnStatus)orToExchange.Status)
                {
                    case OrderReturnStatus.SendToSeller:
                        log.Add(new ApiRefundLog(orToExchange.CreateTime.ToString("yyyy/MM/dd"), string.Empty, 0, 0, 0, 0, 0, "換貨處理中", true));
                        break;

                    case OrderReturnStatus.ExchangeProcessing:
                    case OrderReturnStatus.ExchangeSuccess:
                        for (int i = 0; i < orToExchange.ExchangeQuantity; i++)
                        {
                            log.Add(new ApiRefundLog(orToExchange.CreateTime.ToString("yyyy/MM/dd"), string.Empty, 0, 0, 0, 0, 0, Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (OrderReturnStatus)orToExchange.Status), true));
                        }
                        break;

                    case OrderReturnStatus.ExchangeFailure:
                        log.Add(new ApiRefundLog(orToExchange.CreateTime.ToString("yyyy/MM/dd"), string.Empty, 0, 0, 0, 0, 0, "換貨失敗", true));
                        break;

                    case OrderReturnStatus.ExchangeCancel:
                        log.Add(new ApiRefundLog(orToExchange.CreateTime.ToString("yyyy/MM/dd"), string.Empty, 0, 0, 0, 0, 0, "換貨取消", true));
                        break;
                }
            }

            #endregion orToExchange

            return log;
        }

        /// <summary>
        /// 整理退貨資料
        /// </summary>
        /// <param name="os"></param>
        /// <param name="returnForms"></param>
        /// <param name="orToExchange"></param>
        /// <param name="ctatmrefund"></param>
        /// <returns></returns>
        protected static List<ApiRefundLog> GetRefundLogList(CashTrustLogCollection os, IList<ReturnFormEntity> returnForms, CtAtmRefund ctatmrefund)
        {
            List<ApiRefundLog> log = new List<ApiRefundLog>();

            #region returnForm

            foreach (var returnForm in returnForms)
            {
                string requestedItems = returnForm.GetReturnSpec();
                string returnedItems = returnForm.GetRefundedSpec();
                bool isShowCash = (returnForm.ProgressStatus == ProgressStatus.Completed ||
                                   returnForm.ProgressStatus == ProgressStatus.CompletedWithCreditCardQueued);

                #region 退貨進度

                string returnProgress = string.Empty;

                switch (returnForm.ProgressStatus)
                {
                    case ProgressStatus.Processing:
                    case ProgressStatus.AtmQueueing:
                    case ProgressStatus.AtmQueueSucceeded:
                    case ProgressStatus.AtmFailed:
                    case ProgressStatus.ConfirmedForCS:
                    case ProgressStatus.ConfirmedForUnArrival:
                        returnProgress = "退貨處理中";
                        break;
                    case ProgressStatus.Retrieving:
                    case ProgressStatus.RetrieveToPC:
                    case ProgressStatus.RetrieveToCustomer:
                        returnProgress = "已前往收件";
                        break;
                    case ProgressStatus.ConfirmedForVendor:
                        returnProgress = "廠商確認中";
                        break;
                    case ProgressStatus.Completed:
                    case ProgressStatus.CompletedWithCreditCardQueued:
                        returnProgress = "退貨已完成";
                        break;
                    case ProgressStatus.Canceled:
                        returnProgress = "取消退貨";
                        break;
                    case ProgressStatus.Unreturnable:
                        returnProgress = "退貨失敗";
                        break;
                        break;
                }


                RefundedAmount summary = returnForm.GetRefundedAmount();

                #endregion 退貨進度

                var refundInfoList = new List<ApiPayInfo>();

                if (summary.PCash != 0)
                {
                    refundInfoList.Add(new ApiPayInfo(I18N.Message.PCash, summary.PCash));
                }
                if (summary.SCash != 0 || summary.Pscash != 0)
                {
                    refundInfoList.Add(new ApiPayInfo("購物金", 0, "購物金"));
                }
                if (summary.SCash != 0)
                {
                    refundInfoList.Add(new ApiPayInfo("17Life", summary.SCash, "購物金"));
                }
                if (summary.Pscash != 0)
                {
                    refundInfoList.Add(new ApiPayInfo(I18N.Message.PScash, summary.Pscash, "購物金"));
                }
                if (summary.BCash != 0)
                {
                    refundInfoList.Add(new ApiPayInfo(I18N.Message.BonusPoint, summary.BCash));
                }
                if (summary.CreditCard != 0)
                {
                    refundInfoList.Add(new ApiPayInfo(I18N.Message.CreditCard, summary.CreditCard));
                }
                if (summary.Atm != 0)
                {
                    refundInfoList.Add(new ApiPayInfo(I18N.Message.ATM, summary.Atm));
                }
                if (summary.LinePay != 0)
                {
                    refundInfoList.Add(new ApiPayInfo(Helper.GetEnumDescription(ThirdPartyPayment.LinePay), summary.LinePay));
                }
                if (summary.TaishinPay != 0)
                {
                    refundInfoList.Add(new ApiPayInfo(Helper.GetEnumDescription(ThirdPartyPayment.TaishinPay), summary.TaishinPay));
                }

                log.Add(new ApiRefundLog(returnForm.CreateTime.ToString("yyyy/MM/dd"), requestedItems, returnedItems,
                    summary.PCash,
                    summary.BCash,
                    summary.SCash,
                    summary.Pscash,
                    summary.CreditCard,
                    summary.Atm, summary.LinePay, summary.TaishinPay, returnProgress, isShowCash, refundInfoList));
            }

            #endregion returnForm

            return log;
        }

        private static List<ApiExchangeLog> GetExchangeLogList(IEnumerable<OrderReturnList> exchangeInfo)
        {
            var log = new List<ApiExchangeLog>();

            foreach (var exchange in exchangeInfo)
            {
                string requestedItems = exchange.Reason;
                switch ((OrderReturnStatus)exchange.Status)
                {
                    case OrderReturnStatus.SendToSeller:
                        log.Add(new ApiExchangeLog(exchange.CreateTime.ToString("yyyy/MM/dd"), requestedItems, "換貨處理中"));
                        break;

                    default:
                        log.Add(new ApiExchangeLog(exchange.CreateTime.ToString("yyyy/MM/dd"), requestedItems,
                            Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (OrderReturnStatus)exchange.Status)));
                        break;
                }
            }

            return log;
        }

        /// <summary>
        /// 整理銀行退款資料
        /// </summary>
        /// <param name="ctatmrefund"></param>
        /// <returns></returns>
        private static string GetReturnAccountData(CtAtmRefund ctatmrefund)
        {
            return (ctatmrefund.Si > 0)
                    ? "<br>銀行(分行)：" + ctatmrefund.BankName + "(" + ctatmrefund.BranchName + ")<br>匯款帳號：*********" + ctatmrefund.AccountNumber.Substring(9, 5)
                    : string.Empty;
        }

        #endregion 整理退貨資料

        #region 會員基本資料

        /// <summary>
        /// 取得會員基本資料
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="message"></param>
        /// <param name="withBuilding"></param>
        /// <returns></returns>
        public static ApiMemberDetail ApiMemberDetailGet(string userName, out string message, bool withBuilding)
        {
            message = string.Empty;
            //ViewMemberBuildingCity member = _memberProvider.ViewMemberBuildingCityGet(userName);
            Member member = _memberProvider.MemberGet(userName);
            if (!member.IsLoaded)
            {
                message = "查無會員資料";
                return null;
            }
            ApiMemberDetailV2 rtnMember = new ApiMemberDetailV2();
            rtnMember.FirstName = member.FirstName ?? "";
            rtnMember.LastName = member.LastName ?? "";
            rtnMember.UserEmail = member.UserEmail;
            rtnMember.PicUrl = MemberFacade.GetMemberPicUrl(MemberFacade.GetMember(userName));

            rtnMember.Address = LkAddress.Empty;
            if (member.BuildingGuid != null && member.BuildingGuid.Value != Guid.Empty &&
                member.BuildingGuid != MemberUtilityCore.DefaultBuildingGuid)
            {
                Building building = _locationProvider.BuildingGet(member.BuildingGuid.Value);
                if (building.IsLoaded)
                {
                    City town = _locationProvider.CityGet(building.CityId);
                    if (town.IsLoaded && town.ParentId != null)
                    {
                        rtnMember.Address.BuildingGuid = member.BuildingGuid.Value;
                        rtnMember.Address.TownshipId = building.CityId;
                        rtnMember.Address.CityId = town.ParentId.Value;
                    }
                }
            }

            //相容包含building的APP
            if (withBuilding)
            {
                if (rtnMember.Address.BuildingGuid == Guid.Empty)
                {
                    rtnMember.Address.AddressDesc = string.Empty;
                }
                else
                {
                    rtnMember.Address.AddressDesc =
                        member.CompanyAddress.Replace(
                            CityManager.CityTownShopBuildingStringGet(rtnMember.Address.BuildingGuid), string.Empty);
                }
            }
            else
            {
                if (rtnMember.Address.TownshipId == -1)
                {
                    rtnMember.Address.AddressDesc = string.Empty;
                }
                else
                {
                    rtnMember.Address.AddressDesc =
                        member.CompanyAddress.Replace(
                            CityManager.CityTownShopStringGet(rtnMember.Address.TownshipId), string.Empty);
                }
            }

            rtnMember.Mobile = member.Mobile ?? "";

            return rtnMember;
        }

        /// <summary>
        /// 修改會員基本資料
        /// </summary>
        /// <param name="newMember"></param>
        /// <param name="userName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool ApiMemberDetailSet(ApiMemberDetail newMember, string userName, out string message)
        {
            message = string.Empty;
            Member member = _memberProvider.MemberGet(userName);
            if (!member.IsLoaded)
            {
                message = "查無會員資料";
                return false;
            }
            if (newMember.FirstName != null)
            {
                member.FirstName = newMember.FirstName;
            }
            if (newMember.LastName != null)
            {
                member.LastName = newMember.LastName;
            }
            if (newMember.Mobile != null)
            {
                member.Mobile = newMember.Mobile;
            }
            if (newMember.Address != null)
            {
                BuildingCollection buildings = CityManager.BuildingListGetByTownshipId(newMember.Address.TownshipId);

                if (newMember.Address.TownshipId >= 0 && buildings.Count > 0)
                {
                    member.BuildingGuid = buildings.First().Guid;
                }
                else
                {
                    Building defaultBuilding = _locationProvider.BuildingGet(Building.Columns.BuildingName, "DEFAULT");
                    member.BuildingGuid = defaultBuilding.IsLoaded ? defaultBuilding.Guid : Guid.Empty;
                }

                member.CityId = newMember.Address.CityId;
                member.CompanyAddress = newMember.Address.CompleteAddress;
            }
            if (newMember is ApiMemberDetailV2)
            {
                member.UserEmail = ((ApiMemberDetailV2)newMember).UserEmail;
            }

            _memberProvider.MemberSet(member);
            return true;
        }

        /// <summary>
        /// 會員手機號碼註冊，更新基本資料
        /// </summary>
        /// <param name="registerInfo"></param>
        /// <param name="userName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool SaveMobileMemberRegisterInfo(ApiMobileMemberRegisterInfo registerInfo, string userName, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrWhiteSpace(userName))
            {
                logger.Info("SaveMobileMemberRegisterInfo 有傳入userName為null的情況, registerInfo=" +
                            ProviderFactory.Instance().GetSerializer().Serialize(registerInfo));
                message = "會員未登入，請重新整理網頁再操作";
                return false;
            }
            Member member = _memberProvider.MemberGet(userName);
            if (member.IsLoaded == false)
            {
                message = "查無會員資料";
                return false;
            }
            if (registerInfo.FirstName != null)
            {
                member.FirstName = registerInfo.FirstName;
            }
            if (registerInfo.LastName != null)
            {
                member.LastName = registerInfo.LastName;
            }
            if (registerInfo.CityId != null)
            {
                member.CityId = registerInfo.CityId.Value;
            }
            if (string.IsNullOrEmpty(registerInfo.UserEmail) == false)
            {
                if (RegExRules.CheckEmail(registerInfo.UserEmail))
                {
                    member.UserEmail = registerInfo.UserEmail;
                }
                else
                {
                    message = "Email格式不符合";
                    return false;
                }
            }
            else if (registerInfo.UserEmail == string.Empty)
            {
                member.UserEmail = string.Empty;
            }
            try
            {
                _memberProvider.MemberSet(member);
                return true;
            }
            catch (Exception)
            {
                message = "更新失敗";
                return false;
            }
        }

        /// <summary>
        /// 取得註冊時基本填寫的資料
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static ApiMobileMemberRegisterInfo GetMobileMemberRegisterInfo(string userName)
        {
            Member member = _memberProvider.MemberGet(userName);
            if (member.IsLoaded == false)
            {
                return null;
            }
            return new ApiMobileMemberRegisterInfo
            {
                FirstName = member.FirstName,
                LastName = member.LastName,
                CityId = member.CityId,
                UserEmail = member.UserEmail
            };
        }

        /// <summary>
        /// 取得收件人資料(memberdelivery中isdefault為false的)
        /// </summary>
        /// <returns></returns>
        public static List<ApiMemberDelivery> ApiMemberDeliveryGetByAddressee(string userName, out string message, bool withBuilding)
        {
            message = string.Empty;
            ViewMemberDeliveryBuildingCityCollection deliverys = _memberProvider.ViewMemberDeliveryBuildingCityGetCollection(userName);
            List<ApiMemberDelivery> rtnList = new List<ApiMemberDelivery>();
            foreach (ViewMemberDeliveryBuildingCity delivery in deliverys.OrderByDescending(x => x.Id))
            {
                ApiMemberDelivery apiDelivery = new ApiMemberDelivery();
                apiDelivery.DeliveryId = delivery.Id;

                if (delivery.ParentId != null && !string.IsNullOrEmpty(delivery.Address))
                {
                    apiDelivery.Address.CityId = delivery.ParentId == null ? -1 : delivery.ParentId.Value;
                    apiDelivery.Address.TownshipId = delivery.CityId;
                    apiDelivery.Address.BuildingGuid = delivery.BuildingGuid;
                    //相容包含building的APP
                    if (withBuilding)
                    {
                        apiDelivery.Address.AddressDesc =
                            delivery.Address.Replace(
                                CityManager.CityTownShopBuildingStringGet(delivery.BuildingGuid), string.Empty);
                    }
                    else
                    {
                        apiDelivery.Address.AddressDesc =
                                delivery.Address.Replace(CityManager.CityTownShopStringGet(delivery.CityId), string.Empty);
                    }
                }
                apiDelivery.AddressAlias = delivery.AddressAlias;
                apiDelivery.Mobile = delivery.Mobile;
                apiDelivery.ContactName = delivery.ContactName;

                rtnList.Add(apiDelivery);
            }
            return rtnList;
        }

        /// <summary>
        /// 刪除特定一筆memberdelivery紀錄
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="deliveryId"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool deleteMemberAddressee(string userName, int deliveryId, out string message)
        {
            message = string.Empty;
            MemberDelivery memberDelivery = _memberProvider.MemberDeliveryGet(deliveryId);
            if (!memberDelivery.IsLoaded)
            {
                message = "查無此收件人資料";
                return false;
            }

            int userId = MemberFacade.GetUniqueId(userName);

            if (userId == 0 || userId != memberDelivery.UserId)
            {
                message = "會員未登入";
                return false;
            }
            _memberProvider.MemberDeliveryDelete(memberDelivery);

            return true;
        }

        public static MemberDelivery InsertMemberAddressee(string userName, ApiMemberDelivery deliveryData)
        {
            //移除building的處理
            if (deliveryData.Address != null)
            {
                Guid buildingGuid = deliveryData.Address.BuildingGuid;
                if (Guid.Empty == buildingGuid)
                {
                    //給予預設值，該township的第一個路段
                    BuildingCollection buildings = CityManager.BuildingListGetByTownshipId(deliveryData.Address.TownshipId);
                    if (buildings.Count > 0)
                    {
                        deliveryData.Address.BuildingGuid = buildings.First().Guid;
                    }
                    else
                    {
                        //表示地址資料有錯，回傳異動失敗。
                        throw new Exception("PponDealApiManager.InsertMemberAddressee 地址資料錯誤");
                    }

                }
            }
            return MemberFacade.MemberAddresseeSet(userName, deliveryData);
        }

        public static ApiUserCashDetail ApiUserCashDetailGetByUserName(string userName)
        {
            ApiUserCashDetail detail = new ApiUserCashDetail();
            var mem = MemberFacade.GetMember(userName);

            double userBcash = MemberFacade.GetAvailableMemberPromotionValue(userName);
            double userBcashAmt = userBcash / 10; //紅利與現金的轉換 10紅利:1新台幣
            if (userBcashAmt < 1)
            {
                userBcashAmt = 0;
            }
            detail.BCash = userBcashAmt;
            detail.SCash = OrderFacade.GetSCashSum(userName);

            decimal userScash;
            decimal userPscash;
            detail.SCash = OrderFacade.GetSCashSum2(mem.UniqueId, out userScash, out userPscash);
            //PCash
            // the user has not signed-in using pez account
            MemberLink l = _memberProvider.MemberLinkGet(MemberFacade.GetUniqueId(userName), SingleSignOnSource.PayEasy);
            if (l != null && l.IsLoaded)
            {
                detail.PCash = PCashWorker.CheckUserRemaining(l.ExternalUserId);
            }



            //APP呼叫時，直接發送尚未領取的折價券資料
            OrderFacade.SendUncollectedDiscountList(mem.UniqueId, mem.UserName);

            var dcData = _orderProvider.ViewDiscountDetailCollectionGetByOwner(1, 1000, "", mem.UniqueId);
            var dc = new List<ApiDiscountCode>();

            //怕折價券過多，超過30天的就不要傳出去
            foreach (var item in dcData.Where(x => x.EndTime > DateTime.Now.AddDays(-30)))
            {
                var isAppOnly = Helper.IsFlagSet(item.Flag, DiscountCampaignUsedFlags.AppUseOnly);
                var desc = PromotionFacade.GetDiscountCategoryString(item.Id, true, isAppOnly);

                var tmp = new ApiDiscountCode
                {
                    Amount = item.Amount ?? 0.0m,
                    StartTime = DateTimeToDateTimeString(item.StartTime.Value),
                    EndTime = DateTimeToDateTimeString(item.EndTime.Value),
                    Code = item.Code,
                    Used = item.UseTime != null,
                    Type = DiscountCampaignType.PponDiscountTicket,
                    MinimumAmount = item.MinimumAmount ?? 0,
                    ChannelRestrict = desc
                };

                dc.Add(tmp);
            }

            detail.DiscountCodes = dc;

            return detail;
        }

        /// <summary>
        /// 出貨選項
        /// </summary>
        /// <param name="theDeal"></param>
        /// <returns></returns>
        public static List<DeliveryOption> GetDeliveryOption(IViewPponDeal theDeal)
        {
            bool enableIsp = theDeal.EnableIsp;

            var deliveryOptions = new List<DeliveryOption>();
            if (theDeal.DeliveryType == (int)DeliveryType.ToHouse)
            {
                //if (theDeal.EnableDelivery)
                //{
                deliveryOptions.Add(new DeliveryOption
                {
                    ProductDeliveryType = theDeal.IsWms ? ProductDeliveryType.Wms : ProductDeliveryType.Normal,
                    MaxQuantity = 0,
                    PromoTips = ""
                });
                //}

                if (enableIsp)
                {
                    var dealIspServiceChannel = PponFacade.GetIspServiceChannelBySellerGuid(theDeal.SellerGuid);

                    if (dealIspServiceChannel.Contains(ServiceChannel.FamilyMart) && _config.EnableFamiMap)
                    {
                        deliveryOptions.Add(new DeliveryOption
                        {
                            ProductDeliveryType = ProductDeliveryType.FamilyPickup,
                            MaxQuantity = theDeal.IspQuantityLimit,
                            PromoTips = _config.FamilyIspPromoTips
                        });
                    }

                    if (dealIspServiceChannel.Contains(ServiceChannel.SevenEleven) && _config.EnableSevenMap && _config.EnableSevenIsp)
                    {
                        deliveryOptions.Add(new DeliveryOption
                        {
                            ProductDeliveryType = ProductDeliveryType.SevenPickup,
                            MaxQuantity = theDeal.IspQuantityLimit,
                            PromoTips = _config.SevenIspPromoTips
                        });
                    }
                }
            }
            return deliveryOptions;
        }

        /// <summary>
        /// 取得會員最後取貨店家及地址
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static LastMemberDelivery GetLastMemberDelivery(int userId)
        {
            var checkoutInfo = _memberProvider.MemberCheckoutInfoGet(userId);
            if (!checkoutInfo.IsLoaded)
            {
                return new LastMemberDelivery();
            }

            var lastInfo = new LastMemberDelivery();

            lastInfo.ProductDeliveryType = checkoutInfo.LastProductDeliveryType;
            lastInfo.DeliveryId = checkoutInfo.DeliveryId ?? -1;

            lastInfo.FamilyStoreInfo = new PickupStore
            {
                StoreId = checkoutInfo.FamilyStoreId ?? string.Empty,
                StoreName = checkoutInfo.FamilyStoreName ?? string.Empty,
                StoreAddr = checkoutInfo.FamilyStoreAddr ?? string.Empty,
                StoreTel = checkoutInfo.FamilyStoreTel ?? string.Empty,
            };

            lastInfo.SevenStoreInfo = new PickupStore
            {
                StoreId = checkoutInfo.SevenStoreId ?? string.Empty,
                StoreName = checkoutInfo.SevenStoreName ?? string.Empty,
                StoreAddr = checkoutInfo.SevenStoreAddr ?? string.Empty,
                StoreTel = checkoutInfo.SevenStoreTel ?? string.Empty,
            };
            return lastInfo;
        }

        #endregion

        #region 其他未分類 public 

        public static BehaviorType GetDealBehaviorType(IViewPponDeal deal)
        {
            if (deal.CategoryIds != null && deal.CategoryIds.Contains(HxRootCategory._PIINLIFE_CATEGORY_ID))
            {
                return BehaviorType.PiinLife;
            }
            else if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.SKMDeal))
            {
                return BehaviorType.Skm;
            }
            else if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal))
            {
                return BehaviorType.Fami;
            }
            return BehaviorType.Ppon;
        }

        /// <summary>
        /// 去除HTML标记
        /// </summary>
        /// <param name="htmlstring">包括HTML </param>
        /// <returns>已经去除后的文字 </returns>
        public static string NoHTML(string htmlstring)
        {
            //檢查輸入的資料，若為空白或null，直接回傳空字串
            if (string.IsNullOrWhiteSpace(htmlstring))
            {
                return string.Empty;
            }
            int startIndex = htmlstring.IndexOf("<");
            int endIndex = htmlstring.IndexOf(">");
            while (startIndex >= 0 && endIndex > startIndex)
            {
                int length = endIndex - startIndex + 1;
                htmlstring = htmlstring.Remove(startIndex, length);
                startIndex = htmlstring.IndexOf("<");
                endIndex = htmlstring.IndexOf(">");
            }
            htmlstring = htmlstring.Replace("\t", "");
            htmlstring = htmlstring.Replace("\r\n\r\n", "\r\n");
            if (htmlstring.IndexOf("\r\n") == 0)
            {
                htmlstring = htmlstring.Remove(0, 2);
            }
            //轉換特殊字元
            htmlstring = HttpUtility.HtmlDecode(htmlstring);

            return htmlstring;
        }

        /// <summary>
        /// 取得未轉換為短網址的分享連結，
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="userId"></param>
        /// <param name="promoUrl"></param>
        /// <returns></returns>
        public static PponDealPromoUrlType TryGetPponDealPromoUrl(Guid bid, int? userId, out string promoUrl)
        {
            promoUrl = string.Empty;

            if (userId != null)
            {
                MemberLinkCollection memberLinks = _memberProvider.MemberLinkGetList(userId.Value);

                if (memberLinks != null && memberLinks.Count > 0)
                {
                    MemberLink memberLink = memberLinks[0];
                    promoUrl = string.Format(_sysConfProvider.SiteUrl + PponPromoPageUrl + "?bid={0},{1}|{2}"
                        , bid.ToString(), memberLink.ExternalOrg, memberLink.ExternalUserId);
                    return PponDealPromoUrlType.BonusFeedback;
                }
            }

            promoUrl = string.Format(_sysConfProvider.SiteUrl + PponDealPageUrl + "?bid={0}", bid.ToString());
            return PponDealPromoUrlType.OnlyShare;
        }

        /// <summary>
        /// 依據檔次內容決定顯示的檔次名稱
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static string GetAppDealName(IViewPponDeal deal)
        {
            return GetAppDealName(deal.CouponUsage, deal.AppTitle);
        }

        /// <summary>
        /// 依據會員收藏資料的物件，回傳檔次名稱
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static string GetAppDealName(ViewMemberCollectDealContent deal)
        {
            return GetAppDealName(deal.CouponUsage, deal.AppTitle);
        }

        public static List<ApiPponSimpleData> GetApiPponSimpleData()
        {
            var result = new List<ApiPponSimpleData>();

            var tempAllDeals = ViewPponDealManager.DefaultManager.ViewPponDealGetList();


            foreach (var deal in tempAllDeals)
            {
                var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
                //複數的店家資訊
                var storeArr = ProviderFactory.Instance().GetSerializer()
                    .Deserialize<AvailableInformation[]>(contentLite.Availability);

                if (storeArr is AvailableInformation[])
                {
                    foreach (var store in storeArr)
                    {
                        var storeDetail = store as AvailableInformation;
                        if (storeDetail == null) { continue; }

                        if (storeDetail.HasLongLat)
                        {
                            result.Add(new ApiPponSimpleData
                            {
                                Bid = deal.BusinessHourGuid.ToString(),
                                ItemName = deal.ItemName,
                                StoreName = store.N,
                                StoreLocationLat = store.Latitude,
                                StoreLocationLng = store.Longitude,
                            });
                        }
                    }
                }
            }

            return result;
        }

        public static AppSearchResult GetAppSearchResult(string queryString, bool isNewPrototype = false)
        {
            var result = new AppSearchResult { };
            var dealList = new List<PponDealSynopsis>();

            queryString = HttpUtility.UrlDecode(queryString, Encoding.UTF8);
            string queryLog = string.Empty;
            List<IViewPponDeal> searchedMainDealGuids = SearchFacade.GetPponMainDealsByQueryString(queryString, null, out queryLog);

            //有搜尋結果
            if (searchedMainDealGuids.Any())
            {
                result.HasResult = true;
                dealList = PponDealApiManager.PponDealSynopsesGetListByBids(searchedMainDealGuids, isNewPrototype, true);
                result.ResultCount = dealList.Count;
            }
            //找不到
            else
            {
                var categories = new List<int>()
                {
                    CategoryManager.Default.Delivery.CategoryId,
                    //CategoryManager.Default.Travel.CategoryId,
                    //CategoryManager.Default.Beauty.CategoryId,
                    //CategoryManager.Default.Family.CategoryId,                
                };

                List<MultipleMainDealPreview> previewList = new List<MultipleMainDealPreview>();
                foreach (int categoryId in categories)
                {
                    var tmp = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetNewListByCategory(categoryId);
                    previewList.AddRange(tmp);
                }


                foreach (var dealPreview in previewList.Distinct())
                {
                    if (dealPreview.DealCategoryIdList.Contains(FamiBeaconLockCategoryId))
                    {
                        //不顯示全家Beacon鎖定檔
                        continue;
                    }

                    //產生APP用的檔次說明物件
                    PponDealSynopsis synopsis = PponDealSynopsisGetByDeal(dealPreview.PponDeal, dealPreview.DealCategoryIdList, dealPreview.Sequence, VbsDealType.Ppon, isNewPrototype);

                    //已存在檔次不寫入，行銷檔全頻道都有勾選
                    if (!dealList.Any(x => x.Bid == synopsis.Bid))
                    {
                        dealList.Add(synopsis);
                    }
                }
            }

            result.DealList = dealList;

            return result;
        }

        /// <summary>
        /// 查詢檔次店家列表，用於地圖顯示
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="area"></param>
        /// <param name="category"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static List<ApiStoreMap> ApiStoreMapGetListForShow(int channel, int? area, int? category, string latitude, string longitude, int distance)
        {
            int categoryId = 100001;
            int tempChannel = channel;
            //如果channel = 100001  則挑選美食87 即買即用
            if (channel == categoryId)
            {
                channel = 87;
            }


            List<ApiStoreMap> rtnList = new List<ApiStoreMap>();
            //檢查有無傳入地理定位資訊
            SqlGeography geo = LocationFacade.GetGeographicPoint(latitude, longitude);
            if (geo == null)
            {
                return rtnList;
            }

            //查詢距離為負，一律設為1公里
            if (distance <= 0)
            {
                distance = 1000;
            }

            //若美食頻道沒選擇區域，則取得所在位置的areaId
            int? areaId = 0;
            if (area > 0)
            {
                areaId = area;
            }
            else
            {
                if (channel == 87) //若是美食頻道，需有areaId才能對應到pponcity
                {
                    City city;

                    if (_config.EnableBingMapAPI)
                    {
                        areaId = LocationFacade.GetAreaCityWithBingMap(double.Parse(latitude), double.Parse(longitude), out city).ChannelAreaId;
                    }
                    else
                    {
                        areaId = LocationFacade.GetAreaCityWithGoogleMap(double.Parse(latitude), double.Parse(longitude), out city).ChannelAreaId;
                    }

                }
            }

            List<int> categoryList = new List<int>();
            categoryList.Add((int)category);

            List<MultipleMainDealPreview> dealPreviewList = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(channel, areaId, categoryList);

            #region 即買即用--虛擬頻道  id:100001

            if (tempChannel == categoryId)
            {
                //DealTags:4, Categories:137 即買即用
                dealPreviewList = dealPreviewList.Where(x => x.DealCategoryIdList.Contains(137)).ToList();
            }
            #endregion


            PponCity pcity = PponCityGroup.GetPponCityByChannel(channel, areaId);

            //逐筆將資料過濾為ApiStoreMap
            foreach (var preview in dealPreviewList)
            {
                if (HiddenInApp(preview.PponDeal.BusinessHourGuid, pcity.CityId))
                {
                    //不顯示於APP
                    continue;
                }

                //產生APP用的地圖模式資料
                ViewPponStoreCollection vpsList = _pponProvider.ViewPponStoreGetListByBidWithNoLock(preview.PponDeal.BusinessHourGuid, VbsRightFlag.Location);

                //有經緯度資料，且計算後距離再目標範圍內的分店
                foreach (var vpsData in vpsList
                                            .Where<ViewPponStore>(a => (!string.IsNullOrWhiteSpace(a.Coordinate))
                                                && (LocationFacade.GetGeographyByCoordinate(a.Coordinate).STDistance(geo) <= distance).Value))
                {
                    ApiStoreMap sm = new ApiStoreMap();
                    sm.Bid = preview.PponDeal.BusinessHourGuid;
                    sm.DealName = GetAppDealName(preview.PponDeal);
                    sm.Price = preview.PponDeal.ItemPrice;

                    SqlGeography storeGeo = LocationFacade.GetGeographyByCoordinate(((ViewPponStore)vpsData).Coordinate);
                    if (storeGeo != null)
                    {
                        sm.Latitude = (double)storeGeo.Lat;
                        sm.Longitude = (double)storeGeo.Long;
                        sm.Distance = ((double)LocationFacade.GetGeographyByCoordinate(((ViewPponStore)vpsData).Coordinate).STDistance(geo)) / 1000;
                    }

                    rtnList.Add(sm);
                }
            }

            return rtnList;
        }

        #endregion  其他未分類 public 

        #region private

        /// <summary>
        /// 驗證cashTrustLog的status欄位，判斷憑證是否未使用
        /// </summary>
        /// <param name="status"></param>
        private static bool CouponIsNotUsed(TrustStatus status)
        {
            //憑證狀態若為信託準備中或已信託，則表示憑證未使用
            if (status == TrustStatus.Initial || status == TrustStatus.Trusted)
            {
                return true;
            }
            else
            {
                //其他類型為已使用、宅配、或退貨的狀態
                return false;
            }
        }

        /// <summary>
        /// 依據deal內容，產生要顯示的DEAL ICON 的id列表
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="dealType"></param>
        /// <returns></returns>
        public static List<int> GetDealTags(IViewPponDeal deal, int iconLimit, VbsDealType dealType = VbsDealType.Ppon)
        {
            List<int> dealTags = new List<int>();

            List<int> labelIconList = new List<int>();
            if (!string.IsNullOrEmpty(deal.LabelIconList))
            {
                labelIconList = deal.LabelIconList.Split(',').Select(int.Parse).ToList();
                labelIconList.Sort();
            }

            //優先出現1-24H到貨
            if (labelIconList.Contains((int)DealLabelSystemCode.TwentyFourHoursArrival))
            {
                dealTags.Add((int)DealLabelSystemCode.TwentyFourHoursArrival);
            }

            //優先出現2-折價券
            if (deal.DiscountUseType.GetValueOrDefault())
            {
                dealTags.Add((int)DealLabelSystemCode.DiscountCanUse);
            }

            //優先出現3、超商取貨
            if (deal.EnableIsp)
            {
                dealTags.Add((int)DealLabelSystemCode.IspShipping);
            }

            //排除公益檔次後，判斷破千破萬
            if (!Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
            {
                QuantityAdjustment adj = deal.GetAdjustedOrderedQuantity();
                if (adj.Quantity >= 1000)
                {
                    //破千熱銷
                    dealTags.Add((int)DealLabelSystemCode.SellOverOneThousand);
                }
            }

            if (deal.LabelIcons.Contains(DealLabelSystemCode.GroupCoupon))
            {
                dealTags.Add((int)DealLabelSystemCode.GroupCoupon);
            }

            if (deal.LabelIcons.Contains(DealLabelSystemCode.CanBeUsedImmediately))
            {
                dealTags.Add((int)DealLabelSystemCode.CanBeUsedImmediately);
            }

            if (dealType == VbsDealType.PiinLife)
            {
                int dealIconCount = 0;
                //目前Icon計數，品生活最多3個icon
                const int dealIconsLimit = 3;
                List<int> tmpdealTags = new List<int>();
                foreach (var iconCodeId in dealTags)
                {
                    switch ((DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), iconCodeId))
                    {
                        case DealLabelSystemCode.CanBeUsedImmediately:
                        case DealLabelSystemCode.PiinlifeEvent:
                        case DealLabelSystemCode.PiinlifeMoonEvent:
                        case DealLabelSystemCode.PiinlifeValentinesEvent:
                        case DealLabelSystemCode.PiinlifeFatherEvent:
                        case DealLabelSystemCode.PiinlifeMajiEvent:
                        case DealLabelSystemCode.PiinlifeRecommend:
                            tmpdealTags.Add((int)(DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), iconCodeId));
                            dealIconCount++;
                            break;
                        case DealLabelSystemCode.Visa:
                            if (deal.IsVisaNow)
                            {
                                tmpdealTags.Add((int)DealLabelSystemCode.Visa);
                                dealIconCount++;
                            }
                            break;
                        default:
                            break;
                    }
                    if (dealIconCount >= dealIconsLimit)
                    {
                        break;
                    }
                }
                return tmpdealTags;
            }
            if (deal.Tags.Count > iconLimit)
            {
                dealTags = dealTags.Take(iconLimit).ToList();
            }
            return dealTags;
        }

        /// <summary>
        /// 依據品生活deal內容，產生要顯示的Menu Tabs & contents
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        private static List<ApiPiinlifeMenuTab> ApiPiinlifeMenuTabForApiGet(IViewPponDeal deal, bool isNewPrototype)
        {
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
            List<ApiPiinlifeMenuTab> menuTabs = new List<ApiPiinlifeMenuTab>();
            int count = 0;

            if (deal == null)
            {
                return new List<ApiPiinlifeMenuTab>();
            }

            if (string.IsNullOrEmpty(contentLite.Remark) == true)
            {
                return new List<ApiPiinlifeMenuTab>();
            }

            // 各項頁籤說明
            foreach (string tab in contentLite.Remark.Split(new[] { "||" }, StringSplitOptions.None))
            {
                string[] item = tab.Split(new[] { "##" }, StringSplitOptions.None);
                if (item.Length == 2 && !string.IsNullOrWhiteSpace(item[0]))
                {
                    if (count >= 1) //第一筆資料為右邊說明不需回傳
                    {
                        ApiPiinlifeMenuTab rtn = new ApiPiinlifeMenuTab();
                        rtn.Title = item[0];
                        if (isNewPrototype == false)
                        {
                            rtn.Content = item[1];
                        }
                        rtn.Url = string.Format("https://{0}/mvc/ppon/PponDescription?bid={1}&type={2}&tab={3}", HttpContext.Current.Request.Url.Authority, deal.BusinessHourGuid, 3, HttpUtility.UrlEncode(item[0]));
                        menuTabs.Add(rtn);
                    }
                    count++;
                }
            }

            return menuTabs;
        }

        /// <summary>
        /// 依據品生活deal內容，產生要顯示的Right contents
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        private static string GetPiinlifeRightContent(IViewPponDeal deal)
        {
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
            if (deal == null)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(contentLite.Remark) == true)
            {
                return string.Empty;
            }

            // 各項頁籤說明
            foreach (string tab in contentLite.Remark.Split(new[] { "||" }, StringSplitOptions.None))
            {
                string[] item = tab.Split(new[] { "##" }, StringSplitOptions.None);
                if (item.Length == 2 && !string.IsNullOrWhiteSpace(item[0]))
                {
                    return item[1]; //第一筆資料為右邊說明
                }
            }

            return "";
        }

        /// <summary>
        /// 將日期欄位轉換成API用的字串
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private static string DateTimeToDateTimeString(DateTime date)
        {
            return ApiSystemManager.DateTimeToDateTimeString(date);
        }

        /// <summary>
        /// 成套商品Type
        /// </summary>
        private static List<NewCouponListFilterType> _groupCouponType = new List<NewCouponListFilterType>
        {
                            NewCouponListFilterType.GroupCoupon,
                            NewCouponListFilterType.GroupCouponNotUsed,
                            NewCouponListFilterType.GroupCouponReady,
        };

        /// <summary>
        /// 咖啡寄杯Type
        /// </summary>
        private static List<NewCouponListFilterType> _depositCoffee = new List<NewCouponListFilterType>
        {
                            NewCouponListFilterType.DepositCoffee,
                            NewCouponListFilterType.FamiCoffee,
        };

        /// <summary>
        /// App 辨識訂單型態 目前只分 商品券 與 好康
        /// </summary>
        /// <param name="filterTypes"></param>
        /// <returns></returns>
        public static GroupCouponType GetGroupCouponType(List<NewCouponListFilterType> filterTypes)
        {
            GroupCouponType groupCouponType = GroupCouponType.None;
            if (filterTypes.Contains(NewCouponListFilterType.GroupCoupon) ||
                filterTypes.Contains(NewCouponListFilterType.DepositCoffee) ||
                filterTypes.Contains(NewCouponListFilterType.FamiCoffee))
            {
                groupCouponType = GroupCouponType.Normal;
            }

            return groupCouponType;
        }


        private static Dictionary<Guid, ExpirationDateSelector> expirationComponents = new Dictionary<Guid, ExpirationDateSelector>();

        public static ExpirationDateSelector GetExpirationComponent(Guid orderGuid)
        {
            if (expirationComponents.ContainsKey(orderGuid))
            {
                return expirationComponents[orderGuid];
            }

            ExpirationDateSelector selector = new ExpirationDateSelector();
            selector.ExpirationDates = new PponUsageExpiration(orderGuid);

            expirationComponents.Add(orderGuid, selector);
            return selector;
        }

        /// <summary>
        /// 判斷憑證是否過期. 過期日會考慮到店家倒店以及變更使用期限的情況.
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        public static bool IsPastFinalExpireDate(ViewCouponListMain view)
        {
            if (view == null)
            {
                return false;
            }

            if (view.Department > 3)
            {
                return false;
            }

            ExpirationDateSelector selector = GetExpirationComponent(view.Guid);

            if (selector == null)
            {
                return false;
            }

            return selector.IsPastExpirationDate(DateTime.Now);
        }

        /// <summary>
        /// 訂單狀態說明文字
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static string GetOrderTypeDesc(ViewCouponListMain main)
        {
            bool isPpon = (!main.DeliveryType.HasValue || int.Equals((int)DeliveryType.ToShop, main.DeliveryType.Value));
            //是否為超取
            bool isIsp = main.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup ||
                         main.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup;
            //部分退貨
            int returnedQuantity = GetReturnedQuantity(main.Guid);
            bool isPartialCancel = returnedQuantity > 0 && main.TotalCount > returnedQuantity;
            //換貨訂單
            bool isExchangeOrder = (main.ExchangeStatus == (int)OrderLogStatus.ExchangeCancel ||
                                    main.ExchangeStatus == (int)OrderLogStatus.ExchangeFailure ||
                                    main.ExchangeStatus == (int)OrderLogStatus.ExchangeProcessing ||
                                    main.ExchangeStatus == (int)OrderLogStatus.ExchangeSuccess ||
                                    main.ExchangeStatus == (int)OrderLogStatus.SendToSeller);
            //退貨訂單
            bool isReturnOrder = (main.ReturnStatus != (int)ProgressStatus.Canceled && main.ReturnStatus != -1);
            //0元
            bool isZeroPponDeal = decimal.Equals(0, main.ItemPrice);

            string rtn = string.Empty;
            DateTime now = DateTime.Now;

            if (!isPpon)
            {
                //非憑證類型，一律設定預設值為購物商品，後續再依照狀況調整
                rtn = "購物商品";
            }
            if ((main.OrderStatus & (int)Core.OrderStatus.Cancel) > 0) //退貨完成
            {

                if (_config.EnableCancelPaidByIspOrder)
                {
                    rtn = isPartialCancel
                    ? "部分退貨"
                    : (main.OrderStatus & (int)OrderStatus.ATMOrder) > 0 && main.CreateTime.ToShortDateString() != now.ToShortDateString()
                        ? "逾期未付款"
                        : main.SellerGoodsStatus == (int)GoodsStatus.DcReturn
                            ? "訂單已取消"
                            : main.SellerGoodsStatus == (int)GoodsStatus.DcReceiveFail
                                ? "訂單異常，訂單已取消" : "訂單已取消";
                }
                else
                {
                    //rtn = isPartialCancel
                    //? "部分退貨"
                    //: (main.OrderStatus & (int)OrderStatus.ATMOrder) > 0 && main.CreateTime.ToShortDateString() != now.ToShortDateString()
                    //    ? "逾期未付款" : "訂單已取消";
                }

                //憑證類，部分退貨，還有可用的憑證時的處理
                if (isPpon && isPartialCancel && main.TotalCount != 0 && main.RemainCount > 0)
                {
                    rtn = string.Format("未使用:{0}", main.RemainCount);
                    if (IsPastFinalExpireDate(main))
                    {
                        rtn = "憑證已過期";
                    }
                }
            }
            else if (isExchangeOrder || isReturnOrder)  //退or換貨訂單
            {
                var returnModifyTime = main.ReturnModifyTime ?? DateTime.MinValue;
                var exchangeModifyTime = main.ExchangeModifyTime ?? DateTime.MinValue;

                //依最新一筆退換貨單紀錄之異動時間 抓取最新一筆 顯示訂單退換貨狀態
                if (exchangeModifyTime > returnModifyTime)
                {
                    #region 換貨部分

                    switch (main.ExchangeStatus)
                    {

                        case (int)OrderLogStatus.SendToSeller:
                        case (int)OrderLogStatus.ExchangeProcessing:
                            rtn = I18N.Phrase.ExchangeProcessing;
                            break;

                        case (int)OrderLogStatus.ExchangeSuccess:
                            rtn = I18N.Phrase.ExchangeSuccess;
                            break;

                        case (int)OrderLogStatus.ExchangeFailure:
                            rtn = I18N.Phrase.ExchangeFailure;
                            break;

                        case (int)OrderLogStatus.ExchangeCancel:
                            rtn = I18N.Phrase.ExchangeCancel;
                            break;
                    }

                    #endregion 換貨部分
                }
                else
                {
                    #region 退貨部分

                    switch (main.ReturnStatus)
                    {
                        case (int)ProgressStatus.Processing:
                        case (int)ProgressStatus.AtmQueueing:
                        case (int)ProgressStatus.AtmQueueSucceeded:
                        case (int)ProgressStatus.AtmFailed:
                        case (int)ProgressStatus.Retrieving:
                        case (int)ProgressStatus.ConfirmedForCS:
                        case (int)ProgressStatus.RetrieveToPC:
                        case (int)ProgressStatus.RetrieveToCustomer:
                        case (int)ProgressStatus.ConfirmedForVendor:
                        case (int)ProgressStatus.ConfirmedForUnArrival:
                            rtn = "退貨處理中";
                            break;

                        case (int)ProgressStatus.Completed:
                        case (int)ProgressStatus.CompletedWithCreditCardQueued:
                            #region Completed
                            rtn = (isPartialCancel) ? "部分退貨" : "訂單已取消";
                            if (isPpon)
                            {
                                //憑證檔，部分退貨，有未使用憑證
                                if (main.TotalCount != 0 && isPartialCancel && main.RemainCount > 0)
                                {
                                    if (IsPastFinalExpireDate(main))
                                    {
                                        rtn = "憑證已過期";
                                    }
                                    else
                                    {
                                        rtn = string.Format("未使用:{0}", main.RemainCount);
                                    }
                                }
                            }
                            else
                            {
                                //宅配檔
                                if (isPartialCancel)
                                {
                                    rtn = "部分退貨";

                                    if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                                    {
                                        rtn = " 已過鑑賞期";
                                    }
                                }
                            }
                            break;
                        #endregion Completed

                        case (int)ProgressStatus.Unreturnable:
                            #region 退貨失敗
                            rtn = "退貨失敗";
                            break;
                            #endregion 退貨失敗
                    }

                    #endregion 退貨部分
                }
            }
            else if (main.IsCanceling && _config.EnableCancelPaidByIspOrder)
            {
                rtn = "訂單取消中";
            }
            else if (main.BusinessHourOrderTimeE <= now)    //已結檔
            {
                int slug;
                if (int.TryParse(main.Slug, out slug) && slug < main.BusinessHourOrderMinimum)
                {
                    rtn = "未達門檻";
                }
                else
                {
                    if (isPpon)
                    {
                        if (main.TotalCount != 0)
                        {
                            rtn = "使用完畢";

                            if (main.RemainCount > 0)
                            {
                                var canUsedCount = CouponFacade.GetCouponCanUsedCount(main);
                                if (canUsedCount > 0)
                                {
                                    rtn = string.Format("未使用:{0}", canUsedCount);
                                    if (IsPastFinalExpireDate(main))
                                    {
                                        rtn = "憑證已過期";
                                    }
                                }
                                else
                                {
                                    rtn = "使用完畢";
                                }
                            }
                        }
                        else if (IsPastFinalExpireDate(main))
                        {
                            rtn = "憑證已過期";
                        }
                    }
                    else
                    {
                        if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                        {
                            rtn = "已過鑑賞期";
                        }
                        else
                        {
                            //訂單狀態顯示出貨資訊
                            //檢查是否已出貨
                            if (isIsp) //超取
                            {
                                rtn = OrderFacade.GetIspGoodsStatusDesc(main);
                            }
                            else if (main.IsWms)
                            {
                                rtn = OrderFacade.GetOrderShipInfo(main).ShipStatusDesc;
                            }
                            else
                            {
                                if (PponOrderManager.CheckIsShipped(main.Guid, OrderClassification.LkSite))
                                {
                                    rtn = "已出貨";
                                }
                                else
                                {
                                    rtn = string.Empty;
                                }
                            }
                        }
                    }

                    if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 && (main.OrderStatus & (int)Core.OrderStatus.Complete) == 0) //ATM未付款，已達門檻不顯示退款
                    {
                        if (isPpon)
                        {
                            rtn = string.Empty;
                        }
                    }
                }
            }
            else    //尚在搶購中
            {
                if (isPpon)
                {
                    if (main.TotalCount != 0)
                    {
                        if (main.RemainCount > 0)
                        {
                            var canUsedCount = CouponFacade.GetCouponCanUsedCount(main);
                            if (canUsedCount > 0)
                            {
                                rtn = string.Format("未使用:{0}", canUsedCount);
                                if (IsPastFinalExpireDate(main))
                                {
                                    rtn = "憑證已過期";
                                }
                            }
                            else
                            {
                                rtn = "使用完畢";
                            }
                        }
                        else
                        {
                            rtn = "使用完畢";
                        }
                    }
                    //ATM
                    if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 && (main.OrderStatus & (int)Core.OrderStatus.Complete) == 0)
                    {
                        rtn = string.Empty;
                        if (!(main.CreateTime.Year == now.Year && main.CreateTime.Month == now.Month && main.CreateTime.Day == now.Day))
                        {
                            rtn = "逾期未付款";
                        }
                        else
                        {
                            rtn = "ATM待付款";
                        }
                    }
                }
                else
                {
                    if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                    {
                        rtn = "已過鑑賞期";
                    }
                    else
                    {
                        //訂單狀態顯示出貨資訊
                        //檢查是否已出貨      
                        if (isIsp) //超取
                        {
                            rtn = OrderFacade.GetIspGoodsStatusDesc(main);
                        }
                        else if (main.IsWms)
                        {
                            rtn = OrderFacade.GetOrderShipInfo(main).ShipStatusDesc;
                        }
                        else
                        {
                            if (PponOrderManager.CheckIsShipped(main.Guid, OrderClassification.LkSite))
                            {
                                rtn = "已出貨";
                            }
                            else
                            {
                                rtn = "準備出貨";
                            }
                        }

                        //ATM
                        if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 && (main.OrderStatus & (int)Core.OrderStatus.Complete) == 0)
                        {
                            if (!(main.CreateTime.Year == now.Year && main.CreateTime.Month == now.Month && main.CreateTime.Day == now.Day))
                            {
                                rtn = "逾期未付款";
                            }
                            else
                            {
                                rtn = "ATM待付款";
                            }
                        }
                    }
                }
            }
            //0元好康
            if (isZeroPponDeal)
            {
                rtn = string.Empty;

                if ((main.Status & (int)GroupOrderStatus.PEZevent) > 0 || (main.Status & (int)GroupOrderStatus.FamiDeal) > 0)
                {
                    if (main.PinType == (int)PinType.Single)
                    {
                        //如果只有一組序號則不顯示使用狀態
                        rtn = (IsPastFinalExpireDate(main)) ? "已過期" : "可使用";
                    }
                    else if (main.PrintCount == 0)
                    {
                        if (IsPastFinalExpireDate(main))
                        {
                            rtn = "已過期";
                        }
                        else
                        {
                            rtn = (main.CouponCodeType == (int)CouponCodeType.Pincode) ? "未使用" : "可使用";
                        }
                    }
                    else
                    {
                        rtn = "使用完畢";
                    }
                }
            }

            //公益檔
            if ((main.Status & (int)GroupOrderStatus.KindDeal) > 0)
            {
                rtn = "公益檔次";
            }
            return rtn;
        }

        private static int GetReturnedQuantity(Guid orderGuid)
        {
            CashTrustLogCollection ctCol = _memberProvider.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            return ctCol.Where(x => (x.Status == (int)TrustStatus.Returned || x.Status == (int)TrustStatus.Refunded) && !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).Count();
        }

        /// <summary>
        /// 訂單狀態說明文字
        /// </summary>
        /// <param name="main"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string getOrderTypeDesc(ViewCouponListMain main, CouponListFilterType type)
        {
            string rtn = string.Empty;
            DateTime now = DateTime.Now;
            switch (type)
            {
                case CouponListFilterType.Expired://使用期限已過，有憑證，已結檔，大於門檻，非取消訂單::ViewCouponListMain.Columns.Department + "<4
                    if (main.ItemPrice == 0 && main.PrintCount != null && main.PrintCount.Value > 0)
                    {
                        return "已列印";
                    }
                    rtn = "憑證已過期";
                    break;

                case CouponListFilterType.NotExpired://使用期限未過，有憑證，已結檔，大於門檻，非取消訂單
                    rtn = "";
                    break;

                case CouponListFilterType.NotUsed://尚有憑證，有憑證，已結檔，大於門檻，非取消訂單，大於0元
                    rtn = "未使用：" + main.RemainCount;
                    break;

                case CouponListFilterType.Used://憑證用完，有憑證，已結檔，大於門檻，非取消訂單
                    rtn = "憑證使用完畢";
                    break;

                case CouponListFilterType.Cancel://取消訂單
                    rtn = "取消訂單";
                    break;

                case CouponListFilterType.Product://無憑證，已結檔，大於門檻，非取消單 :::ViewCouponListMain.Columns.Department + ">3
                    if (main.ChangedExpireDate != null)
                    {
                        if (main.ChangedExpireDate.Value.AddDays(7) < now)
                        {
                            rtn = "已過鑑賞期";
                        }
                        else
                        {
                            rtn = "購物商品";
                        }
                    }
                    else if ((main.BusinessHourDeliverTimeE != null) && (main.BusinessHourDeliverTimeE.Value.AddDays(7) < now))
                    {
                        rtn = "已過鑑賞期";
                    }
                    else
                    {
                        rtn = "購物商品";
                    }
                    break;

                case CouponListFilterType.NotCompleted://未完成付款;包括逾期未付款與等待繼續付款 (ATM且非取消單且未完成單)

                    if (main.CreateTime.Year == now.Year && main.CreateTime.Month == now.Month && main.CreateTime.Day == now.Day)
                    {
                        rtn = "未完成付款";
                    }
                    else
                    {
                        rtn = "逾期未付款";
                    }
                    break;

                case CouponListFilterType.Event://優惠活動;包含全家優惠憑證、抽獎好康)]： 包含 0元好康 、全家專區檔次(排除已過期且未使用的訂單，保留已使用的 (訂單金額為0且未過期)
                    if (main.PrintCount > 0)
                    {
                        return "已列印";
                    }
                    break;

                case CouponListFilterType.Kind://公益檔次
                    rtn = string.Empty;
                    break;
            }
            return rtn;
        }

        public static string GetPponBaseIntroduction(IViewPponDeal deal, bool isOpenWebView = false)
        {
            // 公益檔不顯示任何預設權益說明
            if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, (int)GroupOrderStatus.KindDeal))
            {
                return string.Empty;
            }

            List<string> introductionList = new List<string>();

            if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands))
            {
                if (deal.IsHouseDealNewVersion() == false)
                {
                    introductionList.Add("●此購物商品恕不適用離島、外島地區");
                }
            }

            if (deal.DeliveryType != null && (DeliveryType)deal.DeliveryType == DeliveryType.ToShop)
            {
                if (isOpenWebView)
                {
                    introductionList.Add("<br /><font style='color: Red'>●此憑證由等值購物金兌換之</font>");
                }
                else
                {
                    introductionList.Add("●此憑證由等值購物金兌換之");
                }
                introductionList.Add("●本憑證不得與其他優惠合併使用，且恕無法兌換現金及找零");
                introductionList.Add("●好康憑證不限本人使用，使用時店家將會核對憑證編號，一組編號限用一次");
            }
            // 信託銀行文字說明
            TrustProvider trustProvider = Helper.GetBusinessHourTrustProvider(deal.BusinessHourStatus);
            switch (trustProvider)
            {
                case TrustProvider.TaiShin:
                    if (deal.DeliveryType != null && (DeliveryType)deal.DeliveryType == DeliveryType.ToShop)
                    {
                        introductionList.Add("●信託期間：發售日起一年內");
                        introductionList.Add("●本商品(服務)禮券所收取之金額，已存入發行人於台新銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用");
                    }
                    break;

                case TrustProvider.Sunny:
                    introductionList.Add("●信託期間：發售日起一年內");
                    introductionList.Add("●本商品(服務)禮券所收取之金額，已存入發行人於陽信銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用");
                    introductionList.Add("●信託履約禮券查詢網址 http://trust.sunnybank.com.tw/ (於收到兌換券後次日10點起即可查詢)");
                    break;

                case TrustProvider.Hwatai:
                    introductionList.Add("●信託期間：發售日起一年內");
                    introductionList.Add("●本商品(服務)禮券所收取之金額，已存入發行人於華泰銀行開立之信託專戶，專款專用。所稱專 用，係指供發行人履行交付商品或提供服務義務使用");
                    break;

                case TrustProvider.Initial:
                default:
                    break;
            }
            // 代收轉付文字說明
            if ((EntrustSellType)deal.EntrustSell != EntrustSellType.No)
            {
                introductionList.Add("●代收轉付收據將於核銷使用後開立");
            }

            if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
            {
                introductionList.Add("●成套票券商品為整套/組購入，故僅限於所有套/組憑證均兌換完畢才能享有完整優惠");
                introductionList.Add("●當有退貨情事發生時，將依其整套/組進貨規則進行剩餘面額價值進行退款");
            }

            if (isOpenWebView)
            {
                return string.Join("<br />", introductionList);
            }
            return string.Join("\r\n", introductionList);
        }

        private static string GetAppDealName(string couponUsage, string appTitle)
        {
            string rtn = couponUsage;
            if (!string.IsNullOrWhiteSpace(appTitle))
            {
                rtn = appTitle;
            }
            return rtn;
        }

        #endregion private


        public static void InsertFrontViewCount(int modelTrackingType, int modelViewCount, int userId, ApiUserDeviceType deviceType)
        {
            var frontViewCount = new FrontViewCount
            {
                UserId = userId,
                Date = DateTime.Now,
                AppFrontpageViewCount = modelViewCount,
                DeviceAndroid = deviceType == ApiUserDeviceType.Android,
                DeviceIos = deviceType == ApiUserDeviceType.IOS,
                CreateTime = DateTime.Now,
            };

            _pponEntityProvider.InsertFrontViewCount(frontViewCount);
        }

        #region 限時優惠

        public static ApiLimitedTimeSelectionResult GetLimitedTimeSelectionResult(int? topNum = null)
        {
            var result = new ApiLimitedTimeSelectionResult();
            var dealList = new List<PponDealSynopsisBasic>();

            DateTime limitedTimeSelectionDay = DateTime.Today.AddDays(-1);
            if (DateTime.Now >= DateTime.Today.AddHours(12))
            {
                limitedTimeSelectionDay = DateTime.Today;
            }

            LimitedTimeSelection limitedTimeSelection = EventFacade.GetEffectiveLimitedTimeSelections(limitedTimeSelectionDay, string.Empty);
            if (limitedTimeSelection.Id == 0)
            {
                return result;
            }
            result.StartTime = ApiSystemManager.DateTimeToDateTimeString(limitedTimeSelection.StartDate);
            result.EndTime = ApiSystemManager.DateTimeToDateTimeString(limitedTimeSelection.EndDate);
            result.BannerImagePath = string.Format("{0}?{1}", "https://www.17life.com/images/rushbuy/bn_mb.png", limitedTimeSelection.ModifyTime.Ticks);
            if (limitedTimeSelection.LimitedTimeSelectionDealViews.Any())
            {
                dealList = PponDealApiManager.PponDealSynopsesGetListByBids(limitedTimeSelection.LimitedTimeSelectionDealViews.Select(x => x.Bid).ToList(), true, true)
                    .Select(x => PponDealSynopsisToBasic(x)).ToList();
                foreach (var pponDealSynopsisBasic in dealList)
                {
                    pponDealSynopsisBasic.Seq = limitedTimeSelection.LimitedTimeSelectionDealViews.First(x => x.Bid == pponDealSynopsisBasic.Bid).Sequence;
                }
            }

            if (topNum.GetValueOrDefault() > 0 && dealList.Count > topNum)
            {
                result.DealList = dealList.OrderBy(x => x.Seq).Take(topNum.Value).ToList();
            }
            else
            {
                result.DealList = dealList.OrderBy(x => x.Seq).ToList();
            }
            return result;
        }

        #endregion
    }
}
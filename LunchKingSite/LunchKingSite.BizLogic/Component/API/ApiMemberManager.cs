﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component.API
{
    public class ApiMemberManager
    {
        private static readonly IMemberProvider mp;
        private static readonly ISysConfProvider config;
        private static readonly log4net.ILog log;

        static ApiMemberManager()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            config = ProviderFactory.Instance().GetConfig();
            log = log4net.LogManager.GetLogger(typeof(ApiPromoEventManager));
        }

        #region public method

        /// <summary>
        /// 取得會員所有的收藏檔次資料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="collectType"></param>
        /// <returns></returns>
        public static List<ApiMemberCollectDeal> GetApiMemberCollectDealList(int userId, MemberCollectDealType collectType = MemberCollectDealType.Coupon)
        {
            ViewMemberCollectDealContentCollection collectDealList = config.MemberCollectionDealAfterCloseMonths > 0
                ? mp.ViewMemberCollectDealContentGetList(userId, config.MemberCollectionDealAfterCloseMonths, collectType)
                : mp.ViewMemberCollectDealContentGetList(-1, -1,
                        ViewMemberCollectDealContent.Columns.CollectTime + " desc ",
                        ViewMemberCollectDealContent.Columns.UserId + " = " + userId,
                        ViewMemberCollectDealContent.Columns.CollectStatus + " = " + (int)MemberCollectDealStatus.Collected,
                        ViewMemberCollectDealContent.Columns.CollectType + " = " + (byte)collectType);

            return collectDealList.Select(GetApiMemberCollectDeal).ToList();
        }

        /// <summary>
        /// 取得SKM會員所有的收藏檔次資料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="collectType"></param>
        /// <returns></returns>
        public static List<ApiMemberCollectDeal> GetSkmMemberCollectDealList(int userId, MemberCollectDealType collectType = MemberCollectDealType.Coupon)
        {
            var checkDate = DateTime.Now.AddMonths(-config.SkmExpiredDataMonth);

            ViewMemberCollectDealContentCollection collectDealList = mp.ViewMemberCollectDealContentGetList(-1, -1,
                        ViewMemberCollectDealContent.Columns.CollectTime + " desc ",
                        ViewMemberCollectDealContent.Columns.BusinessHourOrderTimeE + " > " + checkDate,
                        ViewMemberCollectDealContent.Columns.UserId + " = " + userId,
                        ViewMemberCollectDealContent.Columns.CollectStatus + " = " + (int)MemberCollectDealStatus.Collected,
                        ViewMemberCollectDealContent.Columns.CollectType + " = " + (byte)collectType);

            return collectDealList.Select(GetApiMemberCollectDeal).ToList();
        }

        #endregion public method

        #region private method
        private static ApiMemberCollectDeal GetApiMemberCollectDeal(ViewMemberCollectDealContent collectDeal)
        {
            var discountStringModel = ViewPponDealManager.GetDealDiscountStringByCollectDealContent(collectDeal);

            ApiMemberCollectDeal rtn = new ApiMemberCollectDeal()
            {
                Bid = collectDeal.BusinessHourGuid,
                DealName = PponDealApiManager.GetAppDealName(collectDeal),
                ImagePath = PponFacade.GetPponDealFirstImagePath(collectDeal.ImagePath),
                Price = collectDeal.Price,
                OriPrice = collectDeal.OrigPrice,
                DealEndTime = ApiSystemManager.DateTimeToDateTimeString(collectDeal.BusinessHourOrderTimeE),
                DealStartTime = ApiSystemManager.DateTimeToDateTimeString(collectDeal.BusinessHourOrderTimeS),
                AppNotice = collectDeal.AppNotice,
                ShowExchangePrice = (collectDeal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0 && collectDeal.Price == 0,
                ExchangePrice = collectDeal.ExchangePrice,

                DiscountDisplayType = discountStringModel.DiscountDisplayType, // discountDisplayType
                DisplayPrice = discountStringModel.DisplayPrice, //displayprice, //for 17Life
                DiscountString = discountStringModel.DiscountString //discountString, //for SKM
            };

            if (Helper.IsFlagSet(collectDeal.GroupOrderStatus ?? 0, GroupOrderStatus.SKMDeal))
            {
                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(collectDeal.BusinessHourGuid);
                var skmPponDeal = PponDealApiManager.CreateSkmOrderPponDeal(vpd);

                //SKM App 專用
                rtn.AppPicUrl = skmPponDeal.AppPicUrl;
                rtn.ExperienceDealCollectCount = skmPponDeal.ExperienceDealCollectCount;
                rtn.ExchangeDealCount = skmPponDeal.ExchangeDealCount;
                rtn.IsExperience = skmPponDeal.IsExperience;
                rtn.IsSettlementDeal = skmPponDeal.IsSettlementDeal;
                rtn.NextSettlementTime = skmPponDeal.NextSettlementTime;
                rtn.SkmAvailabilities = skmPponDeal.SkmAvailabilities;
                rtn.DiscountType = skmPponDeal.DiscountType;
                rtn.Discount = skmPponDeal.Discount;
                rtn.SoldOut = vpd.OrderedQuantity >= vpd.OrderTotalLimit;
                var be = SkmFacade.GetBurningEventByBid(vpd.BusinessHourGuid);
                if (be.IsLoaded && be.BurningPoint > 0)
                {
                    rtn.IsBurningEvent = true;
                    rtn.BurningPoint = be.BurningPoint;
                }
            }

            return rtn;
        }
        #endregion private method
    }
}

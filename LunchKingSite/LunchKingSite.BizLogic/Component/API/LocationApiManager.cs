﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component.API
{
    public class LocationApiManager
    {
        private static readonly ILocationProvider _locationProvider;

        static LocationApiManager()
        {
            _locationProvider = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        }


        /// <summary>
        /// 回傳目前ApiCity資料的版本，與CityManager.CityDataVersion的值一致
        /// </summary>
        public static ApiVersionItem CityItemVersion
        {
            get { return new ApiVersionItem("CityTownships", CityManager.CityDataVersion.ToString()); }
        }

        public static List<ApiCity> GetApiCityList()
        {
            List<ApiCity> cityList = new List<ApiCity>();
            foreach (City city in CityManager.Citys.Where(x => x.Code != "SYS"))
            {
                ApiCity apiCity = new ApiCity();
                apiCity.Id = city.Id;
                apiCity.Name = city.CityName;
                apiCity.Island = !city.Visible;
                apiCity.TownshipsList = new List<ApiTownship>();

                var townships = CityManager.TownShipGetListByCityId(city.Id);
                foreach (City township in townships)
                {
                    ApiTownship apiTownship = new ApiTownship();
                    apiTownship.Id = township.Id;
                    apiTownship.Name = township.CityName;
                    apiTownship.Island = !township.Visible;
                    apiTownship.ZipCode = township.ZipCode;
                    apiCity.TownshipsList.Add(apiTownship);
                }

                cityList.Add(apiCity);
            }
            return cityList;
        }

    }
}

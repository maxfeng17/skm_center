﻿using iTextSharp.text;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;

namespace LunchKingSite.BizLogic.Component.API
{
    public class ApiVourcherManager
    {
        private static readonly IEventProvider _eventProvider;
        private static readonly ISellerProvider _sellerProvider;

        public const string ManagerNameForJob = "ApiVourcherManager";

        private static readonly ISysConfProvider _config;
        private static readonly ICacheProvider _cache;
        private static List<string> _cacheIndexer;
        private static object _locker = new object();

        private static List<ApiCitySellerSampleCategory> _citySellerCategory;//商家類型
        private static List<ApiSellerSampleCategory> _sellerCategory;//不分城市情況下的商家類別
        private static object _sellerCategoryLock = new object();

        private static readonly Dictionary<string, int> _citySortList;

        /// <summary>
        /// 清除快取
        /// </summary>
        public static void ClearCache()
        {
            lock (_locker)
            {
                foreach (string key in _cacheIndexer)
                {
                    bool success = _cache.Remove(key);
                }

                //清除cache indexer
                _cacheIndexer.Clear();
            }
        }

        /// <summary>
        /// 取得快取逾時時間
        /// </summary>
        /// <returns></returns>
        private static TimeSpan GetCacheTimeSpan()
        {
            //到午夜12點Expire
            return DateTime.Today.AddDays(1).Subtract(DateTime.Now);
        }

        /// <summary>
        /// 判斷是否有快取
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        private static Tuple<bool, T> TryGetCache<T>(string key)
        {
            var ret = _cache.Get<T>(key, _config.CacheItemCompress);
            if (ret != null)
            {
                lock (_locker)
                {
                    if (!_cacheIndexer.Contains(key))
                    {
                        _cacheIndexer.Add(key);

                    }
                }
                return Tuple.Create(true, ret);
            }


            return Tuple.Create(false, default(T));
        }

        /// <summary>
        /// 設定快取
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        private static bool SetCache<T>(string key, T val)
        {
            bool _suc = _cache.Set(key, val, GetCacheTimeSpan(), _config.CacheItemCompress);

            lock (_locker)
            {
                if (!_cacheIndexer.Contains(key) && _suc)
                {
                    _cacheIndexer.Add(key);
                }
            }
            return _suc;
        }

        /// <summary>
        /// 取得目前VourcherRegion資料的版本，版號目前與CityManager.CityGroupDataVersion的內容一致
        /// </summary>
        public static ApiVersionItem ApiVourcherRegionVersion
        {
            get { return new ApiVersionItem("VourcherRegionVersion", CityManager.CityGroupDataVersion.ToString()); }
        }

        static ApiVourcherManager()
        {
            _eventProvider = ProviderFactory.Instance().GetProvider<IEventProvider>();
            _sellerProvider = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _citySellerCategory = new List<ApiCitySellerSampleCategory>();
            _sellerCategory = new List<ApiSellerSampleCategory>();
            _config = ProviderFactory.Instance().GetConfig();
            _cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
            _cacheIndexer = new List<string>();

            _citySortList = new Dictionary<string, int>
                {
                    { "JL", 1 }, { "TP", 2 }, { "TY", 3 }, { "XZ", 4 }, { "TC", 5 },
                    { "NT", 6 }, { "YL", 7 }, { "TN", 8 }, { "GX", 9 }, { "IL", 10 }, { "HL", 11 },
                    { "PH",12}
                };
        }

        /// <summary>
        /// 取得優惠券區域
        /// </summary>
        /// <returns></returns>
        public static List<ApiVourcherRegion> ApiVourcherRegionGetList()
        {
            string key = "ApiVourcherRegionGetList";

            //Get Cache
            var result = TryGetCache<List<ApiVourcherRegion>>(key);
            if (result.Item1)
            {
                return result.Item2;
            }

            List<ApiVourcherRegion> rtnList = new List<ApiVourcherRegion>();
            //設定排列順序
            var groupList =
                CityManager.CityGroupGetListByType(CityGroupType.Vourcher).GroupBy(
                    x => new { x.RegionId, x.RegionName, x.RegionCode }).ToList();

            foreach (KeyValuePair<string, int> item in _citySortList)
            {
                KeyValuePair<string, int> item1 = item;
                var data = groupList.Where(x => x.Key.RegionCode == item1.Key).ToList();
                if (data.Count == 0)
                {
                    continue;
                }
                var groupData = data.First();
                ApiVourcherRegion vourcherRegion = new ApiVourcherRegion();

                vourcherRegion.CityId = groupData.Key.RegionId;
                string regionName = groupData.Key.RegionName;
                vourcherRegion.CityName = regionName.Length > 2 ? regionName.Substring(0, 2) : regionName;
                vourcherRegion.AreaNameList = groupData.Select(x => x.CityName).ToList();

                rtnList.Add(vourcherRegion);
            }

            //Add Cache
            SetCache(key, rtnList);

            return rtnList;
        }

        /// <summary>
        /// 依據傳入條件查詢優惠資料
        /// </summary>
        /// <param name="pageStart">第幾頁</param>
        /// <param name="pageLength">每頁資料筆數</param>
        /// <param name="regionId">區域編號(也就是CityId)</param>
        /// <param name="category">商家分類傳入NULL搜尋所有分類</param>
        /// <param name="sortType">排序方式，若為VourcherSortType.Near則regionId的條件失效</param>
        /// <param name="coordinates">經緯度，排序條件為VourcherSortType.Near時才有作用</param>
        /// <param name="distance">查詢距離，排序條件為VourcherSortType.Near時才有作用</param>
        /// <returns></returns>
        public static List<ApiVourcherStore> ApiVourcherStoreGetList(int pageStart, 
                                                                     int pageLength, 
                                                                     int regionId,
                                                                     SellerSampleCategory? category, 
                                                                     VourcherSortType sortType, 
                                                                     ApiCoordinates coordinates, 
                                                                     int? distance)
        {        
            List<ApiVourcherStore> vourcherStores = new List<ApiVourcherStore>();
            //檢查有無傳入地理定位資訊
            SqlGeography geo = LocationFacade.GetGeographicPoint(coordinates);// LocationFacade.GetGeographicPoint(latitude, longitude);
            GeographySearchParameter geography = null;
            if ((geo != null) && (distance != null))
            {
                geography = new GeographySearchParameter(geo, distance.Value);
                geography.Geography = geo;
            }

            ViewVourcherStoreFacadeCollection storeList = null;
            string key = string.Format("ViewVourcherStoreFacadeCollectionGet_{0}_{1}_{2}_{3}_{4}_{5}",
                                                                                            pageStart,
                                                                                            pageLength,
                                                                                            regionId,
                                                                                            category == null ? string.Empty : category.ToString(),
                                                                                            string.Empty,
                                                                                            string.Empty);

            var result = TryGetCache<ViewVourcherStoreFacadeCollection>(key);
            if (result.Item1)
            {
                storeList = result.Item2;
            }
            else
            {
                storeList = _eventProvider.ViewVourcherStoreFacadeCollectionGet(pageStart,
                                                                                pageLength,
                                                                                regionId,
                                                                                category,
                                                                                null,//不傳入geography，改為在程式中比對。
                                                                                string.Empty);
                //Add Cache
                SetCache(key, storeList);
            }

            IEnumerable<ViewVourcherStoreFacade> tempStroeList = storeList;
            if (distance.HasValue && geo != null)
            {
                tempStroeList = storeList
                    .Where<ViewVourcherStoreFacade>(a => (LocationFacade.GetGeographyByCoordinate(a.Coordinate)
                                                                        .STDistance(geo) <= (int) distance)
                                                             .Value);
            }

            foreach (var groupData in tempStroeList
                                        .GroupBy(x => new { x.SellerGuid, x.SellerId, x.SellerName, x.SellerLogoimgPath, x.SellerCategory }))
            {
                ApiVourcherStore store = new ApiVourcherStore();
                ViewVourcherStoreFacade storeFacade = groupData.FirstOrDefault();

                store.SellerGuid = groupData.Key.SellerGuid;
                store.SellerId = groupData.Key.SellerId;
                store.SellerName = groupData.Key.SellerName;

                var eventGroup = groupData.GroupBy(y => new { y.VourcherEventId, y.VourcherPageCount }).ToList();

                string eventPicUrl = string.Empty;
                if (storeFacade != null)
                {
                    eventPicUrl = storeFacade.EventPicUrl;
                }
                store.SellerLogoimgPath = VourcherFacade.GetSellerLogoImageUrl(groupData.Key.SellerLogoimgPath, eventPicUrl);

                store.SellerCategory = (SellerSampleCategory)groupData.Key.SellerCategory;
                store.AttentionCount = eventGroup.Sum(x => x.Key.VourcherPageCount == null ? 0 : x.Key.VourcherPageCount.Value);

                if (geography != null)
                {
                    double minDistance = groupData.Min(s => LocationFacade.GetDistance(geography.Geography, LocationFacade.GetGeographyByCoordinate(s.Coordinate)));
                    store.Distance = minDistance;
                    store.DistanceDesc = LocationFacade.GetDistanceDesc(minDistance);
                }
                else
                {
                    store.DistanceDesc = string.Empty;
                }

                if (storeFacade != null)
                {
                    store.VourcherContent = storeFacade.Contents;
                    store.EventId = storeFacade.VourcherEventId;
                }
                else
                {
                    store.VourcherContent = string.Empty;
                    store.EventId = 0;
                }

                DateTime startTime = DateTime.Now;
                if ((storeFacade != null) && (storeFacade.StartDate != null))
                {
                    startTime = storeFacade.StartDate.Value;
                }
                store.StartDate = ApiSystemManager.DateTimeToDateTimeString(startTime);
                store.VourcherCount = eventGroup.Count();
                vourcherStores.Add(store);
            }

            switch (sortType)
            {
                case VourcherSortType.Popular:
                    return vourcherStores.OrderByDescending(x => x.AttentionCount).ToList();

                case VourcherSortType.NewVourcher:
                    return vourcherStores.OrderByDescending(x => x.StartDate).ToList();

                case VourcherSortType.Near:
                    return vourcherStores.OrderBy(x => x.Distance).ToList();
            }

            return vourcherStores;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="category"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static List<ApiVourcherStore> ApiVourcherStoreGetListForMapShow(int regionId,
                                                                               SellerSampleCategory? category, 
                                                                               string latitude, 
                                                                               string longitude, 
                                                                               int distance)
        {
            List<ApiVourcherStore> vourcherStores = new List<ApiVourcherStore>();
            //檢查有無傳入地理定位資訊
            SqlGeography geo = LocationFacade.GetGeographicPoint(latitude, longitude);
            if (geo == null)
            {
                return vourcherStores;
            }

            //查詢距離為負，一律設為1公里
            if (distance <= 0)
            {
                distance = 1000;
            }

            GeographySearchParameter geography = new GeographySearchParameter(geo, distance);
            geography.Geography = geo;

            ViewVourcherStoreFacadeCollection storeList = null;
            //取得快取
            string key = string.Format("ViewVourcherStoreFacadeCollectionGet_{0}_{1}_{2}_{3}_{4}_{5}", 
                                                                                                    0, 
                                                                                                    0, 
                                                                                                    regionId, 
                                                                                                    category == null ? string.Empty : category.ToString(), 
                                                                                                    string.Empty, 
                                                                                                    string.Empty);

            var result = TryGetCache<ViewVourcherStoreFacadeCollection>(key);
            if (result.Item1)
            {
                storeList = result.Item2;
            }
            else
            {
                storeList = _eventProvider.ViewVourcherStoreFacadeCollectionGet(0,
                                                                                0,
                                                                                regionId,
                                                                                category,
                                                                                null,//不傳入geography，改為在程式中比對。
                                                                                string.Empty);
                //Add Cache
                SetCache(key, storeList);
            }


            foreach (var groupData in storeList
                                        .Where<ViewVourcherStoreFacade>(a => (LocationFacade.GetGeographyByCoordinate(a.Coordinate)
                                                                                            .STDistance(geo) <= distance)
                                                                                            .Value)
                                        .GroupBy(x => new { x.StoreGuid }))
            {
                ViewVourcherStoreFacade voucherStore = groupData.FirstOrDefault();
                ApiVourcherStore store = new ApiVourcherStore();

                store.SellerGuid = voucherStore.SellerGuid;
                store.SellerId = voucherStore.SellerId;
                store.SellerName = voucherStore.SellerName;

                store.SellerLogoimgPath = VourcherFacade.GetSellerLogoImageUrl(voucherStore.SellerLogoimgPath, voucherStore.EventPicUrl);

                store.SellerCategory = (voucherStore.SellerCategory == null
                                            ? SellerSampleCategory.Others
                                            : (SellerSampleCategory)voucherStore.SellerCategory);
                store.AttentionCount = voucherStore.PageCount;
                SqlGeography storeGeo = LocationFacade.GetGeographyByCoordinate(voucherStore.Coordinate); 
                double storedistance = LocationFacade.GetDistance(geo, storeGeo);
                string distanceDesc = LocationFacade.GetDistanceDesc(storedistance);
                store.DistanceDesc = distanceDesc;
                store.VourcherContent = voucherStore.Contents;
                store.EventId = voucherStore.VourcherEventId;
                DateTime startTime = DateTime.Now;
                if (voucherStore.StartDate != null)
                {
                    startTime = voucherStore.StartDate.Value;
                }
                if (storeGeo != null)
                {
                    store.Latitude = (double)storeGeo.Lat;
                    store.Longitude = (double)storeGeo.Long;

                    store.Coordinates = new ApiCoordinates((double) storeGeo.Long, (double) storeGeo.Lat);
                }
                store.StartDate = ApiSystemManager.DateTimeToDateTimeString(startTime);
                store.VourcherCount = groupData.GroupBy(y => y.VourcherEventId).Count();

                vourcherStores.Add(store);
            }

            return vourcherStores;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="category"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static List<ApiVourcherStore> ApiVourcherStoreGetListForMapShow(SellerSampleCategory? category, 
                                                                               string latitude, 
                                                                               string longitude, 
                                                                               int distance)
        {

            List<ApiVourcherStore> vourcherStores = new List<ApiVourcherStore>();
            //檢查有無傳入地理定位資訊
            SqlGeography geo = LocationFacade.GetGeographicPoint(latitude, longitude);
            if (geo == null)
            {
                return vourcherStores;
            }

            //查詢距離為負，一律設為1公里
            if (distance <= 0)
            {
                distance = 1000;
            }

            GeographySearchParameter geography = new GeographySearchParameter(geo, distance);
            geography.Geography = geo;

            //取得快取
            ViewVourcherStoreFacadeCollection storeList = null;

            string key = string.Format("ViewVourcherStoreFacadeGetList_{0}_{1}_{2}_{3}_{4}", 
                                                                                      0, 
                                                                                      0, 
                                                                                      category == null? string.Empty:category.ToString(), 
                                                                                      string.Empty, 
                                                                                      string.Empty);

            var result = TryGetCache<ViewVourcherStoreFacadeCollection>(key);
            if (result.Item1)
            {
                storeList = result.Item2;
            }
            else
            {
                storeList = _eventProvider.ViewVourcherStoreFacadeGetList(0, 
                                                                          0, 
                                                                          category,
                                                                          null, //不傳入geography，改為在程式中比對。
                                                                          string.Empty);
                //Add Cache
                SetCache(key, storeList);
            }

            //有經緯度資料，且計算後距離再目標範圍內的分店
            foreach (var groupData in storeList
                                        .Where<ViewVourcherStoreFacade>(a => (!string.IsNullOrWhiteSpace(a.Coordinate)) 
                                            && (LocationFacade.GetGeographyByCoordinate(a.Coordinate).STDistance(geo) <= distance).Value)
                                        .GroupBy(x => new { x.StoreGuid }))
            {
                ViewVourcherStoreFacade voucherStore = groupData.FirstOrDefault();
                ApiVourcherStore store = new ApiVourcherStore();

                store.SellerGuid = voucherStore.SellerGuid;
                store.SellerId = voucherStore.SellerId;
                store.SellerName = voucherStore.SellerName;

                store.SellerLogoimgPath = VourcherFacade.GetSellerLogoImageUrl(voucherStore.SellerLogoimgPath, voucherStore.EventPicUrl);

                store.SellerCategory = (voucherStore.SellerCategory == null
                                            ? SellerSampleCategory.Others
                                            : (SellerSampleCategory)voucherStore.SellerCategory);
                store.AttentionCount = voucherStore.PageCount;
                SqlGeography storeGeo = LocationFacade.GetGeographyByCoordinate(voucherStore.Coordinate);
                double storedistance = LocationFacade.GetDistance(geo, storeGeo);
                string distanceDesc = LocationFacade.GetDistanceDesc(storedistance);
                store.DistanceDesc = distanceDesc;
                store.VourcherContent = voucherStore.Contents;
                store.EventId = voucherStore.VourcherEventId;
                DateTime startTime = DateTime.Now;
                if (voucherStore.StartDate != null)
                {
                    startTime = voucherStore.StartDate.Value;
                }

                if (storeGeo != null)
                {
                    store.Latitude = (double)storeGeo.Lat;
                    store.Longitude = (double)storeGeo.Long;

                    store.Coordinates = new ApiCoordinates((double)storeGeo.Long, (double)storeGeo.Lat);
                }
                store.StartDate = ApiSystemManager.DateTimeToDateTimeString(startTime);
                store.VourcherCount = groupData.GroupBy(y => y.VourcherEventId).Count();

                vourcherStores.Add(store);
            }


            return vourcherStores;
        }

        public static List<ApiVourcherSortType> ApiVourcherSortTypeGetList()
        {
            string key = "ApiVourcherSortTypeGetList";

            var result = TryGetCache<List<ApiVourcherSortType>>(key);
            if (result.Item1)
            {
                return result.Item2;
            }


            List<ApiVourcherSortType> sortTypes = new List<ApiVourcherSortType>();
            //依照順序建立取得要回傳的資料 最新優惠、熱門關注、離我最近
            var sortList = new Dictionary<int, string>();
            sortList.Add((int)VourcherSortType.NewVourcher, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, VourcherSortType.NewVourcher));
            sortList.Add((int)VourcherSortType.Popular, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, VourcherSortType.Popular));
            sortList.Add((int)VourcherSortType.Near, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, VourcherSortType.Near));

            foreach (KeyValuePair<int, string> keyValuePair in sortList)
            {
                ApiVourcherSortType sortType = new ApiVourcherSortType();
                sortType.Text = keyValuePair.Value;
                sortType.Value = keyValuePair.Key;
                if ((VourcherSortType)keyValuePair.Key == VourcherSortType.Near)
                {
                    sortType.NeedLocate = true;
                }
                else
                {
                    sortType.NeedLocate = false;
                }
                sortTypes.Add(sortType);
            }

            //Add Cache
            SetCache(key, sortTypes);

            return sortTypes;
        }

        /// <summary>
        /// 依據sellerId取得活動進行中的優惠券
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="uniqueId"></param>
        /// <returns></returns>
        public static ApiVourcherEvent ApiVourcherEventGet(int eventId, string latitude, string longitude, int? uniqueId)
        {
            ViewVourcherSeller vourcherSeller = VourcherFacade.ViewVourcherSellerGetEnabledById(eventId);
            if (vourcherSeller == null)
            {
                return null;
            }
            // 關注數加1
            VourcherFacade.UpdateVourcherEventPageCountAddCount(vourcherSeller.Id, 1);
            //商家說明
            Seller seller = _sellerProvider.SellerGet(vourcherSeller.SellerGuid);
            string sellerDescription = string.Empty;
            if (seller.IsLoaded)
            {
                sellerDescription = seller.SellerDescription;
            }

            ApiVourcherEvent apiVourcherEvent = new ApiVourcherEvent();
            apiVourcherEvent.Id = vourcherSeller.Id;
            apiVourcherEvent.Contents = vourcherSeller.Contents;
            apiVourcherEvent.Instruction = vourcherSeller.Instruction;
            apiVourcherEvent.Restriction = vourcherSeller.Restriction;
            apiVourcherEvent.Others = vourcherSeller.Others;
            apiVourcherEvent.EndDate = ApiSystemManager.DateTimeToDateTimeString(vourcherSeller.EndDate.Value);
            apiVourcherEvent.Mode = (VourcherEventMode)vourcherSeller.Mode;
            apiVourcherEvent.MaxQuantity = vourcherSeller.MaxQuantity;
            apiVourcherEvent.CurrentQuantity = vourcherSeller.CurrentQuantity;
            apiVourcherEvent.PicUrl = ImageFacade.GetMediaPathsFromRawData(vourcherSeller.PicUrl, MediaType.PponDealPhoto).ToList();
            apiVourcherEvent.SellerName = vourcherSeller.SellerName;
            apiVourcherEvent.SellerDescription = sellerDescription;
            apiVourcherEvent.ConsumptionAvg = (SellerConsumptionAvg?)vourcherSeller.SellerConsumptionAvg; //平均店消費

            //檢查有無傳入地理定位資訊
            SqlGeography geo = LocationFacade.GetGeographicPoint(latitude, longitude);

            //取得分店快取
            ViewVourcherStoreCollection stores = null;

            string key = string.Format("ViewVourcherStoreCollectionGetByEventId_{0}_{1}_{2}_{3}", 0, 0, vourcherSeller.Id, string.Empty);

            var result = TryGetCache<ViewVourcherStoreCollection>(key);
            if (result.Item1)
            {
                stores = result.Item2;
            }
            else
            {
                stores = VourcherFacade.ViewVourcherStoreCollectionGetByEventId(0, 0, vourcherSeller.Id, string.Empty);
                //Add Cache
                SetCache(key, stores);
            }

            apiVourcherEvent.EventStores = new List<ApiVourcherEventStore>();
            foreach (ViewVourcherStore store in stores)
            {
                ApiVourcherEventStore apiStore = new ApiVourcherEventStore();
                apiStore.Guid = store.StoreGuid;
                apiStore.Id = store.Id;
                apiStore.StoreName = store.StoreName;
                apiStore.Address = store.City + store.Town + store.AddressString;
                apiStore.Phone = store.Phone;
                apiStore.OpenTime = store.OpenTime;
                apiStore.CloseDate = store.CloseDate;
                apiStore.Remarks = store.Remarks;
                apiStore.Mrt = store.Mrt;
                apiStore.Car = store.Car;
                apiStore.Bus = store.Bus;
                apiStore.OtherVehicles = store.OtherVehicles;
                apiStore.WebUrl = store.WebUrl;
                apiStore.FbUrl = store.FacebookUrl;
                apiStore.PlurkUrl = store.PlurkUrl;
                apiStore.BlogUrl = store.BlogUrl;
                apiStore.OtherUrl = store.OtherUrl;
                apiStore.CreditcardAvailable = store.CreditcardAvailable;
                SqlGeography storeGeo = LocationFacade.GetGeographyByCoordinate(store.Coordinate);
                if (storeGeo != null && storeGeo != SqlGeography.Null)
                {
                    apiStore.Longitude = (double)storeGeo.Long;
                    apiStore.Latitude = (double)storeGeo.Lat;
                }

                if (geo != null && !string.IsNullOrWhiteSpace(store.Coordinate))
                {
                    //若有提供經緯度，計算店舖與使用者的距離
                    double distance = LocationFacade.GetDistance(geo, storeGeo);
                    string distanceDesc = LocationFacade.GetDistanceDesc(distance);
                    apiStore.Distance = distance;
                    apiStore.DistanceDesc = distanceDesc;
                }
                else
                {
                    apiStore.Distance = 0;
                    apiStore.DistanceDesc = string.Empty;
                }

                apiVourcherEvent.EventStores.Add(apiStore);
            }
            apiVourcherEvent.AttentionCount = vourcherSeller.PageCount;
            //查詢使用者 優惠券登記狀態
            apiVourcherEvent.UserFavoriteState = null;
            if (uniqueId != null)
            {
                var vourchers = VourcherFacade.GetVourcherCollect(uniqueId.Value);
                ApiVourcherUserFavoriteState userData = new ApiVourcherUserFavoriteState();
                userData.EventId = vourcherSeller.Id;
                List<VourcherCollect> collects = vourchers.Where(x => x.EventId == vourcherSeller.Id).ToList();
                if (collects.Count > 0)
                {
                    VourcherCollect vourcher = collects.First();
                    userData.IsCollect = (VourcherCollectStatus)vourcher.Status == VourcherCollectStatus.Initial;
                }
                else
                {
                    userData.IsCollect = false;
                }
                userData.Count = vourchers.Count(x => x.Status == (int)VourcherCollectStatus.Initial);
                //查詢使用紀錄
                VourcherOrderCollection orders = _eventProvider.VourcherOrderGetList(0, 0, VourcherOrder.Columns.Id,
                    VourcherOrder.Columns.EventId + "=" + vourcherSeller.Id,
                    VourcherOrder.Columns.UniqueId + "=" + uniqueId,
                    VourcherOrder.Columns.Status + "=" + (int)VourcherOrderStatus.Used);
                userData.EventIsUsed = orders.Count > 0;//是否有使用過

                apiVourcherEvent.UserFavoriteState = userData;
            }

            return apiVourcherEvent;
        }

        /// <summary>
        /// 依據sellerId取得活動進行中的優惠券
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="sellerId"></param>
        /// <returns></returns>
        public static List<ApiVourcherEventSynopses> ApiVourcherEventSynopsesGetList(int pageStart, int pageLength, string sellerId)
        {
            string key = string.Format("ApiVourcherEventSynopsesGetList_{0}_{1}_{2}", pageStart, pageLength, sellerId);

            var result = TryGetCache<List<ApiVourcherEventSynopses>>(key);
            if (result.Item1)
            {
                return result.Item2;
            }

            List<ApiVourcherEventSynopses> rtnList = new List<ApiVourcherEventSynopses>();
            ViewVourcherSellerCollection vourcherSellers = VourcherFacade.VourcherEventSellerCollectionGetSellerId(pageStart, pageLength, sellerId, string.Empty);
            if (vourcherSellers.Count == 0)
            {
                return rtnList;
            }
            ViewVourcherSeller firstData = vourcherSellers.First();
            Seller seller = _sellerProvider.SellerGet(firstData.SellerGuid);
            string sellerDescription = string.Empty;
            if (seller.IsLoaded)
            {
                sellerDescription = seller.SellerDescription;
            }

            ViewVourcherStoreCollection vourcherStores = _eventProvider.ViewVourcherStoreGetList(0, 0, ViewVourcherStore.Columns.VourcherEventId, ViewVourcherStore.Columns.SellerGuid + "=" + firstData.SellerGuid);

            foreach (ViewVourcherSeller vourcherSeller in vourcherSellers)
            {
                ApiVourcherEventSynopses apiVourcherEvent = new ApiVourcherEventSynopses();
                apiVourcherEvent.Id = vourcherSeller.Id;
                apiVourcherEvent.Contents = vourcherSeller.Contents;
                apiVourcherEvent.Instruction = vourcherSeller.Instruction;
                apiVourcherEvent.Restriction = vourcherSeller.Restriction;
                apiVourcherEvent.EndDate = ApiSystemManager.DateTimeToDateTimeString(vourcherSeller.EndDate.Value);
                List<ViewVourcherStore> stores = vourcherStores.Where(x => x.VourcherEventId == vourcherSeller.Id).ToList();
                apiVourcherEvent.AttentionCount = vourcherSeller.PageCount;
                apiVourcherEvent.Mode = (VourcherEventMode)vourcherSeller.Mode;
                apiVourcherEvent.MaxQuantity = vourcherSeller.MaxQuantity;
                apiVourcherEvent.CurrentQuantity = vourcherSeller.CurrentQuantity;
                apiVourcherEvent.PicUrl = ImageFacade.GetMediaPathsFromRawData(vourcherSeller.PicUrl, MediaType.PponDealPhoto).ToList();
                apiVourcherEvent.SellerName = vourcherSeller.SellerName;
                apiVourcherEvent.SellerDescription = sellerDescription;

                rtnList.Add(apiVourcherEvent);
            }

            //Add Cache
            SetCache(key, rtnList);

            return rtnList;
        }

        /// <summary>
        /// 傳入會員編號，取出會員收藏優惠券的數量，依不同類別分別統計
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <returns></returns>
        public static List<ApiUserVourcherGroupMain> ApiUserVourcherGroupMainGetList(int uniqueId)
        {
            VourcherEventCollection eventList = _eventProvider.VourcherEventGetByUserCollect(uniqueId,
                                                                                             VourcherCollectType.Favorite,
                                                                                             VourcherCollectStatus.Initial);
            List<ApiUserVourcherGroupMain> rtn = new List<ApiUserVourcherGroupMain>();

            ApiUserVourcherGroupMain availableMain = new ApiUserVourcherGroupMain(VourcherGroupMainType.Available, eventList.Count(x => x.EndDate >= DateTime.Now));
            ApiUserVourcherGroupMain overdueMain = new ApiUserVourcherGroupMain(VourcherGroupMainType.Overdue, eventList.Count - availableMain.Count);

            rtn.Add(availableMain);
            rtn.Add(overdueMain);

            return rtn;
        }

        public static List<ApiVourcherEventSynopses> ApiVourcherEventSynopsesGetListUserAndByGroupType(int pageStart,
                                                                                                        int pageLength,
                                                                                                        int uniqueId, 
                                                                                                        VourcherGroupMainType type)
        {
            var rtnList = new List<ApiVourcherEventSynopses>();

            ViewVourcherSellerCollectCollection sellerCollectData = VourcherFacade.ViewVourcherSellerCollectGetList(pageStart,
                                                                                                      pageLength, uniqueId,
                                                                                                      string.Empty, type);
            var sellerCollectGroupDatas = sellerCollectData.GroupBy(x => new { x.SellerId, x.SellerName }).OrderByDescending(x => x.Max(y => y.CollectId));
            foreach (var groupData in sellerCollectGroupDatas)
            {
                foreach (ViewVourcherSellerCollect collect in groupData.OrderByDescending(x => x.CollectId))
                {
                    var eventSynopses = new ApiVourcherEventSynopses();
                    eventSynopses.Id = collect.Id;
                    eventSynopses.Contents = collect.Contents;
                    eventSynopses.Instruction = collect.Instruction;
                    eventSynopses.Restriction = collect.Restriction;
                    eventSynopses.EndDate = ApiSystemManager.DateTimeToDateTimeString(collect.EndDate.Value);
                    eventSynopses.AttentionCount = collect.PageCount;
                    eventSynopses.Mode = (VourcherEventMode)collect.Mode;
                    eventSynopses.MaxQuantity = collect.MaxQuantity;
                    eventSynopses.CurrentQuantity = collect.CurrentQuantity;
                    eventSynopses.PicUrl = ImageFacade.GetMediaPathsFromRawData(collect.PicUrl,
                                                    MediaType.PponDealPhoto).ToList();
                    eventSynopses.SellerName = collect.SellerName;

                    rtnList.Add(eventSynopses);
                }
            }

            return rtnList;
        }

        /// <summary>
        /// 將所有已過期的優惠券，取消收藏
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <returns></returns>
        public static int VourcherCollectSetDisabledForOverdueEvent(int uniqueId)
        {
            return VourcherFacade.VourcherCollectSetDisabledForOverdueEvent(uniqueId, VourcherCollectType.Favorite);
        }

        /// <summary>
        /// 依據傳入的會員編號，取出會員收藏的的優惠券資料
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="uniqueId">會員的唯一識別碼</param>
        /// <returns></returns>
        public static List<ApiVourcherEventSynopses> ApiVourcherEventSynopsesGetUserCollectListByUserId(int pageStart, 
                                                                                                        int pageLength, 
                                                                                                        int uniqueId)
        {
            var rtnList = new List<ApiVourcherEventSynopses>();

            ViewVourcherSellerCollectCollection sellerCollectData = VourcherFacade.ViewVourcherSellerCollectGetList(pageStart,
                                                                                                      pageLength, 
                                                                                                      uniqueId,
                                                                                                      string.Empty);
            var sellerCollectGroupDatas = sellerCollectData.GroupBy(x => new { x.SellerId, x.SellerName }).OrderByDescending(x => x.Max(y => y.CollectId));
            foreach (var groupData in sellerCollectGroupDatas)
            {
                foreach (ViewVourcherSellerCollect collect in groupData.OrderByDescending(x => x.CollectId))
                {
                    var eventSynopses = new ApiVourcherEventSynopses();
                    eventSynopses.Id = collect.Id;
                    eventSynopses.Contents = collect.Contents;
                    eventSynopses.Instruction = collect.Instruction;
                    eventSynopses.Restriction = collect.Restriction;
                    eventSynopses.EndDate = ApiSystemManager.DateTimeToDateTimeString(collect.EndDate.Value);
                    eventSynopses.AttentionCount = collect.PageCount;
                    eventSynopses.Mode = (VourcherEventMode)collect.Mode;
                    eventSynopses.MaxQuantity = collect.MaxQuantity;
                    eventSynopses.CurrentQuantity = collect.CurrentQuantity;
                    eventSynopses.PicUrl = ImageFacade.GetMediaPathsFromRawData(collect.PicUrl,
                                                    MediaType.PponDealPhoto).ToList();
                    eventSynopses.SellerName = collect.SellerName;

                    rtnList.Add(eventSynopses);
                }
            }

            return rtnList;
        }

        public static List<ApiSellerSampleCategory> ApiSellerSampleCategoryGetList()
        {
            string key = "ApiSellerSampleCategoryGetList";

            var result = TryGetCache<List<ApiSellerSampleCategory>>(key);
            if (result.Item1)
            {
                return result.Item2;
            }

            if (_sellerCategory == null || _sellerCategory.Count == 0)
            {
                lock (_sellerCategoryLock)
                {
                    if (_sellerCategory == null || _sellerCategory.Count == 0)
                    {
                        _sellerCategory = new List<ApiSellerSampleCategory>();

                        SystemCodeCollection codes =
                            SystemCodeManager.GetSystemCodeListByGroup(SellerSampleCategory.Cuisine);
                        string nowDateString = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        foreach (SystemCode systemCode in codes)
                        {
                            ApiSellerSampleCategory category = new ApiSellerSampleCategory();
                            category.Name = systemCode.CodeName;
                            category.Value = systemCode.CodeId;
                            category.ShortName = systemCode.ShortName;
                            //查詢優惠券數量
                            int count =
                                _eventProvider.ViewVourcherSellerGetSellerCountBySampleCategory(
                                    (SellerSampleCategory)systemCode.CodeId);
                            category.Count = count;

                            _sellerCategory.Add(category);
                        }
                    }
                }
            }

            //Add Cache
            SetCache(key, _sellerCategory);

            return _sellerCategory;
        }

        public static List<ApiSellerSampleCategory> ApiSellerSampleCategoryGetListByCityId(int cityId)
        {
            string key = string.Format("ApiSellerSampleCategoryGetListByCityId_{0}", cityId);

            var result = TryGetCache<List<ApiSellerSampleCategory>>(key);
            if (result.Item1)
            {
                return result.Item2;
            }

            City selectCity = CityManager.CityGetById(cityId);
            if (selectCity == null)
            {
                //此城市編號有錯，直接回傳空的list
                return new List<ApiSellerSampleCategory>();
            }

            List<ApiCitySellerSampleCategory> categoryList = _citySellerCategory.Where(x => x.CityId == cityId).ToList();
            ApiCitySellerSampleCategory rtnCityCategory;

            if (categoryList.Count == 0)
            {
                lock (_sellerCategoryLock)
                {
                    //再檢查一次
                    List<ApiCitySellerSampleCategory> subCheck = _citySellerCategory.Where(x => x.CityId == cityId).ToList();
                    if (subCheck.Count == 0)
                    {
                        ApiCitySellerSampleCategory cityCategory = new ApiCitySellerSampleCategory();
                        cityCategory.CityId = cityId;
                        SystemCodeCollection codes = SystemCodeManager.GetSystemCodeListByGroup(SellerSampleCategory.Cuisine);

                        foreach (SystemCode systemCode in codes)
                        {
                            ApiSellerSampleCategory category = new ApiSellerSampleCategory();
                            category.Name = systemCode.CodeName;
                            category.Value = systemCode.CodeId;
                            category.ShortName = systemCode.ShortName;
                            //查詢優惠券數量
                            int count =
                                _eventProvider.ViewVourcherSellerGetSellerCountBySampleCategoryAndCityId
                                ((SellerSampleCategory)systemCode.CodeId, cityId);
                            category.Count = count;

                            cityCategory.SellerCategoryList.Add(category);
                        }
                        _citySellerCategory.Add(cityCategory);
                        rtnCityCategory = cityCategory;
                    }
                    else
                    {
                        rtnCityCategory = subCheck.First();
                    }
                }
            }
            else
            {
                rtnCityCategory = categoryList.First();
            }

            //Add Cache
            SetCache(key, rtnCityCategory.SellerCategoryList);

            return rtnCityCategory.SellerCategoryList;
        }

        /// <summary>
        /// 清除暫存資料
        /// </summary>
        public static void ApiSellerSampleCategoryClear()
        {
            lock (_sellerCategoryLock)
            {
                _sellerCategory = new List<ApiSellerSampleCategory>();
                _citySellerCategory = new List<ApiCitySellerSampleCategory>();
                //清快取
                ClearCache();
            }
        }

        /// <summary>
        /// 重新查詢選單資料
        /// </summary>
        public static void ReloadDataManager()
        {
            var tmpSellerCategory = new List<ApiSellerSampleCategory>();
            var tmpCitySellerCategory = new List<ApiCitySellerSampleCategory>();

            //查詢不分區全部分類的統計資料
            SystemCodeCollection codes = SystemCodeManager.GetSystemCodeListByGroup(SellerSampleCategory.Cuisine);

            //查詢各分區全部分類的統計資料
            var groupList = CityManager.CityGroupGetListByType(CityGroupType.Vourcher).
                GroupBy(x => new { x.RegionId, x.RegionName, x.RegionCode }).ToList();
            foreach (var cityGroup in groupList)
            {
                ApiCitySellerSampleCategory cityCategory = new ApiCitySellerSampleCategory();
                cityCategory.CityId = cityGroup.Key.RegionId;
                foreach (SystemCode systemCode in codes)
                {
                    ApiSellerSampleCategory category = new ApiSellerSampleCategory();
                    category.Name = systemCode.CodeName;
                    category.Value = systemCode.CodeId;
                    category.ShortName = systemCode.ShortName;
                    //查詢優惠券數量
                    int count =
                        _eventProvider.ViewVourcherSellerGetSellerCountBySampleCategoryAndCityId
                            ((SellerSampleCategory)systemCode.CodeId, cityCategory.CityId);
                    category.Count = count;

                    cityCategory.SellerCategoryList.Add(category);
                }
                tmpCitySellerCategory.Add(cityCategory);
            }
            //查詢不分區全部分類的統計資料
            tmpSellerCategory = new List<ApiSellerSampleCategory>();

            foreach (SystemCode systemCode in codes)
            {
                ApiSellerSampleCategory category = new ApiSellerSampleCategory();
                category.Name = systemCode.CodeName;
                category.Value = systemCode.CodeId;
                category.ShortName = systemCode.ShortName;
                //查詢優惠券數量
                int count = tmpCitySellerCategory.Sum(
                    x => x.SellerCategoryList.Where(y => y.Value == systemCode.CodeId).Sum(y => y.Count));
                category.Count = count;

                tmpSellerCategory.Add(category);
            }

            _sellerCategory = tmpSellerCategory;
            _citySellerCategory = tmpCitySellerCategory;

            //清快取
            ClearCache();
        }

        /// <summary>
        /// 取得 優惠券行銷顯示 資料
        /// </summary>
        /// <returns></returns>
        public static List<ApiVourcherPromoData> ApiVourcherPromoDataGetList()
        {
            string key = "ApiVourcherPromoDataGetList";

            var result = TryGetCache<List<ApiVourcherPromoData>>(key);
            if (result.Item1)
            {
                return result.Item2;
            }


            List<ApiVourcherPromoData> rtnList = new List<ApiVourcherPromoData>();
            ViewVourcherPromoShowCollection promoShowCollection = _eventProvider.ViewVourcherPromoShowGetList(0, 0,
                                                                                                              ViewVourcherPromoShow.Columns.Rank, null);
            foreach (ViewVourcherPromoShow promoShow in promoShowCollection)
            {
                ApiVourcherPromoData promoData = new ApiVourcherPromoData();
                promoData.PromoId = promoShow.PromoId;
                promoData.Type = (VourcherPromoType)promoShow.PromoType;
                promoData.SellerId = promoShow.SellerId;
                promoData.SellerName = promoShow.SellerName;
                promoData.LogoPic = VourcherFacade.GetSellerLogoImageUrl(promoShow.SellerLogoimgPath, promoShow.PicUrl);
                promoData.EventId = promoShow.EventId;
                promoData.AttentionCount = promoShow.PageCount == null ? 0 : promoShow.PageCount.Value;
                promoData.VourcherContent = promoShow.Contents;

                rtnList.Add(promoData);
            }

            //Add Cache
            SetCache(key, rtnList);

            return rtnList;
        }
    }
}
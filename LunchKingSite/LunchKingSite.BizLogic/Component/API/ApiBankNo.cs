﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Component.API
{
    public class ApiBankNo
    {
        public ApiBankNo()
        {
            Branchs = new List<ApiBranchNo>();
        }

        /// <summary>
        /// 銀行帳號
        /// </summary>
        public string BankNo { get; set; }
        /// <summary>
        /// 銀行名稱
        /// </summary>
        public string BankName { get; set; }

        public List<ApiBranchNo> Branchs { get; set; } 
    }

    public class ApiBranchNo
    {
        /// <summary>
        /// 分行帳號
        /// </summary>
        public string BranchNo { get; set; }
        /// <summary>
        /// 分行名稱
        /// </summary>
        public string BranchName { get; set; }
    }
}

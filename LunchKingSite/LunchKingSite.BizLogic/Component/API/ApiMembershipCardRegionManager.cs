﻿using System.Globalization;
using System.Linq;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component.API
{
    public class ApiMembershipCardRegionManager
    {
        private static ISystemProvider _systemProvider;
        private static ILocationProvider _locationProvider;
        private static object _regionCatchLock = new object();
        private static ApiMembershipCardRegionListCatch _regionCatch;

        public const string SystemDataName = "ApiMembershipCardRegionListCatch";
        public static ApiVersionItem MembershipCardRegionVersion
        {
            get
            {
                if (_regionCatch == null || _regionCatch.RegionList.Count == 0)
                {
                    lock (_regionCatchLock)
                    {
                        //再檢查一次說不定等待的時間已經有資料了
                        if (_regionCatch == null || _regionCatch.RegionList.Count == 0)
                        {
                            ReloadManagerData();
                        }
                    }
                }
                return new ApiVersionItem("MembershipCardRegionVersion", _regionCatch.Version.ToString(CultureInfo.InvariantCulture));   
            }
        }

        public static ApiMembershipCardRegionListCatch RegionsCatchData
        {
            get
            {
                if (_regionCatch == null || _regionCatch.RegionList.Count == 0)
                {
                    lock (_regionCatchLock)
                    {
                        //再檢查一次說不定等待的時間已經有資料了
                        if (_regionCatch == null || _regionCatch.RegionList.Count == 0)
                        {
                            ReloadManagerData();
                        }
                    }
                }
                return _regionCatch;
            }
        }

        static ApiMembershipCardRegionManager()
        {
            _systemProvider = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _locationProvider = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            _regionCatch = new ApiMembershipCardRegionListCatch();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void ReloadManagerData()
        {
            SystemData data = _systemProvider.SystemDataGet(SystemDataName);
            if (data.IsLoaded)
            {
                //直接以systemData的資料為準
                JsonSerializer json = new JsonSerializer();
                try
                {
                    _regionCatch =
                        json.Deserialize<ApiMembershipCardRegionListCatch>(data.Data);
                }
                catch
                {
                    _regionCatch = GetCatchDataByDB();
                }
            }
            else
            {
                //沒有讀到資料。需重新查詢
                _regionCatch = GetCatchDataByDB();
            }
        }
        /// <summary>
        /// 清除CatchServer的資料
        /// </summary>
        public static void ClearDataAtCacheServer()
        {
            _systemProvider.SystemDataDelete(SystemDataName);
        }

        /// <summary>
        /// 由資料庫取得資料
        /// </summary>
        /// <returns></returns>
        public static ApiMembershipCardRegionListCatch GetCatchDataByDB()
        {
            CityCollection cityList = _locationProvider.CityGetListForMembershipCardStore();
            ApiMembershipCardRegionListCatch catchData = new ApiMembershipCardRegionListCatch();
            catchData.Version = cityList.Sum(x => x.BitNumber);
            foreach (var city in cityList)
            {
                ApiMembershipCardRegion region = new ApiMembershipCardRegion();
                region.RegionId = city.Id;
                region.RegionName = city.CityName;

                catchData.RegionList.Add(region);
            }
            //如果目前每有半筆資料，預設一筆台北的紀錄
            if (catchData.RegionList.Count == 0)
            {
                catchData.RegionList.Add(new ApiMembershipCardRegion()
                {
                    RegionId = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId,
                    RegionName = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityName
                });
            }
            //補上離我最近的項目
            catchData.RegionList.Add(new ApiMembershipCardRegion()
            {
                RegionId = 0,
                RegionName = "全台"
            });
            return catchData;
        }
    }
}

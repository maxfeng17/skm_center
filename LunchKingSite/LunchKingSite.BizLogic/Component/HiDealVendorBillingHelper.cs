﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.SsBLL;

namespace LunchKingSite.BizLogic.Component
{
    public class HiDealVendorBillingHelper : IVendorBillingHelper
    {
        public int Id { get; set; }

        private HiDealProduct dp = null;
        public VendorBillingModel VendorBillingModel
        {
            get
            {
                if (dp == null)
                {
                    dp = hp.HiDealProductGet(Id);
                }
                return (VendorBillingModel)dp.VendorBillingModel;
            }
        }

        public RemittanceType FinancialStaffPaymentMethod
        {
            get
            {
                if (dp == null)
                {
                    dp = hp.HiDealProductGet(Id);
                }
                return (RemittanceType)dp.RemittanceType;
            }
        }

        public bool IsPayToCompany
        {
            get
            {
                if (dp == null)
                {
                    dp = hp.HiDealProductGet(Id);
                }
                return int.Equals(1, dp.PayToCompany);
            }
        }

        public int? DeliveryType
        {
            get
            {
                if (dp == null)
                {
                    dp = hp.HiDealProductGet(Id);
                }
                //品生活不產宅配對帳單 
                //故判斷宅配 or 到店時；若該檔次同時為宅配及到店 以到店的設定為主
                if (dp.IsInStore)
                {
                    return (int)Core.DeliveryType.ToShop;
                }
                if (dp.IsHomeDelivery)
                {
                    return (int)Core.DeliveryType.ToHouse;
                }

                return null;
            }
        }

        private HiDealProductStoreCollection hiDealProductStores = null;
        public IEnumerable<Guid> ParticipatingStores
        {
            get
            {
                if(hiDealProductStores == null)
                {
                    hiDealProductStores = hp.HiDealProductStoreGetListByProductId(Id);
                }

                if(hiDealProductStores == null) //避免 Subsonic 作怪
                {
                    return new List<Guid>();
                }

                return hiDealProductStores.Select(hiDealProductStore => hiDealProductStore.StoreGuid);
            }
            
            
        }

        public CashTrustLogCollection GetAllSuccessfulOrderTrustList()
        {
            return mp.CashTrustLogGetListByHiDealProductId(Id);
        }

        public CashTrustLogCollection GetAllSuccessfulOrderTrustList(DateTime genDate)
        {
            throw new NotImplementedException();
        }


        private IHiDealProvider hp;
        private IMemberProvider mp;

        #region .ctors
        
        public HiDealVendorBillingHelper(int productId)
        {
            Id = productId;
            InitializeProviders();
        }

        #endregion

        private void InitializeProviders()
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        

    }
}

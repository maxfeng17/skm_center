﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// 簡單工廠模式:由一個工廠類根據傳入的參量決定創建出哪一種産品類的實例
    /// </summary>
    public class SkmQrcodeFatory
    {
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        /// <summary>
        /// 透過參數去決定用哪個Qrcode類別實現
        /// </summary>
        /// <param name="ctl">CashTrustLog</param>
        /// <param name="vpd">ViewPponDeal</param>
        /// <returns></returns>
        public ISkmQrcodeBase CreateFatory(SkmQrcodeDataModel qrcodeModel)
        {
            switch (qrcodeModel.ActiveType)
            {
                case (int)ActiveType.Product:
                    return new SkmQrcodeLite(qrcodeModel);
                case (int)ActiveType.Shoppe:
                    return new SkmQrcodeLite(qrcodeModel);
                case (int)ActiveType.BuyGetFree:
                    return new SkmQrcodeLiteGetByFree(qrcodeModel);
                default:
                    return null;
            }
        }
    }

    /// <summary>
    /// Qrcode 介面
    /// </summary>
    public interface ISkmQrcodeBase
    {
        string TrustCode { get; set; }
        string GetSkmQrCodeString();
        string GetSkmValidateCode();
    }

    /// <summary>
    /// Qrcode基底類別，需透過
    /// </summary>
    public abstract class SkmQrcodeLiteBase : ISkmQrcodeBase
    {
        #region 基本欄位
        /// <summary>
        /// AES編碼後的憑證資訊
        /// </summary>
        [JsonProperty("a", Order = 1)]
        public string TrustCode { set; get; }
        /// <summary>
        /// Coupon.Id
        /// </summary>
        [JsonProperty("b", Order = 2)]
        public int CouponId { set; get; }

        /// <summary>
        /// 可使用館別櫃位
        /// </summary>
        [JsonProperty("e", Order = 5)]
        public List<UsingShoppeLite> ShoppeData { set; get; }

        /// <summary>
        /// 憑證使用日期(起)
        /// </summary>
        [JsonProperty("h", Order = 8)]
        public long UsingDateS { set; get; }
        /// <summary>
        /// 憑證使用日期(迄)
        /// </summary>
        [JsonProperty("i", Order = 9)]
        public long UsingDateE { set; get; }
        /// <summary>
        /// 憑證存活期限 (now+10min)
        /// </summary>
        [JsonProperty("j", Order = 10)]
        public long ExpireTime { set; get; }
        /// <summary>
        /// eventName+(string)eventType+(string)discountType+(string)discountValue+(string)usingDateS(月份2碼)+(string)expireTime(秒數2碼)+(string)usingDateE(日2碼)+(string)expireTime(分鐘數2碼) 的字串組合MD5
        /// </summary>
        [JsonProperty("k", Order = 11)]
        public string ValidateCode { set; get; }
        #endregion

        protected static long DateTimeConvertLong(DateTime dt)
        {
            return long.Parse(dt.ToString("yyyyMMddHHmmss"));
        }

        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        /// <summary>
        /// ValidateCode 前後固定字串: 繼承後可修改
        /// </summary>
        protected string preFix = "mj[,ps|/8@";
        protected string postFix = "ejo3nj45@8d[~";

        /// <summary>
        /// QrCode 開頭固定字串: 繼承後可修改
        /// </summary>
        protected string postFixBase64 = "LIFE";

        public SkmQrcodeLiteBase()
        {
          
        }

        /// <summary>
        /// 衍生類別中予以覆寫，資料欄位差異得部分
        /// </summary>
        /// <param name="qrcodeMode">另外獨立的Input Data Model</param>
        public virtual void Difference(SkmQrcodeDataModel qrcodeMode)
        {
        }

        /// <summary>
        /// 設定基本欄位
        /// </summary>
        /// <param name="qrcodeModel"></param>
        public SkmQrcodeLiteBase(SkmQrcodeDataModel qrcodeModel)
        {
            DateTime expireTime = DateTime.Now.AddMinutes(config.SkmQrCodeExpireTime);

            TrustCode = qrcodeModel.TrustId.ToString();
            CouponId = qrcodeModel.CouponId;
            ShoppeData = SkmFacade.GetSkmUsingShoppeLiteList(qrcodeModel.BusinessHourGuid);
            UsingDateS = DateTimeConvertLong(qrcodeModel.BusinessHourDeliverTimeS);
            UsingDateE = DateTimeConvertLong(qrcodeModel.BusinessHourDeliverTimeE);
            ExpireTime = DateTimeConvertLong(expireTime);

            Difference(qrcodeModel);
            
            //憑證防偽驗證碼，需留在最後
            ValidateCode = new Security(SymmetricCryptoServiceProvider.DefaultMD5).Encrypt(GetSkmValidateCode()).Substring(0, 4);

        }

        /// <summary>
        /// 取得QrCode字串
        /// </summary>
        /// <returns></returns>
        public string GetSkmQrCodeString()
        {
            var json = new Core.JsonSerializer().Serialize(this);
            var plainTextBytes = Encoding.UTF8.GetBytes(json);
            var qrBase64 = Convert.ToBase64String(plainTextBytes);
            return string.Format("{0}{1}", postFixBase64, qrBase64);
        }
        
        /// <summary>
        /// 所有欄位加總等於Validate Code
        /// </summary>
        /// <returns></returns>
        public virtual string GetSkmValidateCode()
        {
            //串接館櫃代碼 all (storeCode + isAllShoppe + shoppe list)
            string storeCode = string.Empty;
            foreach (var counter in ShoppeData)
            {
                var shoppe = counter.Shoppe.Any() ? string.Join(string.Empty, counter.Shoppe) : string.Empty;
                storeCode += string.Format("{0}{1}{2}", counter.StoreCode, counter.IsAllShoppe.ToString().ToLower(), shoppe);
            }
            
            var code = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}"
               , preFix//1
               , TrustCode.Substring(0, 6)//2
               , CouponId//3
               , GetDiffModelString()//discoutSetting//4-5-6-7
               , UsingDateS//8
               , UsingDateE//9
               , ExpireTime//10
               , storeCode//11
               , postFix);//12

            return code;
        }

        /// <summary>
        /// ValidateCode中間差異的部分
        /// </summary>
        /// <returns></returns>
        public virtual string GetDiffModelString()
        {
            return string.Empty;
        }

    }

    /// <summary>
    /// 縮小版QrCode
    /// </summary>
    public class SkmQrcodeLite : SkmQrcodeLiteBase
    {
        #region 此次新增欄位
        /// <summary>
        /// 0:指定商品代碼(自營) 1:限定櫃位使用
        /// </summary>
        [JsonProperty("c", Order = 3)]
        public int EventType { set; get; }
        /// <summary>
        /// 商品代碼(eventType = 0才有值, 否則為空值)
        /// </summary>
        [JsonProperty("d", Order = 4)]
        public string ItemId { set; get; }

        /// <summary>
        /// 優惠方式 0:折價 1:折數 2:指定金額
        /// </summary>
        [JsonProperty("f", Order = 6)]
        public int DiscountType { set; get; }
        /// <summary>
        /// 優惠內容值 ex: 200 or 0.9
        /// </summary>
        [JsonProperty("g", Order = 7)]
        public double DiscountValue { set; get; }
        #endregion

        public SkmQrcodeLite() { }

        public SkmQrcodeLite(SkmQrcodeDataModel qrcodeModel) : base(qrcodeModel) { }

        public override void Difference(SkmQrcodeDataModel qrcodeModel)
        {
            #region 商品型態
            int eventType = 0;

            if (qrcodeModel.ActiveType == (int)ActiveType.Product)
            {
                eventType = 0;
            }
            else if (qrcodeModel.ActiveType == (int)ActiveType.Shoppe)
            {
                eventType = 1;
            }

            #endregion

            #region 優惠方式

            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(qrcodeModel.BusinessHourGuid, true);

            int discountType = 0;
            double discountVal = vpd.DiscountValue.GetValueOrDefault();
            if (vpd.DiscountType == (int)LunchKingSite.Core.DiscountType.DiscountPrice)
            {
                discountType = 2;
            }
            else if (vpd.DiscountType == (int)LunchKingSite.Core.DiscountType.DiscountCash)
            {
                discountType = 0;
            }
            else if (vpd.DiscountType == (int)LunchKingSite.Core.DiscountType.DiscountPercent)
            {
                discountType = 1;
                discountVal = discountVal / 100;
            }
            else if (vpd.DiscountType == (int)LunchKingSite.Core.DiscountType.DiscountFree)
            {
                discountType = 3;
            }
            else if (vpd.DiscountType == (int)LunchKingSite.Core.DiscountType.DiscountOther)
            {
                discountType = 4;
            }

            if (qrcodeModel.IsSkmPay)
            {
                discountType = 4;
            }

            #endregion

            #region 法雅客遇指定金額折等於優惠金額, 折扣需為0, 折數需為 1
            if (PponFacade.GetDealAllCategories(qrcodeModel.BusinessHourGuid).Select(x => x.Cid).Contains(config.SkmFayaqueCategoryId))
            {
                switch ((SkmQrDiscountType)discountType)
                {
                    case SkmQrDiscountType.DiscountCash:
                        discountVal = 0;
                        break;
                    case SkmQrDiscountType.DiscountPercent:
                        discountVal = 1;
                        break;
                }
            }
            #endregion

            EventType = eventType;
            ItemId = qrcodeModel.ProductCode;
            DiscountType = discountType;
            DiscountValue = discountVal;
        }

        public override string GetDiffModelString()
        {
            return EventType+ ItemId+ DiscountType+ DiscountValue.ToString("0.0##");
        }

    }

    /// <summary>
    /// 縮小版QrCode(專屬用在買幾送幾的優惠型態)
    /// </summary>
    public class SkmQrcodeLiteGetByFree : SkmQrcodeLiteBase
    {
        #region 此次新增欄位
        /// <summary>
        /// 買幾送幾的主要優惠設定內容
        /// </summary>
        [JsonProperty("c", Order = 3)]
        public DiscoutSettingLite DiscoutSetting { set; get; }

        #endregion

        public SkmQrcodeLiteGetByFree() {}

        public SkmQrcodeLiteGetByFree(SkmQrcodeDataModel qrcodeModel) : base(qrcodeModel) { }

        public override void Difference(SkmQrcodeDataModel qrcodeModel)
        {
            DiscoutSetting = new DiscoutSettingLite();

            preFix = "mj[,ps|/2@";
            postFix = "ejo3nj45@2d[~";
            postFixBase64 = "LIFE2";

            if (qrcodeModel.ActiveType == 4)
            {
                DiscoutSetting.BuyCount = qrcodeModel.Buy;
                DiscoutSetting.DiscountCount = qrcodeModel.Free;
            }

            ExternalDealRelationItemCollection dealItemList = pp.ExternalDealItemGetByBid(qrcodeModel.BusinessHourGuid);
            DiscoutSetting.DiscountItemIds = dealItemList.Where(p => p.IsFree == true).Select(p => p.ProductCode).ToList();
            DiscoutSetting.BuyItemIds = dealItemList.Where(p => p.IsFree == false).Select(p => p.ProductCode).ToList();
        }

        public override string GetDiffModelString()
        {
            string code = string.Empty;
            code += DiscoutSetting.BuyCount.ToString();
            code += string.Join("", DiscoutSetting.BuyItemIds);
            code += DiscoutSetting.DiscountCount.ToString();
            code += string.Join("", DiscoutSetting.DiscountItemIds);
            return code;
        }

        public override string GetSkmValidateCode()
        {
            //串接館櫃代碼 all (storeCode + isAllShoppe + shoppe list)
            string storeCode = string.Empty;
            foreach (var counter in ShoppeData)
            {
                var shoppe = counter.Shoppe.Any() ? string.Join(string.Empty, counter.Shoppe) : string.Empty;
                storeCode += string.Format("{0}{1}{2}", counter.StoreCode, counter.IsAllShoppe.ToString().ToLower(), shoppe);
            }

            var code = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}"
                , preFix//1
                , TrustCode.Substring(0, 6)//2
                , CouponId//3
                , GetDiffModelString()//discoutSetting//4
                , storeCode//5
                , UsingDateS//6
                , UsingDateE//7
                , ExpireTime//8
                , postFix);//9

            return code;
        }

    }

    /// <summary>
    /// 為降低與SubStage耦合性，將Input資料模型獨立處理
    /// </summary>
    public class SkmQrcodeDataModel
    {
        public IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        public IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        public Guid TrustId { get; set; }
        public int CouponId { get; set; }
        public Guid BusinessHourGuid { get; set; }
        public List<UsingShoppeLite> ShoppeData { get; set; }
        public DateTime BusinessHourDeliverTimeS { get; set; }
        public DateTime BusinessHourDeliverTimeE { get; set; }
        public int ActiveType { get; set; }
        public string ProductCode { get; set; }
        public int Buy { get; set; }
        public int Free { get; set; }
        public bool IsSkmPay { get; set; }

        public SkmQrcodeDataModel()
        {

        }

        /// <summary>
        /// 多重介面: new SkmQrcodeDataModel(trustId, vpd) { }
        /// </summary>
        /// <param name="trustId"></param>
        /// <param name="vpd"></param>
        public SkmQrcodeDataModel(Guid trustId, ViewPponDeal vpd)
        {
            var ctl = mp.CashTrustLogGet(trustId);
            SetValue(ctl, vpd);
        }

        /// <summary>
        /// 多重介面: new SkmQrcodeDataModel(orderGuid, sequenceNumber) { }
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="sequenceNumber"></param>
        public SkmQrcodeDataModel(Guid orderGuid, string sequenceNumber)
        {
            var ctls = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.Skm);

            var ctl = ctls.FirstOrDefault(x => x.CouponSequenceNumber == sequenceNumber);
            if (ctl != null)
            {
                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(ctl.BusinessHourGuid ?? Guid.Empty);
                SetValue(ctl, vpd);
            }
        }

        /// <summary>
        /// 多重介面: new SkmQrcodeDataModel(ctl, vpd) { }
        /// </summary>
        /// <param name="ctl"></param>
        /// <param name="vpd"></param>
        public SkmQrcodeDataModel(CashTrustLog ctl, IViewPponDeal vpd)
        {
            SetValue(ctl, vpd);
        }

        /// <summary>
        /// 主要塞值
        /// </summary>
        /// <param name="ctl"></param>
        /// <param name="vpd"></param>
        public void SetValue(CashTrustLog ctl, IViewPponDeal vpd)
        {
            TrustId = ctl.TrustId;
            CouponId = ctl.CouponId ?? 0;

            ShoppeData = SkmFacade.GetSkmUsingShoppeLiteList(vpd.BusinessHourGuid);
            BusinessHourDeliverTimeS = vpd.BusinessHourDeliverTimeS ?? new DateTime();
            BusinessHourDeliverTimeE = vpd.BusinessHourDeliverTimeE ?? new DateTime();
            BusinessHourGuid = vpd.BusinessHourGuid;

            var externalDeal = pp.ViewExternalDealGetByBid(vpd.BusinessHourGuid);
            ShoppeData.RemoveAll(x => x.StoreCode != externalDeal.ShopCode);
            ActiveType = externalDeal.ActiveType;
            ProductCode = externalDeal.ProductCode;
            Buy = externalDeal.Buy ?? 0;
            Free = externalDeal.Free ?? 0;
            IsSkmPay = externalDeal.IsSkmPay;
        }
    }
}
    

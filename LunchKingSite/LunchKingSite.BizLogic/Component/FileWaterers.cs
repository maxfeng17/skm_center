﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Component
{
    public class FileWaterers
    {
        static ILog logger = LogManager.GetLogger(typeof(FileWaterers).Name);
        private static FileSystemWatcher watchE7LifeWebConfig;

        private static object prevLastWriteTimeLock = new object();
        private static DateTime prevLastWriteTime;
        private static DateTime PrevLastWriteTime
        {
            get
            {
                lock (prevLastWriteTimeLock)
                {
                    return prevLastWriteTime;
                }
            }
            set
            {
                lock (prevLastWriteTimeLock)
                {
                    prevLastWriteTime = value;
                }
            }
        }

        public delegate void ConfigChangeHandler(ConfigChangedInfo info);
        public static event ConfigChangeHandler OnConfigChange;


        static FileWaterers()
        {
            watchE7LifeWebConfig = new FileSystemWatcher();
            watchE7LifeWebConfig.Path = HostingEnvironment.MapPath("~/Config/test/");
            watchE7LifeWebConfig.Filter = "web.config";
            watchE7LifeWebConfig.NotifyFilter = NotifyFilters.LastWrite;
            watchE7LifeWebConfig.Changed += delegate (object sender, FileSystemEventArgs e)
            {
                string filePath = Path.Combine(
                    watchE7LifeWebConfig.Path, watchE7LifeWebConfig.Filter);
                DateTime lastWriteTime = File.GetLastWriteTime(filePath);
                if ((lastWriteTime - PrevLastWriteTime).TotalSeconds >= 1)
                {
                    try
                    {
                        List<ConfigChangedInfo> confChangedList = Helper.CopyObjectProperties(
                            new WebConfSysConfProvider(),
                            ProviderFactory.Instance().GetConfig());

                        string changedInfo = "";
                        foreach (ConfigChangedInfo confInfo in confChangedList)
                        {
                            changedInfo += string.Format("\r\n{0}: \"{1}\" 調整為 \"{2}\"", confInfo.Key, confInfo.FromValue, confInfo.ToValue);
                            if (OnConfigChange != null)
                            {
                                OnConfigChange(confInfo);
                            }
                        }

                        string reloadJobsLog = LunchKingSite.Core.Jobs.Instance.Reload();

                        /*
                        //測試新的web.config是否能正確載入, 如果不行，不要蓋掉目前的版本
                        new WebConfSysConfProvider()
                        var builder = new ContainerBuilder();
                        builder.RegisterType<WebConfSysConfProvider>().AsImplementedInterfaces().InstancePerLifetimeScope();
                        builder.Update(ProviderFactory.Instance().Container.ComponentRegistry);                         
                        ProviderFactory.Instance().GetConfig();
                        */
                        logger.Warn("提醒 小 web.config 已更新..." + changedInfo + reloadJobsLog);
                    }
                    catch (Exception ex)
                    {
                        logger.Error("警告!!! 小 web.config 載入失敗...", ex);
                    }
                    PrevLastWriteTime = lastWriteTime;
                }
            };
            watchE7LifeWebConfig.EnableRaisingEvents = true;
        }


        public static void Register()
        {
            OnConfigChange += delegate (ConfigChangedInfo info) {
                //如果想在小webconfig的值變更時，做些什麼 ，可以寫在這
                //if (info.Key == "if_key_is") { do something }

                switch (info.Key)
                {
                    case "MobileChannel":
                        LunchKingSite.BizLogic.Component.PponDealPreviewManager.ReloadManager();
                        break;
                    case "ActiveMQServerUri":
                        try
                        {
                            ProviderFactory.Instance().GetProvider<IMessageQueueProvider>().ResetConnect();
                        }
                        catch (Exception ex)
                        {
                            logger.Warn(ex);
                        }
                        break;
                }
            };
        }
    }
}

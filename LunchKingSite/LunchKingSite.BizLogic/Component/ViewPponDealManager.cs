﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using HtmlAgilityPack;
using log4net;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Models;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Component
{
    public class ViewPponDealManager
    {
        /// <summary>
        /// 備用快取，當ViewPponDealManager 重置時，MultipleMainDealPreview的快取並沒有預先載入，
        /// 此時如果被呼叫會先跟資料庫要資料，再放進一級快取，然後回應給呼叫方
        /// SpareCache的目是在保留重置前的快取資料
        /// 當重置後被呼叫時，改成從SpareCache先回應給呼叫方，然後再跟資料庫讀取最新資料，更新一級快取與備用快取
        /// </summary>
        protected class SpareCache
        {
            static SpareCache()
            {
                PrevCategoryDeals = new ConcurrentDictionary<string, List<MultipleMainDealPreview>>();
                PrevCategoryDealsLocks = new ConcurrentDictionary<string, bool>();
            }

            public static ConcurrentDictionary<string, bool> PrevCategoryDealsLocks { get; set; }
            public static ConcurrentDictionary<string, List<MultipleMainDealPreview>> PrevCategoryDeals { get; set; }

            public static bool TryGetCategoryDeals(string key, PponCity city, List<int> categoryCriteria,
                Action<List<MultipleMainDealPreview>> updateFirstCacheAction, out List<MultipleMainDealPreview> dataList)
            {
                if (PrevCategoryDeals.TryGetValue(key, out dataList))
                {
                    Action<string, PponCity, List<int>> actGetData =
                        delegate (string argKey, PponCity argCity, List<int> argCategoryCriteria)
                        {
                            bool lockValue;
                            if (PrevCategoryDealsLocks.TryGetValue(argKey, out lockValue) == false)
                            {
                                try
                                {
                                    PrevCategoryDealsLocks.TryAdd(key, true);
                                    List<MultipleMainDealPreview> data = DefaultManager.MultipleMainDealPreviewGetListByKey(argCity,
                                        argCategoryCriteria);

                                    updateFirstCacheAction(data);

                                    List<MultipleMainDealPreview> temp;
                                    PrevCategoryDeals.TryRemove(key, out temp);
                                    PrevCategoryDeals.TryAdd(key, data);
                                }
                                finally
                                {
                                    PrevCategoryDealsLocks.TryRemove(key, out lockValue);
                                }
                            }
                        };
                    actGetData.BeginInvoke(key, city, categoryCriteria, null, null);
                    return true;
                }
                return false;
            }
        }

        protected class Kernel
        {
            protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            protected ICacheProvider cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
            public ViewPponDealCollection _collection { get; private set; }
            public CpaMainCollection _cpamainCollection { get; private set; }
            public List<CityViewPponDealData> _cityViewPponDealPponDatas { get; private set; }
            public Dictionary<Guid, List<int>> _dealChannelList { get; private set; }
            public Dictionary<Guid, List<IViewPponDeal>> _sellerDeals { get; private set; }
            /// <summary>
            /// 活動時程表資訊
            /// </summary>
            public DealTimeSlotList _dealTimeSlotCollection { get; private set; }

            public ConcurrentDictionary<int, List<CategoryDealCountSummary>> _cityCategories { get; private set; }
            public ConcurrentDictionary<string, List<MultipleMainDealPreview>> _categoryDeals { get; private set; }
            /// <summary>
            /// 每日新檔次的資料，鍵值為 日期+分類的ID
            /// </summary>
            public ConcurrentDictionary<KeyValuePair<DateTime, int>, List<MultipleMainDealPreview>> _categoryNewDeals { get; private set; }
            public List<Category> _travelCategories { get; private set; }
            public CategoryCollection _femaleCategories { get; private set; }
            public List<Category> _pponFixCategories { get; private set; }
            public ConcurrentDictionary<Guid, List<ViewComboDeal>> _viewComboDealCollection =
                new ConcurrentDictionary<Guid, List<ViewComboDeal>>();

            public ConcurrentDictionary<Guid, List<ComboDeal>> _comboDeals =
                new ConcurrentDictionary<Guid, List<ComboDeal>>();

            public ConcurrentDictionary<string, ViewPponDeal> _overdueDeals = new ConcurrentDictionary<string, ViewPponDeal>();

            public ConcurrentDictionary<KeyValuePair<int, int?>, List<MultipleMainDealPreview>> _cityHotSaleDeals { get; private set; }

            public ConcurrentDictionary<Guid, BaseCostGrossMargin> _baseCostGrossMargin { get; private set; }
            public ConcurrentDictionary<KeyValuePair<Guid, bool>, QuantityAdjustment> _adjustedOrderedQuantity { get; set; }

            public Dictionary<Guid, PponDealEvaluateStar> _dealEvaluateStarList { get; private set; }

            public Dictionary<int, Category> _categories { get; private set; }
            public Dictionary<Guid, EveryDayNewDeal> _everydaynewdeals { get; private set; }

            public Kernel()
            {
                _collection = new ViewPponDealCollection();
                _cpamainCollection = new CpaMainCollection();
                _cityViewPponDealPponDatas = new List<CityViewPponDealData>();
                _dealChannelList = new Dictionary<Guid, List<int>>();
                _dealTimeSlotCollection = new DealTimeSlotList();
                _cityCategories = new ConcurrentDictionary<int, List<CategoryDealCountSummary>>();
                _categoryDeals = new ConcurrentDictionary<string, List<MultipleMainDealPreview>>();
                _categoryNewDeals = new ConcurrentDictionary<KeyValuePair<DateTime, int>, List<MultipleMainDealPreview>>();
                _cityHotSaleDeals = new ConcurrentDictionary<KeyValuePair<int, int?>, List<MultipleMainDealPreview>>();
                _baseCostGrossMargin = new ConcurrentDictionary<Guid, BaseCostGrossMargin>();
                _adjustedOrderedQuantity = new ConcurrentDictionary<KeyValuePair<Guid, bool>, QuantityAdjustment>();
                _dealEvaluateStarList = new Dictionary<Guid, PponDealEvaluateStar>();
                _everydaynewdeals = new Dictionary<Guid, EveryDayNewDeal>();
            }

            public virtual void Reset()
            {
            }

            public virtual void Reload()
            {
                StopwatchLog watch = StopwatchLog.Get("vpd");
                LoadComboDeals();
                watch.Log("LoadComboDeals");
                LoadCpaMainGetListByCurrent();
                watch.Log("LoadCpaMainGetListByCurrent");
                LoadCategories();
                watch.Log("LoadDealMapCategoriesDic");
                LoadTodayBaseDeal(false);
                watch.Log("LoadTodayBaseDeal");
                InitSellerDeals();
                LoadLastDayTags();
                watch.Log("LoadLastDayTags");
                LoadDealHoverMessage();
                watch.Log("LoadDealHoverMessage");
                LoadViewPponSellerNameData();
                watch.Log("LoadViewPponSellerNameData");
                LoadTodayDealTimeSlot();
                watch.Log("LoadTodayDealTimeSlot");
                LoadTodayDealEvaluate();
                watch.Log("LoadTodayDealEvaluate");
                LoadTravelCategories();
                watch.Log("LoadTravelCategories");
                LoadPponFixCategories();
                watch.Log("LoadPponFixCategories");
                LoadBaseCostAndGrossProfit();
                watch.Log("LoadBaseCostAndGrossProfit");
                SetLogicProperties();
                watch.Log("SetLogicProperties");
                LoadEveryDayNewDeals();
                watch.Log("LoadEveryDayNewDeals");
            }

            protected void LoadComboDeals()
            {
                ComboDealCollection todayComboDeal = pp.GetComboDealToday(false);
                var dict = todayComboDeal.GroupBy(c => c.MainBusinessHourGuid)
                    .Where(item => item.Key != null).ToDictionary(g => g.Key.GetValueOrDefault(), g => g.ToList());
                this._comboDeals = new ConcurrentDictionary<Guid, List<ComboDeal>>(dict);
            }

            protected void InitSellerDeals()
            {
                _sellerDeals = new Dictionary<Guid, List<IViewPponDeal>>();
                foreach (var deal in _collection)
                {
                    if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                    {
                        continue;
                    }
                    if (_sellerDeals.ContainsKey(deal.SellerGuid) == false)
                    {
                        _sellerDeals.Add(deal.SellerGuid, new List<IViewPponDeal>());
                    }
                    _sellerDeals[deal.SellerGuid].Add(deal);
                }
            }

            /// <summary>
            /// 將系統日當天與前一天的好康資料寫入暫存的collection中
            /// </summary>
            protected void LoadTodayBaseDeal(bool tryGetFromCache, params Task[] priorTasks)
            {
                DateTime baseDate = PponDealHelper.GetTodayBaseDate();
                ViewPponDealCollection vpdc = null;

                StopwatchLog watch = StopwatchLog.Get("vpd");

                List<Task> tasks = new List<Task>();
                priorTasks.ForEach(t => t.Start());
                tasks.AddRange(priorTasks);

                if (tryGetFromCache == false)
                {
                    vpdc = pp.ViewPponDealGetListByDayWithNoLock(baseDate.AddDays(-1), baseDate.AddDays(1));
                    watch.Log("LoadTodayBaseDeal: _collection");
                }
                else
                {
                    vpdc = new ViewPponDealCollection();
                    List<Guid> bids = pp.OnlineDealGuidGetList(baseDate.AddDays(-1), baseDate.AddDays(1));
                    if (RoundNo == 0)
                    {
                        int getDealSize = 4000;
                        int taskCount = bids.Count / getDealSize;
                        if ((bids.Count % getDealSize) > 0)
                        {
                            taskCount++;
                        }
                        for (int i = 0; i < taskCount; i++)
                        {
                            List<Guid> partBids = bids.Skip(i * getDealSize).Take(getDealSize).ToList();
                            var task = new Task(delegate
                            {
                                foreach (Guid bid in partBids)
                                {
                                    var deal = cache.Get<ViewPponDeal>("vpd::" + bid, config.CacheItemCompress);
                                    if (deal == null)
                                    {
                                        deal = pp.ViewPponDealGetByBusinessHourGuid(bid);
                                    }
                                    else
                                    {
                                        deal.IsLoaded = true;
                                    }
                                    if (deal.IsLoaded == false)
                                    {
                                        continue;
                                    }
                                    lock (vpdc)
                                    {
                                        vpdc.Add(deal);
                                    }
                                }
                            }, TaskCreationOptions.LongRunning);
                            task.Start();
                            tasks.Add(task);
                        }
                    }
                    else
                    {
                        var task = new Task(delegate
                        {
                            foreach (Guid bid in bids)
                            {
                                var deal = cache.Get<ViewPponDeal>("vpd::" + bid, config.CacheItemCompress);
                                if (deal == null)
                                {
                                    deal = pp.ViewPponDealGetByBusinessHourGuid(bid);
                                }
                                else
                                {
                                    deal.IsLoaded = true;
                                }
                                if (deal.IsLoaded == false)
                                {
                                    continue;
                                }
                                lock (vpdc)
                                {
                                    vpdc.Add(deal);
                                }
                            }
                        }, TaskCreationOptions.LongRunning);
                        task.Start();
                        tasks.Add(task);
                    }
                }
                Task.WaitAll(tasks.ToArray());
                watch.Log("LoadTodayBaseDeal from cache: _collection");

                _collection = vpdc;
                _collection.InitIndex();
                watch.Log("LoadTodayBaseDeal from cache: InitIndex");

                foreach (var item in vpdc)
                {
                    FillVpdComboDeals(item);
                }
                watch.Log("LoadTodayBaseDeal: GetViewComboDealCollectionByMainGuid");
            }

            protected void LoadCpaMainGetListByCurrent()
            {
                if (config.NewCpaEnable)
                {
                    _cpamainCollection = pp.CpaMainGetListByCurrent(DateTime.Now);
                }
            }

            private void FillVpdComboDeals(ViewPponDeal item)
            {
                if ((item.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                {
                    var combodeals = this.GetViewComboDealCollectionByMainGuid(item.BusinessHourGuid) ?? new List<ViewComboDeal>();
                    if (config.EnableComboDealNewOrderTotalLimitSum)//針對檔次"鎖量"特殊判斷，避免母檔售完
                    {
                        item.OrderedQuantity = (int?)combodeals.Sum(x => (x.OrderTotalLimit == default(int)) ? default(int) : x.OrderedQuantity);
                        item.OrderedTotal = combodeals.Sum(x => x.OrderedQuantity * x.ItemPrice);
                        item.ComboDelas = combodeals;
                        item.OrderTotalLimit = combodeals.Sum(x => (x.OrderTotalLimit == default(int)) ? default(int) : x.OrderTotalLimit);
                        //子檔結檔，最大可購買數會被壓成0，造成雖有部份子檔可賣，
                        //但最大可購買數卻可能小於已購買數，造成售完
                        if (item.OrderTotalLimit > 0 && item.OrderTotalLimit < 99999)
                        {
                            item.OrderTotalLimit = 99999;
                        }
                    }
                    else
                    {
                        item.OrderedQuantity = combodeals.Sum(x => x.OrderedQuantity);
                        item.OrderedTotal = combodeals.Sum(x => x.OrderedQuantity * x.ItemPrice);
                        item.ComboDelas = combodeals;
                        item.OrderTotalLimit = combodeals.Sum(x => x.OrderTotalLimit);
                    }
                    //子檔全數完售，母檔就算完售
                    item.IsSoldOut = combodeals.All(x => x.OrderedQuantity >= x.OrderTotalLimit);
                }
                else
                {
                    item.ComboDelas = new List<ViewComboDeal>();
                    item.IsSoldOut = item.OrderedQuantity >= item.OrderTotalLimit;
                }
            }

            /// <summary>
            /// 替換黑標
            /// </summary>
            /// <param name="item"></param>
            private static void UpdatePponDealEventName(ViewPponDeal item)
            {
                //前台不顯示內文就不用置換了
                if (item.IsHideContent != null && item.IsHideContent == false)
                {
                    if (!string.IsNullOrEmpty(item.ContentName))
                    {
                        item.EventName = item.EventName;
                    }

                }
            }

            protected virtual void LoadLastDayTags()
            {
                foreach (var item in _collection)
                {
                    LoadLastDayTag(item);
                }
            }

            /// <summary>
            /// 當檔次符合"最後一天"的條件，將"最後一天"的LabelIcon加進List裡，讓RWD版首頁可以判斷篩選
            /// </summary>
            /// <param name="deal"></param>
            private void LoadLastDayTag(ViewPponDeal deal)
            {
                int lastDayLabelIcon = (int)DealLabelSystemCode.SoldEndAfterOneDay;
                //加上最後一天的tag
                if (DateTime.Now >= deal.BusinessHourOrderTimeE.AddDays(-1))
                {
                    if (string.IsNullOrEmpty(deal.LabelIconList))
                    {
                        deal.LabelIconList = lastDayLabelIcon.ToString();
                    }
                    else
                    {
                        deal.LabelIconList = string.Format("{0},{1}", deal.LabelIconList, lastDayLabelIcon);
                    }
                    deal.LabelIcons.Add(DealLabelSystemCode.SoldEndAfterOneDay);
                }
            }

            /// <summary>
            ///處理檔次的詳細介紹部分，調整內容供APP呼叫使用
            /// </summary>
            protected virtual void LoadDealHoverMessage()
            {
                StopwatchLog watch = StopwatchLog.Get("vpd");
                foreach (ViewPponDeal item in _collection)
                {
                    LoadHoverMessageByDeal(item);
                }
                watch.Log("LoadTodayBaseDeal: LoadDealHoverMessage");
            }

            private void LoadHoverMessageByDeal(ViewPponDeal item)
            {
                bool is_multiplestores;
                item.HoverMessage = ViewPponStoreManager.DefaultManager.GetPponStoresShortDesc(item.BusinessHourGuid, out is_multiplestores);
                item.IsMultipleStores = is_multiplestores;
                item.DefaultDealImage = ImageFacade.GetMediaPathsFromRawData(item.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(@"https://www.17life.com/Themes/default/images/17Life/G2/ppon-M1_pic.jpg").First();
                item.IconTags = item.PromoImageHtml = string.Empty;
                item.RestrictionTags = new List<string>();
                if (!string.IsNullOrEmpty(item.LabelTagList))
                {
                    string[] tag_list = item.LabelTagList.Split(',');
                    int i = 0;
                    foreach (int tag in tag_list.Where(x => int.TryParse(x, out i)).Select(x => i))
                    {
                        item.IconTags += string.Format("<span class=\"dds-label\">{0}</span>", SystemCodeManager.GetDealLabelSystemCodeNameById(i));
                        item.RestrictionTags.Add(SystemCodeManager.GetDealLabelSystemCodeNameById(i));
                    }
                }
                if (!string.IsNullOrEmpty(item.CustomTag))
                {
                    item.IconTags += string.Format("<span class=\"dds-label\">{0}</span>", item.CustomTag);
                    item.RestrictionTags.Add(item.CustomTag);
                }
                if (!string.IsNullOrEmpty(item.DealPromoImage))
                {
                    item.PromoImageHtml = string.Format(@"<img src='{0}' alt='' />"
                    , ImageFacade.GetMediaPath(item.DealPromoImage, MediaType.DealPromoImage));
                }
            }

            /// <summary>
            /// 讀取每個城市系統時間的所有Deal資料，並依據排程設定順序排列
            /// </summary>
            public void LoadViewPponSellerNameData()
            {
                StopwatchLog watch = StopwatchLog.Get("vpd");

                List<CityViewPponDealData> cityDatas = new List<CityViewPponDealData>();
                Dictionary<Guid, List<int>> dealChannelList = new Dictionary<Guid, List<int>>();
                List<PponCity> cities = PponCityGroup.DefaultPponCityGroup.GetPponCityForSetting();
                foreach (PponCity city in cities)
                {
                    List<Guid> bids = pp.BusinessHourGuidGetListByCityIdOrderBySlotSeq(city.CityId, PponDeliverytype);
                    if (bids.Count > 0)
                    {
                        List<IViewPponDeal> vpds = new List<IViewPponDeal>();
                        foreach (Guid bid in bids)
                        {
                            IViewPponDeal vpd = ViewPponDealGetByBid(bid, true);
                            if (vpd.IsLoaded)
                            {
                                vpds.Add(vpd);
                            }
                        }
                        cityDatas.Add(new CityViewPponDealData(city.CityId, vpds, city.ChannelId, city.ChannelAreaId));
                    }
                }
                watch.Log("LoadViewCityPpon: cityDatas");


                foreach (ViewPponDeal item in _collection)
                {
                    if ((item.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                    {
                        var combodeals = this.GetViewComboDealCollectionByMainGuid(item.BusinessHourGuid);
                        item.OrderedQuantity = combodeals.Sum(x => x.OrderedQuantity);
                        if (item.Slug != null) //若已結檔才將數字重新塞進去
                        {
                            item.Slug = item.OrderedQuantity.ToString();
                        }
                        item.OrderedTotal = combodeals.Sum(x => x.OrderedQuantity * x.ItemPrice);
                    }
                }
                watch.Log("LoadViewCityPpon: combodeals");

                _cityViewPponDealPponDatas = cityDatas;
                var dealchannel = _cityViewPponDealPponDatas.SelectMany(x => x.DataCollection.Select(y => new KeyValuePair<Guid, int?>(y.BusinessHourGuid, x.ChannelId))).Distinct();
                dealChannelList = dealchannel.GroupBy(x => x.Key).ToDictionary(x => x.Key, y => y.Select(z => z.Value ?? 0).ToList());
                _dealChannelList = dealChannelList;
            }

            public void LoadTodayDealTimeSlot()
            {
                DateTime baseDate = DateTime.ParseExact(DateTime.Today.ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);

                var dealTimeSlots = pp.DealTimeSlotGetList(DealTimeSlot.Columns.Sequence,
                    DealTimeSlot.Columns.EffectiveStart + ">=" + baseDate.AddDays(-1),
                    DealTimeSlot.Columns.EffectiveStart + " < " + baseDate.AddDays(1));
                _dealTimeSlotCollection.AddRange(dealTimeSlots.ToList().ConvertAll(t => new DealTimeSlotModel(t)));
                _dealTimeSlotCollection.InitIndex();
            }

            /// <summary>
            /// 取得今日檔次評價星等
            /// </summary>
            protected void LoadTodayDealEvaluate()
            {
                if (config.EnableEvaluateCache)
                {
                    List<PponDealEvaluateStar> ds = pp.PponDealEvaluateStarCollectionGet().ToList();
                    _dealEvaluateStarList = ds.ToDictionary(t => t.Bid);
                }
            }

            protected void LoadTravelCategories()
            {
                _travelCategories = _categories.Where(t => t.Value.Type == (int)CategoryType.Travel)
                    .Select(t => t.Value)
                    .ToList();
            }

            protected void LoadPponFixCategories()
            {
                _pponFixCategories = this._categories
                    .Where(t => t.Value.Type == (int)CategoryType.PponFix)
                    .OrderBy(t => t.Value.Rank)
                    .Select(t => t.Value).ToList();
            }

            protected void LoadCategories()
            {
                _categories = sp.CategoryGetDictionary();
            }

            /// <summary>
            /// 讀取基礎成本與基礎毛利率(以第一筆成本為基準的毛利率)
            /// </summary>
            protected void LoadBaseCostAndGrossProfit()
            {
                var data = pp.ViewDealBaseGrossMarginGetTodayDeal();
                foreach (var item in data)
                {
                    BaseCostGrossMarginGet(item.BusinessHourGuid, new BaseCostGrossMargin(item));
                }
            }

            public void SetLogicProperties()
            {
                foreach (var vpd in _collection)
                {
                    vpd.IsHiddenInRecommendDeal = false;
                    //先排除公益檔次
                    if ((EntrustSellType)vpd.EntrustSell.GetValueOrDefault() != EntrustSellType.No)
                    {
                        vpd.IsHiddenInRecommendDeal = true;
                        continue;
                    }
                    //排除新光檔，如果新光檔未啟用
                    if (Helper.IsFlagSet(vpd.GroupOrderStatus.GetValueOrDefault(), GroupOrderStatus.SKMDeal)
                        && AppChannelIdNew.Contains(config.SkmCategordId) == false)
                    {
                        vpd.IsHiddenInRecommendDeal = true;
                        continue;
                    }
                    if (vpd.CityIds.Count == 0)
                    {
                        vpd.IsHiddenInRecommendDeal = true;
                        continue;
                    }
                    if (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
                    {
                        vpd.IsHiddenInRecommendDeal = true;
                        continue;
                    }
                    List<IDealTimeSlot> dealTimeSlots = _dealTimeSlotCollection.GetByBidAndTime(
                            vpd.BusinessHourGuid, DateTime.Now);
                    //判斷是否不顯示於檔次列表
                    if (dealTimeSlots.Any(t => t.Status == (int)DealTimeSlotStatus.NotShowInPponDefault))
                    {
                        vpd.IsHiddenInRecommendDeal = true;
                        continue;
                    }
                }

                //替換黑標
                foreach (var item in _collection)
                {
                    UpdatePponDealEventName(item);
                }

                Fill24HCategoryByDeals();
            }

            #region AdjustedOrderedQuantity

            /// <summary>
            /// 將份數計算存於暫存
            /// </summary>
            /// <param name="bid"></param>
            /// <param name="isIncludePreviousQuantity"></param>
            /// <param name="quantityAdjustemnt"></param>
            /// <param name="adjustedOrderedQuantity"></param>
            /// <returns></returns>
            public bool TryGetAdjustedOrderedQuantity(Guid bid, bool isIncludePreviousQuantity, QuantityAdjustment quantityAdjustemnt,
                out QuantityAdjustment adjustedOrderedQuantity)
            {
                if (quantityAdjustemnt != null)
                {
                    adjustedOrderedQuantity = _adjustedOrderedQuantity.AddOrUpdate(new KeyValuePair<Guid, bool>(bid, isIncludePreviousQuantity), quantityAdjustemnt, (x, y) => quantityAdjustemnt);
                    return true;
                }
                if (_adjustedOrderedQuantity.TryGetValue(new KeyValuePair<Guid, bool>(bid, isIncludePreviousQuantity), out adjustedOrderedQuantity))
                {
                    return true;
                }
                adjustedOrderedQuantity = new QuantityAdjustment();
                return false;
            }

            #endregion

            /// <summary>
            /// 多檔次的子檔也記在暫存中
            /// </summary>
            /// <param name="bid"></param>
            /// <returns></returns>
            public IList<ViewComboDeal> GetViewComboDealCollectionByMainGuid(Guid bid)
            {
                return _viewComboDealCollection.GetOrAdd(bid, delegate
                {
                    List<ViewComboDeal> data = new List<ViewComboDeal>();
                    //要求只從主DB讀資料，試圖避免刷卡時，丟出要求啟用MSDTC的Exception
                    List<ComboDeal> combodeals = GetComboDealByMainBid(bid);
                    foreach (var deal in combodeals.OrderBy(x => x.Sequence))
                    {
                        IViewPponDeal vpd = this.ViewPponDealGetByBid(deal.BusinessHourGuid, true);
                        ViewComboDeal view_combo = new ViewComboDeal()
                        {
                            Id = deal.Id,
                            BusinessHourGuid = deal.BusinessHourGuid,
                            MainBusinessHourGuid = deal.MainBusinessHourGuid,
                            Title = deal.Title,
                            Sequence = deal.Sequence,
                            BusinessHourOrderMinimum = vpd.BusinessHourOrderMinimum,
                            BusinessHourStatus = vpd.BusinessHourStatus,
                            OrderTotalLimit = vpd.OrderTotalLimit,
                            BusinessHourOrderTimeS = vpd.BusinessHourOrderTimeS,
                            BusinessHourOrderTimeE = vpd.BusinessHourOrderTimeE,
                            BusinessHourDeliverTimeS = vpd.BusinessHourDeliverTimeS,
                            BusinessHourDeliverTimeE = vpd.BusinessHourDeliverTimeE,
                            ChangedExpireDate = vpd.ChangedExpireDate,
                            QuantityMultiplier = vpd.QuantityMultiplier,
                            UniqueId = vpd.UniqueId ?? 0,
                            ItemOrigPrice = vpd.ItemOrigPrice,
                            ItemPrice = vpd.ItemPrice,
                            Slug = vpd.Slug,
                            OrderedQuantity = vpd.OrderedQuantity ?? 0,
                            OrderedIncludeRefundQuantity = vpd.OrderedIncludeRefundQuantity ?? 0,
                            IsContinuedQuantity = vpd.IsContinuedQuantity ?? false,
                            ContinuedQuantity = vpd.ContinuedQuantity ?? 0,
                            CouponUsage = vpd.CouponUsage,
                            IsQuantityMultiplier = vpd.IsQuantityMultiplier ?? false,
                            IsAveragePrice = vpd.IsAveragePrice ?? false,
                            SaleMultipleBase = vpd.SaleMultipleBase ?? 0,
                            PresentQuantity = vpd.PresentQuantity ?? 0,
                            GroupCouponDealType = vpd.GroupCouponDealType,
                            EnableIsp = vpd.EnableIsp
                        };
                        data.Add(view_combo);
                    }
                    return data;
                });
            }

            public List<ComboDeal> GetComboDealByMainBid(Guid bid)
            {
                return _comboDeals.GetOrAdd(bid, delegate
                {
                    return pp.GetComboDealByBid((Guid)bid, false).ToList();
                });
            }

            /// <summary>
            /// 依據傳遞的businessHourGuid查詢ViewPponDeal的資料，
            /// 如果暫存的資料中，已有，則直接回傳
            /// 否則 查詢DB的資料回傳。
            /// </summary>
            /// <param name="businessHourGuid"></param>
            /// <returns></returns>
            public IViewPponDeal ViewPponDealGetByBid(Guid businessHourGuid)
            {
                return ViewPponDealGetByBid(businessHourGuid, false);
            }

            /// <summary>
            /// 清檔次1層/2層快取
            /// </summary>
            /// <param name="bid"></param>
            public void ClearViewPponDealCache(Guid bid)
            {
                string cacheKey = string.Format("cache://vpd/{0}", bid);
                ViewPponDeal temp;
                _overdueDeals.TryRemove(cacheKey, out temp);
                cache.Remove(cacheKey);
            }

            /// <summary>
            /// 依據傳遞的businessHourGuid List查詢ViewPponDeal的資料，
            /// 如果暫存的資料中，已有，則直接回傳
            /// 否則 查詢DB的資料回傳。
            /// </summary>
            /// <param name="businessHourGuidList"></param>
            /// <returns></returns>
            public List<ViewPponDeal> ViewPponDealGetByBid(List<Guid> businessHourGuidList)
            {
                //取得非過期暫存資料
                var vpdList = _collection.Where(x => businessHourGuidList.Contains(x.BusinessHourGuid)).Select(x => x.Clone()).ToList();
                if (vpdList.Count() == businessHourGuidList.Count())
                {
                    return vpdList;
                }

                //取差集的過期資料 cacheKey
                List<string> expectedBidsCacheKey = businessHourGuidList.Except(vpdList.Select(x => x.BusinessHourGuid))
                    .Select(x => string.Format("cache://vpd/{0}", x)).ToList();
                //補上已過期暫存資料
                vpdList.AddRange(_overdueDeals.Where(x => expectedBidsCacheKey.Contains(x.Key)).Select(x => x.Value));
                if (vpdList.Count() == businessHourGuidList.Count())
                {
                    return vpdList;
                }

                //再取得差集Bid
                List<Guid> expectedBids = businessHourGuidList.Except(vpdList.Select(x => x.BusinessHourGuid)).ToList();
                //補上暫存中沒有的資料&存入過期資料暫存
                var temp = pp.ViewPponDealGetByBusinessHourGuidList(expectedBids).Select(x => MakeViewPponDealOverdueCache(x));
                vpdList.AddRange(temp);
                return vpdList;
            }

            private ViewPponDeal MakeViewPponDealOverdueCache(ViewPponDeal vpd, bool noClone = false)
            {
                string cacheKey = string.Format("cache://vpd/{0}", vpd.BusinessHourGuid);
                ViewPponDeal tempVpd;
                if (_overdueDeals.TryGetValue(cacheKey, out tempVpd) == false)
                {
                    _overdueDeals.TryAdd(cacheKey, vpd);
                }
                else if (noClone == false)
                {
                    vpd = tempVpd.Clone();
                    vpd.IsLoaded = true;
                }
                LoadLastDayTag(vpd);
                LoadHoverMessageByDeal(vpd);
                if ((vpd.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0 && vpd.ComboDelas == null)
                {
                    var combodeals = GetViewComboDealCollectionByMainGuid(vpd.BusinessHourGuid);
                    vpd.OrderedQuantity = combodeals.Sum(x => x.OrderedQuantity);
                    if (vpd.Slug != null) //若已結檔才將數字重新塞進去
                    {
                        vpd.Slug = vpd.OrderedQuantity.ToString();
                    }
                    vpd.OrderedTotal = combodeals.Sum(x => x.OrderedQuantity * x.ItemPrice);
                }
                return vpd;
            }

            public IViewPponDeal ViewPponDealGetByBid(Guid businessHourGuid, bool noClone, bool reload = false)
            {
                ViewPponDeal rtn;
                if (reload)
                {
                    rtn = pp.ViewPponDealGetByBusinessHourGuid(businessHourGuid);
                    SetCustomProps(rtn);
                    return rtn;
                }
                rtn = _collection.Get(businessHourGuid);
                if (rtn != null)
                {
                    if (noClone == false)
                    {
                        rtn = rtn.Clone();
                    }
                }
                else
                {
                    string cacheKey = string.Format("cache://vpd/{0}", businessHourGuid);
                    if (_overdueDeals.TryGetValue(cacheKey, out rtn) == false)
                    {
                        rtn = cache.Get<ViewPponDeal>(cacheKey);
                        if (rtn == null)
                        {
                            rtn = pp.ViewPponDealGetByBusinessHourGuid(businessHourGuid);

                            if (rtn.IsLoaded == false)
                            {
                                return rtn;
                            }
                            SetCustomProps(rtn);
                            if (Helper.IsFlagSet(rtn.GroupOrderStatus.GetValueOrDefault(), GroupOrderStatus.Completed))
                            {
                                cache.Set(cacheKey, rtn, new TimeSpan(30, 0, 0, 0));
                            }
                        }
                        _overdueDeals.TryAdd(cacheKey, rtn);
                    }
                    else if (noClone == false)
                    {
                        rtn = rtn.Clone();
                    }
                }

                //若已結檔才將數字重新塞進去
                if ((rtn.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0 && rtn.ComboDelas != null && rtn.Slug != null)
                {
                    rtn.Slug = rtn.OrderedQuantity.ToString();
                }

                rtn.IsLoaded = true;
                return rtn;
            }
            private void SetCustomProps(ViewPponDeal deal)
            {
                LoadLastDayTag(deal);
                LoadHoverMessageByDeal(deal);
                UpdatePponDealEventName(deal);
                FillVpdComboDeals(deal);
                Fill24HCategory(deal);
            }

            private void Fill24HCategoryByDeals()
            {
                foreach (var item in _collection)
                {
                    Fill24HCategory(item);
                }
            }

            private void Fill24HCategory(ViewPponDeal deal)
            {
                if (deal.IsWms && deal.CategoryIds.Contains(_24H_ARRIVAL_CATEGORY_ID) == false)
                {
                    deal.CategoryIds.Add(_24H_ARRIVAL_CATEGORY_ID);
                }
                //最後一天
                if (DateTime.Now >= deal.BusinessHourOrderTimeE.AddDays(-1))
                {
                    deal.CategoryIds.Add(318);
                }
            }

            /// <summary>
            /// 依照檔號搜尋ViewPponDeal資料，先找暫存中，若無則找DB
            /// </summary>
            /// <param name="uid"></param>
            /// <param name="clone"></param>
            /// <returns></returns>
            public IViewPponDeal ViewPponDealGetByUniqueId(int uid, bool clone = false)
            {
                ViewPponDeal rtn = _collection.Get(uid);
                if (rtn != null)
                {
                    if (clone)
                    {
                        rtn = rtn.Clone();
                    }
                    rtn.IsLoaded = true;
                }
                else
                {
                    rtn = pp.ViewPponDealGetByUniqueId(uid);
                    if (rtn.IsLoaded == false)
                    {
                        return rtn;
                    }
                    LoadLastDayTag(rtn);
                    LoadHoverMessageByDeal(rtn);
                    UpdatePponDealEventName(rtn);
                    FillVpdComboDeals(rtn);
                }

                //若已結檔才將數字重新塞進去
                if ((rtn.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0 && rtn.ComboDelas == null && rtn.Slug != null)
                {
                    rtn.Slug = rtn.OrderedQuantity.ToString();
                }

                return rtn;
            }

            public List<IViewPponDeal> GetOnlineDealsBySeller(Guid sellerGuid)
            {
                if (_sellerDeals.ContainsKey(sellerGuid) == false)
                {
                    return new List<IViewPponDeal>();
                }
                return _sellerDeals[sellerGuid];
            }

            public BaseCostGrossMargin BaseCostGrossMarginGet(Guid bid, BaseCostGrossMargin gm = null)
            {
                return _baseCostGrossMargin.GetOrAdd(bid, delegate
                {
                    return gm ?? new BaseCostGrossMargin(pp.ViewDealBaseGrossMarginGet(bid));
                });
            }

            public List<BaseCostGrossMargin> BaseCostGrossMarginGet(List<Guid> bids)
            {
                List<BaseCostGrossMargin> result = new List<BaseCostGrossMargin>();

                foreach (var bid in bids)
                {
                    BaseCostGrossMargin ggm = BaseCostGrossMarginGet(bid);
                    if (ggm != null && ggm.IsLoaded)
                    {
                        result.Add(ggm);
                    }
                }
                return result;
            }

            /// <summary>
            /// 取得每日一物類型
            /// </summary>
            protected void LoadEveryDayNewDeals()
            {
                int categoryId = config.EveryDayNewDealsCategoryId;

                ViewCategoryDealListCollection vcdlc = pp.GetCategoryDealListByCidAndOrderTime(categoryId, DateTime.Now.ToString(), DateTime.Now.ToString());
                VacationCollection recentHolidays = pp.GetVacationListByPeriod(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(14));


                foreach (var cdl in vcdlc)
                {
                    DateTime BusinessHourOrderTimeS, BusinessHourOrderTimeE;
                    if (DateTime.TryParse(cdl.BusinessHourOrderTimeS.ToString(), out BusinessHourOrderTimeS) && DateTime.TryParse(cdl.BusinessHourOrderTimeE.ToString(), out BusinessHourOrderTimeE))
                    {
                        DateTime NextWorkDay = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, BusinessHourOrderTimeS, BusinessHourOrderTimeS.AddDays(1));
                        int DiffDay = 0;
                        if (NextWorkDay >= BusinessHourOrderTimeE)
                        {
                            DiffDay = (Convert.ToDateTime(BusinessHourOrderTimeE.ToString("yyyy/MM/dd")) - Convert.ToDateTime(BusinessHourOrderTimeS.ToString("yyyy/MM/dd"))).Days;
                        }
                        else
                        {
                            DiffDay = (NextWorkDay - BusinessHourOrderTimeS).Days;
                        }

                        EveryDayNewDeal flag = EveryDayNewDeal.None;
                        if (NextWorkDay < DateTime.Now)
                        {
                            flag = EveryDayNewDeal.None;
                        }
                        else if (DiffDay <= 1)
                        {
                            flag = EveryDayNewDeal.Limited24HR;
                        }
                        else if (DiffDay == 2)
                        {
                            flag = EveryDayNewDeal.Limited48HR;
                        }
                        else if (DiffDay == 3)
                        {
                            flag = EveryDayNewDeal.Limited72HR;
                        }
                        else if (DiffDay >= 4)
                        {
                            flag = EveryDayNewDeal.Time;
                        }

                        if (!_everydaynewdeals.ContainsKey(cdl.Bid))
                        {
                            _everydaynewdeals.Add(cdl.Bid, flag);
                        }
                    }
                }
            }

        }

        protected class CacheableKernel : Kernel
        {
            public override void Reset()
            {
                DateTime baseDate = PponDealHelper.GetTodayBaseDate();
                List<Guid> bids = pp.OnlineDealGuidGetList(baseDate.AddDays(-1), baseDate.AddDays(1));
                foreach (Guid bid in bids)
                {
                    string cacheKey = "vpd::" + bid;
                    cache.Remove(cacheKey);
                }
            }

            public override void Reload()
            {
                StopwatchLog watch = StopwatchLog.Get("vpd");
                LoadTodayBaseDeal(true && cache is RedisCacheProvider,
                    new Task(delegate ()
                    {
                        LoadComboDeals();
                    }, TaskCreationOptions.LongRunning),
                    new Task(delegate ()
                    {
                        LoadCategories();
                    }, TaskCreationOptions.LongRunning),
                    new Task(delegate ()
                    {
                        LoadTodayDealTimeSlot();
                    }, TaskCreationOptions.LongRunning),
                    new Task(delegate ()
                    {
                        LoadBaseCostAndGrossProfit();
                    }, TaskCreationOptions.LongRunning),
                    new Task(delegate ()
                    {
                        LoadEveryDayNewDeals();
                    }, TaskCreationOptions.LongRunning),
                    new Task(delegate ()
                    {
                        LoadTodayDealEvaluate();
                    }, TaskCreationOptions.LongRunning),
                    new Task(delegate ()
                    {
                        LoadCpaMainGetListByCurrent();
                    }, TaskCreationOptions.LongRunning)
                );

                if (_collection.Count == 0)
                {
                    logger.Fatal("嚴重錯誤，從cahche跟db都找不到檔次");
                    throw new Exception("嚴重錯誤，從cahche跟db都找不到檔次");
                }
                watch.Log("LoadTodayBaseDeal");
                LoadPponFixCategories();
                LoadTravelCategories();
                InitSellerDeals();
                LoadLastDayTags();
                watch.Log("LoadLastDayTags");
                LoadDealHoverMessage();
                watch.Log("LoadDealHoverMessage");
                LoadViewPponSellerNameData();
                watch.Log("LoadViewPponSellerNameData");
                SetLogicProperties();
                watch.Log("SetLogicProperties");
            }
        }

        #region const

        public const string ManagerNameForJob = "ViewPponDealManager";

        public static readonly DeliveryType[] PponDeliverytype = new DeliveryType[] { DeliveryType.ToHouse, DeliveryType.ToShop };

        public const int _24H_ARRIVAL_CATEGORY_ID = 3000;

        #endregion const

        #region static

        private static ViewPponDealManager _manager;
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(ViewPponDealManager));

        public static ViewPponDealManager DefaultManager
        {
            get
            {
                return _manager ?? (_manager = new ViewPponDealManager());
            }
        }

        public static int MaxLoadDataWaitSeconds = 60;

        public static int RoundNo
        {
            get;
            private set;
        }

        public static List<int> AppChannelIdNew
        {
            get
            {
                List<int> result = new List<int>();
                foreach (string item in config.MobileChannel.Split(','))
                {
                    result.Add(Convert.ToInt32(item));
                }
                return result;
            }
        }

        private static object isRunnningLock = new object();
        private static bool _isRunnning;
        private static object lastErrorLock = new object();
        public static Exception _lastError;
        public static Exception LastError
        {
            get
            {
                lock (lastErrorLock)
                {
                    return _lastError;
                }
            }
            set
            {
                lock (lastErrorLock)
                {
                    _lastError = value;
                }
            }
        }
        public static bool IsRunnning
        {
            get
            {
                lock (isRunnningLock)
                {
                    return _isRunnning;
                }
            }
            set
            {
                lock (isRunnningLock)
                {
                    _isRunnning = value;
                }
            }
        }

        /// <summary>
        /// 產生新的DefaultPponDealManager資料將會同步更新
        /// </summary>
        /// <param name="clearDataAtCacheServer">強迫清空  cache server 上的資料</param>        
        public static void ReloadDefaultManager(bool clearDataAtCacheServer = false)
        {
            if (IsRunnning)
            {
                return;
            }
            IsRunnning = true;
            try
            {
                StopwatchLog watch = StopwatchLog.Get("vpd")
                    .SetLog(StopwatchLog.LogLevel.Warn, t => RoundNo < 3 ||
                        ProviderFactory.Instance().GetConfig().ViewPponDealManagerLogEnabled);

                watch.LogFormat(false, "Round {0} begin... mode {1}{2}.",
                    RoundNo + 1,
                    config.CacheServerMode,
                    config.CacheItemCompress ? ", data compressed" : "");

                ViewPponDealManager newOne = new ViewPponDealManager();
                if (clearDataAtCacheServer)
                {
                    newOne._kernel.Reset();
                }
                newOne._kernel.Reload();
                watch.Stop();
                newOne.IsLoaded = true;
                _manager = newOne;
                RoundNo++;
                LastError = null;
            }
            catch (Exception ex)
            {
                logger.Error("ReloadDefaultManager error,", ex);
                LastError = ex;
            }
            finally
            {
                IsRunnning = false;
            }

        }

        /// <summary>
        /// 將DefaultManager清空
        /// </summary>
        public static void ClearDefaultManager()
        {
            var newOne = new ViewPponDealManager();
            _manager = newOne;
        }

        /// <summary>
        /// 剔除傳入html自串中, table 與 iframe 標籤的內容
        /// </summary>
        /// <param name="htmlstring"></param>
        /// <returns></returns>
        public static string RemoveSpecialHtmlForApp(string htmlstring)
        {
            if (string.IsNullOrWhiteSpace(htmlstring))
            {
                return string.Empty;
            }
            //去除多餘的空格
            htmlstring = htmlstring.Replace("\t", "");

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlstring);

            List<string> removeXpaths = new List<string>();
            foreach (HtmlNode node in doc.DocumentNode.Descendants())
            {
                //拿掉所有table
                if (node.Name.ToLower() == "table")
                {
                    removeXpaths.Add(node.XPath);
                    continue;
                }

                if (config.AppDescEnableIframe == false)
                {
                    //拿掉所有的iframe
                    if ((!config.AppHtmlIncludeIFrame) && (node.Name.ToLower() == "iframe"))
                    {
                        removeXpaths.Add(node.XPath);
                        continue;
                    }
                }

                //修改img的style
                if (node.Name.ToLower() == "img")
                {
                    HtmlAttribute attr = node.Attributes["style"];
                    if (attr != null)
                    {
                        attr.Value = @"max-width: 100%; width: auto; height: auto;";
                    }
                    else
                    {
                        //原本沒有style這個屬性，新增
                        node.SetAttributeValue("style", @"max-width: 100%; width: auto; height: auto;");
                    }
                }
            }

            for (int i = removeXpaths.Count - 1; i >= 0; i--)
            {
                string xpath = removeXpaths[i];
                doc.DocumentNode.SelectSingleNode(xpath).Remove();
            }

            string fullyFormarttedContentString = doc.DocumentNode.OuterHtml;
            return fullyFormarttedContentString;
        }

        #endregion static

        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();

        //需留意 thread safe

        private ConcurrentDictionary<Guid, IThumbNailImage> _thumbnails;
        public bool IsLoaded { get; set; }
        private Kernel _kernel;

        private ViewPponDealManager()
        {
            _thumbnails = new ConcurrentDictionary<Guid, IThumbNailImage>();
            if (config.CacheServerMode == 1)
            {
                _kernel = new CacheableKernel();
            }
            else
            {
                _kernel = new Kernel();
            }
        }

        #region public method

        /// <summary>
        /// 回傳銷售量最好之前N筆
        /// </summary>
        /// <param name="topNumber"></param>
        /// <returns></returns>
        public List<IViewPponDeal> ViewPponDealGetByTopSalesDeal(int topNumber)
        {
            var items = _kernel._collection
                .Where(x => x.BusinessHourOrderTimeS <= System.DateTime.Now && x.BusinessHourOrderTimeE >= System.DateTime.Now)
                .OrderByDescending(x => x.OrderedTotal).Take(topNumber);
            return new List<IViewPponDeal>(items);
        }

        /// <summary>
        /// 回傳憑證銷售量最好之前N筆
        /// </summary>
        /// <param name="topNumber"></param>
        /// <returns></returns>
        public List<IViewPponDeal> ViewPponDealFoolGetByTopSalesDeal(int topNumber)
        {
            var items = _kernel._collection
                .Where(
                    x => x.BusinessHourOrderTimeS <= DateTime.Now && x.BusinessHourOrderTimeE >= System.DateTime.Now)
                .Where(x => x.DeliveryType == 1)
                .OrderByDescending(x => x.OrderedTotal).Take(topNumber);
            return new List<IViewPponDeal>(items);
        }

        /// <summary>
        /// 依據傳遞的businessHourGuid查詢ViewPponDeal的資料，
        /// 如果暫存的資料中，已有，則直接回傳
        /// 否則 查詢DB的資料回傳。
        /// </summary>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public IViewPponDeal ViewPponDealGetByBid(Guid businessHourGuid)
        {
            return ViewPponDealGetByBid(businessHourGuid, false);
        }

        public IViewPponDeal ViewPponDealGetByBid(Guid businessHourGuid, bool noClone, bool reload = false)
        {
            return _kernel.ViewPponDealGetByBid(businessHourGuid, noClone, reload);
        }

        public List<IViewPponDeal> ViewPponDealGetByBid(List<Guid> businessHourGuid)
        {
            List<IViewPponDeal> result = new List<IViewPponDeal>();
            foreach (Guid bid in businessHourGuid)
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
                if (deal != null)
                {
                    result.Add(deal);
                }
            }
            return result;
        }

        public List<IViewPponDeal> GetOnlineDealsBySeller(Guid sellerGuid, Guid excludeBid)
        {
            List<IViewPponDeal> deals = _kernel.GetOnlineDealsBySeller(sellerGuid)
                .Where(t => t.BusinessHourGuid != excludeBid).ToList();
            return deals.Where(t => t.IsHiddenInRecommendDeal == false).ToList();
        }
        /// <summary>
        /// 清檔次1層、2層快取
        /// </summary>
        /// <param name="businessHourGuid"></param>
        public void ClearViewPponDealCache(Guid businessHourGuid)
        {
            _kernel.ClearViewPponDealCache(businessHourGuid);
        }

        /// <summary>
        /// 依照檔號搜尋ViewPponDeal資料，先找暫存中，若無則找DB
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public IViewPponDeal ViewPponDealGetByUniqueId(int uid)
        {
            return _kernel.ViewPponDealGetByUniqueId(uid, false);
        }

        /// <summary>
        /// 回傳目前暫存資料中所擁有的所有ViewPponDeal物件
        /// </summary>
        /// <param name="excludeSubDeals">回傳結果是否排除子檔</param>
        /// <param name="onDeal">過瀘開檔結檔時間，只顯示上檔的</param>
        /// <returns></returns>
        public List<IViewPponDeal> ViewPponDealGetList(bool excludeSubDeals = false, bool onDeal = false)
        {
            string cacheKey = string.Format("ViewPponDealGetList:{0},{1}", excludeSubDeals, onDeal);
            List<IViewPponDeal> vpdCol = MemoryCache2.Get<List<IViewPponDeal>>(cacheKey);
            if (vpdCol != null)
            {
                return vpdCol;
            }
            DateTime now = DateTime.Now;
            vpdCol = new List<IViewPponDeal>();
            foreach (var item in _kernel._collection)
            {
                //排除子檔
                if (excludeSubDeals &&
                    (item.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                {
                    continue;
                }
                if (onDeal &&
                    now < item.BusinessHourOrderTimeS || now > item.BusinessHourOrderTimeE)
                {
                    continue;
                }
                vpdCol.Add(item);

            }
            MemoryCache2.Set(cacheKey, vpdCol, 1);
            return vpdCol;
        }

        /// <summary>
        /// 依據傳遞的businessHourGuid查詢MainBusinessHourGuid的資料，
        /// 如果暫存的資料中，已有，則直接回傳否則 查詢DB的資料回傳。
        /// 子檔回傳MainBusinessHourGuid，一般回傳檔次BusinessHourGuid
        /// </summary>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public Guid MainBusinessHourGuidGetByBid(Guid businessHourGuid)
        {
            IViewPponDeal deal = DefaultManager.ViewPponDealGetByBid(businessHourGuid, true);
            Guid result;
            if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0 && deal.MainBid != null)
            {
                result = deal.MainBid.Value;
            }
            else
            {
                result = deal.BusinessHourGuid;
            }
            return result;
        }

        /// <summary>
        /// 傳入一串BID，依據此BID回傳符合條件的ViewPponDeal的Collection
        /// 會先搜尋暫存檔，若暫存檔中沒有對應的資料，才搜尋資料庫，所以資料為非即時的。
        /// </summary>
        /// <param name="guids"></param>
        /// <param name="cloneDeal"></param>
        /// <returns></returns>
        public List<IViewPponDeal> ViewPponDealGetListByGuidList(List<Guid> guids)
        {
            var rtns = new List<IViewPponDeal>();

            foreach (Guid bid in guids)
            {
                var deal = ViewPponDealGetByBid(bid, true);
                if (deal != null && deal.IsLoaded)
                {
                    rtns.Add(deal);
                }
            }
            return rtns;
        }

        /// <summary>
        /// 取得某城市的P好康的ViewPponDeal資料，DepartmentType為DepartmentTypes.Ppon, DepartmentTypes.PponItem 這兩種
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public List<IViewPponDeal> ViewPponDealGetListForPponByCityWithDataSlot(int cityId)
        {
            CityViewPponDealData data = _kernel._cityViewPponDealPponDatas.FirstOrDefault(x => x.CityId == cityId);
            if (data != null)
            {
                //這邊有效能問題，但也不好說改就改，擔心造成多緒衝突
                return data.DataCollection;
            }
            return new List<IViewPponDeal>(
                pp.ViewPponDealGetListByCityIdOrderBySlotSeq(cityId, PponDeliverytype));
        }

        public List<int> ViewPponDealGetChannelIdListByGuid(Guid business_hour_guid)
        {
            List<int> result;
            _kernel._dealChannelList.TryGetValue(business_hour_guid, out result);
            return result;
        }

        /// <summary>
        /// 取得基礎成本毛利率
        /// </summary>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public BaseCostGrossMargin BaseCostGrossMarginGet(Guid businessHourGuid)
        {
            return _kernel.BaseCostGrossMarginGet(businessHourGuid);
        }

        /// <summary>
        /// 取得基礎成本毛利率
        /// </summary>
        /// <param name="bids"></param>
        /// <returns></returns>
        public List<BaseCostGrossMargin> BaseCostGrossMarginGet(List<Guid> bids)
        {
            return _kernel.BaseCostGrossMarginGet(bids);
        }

        /// <summary>
        /// 首頁類別分類
        /// </summary>
        public List<Category> PponFixCategories
        {
            get
            {
                if (_kernel._pponFixCategories == null)
                {
                    return sp.CategoryGetList((int)CategoryType.PponFix).ToList();
                }
                return _kernel._pponFixCategories ?? new List<Category>();
            }
        }

        /// <summary>
        /// 旅遊區域分類
        /// </summary>
        public List<Category> TravelCategories
        {
            get
            {
                if (_kernel._travelCategories == null)
                {
                    return sp.CategoryGetList((int)CategoryType.Travel).ToList();
                }
                return _kernel._travelCategories;
            }
        }

        public int TravelDefaultCategoryId
        {
            get
            {
                var _travelCategories = _kernel._travelCategories;
                if (_travelCategories == null)
                {
                    _travelCategories = sp.CategoryGetList((int)CategoryType.Travel).ToList();
                }
                Category travel = _travelCategories
                    .FirstOrDefault(t => t.ParentCode == null);
                return travel.Id;
            }
        }

        /// <summary>
        /// 採用新版Category規則，依據channel的內容回傳MultipleMainDealPreview的資料供檔次顯示於前台
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="channelAreaId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public List<MultipleMainDealPreview> MultipleMainDealPreviewGetListByChannel(int channelId, int? channelAreaId, int? categoryId)
        {
            List<int> keyList = new List<int>();
            keyList.Add(channelId);
            if (channelAreaId.HasValue)
            {
                keyList.Add(channelAreaId.Value);
            }
            if (categoryId.HasValue)
            {
                keyList.Add(categoryId.Value);
            }
            //由小到大排序
            keyList = keyList.Where(x => x > 0).OrderBy(x => x).ToList();

            //取出對應的pponCity，若無對應資料回傳空的陣列
            PponCity city = PponCityGroup.GetPponCityByChannel(channelId, channelAreaId);
            if (city == null)
            {
                return new List<MultipleMainDealPreview>();
            }

            return MultipleMainDealPreviewGetListByCategoryCriteria(city, keyList);

        }

        /// <summary>
        /// 採用新版Category規則，依據channel的內容回傳MultipleMainDealPreview的資料供檔次顯示於前台
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="channelAreaId"></param>
        /// <param name="categoryList"></param>
        /// <param name="city">//取出對應的pponCity，若無對應資料回傳空的陣列</param>
        /// <returns></returns>
        public List<MultipleMainDealPreview> MultipleMainDealPreviewGetListByCategoryCriteria(int channelId,
                                                                                              int? channelAreaId,
                                                                                              List<int> categoryList,
                                                                                              PponCity city)
        {
            List<int> keyList = new List<int>();
            keyList.Add(channelId);
            if (channelAreaId.HasValue)
            {
                keyList.Add(channelAreaId.Value);
            }
            if (categoryList.Count > 0)
            {
                keyList.AddRange(categoryList);
            }
            //過濾掉數值小於0的項目，由小到大排序
            keyList = keyList.Where(x => x > 0).OrderBy(x => x).ToList();

            if (city == null)
            {
                return new List<MultipleMainDealPreview>();
            }

            return MultipleMainDealPreviewGetListByCategoryCriteria(city, keyList);
        }

        /// <summary>
        /// 採用新版Category規則，依據channel的內容回傳MultipleMainDealPreview的資料供檔次顯示於前台
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="channelAreaId"></param>
        /// <param name="categoryList"></param>
        /// <returns></returns>
        public List<MultipleMainDealPreview> MultipleMainDealPreviewGetListByCategoryCriteria(int channelId,
                                                                                              int? channelAreaId,
                                                                                              List<int> categoryList)
        {
            List<int> keyList = new List<int>();
            keyList.Add(channelId);
            if (channelAreaId.HasValue)
            {
                keyList.Add(channelAreaId.Value);
            }
            if (categoryList.Count > 0)
            {
                keyList.AddRange(categoryList);
            }
            //過濾掉數值小於0的項目，由小到大排序
            keyList = keyList.Where(x => x > 0).Distinct().OrderBy(x => x).ToList();

            PponCity city = PponCityGroup.GetPponCityByChannel(channelId, channelAreaId);
            if (city == null)
            {
                return new List<MultipleMainDealPreview>();
            }
            var result = MultipleMainDealPreviewGetListByCategoryCriteria(city, keyList);
            return result;
        }

        public List<MultipleMainDealPreview> MultipleTodayDealPreviewGetListByCategoryCriteria(int workCityId,
                                                                                              List<int> categoryList)
        {
            List<MultipleMainDealPreview> previewList = new List<MultipleMainDealPreview>();
            DateTime date = PponFacade.GetOrderStartTimeAtPresent();
            foreach (int categoryId in categoryList)
            {
                List<MultipleMainDealPreview> dealList = new List<MultipleMainDealPreview>();
                PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(workCityId);
                List<int> cid = new List<int>();
                cid.Add(categoryId);
                int _cityid = workCityId;
                if (categoryId == CategoryManager.Default.PponDeal.CategoryId)
                {
                    if (city != null)
                    {
                        cid.Add(city.CategoryId);
                    }
                    else
                    {
                        cid.Add(workCityId);
                    }
                }
                //全家的代碼要換成430，網頁才會導去正確的頁面
                if (categoryId == CategoryManager.Default.Family.CategoryId)
                {
                    _cityid = PponCityGroup.DefaultPponCityGroup.Family.CityId;
                }

                List<PponDealPreviewData> matchPreviewDataList = PponDealPreviewManager.GetTodayDealPreviewDataListWithCategory(date, cid);
                if (matchPreviewDataList.Count == 0)
                {
                    List<PponDealPreviewData> multipDataList = PponDealPreviewManager.GetDealPreviewDataListWithCategory(cid);
                    List<PponDealPreviewData> dataList = new List<PponDealPreviewData>();
                    int switchDay = 0;
                    //如果沒有當日上檔的資料(假日)，往前撈十天
                    do
                    {
                        DateTime workDate = date.AddDays(-switchDay);

                        dataList = PponDealPreviewManager.GetTodayDealPreviewDataListWithCategory(workDate, cid);

                        foreach (PponDealPreviewData _data in dataList)
                        {
                            IViewPponDeal ppdeal = this.ViewPponDealGetByBid(_data.BusinessHourGuid, true);
                            MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(
                                _cityid, categoryId, ppdeal, cid, DealTimeSlotStatus.Default, 0);
                            dealList.Add(mainDealPreview);
                        }

                        //預備搜尋前一天的資料
                        switchDay++;
                    } while (dataList.Count == 0 && switchDay < 10);
                }
                else
                {
                    foreach (PponDealPreviewData _data in matchPreviewDataList)
                    {
                        ViewPponDeal ppdeal = pp.ViewPponDealGet(ViewPponDeal.Columns.UniqueId + "=" + _data.DealId);
                        MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(_cityid, categoryId, ppdeal, cid, DealTimeSlotStatus.Default, 0);
                        dealList.Add(mainDealPreview);
                    }
                }

                previewList.AddRange(dealList);
            }

            return previewList;
        }

        public List<MultipleMainDealPreview> MultipleLastDealPreviewGetListByCategoryCriteria(int workCityId,
                                                                                              List<int> categoryList)
        {
            List<MultipleMainDealPreview> previewList = new List<MultipleMainDealPreview>();
            DateTime date = PponFacade.GetOrderStartTimeAtPresent();
            foreach (int categoryId in categoryList)
            {
                List<MultipleMainDealPreview> dealList = new List<MultipleMainDealPreview>();
                PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(workCityId);
                List<int> cidList = new List<int>();
                cidList.Add(categoryId);
                int cityId = workCityId;
                if (categoryId == CategoryManager.Default.PponDeal.CategoryId)
                {
                    if (city != null)
                    {
                        cidList.Add(city.CategoryId);
                    }
                    else
                    {
                        cidList.Add(workCityId);
                    }
                }
                //全家的代碼要換成430，網頁才會導去正確的頁面
                if (categoryId == CategoryManager.Default.Family.CategoryId)
                {
                    cityId = PponCityGroup.DefaultPponCityGroup.Family.CityId;
                }

                List<PponDealPreviewData> matchPreviewDataList = PponDealPreviewManager.GetLastDayDealPreviewDataListWithCategory(cidList);
                if (matchPreviewDataList != null)
                {
                    foreach (PponDealPreviewData data in matchPreviewDataList)
                    {
                        IViewPponDeal ppdeal = ViewPponDealGetByBid(data.BusinessHourGuid, true);
                        MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(
                            cityId, categoryId, ppdeal, cidList, DealTimeSlotStatus.Default, 0);
                        dealList.Add(mainDealPreview);
                    }
                }

                previewList.AddRange(dealList);
            }

            return previewList;
        }

        /// <summary>
        /// 依據某個城市查詢此城市下某個分類的資料
        /// </summary>
        /// <param name="city"></param>
        /// <param name="categoryCriteria"></param>
        /// <returns></returns>
        public List<MultipleMainDealPreview> MultipleMainDealPreviewGetListByCategoryCriteria(PponCity city,
                                                                                              List<int> categoryCriteria)
        {
            //由小到大排序
            categoryCriteria = categoryCriteria.Where(x => x > 0).OrderBy(x => x).Distinct().ToList();

            //city若為空，回傳空的陣列
            if (city == null)
            {
                return new List<MultipleMainDealPreview>();
            }
            string key = string.Join(",", categoryCriteria.ToArray());
            List<MultipleMainDealPreview> dataList;
            if (_kernel._categoryDeals.TryGetValue(key, out dataList))
            {
                return dataList;
            }
            //嘗試從備用快取讀取資料，如果有，回傳結果給使用者，再進一步更新資料到一級、備用快取
            if (SpareCache.TryGetCategoryDeals(key, city, categoryCriteria,
                delegate (List<MultipleMainDealPreview> newList)
                {
                    _kernel._categoryDeals.TryAdd(key, newList);
                }, out dataList))
            {
                return dataList;
            }
            dataList = MultipleMainDealPreviewGetListByKey(city, categoryCriteria);
            _kernel._categoryDeals.TryAdd(key, dataList);
            SpareCache.PrevCategoryDeals.TryAdd(key, dataList);
            return dataList;
        }

        /// <summary>
        /// 依據某個城市查詢此城市下某個分類的資料
        /// </summary>
        /// <param name="city"></param>
        /// <param name="categoryCriteria"></param>
        /// <returns></returns>
        public List<MultipleMainDealPreview> MultipleMainDealPreviewGetListByKey(
            PponCity city, List<int> categoryCriteria)
        {
            //由小到大排序
            categoryCriteria = categoryCriteria.Where(x => x > 0).OrderBy(x => x).Distinct().ToList();

            //city若為空，回傳空的陣列
            if (city == null)
            {
                return new List<MultipleMainDealPreview>();
            }
            string key = string.Join(",", categoryCriteria.ToArray());
            return _kernel._categoryDeals.GetOrAdd(key, delegate
            {
                List<MultipleMainDealPreview> dataList = new List<MultipleMainDealPreview>();

                List<IViewPponDeal> vpdc = ViewPponDealGetListForPponByCityWithDataSlot(city.CityId);

                DateTime effectiveTime = DateTime.Now;

                Dictionary<Guid, IDealTimeSlot> timeSlotDict;

                try
                {
                    timeSlotDict = _kernel._dealTimeSlotCollection
                        .Where(t => t.EffectiveStart <= effectiveTime && t.EffectiveEnd > effectiveTime
                            && t.CityId == city.CityId).ToDictionary(t => t.BusinessHourGuid);                    
                }
                catch (Exception ex)
                {
                    logger.WarnFormat("MultipleMainDealPreviewGetListByKey 發生重複bid值事件, key={0},ex={1},啟用B計劃",
                        key, ex);
                    timeSlotDict = new Dictionary<Guid, IDealTimeSlot>();
                    var rawSlots = _kernel._dealTimeSlotCollection
                        .Where(t => t.EffectiveStart <= effectiveTime && t.EffectiveEnd > effectiveTime
                                    && t.CityId == city.CityId);
                    foreach (var slots in rawSlots)
                    {
                        if (timeSlotDict.ContainsKey(slots.BusinessHourGuid) == false)
                        {
                            timeSlotDict.Add(slots.BusinessHourGuid, slots);
                        }
                    }

                }



                foreach (var vpd in vpdc)
                {
                    bool match = categoryCriteria.All(t => vpd.CategoryIds.Contains(t));
                    if (match == false)
                    {
                        continue;
                    }
                    if (timeSlotDict.ContainsKey(vpd.BusinessHourGuid) == false)
                    {
                        continue;
                    }
                    var slot = timeSlotDict[vpd.BusinessHourGuid];
                    dataList.Add(new MultipleMainDealPreview(
                        city.CityId, slot.Sequence, vpd, vpd.CategoryIds, (DealTimeSlotStatus)slot.Status,
                        _kernel.BaseCostGrossMarginGet(vpd.BusinessHourGuid).BaseGrossMargin));
                }
                dataList = dataList.OrderBy(t => t.Sequence).ToList();

                // 點選品生活的全部(無分類過瀘)，不免顯示 visa優先 的檔次
                if (categoryCriteria.Count == 1 && categoryCriteria[0] == CategoryManager.Default.Piinlife.CategoryId)
                {
                    dataList = dataList.Where(t => t.PponDeal.IsVisaNow == false).ToList();
                }
                VbsDealType dealType = categoryCriteria.Contains(CategoryManager.Default.Piinlife.CategoryId)
                    ? VbsDealType.PiinLife
                    : VbsDealType.Ppon;
                for (int i = 0; i < dataList.Count; i++)
                {
                    var preview = dataList[i];
                    preview.AppPponDeal = PponDealApiManager.PponDealSynopsisGetByDeal(
                        preview.PponDeal, preview.DealCategoryIdList, preview.Sequence,
                        dealType, true, null);
                }
                return dataList;
            });
        }

        public void ResetForDealTimeSlotsChange()
        {
            this._kernel.LoadViewPponSellerNameData();
            this._kernel.LoadTodayDealTimeSlot();
            SpareCache.PrevCategoryDeals.Clear();
            _kernel._categoryDeals.Clear();
        }

        public List<MultipleMainDealPreview> HotSaleMultipleMainDealPreviewGetListByCityId(int cityId, int? categoryId)
        {
            int hotSaleBigDealsRangeCount = 20;
            KeyValuePair<int, int?> kp = new KeyValuePair<int, int?>(cityId, categoryId);
            return _kernel._cityHotSaleDeals.GetOrAdd(kp, delegate
            {
                List<MultipleMainDealPreview> dataList = new List<MultipleMainDealPreview>();
                //取得目前城市
                PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId);
                //目前城市沒有對應的頻道，目前應該只有24或72小時會這樣，直接設定為台北
                if (!city.ChannelId.HasValue)
                {
                    city = PponCityGroup.DefaultPponCityGroup.TaipeiCity;
                }
                //需依據不同的狀況設定區域的categoryId，主要是針對旅遊需另外判斷
                int? areaCategoryId = null;
                //判斷是否為旅遊頻道
                if (cityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId || cityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
                {
                    areaCategoryId = categoryId;
                }
                else
                {
                    areaCategoryId = city.ChannelAreaId;
                }

                dataList = MultipleMainDealPreviewGetListByCategoryCriteria(city.ChannelId.Value, areaCategoryId, new List<int>());
                dataList = dataList.OrderByDescending(x => x.PponDeal.ItemPrice * x.PponDeal.OrderedQuantity).Take(dataList.Count > hotSaleBigDealsRangeCount ? hotSaleBigDealsRangeCount : dataList.Count).ToList();

                return dataList;
            });
        }

        /// <summary>
        /// 依據categoryId取得今日最新上檔的資料
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public List<MultipleMainDealPreview> MultipleMainDealPreviewGetNewListByCategory(int categoryId)
        {
            DateTime date = PponFacade.GetOrderStartTimeAtPresent();

            KeyValuePair<DateTime, int> keyData = new KeyValuePair<DateTime, int>(date, categoryId);
            return _kernel._categoryNewDeals.GetOrAdd(keyData, delegate
            {
                int? channelId = null;
                int? channelAreaId = null;
                CategoryTypeNode channelTree = CategoryManager.PponChannelCategoryTree;

                //檢查所有頻道的區域屬性，用以確定傳入的categoryId屬於哪個頻道的與哪個區域
                foreach (var channelNode in channelTree.CategoryNodes)
                {
                    //傳入的categoryId屬於頻道，直接設定channelId的值，結束迴圈
                    if (channelNode.CategoryId == categoryId)
                    {
                        channelId = categoryId;
                        break;
                    }
                    //不屬於頻道，檢查區域有無符合的資料
                    foreach (var areaNode in channelNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea))
                    {
                        if (areaNode.CategoryId == categoryId)
                        {
                            channelId = channelNode.CategoryId;
                            channelAreaId = areaNode.CategoryId;
                            break;
                        }
                    }
                    //有找到值了，終止迴圈
                    if (channelId.HasValue)
                    {
                        break;
                    }
                }
                //頻道資料為null，表示傳入的categoryId不為頻道或區域的ID，直接回傳空陣列
                if (!channelId.HasValue)
                {
                    return new List<MultipleMainDealPreview>();
                }
                //取得某個分區所有的檔次資料
                List<MultipleMainDealPreview> multipDataList = MultipleMainDealPreviewGetListByChannel(channelId.Value,
                                                                                                       channelAreaId, null);
                //我們只要最新上檔的，排除時間不符合的檔次，如果碰到假日，不會有新的檔次，所以需往前搜尋直到找到檔次或已經嘗試十次為止
                List<MultipleMainDealPreview> rtnDataList;
                int switchDay = 0;
                do
                {
                    DateTime workDate = date.AddDays(-switchDay);
                    rtnDataList = multipDataList.Where(
                        x =>
                        x.PponDeal.BusinessHourOrderTimeS >= workDate && x.PponDeal.BusinessHourOrderTimeS < workDate.AddDays(1))
                                                   .ToList();
                    //預備搜尋前一天的資料
                    switchDay++;
                } while (rtnDataList.Count == 0 && switchDay < 10);

                return rtnDataList;
            });
        }

        public IEnumerable<MultipleMainDealPreview> SortMultipleMainDealPreview(List<MultipleMainDealPreview> deals, CategorySortType sortType)
        {
            IEnumerable<MultipleMainDealPreview> rtn;
            switch (sortType)
            {
                case CategorySortType.TopNews:
                    rtn = deals.OrderByDescending(x => x.PponDeal.BusinessHourOrderTimeS).ThenBy(x => x.Sequence);
                    break;

                case CategorySortType.TopOrderTotal:
                    rtn = deals.OrderByDescending(x => x.PponDeal.GetAdjustedOrderedQuantity().Quantity);
                    break;

                case CategorySortType.DiscountAsc:
                    rtn = deals.OrderBy(x => x.PponDeal.ItemPrice / (x.PponDeal.ItemOrigPrice != 0 ? x.PponDeal.ItemOrigPrice : 1));
                    break;

                case CategorySortType.PriceDesc:
                    rtn = deals.OrderByDescending(x => x.PponDeal.ItemPrice).ThenByDescending(x => x.PponDeal.ExchangePrice);
                    break;

                case CategorySortType.PriceAsc:
                    rtn = deals.OrderBy(x => x.PponDeal.ItemPrice).ThenBy(x => x.PponDeal.ExchangePrice);
                    break;

                default:
                    rtn = deals.OrderBy(x => x.Sequence);
                    break;
            }
            return rtn;
        }

        public string GetDebugData(Guid bid)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var pair in _kernel._categoryDeals)
            {
                if (pair.Value.Exists(t => t.PponDeal.BusinessHourGuid == bid))
                {
                    sb.AppendFormat("_categoryDeals:{0}\r\n", pair.Key);
                }
            }
            return sb.ToString();
        }

        public bool TryGetAdjustedOrderedQuantity(Guid bid, bool isIncludePreviousQuantity, QuantityAdjustment quantityAdjustment,
            out QuantityAdjustment adjustedOrderedQuantity)
        {
            return _kernel.TryGetAdjustedOrderedQuantity(bid, isIncludePreviousQuantity, quantityAdjustment, out adjustedOrderedQuantity);
        }

        /// <summary>
        /// 多檔次的子檔也記在暫存中
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public IList<ViewComboDeal> GetViewComboDealCollectionByMainGuid(Guid bid)
        {
            return _kernel.GetViewComboDealCollectionByMainGuid(bid);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="businessHourGuid"></param>
        /// <param name="cityId"></param>
        /// <param name="effectiveTime"></param>
        /// <returns></returns>
        public IDealTimeSlot DealTimeSlotGetByBidCityAndTime(Guid businessHourGuid, int cityId, DateTime effectiveTime)
        {
            IDealTimeSlot dts = _kernel._dealTimeSlotCollection.GetByBidCityAndTime(businessHourGuid, cityId, effectiveTime);
            if (dts != null)
            {
                //預設資料同城市同時間應該只會有一筆
                return dts;
            }
            else
            {
                //沒有查到資料，由資料庫查。
                DateTime baseDate = DateTime.ParseExact(effectiveTime.ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
                List<DealTimeSlot> rtns = pp.DealTimeSlotGetList(DealTimeSlot.Columns.Sequence,
                    DealTimeSlot.Columns.BusinessHourGuid + " = " + businessHourGuid,
                    DealTimeSlot.Columns.EffectiveStart + " >= " + baseDate.AddDays(-1),
                    DealTimeSlot.Columns.EffectiveStart + " < " + baseDate.AddDays(1),
                    DealTimeSlot.Columns.CityId + " = " + cityId).ToList();

                if (rtns.Count > 0)
                {
                    return rtns.First();
                }
            }
            return null;
        }

        public IDealTimeSlot DealTimeSlotGetByBidCityAndTimeNotFoundReturnNull(Guid businessHourGuid, int cityId, DateTime effectiveTime)
        {
            IDealTimeSlot dts = _kernel._dealTimeSlotCollection.GetByBidCityAndTime(businessHourGuid, cityId, effectiveTime);
            return dts;
        }

        public List<IDealTimeSlot> DealTimeSlotGetByBidAndTime(Guid businessHourGuid, DateTime effectiveTime)
        {
            List<IDealTimeSlot> dts = _kernel._dealTimeSlotCollection.GetByBidAndTime(businessHourGuid, effectiveTime);
            if (dts.Any())
            {
                return dts;
            }
            else
            {
                //沒有查到資料，由資料庫查。
                DealTimeSlotCollection rtns = pp.DealTimeSlotGetList(DealTimeSlot.Columns.CityId,
                                                DealTimeSlot.BusinessHourGuidColumn + "=" +
                                                businessHourGuid,
                                                DealTimeSlot.EffectiveStartColumn + "=" +
                                                effectiveTime);
                if (rtns.Count > 0)
                {
                    dts.AddRange(rtns);
                    return dts;
                }
            }
            //查無資料，回傳一個空的物件
            return new List<IDealTimeSlot>();
        }
        /// <summary>
        /// 依據城市與時間，取得該時間該城市的檔次排序列表。
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="effectiveTime"></param>
        /// <returns></returns>
        public List<IDealTimeSlot> DealTimeSlotGetByCityAndTime(int cityId,
                                                                   DateTime effectiveTime)
        {
            return
                _kernel._dealTimeSlotCollection.Where(
                    x => x.EffectiveStart <= effectiveTime && x.EffectiveEnd > effectiveTime && x.CityId == cityId)
                       .ToList();
        }

        /// <summary>
        /// 取得當前活動時程開賣的檔次列表
        /// </summary>
        public List<IDealTimeSlot> GetDealTimeSlotInEffectiveTime()
        {
            DateTime effectiveTime = DateTime.Now;
            return _kernel._dealTimeSlotCollection.Where(x => x.EffectiveStart <= effectiveTime && x.EffectiveEnd > effectiveTime).ToList();
        }

        /// <summary>
        /// 取得單檔評價
        /// </summary>
        public PponDealEvaluateStar GetPponDealEvaluateStarList(Guid bid)
        {
            if (config.EnableEvaluateCache)
            {
                var vpd = ViewPponDealGetByBid(bid, true);
                bool isGeneralPpon =
                    vpd.DeliveryType == (int)DeliveryType.ToShop &&
                    !(Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal) ||
                      Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal) ||
                      Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.SKMDeal));

                if (isGeneralPpon)
                {
                    PponDealEvaluateStar result;
                    if (_kernel._dealEvaluateStarList.TryGetValue(bid, out result))
                    {
                        return result;
                    }
                    //可評價檔次，但評價資料不足
                    return new PponDealEvaluateStar { Bid = bid, Cnt = 0, Star = 0 };
                }
                //不可評價檔次，不回傳任何憑價資訊
                return null;
            }
            return null;
        }

        /// <summary>
        /// 依BusinessHourGuid撈取縮圖
        /// </summary>
        /// <param name="bid">BusinessHourGuid</param>
        /// <returns></returns>
        public IThumbNailImage ThumbNailImageGetByBid(Guid bid)
        {
            return _thumbnails.GetOrAdd(bid, delegate
            {
                IViewPponDeal deal = ViewPponDealGetByBid(bid);
                if (deal == null)
                {
                    throw new Exception(string.Format("ThumbNailImageGetByBid bid {0} not found.", bid));
                }
                var thumb = new ThumbNailImage();
                thumb.TransferOriginalImage(
                    ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto)
                        .DefaultIfEmpty(config.SiteUrl + @"/Themes/PCweb/images/ppon-M1_pic.jpg")
                        .First());
                return thumb;
            });
        }

        /// <summary>
        /// 抓取目前運行的cpa列表
        /// </summary>
        /// <returns></returns>
        public CpaMainCollection CpaMainCollectionGetAll()
        {
            if (_kernel._cpamainCollection.Count == 0)
            {
                return pp.CpaMainGetListByCurrent(DateTime.Now);
            }

            return _kernel._cpamainCollection;
        }

        /// <summary>
        /// 使用地點有設定即顯示使用地點 無則city.name
        /// </summary>
        /// <param name="pponcity">目前己不需要，可移除</param>
        /// <param name="bid"></param>
        /// <param name="categoryCode"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public string GetCityNameOrTravelPlace(PponCity pponcity, Guid bid, int? categoryCode = null, int? categoryId = null)
        {
            IViewPponDeal deal = ViewPponDealGetByBid(bid, true);
            if (deal == null)
            {
                return string.Empty;
            }
            return GetCityNameOrTravelPlace(pponcity, deal, categoryCode, categoryId);
        }

        public string GetCityNameOrTravelPlace(PponCity pponcity, IViewPponDeal deal, int? categoryCode = null, int? categoryId = null)
        {
            if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, (int)GroupOrderStatus.KindDeal))
            {
                return I18N.Phrase.KindDealLabel;
            }
            Guid bid = deal.BusinessHourGuid;
            string travelPlace = GetTravelPlace(bid);
            if (string.IsNullOrEmpty(travelPlace) == false)
            {
                return travelPlace;
            }
            if (categoryCode != null)
            {
                var category = PponFixCategories.FirstOrDefault(
                    t => t.Code == categoryCode && t.Status == (int)CategoryStatus.Enabled);
                if (category != null)
                {
                    return category.Name;
                }
            }
            if (categoryId != null)
            {
                if (categoryId == PponCityGroup.DefaultPponCityGroup.Skm.CategoryId)
                {
                    return ViewPponStoreManager.DefaultManager.GetPponStoreCityTownshipName(bid);
                }
            }
            IViewPponDeal vpd = DefaultManager.ViewPponDealGetByBid(bid, noClone: true);
            if (Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal))
            {
                //return Phrase.PponCityFamily;
                return "多分店";//"全家";
            }
            if (vpd.DeliveryType == (int)DeliveryType.ToHouse)
            {
                //return Phrase.PponCityAllCountry; 如果 PponCityAllCountry 的內容改成"宅配"，就可以將這行取代下一行
                return string.Empty;//"宅配";
            }
            if (vpd.ItemPrice == 0)
            {
                return string.Empty;//"優惠";
            }
            if (vpd.IsTravelDeal.GetValueOrDefault())
            {
                return ViewPponStoreManager.DefaultManager.GetPponStoreCityTownshipName(bid) ?? string.Empty;
            }
            else
            {
                return ViewPponStoreManager.DefaultManager.GetPponStoreTownshipName(bid) ?? string.Empty; ;
            }
        }

        /// <summary>
        /// 使用地點有設定即顯示使用地點 無則PponChannelArea category.name 
        /// </summary>
        /// <returns></returns>
        public string GetAreaNameOrTravelPlace(PponCity pponcity, Guid bid, int? categoryCode = null)
        {
            string travelPlace = GetTravelPlace(bid);
            int cid = default(int);
            if (categoryCode != null && int.TryParse(categoryCode.Value.ToString(), out cid))
            {
                var category = CategoryManager.CategoryGetListByType(CategoryType.PponChannelArea)
                    .FirstOrDefault(x => x.Status == 1 && x.Code.HasValue && x.Id == cid);
                if (category != null)
                {
                    return string.IsNullOrEmpty(travelPlace) ? category.Name : travelPlace;
                }
            }
            return string.IsNullOrEmpty(travelPlace) ? GetCityName(pponcity) : travelPlace;
        }

        /// <summary>
        /// 一般區域回傳[城市名稱]
        /// 但如果是旅遊渡假回傳[旅遊渡假]-旅遊地區
        /// </summary>
        /// <param name="pponcity"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        public string GetCityNameWithTravelPlaceIfDealIsTravel(PponCity pponcity, Guid bid)
        {
            string cityName = DefaultManager.GetCityName(pponcity);

            if (pponcity == PponCityGroup.DefaultPponCityGroup.Travel)
            {
                string travelPlace = DefaultManager.GetCityNameOrTravelPlace(pponcity, bid);
                return string.Format("[{0}]{1}", cityName, string.IsNullOrEmpty(travelPlace) ? "" : travelPlace + "-");
            }
            else
            {
                return string.Format("[{0}]", cityName);
            }
        }

        public string GetCityName(PponCity pponcity)
        {
            if (pponcity == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity)
            {
                return PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityName;
            }
            return pponcity.CityName;
        }

        private string GetTravelPlace(Guid bid)
        {
            return ViewPponDealGetByBid(bid, true).TravelPlace;
        }

        #region CategoryDeals相關

        public List<int> GetCategoryIds(Guid bid)
        {
            var deal = ViewPponDealGetByBid(bid, true);
            if (deal == null || deal.IsLoaded == false)
            {
                return new List<int>();
            }
            return deal.CategoryIds;
        }

        public List<string> GetCategoryNames(Guid bid)
        {
            return GetCategories(bid).Select(t => t.Name).ToList();
        }
        /// <summary>
        /// 取得檔次的分類，目前抓取的type為 4,5,6 即 團購頻道,頻道區域分區,檔次分類(新版分類2)
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public List<Category> GetCategories(Guid bid)
        {
            List<Category> result = new List<Category>();
            var cids = GetCategoryIds(bid);
            if (cids != null && cids.Count > 0)
            {
                foreach (int cid in cids)
                {
                    Category c;
                    if (_kernel._categories.TryGetValue(cid, out c))
                    {
                        result.Add(c);
                    }
                }
            }
            return result;
        }

        #endregion

        public EveryDayNewDeal GetEveryDayNewDealByBid(Guid Bid)
        {
            EveryDayNewDeal flag = EveryDayNewDeal.None;
            if (_kernel._everydaynewdeals.TryGetValue(Bid, out flag))
            {
                return flag;
            }
            else
            {
                return EveryDayNewDeal.None;
            }
        }

        public PponContentLite GetPponContentLite(IViewPponDeal deal)
        {
            string cacheKey = string.Format("{0}.{1}", deal.BusinessHourGuid, deal.EventModifyTime.ToJavaScriptMilliseconds());
            PponContentLite contentLite;

            if (StaticContentManager.TryGetContentLite(cacheKey, out contentLite) == false)
            {
                CouponEventContent content = pp.CouponEventContentGetByBid(deal.BusinessHourGuid);
                contentLite = new PponContentLite
                {
                    Description = content.Description,
                    AppDescription = content.AppDescription,
                    Introduction = content.Introduction,
                    Restrictions = content.Restrictions,
                    Remark = content.Remark,
                    Reasons = content.Reasons,
                    ReferenceText = content.ReferenceText,
                    Availability = content.Availability,
                    ProductSpec = content.ProductSpec
                };
                //替換成CDN的圖片網址
                if (config.EnableImagesCDN)
                {
                    var imagPools = pp.ImgPoolGetAllBybid(deal.BusinessHourGuid);

                    if (string.IsNullOrEmpty(contentLite.Description) == false && imagPools.Count > 0)
                    {
                        foreach (var imgpool in imagPools)
                        {
                            contentLite.Description = contentLite.Description.Replace(imgpool.Host + imgpool.Path,
                                string.Format("{0}{1}?{2}", config.ImagePoolUrl, imgpool.Path, deal.EventModifyTime.ToJavaScriptseconds()));
                        }
                    }
                }
                if (config.SwitchableHttpsEnabled)
                {
                    if (string.IsNullOrEmpty(contentLite.Description) == false)
                    {
                        contentLite.Description = contentLite.Description.Replace("http://www.17life.com", "https://www.17life.com");
                    }
                }
                StaticContentManager.SetContentLite(cacheKey, contentLite);
            }
            return contentLite;
        }

        public string GetDealDescription(IViewPponDeal deal)
        {
            return GetPponContentLite(deal).Description;
        }

        public string GetDealAppDescription(IViewPponDeal deal)
        {
            return GetPponContentLite(deal).AppDescription;
        }

        public string GetDealIntroduction(IViewPponDeal deal)
        {
            return GetPponContentLite(deal).Introduction;
        }

        public string GetDealRestrictions(IViewPponDeal deal)
        {
            return GetPponContentLite(deal).Restrictions;
        }

        #endregion public method

        #region statistic

        public Dictionary<string, List<ViewPponDealManagerSummary>> GetViewPponDealManagerSummary()
        {
            Dictionary<string, List<ViewPponDealManagerSummary>> summary = new Dictionary<string, List<ViewPponDealManagerSummary>>();
            List<ViewPponDealManagerSummary> detail = new List<ViewPponDealManagerSummary>();
            foreach (var item in _kernel._collection)
            {
                detail.Add(new ViewPponDealManagerSummary(item.BusinessHourGuid));
            }
            summary.Add("Collection", detail.ToList());
            detail.Clear();
            foreach (var item in _kernel._cityViewPponDealPponDatas)
            {
                foreach (var innitem in item.DataCollection)
                {
                    detail.Add(new ViewPponDealManagerSummary(innitem.BusinessHourGuid, item.CityId));
                }
            }
            summary.Add("CityViewPponDealPponDatas", detail.ToList());
            detail.Clear();
            foreach (var item in _kernel._viewComboDealCollection)
            {
                foreach (var innitem in item.Value)
                {
                    detail.Add(new ViewPponDealManagerSummary(innitem.BusinessHourGuid));
                }
            }
            summary.Add("ViewComboDealCollection", detail.ToList());
            detail.Clear();
            return summary;
        }

        #region 前台折扣、優惠價格等相關顯示文字

        /// <summary>
        /// 回傳前台頁面，檔次的折扣數字顯示文字
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static string GetDealDiscountString(IViewPponDeal deal)
        {
            return GetDealDiscountString(
                Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                deal.ItemPrice, deal.ItemOrigPrice, false, deal.ExchangePrice);
        }

        public static string GetDealDiscountStringLite(decimal itemPrice, decimal itemOrigPrice, decimal? exchangePrice = null)
        {
            string result = string.Empty;
            if (itemOrigPrice <= 0m)
            {
                return result;
            }
            if (exchangePrice.GetValueOrDefault() > 0)
            {
                result = (exchangePrice.Value * 10 / itemOrigPrice).FractionFloor(1).ToString("N1");
            }
            if (itemOrigPrice > 0)
            {
                result = (itemPrice * 10 / itemOrigPrice).FractionFloor(1).ToString("N1");
            }
            return result;
        }
        /// <summary>
        /// 用來計算檔次的折扣數字
        /// </summary>
        public static string GetDealDiscountString(bool noDiscountShown, decimal itemPrice, decimal itemOrigPrice, bool onlyNumber = true, decimal? exchangePrice = null)
        {
            string rtn = string.Empty;
            string rateStr = string.Empty;
            if (onlyNumber)
            {
                if (noDiscountShown)
                {
                    return string.Empty;
                }
                else if (itemOrigPrice > 0)
                {
                    return (itemPrice * 10 / itemOrigPrice).FractionFloor(1).ToString("N1");
                }
                else
                {
                    return "--";
                }
            }

            //有原價
            bool haveOrigPrice = (itemOrigPrice > 0);
            //有兌換價
            bool haveExchangePrice = (exchangePrice.HasValue && exchangePrice > 0);

            //沒有原價
            if (!haveOrigPrice)
            {
                rtn = "優惠";
            }
            else if (!haveExchangePrice)
            {
                //沒有兌換價
                if (itemPrice == itemOrigPrice || noDiscountShown)
                {
                    //原價與售價相同
                    rtn = "特選";
                }
                else if (itemPrice > 0)
                {
                    //售價大於0顯示折數
                    rateStr = (itemPrice * 10 / itemOrigPrice).FractionFloor(1).ToString("N1");
                    rtn = rateStr + " 折";
                }
                else
                {
                    //售價為0顯示優惠
                    rtn = "優惠";
                }
            }
            else
            {
                if (config.ShowFamiDiscount)
                {
                    //也原價也有兌換價
                    //售價大於0顯示折數
                    if (exchangePrice == itemOrigPrice)
                    {
                        //原價與兌換價相同
                        rtn = "特選";
                    }
                    else if (exchangePrice > 0)
                    {
                        //兌換價大於0顯示折數
                        rateStr = (exchangePrice.Value * 10 / itemOrigPrice).FractionFloor(1).ToString("N1");
                        rtn = rateStr + " 折";
                    }
                    else
                    {
                        //兌換價為0顯示優惠
                        rtn = "優惠";
                    }
                }
                else
                {
                    rtn = "優惠";
                }
            }
            return rtn;
        }

        /// <summary>
        /// 傳入IViewPponDeal檔次產生優惠方式與金額
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="isOnlyNumber"></param>
        /// <returns></returns>
        public static DealDiscountStringModel GetDealDiscountStringByIViewPponDeal(IViewPponDeal deal, bool isOnlyNumber = false)
        {
            var priceModel = new DealPriceModel
            {
                OnlyNumber = isOnlyNumber,
                BusinessHourStatus = deal.BusinessHourStatus,
                GroupOrderStatus = deal.GroupOrderStatus,
                DiscountType = deal.DiscountType,
                ItemOrigPrice = deal.ItemOrigPrice,
                ItemPrice = deal.ItemPrice,
                ExchangePrice = deal.ExchangePrice,
                DiscountValue = deal.DiscountValue
            };

            return GetDealDiscountStringandDiscountType(priceModel);
        }

        /// <summary>
        /// 傳入收藏檔次產生優惠方式與金額
        /// </summary>
        /// <param name="collectDeal"></param>
        /// <returns></returns>
        public static DealDiscountStringModel GetDealDiscountStringByCollectDealContent(ViewMemberCollectDealContent collectDeal)
        {
            var priceModel = new DealPriceModel
            {
                OnlyNumber = false,
                BusinessHourStatus = collectDeal.BusinessHourStatus,
                GroupOrderStatus = collectDeal.GroupOrderStatus,
                DiscountType = collectDeal.DiscountType,
                ItemOrigPrice = collectDeal.OrigPrice,
                ItemPrice = collectDeal.Price,
                ExchangePrice = collectDeal.ExchangePrice,
                DiscountValue = collectDeal.Discount
            };
            return GetDealDiscountStringandDiscountType(priceModel);
        }

        /// <summary>
        /// 傳入DealPriceMode 產生優惠方式與金額
        /// </summary>
        /// <param name="dealPrice"></param>
        /// <returns></returns>
        private static DealDiscountStringModel GetDealDiscountStringandDiscountType(DealPriceModel dealPrice)
        {
            var dealDiscount = new DealDiscountStringModel { DisplayPrice = dealPrice.ItemPrice };

            bool noDiscountShown = Helper.IsFlagSet(dealPrice.BusinessHourStatus, BusinessHourStatus.NoDiscountShown);
            decimal itemOrigPrice = dealPrice.ItemOrigPrice;
            decimal itemPrice = dealPrice.ItemPrice;
            decimal? exchangePrice = dealPrice.ExchangePrice;

            if (dealPrice.OnlyNumber)
            {
                if (noDiscountShown)
                {
                    dealDiscount.DiscountString = string.Empty;
                }
                else if (itemOrigPrice > 0)
                {
                    dealDiscount.DiscountString = (itemPrice * 10 / itemOrigPrice).FractionFloor(1).ToString("N1");
                }
                else
                {
                    dealDiscount.DiscountString = "--";
                }
                return dealDiscount;
            }

            if (dealPrice.DiscountType == (int)DiscountType.None)
            {
                dealDiscount.DisplayPrice = dealPrice.ItemPrice;

                //是否有原價
                bool haveOrigPrice = (itemOrigPrice > 0);
                //是否有兌換價
                bool haveExchangePrice = (exchangePrice.HasValue && exchangePrice > 0);

                #region 沒有原價

                if (!haveOrigPrice)
                {
                    dealDiscount.DiscountString = "優惠";
                    dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DiscountStringOnly;
                    return dealDiscount;
                }

                #endregion

                #region 沒有兌換價

                if (!haveExchangePrice)
                {
                    if (itemPrice == itemOrigPrice || noDiscountShown)
                    {
                        //原價與售價相同
                        dealDiscount.DiscountString = "特選";
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DPO;
                    }
                    else if (itemPrice > 0)
                    {
                        //售價大於0顯示折數 (全家寄杯也在這裡)
                        dealDiscount.DiscountString = (itemPrice * 10 / itemOrigPrice).FractionFloor(1).ToString("N1") + " 折";
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DPO;
                    }
                    else
                    {
                        //售價為0顯示優惠
                        dealDiscount.DiscountString = "優惠";
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DPO;
                    }

                    if (dealDiscount.DisplayPrice <= 0) //如果價值比0小
                    {
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DiscountStringOnly;
                    }

                    return dealDiscount;
                }

                #endregion

                #region 有原價也有兌換價

                //售價大於0顯示折數
                if (config.ShowFamiDiscount)
                {
                    if (exchangePrice == itemOrigPrice)
                    {
                        //原價與兌換價相同
                        dealDiscount.DiscountString = "特選";
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DPO;

                        if ((dealPrice.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)   //全家
                        {
                            dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DEPO;
                            dealDiscount.DisplayPrice = dealPrice.ExchangePrice ?? 0;
                        }
                    }
                    else if (exchangePrice > 0)
                    {
                        //兌換價大於0顯示折數
                        dealDiscount.DiscountString = (exchangePrice.Value * 10 / itemOrigPrice).FractionFloor(1).ToString("N1") + " 折";
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DPO;

                        if ((dealPrice.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)   //全家
                        {
                            dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DEPO;
                            dealDiscount.DisplayPrice = dealPrice.ExchangePrice ?? 0;
                        }
                    }
                    else
                    {
                        //兌換價為0顯示優惠
                        dealDiscount.DiscountString = "優惠";
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DPO;

                        if ((dealPrice.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)   //全家
                        {
                            dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DEPO;
                            dealDiscount.DisplayPrice = dealPrice.ExchangePrice ?? 0;
                        }
                    }
                }
                else
                {
                    dealDiscount.DiscountString = "優惠";
                    dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DPO;

                    if ((dealPrice.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)   //全家
                    {
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DEPO;
                        dealDiscount.DisplayPrice = dealPrice.ExchangePrice ?? 0;
                    }
                }

                #endregion
            }
            else
            {
                //阿吉時期的程式碼註記此段為新光，不知為何是新光，只是把原邏輯搬過來
                switch (dealPrice.DiscountType)
                {
                    case (int)PponDiscountType.ExchangePrice:
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DPO;
                        dealDiscount.DisplayPrice = dealPrice.DiscountValue.GetValueOrDefault();
                        dealDiscount.DiscountString = "兌換價";
                        break;
                    case (int)PponDiscountType.DiscountCash:
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DP;
                        dealDiscount.DisplayPrice = dealPrice.DiscountValue.GetValueOrDefault();
                        dealDiscount.DiscountString = "折抵";
                        break;
                    case (int)PponDiscountType.DiscountPercent:
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DiscountStringOnly;
                        dealDiscount.DiscountString = string.Format("{0} 折", dealPrice.DiscountValue.GetValueOrDefault());
                        break;
                    case (int)PponDiscountType.FavoExchangePrice:
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.DEPO;
                        dealDiscount.DisplayPrice = dealPrice.DiscountValue.GetValueOrDefault();
                        dealDiscount.DiscountString = string.Empty;
                        break;
                    default:
                        dealDiscount.DiscountDisplayType = (int)DiscountDisplayType.None;
                        dealDiscount.DisplayPrice = dealPrice.DiscountValue.GetValueOrDefault();
                        dealDiscount.DiscountString = string.Empty;
                        break;
                }
            }
            return dealDiscount;
        }
        #endregion

        #endregion statistic

        public IList<Category> GetAllCategories()
        {
            return _kernel._categories.Values.ToList();
        }

        public Category GetCategory(int id)
        {
            Category result;
            _kernel._categories.TryGetValue(id, out result);
            return result;
        }
    }

    public class StaticContentManager
    {
        private static ConcurrentDictionary<string, PponContentLite> _data = new ConcurrentDictionary<string, PponContentLite>();

        public static string GetContentDescription(string key)
        {
            return key;
        }

        public static void SetContentLite(string cacheKey, PponContentLite contentLite)
        {
            _data.TryAdd(cacheKey, contentLite);
        }

        public static bool TryGetContentLite(string cacheKey, out PponContentLite content)
        {
            return _data.TryGetValue(cacheKey, out content);
        }
    }

    public class PponContentLite
    {
        public PponContentLite()
        {
            this.Description = string.Empty;
            this.AppDescription = string.Empty;
            this.Introduction = string.Empty;
            this.Restrictions = string.Empty;
            this.Reasons = string.Empty;
            this.Remark = string.Empty;
            this.Availability = string.Empty;
            this.ReferenceText = string.Empty;
            this.ProductSpec = string.Empty;
        }

        public string Description { get; set; }
        public string AppDescription { get; set; }
        public string Introduction { get; set; }
        public string Restrictions { get; set; }
        public string Reasons { get; set; }
        public string Remark { get; set; }
        public string Availability { get; set; }
        public string ReferenceText { get; set; }
        public string ProductSpec { get; set; }
    }

    /// <summary>
    /// 用來記錄某城市的檔次資料
    /// 20140218 增加Category相關資料，記錄城市對應之Category，無對應相關值為NULL
    /// </summary>
    [Serializable]
    public class CityViewPponDealData
    {
        public int CityId { get; set; }
        /// <summary>
        /// 頻道的CategoryId
        /// </summary>
        public int? ChannelId { get; set; }
        /// <summary>
        /// 區域的CategoryId
        /// </summary>
        public int? ChannelAreaId { get; set; }
        public List<IViewPponDeal> DataCollection { get; set; }

        public CityViewPponDealData(int cityId, List<IViewPponDeal> data, int? channelId = null, int? channelAreaId = null)
        {
            CityId = cityId;
            DataCollection = data ?? new List<IViewPponDeal>();
            ChannelId = channelId;
            ChannelAreaId = channelAreaId;
        }

        public bool IsMatchChannel(int channelId, int? channelAreaId)
        {
            //未設定頻道，直接回覆不相符
            if (!ChannelId.HasValue)
            {
                return false;
            }
            if (ChannelId.Value == channelId)
            {

                if ((ChannelAreaId.HasValue) && (channelAreaId.HasValue) && (ChannelAreaId.Value == channelAreaId.Value))
                {
                    //有設定區域ID且有傳入要比對的區域ID，且傳入值比對後相同
                    return true;
                }
                else if (!ChannelAreaId.HasValue && !channelAreaId.HasValue)
                {
                    //未設定區域且未傳入需比對的區域參數
                    return true;
                }
                else
                {
                    //會到這邊表示 未設定參數卻需比對區域 或 有設定卻未傳入需比對的區域 或有設定也有傳入參數卻不相等
                    return false;
                }
            }
            else
            {
                //頻道編號不符
                return false;
            }
        }
    }

    public class CategoryDealCountSummary
    {
        public int CityId { get; set; }

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int TotalCount { get; set; }

        public int Rank { get; set; }

        public DateTime BusinessHourOrderStart { get; set; }

        public CategoryDealCountSummary(int cityid, int categoryid, string categoryname, int totalcount, int rank)
        {
            CityId = cityid;
            CategoryId = categoryid;
            CategoryName = categoryname;
            TotalCount = totalcount;
            Rank = rank;
        }
    }

    public class MultipleMainDealPreview
    {
        public Guid DealId { get; set; }

        public int CityID { get; set; }

        public int Sequence { get; set; }

        public IViewPponDeal PponDeal { get; set; }

        public List<int> DealCategoryIdList { get; set; }

        public DealTimeSlotStatus TimeSlotStatus { get; set; }

        public decimal BaseGrossProfit { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public PponDealSynopsis AppPponDeal { get; set; }

        public MultipleMainDealPreview(int cityId, int sequence, IViewPponDeal deal, List<int> dealCategoryIdList,
            DealTimeSlotStatus dealTimeSlotStatus, decimal baseGrossProfit)
        {
            DealId = deal.BusinessHourGuid;
            CityID = cityId;
            Sequence = sequence;
            PponDeal = deal;
            DealCategoryIdList = dealCategoryIdList;
            TimeSlotStatus = dealTimeSlotStatus;
            BaseGrossProfit = baseGrossProfit;
        }

        public override string ToString()
        {
            if (PponDeal == null)
            {
                return string.Empty;
            }
            return string.Format("{0} {1} {2}", Sequence, PponDeal.BusinessHourGuid, PponDeal.ItemName);
        }

        public override int GetHashCode()
        {
            if (this.DealId == Guid.Empty)
            {
                return 0;
            }
            return this.DealId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            MultipleMainDealPreview castObj = obj as MultipleMainDealPreview;
            if (castObj == null)
            {
                return false;
            }

            return GetHashCode() == castObj.GetHashCode();
        }
    }

    public class ViewPponDealManagerSummary
    {
        public Guid Bid { get; set; }

        public int CityId { get; set; }

        public int CategoryId { get; set; }

        public int TravelCategoryId { get; set; }

        public ViewPponDealManagerSummary(Guid bid, int cityid = 0, int categoryid = 0, int travelcategoryid = 0)
        {
            Bid = bid;
            CityId = cityid;
            CategoryId = categoryid;
            TravelCategoryId = travelcategoryid;
        }
    }

    public class ViewCategoryDealComparer : IEqualityComparer<ViewCategoryDeal>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(ViewCategoryDeal x, ViewCategoryDeal y)
        {

            //Check whether the compared objects reference the same data.
            if (object.ReferenceEquals(x, y))
            {
                return true;
            }

            //Check whether any of the compared objects is null.
            if (object.ReferenceEquals(x, null) || object.ReferenceEquals(y, null))
            {
                return false;
            }

            //Check whether the products' properties are equal.
            return x.Bid == y.Bid && x.Cid == y.Cid;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(ViewCategoryDeal viewCategoryDeal)
        {
            //Check whether the object is null
            if (object.ReferenceEquals(viewCategoryDeal, null))
            {
                return 0;
            }

            //Get hash code for the Name field if it is not null.
            int hashViewCategoryDealBid = viewCategoryDeal.Bid.GetHashCode();

            //Get hash code for the Code field.
            int hashViewCategoryDealCode = viewCategoryDeal.Bid.GetHashCode();

            //Calculate the hash code for the product.
            return hashViewCategoryDealBid ^ hashViewCategoryDealCode;
        }

    }

    public class CategoryDealPromoData
    {
        public int CategoryId { get; set; }

        public string DealPromoTitle { get; set; }

        public string DealPromoDescription { get; set; }

        public string DealPromoImage { get; set; }

        public CategoryDealPromoData(int categoryId, string dealPromoTitle, string dealPromoDescription, string dealPromoImage)
        {
            CategoryId = categoryId;
            DealPromoTitle = dealPromoTitle;
            DealPromoDescription = dealPromoDescription;
            DealPromoImage = dealPromoImage;
        }
    }


}
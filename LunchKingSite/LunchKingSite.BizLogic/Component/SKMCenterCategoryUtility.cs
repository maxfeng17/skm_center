﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Component
{
    public class SKMCenterCategoryUtility
    {
        private static ISellerProvider _sp;
        private static IPponProvider _pp;
        private static ISysConfProvider _config;
        private static string _modifyUser;

        public SKMCenterCategoryUtility(string modifyUser = "skm_center@17life.com")
        {
            _config = ProviderFactory.Instance().GetConfig();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _modifyUser = modifyUser;
        }

        private SKMCenterCategoryMain getSkmCenterData(long itemId)
        {
            SKMOpenAPIMain sKMOpenAPIMain = new SKMOpenAPIMain();

            string categoryResultString = sKMOpenAPIMain.GetChannelCategoryInfo(itemId);

            //需要有錯誤時的處理方式
            var getCategoryData = JsonConvert.DeserializeObject<SKMCenterCategoryMain>(categoryResultString);

            return getCategoryData;
        }

        private bool CreateSingleFirstLevelCategory(SKMCenterCategoryMain getCategoryData, ref int insertId , ref string errorMessage)
        {
            bool actionSuccess = false;

            if (getCategoryData.success && getCategoryData.result.level == 1)
            {
                //先查是否已有資料
                CategoryCollection dataCollection = _sp.CategoryGetListByAlibabaId(getCategoryData.result.id);

                Category data = null;

                if (dataCollection.Count > 0)
                {
                    data = dataCollection[0];
                }

                if (data == null)
                {
                    int maxCode = _sp.GetCategoryMaxCode();
                    int maxId = _sp.GetCategoryMaxId();
                    insertId = maxId + 1;

                    //無資料時新增
                    data = new Category
                    {
                        Name = getCategoryData.result.name,
                        CreateId = _modifyUser,
                        IsShowBackEnd = true,
                        IsShowFrontEnd = true,//預設上架
                        CreateTime = DateTime.Now,
                        IsFinal = false,
                        Status = (int)CategoryStatus.Enabled,
                        Type = (int)CategoryType.DealCategory,
                        //修改為所有category的max，而不是同type的max
                        Code = maxCode + 1,
                        Id = insertId,
                        AlibabaId = getCategoryData.result.id

                    };

                    //計算rank / isfinal=子分類
                    data.Rank = _sp.ViewCategoryDependencyGetByParentId(_config.SkmCategordId)
                                    .Where(p => p.IsFinal == false && p.CategoryType == (int)CategoryType.DealCategory
                                              && p.IsShowBackEnd == true).ToList().Count + 1;

                    var categorySetSuccess = _sp.CategorySet(data);
                    var depSetSuccess = _sp.CategoryDependencySet(new CategoryDependency()
                    {
                        ParentId = _config.SkmCategordId,
                        CategoryId = data.Id,
                    });

                }
                else
                {
                    //已有資料時的處理方式 等待
                    var category = _sp.CategoryGet(data.Id);
                    category.Name = getCategoryData.result.name;
                    category.IsShowFrontEnd = true;
                    category.ModifyId = _modifyUser;
                    category.ModifyTime = DateTime.Now;

                    _sp.CategorySet(category);
                }

                actionSuccess = true;
            }
            else
            {
                if (!getCategoryData.success)
                {
                    errorMessage = "資料不正確";
                }
                else
                {
                    errorMessage = string.Format("資料層級必需為第1級，目前為：{0}", getCategoryData.result.level);
                }
            }

            return actionSuccess;
        }

        private bool CreateSingleSecondLevelCategory(SKMCenterCategoryMain getCategoryData, int parentCatetoryId, ref int insertId, ref string errorMessage)
        {
            bool output = false;

            //先查是否已有資料
            CategoryCollection dataCollection = _sp.CategoryGetListByAlibabaId(getCategoryData.result.id);

            Category subCategory = null;

            if (dataCollection.Count > 0)
            {
                subCategory = dataCollection[0];
            }

            if (subCategory == null)
            {
                //新增資料
                subCategory = new Category();
                //有正確的父類別資料時，可以開始寫子類別資料
                subCategory.Name = getCategoryData.result.name;
                subCategory.CreateId = "skm_center@17life.com";
                subCategory.IsShowBackEnd = true;
                subCategory.IsShowFrontEnd = true;//預設上架
                subCategory.CreateTime = DateTime.Now;
                subCategory.IsFinal = true;
                subCategory.Status = (int)CategoryStatus.Enabled;
                subCategory.Type = (int)CategoryType.DealCategory;
                //修改為所有category的max，而不是同type的max
                subCategory.Code = _sp.GetCategoryMaxCode() + 1;

                //category 的 id並非  identity insert, 所以得自己取max(id)
                var maxId = _sp.GetCategoryMaxId();
                insertId = maxId + 1;
                subCategory.Id = insertId;

                subCategory.Rank = _sp.CategoryDependencyByRegionGet(parentCatetoryId).Count + 1;
                subCategory.AlibabaId = getCategoryData.result.id;

                // 1. save 本身 category
                var mainSetSuccess = _sp.CategorySet(subCategory);
                // 1. 依附_config.SkmCategordId
                var depSkmSetSuccess = _sp.CategoryDependencySet(new CategoryDependency()
                {
                    ParentId = _config.SkmCategordId,
                    CategoryId = subCategory.Id,
                });
                // 2. 依附 parent_id
                var depSetSuccess = _sp.CategoryDependencySet(new CategoryDependency()
                {
                    ParentId = parentCatetoryId,
                    CategoryId = subCategory.Id,
                });
            }
            else
            {
                //修改資料
                subCategory.Name = getCategoryData.result.name;
                subCategory.IsShowFrontEnd = true;
                subCategory.ModifyId = "skm_center@17life.com";
                subCategory.ModifyTime = DateTime.Now;

                _sp.CategorySet(subCategory);
            }

            return output;
        }

        public string CreateOrModifyCategory(long itemId)
        {
            string returnMessage = string.Empty;

            SKMCenterCategoryMain getCategoryData = getSkmCenterData(itemId);
            
            string errorMessage = string.Empty;

            bool insertSuccess = false;

            if (getCategoryData.success)
            {
                //查看層級，只處理1、2層
                if (getCategoryData.result.level == 1)
                {
                    int insertId = 0;
                    insertSuccess = CreateSingleFirstLevelCategory(getCategoryData, ref insertId,ref errorMessage);

                }
                else if (getCategoryData.result.level == 2)
                {
                    //先檢查父類別是否有資料
                    long parentId = getCategoryData.result.pid;

                    CategoryCollection parentDataCollection = _sp.CategoryGetListByAlibabaId(parentId);

                    int parentCatetoryId = 0;

                    bool insertResult = true;

                    if (parentDataCollection.Count == 0)
                    {
                        //寫入父類別資料 並取得id
                        SKMCenterCategoryMain parentLevelData = getSkmCenterData(parentId);

                        insertResult = CreateSingleFirstLevelCategory(parentLevelData, ref parentCatetoryId, ref errorMessage);
                    }
                    else
                    {
                        //取得父類別id
                        parentCatetoryId = parentDataCollection[0].Id;
                    }

                    if (insertResult && parentCatetoryId != 0)
                    {
                        int insertId = 0;

                        CreateSingleSecondLevelCategory(getCategoryData, parentCatetoryId, ref insertId, ref errorMessage);
                    }

                }
            }
            else 
            {
                returnMessage = string.Format("取得分類資料錯誤，錯誤代碼為：{0}", getCategoryData.code);
            }


            return returnMessage;
        }

        public string DeleteCategory(long itemId) 
        {
            string returnMessage = string.Empty;

            SKMCenterCategoryMain getCategoryData = getSkmCenterData(itemId);

            if (getCategoryData.success)
            {
                //取得在我方類別的資料,先查第一分類
                CategoryCollection dataCollection = _sp.CategoryGetListByAlibabaId(itemId);

                if (dataCollection.Count > 0)
                {
                    Category category = dataCollection[0];
                    if (category.IsShowFrontEnd)
                    {
                        category.IsShowFrontEnd = false;
                    }
                    category.ModifyId = _modifyUser;
                    category.ModifyTime = DateTime.Now;

                    if (_sp.CategorySet(category) > 0)
                    {
                        //修改子分類讓其下架
                        var dependencyCol = _sp.CategoryDependencyByRegionGet(category.Id);
                        if (dependencyCol.Any())
                        {
                            List<string> filters = new List<string>();
                            filters.Add(string.Format("{0} = {1}", Category.Columns.Status, (int)CategoryStatus.Enabled));
                            filters.Add(string.Format("{0} = {1}", Category.Columns.Type, (int)CategoryType.DealCategory));
                            //手動將舊有分類隱藏_is_show_back_end=false
                            filters.Add(string.Format("{0} = {1}", Category.Columns.IsShowBackEnd, true));
                            filters.Add(string.Format("{0} in ({1})", Category.Columns.Id, string.Join(",", dependencyCol.Select(p => p.CategoryId).ToList())));
                            var subCategoryCol = _sp.CategoryGetList(0, 0, Category.Columns.Rank.ToString(), filters.ToArray());
                            foreach (var sc in subCategoryCol)
                            {
                                sc.IsShowFrontEnd = false;
                                sc.ModifyId = _modifyUser;
                                sc.ModifyTime = DateTime.Now;
                            }
                            var successCount = _sp.CategorySetList(subCategoryCol);
                        }
                    }
                }
                else
                {
                    //取得在我方類別的資料
                    CategoryCollection dataCollectionSub = _sp.CategoryGetListByAlibabaId(itemId);

                    if (dataCollectionSub.Count > 0)
                    {
                        var category = dataCollectionSub[0];
                        if (category.IsShowFrontEnd)
                        {
                            category.IsShowFrontEnd = false;
                        }
                        category.ModifyId = _modifyUser;
                        category.ModifyTime = DateTime.Now;
                        _sp.CategorySet(category);
                    }
                    else
                    {
                        returnMessage = "查無分類資料";
                    }
                }
            }

            return returnMessage;
        }

        /// <summary>
        /// 綁商品( 阿里 Alibaba
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="category_id"></param>
        /// <param name="isAdd"></param>
        /// <returns></returns>
        public bool setExternalDealCategory (long itemId , string category_id , bool isAdd) {
            SKMOpenAPIMain sKMOpenAPIMain = new SKMOpenAPIMain();
            string returnTransData = sKMOpenAPIMain.GetItemInfo(itemId);
            var tempstr = returnTransData.Substring(returnTransData.IndexOf("skuIdList"));
            string [] skucode = tempstr.Substring(0 , tempstr.IndexOf("]") + 1).Split(":");
            bool saveSuccess = false;
            Regex rgx = new Regex(@"(?i)(?<=\[)(.*)(?=\])");//中括号[]
            var dealIds = rgx.Match(skucode [1]).Value.Split(",");//中括号[]

            using (var transactionScope = new System.Transactions.TransactionScope()) {
                foreach (var dealId in dealIds) {
                    var deal = _pp.ExternalDealGet(dealId);
                    if (Guid.Empty != deal.Guid) {
                        var category = _sp.CategoryGetAlibaba(category_id);
                        var parentCategories = _sp.GatCategoryParent(category.Id).Where(p => p.ParentId != 185).ToList();
                        List<int> categoryList = rgx.Match(deal.CategoryList).Value.Split(",").ToList().ConvertAll(int.Parse);
                        if (isAdd) {
                            if (parentCategories.Count == 1) {
                                categoryList.Add(parentCategories [0].ParentId);
                                categoryList.Add(parentCategories [0].CategoryId);
                            } else {
                                categoryList.Add(category.Id);
                            }
                        } else {
                            if (parentCategories.Count == 0) {
                                categoryList.Remove(category.Id);
                            } else {
                                categoryList.Remove(parentCategories [0].ParentId);
                                categoryList.Remove(parentCategories [0].CategoryId);
                            }
                        }
                        deal.CategoryList = JsonConvert.SerializeObject(categoryList);
                        try {
                            _pp.ExternalDealSet(deal);
                            SkmFacade.UpdateSkmDealCategory(deal);
                            saveSuccess = true;
                        } catch( Exception ex ) {
                            throw ex;
                        }
                    } else {
                        saveSuccess = false;
                    }
                }
                if (saveSuccess) {
                    transactionScope.Complete();
                }
            }
            return saveSuccess;
        }
    }

    public class SKMCenterCategoryMain
    { 
        public string code { get; set; }
        public bool success { get; set; }

        public SKMCenterCategoryResult result { get; set; }
    }

    public class SKMCenterCategoryResult
    { 
        public SKMCenterCategoryResult()
        {
            //初始化資料
            this.id = 0;
            this.shopId = 0;
            this.pid = 0;
            this.logo = string.Empty;
            this.level = 0;
            this.hasChildren = false;
            this.type = 0;
            this.name = string.Empty;
            this.index = 0;
            this.disclosed = false;
            this.status = 0;
            this.hasBind = false;
            this.extra = new Dictionary<string, string>();
        }

        /// <summary>
        /// 主鍵
        /// </summary>
        public long id { get; set; }

        /// <summary>
        /// 店鋪id
        /// </summary>
        public long shopId { get; set; }

        /// <summary>
        /// 租戶id
        /// </summary>
        public int tenantId { get; set; }

        /// <summary>
        /// 父類目id
        /// </summary>
        public long pid { get; set; }

        /// <summary>
        /// 類目logo
        /// </summary>
        public string logo { get; set; }

        /// <summary>
        /// 類目層級
        /// </summary>
        public int level { get; set; }

        /// <summary>
        /// 類目是否有子類目
        /// </summary>
        public bool hasChildren { get; set; }

        /// <summary>
        /// 類型
        /// </summary>
        public int type { get; set; }

        /// <summary>
        /// 類目名
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 類目在本級中的排序
        /// </summary>
        public int index { get; set; }

        /// <summary>
        /// 默認是否展開
        /// </summary>
        public bool disclosed { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public int status { get; set; }

        /// <summary>
        /// 綁定狀態
        /// </summary>
        public bool hasBind { get; set; }

        /// <summary>
        /// 其他信息
        /// </summary>
        public Dictionary<string, string> extra { get; set; }
    }

    public class SKMCenterUserAuthMain
    {
        public string code { get; set; }

        public bool success { get; set; }

        public SKMCenterUserAuthResult result { get; set; }
    }

    public class SKMCenterUserAuthMainError
    {
        public string code { get; set; }

        public bool success { get; set; }

        public string result { get; set; }
    }

    public class SKMCenterMc
    {
        public SKMCenterMc()
        {
            this.start = string.Empty;
            this.end = string.Empty;
        }

        public string start { get; set; }

        public string end { get; set; }
    }

    public class SKMCenterUserAuthResult
    {
        public SKMCenterUserAuthResult()
        {
            this.primary = false;
            this.materialTypeList = new string[0];
            this.mcList = new SKMCenterMc[0];
            this.industryList = new string[0];
            this.storeList = new string[0];
        }

        public bool primary { get; set; }

        public string[] materialTypeList { get; set; }

        public SKMCenterMc[] mcList { get; set; }

        public string[] industryList { get; set; }

        public string[] storeList { get; set; }
    }

}

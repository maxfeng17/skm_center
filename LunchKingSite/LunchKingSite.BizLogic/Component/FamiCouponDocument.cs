﻿using System;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Phrase = iTextSharp.text.Phrase;
using iTextSharp.text;
using iTextSharp.text.pdf;
using LunchKingSite.Core.Interface;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Drawing;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.BizLogic.Component
{
    public class FamiCouponDocument : CouponDocumentBase
    {
        protected static ISysConfProvider Config;

        protected static IFamiportProvider famip;

        /// <summary>
        /// 憑證序號
        /// </summary>
        public string CouponCode { set; get; }
        
        public CouponCodeType CodeType { set; get; }
        /// <summary>
        /// 短標
        /// </summary>
        public string CouponUsage { set; get; }
        /// <summary>
        /// 黑標
        /// </summary>
        public string EventName { set; get; }
        /// <summary>
        /// 優惠有效日期(起)
        /// </summary>
        public DateTime DeliverTimeStart { set; get; }

        /// <summary>
        /// 優惠有效日期(迄)
        /// </summary>
        public DateTime DeliverTimeEnd { set; get; }

        /// <summary>
        /// 啟用三段式條碼
        /// </summary>
        private bool EnableFami3Barcode { set; get; }

        private IViewPponDeal Vpd { set; get; }

        private Dictionary<string, System.Drawing.Image> BarcodeList { get; set;}

        public FamiCouponDocument(ViewPponCoupon coupon, CouponCodeType codeType, IViewPponDeal vpd)
        {
            Config = ProviderFactory.Instance().GetConfig();
            famip = ProviderFactory.Instance().GetProvider<IFamiportProvider>();

            //全家咖啡寄杯(成套票券)
            if ((Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon) && (vpd.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0))
            {
                FamilyNetEvent ed = famip.GetFamilyNetEvent(vpd.BusinessHourGuid);
                if(ed != null && ed.Version == (int)FamilyBarcodeVersion.FamiSingleBarcode)
                {
                    codeType = CouponCodeType.FamiSingleBarcode;
                }
                else
                {
                    codeType = CouponCodeType.FamiItemCode;
                }
            }

            EnableFami3Barcode = Config.EnableFami3Barcode;
            Vpd = vpd;
            CouponCode = coupon.CouponCode;
            CodeType = codeType;
            CouponUsage = coupon.CouponUsage;
            EventName = coupon.EventName;
            DeliverTimeStart = coupon.BusinessHourDeliverTimeS.Value;
            DeliverTimeEnd = coupon.BusinessHourDeliverTimeE.Value;
            BarcodeList = new Dictionary<string, System.Drawing.Image>();

            switch (CodeType)
            {
                case CouponCodeType.Pincode://3 Barcode
                    if (EnableFami3Barcode)
                    {
                        var famipos = OrderFacade.FamiposBarcodeGetCreate(coupon.OrderDetailGuid, vpd.BusinessHourGuid, vpd);
                        if (!famipos.IsLoaded)
                        {
                            return;
                        }

                        if (!string.IsNullOrEmpty(famipos.Barcode1))
                        {
                            BarcodeList.Add(famipos.Barcode1, new Core.Component.BarCode39(famipos.Barcode1, false, false).Paint());
                        }

                        if (!string.IsNullOrEmpty(famipos.Barcode2))
                        {
                            BarcodeList.Add(famipos.Barcode2, new Core.Component.BarCode39(famipos.Barcode2, false, false).Paint());
                        }

                        if (!string.IsNullOrEmpty(famipos.Barcode3))
                        {
                            BarcodeList.Add(famipos.Barcode3, new Core.Component.BarCode39(famipos.Barcode3, false, false).Paint());
                        }
                    }
                    break;
                case CouponCodeType.FamiItemCode://4 Barcode
                    var famiNetPincode = famip.GetFamilyNetPincodeInfo(coupon.OrderGuid, coupon.CouponId.Value);
                    if (famiNetPincode == null)
                    {
                        return;
                    }

                    if (!string.IsNullOrEmpty(famiNetPincode.Barcode1))
                    {
                        BarcodeList.Add(famiNetPincode.Barcode1, new Core.Component.BarCodeEAN13(famiNetPincode.Barcode1).Paint(2.5f, 350, 80, 21));
                    }

                    if (!string.IsNullOrEmpty(famiNetPincode.Barcode2))
                    {
                        BarcodeList.Add(famiNetPincode.Barcode2, new Core.Component.BarCode39(famiNetPincode.Barcode2, false, false).Paint());
                    }

                    if (!string.IsNullOrEmpty(famiNetPincode.Barcode3))
                    {
                        BarcodeList.Add(famiNetPincode.Barcode3, new Core.Component.BarCode39(famiNetPincode.Barcode3, false, false).Paint());
                    }

                    if (!string.IsNullOrEmpty(famiNetPincode.Barcode4))
                    {
                        BarcodeList.Add(famiNetPincode.Barcode4, new Core.Component.BarCode39(famiNetPincode.Barcode4, false, false).Paint());
                    }

                    break;
                case CouponCodeType.FamiSingleBarcode: //全家一段式條碼
                    var famiSinglePincode = famip.GetFamilyNetPincodeInfo(coupon.OrderGuid, coupon.CouponId.Value);
                    if (famiSinglePincode == null)
                    {
                        return;
                    }

                    if (!string.IsNullOrEmpty(famiSinglePincode.Barcode2))
                    {
                        BarcodeList.Add(famiSinglePincode.Barcode2, new Core.Component.BarCode39(famiSinglePincode.Barcode2, false, false).Paint());
                    }
                    break;
                default:
                    PrepareImages(CouponCode, (CouponCodeType)vpd.CouponCodeType);
                    break;
            }
        }
        
        protected override void RenderPDFContent()
        {

            switch (CodeType)
            {
                case CouponCodeType.FamiItemCode://4 Barcode
                    RenderLogoAndItemName();
                    RenderBarcode();
                    RenderCouponInfo();
                    break;
                case CouponCodeType.Pincode://3 Barcode
                    if (EnableFami3Barcode)
                    {
                        RenderLogoAndItemName();
                        RenderBarcode();
                        RenderCouponInfo();
                    }
                    break;
                case CouponCodeType.FamiSingleBarcode://全家一段式條碼
                    RenderLogoAndItemName();
                    RenderBarcode();
                    RenderCouponInfo();
                    break;
                default:
                    RenderLogoAndBarcode();
                    RenderItemName(CouponUsage);
                    RenderEventName(EventName);
                    RenderUsageDate(DeliverTimeStart, DeliverTimeEnd);
                    RenderCouponInfo();
                    break;
            }
        }

        #region Render PDF

        private void RenderLogoAndItemName()
        {
            PrepareLogoImage();

            //增加上下間的空白間距
            var p = new Paragraph();
            p.SpacingBefore = 2;
            p.SpacingAfter = 3;
            doc.Add(p);

            PdfPTable table = new PdfPTable(3);
            table.SetWidths(new int[] { 175, 220, 200 });
            table.DefaultCell.Border = 1;
            table.WidthPercentage = 95;

            table.AddCell(
                new PdfPCell(pdfImgLogo)
                {
                    FixedHeight = 60f,
                    PaddingTop = 5,
                    PaddingLeft = 3,
                    PaddingRight = 3,
                    Rowspan = 1,
                    Border = 0,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BorderWidthRight = _BREAKLINE_BORDER_WIDTH,
                    BorderColorRight = _BREAKLINE_BORDER_COLOR,
                    BorderWidthBottom = _BREAKLINE_BORDER_WIDTH,
                    BorderColorBottom = _BREAKLINE_BORDER_COLOR,
                });

            Paragraph itemName = new Paragraph();
            Fonts font = new Fonts();

            if (CodeType == CouponCodeType.FamiItemCode || CodeType == CouponCodeType.FamiSingleBarcode)
            {
                itemName.Add(new Phrase(Vpd.CouponUsage, Fonts.Mingliu10Bold));
            }
            else
            {
                itemName.Add(new Phrase(Vpd.CouponUsage, Fonts.Arial12Bold));
            }
            
            itemName.Add("\n\n");
            itemName.Add(new Chunk(string.Format(@"優惠有效期限：{0} ~ {1}"
                , DeliverTimeStart.ToString("yyyy/MM/dd")
                , DeliverTimeEnd.ToString("yyyy/MM/dd")), Fonts.Mingliu10));

            var itemNameCell = new PdfPCell(itemName);
            itemNameCell.PaddingLeft = 10;
            itemNameCell.Border = 0;
            itemNameCell.BorderWidthBottom = _BREAKLINE_BORDER_WIDTH;
            itemNameCell.BorderColorBottom = _BREAKLINE_BORDER_COLOR;
            itemNameCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            itemNameCell.FixedHeight = 60f;
            itemNameCell.Colspan = 2;

            table.AddCell(itemNameCell);
            doc.Add(table);
        }

        private void RenderBarcode()
        {
            PdfPTable barTable = new PdfPTable(1);
            barTable.SetWidths(new int[] { 400 });
            barTable.DefaultCell.Border = 0;
            if (CodeType == CouponCodeType.FamiItemCode)
            {
                barTable.WidthPercentage = 50;
            }
            else
            {
                barTable.WidthPercentage = 95;
            }

            foreach (var barcodeImg in BarcodeList)
            {
                barTable.AddCell(
                    new PdfPCell(iTextSharp.text.Image.GetInstance(barcodeImg.Value, ImageFormat.Png))
                    {
                        FixedHeight = 40f,
                        PaddingTop = (barcodeImg.Value.Height >= 70) ? (barcodeImg.Value.Height - 70) + 5 : 5,
                        PaddingLeft = 0,
                        PaddingRight = 0,
                        PaddingBottom = 1,
                        Border = 0,
                        BorderWidth = 0,
                        VerticalAlignment = Element.ALIGN_BOTTOM,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        BorderWidthRight = 0,
                        BorderWidthBottom = 0
                    });

                barTable.AddCell(
                    new PdfPCell(new Phrase(barcodeImg.Key, Fonts.Arial10))
                    {
                        PaddingTop = 0,
                        VerticalAlignment = Element.ALIGN_TOP,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        BorderWidthTop = 0,
                        Border = 0
                    });
            }
            doc.Add(barTable);
        }
        
        private void RenderCouponInfo()
        {
            //增加上下間的空白間距
            var space = new Paragraph();
            space.SpacingBefore = 4;
            space.SpacingAfter = 3;
            doc.Add(space);

         
            var p = new Paragraph();
            p.Leading = 15;
            p.IndentationLeft = 20;
            p.IndentationRight = 10;
            p.Alignment = Element.ALIGN_LEFT;

            string info = string.Empty;
            switch(CodeType)
            {
                case CouponCodeType.Pincode:
                    info = (CodeType == CouponCodeType.Pincode && !EnableFami3Barcode) ?
                    "※ 請於兌換期限內至全省全家便利商店門市使用FamiPort依照步驟輸入PIN碼並取得單據後，至店內拿取欲兌換之商品，持繳款單據至櫃檯完成結帳動作，即可享有優惠價購買商品(恕不提供網路付款)"
                    : "※ 請於兌換期限內至全省全家便利商店門市使用，於店內拿取欲兌換之商品，持條碼請店員刷條碼結帳，即可享有優惠價購買商品(恕不提供網路付款)";
                    p.Add(new Phrase(info, Fonts.Mingliu8));
                    if (EnableFami3Barcode)
                    {
                        p.Add("\n");
                        p.Add(new Phrase(string.Format(@"※ 條碼無法正常讀取，請使用FamiPort輸入紅利PIN碼「{0}」列印後，即可持單據至櫃檯完成結帳動作。", CouponCode), Fonts.Mingliu8));
                        p.Add("\n");
                        p.Add(new Phrase("※ 列印小白單步驟說明：使用 Famifport → 點選「紅利」 → 點選「17Life」 → 同意條款 → 輸入「紅利pin碼」 → 確認商品資訊 → 列印小白單 → 至櫃檯結帳。", Fonts.Mingliu8));
                        p.Add("\n");
                        p.Add(new Phrase("※ 刷取手機條碼結帳，店舖可不回收相關單據。", Fonts.Mingliu8));
                    }
                    break;
                case CouponCodeType.FamiItemCode:
                case CouponCodeType.FamiSingleBarcode:
                    p.Add(new Phrase("＊如無法掃描請來電至全家客服，並且告知此杯紅利PIN碼編號，一杯有一組紅利PIN碼編號，如需兌換多杯要告知每杯的紅利PIN碼編號。", Fonts.Mingliu8));
                    p.Add("\n");
                    p.Add(new Phrase("全家客服電話 0800-071-999", Fonts.Mingliu8));
                    p.Add("\n");
                    p.Add(new Phrase(string.Format("＊打完後即給利用全家FamiPort輸入紅利PIN碼「{0}」列印後，請持單據至櫃檯完成結帳動作。", CouponCode), Fonts.Mingliu8));
                    break;
                default:
                    break;
            }
            
            doc.Add(p);
        }

        private void RenderLogoAndBarcode()
        {
            //增加上下間的空白間距
            var p = new Paragraph();
            p.SpacingBefore = 2;
            p.SpacingAfter = 3;
            doc.Add(p);

            PdfPTable table = new PdfPTable(3);
            table.SetWidths(new int[] { 175, 220, 200 });
            table.DefaultCell.Border = 1;
            table.WidthPercentage = 95;

            table.AddCell(
                new PdfPCell(pdfImgLogo)
                {
                    FixedHeight = 60f,
                    PaddingTop = 5,
                    PaddingLeft = 3,
                    PaddingRight = 3,
                    Rowspan = 2,
                    Border = 0,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BorderWidthRight = _BREAKLINE_BORDER_WIDTH,
                    BorderColorRight = _BREAKLINE_BORDER_COLOR,
                    BorderWidthBottom = _BREAKLINE_BORDER_WIDTH,
                    BorderColorBottom = _BREAKLINE_BORDER_COLOR
                });

            Phrase phBarcode = new Phrase();

            if (this.CodeType == CouponCodeType.Pincode) 
            {
                phBarcode.Add(new Chunk("紅利PIN碼：" + this.CouponCode, Fonts.Mingliu10Bold)); 
            }
            else if (this.CodeType == CouponCodeType.BarcodeEAN13) 
            {
                phBarcode.Add(new Chunk("優惠條碼", Fonts.Mingliu10Bold));
            }

            var barcodeCell = new PdfPCell(phBarcode);
            barcodeCell.PaddingLeft = 10;
            barcodeCell.Border = 0;
            barcodeCell.BorderWidthBottom = _BREAKLINE_BORDER_WIDTH;
            barcodeCell.BorderColorBottom = _BREAKLINE_BORDER_COLOR;
            barcodeCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            barcodeCell.FixedHeight = 60f;

            if (CodeType == CouponCodeType.Pincode) 
            {
                barcodeCell.Colspan = 2;
            }

            table.AddCell(barcodeCell);

            if (CodeType == CouponCodeType.BarcodeEAN13) 
            {
                var ImgEAN13 = new PdfPCell(this.pdfImgBarcodeEAN13);
                ImgEAN13.PaddingRight = 2;
                ImgEAN13.PaddingTop = 3;
                ImgEAN13.PaddingBottom = 3;
                ImgEAN13.Rowspan = 2;
                ImgEAN13.Border = 0;
                ImgEAN13.BorderWidthBottom = _BREAKLINE_BORDER_WIDTH;
                ImgEAN13.BorderColorBottom = _BREAKLINE_BORDER_COLOR;
                ImgEAN13.HorizontalAlignment = Element.ALIGN_RIGHT;
                ImgEAN13.VerticalAlignment = Element.ALIGN_MIDDLE;
                ImgEAN13.FixedHeight = 60f;
                table.AddCell(ImgEAN13);
            }

            doc.Add(table);
        }
        
        private void RenderItemName(string itemName)
        {
            var p = new Paragraph();
            p.Leading = 20;
            p.IndentationLeft = 20;
            p.Alignment = Element.ALIGN_LEFT;
            p.Add(new Phrase(itemName, Fonts.Mingliu10));
            doc.Add(p);
        }

        private void RenderEventName(string eventName)
        {
            var p = new Paragraph();
            p.Leading = 15;
            p.IndentationLeft = 20;
            p.IndentationRight = 10;
            p.Alignment = Element.ALIGN_LEFT;
            p.Add(new Phrase(eventName, Fonts.Mingliu10Bold));
            doc.Add(p);
        }

        private void RenderUsageDate(DateTime usageStartDate, DateTime usageEndDate)
        {
            var p = new Paragraph();
            p.Leading = 20;
            p.IndentationLeft = 20;
            p.Alignment = Element.ALIGN_LEFT;
            p.Add(new Chunk("優惠有效期間：", Fonts.Mingliu10));
            p.Add(new Chunk(usageStartDate.ToShortDateString() + " ~ ", Fonts.Arial10));
            p.Add(new Chunk(usageEndDate.ToShortDateString(), Fonts.Arial10));
            doc.Add(p);
        }

        #endregion
    }
}

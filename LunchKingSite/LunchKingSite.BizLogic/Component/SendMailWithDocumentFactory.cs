﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using log4net;
using LunchKingSite.BizLogic.Jobs;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Interface;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// 將資料轉成檔案儲存的Model都寫在這邊(csv,html,txt...)
    /// </summary>
    public class SendMailWithDocumentFactory : IdocumentModel
    {

        private static ILog logger = LogManager.GetLogger(typeof(PaymentCharge));
        public string Header { get; set; }
        List<string> _rowList = new List<string>();
        public List<string> RowList { set { _rowList = value; } }

        /// <summary>
        /// 存成csv(Excel)檔
        /// 格式範例:data1,data2,data3....\n(換列)
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string csvModelGen(string path, string fileName)
        {
            //Create Dir
            string dir = path;

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            //Create File
            FileStream fs = null;
            StreamWriter sw = null;
            string file = fileName;

            try
            {
                if (File.Exists(dir + file))
                {
                    File.Delete(dir + file);
                }

                fs = new FileStream(dir + file, FileMode.Create);
                sw = new StreamWriter(fs, System.Text.Encoding.Default);

                var table = Header + "\n"; ;

                foreach (var dates in _rowList)
                {
                    table += dates + "\n";
                }

                sw.WriteLine(table);
            }
            catch (Exception ex)
            {
                logger.Error(ex + "IdocumentModel Factory - csv檔建立失敗");
            }
            finally
            {
                sw.Close();
                fs.Close();
            }
            return path + file;
        }

    }

    public static class sendMailExtension
    {
        /// <summary>
        /// 附上檔案路徑並自動發送信件
        /// </summary>
        /// <param name="toEmail"></param>        //收件人
        /// <param name="ccEmail"></param>        //副本
        /// <param name="subject"></param>        //主題
        /// <param name="body"></param>           //內容
        /// <param name="attachmentUrl"></param>  //附件
        public static void sendMail(this string attachmentUrl, List<MailAddress> toEmail, List<MailAddress> ccEmail,
            string subject, string body)
        {
            MailMessage msg = new MailMessage();
            foreach (var item in toEmail)
            {
                msg.To.Add(item);
            }
            foreach (var item in ccEmail)
            {
                msg.CC.Add(item);
            }
            msg.Subject = subject;
            msg.Body = body;

            //check file is exists
            if (File.Exists(attachmentUrl))
            {
                Attachment attachment = new Attachment(attachmentUrl);
                msg.Attachments.Add(attachment);
            }

            PostMan.Instance().Send(msg, SendPriorityType.Immediate);

            //release Attachment
            if (msg.Attachments == null || msg.Attachments.Count <= 0)
            {
                return;
            }

            foreach (Attachment t in msg.Attachments)
            {
                t.Dispose();
            }

        }
    }
}
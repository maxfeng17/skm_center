﻿using System;
using System.IO;
using System.Linq;
using Amazon.Runtime;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Component
{
    public class AWSUtility
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private static AmazonS3 client = new AmazonS3(config.AWSBucketName);

        public static bool Upload(string baseFolder, Stream data, string fileName)
        {
            return client.Upload(baseFolder, data, fileName);
        }

        public static bool Upload(string baseFolder, string filePath, string fileName)
        {
            return client.Upload(baseFolder, filePath, fileName);
        }

        public static void Delete(string baseFolder, string filePath)
        {
            client.Delete(baseFolder, filePath);
        }

        public static void Delete(string resourceKey)
        {
            client.DeleteObject(resourceKey);
        }

        public static void DeleteByPrefix(string baseFolder, string prefix)
        {
            client.DeleteByPrefix(baseFolder, prefix);
        }

        public static void Copy(string baseFolder, string sourceKey, string destKey)
        {
            client.Copy(baseFolder, sourceKey, destKey);
        }
    }

    public class AmazonS3
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static ILog Logger = LogManager.GetLogger(typeof(AmazonS3));

        private static RegionEndpoint endPoint = RegionEndpoint.APNortheast1;
        private string bucketName;
        private string accessKey = config.AWSAccessKey;
        private string secretKey = config.AWSSecretKey;

        static IAmazonS3 client;

        public AmazonS3(string bucketName)
        {
            this.bucketName = bucketName;
        }

        public bool Upload(string baseFolder, Stream data, string fileName)
        {
            return PutObject(data, CombinePath(baseFolder, fileName), TimeSpan.FromDays(30));
        }

        public bool Upload(string baseFolder, string filePath, string fileName)
        {
            return PutObject(filePath, CombinePath(baseFolder, fileName), TimeSpan.FromDays(30));
        }

        public void Delete(string baseFolder, string filePath)
        {
            DeleteObject(CombinePath(baseFolder, filePath));
        }

        public void DeleteByPrefix(string baseFolder, string prefix)
        {
            DeleteByPrefix(CombinePath(baseFolder, prefix));
        }

        public void Copy(string baseFolder, string sourceKey, string destKey)
        {
            CopyObject(bucketName, CombinePath(baseFolder, sourceKey), bucketName, CombinePath(baseFolder, destKey));
        }

        public bool PutObject(Stream data, string keyName, TimeSpan? cacheControl)
        {
            try
            {
                AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
                using (client = new AmazonS3Client(credentials, endPoint))
                {
                    PutObjectRequest request = new PutObjectRequest
                    {
                        BucketName = bucketName,
                        Key = keyName,
                        InputStream = data
                    };
                    if (cacheControl != null)
                    {
                        request.Headers.CacheControl = "max-age=" + (int)cacheControl.Value.TotalSeconds;
                    }
                    client.PutObject(request);
                    return true;
                }
            }
            catch (AmazonS3Exception ex)
            {
                Logger.ErrorFormat("AWS Stream PutObject error!! errorCode:{0}, msg:{1} ", ex.ErrorCode, ex.Message);
                return false;
            }
        }

        public bool PutObject(string filePath, string keyName, TimeSpan? cacheControl)
        {
            using (var fs = File.OpenRead(filePath))
            {
                return PutObject(fs, keyName, cacheControl);
            }            
        }

        public void DeleteObject(string keyName)
        {
            try
            {
                AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
                using (client = new AmazonS3Client(credentials, endPoint))
                {
                    DeleteObjectRequest request = new DeleteObjectRequest
                    {
                        BucketName = bucketName,
                        Key = keyName
                    };
                    client.DeleteObject(request);
                }
            }
            catch (AmazonS3Exception ex)
            {
                Logger.ErrorFormat("AWS DeleteObject error!! errorCode:{0}, msg:{1} ", ex.ErrorCode, ex.Message);
            }
        }

        public void DeleteByPrefix(string prefix)
        {
            try
            {
                AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
                using (client = new AmazonS3Client(credentials, endPoint))
                {
                    string[] keyNames;
                    ListObjectsRequest request = new ListObjectsRequest
                    {
                        BucketName = bucketName,
                        Prefix = prefix
                    };

                    do
                    {
                        ListObjectsResponse response = client.ListObjects(request);
                        keyNames = response.S3Objects.Select(x => x.Key).ToArray();

                        if (response.IsTruncated)
                        {
                            request.Marker = response.NextMarker;
                        }
                        else
                        {
                            request = null;
                        }
                    }
                    while (request != null);

                    DeleteObjectsRequest multiObjectDeleteRequest = new DeleteObjectsRequest();
                    multiObjectDeleteRequest.BucketName = bucketName;

                    for (int i = 0; i < keyNames.Length; i++)
                    {
                        multiObjectDeleteRequest.AddKey(keyNames[i], null);
                    }

                    client.DeleteObjects(multiObjectDeleteRequest);
                }
            }
            catch (AmazonS3Exception ex)
            {
                Logger.ErrorFormat("AWS DeleteByPrefix error!! errorCode:{0}, msg:{1} ", ex.ErrorCode, ex.Message);
            }
        }

        private void CopyObject(string sourceBucket, string sourceKey, string destBucket, string destKey)
        {
            try
            {
                AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
                using (client = new AmazonS3Client(credentials, endPoint))
                {
                    CopyObjectRequest request = new CopyObjectRequest
                    {
                        SourceBucket = sourceBucket,
                        SourceKey = sourceKey,
                        DestinationBucket = destBucket,
                        DestinationKey = destKey
                    };
                    client.CopyObject(request);
                }
            }
            catch (AmazonS3Exception ex)
            {
                Logger.ErrorFormat("AWS CopyObject error!! errorCode:{0}, msg:{1} ", ex.ErrorCode, ex.Message);
            }
        }

        private static string CombinePath(params string[] paths)
        {
            return string.Join("/", paths).Replace("\\", "/");
        }
    }
}

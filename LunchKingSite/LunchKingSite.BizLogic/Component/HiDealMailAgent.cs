﻿using EnterpriseDT.Net.Ftp;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.XPath;

namespace LunchKingSite.BizLogic.Component
{
    public class HiDealMailAgent
    {
        public const int DivideInto = 5;
        private static ILog _log;
        private static IHiDealProvider _hdp;
        private static ISysConfProvider _config;
        private static EmailAgent _ea = new EmailAgent();
        private static MailAddress _service_email;

        private static readonly DateTime Today = DateTime.ParseExact(
                                                ((DateTime.Today).ToString("yyyy/MM/dd") + " 12:00:00"), "yyyy/MM/dd HH:mm:ss", null);

        private static readonly string Now = string.Format("{0:yyyy/MM/dd HH:mm:ss}", DateTime.Now);
        private const int MailToSaveLength = 100000;
        private const string STimeFormat = "{0:HH:mm:ss}";
        private const string SDateFormat = "{0:yyyy-MM-dd}";

        public HiDealMailAgent()
        {
            _log = LogManager.GetLogger(typeof(EmailAgent));
            _hdp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _service_email = new MailAddress(_config.ServiceEmail);
        }

        #region 通知信

        public static IXPathNavigable GetSellerOrStoreCloseDownXsl()
        {
            string physicalPath = HttpContext.Current.Server.MapPath(@"~/template/CloseDownNotification.xslt");

            var doc = new XmlDocument();
            doc.Load(physicalPath);

            return doc;
        }

        public static IXPathNavigable GetCouponExpireDateChangedNotificationXsl()
        {
            string physicalPath = HttpContext.Current.Server.MapPath(@"~/template/CouponExpireDateChangedNotification.xslt");

            var doc = new XmlDocument();
            doc.Load(physicalPath);

            return doc;
        }

        #endregion 通知信

        #region HiDeal 通知信

        public static IXPathNavigable GetHiDealNotificationXsl(PiinlifeNotificationType type)
        {
            string fullPath = "";

            switch (type)
            {
                case PiinlifeNotificationType.PurchaseSuccessNotification:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "PurchaseSuccessNotification.xsl");
                    break;

                case PiinlifeNotificationType.AtmTransferReminder:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "AtmTransferReminder.xsl");
                    break;

                case PiinlifeNotificationType.EInvoice:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "EInvoice.xsl");
                    break;

                case PiinlifeNotificationType.EInvoiceWonNotification:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "EInvoiceWonNotification.xsl");
                    break;

                case PiinlifeNotificationType.RefundSuccessNotification:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "RefundSuccessNotification.xsl");
                    break;

                case PiinlifeNotificationType.RefundFailureNotification:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "RefundFailureNotification.xsl");
                    break;

                case PiinlifeNotificationType.RefundApplicationFormNotification:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "RefundApplicationFormNotification.xsl");
                    break;

                case PiinlifeNotificationType.StartUseReminder:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "StartUseReminder.xsl");
                    break;

                case PiinlifeNotificationType.ExpireReminder:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "ExpireReminder.xsl");
                    break;

                case PiinlifeNotificationType.CouponMail:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "GetCouponMail.xsl");
                    break;

                case PiinlifeNotificationType.RefundCouponNotification:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "ApplyRefundCouponNotification.xsl");
                    break;

                case PiinlifeNotificationType.RefundGoodsNotification:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "ApplyRefundGoodsNotification.xsl");
                    break;

                case PiinlifeNotificationType.DiscountSingleFormNotification:
                    fullPath = Path.Combine(_config.HiDealNotificationTemplatesPath, "DiscountSingleFormNotification.xsl");
                    break;
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(fullPath);

            return doc;
        }

        #endregion HiDeal 通知信

        #region Piinlife EDM

        #region 產生取消訂閱所需之連結

        //還沒有取消訂閱的頁面

        #endregion 產生取消訂閱所需之連結

        #endregion Piinlife EDM

        #region SendEDMMail

   
        /// <summary>
        /// 判斷要寄送時的郵件格式是否合法，
        /// 判斷包含
        /// 1.格式、 2.正式/測試站
        /// </summary>
        /// <param name="strEmailAddress"></param>
        /// <returns></returns>
        public bool ChkLegalEmailWhenSend(string strEmailAddress)
        {
            bool isLegal = false;
            try
            {
                if (RegExRules.CheckEmail(strEmailAddress)) //判斷格式
                {
                    #region 判斷正式機或是測試機環境

                    if (_config.SmtpMailAgent == SmtpMailServerAgent.SmtpMailAgent)  //正式機環境
                    {
                        isLegal = true;
                    }
                    else if (_config.SmtpMailAgent == SmtpMailServerAgent.SiteSmtpMailAgentFilterHost)
                    {
                        MailAddress userEmail = new MailAddress(strEmailAddress);
                        if (userEmail.Host.Equals(_service_email.Host))
                        {
                            isLegal = true;
                        }
                    }

                    #endregion 判斷正式機或是測試機環境
                }
            }
            catch
            {
                isLegal = false;
            }

            return isLegal;
        }

        #endregion SendEDMMail

        #region Mail Event

        #region PostMan

        /// <summary>
        /// 使用 PostMan 發送，
        /// 此函式已包含發出的 mail address 是否合法之判斷
        /// </summary>
        /// <param name="strSenderMail">寄件人郵件</param>
        /// <param name="strTo">收件人郵件</param>
        /// <param name="strSubject">主旨</param>
        /// <param name="strBody">信件內容</param>
        /// <param name="isChkEmail">是否驗證 mail 是否合法。預設不驗證</param>
        /// <param name="strSenderName">寄件人名稱</param>
        /// <param name="attachments">附件</param>
        public void SendWithPostMan(string strSenderMail, string strTo, string strSubject, string strBody,
            string strSenderName = "", bool isChkEmail = false, List<Attachment> attachments = null)
        {
            bool isSend = (isChkEmail && !ChkLegalEmailWhenSend(strTo)) ? false : true;

            if (isSend)
            {
                MailMessage msg = GetMailMessage(strSenderMail, strTo, strSubject, strBody, strSenderName);
                if (attachments != null && attachments.Count > 0)
                {
                    foreach (var attachment in attachments)
                    {
                        msg.Attachments.Add(attachment);
                    }
                }
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        /// <summary>
        /// PostMan所需之MailMessage
        /// </summary>
        /// <param name="strFrom"></param>
        /// <param name="strTo"></param>
        /// <param name="strSubject"></param>
        /// <param name="strBody"></param>
        /// <returns></returns>
        public static MailMessage GetMailMessage(string strFrom, string strTo, string strSubject, string strBody, string strFromName = "")
        {
            MailMessage msg = new MailMessage();
            msg.From = (string.IsNullOrEmpty(strFromName)) ? new MailAddress(strFrom) : new MailAddress(strFrom, strFromName);
            msg.To.Add(strTo);
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            return msg;
        }

        #endregion PostMan


        #endregion Mail Event
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component.PCP
{
    /// <summary>
    /// POS機 ，熟客卡實作 (ex:錢都)
    /// </summary>
    public class PcpPosService : PcpBase, IPcpService
    {

        public PcpPosService(int userId, int cardGroupId)
        {

            _userId = userId;
            _cardGroupId = cardGroupId;
        }


        public PcpCheckoutResult DepositAdd(UserDepositAddItemInputModel inputModel)
        {
            throw new NotImplementedException();
        }

        public PcpCheckoutResult DepositUse(UserDepositItemParentInputModel inputModel)
        {
            throw new NotImplementedException();
        }

        public PcpCheckoutResult DepositCorrect(UserDepositCorrectInputModel inputModel)
        {
            throw new NotImplementedException();
        }

        public PcpCheckoutResult PointAdd(UserPointAddInputModel inputModel)
        {
            throw new NotImplementedException();
        }

        public PcpCheckoutResult PointUse(UserPointUseInputModel inputModel)
        {
            throw new NotImplementedException();
        }

        public PcpCheckoutResult PointCorrect(UserPointCorrectInputModel inputModel)
        {
            throw new NotImplementedException();
        }
    }
}

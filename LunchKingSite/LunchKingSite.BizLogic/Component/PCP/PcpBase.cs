﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component.PCP
{

    public abstract class PcpBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected static ISellerProvider _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        /// <summary>
        /// 
        /// </summary>
        protected static readonly IPCPProvider _pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();

        /// <summary>
        /// 
        /// </summary>
        protected static readonly IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        /// <summary>
        /// 
        /// </summary>
        protected static readonly ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        /// <summary>
        /// 
        /// </summary>
        protected int _userId;
        /// <summary>
        /// 
        /// </summary>
        protected int _cardGroupId;

        /// <summary>
        /// 領取熟客卡
        /// </summary>
        /// <returns></returns>
        public bool ApplyCard()
        {
            var cardCollect = _pcp.ViewMembershipCardGetListByDatetime(_cardGroupId, DateTime.Now);
            ViewMembershipCard membershipCard = null;

            //領卡等級規則，只有0就領0，如果012領1
            if (cardCollect.FirstOrDefault(x => x.Level == (int)MembershipCardLevel.Level0) != null && cardCollect.Count == 1)
            {
                membershipCard = cardCollect.FirstOrDefault(x => x.Level == (int)MembershipCardLevel.Level0);
            }
            else
            {
                membershipCard = cardCollect.FirstOrDefault(x => x.Level == (int)MembershipCardLevel.Level1);
            }

            if (membershipCard == null)
            {
                return false;
            }

            //曾經擁有此卡片，目前狀態為關閉
            var checkExist = _pcp.UserMembershipCardGetByUserId(_userId);
            //已擁有此卡片，並且運作正常
            if (checkExist.Any(x => x.CardGroupId == _cardGroupId && x.Enabled == true))
            {
                return true;
            }

            if (checkExist.Any(x => x.CardGroupId == _cardGroupId && x.Enabled == false))
            {
                int useToCardId = 0;

                UserMembershipCard userMembershipCard = checkExist.FirstOrDefault(x => x.CardGroupId == _cardGroupId && x.Enabled == false);

                //取得先前的CardId，目的是為了要拿舊版的Level
                useToCardId = userMembershipCard.CardId;

                //Update version為最新版本
                var membershipCardGroupVersion = _pcp.MembershipCardGroupVersionGetCardGroupLastData(_cardGroupId);
                if (membershipCardGroupVersion.IsLoaded)
                {
                    //之前的Level等級
                    var level = _pcp.MembershipCardGet(useToCardId).Level;

                    //取得熟客卡
                    var lastMembershipCard = _pcp.MembershipCardGetByGroupAndVersion(_cardGroupId, membershipCardGroupVersion.Id).FirstOrDefault(x => x.Level == level);

                    if (userMembershipCard.IsLoaded)
                    {
                        userMembershipCard.Enabled = true;                        
                        userMembershipCard.CardId = lastMembershipCard.Id;
                        userMembershipCard.StartTime = lastMembershipCard.OpenTime;
                        userMembershipCard.EndTime = lastMembershipCard.CloseTime;
                        _pcp.UserMembershipCardSet(userMembershipCard);
                    }
                }
                else
                {
                    return false;
                }
            }
            //從來都沒拿過此卡片，需要新增
            else
            {
                UserMembershipCard userCard = new UserMembershipCard
                {
                    UserId = _userId,
                    CardGroupId = membershipCard.CardGroupId,
                    CardId = membershipCard.CardId,
                    OrderCount = 0,
                    AmountTotal = 0,
                    Enabled = true,
                    StartTime = membershipCard.OpenTime,
                    EndTime = membershipCard.CloseTime,
                    CreateTime = DateTime.Now,
                    CreateId = _config.SystemUserId,
                    RequestTime = DateTime.Now,
                    //編碼規則:U+西元年後兩碼+月份兩碼＋日期兩碼＋4碼流水號  例如： U1412010001
                    CardNo = _pcp.UserMembershipCardNoGet(DateTime.Now)
                };

                _pcp.UserMembershipCardSet(userCard);

                _mp.MembershipCardLogSet(new MembershipCardLog
                {
                    MembershipCardIdBefore = membershipCard.CardId,
                    MembershipCardIdAfter = membershipCard.CardId,
                    UserMembershipCardId = userCard.Id,
                    Memo = BonusFacade.MembershipCardLevelUpLogMemo(1),
                    Type = (byte)MembershipCardLogType.System,
                    CreateId = _userId,
                    CreateTime = DateTime.Now
                });

                var mcg = _pcp.MembershipCardGroupGet(membershipCard.CardGroupId);
                if (mcg.IsLoaded)
                {
                    mcg.HotPoint += 1;
                    _pcp.MembershipCardGroupSet(mcg);
                }
            }

            return true;
        }

    }

    /// <summary>
    /// 熟客卡基底實作
    /// </summary>
    public interface IPcpService : IPcpDepositService, IPcpPointService
    {
        bool ApplyCard();
    }

    public interface IPcpPointService
    {
        PcpCheckoutResult PointAdd(UserPointAddInputModel inputModel);
        PcpCheckoutResult PointUse(UserPointUseInputModel inputModel);
        PcpCheckoutResult PointCorrect(UserPointCorrectInputModel inputModel);
    }

    /// <summary>
    /// 寄杯實作
    /// </summary>
    public interface IPcpDepositService
    {
        /// <summary>
        /// 寄杯商品
        /// </summary>
        PcpCheckoutResult DepositAdd(UserDepositAddItemInputModel inputModel);
        /// <summary>
        /// 使用寄杯
        /// </summary>
        PcpCheckoutResult DepositUse(UserDepositItemParentInputModel inputModel);
        /// <summary>
        /// 更正寄杯
        /// </summary>
        /// <returns></returns>
        PcpCheckoutResult DepositCorrect(UserDepositCorrectInputModel inputModel);
    }

    /// <summary>
    ///  核銷實作
    /// </summary>
    public interface IPcpVerify
    {
        /// <summary>
        /// 確認核銷前
        /// </summary>
        void CheckVerify();
        /// <summary>
        /// 執行核銷
        /// </summary>
        void Verify();
    }
}


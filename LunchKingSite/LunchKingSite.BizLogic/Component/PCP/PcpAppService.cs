﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using log4net;

namespace LunchKingSite.BizLogic.Component.PCP
{
    /// <summary>
    /// 手機App ，熟客卡實作
    /// </summary>
    public class PcpAppService : PcpBase, IPcpService
    {

        private PcpType _type;
        public PcpType Type { get { return _type; } }
        private static ILog logger;

        public PcpAppService(int userId, int cardGroupId)
        {
            _userId = userId;
            _cardGroupId = cardGroupId;
            logger = LogManager.GetLogger(typeof(PcpAppService));
        }
        
        public PcpCheckoutResult DepositAdd(UserDepositAddItemInputModel userDepositItem)
        {
            //建立訂單
            Guid pcpOrderGuid = Guid.NewGuid();
            double discountAmount = 0;            //TODO: 再確認，暫定折扣金額0
            int? discountCodeId = null;
            var superBonus = 0;
            var userMembershipCard = _pcp.UserMembershipCardGetByCardGroupIdAndUserId(userDepositItem.GroupId, _userId);
            var depositItem = _pcp.PcpDepositItemGet(userDepositItem.ItemId);
            var ppus = PcpFacade.GetPollingStatusById(_userId);
            var timeNow = DateTime.Now;
            using (var tran = TransactionScopeBuilder.CreateReadCommitted())
            {
                try
                {
                    #region 建立訂單(pcp_order)
                    var order = new PcpOrder
                    {
                        Guid = pcpOrderGuid,
                        UserId = _userId,
                        CardId = userMembershipCard.Id,
                        Amount = 0,
                        DiscountAmount = (decimal)discountAmount,
                        DiscountCodeId = discountCodeId,
                        BonusPoint = (decimal)superBonus,
                        OrderTime = timeNow,
                        Status = (int)PcpOrderStatus.Normal,
                        StatusDescription = Helper.GetDescription(PcpUserDepositLogType.Add),
                        IdentityCodeId = 0,              //TODO: 再確認，寄杯先預設個0
                        StoreGuid = Guid.Parse(userDepositItem.SellerGuid),
                        CreateId = userDepositItem.CreateId,
                        OrderType = (int)PcpOrderType.Deposit
                    };
                    _pcp.PcpOrderSet(order);
                    #endregion

                    #region 熟客卡(會員卡)更新 
                    //TODO:再確認，寄杯次數是否要更新熟客卡累積次數
                    //Pcp.UserMembershipCardUpdateOrderCount(cardId, amount ?? 0);

                    #endregion 熟客卡(會員卡)更新

                    #region 使用者購買品項
                    var pudi = new PcpUserDeposit()
                    {
                        DepositItemId = userDepositItem.ItemId,
                        UserId = _userId,
                        TotalAmount = userDepositItem.TotalAmount,
                        RemainAmount = userDepositItem.RemainAmount,
                        PcpOrderGuid = pcpOrderGuid,
                        CreateId = userDepositItem.CreateId,
                        CreateTime = timeNow,
                        LastUseTime = timeNow
                    };
                    _pcp.PcpUserDepositSet(pudi);

                    #endregion

                    #region 核銷品項

                    //用總數新增pcp_deposit_coupon ，並核銷已使用數量coupon
                    var pdcs = new PcpDepositCouponCollection();
                    for (int i = 0; i < userDepositItem.TotalAmount; i++)
                    {
                        var pdc = new PcpDepositCoupon()
                        {
                            UserDepositItemId = pudi.Id,
                            Status = (int)PcpDespositCouponStatus.Unused,
                            CreatedTime = timeNow
                        };
                        pdcs.Add(pdc);
                    }
                    //已使用數
                    int usedCount = userDepositItem.TotalAmount - userDepositItem.RemainAmount;

                    pdcs.Take(usedCount).ForEach(x => x.Status = 1);
                    pdcs.Take(usedCount).ForEach(x => x.ExchangeTime = DateTime.Now);

                    _pcp.PcpDepositCouponCollectionSet(pdcs);
                    #endregion

                    #region 購買Log
                    //新增寄杯
                    var pudlAdd = new PcpUserDepositLog()
                    {
                        UserId = _userId,
                        ItemName = depositItem.ItemName,
                        UserDepositId = pudi.Id,
                        Description = Helper.GetDescription(PcpUserDepositLogType.Add),
                        SellerGuid = Guid.Parse(userDepositItem.SellerGuid),
                        CreateId = userDepositItem.CreateId,
                        CreateTime = timeNow,
                        DepositType = (int)PcpUserDepositLogType.Add,
                        Amount = userDepositItem.TotalAmount//新增寄杯為+；兌換寄杯為-
                    };
                    _pcp.PcpUserDepositLogSet(pudlAdd);

                    //兌換寄杯(買了馬上用)
                    if (userDepositItem.TotalAmount - userDepositItem.RemainAmount > 0)
                    {
                        var pudlUse = new PcpUserDepositLog()
                        {
                            UserId = _userId,
                            ItemName = depositItem.ItemName,
                            UserDepositId = pudi.Id,
                            Description = Helper.GetDescription(PcpUserDepositLogType.Use),
                            CreateId = userDepositItem.CreateId,
                            SellerGuid = Guid.Parse(userDepositItem.SellerGuid),
                            CreateTime = timeNow,
                            DepositType = (int)PcpUserDepositLogType.Use,
                            Amount = -(userDepositItem.TotalAmount - userDepositItem.RemainAmount)//新增寄杯為+；兌換寄杯為-
                        };
                        _pcp.PcpUserDepositLogSet(pudlUse);
                    }
                    #endregion

                    #region update polling status

                    ppus.Action = (int)PcpPollingUserActionType.Complete;
                    ppus.UserMembershipCardId = userMembershipCard.Id;
                    ppus.ModifiedTime = DateTime.Now;
                    _pcp.PcpPollingUserStatusSet(ppus);
                    #endregion

                    tran.Complete();
                }
                catch (Exception ex)
                {
                    ppus.Action = (int)PcpPollingUserActionType.TransactionFail;
                    ppus.ModifiedTime = timeNow;
                    ppus.Alive = (int)PcpPollingUserAlive.Dead;
                    _pcp.PcpPollingUserStatusSet(ppus);

                    logger.ErrorFormat("新增寄杯失敗，DepositAdd Error:cardGroupId={0}&userId={1}\n{2}", _cardGroupId, _userId, ex);

                    return PcpCheckoutResult.PcpOrderTransactionFail;
                }

            }

            return PcpCheckoutResult.Success;
        }

        public PcpCheckoutResult DepositUse(UserDepositItemParentInputModel inputModel)
        {
            var ppus = PcpFacade.GetPollingStatusById(_userId);
            var userMembershipCard = _pcp.UserMembershipCardGetByCardGroupIdAndUserId(_cardGroupId, _userId);
            var timeNow = DateTime.Now;
            using (var tran = TransactionScopeBuilder.CreateReadCommitted())
            {
                try
                {
                    foreach (var m in inputModel.InputList)
                    {
                        #region 建立訂單(pcp_order)
                        var order = new PcpOrder
                        {
                            Guid = Guid.NewGuid(),
                            UserId = _userId,
                            CardId = userMembershipCard.Id,
                            Amount = 0,
                            //DiscountAmount = null,
                            //DiscountCodeId = null,
                            //BonusPoint = null,
                            OrderTime = timeNow,
                            Status = (int)PcpOrderStatus.Normal,
                            StatusDescription = Helper.GetDescription(PcpUserDepositLogType.Use),
                            //IdentityCodeId = null,
                            StoreGuid = Guid.Parse(inputModel.SellerGuid),
                            CreateId = inputModel.CreateId,
                            OrderType = (int)PcpOrderType.Deposit
                        };
                        _pcp.PcpOrderSet(order);
                        #endregion

                        var pudi = PcpFacade.GetUserDepositItemByUserAndId(_userId, m.UserDepositItemId);   //TODO:回傳錯誤使用者寄杯編號，補上回傳編號Msg
                        if (!pudi.IsLoaded)
                        { 
                            return PcpCheckoutResult.PcpOrderTransactionFail;
                        }

                        var pdcs = PcpFacade.GetPcpDepositCouponCollection(m.UserDepositItemId);
                        if (pdcs.Count(x => x.Status != (int)PcpDespositCouponStatus.Invalid) != pudi.TotalAmount || //總數對不上
                            pdcs.Count(x => x.Status == (int)PcpDespositCouponStatus.Unused) != pudi.RemainAmount)//剩餘數對不上 
                        { 
                            return PcpCheckoutResult.PcpOrderTransactionFail;
                        }

                        if (pdcs.Count(x => x.Status == 0) < m.UseAmount)   //TODO:回傳使用者使用量>剩餘數量Msg
                        {
                            return PcpCheckoutResult.PcpOrderTransactionFail;
                        }

                        pudi.RemainAmount -= m.UseAmount; //剩餘減掉使用
                        pudi.LastUseTime = timeNow;
                        _pcp.PcpUserDepositSet(pudi);
                        var depositItem = _pcp.PcpDepositItemGet(pudi.DepositItemId);

                        PcpDepositCouponCollection useCopoun=new PcpDepositCouponCollection();
                        foreach (var c in pdcs.Where(c => c.Status == 0))
                        {
                            useCopoun.Add(c);
                        }
                        
                        useCopoun.Take(m.UseAmount).ForEach(x => x.Status = 1);
                        useCopoun.Take(m.UseAmount).ForEach(x => x.ExchangeTime = DateTime.Now);
                        _pcp.PcpDepositCouponCollectionSet(useCopoun);

                        #region 寄杯紀錄(pcp_user_deposit_log)
                        var pudl = new PcpUserDepositLog()
                        {
                            UserId = _userId,
                            ItemName = depositItem.ItemName,
                            UserDepositId = pudi.Id,
                            Description = Helper.GetDescription(PcpUserDepositLogType.Use),
                            SellerGuid = Guid.Parse(inputModel.SellerGuid),
                            CreateId = inputModel.CreateId,
                            CreateTime = timeNow,
                            DepositType = (int)PcpUserDepositLogType.Use,
                            Amount = -m.UseAmount//新增寄杯為+；兌換寄杯為-
                        };
                        _pcp.PcpUserDepositLogSet(pudl);

                        #endregion

                        #region update polling status

                        ppus.Action = (int)PcpPollingUserActionType.Complete;
                        ppus.UserMembershipCardId = userMembershipCard.Id;
                        ppus.ModifiedTime = timeNow;
                        _pcp.PcpPollingUserStatusSet(ppus);
                        #endregion

                    }
                    tran.Complete();
                }
                catch (Exception ex)
                {
                    ppus.Action = (int)PcpPollingUserActionType.TransactionFail;
                    ppus.ModifiedTime = timeNow;
                    ppus.Alive = (int)PcpPollingUserAlive.Dead;
                    _pcp.PcpPollingUserStatusSet(ppus);

                    logger.ErrorFormat("兌換寄杯失敗，DepositUse Error:cardGroupId={0}&userId={1}\n{2}", _cardGroupId, _userId, ex);

                    return PcpCheckoutResult.PcpOrderTransactionFail;
                }
            }

            return PcpCheckoutResult.Success;
        }

        public PcpCheckoutResult DepositCorrect(UserDepositCorrectInputModel inputModel)
        {
            var pollingStatus = PcpFacade.GetPollingStatusById(_userId);
            var userMembershipCard = _pcp.UserMembershipCardGetByCardGroupIdAndUserId(_cardGroupId, _userId);
            try
            {
                using (var tran = TransactionScopeBuilder.CreateReadCommitted())
                {
                    var userDeposit = PcpFacade.GetUserDepositItemByUserAndId(_userId, inputModel.DepositId);
                    var depositItem = _pcp.PcpDepositItemGet(userDeposit.DepositItemId);//←為了取當下的ItemName，Log用
                    var timeNow = DateTime.Now;

                    #region 建立訂單(pcp_order)
                    var order = new PcpOrder
                    {
                        Guid = Guid.NewGuid(),
                        UserId = _userId,
                        CardId = userMembershipCard.Id,
                        Amount = 0,
                        //DiscountAmount = null,
                        //DiscountCodeId = null,
                        //BonusPoint = null,
                        OrderTime = timeNow,
                        Status = (int)PcpOrderStatus.Normal,
                        StatusDescription = Helper.GetDescription(PcpUserDepositLogType.Correct),
                        //IdentityCodeId = null,
                        StoreGuid = Guid.Parse(inputModel.SellerGuid),
                        CreateId = inputModel.CreateId,
                        OrderType = (int)PcpOrderType.Deposit
                    };
                    _pcp.PcpOrderSet(order);
                    #endregion

                    #region 檢查數量是否一致 (pcp_deposit_coupon、pcp_user_deposit)
                    var userCoupons = _pcp.PcpDepositCouponCollectionGet(inputModel.DepositId);
                    if (userCoupons.Count(x => x.Status != (int)PcpDespositCouponStatus.Invalid) != userDeposit.TotalAmount || //總數對不上
                        userCoupons.Count(x => x.Status == (int)PcpDespositCouponStatus.Unused) != userDeposit.RemainAmount)//剩餘數對不上
                    {
                        return PcpCheckoutResult.PcpOrderTransactionFail;
                    }
                    #endregion

                    #region 更新總數、剩餘數 (pcp_user_deposit)
                    userDeposit.TotalAmount += inputModel.Adjust;
                    userDeposit.RemainAmount += inputModel.Adjust;
                    if (userDeposit.RemainAmount < 0)
                    {
                        return PcpCheckoutResult.PcpOrderTransactionFail;//更正後 剩餘數變負數→不正常操作
                    }
                    else
                    {
                        _pcp.PcpUserDepositSet(userDeposit);
                    }
                    #endregion

                    #region 更新coupon數量 (pcp_deposit_coupon)
                    if (inputModel.Adjust > 0)
                    {
                        #region 補coupon (+)
                        var toAddCoupons = new PcpDepositCouponCollection();
                        for (int i = 0; i < inputModel.Adjust; i++)
                        {
                            var coupon = new PcpDepositCoupon()
                            {
                                UserDepositItemId = userDeposit.Id,
                                Status = (int)PcpDespositCouponStatus.Unused,
                                CreatedTime = DateTime.Now
                            };
                            toAddCoupons.Add(coupon);
                        }
                        _pcp.PcpDepositCouponCollectionSet(toAddCoupons);
                        #endregion
                    }
                    else
                    {
                        #region 作廢coupon (-)
                        int minusCount = Math.Abs(inputModel.Adjust);
                        foreach (var coupon in userCoupons.Where(x => x.Status == (int)PcpDespositCouponStatus.Unused)
                                                          .OrderBy(x => x.CreatedTime)
                                                          .Take(minusCount))
                        {
                            coupon.Status = (int)PcpDespositCouponStatus.Invalid;
                        }
                        _pcp.PcpDepositCouponCollectionSet(userCoupons);
                        #endregion
                    }
                    #endregion

                    #region 新增Log (pcp_user_deposit_log)
                    var log = new PcpUserDepositLog()
                    {
                        UserId = _userId,
                        UserDepositId = userDeposit.Id,
                        DepositType = (int)PcpUserDepositLogType.Correct,
                        Amount = inputModel.Adjust,//更正，+-都有可能
                        ItemName = depositItem.ItemName,
                        Description = inputModel.Description,
                        SellerGuid = Guid.Parse(inputModel.SellerGuid),
                        CreateId = inputModel.CreateId,
                        CreateTime = DateTime.Now
                    };
                    _pcp.PcpUserDepositLogSet(log);
                    #endregion

                    #region update polling status

                    pollingStatus.Action = (int)PcpPollingUserActionType.Complete;
                    pollingStatus.UserMembershipCardId = userMembershipCard.Id;
                    pollingStatus.ModifiedTime = DateTime.Now;
                    _pcp.PcpPollingUserStatusSet(pollingStatus);
                    #endregion

                    tran.Complete();

                    return PcpCheckoutResult.Success;
                }
            }
            catch (Exception ex)
            {
                pollingStatus.Action = (int)PcpPollingUserActionType.TransactionFail;
                pollingStatus.ModifiedTime = DateTime.Now;
                pollingStatus.Alive = (int)PcpPollingUserAlive.Dead;
                _pcp.PcpPollingUserStatusSet(pollingStatus);

                logger.ErrorFormat("更正寄杯失敗，DepositCorrect Error:cardGroupId={0}&userId={1}\n{2}", _cardGroupId, _userId, ex);

                return PcpCheckoutResult.PcpOrderTransactionFail;
            }
        }

        public PcpCheckoutResult PointAdd(UserPointAddInputModel inputModel)
        {
            var pollingStatus = PcpFacade.GetPollingStatusById(_userId);
            try
            {
                using (var tran = TransactionScopeBuilder.CreateReadCommitted())
                {
                    var userMembershipCard = _pcp.UserMembershipCardGetByCardGroupIdAndUserId(_cardGroupId, _userId);
                    var collectRule = _pcp.PcpPointCollectRuleGet(_cardGroupId, inputModel.CollectRuleId);//集點規則
                    int currentPoint = PcpFacade.GetUserRemainPoint(_cardGroupId, _userId);

                    #region 建立訂單(pcp_order)
                    var order = new PcpOrder
                    {
                        Guid = Guid.NewGuid(),
                        UserId = _userId,
                        CardId = userMembershipCard.Id,
                        Amount = inputModel.Price,
                        //DiscountAmount = null,
                        //DiscountCodeId = null,
                        //BonusPoint = null,
                        OrderTime = DateTime.Now,
                        Status = (int)PcpOrderStatus.Normal,
                        StatusDescription = Helper.GetDescription(PcpUserPointSetType.Collect),
                        //IdentityCodeId = null,
                        StoreGuid = Guid.Parse(inputModel.SellerGuid),
                        CreateId = inputModel.CreateId,
                        OrderType = (int)PcpOrderType.Point
                    };
                    _pcp.PcpOrderSet(order);
                    #endregion

                    #region 新增集點(pcp_user_point)
                    //count = 組數；考量到之後若可以寄2杯得一點之類的，用規則算應該比較正確，故不取前台傳的
                    int point = Convert.ToInt32(inputModel.Price) / collectRule.CollectValue;
                    var userPoint = new PcpUserPoint
                    {
                        CardGroupId = _cardGroupId,
                        UserId = _userId,
                        TotalPoint = point,
                        RemainPoint = point,
                        FinalPoint = currentPoint + point,//當前點數 + 新增點數
                        Price = inputModel.Price,
                        SetType = (int)PcpUserPointSetType.Collect,
                        PointRuleId = inputModel.CollectRuleId,
                        ItemName = string.Format(Message.RegularsPointCollectRule, collectRule.CollectValue),
                        Count = point,
                        PcpOrderGuid = order.Guid,
                        SellerGuid = new Guid(inputModel.SellerGuid),
                        ExpireTime = new DateTime(DateTime.Now.Year + 1, 2, 1),//yyyy/2/1 00:00:00失效
                        CreateId = inputModel.CreateId,
                        CreateTime = DateTime.Now,
                    };
                    _pcp.PcpUserPointSet(userPoint);
                    #endregion

                    #region update polling status
                    pollingStatus.Action = (int)PcpPollingUserActionType.Complete;
                    pollingStatus.ModifiedTime = DateTime.Now;
                    pollingStatus.UserMembershipCardId = userMembershipCard.Id;
                    _pcp.PcpPollingUserStatusSet(pollingStatus);
                    #endregion

                    tran.Complete();

                    return PcpCheckoutResult.Success;
                }
            }
            catch (Exception ex)
            {
                pollingStatus.Action = (int)PcpPollingUserActionType.TransactionFail;
                pollingStatus.ModifiedTime = DateTime.Now;
                pollingStatus.Alive = (int)PcpPollingUserAlive.Dead;
                _pcp.PcpPollingUserStatusSet(pollingStatus);

                logger.ErrorFormat("發送點數失敗，PointAdd Error:cardGroupId={0}&userId={1}\n{2}", _cardGroupId, _userId, ex);

                return PcpCheckoutResult.PcpOrderTransactionFail;
            }
        }

        public PcpCheckoutResult PointUse(UserPointUseInputModel inputModel)
        {
            var pollingStatus = PcpFacade.GetPollingStatusById(_userId);
            try
            {
                using (var tran = TransactionScopeBuilder.CreateReadCommitted())
                {
                    var userMembershipCard = _pcp.UserMembershipCardGetByCardGroupIdAndUserId(_cardGroupId, _userId);//消費者熟客卡
                    var userPointList = PcpFacade.GetUserRemainPointCollection(_cardGroupId, _userId);//消費者剩餘點數列表
                    var exchangeRule = _pcp.PcpPointExchangeRuleGet(_cardGroupId, inputModel.ExchangeRuleId);//兌換項目

                    int currentPoint = userPointList.Sum(x => x.RemainPoint);
                    int cost = inputModel.Count * exchangeRule.Point;//此次兌點花費點數 (數量x兌換所需點數)

                    if (currentPoint >= cost)
                    {
                        #region 建立訂單(pcp_order)
                        var order = new PcpOrder
                        {
                            Guid = Guid.NewGuid(),
                            UserId = _userId,
                            CardId = userMembershipCard.Id,
                            Amount = 0,
                            //DiscountAmount = null,
                            //DiscountCodeId = null,
                            //BonusPoint = null,
                            OrderTime = DateTime.Now,
                            Status = (int)PcpOrderStatus.Normal,
                            StatusDescription = Helper.GetDescription(PcpUserPointSetType.Exchange),
                            //IdentityCodeId = null,
                            StoreGuid = Guid.Parse(inputModel.SellerGuid),
                            CreateId = inputModel.CreateId,
                            OrderType = (int)PcpOrderType.Point
                        };
                        _pcp.PcpOrderSet(order);
                        #endregion

                        #region 新增兌點(pcp_user_point)
                        var userPointExchange = new PcpUserPoint
                        {
                            CardGroupId = _cardGroupId,
                            UserId = _userId,
                            TotalPoint = -cost,
                            RemainPoint = 0,
                            FinalPoint = currentPoint - cost,//當前點數 - 花費點數
                            Price = null,
                            SetType = (int)PcpUserPointSetType.Exchange,
                            PointRuleId = inputModel.ExchangeRuleId,
                            ItemName = string.Format(Message.RegularsPointExchangeRule, exchangeRule.Point, exchangeRule.ItemName),
                            Count = inputModel.Count,
                            PcpOrderGuid = order.Guid,
                            SellerGuid = new Guid(inputModel.SellerGuid),
                            ExpireTime = null,
                            CreateId = inputModel.CreateId,
                            CreateTime = DateTime.Now,
                        };
                        _pcp.PcpUserPointSet(userPointExchange);
                        #endregion

                        #region 扣點 (更新pcp_user_point、新增pcp_use_point_alter_pair)
                        int lack = cost;
                        for (int i = 0; i < userPointList.Count() && lack > 0; i++)
                        {
                            #region 扣點
                            int pointConsume = 0;
                            if (userPointList[i].RemainPoint >= lack)
                            {
                                //此筆夠扣
                                pointConsume = lack;
                                userPointList[i].RemainPoint -= lack;
                                lack = 0;
                            }
                            else
                            {
                                //此筆不夠需扣下一筆
                                pointConsume = userPointList[i].RemainPoint;
                                lack -= userPointList[i].RemainPoint;
                                userPointList[i].RemainPoint = 0;
                            }
                            _pcp.PcpUserPointSet(userPointList[i]);
                            #endregion

                            #region 扣哪一筆點數的紀錄
                            var alterPair = new PcpUserPointAlterPair()
                            {
                                UserPointIdNegative = userPointExchange.Id,
                                UserPointIdPositive = userPointList[i].Id,
                                PointConsume = pointConsume,
                                CreateTime = DateTime.Now
                            };
                            _pcp.PcpUserPointAlterPairSet(alterPair);
                            #endregion
                        }
                        #endregion

                        #region update polling status
                        pollingStatus.Action = (int)PcpPollingUserActionType.Complete;
                        pollingStatus.ModifiedTime = DateTime.Now;
                        pollingStatus.UserMembershipCardId = userMembershipCard.Id;
                        _pcp.PcpPollingUserStatusSet(pollingStatus);
                        #endregion
                    }
                    else
                    {
                        //點數不足
                        return PcpCheckoutResult.PcpOrderTransactionFail;
                    }

                    tran.Complete();

                    return PcpCheckoutResult.Success;
                }
            }
            catch (Exception ex)
            {
                pollingStatus.Action = (int)PcpPollingUserActionType.TransactionFail;
                pollingStatus.ModifiedTime = DateTime.Now;
                pollingStatus.Alive = (int)PcpPollingUserAlive.Dead;
                _pcp.PcpPollingUserStatusSet(pollingStatus);

                logger.ErrorFormat("兌換點數失敗，PointUse Error:cardGroupId={0}&userId={1}\n{2}", _cardGroupId, _userId, ex);

                return PcpCheckoutResult.PcpOrderTransactionFail;
            }
        }

        public PcpCheckoutResult PointCorrect(UserPointCorrectInputModel inputModel)
        {
            var pollingStatus = PcpFacade.GetPollingStatusById(_userId);
            try
            {
                using (var tran = TransactionScopeBuilder.CreateReadCommitted())
                {
                    var userMembershipCard = _pcp.UserMembershipCardGetByCardGroupIdAndUserId(_cardGroupId, _userId);//消費者熟客卡
                    var userPointList = PcpFacade.GetUserRemainPointCollection(_cardGroupId, _userId);//消費者剩餘點數列表
                    
                    int currentPoint = userPointList.Sum(x => x.RemainPoint);
                    int finalPoint = currentPoint + inputModel.Adjust;

                    if (finalPoint >= 0)
                    {
                        #region 建立訂單(pcp_order)
                        var order = new PcpOrder
                        {
                            Guid = Guid.NewGuid(),
                            UserId = _userId,
                            CardId = userMembershipCard.Id,
                            Amount = 0,
                            OrderTime = DateTime.Now,
                            Status = (int)PcpOrderStatus.Normal,
                            StatusDescription = Helper.GetDescription(PcpUserPointSetType.Correct),
                            StoreGuid = Guid.Parse(inputModel.SellerGuid),
                            CreateId = inputModel.CreateId,
                            OrderType = (int)PcpOrderType.Point
                        };
                        _pcp.PcpOrderSet(order);
                        #endregion

                        #region 更正點數 (新增pcp_user_point)
                        var userPointCorrect = new PcpUserPoint
                        {
                            CardGroupId = _cardGroupId,
                            UserId = _userId,
                            TotalPoint = inputModel.Adjust,
                            RemainPoint = (inputModel.Adjust > 0) ? inputModel.Adjust : 0,
                            FinalPoint = finalPoint,
                            Price = null,
                            SetType = (int)PcpUserPointSetType.Correct,
                            PointRuleId = null,
                            ItemName = inputModel.Description,
                            Count = 0,
                            PcpOrderGuid = order.Guid,
                            SellerGuid = new Guid(inputModel.SellerGuid),
                            ExpireTime = new DateTime(DateTime.Now.Year + 1, 2, 1),//yyyy/2/1 00:00:00失效
                            CreateId = inputModel.CreateId,
                            CreateTime = DateTime.Now,
                        };
                        _pcp.PcpUserPointSet(userPointCorrect);
                        #endregion

                        
                        if (inputModel.Adjust < 0)
                        {
                            #region 更正為扣點 (更新pcp_user_point、新增pcp_use_point_alter_pair)
                            int lack = Math.Abs(inputModel.Adjust);
                            for (int i = 0; i < userPointList.Count() && lack > 0; i++)
                            {
                                #region 扣點
                                int pointConsume = 0;
                                if (userPointList[i].RemainPoint >= lack)
                                {
                                    //此筆夠扣
                                    pointConsume = lack;
                                    userPointList[i].RemainPoint -= lack;
                                    lack = 0;
                                }
                                else
                                {
                                    //此筆不夠需扣下一筆
                                    pointConsume = userPointList[i].RemainPoint;
                                    lack -= userPointList[i].RemainPoint;
                                    userPointList[i].RemainPoint = 0;
                                }
                                _pcp.PcpUserPointSet(userPointList[i]);
                                #endregion

                                #region 扣哪一筆點數的紀錄
                                var alterPair = new PcpUserPointAlterPair()
                                {
                                    UserPointIdNegative = userPointCorrect.Id,
                                    UserPointIdPositive = userPointList[i].Id,
                                    PointConsume = pointConsume,
                                    CreateTime = DateTime.Now
                                };
                                _pcp.PcpUserPointAlterPairSet(alterPair);
                                #endregion
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        //點數不足
                        return PcpCheckoutResult.PcpOrderTransactionFail;
                    }
                    #region update polling status

                    pollingStatus.Action = (int)PcpPollingUserActionType.Complete;
                    pollingStatus.UserMembershipCardId = userMembershipCard.Id;
                    pollingStatus.ModifiedTime = DateTime.Now;
                    _pcp.PcpPollingUserStatusSet(pollingStatus);
                    #endregion
                    tran.Complete();
                    return PcpCheckoutResult.Success;
                }
            }
            catch (Exception ex)
            {
                pollingStatus.Action = (int)PcpPollingUserActionType.TransactionFail;
                pollingStatus.ModifiedTime = DateTime.Now;
                pollingStatus.Alive = (int)PcpPollingUserAlive.Dead;
                _pcp.PcpPollingUserStatusSet(pollingStatus);
                
                logger.ErrorFormat("更正點數失敗，PointCorrect Error:cardGroupId={0}&userId={1}\n{2}", _cardGroupId, _userId, ex);

                return PcpCheckoutResult.PcpOrderTransactionFail;
            }
        }

    }
}

﻿using System;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.Security;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using log4net;

namespace LunchKingSite.BizLogic.Component
{
    public class CookieManager
    {
        private static readonly ISysConfProvider config;

        static CookieManager()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        public static void SetMemberAuth(string userName, bool isKeepLogin = false)
        {
            int timeout;
            if (isKeepLogin == false)
            {
                timeout = config.SessionStateTimeOut;
            }
            else
            {
                timeout = (int)FormsAuthentication.Timeout.TotalMinutes;
            }
            var authTicket = new FormsAuthenticationTicket(userName, true, timeout);
            var encryptTicket = FormsAuthentication.Encrypt(authTicket);
            var cookie = NewCookie(new HttpCookie(FormsAuthentication.FormsCookieName, encryptTicket)
            {
                HttpOnly = true,
                Expires = DateTime.Now.AddMinutes(timeout)
            });
            HttpContext.Current.Response.AppendCookie(cookie);
        }

        public static void SetVersion(int version, bool isKeepCookie = false)
        {
            var response = HttpContext.Current.Response;
            response.Cookies.Add(MakeCookie(LkSiteCookie.CookieVersion.ToString(), version.ToString(CultureInfo.InvariantCulture), isKeepCookie));
        }

        public static void SetMemberSource(SingleSignOnSource singelSource, bool isKeepCookie = false)
        {
            var response = HttpContext.Current.Response;
            var str = Convert.ToString((int)singelSource);
            response.Cookies.Add(MakeCookie(LkSiteCookie.LoginSource.ToString(), str, isKeepCookie));
        }

        public static void SetMemberDiscount(Dictionary<decimal, int> discount, bool isKeepCookie = true)
        {
            HttpResponse response = HttpContext.Current.Response;
            string discountinfo = string.Join("|", discount.Select(x => x.Key + "#" + x.Value));
            DateTime expire = DateTime.Now.AddMinutes(20); // 預設保留20分鐘
            response.Cookies.Set(MakeCookie(LkSiteCookie.MemberDiscount.ToString(), discountinfo, isKeepCookie, expire));
        }

        public static SingleSignOnSource GetLoginSource()
        {
            var cookie = HttpContext.Current.Request.Cookies[LkSiteCookie.LoginSource.ToString()];

            if (cookie == null || string.IsNullOrEmpty(cookie.Value))
            {
                return SingleSignOnSource.Undefined;
            }

            int loginSource;
            if (int.TryParse(cookie.Value, out loginSource))
            {
                return (SingleSignOnSource)loginSource;
            }

            SingleSignOnSource source;
            var data = new JsonSerializer().Deserialize<CookieModel>(Helper.Decrypt(cookie.Value));
            return Enum.TryParse(data.Value, out source) ? source : SingleSignOnSource.Undefined;
        }

        /// <summary>
        /// 取得辨識訪問者唯一的識別碼 (類MAC address的替代方案)
        /// </summary>
        /// <returns></returns>
        public static Guid? GetVisitorIdentity()
        {
            HttpCookie visitorIdentityCookie = HttpContext.Current.Request.Cookies[LkSiteCookie.VisitorIdentity.ToString()];
            if (visitorIdentityCookie != null)
            {
                var cookieValue = Helper.FixCookieVersionIssueForAndroid(visitorIdentityCookie.Value);
                Guid parsevisitorIdentity;
                Guid.TryParse(cookieValue, out parsevisitorIdentity);
                return parsevisitorIdentity;
            }
            return null;
        }

        /// <summary>
        /// 設定辨識訪問者唯一的識別碼
        /// </summary>
        /// <returns></returns>
        public static void SetVisitorIdentity()
        {
            HttpCookie visitorIdentityCookie = HttpContext.Current.Request.Cookies[LkSiteCookie.VisitorIdentity.ToString()];
            if (visitorIdentityCookie == null)
            {
                var newVisitorIdentity = Guid.NewGuid();
                var newCookie = CookieManager.NewCookie(new HttpCookie(LkSiteCookie.VisitorIdentity.ToString(), newVisitorIdentity.ToString())
                {
                    Expires = DateTime.Now.AddDays(config.VisitorIdentityCookieExpires)
                });

                HttpContext.Current.Response.Cookies.Add(newCookie);
            }
        }

        public static HttpCookie NewCookie(HttpCookie cookie)
        {
            //cookie.Secure = true;
            return cookie;
        }

        public static HttpCookie NewCookie(string cookieName)
        {
            return new HttpCookie(cookieName)
            {
                //Secure = true
            };
        }

        public static HttpCookie NewCookie(string cookieName, string value)
        {
            return new HttpCookie(cookieName, value)
            {
                //Secure = true
            };
        }

        public static HttpCookie NewCookie(string cookieName, string value, DateTime expires)
        {
            return new HttpCookie(cookieName, value)
            {
                //Secure = true
            };
        }

        public static HttpCookie MakeCookie(string cookieName, string value, bool isKeepCookie)
        {
            DateTime expire = isKeepCookie ? DateTime.Now.AddMinutes(FormsAuthentication.Timeout.TotalMinutes)
                : DateTime.Now.AddMinutes(config.SessionStateTimeOut);

            return MakeCookie(cookieName, value, isKeepCookie, expire);
        }

        public static HttpCookie MakeCookie(string cookieName, string value, bool isKeepCookie, DateTime expire)
        {
            var data = new CookieModel
            {
                Value = value,
                IsKeepCookie = isKeepCookie,
                Expires = expire
            };

            var cookie = NewCookie(cookieName, Helper.Encrypt(new JsonSerializer().Serialize(data)));

            if (data.Expires != null)
            {
                cookie.Expires = (DateTime)data.Expires;
            }

            return cookie;
        }

        /// <summary>
        /// set cookie expired
        /// </summary>
        /// <param name="lkSiteCookieName"></param>
        public static void DeleteCookie(LkSiteCookie lkSiteCookieName)
        {
            HttpResponse response = HttpContext.Current.Response;
            HttpRequest request = HttpContext.Current.Request;
            string cookieName = lkSiteCookieName.ToString();
            if (request.Cookies[cookieName] != null)
            {
                response.Cookies[cookieName].Expires = DateTime.Now.AddDays(-1);
            }
        }

        /// <summary>
        /// 設定 APP OS Version
        /// </summary>
        /// <param name="os"></param>
        /// <param name="osVersion"></param>
        public static void SetAppOsVersion(string os, string osVersion)
        {
            HttpCookie appOsVersionCookie = HttpContext.Current.Request.Cookies[LkSiteCookie.AppOsVersion.ToString()];
            if (appOsVersionCookie != null)
            {
                DeleteCookie(LkSiteCookie.AppOsVersion);
            }

            var newCookie = CookieManager.NewCookie(new HttpCookie(LkSiteCookie.AppOsVersion.ToString(),
                new JsonSerializer().Serialize(new KeyValuePair<string, string>(os, osVersion)))
            {
                Expires = DateTime.Now.AddDays(config.VisitorIdentityCookieExpires)
            });

            HttpContext.Current.Response.Cookies.Add(newCookie);
        }

        public static AppOsVersion GetAppOsVersion()
        {
            HttpCookie appOsVersionCookie = HttpContext.Current.Request.Cookies[LkSiteCookie.AppOsVersion.ToString()];
            if (appOsVersionCookie != null)
            {
                var appOsVersion = new JsonSerializer().Deserialize<KeyValuePair<string, string>>(appOsVersionCookie.Value);
                if (appOsVersion.Key.Equals("Android", StringComparison.OrdinalIgnoreCase))
                {
                    int version;
                    if (int.TryParse(appOsVersion.Value, out version) && Enum.IsDefined(typeof(AppOsVersion), version))
                    {
                        return (AppOsVersion)version;
                    }
                }
            }
            return AppOsVersion.None;
        }

        #region Marketing

        public static int? GetHomeCategoryId()
        {
            HttpRequest request = HttpContext.Current.Request;
            HttpCookie cookie = request.Cookies[LkSiteCookie.HomeCategoryId.ToString()];
            int result;
            if (cookie == null || int.TryParse(cookie.Value, out result) == false)
            {
                return null;
            }
            return result;
        }

        public static void SetHomeCategoryId(int categoryId)
        {
            var response = HttpContext.Current.Response;
            HttpCookie cookie = CookieManager.NewCookie(new HttpCookie(LkSiteCookie.HomeCategoryId.ToString())
            {
                Expires = DateTime.Now.AddMonths(2),
                Value = categoryId.ToString()
            });
            response.Cookies.Add(cookie);
        }

        public static int? GetPponCityId()
        {
            HttpRequest request = HttpContext.Current.Request;
            HttpCookie cookie = request.Cookies[LkSiteCookie.CityId.ToString()];
            int result;
            if (cookie == null || int.TryParse(cookie.Value, out result) == false)
            {
                return null;
            }
            return result;
        }

        public static void SetPponCityId(int cityId)
        {
            var response = HttpContext.Current.Response;
            HttpCookie cookie = CookieManager.NewCookie(new HttpCookie(LkSiteCookie.CityId.ToString())
            {
                Expires = DateTime.Now.AddYears(1),
                Value = cityId.ToString()
            });
            response.Cookies.Add(cookie);
        }

        public static string GetCpaKey()
        {
            HttpRequest request = HttpContext.Current.Request;
            HttpCookie cookie = request.Cookies[LkSiteCookie.CpaKey.ToString()];
            if (cookie == null)
            {
                return string.Empty;
            }
            return cookie.Value;
        }

        public static void SetCpaKey(string cpaKey, int totalSeconds)
        {
            var response = HttpContext.Current.Response;
            HttpCookie cookie = CookieManager.NewCookie(new HttpCookie(LkSiteCookie.CpaKey.ToString())
            {
                Expires = DateTime.Now.AddSeconds(totalSeconds),
                Value = cpaKey
            });
            response.Cookies.Add(cookie);
        }

        #endregion
    }
}

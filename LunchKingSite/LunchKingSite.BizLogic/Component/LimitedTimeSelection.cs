﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.PponEntities;

namespace LunchKingSite.BizLogic.Component
{
    public class LimitedTimeSelection
    {
        public int Id { get; set; }

        public DateTime TheDate { get; set; }

        public List<LimitedTimeSelectionDealView> LimitedTimeSelectionDealViews { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime ModifyTime { get; set; }
    }

    public class LimitedTimeSelectionDealView
    {
        public int Id { get; set; }

        public int MainId { get; set; }

        public Guid Bid { get; set; }

        public short Sequence { get; set; }

        public bool Enabled { get; set; }      

        public MultipleMainDealPreview MultipleMainDealPreviews { get; set; }
    }
}

﻿using com.hitrust.b2ctoolkit.b2cpay;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using LunchKingSite.BizLogic.Facade;
using System.Web;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.BizLogic.CathayCRDOrderService;

namespace LunchKingSite.BizLogic.Component
{
    public class CreditCardUtility
    {
        #region settings

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        private static ILog logger = LogManager.GetLogger("CreditCardUtility");
        private static ILog logger_db = LogManager.GetLogger("LogToDb");
        private static HashSet<string> creditcardInBlackList = null;
        private static PaymentAPIProvider defaultProvider = PaymentAPIProvider.HiTrustDotNetTest;

        public const string _TEST_CARD_NUMBER = "8888880000000006";

        #region HiTrust

        private static string hitrustDatetimeFormat = "yyyy-MM-dd-HH.mm.ss";

        #endregion HiTrust

        #endregion settings

        #region creditcard payment provider implementation

        /// <summary>
        ///     新增HiTrust收款帳戶的步驟
        ///     1.在ISysConfProvider增加一個屬性設定，參考 HiTrustMerchantIDForPiinLife
        ///     2.在列舉 PaymentAPIProvider 增加新選項
        ///     3.建立一個class繼承HiTrustPaymentProviderBase並實作MerConfigName
        ///     4.在CreditcardPaymentProviderContext建立列舉選項與物件的關聯
        /// </summary>
        private interface ICreditcardPaymentProvider
        {
            CreditCardQueryDetail Query(string transactionID);

            CreditCardAuthResult Auth(CreditCardAuthObject authObject);

            CreditCardAuthResult BeginAuth(CreditCardAuthObject authObject);

            CreditCardAuthResult Auth(ApplePayAuthObject authObject,bool isOrderFromApp);

            bool AuthReverse(string transactionID, string authCode);

            bool Capture(string transactionID, int amount, string authCode);

            bool CaptureReverse(string transactionID, string authCode);

            bool Refund(string transactionID, int amount, string authCode);

            bool RefundReverse(string transactionID, string authCode);
        }

        private class CreditcardPaymentProviderContext
        {
            private static Dictionary<PaymentAPIProvider, ICreditcardPaymentProvider> _providers =
                new Dictionary<PaymentAPIProvider, ICreditcardPaymentProvider>();

            static CreditcardPaymentProviderContext()
            {
                #region HiTrust

                _providers.Add(PaymentAPIProvider.HiTrust, new HiTrustPaymentProvider());
                _providers.Add(PaymentAPIProvider.HiTrustPiinLife, new HiTrustPiinLifePaymentProvider());
                _providers.Add(PaymentAPIProvider.HiTrustPayeasyTravel, new HiTrustPayeasyTravelPaymentProvider());
                _providers.Add(PaymentAPIProvider.HiTrustUnionPay, new HiTrustUnionPayPaymentProvider());
                _providers.Add(PaymentAPIProvider.HiTrustPiinLifeUnionPay, new HiTrustPiinLifeUnionPayPaymentProvider());
                _providers.Add(PaymentAPIProvider.HiTrustContactWithCVV2, new HiTrustContactWithCVV2PaymenProvider());
                _providers.Add(PaymentAPIProvider.HiTrustContactWithoutCVV2, new HiTrustContactWithoutCVV2PaymenProvider());

                #endregion HiTrust

                #region Neweb

                _providers.Add(PaymentAPIProvider.Neweb, new NewebPaymentProvider());
                _providers.Add(PaymentAPIProvider.NewebPiinLife, new NewebPiinLifePaymentProvider());
                _providers.Add(PaymentAPIProvider.NewebContactWithCVV2, new NewebContactWithCVV2PaymentProvider());
                _providers.Add(PaymentAPIProvider.NewebContactWithoutCVV2, new NewebContactWithoutCVV2PaymentProvider());
                _providers.Add(PaymentAPIProvider.NewebContactInstallment, new NewebContactInstallmentPaymentProvider());

                #endregion Neweb

                #region Test

                _providers.Add(PaymentAPIProvider.Mock, new MockPaymentProvider());
                _providers.Add(PaymentAPIProvider.HiTrustComTest, new HiTrustDotNetTestPaymentProvider());
                _providers.Add(PaymentAPIProvider.HiTrustDotNetTest, new HiTrustDotNetTestPaymentProvider());
                _providers.Add(PaymentAPIProvider.NewebTest, new NewebTestPaymentProvider());

                #endregion Test

                #region  TaishinPaymentGateway

                _providers.Add(PaymentAPIProvider.TaishinPaymnetGateway, new TaishinPaymentProvider());
                _providers.Add(PaymentAPIProvider.TaishinMallPaymnetGateway, new TaishinMallPaymentProvider());
                _providers.Add(PaymentAPIProvider.ApplePay, new ApplePayProvider());
                _providers.Add(PaymentAPIProvider.TaishinPaymnetOTPGateway, new TaishinPaymentOTPProvider());

                #endregion TaishinPayment

                #region CathayPaymnetGateway

                _providers.Add(PaymentAPIProvider.CathayPaymnetGateway, new CathayPaymentProvider());
                _providers.Add(PaymentAPIProvider.CathayPaymnetOTPGateway, new CathayPaymentOtpProvider());
                _providers.Add(PaymentAPIProvider.CathayPaymnetInstallment, new CathayPaymentInstallment());
                _providers.Add(PaymentAPIProvider.CathayPaymnetInstallmentWithOtp, new CathayPaymentInstallmentWithOtpProvider());

                #endregion CathayPaymnetGateway
            }

            public static ICreditcardPaymentProvider Get(PaymentAPIProvider providerType)
            {
                if (_providers.ContainsKey(providerType))
                {
                    return _providers[providerType];
                }
                throw new Exception(string.Format("{0} creditcard provider not supported.", providerType));
            }
        }

        #region HiTrust

        private abstract class HiTrustPaymentProviderBase : ICreditcardPaymentProvider
        {
            public abstract string MerchantConfigName { get; }

            public abstract string ServerConfigName { get; }

            public abstract string MerchantId { get; }

            public virtual CreditCardAuthResult Auth(CreditCardAuthObject authObject)
            {
                return HiTrustAuth(authObject, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public virtual CreditCardAuthResult BeginAuth(CreditCardAuthObject authObject)
            {
                throw new NotImplementedException();
            }

            public virtual CreditCardAuthResult Auth(ApplePayAuthObject authObject, bool isOrderFromApp)
            {
                throw new NotImplementedException();
            }

            public virtual bool AuthReverse(string transactionID, string authCode)
            {
                return HiTrustAuthReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public virtual bool Capture(string transactionID, int amount, string authCode)
            {
                return HiTrustCapture(transactionID, amount, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public virtual bool CaptureReverse(string transactionID, string authCode)
            {
                return HiTrustCaptureReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public virtual bool Refund(string transactionID, int amount, string authCode)
            {
                return HiTrustRefund(transactionID, amount, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public virtual bool RefundReverse(string transactionID, string authCode)
            {
                return HiTrustRefundReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public virtual CreditCardQueryDetail Query(string transactionID)
            {
                return HiTrustQuery(transactionID, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

        }

        private class HiTrustPaymentProvider : HiTrustPaymentProviderBase
        {
            public override string MerchantConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + config.HiTrustMerchantID + ".conf";
                }
            }

            public override string ServerConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + "HiServer.conf";
                }
            }

            public override string MerchantId
            {
                get
                {
                    return config.HiTrustMerchantID;
                }
            }
        }

        private class HiTrustPiinLifePaymentProvider : HiTrustPaymentProviderBase
        {
            public override string MerchantConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + config.HiTrustPiinLifeMerchantID + ".conf";
                }
            }

            public override string ServerConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + "HiServer.conf";
                }
            }

            public override string MerchantId
            {
                get
                {
                    return config.HiTrustPiinLifeMerchantID;
                }
            }
        }

        private class HiTrustPayeasyTravelPaymentProvider : HiTrustPaymentProviderBase
        {
            public override string MerchantConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + config.HiTrustPayeasyTravelMerchantID + ".conf";
                }
            }

            public override string ServerConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + "HiServer.conf";
                }
            }

            public override string MerchantId
            {
                get
                {
                    return config.HiTrustPayeasyTravelMerchantID;
                }
            }
        }

        private class HiTrustUnionPayPaymentProvider : HiTrustPaymentProviderBase
        {
            public override string MerchantConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + config.HiTrustUnionPayMerchantID + ".conf";
                }
            }

            public override string ServerConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + "HiServer.conf";
                }
            }

            public override string MerchantId
            {
                get
                {
                    return config.HiTrustUnionPayMerchantID;
                }
            }

            public override CreditCardAuthResult Auth(CreditCardAuthObject authObject)
            {
                return HiTrustAuth(authObject, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override bool AuthReverse(string transactionID, string authCode)
            {
                return HiTrustAuthReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override bool Capture(string transactionID, int amount, string authCode)
            {
                return HiTrustCapture(transactionID, amount, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override bool CaptureReverse(string transactionID, string authCode)
            {
                return HiTrustCaptureReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override bool Refund(string transactionID, int amount, string authCode)
            {
                return HiTrustRefund(transactionID, amount, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override bool RefundReverse(string transactionID, string authCode)
            {
                return HiTrustRefundReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override CreditCardQueryDetail Query(string transactionID)
            {
                return HiTrustQuery(transactionID, MerchantConfigName, ServerConfigName, MerchantId, true);
            }
        }

        private class HiTrustPiinLifeUnionPayPaymentProvider : HiTrustPaymentProviderBase
        {
            public override string MerchantConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + config.HiTrustPiinLifeUnionPayMerchantID + ".conf";
                }
            }

            public override string ServerConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + "HiServer.conf";
                }
            }

            public override string MerchantId
            {
                get
                {
                    return config.HiTrustPiinLifeUnionPayMerchantID;
                }
            }

            public override CreditCardAuthResult Auth(CreditCardAuthObject authObject)
            {
                return HiTrustAuth(authObject, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override bool AuthReverse(string transactionID, string authCode)
            {
                return HiTrustAuthReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override bool Capture(string transactionID, int amount, string authCode)
            {
                return HiTrustCapture(transactionID, amount, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override bool CaptureReverse(string transactionID, string authCode)
            {
                return HiTrustCaptureReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override bool Refund(string transactionID, int amount, string authCode)
            {
                return HiTrustRefund(transactionID, amount, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override bool RefundReverse(string transactionID, string authCode)
            {
                return HiTrustRefundReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, true);
            }

            public override CreditCardQueryDetail Query(string transactionID)
            {
                return HiTrustQuery(transactionID, MerchantConfigName, ServerConfigName, MerchantId, true);
            }
        }

        private class HiTrustContactWithCVV2PaymenProvider : HiTrustPaymentProviderBase
        {
            public override string MerchantConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + config.HiTrustContactWithCVV2MerchantID + ".conf";
                }
            }

            public override string ServerConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + "HiServer.conf";
                }
            }

            public override string MerchantId
            {
                get
                {
                    return config.HiTrustContactWithCVV2MerchantID;
                }
            }
        }

        private class HiTrustContactWithoutCVV2PaymenProvider : HiTrustPaymentProviderBase
        {
            public override string MerchantConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + config.HiTrustContactWithoutCVV2MerchantID + ".conf";
                }
            }

            public override string ServerConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + "HiServer.conf";
                }
            }

            public override string MerchantId
            {
                get
                {
                    return config.HiTrustContactWithoutCVV2MerchantID;
                }
            }
        }

        #endregion HiTrust

        #region Neweb

        private abstract class NewebPaymentProviderBase : ICreditcardPaymentProvider
        {
            public abstract string UserID { get; }

            public abstract string UserPassword { get; }

            public abstract string MerchantID { get; }

            public abstract string AcceptServer { get; }

            public abstract string AdminServer { get; }

            public CreditCardAuthResult Auth(CreditCardAuthObject authObject)
            {
                return NewebAuth(authObject, UserID, UserPassword, MerchantID, AcceptServer, AdminServer);
            }
            
            public virtual CreditCardAuthResult BeginAuth(CreditCardAuthObject authObject)
            {
                throw new NotImplementedException();
            }

            public virtual CreditCardAuthResult Auth(ApplePayAuthObject authObject, bool isOrderFromApp)
            {
                throw new NotImplementedException();
            }

            public bool AuthReverse(string transactionID, string authCode)
            {
                return NewebAuthReverse(transactionID, UserID, UserPassword, MerchantID, AdminServer);
            }

            public bool Capture(string transactionID, int amount, string authCode)
            {
                return NewebCapture(transactionID, amount, UserID, UserPassword, MerchantID, AdminServer);
            }

            public bool CaptureReverse(string transactionID, string authCode)
            {
                return NewebCaptureReverse(transactionID, UserID, UserPassword, MerchantID, AdminServer);
            }

            public bool Refund(string transactionID, int amount, string authCode)
            {
                return NewebRefund(transactionID, amount, UserID, UserPassword, MerchantID, AdminServer);
            }

            public bool RefundReverse(string transactionID, string authCode)
            {
                return NewebRefundReverse(transactionID, UserID, UserPassword, MerchantID, AdminServer);
            }

            public CreditCardQueryDetail Query(string transactionID)
            {
                return NewebQuery(transactionID, UserID, UserPassword, MerchantID, AdminServer);
            }
        }

        private class NewebPaymentProvider : NewebPaymentProviderBase
        {
            public override string UserID
            {
                get
                {
                    return config.NewebUserID;
                }
            }

            public override string UserPassword
            {
                get
                {
                    return config.NewebUserPassword;
                }
            }

            public override string MerchantID
            {
                get
                {
                    return config.NewebMerchantID;
                }
            }

            public override string AcceptServer
            {
                get
                {
                    return config.NewebAcceptServer;
                }
            }

            public override string AdminServer
            {
                get
                {
                    return config.NewebAdminServer;
                }
            }
        }

        private class NewebPiinLifePaymentProvider : NewebPaymentProviderBase
        {
            public override string UserID
            {
                get
                {
                    return config.NewebUserID;
                }
            }

            public override string UserPassword
            {
                get
                {
                    return config.NewebUserPassword;
                }
            }

            public override string MerchantID
            {
                get
                {
                    return config.NewebPiinLifeMerchantID;
                }
            }

            public override string AcceptServer
            {
                get
                {
                    return config.NewebAcceptServer;
                }
            }

            public override string AdminServer
            {
                get
                {
                    return config.NewebAdminServer;
                }
            }
        }

        private class NewebContactWithCVV2PaymentProvider : NewebPaymentProviderBase
        {
            public override string UserID
            {
                get
                {
                    return config.NewebContactUserID;
                }
            }

            public override string UserPassword
            {
                get
                {
                    return config.NewebContactUserPassword;
                }
            }

            public override string MerchantID
            {
                get
                {
                    return config.NewebContactMerchantID;
                }
            }

            public override string AcceptServer
            {
                get
                {
                    return config.NewebAcceptServer;
                }
            }

            public override string AdminServer
            {
                get
                {
                    return config.NewebAdminServer;
                }
            }
        }

        private class NewebContactWithoutCVV2PaymentProvider : NewebPaymentProviderBase
        {
            public override string UserID
            {
                get
                {
                    return config.NewebContactUserID;
                }
            }

            public override string UserPassword
            {
                get
                {
                    return config.NewebContactUserPassword;
                }
            }

            public override string MerchantID
            {
                get
                {
                    return config.NewebContactPiinLifeMerchantID;
                }
            }

            public override string AcceptServer
            {
                get
                {
                    return config.NewebAcceptServer;
                }
            }

            public override string AdminServer
            {
                get
                {
                    return config.NewebAdminServer;
                }
            }
        }

        private class NewebContactInstallmentPaymentProvider : NewebPaymentProviderBase
        {
            public override string UserID
            {
                get
                {
                    return config.NewebContactUserID;
                }
            }

            public override string UserPassword
            {
                get
                {
                    return config.NewebContactUserPassword;
                }
            }

            public override string MerchantID
            {
                get
                {
                    return config.NewebContactInstallmentMerchantID;
                }
            }

            public override string AcceptServer
            {
                get
                {
                    return config.NewebAcceptServer;
                }
            }

            public override string AdminServer
            {
                get
                {
                    return config.NewebAdminServer;
                }
            }
        }

        #endregion Neweb

        #region Test

        private class MockPaymentProvider : ICreditcardPaymentProvider
        {
            public CreditCardAuthResult BeginAuth(CreditCardAuthObject authObject)
            {
                throw new NotImplementedException();
            }
            public CreditCardAuthResult Auth(CreditCardAuthObject authObject)
            {
                if (authObject.CardNumber == _TEST_CARD_NUMBER)
                {
                    return new CreditCardAuthResult()
                    {
                        CardNumber = authObject.CardNumber.Substring(0, 6) + "******" + authObject.CardNumber.Substring(12, 4),
                        ReturnCode = "0",
                        TransPhase = PayTransPhase.Successful,
                        TransType = PayTransType.Authorization,
                        AuthenticationCode = "7777777",
                        TransMessage = "MOCK測試",
                        OrderDate = DateTime.Now,
                        APIProvider = PaymentAPIProvider.Mock
                    };
                }
                else
                {
                    return new CreditCardAuthResult()
                    {
                        CardNumber = authObject.CardNumber.Substring(0, 6) + "******" + authObject.CardNumber.Substring(12, 4),
                        ReturnCode = "-1",
                        TransPhase = PayTransPhase.Failed,
                        TransType = PayTransType.Authorization,
                        AuthenticationCode = "7777777",
                        TransMessage = "MOCK測試",
                        OrderDate = DateTime.Now,
                        APIProvider = PaymentAPIProvider.Mock
                    };
                }
            }

            public virtual CreditCardAuthResult Auth(ApplePayAuthObject authObject, bool isOrderFromApp)
            {
                throw new NotImplementedException();
            }

            public bool AuthReverse(string transactionID, string authCode)
            {
                throw new NotImplementedException();
            }

            public bool Capture(string transactionID, int amount, string authCode)
            {
                throw new NotImplementedException();
            }

            public bool CaptureReverse(string transactionID, string authCode)
            {
                throw new NotImplementedException();
            }

            public bool Refund(string transactionID, int amount, string authCode)
            {
                throw new NotImplementedException();
            }

            public bool RefundReverse(string transactionID, string authCode)
            {
                throw new NotImplementedException();
            }

            public CreditCardQueryDetail Query(string transactionID)
            {
                throw new NotImplementedException();
            }
        }

        private class HiTrustDotNetTestPaymentProvider : HiTrustPaymentProviderBase
        {
            public override string MerchantConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + config.HiTrustTestMerchantID + ".conf";
                }
            }

            public override string ServerConfigName
            {
                get
                {
                    return config.HiTrustConfigPath + "HiServerTest.conf";
                }
            }

            public override string MerchantId
            {
                get
                {
                    return config.HiTrustTestMerchantID;
                }
            }

            public override CreditCardAuthResult Auth(CreditCardAuthObject authObject)
            {
                return HiTrustAuth(authObject, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public override bool AuthReverse(string transactionID, string authCode)
            {
                return HiTrustAuthReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public override bool Capture(string transactionID, int amount, string authCode)
            {
                return HiTrustCapture(transactionID, amount, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public override bool CaptureReverse(string transactionID, string authCode)
            {
                return HiTrustCaptureReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public override bool Refund(string transactionID, int amount, string authCode)
            {
                return HiTrustRefund(transactionID, amount, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public override bool RefundReverse(string transactionID, string authCode)
            {
                return HiTrustRefundReverse(transactionID, MerchantConfigName, ServerConfigName, MerchantId, false);
            }

            public override CreditCardQueryDetail Query(string transactionID)
            {
                return HiTrustQuery(transactionID, MerchantConfigName, ServerConfigName, MerchantId, false);
            }
        }

        private class NewebTestPaymentProvider : NewebPaymentProviderBase
        {
            public override string UserID
            {
                get
                {
                    return config.NewebTestUserID;
                }
            }

            public override string UserPassword
            {
                get
                {
                    return config.NewebTestUserPassword;
                }
            }

            public override string MerchantID
            {
                get
                {
                    return config.NewebTestMerchantID;
                }
            }

            public override string AcceptServer
            {
                get
                {
                    return config.NewebTestAcceptServer;
                }
            }

            public override string AdminServer
            {
                get
                {
                    return config.NewebTestAdminServer;
                }
            }
        }

        #endregion Test

        #region TaishinPayment

        private abstract class TaishinPaymentProviderBase : ICreditcardPaymentProvider
        { 
            public abstract string MerchantId { get; }

            public abstract string TerminalId { get; }

            public CreditCardAuthResult Auth(CreditCardAuthObject authObject)
            {
                return TaishinPaymentAuth(authObject, MerchantId, TerminalId);
            }

            public virtual CreditCardAuthResult BeginAuth(CreditCardAuthObject authObject)
            {
                throw new NotImplementedException();
            }

            public CreditCardAuthResult Auth(ApplePayAuthObject authObject, bool isOrderFromApp)
            {
                return TaishinPaymentApplePayAuth(authObject, MerchantId, TerminalId, isOrderFromApp);
            }

            public virtual bool AuthReverse(string transactionID, string authCode)
            {
                return TaishinPaymentRequest(transactionID, MerchantId, TerminalId, TaishinPaymentType.AuthReverse);
            }

            public virtual bool Capture(string transactionID, int amount, string authCode)
            {
                return TaishinPaymentRequest(transactionID, MerchantId, TerminalId, TaishinPaymentType.Capture, amount);
            }

            public virtual bool CaptureReverse(string transactionID, string authCode)
            {
                return TaishinPaymentRequest(transactionID, MerchantId, TerminalId, TaishinPaymentType.CaptureReverse);
            }

            public virtual bool Refund(string transactionID, int amount, string authCode)
            {
                return TaishinPaymentRequest(transactionID, MerchantId, TerminalId, TaishinPaymentType.Refund, amount);
            }

            public virtual bool RefundReverse(string transactionID, string authCode)
            {
                return TaishinPaymentRequest(transactionID, MerchantId, TerminalId, TaishinPaymentType.RefundReverse);
            }

            public virtual CreditCardQueryDetail Query(string transactionID)
            {
                return TaishinPaymentRequest(transactionID, MerchantId, TerminalId);
            }
        }

        private class TaishinPaymentProvider : TaishinPaymentProviderBase
        {
            public override string MerchantId
            {
                get
                {
                    return config.TaiShinMerchantID;
                }
            }

            public override string TerminalId
            {
                get
                {
                    return config.TaiShinTerminalID;
                }
            }
        }

        private class TaishinMallPaymentProvider : TaishinPaymentProviderBase
        {
            public override string MerchantId
            {
                get
                {
                    return config.TaiShinMallMerchantID;
                }
            }

            public override string TerminalId
            {
                get
                {
                    return config.TaiShinTerminalID;
                }
            }
        }

        private class ApplePayProvider : TaishinPaymentProviderBase
        {
            public override string MerchantId
            {
                get
                {
                    return config.TaiShinMerchantID;
                }
            }

            public override string TerminalId
            {
                get
                {
                    return config.TaiShinTerminalID;
                }
            }
        }

        private class TaishinPaymentOTPProvider : TaishinPaymentProviderBase
        {
            public override string MerchantId
            {
                get
                {
                    return config.TaiShinMerchantID;
                }
            }

            public override string TerminalId
            {
                get
                {
                    return config.TaiShin3DTerminalID;
                }
            }

            public override CreditCardAuthResult BeginAuth(CreditCardAuthObject authObject)
            {
                return TaishinPaymentOTPAuth(authObject, MerchantId, TerminalId);
            }

            public override bool AuthReverse(string transactionID, string authCode)
            {
                return TaishinPaymentOTPRequest(transactionID, MerchantId, TerminalId, TaishinPaymentType.AuthReverse);
            }

            public override bool Capture(string transactionID, int amount, string authCode)
            {
                return TaishinPaymentOTPRequest(transactionID, MerchantId, TerminalId, TaishinPaymentType.Capture, amount);
            }

            public override bool CaptureReverse(string transactionID, string authCode)
            {
                return TaishinPaymentOTPRequest(transactionID, MerchantId, TerminalId, TaishinPaymentType.CaptureReverse);
            }

            public override bool Refund(string transactionID, int amount, string authCode)
            {
                return TaishinPaymentOTPRequest(transactionID, MerchantId, TerminalId, TaishinPaymentType.Refund, amount);
            }

            public override bool RefundReverse(string transactionID, string authCode)
            {
                return TaishinPaymentOTPRequest(transactionID, MerchantId, TerminalId, TaishinPaymentType.RefundReverse);
            }

            public override CreditCardQueryDetail Query(string transactionID)
            {
                return TaishinPaymentOTPRequest(transactionID, MerchantId, TerminalId);
            }
        }

        #endregion TaishinPayment

        #region CathayPaymnetGateway

        private class CathayPaymentProvider : ICreditcardPaymentProvider
        {
            public virtual string MerchantId
            {
                get
                {
                    return config.CathayMerchantID;
                }
            }

            public virtual string CubKey
            {
                get
                {
                    return config.CathayCubKey;
                }
            }

            public CreditCardAuthResult Auth(CreditCardAuthObject authObject)
            {
                return CathayPaymentAuth(authObject, MerchantId, CubKey);
            }

            public virtual CreditCardAuthResult BeginAuth(CreditCardAuthObject authObject)
            {
                throw new NotImplementedException();
            }

            public CreditCardAuthResult Auth(ApplePayAuthObject authObject, bool isOrderFromApp)
            {
                throw new NotImplementedException();
            }

            public bool Refund(string transactionID, int amount, string authCode)
            {
                string apiUrl = config.CathayPaymnetGatewayApi + "CRDOrderService.asmx?wsdl";
                var data = new
                {
                    MERCHANTXML = new
                    {
                        MSGID = "ORD0003",
                        CAVALUE = Security.MD5Hash(MerchantId + transactionID.ToUpper() + amount.ToString() + authCode + CubKey,false),
                        REFUNDORDERINFO = new
                        {
                            STOREID = MerchantId,
                            ORDERNUMBER = transactionID.ToUpper(),
                            AMOUNT = amount.ToString(),
                            AUTHCODE = authCode,
                        }
                    }
                };

                var jsonText = new LunchKingSite.Core.JsonSerializer().Serialize(data);
                System.Xml.XmlDocument doc = JsonConvert.DeserializeXmlNode(jsonText);
                doc.PrependChild(doc.CreateXmlDeclaration("1.0", "utf-8", null));

                var result = CathayPaymentResponseResult<CathayPaymentRefundResult>(doc.InnerXml, apiUrl);
                if (result.Verify(CubKey))
                {
                    LogAction(transactionID, result != null ? result.CUBXML.REFUNDORDERINFO.STATUS : string.Empty);
                    if (result != null)
                    {
                        return result.CUBXML.REFUNDORDERINFO.STATUS == "0000";
                    }
                }
                else
                {
                    logger.Info("CathayPayment Refund Verify Error");
                }
                return false;
            }

            public bool RefundReverse(string transactionID, string authCode)
            {
                string apiUrl = config.CathayPaymnetGatewayApi + "CRDOrderService.asmx?wsdl";
                var data = new
                {
                    MERCHANTXML = new
                    {
                        MSGID = "ORD0004",
                        CAVALUE = Security.MD5Hash(MerchantId + transactionID.ToUpper() + authCode + CubKey,false),
                        CANCELREFUNDINFO = new
                        {
                            STOREID = MerchantId,
                            ORDERNUMBER = transactionID.ToUpper(),
                            AUTHCODE = authCode,
                        }
                    }
                };

                var jsonText = new LunchKingSite.Core.JsonSerializer().Serialize(data);
                System.Xml.XmlDocument doc = JsonConvert.DeserializeXmlNode(jsonText);
                doc.PrependChild(doc.CreateXmlDeclaration("1.0", "utf-8", null));

                var result = CathayPaymentResponseResult<CathayPaymentRefundReverseResult>(doc.InnerXml, apiUrl);
                if (result.Verify(CubKey))
                {
                    LogAction(transactionID, result != null ? result.CUBXML.CANCELREFUNDINFO.STATUS : string.Empty);
                    if (result != null)
                    {
                        return result.CUBXML.CANCELREFUNDINFO.STATUS == "0000";
                    }
                }
                else
                {
                    logger.Info("CathayPayment Cancel Refund Verify Error");
                }
                return false;

            }

            public bool Capture(string transactionID, int amount, string authCode)
            {
                string apiUrl = config.CathayPaymnetGatewayApi + "CRDOrderService.asmx?wsdl";
                var data = new
                {
                    MERCHANTXML = new
                    {
                        MSGID = "ORD0005",
                        CAVALUE = Security.MD5Hash(MerchantId + transactionID.ToUpper() + amount.ToString() + authCode + CubKey,false),
                        CAPTUREORDERINFO = new
                        {
                            STOREID = MerchantId,
                            ORDERNUMBER = transactionID.ToUpper(),
                            AMOUNT = amount.ToString(),
                            AUTHCODE = authCode,
                        }
                    }
                };

                var jsonText = new LunchKingSite.Core.JsonSerializer().Serialize(data);
                System.Xml.XmlDocument doc = JsonConvert.DeserializeXmlNode(jsonText);
                doc.PrependChild(doc.CreateXmlDeclaration("1.0", "utf-8", null));

                var result = CathayPaymentResponseResult<CathayPaymentCaptureResult>(doc.InnerXml, apiUrl);
                if (result.Verify(CubKey))
                {
                    LogAction(transactionID, result != null ? result.CUBXML.CAPTUREORDERINFO.STATUS : string.Empty);
                    if (result != null)
                    {
                        return result.CUBXML.CAPTUREORDERINFO.STATUS == "0000";
                    }
                }
                else
                {
                    logger.Info("CathayPayment Capture Verify Error");
                }
                return false;
            }

            public bool CaptureReverse(string transactionID, string authCode)
            {
                string apiUrl = config.CathayPaymnetGatewayApi + "CRDOrderService.asmx?wsdl";
                var data = new
                {
                    MERCHANTXML = new
                    {
                        MSGID = "ORD0006",
                        CAVALUE = Security.MD5Hash(MerchantId + transactionID.ToUpper() + authCode + CubKey,false),
                        CANCELCAPTUREINFO = new
                        {
                            STOREID = MerchantId,
                            ORDERNUMBER = transactionID.ToUpper(),
                            AUTHCODE = authCode,
                        }
                    }
                };

                var jsonText = new LunchKingSite.Core.JsonSerializer().Serialize(data);
                System.Xml.XmlDocument doc = JsonConvert.DeserializeXmlNode(jsonText);
                doc.PrependChild(doc.CreateXmlDeclaration("1.0", "utf-8", null));

                var result = CathayPaymentResponseResult<CathayPaymentCaptureReverseResult>(doc.InnerXml, apiUrl);
                if (result.Verify(CubKey))
                {
                    LogAction(transactionID, result != null ? result.CUBXML.CANCELCAPTUREINFO.STATUS : string.Empty);
                    if (result != null)
                    {
                        return result.CUBXML.CANCELCAPTUREINFO.STATUS == "0000";
                    }
                }
                else
                {
                    logger.Info("CathayPayment Cancel Capture Verify Error");
                }
                return false;
            }

            public bool AuthReverse(string transactionID, string authCode)
            {
                string apiUrl = config.CathayPaymnetGatewayApi + "CRDOrderService.asmx?wsdl";
                var data = new
                {
                    MERCHANTXML = new
                    {
                        MSGID = "ORD0007",
                        CAVALUE = Security.MD5Hash(MerchantId + transactionID.ToUpper() + authCode + CubKey,false),
                        CANCELORDERINFO = new
                        {
                            STOREID = MerchantId,
                            ORDERNUMBER = transactionID.ToUpper(),
                            AUTHCODE = authCode,
                        }
                    }
                };

                var jsonText = new LunchKingSite.Core.JsonSerializer().Serialize(data);
                System.Xml.XmlDocument doc = JsonConvert.DeserializeXmlNode(jsonText);
                doc.PrependChild(doc.CreateXmlDeclaration("1.0", "utf-8", null));

                var result = CathayPaymentResponseResult<CathayPaymentAuthReverseResult>(doc.InnerXml, apiUrl);
                if (result.Verify(CubKey))
                {
                    LogAction(transactionID, result != null ? result.CUBXML.CANCELORDERINFO.STATUS : string.Empty);
                    if (result != null)
                    {
                        return result.CUBXML.CANCELORDERINFO.STATUS == "0000";
                    }
                }
                else
                {
                    logger.Info("CathayPayment Cancel Auth Verify Error");
                }
                return false;
            }

            /// <summary>
            /// todo:
            /// </summary>
            /// <param name="transactionID"></param>
            /// <returns></returns>
            public CreditCardQueryDetail Query(string transactionID)
            {
                string apiUrl = config.CathayPaymnetGatewayApi + "CRDOrderService.asmx?wsdl";
                var data = new
                {
                    MERCHANTXML = new
                    {
                        MSGID = "ORD0001",
                        CAVALUE = Security.MD5Hash(MerchantId + transactionID.ToUpper() + "" + CubKey,false),
                        ORDERINFO = new
                        {
                            STOREID = MerchantId,
                            ORDERNUMBER = transactionID.ToUpper(),
                            AMOUNT = "",
                        }
                    }
                };

                var jsonText = new LunchKingSite.Core.JsonSerializer().Serialize(data);
                System.Xml.XmlDocument doc = JsonConvert.DeserializeXmlNode(jsonText);
                doc.PrependChild(doc.CreateXmlDeclaration("1.0", "utf-8", null));

                var result = CathayPaymentResponseResult<CathayPaymentQueryResult>(doc.InnerXml, apiUrl);
                if (result.Verify(CubKey))
                {
                    LogAction(transactionID, string.Empty);
                    CreditCardQueryDetail detail = new CreditCardQueryDetail();
                    if (result != null)
                    {
                        detail.OrderStatus = result.CUBXML.ORDERINFO.STATUS;
                    }
                }
                else
                {
                    logger.Info("CathayPayment Cancel Auth Verify Error");
                }

                return null;
            }
        }

        private class CathayPaymentOtpProvider : CathayPaymentProvider
        {
            public override string MerchantId
            {
                get
                {
                    return config.CathayMerchantID;
                }
            }
            public override string CubKey
            {
                get
                {
                    return config.CathayCubKey;
                }
            }

            public override CreditCardAuthResult BeginAuth(CreditCardAuthObject authObject)
            {
                return CathayPaymentOTPAuth(authObject, MerchantId, CubKey);
            }
        }

        private class CathayPaymentInstallmentWithOtpProvider : CathayPaymentProvider
        {
            public override string MerchantId
            {
                get
                {
                    return config.CathayInstallmentMerchantID;
                }
            }
            public override string CubKey
            {
                get
                {
                    return config.CathayInstallmentCubKey;
                }
            }
            public override CreditCardAuthResult BeginAuth(CreditCardAuthObject authObject)
            {
                return CathayPaymentOTPAuth(authObject, MerchantId, CubKey);
            }
        }

        private class CathayPaymentInstallment : CathayPaymentProvider
        {
            public override string MerchantId
            {
                get
                {
                    return config.CathayInstallmentMerchantID;
                }
            }

            public override string CubKey
            {
                get
                {
                    return config.CathayInstallmentCubKey;
                }
            }
        }
        #endregion

        #endregion creditcard payment provider implementation

        #region Utility Methods

        /// <summary>
        /// 授權(包含OTP)
        /// </summary>
        /// <param name="authObject">授權內容</param>
        /// <param name="apiProvider">服務廠商</param>
        /// <param name="orderGuid"></param>
        /// <param name="userName"></param>
        /// <param name="type"></param>
        public static CreditCardAuthResult Authenticate(CreditCardAuthObject authObject, Guid orderGuid, string userName,
            OrderClassification type, CreditCardOrderSource orderSource)
        {
            bool isInBlackList = CheckBlackList(authObject.CardNumber);

            CreditCardAuthResult result;

            if (config.EnableAntiCreditcardFraud && isInBlackList)
            {
                // 黑名單
                string getResultType = GetResult(authObject.CardNumber);
                result = GetBlackListCardResult(authObject.CardNumber, getResultType);
                LogCreditcardOrder(authObject.CardNumber, userName, (authObject.ExpireMonth + "/" + authObject.ExpireYear),
                    result, orderGuid, authObject.Amount, type, (int)result.APIProvider, isInBlackList, false);
                return result;
            }

            #region 執行信用卡認證

            bool isOTP = authObject.OTP.Enabled;

            Guid? bid = null;
            var order = op.ViewPponOrderDetailGetListByOrderGuid(orderGuid).FirstOrDefault();
            if (order.IsLoaded)
            {
                bid = order.BusinessHourGuid;
                logger.Info("[Auth][bid]" + bid.Value);
            }

            authObject.APIProvider = DetermineAPIProvider(orderSource, authObject, isOTP, bid);

            ICreditcardPaymentProvider paymentProvider = CreditcardPaymentProviderContext.Get(authObject.APIProvider);
            if (isOTP)
            {
                result = paymentProvider.BeginAuth(authObject);

                PaymentTransaction pt = op.PaymentTransactionGet(authObject.TransactionId, Core.PaymentType.Creditcard, PayTransType.Authorization);
                pt.Message = "OTP驗證中";
                pt.ApiProvider = (int)authObject.APIProvider;
                op.PaymentTransactionSet(pt);
            }
            else
            {
                result = paymentProvider.Auth(authObject);
                if (null != result)
                {
                    LogCreditcardOrder(authObject.CardNumber, userName, (authObject.ExpireMonth + "/" + authObject.ExpireYear),
                        result, orderGuid, authObject.Amount, type, (int)result.APIProvider, isInBlackList, false);
                }
            }

            #endregion 執行信用卡認證

            return result;
        }
         
        /// <summary>
        /// 授權 for apple pay
        /// </summary>
        /// <param name="authObject"></param>
        /// <param name="orderGuid"></param>
        /// <param name="userName"></param>
        /// <param name="type"></param>
        /// <param name="orderSource"></param>
        /// <param name="isOrderFromApp"></param>
        /// <returns></returns>
        public static CreditCardAuthResult Authenticate(ApplePayAuthObject authObject, Guid orderGuid, string userName,
            OrderClassification type, CreditCardOrderSource orderSource,bool isOrderFromApp)
        {
            CreditCardAuthResult result;

            #region 執行信用卡認證

            authObject.APIProvider = PaymentAPIProvider.ApplePay;

            ICreditcardPaymentProvider paymentProvider = CreditcardPaymentProviderContext.Get(authObject.APIProvider);
            result = paymentProvider.Auth(authObject, isOrderFromApp);

            #endregion 執行信用卡認證

            if (null != result)
            {
                LogCreditcardOrder(authObject.CardNumber, userName, authObject.ExpireDate,
                    result, orderGuid, authObject.Amount / 100, type, (int)result.APIProvider, false, false);
            }

            return result;
        }


        /// <summary>
        /// OTP查詢交易
        /// </summary>
        /// <param name="authObject"></param>
        /// <param name="orderId"></param>
        /// <param name="orderGuid"></param>
        /// <param name="userName"></param>
        /// <param name="type"></param>
        /// <param name="orderSource"></param>
        /// <returns></returns>
        public static CreditCardAuthResult QueryOTP(CreditCardAuthObject authObject, Guid orderGuid, string userName,
            OrderClassification type, CreditCardOrderSource orderSource)
        {
            CreditCardAuthResult result = new Component.CreditCardAuthResult();
            //CreditCardQueryDetail detail = Query(authObject.TransactionId);
            PaymentTransaction ptCreditCard = op.PaymentTransactionGet(authObject.TransactionId, PaymentType.Creditcard, PayTransType.Authorization);
            //目前OTP只有台新、國泰
            if (ptCreditCard.ApiProvider == (int)PaymentAPIProvider.TaishinPaymnetOTPGateway)
            {
                #region 查詢交易結果API
                var argumentParams = new
                {
                    order_no = authObject.TransactionId,
                    result_flag = "1",
                };

                var argument = new
                {
                    sender = "rest",
                    ver = "1.0.0",
                    mid = config.TaiShinMerchantID,
                    tid = config.TaiShin3DTerminalID,
                    pay_type = 1,
                    tx_type = 7,
                    @params = argumentParams
                };

                PaymentFacade.OTPApiLog(authObject.TransactionId, JsonConvert.SerializeObject(argument));

                string url = Helper.CombineUrl(config.TaiShinPaymnetGatewayApi, "other.ashx");
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(argument);

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                string otherResult = "";
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    otherResult = streamReader.ReadToEnd();
                }

                PaymentFacade.OTPApiLog(authObject.TransactionId, otherResult);
                #endregion 查詢交易結果API
                OTPOther other = Newtonsoft.Json.JsonConvert.DeserializeObject<OTPOther>(otherResult);
                string orderStatus = other.@params.order_status;

                result.APIProvider = PaymentAPIProvider.TaishinPaymnetOTPGateway;
                result.CardNumber = MarkCardNumber(authObject.CardNumber);
                result.InstallmentPeriod = authObject.InstallmentPeriod;
                result.OrderDate = DateTime.Now;
                //02:已授權
                //可參考台新 TSPG REST API 6.2 訂單狀態碼說明
                result.ReturnCode = orderStatus == "02" ? "00" : orderStatus;
                result.TransPhase = PayTransPhase.Failed;

                if (ptCreditCard.IsLoaded && ptCreditCard.Result.ToString() != "-1")
                {
                    //表示非同步已回來
                    result.AuthenticationCode = ptCreditCard.AuthCode;
                    result.TransMessage = ptCreditCard.Message;
                    result.ReturnCode = ptCreditCard.Result.ToString() == "0" ? "00" : ptCreditCard.Result.ToString();
                }
            }
            else if (ptCreditCard.ApiProvider == (int)PaymentAPIProvider.CathayPaymnetInstallmentWithOtp || ptCreditCard.ApiProvider == (int)PaymentAPIProvider.CathayPaymnetOTPGateway)
            {
                result.APIProvider = (PaymentAPIProvider)ptCreditCard.ApiProvider;
                result.CardNumber = MarkCardNumber(authObject.CardNumber);
                result.InstallmentPeriod = authObject.InstallmentPeriod;
                result.OrderDate = DateTime.Now;
                result.TransPhase = ptCreditCard.Result.ToString() == "0" ? PayTransPhase.Successful : PayTransPhase.Failed;
                result.AuthenticationCode = ptCreditCard.AuthCode;
                result.TransMessage = ptCreditCard.Message;
                result.ReturnCode = ptCreditCard.Result.ToString() == "0" ? "00" : ptCreditCard.Result.ToString();
            }

            //若尚未非同步已主動查詢為主,有非同步以非同步為主
            if (result.ReturnCode == "00")
            {
                result.TransPhase = PayTransPhase.Successful;
                //這兩個透過非同步更新
                //result.TransMessage = ConvertMsg(jsonResult.Detail.RetMsg);
                //result.AuthenticationCode = jsonResult.Detail.AuthIdResp;

                #region 紀錄驗證OTP成功
                string creditcardInfo = authObject.CardNumber;
                Security sec = new Security(SymmetricCryptoServiceProvider.AES);
                string enCreditcard = sec.Encrypt(creditcardInfo);
                int userId = mp.MemberGet(userName).UniqueId;

                CreditcardOtp otp = op.CreditcardOtpGet(userId, enCreditcard);
                if (otp.IsLoaded)
                {
                    //更新驗證時間
                    otp.AuthTime = DateTime.Now;
                    op.CreditcardOtpSet(otp);
                }
                else
                {
                    //新增一筆
                    CreditcardOtp newOtp = new CreditcardOtp
                    {
                        UserId = userId,
                        CreditcardInfo = enCreditcard,
                        CreateTime = DateTime.Now,
                        AuthTime = DateTime.Now,
                    };
                    op.CreditcardOtpSet(newOtp);
                }
                #endregion
            }
            else if (result.ReturnCode == "ZP")
            {
                //訂單授權處理中
                logger.Error("OTPWarn:查詢訂單仍在處理中 transid=" + authObject.TransactionId);

            }

            if (null != result)
            {
                LogCreditcardOrder(authObject.CardNumber, userName, (authObject.ExpireMonth + "/" + authObject.ExpireYear),
                    result, orderGuid, authObject.Amount, type, (int)result.APIProvider, false, true);
            }

            return result;
        }

        /// <summary>
        /// 取消授權
        /// </summary>
        /// <param name="transactionID">訂單編號</param>
        /// <param name="authCode">授權碼</param>
        /// <returns></returns>
        public static bool AuthenticationReverse(string transactionID, string authCode = "")
        {
            PaymentAPIProvider apiProvider = LookUpAPIProvider(transactionID);
            ICreditcardPaymentProvider paymentProvider = CreditcardPaymentProviderContext.Get(apiProvider);

            return paymentProvider.AuthReverse(transactionID, authCode);
        }

        /// <summary>
        /// 信用卡取消授權 購物車版
        /// </summary>
        /// <param name="cartPt"></param>
        /// <returns></returns>
        public static bool AuthenticationReverse(CartPaymentTransaction cartPt, string authCode = "")
        {
            ICreditcardPaymentProvider paymentProvider = CreditcardPaymentProviderContext.Get((PaymentAPIProvider)cartPt.ApiProvider);
            return paymentProvider.AuthReverse(cartPt.MainTransId, authCode);
        }
        
        //TODO 待確認
        public static bool AuthenticationReverse(PaymentTransactionCollection ptc)
        {
            if (ptc.Count == 0)
            {
                return false;
            }
            string transId = ptc[0].CartTransId ?? ptc[0].TransId;
            string authCode= ptc[0].AuthCode;
            PaymentAPIProvider apiProvider = Helper.GetPaymentAPIProvider(ptc[0].ApiProvider);
            ICreditcardPaymentProvider paymentProvider = CreditcardPaymentProviderContext.Get(apiProvider);
            
            return paymentProvider.AuthReverse(transId, authCode);
        }

        /// <summary>
        /// 請款
        /// </summary>
        /// <param name="transactionID">訂單編號</param>
        /// <param name="amount">請款金額</param>
        /// <param name="authCode">授權碼</param>
        /// <returns></returns>
        public static bool Capture(string transactionID, int amount, string authCode)
        {
            PaymentAPIProvider apiProvider = LookUpAPIProvider(transactionID);
            ICreditcardPaymentProvider paymentProvider = CreditcardPaymentProviderContext.Get(apiProvider);

            return paymentProvider.Capture(transactionID, amount, authCode);
        }

        /// <summary>
        /// 取消請款
        /// </summary>
        /// <param name="transactionID">訂單編號</param>
        /// <param name="authCode">授權碼</param>
        /// <returns></returns>
        public static bool CaptureReverse(string transactionID, string authCode = "")
        {
            PaymentAPIProvider apiProvider = LookUpAPIProvider(transactionID);
            ICreditcardPaymentProvider paymentProvider = CreditcardPaymentProviderContext.Get(apiProvider);

            return paymentProvider.CaptureReverse(transactionID, authCode);
        }

        /// <summary>
        /// 退款
        /// </summary>
        /// <param name="transactionID">訂單編號</param>
        /// <param name="amount">退款金額</param>
        /// <param name="authCode">授權碼</param>
        /// <returns></returns>
        public static bool Refund(string transactionID, int amount, string authCode)
        {
            PaymentAPIProvider apiProvider = LookUpAPIProvider(transactionID);
            ICreditcardPaymentProvider paymentProvider = CreditcardPaymentProviderContext.Get(apiProvider);

            return paymentProvider.Refund(transactionID, amount, authCode);
        }

        /// <summary>
        /// 取消退款
        /// </summary>
        /// <param name="transactionID">訂單編號</param>
        /// <param name="authCode">授權碼</param>
        /// <returns></returns>
        public static bool RefundReverse(string transactionID, string authCode = "")
        {
            PaymentAPIProvider apiProvider = LookUpAPIProvider(transactionID);
            ICreditcardPaymentProvider paymentProvider = CreditcardPaymentProviderContext.Get(apiProvider);

            return paymentProvider.RefundReverse(transactionID, authCode);
        }

        /// <summary>
        /// 訂單查詢
        /// </summary>
        /// <param name="transactionID">訂單編號</param>
        /// <returns></returns>
        public static CreditCardQueryDetail Query(string transactionID)
        {
            PaymentAPIProvider apiProvider = LookUpAPIProvider(transactionID);
            ICreditcardPaymentProvider paymentProvider = CreditcardPaymentProviderContext.Get(apiProvider);
            CreditCardQueryDetail result = paymentProvider.Query(transactionID);

            return result;
        }

        /// <summary>
        /// 取得信用卡號編碼
        /// </summary>
        /// <param name="creditcardInfo">信用卡號</param>
        /// <returns></returns>
        private static string GetEncryptedCreditcardInfo(string creditcardInfo)
        {
            Security sec = new Security(SymmetricCryptoServiceProvider.AES);
            return sec.Encrypt(creditcardInfo);
        }

        /// <summary>
        /// 將信用卡交易資料記錄
        /// </summary>
        /// <param name="creditcardInfo"></param>
        /// <param name="userName"></param>
        /// <param name="expDate"></param>
        /// <param name="result"></param>
        /// <param name="orderGuid"></param>
        /// <param name="amount"></param>
        /// <param name="payType"></param>
        /// <param name="apiProvider"></param>
        /// <param name="isInBlacklist"></param>
        /// <param name="isOTP"></param>
        public static void LogCreditcardOrder(string creditcardInfo, string userName, string expDate, CreditCardAuthResult result,
            Guid orderGuid, int amount, OrderClassification payType, int apiProvider, bool isInBlacklist, bool isOTP)
        {
            int resCode = (int)CreditcardResult.UnDefinedBySelf;
            try
            {
                resCode = int.Parse(result.ReturnCode);
            }
            catch
            {
                resCode = -9527;
            }

            CreditcardOrder orderInfo = new CreditcardOrder();

            CreditcardOrderCollection oldOrders = op.GetCreditcardOrderByInfo(creditcardInfo);
            if (oldOrders != null && oldOrders.Count > 0)
            {
                orderInfo = oldOrders[0];
                orderInfo.IsNew = true;
            }
            else
            {
                orderInfo.CreditcardInfo = GetEncryptedCreditcardInfo(creditcardInfo);
            }

            if (isInBlacklist)
            {
                orderInfo.IsLocked = true;
                //加上信用卡無效次數
                orderInfo.CreditcardInvalidTimes = 1;
                creditcardInBlackList.Add(orderInfo.CreditcardInfo);
            }

            orderInfo.MemberId = mp.MemberGet(userName).UniqueId;
            orderInfo.OrderGuid = orderGuid;
            orderInfo.CreditcardAmount = amount;
            orderInfo.CreateTime = result.OrderDate;
            orderInfo.CreateId = userName;
            orderInfo.Result = resCode;
            orderInfo.IsOtp = isOTP;

            if ((result.APIProvider == PaymentAPIProvider.HiTrustContactWithCVV2 || result.APIProvider == PaymentAPIProvider.HiTrustContactWithoutCVV2) &&
                (resCode == (int)CreditcardResult.LossCard || resCode == (int)CreditcardResult.StolenCard || resCode == (int)CreditcardResult.PickupCard))
            {
                orderInfo.IsLocked = true;
                orderInfo.LockerId = "sys";
                orderInfo.LockedTime = DateTime.Now;

               
                if (orderInfo.Result == (int)CreditcardResult.Approve)
                {
                    orderInfo.Result = (int)CreditcardResult.OverQuantityWithin1Hr;
                }

            }

            #region 照會恢復預設

            orderInfo.IsReference = false;
            orderInfo.ReferenceId = null;
            orderInfo.ReferenceTime = null;

            #endregion 照會恢復預設

            orderInfo.ConsumerIp = Helper.GetClientIP();
            orderInfo.ExpDate = expDate;
            orderInfo.OrderClassification = (int)payType;
            orderInfo.ApiProvider = apiProvider;
            orderInfo.MaskedCardNumber = Helper.MaskCreditCard(creditcardInfo);
            op.SetCreditcardOrder(orderInfo);
        }
        static Security securityAESProvider = new Security(SymmetricCryptoServiceProvider.AES);
        public static string GetCreditcardOrderCardTail(Guid orderGuid)
        {
            var cdOrder = op.CreditcardOrderGetByOrderGuid(orderGuid);
            if (cdOrder.IsLoaded)
            {
                string cardNumber = DecryptCardNumber(cdOrder.CreditcardInfo);
                if (cardNumber != null && cardNumber.Length > 4)
                {
                    return cardNumber.Substring(cardNumber.Length - 4);
                }
            }
            return null;
        }
        public static string DecryptCardNumber(string cardNumber)
        {
            return securityAESProvider.Decrypt(cardNumber);
        }

        internal static void AddCreditcardOrderByAntiCreditcardFraud(string creditcardInfo, string creditcardType, string strUserId)
        {
            CreditCardAuthResult result=new CreditCardAuthResult();
            result.APIProvider = PaymentAPIProvider.Mock;
            Guid guid = new Guid();
            guid = Guid.Parse("00000000-0000-0000-0000-999999999999");
            CreditcardOrder orderInfo = new CreditcardOrder();
            orderInfo.MemberId = mp.MemberGet(strUserId).UniqueId;
            orderInfo.OrderGuid = guid;
            orderInfo.OrderClassification = 1;
            orderInfo.CreditcardAmount = 0;
            orderInfo.ExpDate = "";
            orderInfo.Result = int.Parse(creditcardType);
            orderInfo.ConsumerIp = Helper.GetClientIP();
            orderInfo.CreateId = config.SystemEmail;
            orderInfo.CreateTime = DateTime.Now;
            orderInfo.IsLocked = true;
            orderInfo.LockerId = config.SystemEmail;
            orderInfo.LockedTime = DateTime.Now;
            orderInfo.ApiProvider = (int)result.APIProvider;
            orderInfo.CreditcardInfo = GetEncryptedCreditcardInfo(creditcardInfo);
            op.SetCreditcardOrder(orderInfo);
        }

        private static string GetResult(string cardNumber)
        {
            CreditcardOrder orderInfo = new CreditcardOrder();
            CreditcardOrderCollection creditcarOrders = op.GetCreditcardOrderByInfo(cardNumber);
            CreditcardProcessor cp = new CreditcardProcessor();
            string result = cp.GetResultName(creditcarOrders[0].Result);
            return result;
        }

        public static bool CheckBlackList(string cardNumber)
        {
            string creditcardInfo = GetEncryptedCreditcardInfo(cardNumber);

            //if (null == creditcardInBlackList)
            //{
            creditcardInBlackList = null;
            LoadBlackList();
            //}

            return creditcardInBlackList.Contains(creditcardInfo);
        }

        private static void LoadBlackList()
        {
            creditcardInBlackList = new HashSet<string>();
            foreach (CreditcardOrder card in op.GetCreditcardOrderBlackList())
            {
                if (creditcardInBlackList.Contains(card.CreditcardInfo) == false)
                {
                    creditcardInBlackList.Add(card.CreditcardInfo);
                }
            }
        }

        private static PaymentAPIProvider LookUpAPIProvider(string transactionID)
        {
            #region Test

            if (transactionID.IndexOf("htest") >= 0)
            {
                return PaymentAPIProvider.HiTrustDotNetTest;
            }

            if (transactionID.IndexOf("ntest") >= 0)
            {
                return PaymentAPIProvider.NewebTest;
            }

            #endregion Test

            PaymentTransaction pt = op.PaymentTransactionGet(transactionID, LunchKingSite.Core.PaymentType.Creditcard, PayTransType.Authorization);

            if (null != pt && pt.IsLoaded)
            {
                return Helper.GetPaymentAPIProvider(pt.ApiProvider);
            }

            return defaultProvider;
        }

        private static bool CheckUnionPayBin(string cardNumber)
        {
            string[] unionPayBins = config.UnionPayBinPatterns.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string pattern in unionPayBins)
            {
                if (int.Equals(0, cardNumber.IndexOf(pattern)))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 檢查是否為台新卡
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        private static bool CheckBankCard(string cardNumber, TaishinECPayCreditCardCheck cardBank)
        {
            string bin = cardNumber.Substring(0, 6);
            var bankInfo = op.CreditcardBankInfoGetByBin(bin);
            return bankInfo != null && bankInfo.BankId == (int)cardBank;
        }

        /// <summary>
        /// 若是黑名單卡的話，直接跳過刷卡階段，取得result的設定
        /// </summary>
        /// <param name="cardNumber">信用卡號</param>
        /// <returns>CreditCardAuthResult</returns>
        private static CreditCardAuthResult GetBlackListCardResult(string cardNumber,string result)
        {
            return new CreditCardAuthResult()
            {
                APIProvider = PaymentAPIProvider.ForgionCard,
                CardNumber = MarkCardNumber(cardNumber),
                TransPhase = PayTransPhase.Failed,
                OrderDate = DateTime.Now,
                ReturnCode = ((int)CreditcardResult.IsBlacklist).ToString(),//交易之後會在此處將信用卡的狀態壓成『17Life - 設定黑名單』，如果Esther要求此處要保留當初建立的狀態，那就把它改成帶入result參數就可以了
                TransMessage = "17Life黑名單("+result+")"/*"被設定為黑名單卡號，直接跳過刷卡階段"*/,
                AuthenticationCode = ((int)CreditcardResult.IsBlacklist).ToString(),
                BuyTimes = 1
            };
        }

        private static CreditCardAuthResult GetFailResult(PaymentAPIProvider apiProvider, string cardNumber, string message)
        {
            return new CreditCardAuthResult()
            {
                APIProvider = apiProvider,
                CardNumber = MarkCardNumber(cardNumber),
                TransPhase = PayTransPhase.Failed,
                OrderDate = DateTime.Now,
                TransMessage = message
            };
        }

        private static string MarkCardNumber(string cardNumber)
        {
            if (int.Equals(16, cardNumber.Length))
            {
                return cardNumber.Substring(0, 6) + "******" + cardNumber.Substring(12, 4);
            }
            else
            {
                return string.Empty;
            }
        }

        private static PaymentAPIProvider DetermineAPIProvider(CreditCardOrderSource orderSource, CreditCardAuthObject authObject, bool isOTP,Guid? bid = null)
        {
            if (config.EnabledCathayPaymnetGatewayTest && bid.HasValue && bid.Value.ToString() == config.CathayPaymnetTestBid)
            {
                if (authObject.InstallmentPeriod > 0)
                {
                    return isOTP ? PaymentAPIProvider.CathayPaymnetInstallmentWithOtp : PaymentAPIProvider.CathayPaymnetInstallment;
                }
                if (isOTP)
                {
                    return PaymentAPIProvider.CathayPaymnetOTPGateway;
                }
                else
                {
                    return PaymentAPIProvider.CathayPaymnetGateway;
                }
            }
            
            #region Test

            if (Enum.Equals(CreditCardOrderSource.HiTrustComTest, orderSource))
            {
                return PaymentAPIProvider.HiTrustComTest;
            }

            if (Enum.Equals(CreditCardOrderSource.HiTrustDotNetTest, orderSource))
            {
                return PaymentAPIProvider.HiTrustDotNetTest;
            }

            if (Enum.Equals(CreditCardOrderSource.NewebTest, orderSource))
            {
                return PaymentAPIProvider.NewebTest;
            }

            if (orderSource == CreditCardOrderSource.Mock)
            {
                return PaymentAPIProvider.Mock;
            }

            #endregion Test

            #region 台新&國泰

            if (CheckBankCard(authObject.CardNumber, TaishinECPayCreditCardCheck.Taishin) && config.EnabledTaiShinPaymnetGateway)
            {
                return isOTP ? PaymentAPIProvider.TaishinPaymnetOTPGateway : PaymentAPIProvider.TaishinPaymnetGateway;
            }
            else if (CheckBankCard(authObject.CardNumber, TaishinECPayCreditCardCheck.Cathay) && config.EnabledCathayPaymnetGateway)
            {
                if (authObject.InstallmentPeriod >= (int)OrderInstallment.In3Months)
                {
                    return isOTP ? PaymentAPIProvider.CathayPaymnetInstallmentWithOtp : PaymentAPIProvider.CathayPaymnetInstallment;
                }
                return isOTP ? PaymentAPIProvider.CathayPaymnetOTPGateway : PaymentAPIProvider.CathayPaymnetGateway;
            }

            if (isOTP)
            {
                if (config.EnabledTaiShinPaymnetGateway)
                {
                    return PaymentAPIProvider.TaishinPaymnetOTPGateway;

                }
                else if (config.EnabledCathayPaymnetGateway)
                {
                    return PaymentAPIProvider.CathayPaymnetOTPGateway;
                }
            }

            #endregion 台新&國泰

            #region 銀聯

            // 銀聯目前只有 HiTrust 支援
            if (CheckUnionPayBin(authObject.CardNumber))
            {
                if (Enum.Equals(CreditCardOrderSource.Ppon, orderSource))
                {
                    return PaymentAPIProvider.HiTrustUnionPay;
                }

                if (Enum.Equals(CreditCardOrderSource.PiinLife, orderSource))
                {
                    return PaymentAPIProvider.HiTrustPiinLifeUnionPay;
                }
            }

            #endregion 銀聯

            PaymentAPIProvider result;
            if (Enum.IsDefined(typeof(PaymentAPIProvider), config.CreditCardAPIProvider))
            {
                result = (PaymentAPIProvider)Enum.Parse(typeof(PaymentAPIProvider), config.CreditCardAPIProvider);
            }
            else
            {
                result = defaultProvider;
            }

            // 依系統設定比例決定服務廠商
            if (result == PaymentAPIProvider.TaishinPaymnetGateway || result == PaymentAPIProvider.Neweb)
            {
                result = GetProviderByRatio(orderSource, authObject.InstallmentPeriod>=(int)OrderInstallment.In3Months);
            }

            return result;
        }

        /// <summary>
        /// 依系統設定比例決定服務廠商
        /// </summary>
        /// <param name="orderSource"></param>
        /// <param name="isInstallment"></param>
        /// <returns></returns>
        private static PaymentAPIProvider GetProviderByRatio(CreditCardOrderSource orderSource, bool isInstallment)
        {
            IList<string> sampleSpace = new List<string>();
            string[] ratio = config.CreditCardAPIProviderRatio.Split(':');
            int tspg = Convert.ToInt16(ratio[0]);
            int neweb = Convert.ToInt16(ratio[1]);

            // TSPG
            for (int i = 0; i < tspg; i++)
            {
                if (config.EnabledTaiShinPaymnetGateway)
                {
                    sampleSpace.Add("TaishinPaymnetGateway");
                }
                else if (config.EnabledCathayPaymnetGateway)
                {
                    sampleSpace.Add("CathayPaymnetGateway");
                }
            }

            // Neweb
            for (int i = 0; i < neweb; i++)
            {
                sampleSpace.Add("Neweb");
            }

            

            if (sampleSpace.Count > 0)
            {
                string temp = sampleSpace[new Random().Next(sampleSpace.Count)];

                switch (temp)
                {
                    case "Neweb":
                        if (isInstallment)
                        {
                            return PaymentAPIProvider.NewebContactInstallment;
                        }
                        if (Enum.Equals(CreditCardOrderSource.Ppon, orderSource))
                        {
                            return PaymentAPIProvider.NewebContactWithCVV2;
                        }

                        if (Enum.Equals(CreditCardOrderSource.PiinLife, orderSource))
                        {
                            return PaymentAPIProvider.NewebContactWithoutCVV2;
                        }
                        break;
                    case "TaishinPaymnetGateway":
                        return PaymentAPIProvider.TaishinPaymnetGateway;
                    case "CathayPaymnetGateway":
                        return PaymentAPIProvider.CathayPaymnetGateway;
                    default:
                        break;
                }
            }

            return defaultProvider;
        }

        #endregion Utility Methods

        #region Provider section

        #region HiTrust API

        private static CreditCardAuthResult HiTrustAuth(CreditCardAuthObject authObject, string merchantConfigName, string serverConfigName, string merchantId, bool isUPOP)
        {
            CreditCardAuthResult result = new CreditCardAuthResult();

            if (isUPOP)
            {
                #region UPOP

                UPOPPayAuth b2cpay = new UPOPPayAuth()
                {
                    StoreId = merchantId,
                    OrderNo = authObject.TransactionId,
                    Amount = authObject.Amount.ToString() + "00",
                    Pan = authObject.CardNumber,
                    Expiry = authObject.ExpireYear + authObject.ExpireMonth,
                    OrderDesc = string.IsNullOrWhiteSpace(authObject.Description)
                                        ? string.Empty
                                        : authObject.Description.Length > 40 ? authObject.Description.Substring(0, 40) : authObject.Description,
                    E01 = authObject.SecurityCode,
                    E03 = authObject.InstallmentPeriod.ToString(),
                    E14 = authObject.CardHolderId
                };

                b2cpay.transaction();

                result.APIProvider = authObject.APIProvider;
                result.CardNumber = MarkCardNumber(authObject.CardNumber);
                result.ReturnCode = b2cpay.RetCode;
                result.TransMessage = GetHiTrustDesc(b2cpay.RetCode);
                result.AuthenticationCode = b2cpay.AuthCode;
                result.TransPhase = (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode))
                                        ? PayTransPhase.Successful
                                        : PayTransPhase.Failed;
                result.InstallmentPeriod = authObject.InstallmentPeriod;
                if (!string.IsNullOrWhiteSpace(b2cpay.OrderDate))
                {
                    result.OrderDate = DateTime.ParseExact(b2cpay.OrderDate.Substring(0, hitrustDatetimeFormat.Length), hitrustDatetimeFormat, CultureInfo.InvariantCulture);
                }

                #endregion UPOP
            }
            else
            {
                #region Non UPOP

                B2CPayAuthSSL b2cpay = new B2CPayAuthSSL()
                {
                    StoreId = merchantId,
                    OrderNo = authObject.TransactionId,
                    Amount = authObject.Amount.ToString() + "00",
                    Pan = authObject.CardNumber,
                    Expiry = authObject.ExpireYear + authObject.ExpireMonth,
                    OrderDesc = string.IsNullOrWhiteSpace(authObject.Description)
                                         ? string.Empty
                                         : authObject.Description.Length > 40 ? authObject.Description.Substring(0, 40) : authObject.Description,
                    E01 = authObject.SecurityCode,
                    E03 = authObject.InstallmentPeriod.ToString(),
                    E14 = authObject.CardHolderId
                };

                b2cpay.transaction();

                result.APIProvider = authObject.APIProvider;
                result.CardNumber = MarkCardNumber(authObject.CardNumber);
                result.AuthenticationCode = b2cpay.AuthCode;
                result.TransPhase = (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode))
                                        ? PayTransPhase.Successful
                                        : PayTransPhase.Failed;
                result.InstallmentPeriod = authObject.InstallmentPeriod;
                if (!string.IsNullOrWhiteSpace(b2cpay.OrderDate))
                {
                    result.OrderDate = DateTime.ParseExact(b2cpay.OrderDate.Substring(0, hitrustDatetimeFormat.Length), hitrustDatetimeFormat, CultureInfo.InvariantCulture);
                }

                #endregion Non UPOP
            }

            LogAuth(authObject, result);

            return result;
        }

        private static bool HiTrustAuthReverse(string transactionID, string merchantConfigName, string serverConfigName, string merchantId, bool isUPOP)
        {
            if (isUPOP)
            {
                #region UPOP

                UPOPPayOther b2cpay = new UPOPPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.RE_AUTH,
                    OrderNo = transactionID
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                return (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode));

                #endregion UPOP
            }
            else
            {
                #region Non UPOP

                B2CPayOther b2cpay = new B2CPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.RE_AUTH,
                    OrderNo = transactionID
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                return (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode));

                #endregion Non UPOP
            }
        }

        private static bool HiTrustCapture(string transactionID, int amount, string merchantConfigName, string serverConfigName, string merchantId, bool isUPOP)
        {
            if (isUPOP)
            {
                #region UPOP

                UPOPPayOther b2cpay = new UPOPPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.CAPTURE,
                    OrderNo = transactionID,
                    Amount = amount.ToString() + "00"
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                return (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode));

                #endregion UPOP
            }
            else
            {
                #region Non UPOP

                B2CPayOther b2cpay = new B2CPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.CAPTURE,
                    OrderNo = transactionID,
                    Amount = amount.ToString() + "00"
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                return (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode));

                #endregion Non UPOP
            }
        }

        private static bool HiTrustCaptureReverse(string transactionID, string merchantConfigName, string serverConfigName, string merchantId, bool isUPOP)
        {
            if (isUPOP)
            {
                #region UPOP

                UPOPPayOther b2cpay = new UPOPPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.RE_CAPT,
                    OrderNo = transactionID
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                return (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode));

                #endregion UPOP
            }
            else
            {
                #region Non UPOP

                B2CPayOther b2cpay = new B2CPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.RE_CAPT,
                    OrderNo = transactionID
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                return (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode));

                #endregion Non UPOP
            }
        }

        private static bool HiTrustRefund(string transactionID, int amount, string merchantConfigName, string serverConfigName, string merchantId, bool isUPOP)
        {
            if (isUPOP)
            {
                #region UPOP

                UPOPPayOther b2cpay = new UPOPPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.REFUND,
                    OrderNo = transactionID,
                    Amount = amount.ToString() + "00"
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                return (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode));

                #endregion UPOP
            }
            else
            {
                #region Non UPOP

                B2CPayOther b2cpay = new B2CPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.REFUND,
                    OrderNo = transactionID,
                    Amount = amount.ToString() + "00"
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                return (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode));

                #endregion Non UPOP
            }
        }

        private static bool HiTrustRefundReverse(string transactionID, string merchantConfigName, string serverConfigName, string merchantId, bool isUPOP)
        {
            if (isUPOP)
            {
                #region UPOP

                UPOPPayOther b2cpay = new UPOPPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.RE_REFU,
                    OrderNo = transactionID
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                return (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode));

                #endregion UPOP
            }
            else
            {
                #region Non UPOP

                B2CPayOther b2cpay = new B2CPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.RE_REFU,
                    OrderNo = transactionID
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                return (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode));

                #endregion Non UPOP
            }
        }

        private static CreditCardQueryDetail HiTrustQuery(string transactionID, string merchantConfigName, string serverConfigName, string merchantId, bool isUPOP)
        {
            if (isUPOP)
            {
                #region UPOP

                UPOPPayOther b2cpay = new UPOPPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.QUERY,
                    OrderNo = transactionID
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                if (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode))
                {
                    CreditCardQueryDetail detail = new CreditCardQueryDetail();
                    detail.ApproveAmount = string.IsNullOrWhiteSpace(b2cpay.ApproveAmount) ? -1 : int.Parse(b2cpay.ApproveAmount) / 100;
                    detail.AuthCode = b2cpay.AuthCode;
                    detail.AuthRRN = b2cpay.AuthRRN;
                    if (!string.IsNullOrWhiteSpace(b2cpay.CaptureDate))
                    {
                        detail.CaptureDate = DateTime.ParseExact(b2cpay.CaptureDate.Substring(0, hitrustDatetimeFormat.Length), hitrustDatetimeFormat, CultureInfo.InvariantCulture);
                    }
                    detail.CaptureAmount = string.IsNullOrWhiteSpace(b2cpay.CaptureAmount) ? -1 : int.Parse(b2cpay.CaptureAmount) / 100;
                    detail.Currency = b2cpay.Currency;
                    detail.ECI = b2cpay.Eci;
                    if (!string.IsNullOrWhiteSpace(b2cpay.OrderDate))
                    {
                        detail.OrderDate = DateTime.ParseExact(b2cpay.OrderDate.Substring(0, hitrustDatetimeFormat.Length), hitrustDatetimeFormat, CultureInfo.InvariantCulture);
                    }
                    detail.OrderStatus = b2cpay.OrderStatus;
                    detail.PayBatchNumber = b2cpay.PayBatchNum;
                    detail.RefundAmount = string.IsNullOrWhiteSpace(b2cpay.RefundAmount) ? -1 : int.Parse(b2cpay.RefundAmount) / 100;
                    detail.RefundBatchNumber = b2cpay.RefundBatch;
                    detail.RefundCode = b2cpay.RefundCode;
                    if (!string.IsNullOrWhiteSpace(b2cpay.RefundDate))
                    {
                        detail.RefundDate = DateTime.ParseExact(b2cpay.RefundDate.Substring(0, hitrustDatetimeFormat.Length), hitrustDatetimeFormat, CultureInfo.InvariantCulture);
                    }
                    detail.RefundRRN = b2cpay.RefundRRN;
                    detail.ReturnCode = b2cpay.RetCode;
                    detail.InstallmentPeriod = string.IsNullOrWhiteSpace(b2cpay.E06) ? 0 : int.Parse(b2cpay.E06);

                    return detail;
                }
                else
                {
                    return null;
                }

                #endregion UPOP
            }
            else
            {
                #region Non UPOP

                B2CPayOther b2cpay = new B2CPayOther()
                {
                    StoreId = merchantId,
                    Type = B2CPay.QUERY,
                    OrderNo = transactionID
                };

                b2cpay.transaction();

                LogAction(transactionID, b2cpay.RetCode);

                if (string.Equals("0", b2cpay.RetCode) || string.Equals("00", b2cpay.RetCode))
                {
                    CreditCardQueryDetail detail = new CreditCardQueryDetail();
                    detail.ApproveAmount = string.IsNullOrWhiteSpace(b2cpay.ApproveAmount) ? -1 : int.Parse(b2cpay.ApproveAmount) / 100;
                    detail.AuthCode = b2cpay.AuthCode;
                    detail.AuthRRN = b2cpay.AuthRRN;
                    if (!string.IsNullOrWhiteSpace(b2cpay.CaptureDate))
                    {
                        detail.CaptureDate = DateTime.ParseExact(b2cpay.CaptureDate.Substring(0, hitrustDatetimeFormat.Length), hitrustDatetimeFormat, CultureInfo.InvariantCulture);
                    }
                    detail.CaptureAmount = string.IsNullOrWhiteSpace(b2cpay.CaptureAmount) ? -1 : int.Parse(b2cpay.CaptureAmount) / 100;
                    detail.Currency = b2cpay.Currency;
                    detail.ECI = b2cpay.Eci;
                    if (!string.IsNullOrWhiteSpace(b2cpay.OrderDate))
                    {
                        detail.OrderDate = DateTime.ParseExact(b2cpay.OrderDate.Substring(0, hitrustDatetimeFormat.Length), hitrustDatetimeFormat, CultureInfo.InvariantCulture);
                    }
                    detail.OrderStatus = b2cpay.OrderStatus;
                    detail.PayBatchNumber = b2cpay.PayBatchNum;
                    detail.RefundAmount = string.IsNullOrWhiteSpace(b2cpay.RefundAmount) ? -1 : int.Parse(b2cpay.RefundAmount) / 100;
                    detail.RefundBatchNumber = b2cpay.RefundBatch;
                    detail.RefundCode = b2cpay.RefundCode;
                    if (!string.IsNullOrWhiteSpace(b2cpay.RefundDate))
                    {
                        detail.RefundDate = DateTime.ParseExact(b2cpay.RefundDate.Substring(0, hitrustDatetimeFormat.Length), hitrustDatetimeFormat, CultureInfo.InvariantCulture);
                    }
                    detail.RefundRRN = b2cpay.RefundRRN;
                    detail.ReturnCode = b2cpay.RetCode;
                    detail.InstallmentPeriod = string.IsNullOrWhiteSpace(b2cpay.E06) ? 0 : int.Parse(b2cpay.E06);

                    return detail;
                }
                else
                {
                    return null;
                }

                #endregion Non UPOP
            }
        }

        private static string GetHiTrustDesc(string retCode)
        {
            switch (retCode)
            {
                // from HiTrust
                case "0":
                    return "ByPass";

                case "00":
                    return "Success";

                case "-1":
                    return "Unable to initialize winsock dll.";

                case "-2":
                    return "Can't create stream socket.";

                case "-3":
                    return "No Request Message.";

                case "-4":
                    return "Can't connect to server.";

                case "-5":
                    return "Send socket error.";

                case "-6":
                    return "Couldn't receive data.";

                case "-7":
                    return "Receive Broken message.";

                case "-8":
                    return "Unable to initialize Environment.";

                case "-9":
                    return "Can't Read Server RSA File.";

                case "-10":
                    return "Can't Read Client RSA File.";

                case "-11":
                    return "Web Server error.";

                case "-12":
                    return "Receive Message type error.";

                case "-13":
                    return "No Request Message.";

                case "-14":
                    return "No Response Content.";

                case "-15":
                    return "Connection timeout.";

                case "-16":
                    return "Encrypt data error.";

                case "-17":
                    return "Decrypt data error.";

                case "-18":
                    return "Merchant Update URL not found.";

                case "-19":
                    return "Server URL not find Domain or IP.";

                case "-20":
                    return "Server URL only can fill http or https.";

                case "-21":
                    return "Server Config File open error.";

                case "-22":
                    return "Server RSA Key File open error.";

                case "-23":
                    return "Server RSA Key File read error.";

                case "-24":
                    return "Server Config File have some errors, Please to check it.";

                case "-25":
                    return "Merchant Config File open error.";

                case "-26":
                    return "Merchant RSA Key File open error.";

                case "-27":
                    return "Merchant RSA Key File read error.";

                case "-28":
                    return "Merchant Config File have some errors, Please to check it.";

                case "-29":
                    return "Server Type is unknown.";

                case "-30":
                    return "Comm Type is unknown.";

                case "-31":
                    return "Input Parameter [ORDERNO] is null or empty.";

                case "-32":
                    return "Input Parameter [STOREID] is null or empty.";

                case "-33":
                    return "Input Parameter [ORDERDESC] is null or empty.";

                case "-34":
                    return "Input Parameter [CURRENCY] is null or empty.";

                case "-35":
                    return "Input Parameter [AMOUNT] is null or empty.";

                case "-36":
                    return "Input Parameter [ORDERURL] is null or empty.";

                case "-37":
                    return "Input Parameter [RETURNURL] is null or empty.";

                case "-38":
                    return "Input Parameter [DEPOSIT] is null or empty.";

                case "-39":
                    return "Input Parameter [QUERYFLAG] is null or empty.";

                case "-40":
                    return "Input Parameter [UPDATEURL] is null or empty.";

                case "-41":
                    return "Input Parameter [MERUPDATEURL] is null or empty.";

                case "-42":
                    return "Input Parameter [KEY] is null or empty.";

                case "-43":
                    return "Input Parameter [MAC] is null or empty.";

                case "-44":
                    return "Input Parameter [CIPHER] is null or empty.";

                case "-45":
                    return "Input Parameter [TrxType] is wrong.";

                case "-47":
                    return "Log Config File Open Error.";

                case "-100":
                    return "TrustLink Server is closed. Or Local IP is not consistent with TrustLink Server setting.";

                case "-101":
                    return "TrustLink Server receives NULL.";

                case "-308":
                    return "Order Number already exists.";

                case "O4":
                case "O6":
                    return "拒絕交易，建議持卡人詢問發卡行";

                case "N7":
                    return "末三碼錯誤或是不正確";

                case "T5":
                    return "未開卡";

                case "Z5":
                    return "系統裡等待的交易超過上限，後面進來的有些會直接被拒絕";

                case "ZY":
                    return "此回應代碼表示銀行授權回覆時間過久(超過系統預定時間)，系統先行回覆ZY。交易逾時訂單系統會直接送出取消授權，此對應碼之訂單為失敗訂單";

                case "ZU":
                    return "系統進行商家交易清算時，會鎖住商家交易狀態，此時進來的交易會回覆ZU. 清算時間僅3-5分鐘，請消費者稍後再做交易。";
                // from Bank
                case "01":
                    return "請查詢發卡銀行 (當天交易筆數過多，或是金額過高)";

                case "02":
                    return "請查詢發卡銀行";

                case "03":
                    return "商店代號錯誤，再次確與系統管理櫃台上設定的MID/TID與銀行提供是否相符。如相同，需與銀行聯絡。";

                case "04":
                    return "請查詢銀行";

                case "05":
                    return "請查詢銀行，此為常見錯誤訊息，可能原因為 超額/管制/效期錯";

                case "06":
                    return "請查詢銀行";

                case "07":
                    return "掛失卡";

                case "08":
                    return "特定狀況之核准";

                case "11":
                    return "商家與收單銀行簽約的授信額度已滿，無法繼續交易，訂單會於收單銀行先擋住，不會轉向發卡銀行。需請商家向收單銀行提出授信額度提升的申請，待提升後，未授權之訂單再重新取授權即可。";

                case "12":
                    return "無效交易（常發生於3D驗證失敗）";

                case "13":
                    return "金額錯誤，常發生在請款時，請款金額大於授權金額";

                case "14":
                    return "卡號錯誤";

                case "15":
                    return "無此發卡行，請查詢銀行";

                case "19":
                    return "重新輸入/稍後再試";

                case "30":
                    return "格式錯誤，請查詢銀行(CVV錯誤/無效卡)";

                case "31":
                    return "請查詢銀行。(發卡行系統維護)";

                case "33":
                    return "過期卡";

                case "36":
                    return "不正常卡";

                case "38":
                    return "密碼錯誤次數已滿";

                case "39":
                    return "無此卡號";

                case "41":
                    return "掛失卡-遺失卡";

                case "43":
                    return "掛失卡-被竊卡";

                case "51":
                    return "婉拒交易-超額";

                case "52":
                    return "卡號未尚未成立";

                case "53":
                    return "無此(卡/帳)號";

                case "54":
                    return "過期卡/有效日期錯誤";

                case "55":
                    return "PIN 碼錯誤，包含 Debit card";

                case "57":
                    return "不核準，請查詢銀行";

                case "58":
                    return "不核準，端未機問題";

                case "61":
                    return "不核準，超額";

                case "62":
                    return "管制卡(未開卡/管制)";

                case "65":
                    return "不核準，超額";

                case "75":
                    return "PIN輸入超次限，包含Debit card";

                case "87":
                    return "發卡行對持卡人執行拒絕交易的訊息，通常是刷卡金額過大、無法於EC上使用等需請持卡人回電發卡銀行詢問原因";

                case "89":
                    return "請查詢銀行，常見原因為銀行尚未開通 Merchant Terminal ID";

                case "91":
                    return "系統忙線中，通常發生於銀行端沒回應";

                case "96":
                    return "系統異常，通常發生於發卡行當下系統在整理，導致無法給予授權碼";

                case "97":
                    return "紅利點數不足";

                case "P1":
                case "P4":
                case "P6":
                case "P9":
                case "T4":
                    return "請與發卡銀行聯絡 (Call Bank)";

                case "N1":
                case "N2":
                case "N3":
                case "N4":
                case "N5":
                case "N6":
                case "N8":
                case "N9":
                case "O0":
                case "O1":
                case "O2":
                case "O3":
                case "O5":
                case "O7":
                case "O8":
                case "O9":
                case "P0":
                case "P2":
                case "P3":
                case "P5":
                case "P7":
                case "P8":
                case "Q0":
                case "Q1":
                case "Q2":
                case "Q3":
                case "Q4":
                case "Q6":
                case "Q7":
                case "Q8":
                case "Q9":
                case "R0":
                case "R1":
                case "R2":
                case "R3":
                case "R4":
                case "R5":
                case "R6":
                case "R7":
                case "R8":
                case "S4":
                case "S5":
                case "S6":
                case "S7":
                case "S8":
                case "S9":
                case "A3":
                case "A9":
                    return "拒絕交易 (Decline) ";

                case "Q5":
                    return "沒收卡片 (Pickup) ";

                case "T2":
                    return "交易日期錯誤";

                case "AY":
                    return "交易逾時(  發卡行之授權訊息無法於規定之時間內(25  秒)回覆至授權主機 ) ";

                case "AD":
                    return "分期交易之分期數不對";

                case "AE":
                    return "(發卡行回覆之分期資料有誤) ";

                case "AI":
                    return "紅利資料錯誤";

                case "AH":
                    return "特店針對此卡別之分期交易已過期限";

                case "AG":
                    return "特店針對此卡別無分期交易之功能";

                case "AK":
                    return "特店針對此卡別之 交易已過期限";

                case "AL":
                    return "特店針對此卡別無紅利交易之功能";

                default:
                    return "Unkown";
            }
        }

        #endregion HiTrust API

        #region Neweb

        private static CreditCardAuthResult NewebAuth(CreditCardAuthObject authObject, string userID, string userPassword, string merchantID, string acceptServer, string adminServer)
        {
            NameValueCollection parameters = new NameValueCollection(){
                    {"PaymentType", "SSL"},
                    {"OrderNumber", authObject.TransactionId},
                    {"OrgOrderNumber", authObject.TransactionId},
                    {"Amount", authObject.Amount.ToString()},
                    {"Currency", "901"},
                    {"CardNumber", authObject.CardNumber},
                    {"CardExpiry", "20" + authObject.ExpireYear + authObject.ExpireMonth},
                    {"ApproveFlag", "1"},
                    {"Period", authObject.InstallmentPeriod.ToString()},
                    {"ID", authObject.CardHolderId},
                    {"userid", userID},
                    {"passwd", userPassword},
                    {"MerchantNumber", merchantID},
                    {"DepositFlag", "0"}
            };

            // 2014-10-23 決議 MasterPass 的交易都走 piinlife 的特店，因 MasterPass 不回傳後三碼，所以不傳後三碼
            if (!(Enum.Equals(PaymentAPIProvider.NewebPiinLife, authObject.APIProvider) ||
                Enum.Equals(PaymentAPIProvider.NewebContactWithoutCVV2, authObject.APIProvider))
                )
            {
                parameters.Add("CVC2", authObject.SecurityCode);
            }

            NameValueCollection nvc = GetNewebResult(acceptServer, parameters);

            CreditCardAuthResult result = new CreditCardAuthResult();
            result.APIProvider = authObject.APIProvider;
            result.CardNumber = MarkCardNumber(authObject.CardNumber);
            result.TransPhase = PayTransPhase.Failed;
            result.OrderDate = DateTime.Now;
            result.InstallmentPeriod = authObject.InstallmentPeriod;
            if (nvc.Count > 0)
            {
                #region generate result

                result.ReturnCode = nvc["PRC"];
                result.TransMessage = GetNewebMessage(nvc["PRC"], nvc["SRC"]);
                result.AuthenticationCode = nvc["APPROVALCODE"];
                result.TransPhase = (string.Equals("0", nvc["PRC"]))
                                            ? PayTransPhase.Successful
                                            : PayTransPhase.Failed;

                #endregion generate result
            }

            LogAuth(authObject, result);

            return result;
        }

        private static bool NewebAuthReverse(string transactionID, string userID, string userPassword, string merchantID, string adminServer)
        {
            NameValueCollection result = GetNewebResult(adminServer,
                new NameValueCollection(){
                    {"OrderNumber", transactionID},
                    {"operation", "ApproveReversal"},
                    {"userid", userID},
                    {"passwd", userPassword},
                    {"MerchantNumber", merchantID}
                    });

            LogAction(transactionID, result["PRC"] + "|" + result["SRC"]);

            return (result.Count > 0 && string.Equals("0", result["PRC"]));
        }

        private static bool NewebCapture(string transactionID, int amount, string userID, string userPassword, string merchantID, string adminServer)
        {
            NameValueCollection result = GetNewebResult(adminServer, new NameValueCollection(){
                {"OrderNumber", transactionID},
                {"Amount", amount.ToString()},
                {"operation", "Deposit"},
                {"userid", userID},
                {"passwd", userPassword},
                {"MerchantNumber", merchantID}
                });

            LogAction(transactionID, result["PRC"] + "|" + result["SRC"]);

            return (result.Count > 0 && string.Equals("0", result["PRC"]));
        }

        private static bool NewebCaptureReverse(string transactionID, string userID, string userPassword, string merchantID, string adminServer)
        {
            NameValueCollection result = GetNewebResult(adminServer, new NameValueCollection(){
                {"OrderNumber", transactionID},
                {"operation", "DepositReversal"},
                {"userid", userID},
                {"passwd", userPassword},
                {"MerchantNumber", merchantID}
                });

            LogAction(transactionID, result["PRC"] + "|" + result["SRC"]);

            return (result.Count > 0 && string.Equals("0", result["PRC"]));
        }

        private static bool NewebRefund(string transactionID, int amount, string userID, string userPassword, string merchantID, string adminServer)
        {
            NameValueCollection result = GetNewebResult(adminServer, new NameValueCollection(){
                {"OrderNumber", transactionID},
                {"Amount", amount.ToString()},
                {"operation", "Refund"},
                {"userid", userID},
                {"passwd", userPassword},
                {"MerchantNumber", merchantID}
                });

            LogAction(transactionID, result["PRC"] + "|" + result["SRC"]);

            if ((result.Count > 0 && string.Equals("0", result["PRC"])))
            {
                CreditcardRefundRecord crr = new CreditcardRefundRecord()
                {
                    TransId = transactionID,
                    ApiProvider = (int)PaymentAPIProvider.Neweb,
                    RefundReturnNumber = result["CreditNumber"],
                    Message = GetNewebMessage(result["PRC"], result["SRC"])
                };
                op.CreditcardRefundRecordSet(crr);

                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool NewebRefundReverse(string transactionID, string userID, string userPassword, string merchantID, string adminServer)
        {
            CreditcardRefundRecord crr = op.CreditcardRefundRecordGet(transactionID);
            if (null != crr && crr.IsLoaded)
            {
                NameValueCollection result = GetNewebResult(adminServer, new NameValueCollection(){
                    {"OrderNumber", transactionID},
                    {"CreditNumber", crr.RefundReturnNumber},
                    {"operation", "RefundReversal"},
                    {"userid", userID},
                    {"passwd", userPassword},
                    {"MerchantNumber", merchantID}
                    });

                LogAction(transactionID, result["PRC"] + "|" + result["SRC"]);

                return (result.Count > 0 && string.Equals("0", result["PRC"]));
            }
            else
            {
                return false;
            }
        }

        private static CreditCardQueryDetail NewebQuery(string transactionID, string userID, string userPassword, string merchantID, string adminServer)
        {
            NameValueCollection result = GetNewebResult(adminServer, new NameValueCollection(){
                {"OrderNumber", transactionID},
                {"operation", "QueryOrders"},
                {"userid", userID},
                {"passwd", userPassword},
                {"MerchantNumber", merchantID}
                });

            if (result.Count > 0 && string.Equals("0", result["PRC"]))
            {
                LogAction(transactionID, result["PRC"] + "|" + result["SRC"]);
                if (result.Count > 2)
                {
                    //"PRC : 0|SRC : 0|MERCHANTNUMBER : 759851|ORDERNUMBER : testk0fgdg1uzef|STATUS : 5|APPROVEAMOUNT : 1|PERIOD : 0|CURRENCY : 901|CARDNUMBER : 434411******1014|ORDERDATE : 2013-09-24 11:05:25|APPROVEDATE : 2013-09-24 11:05:25|APPROVALCODE : TS5372|DEPOSITAMOUNT : 0|DEPOSITDATE : |CREDITAMOUNT : 0|CREDITDATE : |"
                    CreditCardQueryDetail detail = new CreditCardQueryDetail();
                    detail.ReturnCode = result["PRC"] + "|" + result["SRC"];
                    if (!string.IsNullOrWhiteSpace(result["ORDERDATE"]))
                    {
                        detail.OrderDate = DateTime.Parse(result["ORDERDATE"]);
                    }
                    detail.ApproveAmount = string.IsNullOrWhiteSpace(result["APPROVEAMOUNT"]) ? -1 : int.Parse(result["APPROVEAMOUNT"]);
                    detail.AuthCode = result["APPROVELCODE"];
                    detail.Currency = result["CURRENCY"];
                    detail.OrderStatus = result["STATUS"];
                    detail.CaptureAmount = string.IsNullOrWhiteSpace(result["DEPOSITAMOUNT"]) ? -1 : int.Parse(result["DEPOSITAMOUNT"]);
                    if (!string.IsNullOrWhiteSpace(result["DEPOSITDATE"]))
                    {
                        detail.CaptureDate = DateTime.Parse(result["DEPOSITDATE"]);
                    }
                    detail.RefundAmount = string.IsNullOrWhiteSpace(result["CREDITAMOUNT"]) ? -1 : int.Parse(result["CREDITAMOUNT"]);
                    if (!string.IsNullOrWhiteSpace(result["CREDITDATE"]))
                    {
                        detail.RefundDate = DateTime.Parse(result["CREDITDATE"]);
                    }
                    detail.InstallmentPeriod = string.IsNullOrWhiteSpace(result["PERIOD"]) ? 0 : int.Parse(result["PERIOD"]);

                    return detail;
                }
                else
                {
                    return null;
                }
            }

            logger.Info("NewebQuery error : transactionID = " + transactionID);
            return null;
        }

        private static NameValueCollection GetNewebResult(string requestLink, NameValueCollection parameters)
        {
            NameValueCollection res = new NameValueCollection();
            using (WebClient client = new WebClient())
            {
                try
                {
                    string response = Encoding.UTF8.GetString(client.UploadValues(requestLink, parameters));

                    if (!string.IsNullOrEmpty(response))
                    {
                        // PRC=0&SRC=0&BANKRESPONSECODE=0/00&STATUS=1&XID=&APPROVALCODE=FFFFFF

                        #region parse response

                        string[] kk = response.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);
                        string[] temp;
                        foreach (string y in kk)
                        {
                            temp = y.Split('=');
                            if (!string.IsNullOrEmpty(temp[0]))
                            {
                                res.Add(temp[0], temp[1]);
                            }
                        }

                        #endregion parse response
                    }
                }
                catch (Exception ex)
                {
                    logger.Info("Neweb error! link = " + requestLink + ", operation = " + parameters["operation"] + ", error message = " + ex.Message);
                }
            }

            return res;
        }

        private static string GetNewebMessage(string prc, string src)
        {
            return string.Format("{0}|{1}", GetNewebPrcDesc(prc), GetNewebSrcDesc(src));
        }

        private static string GetNewebPrcDesc(string prc)
        {
            switch (prc)
            {
                //PRC_OPERATION_SUCCESS
                case "0":
                    return "作業順利完成。";
                //PRC_UNDEFINED_OBJECT
                case "2":
                    return " 找不到指定的物件。";
                //PRC_PARAMETER_NOT_FOUND
                case "3":
                    return " 找不到必要參數。";
                //PRC_PARAMETER_FORMAT_ERROR
                case "6":
                    return " 必要參數的格式不正確。";
                //PRC_PARAMETER_VALUE_ERROR
                case "7":
                    return " 必要參數的值不正確。";
                //PRC_DUPLICATE_OBJECT
                case "8":
                    return " 有重複物件存在。按 SRC 中指出，已存在此付款號碼的付款。";
                //PRC_INPUT_ERROR
                case "10":
                    return " 剖析輸入串流時發生錯誤。指令或其中一個參數的長度無效。";
                //PRC_NOT_VALID_STATE
                case "11":
                    return " 對此動作而言，物件未處於正確狀態。";
                //PRC_COMMUNICATION_ERROR
                case "12":
                    return " Payment Manager 中發生通信錯誤。";
                //PRC_INTERNAL_ERROR
                case "13":
                    return " Payment Manager 遇到非預期的內部錯誤。";
                //PRC_DATABASE_ERROR
                case "14":
                    return " 發生資料庫通信錯誤。";
                //PRC_CASSETTE_ERROR
                case "15":
                    return " 發生卡匣特定錯誤。相關說明請參閱卡匣補充資訊。";
                //PRC_INVALID_PARAMETER_COMBINATION
                case "32":
                    return " 不容許 API 指令中所指定的參數組合。";
                //PRC_FINANCIAL_FAILURE
                case "34":
                    return " 因金融理由導致作業失敗。";
                //PRC_SECURITY_ISSUE
                case "43":
                    return " 為特定商店做的風險控管。";
                //PRC_AUTHORIZATION_ERROR
                case "52":
                    return " 進行使用者授權期間發生錯誤。";
                //PRC_COMMAND_NOT_SUPPORTED
                case "55":
                    return " 指令名稱未被視為有效的 $til; 指令。";
                default:
                    return "Unknown";
            }
        }

        private static string GetNewebSrcDesc(string src)
        {
            switch (src)
            {
                //RC_NONE
                case "0":
                    return "無其它資訊可用。";
                //RC_INPUT_ERROR_UNKNOWN_COMMAND
                case "3":
                    return "不明指令。";
                //RC_UNEXPECTED
                case "4":
                    return "發生異常錯誤。";
                //RC_ENCODING_UNSUPPORTED
                case "10":
                    return "不支援之編碼。";
                //RC_MERCHANTNUMBER
                case "110":
                    return "此回應與商家號碼參數有關。";
                //RC_ORDERNUMBER
                case "111":
                    return "此回應與訂單號碼參數有關。";
                //RC_ORDERDATE
                case "112":
                    return "此回應與 ORDERDATE 參數有關。";
                //RC_BATCHCLOSEDATE
                case "113":
                    return "此回應與 BATCHCLOSEDATE 參數有關。";
                //RC_BATCHNUMBER
                case "114":
                    return "此回應與 BATCHNUMBER 參數有關。（註：在舊版中，此回覆碼是參照 BATCHID 參數。）";
                //RC_AMOUNT
                case "117":
                    return "此回應與 AMOUNT 參數有關。";
                //RC_AMOUNTEXP10
                case "118":
                    return "此回應與 AMOUNTEXP10 參數有關。";
                //RC_CURRENCY
                case "119":
                    return "此回應與 CURRENCY 參數有關。";
                //RC_ORDERURLS
                case "130":
                    return "此回應與訂單 URL 參數有關。";
                //RC_CHECK_CASSETTE_STATUS
                case "171":
                    return "查看卡匣特定資料取得進一步資訊。";
                //RC_MERCHANTPAYSYS
                case "202":
                    return "此回應與商家付款系統（如 SET）有關。";
                //RC_ORDER
                case "204":
                    return "此回應與訂單實體有關。";
                //RC_PAYMENT
                case "205":
                    return "此回應與付款實體有關。";
                //RC_CREDIT
                case "206":
                    return "此回應與退款實體有關。";
                //RC_BATCH
                case "207":
                    return "此回應與批次實體有關。";
                //RC_COMMUNICATION_ERROR
                case "309":
                    return "發生通信錯誤。";
                //RC_ERROR_CONNECTING_DATABASE_OR_EXECUTING_SQL
                case "512":
                    return "連接資料庫或執行 SQL 陳述式時發生錯誤。";
                //RC_USER_NOT_AUTHORIZED
                case "554":
                    return "指定的使用者無權執行所要求的作業。";
                //RC_CASSETTE_PAN
                case "1015":
                    return "此回應與 PAN 參數（指定於通信協定資料中）有關。";
                //RC_CASSETTE_EXPIRY
                case "1016":
                    return "此回應與過期參數（指定於通信協定資料中）有關。";
                //RC_CASSETTE_COMMUNICATION_ERROR
                case "1018":
                    return "卡匣與其所通信之實體間發生通信錯誤。";
                //RC_DEPOSITFLAG
                case "1200":
                    return "此回應與請款指標參數有關。";
                //RC_WITHDETAIL
                case "1201":
                    return "此回應與訂單明細參數有關。";
                //RC_CREDITNUMBER
                case "1202":
                    return "此回應與信用卡卡號參數有關。";
                //RC_CASSETTE_XID
                case "2005":
                    return "XIDINDEX。";
                //RC_CASSETTE_CAVV
                case "2006":
                    return "CAVV。";
                //RC_CASSETTE_ECI
                case "2007":
                    return "ECI。";
                //RC_CASSETTE_ERRORCODE
                case "2009":
                    return "ERRORCODE。";
                //RC_CASSETTE_PINCODE
                case "2011":
                    return "PINCODE。";
                //RC_CASSETTE_ID
                case "2015":
                    return "ID。";
                //RC_CASSETTE_PERIOD
                case "2016":
                    return "此回應與分期付款期數參數有關。";
                //RC_CASSETTE_CVV2
                case "2017":
                    return "CVV2。";
                //RC_CASSETTE_APPROVECODE
                case "2018":
                    return "此回應與授權碼參數有關。";
                //RC_CASSETTE_ORDER_DESC
                case "2050":
                    return "此回應與訂單說明參數有關。";
                //RC_CASSETTE_REDEMPTION
                case "2052":
                    return "REDEMPTION。";
                //RC_CASSETTE_STARTDATE
                case "2053":
                    return "此回應與起始日期參數有關。";
                //RC_CASSETTE_ENDDATE
                case "2054":
                    return "此回應與結束日期參數有關。";
                //RC_CASSETTE_ZIPCODE
                case "2055":
                    return "此回應與郵遞區號參數有關。";
                //RC_AMOUNT_UPPER_THAN_UPPERLIMIT
                case "4001":
                    return "限額阻擋，交易超過額度上限。";
                //RC_AMOUNT_LOWER_THAN_UPPERLIMIT
                case "4003":
                    return "限額阻擋，單筆交易金額低於下限。";
                //RC_BLACKBIN_SYSTEM
                case "4004":
                    return "系統黑名單。";
                //RC_BLACKBIN_MERCHANT
                case "4005":
                    return "商店黑名單。";
                //RC_WHITEBIN
                case "4006":
                    return "白名單。";
                //RC_JUST_ACCEPT_NATIVE_BIN
                case "4007":
                    return "僅接受國內卡。";
                //RC_JUST_ACCEPT_FOREIGN_BIN
                case "4008":
                    return "僅接受國外卡。";
                //RC_JUST_ACCEPT_SELF_BIN
                case "4009":
                    return "僅接受自行卡。";
                //RC_DAYSLIMIT_DEPOSIT
                case "4010": return "請款天數限制。";
                //RC_DAYSLIMIT_REFUND
                case "4011":
                    return "退款天數限制。";
                //RC_CASSETTE_MERCHANTID
                case "5005":
                    return "銀行 Payment Gateway 商家代碼，非特店代號。";
                //RC_CASSETTE_PRODUCT_CODE
                case "5013":
                    return "此回應與商品代號參數有關。";
                //RC_CASSETTE_TRANSNUMBER
                case "5014":
                    return "此回應與交易序號參數有關。";
                //RC_CASSETTE_PRODUCT_COUNT
                case "5020":
                    return "此回應與商品總數參數有關。";

                default:
                    return string.Empty;
            }
        }

        #endregion Neweb

        #region Taishin PaymentGateway API

        /// <summary>
        /// 授權
        /// </summary>
        /// <param name="authObject"></param>
        /// <param name="merchantId"></param>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        private static CreditCardAuthResult TaishinPaymentAuth(CreditCardAuthObject authObject, string merchantId, string terminalId)
        {
            string apiUrl = Helper.CombineUrl(config.TaiShinPaymnetGatewayApi, "DirectAuth.ashx");
            var data = new TaishinPaymentRequestBase
            {
                Mid = merchantId,
                Tid = terminalId,
                PayType = (int)TaishinPayType.CredidCard,
                TxType = (int)TaishinPaymentType.Auth,
                Detail = new
                {
                    order_no = authObject.TransactionId,
                    amt = (authObject.Amount * 100).ToString(), //包含兩位小數 100 代表1.00 元
                    cur = "NTD",
                    order_desc = authObject.Description,
                    capt_flag = "0", //0:不同步請款 1:同步請款
                    pan = authObject.CardNumber,
                    exp_date = authObject.ExpireYear + authObject.ExpireMonth,
                    cvv2 = authObject.SecurityCode,
                    install_period = authObject.InstallmentPeriod > 0 ? authObject.InstallmentPeriod.ToString() : string.Empty,
                }
            };
            //{"ver":"1.2.0","mid":"000812770000864","tid":"T0000001","pay_type":3,"tx_type":1,"ret_value":0,"params":{"ret_code":"00",
            //"ret_msg":"\u4EA4\u6613\u6210\u529F(Approved or completed successfully)","auth_id_resp":"051422","rrn":"826316609285","order_status":"02","cur":"NTD","purchase_date":"2018-09-21 00:13:10","tx_amt":"50800"}}
            //{"pay_type":0,"tx_type":0,"ret_value":0,"params":{"ret_code":"-321","ret_msg":"Expired card."}}
            TaishinPaymentResult jsonResult = TaishinPaymentResponseResult(data, apiUrl);
            CreditCardAuthResult result = new CreditCardAuthResult();
            result.APIProvider = authObject.APIProvider;
            result.CardNumber = MarkCardNumber(authObject.CardNumber);
            result.TransPhase = PayTransPhase.Failed;
            result.OrderDate = DateTime.Now;

            if (jsonResult != null)
            {
                logger.Info("台新PaymentGateway" + JsonConvert.SerializeObject(jsonResult));
                if (jsonResult.Detail.RetCode == "00")
                {
                    result.InstallmentPeriod = authObject.InstallmentPeriod;
                    result.ReturnCode = jsonResult.Detail.RetCode;
                    result.TransMessage = ConvertMsg(jsonResult.Detail.RetMsg);
                    result.AuthenticationCode = jsonResult.Detail.AuthIdResp;
                    result.TransPhase = PayTransPhase.Successful;
                }
                else
                {
                    result.InstallmentPeriod = authObject.InstallmentPeriod;
                    result.ReturnCode = jsonResult.Detail.RetCode;
                    result.TransMessage = ConvertMsg(jsonResult.Detail.RetMsg);
                    result.TransPhase = PayTransPhase.Failed;
                }
            }

            LogAuth(authObject, result);

            return result;
        }

        /// <summary>
        /// 其他交易(取消授權/請款/取消請款/退貨/取消退貨)
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="merchantId"></param>
        /// <param name="terminalId"></param>
        /// <param name="type"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        private static bool TaishinPaymentRequest(string transactionID, string merchantId, string terminalId, TaishinPaymentType type,int? amount = null)
        {
            string apiUrl = Helper.CombineUrl(config.TaiShinPaymnetGatewayApi, "other.ashx");
            TaishinPaymentRequestBase data = new TaishinPaymentRequestBase();

            if (type == TaishinPaymentType.AuthReverse || type == TaishinPaymentType.Query)
            {
                data = new TaishinPaymentRequestBase
                {
                    Mid = merchantId,
                    Tid = terminalId,
                    PayType = (int)TaishinPayType.CredidCard,
                    TxType = (int)type,
                    Detail = new
                    {
                        order_no = transactionID,
                    }
                };
            }
            else
            {
                amount = amount ?? int.Parse(op.PaymentTransactionGet(transactionID, LunchKingSite.Core.PaymentType.Creditcard, (int)PayTransType.Authorization).Amount.ToString());
                data = new TaishinPaymentRequestBase
                {
                    Mid = merchantId,
                    Tid = terminalId,
                    PayType = (int)TaishinPayType.CredidCard,
                    TxType = (int)type,
                    Detail = new
                    {
                        order_no = transactionID,
                        amt = (amount * 100).ToString()
                    }
                };
            }

            TaishinPaymentResult result = TaishinPaymentResponseResult(data, apiUrl);
            LogAction(transactionID, result != null ? result.Detail.RetCode : string.Empty);
            if (result != null)
            {
                //超過180天交易退貨失敗
                if (type == TaishinPaymentType.Refund && result.Detail.RetCode == "-324")
                {
                    result.Detail.RetCode = ProcessTaishinRefund(transactionID) ? "00" : result.Detail.RetCode;
                }
                return result.Detail.RetCode == "00";
            }

            return false;
        }

        /// <summary>
        /// 2019/08/28 超過180天的訂單改由人工處理
        /// </summary>
        /// <param name="transId"></param>
        /// <param name="apiProvider"></param>
        private static bool ProcessTaishinRefund(string transId)
        {
            try
            {
                PaymentTransaction ptAuth = op.PaymentTransactionGet(transId, PaymentType.Creditcard, PayTransType.Authorization);
                PaymentTransaction ptCharging = op.PaymentTransactionGet(transId, PaymentType.Creditcard, PayTransType.Charging);
                PaymentTransaction ptRefund = op.PaymentTransactionGet(transId, PaymentType.Creditcard, PayTransType.Refund);
                PaymentTransaction ptScash = op.PaymentTransactionGet(transId, PaymentType.SCash, PayTransType.Authorization);

                var apiProvider = ptAuth.ApiProvider;

                string merchantId = string.Empty;
                if (apiProvider == (int)PaymentAPIProvider.TaishinMallPaymnetGateway)
                {
                    merchantId = config.TaiShinMallMerchantID;
                }
                else if (apiProvider == (int)PaymentAPIProvider.TaishinPaymnetGateway || apiProvider == (int)PaymentAPIProvider.TaishinPaymnetOTPGateway || apiProvider == (int)PaymentAPIProvider.ApplePay)
                {
                    merchantId = config.TaiShinMerchantID;
                }

                CreditcardOrder creditcardOrder = op.CreditcardOrderGetByOrderGuid(ptScash.OrderGuid.GetValueOrDefault());
                string cardNumber = DecryptCardNumber(creditcardOrder.CreditcardInfo);
                DateTime transTime = ptAuth.TransTime.GetValueOrDefault();
                decimal transAmount = ptCharging.Amount;
                decimal refundAmount = ptRefund.Amount;
                decimal remainAmount = transAmount - refundAmount;
                string authCode = ptAuth.AuthCode;
                int installment = ptAuth.Installment.GetValueOrDefault();
                DateTime createTime = DateTime.Now;

                HitrustRefundRecord record = new HitrustRefundRecord
                {
                    TransId = transId,
                    TheDate = DateTime.Today,
                    MerchantId = merchantId,
                    CardNumber = cardNumber,
                    TransTime = transTime,
                    TransAmount = transAmount,
                    RefundAmount = refundAmount,
                    RemainAmount = remainAmount,
                    AuthCode = authCode,
                    Installment = installment,
                    CreateTime = createTime,
                    ApiProvider = (int)apiProvider
                };

                op.HitrustRefundRecordSet(record);

            }
            catch (Exception ex)
            {
                logger.WarnFormat("ProcessTaishinRefund general fail, trans_id={0}, ex={1}", transId, ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// OTP其他交易(取消授權/請款/取消請款/退貨/取消退貨)
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="merchantId"></param>
        /// <param name="terminalId"></param>
        /// <param name="type"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        private static bool TaishinPaymentOTPRequest(string transactionID, string merchantId, string terminalId, TaishinPaymentType type, int? amount = null)
        {
            string apiUrl = Helper.CombineUrl(config.TaiShinPaymnetGatewayApi, "other.ashx");
            TaishinPaymentOTPRequest data = new TaishinPaymentOTPRequest();

            if (type == TaishinPaymentType.AuthReverse || type == TaishinPaymentType.Query)
            {
                data = new TaishinPaymentOTPRequest
                {
                    Mid = merchantId,
                    Tid = terminalId,
                    PayType = (int)TaishinPayType.CredidCard,
                    TxType = (int)type,
                    Detail = new
                    {
                        order_no = transactionID,
                    }
                };
            }
            else
            {
                amount = amount ?? int.Parse(op.PaymentTransactionGet(transactionID, LunchKingSite.Core.PaymentType.Creditcard, (int)PayTransType.Authorization).Amount.ToString());
                data = new TaishinPaymentOTPRequest
                {
                    Mid = merchantId,
                    Tid = terminalId,
                    PayType = (int)TaishinPayType.CredidCard,
                    TxType = (int)type,
                    Detail = new
                    {
                        order_no = transactionID,
                        amt = (amount * 100).ToString()
                    }
                };
            }
            PaymentFacade.OTPApiLog(transactionID, JsonConvert.SerializeObject(data));
            TaishinPaymentResult result = TaishinPaymentResponseResult(data, apiUrl);
            PaymentFacade.OTPApiLog(transactionID, JsonConvert.SerializeObject(result));
            LogAction(transactionID, result != null ? result.Detail.RetCode : string.Empty);
            if (result != null)
            {
                if (type == TaishinPaymentType.Refund && result.Detail.RetCode == "-324")
                {
                    result.Detail.RetCode = ProcessTaishinRefund(transactionID) ? "00" : result.Detail.RetCode;
                }
                return result.Detail.RetCode == "00";
            }

            return false;
        }

        /// <summary>
        /// 查詢交易
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="merchantId"></param>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        private static CreditCardQueryDetail TaishinPaymentRequest(string transactionID, string merchantId, string terminalId)
        {
            string apiUrl = Helper.CombineUrl(config.TaiShinPaymnetGatewayApi, "other.ashx");
            TaishinPaymentRequestBase data = new TaishinPaymentRequestBase();

            data = new TaishinPaymentRequestBase
            {
                Mid = merchantId,
                Tid = terminalId,
                PayType = (int)TaishinPayType.CredidCard,
                TxType = (int)TaishinPaymentType.Query,
                Detail = new
                {
                    order_no = transactionID,
                    result_flag = 1,
                }
            };

            TaishinPaymentResult result = TaishinPaymentResponseResult(data, apiUrl);
            LogAction(transactionID, result != null ? result.Detail.RetCode : string.Empty);
            CreditCardQueryDetail detail = new CreditCardQueryDetail();

            if (result != null)
            {
                detail.ReturnCode = result.Detail.RetCode;
                detail.OrderStatus = result.Detail.OrderStatus;
            }

            return detail;
        }

        /// <summary>
        /// OTP查詢交易
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="merchantId"></param>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        private static CreditCardQueryDetail TaishinPaymentOTPRequest(string transactionID, string merchantId, string terminalId)
        {
            string apiUrl = Helper.CombineUrl(config.TaiShinPaymnetGatewayApi, "other.ashx");
            TaishinPaymentOTPRequest data = new TaishinPaymentOTPRequest();

            data = new TaishinPaymentOTPRequest
            {
                Mid = merchantId,
                Tid = terminalId,
                PayType = (int)TaishinPayType.CredidCard,
                TxType = (int)TaishinPaymentType.Query,
                Detail = new
                {
                    order_no = transactionID,
                    result_flag = "1",
                }
            };

            TaishinPaymentResult result = TaishinPaymentResponseResult(data, apiUrl);
            LogAction(transactionID, result != null ? result.Detail.RetCode : string.Empty);
            CreditCardQueryDetail detail = new CreditCardQueryDetail();

            if (result != null)
            {
                detail.ReturnCode = result.Detail.RetCode;
                detail.OrderStatus = result.Detail.OrderStatus;
            }

            return detail;
        }

        /// <summary>
        /// ApplePay
        /// </summary>
        /// <param name="authObject"></param>
        /// <param name="merchantId"></param>
        /// <param name="terminalId"></param>
        /// <param name="isOrderFromApp"></param>
        /// <returns></returns>
        private static CreditCardAuthResult TaishinPaymentApplePayAuth(ApplePayAuthObject authObject, string merchantId, string terminalId, bool isOrderFromApp)
        {
            string apiUrl = Helper.CombineUrl(config.TaiShinPaymnetGatewayApi, "tokenPayAuth.ashx");
            TaishinPaymentRequestBase data = new TaishinPaymentRequestBase();
            if (authObject.PaymentDataType == "EMV")
            {
                data = new TaishinPaymentRequestBase
                {
                    Mid = merchantId,
                    Tid = terminalId,
                    PayType = (int)TaishinPayType.ApplePayEMV,
                    TxType = (int)TaishinPaymentType.Auth,
                    Detail = new
                    {
                        order_no = authObject.TransactionId,
                        order_desc = authObject.Description,
                        capt_flag = "0",
                        amt = (authObject.Amount * 100).ToString(),
                        payTokenData = new
                        {
                            applicationPrimaryAccountNumber = authObject.CardNumber, //card number
                            applicationExpirationDate = authObject.ExpireDate, //expire date YYMMDD
                            currencyCode = "901", //currency Code
                            transactionAmount = 0,  //amount
                            deviceManufacturerIdentifier = authObject.DeviceManufacturerIdentifier, // Hex-encoded device manufacturer identifier
                            paymentDataType = "EMV",
                            paymentData = new
                            {
                                emvData = authObject.EmvData,
                                encryptedPINData = authObject.EncryptedPINData
                            }
                        }
                    }
                };
            }
            else
            {
                if (string.IsNullOrEmpty(authObject.EciIndicator))
                {
                    data = new TaishinPaymentRequestBase
                    {
                        Mid = merchantId,
                        Tid = terminalId,
                        PayType = isOrderFromApp ? (int)TaishinPayType.ApplePayInApp : (int)TaishinPayType.ApplePayOnWeb,
                        TxType = (int)TaishinPaymentType.Auth,
                        Detail = new
                        {
                            order_no = authObject.TransactionId,
                            order_desc = authObject.Description,
                            capt_flag = "0",
                            payTokenData = new
                            {
                                applicationPrimaryAccountNumber = authObject.CardNumber, //card number
                                applicationExpirationDate = authObject.ExpireDate, //expire date YYMMDD
                                currencyCode = "901", //currency Code
                                transactionAmount = authObject.Amount,  //amount
                                deviceManufacturerIdentifier = authObject.DeviceManufacturerIdentifier, // Hex-encoded device manufacturer identifier
                                paymentDataType = "3DSecure",
                                paymentData = new
                                {
                                    OnlinePaymentCryptogram = authObject.OnlinePaymentCryptogram,
                                }
                            }
                        }
                    };
                }
                else
                {
                    data = new TaishinPaymentRequestBase
                    {
                        Mid = merchantId,
                        Tid = terminalId,
                        PayType = isOrderFromApp ? (int)TaishinPayType.ApplePayInApp : (int)TaishinPayType.ApplePayOnWeb,
                        TxType = (int)TaishinPaymentType.Auth,
                        Detail = new
                        {
                            order_no = authObject.TransactionId,
                            order_desc = authObject.Description,
                            capt_flag = "0",
                            payTokenData = new
                            {
                                applicationPrimaryAccountNumber = authObject.CardNumber, //card number
                                applicationExpirationDate = authObject.ExpireDate, //expire date YYMMDD
                                currencyCode = "901", //currency Code
                                transactionAmount = authObject.Amount,  //amount
                                deviceManufacturerIdentifier = authObject.DeviceManufacturerIdentifier, // Hex-encoded device manufacturer identifier
                                paymentDataType = "3DSecure",
                                paymentData = new
                                {
                                    OnlinePaymentCryptogram = authObject.OnlinePaymentCryptogram,
                                    EciIndicator = authObject.EciIndicator
                                }
                            }
                        }
                    };
                }
            }
            
            var jsonResult = TaishinPaymentResponseResult(data, apiUrl);
            CreditCardAuthResult result = new CreditCardAuthResult();
            result.APIProvider = authObject.APIProvider;
            result.CardNumber = MarkCardNumber(authObject.CardNumber);
            result.TransPhase = PayTransPhase.Failed;
            result.OrderDate = DateTime.Now;

            if (jsonResult != null)
            {
                string retCode = string.IsNullOrEmpty(jsonResult.Detail.RetCode) ?  jsonResult.Detail.RespCode : jsonResult.Detail.RetCode;
                string retMsg = string.IsNullOrEmpty(jsonResult.Detail.RetMsg) ? jsonResult.Detail.RespMsg : jsonResult.Detail.RetMsg;
                if (retCode == "00")
                {
                    result.InstallmentPeriod = authObject.InstallmentPeriod;
                    result.ReturnCode = retCode;
                    result.TransMessage = ConvertMsg(retMsg);
                    result.AuthenticationCode = jsonResult.Detail.AuthIdResp;
                    result.TransPhase = PayTransPhase.Successful;
                }
            }

            LogAuth(authObject, result);

            return result;
        }

        /// <summary>
        /// OTP授權
        /// </summary>
        /// <param name="authObject"></param>
        /// <param name="merchantId"></param>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        private static CreditCardAuthResult TaishinPaymentOTPAuth(CreditCardAuthObject authObject, string merchantId, string terminalId)
        {
            string apiUrl = Helper.CombineUrl(config.TaiShinPaymnetGatewayApi, "Auth.ashx");
            var data = new TaishinPaymentOTPRequest
            {
                Mid = merchantId,
                Tid = terminalId,
                PayType = (int)TaishinPayType.CredidCard,
                TxType = (int)TaishinPaymentType.Auth,
                Detail = new
                {
                    layout = "2",
                    order_no = authObject.TransactionId,
                    amt = (authObject.Amount * 100).ToString(), //包含兩位小數 100 代表1.00 元
                    cur = "NTD",
                    order_desc = authObject.Description,
                    capt_flag = "0", //0:不同步請款 1:同步請款
                    result_flag = "1",
                    post_back_url = authObject.OTP.PostBackUrl,
                    result_url = authObject.OTP.ReturnUrl,
                    pan = authObject.CardNumber,
                    exp_date = authObject.ExpireYear + authObject.ExpireMonth,
                    cvv2 = authObject.SecurityCode,
                    install_period = authObject.InstallmentPeriod > 0 ? authObject.InstallmentPeriod.ToString() : string.Empty,
                }
            };

            string requestLog = JsonConvert.SerializeObject(data);
            requestLog = requestLog.Replace(requestLog.Substring(requestLog.IndexOf("pan") + 6, 16), MarkCardNumber(requestLog.Substring(requestLog.IndexOf("pan") + 6, 16)));
            requestLog = requestLog.Replace(requestLog.Substring(requestLog.IndexOf("cvv2") + 7, 3),"***");
            PaymentFacade.OTPApiLog(authObject.TransactionId, requestLog);

            TaishinPaymentResult jsonResult = TaishinPaymentResponseResult(data, apiUrl);
            PaymentFacade.OTPApiLog(authObject.TransactionId, JsonConvert.SerializeObject(jsonResult));
            CreditCardAuthResult result = new CreditCardAuthResult();
            result.APIProvider = authObject.APIProvider;
            result.CardNumber = MarkCardNumber(authObject.CardNumber);
            result.TransPhase = PayTransPhase.Failed;
            result.OrderDate = DateTime.Now;

            if (jsonResult != null)
            {
                logger.Info("台新PaymentGateway" + JsonConvert.SerializeObject(jsonResult));
                if (jsonResult.Detail.RetCode == "00")
                {
                    result.InstallmentPeriod = authObject.InstallmentPeriod;
                    result.ReturnCode = jsonResult.Detail.RetCode;
                    result.TransMessage = ConvertMsg(jsonResult.Detail.RetMsg);
                    result.AuthenticationCode = jsonResult.Detail.AuthIdResp;
                    result.TransPhase = PayTransPhase.Successful;

                    result.IsOTP = true;
                    result.HppUrl = jsonResult.Detail.HppUrl;
                }
                else
                {
                    result.InstallmentPeriod = authObject.InstallmentPeriod;
                    result.ReturnCode = jsonResult.Detail.RetCode;
                    result.TransMessage = ConvertMsg(jsonResult.Detail.RetMsg);
                    result.TransPhase = PayTransPhase.Failed;
                }
            }

            LogAuth(authObject, result);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="apiUrl"></param>
        /// <returns></returns>
        private static TaishinPaymentResult TaishinPaymentResponseResult(TaishinPaymentRequestBase data, string apiUrl)
        {
            var request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = WebRequestMethods.Http.Post;
            var jsonText = new LunchKingSite.Core.JsonSerializer().Serialize(data);
            var markJsonText = jsonText;

            markJsonText = markJsonText.Replace(jsonText.Substring(markJsonText.IndexOf("pan") + 6, 16), MarkCardNumber(markJsonText.Substring(markJsonText.IndexOf("pan") + 6, 16)));
            markJsonText = markJsonText.Replace(markJsonText.Substring(markJsonText.IndexOf("cvv2") + 7, 3), "***");

            logger.Info(string.Format("taishin {0} request:{1}", (TaishinPaymentType)data.TxType, markJsonText));

            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;

            try
            {
                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                    requestStream.Flush();
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            return null;
                        }

                        using (var reader = new StreamReader(stream))
                        {
                            var responseResult = reader.ReadToEnd();

                            logger.Info(string.Format(@"taishin {0} response:{1}", (TaishinPaymentType)data.TxType, responseResult));

                            var jsonResult = ProviderFactory.Instance().GetSerializer().Deserialize<TaishinPaymentResult>(responseResult);

                            return jsonResult;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                logger.Info(string.Format("taishin Response error : " + ex.Message));
            }
            return null;
        }

        #endregion

        #region Cathay PaymnetGateway API

        private static CreditCardAuthResult CathayPaymentOTPAuth(CreditCardAuthObject authObject, string merchantId, string CubKey)
        {
            var authResult = new CathayPaymentOtpFirstResult();
            CreditCardAuthResult result = new CreditCardAuthResult();
            dynamic auth;
            if (authObject.InstallmentPeriod > 0)
            {
                auth = new
                {
                    MERCHANTXML = new
                    {
                        CAVALUE = Security.MD5Hash(merchantId + authObject.TransactionId.ToUpper() + authObject.Amount.ToString() + authObject.InstallmentPeriod.ToString() + authObject.CardNumber + CubKey, false),
                        MSGID = "TRS0002",
                        ORDERINFO = new
                        {
                            STOREID = merchantId,
                            ORDERNUMBER = authObject.TransactionId.ToUpper(),
                            AMOUNT = authObject.Amount.ToString(),
                            PERIODNUMBER = authObject.InstallmentPeriod.ToString()
                        },
                        PAYMENTINFO = new
                        {
                            CARDNO = authObject.CardNumber,
                            EXPIRE = "20" + authObject.ExpireYear + authObject.ExpireMonth,
                            CVC2 = authObject.SecurityCode
                        }
                    }
                };
            }
            else
            {
                auth = new
                {
                    MERCHANTXML = new
                    {
                        CAVALUE = Security.MD5Hash(merchantId + authObject.TransactionId.ToUpper() + authObject.Amount.ToString() + authObject.CardNumber + CubKey,false),
                        MSGID = "TRS0001",
                        ORDERINFO = new
                        {
                            STOREID = merchantId,
                            ORDERNUMBER = authObject.TransactionId.ToUpper(),
                            AMOUNT = authObject.Amount.ToString(),
                        },
                        PAYMENTINFO = new
                        {
                            CARDNO = authObject.CardNumber,
                            EXPIRE = "20" + authObject.ExpireYear + authObject.ExpireMonth,
                            CVC2 = authObject.SecurityCode
                        }
                    }
                };
            }

            var jsonText = new LunchKingSite.Core.JsonSerializer().Serialize(auth);
            System.Xml.XmlDocument data = JsonConvert.DeserializeXmlNode(jsonText);
            data.PrependChild(data.CreateXmlDeclaration("1.0", "utf-8", null));

            string markData = data.InnerXml.ToString().Replace("<CARDNO>" + authObject.CardNumber + "</CARDNO>", "<CARDNO>" + MarkCardNumber(authObject.CardNumber) + "</CARDNO>").
                Replace("<CVC2>" + authObject.SecurityCode + "</CVC2>", "<CVC2>***</CVC2>");
            PaymentFacade.OTPApiLog(authObject.TransactionId, markData);

            result.APIProvider = authObject.InstallmentPeriod > 0 ? PaymentAPIProvider.CathayPaymnetInstallmentWithOtp : PaymentAPIProvider.CathayPaymnetOTPGateway;
            result.CardNumber = MarkCardNumber(authObject.CardNumber);
            result.TransPhase = PayTransPhase.Successful;
            result.OrderDate = DateTime.Now;
            result.IsOTP = true;
            result.HppUrl = Helper.CombineUrl(config.SiteUrl, "/ppon/CathayOtpConfirmPage");

            //加密
            string salt = Guid.NewGuid().ToString("N").Substring(0, 16);
            TempCreditcardOtp tempOtp = new TempCreditcardOtp();
            tempOtp.TransId = authObject.TransactionId;
            tempOtp.UserId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            tempOtp.OtpInfo = MemberFacade.EncryptCreditCardInfo(data.InnerXml.ToString(), salt);
            tempOtp.CreateTime = DateTime.Now;
            tempOtp.EncryptSalt = salt;
            op.TempCreditcardOtpSet(tempOtp);

            LogAuth(authObject, result);
            return result;
        }

        /// <summary>
        /// 授權
        /// </summary>
        /// <param name="authObject"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        private static CreditCardAuthResult CathayPaymentAuth(CreditCardAuthObject authObject, string merchantId, string CubKey)
        {
            CathayPaymentAuthResult authResult = new CathayPaymentAuthResult();
            CreditCardAuthResult result = new CreditCardAuthResult();
            result.InstallmentPeriod = authObject.InstallmentPeriod;
            result.APIProvider = authObject.InstallmentPeriod > 0 ? PaymentAPIProvider.CathayPaymnetInstallment : PaymentAPIProvider.CathayPaymnetGateway;
            result.TransPhase = PayTransPhase.Failed;
            string apiUrl = config.CathayPaymnetGatewayApi + "CRDOrderService.asmx?wsdl";
            dynamic auth;

            if (authObject.InstallmentPeriod > 0)
            {
                auth = new 
                {
                    MERCHANTXML = new 
                    {
                        MSGID = "TRS0002",
                        CAVALUE = Security.MD5Hash(merchantId + authObject.TransactionId.ToUpper() + authObject.Amount.ToString() + authObject.InstallmentPeriod.ToString() + authObject.CardNumber + CubKey, false),
                        AUTHORDERINFO = new 
                        {
                            STOREID = merchantId,
                            ORDERNUMBER = authObject.TransactionId.ToUpper(),
                            AMOUNT = authObject.Amount.ToString(),
                            PERIODNUMBER = authObject.InstallmentPeriod.ToString(),
                            CARDNO = authObject.CardNumber,
                            EXPIRE = "20" + authObject.ExpireYear + authObject.ExpireMonth,
                            CVC = authObject.SecurityCode
                        }
                    }
                };
            }
            else
            {
                auth = new 
                {
                    MERCHANTXML = new 
                    {
                        MSGID = "TRS0001",
                        CAVALUE = Security.MD5Hash(merchantId + authObject.TransactionId.ToUpper() + authObject.Amount.ToString() + authObject.CardNumber + CubKey, false),
                        AUTHORDERINFO = new 
                        {
                            STOREID = merchantId,
                            ORDERNUMBER = authObject.TransactionId.ToUpper(),
                            AMOUNT = authObject.Amount.ToString(),
                            CARDNO = authObject.CardNumber,
                            EXPIRE = "20" + authObject.ExpireYear + authObject.ExpireMonth,
                            CVC = authObject.SecurityCode
                        }
                    }
                };
            }

            var jsonText = new LunchKingSite.Core.JsonSerializer().Serialize(auth);
            System.Xml.XmlDocument data = JsonConvert.DeserializeXmlNode(jsonText);
            data.PrependChild(data.CreateXmlDeclaration("1.0", "utf-8", null));

            string markData = data.InnerXml.Replace("<CARDNO>" + authObject.CardNumber + "</CARDNO>", "<CARDNO>" + MarkCardNumber(authObject.CardNumber) + "</CARDNO>").
                Replace("<CVC>" + authObject.SecurityCode + "</CVC>", "<CVC>***</CVC>");
            logger.Info("[AuthData]" + markData);

            authResult = CathayPaymentResponseResult<CathayPaymentAuthResult>(data.InnerXml);

            if (authResult != null)
            {
                if (authResult.Verify(CubKey))
                {
                    result.ReturnCode = authResult.CUBXML.AUTHORDERINFO.AUTHSTATUS;
                    result.TransMessage = ConvertMsg(authResult.CUBXML.AUTHORDERINFO.AUTHMSG);
                    result.CardNumber = MarkCardNumber(authObject.CardNumber);
                    if (authResult.CUBXML.AUTHORDERINFO.AUTHSTATUS == "0000")
                    {
                        result.AuthenticationCode = authResult.CUBXML.AUTHORDERINFO.AUTHCODE;
                        result.TransPhase = PayTransPhase.Successful;
                    }
                    else
                    {
                        result.TransPhase = PayTransPhase.Failed;
                    }
                }
                else
                {
                    //log to security error
                    //cancel
                    logger.Info("Verify ERROR:" + authResult.CUBXML.CAVALUE);
                    //new CathayPaymentProvider().AuthReverse(authObject.TransactionId, authResult.CUBXML.AUTHORDERINFO.AUTHCODE);
                }
            }

            LogAuth(authObject, result);

            return result;
        }

        private static T CathayPaymentResponseResult<T>(string data, string apiUrl = "")
        {
            try
            {
                CRDOrderServiceSoapClient client = new CRDOrderServiceSoapClient();
                string result = client.CRDOrderMethod(data);
                logger.Info("[Cathay PaymentGateway Result]" + result);
                System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
                xml.LoadXml(result);
                string jsonSerialize = JsonConvert.SerializeXmlNode(xml);
                return JsonConvert.DeserializeObject<T>(jsonSerialize);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return default(T);
        }

        #endregion

        private static void LogAuth(CreditCardAuthObject authObject, CreditCardAuthResult authResult)
        {
            string caller = new StackFrame(1, true).GetMethod().Name;
            if (caller.Contains("HiTrust"))
            {
                caller += ".Net";
            }
            logger.Info("Creditcard Utility action " + caller);
            logger.Info("AuthObject provider = " + authObject.APIProvider.ToString());
            logger.Info("AuthResult provider = " + authResult.APIProvider.ToString());
            logger.Info("OrderNumber = " + authObject.TransactionId);
            logger.Info("Amount = " + authObject.Amount.ToString());
            logger.Info("CardNumber = " + authResult.CardNumber);
            logger.Info("ReturnCode = " + authResult.ReturnCode);
            logger.Info("TransMessage = " + authResult.TransMessage);
            logger.Info("AuthenticationCode = " + authResult.AuthenticationCode);
            logger.Info("InstallmentPeriod = " + authResult.InstallmentPeriod);
            logger.Info("TransPhase = " + authResult.TransPhase.ToString() + "\r\n");
        }

        private static void LogAction(string transactionID, string returnCode)
        {
            string caller = new StackFrame(1, true).GetMethod().Name;
            logger.Info("Creditcard Utility action " + caller);
            logger.Info("transactionID = " + transactionID);
            logger.Info("Return Code = " + returnCode + "\r\n");
        }

        public static void PushToLineBot(CreditCardAuthObject authObject, CreditCardAuthResult authResult, Guid dealId)
        {
            if (HttpContext.Current != null && HttpContext.Current != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                int userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
                if (userId > 0)
                {
                    new AntiFraud(userId).PushToLineBot(authObject, authResult, dealId);
                }
            }
        }

        public static bool AutoBlockCreditcardFraudSuspect(CreditCardAuthObject authObject, CreditCardAuthResult authResult,
            Guid dealId, out int hazard)
        {
            hazard = 0;
            if (HttpContext.Current != null && HttpContext.Current != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                int userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
                if (userId > 0)
                {
                    return new AntiFraud(userId).AutoBlockCreditcardFraudSuspect(
                        authObject, authResult, dealId, out hazard);
                }
            }
            return false;
        }

        #endregion Provider section

        private static string ConvertMsg(string unicodeString)
        {
            if (unicodeString == null)
            {
                return string.Empty;
            }
            Encoding utf8 = Encoding.UTF8;
            Encoding unicode = Encoding.Unicode;

            byte[] unicodeBytes = unicode.GetBytes(unicodeString);

            byte[] utf8Bytes = Encoding.Convert(unicode, utf8, unicodeBytes);

            char[] utf8Chars = new char[utf8.GetCharCount(utf8Bytes, 0, utf8Bytes.Length)];
            utf8.GetChars(utf8Bytes, 0, utf8Bytes.Length, utf8Chars, 0);
            return new string(utf8Chars);
        }
    }

    #region Module

    public class ApplePayAuthObject : CreditCardAuthObject
    {
        /// <summary>
        /// YYMMDD
        /// </summary>
        public string ExpireDate { get; set; }
        public string DeviceManufacturerIdentifier { get; set; }
        public string PaymentDataType { get; set; }
        public string OnlinePaymentCryptogram { get; set; }
        public string EciIndicator { get; set; }
        /// <summary>
        /// for emv
        /// </summary>
        public string EmvData { get; set; }
        /// <summary>
        /// for emv
        /// </summary>
        public string EncryptedPINData { get; set; }
    }

    public class CreditCardAuthObject
    {
        public CreditCardAuthObject()
        {
            OTP = new OTPAuthParam();
        }

        /// <summary>
        /// 訂單編號，長度限制如下：
        /// HiTrust - 20
        /// Neweb - 20
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// 交易金額
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// 信用卡號
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// 有限期限 - 年
        /// </summary>
        public string ExpireYear { get; set; }

        /// <summary>
        /// 有限期限 - 月
        /// </summary>
        public string ExpireMonth { get; set; }

        /// <summary>
        /// 卡片背面三碼
        /// </summary>
        public string SecurityCode { get; set; }

        /// <summary>
        /// 訂單說明
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 請款指標。
        /// 1：授權後自動請款
        /// 0：儘操作授權
        /// </summary>
        public int DepositFlag { get; set; }

        /// <summary>
        /// 交易幣別
        /// HiTrust (CNY：人民幣/NTD：台幣/HKD：港幣/USD：美金/AUD：澳幣)
        /// Neweb (901：台幣)
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// 分期期數
        /// </summary>
        public int InstallmentPeriod { get; set; }

        /// <summary>
        /// 持卡人身份字號或護照號碼，分期交易時使用
        /// </summary>
        public string CardHolderId { get; set; }

        /// <summary>
        /// 串接服務廠商
        /// </summary>
        public PaymentAPIProvider APIProvider { get; set; }

        public string CreditCardName { get; set; }

        public OTPAuthParam OTP { get; set; }
    }    

    public class OTPAuthParam
    {
        public bool Enabled { get; set; }
        public string PostBackUrl { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class CreditCardQueryDetail
    {
        /// <summary>
        /// 交易結果
        /// </summary>
        public string ReturnCode { get; set; }

        /// <summary>
        /// 幣別
        /// HiTrust (CNY：人民幣/NTD：台幣/HKD：港幣/USD：美金/AUD：澳幣)
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// 訂單日期
        /// </summary>
        public DateTime? OrderDate { get; set; }

        /// <summary>
        /// 訂單狀態碼
        /// HiTrust (02 已授權/03 已請款/04 已清算/06 已退款/08 退款已清算/12 訂單已取消/ZY 授權失敗)
        /// Neweb (0 已建立/1 已授權/2 已請款/3 退款/4 已退款已關帳/5 作廢/6 拒絕/7 已請款已關帳)
        /// </summary>
        public string OrderStatus { get; set; }

        /// <summary>
        /// 核准金額
        /// </summary>
        public int ApproveAmount { get; set; }

        /// <summary>
        /// 銀行授權碼
        /// </summary>
        public string AuthCode { get; set; }

        /// <summary>
        /// 銀行調單編號
        /// </summary>
        public string AuthRRN { get; set; }

        /// <summary>
        /// 請款批次號碼
        /// </summary>
        public string PayBatchNumber { get; set; }

        /// <summary>
        /// 請款金額
        /// </summary>
        public int CaptureAmount { get; set; }

        /// <summary>
        /// 請款日期
        /// </summary>
        public DateTime? CaptureDate { get; set; }

        /// <summary>
        /// 退款金額
        /// </summary>
        public int RefundAmount { get; set; }

        /// <summary>
        /// 退款批次號碼
        /// </summary>
        public string RefundBatchNumber { get; set; }

        /// <summary>
        /// 退款調單編號
        /// </summary>
        public string RefundRRN { get; set; }

        /// <summary>
        /// 退款授權碼
        /// </summary>
        public string RefundCode { get; set; }

        /// <summary>
        /// 退款日期
        /// </summary>
        public DateTime? RefundDate { get; set; }

        /// <summary>
        /// 授權方式( SSL, MIA, SET )
        /// </summary>
        public string ECI { get; set; }

        /// <summary>
        /// 分期期數
        /// </summary>
        public int InstallmentPeriod { get; set; }
    }

    public class CreditCardAuthResult
    {
        public CreditCardAuthResult()
        {
            CardNumber = string.Empty;
            ReturnCode = string.Empty;
            TransMessage = string.Empty;
            AuthenticationCode = string.Empty;
            TransType = PayTransType.Authorization;
            TransPhase = PayTransPhase.All;
            OrderDate = DateTime.Now;
            APIProvider = PaymentAPIProvider.Mock;
            BuyTimes = 0;
        }

        public string CardNumber { get; set; }

        public string ReturnCode { get; set; }

        public string TransMessage { get; set; }

        public string AuthenticationCode { get; set; }

        public PayTransType TransType { get; set; }

        public PayTransPhase TransPhase { get; set; }

        public DateTime OrderDate { get; set; }

        public PaymentAPIProvider APIProvider { get; set; }

        public int InstallmentPeriod { get; set; }


        public bool IsOTP { get; set; }
        public int BuyTimes { get; set; }
        public string HppUrl { get; set; }
    }

    public enum CreditCardOrderSource
    {
        Mock = 0,
        Ppon = 1,
        PiinLife = 2,
        PayeasyTravel = 3,
        HiTrustComTest = 7771,
        NewebTest = 7772,
        HiTrustDotNetTest = 7773
    }

    public class TaishinPaymentRequestBase
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("sender")]
        public string Sender
        {
            get
            {
                return "rest";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("ver")]
        public virtual string Ver
        {
            get
            {
                return "1.2.0";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("mid")]
        public string Mid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("tid")]
        public string Tid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("pay_type")]
        public int PayType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("tx_type")]
        public int TxType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("params")]
        public object Detail { get; set; }

    }

    public class TaishinPaymentOTPRequest : TaishinPaymentRequestBase
    {
        public override string Ver
        {
            get
            {
                return "1.0.0";
            }
        }
    }

    public class TaishinPaymentResult
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("ver")]
        public string Ver { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("mid")]
        public string Mid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("s_mid")]
        public string Smid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("tid")]
        public string Tid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("pay_type")]
        public int PayType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("tx_type")]
        public int TxType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("params")]
        public TaishinPaymentResultDetail Detail { get; set; }
    }

    public class TaishinPaymentResultDetail
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("ret_code")]
        public string RetCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("ret_msg")]
        public string RetMsg { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("auth_id_resp")]
        public string AuthIdResp { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("order_status")]
        public string OrderStatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("post_redeem_amt")]
        public string PostRedeemAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("install_order_no")]
        public string InstallOrderNo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("install_period")]
        public string InstallPeriod { get; set; }
        /// <summary>
        /// 倘若ret_code、ret_msg 值為null或空值，改判斷此
        /// </summary>
        [JsonProperty("resp_code")]
        public string RespCode { get; set; }
        /// <summary>
        /// /// <summary>
        /// 倘若ret_code、ret_msg 值為null或空值，改判斷此
        /// </summary>
        /// </summary>
        [JsonProperty("resp_msg")]
        public string RespMsg { get; set; }

        /// <summary>
        /// OTP的一次性驗證網址
        /// </summary>
        [JsonProperty("hpp_url")]
        public string HppUrl { get; set; }
    }

    /// <summary>
    /// 交易類別
    /// </summary>
    public enum TaishinPaymentType
    {
        /// <summary>
        /// 授權
        /// </summary>
        Auth = 1,
        /// <summary>
        /// 請款
        /// </summary>
        Capture = 3,
        /// <summary>
        /// 取消請款
        /// </summary>
        CaptureReverse = 4,
        /// <summary>
        /// 退貨
        /// </summary>
        Refund = 5,
        /// <summary>
        /// 取消退貨
        /// </summary>
        RefundReverse = 6,
        /// <summary>
        /// 查詢
        /// </summary>
        Query = 7,
        /// <summary>
        /// 取消授權
        /// </summary>
        AuthReverse = 8,
    }

    /// <summary>
    /// 台新支付類型
    /// </summary>
    public enum TaishinPayType
    {
        CredidCard = 1,
        ApplePayInApp = 3,
        ApplePayOnWeb = 4,
        ApplePayEMV = 6,
    }

    #endregion Module
}
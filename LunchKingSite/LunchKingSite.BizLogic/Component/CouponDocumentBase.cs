﻿using System;
using System.IO;
using System.Web;
using System.Text;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using com.google.zxing;
using com.google.zxing.common;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Image = System.Drawing.Image;
using Phrase = iTextSharp.text.Phrase;
using Rectangle = System.Drawing.Rectangle;

namespace LunchKingSite.BizLogic.Component
{
    public class PdfPageEvent : PdfPageEventHelper
    {
        public override void OnEndPage(PdfWriter writer, Document doc)
        {

            var content = writer.DirectContent;
            var pageBorderRect = new iTextSharp.text.Rectangle(doc.PageSize);
            pageBorderRect.Left += doc.LeftMargin;
            pageBorderRect.Right -= doc.RightMargin;
            pageBorderRect.Top -= doc.TopMargin;


            //符合內容長度的框線
            pageBorderRect.Bottom = writer.GetVerticalPosition(true);
            pageBorderRect.Bottom -= doc.BottomMargin;
            content.SetColorStroke(BaseColor.BLACK);
            content.Rectangle(pageBorderRect.Left, pageBorderRect.Bottom, pageBorderRect.Width,
                              pageBorderRect.Height);
           
            content.Stroke();
            
        }
    }

    public abstract class CouponDocumentBase
    {
        public static float _BREAKLINE_BORDER_WIDTH = 0.7f;
        public static BaseColor _BREAKLINE_BORDER_COLOR = BaseColor.LIGHT_GRAY;

        protected iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.A4, 96, 96, 10, 10);
        protected iTextSharp.text.Image pdfImgLogo;
        protected iTextSharp.text.Image pdfImgBarcode;
        protected iTextSharp.text.Image pdfImgQrCode;
        protected iTextSharp.text.Image pdfImgBarcodeEAN13;

        /// <summary>
        /// 實作PDF表格內容
        /// </summary>
        abstract protected void RenderPDFContent();

        /// <summary>
        /// 取得 pdf 檔案
        /// </summary>
        /// <returns></returns>
        public byte[] GetBuffer() 
        {
            byte[] pdfBuffer;
            using (MemoryStream ms = new MemoryStream())
            {
                PdfPageEvent page = new PdfPageEvent();

                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                writer.PageEvent = page;
                writer.CloseStream = false;

                doc.Open();
                writer.ViewerPreferences = PdfWriter.PageModeUseOutlines;
                RenderPDFContent();

                //OnEndPage(writer, doc);
                doc.Close();
                pdfBuffer = ms.GetBuffer();
            }
            return pdfBuffer;
        }

        //protected void OnEndPage(PdfWriter writer, Document doc)
        //{
        //    var content = writer.DirectContent;
        //    var pageBorderRect = new iTextSharp.text.Rectangle(doc.PageSize);
        //    pageBorderRect.Left += doc.LeftMargin;
        //    pageBorderRect.Right -= doc.RightMargin;
        //    pageBorderRect.Top -= doc.TopMargin;
            
        //    //符合內容長度的框線
        //    pageBorderRect.Bottom = writer.GetVerticalPosition(true);
        //    pageBorderRect.Bottom -= doc.BottomMargin;
        //    content.SetColorStroke(BaseColor.BLACK);
        //    content.Rectangle(pageBorderRect.Left, pageBorderRect.Bottom, pageBorderRect.Width,
        //                      pageBorderRect.Height);
        //    content.Stroke();
        //}

        protected void PrepareLogoImage(bool isEntrepot = false)
        {
            string imgLogo = isEntrepot ? HttpContext.Current.Server.MapPath("~/Themes/default/images/17Life/G2/KangTai.png") 
                : HttpContext.Current.Server.MapPath("~/Themes/default/images/17Life/G2/17PLOGO_BW_HD.png");
            pdfImgLogo = iTextSharp.text.Image.GetInstance(imgLogo);
            pdfImgLogo.WidthPercentage = 100;
        }

        protected void PrepareImages(string code, CouponCodeType type, bool isEntrepot = false)
        {
            PrepareLogoImage(isEntrepot);

            Image imgQrCode = CreateQRCode(code, 100, 100);
            pdfImgQrCode = iTextSharp.text.Image.GetInstance(imgQrCode, ImageFormat.Png);

            Image imgBarcode = new BarCode128(code, BarCode128.BarCode128Types.A).Paint();
            pdfImgBarcode = iTextSharp.text.Image.GetInstance(imgBarcode, ImageFormat.Png);
            
            //BarcodeEAN13 長度 12~13
            if (code.Length >= 12 && code.Length <= 13 && type == CouponCodeType.BarcodeEAN13) 
            {
                Image imgEAN13 = new BarCodeEAN13(code).Paint(1.5f, 200, 80, 21);
                pdfImgBarcodeEAN13 = iTextSharp.text.Image.GetInstance(imgEAN13, ImageFormat.Png);
            }
        }
        
        protected System.Drawing.Bitmap CreateQRCode(string code, int dQRHeight, int dQRWeight)
        {
            ByteMatrix byteMatrix = new MultiFormatWriter().encode(code, BarcodeFormat.QR_CODE, dQRHeight, dQRWeight);
            int width = byteMatrix.Width;
            int height = byteMatrix.Height;

            System.Drawing.Bitmap bmap = new System.Drawing.Bitmap(width, height, PixelFormat.Format32bppArgb);

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    bmap.SetPixel(x, y, byteMatrix.get_Renamed(x, y) != -1
                                            ? System.Drawing.ColorTranslator.FromHtml("Black")
                                            : System.Drawing.ColorTranslator.FromHtml("White"));
                }
            }
            return bmap.Clone(new Rectangle(16, 16, 68, 68), PixelFormat.Format32bppArgb);
        }
    }

    public class Fonts
    {
        private static readonly BaseFont bfChinese;

        static Fonts()
        {
            //細明體
            string fontPath = Environment.GetFolderPath(Environment.SpecialFolder.System) +
                              @"\..\Fonts\mingliu.ttc,0"; // ",0" is necessary, but i don't know why

            FontFactory.Register(fontPath);
            bfChinese = BaseFont.CreateFont(
                fontPath,
                BaseFont.IDENTITY_H, //橫式中文
                BaseFont.EMBEDDED
            );
        }

        #region 中文字型

        private static Font _mingliu7Bold;

        public static Font Mingliu7Bold
        {
            get
            {
                if (_mingliu7Bold == null)
                {
                    _mingliu7Bold = new Font(bfChinese, 7, Font.BOLD, BaseColor.BLACK);
                }
                return _mingliu7Bold;
            }
        }

        private static Font _mingliu10Bold;

        public static Font Mingliu10Bold
        {
            get
            {
                if (_mingliu10Bold == null)
                {
                    _mingliu10Bold = new Font(bfChinese, 10, Font.BOLD, BaseColor.BLACK);
                }
                return _mingliu10Bold;
            }
        }

        private static Font _mingliu8;

        public static Font Mingliu8
        {
            get
            {
                if (_mingliu8 == null)
                {
                    _mingliu8 = new Font(bfChinese, 8, Font.NORMAL, BaseColor.BLACK);
                }
                return _mingliu8;
            }
        }

        private static Font _mingliu8Bold;

        public static Font Mingliu8Bold
        {
            get
            {
                if (_mingliu8Bold == null)
                {
                    _mingliu8Bold = new Font(bfChinese, 8, Font.BOLD, BaseColor.BLACK);
                }
                return _mingliu8Bold;
            }
        }

        private static Font _mingliu9;

        public static Font Mingliu9
        {
            get
            {
                if (_mingliu9 == null)
                {
                    _mingliu9 = new Font(bfChinese, 9, Font.NORMAL, BaseColor.BLACK);
                }
                return _mingliu9;
            }
        }

        private static Font _mingliu10;

        public static Font Mingliu10
        {
            get
            {
                if (_mingliu10 == null)
                {
                    _mingliu10 = new Font(bfChinese, 10, Font.NORMAL, BaseColor.BLACK);
                }
                return _mingliu10;
            }
        }

        private static Font _mingliu11;

        public static Font Mingliu11
        {
            get
            {
                if (_mingliu11 == null)
                {
                    _mingliu11 = new Font(bfChinese, 11, Font.NORMAL, BaseColor.BLACK);
                }
                return _mingliu11;
            }
        }

        private static Font _mingliu12;

        public static Font Mingliu12
        {
            get
            {
                if (_mingliu12 == null)
                {
                    _mingliu12 = new Font(bfChinese, 12, Font.NORMAL, BaseColor.BLACK);
                }
                return _mingliu12;
            }
        }

        #endregion 中文字型

        #region 英文字型

        private static Font _arial8;

        public static Font Arial8
        {
            get
            {
                if (_arial8 == null)
                {
                    _arial8 = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.BLACK);
                }
                return _arial8;
            }
        }

        private static Font _arial9;

        public static Font Arial9
        {
            get
            {
                if (_arial9 == null)
                {
                    _arial9 = FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK);
                }
                return _arial9;
            }
        }

        private static Font _arial10;

        public static Font Arial10
        {
            get
            {
                if (_arial10 == null)
                {
                    _arial10 = FontFactory.GetFont("Arial", 10, Font.NORMAL, BaseColor.BLACK);
                }
                return _arial10;
            }
        }

        private static Font _arial10Strikethru;

        public static Font Arial10Strikethru
        {
            get
            {
                if (_arial10Strikethru == null)
                {
                    _arial10Strikethru = FontFactory.GetFont("Arial", 10, Font.STRIKETHRU, BaseColor.BLACK);
                }
                return _arial10Strikethru;
            }
        }

        private static Font _arial7Bold;

        public static Font Arial7Bold
        {
            get
            {
                if (_arial7Bold == null)
                {
                    _arial7Bold = FontFactory.GetFont("Arial", 7, Font.BOLD, BaseColor.BLACK);
                }
                return _arial7Bold;
            }
        }

        private static Font _arial11Bold;

        public static Font Arial11Bold
        {
            get
            {
                if (_arial11Bold == null)
                {
                    _arial11Bold = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.BLACK);
                }
                return _arial11Bold;
            }
        }

        private static Font _arial12Bold;

        public static Font Arial12Bold
        {
            get
            {
                if (_arial12Bold == null)
                {
                    _arial12Bold = FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.BLACK);
                }
                return _arial12Bold;
            }
        }

        private static Font _arial13Bold;

        public static Font Arial13Bold
        {
            get
            {
                if (_arial13Bold == null)
                {
                    _arial13Bold = FontFactory.GetFont("Arial", 13, Font.BOLD, BaseColor.BLACK);
                }
                return _arial13Bold;
            }
        }

        #endregion 英文字型
    }
}

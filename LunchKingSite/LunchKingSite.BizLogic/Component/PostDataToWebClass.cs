﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace LunchKingSite.BizLogic.Component
{
    public class PostDataToWebClass
    {
        string _url = string.Empty;
        string _data = string.Empty;
        Dictionary<string, string> _header = new Dictionary<string, string>();

        public PostDataToWebClass(string url, string data, Dictionary<string, string> header)
        {
            this._url = url;
            this._data = data;
            this._header = header;
        }

        public string PostData()
        {
            return Post(string.Empty);
        }

        public string PostData(string szContentType)
        {
            return Post(szContentType);
        }

        private string Post(string szContentType)
        {
            string responseFromServer = "";
            try
            {
                if (szContentType == string.Empty) { szContentType = "application/x-www-form-urlencoded"; }

                WebRequest requset = WebRequest.Create(this._url);

                requset.Method = "POST";

                foreach (KeyValuePair<string, string> k in _header)
                {
                    requset.Headers.Add(k.Key, k.Value);
                }

                byte[] byteArray = Encoding.UTF8.GetBytes(this._data);

                requset.ContentType = szContentType;

                requset.ContentLength = byteArray.Length;

                Stream dataStream = requset.GetRequestStream();

                dataStream.Write(byteArray, 0, byteArray.Length);

                dataStream.Close();

                WebResponse response = requset.GetResponse();

                dataStream = response.GetResponseStream();

                StreamReader reader = new StreamReader(dataStream);

                responseFromServer = reader.ReadToEnd();

                reader.Close();

                dataStream.Close();

                response.Close();
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            responseFromServer = reader.ReadToEnd();
                            //TODO: use JSON.net to parse this string and look at the error message
                        }
                    }
                }
            }

            return responseFromServer;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

using JsonSerializer = LunchKingSite.Core.JsonSerializer;

namespace LunchKingSite.BizLogic.Component
{
    public class LinePayUtility
    {
        public const string LinePaySuccessCode = "0000"; //Line Pay Success Code
        public const string LinePaySimulatedFail = "99999"; //模擬失敗
        //可以 Retry 的 Code 請參考本檔案 GetReturnCodeMsg Method. 
        private static List<string> canRetryCode = new List<string> { "1145", "1198", "9000", "99999" };
        private static IOrderProvider _op;
        private static IMemberProvider _mp;
        private static ISysConfProvider _config;

        static LinePayUtility()
        {
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _config = ProviderFactory.Instance().GetConfig();
        }

        public static LinePayTransLog MakeLinePayTranscation(string ticketId, string transactionId)
        {
            var trans = new LinePayTransLog
            {
                TicketId = ticketId,
                TransactionId = transactionId,
                TransStatus = (byte)LinePayTransStatus.Created,
                CreateTime = DateTime.Now,
                ModifyTime = DateTime.Now,
                IsCaptureSeparate = !_config.LinePayCaptureWithConfirm,
                AlreadyRefundTotal = 0,
                BuyFailRefundRetryTimes = 0
            };
            return _op.LinePayTransLogSet(trans);
        }

        #region LinePay Api

        /// <summary>
        /// 呼叫 Reservice Api 建立 Line Pay 交易
        /// </summary>
        /// <param name="model"></param>
        /// <param name="output"></param>
        /// <returns>Result:Success/Fail, if fail show StatusCode</returns>
        private static KeyValuePair<bool, string> CreateReserve(ReserveInputModel model, out ReserveOutputModel output)
        {
            output = new ReserveOutputModel();
            var request = (HttpWebRequest) WebRequest.Create(_config.LinePayApiReserve);
            request.Method = WebRequestMethods.Http.Post;
            request.Headers.Add("X-LINE-ChannelId", _config.LinePayChannelId);
            request.Headers.Add("X-LINE-ChannelSecret", _config.LinePayChannelSecretKey);

            var jsonText = new JsonSerializer().Serialize(model);
            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;
            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                requestStream.Flush();
            }
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return new KeyValuePair<bool, string>(false, response.StatusCode.ToString());
                    }

                    using (var reader = new StreamReader(stream))
                    {
                        var result = reader.ReadToEnd();
                        output = new JsonSerializer().Deserialize<ReserveOutputModel>(result);
                        return new KeyValuePair<bool, string>(true, result);
                    }
                }
            }
        }

        /// <summary>
        /// 確認授權API
        /// </summary>
        /// <param name="lineTransactionId"></param>
        /// <param name="amount"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        private static KeyValuePair<bool, string> ConfirmToPay(string lineTransactionId, int amount, out ConfirmAndCaptureModel output)
        {
            output = new ConfirmAndCaptureModel();
            var request = (HttpWebRequest)WebRequest.Create(string.Format(_config.LinePayApiConfirm, lineTransactionId));
            request.Method = WebRequestMethods.Http.Post;
            request.Headers.Add("X-LINE-ChannelId", _config.LinePayChannelId);
            request.Headers.Add("X-LINE-ChannelSecret", _config.LinePayChannelSecretKey);

            var jsonText = new JsonSerializer().Serialize(new { amount, currency = "TWD"});
            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;
            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                requestStream.Flush();
            }
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return new KeyValuePair<bool, string>(false, response.StatusCode.ToString());
                    }

                    using (var reader = new StreamReader(stream))
                    {
                        string result = reader.ReadToEnd();
                        output = new JsonSerializer().Deserialize<ConfirmAndCaptureModel>(result);
                        return new KeyValuePair<bool, string>(true, result);
                    }
                }
            }
        }

        /// <summary>
        /// 請款API
        /// </summary>
        /// <param name="lineTransactionId"></param>
        /// <param name="amount"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        private static KeyValuePair<bool, string> CaptureMoney(string lineTransactionId, int amount, out ConfirmAndCaptureModel output)
        {
            output = new ConfirmAndCaptureModel();
            var request = (HttpWebRequest)WebRequest.Create(string.Format(_config.LinePayApiCapture, lineTransactionId));
            request.Method = WebRequestMethods.Http.Post;
            request.Headers.Add("X-LINE-ChannelId", _config.LinePayChannelId);
            request.Headers.Add("X-LINE-ChannelSecret", _config.LinePayChannelSecretKey);

            var jsonText = new JsonSerializer().Serialize(new { amount, currency = "TWD" });
            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;
            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                requestStream.Flush();
            }
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return new KeyValuePair<bool, string>(false, response.StatusCode.ToString());
                    }

                    using (var reader = new StreamReader(stream))
                    {
                        string result = reader.ReadToEnd();
                        output = new JsonSerializer().Deserialize<ConfirmAndCaptureModel>(result);
                        return new KeyValuePair<bool, string>(true, result);
                    }
                }
            }
        }

        /// <summary>
        /// 授權作廢
        /// </summary>
        /// <param name="lineTransactionId"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        private static KeyValuePair<bool, string> VoidApi(string lineTransactionId, out LinePayBaseOutputModel output)
        {
            output = new LinePayBaseOutputModel();
            var request = (HttpWebRequest)WebRequest.Create(string.Format(_config.LinePayApiCapture, lineTransactionId));
            request.Method = WebRequestMethods.Http.Post;
            request.Headers.Add("X-LINE-ChannelId", _config.LinePayChannelId);
            request.Headers.Add("X-LINE-ChannelSecret", _config.LinePayChannelSecretKey);

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return new KeyValuePair<bool, string>(false, response.StatusCode.ToString());
                    }

                    using (var reader = new StreamReader(stream))
                    {
                        string result = reader.ReadToEnd();
                        output = new JsonSerializer().Deserialize<LinePayBaseOutputModel>(result);
                        return new KeyValuePair<bool, string>(true, result);
                    }
                }
            }
        }

        /// <summary>
        /// 退款API
        /// </summary>
        /// <param name="lineTransactionId"></param>
        /// <param name="amount"></param>
        /// <param name="output"></param>
        /// <param name="simulatedFail">模擬失敗</param>
        /// <returns></returns>
        private static KeyValuePair<bool, string> RefundApi(string lineTransactionId, int amount, out RefundOutputModel output, bool simulatedFail = false)
        {
            output = new RefundOutputModel();
            if (simulatedFail)
            {
                output.ReturnCode = LinePaySimulatedFail;
                output.ReturnMessage = "simulated fail";
                return new KeyValuePair<bool, string>(false, new JsonSerializer().Serialize(output));
            }

            var request = (HttpWebRequest)WebRequest.Create(string.Format(_config.LinePayApiRefund, lineTransactionId));
            request.Method = WebRequestMethods.Http.Post;
            request.Headers.Add("X-LINE-ChannelId", _config.LinePayChannelId);
            request.Headers.Add("X-LINE-ChannelSecret", _config.LinePayChannelSecretKey);

            var jsonText = new JsonSerializer().Serialize(new { refundAmount = amount });
            var jsonBytes = Encoding.UTF8.GetBytes(jsonText);
            request.ContentType = "application/json";
            request.ContentLength = jsonBytes.Length;
            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                requestStream.Flush();
            }
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return new KeyValuePair<bool, string>(false, response.StatusCode.ToString());
                    }

                    using (var reader = new StreamReader(stream))
                    {
                        string result = reader.ReadToEnd();
                        output = new JsonSerializer().Deserialize<RefundOutputModel>(result);
                        return new KeyValuePair<bool, string>(true, result);
                    }
                }
            }
        }

        #endregion LinePay Api

        #region Method

        /// <summary>
        /// 建立 Line Pay 交易 / create reserve
        /// </summary>
        /// <param name="lineTrans"></param>
        /// <param name="reserve"></param>
        /// <returns>Success:true/string.empty, Fail:false/fail message</returns>
        public static KeyValuePair<bool, string> CreateReserve(LinePayTransLog lineTrans, ReserveInputModel reserve, bool openInApp)
        {
            ReserveOutputModel outputModel;
            var result = CreateReserve(reserve, out outputModel);
            if (!result.Key)
            {
                string resultVal = string.Format("連線Line Pay伺服器發生錯誤({0})", result.Value);
                lineTrans.TransStatus = (byte)LinePayTransStatus.ReserveApiConnectFail;
                lineTrans.TransMsg = resultVal;
                _op.LinePayTransLogSet(lineTrans);
                return new KeyValuePair<bool, string>(false, resultVal);
            }
            AddLinePayApiLog(lineTrans.Id, LinePayResultHistoryType.Reserve
                , outputModel.ReturnCode, outputModel.ReturnMessage, result.Value);

            if (outputModel.ReturnCode != LinePaySuccessCode)
            {
                lineTrans.TransStatus = (byte)LinePayTransStatus.GetReserveFail;
                var failReson = GetReturnCodeMsg(outputModel.ReturnCode);
                _op.LinePayTransLogSet(lineTrans);
                return new KeyValuePair<bool, string>(false, string.Format("請求Line Pay伺服器失敗({0})", failReson));
            }
            lineTrans.LinePayTransactionId = outputModel.Info.TransactionId;
            //將狀態改為已去要求LinePay授權
            lineTrans.TransStatus = (byte)LinePayTransStatus.GetReserveSuccessAndRedirect;
            _op.LinePayTransLogSet(lineTrans);
            return new KeyValuePair<bool, string>(true, outputModel.Info.PaymentUrl.Where(x => openInApp ? x.Key == "app" : x.Key == "web").Select(x => x.Value).First());
        }

        /// <summary>
        /// 授權Api (授權即請款與否視小config LinePayIsCaptureSeparate設定)
        /// </summary>
        /// <param name="linePayTrans"></param>
        /// <param name="confirmAmount"></param>
        /// <returns>Tuple SuccessResult(成功失敗結果)/ReturnCode(LinePay回傳碼)/ReturnMessage(結果訊息)></returns>
        public static Tuple<bool, int, string> Confirm(LinePayTransLog linePayTrans, int confirmAmount)
        {
            ConfirmAndCaptureModel lineResultData;
            var lineConfirmResult = ConfirmToPay(linePayTrans.LinePayTransactionId, confirmAmount, out lineResultData);

            if (!lineConfirmResult.Key)
            {
                string resultVal = string.Format("連線Line Pay伺服器發生錯誤({0}), confirm trans id:{1}", lineConfirmResult.Value
                    , linePayTrans.LinePayTransactionId);
                linePayTrans.TransMsg = resultVal;
                linePayTrans.TransStatus = (byte)LinePayTransStatus.ConfirmApiConnectFail;
                _op.LinePayTransLogSet(linePayTrans);
                return new Tuple<bool, int, string>(false, 0, resultVal);
            }

            AddLinePayApiLog(linePayTrans.Id, LinePayResultHistoryType.Confirm
                , lineResultData.ReturnCode, lineResultData.ReturnMessage, lineConfirmResult.Value);
            linePayTrans.TransStatus = (byte)LinePayTransStatus.ConfirmSuccess;

            if (lineResultData.ReturnCode != LinePaySuccessCode)
            {
                int returnCode = 0;
                int.TryParse(lineResultData.ReturnCode, out returnCode);
                linePayTrans.TransStatus = (byte)LinePayTransStatus.ConfirmFail;
                _op.LinePayTransLogSet(linePayTrans);
                return new Tuple<bool, int, string>(false, returnCode, string.Format("LinePay {0}(line trans id:{1})",
                    GetReturnCodeMsg(lineResultData.ReturnCode),
                    linePayTrans.LinePayTransactionId));
            }
            _op.LinePayTransLogSet(linePayTrans);
            return new Tuple<bool, int, string>(true, (int)PayTransResponseType.OK, string.Format("LinePay {0}(line trans id:{1})",
                GetReturnCodeMsg(lineResultData.ReturnCode),
                linePayTrans.LinePayTransactionId));
        }

        public static bool Capture(LinePayTransLog linePayTrans, int refundAmount)
        {
            var result = false;
            ConfirmAndCaptureModel linePayModel;
            var captureResult = CaptureMoney(linePayTrans.LinePayTransactionId, refundAmount, out linePayModel);
            if (!captureResult.Key)
            {
                linePayTrans.TransMsg = string.Format("請款時連線Line Pay伺服器發生錯誤({0})", captureResult.Value);
                linePayTrans.TransStatus = (byte)LinePayTransStatus.CaptureConnectFail;
            }
            else
            {
                AddLinePayApiLog(linePayTrans.Id, LinePayResultHistoryType.Capture
                    , linePayModel.ReturnCode, linePayModel.ReturnMessage, captureResult.Value);
                linePayTrans.TransStatus = linePayModel.ReturnCode == LinePaySuccessCode
                    ? (byte)LinePayTransStatus.CaptureSuccess
                    : (byte)LinePayTransStatus.CaptureFail;
                if (linePayModel.ReturnCode == LinePaySuccessCode)
                {
                    result = true;
                }
            }
            _op.LinePayTransLogSet(linePayTrans);
            return result;
        }

        /// <summary>
        /// 取消授權
        /// </summary>
        /// <param name="linePayTrans"></param>
        public static void VoidTrans(LinePayTransLog linePayTrans)
        {
            LinePayBaseOutputModel linePayModel;
            var captureResult = VoidApi(linePayTrans.LinePayTransactionId, out linePayModel);
            if (!captureResult.Key)
            {
                linePayTrans.TransMsg = string.Format("取消授權時連線Line Pay伺服器發生錯誤({0})",
                    captureResult.Value);
                linePayTrans.TransStatus = (byte)LinePayTransStatus.VoidConnectFail;
            }
            else
            {
                AddLinePayApiLog(linePayTrans.Id, LinePayResultHistoryType.Void
                    , linePayModel.ReturnCode, linePayModel.ReturnMessage, captureResult.Value);
                linePayTrans.TransStatus = linePayModel.ReturnCode == LinePaySuccessCode
                    ? (byte)LinePayTransStatus.VoidSuccess
                    : (byte)LinePayTransStatus.VoidFail;
            }
            _op.LinePayTransLogSet(linePayTrans);
        }

        public static bool Refund(LinePayRefundLog refundLog, LinePayTransLog lineLog, CashTrustLog ctl, out bool canRetry, out string failReason, string userId = "sys", bool simulatedFail = false)
        {
            var result = false;
            failReason = string.Empty;
            canRetry = false;
            var ptLog = _op.PaymentTransactionGet(refundLog.PaymentTransactionId);
            var returnForm = new ReturnFormEntity(refundLog.ReturnFormId);

            #region check

            //check object exist
            if (!ptLog.IsLoaded || !ctl.IsLoaded)
            {
                return false;
            }

            //check line pay refund status
            var checkStatus = new List<LinePayRefundStatus>
            {
                LinePayRefundStatus.RefundSuccess,
                LinePayRefundStatus.Requested
            };

            if (checkStatus.Contains((LinePayRefundStatus) refundLog.RefundStatus))
            {
                return false;
            }

            //check payment transaction status
            if (Helper.GetPaymentTransactionPhase(ptLog.Status) == PayTransPhase.Successful)
            {
                return false;
            }

            #endregion check

            #region update status set request
            
            ptLog.Status = Helper.SetPaymentTransactionPhase(ptLog.Status, PayTransPhase.Requested);
            _op.PaymentTransactionSet(ptLog);

            refundLog.RefundStatus = (byte)LinePayRefundStatus.Requested;
            _op.LinePayRefundLogSet(refundLog);

            #endregion update status set request

            RefundOutputModel linePayModel;
            var refundResult = RefundApi(lineLog.LinePayTransactionId, (int)ptLog.Amount,
                out linePayModel, simulatedFail);
            
            if (!refundResult.Key)
            {
                failReason = refundLog.RefundMsg = string.Format("Line Pay伺服器連線錯誤, 退款失敗({0})",
                    refundResult.Value);
                refundLog.RefundStatus = (byte)LinePayRefundStatus.RequestedFail;
                canRetry = refundLog.CanRetry = true;
            }
            else
            {
                AddLinePayApiLog(lineLog.Id, LinePayResultHistoryType.Refund
                    , linePayModel.ReturnCode, linePayModel.ReturnMessage, refundResult.Value);
                refundLog.RefundStatus = linePayModel.ReturnCode == LinePaySuccessCode
                    ? (byte)LinePayRefundStatus.RefundSuccess
                    : (byte)LinePayRefundStatus.RequestedFail;

                PayTransPhase ptp = PayTransPhase.Failed;
                ptLog.TransTime = DateTime.Now;

                if (refundLog.RefundStatus == (byte) LinePayRefundStatus.RefundSuccess)
                {
                    ptp = PayTransPhase.Successful;
                    result = true;
                    lineLog.AlreadyRefundTotal += (int) ptLog.Amount;
                }
                else
                {
                    failReason = GetReturnCodeMsg(linePayModel.ReturnCode);
                    if (canRetryCode.Contains(linePayModel.ReturnCode))
                    {
                        canRetry = refundLog.CanRetry = true;
                    }
                }

                ptLog.Status = Helper.SetPaymentTransactionPhase(ptLog.Status, ptp);
                _op.PaymentTransactionSet(ptLog);
            }
            
            _op.LinePayTransLogSet(lineLog);
            _op.LinePayRefundLogSet(refundLog, userId);
            return result;
        }

        ///  <summary>
        ///  buy頁購買失敗 Line Pay 退款
        ///  </summary>
        ///  <param name="linePayTrans"></param>
        ///  <param name="refundAmount"></param>
        /// <param name="simulatedFail"></param>
        /// <param name="isOrderFail"></param>
        public static bool BuyFailRefund(LinePayTransLog linePayTrans, int refundAmount, bool simulatedFail = false, bool isOrderFail = false)
        {
            bool result = false;
            RefundOutputModel linePayModel;
            var refundResult = RefundApi(linePayTrans.LinePayTransactionId, refundAmount,
                out linePayModel, simulatedFail);
            if (!refundResult.Key)
            {
                linePayTrans.TransMsg = string.Format("交易失敗退款時連線Line Pay伺服器發生錯誤({0})",
                    refundResult.Value);
                linePayTrans.TransStatus = (byte)LinePayTransStatus.BuyFailRefundConnectFail;
                linePayTrans.CanRetry = true;
            }
            else
            {
                AddLinePayApiLog(linePayTrans.Id, LinePayResultHistoryType.Refund
                    , linePayModel.ReturnCode, linePayModel.ReturnMessage, refundResult.Value);
                
                if (linePayModel.ReturnCode == LinePaySuccessCode)
                {
                    if (isOrderFail)
                    {
                        linePayTrans.TransStatus = (byte) LinePayTransStatus.OrderFailRefundSuccess;
                    }
                    else
                    {
                        linePayTrans.TransStatus = (byte) LinePayTransStatus.BuyFailRefundSuccess;
                    }
                }
                else
                {
                    if (isOrderFail)
                    {
                        linePayTrans.TransStatus = (byte) LinePayTransStatus.OrderFailRefundFail;
                    }
                    else
                    {
                        linePayTrans.TransStatus = (byte) LinePayTransStatus.BuyFailRefundFail;
                    }
                }

                if (linePayTrans.TransStatus == (byte) LinePayTransStatus.BuyFailRefundSuccess 
                    || linePayTrans.TransStatus == (byte)LinePayTransStatus.OrderFailRefundSuccess)
                {
                    result = true;
                    linePayTrans.AlreadyRefundTotal += refundAmount;
                }
                else
                {
                    if (canRetryCode.Contains(linePayModel.ReturnCode))
                    {
                        linePayTrans.CanRetry = true;
                    }
                    linePayTrans.TransMsg = GetReturnCodeMsg(linePayModel.ReturnCode);
                }
            }
            _op.LinePayTransLogSet(linePayTrans);
            return result;
        }

        #endregion Method

        public static void AddLinePayApiLog(int transLogId, LinePayResultHistoryType resultType, string returnCode, string returnMsg, string returnJson)
        {
            var data = new LinePayApiLog
            {
                LinePayTransId = transLogId,
                LogType = (byte)resultType,
                ReturnCode = returnCode,
                CodeMeaning = GetReturnCodeMsg(returnCode),
                ReturnMsg = returnMsg,
                ReturnObj = returnJson,
                LogTime = DateTime.Now
            };
            _op.LinePayApiLogSet(data);
        }

        public static string GetReturnCodeMsg(string returnCode)
        {
            switch (returnCode)
            {
                case "0000":
                    return "Success";
                case "1101":
                case "1102":
                    return "買方被停止交易";
                case "1104":
                    return "找不到商家";
                case "1105":
                    return "此商家無法使用Line Pay";
                case "1106":
                    return "標頭資訊錯誤";
                case "1110":
                    return "無法使用的信用卡";
                case "1124":
                    return "金額錯誤(scale)";
                case "1141":
                    return "付款帳戶狀態錯誤";
                case "1142":
                    return "餘額不足";
                case "1145":
                    return "正在進行付款";
                case "1150":
                    return "找不到交易記錄";
                case "1152":
                    return "已有保存付款";
                case "1153":
                    return "付款reserve時的金額與申請的金額不一致";
                case "1154":
                    return "自動付款帳戶無法使用";
                case "1155":
                    return "交易編號不符合退款資格";
                case "1159":
                    return "無付款申請資訊";
                case "1164":
                    return "超過退款額度";
                case "1165":
                    return "交易已進行退款";
                case "1169":
                    return "付款confirm所需要資訊錯誤(在Line Pay)";
                case "1170":
                    return "使用者帳戶的餘額有變動";
                case "1172":
                    return "已有同一訂單編號的交易資料";
                case "1177":
                    return "超過允許擷取的交易數目(100)";
                case "1178":
                    return "不支援的貨幣";
                case "1179":
                    return "無法處理狀態";
                case "1180":
                    return "付款時限已過";
                case "1183":
                    return "付款金額必須大於0";
                case "1184":
                    return "付款金額比付款申請時候的金額還大";
                case "1190":
                    return "regKey 不存在";
                case "1193":
                    return "regKey 已過期";
                case "1194":
                    return "此商家無法使用自動付款";
                case "1198":
                    return "正在處理請求...";
                case "1199":
                    return "內部請求錯誤";
                case "1281":
                    return "信用卡付款錯誤";
                case "1282":
                    return "信用卡授權錯誤";
                case "1283":
                    return "因疑似詐騙，拒絕付款";
                case "1284":
                    return "暫時無法以信用卡付款";
                case "1285":
                    return "信用卡資訊不完整";
                case "1286":
                    return "信用卡付款資訊不正確";
                case "1287":
                    return "信用卡已過期";
                case "1288":
                    return "信用卡額度不足";
                case "1289":
                    return "超過信用卡付款金額上限";
                case "1290":
                    return "超過一次性付款的額度";
                case "1291":
                    return "此信用卡已被掛失";
                case "1292":
                    return "此信用卡已被停卡";
                case "1293":
                    return "信用卡驗證碼(CVN)無效";
                case "1294":
                    return "此信用卡已被列入黑名單";
                case "1295":
                    return "信用卡號無效";
                case "1296":
                    return "無效的金額";
                case "1298":
                    return "信用卡付款遭拒";
                case "2101":
                    return "參數錯誤";
                case "2102":
                    return "JSON資料格式錯誤";
                case "9000":
                    return "Line內部錯誤";
                case "99999":
                    return "模擬失敗";
                default:
                    return "未定義";
            }
        }
    }
}

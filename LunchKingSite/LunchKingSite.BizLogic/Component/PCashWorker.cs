﻿using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace LunchKingSite.BizLogic.Component
{
    public class PCashWorker
    {
        private static ILog log = LogManager.GetLogger(typeof(PCashWorker));

        #region Property

        private static IMemberProvider mp;
        private static IOrderProvider op;

        #endregion Property

        static PCashWorker()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        #region Method

        public static bool IsWelfareMember(string PEZId)
        {
            try
            {
                if (string.IsNullOrEmpty(PEZId))
                {
                    return false;
                }
                else
                {
                    string strContext = string.Format(PCashContext.GetUserRemaining(), PEZId);
                    string strResult = PostToPEZ(strContext);
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(strResult);
                    XmlNodeList xns = xd.GetElementsByTagName("IsPezMember");
                    if (xns.Count <= 0)
                    {
                        return false;
                    }

                    if (xns[0].InnerText == "0")
                    {
                        return false;
                    }
                    else
                    {
                        XmlNodeList xnR = xd.GetElementsByTagName("IsWelfareMember");
                        if (xnR.Count > 0)
                        {
                            if (xnR[0].InnerText == "1")
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 再次確認目前登入者與 PEZId 為同使用者
        /// 如果很久都沒收到log信，可以移掉這個檢查。
        /// </summary>
        /// <param name="PEZId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static decimal CheckUserRemaining(string PEZId, int userId)
        {
            MemberLink ml = mp.MemberLinkGetByExternalId(PEZId, SingleSignOnSource.PayEasy);
            if (ml.IsLoaded == false)
            {
                log.Warn(string.Format("偵測到 PCash 查詢餘額異常, 查詢不到 MemberLink，PEZId = {0}, userId={1}", PEZId, userId),
                    LogWith.LoginIdAndUrl);
                return 0;
            }

            if (ml.UserId != userId)
            {
                log.Warn(string.Format("偵測到 PCash 查詢餘額異常, PEZ查詢的身份跟登入者的身份不合，PEZId = {0}, userId={1}", PEZId, userId),
                    LogWith.LoginIdAndUrl);
                return 0;
            }

            return CheckUserRemaining(PEZId);
        }

        public static decimal CheckUserRemaining(string PEZId)
        {
            //因 自2015/5/1起，pcash無法使用，所以所有查詢都回傳0，避免再跟pez查詢
            return 0;   
        }

        public static string CheckOut(string PEZId, string transID, decimal cost, DepartmentTypes deptType, out string authCode)
        {
            authCode = string.Empty;
            if (string.IsNullOrEmpty(PEZId))
            {
                log.Error("PEZId空值，TransID:" + transID);
                throw new Exception("PEZId空值，TransID:" + transID);
            }
            else
            {
                #region PostToPEZ

                string strResult = PostToPEZ(PCashContext.GetChargeContext(transID, PEZId, cost, "17P好康"));

                #endregion PostToPEZ

                try
                {
                    #region CheckResult

                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(strResult);
                    XmlNodeList xns = xd.GetElementsByTagName("ReturnCode");
                    if (xns.Count <= 0)
                    {
                        return "no result";
                    }

                    #endregion CheckResult

                    #region AuthCode

                    XmlNodeList xnAuthCode = xd.GetElementsByTagName("PezAuthCode");
                    if (xnAuthCode.Count > 0)
                    {
                        authCode = xnAuthCode[0].InnerText;
                    }

                    #endregion AuthCode

                    if (xns[0].InnerText == "0000")
                    {
                        return xns[0].InnerText;
                    }
                    else
                    {
                        return xns[0].InnerText + "(" + PCashContext.GetChargeContext(transID, PEZId, cost, "17P好康") + ")";
                    }
                }
                catch (Exception ex)
                {
                    log.Error("PCash讀取結果錯誤:接收到的XML為:" + strResult);
                    throw ex;
                }
            }
        }

        public static string DeCheckOut(Guid orderGuid, out string authCode)
        {
            authCode = string.Empty;

            PaymentTransactionCollection ptc = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                        PaymentTransaction.Columns.OrderGuid + " = " + orderGuid,
                                        PaymentTransaction.Columns.PaymentType + " = " + (int)PaymentType.PCash);

            if (ptc.Count > 0)
            {
                string context = PCashContext.GetDeChargeContent(ptc[0].TransId, ptc[0].AuthCode, ptc[0].Amount);
                string result = PostToPEZ(context);

                #region CheckResult

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(result);
                XmlNodeList xns = xd.GetElementsByTagName("ReturnCode");
                if (xns.Count <= 0)
                {
                    return "no result";
                }

                #endregion CheckResult

                #region AuthCode

                XmlNodeList xnAuthCode = xd.GetElementsByTagName("PezAuthCode");
                if (xnAuthCode.Count > 0)
                {
                    authCode = xnAuthCode[0].InnerText;
                }

                #endregion AuthCode

                if (xns[0].InnerText == "0000")
                {
                    return xns[0].InnerText;
                }
                else
                {
                    return xns[0].InnerText + "(" + context + ")";
                }
            }
            else
            {
                return "沒有PCash請款資料";
            }
        }

        public static string DeCheckOut(string transId, string authCode, decimal amount)
        {
            string context = PCashContext.GetDeChargeContent(transId, authCode, amount);
            string result = PostToPEZ(context);

            #region CheckResult

            XmlDocument xd = new XmlDocument();
            xd.LoadXml(result);
            XmlNodeList xns = xd.GetElementsByTagName("ReturnCode");
            if (xns.Count <= 0)
            {
                return "no result";
            }

            #endregion CheckResult

            #region AuthCode

            XmlNodeList xnAuthCode = xd.GetElementsByTagName("PezAuthCode");
            if (xnAuthCode.Count > 0)
            {
                authCode = xnAuthCode[0].InnerText;
            }

            #endregion AuthCode

            if (xns[0].InnerText == "0000")
            {
                return xns[0].InnerText;
            }
            else
            {
                return xns[0].InnerText + "(" + context + ")";
            }
        }

        #endregion Method

        #region PostMethod

        public static string PostToPEZ(string context)
        {
            ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
            string strUrl = _config.PCashAPI;

            #region 傳送資料

            byte[] data = Encoding.ASCII.GetBytes(context);
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strUrl);
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = data.Length;
            using (Stream stm = req.GetRequestStream())
            {
                stm.Write(data, 0, data.Length);
                stm.Close();
            }

            #endregion 傳送資料

            #region 取得回傳資料

            try
            {
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                if (res != null && res.StatusCode == HttpStatusCode.OK)
                {
                    string line = string.Empty;
                    using (StreamReader rdr = new StreamReader(res.GetResponseStream()))
                    {
                        line = rdr.ReadToEnd();
                        rdr.Close();
                    }

                    return line;
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return "false";
            }

            #endregion 取得回傳資料
        }

        #endregion PostMethod
    }
}
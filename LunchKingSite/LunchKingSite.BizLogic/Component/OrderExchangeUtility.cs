﻿using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class OrderExchangeUtility
    {
        protected static IOrderProvider op;
        protected static IPponProvider pp;
        protected static ISysConfProvider config;
        static OrderExchangeUtility()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }
        
        #region 換貨相關

        public static IEnumerable<OrderReturnList> FindAllExchangeLog(Guid orderGuid)
        {
            return op.OrderReturnListGetListByType(orderGuid, (int)OrderReturnType.Exchange)
                        .OrderByDescending(x => x.CreateTime)
                        .ToList();
        }

        public static bool AllowExchange(Guid orderGuid, out string denyReason, bool skipCheck = false)
        {
            PponOrder order = new PponOrder(orderGuid);

            DealProperty dp = pp.DealPropertyGet(order.Bid);
            if (dp.DeliveryType == (int)DeliveryType.ToShop)
            {
                denyReason = "憑證訂單無提供換貨服務。";
                return false;
            }

            if (HasProcessingExchangeLog(orderGuid))
            {
                denyReason = "訂單已有處理中之換貨申請。";
                return false;
            }

            if (!order.Products.Any(x => x.IsCurrent &&
                                        !x.IsReturned &&
                                        !x.IsReturning &&
                                        !x.IsExchanging))
            {
                denyReason = "訂單無可換貨份數。";
                return false;
            }

            if (!PponOrderManager.CheckIsShipped(orderGuid, OrderClassification.LkSite))
            {
                denyReason = "商品尚未出貨，無法進行換貨。";
                return false;
            }

            if (PponOrderManager.CheckIsOverTrialPeriod(orderGuid))
            {
                denyReason = "商品已過鑑賞期，無法換貨。";
                return skipCheck;
            }

            if (config.IsConsignment && dp.Consignment)
            {
                denyReason = "商品不提供換貨服務。";
                return false;
            }

            denyReason = string.Empty;
            return true;
        }

        public static bool HasProcessingExchangeLog(Guid orderGuid)
        {
            return op.OrderReturnListGetListByType(orderGuid, (int)OrderReturnType.Exchange)
                        .Any(x => x.Status != (int)OrderReturnStatus.ExchangeCancel &&
                                  x.Status != (int)OrderReturnStatus.ExchangeSuccess &&
                                  x.Status != (int)OrderReturnStatus.ExchangeFailure);
        }

        #endregion 換貨相關
    }
}

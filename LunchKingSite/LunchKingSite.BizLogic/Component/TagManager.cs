﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class TagManager
    {
        public static ITrackTagBase CreateFatory(TrackTagInputModel tt, TagProvider tp)
        {
            switch (tp)
            {
                case TagProvider.MicroData:
                    return new DealMicroData(tt);
                case TagProvider.Scupio:
                    return new DealScupioData(tt);
                case TagProvider.GoogleGtag:
                    return new DealGtagData(tt);
                case TagProvider.GoogleGtagConversion:
                    return new DealGtagConversionData(tt);
                case TagProvider.UrADGoogleGtagConversion:
                    return new DealGtagConversionData(tt, tt.Config.UrADGoogleGtagConversionSendToData);
                case TagProvider.Yahoo:
                    return new DealYahooTagData(tt);
                default:
                    return null;
            }
        }
    }

    public enum TagProvider
    {
        MicroData,
        GoogleGtag,
        GoogleGtagConversion,
        UrADGoogleGtagConversion,
        Scupio,
        Yahoo
    }

    public interface ITrackTagBase
    {
        string GetJson();
    }

    public abstract class TrackTagBase : ITrackTagBase
    {
        public virtual string GetJson()
        {
            return string.Empty;
        }
    }

    public class TrackTagInputModel
    {
        public IViewPponDeal ViewPponDealData { get; set; }
        public ISysConfProvider Config { get; set; }
        public bool IsPiinLifeDeal { get; set; }
        public GtagPageType GtagPage { get; set; }
        public YahooEaType YahooEa { get; set; }
        //public ScupioPageId ScupioPageId { get; set; }
        public Guid OrderGuid { get; set; }
        public int OrderTotalAmountWithoutDiscount { get; set; }
        public ScupioEventCategory ScupioEventCategory { get; set; }
        public ScupioPageType ScupioPageType { get; set; }
        public IList<OrderDetail> OrderDetails { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Component
{
    public class SKMOpenAPIGeneralExchange
    {
        private string _appKey = string.Empty;
        private string _appSecret = string.Empty;
        private string _apiUrl = string.Empty;

        public SKMOpenAPIGeneralExchange(string appKey, string appSecret, string apiUrl)
        {
            this._apiUrl = apiUrl;
            this._appSecret = appSecret;
            this._appKey = appKey;
        }

        public string ExchangeInfo(Dictionary<string, string> data, string pampasCall)
        {
            string putData = JsonConvert.SerializeObject(data);

            return ExchangeInfo(putData, pampasCall);

        }

        public string ExchangeInfo(string data, string pampasCall)
        {
            //取得參數資料
            Dictionary<string, string> param = new Dictionary<string, string> { };

            param.Add("appKey", this._appKey);
            param.Add("data", data);
            param.Add("pampasCall", pampasCall);

            string sign = SignParam(param, this._appSecret);

            param.Add("sign", sign);

            string putInfo = JsonConvert.SerializeObject(param);

            //header制作
            Dictionary<string, string> myHeader = new Dictionary<string, string>();

            myHeader.Add("x-op-req-body", "true");

            string result = new PostDataToWebClass(this._apiUrl, putInfo, myHeader).PostData("Application/JSON");

            return result;
        }


        private static string SignParam(Dictionary<string, string> param, string appSecret)
        {
            Dictionary<string, string> paramAsc = param.OrderBy(o => o.Key).ToDictionary(o => o.Key, p => p.Value);

            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> k in paramAsc)
            {
                sb.Append(string.Format("&{0}={1}", k.Key, k.Value));
            }

            sb.Append(appSecret);

            string szResult = sb.ToString().TrimStart('&');

            string sign = Md5Encode(szResult);

            return sign;
        }


        private static string Md5Encode(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }
}

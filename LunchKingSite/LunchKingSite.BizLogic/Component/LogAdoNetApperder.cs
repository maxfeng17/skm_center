﻿using log4net.Appender;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Data;
using log4net.Layout;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using System.Security;
using log4net.Core;
using log4net.Layout.Pattern;
using System.Web;

namespace LunchKingSite.BizLogic.Component
{
    //避免exception中有特殊字元，造成xml解析錯誤
    public class CustomerExceptionPatternLayoutConverter : PatternLayoutConverter
    {
        protected override void Convert(System.IO.TextWriter writer, LoggingEvent loggingEvent)
        {
            if (loggingEvent == null)
            {
                throw new NotImplementedException();
            }
            else
            {
                writer.Write(SecurityElement.Escape(loggingEvent.GetExceptionString()).Replace("=", "&#61;"));
            }
        }
    }

    public class CustomerMessagePatternLayoutConverter : PatternLayoutConverter
    {
        protected override void Convert(System.IO.TextWriter writer, LoggingEvent loggingEvent)
        {
            if (loggingEvent == null)
            {
                throw new NotImplementedException();
            }
            else
            {
                writer.Write(SecurityElement.Escape(loggingEvent.RenderedMessage).Replace("=", "&#61;"));
            }
        }
    }

    public class CustomerUrlPatternLayoutConverter : PatternLayoutConverter
    {
        protected override void Convert(System.IO.TextWriter writer, LoggingEvent loggingEvent)
        {
            if (loggingEvent == null)
            {
                throw new NotImplementedException();
            }
            else
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                {
                    writer.Write(HttpContext.Current.Request.Path);
                }
                else
                {
                    writer.Write(string.Empty);
                }
            }
        }
    }

    public class LogAdoNetApperder : AdoNetAppender
    {
        public string ConnectionStringName
        {
            set { ConnectionString = ConfigurationManager.ConnectionStrings[value].ToString(); }
        }

        public LogAdoNetApperder()
        {
            //CommandText = "INSERT INTO log4net ([create_time],[thread],[log_level],[host],[type],[method],[line],[message],[exception],[username]) VALUES (@create_time, @thread, @log_level, @host,@type,@method,@line, @message, @exception,@username)";
            CommandText = @"INSERT INTO ELMAH_Error ([ErrorId],[Application],[Host],[Type],[Source],[Message],[User],[StatusCode],[TimeUtc],[AllXml])
                            VALUES(NEWID(),@application,@host,@logtype,@source,@message,@username,405,GETUTCDATE(),@allxml)";

            #region elmah table
            this.AddParameter(new AdoNetAppenderParameter()
            {
                DbType = DbType.String,
                ParameterName = "@logtype",
                Size = 100,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("Log"))
            });
            this.AddParameter(new AdoNetAppenderParameter()
            {
                DbType = DbType.String,
                ParameterName = "@application",
                Size = 60,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%a"))
            });
            this.AddParameter(new AdoNetAppenderParameter()
            {
                DbType = DbType.String,
                ParameterName = "@source",
                Size = 60,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%C/%M"))
            });

            string error_format = "<error host=\"%property{log4net:HostName}\" type=\"Log-%level\" message=\"%messagexml\" source=\"%C/%M(%L)\" detail=\"%exceptionxml\" user=\"%identity\" time=\"%utcdate{yyyy-MM-ddTHH:mm:ssZ}\" statusCode=\"405\">"
                + "<serverVariables><item name=\"URL\"><value string=\"%url\" /></item><item name=\"REMOTE_USER\"><value string=\"%identity\" /></item></serverVariables>"
                + "<cookies><item name=\"Thread\"><value string=\"%thread\"/></item><item name=\"Level\"><value string=\"%level\"/></item><item name=\"Type\"><value string=\"%C\"/></item><item name=\"Method\"><value string=\"%M\"/></item><item name=\"LineNum\"><value string=\"%L\"/></item></cookies></error>";
            PatternLayout exception_pattern = new PatternLayout(error_format);
            exception_pattern.AddConverter("exceptionxml", typeof(CustomerExceptionPatternLayoutConverter));
            exception_pattern.AddConverter("messagexml", typeof(CustomerMessagePatternLayoutConverter));
            exception_pattern.AddConverter("url", typeof(CustomerUrlPatternLayoutConverter));
            exception_pattern.ActivateOptions();
            this.AddParameter(new AdoNetAppenderParameter()
            {
                DbType = DbType.String,
                ParameterName = "@allxml",
                Size = int.MaxValue,
                Layout = new Layout2RawLayoutAdapter(exception_pattern)
            });
            #endregion

            #region 共用
            this.AddParameter(new AdoNetAppenderParameter()
            {
                DbType = DbType.String,
                ParameterName = "@host",
                Size = 255,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%property{log4net:HostName}"))
            });
            this.AddParameter(new AdoNetAppenderParameter()
            {
                DbType = DbType.String,
                ParameterName = "@message",
                Size = 500,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%message"))
            });
            this.AddParameter(new AdoNetAppenderParameter()
            {
                DbType = DbType.String,
                ParameterName = "@username",
                Size = 50,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%identity"))
            });
            #endregion

            #region log4net table
            //this.AddParameter(new AdoNetAppenderParameter()
            //{
            //    DbType = DbType.DateTime,
            //    ParameterName = "@create_time",
            //    Layout = new RawTimeStampLayout()
            //});
            //this.AddParameter(new AdoNetAppenderParameter()
            //{
            //    DbType = DbType.String,
            //    ParameterName = "@log_level",
            //    Size = 255,
            //    Layout = new Layout2RawLayoutAdapter(new PatternLayout("%level"))
            //});
            //this.AddParameter(new AdoNetAppenderParameter()
            //{
            //    DbType = DbType.String,
            //    ParameterName = "@thread",
            //    Size = 255,
            //    Layout = new Layout2RawLayoutAdapter(new PatternLayout("%thread"))
            //});
            //this.AddParameter(new AdoNetAppenderParameter()
            //{
            //    DbType = DbType.String,
            //    ParameterName = "@type",
            //    Size = 255,
            //    Layout = new Layout2RawLayoutAdapter(new PatternLayout("%C"))
            //});
            //this.AddParameter(new AdoNetAppenderParameter()
            //{
            //    DbType = DbType.String,
            //    ParameterName = "@method",
            //    Size = 255,
            //    Layout = new Layout2RawLayoutAdapter(new PatternLayout("%M"))
            //});
            //this.AddParameter(new AdoNetAppenderParameter()
            //{
            //    DbType = DbType.Int32,
            //    ParameterName = "@line",
            //    Layout = new Layout2RawLayoutAdapter(new PatternLayout("%L"))
            //});
            //this.AddParameter(new AdoNetAppenderParameter()
            //{
            //    DbType = DbType.String,
            //    ParameterName = "@exception",
            //    Size = 4000,
            //    Layout = new Layout2RawLayoutAdapter(new ExceptionLayout())
            //});
            #endregion
        }

        //變更LogToDb Buffer緩衝數
        //log4net.Repository.Hierarchy.Hierarchy hierarchy = log4net.LogManager.GetRepository() as log4net.Repository.Hierarchy.Hierarchy;
        //if (hierarchy != null)
        //{
        //    log4net.Appender.AdoNetAppender appender
        //    = (log4net.Appender.AdoNetAppender)hierarchy.GetAppenders()
        //        .Where(x => x.GetType() ==
        //            typeof(LunchKingSite.BizLogic.Component.LogAdoNetApperder))
        //        .FirstOrDefault();

        //    if (appender != null)
        //    {
        //        appender.BufferSize = ProviderFactory.Instance().GetConfig().Log4NetToDbBufferSize;
        //    }
        //}
    }
}

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Transactions;

namespace LunchKingSite.BizLogic.Component
{
    public class HiDealCouponManager
    {
        public const string ManagerNameForJob = "HiDealCouponManager";

        private static List<HiDealCouponIssuer> _couponIssuers;

        private static object PrefixLock = new object();

        private static IHiDealProvider hp;
        private static ISysConfProvider sp;
        private static readonly log4net.ILog log;

        static HiDealCouponManager()
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            sp = ProviderFactory.Instance().GetConfig();
            log = log4net.LogManager.GetLogger(typeof(HiDealCouponManager));
            _couponIssuers = new List<HiDealCouponIssuer>();
        }

        #region public

        /// <summary>
        /// 依據傳入的的訂單PK與orderDetail資料，分派coupon給消費者
        /// </summary>
        /// <param name="order">訂單資料</param>
        /// <param name="orderDetail">訂單明細</param>
        /// <param name="memberUserName">會員帳號</param>
        /// <returns></returns>
        public static bool SetCouponData(PiinLifeCouponWorkFlow coupon_workflow)
        {
            var couponCodes = coupon_workflow.StoreGuid == null ? RequestCouponList(coupon_workflow.ProductId, coupon_workflow.ItemQuantity)
                : RequestCouponList(coupon_workflow.ProductId, coupon_workflow.StoreGuid.Value, coupon_workflow.ItemQuantity);
            //若couponCodes數量為0，表示已沒有足夠的數量。
            if (couponCodes.Count == 0)
            {
                return false;
            }

            var couponOffers = new HiDealCouponOfferCollection();
            foreach (var code in couponCodes)
            {
                var couponOffer = new HiDealCouponOffer();
                couponOffer.CouponId = code;
                couponOffer.DealId = coupon_workflow.HiDealId;
                couponOffer.ProductId = coupon_workflow.ProductId;
                couponOffer.StoreGuid = coupon_workflow.StoreGuid;
                couponOffer.OrderPk = coupon_workflow.PK;
                couponOffer.CreateTime = DateTime.Now;
                couponOffers.Add(couponOffer);
            }

            bool rtn = true;
            using (var transScope = new TransactionScope())
            {
                try
                {
                    hp.HiDealCouponOfferSetList(couponOffers);
                    hp.HiDealCouponUpdateToAssigned(couponCodes, coupon_workflow.PK, coupon_workflow.OrderDetailGuid,
                                                    MemberFacade.GetUniqueId(coupon_workflow.MemberUserName));
                    transScope.Complete();
                }
                catch (Exception ex)
                {
                    var json = new JsonSerializer();
                    log.Error("HiDealCoupon:" + json.Serialize(coupon_workflow) + "  ,  CouponId:" + json.Serialize(couponCodes), ex);
                    rtn = false;
                }
            }
            return rtn;
        }

        /// <summary>
        /// 依據傳入的的訂單PK與orderDetail資料，分派coupon給消費者
        /// </summary>
        /// <param name="order">訂單資料</param>
        /// <param name="orderDetail">訂單明細</param>
        /// <param name="memberUserName">會員帳號</param>
        /// <returns></returns>
        public static bool SetCouponData(HiDealOrder order, HiDealOrderDetail orderDetail, string memberUserName)
        {
            var couponCodes = orderDetail.StoreGuid == null ? RequestCouponList(orderDetail.ProductId, orderDetail.ItemQuantity)
                : RequestCouponList(orderDetail.ProductId, orderDetail.StoreGuid.Value, orderDetail.ItemQuantity);
            //若couponCodes數量為0，表示已沒有足夠的數量。
            if (couponCodes.Count == 0)
            {
                return false;
            }

            var couponOffers = new HiDealCouponOfferCollection();
            foreach (var code in couponCodes)
            {
                var couponOffer = new HiDealCouponOffer();
                couponOffer.CouponId = code;
                couponOffer.DealId = orderDetail.HiDealId;
                couponOffer.ProductId = orderDetail.ProductId;
                couponOffer.StoreGuid = orderDetail.StoreGuid;
                couponOffer.OrderPk = order.Pk;
                couponOffer.CreateTime = DateTime.Now;
                couponOffers.Add(couponOffer);
            }

            bool rtn = true;
            using (var transScope = new TransactionScope())
            {
                try
                {
                    hp.HiDealCouponOfferSetList(couponOffers);
                    hp.HiDealCouponUpdateToAssigned(couponCodes, order.Pk, orderDetail.Guid,
                                                    MemberFacade.GetUniqueId(memberUserName));
                    transScope.Complete();
                }
                catch (Exception ex)
                {
                    var json = new JsonSerializer();
                    log.Error("HiDealCoupon:" + json.Serialize(orderDetail) + "  ,  CouponId:" + json.Serialize(couponCodes), ex);
                    rtn = false;
                }
            }
            return rtn;
        }

        /// <summary>
        /// 檢查檔次是否為關閉檔，若是則清空該檔次於暫存物件中的資料
        /// </summary>
        public static void ClearCloseDealData()
        {
            for (var i = _couponIssuers.Count - 1; i >= 0; i--)
            {
                var issuer = _couponIssuers[i];
                if (hp.HiDealCityGetCityCountByPidAndTime(issuer.ProductId, DateTime.Now) == 0)
                {
                    _couponIssuers.Remove(issuer);
                }
            }
        }

        /// <summary>
        /// 移除特定檔次的coupon暫存資料
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="isSyncWebServer"></param>
        public static void ClearProductCouponData(int productId, bool isSyncWebServer = true)
        {
            var rtns = _couponIssuers.Where(x => x.ProductId == productId).ToList();
            if (rtns.Count > 0)
            {
                _couponIssuers.Remove(rtns.First());
            }
        }

        #region 發放coupon相關

        /// <summary>
        /// 回收已分配的憑證，修改狀態為未使用，並重新修改庫存數量
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <param name="orderPk">訂單流水號碼</param>
        /// <returns></returns>
        public static bool ReleaseAssignedCoupon(int productId, int orderPk)
        {
            ClearProductCouponData(productId);
            var issuer = GetHiDealCouponIssuer(productId);
            return issuer.ReleaseAssignedCoupon(orderPk);
        }

        /// <summary>
        /// 傳入商品序號與需要的憑證數量，回傳分配的憑證編號，
        /// 如果沒有該商品或憑證數量不足，則回傳空的List
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <param name="quantity">所需憑證數</param>
        /// <param name="isGetNewestByDB">是否需要從DB撈最新即時資料</param>
        /// <returns>憑證編號List</returns>
        public static List<long> RequestCouponList(int productId, int quantity, bool isGetNewestByDB = false)
        {
            if (isGetNewestByDB)
            {
                ClearProductCouponData(productId);
            }
            var issuer = GetHiDealCouponIssuer(productId);
            return issuer.RequestCouponList(quantity, Guid.Empty);
        }

        /// <summary>
        /// 傳入商品序號、店鋪GUID與需要的憑證數量，回傳分配的憑證編號，
        /// 如果沒有該商品或憑證數量不足，則回傳空的List
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <param name="storeGuid">分店GUID</param>
        /// <param name="quantity">所需憑證數目</param>
        /// <param name="isGetNewestByDB">是否需要從DB撈最新即時資料</param>
        /// <returns>憑證編號List</returns>
        public static List<long> RequestCouponList(int productId, Guid storeGuid, int quantity, bool isGetNewestByDB = false)
        {
            if (isGetNewestByDB)
            {
                ClearProductCouponData(productId);
            }
            var issuer = GetHiDealCouponIssuer(productId);
            return issuer.RequestCouponList(quantity, storeGuid);
        }

        /// <summary>
        /// 傳入商品編號，回傳目前還可以發放的coupon數量(庫存數)
        /// 包含宅配與分店
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <param name="isGetNewestByDB">是否需要從DB撈最新即時資料</param>
        /// <returns>該商品可販售COUPON數</returns>
        public static int GetCouponInventoryQuantity(int productId, bool isGetNewestByDB = false)
        {
            if (isGetNewestByDB)
            {
                ClearProductCouponData(productId);
            }
            var issuer = GetHiDealCouponIssuer(productId);
            return issuer.GetCouponInventoryQuantity();
        }

        /// <summary>
        /// 傳入商品編號與分店編號，回傳目前還可以發放的coupon數量(庫存數)
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <param name="storeGuid">分店GUID</param>
        /// <param name="isGetNewestByDB">是否需要從DB撈最新即時資料</param>
        /// <returns>該分店可販售商品Coupon數</returns>
        public static int GetCouponInventoryQuantity(int productId, Guid storeGuid, bool isGetNewestByDB = false)
        {
            if (isGetNewestByDB)
            {
                ClearProductCouponData(productId);
            }
            var issuer = GetHiDealCouponIssuer(productId);
            return issuer.GetCouponInventoryQuantity(storeGuid);
        }

        /// <summary>
        /// 依據傳入的商品編號，取出該商品所有分店的庫存資料
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <param name="isGetNewestByDB">是否需要從DB撈最新即時資料</param>
        /// <returns>該商店所有分店庫存List</returns>
        public static List<HiDealProductStoreInventory> GetAllStoreInventory(int productId, bool isGetNewestByDB = false)
        {
            if (isGetNewestByDB)
            {
                ClearProductCouponData(productId);
            }
            var issuer = GetHiDealCouponIssuer(productId);
            return issuer.GetAllStoreInventory();
        }

        /// <summary>
        /// 依據傳入的商品編號，取出該商品宅配的庫存資料
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <param name="isGetNewestByDB">是否需要從DB撈最新即時資料</param>
        /// <returns>商品宅配庫存數目</returns>
        public static int GetToHomeCouponInventoryQuantity(int productId, bool isGetNewestByDB = false)
        {
            if (isGetNewestByDB)
            {
                ClearProductCouponData(productId);
            }
            var issuer = GetHiDealCouponIssuer(productId);
            return issuer.GetCouponInventoryQuantity(Guid.Empty);  //Guid.Empty時，是宅配Coupon可用數量
        }

        #endregion 發放coupon相關

        #region 產生coupon相關

        /// <summary>
        /// Deal產生Coupon
        /// </summary>
        /// <param name="dealId">檔次id</param>
        /// <param name="userName">操作者</param>
        /// <returns></returns>
        public static bool GenerateHiDealCoupon(int dealId, string userName)
        {
            //是否有同賣家不同檔次販售，調整prefix碼，避免憑證Prefix相同導致店家對照上困難
            HiDealDeal hidealdeal = hp.HiDealDealGet(dealId);
            int sellerDealMaxSequence = hp.HiDealCouponPrefixGetMaxSequenceBySellerGuid(hidealdeal.SellerGuid);

            //尚未產生憑證
            //查詢所有商品
            //HiDealDeal deal = hp.HiDealDealGet(dealId);
            HiDealProductCollection pc = hp.HiDealProductCollectionGet(dealId);
            //每個商品逐一產生憑證
            foreach (var product in pc)
            {
                if (!GenerateHiDealCouponByProduct(product, userName, sellerDealMaxSequence))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 依據傳入的product 建立coupon
        /// </summary>
        /// <param name="product"></param>
        /// <param name="userName">操作者</param>
        /// <returns></returns>
        public static bool GenerateHiDealCouponByProduct(HiDealProduct product, string userName, int sellerDealMaxSequence = 0)
        {
            int productId = product.Id;
            var dealStatics = hp.HiDealStaticDealGetListByProductIdAndMaxChangeSet(product.Id);
            //檢查dealStatic 是否有資料，確認是否產生過憑證的紀錄
            if (dealStatics.Count > 0)
            {
                var dealStatic = dealStatics.First();
                if (dealStatic.HasBranchStoreQuantityLimit != product.HasBranchStoreQuantityLimit)
                {
                    //檢查coupon是否已有被發放的紀錄
                    if (hp.HiDealCouponHaveAssignedOrUsed(productId))
                    {
                        //已有使用中的憑證，不可修改數量控制的方式
                        return false;
                    }
                    else
                    {
                        //尚未使用，清空coupon重新發放。
                        hp.HiDealCouponDeleteAllByProductId(productId);
                        //將讀取出來的紀錄清空，迫使程式重新產生憑證
                        dealStatics = new HiDealStaticDealCollection();
                    }
                }
            }

            #region 以分店與宅配商品設定數量為準

            //分店部分
            HiDealProductStoreCollection stores = hp.HiDealProductStoreGetListByProductId(product.Id);
            foreach (var store in stores)
            {
                if (!GenerateHiDealCouponByStore(product, store, dealStatics, sellerDealMaxSequence))
                {
                    return false;
                }
            }
            //宅配商品部分
            if (!GenerateHiDealCouponByProduct(product, dealStatics, sellerDealMaxSequence))
            {
                return false;
            }

            #endregion 以分店與宅配商品設定數量為準

            //產生異動紀錄
            int changeSet = SaveStaticDeal(product, userName);
            hp.HiDealCouponLogSet(new HiDealCouponLog { DealId = product.DealId, ProductId = product.Id, Status = (int)HiDealCouponLogStatus.Modify, CreateTime = DateTime.Now, ChangeSet = changeSet, UserName = userName });
            //清除coupon暫存資料
            ClearProductCouponData(productId);
            return true;
        }

        /// <summary>
        /// 新增product時對product生coupon
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <param name="userName">操作者</param>
        /// <returns></returns>
        public static bool GenerateHiDealCouponByProduct(int productId, string userName)
        {
            HiDealProduct product = hp.HiDealProductGet(productId);
            //取得該賣家所有檔次目前MaxSequence
            ViewHiDeal hidealdeal = hp.ViewHiDealGetByDealId(product.DealId);
            int sellerDealMaxSequence = hp.HiDealCouponPrefixGetMaxSequenceBySellerGuid(hidealdeal.SellerGuid);

            return GenerateHiDealCouponByProduct(product, userName, sellerDealMaxSequence);
        }

        /// <summary>
        /// 退換憑證編號，輸入一個舊有且有效的憑證物件，作廢舊有憑證並於DB中產生新憑證，並回傳新憑證的物件
        /// </summary>
        /// <returns>回傳新的憑證物件，若發生錯誤，回傳null</returns>
        public static HiDealCoupon HiDealOneCouponChangeNewOne(long oldCouponId)
        {
            HiDealCoupon rtn = null;
            //lock宣告，一次只會有一個地方執行。
            lock (PrefixLock)
            {
                //檢查舊憑證
                var coupon = hp.HiDealCouponGet(oldCouponId);
                if (!coupon.IsLoaded)
                {
                    throw new Exception("原憑證編號不存在:HiDealOneCouponChangeNewOne退換HiDeal憑證失敗" +
                                        new JsonSerializer().Serialize(coupon));
                }

                //檢查是否為有效憑證
                if (coupon.CouponIsEnabled())
                {
                    //將舊憑證作廢。
                    coupon.Status = (int)HiDealCouponStatus.Refund;
                    hp.HiDealCouponSet(coupon);

                    //判斷憑證屬於商品or憑證
                    if (coupon.StoreGuid == null)
                    {
                        //商品檔需憑證作廢後才產出新Coupon
                        GenerateHiDealCouponByRefundCouponId(coupon, out rtn);
                    }
                }
            }
            return rtn;
        }

        public static void GenerateHiDealCouponByRefundCouponId(HiDealCoupon oldHiDealCoupon, out HiDealCoupon newHiDealCoupon)
        {
            //取得商品資料
            var product = hp.HiDealProductGet(oldHiDealCoupon.ProductId);
            if (!product.IsLoaded)
            {
                //商品不存在，無法預期的錯誤
                throw new Exception("商品不存在:GenerateHiDealCouponByRefundCouponId退換HiDeal憑證失敗" +
                                    new JsonSerializer().Serialize(oldHiDealCoupon));
            }
            //建立新憑證
            hp.HiDealCouponLogSet(new HiDealCouponLog
            {
                DealId = oldHiDealCoupon.DealId,
                Status = (int)HiDealCouponLogStatus.Add,
                CreateTime = DateTime.Now,
                ChangeSet = -1,
                UserName = "system"
            });

            if (oldHiDealCoupon.StoreGuid != null && oldHiDealCoupon.StoreGuid != Guid.Empty)
            {
                #region 有分店

                //取得分店資料
                var store = hp.HiDealProductStoreGetByPidAndStoreGuid(oldHiDealCoupon.ProductId, oldHiDealCoupon.StoreGuid.Value);
                if (!store.IsLoaded)
                {
                    throw new Exception("店鋪紀錄不存在:GenerateHiDealCouponByRefundCouponId退換HiDeal憑證失敗" +
                                        new JsonSerializer().Serialize(oldHiDealCoupon));
                }

                //取得目前最後一筆憑證紀錄
                var lastCoupon = hp.HiDealCouponGetTop(oldHiDealCoupon.DealId, oldHiDealCoupon.ProductId,
                                                                         oldHiDealCoupon.StoreGuid);
                var i = Convert.ToInt32(lastCoupon.Sequence.Substring(0, 6)) + 1;
                //產生憑證
                newHiDealCoupon = GenerateAndSaveCoupon(i, product, oldHiDealCoupon.DealId, store, lastCoupon.Prefix, GetCostTable(product.Id));

                #endregion 有分店
            }
            else
            {
                #region 無分店

                //取得目前最後一筆憑證紀錄
                var lastCoupon = hp.HiDealCouponGetTop(oldHiDealCoupon.DealId, oldHiDealCoupon.ProductId);
                var i = Convert.ToInt32(lastCoupon.Sequence.Substring(0, 6)) + 1;
                //產生憑證
                newHiDealCoupon = GenerateAndSaveCoupon(i, product, oldHiDealCoupon.DealId, null, lastCoupon.Prefix, GetCostTable(product.Id));

                #endregion 無分店
            }
        }

        #endregion 產生coupon相關

        #endregion public

        #region private

        #region 發放COUPON相關

        private static HiDealCouponIssuer GetHiDealCouponIssuer(int productId)
        {
            HiDealCouponIssuer rtn = null;
            var selIssuers = _couponIssuers.Where(x => x.ProductId == productId).ToList();
            if (selIssuers.Count == 0)
            {
                //查無資料，由資料庫中取出，並建立物件
                lock (typeof(HiDealCouponManager))
                {
                    //避免排隊的時候，資料已經查出來了，所以再檢查一次。
                    var checkAngin = _couponIssuers.Where(x => x.ProductId == productId).ToList();
                    if (checkAngin.Count > 0)
                    {
                        rtn = checkAngin.First();
                    }
                    else
                    {
                        //查詢product，確認商品存在
                        var product = hp.HiDealProductGet(productId);
                        if (product.IsLoaded)
                        {
                            var issuer = new HiDealCouponIssuer(product);
                            rtn = issuer;
                            _couponIssuers.Add(issuer);
                        }
                    }
                }
            }
            else
            {
                rtn = selIssuers.First();
            }
            return rtn;
        }

        #endregion 發放COUPON相關

        #region 產生coupon相關

        /// <summary>
        /// 產生或調整憑證數量，若傳入的statics為空，一律重新產生coupon
        /// </summary>
        /// <param name="product">此檔次商品</param>
        /// <param name="store">此檔次分店</param>
        /// <param name="statics">此檔次修改紀錄檔HiDealStaticDeal</param>
        /// <param name="sellerDealMaxSequence">賣家所有檔次最大DealSequence</param>
        /// <returns>憑證產生完成回傳True</returns>
        private static bool GenerateHiDealCouponByStore(HiDealProduct product, HiDealProductStore store, IEnumerable<HiDealStaticDeal> statics, int sellerDealMaxSequence = 0)
        {
            //var dealStatics = hp.HiDealStaticDealGetMaxStore(product.Id, store.StoreGuid);
            var dealStatics = statics.Where(x => x.StoreGuid == store.StoreGuid).ToList();
            if (dealStatics.Count == 0)
            {
                return FirstGenerateHiDealCouponByStore(product, store, sellerDealMaxSequence);
            }
            else
            {
                return HiDealCouponStoreQuantityChange(product, store, dealStatics.First());
            }
        }

        /// <summary>
        /// 產生或調整憑證數量，若傳入的statics為空，一律重新產生coupon
        /// </summary>
        /// <param name="product">此檔次商品</param>
        /// <param name="statics">此檔次修改紀錄檔HiDealStaticDeal</param>
        /// <param name="sellerDealMaxSequence">賣家所有檔次最大DealSequence</param>
        /// <returns>憑證產生完成回傳True</returns>
        private static bool GenerateHiDealCouponByProduct(HiDealProduct product, IEnumerable<HiDealStaticDeal> statics, int sellerDealMaxSequence = 0)
        {
            var dealStatics = statics.Where(x => x.StoreGuid == null).ToList();
            if (dealStatics.Count == 0)
            {
                return FirstGenerateHiDealCouponByProduct(product, sellerDealMaxSequence);
            }
            else
            {
                return HiDealCouponProductQuantityChange(product, dealStatics.First());
            }
        }

        /// <summary>
        /// 第一次產生憑證，依據商品設定分配
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        private static bool FirstGenerateHiDealCouponByProduct(HiDealProduct product, int sellerDealMaxSequence = 0)
        {
            var quantity = product.Quantity.GetValueOrDefault();
            //是否已分店與宅配數量為準
            if (product.HasBranchStoreQuantityLimit)
            {
                //以分店或宅配數量為準則，宅配商品需有數量。
                quantity = product.HomeDeliveryQuantity.GetValueOrDefault();
            }

            //有數量才產生憑證，避免 quantity=0也產出hi_deal_coupon_prefix
            if (quantity > 0)
            {
                string prefix;

                #region 依同時段檔前置字MaxSequence，新檔次產生後續前置字

                lock (PrefixLock)
                {
                    int _productPrefixNo = hp.HiDealCouponPrefixGetMaxSequence(product.DealId);
                    int prefixNo = _productPrefixNo >= sellerDealMaxSequence ? _productPrefixNo + 1 : sellerDealMaxSequence + 1;
                    prefix = AlphaSequence(prefixNo);
                    hp.HiDealCouponPrefixSet(new HiDealCouponPrefix
                    {
                        CreateTime = DateTime.Now,
                        DealId = product.DealId,
                        ProductId = product.Id,
                        Prefix = prefix,
                        Sequence = prefixNo,
                    });
                }

                #endregion 依同時段檔前置字MaxSequence，新檔次產生後續前置字

                Dictionary<int, decimal> costTable = GetCostTable(product.Id);
                for (int i = 1; i <= quantity; i++)
                {
                    GenerateAndSaveCoupon(i, product, product.DealId, null, prefix, costTable);
                }
            }
            return true;
        }

        /// <summary>
        /// 第一次產生憑證，依據商品與分店設定分配
        /// </summary>
        /// <param name="product"></param>
        /// <param name="store"></param>
        /// <returns></returns>
        private static bool FirstGenerateHiDealCouponByStore(HiDealProduct product, HiDealProductStore store, int sellerDealMaxSequence = 0)
        {
            string prefix;
            //store.QuantityLimi要大於0，才產生憑證
            if (store.QuantityLimit.GetValueOrDefault() > 0)
            {
                //是否有同賣家同時段不同檔次販售，調整prefix碼，避免憑證Prefix相同導致店家對照上困難

                #region 依賣家所有檔次前置字MaxSequence，新檔次產生後續前置字

                lock (PrefixLock)
                {
                    int _productPrefixNo = hp.HiDealCouponPrefixGetMaxSequence(product.DealId);
                    int prefixNo = _productPrefixNo >= sellerDealMaxSequence ? _productPrefixNo + 1 : sellerDealMaxSequence + 1;
                    prefix = AlphaSequence(prefixNo);
                    hp.HiDealCouponPrefixSet(new HiDealCouponPrefix
                    {
                        CreateTime = DateTime.Now,
                        DealId = product.DealId,
                        ProductId = product.Id,
                        Prefix = prefix,
                        Sequence = prefixNo,
                        StoreGuid = store.StoreGuid
                    });
                }

                #endregion 依賣家所有檔次前置字MaxSequence，新檔次產生後續前置字

                Dictionary<int, decimal> costTable = GetCostTable(product.Id);
                for (int i = 1; i <= store.QuantityLimit; i++)
                {
                    GenerateAndSaveCoupon(i, product, product.DealId, store, prefix, costTable);
                }
            }
            return true;
        }

        /// <summary>
        /// 修改product數量
        /// </summary>
        /// <param name="product"></param>
        /// <param name="dealStatic">上次產生COUPON的紀錄</param>
        /// <returns></returns>
        private static bool HiDealCouponProductQuantityChange(HiDealProduct product, HiDealStaticDeal dealStatic)
        {
            //新的數量
            var quantity = product.Quantity.GetValueOrDefault();
            //商品數量以分店或宅配數量為準。
            if (product.HasBranchStoreQuantityLimit)
            {
                quantity = product.HomeDeliveryQuantity.GetValueOrDefault();
            }

            //減少
            if (dealStatic.Quantity > quantity)
            {
                int amount = dealStatic.Quantity - quantity;
                int available = hp.HiDealCouponGetProductAvailable(product.DealId, product.Id);
                //檢查可用單數量，需大於等於欲減少的數量
                if (available >= amount)
                {
                    int i = 0;
                    //將有效的憑證先註記為marked
                    hp.HiDealCouponUpdateStatus(HiDealCouponStatus.Available, HiDealCouponStatus.Marked, product.DealId, product.Id);
                    HiDealCouponCollection hidealcouponcollection = hp.HiDealCouponGetByStatus(product.DealId, product.Id,
                                                                                                HiDealCouponStatus.Marked,
                                                                                                new List<string> { HiDealCoupon.SequenceColumn + " desc" });
                    //取消憑證使數量符合設定。
                    foreach (HiDealCoupon hidealcoupon in hidealcouponcollection)
                    {
                        if (i < amount)
                        {
                            hidealcoupon.Status = (int)HiDealCouponStatus.Cancel;
                            hp.HiDealCouponSet(hidealcoupon);
                            i++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    //處理完後將marked的紀錄改回可用
                    hp.HiDealCouponUpdateStatus(HiDealCouponStatus.Marked, HiDealCouponStatus.Available, product.DealId, product.Id);
                }
                else
                {
                    return false;
                }
            }
            //增加
            else
            {
                HiDealCoupon lastCoupon = hp.HiDealCouponGetTop(product.DealId, product.Id);
                int amount = quantity - dealStatic.Quantity;
                int i = Convert.ToInt32(lastCoupon.Sequence.Substring(0, 6)) + 1;
                Dictionary<int, decimal> costTable = GetCostTable(product.Id);
                for (int j = i; j < i + amount; j++)
                {
                    GenerateAndSaveCoupon(j, product, product.DealId, null, lastCoupon.Prefix, costTable);
                }
            }
            return true;
        }

        /// <summary>
        /// 修改Hideal store，販賣數量時，處理憑證
        /// </summary>
        /// <param name="product">商品物件</param>
        /// <param name="store">商品店鋪檔物件</param>
        /// <param name="dealStatic">上次產生的紀錄</param>
        /// <returns></returns>
        private static bool HiDealCouponStoreQuantityChange(HiDealProduct product, HiDealProductStore store, HiDealStaticDeal dealStatic)
        {
            HiDealCoupon lastCoupon = hp.HiDealCouponGetTop(product.DealId, product.Id, store.StoreGuid);

            if (store.QuantityLimit != null)
            {
                //減少
                if (dealStatic.Quantity > store.QuantityLimit)
                {
                    int amount = dealStatic.Quantity - (int)store.QuantityLimit;
                    int available = hp.HiDealCouponGetStoreAvailable(product.DealId, product.Id, store.StoreGuid);
                    if (available >= amount)
                    {
                        int i = 0;
                        hp.HiDealCouponUpdateStatus(HiDealCouponStatus.Available, HiDealCouponStatus.Marked,
                                                                      product.DealId, product.Id, store.StoreGuid);
                        HiDealCouponCollection hidealcouponcollection = hp.HiDealCouponGetByStatus(product.DealId, product.Id,
                                                                                                 store.StoreGuid,
                                                                                                 HiDealCouponStatus.Marked, new List<string> { HiDealCoupon.SequenceColumn + " desc" });
                        foreach (HiDealCoupon hidealcoupon in hidealcouponcollection)
                        {
                            if (i < amount)
                            {
                                hidealcoupon.Status = (int)HiDealCouponStatus.Cancel;
                                hp.HiDealCouponSet(hidealcoupon);
                                i++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        hp.HiDealCouponUpdateStatus(HiDealCouponStatus.Marked, HiDealCouponStatus.Available, product.DealId, product.Id, store.StoreGuid);
                    }
                    else
                    {
                        return false;
                    }
                }
                //增加
                else
                {
                    int amount = (int)store.QuantityLimit - dealStatic.Quantity;
                    int i = Convert.ToInt32(lastCoupon.Sequence.Substring(0, 6)) + 1;
                    for (int j = i; j < i + amount; j++)
                    {
                        GenerateAndSaveCoupon(j, product, product.DealId, store, lastCoupon.Prefix, GetCostTable(product.Id));
                    }
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 產生憑證並新增至DB
        /// </summary>
        /// <param name="i"></param>
        /// <param name="p"></param>
        /// <param name="dealId"></param>
        /// <param name="store"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        private static HiDealCoupon GenerateAndSaveCoupon(int orderindex, HiDealProduct hidealproduct, int dealId, HiDealProductStore store, string prefix,
            Dictionary<int, decimal> costTable)
        {
            string sequence;
            Guid? storeGuid = null;
            if ((store == null) || (!store.IsLoaded))
            {
                sequence = OrderFacade.GenerateHidealCouponSequence(hidealproduct.Guid.ToString(), orderindex);
            }
            else
            {
                sequence = OrderFacade.GenerateHidealCouponSequence(hidealproduct.Guid.ToString(), store.StoreGuid.ToString(), orderindex);
                storeGuid = store.StoreGuid;
            }
            string code = OrderFacade.GenerateCouponVerification(sequence, orderindex.ToString());
            var coupon = new HiDealCoupon
            {
                Sequence = sequence,
                Code = code,
                CreateDate = DateTime.Now,
                DealId = dealId,
                ProductId = hidealproduct.Id,
                Prefix = prefix,
                StoreGuid = storeGuid,
                Status = (int)HiDealCouponStatus.Available,
                Cost = GetCouponCost(costTable, orderindex, hidealproduct.Id)
            };
            hp.HiDealCouponSet(coupon);
            return coupon;
        }

        private static Dictionary<int, decimal> GetCostTable(int productId)
        {
            Dictionary<int, decimal> result = new Dictionary<int, decimal>();
            foreach (HiDealProductCost cost in hp.HiDealProductCostCollectionGetByProductId(productId))
            {
                result.Add(cost.CumulativeQuantitiy, cost.Cost);
            }
            return result;
        }

        private static decimal GetCouponCost(Dictionary<int, decimal> costTable, int idx, int productId)
        {
            if (costTable.Count == 0)
            {
                throw new Exception("此操作必需先設定產品的成本.");
            }
            decimal lastCost = 0;
            decimal lastCumulativeQuantitiy = 0;
            foreach (KeyValuePair<int, decimal> pair in costTable)
            {
                lastCumulativeQuantitiy = pair.Key;
                lastCost = pair.Value;
                if (idx <= lastCumulativeQuantitiy)
                {
                    return lastCost;
                }
            }
            log.InfoFormat("當coupon設定成本時，編號己經超出可賣的累積份數，使用最大的成本設定。產品Id={0}, 編號={1}, 累積份數限制={2}, 回傳成本={3}",
                productId, idx, lastCumulativeQuantitiy, lastCost);
            return lastCost;
        }

        private static int SaveStaticDeal(HiDealProduct product, string userName)
        {
            HiDealDeal deal = hp.HiDealDealGet(product.DealId);
            int changeSet = hp.HiDealStaticDealGetProductMaxChangeSet(product.Id) + 1;

            //分店部分
            if (product.HasBranchStoreQuantityLimit)
            {
                #region 以分店與宅配數量為準

                HiDealProductStoreCollection store = hp.HiDealProductStoreGetListByProductId(product.Id);
                foreach (var s in store)
                {
                    hp.HiDealStaticDealSet(new HiDealStaticDeal
                    {
                        DealId = deal.Id,
                        ChangeSet = changeSet,
                        DealName = deal.Name,
                        ProductId = product.Id,
                        ProductName = product.Name,
                        StoreGuid = s.StoreGuid,
                        Quantity = s.QuantityLimit.Value,
                        HasBranchStoreQuantityLimit = product.HasBranchStoreQuantityLimit,
                        CreateTime = DateTime.Now,
                        CreateId = userName
                    });
                }

                //宅配部分
                if (product.HomeDeliveryQuantity != null)
                {
                    hp.HiDealStaticDealSet(new HiDealStaticDeal
                    {
                        DealId = deal.Id,
                        ChangeSet = changeSet,
                        DealName = deal.Name,
                        ProductId = product.Id,
                        ProductName = product.Name,
                        StoreGuid = null,
                        Quantity = product.HomeDeliveryQuantity.Value,
                        HasBranchStoreQuantityLimit = product.HasBranchStoreQuantityLimit,
                        CreateTime = DateTime.Now,
                        CreateId = userName
                    });
                }

                #endregion 以分店與宅配數量為準
            }
            else
            {
                #region 以商品設限為準

                hp.HiDealStaticDealSet(new HiDealStaticDeal
                {
                    DealId = deal.Id,
                    ChangeSet = changeSet,
                    DealName = deal.Name,
                    ProductId = product.Id,
                    ProductName = product.Name,
                    StoreGuid = null,
                    Quantity = product.Quantity.Value,
                    HasBranchStoreQuantityLimit = product.HasBranchStoreQuantityLimit,
                    CreateTime = DateTime.Now,
                    CreateId = userName
                });

                #endregion 以商品設限為準
            }
            return changeSet;
        }

        private static string AlphaSequence(int ord)
        {
            //Prefix文字限定在兩碼，故超過回逤為一碼繼續開始  A~Z共26個，AA~ZZ共676個，加總702，702 = ZZ
            int dividend = (ord % 702) == 0 ? 702 : (ord % 702);
            string columnName = string.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = ((dividend - modulo) / 26);
            }
            return columnName;
        }

        #endregion 產生coupon相關

        #endregion private

        #region HiDealCouponIssuer

        private class HiDealCouponIssuer
        {
            #region Public member

            public int DealId { get; private set; }

            public int ProductId { get; private set; }

            public bool HasBranchStoreQuantityLimit { get; private set; }

            #endregion Public member

            #region Private member

            /// <summary>
            /// 依次查詢的數量
            /// </summary>
            public const int QueryCouponQuantityAtOneTime = 100;

            /// <summary>
            /// 紀錄上次由資料庫取出的coupon，已經取到幾號了。
            /// </summary>
            private long _lastCouponId;

            private List<StoreCoupon> _storeCouponList;

            /// <summary>
            /// 目前已分配到_standbyCouponList中的第幾筆
            /// </summary>
            private int _issuerIndex;

            /// <summary>
            /// 預備分配的COUPON
            /// </summary>
            private List<long> _standbyCouponList;

            /// <summary>
            /// 宅配商品庫存
            /// </summary>
            private int _toHomeInventoryQuantity;

            /// <summary>
            /// 宅配庫存上限
            /// </summary>
            private int _toHomeQuantityLimit;

            /// <summary>
            /// 商品銷售上限
            /// </summary>
            private int _productSalesCaps;

            #endregion Private member

            public HiDealCouponIssuer(HiDealProduct product)
            {
                //紀錄銷售上限
                _productSalesCaps = product.Quantity == null ? 0 : product.Quantity.Value;
                //宅配數量
                _toHomeInventoryQuantity = 0;
                _toHomeQuantityLimit = 0;
                //有宅配
                if (product.IsHomeDelivery)
                {
                    //檢查有無設定宅配商品數量，若沒有，數量預設為商品上限。
                    if ((product.HomeDeliveryQuantity == null) || (product.HomeDeliveryQuantity.Value == 0))
                    {
                        _toHomeQuantityLimit = product.Quantity == null ? 0 : product.Quantity.Value;
                    }
                    else
                    {
                        _toHomeQuantityLimit = product.HomeDeliveryQuantity.Value;
                    }
                }

                DealId = product.DealId;
                ProductId = product.Id;
                HasBranchStoreQuantityLimit = product.HasBranchStoreQuantityLimit;
                _lastCouponId = 0;
                _issuerIndex = 0;
                _storeCouponList = new List<StoreCoupon>();
                //是否依分店控制數量
                if (product.HasBranchStoreQuantityLimit)
                {
                    //依分店控制數量
                    //var productStores = hp.HiDealProductStoreGetListByProductId(product.Id);
                    var productStores = hp.ViewHiDealProductStoreGetListByProductId(ProductId);
                    foreach (var productStore in productStores)
                    {
                        var storeCoupon = new StoreCoupon(DealId, ProductId, productStore);
                        _storeCouponList.Add(storeCoupon);
                    }
                    //宅配數量
                    LoadCouponToStandby();
                }
                else
                {
                    //正常狀況，數量控制在商品端
                    LoadCouponToStandby();
                }
            }

            /// <summary>
            /// 取得憑證
            /// </summary>
            /// <param name="quantity">需要的數量</param>
            /// <param name="storeGuid">表示需要哪個分店的憑證，為空值則表示取得宅配商品的憑證</param>
            /// <returns></returns>
            public List<long> RequestCouponList(int quantity, Guid storeGuid)
            {
                //需要的數量 > 剩餘可銷售數量，則表示數量不足，回傳空的陣列
                if (quantity > GetSalableCouponQuantity())
                {
                    return new List<long>();
                }

                //檢查有無分店
                if (storeGuid == Guid.Empty)
                {
                    return RequestCouponListWithNoStore(quantity);
                }
                else
                {
                    return RequestCouponListWithStore(storeGuid, quantity);
                }
            }

            /// <summary>
            /// 回傳可分配憑證數量
            /// </summary>
            /// <returns></returns>
            public int GetCouponInventoryQuantity()
            {
                //若以分店數量或宅配數量為準，則取出分店的庫存總數+商品庫存，否則只取商品庫存
                var rtn = HasBranchStoreQuantityLimit ? _storeCouponList.Sum(x => x.InventoryQuantity) + _toHomeInventoryQuantity : _toHomeInventoryQuantity;
                //若目前庫存大於可銷售數量，回傳可銷售數量
                var salableQuantity = GetSalableCouponQuantity();
                if (rtn > GetSalableCouponQuantity())
                {
                    rtn = salableQuantity;
                }
                return rtn;
            }

            /// <summary>
            /// 回傳分店可分配憑證數量
            /// </summary>
            /// <param name="storeGuid"></param>
            /// <returns></returns>
            public int GetCouponInventoryQuantity(Guid storeGuid)
            {
                var rtn = 0;
                if (storeGuid == Guid.Empty)
                {
                    rtn = _toHomeInventoryQuantity;
                }
                else
                {
                    var storeCoupons = _storeCouponList.Where(x => x.StoreGuid == storeGuid).ToList();
                    if (storeCoupons.Count > 0)
                    {
                        rtn = storeCoupons.First().InventoryQuantity;
                    }
                }
                //若目前庫存大於可銷售數量，回傳可銷售數量
                var salableQuantity = GetSalableCouponQuantity();
                if (rtn > GetSalableCouponQuantity())
                {
                    rtn = salableQuantity;
                }
                return rtn;
            }

            /// <summary>
            /// 回傳所有分店的庫存數量。
            /// </summary>
            /// <returns></returns>
            public List<HiDealProductStoreInventory> GetAllStoreInventory()
            {
                var rtn = new List<HiDealProductStoreInventory>();
                foreach (StoreCoupon storeCoupon in _storeCouponList.OrderBy(y => y.Seq))
                {
                    //庫存數量需檢查可銷售數量，如果可銷售數量較少，則已可銷售數量為準，否則以庫存數量為準
                    var quantity = GetSalableCouponQuantity();
                    if (storeCoupon.InventoryQuantity < quantity)
                    {
                        quantity = storeCoupon.InventoryQuantity;
                    }

                    HiDealProductStoreInventory storeInventory = new HiDealProductStoreInventory();
                    storeInventory.Inventory = quantity;
                    storeInventory.StoreGuid = storeCoupon.StoreGuid;
                    storeInventory.StoreName = storeCoupon.StoreName;
                    storeInventory.Seq = storeCoupon.Seq;
                    rtn.Add(storeInventory);
                }
                return rtn;
            }

            /// <summary>
            /// 回收已分配的憑證，修改狀態為未使用，並重新修改庫存數量
            /// </summary>
            /// <param name="orderPk"></param>
            /// <returns></returns>
            public bool ReleaseAssignedCoupon(int orderPk)
            {
                //取得此訂單所有的coupon
                var couponList = hp.HiDealCouponGetListByOrderPk(ProductId, orderPk, null);
                foreach (var coupon in couponList)
                {
                    //如果憑證狀態不為 已分配 回傳錯誤。
                    if (coupon.Status != (int)HiDealCouponStatus.Assigned)
                    {
                        return false;
                    }
                    coupon.OrderPk = null;
                    coupon.OrderDetailGuid = null;
                    coupon.Status = (int)HiDealCouponStatus.Available;
                    coupon.UserId = null;
                    coupon.BoughtDate = null;
                }
                hp.HiDealCouponSetList(couponList);
                //補庫存
                var couponListGroup = couponList.ToLookup(x => x.StoreGuid);
                foreach (var group in couponListGroup)
                {
                    if (group.Key == null)
                    {
                        _toHomeInventoryQuantity += group.Count();
                    }
                    else
                    {
                        var storeCoupons = _storeCouponList.Where(x => x.StoreGuid == group.Key).ToList();
                        if (storeCoupons.Count > 0)
                        {
                            storeCoupons.First().InventoryQuantity += group.Count();
                        }
                    }
                }
                return true;
            }

            #region private method

            /// <summary>
            /// 回傳可銷售數量(商品設定的最大數量 減掉已分配的數量)
            /// </summary>
            /// <returns></returns>
            private int GetSalableCouponQuantity()
            {
                //已分配商品數量 = 商品上限 - 剩餘庫存
                int quantity = _toHomeInventoryQuantity;
                int limit = _toHomeQuantityLimit;
                if (HasBranchStoreQuantityLimit)
                {
                    quantity = _storeCouponList.Sum(x => x.InventoryQuantity) + _toHomeInventoryQuantity;
                    limit = _storeCouponList.Sum(x => x.QuantityLimit) + _toHomeQuantityLimit;
                }
                var salQuantity = limit - quantity;

                //可銷售數量 = 商品可銷售上限-已銷售數量
                return _productSalesCaps - salQuantity;
            }

            /// <summary>
            /// 輸入需要的coupon數量，如果庫存還有足夠的數量，回傳配給的Coupon鍵值(ID)的List
            /// 若數量不夠，或者該檔次是依據分店數量產生憑證，則回傳空的List
            /// </summary>
            /// <param name="quantity"></param>
            /// <returns></returns>
            private List<long> RequestCouponListWithNoStore(int quantity)
            {
                //以分店數量產生憑證
                lock (this)
                {
                    var rtnList = new List<long>();
                    if (quantity <= _toHomeInventoryQuantity)
                    {
                        //暫存List中沒有足夠的憑證，重新查詢。
                        if ((_issuerIndex + quantity) > _standbyCouponList.Count)
                        {
                            //重新查詢。
                            LoadCouponToStandby();
                            //重新判斷剩餘數量是否足夠
                            if (quantity > _toHomeInventoryQuantity)
                            {
                                //數量依然不足
                                return new List<long>();
                            }
                            //有數量可是無憑證==>時間差造成的差異
                            if ((quantity <= _toHomeInventoryQuantity) && (quantity > _standbyCouponList.Count))
                            {
                                //修改剩餘數量
                                _toHomeInventoryQuantity = _standbyCouponList.Count;
                                return new List<long>();
                            }
                        }

                        //開始分配序號
                        for (var i = 0; i < quantity; i++)
                        {
                            rtnList.Add(_standbyCouponList[_issuerIndex]);
                            _issuerIndex++;
                            _toHomeInventoryQuantity--;
                        }
                    }
                    return rtnList;
                }
            }

            /// <summary>
            /// 輸入分店序號與需要的coupon數量，如果庫存還有足夠的數量，回傳配給的Coupon鍵值(ID)的List
            /// 若數量不夠，則回傳空的List
            /// </summary>
            /// <param name="storeGuid"></param>
            /// <param name="quantity"></param>
            /// <returns></returns>
            private List<long> RequestCouponListWithStore(Guid storeGuid, int quantity)
            {
                var selObjs = _storeCouponList.Where(x => x.StoreGuid == storeGuid).ToList();
                if (selObjs.Count > 0)
                {
                    return selObjs.First().RequestCouponList(quantity);
                }
                else
                {
                    return new List<long>();
                }
            }

            private void LoadCouponToStandby()
            {
                //重新查詢剩餘的coupon數
                _toHomeInventoryQuantity = hp.HiDealCouponGetNotInHiDealCouponOfferCount(DealId, ProductId);

                var coupons = hp.HiDealCouponGetListInAvailableAfterIndex(DealId, ProductId, _lastCouponId + 1,
                                                                      QueryCouponQuantityAtOneTime);
                //有資料
                if (coupons.Count > 0)
                {
                    _lastCouponId = coupons.Last().Id;
                }
                else
                {
                    //沒有查到資料
                    if (_lastCouponId != 0)
                    {
                        //範圍放到最大，重新查詢一次
                        coupons = hp.HiDealCouponGetListInAvailableAfterIndex(DealId, ProductId, 0,
                                                                              QueryCouponQuantityAtOneTime);
                        if (coupons.Count > 0)
                        {
                            _lastCouponId = coupons.Last().Id;
                        }
                        else
                        {
                            _lastCouponId = 0;
                        }
                    }
                }
                var newOne = coupons.Select(coupon => coupon.Id).ToList();
                _standbyCouponList = newOne;
                _issuerIndex = 0;
            }

            #endregion private method

            #region StoreCoupon

            private class StoreCoupon
            {
                /// <summary>
                /// 商品庫存
                /// </summary>
                public int InventoryQuantity { get; set; }

                /// <summary>
                /// 設定的庫存上限
                /// </summary>
                public int QuantityLimit { get; set; }

                public int DealId { get; private set; }

                public int ProductId { get; private set; }

                public Guid StoreGuid { get; private set; }

                public string StoreName { get; private set; }

                public int Seq { get; private set; }

                private List<long> _standbyCouponList;
                private long _lastCouponId;

                /// <summary>
                /// 目前已分配到_standbyCouponList中的第幾筆
                /// </summary>
                private int _issuerIndex;

                public StoreCoupon(int dealId, int productId, ViewHiDealProductStore store)
                {
                    DealId = dealId;
                    ProductId = productId;
                    StoreGuid = store.StoreGuid;
                    StoreName = store.StoreName;
                    _lastCouponId = 0;
                    _issuerIndex = 0;
                    Seq = store.Seq == null ? 0 : store.Seq.Value;
                    QuantityLimit = store.QuantityLimit == null ? 0 : store.QuantityLimit.Value;
                    //查詢憑證
                    LoadCouponToStandby();
                }

                /// <summary>
                /// 輸入需要的coupon數量，如果庫存還有足夠的數量，回傳配給的Coupon鍵值(ID)的List
                /// 若數量不夠，或者該檔次是依據分店數量產生憑證，則回傳空的List
                /// </summary>
                /// <param name="quantity"></param>
                /// <returns></returns>
                public List<long> RequestCouponList(int quantity)
                {
                    lock (this)
                    {
                        var rtnList = new List<long>();
                        if (quantity <= InventoryQuantity)
                        {
                            //暫存List中沒有足夠的憑證，重新查詢。
                            if ((_issuerIndex + quantity) > _standbyCouponList.Count)
                            {
                                //重新查詢。
                                LoadCouponToStandby();
                                //重新判斷剩餘數量是否足夠
                                if (quantity > InventoryQuantity)
                                {
                                    //數量依然不足
                                    return new List<long>();
                                }

                                //有數量可是無憑證==>時間差造成的差異
                                if ((quantity <= InventoryQuantity) && (quantity > _standbyCouponList.Count))
                                {
                                    //修改剩餘數量
                                    InventoryQuantity = _standbyCouponList.Count;
                                    return new List<long>();
                                }
                            }

                            //開始分配序號
                            for (var i = 0; i < quantity; i++)
                            {
                                rtnList.Add(_standbyCouponList[_issuerIndex]);
                                _issuerIndex++;
                                InventoryQuantity--;
                            }
                        }
                        return rtnList;
                    }
                }

                #region private method

                private void LoadCouponToStandby()
                {
                    //重新查詢剩餘的coupon數
                    InventoryQuantity = hp.HiDealCouponGetNotInHiDealCouponOfferCount(DealId, ProductId, StoreGuid);

                    var coupons = hp.HiDealCouponGetListInAvailableAfterIndex(DealId, ProductId, StoreGuid, _lastCouponId + 1,
                                                                          QueryCouponQuantityAtOneTime);
                    //有資料
                    if (coupons.Count > 0)
                    {
                        _lastCouponId = coupons.Last().Id;
                    }
                    else
                    {
                        //沒有查到資料
                        if (_lastCouponId != 0)
                        {
                            //範圍放到最大，重新查詢一次
                            coupons = hp.HiDealCouponGetListInAvailableAfterIndex(DealId, ProductId, StoreGuid, 0,
                                                                                  QueryCouponQuantityAtOneTime);
                            if (coupons.Count > 0)
                            {
                                _lastCouponId = coupons.Last().Id;
                            }
                            else
                            {
                                _lastCouponId = 0;
                            }
                        }
                    }
                    var newOne = coupons.Select(coupon => coupon.Id).ToList();
                    _standbyCouponList = newOne;
                    _issuerIndex = 0;
                }

                #endregion private method
            }

            #endregion StoreCoupon
        }

        #endregion HiDealCouponIssuer
    }

    #region HiDealProductStoreInventory

    [Serializable]
    public class HiDealProductStoreInventory
    {
        public Guid StoreGuid { get; set; }

        public string StoreName { get; set; }

        public int Inventory { get; set; }

        public int Seq { get; set; }
    }

    #endregion HiDealProductStoreInventory

    public static class HiDealCouponExtension
    {
        /// <summary>
        /// 檢查Coupon是否為有效的憑證(尚未使用與尚未作廢判定為有效，其他包含系統作業中判定為無效。)
        /// </summary>
        /// <param name="coupon"></param>
        /// <returns>有效回傳true , 否則為false</returns>
        public static bool CouponIsEnabled(this HiDealCoupon coupon)
        {
            var rtn = false;
            switch ((HiDealCouponStatus)coupon.Status)
            {
                case HiDealCouponStatus.Available:
                    rtn = true;
                    break;

                case HiDealCouponStatus.Assigned:
                    rtn = true;
                    break;

                case HiDealCouponStatus.Used:
                    rtn = false;
                    break;

                case HiDealCouponStatus.Cancel:
                    rtn = false;
                    break;

                case HiDealCouponStatus.Refund:
                    rtn = false;
                    break;

                case HiDealCouponStatus.Marked:
                    rtn = false;
                    break;
            }
            return rtn;
        }
    }
}
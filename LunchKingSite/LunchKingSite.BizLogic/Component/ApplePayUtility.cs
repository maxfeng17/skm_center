﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model;
using System.Security.Cryptography;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using log4net;
using LunchKingSite.BizLogic.Component.Decryption.Parameters;
using LunchKingSite.BizLogic.Component.Decryption.Generators;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1.Sec;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.Encoders;
using Org.BouncyCastle.Cms;
using Org.BouncyCastle.X509.Store;
using Org.BouncyCastle.Asn1.Cms;
using Org.BouncyCastle.Pkix;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Component
{
    public class ApplePayUtility
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(ApplePayUtility).Name);
        private static readonly string G3Thumbprint = "b52cb02fd567e0359fe8fa4d4c41037970fe01b0";
        private static readonly byte[] symmetricIv = Hex.Decode("00000000000000000000000000000000");
        private static readonly string LEAF_OID = "1.2.840.113635.100.6.29";
        private static readonly string INTERMEDIATE_OID = "1.2.840.113635.100.6.2.14";
        private static readonly string COLLECTION = "Collection";
        private static int APPLE_PAY_SIGNATURE_EXPIRATION_IN_MS = 60000;
        private static readonly string privateKey = "MHcCAQEEILXR3ucU83w9Wy3qwQ9WvLkeOkE4gCtCkAoVG7HWRGsEoAoGCCqGSM49AwEHoUQDQgAEU8ATSH9YQiYTDCynv4rqkDLt2teq5FglgCZB2qpiNQXx3U9U02r7b8uQlCWgXIWpc85UAN+AopvpFH2ivC+cpg==";
        private static byte[] encryptionKeyBytes;
        //private static readonly string merchantIdentifier;


        public static ApplePayData DecryptToken(ApplePayToken token)
        {
            try
            {
                if (VerifySignature(token))
                {
                    var payData = Decrypt(token.Data, token.Signature, token.Header.EphemeralPublicKey);
                    logger.Info("decypt token ok");
                    var data = JsonConvert.DeserializeObject<ApplePayData>(payData);
                    return data;
                }
            }
            catch (Exception ex)
            {
                logger.Error("decrypt token error " + ex.Message);
            }

            return null;
        }

        private static string Decrypt(string encryptedMessage, string signature, string publicKey)
        {
            var PrivateKeyParameters = CreatePrivateKeyParameters(Base64.Decode(config.ApplePayPrivateKey));
            var PublicKeyParameters = CreatePublicKeyParameters(Base64.Decode(publicKey));

            byte[] sharedSecretBytes = GenerateSharedSecret(PrivateKeyParameters, PublicKeyParameters);
            RestoreSymmertricKey(sharedSecretBytes);

            // Decrypting the message.
            var decrypedBytes = DoDecrypt(Base64.Decode(encryptedMessage));

            return Encoding.UTF8.GetString(decrypedBytes);
        }

        private static ECPrivateKeyParameters CreatePrivateKeyParameters(byte[] PrivateKeyBytes)
        {
            Asn1Sequence seq = (Asn1Sequence)Asn1Object.FromByteArray(PrivateKeyBytes);
            ECPrivateKeyStructure pKey = GetInstance(seq);
            AlgorithmIdentifier algId = new AlgorithmIdentifier(X9ObjectIdentifiers.IdECPublicKey, pKey.GetParameters());

            PrivateKeyInfo privInfo = new PrivateKeyInfo(algId, pKey.ToAsn1Object());

            return (ECPrivateKeyParameters)PrivateKeyFactory.CreateKey(privInfo);
        }

        private static ECPrivateKeyStructure GetInstance(object obj)
        {
            if (obj == null)
            {
                return null;
            }

            if (obj is ECPrivateKeyStructure)
            {
                return (ECPrivateKeyStructure) obj;
            }

            return new ECPrivateKeyStructure(Asn1Sequence.GetInstance(obj));
        }

        private static ECPublicKeyParameters CreatePublicKeyParameters(byte[] PublicKeyBytes)
        {
            return (ECPublicKeyParameters)PublicKeyFactory.CreateKey(PublicKeyBytes);
        }

        private static byte[] GenerateSharedSecret(ECPrivateKeyParameters PrivateKeyParameters, ECPublicKeyParameters PublicKeyParameters)
        {
            ECPrivateKeyParameters keyParams = PrivateKeyParameters;
            IBasicAgreement agree = AgreementUtilities.GetBasicAgreement("ECDH");
            agree.Init(keyParams);
            Org.BouncyCastle.Math.BigInteger sharedSecret = agree.CalculateAgreement(PublicKeyParameters);
            return sharedSecret.ToByteArrayUnsigned();
        }

        private static void RestoreSymmertricKey(byte[] sharedSecretBytes)
        {
            byte[] merchantIdentifier = ExtractMIdentifier();

            ConcatenationKdfGenerator generator = new ConcatenationKdfGenerator(new Sha256Digest());
            byte[] algorithmIdBytes = Encoding.UTF8.GetBytes((char)0x0d + "id-aes256-GCM");
            byte[] partyUInfoBytes = Encoding.UTF8.GetBytes("Apple");
            byte[] partyVInfoBytes = merchantIdentifier;
            byte[] otherInfoBytes = Combine(Combine(algorithmIdBytes, partyUInfoBytes), partyVInfoBytes);

            generator.Init(new KdfParameters(sharedSecretBytes, otherInfoBytes));
            encryptionKeyBytes = new byte[32];
            generator.GenerateBytes(encryptionKeyBytes, 0, encryptionKeyBytes.Length);
        }

        //private static X509Certificate2 InflateCertificate()
        //{
        //    byte[] bytes = Convert.FromBase64String(merchantIdentifier);

        //    return new X509Certificate2(bytes);
        //}

        private static byte[] ExtractMIdentifier()
        {
            X509Certificate2 merchantCertificate = LoadMerchantCertificate(config.ApplePayMerchantCerThumbprint);
            byte[] merchantIdentifierTlv = merchantCertificate.Extensions["1.2.840.113635.100.6.32"].RawData;
            byte[] merchantIdentifier = new byte[64];

            Buffer.BlockCopy(merchantIdentifierTlv, 2, merchantIdentifier, 0, 64);

            return Hex.Decode(Encoding.ASCII.GetString(merchantIdentifier));

        }

        private static bool VerifySignature(ApplePayToken token)
        {
            try
            {
                byte[] signedData = GetSignedData(token.Data, token.Header);
                CmsSignedData cmsSignedData = new CmsSignedData(new CmsProcessableByteArray(signedData), Base64.Decode(token.Signature));
                var store = cmsSignedData.GetCertificates(COLLECTION);
                ArrayList allCertificates = (ArrayList)store.GetMatches(null);
                ArrayList singers = (ArrayList)cmsSignedData.GetSignerInfos().GetSigners();
                var signerInformation = (SignerInformation)singers[0];

                List<Org.BouncyCastle.X509.X509Certificate> x509Certificates = new List<Org.BouncyCastle.X509.X509Certificate>();
                foreach (Org.BouncyCastle.X509.X509Certificate certificate in allCertificates)
                {
                    x509Certificates.Add(certificate);
                }

                // step 1:
                // Ensure that the certificates contain the correct custom OIDs: 1.2.840.113635.100.6.29
                // for the leaf certificate and 1.2.840.113635.100.6.2.14 for the intermediate CA. The value for these marker OIDs doesn’t matter, only their presence.
                ValidateCustomData(allCertificates);

                Org.BouncyCastle.X509.X509Certificate appleRootCertificate = null;

                // step 2:
                // Ensure that the root CA is the Apple Root CA - G3. This certificate is available from apple.com/certificateauthority.
                try
                {
                    var g3Cer = LoadMerchantCertificate(G3Thumbprint);
                    appleRootCertificate = DotNetUtilities.FromX509Certificate(g3Cer);
                }
                catch
                {

                }
                finally
                {
                    //IOUtils.closeQuietly(inputStream);
                }

                // step 3:
                // Ensure that there is a valid X.509 chain of trust from the signature to the root CA. Specifically,
                // ensure that the signature was created using the private key corresponding to the leaf certificate,
                // that the leaf certificate is signed by the intermediate CA, and that the intermediate CA is signed by the Apple Root CA - G3.
                VerifyCertificate(x509Certificates[0], appleRootCertificate, allCertificates);

                // step 4:
                // Ensure that the signature is a valid ECDSA signature (ecdsa-with-SHA256 1.2.840.10045.4.3.2) of the
                // concatenated values of the ephemeralPublicKey, data, transactionId, and applicationData keys.
                ValidateSignature(signerInformation, store);

                // step 5:
                // Inspect the CMS signing time of the signature, as defined by section 11.3 of RFC 5652.
                // If the time signature and the transaction time differ by more than a few minutes, it's possible that the token is a replay attack.
                ValidateSignatureTime(APPLE_PAY_SIGNATURE_EXPIRATION_IN_MS, signerInformation);

                return true;
            }
            catch(Exception ex)
            {
                logger.Info("apple pay signature failed:" + ex.Message);
            }
            return false;
        }

        private static byte[] DoDecrypt(byte[] cipherData)
        {
            byte[] output;
            try
            {
                KeyParameter keyparam = ParameterUtilities.CreateKeyParameter("AES", encryptionKeyBytes);
                ParametersWithIV parameters = new ParametersWithIV(keyparam, symmetricIv);
                IBufferedCipher cipher = GetCipher();
                cipher.Init(false, parameters);
                try
                {
                    output = cipher.DoFinal(cipherData);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    throw new ApplicationException("Invalid Data");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw new ApplicationException("There was an error occured when decrypting message.");
            }

            return output;
        }

        private static IBufferedCipher GetCipher()
        {
            return CipherUtilities.GetCipher("AES/GCM/NoPadding");
        }

        private static byte[] Combine(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        public static string GetMerchantIdentifier(X509Certificate2 certificate)
        {
            // This OID returns the ASN.1 encoded merchant identifier
            var extension = certificate.Extensions["1.2.840.113635.100.6.32"];

            if (extension == null)
            {
                return string.Empty;
            }

            // Convert the raw ASN.1 data to a string containing the ID
            return System.Text.Encoding.ASCII.GetString(extension.RawData).Substring(2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantCertificateThumbprint"></param>
        /// <returns></returns>
        public static X509Certificate2 LoadMerchantCertificate(string certificateThumbprint)
        {
            X509Certificate2 certificate;
            var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            string thumbprintId = Regex.Replace(certificateThumbprint, @"\s|\W", "").ToUpper();
            //logger.Info("thumbprintId:" + thumbprintId);
            foreach (var cer in store.Certificates)
            {
                string tid = cer.Thumbprint;
                if (tid == thumbprintId)
                {
                    certificate = cer;
                    store.Close();
                    return certificate;
                }
            }
            store.Close();
            return null;
        }

        private static void ValidateCustomData(ArrayList allCertificates)
        {
            if (allCertificates.Count != 2)
            {
                throw new Exception("signature certificates count expected 2, but it's :" + allCertificates.Count);
            }

            if (((Org.BouncyCastle.X509.X509Certificate)allCertificates[0]).CertificateStructure.TbsCertificate.Extensions.GetExtension(new DerObjectIdentifier(LEAF_OID)) == null)
            {
                throw new Exception("leaf certificate doesn't have extension: " + LEAF_OID);
            }
            if (((Org.BouncyCastle.X509.X509Certificate)allCertificates[1]).CertificateStructure.TbsCertificate.Extensions.GetExtension(new DerObjectIdentifier(INTERMEDIATE_OID)) == null)
            {
                throw new Exception("intermediate certificate doesn't have extension: " + INTERMEDIATE_OID);
            }
        }

        private static void ValidateSignature(SignerInformation signerInformation, IX509Store store)
        {
            try
            {
                ArrayList certCollection = (ArrayList)store.GetMatches(signerInformation.SignerID);
                var certIt = certCollection.GetEnumerator();
                object certHolder = null;
                while (certIt.MoveNext())
                {
                    certHolder = certIt.Current;
                }
                Org.BouncyCastle.X509.X509Certificate cert = (Org.BouncyCastle.X509.X509Certificate)certHolder;
                signerInformation.Verify(cert);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to verify apple pay signature, the result is false");
            }
        }

        private static void ValidateSignatureTime(long applePaySignatureExpirationInMs, SignerInformation signerInformation)
        {
            DateTime? signDate = null;
            Org.BouncyCastle.Asn1.Cms.AttributeTable signedAttributes = signerInformation.SignedAttributes;
            Org.BouncyCastle.Asn1.Cms.Attribute signingTime = signedAttributes[CmsAttributes.SigningTime];
            var signingTimeObjects = signingTime.AttrValues.GetEnumerator();
            if (signingTimeObjects != null)
            {
                object signingTimeObject = null;
                while (signingTimeObjects.MoveNext())
                {
                    signingTimeObject = signingTimeObjects.Current;
                }
                if (typeof(DerUtcTime).Equals(signingTimeObject.GetType()))
                {
                    Org.BouncyCastle.Asn1.DerUtcTime asn1Time = (Org.BouncyCastle.Asn1.DerUtcTime)signingTimeObject;
                    signDate = asn1Time.ToDateTime();
                }
            }
            if (signDate == null)
            {
                throw new Exception("Failed to extract sign time from apple pay signature.");
            }

            if ((long)(DateTime.UtcNow - signDate.Value).TotalMilliseconds > applePaySignatureExpirationInMs)
            {
                throw new Exception("apple pay signature is too old, the expiration time is: " + applePaySignatureExpirationInMs + " ms");
            }
        }

        private static void VerifyCertificate(Org.BouncyCastle.X509.X509Certificate leafCertificate, Org.BouncyCastle.X509.X509Certificate trustedRootCert, ArrayList intermediateCerts)
        {
            try
            {
                // Create the selector that specifies the starting certificate
                X509CertStoreSelector selector = new X509CertStoreSelector();
                selector.Certificate = leafCertificate;

                // Create the trust anchors (set of root CA certificates)
                Org.BouncyCastle.Utilities.Collections.ISet trustAnchors = new Org.BouncyCastle.Utilities.Collections.HashSet();
                trustAnchors.Add(new TrustAnchor(trustedRootCert, null));

                // Configure the PKIX certificate builder algorithm parameters
                PkixBuilderParameters pkixParams = new PkixBuilderParameters(trustAnchors, selector);

                // Disable CRL checks (this is done manually as additional step)
                pkixParams.IsRevocationEnabled = false;

                // Specify a list of intermediate certificates
                X509CollectionStoreParameters intermediateCertStore = new X509CollectionStoreParameters(intermediateCerts);
                pkixParams.AddStore(X509StoreFactory.Create("Certificate/Collection", intermediateCertStore));

                // Build and verify the certification chain
                PkixCertPathBuilder builder = new PkixCertPathBuilder();

                // If no exception thrown, it means the validation passed.
                PkixCertPathBuilderResult pkixCertPathBuilderResult = builder.Build(pkixParams);

            }
            catch (Exception ex)
            {
                throw new Exception("Failed to validate chain of trust for apple certificates.");
            }
        }


        private static byte[] GetSignedData(string applePayData, ApplePayTokenHeader applePayHeader)
        {
            byte[] ephemeralPublicKeyBytes = Base64.Decode(applePayHeader.EphemeralPublicKey);
            byte[] applePayDataBytes = Base64.Decode(applePayData);
            byte[] transactionIdBytes = Hex.Decode(applePayHeader.TransactionId);

            using (MemoryStream stream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(ephemeralPublicKeyBytes);
                    writer.Write(applePayDataBytes);
                    writer.Write(transactionIdBytes);
                }
                return stream.ToArray();
            }
        }
    }

    
}

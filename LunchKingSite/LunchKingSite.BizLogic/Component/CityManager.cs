﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public static class CityManagerExtention
    {
        private static List<City> _citysNoIsland = null;
        public static List<City> WithoutIsland(this CityCollection citys)
        {
            if (_citysNoIsland == null)
            {
                _citysNoIsland = citys.Where(x => x.Visible).ToList();
            }

            return _citysNoIsland;
        }
    }

    public class CityManager
    {
        private static ILocationProvider _locProv;
        private static IPponProvider _pponProv;
        private static ISystemProvider _systemProvider;


        private static CityGroupCollection _cityGroups;
        public static CityGroupCollection CityGroups
        {
            get
            {
                if ((_cityGroups == null) || (_cityGroups.Count == 0))
                {
                    ReloadDefaultManager();
                }
                return _cityGroups.Clone();
            }
        }

        private static CityCollection _citys;
        /// <summary>
        /// 取出台灣 縣市(包含直轄市)層級的city資料
        /// </summary>
        public static CityCollection Citys
        {
            get
            {
                if ((_citys == null) || (_citys.Count == 0))
                {
                    ReloadDefaultManager();
                }
                return _citys.Clone();
            }
        }

        /// <summary>
        /// 取得所有parent_id為null的city (包含玩美、旅遊、...)
        /// </summary>
        private static CityCollection _headCitys;

        private static CityCollection _townShips;
        public static CityCollection TownShips
        {
            get
            {
                if ((_townShips == null) || (_townShips.Count == 0))
                {
                    ReloadDefaultManager();
                }
                return _townShips;
            }
        }

        private const string CityVersionExtendedPropertyName = "city_version";
        private static int _cityDataVersion;
        public static int CityDataVersion
        {
            get
            {
                //借助_citys判斷資料是否已查詢
                if ((_citys == null) || (_citys.Count == 0))
                {
                    ReloadDefaultManager();
                }
                return _cityDataVersion;
            }
        }


        private const string CityGroupVersionExtendedPropertyName = "city_group_version";
        private static int _cityGroupDataVersion;
        public static int CityGroupDataVersion
        {
            get
            {
                //借助_citys判斷資料是否已查詢
                if ((_citys == null) || (_citys.Count == 0))
                {
                    ReloadDefaultManager();
                }
                return _cityGroupDataVersion;
            }
        }



        public static void ReloadDefaultManager()
        {
            _citys = _locProv.CityGetListSetOrder(City.Columns.Rank, City.Columns.ParentId + " is null",
                City.Columns.Code + "<>" + PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityCode,
                City.Columns.Code + "<>" + PponCityGroup.DefaultPponCityGroup.Travel.CityCode,
                City.Columns.Code + "<>" + PponCityGroup.DefaultPponCityGroup.AllCountry.CityCode,
                City.Columns.Code + "<>" + PponCityGroup.DefaultPponCityGroup.Family.CityCode,
                City.Columns.Code + "<>" + PponCityGroup.DefaultPponCityGroup.Skm.CityCode,
                City.Columns.Code + "<>" + PponCityGroup.DefaultPponCityGroup.DepositCoffee.CityCode,
                City.Columns.Code + "<>" + PponCityGroup.DefaultPponCityGroup.ArriveIn24Hrs.CityCode,
                City.Columns.Code + "<>" + PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityCode,
                City.Columns.Code + "<>" + PponCityGroup.DefaultPponCityGroup.Tmall.CityCode,
                City.Columns.Code + "<>" + PponCityGroup.DefaultPponCityGroup.Piinlife.CityCode,
                City.Columns.Code + "<>" + PponCityGroup.DefaultPponCityGroup.PponSelect.CityCode,
                City.Columns.Code + "<>com");
            _headCitys = _locProv.CityGetListSetOrder(City.Columns.Rank, City.Columns.ParentId + " is null");
            _townShips = _locProv.CityGetListSetOrder(City.Columns.ZipCode, City.Columns.ParentId + " is not null");
            _cityGroups = _pponProv.CityGroupGetList(0, 0, CityGroup.Columns.CityId, CityGroup.EnableColumn + " = " + true);

            #region CityDataVersion
            //查詢目前City資料設定的版本，供前端判斷是否資料已異動
            string cityVersionData = _systemProvider.DBExtendedPropertiesGet(CityVersionExtendedPropertyName);
            //若查無資料或取出的值不為int，預設為0
            if (!int.TryParse(cityVersionData, out _cityDataVersion))
            {
                _cityDataVersion = 0;
            }
            #endregion CityDataVersion

            #region CityGroupDataVersion
            //查詢cityGroup資料設定的版本，供前端作業判斷資料是否已異動
            string cityGroupVersionData = _systemProvider.DBExtendedPropertiesGet(CityGroupVersionExtendedPropertyName);
            //若查無資料或取出的值不為int，預設為0
            if (!int.TryParse(cityGroupVersionData, out _cityGroupDataVersion))
            {
                _cityGroupDataVersion = 0;
            }
            #endregion CityGroupDataVersion
        }

        /// <summary>
        /// 依據城市id取得鄉鎮市區清單
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static List<City> TownShipGetListByCityId(int cityId)
        {
            return TownShips.Where(x => x.ParentId == cityId).ToList();
        }
        /// <summary>
        /// 依據城市id取得鄉鎮市區清單(排除離島)
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static List<City> TownShipGetListByParentCityIdWithoutIsland(int cityId)
        {
            return TownShipGetListByCityId(cityId).Where(x => x.Visible).ToList();
        }
        /// <summary>
        /// 回傳cityId為傳入值得City物件，查無資料回傳null
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static City CityGetById(int cityId)
        {
            var citys = Citys.Where(x => x.Id == cityId).ToList();
            if (citys.Count > 0)
            {
                return citys.First();
            }
            return null;
        }
        /// <summary>
        /// 以code取得parentId為null的cityId
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static int GetHeadCityByCode(string code)
        {
            int cityId = 0;
            City city = null;
            city = _headCitys.FirstOrDefault(x => x.Code == code);
            if (city != null)
            {
                cityId = city.Id;
            }
            return cityId;
        }
        /// <summary>
        /// 回傳cityId為傳入值得City物件，查無資料回傳null
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static City TownShipGetById(int cityId)
        {
            var citys = TownShips.Where(x => x.Id == cityId).ToList();
            if (citys.Count > 0)
            {
                return citys.First();
            }
            return null;
        }

        public static City TownShipGetByName(string cityName)
        {
            return TownShips.First(t => t.CityName == cityName);
        }

        /// <summary>
        /// 傳入鄉鎮市區的id，回傳城市與鄉鎮市區的字串，如:台北市中山區，若查無對應資料回傳string.Empty
        /// </summary>
        /// <param name="townShopId"></param>
        /// <returns></returns>
        public static string CityTownShopStringGet(int townShopId)
        {
            var townShips = TownShips.Where(x => x.Id == townShopId).ToList();
            if (townShips.Count == 0)
            {
                return string.Empty;
            }
            var citys = Citys.Where(x => x.Id == townShips.First().ParentId).ToList();
            if (citys.Count == 0)
            {
                return string.Empty;
            }
            return citys.First().CityName + townShips.First().CityName;
        }
        /// <summary>
        /// 檢查傳入的id是否為合法的cityId鍵值
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static bool CheckExistsCity(int cityId)
        {
            if (Citys.Where(x => x.Id == cityId).Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 檢查傳入的townshipId是否為存在cityId中的合法鄉鎮市區鍵值
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="townshipId"></param>
        /// <returns></returns>
        public static bool CheckExistsTownship(int cityId, int townshipId)
        {
            var citys = Citys.Where(x => x.Id == cityId).ToList();
            if (citys.Count > 0)
            {
                var townships = TownShipGetListByCityId(cityId);
                if (townships.Where(x => x.Id == townshipId).Count() > 0)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// 依據傳入的cityId取得Building的資料
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static BuildingCollection BuildingListGetByTownshipId(int cityId)
        {
            return _locProv.BuildingGetListByArea(cityId);
        }

        /// <summary>
        /// 依據傳入的cityId取得Building的資料
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static BuildingCollection BuildingListGetByName(string buildingName)
        {
            return _locProv.BuildingGetListByName(buildingName);
        }

        /// <summary>
        /// 依據傳入的路段Guid產生 包含路段名稱之前的地址字串
        /// </summary>
        /// <param name="buildingGuid"></param>
        /// <returns></returns>
        public static string CityTownShopBuildingStringGet(Guid buildingGuid)
        {
            var building = _locProv.BuildingGet(buildingGuid);
            if (building.IsLoaded)
            {
                return CityTownShopStringGet(building.CityId) + building.BuildingName;
            }
            else
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// 傳入地址字串比對城市
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static City CityGetByStringAddress(string address)
        {
            return Citys.Where(x => address.StartsWith(x.CityName)).FirstOrDefault();
        }
        /// <summary>
        /// 傳入城市及地址字串比對鄉鎮市區
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        public static City TownshipIdGetByStringAddress(int cityId, string address)
        {
            List<City> townShiptList = TownShipGetListByCityId(cityId);
            return townShiptList.Where(x => address.IndexOf(x.CityName) > 0).FirstOrDefault();
        }

        /// <summary>
        /// 依據類別取得CityGroup的資料
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<CityGroup> CityGroupGetListByType(CityGroupType type)
        {
            return CityGroups.Where(x => x.Type == (int)type).ToList();
        }

        /// <summary>
        /// 依據 城市的中文名稱 回傳city物件，若查無資料回傳null
        /// </summary>
        /// <param name="cityName"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static CityGroup CityGroupGetByCityName(string cityName, CityGroupType type)
        {
            List<CityGroup> accordCitys = _cityGroups.Where(x => x.CityName == cityName && x.Type == (int)type).ToList();
            if (accordCitys.Count > 0)
            {
                return accordCitys.First();
            }
            return null;
        }

        /// <summary>
        /// 用郵遞區號取得城市，最上層城市
        /// </summary>
        /// <param name="zip_code"></param>
        /// <returns></returns>
        public static City CityGetByZipCode(string zip_code)
        {
            City city;
            if ((city = Citys.FirstOrDefault(x => !string.IsNullOrEmpty(x.ZipCode) && x.ZipCode.Equals(zip_code))) != null)
            {
                return city;
            }
            else if ((city = TownShips.FirstOrDefault(x => !string.IsNullOrEmpty(x.ZipCode) && x.ZipCode.Equals(zip_code))) != null)
            {
                if (city.ParentId.HasValue)
                {
                    city = Citys.FirstOrDefault(x => x.Id == city.ParentId.Value);
                }
                return city;
            }
            else
            {
                return null;
            }
        }

        static CityManager()
        {
            _locProv = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _systemProvider = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            if ((_citys == null) || (_citys.Count == 0) || (_cityGroups == null) || (_cityGroups.Count == 0))
            {
                ReloadDefaultManager();
            }
        }

    }

    public class LkTownShips
    {
        public int ParentId { get; set; }
        public CityCollection Citys { get; set; }
    }
}

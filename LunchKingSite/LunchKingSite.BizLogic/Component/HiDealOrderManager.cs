﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace LunchKingSite.BizLogic.Component
{
    public class HiDealOrderManager
    {
        protected static IHiDealProvider hp;
        protected static IAccountingProvider ap;
        protected static ISysConfProvider config;
        protected static IOrderProvider op;
        private static readonly log4net.ILog log;

        static HiDealOrderManager()
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            config = ProviderFactory.Instance().GetConfig();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            log = log4net.LogManager.GetLogger(typeof(HiDealOrderManager));
        }

        /// <summary>
        /// 建立HiDealOrder訂單，如果成功，回傳建立的訂單Guid值，如果失敗，回傳Guid.Empty;
        /// </summary>
        /// <param name="product">購買的商品</param>
        /// <param name="info">購買資料</param>
        /// <param name="sellerName">商家名稱</param>
        /// <param name="orderGuid">建立的訂單Guid，是若建單失敗為Guid.Empty</param>
        /// <param name="orderId">建立的訂單編號，是若建單失敗為string.Empty</param>
        ///// <param name="receivableId">建立的應收帳款單編號，是若建單失敗為null</param>
        /// <param name="message">作業訊息</param>
        /// <returns></returns>
        public static MarkHiDealOrderReply MakeHiDealOrder(HiDealProduct product, HiDealDeliveryInfo info,
            string sellerName, out Guid orderGuid, out string orderId, out string message)
        {
            orderId = string.Empty;
            orderGuid = Guid.Empty;
            message = string.Empty;
            var rtn = MarkHiDealOrderReply.Default;
            var cashAmt = info.CreditCardAmt + info.AtmAmt;//實付金額
            //取出所有選取的選項
            var selOptions = info.SelectedCategories.Select(categorySel => categorySel.Selected).ToList();

            using (var transScope = new TransactionScope())
            {
                try
                {
                    message = string.Empty;
                    if (product.Id != info.Pid)
                    {
                        message = "購買資料與商品資料不合。";
                        return MarkHiDealOrderReply.InputDataError;
                    }
                    //檢查總金額
                    var totalAmt = info.Count * product.Price + info.FreightAmt;
                    if (totalAmt != info.TotalAmount)
                    {
                        message = "金額錯誤。";
                        return MarkHiDealOrderReply.InputDataError;
                    }

                    if ((totalAmt - info.BCashAmt - info.SCashAmt - info.PCashAmt - info.DiscountAmt -
                         info.CreditCardAmt -
                         info.AtmAmt) != 0)
                    {
                        message = "金額錯誤。";
                        return MarkHiDealOrderReply.InputDataError;
                    }

                    #region 訂單部分

                    orderId = HiDealSerialNumberManager.GetNewHiDealOrderId();


                    var order = new HiDealOrder();
                    order.Guid = Helper.GetNewGuid(order);
                    order.OrderId = orderId;
                    order.HiDealId = product.DealId;
                    order.UserId = info.UserId;
                    order.TotalAmount = info.TotalAmount;
                    order.Pcash = info.PCashAmt;
                    order.Scash = info.SCashAmt;
                    order.Bcash = info.BCashAmt;
                    order.DiscountAmount = info.DiscountAmt;
                    order.CreditCardAmt = info.CreditCardAmt;
                    order.AtmAmount = info.AtmAmt;
                    order.OrderStatus = (int)HiDealOrderStatus.Confirm;
                    order.CreateId = info.UserName;
                    order.CreateTime = DateTime.Now;
                    hp.HiDealOrderSet(order);

                    #endregion 訂單部分

                    #region 訂單明細部分

                    var orderDetails = new HiDealOrderDetailCollection();
                    //建立商品的訂單明細
                    var orderDetail = new HiDealOrderDetail();
                    orderDetail.Guid = Helper.GetNewGuid(orderDetail);
                    orderDetail.HiDealOrderGuid = order.Guid;
                    orderDetail.HiDealId = product.DealId;
                    orderDetail.SellerGuid = product.SellerGuid;
                    orderDetail.SellerName = sellerName;
                    orderDetail.ProductId = product.Id;
                    orderDetail.ProductName = product.Name;
                    orderDetail.ItemQuantity = info.Count;
                    orderDetail.UnitPrice = product.Price.Value;
                    orderDetail.DetailTotalAmt = product.Price.Value * info.Count;
                    if (info.SelectedCategories.Count > 0)
                    {
                        var selCategory = info.SelectedCategories[0];
                        orderDetail.Category1 = selCategory.Id;
                        orderDetail.CatgName1 = selCategory.CatgName;
                        orderDetail.Option1 = selCategory.Selected;
                        orderDetail.OptionName1 = selCategory.SelName;
                    }

                    if (info.SelectedCategories.Count > 1)
                    {
                        var selCategory = info.SelectedCategories[1];
                        orderDetail.Category2 = selCategory.Id;
                        orderDetail.CatgName2 = selCategory.CatgName;
                        orderDetail.Option2 = selCategory.Selected;
                        orderDetail.OptionName2 = selCategory.SelName;
                    }

                    if (info.SelectedCategories.Count > 2)
                    {
                        var selCategory = info.SelectedCategories[2];
                        orderDetail.Category3 = selCategory.Id;
                        orderDetail.CatgName3 = selCategory.CatgName;
                        orderDetail.Option3 = selCategory.Selected;
                        orderDetail.OptionName3 = selCategory.SelName;
                    }

                    if (info.SelectedCategories.Count > 3)
                    {
                        var selCategory = info.SelectedCategories[3];
                        orderDetail.Category4 = selCategory.Id;
                        orderDetail.CatgName4 = selCategory.CatgName;
                        orderDetail.Option4 = selCategory.Selected;
                        orderDetail.OptionName4 = selCategory.SelName;
                    }

                    if (info.SelectedCategories.Count > 4)
                    {
                        var selCategory = info.SelectedCategories[4];
                        orderDetail.Category5 = selCategory.Id;
                        orderDetail.CatgName5 = selCategory.CatgName;
                        orderDetail.Option5 = selCategory.Selected;
                        orderDetail.OptionName5 = selCategory.SelName;
                    }
                    orderDetail.DeliveryType = (int)info.DeliveryType;
                    if (info.StoreGuid == Guid.Empty)
                    {
                        orderDetail.StoreGuid = null;
                    }
                    else
                    {
                        orderDetail.StoreGuid = info.StoreGuid;
                    }
                    orderDetail.AddresseeName = info.AddresseeName;
                    orderDetail.AddresseePhone = info.AddresseePhone;
                    orderDetail.DeliveryAddress = info.AddresseeAddress.ToString();
                    orderDetail.ProductType = (int)HiDealProductType.Product;
                    orderDetail.CreateId = info.UserName;
                    orderDetail.CreateTime = DateTime.Now;
                    orderDetails.Add(orderDetail);

                    //建立運費的訂單明細
                    var freightProduct = HiDealFreightManager.GetFreightProduct();
                    if (info.DeliveryType != null && info.DeliveryType.Value == HiDealDeliveryType.ToHouse && info.FreightAmt > 0)
                    {
                        var orderDetailFre = new HiDealOrderDetail();
                        orderDetailFre.Guid = Helper.GetNewGuid(orderDetailFre);
                        orderDetailFre.HiDealOrderGuid = order.Guid;
                        orderDetailFre.HiDealId = product.DealId;
                        orderDetailFre.SellerGuid = product.SellerGuid;
                        orderDetailFre.SellerName = sellerName;
                        orderDetailFre.ProductId = freightProduct.Id;
                        orderDetailFre.ProductName = freightProduct.Name;
                        orderDetailFre.ItemQuantity = 1;
                        orderDetailFre.UnitPrice = info.FreightAmt;
                        orderDetailFre.DetailTotalAmt = info.FreightAmt;
                        orderDetailFre.Option1 = null;
                        orderDetailFre.OptionName1 = string.Empty;
                        orderDetailFre.Option2 = null;
                        orderDetailFre.OptionName2 = string.Empty;
                        orderDetailFre.Option3 = null;
                        orderDetailFre.OptionName3 = string.Empty;
                        orderDetailFre.Option4 = null;
                        orderDetailFre.OptionName4 = string.Empty;
                        orderDetailFre.Option5 = null;
                        orderDetailFre.OptionName5 = string.Empty;
                        orderDetailFre.ProductType = (int)HiDealProductType.Freight;
                        orderDetailFre.CreateId = info.UserName;
                        orderDetailFre.CreateTime = DateTime.Now;
                        orderDetails.Add(orderDetailFre);
                    }
                    hp.HiDealOrderDetailSetList(orderDetails);

                    #endregion 訂單明細部分

                    #region 訂單商品明細(選項拆分)

                    //取得檔次與商品名稱 以紀錄購買當下購買資訊
                    string orderProductName = string.Format("{0}\\n{1}", hp.HiDealDealGet(product.DealId).Name, product.Name);

                    for (int i = 0; i < info.Count; i++)
                    {
                        var orderProduct = new OrderProduct();
                        orderProduct.OrderGuid = order.Guid;
                        orderProduct.Name = orderProductName;
                        orderProduct.IsOriginal = true;
                        orderProduct.IsCurrent = true;
                        orderProduct.IsReturning = false;
                        orderProduct.IsExchanging = false;

                        int orderProductId = op.OrderProductSet(orderProduct);

                        foreach (var item in info.SelectedCategories)
                        {
                            var orderProductOption = new OrderProductOption();
                            orderProductOption.OrderProductId = orderProductId;
                            orderProductOption.OptionId = item.Selected;
                            orderProductOption.IsOldProduct = false;

                            op.OrderProductOptionSet(orderProductOption);
                        }
                    }

                    #endregion 訂單商品明細(選項拆分)

                    #region 建立應收帳款

                    var receivables = new ReceivableCollection();
                    //以現金購買購物金
                    var receivablePoint = new Receivable();

                    receivablePoint.ClassificationGuid = order.Guid;
                    receivablePoint.AccountsClassification = (int)AccountsClassification.HiDealOrder;
                    receivablePoint.TotalAmount = cashAmt;
                    receivablePoint.Pcash = 0;
                    receivablePoint.Scash = 0;
                    receivablePoint.Bcash = 0;
                    receivablePoint.CreditCardAmt = info.CreditCardAmt;
                    receivablePoint.DiscountAmount = 0;
                    receivablePoint.AtmAmount = info.AtmAmt;
                    receivablePoint.ReceivableStatus = (int)ReceivableStatus.NotReceived;
                    receivablePoint.InvoiceId = null;
                    receivablePoint.CreateId = info.UserName;
                    receivablePoint.CreateTime = DateTime.Now;
                    receivables.Add(receivablePoint);
                    //建立支付17life購物金的應收帳款
                    var payableSCash = new Payable();
                    payableSCash.ClassificationGuid = order.Guid;
                    payableSCash.AccountsClassification = (int)AccountsClassification.HiDealOrder;
                    payableSCash.TotalAmount = cashAmt;
                    payableSCash.Pcash = 0;
                    payableSCash.Scash = cashAmt;
                    payableSCash.Bcash = 0;
                    payableSCash.CreditCardAmt = 0;
                    payableSCash.DiscountAmount = 0;
                    payableSCash.AtmAmount = 0;
                    payableSCash.PayableStatus = (int)PayableStatus.NotPaid;
                    payableSCash.InvoiceId = null;
                    payableSCash.CreateId = info.UserName;
                    payableSCash.CreateTime = DateTime.Now;

                    //以扣抵金額扣除
                    var receivableGoods = new Receivable();

                    receivableGoods.ClassificationGuid = order.Guid;
                    receivableGoods.AccountsClassification = (int)AccountsClassification.HiDealOrder;
                    receivableGoods.TotalAmount = info.TotalAmount;
                    receivableGoods.Pcash = info.PCashAmt;
                    receivableGoods.Scash = info.SCashAmt + cashAmt;
                    receivableGoods.Bcash = info.BCashAmt;
                    receivableGoods.CreditCardAmt = 0;
                    receivableGoods.DiscountAmount = info.DiscountAmt;
                    receivableGoods.AtmAmount = 0;
                    receivableGoods.ReceivableStatus = (int)ReceivableStatus.NotReceived;
                    receivableGoods.InvoiceId = null;
                    receivableGoods.CreateId = info.UserName;
                    receivableGoods.CreateTime = DateTime.Now;
                    receivables.Add(receivableGoods);

                    //DB儲存
                    ap.ReceivableSetList(receivables);
                    ap.PayableSet(payableSCash);

                    #endregion 建立應收帳款

                    #region 建立PaymentTransaction資料

                    if (info.CreditCardAmt > 0)
                    {
                        PaymentFacade.NewTransaction(orderId, order.Guid, Core.PaymentType.Creditcard,
                                                     (int)info.CreditCardAmt, null, PayTransType.Authorization,
                                                     DateTime.Now, info.UserName, null,
                                                     Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.HiDeal),
                                                     OrderClassification.HiDeal, receivablePoint.Id
                            );
                    }

                    if (info.BCashAmt > 0)
                    {
                        int status =
                            Helper.SetPaymentTransactionPhase(
                                Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.HiDeal),
                                PayTransPhase.Requested);
                        PaymentFacade.NewTransaction(orderId, order.Guid, Core.PaymentType.BonusPoint,
                                                     (int)info.BCashAmt,
                                                     null, PayTransType.Authorization, DateTime.Now, info.UserName,
                                                     "以紅利折抵",
                                                     status, PayTransResponseType.OK, OrderClassification.HiDeal,
                                                     receivableGoods.Id);
                    }

                    if ((info.SCashAmt + cashAmt) > 0)
                    {
                        int status =
                            Helper.SetPaymentTransactionPhase(
                                Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.HiDeal),
                                PayTransPhase.Requested);
                        PaymentFacade.NewTransaction(orderId, order.Guid, Core.PaymentType.SCash,
                                                     info.SCashAmt + cashAmt, null,
                                                     PayTransType.Authorization,
                                                     DateTime.Now, info.UserName, "17購物金折抵", status,
                                                     PayTransResponseType.OK, OrderClassification.HiDeal, receivableGoods.Id);
                    }

                    if (info.DiscountAmt > 0)
                    {
                        int status =
                            Helper.SetPaymentTransactionPhase(
                                Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.HiDeal),
                                PayTransPhase.Requested);
                        PaymentFacade.NewTransaction(orderId, order.Guid, Core.PaymentType.DiscountCode,
                                                     info.DiscountAmt,
                                                     null, PayTransType.Authorization, DateTime.Now, info.UserName,
                                                     "DiscountCode折抵", status, PayTransResponseType.OK,
                                                     OrderClassification.HiDeal, receivableGoods.Id);
                    }

                    if (info.PCashAmt > 0)
                    {
                        int status =
                            Helper.SetPaymentTransactionPhase(
                                Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.HiDeal),
                                PayTransPhase.Requested);
                        PaymentFacade.NewTransaction(orderId, order.Guid, Core.PaymentType.PCash, info.PCashAmt, null,
                                                     PayTransType.Authorization, DateTime.Now, info.UserName,
                                                     string.Empty, status, PayTransResponseType.OK,
                                                     OrderClassification.HiDeal, receivableGoods.Id);
                    }

                    #endregion 建立PaymentTransaction資料

                    if (selOptions.Count > 0)
                    {
                        hp.HiDealProductOptionItemUpdateSellQuantity(selOptions, info.Count);
                    }

                    #region 分派coupon給訂單

                    //分派coupon給訂單，若傳回false則
                    bool check_setcoupon = HiDealCouponManager.SetCouponData(order, orderDetail, info.UserName);

                    if (!check_setcoupon)
                    {
                        rtn = MarkHiDealOrderReply.SoldOut;
                        message = "庫存不足或商品已售完。";
                        throw new Exception(string.Format("商品數量不足。pid:{0} , info.Count:{1}, ", info.Pid, info.Count));
                    }

                    #endregion 分派coupon給訂單

                    transScope.Complete();
                    orderGuid = order.Guid;
                    rtn = MarkHiDealOrderReply.Success;
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message, ex);
                }
            }
            return rtn;
        }

        public static bool CancelHiDealOrder(Guid orderGuid)
        {
            //查詢訂單。
            var order = hp.HiDealOrderGet(orderGuid);
            //訂單不存在或狀態為 付款完成、取消單 則表示資料錯誤，停止作業
            if (!order.IsLoaded)
            {
                return false;
            }

            if ((order.OrderStatus == (int)HiDealOrderStatus.Cancel) || (order.OrderStatus == (int)HiDealOrderStatus.Completed))
            {
                return false;
            }
            //修改訂單狀態為作廢
            DateTime workTime = DateTime.Now;
            order.OrderStatus = (int)HiDealOrderStatus.Cancel;
            order.CancelTime = workTime;
            order.CancelReason = Phrase.HiDealOrderPayFailCancelReason;
            order.CancelUserName = config.SystemEmail;
            order.ModifyId = config.SystemEmail;
            order.ModifyTime = workTime;
            hp.HiDealOrderSet(order);

            #region 清除orderDetail的商品選項資料 與 已分配的coupon紀錄。

            //查詢訂單明細
            var orderDetails = hp.HiDealOrderDetailGetListByOrderGuid(order.Guid);
            foreach (var orderDetail in orderDetails)
            {
                var optionIds = new List<int>();
                if (orderDetail.Option1 != null)
                {
                    optionIds.Add(orderDetail.Option1.Value);
                }

                if (orderDetail.Option2 != null)
                {
                    optionIds.Add(orderDetail.Option2.Value);
                }

                if (orderDetail.Option3 != null)
                {
                    optionIds.Add(orderDetail.Option3.Value);
                }

                if (orderDetail.Option4 != null)
                {
                    optionIds.Add(orderDetail.Option4.Value);
                }

                if (orderDetail.Option5 != null)
                {
                    optionIds.Add(orderDetail.Option5.Value);
                }

                if (optionIds.Count > 0)
                {
                    hp.HiDealProductOptionItemUpdateSellQuantity(optionIds, (-orderDetail.ItemQuantity));
                }

                HiDealCouponManager.ReleaseAssignedCoupon(orderDetail.ProductId, order.Pk);
            }

            #endregion 清除orderDetail的商品選項資料 與 已分配的coupon紀錄。

            //清除hiDealCouponOffer資料，讓下一個會員能夠購買同個編號的憑證
            hp.HiDealCouponOfferDeleteByOrderPk(order.Pk);
            return true;
        }

        /// <summary>
        /// 取消傳入時間之前的所有已確定訂單 OrderStatus = Confirm
        /// </summary>
        /// <param name="befDatetime"></param>
        /// <returns></returns>
        public static void CancelConfirmHiDealOrderListBeforeDatetime(DateTime befDatetime)
        {
            try
            {
                //檢查建立時間在15分鐘之前的訂單。
                var orders = hp.HiDealOrderGetListAtConfirm(befDatetime);
                foreach (var order in orders)
                {
                    CancelHiDealOrder(order.Guid);
                }
            }
            catch (Exception ex)
            {
                log.Error("HiDealOrderInvalid running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
                throw;
            }
        }

        public enum MarkHiDealOrderReply
        {
            Default,
            Success,
            InputDataError,
            MakeOrderFail,
            MakeOrderDetailFail,
            SoldOut,
        }
    }
}
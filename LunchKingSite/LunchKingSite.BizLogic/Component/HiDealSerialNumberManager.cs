﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    public class HiDealSerialNumberManager
    {
        private static DateTimeBaseSerialNumber _hiDealOrderId;
        private const string HiDealOrderIdTimeFormat = "yyyyMMddHHmmss";


        static HiDealSerialNumberManager()
        {
            _hiDealOrderId = new DateTimeBaseSerialNumber(DateTime.Now.ToString(HiDealOrderIdTimeFormat));
        }
        /// <summary>
        /// 取得新的訂單編號
        /// </summary>
        /// <returns></returns>
        public static string GetNewHiDealOrderId()
        {
            lock (_hiDealOrderId)
            {
                if(DateTime.Now.ToString(HiDealOrderIdTimeFormat) == _hiDealOrderId.NumberTimeStr)
                {
                    
                    var rtn = _hiDealOrderId.NumberTimeStr + _hiDealOrderId.Number.ToString().PadLeft(3, '0');
                    _hiDealOrderId.Number++;
                    return "H"+rtn;
                }
                else
                {
                    _hiDealOrderId = new DateTimeBaseSerialNumber(DateTime.Now.ToString(HiDealOrderIdTimeFormat));
                    return "H"+_hiDealOrderId.NumberTimeStr + "001";
                }
            }
        }
    }

    public class DateTimeBaseSerialNumber
    {
        public string NumberTimeStr { get; set; }
        public int Number { get; set; }
        public DateTimeBaseSerialNumber(string timeStr)
        {
            NumberTimeStr = timeStr;
            Number = 1;
        }
    }
}

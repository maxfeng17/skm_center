﻿using System;
using LunchKingSite.BizLogic.Model.Verification;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Component.Verification
{
    public class CouponUsageDateSelector
    {
        public CouponUsageExpiration ExpirationDates { get; set; }

        /// <summary>
        /// 取得憑證是否仍在可用的時間裡
        /// </summary>
        /// <param name="enumOriStatus">原始的狀態，可不給值<br />預設值 VerificationStatus.CouponInit</param>
        /// <returns>回傳狀態有三<br />
        /// 1.VerificationStatus.CouponInit 或是 原始傳狀態: 合法的期間內<br />
        /// 2.VerificationStatus.DuInTime: 於可用期間內，開始前一天、結束後10天<br />
        /// 3.VerificationStatus.CouponOver: 不在兌換時間裡<br />
        /// </returns>
        public VerificationStatus ChkCouponUsageStatus(VerificationStatus enumOriStatus = VerificationStatus.CouponInit)
        {
            if (ExpirationDates == null)
            {
                throw new InvalidOperationException("沒有給定期間物件");
            }
            DateTime today = DateTime.Today;
            DateTime usageStartDate = ExpirationDates.DealUseStartDate.Value;
            DateTime expirationDate = (ExpirationDates.DealChangedExpireDate == null
                                           ? ExpirationDates.DealOriginalExpireDate
                                           : ExpirationDates.DealChangedExpireDate).Value;
            
            

            //較彈性的核銷時間
            DateTime usageStartDate0 = usageStartDate;
            DateTime expirationDate2 = expirationDate.AddDays(ProviderFactory.Instance().GetConfig().VerificationBuffer);

            if (today >= usageStartDate && today <= expirationDate)
            {
                //正常時間內
                return enumOriStatus;
            }
            else if (today >= usageStartDate0 && today <= expirationDate2)
            {
                return VerificationStatus.DuInTime;
            }
            else
            {
                return VerificationStatus.CouponOver;
            }            
        }
    }
}

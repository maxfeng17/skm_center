﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// 實驗中的分類相關程式碼，先別使用. 
    /// </summary>
    public class HxRootCategory
    {
        private const int _LAST_DAT_CATEGORY_ID = 318; //檔次屬於"最後一天"的Cid
        public const int _PIINLIFE_CATEGORY_ID = 148; //檔次屬於"最後一天"的Cid
        private DateTime now = DateTime.Now;
        private List<int> specialIds;
        private Dictionary<Guid, PponDealSynopsis> allAppDeals;

        public List<HxCategory> Tops { get; set; }        

        private Dictionary<int, HxCategory> All { get; set; }

        public HxCategory Get(int categoryId)
        {
            HxCategory result;
            All.TryGetValue(categoryId, out result);            
            return result;
        }

        public static HxRootCategory Create()
        {
            HxRootCategory root = new HxRootCategory();
            root.Tops = new List<HxCategory>();
            root.All = new Dictionary<int, HxCategory>();
            root.specialIds = new List<int>();
            root.allAppDeals = new Dictionary<Guid, PponDealSynopsis>();

            Dictionary<int, Category> categories = GetAllCategories()
                .Where(t => t.Status == 1)
                //.Where(t => t.Type == 4 || t.Type == 5 || t.Type == 6  || t.Type == 8)
                .ToDictionary(t => t.Id, t => t);
            foreach (var c in categories)
            {
                if (c.Value.Type == 4 && c.Value.IsShowFrontEnd)// 4 is CategoryType.PponChannel
                {
                    var hc = new HxCategory(c.Value, 0);
                    root.Tops.Add(hc);
                    root.All.Add(hc.Id, hc);
                }
                if (c.Value.Type == 8 && c.Value.IsShowFrontEnd)
                {
                    if (root.specialIds.Contains(c.Value.Id) == false)
                    {
                        root.specialIds.Add(c.Value.Id);
                    }
                }
            }

            var categorieDependencies = GetAllCategorieDependencies().OrderBy(t => t.ParentId).ThenBy(t => t.Seq).ToList();

            foreach (var cd in categorieDependencies)
            {
                if (categories.ContainsKey(cd.CategoryId) == false || categories.ContainsKey(cd.ParentId) == false)
                {
                    continue;
                }
                if (categories[cd.ParentId].IsShowFrontEnd == false || categories[cd.CategoryId].IsShowFrontEnd == false)
                {
                    continue;
                }
                HxCategory hp;
                if (root.All.ContainsKey(cd.ParentId))
                {
                    hp = root.All[cd.ParentId];
                }
                else
                {
                    hp = new HxCategory(categories[cd.ParentId], 0);
                    root.All.Add(hp.Id, hp);
                }
                
                HxCategory hc;
                if (root.All.ContainsKey(cd.CategoryId))
                {
                    hc = root.All[cd.CategoryId];
                    if (hc.ChannelId == 0 && hp.ChannelId != 0)
                    {                        
                        hc.ChannelId = hp.ChannelId;
                    }
                }
                else
                {
                    hc = new HxCategory(categories[cd.CategoryId], hp.ChannelId);
                    root.All.Add(hc.Id, hc);
                }
                hp.Categories.Add(hc.Id, hc);
                if (hc.Type == 5)
                {
                    if (hp.AreaCategories == null)
                    {
                        hp.AreaCategories = new List<HxCategory>();
                    }
                    hp.AreaCategories.Add(hc);
                }
                else if (hc.Type == 6)
                {
                    if (hp.DealCategories == null)
                    {
                        hp.DealCategories = new List<HxCategory>();
                    }
                    hp.DealCategories.Add(hc);
                }        
            }




            var deals = ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true);
            foreach(var deal in deals)
            {
                root.PrepareDeal(deal);
            }
            root.PrepareLasyDayCategory(deals);
            root.PrepareSpecialNodes();

            return root;
        }

        private void PrepareDeal(IViewPponDeal deal)
        {
            foreach (int cid in deal.CategoryIds)
            {
                PrepareDeal(deal, cid);
            }
        }

        private void PrepareDeal(IViewPponDeal deal, int cid)
        {
            var baseCostGrossMargin = ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(deal.BusinessHourGuid);
            HxCategory hc;
            if (this.All.ContainsKey(cid) == false)
            {
                return;
            }
            hc = this.All[cid];
            int cityId = hc.ChannelId;
            var dts = ViewPponDealManager.DefaultManager.DealTimeSlotGetByBidCityAndTimeNotFoundReturnNull(
                deal.BusinessHourGuid, cityId, this.now);
            MultipleMainDealPreview preview = null;
            if (dts == null)
            {
                preview = new MultipleMainDealPreview(cityId, 999999, deal, deal.CategoryIds,
                        DealTimeSlotStatus.Default, baseCostGrossMargin.BaseGrossMargin);

            }
            else
            {
                preview = new MultipleMainDealPreview(cityId, dts.Sequence, deal, deal.CategoryIds,
                        (DealTimeSlotStatus)dts.Status, baseCostGrossMargin.BaseGrossMargin);
            }
            VbsDealType dealType = preview.DealCategoryIdList.Contains(_PIINLIFE_CATEGORY_ID)
            ? VbsDealType.PiinLife
            : VbsDealType.Ppon;
            PponDealSynopsis appDeal;
            if (this.allAppDeals.ContainsKey(preview.DealId))
            {
                appDeal = this.allAppDeals[preview.DealId];
            }
            else
            {
                appDeal = PponDealApiManager.PponDealSynopsisGetByDeal(
                    preview.PponDeal, preview.DealCategoryIdList, preview.Sequence,
                    dealType, true, null);
                this.allAppDeals.Add(preview.DealId, appDeal);
            }
            preview.AppPponDeal = appDeal;
            hc.Deals.Add(preview);
        }

        private void PrepareLasyDayCategory(IList<IViewPponDeal> deals)
        {
            foreach (var deal in deals)
            {
                if (deal.BusinessHourOrderTimeE.AddDays(-1) <= this.now && this.now < deal.BusinessHourOrderTimeE)
                {
                    PrepareDeal(deal, _LAST_DAT_CATEGORY_ID);
                }
            }
        }

        private void PrepareSpecialNodes()
        {
            foreach (int specId in specialIds)
            {
                foreach (var top in this.Tops)
                {
                    var deals = GetDeals(top.Id, 0, new List<int>(new[] { specId }));
                    if (deals.Count == 0)
                    {
                        continue;
                    }
                    top.SpecialNodes.Add(new HxNode()
                    {
                        Title = specId.ToString(),
                        Key = HxNode.CreateKey(top.Id, specId),
                        Deals = deals
                    });
                    if (top.AreaCategories != null)
                    {
                        foreach (var area in top.AreaCategories)
                        {
                            var areaDeals = GetDeals(top.Id, area.Id, new List<int>(new[] { specId }));
                            area.SpecialNodes.Add(new HxNode()
                            {
                                Title = specId.ToString(),
                                Key = HxNode.CreateKey(top.Id, area.Id, specId),
                                Deals = areaDeals
                            });
                        }
                    }
                }
            }
        }

        private static List<CategoryDependency> GetAllCategorieDependencies()
        {
            IPponProvider pp = ProviderFactory.Instance().GetDefaultProvider<IPponProvider>();
            return new List<CategoryDependency>(pp.GetAllCategoryDependencies());
        }

        private static IList<Category> GetAllCategories()
        {
            return ViewPponDealManager.DefaultManager.GetAllCategories();
        }

        public HxRootCategory()
        {
            Tops = new List<HxCategory>();
        }

        public List<MultipleMainDealPreview> GetDeals(int channelId, int areaId, 
            List<int> filterCids,
            params int[] categoryList)
        {
            List<int> cids = new List<int>();
            foreach(int cid in categoryList)
            {
                if (cid == 0)
                {
                    continue;
                }
                cids.Add(cid);
            }            
            if (areaId != 0 && cids.Contains(areaId) == false)
            {
                cids.Add(areaId);
            }
            if (channelId != 0 && cids.Contains(channelId) == false)
            {
                cids.Add(channelId);
            }

            if (cids.Count == 0)
            {
                return new List<MultipleMainDealPreview>();
            }

            HashSet<MultipleMainDealPreview> result = new HashSet<MultipleMainDealPreview>();
            for (int i = 0; i < cids.Count; i++)
            {
                if (result.Count == 0)
                {
                    if (this.All.ContainsKey(cids[i]))
                    {
                        result.UnionWith(this.All[cids[i]].Deals);
                        continue;
                    }
                }
                if (this.All.ContainsKey(cids[i]))
                {
                    var categoryDeals = this.All[cids[i]].Deals;
                    result.IntersectWith(categoryDeals);
                }
            }


            if (filterCids != null && filterCids.Count > 0)
            {
                if (filterCids.Any(t => this.All.ContainsKey(t) == false))
                {
                    return new List<MultipleMainDealPreview>();
                }
                var filterResult = new HashSet<MultipleMainDealPreview>();
                //do filter 做 or 連集
                for (int i = 0; i < filterCids.Count; i++)
                {
                    var filterDeals = this.All[filterCids[i]].Deals.ToList();
                    filterResult.UnionWith(filterDeals);
                }
                result.IntersectWith(filterResult);
            }

            return result.Where(t=>t.TimeSlotStatus == DealTimeSlotStatus.Default).ToList();
            
        }

        public List<HxNode> GetSpecialNodes(int channelId, int areaId)
        {
            var channel = this.Tops.FirstOrDefault(t => t.Id == channelId);
            if (channel != null)
            {
                if (areaId == 0)
                {
                    return channel.SpecialNodes.Where(t => t.Deals.Count > 0).ToList();
                }
                var area  = channel.AreaCategories.FirstOrDefault(t => t.Id == areaId);
                return area.SpecialNodes.Where(t => t.Deals.Count > 0).ToList();
            }
            return new List<HxNode>();
        }
    }

    public class HxNode
    {
        public string Key { get; set; }
        public string Title { get; set; }
        public List<MultipleMainDealPreview> Deals { get; set; }
        public HxNode()
        {
            this.Key = string.Empty;
            this.Title = string.Empty;
            Deals = new List<MultipleMainDealPreview>();
        }

        public static string CreateKey(params int[] ids)
        {
            return string.Join(",", ids.OrderBy(t => t).ToArray());
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", this.Title, this.Deals.Count);
        }
    }

    public class HxCategory
    {
        static Dictionary<int, int> topCategoryChannelDict = new Dictionary<int, int>();
        static HxCategory()
        {
            
            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CategoryId, PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.Taoyuan.CategoryId, PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId);
            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.Hsinchu.CategoryId, PponCityGroup.DefaultPponCityGroup.Hsinchu.CityId);
            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.Taichung.CategoryId, PponCityGroup.DefaultPponCityGroup.Taichung.CityId);
            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.Tainan.CategoryId, PponCityGroup.DefaultPponCityGroup.Tainan.CityId);
            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.Kaohsiung.CategoryId, PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId);

            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CategoryId, PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId);
            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.AllCountry.CategoryId, PponCityGroup.DefaultPponCityGroup.AllCountry.CityId);
            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.Travel.CategoryId, PponCityGroup.DefaultPponCityGroup.Travel.CityId);

            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.Piinlife.CategoryId, PponCityGroup.DefaultPponCityGroup.Piinlife.CityId);
            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.Skm.CategoryId, PponCityGroup.DefaultPponCityGroup.Skm.CityId);
            topCategoryChannelDict.Add(PponCityGroup.DefaultPponCityGroup.DepositCoffee.CategoryId, PponCityGroup.DefaultPponCityGroup.DepositCoffee.CityId);
        }

        public int Id { get; set; }
        public int Type { get; set; }
        public int IconType { get; private set; }
        public string Name { get; set; }
        [JsonIgnore]
        public Dictionary<int, HxCategory> Categories { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<HxCategory> DealCategories { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<HxCategory> AreaCategories { get; set; }
        [JsonIgnore]
        public List<HxNode> SpecialNodes { get; set; }

        [JsonIgnore]
        public List<MultipleMainDealPreview> Deals { get; set; }
        
        [JsonIgnore]
        public int ChannelId { get; set; }

        private static int? GetChannelId(int id)
        {
            if (topCategoryChannelDict.ContainsKey(id))
            {
                return topCategoryChannelDict[id];
            }
            return null;
        }

        public HxCategory(Category category, int channelId) 
        {
            this.Id = category.Id;
            this.Name = category.Name;
            this.Type = category.Type.GetValueOrDefault();
            this.IconType = category.IconType;
            if (channelId == 0)
            {
                this.ChannelId = GetChannelId(category.Id) ?? 0;
            }
            else
            {
                this.ChannelId = channelId;
            }

            Categories = new Dictionary<int, HxCategory>();
            //DealCategories = new List<HxCategory>();
            //AreaCategories = new List<HxCategory>();
            Deals = new List<MultipleMainDealPreview>();
            SpecialNodes = new List<HxNode>();
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}-{3}", this.Id, this.Name, this.Type, this.Deals.Count);
        }

        public override bool Equals(object obj)
        {
            HxCategory castObj = obj as HxCategory;
            if (castObj == null)
            {
                return false;
            }

            return GetHashCode() == castObj.GetHashCode();
        }

        public override int GetHashCode()
        {
            if (Id == 0)
            {
                return 0;
            }

            return Id.GetHashCode();
        }


        public class MultipleMainDealPreviewComparer : IComparer<MultipleMainDealPreview>
        {
            public int Compare(MultipleMainDealPreview x, MultipleMainDealPreview y)
            {
                return x.Sequence.CompareTo(y.Sequence);
            }
        }
    }
}
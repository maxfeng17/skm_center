﻿using System.Text.RegularExpressions;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Model;
using System;
using City = LunchKingSite.DataOrm.City;

namespace LunchKingSite.BizLogic.Component
{
    public class LocationUtility
    {
        protected static IMemberProvider mp;
        protected static ILocationProvider lp;

        /// <summary>
        /// this function will be obsolete
        /// </summary>
        /// <param name="app"></param>
        public static void PrepareCityCache(HttpApplicationState app)
        {
            app.Lock();
            if (app["TopCity"] == null)
            {
                app.Add("TopCity", ProviderFactory.Instance().GetProvider<ILocationProvider>().CityGetListFromAvailBuilding(
                        AppendMode.All, true));
            }
            app.UnLock();
        }

        public static void PrepareCityCache()
        {
            HttpApplicationState app = HttpContext.Current.Application;
            PrepareCityCache(app);
        }

        public static City GetCityFromCache(int id)
        {
            PrepareCityCache();
            int i = ((CityCollection)HttpContext.Current.Application["TopCity"]).Find("Id", id);
            return i >= 0 ? ((CityCollection) HttpContext.Current.Application["TopCity"])[i] : null;
        }

        public static City GetParentCityFromCache(int id)
        {
            City c = GetCityFromCache(id);
            return c != null && c.ParentId != null ? GetCityFromCache(c.ParentId.Value) : null;
        }

        public static string GetCityNameFromCache(int id)
        {
            City c = GetCityFromCache(id);
            return c != null ? c.CityName : string.Empty;
        }

        public static string GetParentCityNameFromCache(int id)
        {
            City c = GetParentCityFromCache(id);
            return c != null ? c.CityName : string.Empty;
        }

        /// <summary>
        /// Gets the splinters of any input address.
        /// </summary>
        /// <param name="address">the address string</param>
        /// <returns></returns>
        public static TaiwanAddress GetSplitAddressFromAddress(string address)
        {
            TaiwanAddress theSplinters = new TaiwanAddress(address);

            return theSplinters;
        }

        /// <summary>
        /// Gets the splinters of the address within one member.
        /// </summary>
        /// <param name="member">the input member</param>
        /// <returns></returns>
        public static TaiwanAddress GetSplitAddressFromMember(Member member)
        {
            return GetSplitAddressFromAddress(member.CompanyAddress);
        }

        /// <summary>
        /// Gets the full address from one member.
        /// </summary>
        /// <param name="member">the input member</param>
        /// <returns></returns>
        public static String GetFullAddressFromMember(Member member)
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            ViewMemberBuildingCity vmbc = mp.ViewMemberBuildingCityGet(member.UserName);
            string fullAddress = string.Empty;
            string theCity = vmbc.CityStatus == (int)CityStatus.Virtual ? string.Empty
                           : vmbc.CityName;
            
            fullAddress = member.CompanyAddress;
            
            if (string.IsNullOrEmpty(GetSplitAddressFromMember(member).City) || GetSplitAddressFromMember(member).City != theCity)
            {
                if (string.IsNullOrEmpty(theCity))
                {
                    fullAddress = lp.BuildingGet(member.BuildingGuid.Value).BuildingStreetName;
                }
                else
                {
                    fullAddress = theCity + member.CompanyAddress;
                }
            }

            return fullAddress;
        }

        /// <summary>
        /// Gets the member with the full address from one member.
        /// </summary>
        /// <param name="member">the input member</param>
        /// <returns></returns>
        public static Member GetMemberWithFullAddressFromMember(Member member)
        {
            Member theMember = member;

            theMember.CompanyAddress = GetFullAddressFromMember(member);
            return theMember;
        }

        /// <summary>
        /// 檢查地址字串是否包含了路段
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static bool CheckAddressBuilding(string address)
        {
            TaiwanAddress twAddress = LocationUtility.GetSplitAddressFromAddress(address);
            string rem = address.Remove(address.IndexOf(twAddress.Town, System.StringComparison.Ordinal) + twAddress.Town.Length);
            string building = string.IsNullOrEmpty(rem) ? address.Trim() : address.Replace(rem, "").Trim();
            if (building.Length > 0)
            {
                string pattern = @"(^\d)"; // 規則字串
                Regex regex = new Regex(pattern);
                Match match = regex.Match(building);
                if (match.Success)
                {
                    return false;
                }
            }
            return true;
        }
    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class PponCityGroup : IEnumerable<PponCity>
    {
        #region 屬性

        private readonly List<PponCity> _pponCities;
        private PponCity _taipeiCity;

        /// <summary>
        /// 台北市
        /// </summary>
        public PponCity TaipeiCity
        {
            get
            {
                return _taipeiCity;
            }
        }

        private PponCity _newTaipeiCity;

        /// <summary>
        /// 新北市
        /// </summary>
        public PponCity NewTaipeiCity
        {
            get
            {
                return _newTaipeiCity;
            }
        }

        private PponCity _taoyuan;

        /// <summary>
        /// 桃園
        /// </summary>
        public PponCity Taoyuan
        {
            get
            {
                return _taoyuan;
            }
        }

        private PponCity _hsinchu;

        /// <summary>
        /// 新竹
        /// </summary>
        public PponCity Hsinchu
        {
            get
            {
                return _hsinchu;
            }
        }

        private PponCity _taichung;

        /// <summary>
        /// 台中
        /// </summary>
        public PponCity Taichung
        {
            get
            {
                return _taichung;
            }
        }

        private PponCity _tainan;

        /// <summary>
        /// 台南
        /// </summary>
        public PponCity Tainan
        {
            get
            {
                return _tainan;
            }
        }

        private PponCity _kaohsiung;

        /// <summary>
        /// 高雄
        /// </summary>
        public PponCity Kaohsiung
        {
            get
            {
                return _kaohsiung;
            }
        }

        private PponCity _PEZ;

        public PponCity PEZ
        {
            get
            {
                return _PEZ;
            }
        }

        private PponCity _travel;
        
        /// <summary>
        /// 旅遊
        /// </summary>
        public PponCity Travel
        {
            get
            {
                return _travel;
            }
        }

        private PponCity _allcountry;

        /// <summary>
        /// 宅配
        /// </summary>
        public PponCity AllCountry
        {
            get
            {
                return _allcountry;
            }
        }

        /// <summary>
        /// 完美
        /// </summary>
        public PponCity PBeautyLocation { get; set; }
        
        private PponCity _skm;
        
        /// <summary>
        /// 新光三越
        /// </summary>
        public PponCity Skm
        {
            get
            {
                return _skm;
            }
        }

        private PponCity _depositCoffee;

        /// <summary>
        /// 咖啡寄杯
        /// </summary>
        public PponCity DepositCoffee
        {
            get
            {
                return _depositCoffee;
            }
        }

        private PponCity _family;

        /// <summary>
        /// 全家
        /// </summary>
        public PponCity Family
        {
            get
            {
                return _family;
            }
        }

        private PponCity _ArriveIn24Hrs { get; set; }

        /// <summary>
        /// 24H
        /// </summary>
        public PponCity ArriveIn24Hrs
        {
            get
            {
                return _ArriveIn24Hrs;
            }
        }

        private PponCity _ArriveIn72Hrs { get; set; }

        /// <summary>
        /// 72H
        /// </summary>
        public PponCity ArriveIn72Hrs
        {
            get
            {
                return _ArriveIn72Hrs;
            }
        }

        private PponCity _tmall { get; set; }

        /// <summary>
        /// 天貓
        /// </summary>
        public PponCity Tmall
        {
            get
            {
                return _tmall;
            }
        }

        private PponCity _piinlife { get; set; }

        /// <summary>
        /// 品生活
        /// </summary>
        public PponCity Piinlife
        {
            get
            {
                return _piinlife;
            }
        }

        private PponCity _pponSelect { get; set; }
        /// <summary>
        /// 精選好康 (edm專用)
        /// </summary>
        public PponCity PponSelect
        {
            get
            {
                return _pponSelect;
            }
        }

        #endregion 屬性

        private PponCityGroup()
        {
            ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            CityCollection cities = lp.CityGetList(City.Columns.Code + " in TP,TA,TY,XZ,TC,TN,GX,TRA,PBU,ALL,com,SKM,COF,FAM,A24,A72,TML,PIN,SLT", City.Columns.ParentId + " is null");

            foreach (City city in cities)
            {
                switch (city.Code)
                {
                    case "TP":
                        _taipeiCity = new PponCity(city.Id, I18N.Phrase.PponCityTaipeiCity, city.Code, city.Rank ?? 0 , 94 , 87 , 94, true);
                        break;

                    case "TA":
                        _newTaipeiCity = new PponCity(city.Id, "台北2", city.Code, city.Rank ?? 0 , -1);
                        break;

                    case "TY":
                        _taoyuan = new PponCity(city.Id, I18N.Phrase.PponCityTaoyuan, city.Code, city.Rank ?? 0, 95, 87, 95, true);
                        break;

                    case "XZ":
                        _hsinchu = new PponCity(city.Id, I18N.Phrase.PponCityHsinchu, city.Code, city.Rank ?? 0, 96, 87, 96, true);
                        break;

                    case "TC":
                        _taichung = new PponCity(city.Id, I18N.Phrase.PponCityTaichung, city.Code, city.Rank ?? 0, 97, 87, 97, true);
                        break;

                    case "TN":
                        _tainan = new PponCity(city.Id, I18N.Phrase.PponCityTainan, city.Code, city.Rank ?? 0, 98, 87, 98, true);
                        break;

                    case "GX":
                        _kaohsiung = new PponCity(city.Id, I18N.Phrase.PponCityKaohsiung, city.Code, city.Rank ?? 0, 99, 87, 99, true);
                        break;

                    case "TRA":
                        _travel = new PponCity(city.Id, I18N.Phrase.PponCityTravel, city.Code, city.Rank ?? 0 , 90 , 90 , null);
                        break;

                    case "PBU":
                        PBeautyLocation = new PponCity(city.Id, I18N.Phrase.PponCityPeauty, city.Code, city.Rank ?? 0 , 89 , 89 , null);
                        break;

                    case "ALL":
                        _allcountry = new PponCity(city.Id, I18N.Phrase.PponCityAllCountry, city.Code, city.Rank ?? 0 , 88 , 88 , null);
                        break;

                    case "com":
                        _PEZ = new PponCity(city.Id, I18N.Phrase.PponCityPEZ, city.Code, city.Rank ?? 0 , 92 , 92 , null );
                        break;

                    case "SKM":
                        _skm = new PponCity(city.Id, I18N.Phrase.PponCitySkm, city.Code, city.Rank ?? 0 , 185 , 185 , null); //依執行環境不同
                        break;
                    case "COF":
                        _depositCoffee = new PponCity(city.Id, I18N.Phrase.PponCityDepositCoffee, city.Code, city.Rank ?? 0, 186, 186, null);
                        break;
                    case "FAM":
                        _family = new PponCity(city.Id, I18N.Phrase.PponCityFamily, city.Code, city.Rank ?? 0 , 91 , 91 , null);
                        break;
                    case "A24":
                        _ArriveIn24Hrs = new PponCity(city.Id, I18N.Phrase.ArriveIn24Hrs, city.Code, city.Rank ?? 0 , 138);
                        break;
                    case "A72":
                        _ArriveIn72Hrs = new PponCity(city.Id, I18N.Phrase.ArriveIn72Hrs, city.Code, city.Rank ?? 0 , 139);
                        break;
                    case "TML":
                        _tmall = new PponCity(city.Id, I18N.Phrase.Tmall, city.Code, city.Rank ?? 0 , 93 , 93 , null );
                        break;
                    case "PIN":
                        _piinlife = new PponCity(city.Id, city.CityName, city.Code, city.Rank ?? 0, 148, 148, null);
                        break;
                    case "SLT":
                        _pponSelect = new PponCity(city.Id, city.CityName, city.Code, city.Rank ?? 0, 184);
                        break;
                    default:    // error
                        break;
                }
            }

            // have to be in this particular order
            _pponCities = new List<PponCity>();
            _pponCities.AddIfExists(_PEZ); //Here they want it to be the first place
            _pponCities.AddIfExists(_allcountry);
            _pponCities.AddIfExists(_taipeiCity);
            _pponCities.AddIfExists(_newTaipeiCity);
            _pponCities.AddIfExists(_taoyuan);
            _pponCities.AddIfExists(_hsinchu);
            _pponCities.AddIfExists(_taichung);
            _pponCities.AddIfExists(_tainan);
            _pponCities.AddIfExists(_kaohsiung);
            _pponCities.AddIfExists(_travel);
            _pponCities.AddIfExists(PBeautyLocation);
            _pponCities.AddIfExists(_skm);
            _pponCities.AddIfExists(_depositCoffee);
            _pponCities.AddIfExists(_family);
            _pponCities.AddIfExists(_ArriveIn24Hrs);
            _pponCities.AddIfExists(_ArriveIn72Hrs);
            _pponCities.AddIfExists(_tmall);
            _pponCities.AddIfExists(_piinlife);
            _pponCities.AddIfExists(_pponSelect);
        }

        #region method

        IEnumerator<PponCity> IEnumerable<PponCity>.GetEnumerator()
        {
            return _pponCities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _pponCities.GetEnumerator();
        }

        public PponCity GetPponCityByCityId(int cityId)
        {
            PponCity rtnCity = _pponCities.FirstOrDefault(x => x.CityId == cityId);

            return rtnCity;
        }

        public List<PponCity> GetPponcityForSubscription()
        {
            List<string> cityUnwant = new List<string> { "com", "TA", Skm.CityCode, Family.CityCode, ArriveIn24Hrs.CityCode, ArriveIn72Hrs.CityCode, Tmall.CityCode }; // 暫不開放全家專區訂閱
            var rtns = PponCityGroup.DefaultPponCityGroup.Where(x => !cityUnwant.Any(y => y.Equals(x.CityCode, StringComparison.OrdinalIgnoreCase))).OrderBy(x => x.CityRank).ToList();
            return rtns;
        }

        /// <summary>
        /// 顯示開放的區域
        /// </summary>
        /// <returns></returns>
        public List<PponCity> GetPponcityForPublic()
        {
            List<string> cityUnwant = new List<string> { "com", "TA", ArriveIn24Hrs.CityCode, ArriveIn72Hrs.CityCode, Tmall.CityCode, Piinlife.CityCode, PponSelect.CityCode };
            var rtns = _pponCities.Where(x => !cityUnwant.Any(y => y.Equals(x.CityCode, StringComparison.OrdinalIgnoreCase))).ToList();

            return rtns;
        }

        public List<PponCity> GetPponCityForSetting()
        {
            string[] cityUnwant = { "TA" };
            var rtns = _pponCities.Where(x => !cityUnwant.Any(y => y.Equals(x.CityCode, StringComparison.OrdinalIgnoreCase))).ToList();
            return rtns;
        }

        /// <summary>
        /// 取得顯示於P好康標籤的活動區域
        /// </summary>
        /// <param name="cityLsit">檔次活動區域</param>
        /// <param name="locationCityId">目前城市</param>
        /// <returns>PponCity</returns>
        public PponCity GetPponCityForPponTag(List<string> cityList, int locationCityId)
        {
            string[] exceptionCityId = { Travel.CityId.ToString(), PBeautyLocation.CityId.ToString(), AllCountry.CityId.ToString() };

            // 若符合當地城市且不包含旅遊/宅配/SPA則帶當地城市，否則依照下述邏輯
            if (cityList.Count > 0 && (cityList.Intersect(exceptionCityId.ToList()).Count() > 0 || !cityList.Contains(locationCityId.ToString())))
            {
                locationCityId = GetDealCity(cityList, locationCityId);
            }
            return GetPponCityByCityId(locationCityId);
        }

        /// <summary>
        /// 取得顯示於頻道的cityId
        /// </summary>
        /// <param name="cityLsit">檔次活動區域</param>
        /// <param name="locationCityId">目前城市</param>
        /// <returns>PponCity</returns>
        public PponCity GetPponCityForDealChannel(List<string> cityList, int locationCityId)
        {
            List<string> channel = new List<string>();
            channel.Add(Travel.CityId.ToString());
            channel.Add(PBeautyLocation.CityId.ToString());
            channel.Add(AllCountry.CityId.ToString());

            // 若符合當地城市則帶當地城市，否則依照下述邏輯
            if (cityList.Count > 0 && cityList.Intersect(channel).Count() > 0)
            {
                locationCityId = GetDealCity(cityList, locationCityId);
                return GetPponCityByCityId(locationCityId);
            }
            else 
            {
                return GetPponCityForPponTag(cityList, locationCityId);
            }
        }

        private int GetDealCity(List<string> cityLsit, int locationCityId) 
        {
            int CityId = locationCityId;
            // 當在地與旅遊、玩美、全國宅配並存時，取旅遊、玩美(SPA)、全國宅配
            List<string> priorityCity = cityLsit.Where(x => x.Equals(Travel.CityId.ToString()) || x.Equals(PBeautyLocation.CityId.ToString()) || x.Equals(AllCountry.CityId.ToString())).ToList();
            if (priorityCity.Count > 0)
            {
                // 當與全國宅配並存時，取全國宅配
                if (priorityCity.Count > 1 && priorityCity.Contains(AllCountry.CityId.ToString()))
                {
                    CityId = AllCountry.CityId;
                }
                else
                {
                    CityId = int.Parse(priorityCity.FirstOrDefault());
                }
            }
            else
            {
                CityId = int.Parse(cityLsit.FirstOrDefault());
            }
            return CityId;
        }

        /// <summary>
        /// 回傳大區域範圍城市
        /// </summary>
        /// <param name="city_code">城市縮寫碼</param>
        /// <returns></returns>
        public static PponCity GetLargeAreaPponCityByCityId(string city_code)
        {
            switch (city_code)
            {
                case "TP":
                case "TA":
                case "JL":
                case "IL":
                case "JM":
                case "PH":
                case "LC":
                default:
                    return DefaultPponCityGroup.TaipeiCity;
                case "TY":
                    return DefaultPponCityGroup.Taoyuan;
                case "XZ":
                case "XA":
                case "ML":
                    return DefaultPponCityGroup.Hsinchu;
                case "TC":
                case "ZH":
                case "NT":
                case "YL":
                    return DefaultPponCityGroup.Taichung;
                case "TN":
                case "JY":
                case "JA":
                    return DefaultPponCityGroup.Tainan;
                case "GX":
                case "BD":
                case "HL":
                case "TD":
                    return DefaultPponCityGroup.Kaohsiung;
            }
        }
        /// <summary>
        /// 依據categoryId回傳對應的PponCity物件，查無資料回傳null
        /// </summary>
        /// <param name="categoryId">需為大於0以上之整數</param>
        /// <returns></returns>
        public static PponCity GetPponCityByCategoryId(int categoryId)
        {
            if (categoryId <= 0)
            {
                return null;
            }

            List<PponCity> selectedCities = DefaultPponCityGroup.Where(x => x.CategoryId == categoryId && categoryId != DefaultPponCityGroup.ArriveIn72Hrs.CategoryId).ToList();
            if (selectedCities.Count > 0)
            {
                return selectedCities.First();
            }
            return null;
        }
        /// <summary>
        /// 依據 頻道與區域的值 回傳對應的PponCity物件，查無資料回傳null
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="channelAreaId"></param>
        /// <returns></returns>
        public static PponCity GetPponCityByChannel(int channelId , int? channelAreaId)
        {
            if (channelId <= 0)
            {
                return null;
            }

            List<PponCity> selectedCities = DefaultPponCityGroup.Where(x => x.IsMatchChannel(channelId, channelAreaId)).ToList();
            if (selectedCities.Count > 0)
            {
                return selectedCities.First();
            }
            return null;
        }
        #endregion method

        #region 靜態函式與變數

        private static PponCityGroup group;

        public static PponCityGroup DefaultPponCityGroup
        {
            get
            {
                if (group == null)
                {
                    group = new PponCityGroup();
                }
                return group;
            }
        }

        #endregion 靜態函式與變數
    }

    public static class CityListExtension
    {
        public static List<PponCity> AddIfExists(this List<PponCity> src, PponCity obj)
        {
            if (obj != null)
            {
                src.Add(obj);
            }

            return src;
        }
    }
}
﻿using System.Net.Mail;

namespace LunchKingSite.BizLogic.Component
{
    public class EmailAgent
    {
        public MailMessage GetMailMessage(string sFrom, string sTo, string sSubject, string sBody)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(sFrom);
            msg.To.Add(sTo);
            msg.Subject = sSubject;
            msg.Body = sBody;
            msg.IsBodyHtml = true;
            return msg;
        }

        public MailMessage GetMailMessage(string sFrom, string sTo, string sSubject, string sBody, string sFromName)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(sFrom, sFromName);
            msg.To.Add(sTo);
            msg.Subject = sSubject;
            msg.Body = sBody;
            msg.IsBodyHtml = true;
            return msg;
        }


    }

}
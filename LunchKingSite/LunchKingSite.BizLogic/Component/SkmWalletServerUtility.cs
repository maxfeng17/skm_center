﻿using LunchKingSite.BizLogic.Models.Wallet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace LunchKingSite.BizLogic.Component
{
    public class SkmWalletServerUtility
    {
        private static readonly ISysConfProvider Config = ProviderFactory.Instance().GetConfig();
        private static ILog Logger = LogManager.GetLogger("Skm");

        private static HttpClient httpClientInstance;
        static SkmWalletServerUtility()
        {
            httpClientInstance = new HttpClient { BaseAddress = new Uri(Config.SkmWalletSiteUrl) };
            httpClientInstance.DefaultRequestHeaders.Connection.Add("keep-alive");
            httpClientInstance.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Config.SkmWalletAuthToken);
            try
            {
                httpClientInstance.SendAsync(new HttpRequestMessage
                {
                    Method = new HttpMethod("HEAD"),
                    RequestUri = new Uri(Config.SkmWalletSiteUrl)
                }).Result.EnsureSuccessStatusCode();
            } 
            catch (Exception ex)
            {
                Logger.Error("SkmWalletServerUtility 初始化靜態單一連線失敗", ex);
            }
        }

        #region EC交易
        public static WalletServerResponse<WalletGetPreTransNoResponse> GetPreTransNo(WalletGetPreTransNoRequest reqest)
        {
            WalletServerResponse<WalletGetPreTransNoResponse> returnValue = new WalletServerResponse<WalletGetPreTransNoResponse>();
            try
            {
                var uri = "/api/ECPay/GetPreTransNo";
                returnValue = Post<WalletServerResponse<WalletGetPreTransNoResponse>, WalletGetPreTransNoRequest>(reqest, uri);
            }
            catch(Exception ex)
            {
                Logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                returnValue.Code = WalletReturnCode.InvokeError;
                returnValue.Message = string.Format("API取得預約交易序號失敗({0})", ex.Message);
            }
            return returnValue;
        }

        /// <summary>
        /// 授權請款交易
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static WalletServerResponse<WalletPaymentResponse> AuthPayment(WalletAuthPaymentRequest request)
        {
            WalletServerResponse<WalletPaymentResponse> returnValue = new WalletServerResponse<WalletPaymentResponse>();
            try
            {
                var uri = "/api/ECPay/Auth";
                returnValue = Post<WalletServerResponse<WalletPaymentResponse>, WalletAuthPaymentRequest>(request, uri);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                returnValue.Code = WalletReturnCode.InvokeError;
                returnValue.Message = string.Format("API授權交易失敗({0})", ex.Message);
            }

            SystemFacade.SetApiLog("SkmWalletServer.Auth", "", request, returnValue);
            return returnValue;
        }


        /// <summary>
        /// Wallet 退貨
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static WalletServerResponse<WalletPaymentResponse> Refund(WalletAuthPaymentRequest request)
        {
            var returnValue = new WalletServerResponse<WalletPaymentResponse> { Code = WalletReturnCode.Success };
            try
            {
                var uri = "/api/ECPay/Refund";
                returnValue = Post<WalletServerResponse<WalletPaymentResponse>, WalletAuthPaymentRequest>(request, uri);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                returnValue.Code = WalletReturnCode.InvokeError;
                returnValue.Message = "API呼叫Wallet退貨失敗";
            }

            SystemFacade.SetApiLog("SkmWalletServer.Refund", "", request, returnValue);
            return returnValue;
        }

        /// <summary>
        /// Wallet 取得訂單狀態
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static WalletServerResponse<WalletPaymentResponse> GetPaymentResult(WalletAuthPaymentRequest request)
        {
            var returnValue = new WalletServerResponse<WalletPaymentResponse> { Code = WalletReturnCode.Success };
            try
            {
                var uri = "/api/ECPay/GetPaymentResult";
                returnValue = Post<WalletServerResponse<WalletPaymentResponse>, WalletAuthPaymentRequest>(request, uri);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                returnValue.Code = WalletReturnCode.InvokeError;
                returnValue.Message = "API呼叫Wallet取得訂單狀態失敗";
            }
            return returnValue;
        }

        /// <summary>
        /// Wallet 取消退貨
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static WalletServerResponse<WalletPaymentResponse> CancelRefund(WalletAuthPaymentRequest request)
        {
            var returnValue = new WalletServerResponse<WalletPaymentResponse> { Code = WalletReturnCode.Success };
            try
            {
                var uri = "/api/ECPay/CancelRefund";
                returnValue = Post<WalletServerResponse<WalletPaymentResponse>, WalletAuthPaymentRequest>(request, uri);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                returnValue.Code = WalletReturnCode.InvokeError;
                returnValue.Message = "API呼叫Wallet取消退貨失敗";
            }
            return returnValue;
        }

        #endregion

        #region 信用卡樣板

        /// <summary>
        /// 新增信用卡卡樣資料
        /// </summary>
        /// <param name="request">新增卡樣API請求資訊</param>
        /// <returns>新增信用卡卡樣API回應資訊</returns>
        public static WalletServerResponse<WalletCreditCardCategory> AddCreditCardCategory(WalletCreditCardCategoryRequest request)
        {
            var returnValue = new WalletServerResponse<WalletCreditCardCategory> { Code = WalletReturnCode.Success };
            try
            {
                var uri = "/api/ECPay/AddCreditCardCategory";
                returnValue = Post<WalletServerResponse<WalletCreditCardCategory>, WalletCreditCardCategoryRequest>(request, uri);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                returnValue.Code = WalletReturnCode.InvokeError;
                returnValue.Message = "API呼叫Wallet新增信用卡卡樣資料失敗";
            }
            return returnValue;
        }

        /// <summary>
        /// 編輯信用卡卡樣資料
        /// </summary>
        /// <param name="request">編輯卡樣API請求資訊</param>
        /// <returns>編輯信用卡卡樣API回應資訊</returns>
        public static WalletServerResponse<WalletCreditCardCategory> EditCreditCardCategory(WalletCreditCardCategoryRequest request)
        {
            var returnValue = new WalletServerResponse<WalletCreditCardCategory> { Code = WalletReturnCode.Success };
            try
            {
                var uri = "/api/ECPay/EditCreditCardCategory";
                returnValue = Post<WalletServerResponse<WalletCreditCardCategory>, WalletCreditCardCategoryRequest>(request, uri);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                returnValue.Code = WalletReturnCode.InvokeError;
                returnValue.Message = "API呼叫Wallet編輯信用卡卡樣資料失敗";
            }
            return returnValue;
        }

        /// <summary>
        /// 刪除信用卡卡樣資料
        /// </summary>
        /// <param name="request">信用卡卡樣編號</param>
        /// <returns>刪除信用卡卡樣API回應資訊</returns>
        public static WalletServerResponse<int> DeleteCreditCardCategory(WalletDeleteCreditCardCategoryRequest request)
        {
            var returnValue = new WalletServerResponse<int> { Code = WalletReturnCode.Success };
            try
            {
                var uri = "/api/ECPay/DeleteCreditCardCategory";
                returnValue = Post<WalletServerResponse<int>, WalletDeleteCreditCardCategoryRequest>(request, uri);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                returnValue.Code = WalletReturnCode.InvokeError;
                returnValue.Message = "API呼叫Wallet刪除信用卡卡樣資料失敗";
            }
            return returnValue;
        }

        /// <summary>
        /// 依條件查詢信用卡卡樣資訊清單
        /// </summary>
        /// <param name="request">查詢條件</param>
        /// <returns>信用卡卡樣資訊清單</returns>
        public static WalletServerResponse<WalletGetCreditCardCategoryResponse> GetCreditCardCategories(WalletGetCreditCardCategoriesRequest request)
        {
            var returnValue = new WalletServerResponse<WalletGetCreditCardCategoryResponse> { Code = WalletReturnCode.Success };
            try
            {
                var uri = "/api/ECPay/GetCreditCardCategories";
                returnValue = Post<WalletServerResponse<WalletGetCreditCardCategoryResponse>, WalletGetCreditCardCategoriesRequest>(request, uri);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                returnValue.Code = WalletReturnCode.InvokeError;
                returnValue.Message = "API呼叫Wallet取得信用卡卡樣資訊清單失敗";
            }
            return returnValue;
        }

        /// <summary>
        /// 依卡樣編號取得信用卡卡樣資訊
        /// </summary>
        /// <param name="request">卡樣編號</param>
        /// <returns>信用卡卡樣資訊</returns>
        public static WalletServerResponse<WalletGetCreditCardCategory> GetCreditCardCategoryById(WalletGetCreditCardCategoryByIdRequest request)
        {
            var returnValue = new WalletServerResponse<WalletGetCreditCardCategory> { Code = WalletReturnCode.Success };
            try
            {
                var uri = "/api/ECPay/GetCreditCardCategoryById";
                returnValue = Post<WalletServerResponse<WalletGetCreditCardCategory>, WalletGetCreditCardCategoryByIdRequest>(request, uri);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                returnValue.Code = WalletReturnCode.InvokeError;
                returnValue.Message = "API呼叫Wallet取得信用卡卡樣資訊失敗";
            }
            return returnValue;
        }
        #endregion


        private static T Post<T, K>(K requestMdel, string uri)
        {
            System.Diagnostics.Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();
            int httpStatusCode;
            var requestJson = JsonConvert.SerializeObject(requestMdel);
            T returnValue = default(T);
            if (Config.SkmWalletUseStaticClient)
            {
                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, uri))
                {
                    httpRequestMessage.Content = new StringContent(requestJson, Encoding.UTF8, "application/json");
                    using (HttpResponseMessage responseMessage = httpClientInstance.SendAsync(httpRequestMessage).Result)
                    {
                        httpStatusCode = (int)responseMessage.StatusCode;
                        if (responseMessage.IsSuccessStatusCode)
                        {
                            string responseBody = responseMessage.Content.ReadAsStringAsync().Result;
                            returnValue = JsonConvert.DeserializeObject<T>(responseBody);
                        }
                        else
                        {
                            watch.Stop();
                            SetApiLog(watch.Elapsed.TotalSeconds, httpStatusCode, uri, requestJson, returnValue);
                            throw new AggregateException(responseMessage.ReasonPhrase);
                        }
                    }
                }
            }
            else
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, uri))
                    {
                        httpClient.BaseAddress = new Uri(Config.SkmWalletSiteUrl);
                        httpClient.DefaultRequestHeaders.Authorization =
                            new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Config.SkmWalletAuthToken);
                        httpRequestMessage.Content = new StringContent(requestJson, Encoding.UTF8, "application/json");
                        using (HttpResponseMessage responseMessage = httpClient.SendAsync(httpRequestMessage).Result)
                        {
                            httpStatusCode = (int)responseMessage.StatusCode;
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                string responseBody = responseMessage.Content.ReadAsStringAsync().Result;
                                returnValue = JsonConvert.DeserializeObject<T>(responseBody);
                            }
                            else
                            {
                                watch.Stop();
                                SetApiLog(watch.Elapsed.TotalSeconds, httpStatusCode, uri, requestJson, returnValue);
                                throw new AggregateException(responseMessage.ReasonPhrase);
                            }
                        }
                    }
                }
            }

            watch.Stop();
            SetApiLog(watch.Elapsed.TotalSeconds, httpStatusCode, uri, requestJson, returnValue);
            return returnValue;
        }

        private static void SetApiLog(double elapsedSecs, int httpStatusCode, string uri, string requestJson, object returnValue)
        {
            var memo = string.Format("{0}, {1}", httpStatusCode, elapsedSecs);
            SystemFacade.SetApiLog(Config.SkmWalletSiteUrl + uri, memo, requestJson, returnValue);
        }
    }
}

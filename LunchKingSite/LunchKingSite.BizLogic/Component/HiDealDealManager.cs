﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class HiDealDealManager
    {
        public const string ManagerNameForJob = "HiDealDealManager";
        private static List<HiDealDealDataEntity> _deals;
        private static readonly IHiDealProvider HiDealProvider;
        private static readonly ISysConfProvider SysConfProvider;
        private static readonly log4net.ILog log;

        static HiDealDealManager()
        {
            HiDealProvider = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            SysConfProvider = ProviderFactory.Instance().GetConfig();
            log = log4net.LogManager.GetLogger(typeof(HiDealDealManager));
            _deals = new List<HiDealDealDataEntity>();
        }

        #region public

        /// <summary>
        /// 取得品生活今日所有檔次
        /// </summary>
        /// <returns></returns>
        public static ViewHiDealCollection HiDealGetTodayDeals()
        {
            ViewHiDealCollection HiDeals = new ViewHiDealCollection();
            HiDeals.AddRange(HiDealProvider.ViewHiDealGetTodayDeals((int)HiDealRegionSystemCode.Overview, "0", null, true, ViewHiDeal.Columns.CitySeq));
            HiDeals.AddRange(HiDealProvider.ViewHiDealGetTodayDeals((int)HiDealRegionSystemCode.Overview, "0", null, false, ViewHiDeal.Columns.CitySeq));
            return HiDeals;
        }

        public static ViewHiDealStoresInfoCollection ViewHiDealStoresInfoGetByDealId(int dealId) 
        {
            return HiDealProvider.ViewHiDealDealStoreInfoGetByHiDealId(dealId);
        }

        /// <summary>
        /// 取得品生活檔次權益說明
        /// </summary>
        /// <param name="dealId"></param>
        /// <returns></returns>
        public static HiDealContent HiDealContentGetRightsAndInterests(int dealId) 
        {
            return HiDealProvider.HiDealContentGetList(-1, -1, HiDealContent.Columns.Seq, HiDealContent.Columns.RefId + "=" + dealId, HiDealContent.Columns.IsShow + "=True", HiDealContent.Columns.Seq + "=5").FirstOrDefault();
        }
        
        public static void ClearManagerData(bool isSyncWebServer = true)
        {
            _deals = new List<HiDealDealDataEntity>();
        }

        public static void ReloadHiDealDealGetById(int dealId, bool isSyncWebServer = true)
        {

            //本機清暫存動作
            var rtns = _deals.Where(x => x.Id == dealId).ToList();
            if (rtns.Count > 0)
            {
                //該檔已有暫存資料，清除舊資料
                var rtn = rtns.First();
                _deals.Remove(rtn);
            }

            var deal = HiDealProvider.HiDealDealGet(dealId);
            var products = HiDealProvider.HiDealProductCollectionGet(dealId);
            var regions = HiDealProvider.HiDealRegionGetList(dealId);

            if (deal.IsLoaded)
            {
                HiDealDealDataEntity entity = new HiDealDealDataEntity(deal, products, regions,
                    GetDealVisaCardType(dealId));
                _deals.Add(entity);
            }
        }

        public static HiDealDeal HiDealDealGetById(int dealId)
        {
            var rtns = _deals.Where(x => x.Id == dealId).ToList();
            var rtn = new HiDealDeal();
            if (rtns.Count > 0)
            {
                rtn = rtns.First().Deal;
            }
            else
            {
                var entity = LoadDeal(dealId);
                if (entity != null)
                {
                    rtn = entity.Deal;
                }
            }
            return rtn;
        }

        public static HiDealProductCollection HiDealProductGetListByDealId(int dealId)
        {
            var rtns = _deals.Where(x => x.Id == dealId).ToList();
            var rtn = new HiDealProductCollection();
            if (rtns.Count > 0)
            {
                rtn = rtns.First().Products;
            }
            else
            {
                var entity = LoadDeal(dealId);
                if (entity != null)
                {
                    rtn = entity.Products;
                }
            }
            return rtn;
        }
        /// <summary>
        /// 查詢檔次設定的區域
        /// </summary>
        /// <param name="dealId"></param>
        /// <returns></returns>
        public static HiDealRegionCollection HiDealRegionGetListByDealId(int dealId)
        {
            var regions = new HiDealRegionCollection();
            var deals = _deals.Where(x => x.Id == dealId).ToList();
            if (deals.Count > 0)
            {
                regions = deals.First().Regions;
            }
            else
            {
                var entity = LoadDeal(dealId);
                if (entity != null)
                {
                    regions = entity.Regions;
                }
            }
            return regions;
        }

        /// <summary>
        /// 回傳商品是否已售完，已售完回傳true，否則回傳false
        /// </summary>
        /// <param name="dealId"></param>
        /// <returns></returns>
        public static bool DealProductIsSoldOut(int dealId)
        {
            var products = HiDealProductGetListByDealId(dealId);
            int inventory = products.Where(x => x.IsOnline != null && x.IsOnline.Value).Sum(product => HiDealCouponManager.GetCouponInventoryQuantity(product.Id));
            return inventory == 0;
        }


        public static bool DealCloseWork(HiDealDeal deal)
        {
            //檢查檔次是否已結檔。
            if (deal.DealEndTime > DateTime.Now)
            {
                //未結檔，停止作業。
                log.Error(string.Format("品生活 DealId：{0} 尚未結檔，不可進行關檔的相關作業。", deal.Id));
                return false;
            }
            //若已結檔，取出所有商品資料，並逐一進行處理
            var products = HiDealProvider.HiDealProductGetList(0, 0, HiDealProduct.Columns.Id, HiDealProduct.Columns.DealId + "=" + deal.Id);
            foreach (var product in products)
            {
                //取得商品的商品成本與運費成本。
                var productCose = HiDealProvider.HiDealProductCostCollectionGetByProductId(product.Id);
            }

            //取出所有成立的訂單，依時間順序由成立時間早到晚排序
            //訂單逐筆計算成本，並修改到cash_trust_log中。

            return true;
        }

        public static HiDealDealDataEntity LoadEntity(int dealId)
        {
            var deal = _deals.FirstOrDefault(x => x.Id == dealId);
            if (deal == null)
            {
                deal = LoadDeal(dealId);
                if (deal != null)
                {
                    return deal;
                }
            }
            return deal;
        }

        public static HiDealSpecialDiscountType GetDealVisaCardType(int dealId)
        {
            int? specialId = HiDealProvider.HiDealSpecialIdGet(dealId);
            if (specialId == null)
            {
                return HiDealSpecialDiscountType.NotVisa;
            }
            return (HiDealSpecialDiscountType)specialId;
        }


        #endregion public

        #region private method
        private static HiDealDealDataEntity LoadDeal(int dealId)
        {
            HiDealDealDataEntity entity = null;
            lock (typeof(HiDealDealManager))
            {
                //查詢前在搜尋一次
                var reSelRtns = _deals.Where(x => x.Id == dealId).ToList();
                if (reSelRtns.Count > 0)
                {
                    entity = reSelRtns.First();
                }
                else
                {
                    var deal = HiDealProvider.HiDealDealGet(dealId);
                    var products = HiDealProvider.HiDealProductCollectionGet(dealId);
                    var regions = HiDealProvider.HiDealRegionGetList(dealId);

                    if (deal.IsLoaded)
                    {
                        entity = new HiDealDealDataEntity(deal, products, regions,
                                                          GetDealVisaCardType(dealId));
                        _deals.Add(entity);
                    }
                }
            }
            return entity;
        }

        #endregion private

        /// <summary>
        /// 判斷是否為Visa合作檔次
        /// 但如果是Visa優先檔，要判斷是否在上檔3天內，如果超過3天，就視為一般檔
        /// </summary>
        /// <param name="dealId"></param>
        /// <returns></returns>
        public static bool IsVisaSpecialWorking(int dealId)
        {
            var deals = _deals.Where(x => x.Id == dealId).ToList();
            HiDealDealDataEntity entity;
            if (deals.Count > 0)
            {
                entity = deals.First();
            }
            else
            {
                entity = LoadDeal(dealId);
            }

            return entity.DealVisaType == HiDealSpecialDiscountType.VisaPrivate ||
                (entity.DealVisaType == HiDealSpecialDiscountType.VisaPriority &&
                    entity.Deal.WithinVisaPriorityDayLimit);
        }

        public static bool IsCategorySpecialTag(int dealId,int categoryId)
        {
            HiDealCategoryCollection categoryCol = HiDealProvider.HiDealDealCategoryGetListByDealId(dealId);

            if (categoryCol != null)
            {
                return (categoryCol.Any(x => x.CategoryCodeId == categoryId));
            }        

            return false;
        }

    }

    public class HiDealDealDataEntity
    {
        public int Id { get; private set; }
        private HiDealDeal deal;
        public HiDealDeal Deal
        {
            get { return deal; }
        }
        private HiDealProductCollection products;
        public HiDealProductCollection Products
        {
            get
            {
                var rtn = new HiDealProductCollection();
                rtn.AddRange(products.Select(x => x.Clone()));
                return rtn;
            }
        }
        private HiDealRegionCollection regions;
        public HiDealRegionCollection Regions
        {
            get
            {
                var rtn = new HiDealRegionCollection();
                rtn.AddRange(regions.Select(x => x.Clone()));
                return rtn;
            }
        }

        public HiDealSpecialDiscountType DealVisaType { get; private set; }

        public HiDealDealDataEntity(HiDealDeal theDeal, HiDealProductCollection theProducts,
            HiDealRegionCollection theRegions, HiDealSpecialDiscountType specialCard)
        {
            Id = theDeal.Id;
            deal = theDeal;
            products = theProducts;
            regions = theRegions;
            DealVisaType = specialCard;
        }

    }

}

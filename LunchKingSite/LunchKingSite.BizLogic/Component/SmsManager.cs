﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    public class SmsManager
    {
        public SmsFormatterBase Formatter { get; set; }
        
        #region .ctor

        public SmsManager() { }

        public SmsManager(SmsFormatterBase formatter)
        {
            Formatter = formatter;
        }

        #endregion
    }


    public abstract class SmsFormatterBase
    {
        public abstract string DbTemplateContent { get; set; }

        /// <summary>
        /// 把相關屬性組合成資料庫的格式
        /// </summary>
        /// <returns></returns>
        public abstract string ToDbFormat();

        /// <summary>
        /// 把資料庫範本值轉換成簡訊字串
        /// </summary>
        /// <param name="dbContent"></param>
        /// <returns></returns>
        public abstract string ToSms();
    }

    #region example implementations

    public class FakeSmsFormatter : SmsFormatterBase
    {
        public override string DbTemplateContent { get; set; }
        public string Title { get; set; }
        public string Prefix { get; set; }
        public string Information { get; set; }
        public string BeginDate { get; set; }
        public string ExpireDate { get; set; }

        public FakeSmsFormatter(int key)
        {
            //get information based on key
            BeginDate = "2012/01/01";
            ExpireDate = "2012/03/31";
            DbTemplateContent = "[亂七八糟]|哈囉|您的憑證號碼為: {0} 驗證碼: {1}. 請在 {2} ~ {3} 內使用.";
        }

        public FakeSmsFormatter(string s)
        {
            DbTemplateContent = s;
        }

        public override string ToDbFormat()
        {
            Title = "[亂七八糟]";
            Prefix = "哈囉";
            Information = "您的憑證號碼為: {0} 驗證碼: {1}. 請在 {2} ~ {3} 內使用.";

            return string.Join("|", Title, Prefix, Information);
        }

        public override string ToSms()
        {
            DbTemplateContent = "[亂七八糟]|哈囉|您的憑證號碼為: {0} 驗證碼: {1}. 請在 {2} ~ {3} 內使用.";

            string transformedStr = DbTemplateContent.Replace("|", "");
            if (!string.IsNullOrWhiteSpace(BeginDate))
            {
                transformedStr = transformedStr.Replace("{2}", BeginDate);
            }

            if (!string.IsNullOrWhiteSpace(ExpireDate))
            {
                transformedStr = transformedStr.Replace("{3}", ExpireDate);
            }

            return transformedStr;
        }
    }
   
    #endregion
    
}

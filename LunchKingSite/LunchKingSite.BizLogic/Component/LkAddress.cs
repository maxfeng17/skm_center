﻿using System;

namespace LunchKingSite.BizLogic.Component
{
    [Serializable]
    public abstract class LkAddressBase
    {
        public const int DefaultCityId = -1;
        public const int DefaultTownshipId = -1;

        protected LkAddressBase()
        {
            CityId = DefaultCityId;
            TownshipId = DefaultTownshipId;
            AddressDesc = "";
        }

        /// <summary>
        /// 城市的Id
        /// </summary>
        public int CityId { get; set; }
        /// <summary>
        /// 鄉鎮市區的id
        /// </summary>
        public int TownshipId { get; set; }
        /// <summary>
        /// 城市與鄉鎮市區以外的地址文字段落
        /// </summary>
        public string AddressDesc { get; set; }
        /// <summary>
        /// 完整地址
        /// </summary>
        public string CompleteAddress
        {
            get { return this.ToStringWithZipCode(); }
        }

        protected abstract string ToStringWithZipCode();
    }

    [Serializable]
    public class LkAddressLite : LkAddressBase
    {
        protected override string ToStringWithZipCode()
        {
            return CityManager.CityTownShopStringGet(this.TownshipId) + AddressDesc;
        }

        public override string ToString()
        {
            var township = CityManager.TownShipGetById(TownshipId);
            if (township != null && string.IsNullOrWhiteSpace(township.ZipCode) == false)
            {
                return string.Format("{0} {1}{2}",
                    township.ZipCode, CityManager.CityTownShopStringGet(this.TownshipId), AddressDesc);
            }
            return CityManager.CityTownShopStringGet(this.TownshipId) + AddressDesc;
        }
    }

    public class LkAddress : LkAddressBase
    {
        public static readonly Guid DefaultBuildingGuid = Guid.Empty;

        /// <summary>
        /// 路段的Building
        /// </summary>
        public Guid BuildingGuid { get; set; }

        public override string ToString()
        {
            var township = CityManager.TownShipGetById(TownshipId);
            if (township != null && string.IsNullOrWhiteSpace(township.ZipCode) == false)
            {
                return string.Format("{0} {1}{2}",
                    township.ZipCode, CityManager.CityTownShopBuildingStringGet(BuildingGuid), AddressDesc);
            }
            return CityManager.CityTownShopBuildingStringGet(BuildingGuid) + AddressDesc;
        }

        protected override string ToStringWithZipCode()
        {
            //return CityManager.CityTownShopBuildingStringGet(BuildingGuid) + AddressDesc;
            return CityManager.CityTownShopStringGet(this.TownshipId) + AddressDesc;
        }

        public LkAddress()
        {
            BuildingGuid = DefaultBuildingGuid;
        }

        public static LkAddress Empty
        {
            get
            {
                return new LkAddress
                {
                    BuildingGuid = Guid.Empty,
                    CityId = -1,
                    TownshipId = -1,
                    AddressDesc = string.Empty
                };
            }
        }
    }
}

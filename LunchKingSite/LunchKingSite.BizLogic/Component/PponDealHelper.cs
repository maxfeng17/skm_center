﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Component
{
    public class PponDealHelper
    {
        /// <summary>
        /// 從Remark欄位截取一姬說好康的內容 (在第2個位置)
        /// </summary>
        /// <param name="remarkPackage"></param>
        /// <returns></returns>
        public static string GetDealContentRemark(string remarkPackage)
        {
            string[] remarks = string.IsNullOrEmpty(remarkPackage) == false ? remarkPackage.Split(new[] { "||" }, StringSplitOptions.None) : new string[3];
            if (remarks.Length < 3)
            {
                return string.Empty;
            }
            return remarks[2];
        }
        /// <summary>
        /// 回寫一姬說好康到 Remark
        /// </summary>
        /// <param name="remarkPackage"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        public static string PutDealContentRemark(string remarkPackage, string remark)
        {
            string[] remarks = !string.IsNullOrEmpty(remarkPackage) ? remarkPackage.Split(new[] { "||" }, StringSplitOptions.None) : new string[3];
            remarks[2] = remark;
            return String.Join("||", remarks);
        }

        public static DateTime GetTodayBaseDate()
        {
            return DateTime.ParseExact(DateTime.Today.ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
        }
    }
}

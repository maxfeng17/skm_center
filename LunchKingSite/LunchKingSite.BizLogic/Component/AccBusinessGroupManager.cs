﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class AccBusinessGroupManager
    {
        static ISystemProvider _systemProv;

        private static AccBusinessGroupCollection _accBusinessGroupList;
        public static AccBusinessGroupCollection AccBusinessGroupList
        {
            get
            {
                if (_accBusinessGroupList.Count == 0)
                {
                    _accBusinessGroupList = _systemProv.AccBusinessGroupGetList();
                }
                return _accBusinessGroupList;
            }
        }
        public static string GetAccBusinessGroupName(int groupId)
        {
            var rtns = AccBusinessGroupList.Where(x => x.AccBusinessGroupId == groupId).ToList();
            if (rtns.Count > 0)
            {
                return rtns[0].AccBusinessGroupName;
            }
            else
            {
                return string.Empty;
            }
        }

        static AccBusinessGroupManager()
        {
            _systemProv = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _accBusinessGroupList = new AccBusinessGroupCollection();
        }

        public static void ClearData()
        {
            _accBusinessGroupList = new AccBusinessGroupCollection();
        }
    }
}

﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class GoogleUser : ExternalUser
    {
        public const string DefaultScope = "email%20profile";

        public static GoogleUser Get(string token)
        {
            GoogleUser exuser = null;

            string url = string.Format(
                "https://www.googleapis.com/oauth2/v1/userinfo?access_token={0}",
                token);
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            request.Method = "GET";

            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());

                    dynamic user = JObject.Parse(reader.ReadToEnd());
                    exuser = new GoogleUser();
                    exuser.Id = user.id;
                    exuser.Email = user.email;
                    exuser.FirstName = user.given_name;
                    exuser.LastName = user.family_name;

                    if (user.picture != null)
                    {
                        exuser.Pic = (string)user.picture;
                    }
                        
                    if (user.gender != null)
                    {
                        if (user.gender == "male")
                        {
                            exuser.Gender = 1;
                        }
                        else if (user.gender == "female")
                        {
                            exuser.Gender = 0;
                        }
                    }
                }
            }
            catch
            {
            }
            return exuser;
        }
    }
}
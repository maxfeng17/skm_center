﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Component
{
    public class HiDealRegionManager
    {
        /// <summary>
        /// 總覽的ID
        /// </summary>
        public static int DealOverviewRegion = (int) PiinlifeMailSendCity.Overview;
        /// <summary>
        /// 取得可以設定的 品生活 區域資料
        /// </summary>
        /// <returns></returns>
        public static SystemCodeCollection GetRegionOfSet()
        {
            return SystemCodeManager.GetHiDealRegion();
        }
        /// <summary>
        /// 取得前台顯示用的 品生活 區域資料 排除不顯示的部分。
        /// </summary>
        /// <returns></returns>
        public static SystemCodeCollection GetRegionOfShow()
        {
            //預留的函示，目前沒有用到，也沒有甚麼要排除的項目。
            return SystemCodeManager.GetHiDealRegion();
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Jobs;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LunchKingSite.BizLogic.Component
{
    public class VerifyAccount
    {
        private int _acctCodeLength = 4;
        private int _pwLength = 4;
        private int _regCodeLength = 6;
        private IMemberProvider _mp;
        private IOrderProvider _op;
        private IPponProvider _pp;
        private ISellerProvider _sp;
        private IVerificationProvider _vp;
        private IHiDealProvider _hdp;
        public static DateTime _postponedToDay = Convert.ToDateTime("2011/11/01 00:00:00");
        public static string _postponedUnit = "w"; //m：月；w：週；d：日
        public static int _postponedQuantity = 1;
        private static readonly ILog log = LogManager.GetLogger(typeof(PponDealChore));

        //建構式
        public VerifyAccount()
        {
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            _hdp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        }

        #region 帳密產生/修改

        /// <summary>
        /// 產生帳號/密碼，請透過 fa
        /// </summary>
        /// <param name="isFilterSeller">是否過濾seller</param>
        /// <param name="isFilterStore">是否過濾seller</param>
        /// <returns></returns>
        public int SetAllInfo(bool isFilterSeller, bool isFilterStore)
        {
            int result = 0;
            SellerCollection sellers = _sp.GetSellerAll();
            try
            {
                result += sellers.Sum(seller => SetStoreAcct(seller, new Store(), isFilterSeller));
                result += (from store in _sp.GetStoreAll()
                           let seller =
                               sellers.Where(x => x.Guid == store.SellerGuid).DefaultIfEmpty(new Seller()).First()
                           where !seller.IsNew
                           select SetStoreAcct(seller, store, isFilterStore)).Sum();
            }
            catch (Exception ex)
            {
                result = -1;
                log.Warn("VerifyAccount.SetAllInfo Error: " + ex.ToString());
            }

            return result;
        }

        /// <summary>
        /// 設定帳號、密碼
        /// </summary>
        /// <param name="objSeller">不論Seller，Store都要有Seller這個物件</param>
        /// <param name="objStore">Store物件，若是Seller的話，直接傳入new store()</param>
        /// <param name="isFilter">是否要檢查該店家的帳號是否已經存在</param>
        /// <returns>
        /// -1: 錯誤 <br />
        /// 0: 沒有新增 <br />
        /// 1: 成功新增 <br />
        /// </returns>
        public int SetStoreAcct(Seller objSeller, Store objStore, bool isFilter)
        {
            bool isInfoHave = _sp.IsStoreAcctInfoHave(objSeller.Guid, objStore.Guid);
            int saveState = 0;
            string acctCode = string.Empty;
            string regCode = string.Empty;
            string regNew = string.Empty;
            if (isFilter && isInfoHave)
            {
                return 0;
            }
            VerificationAcctStatus status = (isInfoHave)
                                                ? VerificationAcctStatus.ModifyPw
                                                : VerificationAcctStatus.Create;
            string acctNo = "00000";
            string pw = GenPassword(_pwLength);
            Guid storeGuid = (objStore.IsNew) ? Guid.Empty : objStore.Guid;
            try
            {
                regCode = GenRegisterCode(objSeller.SellerId, 6);
                regNew = GenRegisterCode(objSeller.SellerId, 6);
                if (objStore.IsNew)
                {
                    int weigth = 0;
                    StoreAcctInfoCollection seller_acct = _sp.StoreAcctInfoGetAllList();
                    do
                    {
                        acctCode = GenAcctCode(objSeller.Guid.ToString(), _acctCodeLength, weigth);
                        weigth++;
                    } while (seller_acct.Count(x => x.AcctCode == acctCode) > 0);
                }
                else
                {
                    //分店
                    acctCode = _sp.GetStoreAcctCodeBySeller(objSeller.Guid);
                    if (acctCode != string.Empty)
                    {
                        acctNo = (isInfoHave)
                                     ? _sp.GetStoreAcctNoLastestBySeller(objSeller.Guid).ToString("00000")
                                     : (_sp.GetStoreAcctNoLastestBySeller(objSeller.Guid) + 1).ToString("00000");
                    }
                }
            }
            catch (Exception ex)
            {
                saveState = -1;
                log.Warn("VerifyAccount.SetStoreAcct Error:" + ex);
            }
            finally
            {
                if (saveState == 0)
                {
                    SetStoresAcct(objSeller.Guid, storeGuid, acctCode, acctNo, pw, status, regCode, regNew);
                    saveState = 1;
                }
            }
            return (saveState > 0) ? 1 : 0;
        }

        private void SetStoresAcct(Guid guidSeller, Guid guidStore, string strAcctCode, string strAcctNo, string strPw,
                                   VerificationAcctStatus status, string strRegCode, string strRegNew)
        {
            StoreAcctInfo sai = _sp.GetStoreAcctByGuids(guidSeller, guidStore);
            if (sai.IsNew)
            {
                sai.MarkNew();
            }
            else
            {
                sai.MarkOld();
            }
            sai.SellerGuid = guidSeller;
            sai.StoreGuid = guidStore;
            sai.AcctCode = strAcctCode;
            sai.AcctNo = strAcctNo;
            sai.AcctPw = strPw;
            sai.Status = (int)status;
            sai.RegisterCode = strRegCode;
            sai.NewRegisterCode = strRegNew;
            _sp.StoreAcctSet(sai);
            StoreAcctInfoLog sail = new StoreAcctInfoLog();
            sail.StoreAcctInfoId = sai.Id;
            sail.CreateTime = DateTime.Now;
            sail.CreateId = "system";
            sail.Status = (int)status;
            _sp.StoreAcctLogSet(sail);
        }

        //產生Seller的帳號
        private string GenAcctCode(string strSellerGuid, int intLength, int intWeight)
        {
            string rtnAcct = string.Empty;
            try
            {
                Random seed = new Random(GetStringAsciiCode(strSellerGuid) + intWeight);

                for (int i = 0; i < intLength; i++)
                {
                    switch (seed.Next(0, 3))
                    {
                        case 0:
                            rtnAcct += seed.Next(0, 10);
                            break;

                        case 1:
                            rtnAcct += (char)seed.Next(65, 91);
                            break;

                        case 2:
                            rtnAcct += (char)seed.Next(97, 122);
                            break;
                    }
                }
            }
            catch
            {
                rtnAcct = string.Empty;
            }
            return rtnAcct;
        }

        /// <summary>
        /// 產生密碼
        /// </summary>
        /// <param name="intLength">欲產出的密碼長度</param>
        /// <returns></returns>
        private string GenPassword(int intLength)
        {
            string rtnPw = string.Empty;
            for (int i = 0; i < intLength; i++)
            {
                rtnPw += new Random(DateTime.Now.Millisecond + i).Next(0, 10).ToString();
            }
            return rtnPw;
        }

        private string GenRegisterCode(string strSellerId, int intLength)
        {
            string rtnRegCode = string.Empty;
            try
            {
                for (int i = 0; i < intLength; i++)
                {
                    Random seed = new Random(DateTime.Now.Millisecond + (i ^ 2));
                    switch (seed.Next(0, 3))
                    {
                        case 0:
                            rtnRegCode += seed.Next(0, 10);
                            break;

                        case 1:
                            rtnRegCode += (char)seed.Next(65, 91);
                            break;

                        case 2:
                            rtnRegCode += (char)seed.Next(97, 122);
                            break;
                    }
                }
            }
            catch
            {
                rtnRegCode = string.Empty;
            }
            return rtnRegCode;
        }

        /// <summary>
        /// 將字串轉成AsciiCode
        /// </summary>
        /// <param name="strTransString"></param>
        /// <returns>int</returns>
        private int GetStringAsciiCode(string strTransString)
        {
            Random seed = new Random(DateTime.Now.Millisecond);
            int length = seed.Next(1, 7);
            string codeString = string.Empty;
            int codeInt = 0;
            foreach (char c in strTransString)
            {
                codeString += Convert.ToInt32(c);
                if (codeString.Length > length)
                {
                    codeInt += Convert.ToInt32(codeString);
                    codeString = string.Empty;
                }
            }
            return codeInt;
        }

        #endregion 帳密產生/修改

        #region Login/LoginOut

        /// <summary>
        /// APP登入，需傳入帳號、密碼、手機機碼以及SIM卡編碼
        /// </summary>
        /// <param name="strAcct"></param>
        /// <param name="strPw"></param>
        /// <param name="strImei"></param>
        /// <param name="strImsi"></param>
        /// <returns></returns>
        public Dictionary<string, string> ActLogin(string strAcct, string strPw, string strImei, string strImsi)
        {
            Dictionary<string, string> rtnInfo = new Dictionary<string, string>();
            StoreAcctInfoLog sail = new StoreAcctInfoLog();
            rtnInfo.Add("is_login", true.ToString());
            rtnInfo.Add("seller_guid", "");
            rtnInfo.Add("store_guid", "");
            rtnInfo.Add("session_id", "");
            rtnInfo.Add("login_id", "");
            rtnInfo.Add("seller_name", "");
            rtnInfo.Add("store_name", "");
            rtnInfo.Add("is_hideal", false.ToString());
            if (!ChkLoginInfo(strAcct, strPw, strImei, strImsi))
            {
                rtnInfo["is_login"] = false.ToString();
            }
            else
            {
                StoreAcctInfo sai = _vp.GetAcctInfoByAcct(strAcct, strPw, strImei, strImsi);
                Guid storeGuid = sai.StoreGuid.GetValueOrDefault(new Guid());

                Store store = (storeGuid == Guid.NewGuid()) ? new Store() : _sp.StoreGet(storeGuid);
                Seller seller = _sp.SellerGet(sai.SellerGuid);
                if (sai.IsNew)
                {
                    rtnInfo["is_login"] = false.ToString();
                }
                else
                {
                    rtnInfo["is_login"] = true.ToString();
                    rtnInfo["session_id"] = "00000";
                    rtnInfo["login_id"] = sai.Id.ToString();
                    rtnInfo["seller_guid"] = sai.SellerGuid.ToString();
                    rtnInfo["store_guid"] = (sai.StoreGuid == new Guid() || sai.StoreGuid == null)
                                                ? string.Empty
                                                : sai.StoreGuid.ToString();
                    rtnInfo["seller_name"] = seller.SellerName;
                    rtnInfo["store_name"] = (store.IsNew) ? "" : store.StoreName;

                    #region 是否有hideal的資料

                    bool isHdp = _vp.HaveHiDealProd(strAcct, strPw, seller.Guid);
                    rtnInfo["is_hideal"] = isHdp.ToString();

                    #endregion 是否有hideal的資料
                }
            }

            return rtnInfo;
        }

        /// <summary>
        /// 判斷傳入的登入資訊是否無誤
        /// </summary>
        /// <param name="strAcct"></param>
        /// <param name="strPw"></param>
        /// <param name="strImei"></param>
        /// <param name="strImsi"></param>
        /// <returns></returns>
        private bool ChkLoginInfo(string strAcct, string strPw, string strImei, string strImsi)
        {
            bool rtnChk = true;
            if (string.IsNullOrEmpty(strAcct) || string.IsNullOrEmpty(strPw)
                || string.IsNullOrEmpty(strImei) || string.IsNullOrEmpty(strImsi))
            {
                rtnChk = false;
            }
            else
            {
                if (strAcct.Count() > 9)
                {
                    rtnChk = false;
                }

                if (strPw.Count() != 4)
                {
                    rtnChk = false;
                }
            }
            return rtnChk;
        }

        /// <summary>
        /// 判斷是否已經登入，每次的APP核銷、統計查詢動作皆要經此判斷
        /// </summary>
        /// <param name="strAcct"></param>
        /// <param name="strPw"></param>
        /// <param name="strImei"></param>
        /// <param name="strImsi"></param>
        /// <param name="strLoginTime"></param>
        /// <returns></returns>
        public VerificationAccountStatusDetail ChkIsLogin(string strAcct, string strPw, string strImei, string strImsi,
                                                          string strRegCode, string strLoginTime)
        {
            VerificationAccountStatusDetail loginStatus = VerificationAccountStatusDetail.Logined;
            if (!ChkLoginInfo(strAcct, strPw, strImei, strImsi))
            {
                loginStatus = VerificationAccountStatusDetail.ReLoginError;
            }
            else
            {
                if (_vp.HaveLegalAcctInfoByAppInfo(strAcct, strPw, strImei, strImsi, strRegCode))
                {
                    if (DateTime.Compare(DateTime.Today, DateTime.Parse(DateTime.Parse(strLoginTime).ToString("yyyy/MM/dd"))) != 0)
                    {
                        loginStatus = VerificationAccountStatusDetail.LoginTimeout;
                    }
                }
                else
                {
                    loginStatus = VerificationAccountStatusDetail.ReLoginError;
                }
            }
            return loginStatus;
        }

        /// <summary>
        /// 這是給特殊帳號用的登入判斷
        /// </summary>
        /// <param name="strAcct"></param>
        /// <param name="strPw"></param>
        /// <returns></returns>
        public VerificationAccountStatusDetail ChkIsLogin(string strAcct, string strPw)
        {
            VerificationAccountStatusDetail loginStatus = VerificationAccountStatusDetail.Logined;
            StoreAcctSpecialInfo sasi = _vp.GetStoreAcctSepcialInfoByAcct(strAcct, strPw);
            if (!(sasi.Acct == strAcct && sasi.AcctPw == strPw && sasi.Status != 0))
            {
                loginStatus = VerificationAccountStatusDetail.ReLoginError;
            }
            return loginStatus;
        }

        #endregion Login/LoginOut

        #region 核銷

        public ViewPponCoupon GetCouponInfo(string strSequence, string strCode, string strSellerGuid,
                                            string strStoreGuid)
        {
            ViewPponCouponCollection vpcc = new ViewPponCouponCollection();
            if (string.IsNullOrEmpty(strStoreGuid))
            {
                vpcc = _vp.GetCouponInfoBySNoAndCodeAndSeller(strSequence, strCode, new Guid(strSellerGuid));
            }
            else
            {
                vpcc = _vp.GetCouponInfoBySNoAndCodeAndSellerAndStoreGuid(strSequence, strCode, new Guid(strSellerGuid),
                                                                          new Guid(strStoreGuid));
            }
            return (vpcc.Count > 0) ? vpcc[0] : null;
        }

        public Dictionary<string, string> GetCashTrustLogStatus(ViewPponCoupon vpc, string strImei, string strImsi)
        {
            Dictionary<string, string> dicStatus = new Dictionary<string, string>();

            CashTrustLog ctl = _mp.CashTrustLogGetByCouponId((int)vpc.CouponId);
            DateTime time = ctl.ModifyTime;
            VerificationStatus flag = VerificationStatus.CouponInit;

            bool isDealLock = (_pp.DealAccountingGet(vpc.BusinessHourGuid).Flag & (int)AccountingFlag.VerificationLocked) > 0;

            if (ctl.TrustId == Guid.Empty)
            {
                flag = VerificationStatus.CouponError;
            }
            else if (ctl.Status == (int)TrustStatus.Verified)
            {
                flag = VerificationStatus.CouponUsed; //已使用過
            }
            else if (ctl.Status > (int)TrustStatus.Verified)
            {
                flag = VerificationStatus.CouponReted; //已退貨
            }
            else if (isDealLock)
            {
                flag = VerificationStatus.CouponOver;//超過使用期限
            }

            //增加一筆憑證欲被核銷時狀態，VerificationLog的記錄
            SetVerificationLog(vpc.SellerGuid, vpc.CouponId, vpc.SequenceNumber, vpc.CouponCode, (int)flag, false,
                               DateTime.Now, "", "IMEI:" + strImei + "& IMSI: " + strImsi);
            dicStatus.Add("flag", ((int)flag).ToString(CultureInfo.InvariantCulture));
            dicStatus.Add("time", string.Format("{0:yyyy/MM/dd HH:mm:ss}", time));
            return dicStatus;
        }

        public ViewHiDealVerificationInfo GetHiDealCouponInfo(string strSequence, string strCode, string strSellerGuid,
                                            string strStoreGuid)
        {
            ViewHiDealVerificationInfoCollection vhdcc = new ViewHiDealVerificationInfoCollection();
            if (string.IsNullOrEmpty(strStoreGuid))
            {
                vhdcc = _vp.GetHiDealCouponInfoBySNoAndCodeAndSeller(strSequence, strCode, new Guid(strSellerGuid));
            }
            else
            {
                vhdcc = _vp.GetHiDealCouponInfoBySNoAndCodeAndSellerAndStoreGuid(strSequence, strCode, new Guid(strSellerGuid),
                                                                          new Guid(strStoreGuid));
            }
            return (vhdcc.Count > 0) ? vhdcc[0] : null;
        }

        #region 核銷log兩步驟

        public VerificationStatus ActVerify(string strSequence, string strCode, string strSellerGuid,
                                            string strStoreGuid, string strImei, string strImsi, string createId)
        {
            //取得證憑資訊 vpc:17P; vhvi:Piinlife
            ViewPponCoupon vpc = GetCouponInfo(strSequence, strCode, strSellerGuid, strStoreGuid);
            ViewHiDealVerificationInfo vhvi = GetHiDealCouponInfo(strSequence, strCode, strSellerGuid, strStoreGuid);

            CashTrustLog ctl;
            var isHideal = false;

            //取出該筆憑證的 cash_trust_log
            if (vhvi != null)
            {
                ctl = _mp.CashTrustLogGetByCouponId((int)vhvi.Cid, OrderClassification.HiDeal);
                isHideal = true;
            }
            else
            {
                ctl = _mp.CashTrustLogGetByCouponId((int)vpc.CouponId);
            }
            //執行核銷動作
            return VerifyLog(ctl, new Guid(strSellerGuid), strCode, isHideal, "" /*ip*/, createId, "IMEI:" + strImei + "& IMSI: " + strImsi);
        }

        /// <summary>
        /// 執行核銷並記錄，請注意！這裡是確定要核銷走的函式，一定會將核銷狀態改成"完成核銷"
        /// </summary>
        /// <param name="objCtl">Cash Trust Log</param>
        /// <param name="sellerGuid">店家的 Guid</param>
        /// <param name="sCouponCode">憑證驗證碼</param>
        /// <param name="isPiinLifeDeal">是否為品生活檔次</param>
        /// <param name="sIp">IP<br />若是從 Pad 過來，請傳入 IMEI: "機碼" & IMSI: SIM卡碼 </param>
        /// <param name="createId"> 操作者的userId </param>
        /// <param name="sDescription">備註，已預設為空值</param>
        /// <returns>VerificationStatus</returns>
        public VerificationStatus VerifyLog(
                CashTrustLog objCtl, Guid? sellerGuid, string sCouponCode, bool isPiinLifeDeal,
                string sIp, string createId, string sDescription = "")
        {
            //試圖把2個核銷流程整回成1個
            return OrderFacade.VerifyCoupon(objCtl.TrustId, false, createId, sIp, sDescription);
        }

        /// <summary>
        /// 寫入Cash Trust Log 以及 Cash Trust Status Log
        /// </summary>
        /// <param name="objCtl"></param>
        /// <param name="dtNow">執行時間</param>
        /// <returns></returns>
        [Obsolete("因應核銷流程2合1，這個method標註不建議用")]
        private VerificationStatus SetTrustLog(CashTrustLog objCtl, DateTime dtNow, string createId)
        {
            var cashTrustStatusLog = new CashTrustStatusLog
                                            {
                                                TrustId = objCtl.TrustId,
                                                TrustProvider = objCtl.TrustProvider,
                                                Amount = objCtl.Scash + objCtl.CreditCard,
                                                Status = (int)TrustStatus.Verified,
                                                ModifyTime = dtNow,
                                                CreateId = createId
                                            };

            VerificationStatus retFlag;
            if (!_mp.CashTrustLogSet(objCtl))
            {
                retFlag = VerificationStatus.CashTrustLogErr;
            }
            else
            {
                if (!_mp.CashTrustStatusLogSet(cashTrustStatusLog))
                {
                    retFlag = VerificationStatus.CashTrustStatusLogErr;
                }
                else
                {
                    retFlag = VerificationStatus.CouponOk;
                    if (objCtl.CouponId != null)
                    {
                        EinvoiceFacade.SetEinvoiceCouponVerifiedTime(
                            objCtl.CouponId.Value, (OrderClassification)objCtl.OrderClassification, dtNow);
                    }
                    else
                    {
                        log.Warn("核銷動作-開立發票(VerifyAccount.SetTrustLog)：此筆記錄無 CouponId, Trsut Id" + objCtl.TrustId);
                    }
                }
            }
            return retFlag;
        }

        /// <summary>
        /// 將店家執行的核銷動作存入log
        /// </summary>
        /// <param name="gSGuid">店家的guid</param>
        /// <param name="couponId">CouponId</param>
        /// <param name="sSequenceNumber">憑證序號</param>
        /// <param name="sCode">憑證的驗證碼</param>
        /// <param name="iStatus">核銷狀態</param>
        /// <param name="ip">IP<br />若是從 Pad 過來，請傳入 IMEI: "機碼" & IMSI: SIM卡碼  </param>
        /// <param name="sDescription">備註，已預設為空值</param>
        /// <param name="isPiinLifeDeal">是否為品生活檔次</param>
        /// <param name="verifiedTime"> </param>
        /// <returns></returns>
        public VerificationLog SetVerificationLog(
            Guid? gSGuid, int? couponId, string sSequenceNumber, string sCode, int iStatus, bool isPiinLifeDeal,
            DateTime verifiedTime, string ip, string sDescription = "")
        {
            var vLog = new VerificationLog
                           {
                               SellerGuid = gSGuid,
                               CouponId = couponId,
                               SequenceNumber = sSequenceNumber,
                               CouponCode = sCode,
                               Status = iStatus,
                               Description = sDescription,
                               CreateTime = verifiedTime,
                               Ip = ip,
                               IsHideal = isPiinLifeDeal
                           };
            return vLog;
        }

        #endregion 核銷log兩步驟

        #endregion 核銷

        #region 統計

        public List<Dictionary<string, string>> GetStores(string strAcct, string strPw, string strImei, string strImsi,
                                                          string strLoginTime, string strSellerGuid)
        {
            List<Dictionary<string, string>> lDicStores = new List<Dictionary<string, string>>();
            Seller seller = _sp.SellerGet(new Guid(strSellerGuid));

            Dictionary<string, string> dicStore = new Dictionary<string, string>();
            dicStore.Add("deal_seller_name", seller.SellerName + " - 總店");
            dicStore.Add("deal_store_name", "");
            dicStore.Add("deal_seller_guid", seller.Guid.ToString());
            dicStore.Add("deal_store_guid", "");
            lDicStores.Add(dicStore);

            StoreCollection stores = new StoreCollection();
            stores = _sp.StoreGetListBySellerGuid(new Guid(strSellerGuid));
            foreach (Store store in stores)
            {
                ViewPponStoreCollection sInfo = _pp.ViewPponStoreGetStoreGuidWithSales(store.Guid);
                ViewHiDealProductSaleInfoCollection hdsInfo =
                    _hdp.GetViewHiDealProductSaleInfoBySellerGuidOnStart(store.Guid);
                if (sInfo.Count > 0 || hdsInfo.Count > 0)
                {
                    dicStore = new Dictionary<string, string>();
                    dicStore.Add("deal_seller_name", seller.SellerName);
                    dicStore.Add("deal_store_name", store.StoreName);
                    dicStore.Add("deal_seller_guid", store.SellerGuid.ToString());
                    dicStore.Add("deal_store_guid", store.Guid.ToString());
                    lDicStores.Add(dicStore);
                }
            }

            return lDicStores;
        }

        public List<Dictionary<string, string>> GetDeals(string strAcct, string strPw, string strImei, string strImsi
                                                         , string strLoginTime, string strSellerGuid,
                                                         string strStoreGuid)
        {
            List<Dictionary<string, string>> lDicDeals = new List<Dictionary<string, string>>();

            ViewPponDealCollection vpdc = new ViewPponDealCollection();
            ViewSellerStoreBidMenuCollection vssbmc = new ViewSellerStoreBidMenuCollection();
            if (string.IsNullOrEmpty(strStoreGuid))
            {
                vssbmc = _vp.GetDealsBySeller(new Guid(strSellerGuid));
            }
            else
            {
                vssbmc = _vp.GetDealsBySellerAndStoreGuid(new Guid(strSellerGuid), new Guid(strStoreGuid));
            }
            //判斷關檔
            vpdc = _pp.ViewPponDealGetBySellerGuidWithDealDone(new Guid(strSellerGuid));
            if (vssbmc.Count == 0 && vpdc.Count == 0)
            {
                lDicDeals = null;
            }
            else
            {
                foreach (ViewSellerStoreBidMenu vssbm in vssbmc)
                {
                    ViewPponDeal vpd =
                        vpdc.Where(x => x.BusinessHourGuid == vssbm.BusinessHourGuid)
                            .DefaultIfEmpty(new ViewPponDeal()).First();
                    Dictionary<string, string> dicDeal = new Dictionary<string, string>();
                    if (!vpd.IsNew)
                    {
                        ViewPponStore store = (vssbm.StoreGuid == null)
                                                  ? new ViewPponStore()
                                                  : _pp.ViewPponStoreGetByBidAndStoreGuid(vpd.BusinessHourGuid,
                                                                                          vssbm.StoreGuid.
                                                                                              GetValueOrDefault(
                                                                                                  Guid.Empty));
                        CashTrustLogCollection ctlc = (store.IsNew)
                                                          ? _vp.GetCashTrustLogGetListByBidAndSeller(
                                                              vssbm.BusinessHourGuid.GetValueOrDefault(new Guid()),
                                                              vssbm.SellerGuid)
                                                          : _vp.GetCashTrustLogGetListByBidAndSellerAndStore(
                                                              vssbm.BusinessHourGuid.GetValueOrDefault(new Guid()),
                                                              vssbm.SellerGuid,
                                                              vssbm.StoreGuid.GetValueOrDefault(Guid.Empty));
                        int vNo = 0;
                        int uNo = 0;
                        int aNo = 0;
                        foreach (CashTrustLog ctl in ctlc)
                        {
                            aNo++;
                            //核銷數需再加上強制退貨數
                            if (ctl.Status == 2 || ((ctl.SpecialStatus & (int)TrustSpecialStatus.ReturnForced) > 0))
                            {
                                vNo++;
                            }
                            else
                            {
                                uNo++;
                            }
                        }

                        dicDeal.Add("deal_bid", vpd.BusinessHourGuid.ToString());
                        dicDeal.Add("deal_pid", string.Empty);
                        dicDeal.Add("deal_item_name", string.IsNullOrEmpty(vpd.ItemName) ? vpd.EventName : vpd.ItemName);

                        dicDeal.Add("deal_order_deliver_time_s",
                                    string.Format("{0:yyyy/MM/dd}", vpd.BusinessHourDeliverTimeS));
                        dicDeal.Add("deal_order_deliver_time_e",
                                    string.Format("{0:yyyy/MM/dd}", vpd.BusinessHourDeliverTimeE));
                        dicDeal.Add("deal_a_quantity", (store.IsNew)
                                                           ? (string.IsNullOrEmpty(vpd.Slug)
                                                                  ? (vNo + uNo).ToString()
                                                                  : vpd.Slug)
                                                           : aNo.ToString());
                        dicDeal.Add("deal_is_deliver_time",
                                    GetIsDeliverTime(vpd.BusinessHourDeliverTimeS.Value,
                                                     vpd.BusinessHourDeliverTimeE.Value,
                                                     int.Parse(dicDeal["deal_a_quantity"]), vNo));
                        dicDeal.Add("deal_v_quantity", vNo.ToString());
                        dicDeal.Add("deal_uv_quantity", uNo.ToString());
                        dicDeal.Add("deal_persent", ((double)vNo / (vNo + uNo)).ToString("0.00%"));
                        dicDeal.Add("deal_seller_name", vpd.SellerName);
                        dicDeal.Add("deal_store_name", (store.IsNew) ? "" : store.StoreName);
                        lDicDeals.Add(dicDeal);
                    }
                }
            }
            return lDicDeals;
        }

        public List<Dictionary<string, string>> GetHiDeals(string strAcct, string strPw, string strImei, string strImsi
                                                        , string strLoginTime, string strSellerGuid,
                                                        string strStoreGuid)
        {
            List<Dictionary<string, string>> lDicDeals = new List<Dictionary<string, string>>();
            ViewHiDealVerificationInfoCollection vhvic = new ViewHiDealVerificationInfoCollection();
            ViewHiDealProductSaleInfoCollection vhpsic = new ViewHiDealProductSaleInfoCollection();
            //取出所有可核銷之資料
            if (string.IsNullOrEmpty(strStoreGuid))
            {
                vhvic = _vp.GetHiDealsBySeller(new Guid(strSellerGuid));
            }
            else
            {
                vhvic = _vp.GetHiDealsBySellerAndStoreGuid(new Guid(strSellerGuid), new Guid(strStoreGuid));
            }

            //取出所有該帳號之檔次資料
            if (string.IsNullOrEmpty(strStoreGuid))
            {
                vhpsic = _hdp.GetViewHiDealProductSaleInfoBySellerGuidOnStart(new Guid(strSellerGuid));
            }
            else
            {
                vhpsic = _hdp.GetViewHiDealProductSaleInfoBySellerAndStoreGuidOnStart(new Guid(strSellerGuid), new Guid(strStoreGuid));
            }

            if (vhvic.Count == 0 && vhpsic.Count == 0)
            {
                lDicDeals = null;
            }
            else
            {
                foreach (ViewHiDealProductSaleInfo vhpsi in vhpsic)
                {
                    int vNo = 0;
                    int uNo = 0;
                    int aNo = 0;
                    Dictionary<string, string> dicDeal = new Dictionary<string, string>();
                    foreach (ViewHiDealVerificationInfo vhvi in vhvic)
                    {
                        if (vhvi.DealId == vhpsi.DealId && vhvi.ProductId == vhpsi.ProductId)
                        {
                            aNo++;
                            //核銷數需再加上強制退貨數
                            if (vhvi.Status == 2 || ((vhvi.SpecialStatus & (int)TrustSpecialStatus.ReturnForced) > 0))
                            {
                                vNo++;
                            }
                            else
                            {
                                uNo++;
                            }
                        }
                    }
                    dicDeal.Add("deal_bid", vhpsi.DealId.ToString());
                    dicDeal.Add("deal_pid", vhpsi.ProductId.ToString());
                    dicDeal.Add("deal_item_name", "[品生活]" + vhpsi.ProductName);

                    dicDeal.Add("deal_order_deliver_time_s",
                                string.Format("{0:yyyy/MM/dd}", vhpsi.UseStartTime));
                    dicDeal.Add("deal_order_deliver_time_e",
                                string.Format("{0:yyyy/MM/dd}", vhpsi.UseEndTime));
                    dicDeal.Add("deal_a_quantity", vhpsi.Q.ToString());
                    dicDeal.Add("deal_is_deliver_time",
                                GetIsDeliverTime(vhpsi.UseStartTime.Value,
                                                 vhpsi.UseEndTime.Value,
                                                 int.Parse(dicDeal["deal_a_quantity"]), vNo));
                    dicDeal.Add("deal_v_quantity", vNo.ToString());
                    dicDeal.Add("deal_uv_quantity", uNo.ToString());
                    dicDeal.Add("deal_persent", ((double)vNo / (vNo + uNo)).ToString("0.00%"));
                    dicDeal.Add("deal_seller_name", vhpsi.SellerName);
                    dicDeal.Add("deal_store_name", (vhpsi.IsNew) ? "" : vhpsi.StoreName);
                    lDicDeals.Add(dicDeal);
                }
            }
            return lDicDeals;
        }

        #endregion 統計

        #region 註冊相關動作

        #region 特殊帳號登入

        public Dictionary<string, string> ActRegLogin(string strAcct, string strPw)
        {
            Dictionary<string, string> dicInfo = new Dictionary<string, string>();
            dicInfo.Add("acct", strAcct);
            dicInfo.Add("status", ((int)VerificationAccountStatusDetail.LoginError).ToString());
            dicInfo.Add("name", string.Empty);
            dicInfo.Add("email", string.Empty);
            StoreAcctSpecialInfo sasi = _vp.GetStoreAcctSepcialInfoByAcct(strAcct, strPw);
            if (sasi.Acct.Equals(strAcct) && sasi.AcctPw.Equals(strPw) && sasi.Status != 0)
            {
                dicInfo["acct"] = sasi.Acct;
                dicInfo["status"] = ((int)VerificationAccountStatusDetail.Login).ToString();
                dicInfo["name"] = sasi.AcctName;
                dicInfo["email"] = sasi.AcctEmail;
            }
            else if (sasi.Status == 0)
            {
                dicInfo["acct"] = sasi.Acct;
                dicInfo["status"] = ((int)VerificationAccountStatusDetail.UnRegist).ToString();
            }
            return dicInfo;
        }

        #endregion 特殊帳號登入

        #region 註冊

        public Dictionary<string, string> ActRegist(string strAcct, string strImei, string strImsi, string strRegCode, string modifier, string now)
        {
            StoreAcctInfo sai = _vp.GetAcctInfoByAcct(strAcct, strRegCode);
            var status = (int)VerificationAccountStatusDetail.RegistError;
            var dicInfo = new Dictionary<string, string>
                              {
                                  {
                                      "status",
                                      ((int) VerificationAccountStatusDetail.RegistError).ToString(
                                          CultureInfo.InvariantCulture)
                                      }
                              };

            if (sai.Id != 0) //確定有此帳號資料
            {
                int codeIndex = ChkCodeExistence(strImsi, strImei, sai);
                sai = setCode(strImsi, strImei, sai, 0, codeIndex);
                if (_vp.RegistAcctInfo(sai))
                {
                    status = (int)VerificationAccountStatusDetail.RegistOk;
                }
                dicInfo["status"] = status.ToString(CultureInfo.InvariantCulture);
            }

            SetStoreAcctInfoLog(sai, status, modifier, now);
            return dicInfo;
        }

        #endregion 註冊

        #region 重新註冊

        public Dictionary<string, string> ActResetRegist(string strAcct, string strImei, string strImsi,
                                                         string strOldRegCode, string strNewRegCode, string modifier, string now)
        {
            int status = (int)VerificationAccountStatusDetail.ResetRegistError; //status = 0, 未註冊
            Dictionary<string, string> dicInfo = new Dictionary<string, string>();
            dicInfo.Add("status", ((int)VerificationAccountStatusDetail.UndoRegistError).ToString());

            StoreAcctInfo sai = _vp.GetAcctInfoByAcctWithReRegist(strAcct, strOldRegCode, strNewRegCode, strImei,
                                                                  strImsi);
            try
            {
                if (sai.Id != 0 && sai.Status == (int)VerificationAccountStatusDetail.Regist)
                {
                    //未註冊即不需重設
                    //取得新的註冊碼
                    string newRegistCode = GenRegisterCode(sai.Id.ToString(), _regCodeLength);
                    if (_vp.ResetRegistAcctInfo(sai, newRegistCode))
                    {
                        status = (int)VerificationAccountStatusDetail.ResetRegistOk;
                    }
                    dicInfo["status"] = status.ToString();
                }
                else if (sai.Id != 0 && sai.Status == (int)VerificationAccountStatusDetail.Invalid)
                {
                    status = (int)VerificationAccountStatusDetail.UnRegist;
                }
            }
            finally
            {
                SetStoreAcctInfoLog(sai, status, modifier, now);
            }

            return dicInfo;
        }

        #endregion 重新註冊

        #region 取消註冊

        public Dictionary<string, string> ActUndoRegist(string strAcct, string strImei, string strImsi,
                                                        string strRegCode, string modifier, string now)
        {
            int status = (int)VerificationAccountStatusDetail.UndoRegistError; //status = 0, 未註冊
            Dictionary<string, string> dicInfo = new Dictionary<string, string>();
            dicInfo.Add("status", ((int)VerificationAccountStatusDetail.UndoRegistError).ToString());

            StoreAcctInfo sai = _vp.GetAcctInfoByAcctWithUndoRegist(strAcct, strRegCode, strImei, strImsi);
            try
            {
                if (!(sai.Id == 0 && sai.Status == null))
                {
                    int codeIndex = ChkCodeExistence(strImsi, strImei, sai);
                    if (codeIndex > -1)
                    {
                        sai = setCode(strImsi, strImei, sai, 1, codeIndex);

                        string RegCode1 = (codeIndex > 0)
                                              ? sai.RegisterCode
                                              : GenRegisterCode(sai.Id.ToString(), _regCodeLength);
                        string RegCode2 = (codeIndex > 0)
                                              ? sai.NewRegisterCode
                                              : GenRegisterCode(sai.Id.ToString(), _regCodeLength);
                        //未註冊即不需重設
                        if (_vp.UndoRegistAcctInfo(sai, RegCode1, RegCode2))
                        {
                            status = (int)VerificationAccountStatusDetail.UndoRegistOk;
                        }
                        dicInfo["status"] = status.ToString();
                    }
                    else
                    {
                        status = (int)VerificationAccountStatusDetail.UnRegist;
                    }
                }
            }
            finally
            {
                SetStoreAcctInfoLog(sai, status, modifier, now);
            }

            return dicInfo;
        }

        #endregion 取消註冊

        #endregion 註冊相關動作

        #region Method

        /// <summary>
        /// 取得檔次兌換狀態
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="aNo"></param>
        /// <param name="vNo"></param>
        /// <returns></returns>
        private string GetIsDeliverTime(DateTime dtStart, DateTime dtEnd, int aNo, int vNo)
        {
            string rtnString = "兌換中";
            if (DateTime.Compare(dtStart, DateTime.Today) > 0)
            {
                rtnString = "尚未開始";
            }
            else if (DateTime.Compare(DateTime.Today, dtEnd) > 0)
            {
                rtnString = "已結束";
            }
            else if (vNo >= aNo)
            {
                rtnString = "兌換完畢";
            }
            return rtnString;
        }

        public VerificationStatus GetVerifyState(TrustStatus status)
        {
            VerificationStatus vs = VerificationStatus.CouponError;
            switch (status)
            {
                case TrustStatus.Initial:
                case TrustStatus.Trusted:
                case TrustStatus.ATM:
                    vs = VerificationStatus.CouponInit;
                    break;

                case TrustStatus.Verified:
                    vs = VerificationStatus.CouponUsed;
                    break;

                case TrustStatus.Returned:
                case TrustStatus.Refunded:
                    vs = VerificationStatus.CouponReted;
                    break;
            }
            return vs;
        }

        private StoreAcctInfo setCode(string strImsi, string strImei, StoreAcctInfo oSai, int iAct, int iCodeIndex)
        {
            StoreAcctInfo sai = oSai;
            List<string> imeiCodes = (oSai.ImeiCode != null) ? oSai.ImeiCode.Split(",").ToList<string>() : new List<string>();
            List<string> imsiCodes = (oSai.SimCode != null) ? oSai.SimCode.Split(",").ToList<string>() : new List<string>();
            switch (iAct)
            {
                case 0: //Add
                    if (imeiCodes.Count > 0)
                    {
                        sai.ImeiCode += "," + strImei;
                        sai.SimCode += "," + strImsi;
                    }
                    else
                    {
                        sai.ImeiCode += strImei;
                        sai.SimCode += strImsi;
                    }
                    break;

                case 1: //Remove
                    if (iCodeIndex > -1)
                    {
                        imeiCodes.RemoveAt(iCodeIndex);
                        imsiCodes.RemoveAt(iCodeIndex);
                        sai.ImeiCode = string.Join(",", imeiCodes);
                        sai.SimCode = string.Join(",", imsiCodes);
                    }
                    break;
            }
            return sai;
        }

        private int ChkCodeExistence(string strImsi, string strImei, StoreAcctInfo oSai)
        {
            int codeIndex = 0;
            List<string> imeiCodes = null;
            List<string> imsiCodes = null;
            if (oSai.ImeiCode == null)
            {
                codeIndex = -2;
            }
            else if (oSai.ImeiCode.IndexOf(",", System.StringComparison.Ordinal) >= 0)
            {
                codeIndex = -1;
                imeiCodes = oSai.ImeiCode.Split(",").ToList<string>();
                imsiCodes = oSai.SimCode.Split(",").ToList<string>();
            }

            if (codeIndex == -1 && imeiCodes != null)
            {
                for (int i = 0; i < imeiCodes.Count; i++)
                {
                    if (imeiCodes[i] == strImei && imsiCodes[i] == strImsi)
                    {
                        codeIndex = i;
                        break;
                    }
                }
            }
            return codeIndex;
        }

        #endregion Method

        #region Log

        private void SetStoreAcctInfoLog(StoreAcctInfo sai, int status, string modifier, string now)
        {
            StoreAcctInfoLog sail = new StoreAcctInfoLog();
            DateTime dtNow = DateTime.ParseExact(now, "yyyy/MM/dd HH:mm:ss", null);

            if (status == (int)VerificationAccountStatusDetail.RegistOk)
            {   //若是"註冊"皆視為 Create
                sail.CreateId = modifier;
                sail.CreateTime = dtNow;
            }
            else
            {   //其餘如"重新註冊"、"取消註冊"皆視為 Modify
                sail.ModifyId = modifier;
                sail.ModifyTime = dtNow;
            }

            try
            {
                sail.StoreAcctInfoId = sai.Id;
                sail.Status = status;
            }
            catch
            {   //只要有錯誤，皆會將 StoreAcctInfoId 記入 0，一定會將之視為 Create
                sail.StoreAcctInfoId = 0;
                sail.CreateId = modifier;
                sail.CreateTime = dtNow;
                sail.ModifyId = modifier;
                sail.ModifyTime = dtNow;
                sail.Status = status;
            }
            finally
            {
                _vp.SetStoreAcctInfoLog(sail);
            }
        }

        #endregion Log

        #region WPF AP

        public StoreAcctInfoCollection GetAcctInfoByAcctForWPF(string acctcode, string acctno, string password)
        {
            return _vp.GetAcctInfoByAcctForWPF(acctcode, acctno, password);
        }

        public ViewPponDealCollection GetDealsBySellerGuidForWPF(Guid sellerguid)
        {
            return _pp.ViewPponDealGetListOnShowBySellerGuid(sellerguid);
        }

        public ViewCouponStoreCollection ViewCouponStoreCollectionGet(Guid bid, Guid seller_guid, Guid store_guid)
        {
            return _sp.ViewCouponStoreCollectionGet(bid, seller_guid, store_guid);
        }

        public ViewCouponStoreCollection ViewCouponStoreCollectionGet(Guid bid, Guid seller_guid, Guid store_guid, string sequencenumber, string code)
        {
            return _sp.ViewCouponStoreCollectionGet(bid, seller_guid, store_guid, sequencenumber, code);
        }

        public ViewCouponStoreCollection ViewCouponStoreCollectionGet(Guid seller_guid, Guid store_guid, string sequencenumber, string code)
        {
            return _sp.ViewCouponStoreCollectionGet(seller_guid, store_guid, sequencenumber, code);
        }

        public void VerifyCouponByWpf(Guid trust_id, VerificationLog log)
        {
            _mp.CashTrustLogToUpdateFromWPF(trust_id);
            _mp.VerificationLogSet(log);
            OrderClassification orderClassification = log.IsHideal ? OrderClassification.HiDeal : OrderClassification.LkSite;
            EinvoiceFacade.SetEinvoiceCouponVerifiedTime(log.CouponId.Value, orderClassification, log.CreateTime.Value);
        }

        public void LogByWpf(VerificationLog log)
        {
            _mp.VerificationLogSet(log);
        }

        public bool GetAcctSpecialInfoForWPF(string acctcode, string password)
        {
            return _vp.GetStoreAcctSepcialInfoByAcct(acctcode, password).Id != 0;
        }

        #endregion WPF AP

        public MohistVerifyInfo GetMohistVerifyInfo(string mohistDealsId, string sequenceNumber, string couponCode)
        {
            return _op.MohistVerifyInfoGet(mohistDealsId, sequenceNumber, couponCode);
        }
    }
}
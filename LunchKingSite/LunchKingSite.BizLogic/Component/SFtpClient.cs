﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using AlexPilotti.FTPS.Client;
using AlexPilotti.FTPS.Common;
using log4net;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    /// <summary>
    /// FTP over SSL
    /// http://ftps.codeplex.com/
    /// </summary>
    public class SFtpClient
    {
        private static ILog logger = LogManager.GetLogger(typeof(SFtpClient));
        public class FtpConfig
        {
            public string UserId { get; set; }
            public string Password { get; set; }
            public string LocalPath { get; set; }
            public string Host { get; set; }
        }

        public FtpConfig Config { get; set; }

        public SFtpClient(Uri uri)
        {
            Config = new FtpConfig
            {
                Host = uri.DnsSafeHost
            };
            if (string.IsNullOrEmpty(uri.UserInfo) == false)
            {
                if (uri.UserInfo.Contains(':'))
                {
                    Config.UserId = uri.UserInfo.Split(':')[0];
                    Config.Password = uri.UserInfo.Split(':')[1];
                }
                else
                {
                    Config.UserId = uri.UserInfo;
                }
            }
        }

        public string[] ListSimple(string remoteDirName, string matchExt)
        {
            FTPSClient cli = new FTPSClient();

            try
            {
                cli.Connect(Config.Host, new NetworkCredential(Config.UserId, Config.Password),
                    ESSLSupportMode.Implicit, delegate(
                    object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    });

                var items = cli.GetDirectoryList(remoteDirName).AsQueryable();                
                if (string.IsNullOrEmpty(matchExt) == false)
                {
                    items = items.Where(t => Path.GetExtension(t.Name) == matchExt);
                }
                return items.Select(t => t.Name).ToArray();
            }
            catch (Exception ex)
            {
                logger.Warn("ListSimple Error, remoteDirName=" + remoteDirName, ex);
                throw;
            }
            finally
            {
                try
                {
                    cli.Close();
                } catch {}
            }
        }

        public void Download(string fileName, string localName)
        {
            FTPSClient cli = new FTPSClient();

            try
            {
                cli.Connect(Config.Host, new NetworkCredential(Config.UserId, Config.Password),
                    ESSLSupportMode.Implicit, delegate(
                    object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    });

                string localFilePath = Path.Combine(Config.LocalPath, localName);
                cli.GetFile(fileName, localFilePath);
            }
            catch (Exception ex)
            {
                logger.Warn("Download Error, fileName=" + fileName, ex);
                throw;
            }
            finally
            {
                try
                {
                    cli.Close();
                } catch {}
            }
        }

        

        

        public void Upload(string fileName, string dirName)
        {
            FTPSClient cli = new FTPSClient();
            try
            {
                cli.Connect(Config.Host, new NetworkCredential(Config.UserId, Config.Password),
                    ESSLSupportMode.Implicit, delegate(
                    object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    });
                string localFilePath = Path.Combine(Config.LocalPath, fileName);
                cli.PutFile(localFilePath, GetFolderFileName(fileName, dirName));
            }
            catch (Exception ex)
            {
                logger.Warn("Upload Error, fileName=" + fileName, ex);
                throw;
            }
            finally
            {
                try
                {
                    cli.Close();
                } catch { }
            }
        }

        

        

        public void MakeDirectory(string folder)
        {
            FtpWebRequest ftpRequest = null;
            try
            {
                ftpRequest = (FtpWebRequest)WebRequest.Create("ftp://" + Config.Host + "/" + folder);
                ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                ftpRequest.Credentials = new NetworkCredential(Config.UserId, Config.Password);
                using (var resp = (FtpWebResponse)ftpRequest.GetResponse())
                {
                    Console.WriteLine(resp.StatusCode);
                }
            }
            catch(WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    response.Close();
                }
                else
                {
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Warn("MakeDirectory Error, folder=" + folder, ex);
                //throw;
            }
            finally
            {
                try
                {
                    ftpRequest = null;
                }
                catch { }
            }
        }

        
        public void Upload(string fileName)
        {
            Upload(fileName, ""); 
        }

        /// <summary>
        /// 回傳 60859/60859.CAPTURE.20130313
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dirName"></param>
        /// <returns></returns>
        public static string GetFolderFileName(string fileName, string dirName)
        {
            if (string.IsNullOrEmpty(dirName))
            {
                return fileName;
            }
            return dirName + "/" + fileName;
        }

        public void SetLocalPath(string path)
        {
            Config.LocalPath = path;
        }

        public bool CheckFileExist(string fileName, string dirName)
        {
            return this.ListSimple(dirName, "").Any(t => t == fileName);
        }
        

        public bool CheckFileExist(string fileName)
        {
            return CheckFileExist(fileName, "");
        }
    }

    public class SkmFtpClient
    {
        public class FtpConfig
        {
            public string UserId { get; set; }
            public string Password { get; set; }
            public string LocalFilePath { get; set; }
            public string Host { get; set; }
            public string RemoteWorkPath { get; set; }
            public string RemoteFinishPath { get; set; }
            public string RemoteFailPath { get; set; }

        }

        public FtpConfig Config { get; set; }

        public void GetFile()
        {
            if (!Directory.Exists(Config.LocalFilePath))
            {
                Directory.CreateDirectory(Config.LocalFilePath);
            }

            using (var client = new FTPSClient())
            {
                client.Connect(Config.Host, new NetworkCredential(Config.UserId, Config.Password), ESSLSupportMode.CredentialsRequested);

                var list = client.GetDirectoryList(Config.RemoteWorkPath);

                foreach (var item in list)
                {
                    client.GetFile(Path.Combine(Config.RemoteWorkPath, item.Name), Path.Combine(Config.LocalFilePath, item.Name));
                }
            }
        }

        public void UploadFinishFile(string fname)
        {
            using (var client = new FTPSClient())
            {
                client.Connect(Config.Host, new NetworkCredential(Config.UserId, Config.Password), ESSLSupportMode.CredentialsRequested);

                client.PutFile(Path.Combine(Config.LocalFilePath, fname), Path.Combine(Config.RemoteFinishPath, fname));
            }
        }

        public void UploadFailFile(string fname)
        {
            using (var client = new FTPSClient())
            {
                client.Connect(Config.Host, new NetworkCredential(Config.UserId, Config.Password), ESSLSupportMode.CredentialsRequested);

                client.PutFile(Path.Combine(Config.LocalFilePath, fname), Path.Combine(Config.RemoteFailPath, fname));
            }
        }

        public void DeleteFile(string fname)
        {
            using (var client = new FTPSClient())
            {
                client.Connect(Config.Host, new NetworkCredential(Config.UserId, Config.Password), ESSLSupportMode.CredentialsRequested);

                client.DeleteFile(Path.Combine(Config.RemoteWorkPath, fname));

            }
        }
    }
}



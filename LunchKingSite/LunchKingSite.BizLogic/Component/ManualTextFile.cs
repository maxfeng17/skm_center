﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Component
{
    public class ManualTextFile
    {
        #region properties

        public StreamReader Reader { get; set; }

        #endregion

        public ManualBatchTransactionResult Load()
        {
            if (this.Reader == null)
            {
                throw new InvalidOperationException("StreamReader not set!");
            }
            
            string everything = Reader.ReadToEnd();
            string[] allLines = everything.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);


            AnsiTextSerializer sw = new AnsiTextSerializer();

            var details = new List<ManualResultTextDetail>();
            for (int i = 0; i < allLines.Length; i++)
            {
                var detail = sw.ReadItem<ManualResultTextDetail>(Encoding.Default.GetBytes(allLines[i]), true);
                details.Add(detail);
            }

            var result = new ManualBatchTransactionResult(details);
            return result;
        }
    }

    public class ManualBatchTransactionResult
    {
        private readonly bool _isCorrupted = false;
        public bool IsCorrupted
        {
            get { return _isCorrupted; }
        }

        private readonly DateTime _batchProcessedTime;
        public DateTime BatchProcessedTime
        {
            get
            {
                if (IsCorrupted)
                {
                    throw new Exception("Data is corrupted!");
                }
                return _batchProcessedTime;
            }
        }

        private readonly List<ManualTransactionResult> trxResults;
        public List<ManualTransactionResult> TransactionResults
        {
            get
            {
                if (IsCorrupted)
                {
                    throw new Exception("Data is corrupted!");
                }
                return trxResults.ToList();
            }
        }

        public ManualBatchTransactionResult(List<ManualResultTextDetail> details)
        {
            _batchProcessedTime = System.DateTime.Now;
            DateTime payDate;
            
            if (!_isCorrupted)
            {
                trxResults = new List<ManualTransactionResult>();
                foreach (ManualResultTextDetail detail in details)
                {
                    trxResults.Add(new ManualTransactionResult(detail));
                    if (!TryGetProcessedTime(detail.TrxPayDate, out payDate))
                    {
                        _isCorrupted = true;
                        break;
                }
            }
        }
    }


        private bool TryGetProcessedTime(string PayDate, out DateTime payDate)
        {
            DateTime outDate;
            bool flag = false;
            payDate = new DateTime(1900, 1, 1);

            if (PayDate.Length >= 8)
            {
                string date = PayDate.Substring(0, 4) + "/" + PayDate.Substring(4, 2) + "/" + PayDate.Substring(6, 2);

                if (DateTime.TryParse(date, out outDate)
                    )
                {
                    flag = true;
                    payDate = outDate;
                }
            }

            return flag;
        }
    }

    public class ManualTransactionResult
    {
        private readonly bool _isCorrupted;
        /// <summary>
        /// 資料是否毀損
        /// </summary>
        public bool IsCorrupted
        {
            get { return _isCorrupted; }
        }

        private readonly int _weeklyPayReportId;
        public int WeeklyPayReportId
        {
            get { return _weeklyPayReportId; }
        }

        private readonly int _dealUniqueId;
        public int DealUniqueId
        {
            get { return _dealUniqueId; }
        }

        private readonly int _amount;
        public int Amount
        {
            get { return _amount; }
        }

        private readonly string _bankNumber;
        /// <summary>
        /// 銀行代號
        /// </summary>
        public string BankNumber
        {
            get { return _bankNumber; }
        }

        private readonly string _branchNumber;
        /// <summary>
        /// 分行代號
        /// </summary>
        public string BranchNumber
        {
            get { return _branchNumber; }
        }

        private readonly string _accountNumber;
        /// <summary>
        /// 收受者帳號
        /// </summary>
        public string AccountNumber
        {
            get { return _accountNumber; }
        }

        private readonly string _companyId;
        /// <summary>
        /// 收受者統一編號/身分證字號
        /// </summary>
        public string CompanyId
        {
            get { return _companyId; }
        }

        public ManualResultType Result { get; private set; }

        public ManualTransactionResult(ManualResultTextDetail detail)
        {
            _bankNumber = detail.ReceiveBankCode;
            _branchNumber = detail.ReceiveBranchCode;
            _accountNumber = detail.ReceiveAccount.PadLeft(14, '0');
            _companyId = detail.ReceiverUniqueId;
            this.Result = new ManualResultType(detail.RejectionCode.Replace("\t", ""));

            int payId;
            int dealId;
            int amount;
            if (int.TryParse(detail.PaymentId.Trim(), out payId)
                && int.TryParse(detail.PaymentUniqueId.Trim(), out dealId)
                && int.TryParse(detail.Amount.Trim(), out amount))
            {
                _weeklyPayReportId = payId;
                _dealUniqueId = dealId;
                _amount = amount;
            }
            else
            {
                _isCorrupted = true;
            }
        }
    }

    public class ManualResultType
    {
        private readonly bool _isSuccess;
        public bool IsSuccess
        {
            get { return _isSuccess; }
        }

        private readonly bool _isSuccessButAmountMismatch;
        public bool IsSuccessButAmountMismatch
        {
            get { return _isSuccessButAmountMismatch; }
        }

        private readonly bool _isNull;
        public bool IsNull
        {
            get { return _isNull; }
        }

        private readonly string _code;
        public string Code
        {
            get { return _code; }
        }

        public string Description
        {
            get
            {
                return GetDescription(this.Code);
            }
        }

        public ManualResultType(string code)
        {
            switch (code)
            {
                case "08":
                    _code = code;
                    _isSuccess = true;
                    _isNull = false;
                    break;
                case "01":
                case "02":
                case "03":
                case "04":
                case "05":
                case "06":
                case "07":
                case "09":
                case "10":
                case "11":
                case "14":
                    _code = code;
                    _isSuccess = false;
                    _isSuccessButAmountMismatch = false;
                    _isNull = false;
                    break;
                case null:
                    _code = "-2";
                    _isSuccess = false;
                    _isSuccessButAmountMismatch = false;
                    _isNull = true;
                    break;
                default:
                    _code = "-1";
                    _isSuccess = false;
                    _isSuccessButAmountMismatch = false;
                    _isNull = false;
                    break;
            }
        }


        private static string GetDescription(string code)
        {
            return descriptionLookup[code];
        }

        private readonly static Dictionary<string, string> descriptionLookup = new Dictionary<string, string>()
                                                                         {
                                                                             {"01", "編輯，資料尚未送審"},
                                                                             {"02", "送審，資料已送審，尚未審核"},
                                                                             {"03", "待審核，資料已審核，但未完成所有的審核"},
                                                                             {"04", "待放行，資料已完成所有的審核，等待放行"},
                                                                             {"05", "退件，資料被審核人員或是放行人員退件"},
                                                                             {"06", "等待回應，資料已放行，FEDI Server尚未處"},
                                                                             {"07", "上傳主機，資料已放行，FEDI Server已驗章完成，等待後續帳務處"},
                                                                             {"08", "扣帳成功"},
                                                                             {"09", "付款失敗"},
                                                                             {"10", "退帳通知，資料扣帳成功，但入帳時被入帳行退回"},
                                                                             {"11", "預約取消，資料原已預約，被用戶取消付款"},
                                                                             {"14", "退帳通知(人工回沖)，財經公司通知，進行入工退帳"}
                                                                         };
    }



    public class ManualResultTextDetail : AnsiTextBase
    {
        /// <summary>
        /// 用戶自訂序號(7)
        /// </summary>
        [Ansi(0, 7)]
        public string TrxSeqNo { get; set; }
        /// <summary>
        /// 付款日期(8)
        /// </summary>
        [Ansi(7, 8)]
        public string TrxPayDate { get; set; }
        /// <summary>
        /// 金額(18)
        /// </summary>
        [Ansi(15, 18)]
        public string Amount { get; set; }
        /// <summary>
        /// 付款帳號(17)
        /// </summary>
        [Ansi(33, 17)]
        public string PayAccount { get; set; }
        /// <summary>
        /// 付款戶名(60)
        /// </summary>
        [Ansi(50, 60)]
        public string PayAccountName { get; set; }
        /// <summary>
        /// 收款帳號(17)
        /// </summary>
        [Ansi(110, 17)]
        public string ReceiveAccount { get; set; }
        /// <summary>
        /// 收款戶名(60)
        /// </summary>
        [Ansi(127, 60)]
        public string ReceiverCompany { get; set; }
        /// <summary>
        /// 付款總行代碼(3)
        /// </summary>
        [Ansi(187, 3)]
        public string PayBankCode { get; set; }
        /// <summary>
        /// 付款分行代碼(4)
        /// </summary>
        [Ansi(190, 4)]
        public string PayBranchCode { get; set; }
        /// <summary>
        /// 收款總行代碼(3)
        /// </summary>
        [Ansi(194, 3)]
        public string ReceiveBankCode { get; set; }
        /// <summary>
        /// 收款分行代碼(4)
        /// </summary>
        [Ansi(197, 4)]
        public string ReceiveBranchCode { get; set; }
        /// <summary>
        /// 附言(100)
        /// </summary>
        [Ansi(201, 100, Align = Align.Left, UnusedChar = UnusedChar.Space)]
        public string Command { get; set; }
        /// <summary>
        /// 收款人識別碼(17) 統一編號/身分證字號
        /// </summary>
        [Ansi(301, 17)]
        public string ReceiverUniqueId { get; set; }
        /// <summary>
        /// 收款人代碼識別(3)53：護照號碼 58：統一編號 174：身分證字號
        /// </summary>
        [Ansi(318, 3)]
        public string ReceiverCodeSymbol { get; set; }
        /// <summary>
        /// 付款人識別碼(17) 統一編號/身分證字號
        /// </summary>
        [Ansi(321, 17)]
        public string PaymentUniqueId { get; set; }
        /// <summary>
        /// 付款人代碼識別(3)13：收款人負擔 15：付款人負擔
        /// </summary>
        [Ansi(338, 3)]
        public string PaymentBurden { get; set; }
        /// <summary>
        /// 手續費負擔別(3)
        /// </summary>
        [Ansi(341, 3)]
        public string Fee { get; set; }
        /// <summary>
        /// 對帳單key值
        /// </summary>
        [Ansi(344, 30)]
        public string PaymentId { get; set; }
        /// <summary>
        /// 付款聯絡人
        /// </summary>
        [Ansi(374, 35)]
        public string PayContactName { get; set; }
        /// <summary>
        /// 付款聯絡電話
        /// </summary>
        [Ansi(409, 25)]
        public string PayContactTel { get; set; }
        /// <summary>
        /// 付款傳真號碼
        /// </summary>
        [Ansi(434, 25)]
        public string PayContactFax { get; set; }
        /// <summary>
        /// 收款聯絡人
        /// </summary>
        [Ansi(459, 35)]
        public string ReceiverContactName { get; set; }
        /// <summary>
        /// 收款聯絡電話
        /// </summary>
        [Ansi(497, 25)]
        public string ReceiverContactTel { get; set; }
        /// <summary>
        /// 收款傳真號碼
        /// </summary>
        [Ansi(519, 25)]
        public string ReceiverContactFax { get; set; }
        /// <summary>
        /// 收款通知E-mail
        /// </summary>
        [Ansi(544, 50)]
        public string ReceiverMail { get; set; }
        /// <summary>
        /// 資料狀態
        /// </summary>
        [Ansi(594, 2)]
        public string RejectionCode { get; set; }
        /// <summary>
        /// 手續費
        /// </summary>
        [Ansi(598, 18)]
        public string Reserved { get; set; }
    }

    
}

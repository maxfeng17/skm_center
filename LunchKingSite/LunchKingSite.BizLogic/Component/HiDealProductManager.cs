﻿using LunchKingSite.BizLogic.Interface;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace LunchKingSite.BizLogic.Component
{
    public class HiDealProductManager
    {
        public const string ManagerNameForJob = "HiDealProductManager";
        private static List<HiDealProductDataModule> _hiDealProductDataModules;
        protected static IHiDealProvider HiDealProvider;
        protected static ISysConfProvider SysConfProvider;
        private static readonly log4net.ILog log;

        static HiDealProductManager()
        {
            HiDealProvider = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            SysConfProvider = ProviderFactory.Instance().GetConfig();
            _hiDealProductDataModules = new List<HiDealProductDataModule>();
            log = log4net.LogManager.GetLogger(typeof(HiDealProductManager));
        }

        /// <summary>
        /// 產生新的HiDealProductDataModule資料將會同步更新
        /// </summary>
        public static void ReloadDefaultManager(bool isSyncWebServer = true)
        {
            var newOne = new List<HiDealProductDataModule>();
            var products = HiDealProvider.HiDealProductGetList(0, 0, HiDealProduct.Columns.Id,
                HiDealProduct.Columns.Id + " is not null");

            foreach (var product in products)
            {
                var module = new HiDealProductDataModule();
                module.Product = product;
                //到府才需要查詢運費
                module.FreightIncome = product.IsHomeDelivery ? HiDealFreightIncomeListGetByPidFromDb(product.Id) : new HiDealFreightCollection();

                newOne.Add(module);
            }
            _hiDealProductDataModules = newOne;
        }

        /// <summary>
        /// 傳入ProductId回傳HiDealProductDataModule物件，包含商品相關的資料
        /// 預設由靜態物件的暫存資料中讀取，若無會查詢DB，所以資料不是最正確的。
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <returns></returns>
        public static HiDealProductDataModule HiDealProductDataModuleGetById(int productId)
        {
            if ((_hiDealProductDataModules != null) && (_hiDealProductDataModules.Count > 0))
            {
                var finds = _hiDealProductDataModules.Where(x => x.Product.Id == productId).ToList();
                if (finds.Count > 0)
                {
                    return finds.First();
                }
            }
            return HiDealProductDataModuleGetByIdFromDb(productId);
        }

        /// <summary>
        /// 依據商品編號取得Product物件
        /// 預設由靜態物件的暫存資料中讀取，若無會查詢DB，所以資料不是最正確的。
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <returns></returns>
        public static HiDealProduct HiDealProductGetById(int productId)
        {
            if ((_hiDealProductDataModules != null) && (_hiDealProductDataModules.Count > 0))
            {
                var finds = _hiDealProductDataModules.Where(x => x.Product.Id == productId).ToList();
                if (finds.Count > 0)
                {
                    return finds.First().Product;
                }
            }
            return HiDealProductGetByIdFromDb(productId);
        }

        /// <summary>
        /// 依據商品編號，取得該商品的運費收入資訊
        /// 預設由靜態物件的暫存資料中讀取，若無會查詢DB，所以資料不是最正確的。
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <returns></returns>
        public static HiDealFreightCollection HiDealFreightIncomeListGetByPid(int productId)
        {
            if ((_hiDealProductDataModules != null) && (_hiDealProductDataModules.Count > 0))
            {
                var finds = _hiDealProductDataModules.Where(x => x.Product.Id == productId).ToList();
                if (finds.Count > 0)
                {
                    return finds.First().FreightIncome;
                }
            }
            return HiDealFreightIncomeListGetByPidFromDb(productId);
        }

        #region ProductCost相關

        /// <summary>
        ///  HiDealProductCost異動且已有產出憑證過的紀錄時，則更新HiDealCoupon 中Cost資訊
        /// </summary>
        /// <param name="hiDealProductId">品生活商品ID</param>
        public static void HiDealProductCostChangeByProductId(int hiDealProductId)
        {
            var dealStatics = HiDealProvider.HiDealStaticDealGetListByProductIdAndMaxChangeSet(hiDealProductId);
            //表示有產生Coupon動作
            if (dealStatics != null && dealStatics.Count > 0)
            {
                HiDealCouponCollection hiDealCoupons = HiDealProvider.HiDealCouponGetListByProdcutId(hiDealProductId);
                int couponSequence = 0; //憑證Id順序給予ProdcutCost更新
                foreach (var coupon in hiDealCoupons.Where(x => x.Status != (int)HiDealCouponStatus.Cancel && x.Status != (int)HiDealCouponStatus.Refund).OrderBy(x => x.Id))
                {
                    coupon.Cost = GetProductCost(HiDealProvider.HiDealProductCostCollectionGetByProductId(hiDealProductId), ++couponSequence);
                }
                HiDealProvider.HiDealCouponSetList(hiDealCoupons);
            }
        }

        /// <summary>
        /// 依憑證順序號碼，對應至品生活商品進貨價集合的Cost位置，回傳該憑證的Cost數字
        /// </summary>
        /// <param name="hiDealProductCostCollection">品生活商品進貨價集合</param>
        /// <param name="couponSequence">憑證順序號碼</param>
        /// <returns></returns>
        private static decimal GetProductCost(HiDealProductCostCollection hiDealProductCostCollection, int couponSequence)
        {
            decimal lastCost = 0;
            foreach (var hidealproductcost in hiDealProductCostCollection.OrderBy(x => x.CumulativeQuantitiy))
            {
                decimal lastCumulativeQuantitiy = hidealproductcost.CumulativeQuantitiy;
                lastCost = hidealproductcost.Cost;
                if (couponSequence <= lastCumulativeQuantitiy)
                {
                    return lastCost;
                }
            }
            //如果  Coupon_Sequence  超過  Product_cost_Table上限就以最大CumulativeQuantitiy's Cost儲存之。
            return lastCost;
        }

        #endregion ProductCost相關

        #region FromDB

        /// <summary>
        /// 依據商品編號由資料庫中取出HiDealProductDataModule物件
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <returns></returns>
        private static HiDealProductDataModule HiDealProductDataModuleGetByIdFromDb(int productId)
        {
            var module = new HiDealProductDataModule();
            module.Product = HiDealProductGetByIdFromDb(productId);
            //如果有提供宅配，取得運費資料
            module.FreightIncome = HiDealFreightIncomeListGetByPidFromDb(productId);

            return module;
        }

        /// <summary>
        /// 依據商品編號由資料庫裝取出Product物件
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <returns></returns>
        private static HiDealProduct HiDealProductGetByIdFromDb(int productId)
        {
            return HiDealProvider.HiDealProductGet(productId);
        }

        /// <summary>
        /// 依據商品編號由資料庫中取出商品運費收入設定
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <returns></returns>
        private static HiDealFreightCollection HiDealFreightIncomeListGetByPidFromDb(int productId)
        {
            return HiDealProvider.HiDealFreightGetListByProductId(productId, HiDealFreightType.Income);
        }

        #endregion FromDB
    }

    public class HiDealProductDataModule
    {
        public HiDealProduct Product { get; set; }

        public HiDealFreightCollection FreightIncome { get; set; }
    }
}
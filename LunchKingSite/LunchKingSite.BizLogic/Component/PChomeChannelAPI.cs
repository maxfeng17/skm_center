﻿using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Models.PchomeChannel;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Collections;

namespace LunchKingSite.BizLogic.Component
{
    public class PChomeChannelAPI
    {

        static PChomeChannelAPI()
        {

            ServicePointManager.ServerCertificateValidationCallback =
                delegate { return true; };


        }

        static ILog logger = LogManager.GetLogger(typeof(PChomeChannelAPI));

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public static bool IsDevelopment
        {
            get
            {
                return config.EnablePChomeChannel == false;
            }
        }

        public static string ApiBaseUrl
        {
            get
            {
                return "https://ecvdr.pchome.com.tw";
            }
        }

        public static string VendorId
        {
            get
            {
                return config.PChomeChannelVendorId;
            }
        }

        public static string VendorNo
        {
            get
            {
                return config.PChomeChannelVendorNo;
            }
        }

        /// <summary>
        /// 新增電子票券商品
        /// </summary>
        /// <param name="prod"></param>
        /// <returns></returns>
        public static bool AddETicketProd(PChomeETicketProd prod)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/eticket/prod", ApiBaseUrl, "vdr_fake", VendorId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/eticket/prod", ApiBaseUrl, "vdr", VendorId);
                    }

                    var postData = JObject.FromObject(prod).ToString();

                    HttpResponseMessage response = client.PostAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var loc = response.Headers.Location;
                        logger.InfoFormat("AddProd: statusCode={0}, loc={1},locstring={2}, prod={3}", response.StatusCode, response.Headers.Location, loc.OriginalString, JsonConvert.SerializeObject(prod));
                        prod.Id = loc.OriginalString.Split("/").LastOrDefault();
                        prod.GroupId = prod.Id.Substring(0, 16);//不確定可否這樣抓

                        //test
                        //logger.InfoFormat("AddProd: {0} {1}, prod={2}", response.StatusCode, " /vdr/prod/v2/index.php/vendor/00000/prod/QIAG8P-D9005999A-000", JsonConvert.SerializeObject(prod));
                        //prod.Id = "QDAN2F-A90060BHQ-000";
                        //prod.GroupId = "QDAN2F-A90060BHQ";
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("AddProd: statusCode={0}, loc={1}, prod={2}, content={3}, apiUrl={4}", response.StatusCode, response.Headers.Location, JsonConvert.SerializeObject(prod), content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("新增Prod錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(prod), ex.ToString());
            }
            return false;
        }

        /// <summary>
        /// 修改商品詳細資訊
        /// </summary>
        /// <param name="desc"></param>
        /// <returns></returns>
        public static bool EditProd(PChomeETicketProd prod)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/eticket/prod/{3}", ApiBaseUrl, "vdr_fake", VendorId, prod.GroupId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/eticket/prod/{3}", ApiBaseUrl, "vdr", VendorId, prod.GroupId);
                    }

                    var postData = JObject.FromObject(prod).ToString();

                    HttpResponseMessage response = client.PutAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditProd: statusCode={0}, desc={1},count={2}", response.StatusCode, JsonConvert.SerializeObject(prod), content);
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditProd: statusCode={0}, desc={1}, content={2}, apiUrl={3}", response.StatusCode, JsonConvert.SerializeObject(prod), content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("異動Prod錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(prod), ex.ToString());
            }
            return false;
        }

        /// <summary>
        /// 修改文案
        /// </summary>
        /// <param name="intro"></param>
        /// <returns></returns>
        public static bool EditIntro(ProdEditIntro intro)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 1, 0);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/{3}/intro", ApiBaseUrl, "vdr_fake", VendorId, intro.GroupId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/{3}/intro", ApiBaseUrl, "vdr", VendorId, intro.GroupId);
                    }

                    var postData = JObject.FromObject(intro).ToString();

                    HttpResponseMessage response = client.PutAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditIntro: statusCode={0}, intro={1}, content={2}", response.StatusCode, JsonConvert.SerializeObject(intro), content);
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditIntro: statusCode={0}, intro={1}, content={2}, apiUrl={3}", response.StatusCode, JsonConvert.SerializeObject(intro), content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("異動Intro錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(intro), ex.ToString());
            }
            return false;
        }

        /// <summary>
        /// 修改商品說明
        /// </summary>
        /// <param name="desc"></param>
        /// <returns></returns>
        public static bool EditDesc(ProdEditDesc desc)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/{3}/desc", ApiBaseUrl, "vdr_fake", VendorId, desc.GroupId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/{3}/desc", ApiBaseUrl, "vdr", VendorId, desc.GroupId);
                    }

                    var postData = JObject.FromObject(desc).ToString();

                    HttpResponseMessage response = client.PutAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditDesc: statusCode={0}, desc={1},count={2}", response.StatusCode, JsonConvert.SerializeObject(desc), content);
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditDesc: statusCode={0}, desc={1}, content={2}, apiUrl={3}", response.StatusCode, JsonConvert.SerializeObject(desc), content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("異動Desc錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(desc), ex.ToString());
            }
            return false;
        }

        /// <summary>
        /// 修改上下架
        /// </summary>
        /// <param name="isShelf"></param>
        /// <returns></returns>
        public static bool EditIsShelf(List<ProdEditIsShelf> isShelf)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/isshelf?groupid={3}", ApiBaseUrl, "vdr_fake", VendorId, string.Join(",", isShelf.Select(x => x.GroupId)));
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/isshelf?groupid={3}", ApiBaseUrl, "vdr", VendorId, string.Join(",", isShelf.Select(x => x.GroupId)));
                    }

                    var postData = JArray.FromObject(isShelf).ToString();

                    HttpResponseMessage response = client.PutAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditIsShelf: statusCode={0}, isShelf={1}, count={2}", response.StatusCode, JsonConvert.SerializeObject(isShelf), content);
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditIsShelf: statusCode={0}, isShelf={1}, content={2}, apiUrl={3}", response.StatusCode, JsonConvert.SerializeObject(isShelf), content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("異動IsShelf錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(isShelf), ex.ToString());
            }
            return false;
        }


        /// <summary>
        /// 修改價格
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public static bool EditPrice(List<ProdEditPrice> price)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/price?groupid={3}", ApiBaseUrl, "vdr_fake", VendorId, string.Join(",", price.Select(x => x.GroupId)));
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/price?groupid={3}", ApiBaseUrl, "vdr", VendorId, string.Join(",", price.Select(x => x.GroupId)));
                    }

                    var postData = JArray.FromObject(price).ToString();

                    HttpResponseMessage response = client.PutAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditPrice: statusCode={0}, price={1}, content={2}", response.StatusCode, JsonConvert.SerializeObject(price), content);
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditPrice: statusCode={0}, price={1}, content={2}, apiUrl={3}", response.StatusCode, JsonConvert.SerializeObject(price), content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("異動price錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(price), ex.ToString());
            }
            return false;
        }

        /// <summary>
        /// 修改庫存
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static bool EditQty(List<ProdEditQry> qty)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/qty?prodid={3}", ApiBaseUrl, "vdr_fake", VendorId, string.Join(",", qty.Select(x => x.Id)));
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/qty?prodid={3}", ApiBaseUrl, "vdr", VendorId, string.Join(",", qty.Select(x => x.Id)));
                    }

                    var postData = JArray.FromObject(qty).ToString();

                    HttpResponseMessage response = client.PutAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditQty: statusCode={0}, qty={1}, content={2}", response.StatusCode, JsonConvert.SerializeObject(qty), content);
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditQty: statusCode={0}, qty={1}, content={2}, apiUrl={3}", response.StatusCode, JsonConvert.SerializeObject(qty), content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("異動qty錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(qty), ex.ToString());
            }
            return false;
        }


        /// <summary>
        /// 修改大圖
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static bool EditPic(List<ProdEditPic> pic)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/pic?prodid={3}", ApiBaseUrl, "vdr_fake", VendorId, string.Join(",", pic.Select(x => x.Id)));
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/pic?prodid={3}", ApiBaseUrl, "vdr", VendorId, string.Join(",", pic.Select(x => x.Id)));
                    }

                    var postData = JArray.FromObject(pic).ToString();

                    HttpResponseMessage response = client.PutAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        logger.InfoFormat("EditPic: statusCode={0}, pic={1}", response.StatusCode, JsonConvert.SerializeObject(pic));
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("EditPic: statusCode={0}, pic={1}, content={2}, apiUrl={3}", response.StatusCode, JsonConvert.SerializeObject(pic), content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("異動pic錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(pic), ex.ToString());
            }
            return false;
        }

        /// <summary>
        /// 查詢產品
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static List<PChomeETicketProd> GetProd(string prodId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/{3}", ApiBaseUrl, "vdr_fake", VendorId, prodId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/prod/v2/index.php/vendor/{2}/prod/{3}", ApiBaseUrl, "vdr", VendorId, prodId);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("GetProd: statusCode={0}, prod={1}, content={2}", response.StatusCode, prodId, content);
                        content = Regex.Unescape(content);
                        List<PChomeETicketProd> prod = JsonConvert.DeserializeObject<List<PChomeETicketProd>>(content);

                        logger.InfoFormat("GetProd: statusCode={0}, prod={1}, content={2}", response.StatusCode, prodId, content);
                        return prod ?? new List<PChomeETicketProd>();
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("GetProd: statusCode={0}, prod={1}, content={2}, apiUrl={3}", response.StatusCode, prodId, content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("查詢prod錯誤, prodId={0}, ex={1}", prodId, ex.ToString());
            }
            return new List<PChomeETicketProd>();
        }

        /// <summary>
        /// 上傳圖片
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static bool UpdateFile(List<UpdateFile> files)
        {
            try
            {
                string apiUrl;
                if (IsDevelopment)
                {
                    apiUrl = string.Format("{0}/{1}/upload/v2/index.php/vendor/{2}/file", ApiBaseUrl, "vdr_fake", VendorId);
                    //apiUrl = @"http://localhost/api/memberservice/UpdatePchomePic";
                }
                else
                {
                    apiUrl = string.Format("{0}/{1}/upload/v2/index.php/vendor/{2}/file", ApiBaseUrl, "vdr", VendorId);
                }


                //1.
                //string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
                //byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

                //HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(apiUrl);
                //wr.ContentType = "multipart/form-data; boundary=" + boundary;
                //wr.Method = "POST";
                //wr.KeepAlive = true;
                //wr.Credentials = System.Net.CredentialCache.DefaultCredentials;

                //Stream rs = wr.GetRequestStream();

                //string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
                //foreach (string key in nvc.Keys)
                //{
                //    rs.Write(boundarybytes, 0, boundarybytes.Length);
                //    string formitem = string.Format(formdataTemplate, key, nvc[key]);
                //    byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                //    rs.Write(formitembytes, 0, formitembytes.Length);
                //}
                //rs.Write(boundarybytes, 0, boundarybytes.Length);

                //string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
                //string header = string.Format(headerTemplate, paramName, file, contentType);
                //byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
                //rs.Write(headerbytes, 0, headerbytes.Length);

                //FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
                //byte[] buffer = new byte[4096];
                //int bytesRead = 0;
                //while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                //{
                //    rs.Write(buffer, 0, bytesRead);
                //}
                //fileStream.Close();

                //byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
                //rs.Write(trailer, 0, trailer.Length);
                //rs.Close();

                //WebResponse wresp = null;
                //try
                //{
                //    wresp = wr.GetResponse();
                //    Stream stream2 = wresp.GetResponseStream();
                //    StreamReader reader2 = new StreamReader(stream2);
                //    string aa = reader2.ReadToEnd();
                //}
                //catch (Exception ex)
                //{
                //    if (wresp != null)
                //    {
                //        wresp.Close();
                //        wresp = null;
                //    }
                //}
                //finally
                //{
                //    wr = null;
                //}


                //2.
                logger.InfoFormat("UpdateFile Start");
                logger.InfoFormat("UpdateFile OriginalFile={0}", JsonConvert.SerializeObject(files));

                string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
                request.ContentType = "multipart/form-data; boundary=" + boundary;
                request.Method = "POST";
                request.KeepAlive = true;

                Stream memStream = new System.IO.MemoryStream();
                var boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
                var endBoundaryBytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--");


                string formdataTemplate = "\r\n--" + boundary + "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

                //if (file.nvc != null)
                //{
                //    foreach (string key in file.nvc.Keys)
                //    {
                //        string formitem = string.Format(formdataTemplate, key, file.nvc[key]);
                //        byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                //        memStream.Write(formitembytes, 0, formitembytes.Length);
                //        logger.Info("formitem：" + formitem);
                //        string str = System.Text.Encoding.Default.GetString(formitembytes);
                //        logger.Info("str：" + str);
                //    }
                //}


                foreach (UpdateFile file in files)
                {
                    string headerTemplate;
                    if (file.E7FileName.EndsWith(".gif", StringComparison.OrdinalIgnoreCase))
                    {
                        headerTemplate =
                            "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n" +
                            "Content-Type: image/gif\r\n\r\n";
                    } 
                    else
                    {
                        headerTemplate =
                            "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n" +
                            "Content-Type: image/jpeg\r\n\r\n";
                    }

                    logger.InfoFormat("檔案是否存在：{0} - {1}", file.E7PhysicalPath, System.IO.File.Exists(file.E7PhysicalPath));

                    memStream.Write(boundarybytes, 0, boundarybytes.Length);
                    var header = string.Format(headerTemplate, "files[]", file.E7FileName);
                    var headerbytes = System.Text.Encoding.UTF8.GetBytes(header);

                    string strBoundarybytes = System.Text.Encoding.Default.GetString(boundarybytes);
                    logger.Info("Boundarybytes：" + strBoundarybytes);


                    memStream.Write(headerbytes, 0, headerbytes.Length);
                    string strHeaderbytes = System.Text.Encoding.Default.GetString(headerbytes);
                    logger.Info("Headerbytes：" + strHeaderbytes);


                    using (var fileStream = new FileStream(file.E7PhysicalPath, FileMode.Open, FileAccess.Read))
                    {
                        var buffer = new byte[1024];
                        var bytesRead = 0;
                        while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }
                }

                memStream.Write(endBoundaryBytes, 0, endBoundaryBytes.Length);
                string strEndBoundaryBytes = System.Text.Encoding.Default.GetString(endBoundaryBytes);
                logger.Info("EndBoundaryBytes：" + strEndBoundaryBytes);


                request.ContentLength = memStream.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    memStream.Position = 0;
                    byte[] tempBuffer = new byte[memStream.Length];
                    memStream.Read(tempBuffer, 0, tempBuffer.Length);
                    memStream.Close();
                    requestStream.Write(tempBuffer, 0, tempBuffer.Length);

                    logger.Info("BufferLength：" + tempBuffer.Length.ToString());
                }

                string result = "";
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                    logger.InfoFormat("UpdateFile： statusCode={0}, result={1}, file={2}", response.StatusCode.ToString(), result, JsonConvert.SerializeObject(files));
                }

                //using (var response = request.GetResponse())
                //{
                //    Stream stream2 = response.GetResponseStream();
                //    StreamReader reader2 = new StreamReader(stream2);
                //    string result = reader2.ReadToEnd();
                //    HttpWebResponse response2 = (HttpWebResponse)response;


                //test
                //1筆
                //result = "[{\"Name\":\"97614_photo_02_PC800.jpg\",\"Path\":\"tmp_222.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"}]";
                //2筆
                //result = "[{\"Name\":\"97614_photo_02_PC800.jpg\",\"Path\":\"tmp_222.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"333.jpg\",\"Path\":\"tmp_333.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_333.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"}]";

                //21筆
                //if (files.Count == 20)
                //{
                //    result = "[{\"Name\":\"1.jpg\",\"Path\":\"tmp_1.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"2.jpg\",\"Path\":\"tmp_2.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"3.jpg\",\"Path\":\"tmp_3.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"4.jpg\",\"Path\":\"tmp_4.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"5.jpg\",\"Path\":\"tmp_5.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"6.jpg\",\"Path\":\"tmp_6.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"7.jpg\",\"Path\":\"tmp_7.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"8.jpg\",\"Path\":\"tmp_8.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"9.jpg\",\"Path\":\"tmp_9.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"10.jpg\",\"Path\":\"tmp_10.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"11.jpg\",\"Path\":\"tmp_11.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"12.jpg\",\"Path\":\"tmp_12.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"13.jpg\",\"Path\":\"tmp_13.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"14.jpg\",\"Path\":\"tmp_14.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"15.jpg\",\"Path\":\"tmp_15.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"16.jpg\",\"Path\":\"tmp_16.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"17.jpg\",\"Path\":\"tmp_17.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"18.jpg\",\"Path\":\"tmp_18.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"19.jpg\",\"Path\":\"tmp_19.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"},{\"Name\":\"20.jpg\",\"Path\":\"tmp_20.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"}]";

                //}
                //if (files.Count == 1)
                //{
                //    result = "[{\"Name\":\"21.jpg\",\"Path\":\"tmp_21.jpg\",\"Url\":\"https://ecvdr.pchome.com.tw/vdr/upload/v2/index.php/vendor/37239/file/tmp_222.jpg\",\"Type\":\"image/jpeg\",\"Size\":147212,\"Md5\":\"b1fe1a0369858e88f670f2435b4495dc\"}]";
                //}


                result = Regex.Unescape(result);
                logger.InfoFormat("UpdateFile Unescape Result={0}", result);
                List<FileDetail> fileDetail = new Core.JsonSerializer().Deserialize<List<FileDetail>>(result);

                if (fileDetail.Count != files.Count)
                {
                    //request和result 數量不一致
                    logger.Warn("UpdateFile request和result數量不一致");
                    return false;
                }
                foreach (FileDetail d in fileDetail)
                {
                    UpdateFile f = files.Find(x => x.E7FileName == d.Name && string.IsNullOrEmpty(x.PCPath));
                    if (f == null)
                    {
                        logger.Warn("UpdateFile 找不到對應的圖片,");
                        return false;
                    }
                    f.PCPath = d.Path;
                    f.PCUrl = d.Url;
                }
                logger.InfoFormat("UpdateFile ConvertFile={0}", JsonConvert.SerializeObject(files));
                logger.InfoFormat("UpdateFile End");
                return true;
            }
            catch (Exception ex)
            {
                logger.WarnFormat("上傳file錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(files), ex.ToString());
            }
            return false;
        }

        /// <summary>
        /// 查PChome聯絡單明細列表
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static PChomeContactList GetContactList(string startDate,string endDate)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/contact/v1/index.php/core/vendor/{2}/contact?notifydate={3}-{4}&offset=1&limit=100", ApiBaseUrl, "vdr_fake", VendorId, startDate, endDate);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/contact/v1/index.php/core/vendor/{2}/contact?notifydate={3}-{4}&offset=1&limit=100", ApiBaseUrl, "vdr", VendorId, startDate, endDate);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        PChomeContactList contactList = JsonConvert.DeserializeObject<PChomeContactList>(content);

                        logger.InfoFormat("GetContactList: statusCode={0}, startDate={1}, endDate={2}, content={3}", response.StatusCode, startDate, endDate, content);
                        return contactList ?? new PChomeContactList();
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("GetContactList: statusCode={0}, startDate={1}, endDate={2}, content={3}", response.StatusCode, startDate, endDate, content);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得ContactList錯誤, data={0}-{1}, ex={2}", startDate, endDate, ex.ToString());
            }
            return new PChomeContactList();
        }

        /// <summary>
        /// 查PChome聯絡單明細
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static List<PChomeContactDetail> GetContactDetail(string contactId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/contact/v1/index.php/vendor/{2}/contact?id={3}", ApiBaseUrl, "vdr_fake", VendorId, contactId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/contact/v1/index.php/vendor/{2}/contact?id={3}", ApiBaseUrl, "vdr", VendorId, contactId);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        List<PChomeContactDetail> contactDetail = JsonConvert.DeserializeObject<List<PChomeContactDetail>>(content);

                        logger.InfoFormat("GetContactDetail: statusCode={0}, contactId={1}, content={2}", response.StatusCode, contactId, content);
                        return contactDetail ?? new List<PChomeContactDetail>();
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        logger.InfoFormat("GetContactDetail: statusCode={0}, contactId={1}, content={2}, apiUrl={3}", response.StatusCode, contactId, content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得ContactDetail錯誤, data={0}, ex={1}", contactId, ex.ToString());
            }
            return new List<PChomeContactDetail>();
        }

        /// <summary>
        /// 回覆聯絡單
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static bool ReplyContact(string contactId,string replayContent)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/contact/v1/index.php/vendor/{2}/contact/{3}/vendorreply", ApiBaseUrl, "vdr_fake", VendorId, contactId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/contact/v1/index.php/vendor/{2}/contact/{3}/vendorreply", ApiBaseUrl, "vdr", VendorId, contactId);
                    }

                    HttpResponseMessage response = client.PutAsync(apiUrl, new StringContent(replayContent, Encoding.UTF8, "text/plain")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        logger.InfoFormat("ReplyContact: statusCode={0}, contactId={1}, replayContent={2}, content={3}", response.StatusCode, contactId, replayContent, content);
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("ReplyContact: statusCode={0}, contactId={1}, replayContent={2}, content={3}, apiUrl={4}", response.StatusCode, contactId, replayContent, content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("回應ReplyContact錯誤, data={0} , {1}, ex={2}", contactId, replayContent, ex.ToString());
            }
            return false;
        }

        /// <summary>
        /// 查客訴列表
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static PChomeComplaintList GetComplaintList(string startDate, string endDate)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/service/v1/index.php/core/vendor/{2}/complaint?q=Wating&createdate={3}-{4}&offset=1&limit=100", ApiBaseUrl, "vdr_fake", VendorId, startDate, endDate);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/service/v1/index.php/core/vendor/{2}/complaint?q=Wating&createdate={3}-{4}&offset=1&limit=100", ApiBaseUrl, "vdr", VendorId, startDate, endDate);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        PChomeComplaintList complaintList = JsonConvert.DeserializeObject<PChomeComplaintList>(content);


                        logger.InfoFormat("GetComplaintList:statusCode={0}, startDate={1}, endDate={2}, content={3}", response.StatusCode, startDate, endDate, content);
                        return complaintList ?? new PChomeComplaintList();
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("GetComplaintList:statusCode={0}, startDate={1}, endDate={2}, content={3}, apiUrl={4}", response.StatusCode, startDate, endDate, content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得ComplaintList錯誤, data={0}-{1}, ex={2}", startDate, endDate, ex.ToString());
            }
            return new PChomeComplaintList();
        }

        /// <summary>
        /// 查PChome客訴明細
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static List<PChomeComplaintDetail> GetComplaintDetail(string complaintId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/service/v1/index.php/vendor/{2}/complaint?uid={3}", ApiBaseUrl, "vdr_fake", VendorId, complaintId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/service/v1/index.php/vendor/{2}/complaint?uid={3}", ApiBaseUrl, "vdr", VendorId, complaintId);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        List<PChomeComplaintDetail> complaintDetail = JsonConvert.DeserializeObject<List<PChomeComplaintDetail>>(content);
                        logger.InfoFormat("GetComplaintDetail: statusCode={0}, complaintId={1}, content={2}", response.StatusCode, complaintId, content);
                        return complaintDetail ?? new List<PChomeComplaintDetail>();
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        logger.InfoFormat("GetComplaintDetail: statusCode={0}, complaintId={1}, content={2}, apiUrl={3}", response.StatusCode, complaintId, content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得ComplaintDetail錯誤, data={0}, ex={1}", complaintId, ex.ToString());
            }
            return new List<PChomeComplaintDetail>();
        }

        /// <summary>
        /// 查PChome客訴回應資訊
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static List<PChomeComplaintReply> GetComplaintReply(string Id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/service/v1/index.php/vendor/{2}/complaint/reply?id={3}", ApiBaseUrl, "vdr_fake", VendorId, Id);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/service/v1/index.php/vendor/{2}/complaint/reply?id={3}", ApiBaseUrl, "vdr", VendorId, Id);
                    }

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = content.Replace("\\\"", "'");
                        content = Regex.Unescape(content);
                        logger.InfoFormat("GetComplaintreply: content={0}", content);
                        List<PChomeComplaintReply> complaintReply = JsonConvert.DeserializeObject<List<PChomeComplaintReply>>(content);
                        logger.InfoFormat("GetComplaintreply: statusCode={0}, Id={1}, content={2}", response.StatusCode, Id, content);
                        return complaintReply ?? new List<PChomeComplaintReply>();
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        logger.InfoFormat("GetComplaintreply: statusCode={0}, Id={1}, content={2}, apiUrl={3}", response.StatusCode, Id, content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("取得GetComplaintReply錯誤, data={0}, ex={1}", Id, ex.ToString());
            }
            return new List<PChomeComplaintReply>();
        }

        /// <summary>
        /// 回覆客訴
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static bool ReplyComplaint(string complaintId, ComplaintReply replayContent)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/{1}/service/v1/index.php/vendor/{2}/complaint/{3}/reply/content", ApiBaseUrl, "vdr_fake", VendorId, complaintId);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/{1}/service/v1/index.php/vendor/{2}/complaint/{3}/reply/content", ApiBaseUrl, "vdr", VendorId, complaintId);
                    }


                    var postData = JObject.FromObject(replayContent).ToString();
                    HttpResponseMessage response = client.PostAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/json")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        logger.InfoFormat("ReplyComplaint: statusCode={0}, complaintId={1}, replyContent={2}, content={3}", response.StatusCode, complaintId, JsonConvert.SerializeObject(replayContent), content);
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("ReplyComplaint: statusCode={0}, complaintId={1}, replyContent={2}, content={3}, apiUrl={4}", response.StatusCode, complaintId, JsonConvert.SerializeObject(replayContent), content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("回應ReplyComplaint錯誤, data={0} {1}, ex={2}", complaintId, JsonConvert.SerializeObject(replayContent), ex.ToString());
            }
            return false;
        }

        /// <summary>
        /// 通知核銷
        /// </summary>
        /// <param name="qty"></param>
        /// <returns></returns>
        public static bool NotifyVerify(PChomeChannleVerifyNotify notify)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiUrl;
                    if (IsDevelopment)
                    {
                        apiUrl = string.Format("{0}/eticket/notify/{1}", ApiBaseUrl, VendorNo);
                    }
                    else
                    {
                        apiUrl = string.Format("{0}/eticket/notify/{1}", ApiBaseUrl, VendorNo);
                    }


                    var postData = JObject.FromObject(notify).ToString();
                    HttpResponseMessage response = client.PostAsync(apiUrl, new StringContent(postData, Encoding.UTF8, "application/x-www-form-urlencoded")).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        content = Regex.Unescape(content);
                        logger.InfoFormat("VerifyNotify: statusCode={0}, notify={1}, content={2}", response.StatusCode, JsonConvert.SerializeObject(notify), content);
                        return true;
                    }
                    else
                    {
                        string content = response.Content.ReadAsStringAsync().Result;
                        logger.InfoFormat("VerifyNotify: statusCode={0}, notify={1}, content={2}, apiUrl={3}", response.StatusCode, JsonConvert.SerializeObject(notify), content, apiUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("回應VerifyNotify錯誤, data={0}, ex={1}", JsonConvert.SerializeObject(notify), ex.ToString());
            }
            return false;
        }

    }
}

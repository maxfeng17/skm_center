﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.TurnCloud;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Models.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Component
{
    public class TurnCloudServerUtility
    {
        private static readonly ISysConfProvider Config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger("TurnCloudServerUtility");

        #region API 功能
        /// <summary>
        /// 開立發票
        /// </summary>
        /// <param name="reqest">開立發票請求資訊</param>
        /// <returns>開立發票回應資訊</returns>
        public static CreateInvoiceResponse CreateInvoice(CreateInvoiceRequest reqest)
        {
            CreateInvoiceResponse returnValue = new CreateInvoiceResponse()
            {
                ResponseBase = new TurnCloudResponseMessage(),
                results = new CreateInvoiceResponseData()
            };

            if (!Config.IsExecuteTurnCloudServer)
            {
                returnValue.ResponseBase.ReturnCode = "1";
                returnValue.ResponseBase.ReturnMessage = "成功";
                returnValue.results.InvoiceNo = "AA12345678";
                return returnValue;
            }

            var uri = "/api/Product/Upload_rm_own";
            returnValue = Post<CreateInvoiceResponse, CreateInvoiceRequest>(reqest, uri);
            return returnValue;
        }

        /// <summary>
        /// 發票退貨
        /// </summary>
        /// <param name="reqest">發票退貨請求資訊</param>
        /// <returns>發票退貨回應資訊</returns>
        public static RefundInvoiceResponse RefundInvoice(RefundInvoiceRequest reqest)
        {
            RefundInvoiceResponse returnValue = new RefundInvoiceResponse()
            {
                ResponseBase = new TurnCloudResponseMessage(),
                results = new RefundInvoiceResponseData()
            };
            if (!Config.IsExecuteTurnCloudServer)
            {
                returnValue.ResponseBase.ReturnCode = "1";
                returnValue.ResponseBase.ReturnMessage = "成功";
                return returnValue;
            }
            var uri = "/api/Product/Upload_rm_own";
            returnValue = Post<RefundInvoiceResponse, RefundInvoiceRequest>(reqest, uri);
            return returnValue;
        }

        /// <summary>
        /// 核銷
        /// </summary>
        /// <param name="reqest">核銷請求資訊</param>
        /// <returns>核銷回應資訊</returns>
        public static WriteOffInvoiceResponse WriteOffInvoice(WriteOffInvoiceRequest reqest)
        {
            WriteOffInvoiceResponse returnValue = new WriteOffInvoiceResponse()
            {
                ResponseBase = new TurnCloudResponseMessage(),
                results = new WriteOffInvoiceResponseData()
            };
            if (!Config.IsExecuteTurnCloudServer)
            {
                returnValue.ResponseBase.ReturnCode = "1";
                returnValue.ResponseBase.ReturnMessage = "成功";
                return returnValue;
            }
            var uri = "/api/Product/Upload_rm_own";
            returnValue = Post<WriteOffInvoiceResponse, WriteOffInvoiceRequest>(reqest, uri);
            return returnValue;
        }

        /// <summary>
        /// 上傳每日銷售總額
        /// </summary>
        /// <param name = "reqest" > 每日銷售總額請求資訊 </ param >
        /// < returns > 每日銷售總額回應資訊 </ returns >
        public static ClearPosResponse ClearPos(ClearPosRequest reqest)
        {
            ClearPosResponse returnValue = new ClearPosResponse()
            {
                ResponseBase = new TurnCloudResponseMessage(),
                Results = string.Empty
            };
            if (!Config.IsExecuteTurnCloudServer)
            {
                returnValue.ResponseBase.ReturnCode = "1";
                returnValue.ResponseBase.ReturnMessage = "成功";
                return returnValue;
            }
            var uri = "/api/Product/Upload_ClearPos_own ";
            returnValue = Post<ClearPosResponse, ClearPosRequest>(reqest, uri);
            return returnValue;
        }
        #endregion

        #region 轉型
        /// <summary>
        /// 使用發票相關資訊產生開立發票請求資訊
        /// </summary>
        /// <param name="skmPayInvoice">發票資訊</param>
        /// <param name="skmPayInvoiceSellDetail">發票銷售明細</param>
        /// <param name="skmPayInvoiceTenderDetails">發票付款明細</param>
        /// <returns>開立發票請求資訊</returns>
        public static CreateInvoiceRequest GenerateCreateInvoiceRequest(
            SkmPayInvoice skmPayInvoice,
            SkmPayInvoiceSellDetail skmPayInvoiceSellDetail,
            IEnumerable<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails)
        {
            string invoice6XStoreId = string.Empty; //發票6X櫃號
            string invoiceHashKey = string.Empty;   //發票交易簽章HashKey
            string invoicePreSalesStatus = string.Empty;
            switch (skmPayInvoice.PlatFormId)
            {
                case "7": //精選優惠
                    if (Config.EnableTurnCloudInvoice6X)
                    {
                        invoice6XStoreId = skmPayInvoice.StoreId;
                    }
                    invoiceHashKey = Config.TurnCloudServerHashKey;
                    invoicePreSalesStatus = skmPayInvoice.PreSalesStatus.ToString();
                    break;
                case "P": //停車支付
                    invoice6XStoreId = skmPayInvoice.StoreId;
                    invoiceHashKey = Config.TurnCloudServerParkingHashKey;
                    //停車支付要傳空白, 但由於資料庫是存INT 所以預設是0, 無法直接tostring
                    invoicePreSalesStatus = "";
                    break;
            }


            List<CreateInvoiceTenderDetail> tenderDetails = new List<CreateInvoiceTenderDetail>();
            foreach (SkmPayInvoiceTenderDetail skmPayInvoiceTenderDetail in skmPayInvoiceTenderDetails)
            {
                tenderDetails.Add(new CreateInvoiceTenderDetail()
                {
                    StoreId = invoice6XStoreId,
                    PayOrderNo = skmPayInvoiceTenderDetail.PayOrderNo,
                    BrandCounterCode = skmPayInvoiceSellDetail.NdType,
                    SecNo = skmPayInvoiceTenderDetail.SecNo,
                    SellTender = skmPayInvoiceTenderDetail.SellTender,
                    TenderPrice = skmPayInvoiceTenderDetail.TenderPrice,
                    TotalAddPoint = skmPayInvoiceTenderDetail.PointAddTotal,
                    TotalDeductPoint = skmPayInvoiceTenderDetail.PointDeductTotal,
                    CreditCardDetail = new CreateInvoiceCreditCardDetail()
                    {
                        CardNo = skmPayInvoiceTenderDetail.CardNo,
                        IsUseBonus = (skmPayInvoiceTenderDetail.BonusUsePoint.HasValue &&
                                      skmPayInvoiceTenderDetail.BonusUsePoint.Value > 0) ? "Y" : "N",
                        CardNo2 = skmPayInvoiceTenderDetail.CardNo2,
                        TransAmount = skmPayInvoiceTenderDetail.TenderPrice,
                        BonusUsePoint = skmPayInvoiceTenderDetail.BonusUsePoint.HasValue ? skmPayInvoiceTenderDetail.BonusUsePoint.Value : 0,
                        BonusDiscountAmount = skmPayInvoiceTenderDetail.BonusDisAmt.HasValue ? skmPayInvoiceTenderDetail.BonusDisAmt.Value : 0,
                        BonusPoint = skmPayInvoiceTenderDetail.BonusPoint.HasValue ? skmPayInvoiceTenderDetail.BonusPoint.Value : 0,
                        //分期
                        Periods = skmPayInvoiceTenderDetail.Periods ?? 0,
                        EachAmount = skmPayInvoiceTenderDetail.EachAmt ?? 0,
                        FirstAmount = skmPayInvoiceTenderDetail.HandAmt ?? 0
                    }
                });
            }

            CreateInvoiceRequest returnValue = new CreateInvoiceRequest()
            {
                PlatFormId = skmPayInvoice.PlatFormId,//parking 用 P
                PosId = skmPayInvoice.SellPosid,//parking 有自己對照表
                InvoiceDetails = new CreateInvoiceDetail[]{
                    new CreateInvoiceDetail()
                    {
                        SellNo = skmPayInvoice.SellNo,
                        SellShopId = skmPayInvoice.SellShopid,
                        SellOrderNo= skmPayInvoice.SellOrderNo,
                        PosId = skmPayInvoice.SellPosid,
                        SellDay = skmPayInvoice.CreateDate.ToString("yyyy/MM/dd"),
                        SellTime = skmPayInvoice.CreateDate.ToString("HH:mm:ss"),
                        StoreId = invoice6XStoreId,
                        TradeDay = skmPayInvoice.CreateDate.ToString("yyyy/MM/dd"),
                        TotalAmount = skmPayInvoice.TotalAmt,
                        InvoiceAmount = skmPayInvoice.TotalSaleAmt,
                        TotalQuantity = skmPayInvoice.TotalQty,
                        DiscountAmount = skmPayInvoice.DiscAmt,
                        RateAmount = skmPayInvoice.RateAmt,
                        ExtraAmount = skmPayInvoice.ExtraAmt,
                        SellTransType = ((int)skmPayInvoice.SellTranstype).ToString().PadLeft(2, '0'),
                        VipNo = skmPayInvoice.VipNo,
                        CompanyId = skmPayInvoice.CompId,
                        VipAppPass = skmPayInvoice.VipAppPass,
                        EuiDonateNo = skmPayInvoice.EuiDonateNo,
                        EuiDonateStatus = skmPayInvoice.EuiDonateStatus ? 1 : 0,
                        EuiVehicleNo = skmPayInvoice.EuiVehicleNo,
                        EuiVehicleTypeNo = skmPayInvoice.EuiVehicleTypeNo,
                        ParkingToken = skmPayInvoice.ParkingToken==null? string.Empty:skmPayInvoice.ParkingToken,//填入車牌號碼  精選優惠此欄位會為空
                        EuiUniversalStatus = skmPayInvoice.EuiUniversalStatus ? 1 : 0,
                        PreDalesStatus = invoicePreSalesStatus,
                        SellDetails = new CreateInvoiceSellDetail[]{
                            new CreateInvoiceSellDetail()
                            {
                                SellDetailNo = skmPayInvoiceSellDetail.SellOrderNoS,
                                SellId = skmPayInvoiceSellDetail.SellId,
                                BrandCounterCode = skmPayInvoiceSellDetail.NdType,
                                StoreId = invoice6XStoreId,
                                Quantity = skmPayInvoiceSellDetail.SellQty,
                                Price = skmPayInvoiceSellDetail.SellPrice,
                                SellDiscount = skmPayInvoiceSellDetail.SellDiscount,
                                SellRate = (TurnCloudSellRateType)skmPayInvoiceSellDetail.SellRate,
                                SellRateAmount = skmPayInvoiceSellDetail.SellRateAmt,
                                SellAmount = skmPayInvoiceSellDetail.SellAmt,
                                ProductId = skmPayInvoiceSellDetail.ProductId,//Parking 固定填9900007090028
                                OriginalPrice = skmPayInvoiceSellDetail.SellOriPrice,
                                ProductDiscountType = Helper.GetEnumDescription((TurnCloudProductDiscountType)skmPayInvoiceSellDetail.ProductDiscountType),
                                PaymentType = skmPayInvoiceSellDetail.PmtType,
                                AddPoints = skmPayInvoiceSellDetail.PointAdd,
                                DeductPoints = skmPayInvoiceSellDetail.PointDeduct,
                                GoodsName = GetFixBytesByGoodsName(skmPayInvoiceSellDetail.GoodsName),
                            }
                        },
                        TenderDetails = tenderDetails.ToArray(),
                    }
                }
            };
            if (Config.IsUseTestVipNoAndShopCode)
            {
                returnValue.PosId = "355555";
                returnValue.InvoiceDetails.ForEach(item =>
                {
                    item.VipNo = "830800000211127";
                    item.SellShopId = "123";
                    item.PosId = "355555";
                    item.SellDetails.ForEach(sellItem => { sellItem.BrandCounterCode = "7090001"; });
                    item.TenderDetails.ForEach(tenderItem => { tenderItem.BrandCounterCode = "7090001"; });
                });
            }

            returnValue.HashKey = GetHashKey(returnValue.InvoiceDetails[0].SellDay,
                                             returnValue.InvoiceDetails[0].SellOrderNo,
                                             returnValue.InvoiceDetails[0].PosId,
                                             returnValue.InvoiceDetails[0].SellShopId,
                                             returnValue.InvoiceDetails[0].SellTransType,
                                             invoiceHashKey);

            return returnValue;
        }

        /// <summary>
        /// 商品名稱只取30 bytes (逐字計算)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string GetFixBytesByGoodsName(string val)
        {
            int byteLength = 0;
            string newValue = string.Empty;

            if (!string.IsNullOrEmpty(val) && Encoding.GetEncoding("big5").GetBytes(val).Length > 30)
            {
                for (int i = 0; i < val.Length; i++)//逐字計算
                {
                    byteLength += Encoding.GetEncoding("big5").GetBytes(val.Substring(i, 1)).Length;
                    if (byteLength > 30) break;
                    newValue += val.Substring(i, 1);
                }
            }
            else
            {
                newValue = val;
            }

            return newValue;
        }

        /// <summary>
        /// 使用發票相關資訊產生退款請求資訊
        /// </summary>
        /// <param name="skmPayInvoice">發票資訊</param>
        /// <param name="skmPayInvoiceSellDetail">發票銷售明細</param>
        /// <param name="skmPayInvoiceTenderDetails">發票付款明細</param>
        /// <returns>退款請求資訊</returns>
        public static RefundInvoiceRequest GenerateRefundInvoiceRequest(SkmPayInvoice skmPayInvoice, SkmPayInvoiceSellDetail skmPayInvoiceSellDetail, IEnumerable<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails)
        {
            string invoice6XStoreId = string.Empty; //發票6X櫃號
            string invoiceHashKey = string.Empty;   //發票交易簽章HashKey
            string invoicePreSalesStatus = string.Empty;
            switch (skmPayInvoice.PlatFormId)
            {
                case "7": //精選優惠
                    if (Config.EnableTurnCloudInvoice6X)
                    {
                        invoice6XStoreId = skmPayInvoice.StoreId;
                    }
                    invoiceHashKey = Config.TurnCloudServerHashKey;
                    invoicePreSalesStatus = skmPayInvoice.PreSalesStatus.ToString();
                    break;
                case "P": //停車支付
                    invoice6XStoreId = skmPayInvoice.StoreId;
                    invoiceHashKey = Config.TurnCloudServerParkingHashKey;
                    //停車支付要傳空白, 但由於資料庫是存INT 所以預設是0, 無法直接tostring
                    invoicePreSalesStatus = "";
                    break;
            }

            List<RefundInvoiceTenderDetail> tenderDetails = new List<RefundInvoiceTenderDetail>();
            foreach (SkmPayInvoiceTenderDetail skmPayInvoiceTenderDetail in skmPayInvoiceTenderDetails)
            {
                tenderDetails.Add(new RefundInvoiceTenderDetail()
                {
                    StoreId = invoice6XStoreId,
                    PayOrderNo = skmPayInvoiceTenderDetail.PayOrderNo,
                    BrandCounterCode = skmPayInvoiceSellDetail.NdType,
                    SecNo = skmPayInvoiceTenderDetail.SecNo,
                    SellTender = skmPayInvoiceTenderDetail.SellTender,
                    TenderPrice = skmPayInvoiceTenderDetail.TenderPrice,
                    TotalAddPoint = skmPayInvoiceTenderDetail.PointAddTotal,
                    TotalDeductPoint = skmPayInvoiceTenderDetail.PointDeductTotal,
                    CreditCardDetail = new RefundInvoiceCreditCardDetail()
                    {
                        CardNo = skmPayInvoiceTenderDetail.CardNo,
                        IsUseBonus = (skmPayInvoiceTenderDetail.BonusUsePoint.HasValue &&
                                      skmPayInvoiceTenderDetail.BonusUsePoint.Value > 0) ? "Y" : "N",
                        CardNo2 = skmPayInvoiceTenderDetail.CardNo2,
                        TransAmount = skmPayInvoiceTenderDetail.TenderPrice,
                        BonusUsePoint = skmPayInvoiceTenderDetail.BonusUsePoint.HasValue ? skmPayInvoiceTenderDetail.BonusUsePoint.Value : 0,
                        BonusDiscountAmount = skmPayInvoiceTenderDetail.BonusDisAmt.HasValue ? skmPayInvoiceTenderDetail.BonusDisAmt.Value : 0,
                        BonusPoint = skmPayInvoiceTenderDetail.BonusPoint.HasValue ? skmPayInvoiceTenderDetail.BonusPoint.Value : 0,
                        //分期
                        Periods = skmPayInvoiceTenderDetail.Periods ?? 0,
                        EachAmt = skmPayInvoiceTenderDetail.EachAmt ?? 0,
                        FirstAmount = skmPayInvoiceTenderDetail.HandAmt ?? 0
                    }
                });
            }

            RefundInvoiceRequest returnValue = new RefundInvoiceRequest()
            {
                PlatFormId = skmPayInvoice.PlatFormId,
                PosId = skmPayInvoice.SellPosid,
                InvoiceDetails = new RefundInvoiceDetail[]{
                    new RefundInvoiceDetail()
                    {
                        SellNo = skmPayInvoice.SellNo,
                        SellShopId = skmPayInvoice.SellShopid,
                        StoreId = invoice6XStoreId,
                        SellOrderNo= skmPayInvoice.SellOrderNo,
                        InvoiceNo = skmPayInvoice.SellInvoice,
                        PosId = skmPayInvoice.SellPosid,
                        SellDay = skmPayInvoice.SellDay.ToString("yyyy/MM/dd"),
                        SellTime = skmPayInvoice.SellDay.ToString("HH:mm:ss"),
                        TradeDay = skmPayInvoice.Tday.ToString("yyyy/MM/dd"),
                        SellOriginalDay = skmPayInvoice.SellOrgDay.Value.ToString("yyyy/MM/dd"),
                        SellOriginalNo = skmPayInvoice.SellOrgNo,
                        SellOriginalPosId = skmPayInvoice.SellOrgPosid,
                        TotalAmount = skmPayInvoice.TotalAmt,
                        InvoiceAmount = skmPayInvoice.TotalSaleAmt,
                        TotalQuantity = skmPayInvoice.TotalQty,
                        DiscountAmount = skmPayInvoice.DiscAmt,
                        RateAmount = skmPayInvoice.RateAmt,
                        ExtraAmount = skmPayInvoice.ExtraAmt,
                        SellTransType = ((int)skmPayInvoice.SellTranstype).ToString().PadLeft(2, '0'),
                        VipNo = skmPayInvoice.VipNo,
                        CompanyId = skmPayInvoice.CompId,
                        EuiDonateNo = skmPayInvoice.EuiDonateNo,
                        EuiDonateStatus = skmPayInvoice.EuiDonateStatus ? 1 : 0,
                        EuiVehicleNo = skmPayInvoice.EuiVehicleNo,
                        EuiVehicleTypeNo = skmPayInvoice.EuiVehicleTypeNo,
                        EuiUniversalStatus = skmPayInvoice.EuiUniversalStatus ? 1 : 0,
                        VipAppPass = skmPayInvoice.VipAppPass,
                        ParkingToken = skmPayInvoice.ParkingToken,
                        PreDalesStatus = invoicePreSalesStatus,
                        SellDetails = new RefundInvoiceSellDetail[]{
                            new RefundInvoiceSellDetail()
                            {
                                SellDetailNo = skmPayInvoiceSellDetail.SellOrderNoS,
                                SellId = skmPayInvoiceSellDetail.SellId,
                                BrandCounterCode = skmPayInvoiceSellDetail.NdType,
                                StoreId = invoice6XStoreId,
                                Quantity = skmPayInvoiceSellDetail.SellQty,
                                Price = skmPayInvoiceSellDetail.SellPrice,
                                SellDiscount = skmPayInvoiceSellDetail.SellDiscount,
                                SellRate = (TurnCloudSellRateType)skmPayInvoiceSellDetail.SellRate,
                                SellRateAmount = skmPayInvoiceSellDetail.SellRateAmt,
                                SellAmount = skmPayInvoiceSellDetail.SellAmt,
                                ProductId = skmPayInvoiceSellDetail.ProductId,//Parking 固定填9900007090028
                                OriginalPrice = skmPayInvoiceSellDetail.SellOriPrice,
                                ProductDiscountType = Helper.GetEnumDescription((TurnCloudProductDiscountType)skmPayInvoiceSellDetail.ProductDiscountType),
                                PaymentType = skmPayInvoiceSellDetail.PmtType,
                                AddPoints = skmPayInvoiceSellDetail.PointAdd,
                                DeductPoints = skmPayInvoiceSellDetail.PointDeduct,
                                GoodsName = GetFixBytesByGoodsName(skmPayInvoiceSellDetail.GoodsName),
                            }
                        },
                        TenderDetails = tenderDetails.ToArray(),
                    }
                }
            };
            if (Config.IsUseTestVipNoAndShopCode)
            {
                returnValue.PosId = "355555";
                returnValue.InvoiceDetails.ForEach(item =>
                {
                    item.VipNo = "830800000211127";
                    item.SellShopId = "123";
                    item.PosId = "355555";
                    item.SellOriginalPosId = "355555";
                    item.SellDetails.ForEach(sellItem => { sellItem.BrandCounterCode = "7090001"; });
                    item.TenderDetails.ForEach(tenderItem => { tenderItem.BrandCounterCode = "7090001"; });
                });
            }
            returnValue.HashKey = GetHashKey(returnValue.InvoiceDetails[0].SellDay,
                                             returnValue.InvoiceDetails[0].SellOrderNo,
                                             returnValue.InvoiceDetails[0].PosId,
                                             returnValue.InvoiceDetails[0].SellShopId,
                                             returnValue.InvoiceDetails[0].SellTransType,
                                             invoiceHashKey);
            return returnValue;
        }

        /// <summary>
        /// 使用發票相關資訊產生核銷請求資訊
        /// </summary>
        /// <param name="skmPayInvoice">發票資訊</param>
        /// <param name="skmPayInvoiceSellDetail">發票銷售明細</param>
        /// <param name="skmPayInvoiceTenderDetails">發票付款明細</param>
        /// <returns>核銷請求資訊</returns>
        public static WriteOffInvoiceRequest GenerateWriteOffInvoiceRequest(SkmPayInvoice skmPayInvoice, SkmPayInvoiceSellDetail skmPayInvoiceSellDetail, IEnumerable<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails)
        {
            List<WriteOffInvoiceTenderDetail> tenderDetails = new List<WriteOffInvoiceTenderDetail>();
            foreach (SkmPayInvoiceTenderDetail skmPayInvoiceTenderDetail in skmPayInvoiceTenderDetails)
            {
                tenderDetails.Add(new WriteOffInvoiceTenderDetail()
                {
                    PayOrderNo = skmPayInvoiceTenderDetail.PayOrderNo,
                    BrandCounterCode = skmPayInvoiceSellDetail.NdType,
                    StoreId = Config.EnableTurnCloudInvoice6X ? skmPayInvoice.StoreId : string.Empty, //Parking 7X櫃號：7090028 6x為6090017
                    SecNo = skmPayInvoiceTenderDetail.SecNo,
                    SellTender = skmPayInvoiceTenderDetail.SellTender,
                    TenderPrice = skmPayInvoiceTenderDetail.TenderPrice,
                    TotalAddPoint = skmPayInvoiceTenderDetail.PointAddTotal,
                    TotalDeductPoint = skmPayInvoiceTenderDetail.PointDeductTotal,
                    CreditCardDetail = new WriteOffInvoiceCreditCardDetail()
                    {
                        CardNo = skmPayInvoiceTenderDetail.CardNo,
                        IsUseBonus = (skmPayInvoiceTenderDetail.BonusUsePoint.HasValue &&
                                      skmPayInvoiceTenderDetail.BonusUsePoint.Value > 0) ? "Y" : "N",
                        CardNo2 = skmPayInvoiceTenderDetail.CardNo2,
                        TransAmount = skmPayInvoiceTenderDetail.TenderPrice,
                        BonusUsePoint = skmPayInvoiceTenderDetail.BonusUsePoint.HasValue ? skmPayInvoiceTenderDetail.BonusUsePoint.Value : 0,
                        BonusDiscountAmount = skmPayInvoiceTenderDetail.BonusDisAmt.HasValue ? skmPayInvoiceTenderDetail.BonusDisAmt.Value : 0,
                        BonusPoint = skmPayInvoiceTenderDetail.BonusPoint.HasValue ? skmPayInvoiceTenderDetail.BonusPoint.Value : 0,
                        //分期
                        Periods = skmPayInvoiceTenderDetail.Periods ?? 0,
                        EachAmount = skmPayInvoiceTenderDetail.EachAmt ?? 0,
                        FirstAmount = skmPayInvoiceTenderDetail.HandAmt ?? 0
                    }
                });
            }

            WriteOffInvoiceRequest returnValue = new WriteOffInvoiceRequest()
            {
                PlatFormId = "7",
                PosId = skmPayInvoice.SellPosid,
                InvoiceDetails = new WriteOffInvoiceDetail[]{
                    new WriteOffInvoiceDetail()
                    {
                        SellNo = skmPayInvoice.SellNo,
                        SellShopId = skmPayInvoice.SellShopid,
                        SellOrderNo= skmPayInvoice.SellOrderNo,
                        PosId = skmPayInvoice.SellPosid,
                        SellDay = skmPayInvoice.SellDay.ToString("yyyy/MM/dd"),
                        SellTime = skmPayInvoice.SellDay.ToString("HH:mm:ss"),
                        StoreId = Config.EnableTurnCloudInvoice6X ? skmPayInvoice.StoreId : string.Empty, //Parking 7X櫃號：7090028 6x為6090017
                        TradeDay = skmPayInvoice.Tday.ToString("yyyy/MM/dd"),
                        SellOriginalDay = skmPayInvoice.SellOrgDay.Value.ToString("yyyy/MM/dd"),
                        SellOriginalNo = skmPayInvoice.SellOrgNo,
                        SellOriginalPosId = skmPayInvoice.SellOrgPosid,
                        TotalAmount = skmPayInvoice.TotalAmt,
                        InvoiceAmount = skmPayInvoice.TotalSaleAmt,
                        TotalQuantity = skmPayInvoice.TotalQty,
                        DiscountAmount = skmPayInvoice.DiscAmt,
                        RateAmount = skmPayInvoice.RateAmt,
                        InvoiceNo = skmPayInvoice.SellInvoice,
                        ExtraAmount = skmPayInvoice.ExtraAmt,
                        SellTransType = ((int)skmPayInvoice.SellTranstype).ToString().PadLeft(2, '0'),
                        VipNo = skmPayInvoice.VipNo,
                        VipAppPass = skmPayInvoice.VipAppPass,
                        PreDalesStatus = (TurnCloudPreDalesStatus)skmPayInvoice.PreSalesStatus,//Parking 填空白
                        SellDetails = new WriteOffInvoiceSellDetail[]{
                            new WriteOffInvoiceSellDetail()
                            {
                                SellId = skmPayInvoiceSellDetail.SellId,
                                BrandCounterCode = skmPayInvoiceSellDetail.NdType,
                                StoreId = Config.EnableTurnCloudInvoice6X ? skmPayInvoice.StoreId : string.Empty, //Parking 7X櫃號：7090028 6x為6090017
                                Quantity = skmPayInvoiceSellDetail.SellQty,
                                Price = skmPayInvoiceSellDetail.SellPrice,
                                SellDiscount = skmPayInvoiceSellDetail.SellDiscount,
                                SellRate = (TurnCloudSellRateType)skmPayInvoiceSellDetail.SellRate,
                                SellRateAmount = skmPayInvoiceSellDetail.SellRateAmt,
                                SellAmount = skmPayInvoiceSellDetail.SellAmt,
                                ProductId = skmPayInvoiceSellDetail.ProductId,//Parking 固定填9900007090028
                                OriginalPrice = skmPayInvoiceSellDetail.SellOriPrice,
                                ProductDiscountType = Helper.GetEnumDescription((TurnCloudProductDiscountType)skmPayInvoiceSellDetail.ProductDiscountType),
                                PaymentType = skmPayInvoiceSellDetail.PmtType,
                                AddPoints = skmPayInvoiceSellDetail.PointAdd,
                                DeductPoints = skmPayInvoiceSellDetail.PointDeduct,
                                GoodsName = GetFixBytesByGoodsName(skmPayInvoiceSellDetail.GoodsName)
                            }
                        },
                        TenderDetails = tenderDetails.ToArray(),
                    }
                }
            };
            if (Config.IsUseTestVipNoAndShopCode)
            {
                returnValue.PosId = "355555";
                returnValue.InvoiceDetails.ForEach(item =>
                {
                    item.VipNo = "830800000211127";
                    item.SellShopId = "123";
                    item.PosId = "355555";
                    item.SellOriginalPosId = "355555";
                    item.SellDetails.ForEach(sellItem => { sellItem.BrandCounterCode = "7090001"; });
                    item.TenderDetails.ForEach(tenderItem => { tenderItem.BrandCounterCode = "7090001"; });
                });
            }
            returnValue.HashKey = GetHashKey(returnValue.InvoiceDetails[0].SellDay,
                                             returnValue.InvoiceDetails[0].SellOrderNo,
                                             returnValue.InvoiceDetails[0].PosId,
                                             returnValue.InvoiceDetails[0].SellShopId,
                                             returnValue.InvoiceDetails[0].SellTransType,
                                             (skmPayInvoice.PlatFormId == "P")
                                                 ? Config.TurnCloudServerParkingHashKey
                                                 : Config.TurnCloudServerHashKey);

            return returnValue;
        }

        /// <summary>
        /// 使用發票資訊清單產生每日[精選優惠]銷售總額請求資訊
        /// 因為搜尋條件太多, 就不跟停車寫在同一個
        /// </summary>
        /// <param name="viewSkmPayClearPosList">發票資訊清單</param>
        /// <returns>每日銷售總額請求資訊</returns>
        public static List<ClearPosRequest> GenerateClearPosRequests(IEnumerable<ViewSkmPayClearPos> viewSkmPayClearPosList)
        {
            List<ClearPosRequest> returnValue = new List<ClearPosRequest>();
            if (!viewSkmPayClearPosList.Any())
            {
                return returnValue;
            }

            //這邊不敢整理重複的程式碼，因為怕難保哪天又改計算邏輯
            //先取出不同的館號，需要依館號傳送API
            List<string> shopIds = viewSkmPayClearPosList.Select(item => item.SellShopid).Distinct().ToList();
            foreach (string shopId in shopIds)
            {
                List<ViewSkmPayClearPos> tempSkmPayInvoiceList = viewSkmPayClearPosList.Where(item => item.SellShopid == shopId).ToList();
                List<ClearPosDetail> clearPosDetails = new List<ClearPosDetail>();
                //T001: 一般交易筆數
                if (tempSkmPayInvoiceList.Any(item => item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale && item.SellTranstype == (int)TurnCloudSellTransType.Sell))
                {
                    tempSkmPayInvoiceList
                    .Where(item => item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale && item.SellTranstype == (int)TurnCloudSellTransType.Sell)
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = (Config.EnableTurnCloudInvoice6X)
                                ? item.FirstOrDefault().StoreId
                                : string.Empty, //6x
                            ClearDataType = "T001",
                            BrandCounterCode = item.Key,
                            Quantity = item.Select(subItem => subItem.SkmPayOrderId).Distinct().Count(),
                            Amount = 0
                        });
                    });
                }
                //T003: 退貨交易筆數
                if (tempSkmPayInvoiceList.Any(item => item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                {
                    tempSkmPayInvoiceList
                    .Where(item => item.SellTranstype == (int)TurnCloudSellTransType.Refund)
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = (Config.EnableTurnCloudInvoice6X)
                                ? item.FirstOrDefault().StoreId
                                : string.Empty, //6x
                            ClearDataType = "T003",
                            BrandCounterCode = item.Key,
                            Quantity = item.Select(subItem => subItem.SkmPayOrderId).Distinct().Count(),
                            Amount = 0
                        });
                    });
                }
                //T004: 應稅合計
                if (tempSkmPayInvoiceList.Any(item => item.SellRate == (int)TurnCloudSellRateType.Taxable &&
                                                      item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell))
                {
                    tempSkmPayInvoiceList
                    .Where(item => item.SellRate == (int)TurnCloudSellRateType.Taxable &&
                                   item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                   item.SellTranstype == (int)TurnCloudSellTransType.Sell)
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = (Config.EnableTurnCloudInvoice6X)
                                ? item.FirstOrDefault().StoreId
                                : string.Empty, //6x
                            ClearDataType = "T004",
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.GroupBy(subItem => subItem.SkmPayOrderDetailId)
                                         .Select(subItem => subItem.First())
                                         .Sum(subItem => subItem.GoodPrice)
                        });
                    });
                }
                //T005: 免稅合計
                if (tempSkmPayInvoiceList.Any(item => item.SellRate == (int)TurnCloudSellRateType.Free &&
                                                      item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell))
                {
                    tempSkmPayInvoiceList
                    .Where(item => item.SellRate == (int)TurnCloudSellRateType.Free &&
                                   item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                   item.SellTranstype == (int)TurnCloudSellTransType.Sell)
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = (Config.EnableTurnCloudInvoice6X)
                                ? item.FirstOrDefault().StoreId
                                : string.Empty, //6x
                            ClearDataType = "T005",
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.GroupBy(subItem => subItem.SkmPayOrderDetailId)
                                         .Select(subItem => subItem.First())
                                         .Sum(subItem => subItem.GoodPrice)
                        });
                    });
                }
                //目前預售跟退貨代碼一樣，所以一起計算
                //41:支付工具-信用卡聯合連線
                if (tempSkmPayInvoiceList.Any(item => (item.SellTender == TurnCloudSellTenderType.NationalCreditCardCenter ||
                                                      item.SellTender == TurnCloudSellTenderType.NationalCreditCardCenterRefund) &&
                                                      ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                        item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                                       item.SellTranstype == (int)TurnCloudSellTransType.Refund)))
                {
                    tempSkmPayInvoiceList
                    .Where(item => (item.SellTender == TurnCloudSellTenderType.NationalCreditCardCenter ||
                                    item.SellTender == TurnCloudSellTenderType.NationalCreditCardCenterRefund) &&
                                    ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                     item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = (Config.EnableTurnCloudInvoice6X)
                                ? item.FirstOrDefault().StoreId
                                : string.Empty, //6x
                            ClearDataType = TurnCloudSellTenderType.NationalCreditCardCenter,
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.Where(saleItem => saleItem.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                            saleItem.SellTranstype == (int)TurnCloudSellTransType.Sell)
                                         .Sum(saleItem => saleItem.TenderPrice) -
                                     item.Where(refundItem => refundItem.SellTranstype == (int)TurnCloudSellTransType.Refund)
                                         .Sum(refundItem => refundItem.TenderPrice)
                        });
                    });
                }
                //43:支付工具-信用卡台新連線
                if (tempSkmPayInvoiceList.Any(item => (item.SellTender == TurnCloudSellTenderType.TaishinCreditCard ||
                                                      item.SellTender == TurnCloudSellTenderType.TaishinCreditCardRefund) &&
                                                      ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                        item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                                       item.SellTranstype == (int)TurnCloudSellTransType.Refund)))
                {
                    tempSkmPayInvoiceList
                    .Where(item => (item.SellTender == TurnCloudSellTenderType.TaishinCreditCard ||
                                    item.SellTender == TurnCloudSellTenderType.TaishinCreditCardRefund) &&
                                    ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                     item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = (Config.EnableTurnCloudInvoice6X)
                                ? item.FirstOrDefault().StoreId
                                : string.Empty, //6x
                            ClearDataType = TurnCloudSellTenderType.TaishinCreditCard,
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.Where(saleItem => saleItem.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                            saleItem.SellTranstype == (int)TurnCloudSellTransType.Sell)
                                         .Sum(saleItem => saleItem.TenderPrice) -
                                     item.Where(refundItem => refundItem.SellTranstype == (int)TurnCloudSellTransType.Refund)
                                         .Sum(refundItem => refundItem.TenderPrice)
                        });
                    });
                }
                //8F:支付工具-SKM PAY
                if (tempSkmPayInvoiceList.Any(item => (item.SellTender == TurnCloudSellTenderType.SkmPay ||
                                                      item.SellTender == TurnCloudSellTenderType.SkmPayRefund) &&
                                                      ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                        item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                                       item.SellTranstype == (int)TurnCloudSellTransType.Refund)))
                {
                    tempSkmPayInvoiceList
                    .Where(item => (item.SellTender == TurnCloudSellTenderType.SkmPay ||
                                    item.SellTender == TurnCloudSellTenderType.SkmPayRefund) &&
                                    ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                     item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = (Config.EnableTurnCloudInvoice6X)
                                ? item.FirstOrDefault().StoreId
                                : string.Empty, //6x
                            ClearDataType = TurnCloudSellTenderType.SkmPay,
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.Where(saleItem => saleItem.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                            saleItem.SellTranstype == (int)TurnCloudSellTransType.Sell)
                                         .Sum(saleItem => saleItem.TenderPrice) -
                                     item.Where(refundItem => refundItem.SellTranstype == (int)TurnCloudSellTransType.Refund)
                                         .Sum(refundItem => refundItem.TenderPrice)
                        });
                    });
                }
                //83:支付工具-會員點數扣點
                if (tempSkmPayInvoiceList.Any(item => (item.SellTender == TurnCloudSellTenderType.MemberPointsDeduct ||
                                                      item.SellTender == TurnCloudSellTenderType.MemberPointsDeductRefund) &&
                                                      ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                        item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                                       item.SellTranstype == (int)TurnCloudSellTransType.Refund)))
                {
                    tempSkmPayInvoiceList
                    .Where(item => (item.SellTender == TurnCloudSellTenderType.MemberPointsDeduct ||
                                    item.SellTender == TurnCloudSellTenderType.MemberPointsDeductRefund) &&
                                    ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                     item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = (Config.EnableTurnCloudInvoice6X)
                                ? item.FirstOrDefault().StoreId
                                : string.Empty, //6x
                            ClearDataType = TurnCloudSellTenderType.MemberPointsDeduct,
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.Where(saleItem => saleItem.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                            saleItem.SellTranstype == (int)TurnCloudSellTransType.Sell)
                                         .Sum(saleItem => saleItem.TenderPrice) -
                                     item.Where(refundItem => refundItem.SellTranstype == (int)TurnCloudSellTransType.Refund)
                                         .Sum(refundItem => refundItem.TenderPrice)
                        });
                    });
                }

                //8G:支付工具-Skm Pay NCCC聯合
                if (tempSkmPayInvoiceList.Any(item => (item.SellTender == TurnCloudSellTenderType.SkmPayNCCC ||
                                                       item.SellTender == TurnCloudSellTenderType.SkmPayNCCCRefund) &&
                                                      ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                        item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                                       item.SellTranstype == (int)TurnCloudSellTransType.Refund)))
                {
                    tempSkmPayInvoiceList
                    .Where(item => (item.SellTender == TurnCloudSellTenderType.SkmPayNCCC ||
                                    item.SellTender == TurnCloudSellTenderType.SkmPayNCCCRefund) &&
                                    ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                     item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = (Config.EnableTurnCloudInvoice6X)
                                ? item.FirstOrDefault().StoreId
                                : string.Empty, //6x
                            ClearDataType = TurnCloudSellTenderType.SkmPayNCCC,
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.Where(saleItem => saleItem.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                            saleItem.SellTranstype == (int)TurnCloudSellTransType.Sell)
                                         .Sum(saleItem => saleItem.TenderPrice) -
                                     item.Where(refundItem => refundItem.SellTranstype == (int)TurnCloudSellTransType.Refund)
                                         .Sum(refundItem => refundItem.TenderPrice)
                        });
                    });
                }
                ViewSkmPayClearPos skmPayInvoice = tempSkmPayInvoiceList.First();
                ClearPosRequest clearPosRequest = new ClearPosRequest()
                {
                    PlatFormId = "7",
                    ShopId = skmPayInvoice.SellShopid,
                    PosId = skmPayInvoice.SellPosid,
                    UploadDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"),
                    SellDate = skmPayInvoice.SellDay.ToString("yyyy/MM/dd"),
                    ClearPosDetails = clearPosDetails.ToArray()
                };
                returnValue.Add(clearPosRequest);
            }
            return returnValue;

        }

        /// <summary>
        /// 使用發票資訊清單產生每日[停車]銷售總額請求資訊
        /// 因為搜尋條件太多, 就不跟精選優惠寫在同一個
        /// </summary>
        /// <param name="viewSkmPayClearPosList">發票資訊清單</param>
        /// <param name="platFormId">哪一個平台編號</param>
        /// <returns>每日銷售總額請求資訊</returns>
        public static List<ClearPosRequest> GenerateParkingClearPosRequests(IEnumerable<ViewSkmPayClearPos> viewSkmPayClearPosList)
        {
            List<ClearPosRequest> returnValue = new List<ClearPosRequest>();
            if (!viewSkmPayClearPosList.Any())
            {
                return returnValue;
            }

            //這邊不敢整理重複的程式碼，因為怕難保哪天又改計算邏輯
            //先取出不同的館號，需要依館號傳送API
            List<string> shopIds = viewSkmPayClearPosList.Select(item => item.SellShopid).Distinct().ToList();
            foreach (string shopId in shopIds)
            {
                List<ViewSkmPayClearPos> tempSkmPayInvoiceList = viewSkmPayClearPosList.Where(item => item.SellShopid == shopId).ToList();

                List<ClearPosDetail> clearPosDetails = new List<ClearPosDetail>();
                //T001: 一般交易筆數
                if (tempSkmPayInvoiceList.Any(item => item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default && item.SellTranstype == (int)TurnCloudSellTransType.Sell))
                {
                    tempSkmPayInvoiceList
                    .Where(item => item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default && item.SellTranstype == (int)TurnCloudSellTransType.Sell)
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = item.FirstOrDefault().StoreId, //6x
                            ClearDataType = "T001",
                            BrandCounterCode = item.Key,
                            Quantity = item.Select(subItem => subItem.SkmPayOrderId).Distinct().Count(),
                            Amount = 0
                        });
                    });
                }
                //T003: 退貨交易筆數
                if (tempSkmPayInvoiceList.Any(item => item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                {
                    tempSkmPayInvoiceList
                    .Where(item => item.SellTranstype == (int)TurnCloudSellTransType.Refund)
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = item.FirstOrDefault().StoreId, //6x
                            ClearDataType = "T003",
                            BrandCounterCode = item.Key,
                            Quantity = item.Select(subItem => subItem.SkmPayOrderId).Distinct().Count(),
                            Amount = 0
                        });
                    });
                }
                //T004: 應稅合計
                if (tempSkmPayInvoiceList.Any(item => item.SellRate == (int)TurnCloudSellRateType.Taxable &&
                                                      item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell))
                {
                    tempSkmPayInvoiceList
                    .Where(item => item.SellRate == (int)TurnCloudSellRateType.Taxable &&
                                   item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                   item.SellTranstype == (int)TurnCloudSellTransType.Sell)
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = item.FirstOrDefault().StoreId, //6x
                            ClearDataType = "T004",
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.GroupBy(subItem => subItem.SkmPayOrderDetailId)
                                         .Select(subItem => subItem.First())
                                         .Sum(subItem => subItem.GoodPrice)
                        });
                    });
                }
                //T005: 免稅合計
                if (tempSkmPayInvoiceList.Any(item => item.SellRate == (int)TurnCloudSellRateType.Free &&
                                                      item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell))
                {
                    tempSkmPayInvoiceList
                    .Where(item => item.SellRate == (int)TurnCloudSellRateType.Free &&
                                   item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                   item.SellTranstype == (int)TurnCloudSellTransType.Sell)
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = item.FirstOrDefault().StoreId, //6x
                            ClearDataType = "T005",
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.GroupBy(subItem => subItem.SkmPayOrderDetailId)
                                         .Select(subItem => subItem.First())
                                         .Sum(subItem => subItem.GoodPrice)
                        });
                    });
                }
                //目前預售跟退貨代碼一樣，所以一起計算
                //41:支付工具-信用卡聯合連線
                if (tempSkmPayInvoiceList.Any(item => (item.SellTender == TurnCloudSellTenderType.NationalCreditCardCenter ||
                                                      item.SellTender == TurnCloudSellTenderType.NationalCreditCardCenterRefund) &&
                                                      ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                        item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                                       item.SellTranstype == (int)TurnCloudSellTransType.Refund)))
                {
                    tempSkmPayInvoiceList
                    .Where(item => (item.SellTender == TurnCloudSellTenderType.NationalCreditCardCenter ||
                                    item.SellTender == TurnCloudSellTenderType.NationalCreditCardCenterRefund) &&
                                    ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                     item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = item.FirstOrDefault().StoreId, //6x
                            ClearDataType = TurnCloudSellTenderType.NationalCreditCardCenter,
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.Where(saleItem => saleItem.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                            saleItem.SellTranstype == (int)TurnCloudSellTransType.Sell)
                                         .Sum(saleItem => saleItem.TenderPrice) -
                                     item.Where(refundItem => refundItem.SellTranstype == (int)TurnCloudSellTransType.Refund)
                                         .Sum(refundItem => refundItem.TenderPrice)
                        });
                    });
                }
                //43:支付工具-信用卡台新連線
                if (tempSkmPayInvoiceList.Any(item => (item.SellTender == TurnCloudSellTenderType.TaishinCreditCard ||
                                                      item.SellTender == TurnCloudSellTenderType.TaishinCreditCardRefund) &&
                                                      ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                        item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                                       item.SellTranstype == (int)TurnCloudSellTransType.Refund)))
                {
                    tempSkmPayInvoiceList
                    .Where(item => (item.SellTender == TurnCloudSellTenderType.TaishinCreditCard ||
                                    item.SellTender == TurnCloudSellTenderType.TaishinCreditCardRefund) &&
                                    ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                     item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = item.FirstOrDefault().StoreId, //6x
                            ClearDataType = TurnCloudSellTenderType.TaishinCreditCard,
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.Where(saleItem => saleItem.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                            saleItem.SellTranstype == (int)TurnCloudSellTransType.Sell)
                                         .Sum(saleItem => saleItem.TenderPrice) -
                                     item.Where(refundItem => refundItem.SellTranstype == (int)TurnCloudSellTransType.Refund)
                                         .Sum(refundItem => refundItem.TenderPrice)
                        });
                    });
                }
                //8F:支付工具-SKM PAY
                if (tempSkmPayInvoiceList.Any(item => (item.SellTender == TurnCloudSellTenderType.SkmPay ||
                                                      item.SellTender == TurnCloudSellTenderType.SkmPayRefund) &&
                                                      ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                        item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                                       item.SellTranstype == (int)TurnCloudSellTransType.Refund)))
                {
                    tempSkmPayInvoiceList
                    .Where(item => (item.SellTender == TurnCloudSellTenderType.SkmPay ||
                                    item.SellTender == TurnCloudSellTenderType.SkmPayRefund) &&
                                    ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                     item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = item.FirstOrDefault().StoreId, //6x
                            ClearDataType = TurnCloudSellTenderType.SkmPay,
                            BrandCounterCode = item.Key,
                            Quantity = 0,
                            Amount = item.Where(saleItem => saleItem.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                            saleItem.SellTranstype == (int)TurnCloudSellTransType.Sell)
                                         .Sum(saleItem => saleItem.TenderPrice) -
                                     item.Where(refundItem => refundItem.SellTranstype == (int)TurnCloudSellTransType.Refund)
                                         .Sum(refundItem => refundItem.TenderPrice)
                        });
                    });
                }
                //83:支付工具-會員點數扣點
                if (tempSkmPayInvoiceList.Any(item => (item.SellTender == TurnCloudSellTenderType.MemberPointsDeduct ||
                                                      item.SellTender == TurnCloudSellTenderType.MemberPointsDeductRefund) &&
                                                      ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                        item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                                       item.SellTranstype == (int)TurnCloudSellTransType.Refund)))
                {
                    tempSkmPayInvoiceList
                    .Where(item => (item.SellTender == TurnCloudSellTenderType.MemberPointsDeduct ||
                                    item.SellTender == TurnCloudSellTenderType.MemberPointsDeductRefund) &&
                                    ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                     item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = item.FirstOrDefault().StoreId, //6x
                            BrandCounterCode = item.Key,
                            ClearDataType = TurnCloudSellTenderType.MemberPointsDeduct,
                            Quantity = 0,
                            Amount = item.Where(saleItem => saleItem.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                            saleItem.SellTranstype == (int)TurnCloudSellTransType.Sell)
                                         .Sum(saleItem => saleItem.TenderPrice) -
                                     item.Where(refundItem => refundItem.SellTranstype == (int)TurnCloudSellTransType.Refund)
                                         .Sum(refundItem => refundItem.TenderPrice)
                        });
                    });
                }

                //8G:支付工具-SKM PAY NCCC聯合
                if (tempSkmPayInvoiceList.Any(item => (item.SellTender == TurnCloudSellTenderType.SkmPayNCCC ||
                                                      item.SellTender == TurnCloudSellTenderType.SkmPayNCCCRefund) &&
                                                      ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                        item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                                       item.SellTranstype == (int)TurnCloudSellTransType.Refund)))
                {
                    tempSkmPayInvoiceList
                    .Where(item => (item.SellTender == TurnCloudSellTenderType.SkmPayNCCC ||
                                    item.SellTender == TurnCloudSellTenderType.SkmPayNCCCRefund) &&
                                    ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                      item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                                     item.SellTranstype == (int)TurnCloudSellTransType.Refund))
                    .GroupBy(skmPayClearPos => skmPayClearPos.NdType)
                    .ForEach(item =>
                    {
                        clearPosDetails.Add(new ClearPosDetail()
                        {
                            BrandCounterCode6X = item.FirstOrDefault().StoreId, //6x
                            BrandCounterCode = item.Key,
                            ClearDataType = TurnCloudSellTenderType.SkmPayNCCC,
                            Quantity = 0,
                            Amount = item.Where(saleItem => saleItem.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                            saleItem.SellTranstype == (int)TurnCloudSellTransType.Sell)
                                         .Sum(saleItem => saleItem.TenderPrice) -
                                     item.Where(refundItem => refundItem.SellTranstype == (int)TurnCloudSellTransType.Refund)
                                         .Sum(refundItem => refundItem.TenderPrice)
                        });
                    });
                }


                ViewSkmPayClearPos skmPayInvoice = tempSkmPayInvoiceList.First();
                ClearPosRequest clearPosRequest = new ClearPosRequest()
                {
                    HashKey = Config.TurnCloudServerParkingHashKey,
                    PlatFormId = "P",
                    ShopId = skmPayInvoice.SellShopid,
                    PosId = skmPayInvoice.SellPosid,
                    UploadDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"),
                    SellDate = skmPayInvoice.SellDay.ToString("yyyy/MM/dd"),
                    ClearPosDetails = clearPosDetails.ToArray()
                };
                returnValue.Add(clearPosRequest);
            }
            return returnValue;

        }

        #endregion

        /// <summary>
        /// 依發票載具類型取得騰雲EuiVehicleTypeNo欄位值
        /// </summary>
        /// <param name="input">發票載具類型</param>
        /// <returns>EuiVehicleTypeNo欄位值</returns>
        public static string GetEuiVehicleTypeNo(SkmPayInvoiceCarrierType invoiceType)
        {
            string returnValue = "";
            switch (invoiceType)
            {
                case SkmPayInvoiceCarrierType.SkmMember:
                    returnValue = TurnCloudEuiVehicleTypeNo.SkmMember;
                    break;
                case SkmPayInvoiceCarrierType.Mobile:
                    returnValue = TurnCloudEuiVehicleTypeNo.Mobile;
                    break;
                case SkmPayInvoiceCarrierType.PersonalCertificate:
                    returnValue = TurnCloudEuiVehicleTypeNo.PersonalCertificate;
                    break;
                case SkmPayInvoiceCarrierType.Donation:
                case SkmPayInvoiceCarrierType.ElectronicInvoiceCertificate:
                case SkmPayInvoiceCarrierType.None:
                    //回傳空值
                    break;
                default:
                    throw new InvalidOperationException("enum:SkmPayInvoiceCarrierType not defined");
            }
            return returnValue;
        }

        private static T Post<T, K>(K requestModel, string uri)
        {
            System.Diagnostics.Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();
            int httpStatusCode = 0;
            var requestJson = JsonConvert.SerializeObject(requestModel);
            T returnValue = default(T);
            try
            {
                using (HttpClient httpClient = new HttpClient())
                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, uri))
                {
                    httpClient.BaseAddress = new Uri(Config.TurnCloudServerSiteUrl);
                    httpRequestMessage.Content = new StringContent(requestJson, Encoding.UTF8, "application/json");
                    using (HttpResponseMessage responseMessage = httpClient.SendAsync(httpRequestMessage).Result)
                    {
                        httpStatusCode = (int)responseMessage.StatusCode;
                        if (responseMessage.IsSuccessStatusCode)
                        {
                            string responseBody = responseMessage.Content.ReadAsStringAsync().Result;
                            returnValue = JsonConvert.DeserializeObject<T>(responseBody);
                        }
                        else
                        {
                            throw new AggregateException(responseMessage.ReasonPhrase);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SetApiLog(watch, httpStatusCode, uri, requestJson, returnValue);
                logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                throw ex;
            }

            SetApiLog(watch, httpStatusCode, uri, requestJson, returnValue);
            return returnValue;
        }

        /// <summary>
        /// 寫Api Log
        /// </summary>
        /// <param name="watch">Stopwatch</param>
        /// <param name="httpStatusCode">http Status Code</param>
        /// <param name="uri">呼叫的方法(controller+action)</param>
        /// <param name="requestJson">請求內容</param>
        /// <param name="returnValue">回應內容</param>
        private static void SetApiLog(System.Diagnostics.Stopwatch watch, int httpStatusCode, string uri, string requestJson, object returnValue)
        {
            watch.Stop();
            var memo = string.Format("{0}, {1}", httpStatusCode, watch.Elapsed.TotalSeconds);
            requestJson = new Regex("(\"Vip_app_pass\":\"\\w*\")").Replace(requestJson, "");
            SystemFacade.SetApiLog(Config.SkmWalletSiteUrl + uri, memo, requestJson, returnValue);
            try
            {
                var results = JsonConvert.SerializeObject(returnValue);
                SkmFacade.SkmLog(memo, 0, SkmLogType.TurnCloudServer, requestJson, results);
            }
            catch (Exception ex)
            {
                logger.Info(ex);
            }
        }

        /// <summary>
        /// 依帶入的參數取得HashKey
        /// </summary>
        /// <param name="sellDay">sellDay</param>
        /// <param name="sellOrderNo">sellOrderNo</param>
        /// <param name="sellPosId">sellPosId</param>
        /// <param name="sellShopId">sellShopId</param>
        /// <param name="sellTransType">sellTransType</param>
        /// <returns>依帶入的參數取得HashKey</returns>
        private static string GetHashKey(string sellDay, string sellOrderNo, string sellPosId, string sellShopId, string sellTransType, string hashKey)
        {
            string returnValue = string.Empty;
            SHA256 sHA256 = SHA256.Create();
            string hashKeyFormat = hashKey + "sell_day={0}&sell_order_no={1}&sell_posid={2}&sell_shopid={3}&sell_transtype={4}";
            byte[] byteText = Encoding.UTF8.GetBytes(string.Format(hashKeyFormat, new string[] { sellDay, sellOrderNo, sellPosId, sellShopId, sellTransType }));
            byte[] encrytpByte = sHA256.ComputeHash(byteText);
            for (int i = 0; i < encrytpByte.Length; i++)
            {
                returnValue += encrytpByte[i].ToString("x2");
            }
            return returnValue;
        }
    }
}

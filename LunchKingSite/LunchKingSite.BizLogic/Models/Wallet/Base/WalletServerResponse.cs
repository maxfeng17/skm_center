﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.BizLogic.Models.Wallet
{
    /// <summary> Wallet Server 回應基本資訊 </summary>
    public class WalletServerResponse<T> where T : new()
    {
        private string msg = string.Empty;
        /// <summary> 回應代碼 </summary>
        [JsonProperty(PropertyName = "code")]
        public WalletReturnCode Code { get; set; }
        /// <summary> 回應訊息 </summary>
        [JsonProperty(PropertyName = "data")]
        public T Data { get; set; }        
        /// <summary> 回應訊息 </summary>
        [JsonProperty(PropertyName = "message")]
        public string Message {
            get
            {
                return string.IsNullOrEmpty(msg) ? Helper.GetEnumDescription(Code) : msg;
            }
            set
            {
                msg = value;
            }
        }
    }
}

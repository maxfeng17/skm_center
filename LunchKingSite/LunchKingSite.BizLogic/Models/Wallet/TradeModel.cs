﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LunchKingSite.BizLogic.Models.Wallet
{
    /// <summary> Wallet [EC交易] 預備交易登記請求資訊 </summary>
    [Serializable]
    public class WalletGetPreTransNoRequest
    {
        /// <summary> 館別代號 </summary>
        public string StoreId { get; set; }
        /// <summary> 館別代號 </summary>
        public List<WalletGetPreTransNoTransItem> TransItems { get; set; }
        /// <summary> 備註欄位1 </summary>
        public string Remark1 { get; set; }
        /// <summary> 備註欄位2 </summary>
        public string Remark2 { get; set; }
        /// <summary> 備註欄位3 </summary>
        public string Remark3 { get; set; }

    }

    /// <summary> 交易商品資訊 </summary>
    [Serializable]
    public class WalletGetPreTransNoTransItem
    {
        /// <summary> 商品名稱 </summary>
        public string ItemName { get; set; }
        /// <summary> 單價 </summary>
        public int Price { get; set; }
        /// <summary> 數量 </summary>
        public int Quantity { get; set; }
        /// <summary> 小計金額 (單價*數量) </summary>
        public int Amount { get; set; }
        /// <summary> 商品圖片 </summary>
        public string PicUrl { get; set; }
        /// <summary> 扣點數量(沒有填0) </summary>
        public int Point { get; set; }
    }

    /// <summary> Wallet [EC交易] 預備交易登記回應資訊 </summary>
    public class WalletGetPreTransNoResponse
    {
        /// <summary> 預備交易編號 </summary>
        [JsonProperty(PropertyName = "preTransNo")]
        public string PreTransNo { get; set; }
    }



    /// <summary>
    /// Wallet [EC交易] 授權請款/退貨/取消退貨交易/確認訂單 共用 Request
    /// </summary>
    public class WalletAuthPaymentRequest
    {
        /// <summary>
        /// 付款Token
        /// </summary>q84
        [JsonProperty(PropertyName = "paymentToken")]
        public string PaymentToken { get; set; }
        /// <summary>
        /// SKM 交易序號(SkmTransId)
        /// </summary>
        [JsonProperty(PropertyName = "ecOrderId")]
        public string EcOrderId { get; set; }
        /// <summary>
        /// 授權版本
        /// </summary>
        [JsonProperty(PropertyName = "authVersion")]
        public int AuthVersion { get; set; }
        public WalletAuthPaymentRequest() { }
        public WalletAuthPaymentRequest(string ecOrderId)
        {
            EcOrderId = ecOrderId;
        }
    }

    /// <summary>
    /// Wallet [EC交易] 授權請款/退貨/取消退貨交易/確認訂單 共用 Response
    /// </summary>
    public class WalletPaymentResponse
    {
        /// <summary>
        /// appos 交易編號
        /// </summary>
        public string ApposTradeNo { get; set; }
        /// <summary>
        /// 交易編號 (skm_trans_id)
        /// </summary>
        public string TradeNo { get; set; }
        /// <summary>
        /// 付款商店
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 付款時間
        /// </summary>
        public string TradeTime { get; set; }
        /// <summary>
        /// 交易金額
        /// </summary>
        public int TradeAmount { get; set; }
        /// <summary>
        /// 付款金額
        /// </summary>
        public int PayAmount { get; set; }
        /// <summary>
        /// 紅利折抵金額
        /// </summary>
        public int RedeemAmt { get; set; }
        /// <summary>
        /// 剩餘紅利點數
        /// </summary>
        public int PostRedeemPt { get; set; }
        /// <summary>
        /// 紅利折抵點數
        /// </summary>
        public int RedeemPt { get; set; }
        /// <summary>
        /// 交易結果是否成功
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// 交易結果中文
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 信用卡授權碼
        /// </summary>
        public string AuthIdResp { get; set; }
        /// <summary>
        /// 信用卡名稱
        /// </summary>
        public string CardName { get; set; }
        /// <summary>
        /// 信用卡隱碼
        /// </summary>
        public string CardNumber { get; set; }
        /// <summary>
        /// 會員點數
        /// </summary>
        public int Point { get; set; }
        /// <summary>
        /// 信用卡加密SHA256
        /// </summary>
        public string CardHash { get; set; }
        /// <summary>
        /// OTP連結
        /// </summary>
        public string OTPUrl { get; set; }
        /// <summary>
        /// 信用卡銀行代碼
        /// </summary>
        public string BankNo { get; set; }
        /// <summary>
        /// 收單銀行代碼
        /// </summary>
        public string GatewayBankNo { get; set; }
        /// <summary>
        /// 分期期數
        /// </summary>
        public int InstallPeriod { get; set; }
        /// <summary>
        /// 分期首期金額
        /// </summary>
        public int InstallDownPay { get; set; }
        /// <summary>
        /// 分期每期金額
        /// </summary>
        public int InstallPay { get; set; }
        /// <summary>
        /// 分期手續費金額
        /// </summary>
        public int InstallFee { get; set; }
        /// <summary>
        /// 付款方式
        /// </summary>
        public int PaymentMethod { get; set; }
        /// <summary>
        /// 付款方式文字描述
        /// </summary>
        public string PaymentMethodDesc { get; set; }
    }

    #region 信用卡卡樣

    /// <summary> 取得信用卡卡樣資訊API回應資訊 </summary>
    public class WalletCreditCardCategory
    {
        /// <summary> 卡樣編號 </summary>
        public int Id { get; set; }
        /// <summary> 卡樣名稱 </summary>
        public string CardName { get; set; }
        /// <summary> 卡號前八碼 </summary>
        public string CardCategory { get; set; }
        /// <summary> 卡樣圖檔URL </summary>
        public string ImageUrl { get; set; }
        /// <summary> 卡片所屬銀行編號 </summary>
        public string Bank { get; set; }
        /// <summary> 是否隱藏 </summary>
        public bool IsHidden { get; set; }
        /// <summary> 最後修改日期 </summary>
        public DateTime ModifyDate { get; set; }
    }

    /// <summary> 新增/編輯卡樣API請求/回應資訊 </summary>
    public class WalletCreditCardCategoryRequest : WalletCreditCardCategory
    {
        /// <summary> 卡樣上傳圖檔 </summary>
        public byte[] UploadImageStream { get; set; }
        /// <summary> 卡樣上傳圖檔檔名 </summary>
        public string FileName { get; set; }
    }

    /// <summary>
    /// 刪除卡樣API請求資訊
    /// </summary>
    public class WalletDeleteCreditCardCategoryRequest
    {
        /// <summary> 卡樣編號 </summary>
        public int Id { get; set; }
    }

    /// <summary>
    /// 查詢信用卡卡樣資料請求資訊
    /// </summary>
    public class WalletGetCreditCardCategoriesRequest
    {
        /// <summary> 卡號前八碼 </summary>
        public string CreditCardCategoryNo { get; set; }
        /// <summary> 銀行編號 </summary>
        public string bank { get; set; }
        /// <summary> 目前頁數 </summary>
        public int PageIndex { get; set; }
        /// <summary> 每頁筆數 </summary>
        public int PageSize { get; set; }
    }

    /// <summary>
    /// 查詢信用卡卡樣資料回應資訊
    /// </summary>
    public class WalletGetCreditCardCategoryResponse
    {
        /// <summary> 信用卡卡樣資料清單 </summary>
        public List<WalletGetCreditCardCategory> CreditCardCategories { get; set; }
        /// <summary> 總筆數 </summary>
        public int TotalCount { get; set; }
        /// <summary> 目前頁數 </summary>
        public int PageIndex { get; set; }
    }

    /// <summary> 信用卡卡樣資料(列表用) </summary>
    public class WalletGetCreditCardCategory : WalletCreditCardCategory
    {
        /// <summary> 信用卡所屬銀行 </summary>
        public string BankName { get; set; }
    }

    /// <summary> 依卡樣編號取得信用卡卡樣資訊請求資訊 </summary>
    public class WalletGetCreditCardCategoryByIdRequest
    {
        /// <summary> 卡樣編號 </summary>
        public int Id { get; set; }
    }
    #endregion
}

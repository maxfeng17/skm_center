﻿using System;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model
{
    public class PayWayLimit
    {
        public Guid Bid { set; get; }
        public int PaymentType { set; get; }
        public DateTime Expires { set; get; }
    }
}

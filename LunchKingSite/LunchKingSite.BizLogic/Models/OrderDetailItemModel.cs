﻿using System;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{
    /// <summary>
    /// 憑證檔訂單明細的Item Name 顯示選項
    /// </summary>
    public class PponItemNameShowOption
    {
        //意即只顯示選項與分店，發送簡訊使用
        public bool HideItemName;
        //只有在 HideItemName=true 才有作用，移掉所有的括號
        public bool RemoveOptionsParentheses;
        public bool ShowPhone;
        public bool StoreInfoInParenthesis;

        public string GetItemName(string itemName)
        {
            if (string.IsNullOrEmpty(itemName))
            {
                return "";
            }
            string result = itemName;
            if (result.IndexOf("&nbsp") > 0)
            {
                if (HideItemName)
                {
                    result = result.Substring(result.IndexOf("&nbsp") + 5);
                    if (RemoveOptionsParentheses)
                    {
                        result = result.Replace("(", "").Replace(")", "");
                    }
                }
                else
                {
                    result = result.Replace("&nbsp", " ");
                }
            }
            else if (HideItemName)
            {
                //沒有&nbsp 視為沒有選項，如果又要求HideItemName(for 簡訊)，即回傳空字串
                return "";
            }
            return result;
        }

        public string GetStoreInfo(ViewPponStore store, bool notOnlyOneStore)
        {
            return GetStoreInfo(store.StoreName, store.Phone, notOnlyOneStore);
        }

        public string GetStoreInfo(string storeName, string storePhone, bool notOnlyOneStore)
        {
            //顯示電話/有無電話/一間或多間分店/前後是否加()符號
            //this.ShowPhone            
            //String.IsNullOrEmpty(storePhone)
            //bool dealHasStores
            //this.StoreInfoInParenthesis
            //output:(《中山店》預約專線:8825252)

            if (string.IsNullOrEmpty(storeName))
            {
                return "";
            }

            StringBuilder sb = new StringBuilder();
            if (this.StoreInfoInParenthesis)
            {
                sb.Append('(');
            }

            if (notOnlyOneStore)
            {
                sb.Append(storeName);
            }

            if (this.ShowPhone && string.IsNullOrEmpty(storePhone) == false)
            {
                //sb.Append("預約專線: ");
                sb.Append(storePhone);
            }

            if (this.StoreInfoInParenthesis)
            {
                sb.Append(')');
            }
            return sb.ToString();
        }
    }

    public class OrderDetailItemModel
    {
        public Guid OrderDetailGuid { get; set; }
        public Guid BusinessHourGuid { get; set; }
        public string ItemName { get; set; }
        public Guid? StoreGuid { get; set; }

        public static OrderDetailItemModel Create(ViewPponCoupon coupon)
        {
            return new OrderDetailItemModel
            {
                BusinessHourGuid = coupon.BusinessHourGuid,
                ItemName = coupon.ItemName,
                OrderDetailGuid = coupon.OrderDetailGuid,
                StoreGuid = coupon.StoreGuid
            };
        }

        public static OrderDetailItemModel Create(ViewPponOrderDetail vpod)
        {
            return new OrderDetailItemModel
            {
                BusinessHourGuid = vpod.BusinessHourGuid.Value,
                ItemName = vpod.ItemName,
                OrderDetailGuid = vpod.OrderDetailGuid,
                StoreGuid = vpod.StoreGuid
            };
        }

        public static OrderDetailItemModel Create(OrderDetail od, Guid bid)
        {
            return new OrderDetailItemModel
            {
                BusinessHourGuid = bid,
                ItemName = od.ItemName,
                OrderDetailGuid = od.Guid,
                StoreGuid = od.StoreGuid
            };
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.BizLogic.Models
{
    public class QuestionnaireValume
    {
        public QuestionnaireValume()
        {
            AlreadyJoin = IsLoaded = false;
            QuestionData = new List<QuestionCategoryModel>();
        }

        public bool IsLoaded { get; set; }
        public string EventToken { get; set; }
        public bool AlreadyJoin { get; set; }
        public int ItemTotal { get; set; }
        public List<QuestionCategoryModel> QuestionData { get; set; }
    }

    public class QuestionCategoryModel
    {
        public QuestionCategoryModel()
        {
            QuestionItems = new List<QuestionItemModel>();
        }
        
        public QuestionItemCategory Category { get; set; }
        public List<QuestionItemModel> QuestionItems { get; set; }
    }

    public class QuestionItemModel
    {
        private static IQuestionProvider _qp = ProviderFactory.Instance().GetProvider<IQuestionProvider>();
        public QuestionItemModel(ViewQuestionItem i)
        {
            Item = i;
            ItemOptions = new List<QuestionItemOption>();
            ItemOptions.AddRange(_qp.GetQuestionItemOptions(i.ItemId));
            IsJump = ItemOptions.Any(x => x.JumpItemId > 0);
        }

        public bool IsJump { get; set; }
        public ViewQuestionItem Item { get; set; }
        public List<QuestionItemOption> ItemOptions { get; set; }
    }

    /// <summary>
    /// 問卷回覆
    /// </summary>
    public class QuestionAnswerModel
    {
        /// <summary>
        /// 會員ID, 未登入 0
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 活動代碼
        /// </summary>
        public string EventToken { get; set; }
        /// <summary>
        /// IP
        /// </summary>
        public string IpAddress { get; set; }
        /// <summary>
        /// Device
        /// </summary>
        public byte FromType { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public List<AnswerVolume> Volume { get; set; }
    }

    /// <summary>
    /// 答案
    /// </summary>
    public class AnswerVolume
    {
        /// <summary>
        /// 選項
        /// </summary>
        public int OptionId { get; set; }
        /// <summary>
        /// 文字回覆
        /// </summary>
        public string AnswerContent { get; set; }
    }
}

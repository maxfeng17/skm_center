﻿using System;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models
{
    public class DealMicroData : TrackTagBase
    {
        public DealMicroData() { }

        public DealMicroData(TrackTagInputModel model)
        {
            Name = string.IsNullOrEmpty(model.ViewPponDealData.AppTitle) ? model.ViewPponDealData.CouponUsage : model.ViewPponDealData.AppTitle;
            Sku = model.ViewPponDealData.BusinessHourGuid.ToString();
            ProductId = model.ViewPponDealData.UniqueId.ToString();
            Description = model.ViewPponDealData.EventTitle;
            Url = string.Format("{0}/{1}/{2}"
                , model.Config.SSLSiteUrl, model.IsPiinLifeDeal ? "piinlife" : "deal"
                , model.ViewPponDealData.BusinessHourGuid.ToString());
            Image = (string.IsNullOrEmpty(model.ViewPponDealData.AppDealPic)) ? PponFacade.GetPponDealFirstImagePath(model.ViewPponDealData.EventImagePath) : PponFacade.GetPponDealFirstImagePath(model.ViewPponDealData.AppDealPic);
            Offers = new Offer(model.ViewPponDealData, model.Config.EnableLowestComboDealPriceForGoogleShopping);
            if (model.ViewPponDealData.DealType != null)
            {
                Category = SystemCodeManager.GetDealTypeName(model.ViewPponDealData.DealType.Value);
            }
        }

        public override string GetJson()
        {
            var data = JsonConvert.SerializeObject(this, Formatting.Indented);
            return data;
        }

        [JsonProperty("@context")]
        public string Context {
            get { return "http://schema.org/"; }
        }
        [JsonProperty("@type")]
        public string Type {
            get { return "Product"; }
        }
        [JsonProperty("sku")]
        public string Sku { get; set; }
        [JsonProperty("brand")]
        public MicrodataBrand Brand
        {
            get { return new MicrodataBrand(); }
        }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("category")]
        public string Category { get; set; }
        [JsonProperty("productID")]
        public string ProductId { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("image")]
        public string Image { get; set; }
        [JsonProperty("offers")]
        public Offer Offers { get; set; }
    }

    public class MicrodataBrand
    {
        [JsonProperty("@type")]
        public string Type
        {
            get { return "Thing"; }
        }
        [JsonProperty("name")]
        public string Name
        {
            get { return "17Life生活電商"; }
        }
    }

    public class Offer
    {
        ISysConfProvider _config = ProviderFactory.Instance().GetConfig();

        public Offer(IViewPponDeal deal, bool enableLowestAvgPrice)
        {
            var isLowAvg = !enableLowestAvgPrice || PromotionFacade.IsLowestAvgPriceDeal(deal.BusinessHourGuid);

            if (isLowAvg)
            {
                if (deal.DiscountPrice == null ||
                    _config.EnableGSASubstituteDiscountPriceForPrice == false)
                {
                    Price = Convert.ToInt32(deal.ItemPrice).ToString();
                }
                else
                {
                    Price = deal.DiscountPrice.ToString("0");
                }
            }
            else
            {
                Price = deal.ComboDelas.Any()
                    ? Convert.ToInt32(deal.ComboDelas.OrderBy(x => x.ItemPrice).First().ItemPrice).ToString()
                    : Convert.ToInt32(deal.ItemPrice).ToString();
            }

            Availability = (deal.IsSoldOut || DateTime.Now > deal.BusinessHourOrderTimeE) ? "http://schema.org/SoldOut" : "http://schema.org/InStock";
        }

        [JsonProperty("@type")]
        public string Type
        {
            get { return "Offer"; }
        }
        [JsonProperty("price")]
        public string Price { get; set; }
        [JsonProperty("priceCurrency")]
        public string PriceCurrency
        {
            get { return "TWD"; }
        }
        [JsonProperty("availability")]
        public string Availability { get; set; }

        [JsonProperty("itemCondition")]
        public string ItemCondition
        {
            get { return "http://schema.org/NewCondition"; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{
    /// <summary>
    /// for mobile app trasnfer data
    /// </summary>
    [Serializable]
    public class PaymentDTO
    {
        #region properties

        /// <summary>
        /// Ticket ID
        /// </summary>
        public string TicketId { get; set; }

        /// <summary>
        /// Transsaction ID
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// order guid
        /// </summary>
        public Guid OrderGuid { get; set; }

        /// <summary>
        /// 預建訂單
        /// </summary>
        public Guid? PrebuiltOrderGuid { get; set; }

        /// <summary>
        /// Session ID
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// payeasy member id
        /// </summary>
        public string PezId { get; set; }

        /// <summary>
        /// bussiness hour guid
        /// </summary>
        public Guid BusinessHourGuid { get; set; }

        /// <summary>
        /// delivery info
        /// </summary>
        public PponDeliveryInfo DeliveryInfo { get; set; }

        /// <summary>
        /// delivery charge
        /// </summary>
        public int DeliveryCharge { get; set; }

        /// <summary>
        /// Item Quantity
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// 主要付款方式 (PayService.asmx用來取代以PaymentTyp判斷付款方式)
        /// </summary>
        public ApiMainPaymentType MainPaymentType;

        /// <summary>
        /// 信用卡、LinePay、MasterPay、ATM、ApplePay 支付金額 (扣掉紅利折抵、購物金折抵、折價券等，還需要支付的金額)
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// bonus cash
        /// </summary>
        public int BCash { get; set; }

        /// <summary>
        /// 17Life cash
        /// </summary>
        public int SCash { get; set; }

        /// <summary>
        /// payeasy cash
        /// </summary>
        public decimal PCash { get; set; }

        /// <summary>
        /// Discount code
        /// </summary>
        public string DiscountCode { get; set; }

        /// <summary>
        /// branch store guid
        /// </summary>
        public Guid SelectedStoreGuid { get; set; }

        /// <summary>
        /// item options, specified format is
        /// "OptionName|^|OptionGuid,OptionName|^|OptionGuid|#|ItemQuantity||OptionName|^|OptionGuid|#|ItemQuantity", just like
        /// "L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|2||L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|3"
        /// </summary>
        public string ItemOptions { get; set; }

        /// <summary>
        ///  invoice type
        /// </summary>
        public DonationReceiptsType? DonationReceiptsType { get; set; }

        /// <summary>
        /// payment api provider
        /// </summary>
        public PaymentAPIProvider APIProvider { get; set; }

        /// <summary>
        /// Is item
        /// </summary>
        [Obsolete("不再使用")]
        public bool IsItem { get; set; }

        /// <summary>
        /// Is pay by ATM
        /// </summary>
        public bool IsPaidByATM { get; set; }

        /// <summary>
        /// Is use shoppingcart
        /// </summary>
        [Obsolete("不再使用")]
        public bool IsShoppingCart { get; set; }

        /// <summary>
        /// Hami Ticket Id
        /// </summary>
        [Obsolete("不再使用")]
        public string IsHami { get; set; }

        /// <summary>
        /// 信用卡代碼
        /// </summary>
        public Guid? CreditCardGuid { get; set; }

        /// <summary>
        /// credit card number
        /// </summary>
        public string CreditcardNumber { get; set; }

        /// <summary>
        /// credit card security code
        /// </summary>
        public string CreditcardSecurityCode { get; set; }

        /// <summary>
        /// credit card expire year in 2 digits
        /// </summary>
        public string CreditcardExpireYear { get; set; }

        /// <summary>
        /// credit card expire month in 2 digits
        /// </summary>
        public string CreditcardExpireMonth { get; set; }

        /// <summary>
        /// ATM account
        /// </summary>
        public string ATMAccount { get; set; }

        /// <summary>
        /// result page type
        /// </summary>
        public PaymentResultPageType ResultPageType { get; set; }

        /// <summary>
        /// buyer and receiver address info, the specified format is:
        /// "BuyerLastName|BuyerFirstName|BuyerMobile|BuyerCityId|BuyerAreaId|BuyerBuildingGuid|BuyerAddress^ReceiverName|ReceiverMobile|ReceiverCityId|ReceiverAreaId|ReceiverBuildingId|ReceiverAddress|IsAddReceiverInfo"
        /// just like "馬|魚|0917171717|294|317|8c3b0067-f777-4461-9d76-d51fae14c009|44號^馬魚444|0917173213|294|317|04803868-3196-45d0-975c-74a0d2e113a6|44號|False".
        /// </summary>
        public string AddressInfo { get; set; }

        /// <summary>
        /// 推薦送紅利活動的推薦人資訊-原WEB活動中紀錄於cookie.referenceId中資訊
        /// </summary>
        public string ReferenceId { get; set; }

        /// <summary>
        /// 使用 MasterPass
        /// </summary>
        public bool IsMasterPass { set; get; }
        /// <summary>
        /// MasterPass Data
        /// </summary>
        public MasterPassTrans MasterPassTransData { set; get; }
        /// <summary>
        /// apple pay token 
        /// </summary>
        public ApplePayToken ApplePayTokenData { get; set; }
        /// <summary>
        /// apple pay decrypt data
        /// </summary>
        public ApplePayData ApplePayData { get; set; }
        
        /// <summary>
        /// CpaKey
        /// </summary>
        public string CpaKey { get; set; }

        /// <summary>
        /// App 主cpakey與副cpakey的集合
        /// </summary>
        public string[] CpaList { get; set; }

        /// <summary>
        /// device id
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// 是否走OTP
        /// </summary>
        public bool IsOTP { get; set; }

        /// <summary>
        /// 是否已跑完OTP
        /// </summary>
        public bool IsFinishOTP { get; set; }

        #endregion properties

        public PaymentDTO()
        {
            IsMasterPass = false;
        }

        /// <summary>
        /// 取得收件者地址資料，該資料由 AddressInfo 依據其內容解析產出，若無收件人資料將回傳NULL
        /// </summary>
        /// <returns></returns>
        public LkAddress GetReceiverAddress()
        {
            if (!string.IsNullOrWhiteSpace(AddressInfo))
            {
                string[] yy = AddressInfo.Split('^');
                //收件人部分
                if (!string.IsNullOrWhiteSpace(yy[1]))
                {
                    LkAddress address = new LkAddress();
                    string[] radd = yy[1].Split('|');
                    address.CityId = int.Parse(radd[2]);
                    address.TownshipId = int.Parse(radd[3]);
                    Guid tempGuid;
                    if (Guid.TryParse(radd[4], out tempGuid))
                    {
                        address.BuildingGuid = tempGuid;
                    }
                    address.AddressDesc = radd[5];
                    return address;
                }
            }
            return null;
        }

    }

    /// <summary>
    /// 用於紀錄masterpass回傳信用卡資料
    /// </summary>
    public class CreditCardInfo
    {
        public string CardNumber { get; set; }
        public string ExpiryYear { get; set; }
        public string ExpiryMonth { get; set; }
        public string MasterPassTransId { get; set; }
        public string PayPassWalletIndicator { get; set; }
        public string OauthToken { set; get; }
        public string CardBrandId { set; get; }
        public string CardBrandName { set; get; }
        public string CardHolder { set; get; }
    }

    /// <summary>
    /// 退貨的訂單狀態資料
    /// </summary>
    public class RefundOrderInfo
    {
        public ViewCouponListMainCollection Coupons { get; set; }
        /// <summary>
        /// 訂單是否已全數退貨
        /// </summary>
        public bool IsReturnable { get; set; }
        /// <summary>
        /// 是否有處理中(未完成)退貨單
        /// </summary>
        public bool HasProcessingReturnForm { get; set; }
        /// <summary>
        /// 是否委託銷售
        /// </summary>
        public bool IsEntrustSell { get; set; }
        /// <summary>
        /// 是否已預約
        /// </summary>
        public bool IsReservationLock { get; set; }
        /// <summary>
        /// 是否換貨處理中
        /// </summary>
        public bool IsExchangeProcessing { get; set; }
        /// <summary>
        /// 是否正在處理發票2聯轉3聯
        /// </summary>
        public bool IsInvoice2To3 { get; set; }
        /// <summary>
        /// 是否退只退現金
        /// </summary>
        public bool IsRefundCashOnly { get; set; }
        /// <summary>
        /// 是否有使用現金付款
        /// </summary>
        public bool IsPaidByCash { get; set; }
        /// <summary>
        /// OrderGuid
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 是否為憑證
        /// </summary>
        public bool IsPpon { get; set; }
        /// <summary>
        /// 是否以第三方支付付款
        /// </summary>
        public bool IsThridPartyPay { set; get; }
        /// <summary>
        /// 第三方付款方式
        /// </summary>
        public ThirdPartyPayment ThirdPartyPaymentSystem { set; get; }

        /// <summary>
        /// 是否為超取超付
        /// </summary>
        public bool IsPaidByIsp { get; set; }
       
        /// <summary>
        /// 部份退貨驗證
        /// </summary>
        public bool HasPartialCancelOrVerified { get; set; }
        /// <summary>
        /// 是否分期付款
        /// </summary>
        public bool IsInstallment { get; set; }
        /// <summary>
        /// 是否超商付款
        /// </summary>
        public bool IsIspPay { get; set; }
        /// <summary>
        /// 是否為超商取貨
        /// </summary>
        public bool IsIsp { get; set; }
        public RefundOrderInfo()
        {
            this.IsReturnable = true;
            this.OrderGuid = Guid.Empty;
        }
    }

    /// <summary>
    /// MasterPass 交易資料
    /// </summary>
    public class MasterPassTrans
    {
        /// <summary>
        /// auth token
        /// </summary>
        public string AuthToken { set; get; }
        /// <summary>
        /// verifier code
        /// </summary>
        public int VerifierCode { set; get; }

        public bool IsValid()
        {
            if (AuthToken == string.Empty || VerifierCode < 10000001)
            {
                return false;
            }
            return true;
        }
    }

    /// <summary>
    /// apple pay 交易資料
    /// </summary>
    public class ApplePayToken
    {
        /// <summary>
        /// Encrypted payment data
        /// </summary>
        public string Data { get; set; }
        /// <summary>
        /// Additional version-dependent information used to decrypt and verify the payment.
        /// </summary>
        public ApplePayTokenHeader Header { get; set; }
        /// <summary>
        /// Signature of the payment and header data.
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// Version information about the payment token.
        /// </summary>
        public string Version { get; set; }

        public string Bid { get; set; }
    }

    /// <summary>
    /// apple pay 交易資料 標頭訊息
    /// </summary>
    public class ApplePayTokenHeader
    {
        /// <summary>
        /// Ephemeral public key bytes.
        /// </summary>
        public string EphemeralPublicKey { get; set; }
        /// <summary>
        /// Hash of the X.509 encoded public key bytes of the merchant’s certificate.
        /// </summary>
        public string PublicKeyHash { get; set; }
        /// <summary>
        /// Transaction identifier, generated on the device.
        /// </summary>
        public string TransactionId { get; set; }
    }


    /// <summary>
    /// OTP授權回傳
    /// </summary>
    public class OTPOther
    {
        public string ver { get; set; }
        public string mid { get; set; }
        public string tid { get; set; }
        public int pay_type { get; set; }
        public int tx_type { get; set; }
        public int ret_value { get; set; }
        public OTPOtherParams @params { get; set; }
    }

    public class OTPOtherParams
    {
        public string ret_code { get; set; }
        public string order_status { get; set; }
        public string purchase_date { get; set; }
    }

    public class OTPMakePaymentModel
    {
        public string ticketId { get; set; }
        public string apiUserId { get; set; }
        public int userId { get; set; }
        public PaymentDTO paymentDTO { get; set; }
    }

    public class LinePayConfirm
    {
        public string TransId { get; set; }
        public string TicketId { get; set; }
        public bool LineCancel { get; set; }
        public string TradeAmount { get; set; }
        public string UserId { get; set; }
    }

    /// <summary>
    /// OTP非同步
    /// </summary>
    public class OTPAsync
    {
        public string ver { get; set; }
        public string mid { get; set; }
        public string tid { get; set; }
        public int pay_type { get; set; }
        public int tx_type { get; set; }
        public int ret_value { get; set; }
        public OTPAsyncParams @params { get; set; }
    }

    public class OTPAsyncParams
    {
        public string ret_code { get; set; }
        public string order_status { get; set; }
        public string order_no { get; set; }
        public string ret_msg { get; set; }
        public string auth_id_resp { get; set; }
        

    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using MongoDB.Bson.Serialization.Attributes;
using Vodka.Mongo;

namespace LunchKingSite.BizLogic.Model.Vourcher
{
    public class VourcherSellerDocument : Entity
    {
        public VourcherSellerDocument()
        {
            StoreList = new List<VourcherStoreSynopsesDocument>();
            EventList = new List<VourcherEventSynopsesDocument>();
        }

        #region 商家部分(Seller)
        /// <summary>
        /// 商家編號
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 商家對外編號
        /// </summary>
        public string SellerId { get; set; }
        /// <summary>
        /// 商家名稱
        /// </summary>
        public string SellerName { get; set; }
        /// <summary>
        /// 商家圖片路徑
        /// </summary>
        public string SellerLogoimgPath { get; set; }
        /// <summary>
        /// 商家類型
        /// </summary>
        public SellerSampleCategory SellerCategory { get; set; }
        /// <summary>
        /// 商家期限內之優惠券數量
        /// </summary>
        public int VourcherCount { get; set; }
        #endregion 商家部分

        #region 有優惠的分店
        /// <summary>
        /// 有優惠的分店資料
        /// </summary>
        public List<VourcherStoreSynopsesDocument> StoreList { get; set; }

        #endregion 有優惠的分店

        #region 優惠券資料

        public List<VourcherEventSynopsesDocument> EventList { get; set; }
        #endregion 優惠券資料
    }

    public class VourcherStoreSynopsesDocument
    {
        public VourcherStoreSynopsesDocument()
        {
            Coordinates = new LatLng();
        }
        /// <summary>
        /// 分店名稱
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 分店緯度
        /// </summary>
        [BsonIgnore]
        public double StoreLatitude { get { return Coordinates.Latitude; } set { Coordinates.Latitude = value; } }
        /// <summary>
        /// 分店經度
        /// </summary>
        [BsonIgnore]
        public double StoreLongitude { get { return Coordinates.Longitude; } set { Coordinates.Longitude = value; } }
        /// <summary>
        /// 分店經緯度(實際儲存於資料庫的物件)
        /// </summary>
        public LatLng Coordinates { get; set; }
        /// <summary>
        /// 配合的優惠券數量
        /// </summary>
        public int EventCount { get; set; }
        /// <summary>
        /// 配合的優惠券
        /// </summary>
        public List<int> EventIdList { get; set; } 
    }

    public class VourcherEventSynopsesDocument
    {
        /// <summary>
        /// 優惠券ID
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 優惠說明文字
        /// </summary>
        public string VourcherContent { get; set; }
        /// <summary>
        /// 優惠開始時間
        /// </summary>
        public DateTime StartDate { get; set; }
    }
}

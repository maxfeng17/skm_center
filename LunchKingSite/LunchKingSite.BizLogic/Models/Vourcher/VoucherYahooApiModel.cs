﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.Vourcher
{
    public class ViewYahooVoucherSellerModel
    {
        public int EventId { get; set; }
        public string Contents { get; set; }
        public string Instruction { get; set; }
        public string Others { get; set; }
        public string PicUrl { get; set; }
        public int Type { get; set; }
        public int Mode { get; set; }
        public int Status { get; set; }
        public bool Enable { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PageCount { get; set; }
        public int Ratio { get; set; }
        public Guid SellerGuid { get; set; }
        public string SellerName { get; set; }
        public string SellerDescription { get; set; }
        public bool IsCloseDown { get; set; }
        public string EventImageTag { get; set; }

        public ViewYahooVoucherSellerModel(ViewYahooVoucherSeller ob)
        {
            EventId = ob.EventId;
            Contents = ob.Contents;
            Instruction = ob.Instruction;
            Others = ob.Others.Replace("\n", string.Empty).Replace("\r", "<br/>");
            PicUrl = ob.PicUrl;
            Type = ob.Type;
            Mode = ob.Mode;
            Status = ob.Status;
            Enable = ob.Enable;
            StartDate = ob.StartDate.Value;
            EndDate = ob.EndDate.Value;
            PageCount = ob.PageCount;
            Ratio = ob.Ratio;
            SellerGuid = ob.SellerGuid;
            SellerName = ob.SellerName;
            SellerDescription = ob.SellerDescription;
            IsCloseDown = ob.IsCloseDown;
            EventImageTag = ImageFacade.GetMediaPathsFromRawData(ob.PicUrl, MediaType.PponDealPhoto).DefaultIfEmpty(string.Empty).First();
        }
    }

    public class ViewYahooVoucherStoreModel
    {
        public int EventId { get; set; }
        public Guid StoreGuid { get; set; }
        public string StoreName { get; set; }
        public string Phone { get; set; }
        public string CityName { get; set; }
        public string TownName { get; set; }
        public int TownshipId { get; set; }
        public int CityId { get;set;}
        public string AddressString { get; set; }

        public ViewYahooVoucherStoreModel(ViewYahooVoucherStore ob)
        {
            EventId = ob.EventId;
            StoreGuid = ob.StoreGuid;
            StoreName = ob.StoreName;
            Phone = ob.Phone;
            CityName = ob.CityName;
            TownName = ob.TownName;
            CityId = ob.CityId;
            TownshipId = ob.TownshipId;
            AddressString = ob.AddressString;
        }
    }

    public class ViewYahooVoucherCategoryModel
    {
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int CategoryType { get; set; }
        public int EventId { get; set; }

        public ViewYahooVoucherCategoryModel(ViewYahooVoucherCategory ob)
        {
            Name = ob.Name;
            CategoryId = ob.CategoryId;
            CategoryType = ob.CategoryType.Value;
            EventId = ob.EventId;
        }
    }

    public class ViewYahooVoucherPromoModel
    {
        public int Id { get; set; }
        public int? EventId { get; set; }
        public string Tag { get; set; }
        public string Content { get; set; }
        public string Link { get; set; }
        public int Rank { get; set; }
        public int Type { get; set; }
        public int? Status { get; set; }
        public bool? Enable { get; set; }
        public int? EventType { get; set; }
        public DateTime? PromoStartDate { get; set; }
        public DateTime? PromoEndDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public ViewYahooVoucherPromoModel(ViewYahooVoucherPromo ob)
        {
            Id = ob.Id;
            EventId = ob.EventId;
            Tag = ob.Tag;
            Content = ob.Content;
            Link = ob.Link;
            Rank = ob.Rank;
            Type = ob.Type;
            Status = ob.Status;
            Enable = ob.Enable;
            EventType = ob.EventType;
            PromoStartDate = ob.PromoStartDate;
            PromoEndDate = ob.PromoEndDate;
            StartDate = ob.StartDate;
            EndDate = ob.EndDate;
        }
    }
}

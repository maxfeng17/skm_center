﻿using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LunchKingSite.BizLogic.Vbs
{
    public class BillEntity
    {                
        #region fields

        private static IAccountingProvider _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static ISysConfProvider _cp = ProviderFactory.Instance().GetConfig();
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IHiDealProvider _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        
        private readonly bool _isNew;

        ICollection<BillingStmtDistribution> _currentDistribution = null;
        internal ICollection<BillingStmtDistribution> CurrentDistribution
        {
            get
            {
                return _currentDistribution;
            }
        }

        private bool _billTypeDetermined = false;
        
        private VendorReceiptType _billType;
        private VendorReceiptType BillType
        {
            get
            {
                if (!this._isNew)
                {
                    return (VendorReceiptType)this.DbBill.VendorReceiptType;
                }
                else
                {
                    if (_billTypeDetermined)
                    {
                        return _billType;
                    }
                    throw new InvalidOperationException("Bill type not determined!");
                }
            }
        }

        private bool _isInvoiceTaxRequired;
        private bool IsInvoiceTaxRequired
        {
            get
            {
                if (!this._isNew)
                {
                    return this.DbBill.IsInputTaxRequired;
                }
                else
                {
                    if (_billTypeDetermined)
                    {
                        return _isInvoiceTaxRequired;
                    }
                    throw new InvalidOperationException("Bill type not determined!");
                }
            }
        }

        private string _billTypeOtherName;
        private string BillTypeOtherName
        {
            get
            {
                if (!this._isNew)
                {
                    return this.DbBill.BillTypeOtherName;
                }
                else
                {
                    if (_billTypeDetermined)
                    {
                        return _billTypeOtherName;
                    }
                    throw new InvalidOperationException("Bill type not determined!");
                }
            }
        }

        private Lazy<Bill> _dbBill;
        private Bill DbBill
        {
            get
            {
                if (_billId.HasValue)
                {
                    return _dbBill.Value.Clone();
                }
                else
                {
                    return null;
                }
            }
        }


        #endregion

        #region props

        private readonly int? _billId;
        public int? BillId
        {
            get 
            { 
                return _billId; 
            }
        }
        
        public BillInfo Info
        {
            get
            {
                if (!_isNew)
                {
                    return new BillInfo()
                    {
                        BillType = (VendorReceiptType)this.DbBill.VendorReceiptType,
                        BillTypeOtherName = this.DbBill.BillTypeOtherName,
                        IsInvoiceTaxRequired = this.DbBill.IsInputTaxRequired,
                        BuyerType = (BillBuyerType)this.DbBill.BuyerType,
                        BillNumber = this.DbBill.BillNumber,
                        BillDate = this.DbBill.InvoiceDate,
                        BillingCompanyId = this.DbBill.InvoiceComId,
                        Total = this.DbBill.BillSum,
                        Subtotal = this.DbBill.BillSumNotaxed,
                        Tax = this.DbBill.BillTax,
                        Remark = this.DbBill.Remark
                    };
                }
                throw new InvalidOperationException("Bill does not exist yet!");
            }
        }
                        
        #endregion

        public static BillEntity New()
        {
            return new BillEntity();
        }

        /// <summary>
        /// 取得此單據的已分配金額
        /// </summary>        
        public List<BillingStmtDistribution> GetDistributedDistribution()
        {
            if (_currentDistribution == null)
            {
                if (!this._isNew)
                {
                    var distribution = new List<BillingStmtDistribution>();

                    var relationships = _ap.BalanceSheetBillRelationshipGetListByBillId(this._billId.Value);
                    if (relationships.Any())
                    {
                        foreach (var rel in relationships)
                        {
                            var spec = new BillingStmtDistribution(rel.BalanceSheetId, rel.BillMoney, rel.BillMoneyNotaxed, rel.BillTax, rel.Remark, (int)BalanceSheetUseType.Deal);
                            distribution.Add(spec);
                        }
                    }
                    var wmsRelationships = _ap.BalanceSheetWmsBillRelationshipGetListByBillId(this._billId.Value);
                    if (wmsRelationships.Any())
                    {
                        foreach (var rel in wmsRelationships)
                        {
                            var spec = new BillingStmtDistribution(rel.BalanceSheetId, rel.BillMoney, rel.BillMoneyNotaxed, rel.BillTax, rel.Remark, (int)BalanceSheetUseType.Wms);
                            distribution.Add(spec);
                        }
                    }
                    
                    

                    _currentDistribution = distribution;
                }
                else
                {
                    return new List<BillingStmtDistribution>();
                }                
            }

            return new List<BillingStmtDistribution>(_currentDistribution);
        }
        
        /// <summary>
        /// 算出此單據的分配金額 (含單據本身已分配金額)
        /// </summary>             
        /// <param name="bsIds">要增加的對帳單分配</param>
        public bool TryCalculateDistribution(Dictionary<int, BalanceSheetUseType> bsIds, out List<BillingStmtDistribution> result, out List<BillingStmtDistribution> wmsResult)
        {
            if (bsIds == null)
            {
                result = null;
                wmsResult = null;
                return false;
            }

            if (!ConflictCheck(bsIds))
            {
                result = null;
                wmsResult = null;
                return false;
            }

            bool calcTax;
            if (this.BillType == VendorReceiptType.Invoice
                && this.IsInvoiceTaxRequired)
            {
                calcTax = true;
            }
            else
            {
                calcTax = false;
            }            
            
            if (this._isNew)
            {
                result = DistributeByBillingStatements(bsIds, calcTax);
                wmsResult = DistributeByBillingStatementsWms(bsIds, calcTax);
            }
            else
            {                
                result = DistributeAccordingToBill(bsIds, calcTax);
                wmsResult = DistributeByBillingStatementsWms(bsIds, calcTax);//在確認
            }            

            return true;
        }
                
        /// <summary>
        /// 算出新單據的分配金額及單據資料
        /// </summary>
        public bool TryCalculateNewBillDistribution(Dictionary<int, BalanceSheetUseType> bsIds, out List<BillingStmtDistribution> distResult, out List<BillingStmtDistribution> distResultWms, out BillInfo billInfoResult)
        {
            if (!this._isNew)
            {
                throw new InvalidOperationException("Use TryCalculateDistribution(IEnumerable<int>, out IList<BillingStmtDistribution>) instead.");
            }
            
            if (!BsConflictCheck(bsIds))
            {
                distResult = null;
                distResultWms = null;
                billInfoResult = null;
                return false;
            }

            DeterminedNewBillType(bsIds);            

            if (TryCalculateDistribution(bsIds, out distResult, out distResultWms))
            {
                BillInfo info = new BillInfo
                {
                    BillType = this.BillType,
                    BillTypeOtherName = this.BillTypeOtherName,
                    IsInvoiceTaxRequired = this.IsInvoiceTaxRequired,
                    BuyerType = BillBuyerType.Contact,  //預設買受人
                    Total = distResult.Sum(x => x.Total) + distResultWms.Sum(x => x.Total),
                    Remark = string.Empty
                };
                //單據中顯示之總計、未稅金額及稅額 計算規則需同單據管理 合併對帳單顯示之計算規則
                //多筆對帳單的未稅金額計算方式: 對帳單應付金額/(1+稅率)
                //稅額計算方式: 總計 - 未稅金額 
                var taxRate = this.BillType == VendorReceiptType.Invoice && info.IsInvoiceTaxRequired 
                                ? _cp.BusinessTax 
                                : 0;
                info.Subtotal = (int)Math.Round(info.Total / (1 + taxRate), 0);
                info.Tax = info.Total - info.Subtotal;

                int theBsId = GetOrderedBsIds(bsIds).FirstOrDefault();
                int theBswId = GetOrderedBswIds(bsIds).FirstOrDefault();
                if (theBsId!=0)
                    info.BillingCompanyId = GetBillingCompanyId(theBsId);
                else if (theBswId!=0)
                    info.BillingCompanyId = GetWmsBillingCompanyId(theBswId);

                billInfoResult = info;
                return true;
            }

            distResult = null;
            distResultWms = null;
            billInfoResult = null;
            return false;
        }

        public bool CheckIsLumpSumBalanceSheet(IEnumerable<int> bsIds)
        {
            return _ap.BalanceSheetGetListByIds(bsIds.ToList())
                      .All(x => x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet) &&
                    _ap.BalanceSheetWmsGetListByIds(bsIds.ToList())
                      .All(x => x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet);
        }
        
        /// <summary>
        /// 驗證單據資料的變動
        /// </summary>
        /// <param name="distributions"></param>        
        public bool ValidateModRequest(BillInfo info, IEnumerable<BillingStmtDistribution> distributions, out string invalidReason)
        {
            if (info == null)
            {
                throw new InvalidOperationException();
            }

            if (info.BillType == VendorReceiptType.Invoice
                && !CheckInvoiceFormat(info.BillNumber))
            {
                invalidReason = "發票格式不符合，需2碼英文8碼數字。";
                return false;
            }

            if (info.BillType == VendorReceiptType.Invoice
                && CheckIsInvoiceExist(info.BillNumber, this.BillId))
            {
                invalidReason = "發票號碼重覆，請重新輸入。";
                return false;
            }

            if (!info.BillDate.HasValue)
            {
                invalidReason = "請輸入發票日期。";
                return false;
            }

            if (info.Total != info.Subtotal + info.Tax)
            {
                invalidReason = "發票金額有誤。請重新計算發票實際金額。";
                return false;
            }

            if (distributions.Sum(x => x.Total) > info.Total)
            {
                invalidReason = "各月份的金額加總，必須小於或等於實際發票金額。請重新計算各月金額。";
                return false;
            }
            
            foreach (var dist in distributions)
            {
                if (dist.Total != dist.Subtotal + dist.Tax)
                {
                    invalidReason = "分配的金額有誤，請重新驗證。";
                    return false;
                }
            }

            foreach (var dist in distributions)
            {
                if (dist.Type == (int)BalanceSheetUseType.Deal)
                {
                    BalanceSheetModel bsModel = new BalanceSheetModel(dist.BalanceSheetId);
                    BalanceSheetType bsType = bsModel.Type;
                    int bsAmount;
                    switch (bsType)
                    {
                        case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
                        case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                            bsAmount = (int)bsModel.TransferInfo.AccountsPaid.Value
                                - bsModel.BillDistribution.Where(x => x.BillId != this.BillId).Sum(x => x.Total);   //週結已經付款了, 分配實際金額
                            break;
                        default:
                            bsAmount = (int)(Math.Round(bsModel.GetAccountsPayable().ReceiptAmount))
                                - bsModel.BillDistribution.Where(x => x.BillId != this.BillId).Sum(x => x.Total);
                            break;
                    }
                    if (dist.Total > bsAmount)
                    {
                        invalidReason = "分配的金額有誤，請重新檢查。";
                        return false;
                    }
                }
                else
                {
                    //update會被擋
                    //int count = _ap.BalanceSheetWmsBillRelationshipGetCount(dist.BalanceSheetId);
                    //if (count > 0)
                    //{
                    //    invalidReason = "單號：" + dist.BalanceSheetId.ToString() + "重複輸入單據，請重新檢查";
                    //    return false;
                    //}

                } 
                
            }

            modInfo = info;
            modDistributions = distributions;
            invalidReason = string.Empty;
            return true;
        }

        private bool CheckInvoiceFormat(string invoiceNumber)
        {
            Regex regex = new Regex(@"[a-zA-Z][a-zA-Z]\d{8}", RegexOptions.Compiled);
            return regex.IsMatch(invoiceNumber);
        }

        public List<dynamic> ToExpandedInfo(List<BillingStmtDistribution> distributions, List<BillingStmtDistribution> distributionsWms)
        {
            List<dynamic> result = new List<dynamic>();
                        
            List<int> bsIds = distributions.Select(x => x.BalanceSheetId).ToList();
            Dictionary<Guid, ViewBalancingDeal> info = GetExpandData(bsIds);            

            foreach (var dist in distributions)
            {                
                BalanceSheetModel bs = this.GetBsModel(dist.BalanceSheetId);
                                
                dynamic obj = dist.ToExpando();
                obj.IsConfirmed = bs.IsConfirmed;
                obj.Year = bs.Year.Value;
                obj.Month = bs.Month.Value;
                obj.Day = bs.Day;
                obj.DealName = info[bs.MerchandiseGuid].DealName;
                obj.StoreName = bs.GetStoreName();
                obj.BalanceSheetUseType = (int)BalanceSheetUseType.Deal;
                result.Add(obj);
            }

            List<int> bswIds = distributionsWms.Select(x => x.BalanceSheetId).ToList();

            foreach (var dist in distributionsWms)
            {
                BalanceSheetWmsModel bs = this.GetBswModel(dist.BalanceSheetId);

                dynamic obj = dist.ToExpando();
                obj.IsConfirmed = bs.IsConfirmed;
                obj.Year = bs.Year.Value;
                obj.Month = bs.Month.Value;
                obj.Day = bs.Day;
                obj.DealName = "倉儲費用";
                obj.StoreName = "";
                obj.BalanceSheetUseType = (int)BalanceSheetUseType.Wms;
                result.Add(obj);
            }
            return result;
        }

        private bool CheckIsInvoiceExist(string invoiceNumber, int? billId)
        {
            List<string> filter = new List<string>();
            if (billId.HasValue)
            {
                filter.Add(Bill.Columns.Id + " <> " + billId.Value);
            }
            filter.Add(Bill.Columns.BillNumber + " = " + invoiceNumber);
            return (_ap.BillGetList(filter.ToArray()).Count > 0);
        } 
        
        #region .ctor

        internal BillEntity(int id)
        {
            _billId = id;
            _isNew = false;
            _dbBill = new Lazy<Bill>(() => _ap.BillGet(id), true);
        }

        private BillEntity()
        {
            _billId = null;
            _isNew = true;
            //how to set _dbBill and _billId after save?
        }

        #endregion

        #region private members        
        
        /// <summary>
        /// 確認每一張對帳單的開立是否一致
        /// </summary>
        /// <param name="balanceSheetIds"></param>
        /// <returns></returns>
        private bool BsConflictCheck(Dictionary<int, BalanceSheetUseType> balanceSheetIds)
        {
            if (balanceSheetIds.Count() == 0)
            {
                return true;
            }

            if (balanceSheetIds.Count() == 1)
            {
                VendorReceiptType dummy1;
                bool dummy2;
                string dummy3;
                if (TryGetBsSettings(balanceSheetIds.First().Key, balanceSheetIds.First().Value, out dummy1, out dummy2, out dummy3))
                {
                    return true;
                }
                return false;
            }

            VendorReceiptType receiptTypeSetting = VendorReceiptType.Invoice;
            bool isInvoiceTaxRequiredSetting = true;                 

            bool firstTimeSet = false;
            foreach (var bsid in balanceSheetIds)
            {
                VendorReceiptType bsReceiptType;
                bool bsInvoiceTaxRequired;
                string dummy;
                if (TryGetBsSettings(bsid.Key, bsid.Value, out bsReceiptType, out bsInvoiceTaxRequired, out dummy))
                {
                    if (bsid.Value == BalanceSheetUseType.Wms)
                    {
                        continue;
                    }

                    if (!firstTimeSet)
                    {                        
                        receiptTypeSetting = bsReceiptType;
                        isInvoiceTaxRequiredSetting = bsInvoiceTaxRequired;
                        firstTimeSet = true;
                    }
                    else
                    {
                        if (receiptTypeSetting == bsReceiptType
                            && isInvoiceTaxRequiredSetting == bsInvoiceTaxRequired)
                        {
                            continue;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }
        
        /// <summary>
        /// 確認對帳單單據開立設定
        /// </summary>
        /// <param name="balanceSheetId"></param>
        /// <param name="type"></param>
        /// <param name="bsReceiptType"></param>
        /// <param name="bsInvoiceTaxRequired"></param>
        /// <param name="bsTypeOtherName"></param>
        /// <returns></returns>
        private bool TryGetBsSettings(int balanceSheetId, BalanceSheetUseType type, out VendorReceiptType bsReceiptType, out bool bsInvoiceTaxRequired, out string bsTypeOtherName)
        {
            if (type == BalanceSheetUseType.Deal)
            {
                BalanceSheet bs = this.GetBsModel(balanceSheetId).GetDbBalanceSheet();
                BusinessModel biz = this.GetBsModel(balanceSheetId).BizModel;
                switch (biz)
                {
                    case BusinessModel.Ppon:
                        DealAccounting da = _pp.DealAccountingGet(bs.ProductGuid);
                        bsReceiptType = (VendorReceiptType)da.VendorReceiptType;
                        bsInvoiceTaxRequired = da.IsInputTaxRequired;
                        bsTypeOtherName = da.Message;
                        return true;
                    case BusinessModel.PiinLife:
                        HiDealProduct prod = _hp.HiDealProductGet(bs.ProductGuid);
                        bsReceiptType = (VendorReceiptType)prod.VendorReceiptType;
                        bsInvoiceTaxRequired = !prod.IsInputTaxFree;
                        bsTypeOtherName = prod.Message;
                        return true;
                    default:
                        bsReceiptType = VendorReceiptType.Invoice;
                        bsInvoiceTaxRequired = true;
                        bsTypeOtherName = string.Empty;
                        return false;
                }
            }
            else
            {
                //倉儲對帳單固定統一發票
                bsReceiptType = VendorReceiptType.Invoice;
                bsInvoiceTaxRequired = true;
                bsTypeOtherName = string.Empty;
                return true;
            }
                       
        }
        
        private bool ConflictCheck(Dictionary<int, BalanceSheetUseType> balanceSheetIds)
        {
            if (BsConflictCheck(balanceSheetIds))
            {
                if (balanceSheetIds.Count() == 0)
                {
                    return true;
                }

                VendorReceiptType bsReceiptType;
                bool bsInvoiceTaxRequired;
                string dummy;
                if (TryGetBsSettings(balanceSheetIds.First().Key, balanceSheetIds.First().Value, out bsReceiptType, out bsInvoiceTaxRequired, out dummy))
                {
                    return this.BillType == bsReceiptType
                        && this.IsInvoiceTaxRequired == bsInvoiceTaxRequired;
                }
            }
            return false;
        }

        private Dictionary<Guid, ViewBalancingDeal> GetExpandData(List<int> bsIds)
        {
            List<Guid> dealGuids = new List<Guid>();
            foreach (int bsId in bsIds)
            {
                BalanceSheetModel bs = this.GetBsModel(bsId);
                if (!dealGuids.Contains(bs.MerchandiseGuid))
                {
                    dealGuids.Add(bs.MerchandiseGuid);
                }
            }

            return _ap.ViewBalancingDealGetListByMerchandiseGuid(dealGuids).ToDictionary(x => x.MerchandiseGuid);
        }

        /// <summary>
        /// 確認發票開立方式
        /// </summary>
        /// <param name="bsIds"></param>
        private void DeterminedNewBillType(Dictionary<int, BalanceSheetUseType> bsIds)
        {
            if (!bsIds.Any())
            {
                _billType = VendorReceiptType.Invoice;
                _isInvoiceTaxRequired = true;
                _billTypeOtherName = string.Empty;
            }
            else
            {
                VendorReceiptType temp;
                bool taxRequired;
                string typeOtherName;
                if (TryGetBsSettings(bsIds.First().Key, bsIds.First().Value, out temp, out taxRequired, out typeOtherName))
                {
                    _billType = temp;
                    _isInvoiceTaxRequired = taxRequired;
                    _billTypeOtherName = typeOtherName;
                }
                else
                {
                    throw new Exception();
                }
            }

            _billTypeDetermined = true;
        }

        private List<BillingStmtDistribution> DistributeAccordingToBill(Dictionary<int, BalanceSheetUseType> bsIds, bool calcTax)
        {
            var distributed = GetDistributedDistribution();

            int billTotal = this.DbBill.BillSum - distributed.Sum(x => x.Total);
            int billSubtotal = this.DbBill.BillSumNotaxed - distributed.Sum(x => x.Subtotal);
            int billTax = this.DbBill.BillTax - distributed.Sum(x => x.Tax);

            Dictionary<int, BalanceSheetUseType> nonDistributedBsIds = GetNonDistributedBsIds(bsIds);
            List<int> orderdBsIds = GetOrderedBsIds(nonDistributedBsIds);
            List<int> orderdBswIds = GetOrderedBswIds(nonDistributedBsIds);
            var result = new List<BillingStmtDistribution>();
            result.AddRange(distributed);

            foreach (int bsid in orderdBsIds)
            {
                int bsAmount;
                BalanceSheetModel bsModel = GetBsModel(bsid);
                BalanceSheetType bsType = bsModel.Type;
                switch (bsType)
                {
                    case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
                    case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                        bsAmount = (int)bsModel.TransferInfo.AccountsPaid.Value
                            - bsModel.GetDistributedAmount();
                        break;
                    default:
                        bsAmount = (int)(Math.Round(bsModel.GetAccountsPayable().ReceiptAmount))
                            - bsModel.GetDistributedAmount();
                        break;
                }

                int distTotal = Math.Min(bsAmount, billTotal);

                int distSubtotal;
                if (calcTax)
                {
                    int temp = (int)Math.Round(distTotal / (1 + _cp.BusinessTax), 0);
                    distSubtotal = Math.Min(temp, billSubtotal);
                }
                else
                {
                    distSubtotal = distTotal;
                }

                var spec = new BillingStmtDistribution(bsid, distTotal, distSubtotal, distTotal - distSubtotal, string.Empty, (int)BalanceSheetUseType.Deal);
                result.Add(spec);

                billTotal -= distTotal;
                billSubtotal -= distSubtotal;
                billTax -= distTotal - distSubtotal;
            }

            foreach (int bswid in orderdBswIds)
            {
                //確認做啥用的
                int bsAmount;
                BalanceSheetWmsModel bswModel = GetBswModel(bswid);
                BalanceSheetType bsType = bswModel.Type;

                bsAmount = 0 - bswModel.GetDistributedAmount();

                int distTotal = Math.Min(bsAmount, billTotal);

                int distSubtotal;
                if (calcTax)
                {
                    int temp = (int)Math.Round(distTotal / (1 + _cp.BusinessTax), 0);
                    distSubtotal = Math.Min(temp, billSubtotal);
                }
                else
                {
                    distSubtotal = distTotal;
                }

                var spec = new BillingStmtDistribution(bswid, distTotal, distSubtotal, distTotal - distSubtotal, string.Empty, (int)BalanceSheetUseType.Wms);
                result.Add(spec);

                billTotal -= distTotal;
                billSubtotal -= distSubtotal;
                billTax -= distTotal - distSubtotal;
            }

            return result;
        }

        private List<BillingStmtDistribution> DistributeByBillingStatements(Dictionary<int, BalanceSheetUseType> bsIds, bool calcTax)
        {
            var result = new List<BillingStmtDistribution>();

            Dictionary<int, BalanceSheetUseType> nonDistributedBsIds = GetNonDistributedBsIds(bsIds);
            List<int> orderdBsIds = GetOrderedBsIds(nonDistributedBsIds);

            foreach (int bsid in orderdBsIds)
            {
                int bsAmount;
                BalanceSheetModel bsModel = GetBsModel(bsid);
                BalanceSheetType bsType = bsModel.Type;
                switch (bsType)
                {
                    case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:       
                    case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                        bsAmount = (int)bsModel.TransferInfo.AccountsPaid.Value
                            - bsModel.GetDistributedAmount();   //週結已經付款了, 分配實際金額
                        break;
                    default:
                        bsAmount = (int)(Math.Round(bsModel.GetAccountsPayable().ReceiptAmount))
                            - bsModel.GetDistributedAmount();
                        break;
                }

                int bsSubtotal;
                if (calcTax)
                {
                    bsSubtotal = (int)Math.Round(bsAmount / (1 + _cp.BusinessTax), 0);
                }
                else
                {
                    bsSubtotal = bsAmount;
                }

                var spec = new BillingStmtDistribution(bsid, bsAmount, bsSubtotal, bsAmount - bsSubtotal, string.Empty, (int)BalanceSheetUseType.Deal);
                result.Add(spec);
            }

            return result;
        }

        private List<BillingStmtDistribution> DistributeByBillingStatementsWms(Dictionary<int, BalanceSheetUseType> bsIds, bool calcTax)
        {
            var result = new List<BillingStmtDistribution>();

            Dictionary<int, BalanceSheetUseType> nonDistributedBsIds = GetNonDistributedBsIds(bsIds);
            List<int> orderdBswIds = GetOrderedBswIds(nonDistributedBsIds);

            foreach (int bsid in orderdBswIds)
            {
                var spec = new BillingStmtDistribution(bsid, 0, 0, 0 - 0, string.Empty, (int)BalanceSheetUseType.Wms);
                result.Add(spec);
            }

            return result;
        }

        private string GetBillingCompanyId(int theBsId)
        {
            //todo: include SignCompanyId inside BalanceSheetModel.FundTransferInfo 
            //currently (2013-6-14), FundTransferInfo only supports Ppon ACH.            

            if (theBsId == 0)
            {
                return string.Empty;
            }
            List<int> notImportant = new List<int>();
            notImportant.Add(theBsId);

            var x = _ap.ViewBalanceSheetBillListGetListByBalanceSheetIds(notImportant).FirstOrDefault();
            if (x != null)
            {
                return x.SignCompanyID;
            }

            return string.Empty;
        }

        private string GetWmsBillingCompanyId(int theBswId)
        {
            //todo: include SignCompanyId inside BalanceSheetModel.FundTransferInfo 
            //currently (2013-6-14), FundTransferInfo only supports Ppon ACH.            

            if (theBswId == 0)
            {
                return string.Empty;
            }
            List<int> notImportant = new List<int>();
            notImportant.Add(theBswId);

            var x = _ap.ViewBalanceSheetWmsBillListGetListByBalanceSheetIds(notImportant).FirstOrDefault();
            if (x != null)
            {
                return x.SignCompanyID;
            }

            return string.Empty;
        }

        private Dictionary<int, BalanceSheetModel> bsPool = new Dictionary<int, BalanceSheetModel>();
        private Dictionary<int, BalanceSheetWmsModel> bswPool = new Dictionary<int, BalanceSheetWmsModel>();
        private BalanceSheetModel GetBsModel(int bsId)
        {
            if (!bsPool.ContainsKey(bsId))
            {
                var bsm = new BalanceSheetModel(bsId);
                bsPool.Add(bsId, bsm);
            }
            return bsPool[bsId];
        }

        private BalanceSheetWmsModel GetBswModel(int bsId)
        {
            if (!bswPool.ContainsKey(bsId))
            {
                var bsm = new BalanceSheetWmsModel(bsId);
                bswPool.Add(bsId, bsm);
            }
            return bswPool[bsId];
        }

        private List<int> GetOrderedBsIds(Dictionary<int, BalanceSheetUseType> nonDistributedBsIds)
        {
            List<BalanceSheetModel> bsModels = new List<BalanceSheetModel>();
            foreach (var bsid in nonDistributedBsIds)
            {
                if (bsid.Value == BalanceSheetUseType.Deal)
                    bsModels.Add(GetBsModel(bsid.Key));
            }
            return bsModels.OrderBy(x => x.Year)
                .ThenBy(x => x.Month)
                .ThenBy(x => x.Day)
                .Select(x => x.Id).ToList();
        }
        private List<int> GetOrderedBswIds(Dictionary<int, BalanceSheetUseType> nonDistributedBsIds)
        {
            List<BalanceSheetWmsModel> bswModels = new List<BalanceSheetWmsModel>();
            foreach (var bsid in nonDistributedBsIds)
            {
                if (bsid.Value == BalanceSheetUseType.Wms)
                    bswModels.Add(GetBswModel(bsid.Key));
            }
            return bswModels.OrderBy(x => x.Year)
                .ThenBy(x => x.Month)
                .ThenBy(x => x.Day)
                .Select(x => x.Id).ToList();
        }

        private Dictionary<int, BalanceSheetUseType> GetNonDistributedBsIds(Dictionary<int, BalanceSheetUseType> bsIds)
        {
            List<int> distributedBsIds = this.GetDistributedDistribution().Select(x => x.BalanceSheetId).ToList();
            foreach (var bsid in bsIds)
            {
                if (!distributedBsIds.Contains(bsid.Key) && bsid.Key != 0)
                {
                    //yield return bsid;
                }
                else
                {
                    bsIds.Remove(bsid.Key);
                }
            }

            return bsIds;
        }


        private BillInfo modInfo;
        private IEnumerable<BillingStmtDistribution> modDistributions;
        internal BillInfo GetModInfo()
        {
            return modInfo;
        }
        internal IEnumerable<BillingStmtDistribution> GetModDistribution()
        {
            return modDistributions;
        }


        #endregion

    }

    public class BillingStmtDistribution
    {        
        public int BalanceSheetId{ get; private set; }                
        public int Total{ get; private set; }        
        public int Subtotal{ get; private set; }        
        public int Tax{ get; private set; }
        public string Remark{ get; private set; }
        public int Type { get; private set; }

        public BillingStmtDistribution(int balanceSheetId, int total, int subtotal, int tax, string remark, int type)
        {
            BalanceSheetId = balanceSheetId;
            Total = total;
            Subtotal = subtotal;
            Tax = tax;
            Remark = remark;
            Type = type;
        }
    }

    public class BillInfo
    {
        public VendorReceiptType BillType { get; set; }
        public string BillTypeOtherName { get; set; }
        public bool IsInvoiceTaxRequired { get; set; }
        public string BillNumber { get; set; }
        public BillBuyerType BuyerType { get; set; }
        public DateTime? BillDate { get; set; }
        /// <summary>
        /// 單據開立公司統編
        /// </summary>
        public string BillingCompanyId { get; set; }
        public int Total { get; set; }
        public int Subtotal { get; set; }
        public int Tax { get; set; }
        public string Remark { get; set; }
    }
}

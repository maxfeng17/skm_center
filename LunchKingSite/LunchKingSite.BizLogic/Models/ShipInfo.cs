﻿using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models
{
    /// <summary>
    /// 一般宅配出貨資訊
    /// </summary>
    public class ShipInfo
    {
        /// <summary>
        /// 出貨方式
        /// </summary>
        public int ProductDeliveryType { get; set; }
        /// <summary>
        /// 宅配商品配送狀態
        /// </summary>
        public ShippingStatus ShipStatus { get; set; }
        /// <summary>
        /// 宅配商品配送狀態說明
        /// </summary>
        public string ShipStatusDesc { get; set; }
        /// <summary>
        /// 物流商名稱
        /// </summary>
        public string ShipCompanyName { get; set; }
        /// <summary>
        /// 物流單編號
        /// </summary>
        public string ShipNo { get; set; }
        /// <summary>
        /// 物流商貨態查詢連結 
        /// </summary>
        public string ShipWebsite { get; set; }
        /// <summary>
        /// 超取店舖資訊
        /// </summary>
        public PickupStore PickupStore { get; set; }
        /// <summary>
        /// 買家取件時間
        /// </summary>
        public string CustomerReceivedTime { get; set; }

        /// <summary>
        /// 預計出貨日
        /// </summary>
        [JsonIgnore]
        public string LastShippingDate { get; set; }
        /// <summary>
        /// 實際出貨日
        /// </summary>
        [JsonIgnore]
        public string ActualShippingDate { get; set; }

        public ShipInfo()
        {
            ProductDeliveryType = (int)Core.ProductDeliveryType.Normal;
            ShipStatus = ShippingStatus.ReadyToShip;
            ShipStatusDesc = string.Empty;
            ShipCompanyName = string.Empty;
            ShipNo = string.Empty;
            ShipWebsite = string.Empty;
            PickupStore = new PickupStore();
            CustomerReceivedTime = string.Empty;
            LastShippingDate = string.Empty;
            ActualShippingDate = string.Empty;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model
{
    public class RegisterContactMemberReplyData
    {
        public RegisterContactMemberReplyData(MemberRegisterReplyType reply , string message)
        {
            Reply = reply;
            ReplyMessage = message;
        }

        /// <summary>
        /// 註冊結果
        /// </summary>
        public MemberRegisterReplyType Reply { get; set; }
        /// <summary>
        /// 回傳訊息
        /// </summary>
        public string ReplyMessage { get; set; }
        public string UserName { get; set; }
    }
}

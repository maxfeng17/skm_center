﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class ProductInfoModel
    {
        public Guid ProductGuid { get; set; }
        /// <summary>
        /// 賣家序號
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 是否為多規格
        /// </summary>
        public bool IsMulti { get; set; }
        /// <summary>
        /// 品牌名稱
        /// </summary>
        public string ProductBrandName { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// 商品明細
        /// </summary>
        public List<ProductItemModel> ProductItems { get; set; }
        /// <summary>
        /// 0 一般宅配(自出含快出與超取)
        /// 1 24H到貨(倉儲+物流)
        /// </summary>
        public int WarehouseType { get; set; }

    }
    public class ProductItemModel
    {
        public Guid Gid { get; set; }
        /// <summary>
        /// 17Life商品編號
        /// </summary>
        public string ProductNo { get; set; }
        /// <summary>
        /// 原廠貨號
        /// </summary>
        public string ProductCode { get; set; }
        /// <summary>
        /// 全球交易識別碼
        /// </summary>
        public string GTINs { get; set; }
        /// <summary>
        /// 製造商零件編號
        /// </summary>
        public string Mpn { get; set; }
        /// <summary>
        /// 原始庫存
        /// </summary>
        public int OriStock { get; set; }
        /// <summary>
        /// 庫存
        /// </summary>
        public int Stock { get; set; }
        public string AdjustType { get; set; }
        /// <summary>
        /// 銷售量
        /// </summary>
        public int Sales { get; set; }
        /// <summary>
        /// 安全庫存
        /// </summary>
        public int SafetyStock { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        #region PC24h
        /// <summary>
        /// 0 17虛疑倉儲
        /// 1 24小時到貨，使用Pchome倉儲
        /// </summary>
        public int WarehouseType { get; set; }
        /// <summary>
        /// 保存日期
        /// </summary>
        public int? ShelfLife { get; set; }
        /// <summary>
        /// 保存價格
        /// </summary>
        public int? Price { get; set; }

        #endregion

        public List<ProductOptionModel> ProductOptions { get; set; }
    }


    public class ProductItemContentModel
    {
        public ProductInfo Products { get; set; }
        public List<ProductItem> ProductItems { get; set; }
        public List<ProductSpec> Specs { get; set; }
    }

    public class ProductOptionModel
    {
        public Guid Gid { get; set; }
        public int Index { get; set; }
        public string CatgName { get; set; }
        public string ItemName { get; set; }
    }

    public class ProductItemSourceModel
    {
        public int Index { get; set; }
        public string CatgName { get; set; }
        public List<Channel.ItemCategory> Items { get; set; }
    }

    public class SerializeableCategoryItem
    {
        public Guid Id { get; set; }
        public List<Channel.ItemCategory> Items { get; set; }
    }

    public class ProductExcelBatchModel
    {
        public string GroupId { get; set; }
        public string SpecType { get; set; }
        public string IsMulti { get; set; }
        public string ProductBrandName { get; set; }
        public string ProductName { get; set; }
        public string CatgName { get; set; }
        public string SpecName { get; set; }
        public string Gtins { get; set; }
        public string Mpn { get; set; }
        public string ProductCode { get; set; }
        public string Stock { get; set; }
        public string SaftyStock { get; set; }
        public string Memo { get; set; }
        public string WarehouseType { get; set; }
        public string Price { get; set; }
        public string ShelfLife { get; set; }
        
    }
}

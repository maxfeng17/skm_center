﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{  
    public class UserScashTransactionInfo
    {
        public DateTime CreateTime { get; set; }
        public int? OrderClassification { get; set; }
        public string OrderId { get; set; }
        public Guid? OrderGuid { get; set; }
        public int? OrderStatus { get; set; }
        public decimal? Amount { get; set; }
        public int WithdrawalId { get; set; }
        public string Message { get; set; }
        public string SellerName { get; set; }
        public decimal? SubTotal { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}={2}", 
                CreateTime.ToString("yyyy/MM/dd HH:mm") , 
                Amount > 0 ? "+" + Amount : Amount.ToString(), 
                SubTotal ?? 0);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{
    public static class HTCDataModel
    {
        public const string HtcDateTimeFormat = "ddd, dd MMM yyyy HH:mm:ss K";


        private static HtcDataProvider _defaultDealProvider;
        public static HtcDataProvider DefaultDealProvider
        {
            get
            {
                if(_defaultDealProvider == null)
                {
                    _defaultDealProvider = new HtcDataProvider()
                                               {
                                                   name = "17Life",
                                                   logo =
                                                       "https://www.17life.com/Themes/default/images/17Life/NewHomepage/17LifeNewHomepage2_03.jpg"
                                               };
                }
                return _defaultDealProvider;
            }
        }
    }

    public class HtcDealRtn
    {
        public HtcDealRtnAttributes attributes { get; set; }
        public HtcDeals deals { get; set; }

        public HtcDealRtn()
        {
            attributes = new HtcDealRtnAttributes() { code = "200", message = "OK" };
        }

        public class HtcDealRtnAttributes
        {
            public string code { get; set; }
            public string message { get; set; }
        }
    }



    public class HtcDeals
    {
        public HtcDealsAttributes attributes { get; set; }
        public string modification_date { get; set; }
        public List<HtcPponDeal> deal { get; set; }

        public HtcDeals()
        {
            attributes = new HtcDealsAttributes(){version = "1.0"};
            deal = new List<HtcPponDeal>();
        }

        public class HtcDealsAttributes
        {
            public string version { get; set; }
        }
    }

    public class HtcPponDeal
    {
        public string id_deal { get; set; }
        public HtcDataProvider deal_provider { get; set; }
        public string featured { get; set; }
        public HtcCategory categories { get; set; }
        public string national { get; set; }
        public string country { get; set; }
        public string price { get; set; }
        public string reduction { get; set; }
        public string savings { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public HtcCondition conditions { get; set; }
        public HtcLanguage language { get; set; }
        //public List<HtcDescription> DESCRIPTION { get; set; }
        public HtcMerchant merchant { get; set; }
        //public List<HtcStoreLocation> STORES { get; set; }

        public HtcPponDeal()
        {
        }
    }

    public class HtcDataProvider
    {
        public string name { get; set; }
        public string logo { get; set; }
    }

    public class HtcCategory
    {
        public string category { get; set; }
    }

    /// <summary>
    /// 好康限制
    /// </summary>
    public class HtcCondition
    {
        public string min_number_purchaser { get; set; }
        public string max_number_purchaser { get; set; }
        public string redeemed_start_date { get; set; }
        public string redeemed_end_date { get; set; }
    }

    public class HtcLanguage
    {
        public HtcLanguageAttributes attributes { get; set; }
        public string catch_phrase { get; set; }
        public string short_description { get; set; }
        public string long_description { get; set; }
        public string purchase_url { get; set; }
        public string preview_image { get; set; }
        public string discount_conditions { get; set; }
        public string action { get; set; }
        public class HtcLanguageAttributes
        {
            public string id { get; set; }
        }
    }

    //public class HtcDescription
    //{
    //    public string LANGUAGE { get; set; }
    //    public string CATCH_PHRASE { get; set; }
    //    public string SHORT_DESCRIPTION { get; set; }
    //    public string LONG_DESCRIPTION { get; set; }
    //    public string PURCHASE_URL { get; set; }
    //    public string PREVIEW_IMAGE { get; set; }
    //    //public string DISCOUNT_CONDITIONS { get; set; }
    //}

    public class HtcMerchant
    {
        public string merchant_name { get; set; }
        public string web_address { get; set; }
        public HtcLocation location { get; set; }
    }

    public class HtcLocation
    {
        public string postal_address { get; set; }
        public string phone_number { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
    }
}

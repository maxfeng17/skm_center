﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.BizLogic.Model
{
    public class PponDealPreviewData
    {
        public PponDealPreviewData()
        {
            HasCategoryIdList = new HashSet<int>();
            HasIconList = new HashSet<DealLabelSystemCode>();
            //ChannelIdList = new List<int>();
            //ChannelAreaIdList = new List<int>();
            //DealCategoryIdList = new List<int>();
        }

        /// <summary>
        /// 檔次編號
        /// </summary>
        public int DealId { get; set; }
        /// <summary>
        /// 檔次BID
        /// </summary>
        public Guid BusinessHourGuid { get; set; }
        /// <summary>
        /// 檔次擁有的所有category的id
        /// </summary>
        public HashSet<int> HasCategoryIdList { get; set; }
        /// <summary>
        /// 檔次有的icon
        /// </summary>
        public HashSet<DealLabelSystemCode> HasIconList { get; set; }
        /// <summary>
        /// 頻道類型的ID
        /// </summary>
        //public List<int> ChannelIdList { get; set; }
        /// <summary>
        /// 頻道區域類型的ID
        /// </summary>
        //public List<int> ChannelAreaIdList { get; set; }
        /// <summary>
        /// 檔次分類的類型ID
        /// </summary>
        //public List<int> DealCategoryIdList { get; set; }
        /// <summary>
        /// 檔次開賣時間
        /// </summary>
        public DateTime BusinessOrderTimeS { get; set; }
        /// <summary>
        /// 檔次截止時間
        /// </summary>
        public DateTime BusinessOrderTimeE { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 是否隱藏
        /// </summary>
        public bool IsHiddenInRecommendDeal { get; set; }
        /// <summary>
        /// 是否售完
        /// </summary>
        public bool IsSoldOut { get; set; }
        ///// <summary>
        ///// 檔次於各類別下的排序狀況
        ///// </summary>
        //public CategorySequenceMap CategorySequenceData { get; set; }

        #region public method
        public PponDealPreviewData Copy()
        {
            PponDealPreviewData rtnObject = new PponDealPreviewData();
            rtnObject.DealId = DealId;
            rtnObject.BusinessHourGuid = BusinessHourGuid;
            foreach (var categoryId in HasCategoryIdList)
            {
                if (rtnObject.HasCategoryIdList.Contains(categoryId) == false)
                {
                    rtnObject.HasCategoryIdList.Add(categoryId);
                }
            }
            foreach (var icon in HasIconList)
            {
                if (rtnObject.HasIconList.Contains(icon) == false)
                {
                    rtnObject.HasIconList.Add(icon);
                }
            }
            //rtnObject.ChannelIdList.AddRange(ChannelAreaIdList);
            //rtnObject.DealCategoryIdList.AddRange(DealCategoryIdList);
            rtnObject.BusinessOrderTimeS = BusinessOrderTimeS;
            rtnObject.BusinessOrderTimeE = BusinessOrderTimeE;
            rtnObject.Price = Price;
            rtnObject.IsHiddenInRecommendDeal = IsHiddenInRecommendDeal;
            rtnObject.IsSoldOut = IsSoldOut;
            return rtnObject;
        }
        #endregion public method
    }

    public class CategorySequenceMap
    {
        public List<CategorySequence> CategorySequenceList { get; private set; }

        public CategorySequenceMap()
        {
            CategorySequenceList = new List<CategorySequence>();
        }


        public CategorySequence SequenceData(int categoryId)
        {
            List<CategorySequence> seqList = CategorySequenceList.Where(x => x.CategoryId == categoryId).ToList();
            if (seqList.Count > 0)
            {
                CategorySequence rtnData = seqList.First();
                return rtnData;
            }
            return CategorySequence.Entity;    
        }
    }

    public class CategorySequence
    {
        #region static
        /// <summary>
        /// 設定CategorySequence的預設空值
        /// </summary>
        public static CategorySequence Entity = new CategorySequence(-1,int.MaxValue);

        public static bool IsNullOrEmpty(CategorySequence categorySequence)
        {
            if (categorySequence == null || Entity.Equals(categorySequence))
            {
                return true;
            }
            return false;
        }
        #endregion static

        /// <summary>
        /// 建構元，當categoryId或sequence 小於0時，會直接設定為entity的值 categoryId=-1,sequence=int.MaxValue
        /// </summary>
        /// <param name="categoryId">類別ID</param>
        /// <param name="sequence">排序的序號</param>
        public CategorySequence(int categoryId, int sequence)
        {
            if ((categoryId < 0) || (sequence < 0))
            {
                CategoryId = -1;
                Sequence = int.MaxValue;
            }
            else
            {
                CategoryId = categoryId;
                Sequence = sequence;
            }
            CategorySequenceList = new List<CategorySequence>();
        }

        /// <summary>
        /// 類別Id
        /// </summary>
        public int CategoryId { get; set; }
        /// <summary>
        /// 設定的此類別ID的排序序號
        /// </summary>
        public int Sequence { get; set; }
        /// <summary>
        /// 子類別的排序資料
        /// </summary>
        public List<CategorySequence> CategorySequenceList { get; private set; }

        /// <summary>
        /// 取得某個子項目的排序資料物件
        /// </summary>
        /// <param name="categoryId">子項目的category id</param>
        /// <returns></returns>
        public CategorySequence SequenceData(int categoryId)
        {
            List<CategorySequence> seqList = CategorySequenceList.Where(x => x.CategoryId == categoryId).ToList();
            if (seqList.Count > 0)
            {
                CategorySequence rtnData = seqList.First();
                return rtnData;
            }
            return CategorySequence.Entity;
        }

        #region override
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            CategorySequence c = obj as CategorySequence;
            if ((object)c == null)
            {
                return false;
            }

            return (Sequence == c.Sequence) && (CategoryId == c.CategoryId);
        }

        public override int GetHashCode()
        {
            return string.Format("{0}_{1}", this.CategoryId, this.Sequence).GetHashCode();
        }
        #endregion override
    }
}

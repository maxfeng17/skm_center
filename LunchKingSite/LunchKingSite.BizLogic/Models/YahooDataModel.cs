﻿using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model
{
    public static class YahooDataModel
    {

    }

    public class YahooDeal
    {
        #region Field
        /// <summary>
        /// 17Life檔次bid/品生活商品檔次pid
        /// </summary>
        [XmlElementName("id")]
        public string Id;
        /// <summary>
        /// 串接動作(新增檔次=new、修改檔次=update、刪除檔次=delete)
        /// </summary>
        [XmlElementName("action")]
        public string Action;
        /// <summary>
        /// JPG格式的商品圖片URL(size=390x260)
        /// </summary>
        [XmlElementName("img")]
        public string Img;
        /// <summary>
        /// 原價
        /// </summary>
        [XmlElementName("cost_price")]
        public string CostPrice;
        /// <summary>
        /// 賣價
        /// </summary>
        [XmlElementName("prefer_price")]
        public string PerferPrice;
        /// <summary>
        /// 商品分類
        /// (餐廳美食=food、美容保養=beauty、 時尚流行=fashion、旅遊=travel、宅配=delivery)
        /// </summary>
        [XmlElementName("category")]
        public string Category;
        /// <summary>
        /// 檔次標題
        /// </summary>
        [XmlElementName("title")]
        public string Title;
        /// <summary>
        /// 檔次連結
        /// </summary>
        [XmlElementName("link")]
        public string Link;
        /// <summary>
        /// 檔次說明
        /// </summary>
        [XmlElementName("desc")]
        public string Desc;
        /// <summary>
        /// 檔次連結
        /// </summary>
        [XmlElementName("area")]
        public List<YahooArea> Area;
        /// <summary>
        /// 檔次開賣開始時間
        /// </summary>
        [XmlElementName("show_datetime")]
        public string ShowDatetime;
        /// <summary>
        /// 檔次開賣結束時間
        /// </summary>
        [XmlElementName("end_datetime")]
        public string EndDatetime;
        /// <summary>
        /// 手機版檔次連結
        /// </summary>
        [XmlElementName("mobile_link")]
        public string MobileLink;

        public YahooDeal()
        {
            Area = new List<YahooArea>();
        }
        #endregion Field
    }

    public class YahooPponDeal
    {
        #region Field
        /// <summary>
        /// 17Life檔次bid/品生活商品檔次pid
        /// </summary>
        [XmlElementName("id")]
        public string Id;
        /// <summary>
        /// 串接動作(新增檔次=new、修改檔次=update、刪除檔次=delete)
        /// </summary>
        [XmlElementName("action")]
        public string Action;
        /// <summary>
        /// JPG格式的商品圖片URL(size=390x260)
        /// </summary>
        [XmlElementName("img")]
        public string Img;
        /// <summary>
        /// JPG格式的商品圖片URL2(size=390x260)
        /// </summary>
        [XmlElementName("img2")]
        public string Img2;
        /// <summary>
        /// JPG格式的商品圖片URL3(size=390x260)
        /// </summary>
        [XmlElementName("img3")]
        public string Img3;
        /// <summary>
        /// 原價
        /// </summary>
        [XmlElementName("cost_price")]
        public string CostPrice;
        /// <summary>
        /// 賣價
        /// </summary>
        [XmlElementName("prefer_price")]
        public string PerferPrice;
        /// <summary>
        /// 商品分類
        /// (餐廳美食=food、美容保養=beauty、 時尚流行=fashion、旅遊=travel、宅配=delivery)
        /// </summary>
        [XmlElementName("category")]
        public string Category;
        /// <summary>
        /// 檔次標題
        /// </summary>
        [XmlElementName("title")]
        public string Title;
        /// <summary>
        /// 檔次連結
        /// </summary>
        [XmlElementName("link")]
        public string Link;
        /// <summary>
        /// 檔次說明
        /// </summary>
        [XmlElementName("desc")]
        public string Desc;
        /// <summary>
        /// 檔次連結
        /// </summary>
        [XmlElementName("area")]
        public List<YahooArea> Area;
        /// <summary>
        /// 檔次開賣開始時間
        /// </summary>
        [XmlElementName("show_datetime")]
        public string ShowDatetime;
        /// <summary>
        /// 檔次開賣結束時間
        /// </summary>
        [XmlElementName("end_datetime")]
        public string EndDatetime;
        /// <summary>
        /// 手機版檔次連結
        /// </summary>
        [XmlElementName("mobile_link")]
        public string MobileLink;
        /// <summary>
        /// 商品注意事項
        /// </summary>
        [XmlElementName("notice")]
        public string Notice;
        /// <summary>
        /// 商品詳細介紹
        /// </summary>
        [XmlElementName("more_detail")]
        public string MoreDetail;
        /// <summary>
        /// 商家資料
        /// </summary>
        [XmlElementName("shop")]
        public List<YahooShop> Shop;

        public YahooPponDeal()
        {
            Area = new List<YahooArea>();
        }
        #endregion Field
    }

    public class NewYahooPponDeal
    {
        #region Field
        /// <summary>
        /// 17Life檔次bid/品生活商品檔次pid
        /// </summary>
        [XmlElementName("id")]
        public string Id;
        /// <summary>
        /// 串接動作(新增檔次=new、修改檔次=update、刪除檔次=delete)
        /// </summary>
        [XmlElementName("action")]
        public string Action;
        /// <summary>
        /// JPG格式的商品圖片URL(size=390x260)
        /// </summary>
        [XmlElementName("img")]
        public string Img;
        /// <summary>
        /// JPG格式的商品圖片URL2(size=390x260)
        /// </summary>
        [XmlElementName("img2")]
        public string Img2;
        /// <summary>
        /// JPG格式的商品圖片URL3(size=390x260)
        /// </summary>
        [XmlElementName("img3")]
        public string Img3;
        /// <summary>
        /// 原價
        /// </summary>
        [XmlElementName("cost_price")]
        public string CostPrice;
        /// <summary>
        /// 賣價
        /// </summary>
        [XmlElementName("prefer_price")]
        public string PerferPrice;
        /// <summary>
        /// 已售出數量
        /// </summary>
        [XmlElementName("sold")]
        public string Sold;
        /// <summary>
        /// 商品分類
        /// (餐廳美食=food、美容保養=beauty、 時尚流行=fashion、旅遊=travel、宅配=delivery)
        /// </summary>
        [XmlElementName("category")]
        public string Category;
        /// <summary>
        /// 檔次標題
        /// </summary>
        [XmlElementName("title")]
        public string Title;
        /// <summary>
        /// 檔次連結
        /// </summary>
        [XmlElementName("link")]
        public string Link;
        /// <summary>
        /// 檔次說明
        /// </summary>
        [XmlElementName("desc")]
        public string Desc;
        /// <summary>
        /// 檔次連結
        /// </summary>
        [XmlElementName("area")]
        public List<NewYahooArea> Area;
        /// <summary>
        /// 檔次開賣開始時間
        /// </summary>
        [XmlElementName("show_datetime")]
        public string ShowDatetime;
        /// <summary>
        /// 檔次開賣結束時間
        /// </summary>
        [XmlElementName("end_datetime")]
        public string EndDatetime;
        /// <summary>
        /// 手機版檔次連結
        /// </summary>
        [XmlElementName("mobile_link")]
        public string MobileLink;
        /// <summary>
        /// 商品注意事項
        /// </summary>
        [XmlElementName("notice")]
        public string Notice;
        /// <summary>
        /// 商品詳細介紹
        /// </summary>
        [XmlElementName("more_detail")]
        public string MoreDetail;
        /// <summary>
        /// 商家資料
        /// </summary>
        [XmlElementName("shop")]
        public List<YahooShop> Shop;

        public NewYahooPponDeal()
        {
            Area = new List<NewYahooArea>();
        }
        #endregion Field
    }

    /// <summary>
    /// 檔次顯示區域
    /// </summary>
    public class YahooArea
    {
        /// <summary>
        /// 北區縣市(taipei、taoyuan、hsinchu、keelung)
        /// </summary>
        [XmlElementName("n")]
        public string N;

        /// <summary>
        /// 中區縣市(taichung、miaoli、changhua、nantou、yunlin)
        /// </summary>
        [XmlElementName("c")]
        public string C;

        /// <summary>
        /// 南區縣市(kaohsiung、tainan、chiayi、pingtung)
        /// </summary>
        [XmlElementName("s")]
        public string S;
    }

    /// <summary>
    /// 檔次顯示區域
    /// </summary>
    public class NewYahooArea
    {

        /// <summary>
        /// 不分區
        /// </summary>
        [XmlElementName("ALL")]
        public string ALL = "0";

        /// <summary>
        /// 台北
        /// </summary>
        [XmlElementName("TAIPEI")]
        public string TAIPEI = "0";

        /// <summary>
        /// 新北
        /// </summary>
        [XmlElementName("NEW_TAIPEI")]
        public string NEW_TAIPEI = "0";

        /// <summary>
        /// 新竹
        /// </summary>
        [XmlElementName("HSINCHU")]
        public string HSINCHU = "0";

        /// <summary>
        /// 桃園
        /// </summary>
        [XmlElementName("TAOYUAN")]
        public string TAOYUAN = "0";

        /// <summary>
        /// 台中
        /// </summary>
        [XmlElementName("TAICHUNG")]
        public string TAICHUNG = "0";

        /// <summary>
        /// 台南
        /// </summary>
        [XmlElementName("TAINAN")]
        public string TAINAN = "0";

        /// <summary>
        /// 高雄
        /// </summary>
        [XmlElementName("KAOHSIUNG")]
        public string KAOHSIUNG = "0";

        /// <summary>
        /// 苗栗
        /// </summary>
        [XmlElementName("MIAOLI")]
        public string MIAOLI = "0";

        /// <summary>
        /// 彰化
        /// </summary>
        [XmlElementName("CHANGHUA")]
        public string CHANGHUA = "0";

        /// <summary>
        /// 南投
        /// </summary>
        [XmlElementName("NANTOU")]
        public string NANTOU = "0";

        /// <summary>
        /// 雲林
        /// </summary>
        [XmlElementName("YUNLIN")]
        public string YUNLIN = "0";

        /// <summary>
        /// 嘉義
        /// </summary>
        [XmlElementName("CHIAYI")]
        public string CHIAYI = "0";

        /// <summary>
        /// 屏東
        /// </summary>
        [XmlElementName("PINGTUNG")]
        public string PINGTUNG = "0";

        /// <summary>
        /// 基隆
        /// </summary>
        [XmlElementName("KEELUNG")]
        public string KEELUNG = "0";

        /// <summary>
        /// 宜蘭
        /// </summary>
        [XmlElementName("YILAN")]
        public string YILAN = "0";

        /// <summary>
        /// 花蓮
        /// </summary>
        [XmlElementName("HUALIEN")]
        public string HUALIEN = "0";

        /// <summary>
        /// 台東
        /// </summary>
        [XmlElementName("TAITUNG")]
        public string TAITUNG = "0";

        /// <summary>
        /// 離島, 外島
        /// </summary>
        [XmlElementName("OTHER")]
        public string OTHER = "0";

    }

    /// <summary>
    /// 商家
    /// </summary>
    public class YahooShop
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElementName("shop_info")]
        public List<YahooShopInfo> ShopInfo;
    }

    /// <summary>
    /// 商家資訊
    /// </summary>
    public class YahooShopInfo
    {
        /// <summary>
        /// 商家名稱
        /// </summary>
        [XmlElementName("name")]
        public string Name;
        /// <summary>
        /// 商家名稱
        /// </summary>
        [XmlElementName("add")]
        public string Address;
        /// <summary>
        /// 商家名稱
        /// </summary>
        [XmlElementName("tel")]
        public string Tel;
        /// <summary>
        /// 商家名稱
        /// </summary>
        [XmlElementName("other")]
        public string Other;
    }
}

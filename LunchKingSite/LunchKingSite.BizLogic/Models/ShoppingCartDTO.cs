﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * 每台購物車內有多個子購物車(By賣家/運費不同而分不同的子購物車)
 * 每台子購物車內有多個商品
 * */
namespace LunchKingSite.BizLogic.Model
{
    public class LiteMainCart
    {
        public LiteMainCart()
        {
            LiteCart = new List<Model.LiteCart>();
        }
        #region properties
        /// <summary>
        /// 賣家
        /// </summary>
        public List<LiteCart> LiteCart { get; set; }
        /// <summary>
        /// 購物車數量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 購物車金額
        /// </summary>
        public int Amount { get; set; }
        public string TransactionId { get; set; }

        #endregion properties
    }

    public class MainCartDTO
    {
        public MainCartDTO()
        {
            IsMasterPass = false;
            SmallCart = new List<Model.SmallCart>();
        }
        #region properties
        //public string BuyerEmail { get; set; }
        //public string BuyerName { get; set; }
        //public string BuyerMobile { get; set; }
        //public string ReceiverName { get; set; }
        //public string ReceiverMobile { get; set; }
        //public string BuyerCity { get; set; }
        //public string BuyerArea { get; set; }
        //public string ReceiverAddress { get; set; }
        public string RememberUserInfo { get; set; }
        public string PersonalCarrier { get; set; }
        public string CarrierType { get; set; }
        /// <summary>
        /// 手機條碼
        /// </summary>
        public string PhoneCarrier { get; set; }
        /// <summary>
        /// 自然人憑證
        /// </summary>
        public string PersonalCertificateCarrier { get; set; }
        /// <summary>
        /// 發票收件人
        /// </summary>
        public string InvoiceBuyerName { get; set; }
        /// <summary>
        /// 發票收件地址
        /// </summary>
        public string InvoiceAddress { get; set; }
        /// <summary>
        /// 捐贈單位
        /// </summary>
        public string InvoiceDonation { get; set; }
        /// <summary>
        /// 發票抬頭
        /// </summary>
        public string InvoiceTitle { get; set; }
        /// <summary>
        /// 統一編號
        /// </summary>
        public string InvoiceNumber { get; set; }
        /// <summary>
        /// 發票收件人
        /// </summary>
        public string InvoiceBuyerName2 { get; set; }
        /// <summary>
        /// 發票收件地址
        /// </summary>
        public string InvoiceAddress2 { get; set; }
        /// <summary>
        /// 賣家
        /// </summary>
        public List<SmallCart> SmallCart { get; set; }
        /// <summary>
        /// 購物車數量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 購物車金額
        /// </summary>
        public int Amount { get; set; }
        /// <summary>
        /// 發票捐贈
        /// </summary>
        public DonationReceiptsType ReceiptsType { get; set; }
        /// <summary>
        /// 愛心碼
        /// </summary>
        public string LoveCode { get; set; }
        /// <summary>
        /// 二聯式、三聯式
        /// </summary>
        public string InvoiceType { get; set; }
        public string TransactionId { get; set; }
        /// <summary>
        /// Is pay by ATM
        /// </summary>
        public bool IsPaidByATM { get; set; }
        /// <summary>
        /// 使用 MasterPass
        /// </summary>
        public bool IsMasterPass { set; get; }
        /// <summary>
        /// MasterPass Data
        /// </summary>
        public MasterPassTrans MasterPassTransData { set; get; }
        /// <summary>
        /// credit card number
        /// </summary>
        public string CreditcardNumber { get; set; }

        /// <summary>
        /// credit card security code
        /// </summary>
        public string CreditcardSecurityCode { get; set; }

        /// <summary>
        /// credit card expire year in 2 digits
        /// </summary>
        public string CreditcardExpireYear { get; set; }

        /// <summary>
        /// credit card expire month in 2 digits
        /// </summary>
        public string CreditcardExpireMonth { get; set; }
        /// <summary>
        /// PayEasy購物金
        /// </summary>
        //public int PayEasyCash { get; set; }

        /// <summary>
        /// bonus cash
        /// </summary>
        public int BCash { get; set; }

        /// <summary>
        /// 17Life cash
        /// </summary>
        public int SCash { get; set; }

        /// <summary>
        /// payeasy cash
        /// </summary>
        public decimal PCash { get; set; }
        public ApiUserDeviceType apiUserDeviceType { get; set; }
        /// <summary>
        /// 付款方式
        /// </summary>
        public PaymentType OrderPaymentType { get; set; }
        /// <summary>
        /// buyer and receiver address info, the specified format is:
        /// "BuyerLastName|BuyerFirstName|BuyerMobile|BuyerCityId|BuyerAreaId|BuyerBuildingGuid|BuyerAddress^ReceiverName|ReceiverMobile|ReceiverCityId|ReceiverAreaId|ReceiverBuildingId|ReceiverAddress|IsAddReceiverInfo"
        /// just like "馬|魚|0917171717|294|317|8c3b0067-f777-4461-9d76-d51fae14c009|44號^馬魚444|0917173213|294|317|04803868-3196-45d0-975c-74a0d2e113a6|44號|False".
        /// </summary>
        public string AddressInfo { get; set; }
        /// <summary>
        /// delivery info
        /// </summary>
        public PponDeliveryInfo DeliveryInfo { get; set; }

        /// <summary>
        /// 取得收件者地址資料，該資料由 AddressInfo 依據其內容解析產出，若無收件人資料將回傳NULL
        /// </summary>
        /// <returns></returns>
        public LkAddress GetReceiverAddress()
        {
            if (!string.IsNullOrWhiteSpace(AddressInfo))
            {
                string[] yy = AddressInfo.Split('^');
                //收件人部分
                if (!string.IsNullOrWhiteSpace(yy[1]))
                {
                    LkAddress address = new LkAddress();
                    string[] radd = yy[1].Split('|');
                    address.CityId = int.Parse(radd[2]);
                    address.TownshipId = int.Parse(radd[3]);
                    address.BuildingGuid = Guid.Parse(radd[4]);
                    address.AddressDesc = radd[5];
                    return address;
                }
            }
            return null;
        }
        #endregion properties
    }

    public class LiteCart
    {
        public LiteCart()
        {
            PaymentDTOList = new List<PaymentDTOLite>();
            IsChecked = true;
        }
        #region properties
        /// <summary>
        /// 賣家Guid
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public int FreightsId { get; set; }
        /// <summary>
        /// 運費描述：滿$1000免收運費$80
        /// </summary>
        public string FreightsDescription { get; set; }
        /// <summary>
        /// 商品
        /// </summary>
        public List<PaymentDTOLite> PaymentDTOList { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public int Freights { get; set; }
        /// <summary>
        /// 子購物車金額
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 是否勾選
        /// </summary>
        public bool IsChecked { get; set; }
        #endregion properties
    }

    public class PaymentDTOLite
    {
        #region properties

        /// <summary>
        /// Ticket ID
        /// </summary>
        public string TicketId { get; set; }

        /// <summary>
        /// seller guid
        /// </summary>
        public Guid SellerGuid { get; set; }

        /// <summary>
        /// Session ID
        /// </summary>
        public string SessionId { get; set; }


        public int PponDeliveryType { get; set; }

        /// <summary>
        /// bussiness hour guid
        /// </summary>
        public Guid BusinessHourGuid { get; set; }
        public string DealName { get; set; }
        public string EventName { get; set; }
        public string DealPic { get; set; }

        public CartDealStatus DealStatus { get; set; }


        /// <summary>
        /// Item Quantity
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// order total amount
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// freights
        /// </summary>
        public int Freights { get; set; }

        /// <summary>
        /// 檔次是否有適用的折價券
        /// </summary>
        public bool IsDealHasDiscount { get; set; }

        /// <summary>
        /// branch store guid
        /// </summary>
        public Guid SelectedStoreGuid { get; set; }

        /// <summary>
        /// item options, specified format is
        /// "OptionName|^|OptionGuid,OptionName|^|OptionGuid|#|ItemQuantity||OptionName|^|OptionGuid|#|ItemQuantity", just like
        /// "L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|2||L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|3"
        /// </summary>
        public List<CartItemOptionList> ItemOptions { get; set; }

        /// <summary>
        ///  invoice type
        /// </summary>
        public DonationReceiptsType DonationReceiptsType { get; set; }

        /// <summary>
        /// Discount code
        /// </summary>
        public string DiscountCode { get; set; }
        /// <summary>
        /// 檔次是否適用折價券
        /// </summary>
        public bool IsDealUseDiscount { get; set; }

        public IEnumerable<DataOrm.ViewDiscountDetail> DiscountList { get; set; }

        #endregion properties
    }

    /// <summary>
    /// 小型購物車內部商店
    /// </summary>
    [Serializable]
    public class SmallCart
    {
        public SmallCart()
        {
            PaymentDTOList = new List<CartPaymentDTO>();
        }
        #region properties
        /// <summary>
        /// 賣家Guid
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public int FreightsId { get; set; }
        /// <summary>
        /// 運費描述：滿$1000免收運費$80
        /// </summary>
        public string FreightsDescription { get; set; }
        /// <summary>
        /// 商品
        /// </summary>
        public List<CartPaymentDTO> PaymentDTOList { get; set; }
        /// <summary>
        /// 應付運費
        /// </summary>
        public int Freights { get; set; }
        /// <summary>
        /// 實際運費
        /// </summary>
        public int ActualFreights { get; set; }
        /// <summary>
        /// 子購物車金額
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 是否勾選
        /// </summary>
        public bool IsChecked { get; set; }
        /// <summary>
        /// 滿額
        /// </summary>
        public int NoFreightLimit { get; set; }
        #endregion properties
    }

    public class CartItemDTO
    {
        public Guid BusinessHourGuid { get; set; }
        /// <summary>
        /// Item Quantity
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// StoreGuid
        /// </summary>
        public Guid StoreGuid { get; set; }
        /// <summary>
        /// item options, specified format is
        /// "OptionName|^|OptionGuid,OptionName|^|OptionGuid|#|ItemQuantity||OptionName|^|OptionGuid|#|ItemQuantity", just like
        /// "L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|2||L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|3"
        /// </summary>
        public List<CartItemOptionList> ItemOptions { get; set; }
        public string TicketId { get; set; }
    }

    public class CartItemOptionList
    {
        public List<CartItemOption> ItemList { get; set; }
        public int ItemQuantity { get; set; }
    }

    public class CartItemOption
    {
        public string OptionName { get; set; }
        public string OptionGuid { get; set; }
    }

    public class CartItemKey
    {
        public string TicketId { get; set; }
        public Guid BusinessHourGuid { get; set; }
    }


    [Serializable]
    public class CartPaymentDTO
    {
        #region properties

        /// <summary>
        /// Ticket ID
        /// </summary>
        public string TicketId { get; set; }

        ///// <summary>
        ///// Transsaction ID
        ///// </summary>
        //public string TransactionId { get; set; }

        /// <summary>
        /// order guid
        /// </summary>
        public Guid OrderGuid { get; set; }

        /// <summary>
        /// seller guid
        /// </summary>
        public Guid SellerGuid { get; set; }

        /// <summary>
        /// Session ID
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// payeasy member id
        /// </summary>
        public string PezId { get; set; }

        public int PponDeliveryType { get; set; }

        /// <summary>
        /// bussiness hour guid
        /// </summary>
        public Guid BusinessHourGuid { get; set; }
        public string DealName { get; set; }
        public string EventName { get; set; }
        public string DealPic { get; set; }

        public CartDealStatus DealStatus { get; set; }

        public IEnumerable<DataOrm.ViewDiscountDetail> DiscountList { get; set; }

        /// <summary>
        /// delivery charge
        /// </summary>
        public int DeliveryCharge { get; set; }

        /// <summary>
        /// Item Quantity
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Item Price
        /// </summary>
        public int ItemPrice { get; set; }

        /// <summary>
        /// order total amount
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// freights
        /// </summary>
        public int Freights { get; set; }
        public int FreightsId { get; set; }

        /// <summary>
        /// bonus cash
        /// </summary>
        public int BCash { get; set; }

        /// <summary>
        /// 17Life cash
        /// </summary>
        public int SCash { get; set; }

        /// <summary>
        /// payeasy cash
        /// </summary>
        public decimal PCash { get; set; }

        /// <summary>
        /// Discount code
        /// </summary>
        public string DiscountCode { get; set; }
        
        /// <summary>
        /// Discount Amount
        /// </summary>
        public decimal DiscountAmount { get; set; }

        /// <summary>
        /// 檔次是否適用折價券
        /// </summary>
        public bool IsDealUseDiscount { get; set; }
        /// <summary>
        /// 檔次是否有適用的折價券
        /// </summary>
        public bool IsDealHasDiscount { get; set; }

        /// <summary>
        /// branch store guid
        /// </summary>
        public Guid SelectedStoreGuid { get; set; }

        /// <summary>
        /// item options, specified format is
        /// "OptionName|^|OptionGuid,OptionName|^|OptionGuid|#|ItemQuantity||OptionName|^|OptionGuid|#|ItemQuantity", just like
        /// "L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|2||L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|3"
        /// </summary>
        public string ItemOptions { get; set; }

        /// <summary>
        ///  invoice type
        /// </summary>
        public DonationReceiptsType DonationReceiptsType { get; set; }

        /// <summary>
        /// payment api provider
        /// </summary>
        public PaymentAPIProvider APIProvider { get; set; }

        /// <summary>
        /// Is item
        /// </summary>
        public bool IsItem { get; set; }

        /// <summary>
        /// Is pay by ATM
        /// </summary>
        public bool IsPaidByATM { get; set; }

        /// <summary>
        /// Is use shoppingcart
        /// </summary>
        public bool IsShoppingCart { get; set; }

        /// <summary>
        /// Hami Ticket Id
        /// </summary>
        public string IsHami { get; set; }



        /// <summary>
        /// ATM account
        /// </summary>
        public string ATMAccount { get; set; }

        /// <summary>
        /// result page type
        /// </summary>
        public PaymentResultPageType ResultPageType { get; set; }

        /// <summary>
        /// buyer and receiver address info, the specified format is:
        /// "BuyerLastName|BuyerFirstName|BuyerMobile|BuyerCityId|BuyerAreaId|BuyerBuildingGuid|BuyerAddress^ReceiverName|ReceiverMobile|ReceiverCityId|ReceiverAreaId|ReceiverBuildingId|ReceiverAddress|IsAddReceiverInfo"
        /// just like "馬|魚|0917171717|294|317|8c3b0067-f777-4461-9d76-d51fae14c009|44號^馬魚444|0917173213|294|317|04803868-3196-45d0-975c-74a0d2e113a6|44號|False".
        /// </summary>
        //public string AddressInfo { get; set; }

        /// <summary>
        /// 推薦送紅利活動的推薦人資訊-原WEB活動中紀錄於cookie.referenceId中資訊
        /// </summary>
        public string ReferenceId { get; set; }

        /// <summary>
        /// 使用 MasterPass
        /// </summary>
        //public bool IsMasterPass { set; get; }
        /// <summary>
        /// MasterPass Data
        /// </summary>
       // public MasterPassTrans MasterPassTransData { set; get; }

        /// <summary>
        /// 是否為成套票券
        /// </summary>
        public bool isGroupCoupon { get; set; }
        /// <summary>
        /// 最大購買數量
        /// </summary>
        public int MaxItemCount { get; set; }

        #endregion properties
    }

    public class CartDeal
    {
        public Guid BusinessHourGuid { get; set; }
    }

    public class DealDetail
    {
        /// <summary>
        /// 好康組合
        /// </summary>
        public List<ItemEntry> ItemEntryList { get; set; }
        /// <summary>
        /// 詳細介紹
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 商品規格
        /// </summary>
        public string Spec { get; set; }
        /// <summary>
        /// 權益說明
        /// </summary>
        public string Restrictions { get; set; }
        /// <summary>
        /// IconTags
        /// </summary>
        public string IconTags { get; set; }
        /// <summary>
        /// 單檔次
        /// </summary>
        public DealBase Deal { get; set; }
        /// <summary>
        /// 多檔次
        /// </summary>
        public Dictionary<string, string> ComboDeals { get; set; }
        /// <summary>
        /// 多分店
        /// </summary>
        public List<CartPponSotres> PponStores { get; set; }
        public List<CartMultiOptions> MultiOptions { get; set; }

        /// <summary>
        /// 是否為品生活檔次
        /// </summary>
        public bool isPiinLife { get; set; }
        /// <summary>
        /// 品生活專用頁籤
        /// </summary>
        public Dictionary<string, string> PiinLifeTabList { get; set; }
        /// <summary>
        /// 檔次金額
        /// </summary>
        public decimal ItemPrice { get; set; }
        /// <summary>
        /// 是否為成套票券
        /// </summary>
        public bool isGroupCoupon { get; set; }
        /// <summary>
        /// 最大購買數量
        /// </summary>
        public int MaxItemCount { get; set; }
    }

    public class DealBase
    {
        public Guid BusinessHourGuid { get; set; }
        public string DealName { get; set; }
    }

    public class CartPponSotres
    {
        public Guid StoreGuid { get; set; }
        public string StoreDescription { get; set; }
        public bool IsDisabled { get; set; }
    }

    public class CartMultiOptions
    {
        public int ItemAccessoryGroupListSequence { get; set; }
        public string ItemAccessoryGroupListName { get; set; }
        public List<CartAccessoryGroupMember> GroupMembers { get; set; }
    }
    public class CartAccessoryGroupMember
    {
        public int AccessoryGroupMemberSequence { get; set; }
        public Guid AccessoryGroupMemberGUID { get; set; }
        public string AccessoryName { get; set; }
        public int? Quantity { get; set; }
    }

    public class SimpleDiscount
    {
        public decimal Amount { get; set; }
        public int MinimumAmount { get; set; }
        public string EndTime { get; set; }
        public string DiscountCode { get; set; }
        /// <summary>
        /// 使用狀態 canuse:未使用/used:無法使用/on:使用中
        /// </summary>
        public string DiscountState { get; set; }
    }

    public class paramAddOn
    {
        public string Sid { get; set; }
        public string Fid { get; set; }
    }
    public class paramDeleteOrder
    {
        public string Tid { get; set; }
        public string Oid { get; set; }
    }
    public class paramCartTicketId
    {
        public string TicketId { get; set; }
    }
    public class paramCartBid
    {
        public string BusinessHourGuid { get; set; }
    }
}

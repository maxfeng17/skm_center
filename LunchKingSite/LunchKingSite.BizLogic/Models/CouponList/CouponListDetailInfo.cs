﻿using System;

namespace LunchKingSite.BizLogic.Model.CouponList
{
    [Serializable]
    public class CouponListDetailInfo
    {
        /// <summary>
        /// 憑證狀態
        /// </summary>
        public string Status { set; get; }

        /// <summary>
        /// 憑證狀態別
        /// </summary>
        public int StatusType { set; get; }

        /// <summary>
        /// 是否可下載憑證
        /// </summary>
        public bool IsEnabledDownLoad { set; get; }

        /// <summary>
        /// 是否可列印憑證
        /// </summary>
        public bool IsEnabledPrint { set; get; }

        /// <summary>
        /// 是否可Email憑證
        /// </summary>
        public bool IsEnabledEmail { set; get; }

        /// <summary>
        /// 是否可寄簡訊憑證
        /// </summary>
        public bool IsEnabledSMS { set; get; }

    }
}

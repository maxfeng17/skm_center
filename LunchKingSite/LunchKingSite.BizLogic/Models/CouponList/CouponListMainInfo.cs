﻿using System;

namespace LunchKingSite.BizLogic.Model.CouponList
{
    [Serializable]
    public class CouponListMainInfo
    {
        /// <summary>
        /// 訂單狀態別
        /// </summary>
        public int MainOrderStatusType { set; get; }

        /// <summary>
        /// 訂單狀態
        /// </summary>
        public string MainOrderStatus { set; get; }

        /// <summary>
        /// 憑證狀態
        /// </summary>
        public string MainCouponStatus { set; get; }

        /// <summary>
        /// 是否可下載憑證
        /// </summary>
        public bool IsDownloadCouponPdf { set; get; }

        /// <summary>
        /// ATM是否未付款
        /// </summary>
        public bool IsPayAtm { set; get; }
        
        /// <summary>
        /// 是否是好康
        /// </summary>
        public bool IsPpon { set; get; }

        /// <summary>
        /// 是否是0元好康
        /// </summary>
        public bool IsZeroPponDeal { set; get; }

        /// <summary>
        /// 是否顯示訂單狀態
        /// </summary>
        public bool IsShowOrderStatus { set; get; }

        /// <summary>
        /// 是否顯示ATM續繼付款Btn
        /// </summary>
        public bool IsShowPayAtm { set; get; }

        /// <summary>
        /// 是否顯示訂位Btn
        /// </summary>
        public bool IsShowBookingBtn { set; get; }

        /// <summary>
        /// 是否顯示訂位Btn2
        /// </summary>
        public bool IsShowBookingBtn2 { set; get; }
        
        public bool IsSetBooking { set; get; }

        public bool IsEnableBooking { set; get; }
    }
}

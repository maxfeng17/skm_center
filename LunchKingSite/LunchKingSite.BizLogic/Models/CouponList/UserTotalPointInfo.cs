﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.CouponList
{
    [Serializable]
    public class UserTotalPointInfo
    {
        /// <summary>
        /// 17Life購物金
        /// </summary>
        public decimal TotalCashPoint { set; get; }
        /// <summary>
        /// 17Life紅利
        /// </summary>
        public double TotalBonusPoint { set; get; }
        /// <summary>
        /// PayEasy
        /// </summary>
        public decimal TotalPayeasyCashPoint { set; get; }

        public UserTotalPointInfo()
        {
            TotalCashPoint = 0;
            TotalBonusPoint = 0;
            TotalPayeasyCashPoint = 0;
        }
    }
}

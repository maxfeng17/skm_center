﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Constant;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{
    public class ActivityEntry
    {
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public Guid? Id { get; set; }

        public ActivityEntry(string desc, decimal amt)
        {
            Description = desc;
            Amount = amt;
        }

        public ActivityEntry(Guid id, string desc, decimal amt)
        {
            Id = id;
            Description = desc;
            Amount = amt;
        }
    }

    public static class ActivityEntryExtensionMethod
    {
        public static OrderDetailCollection ToOrderDetailCollection(this List<ActivityEntry> activities, Guid orderId)
        {
            if (activities == null || activities.Count == 0)
            {
                return null;
            }

            OrderDetailCollection odcol = new OrderDetailCollection();
            DateTime timestamp = DateTime.Now;
            foreach (ActivityEntry entry in activities)
            {
                OrderDetail od = new OrderDetail();
                od.ConsumerName = null;
                od.ConsumerTeleExt = null;
                od.CreateId = "sys";
                od.CreateTime = timestamp;
                od.Guid = Helper.GetNewGuid(typeof(OrderDetail));
                od.ItemGuid = entry.Id ?? GlobalProperty.SystemItemGuid;
                od.ItemName = entry.Description;
                od.ItemQuantity = 1;
                od.ItemUnitPrice = entry.Amount;
                od.OrderGuid = orderId;
                od.Status = (int)OrderDetailStatus.SystemEntry;

                odcol.Add(od);
                timestamp = timestamp.AddSeconds(1);
            }

            return odcol;
        }

        public static List<ActivityEntry> ToActivities(this OrderDetailCollection odcol)
        {
            if (odcol == null || odcol.Count == 0)
            {
                return null;
            }

            List<ActivityEntry> ret =
                (from i in odcol where (i.Status & (int)OrderDetailStatus.SystemEntry) > 0 select new ActivityEntry(i.ItemGuid ?? GlobalProperty.SystemItemGuid, i.ItemName, i.ItemQuantity * i.ItemUnitPrice)).ToList();

            return ret.Count > 0 ? ret : null;
        }

        public static List<ActivityEntry> FromOrder(Guid orderGuid)
        {
            IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            return (op.OrderDetailGetList(1, -1, OrderDetail.Columns.CreateTime, orderGuid, OrderDetailTypes.System)).ToActivities();
        }
    }
}

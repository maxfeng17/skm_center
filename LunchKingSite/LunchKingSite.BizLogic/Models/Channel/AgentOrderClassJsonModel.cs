﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Channel
{
    public class AgentOrderClassJsonModel
    {
        public List<string> ExceptionName { get; set; }
        public Dictionary<int, List<string>> AllowName { get; set; }
    }
}

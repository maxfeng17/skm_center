﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Channel
{
    /// <summary>
    /// 宅配商品規格
    /// </summary>
    public class ItemOptionsModel
    {
        /// <summary>
        /// 商品規格
        /// </summary>
        public List<ItemCategory> ItemSpec { get; set; }
        /// <summary>
        /// 數量
        /// </summary>
        public int Qty { get; set; }
    }

    /// <summary>
    /// 宅配規格分類
    /// </summary>
    public class ItemCategory
    {
        /// <summary>
        /// Option Guid
        /// </summary>
        public Guid OptionGuid { get; set; }
        /// <summary>
        /// Option Name
        /// </summary>
        public string OptionName { get; set; }
    }
    /// <summary>
    /// 宅配資訊
    /// </summary>
    public class ChannelDeliveryInfo
    {
        /// <summary>
        /// 收件地址
        /// </summary>
        public string WriteAddress { get; set; }
        /// <summary>
        /// 收件地址郵遞區號
        /// </summary>
        public string WriteZipCode { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public decimal ShippingFee { get; set; }
        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string WriteName { get; set; }
        /// <summary>
        /// 手機號碼
        /// </summary>
        public string Phone { get; set; }
    }
}

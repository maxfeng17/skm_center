﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.Channel
{
    public class AgentGustBuyModel
    {
        /// <summary>
        /// email
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 宅配寄送資料
        /// </summary>
        public ChannelDeliveryInfo DeliveryInfo { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;

namespace LunchKingSite.BizLogic.Models.Channel
{
    public class EntrepotVerifyModel
    {
        [JsonProperty("coupon_id")]
        public int CouponId { get; set; }
        [JsonProperty("sequence_number")]
        public string SequenceNumber { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("voucher_status")]
        public int VoucherStatus { get; set; }
        [JsonProperty("verified_store_guid")]
        public string VerifiedStoreGuid { get; set; }
        [JsonProperty("verified_store_id")]
        public string VerifiedStoreId { get; set; }
    }

    public class EntrepotCustomerServiceReplyModel
    {
        /// <summary>
        /// 客服單號
        /// </summary>
        [JsonProperty("service_no")]
        public string ServiceNo { get; set; }
        /// <summary>
        /// 內文
        /// </summary>
        [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 圖片路徑
        /// </summary>
        [JsonProperty("image_url")]
        public string ImageUrl { get; set; }
        /// <summary>
        /// 客服
        /// </summary>
        [JsonProperty("create_id")]
        public string CreateId { get; set; }
        /// <summary>
        /// 回覆時間
        [JsonProperty("reply_time")]
        public DateTime? ReplyTime { get; set; }
    }

    public class EntrepotCustomerServiceStatusModel
    {
        /// <summary>
        /// 客服單號
        /// </summary>
        [JsonProperty("service_no")]
        public string ServiceNo { get; set; }
        /// <summary>
        /// 主因
        [JsonProperty("sub_category")]
        public int? SubCategory { get; set; }
        /// <summary>
        /// 是否結案
        [JsonProperty("status")]
        public int Status { get; set; }        
        /// <summary>
        /// 客服
        /// </summary>
        [JsonProperty("create_id")]
        public string CreateId { get; set; }
    }
}

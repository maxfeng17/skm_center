﻿using System;

namespace LunchKingSite.BizLogic.Models.Channel
{
    public class TaishinMallBanner
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid Id { get; set; }    
    }
    public class TaishinMallBannerDDL
    {
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }        
        public string DisplayText { get; set; }
        public Guid Id { get; set; }
    }
}

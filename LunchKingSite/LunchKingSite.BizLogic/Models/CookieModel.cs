﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model
{
    [Serializable]
    public class CookieModel
    {
        /// <summary>
        /// Cookie 值
        /// </summary>
        public string Value { set; get; }
        /// <summary>
        /// 是否保留 cookie 在到期日之前
        /// </summary>
        public bool IsKeepCookie { set; get; }
        /// <summary>
        /// 到期日，若 IsKeepCookie = false 則忽略
        /// </summary>
        public DateTime? Expires { set; get; }
    }
}

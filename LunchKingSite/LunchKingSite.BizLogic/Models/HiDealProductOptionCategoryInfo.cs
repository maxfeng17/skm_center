﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{
    public class HiDealProductOptionCategoryInfo
    {
        public HiDealProductOptionCategory Category { get; set; }
        public HiDealProductOptionItemCollection OptionItems { get; set; }
    }
}

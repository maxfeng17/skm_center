﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Model
{
    public class ApplePayData
    {
        public string ApplicationPrimaryAccountNumber { get; set; }
        public string ApplicationExpirationDate { get; set; }
        public string CurrencyCode { get; set; }
        public decimal TransactionAmount { get; set; }
        public string CardholderName { get; set; }
        public string DeviceManufacturerIdentifier { get; set; }
        public string PaymentDataType { get; set; }
        public ApplePayDataDetail PaymentData { get; set; }
    }

    public class ApplePayDataDetail
    {
        public string OnlinePaymentCryptogram { get; set; }
        public string EciIndicator { get; set; }
        /// <summary>
        /// for emv
        /// </summary>
        public string EmvData { get; set; }
        /// <summary>
        /// for emv
        /// </summary>
        public string EncryptedPINData { get; set; }
    }
}

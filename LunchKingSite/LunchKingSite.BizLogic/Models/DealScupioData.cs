﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models
{
    public class DealScupioData : TrackTagBase
    {
        public DealScupioData() { }
        IMemberProvider mp = Core.Component.ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public DealScupioData(TrackTagInputModel tt)
        {
            EventCategory = tt.ScupioEventCategory;

            var categories = new List<string>();
            foreach (var categoryID in tt.ViewPponDealData.CategoryIds)
            {
                categories.Add(categoryID.ToString());
            }
                

            if (tt.ScupioEventCategory == ScupioEventCategory.PageView && tt.ScupioPageType == ScupioPageType.ProductPage)
            {
                scupioTrack = new ScupioTrack
                {
                    PageType = Helper.GetDescription(tt.ScupioPageType),
                    Contents = new List<ScupioTrackContent>
                    {
                        new ScupioTrackContent
                        {
                            ItemId = tt.ViewPponDealData.BusinessHourGuid.ToString(),
                            Categories = categories,
                        }
                    }
                };
            }
            else if (tt.ScupioEventCategory == ScupioEventCategory.PageView && tt.ScupioPageType == ScupioPageType.CheckoutPage)
            {
                Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(tt.ViewPponDealData.BusinessHourGuid);
                scupioTrack = new ScupioTrack
                {
                    PageType = Helper.GetDescription(tt.ScupioPageType),
                    Contents = new List<ScupioTrackContent>
                    {
                        new ScupioTrackContent
                        {
                            ItemId = mainBid.ToString(),
                            Categories = categories,
                            Price = (int)tt.ViewPponDealData.ItemPrice,
                        }
                    }
                };
            }
            else if (tt.ScupioEventCategory == ScupioEventCategory.PageView && tt.ScupioPageType == ScupioPageType.AddToCart)
            {
                Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(tt.ViewPponDealData.BusinessHourGuid);
                scupioTrack = new ScupioTrack
                {
                    PageType = Helper.GetDescription(tt.ScupioPageType),
                    Contents = new List<ScupioTrackContent>
                    {
                        new ScupioTrackContent
                        {
                            ItemId = mainBid.ToString(),
                            Categories = categories,
                            Quantity = 1,
                            Price = (int)tt.ViewPponDealData.ItemPrice,
                        }
                    }
                };
            }
            else if(tt.ScupioEventCategory == ScupioEventCategory.Purchase)
            {
                Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(tt.ViewPponDealData.BusinessHourGuid);

                List<ScupioTrackContent> contents = new List<ScupioTrackContent>();
                foreach(var orderDetail in tt.OrderDetails)
                {
                    ScupioTrackContent stc = new ScupioTrackContent()
                    {
                        ItemId = mainBid.ToString(),
                        Categories = categories,
                        Quantity = orderDetail.ItemQuantity,
                        Price = Convert.ToInt32(orderDetail.ItemUnitPrice),

                    };
                    contents.Add(stc);
                }

                scupioTrack = new ScupioTrack
                {
                    Currency = "TWD",
                    Value = tt.OrderTotalAmountWithoutDiscount,
                    TransactionId = tt.OrderGuid.ToString(),
                    Contents = contents,
                };
            }
        }

        public override string GetJson()
        {
            if (!string.IsNullOrEmpty(scupioTrack.PageType) && scupioTrack.PageType == Helper.GetDescription(ScupioPageType.AddToCart))
            {
                const string temp = "_bw('track', 'AddToCart', { \"contents\": |data|});";
                return temp.Replace("|data|", JsonConvert.SerializeObject(scupioTrack.Contents));
            }

            var data = string.Format("_bw('track', '{0}', {1});",
               Helper.GetDescription(EventCategory), JsonConvert.SerializeObject(scupioTrack));
            return data;
        }

        /// <summary>
        /// 事件類別
        /// </summary>
        public ScupioEventCategory EventCategory { get; set; }

        /// <summary>
        /// 追蹤碼內容
        /// </summary>
        public ScupioTrack scupioTrack { get; set; }
    }
    
    public class ScupioTrack
    {
        [JsonProperty("page_type", NullValueHandling = NullValueHandling.Ignore)]
        public string PageType { get; set; }

        [JsonProperty("categories", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Categories { get; set; }

        [JsonProperty("currency", NullValueHandling = NullValueHandling.Ignore)]
        public string Currency { get; set; }

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public long? Value { get; set; }

        [JsonProperty("transaction_id", NullValueHandling = NullValueHandling.Ignore)]
        public string TransactionId { get; set; }

        [JsonProperty("contents", NullValueHandling = NullValueHandling.Ignore)]
        public List<ScupioTrackContent> Contents { get; set; }

        [JsonProperty("search_string", NullValueHandling = NullValueHandling.Ignore)]
        public string SearchString { get; set; }
    }

    public class ScupioTrackContent
    {
        [JsonProperty("item_id", NullValueHandling = NullValueHandling.Ignore)]
        public string ItemId { get; set; }

        [JsonProperty("categories", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Categories { get; set; }

        [JsonProperty("quantity", NullValueHandling = NullValueHandling.Ignore)]
        public int? Quantity { get; set; }

        [JsonProperty("price", NullValueHandling = NullValueHandling.Ignore)]
        public int? Price { get; set; }
    }
}


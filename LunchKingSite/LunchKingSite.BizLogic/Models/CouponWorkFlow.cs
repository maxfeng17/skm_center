﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model
{
    /// <summary>
    /// 產生憑證work flow，由於改為web service傳遞參數啟動，減少傳輸量，重組ViewPponDeal
    /// </summary>
    [Serializable]
    public class CouponWorkFlow
    {
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 是否已達門檻
        /// </summary>
        public bool IsDealOn { get; set; }
        public bool IsAtm { get; set; }
        public string IsHami { get; set; }
        public Guid BusinessHourGuid { get; set; }
        public DateTime BusinessHourDeliverTimeS { get; set; }
        public DateTime BusinessHourDeliverTimeE { get; set; }
        public GroupOrder GroupOrder { get; set; }
        public bool IsToShop { get; set; }
        /// <summary>
        /// ViewPponCoupon重組為CouponDetailWorkFlow
        /// </summary>
        public List<CouponDetailWorkFlow> PponCoupon { get; set; }
        public CouponWorkFlow(Guid order_guid, bool is_atm, string is_hami, GroupOrder group_order, ViewPponDeal pponDeal, ViewPponCouponCollection view_ppon_coupon_list)
        {
            OrderGuid = order_guid;
            IsDealOn = (pponDeal.OrderedQuantity >= pponDeal.BusinessHourOrderMinimum);
            IsAtm = is_atm;
            IsHami = is_hami;
            BusinessHourGuid = pponDeal.BusinessHourGuid;
            BusinessHourDeliverTimeS = pponDeal.BusinessHourDeliverTimeS.Value;
            BusinessHourDeliverTimeE = pponDeal.BusinessHourDeliverTimeE.Value;
            GroupOrder = group_order;
            PponCoupon = view_ppon_coupon_list.Select(x => new CouponDetailWorkFlow(x)).ToList();
            IsToShop = !pponDeal.DeliveryType.HasValue || pponDeal.DeliveryType.Value == (int)DeliveryType.ToShop;
        }
    }
    /// <summary>
    /// 重組CouponDetailWorkFlow為ViewPponCoupon
    /// </summary>
    [Serializable]
    public class CouponDetailWorkFlow
    {
        public int OrderDetailStatus { get; set; }
        public Guid? ParentOrderId { get; set; }
        public int OrderStatus { get; set; }
        public string MemberEmail { get; set; }
        public Guid? OrderDetailId { get; set; }
        public int ItemQuantity { get; set; }
        public int BusinessHourStatus { get; set; }
        public Guid? StoreGuid { get; set; }
        public CouponDetailWorkFlow(ViewPponCoupon ppon_coupon)
        {
            OrderDetailStatus = ppon_coupon.OrderDetailStatus;
            ParentOrderId = ppon_coupon.ParentOrderId;
            OrderStatus = ppon_coupon.OrderStatus;
            MemberEmail = ppon_coupon.MemberEmail;
            OrderDetailId = ppon_coupon.OrderDetailId;
            ItemQuantity = ppon_coupon.ItemQuantity;
            BusinessHourStatus = ppon_coupon.BusinessHourStatus;
            StoreGuid = ppon_coupon.StoreGuid;
        }
    }
    /// <summary>
    /// 品生活用web service產生序號，減少傳輸量，篩選必要欄位
    /// </summary>
    [Serializable]
    public class PiinLifeCouponWorkFlow
    {
        public int PK { get; set; }
        public Guid OrderGuid { get; set; }
        public Guid? StoreGuid { get; set; }
        public int ProductId { get; set; }
        public int ItemQuantity { get; set; }
        public int HiDealId { get; set; }
        public Guid OrderDetailGuid { get; set; }
        public string MemberUserName { get; set; }
        public PiinLifeCouponWorkFlow(HiDealOrder order, HiDealOrderDetail orderDetail, string memberUserName)
        {
            PK = order.Pk;
            OrderGuid = order.Guid;
            StoreGuid = orderDetail.StoreGuid;
            ProductId = orderDetail.ProductId;
            ItemQuantity = orderDetail.ItemQuantity;
            HiDealId = orderDetail.HiDealId;
            OrderDetailGuid = orderDetail.Guid;
            MemberUserName = memberUserName;
        }
    }
}

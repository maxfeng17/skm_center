﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Model
{
    /// <summary>
    /// 待用憑證Model
    /// </summary>
    public class CouponsWebModel : BaseCouponsModel
    {
        /// <summary>
        /// 訂單編號是否連結
        /// </summary>
        public bool IsOrderSerialLink { get; set; }

        /// <summary>
        /// 是否顯示詢問按鈕
        /// </summary>
        public bool IsShowServiceBtn { get; set; }

        /// <summary>
        /// 是否顯示訂位/房(查詢)按鈕
        /// </summary>
        public bool IsShowBookingBtn { get; set; }

        /// <summary>
        /// 是否為訂位/房按鈕
        /// </summary>
        public bool IsBookingBtn { get; set; }

        /// <summary>
        /// 是否顯示索取憑證按鈕
        /// </summary>
        public bool IsShowCouponDetailBtn { get; set; }

        /// <summary>
        /// 是否顯示ATM繼續付款按鈕
        /// </summary>
        public bool IsShowAtmPaycontinueBtn { get; set; }
        
        /// <summary>
        /// 是否顯示評價按鈕
        /// </summary>
        public bool IsShowCouponEaluateBtn { get; set; }

        /// <summary>
        /// 是否Enable評價按鈕
        /// </summary>
        public bool IsEnableCouponEaluateBtn { get; set; }
        
        /// <summary>
        /// 是否為全家優惠
        /// </summary>
        public bool IsFamiDeal { get; set; }

        /// <summary>
        /// 好康價Html
        /// </summary>
        public string PriceHtml { get; set; }

        /// <summary>
        /// 最後兌換日Html
        /// </summary>
        public string FinalExpDateHtml { get; set; }
        
        /// <summary>
        /// 憑證兌換事件Html
        /// </summary>
        public string CouponDetailEventHtml { get; set; }

        /// <summary>
        /// 訂位/房按鈕事件Html
        /// </summary>
        public string BookingBtnEventHtml { get; set; }

        /// <summary>
        /// 好康名稱連結
        /// </summary>
        public string OrderNameUrl { get; set; }

        /// <summary>
        /// 訂位/房按鈕文字
        /// </summary>
        public string BookingBtnText { get; set; }
    }

    /// <summary>
    /// 待用憑證Model Base
    /// </summary>
    public class BaseCouponsModel
    {
        /// <summary>
        /// 訂購日期
        /// </summary>
        public string OrderDate { get; set; }

        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderSerial { get; set; }

        /// <summary>
        /// 好康名稱
        /// </summary>
        public string OrderName { get; set; }

        /// <summary>
        /// 數量
        /// </summary>
        public string OrderAmount { get; set; }

        /// <summary>
        /// 兌換期限
        /// </summary>
        public string OrderExp { get; set; }

        /// <summary>
        /// 訂單Guid
        /// </summary>
        public string OrderGuid { get; set; }

        /// <summary>
        /// 未填評價數
        /// </summary>
        public int EvaluateCount { get; set; }

        /// <summary>
        /// 憑證狀態
        /// </summary>
        public string CouponStatus { get; set; }

        /// <summary>
        /// 訂單狀態
        /// </summary>
        public string OrderStatus { get; set; }
    }
}

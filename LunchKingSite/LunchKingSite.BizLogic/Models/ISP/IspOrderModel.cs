﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.ISP
{
    /// <summary>
    /// QueryOrder Result
    /// </summary>
    public class QueryOrderResultItem
    {
        /// <summary>
        /// 店家編號
        /// </summary>
        [JsonProperty("storeKey")]
        public string StoreKey { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        [JsonProperty("orderKey")]
        public string OrderKey { get; set; }
        /// <summary>
        /// 通路商
        /// </summary>
        [JsonProperty("serviceChannel")]
        public int ServiceChannel { get; set; }
        /// <summary>
        /// 通路商
        /// </summary>
        [JsonProperty("serviceChannelDesc")]
        public string ServiceChannelDesc { get; set; }
        /// <summary>
        /// 通路商店鋪號
        /// </summary>
        [JsonProperty("ChannelStoreCode")]
        public string ChannelStoreCode { get; set; }
        /// <summary>
        /// 寄件者
        /// </summary>
        [JsonProperty("senderName")]
        public string SenderName { get; set; }
        /// <summary>
        /// 收件者
        /// </summary>
        [JsonProperty("receiverName")]
        public string ReceiverName { get; set; }
        /// <summary>
        /// 收件者手機號碼
        /// </summary>
        [JsonProperty("receiverMPhone")]
        public string ReceiverMPhone { get; set; }
        /// <summary>
        /// 是否現金付款
        /// </summary>
        [JsonProperty("isCashOnPickup")]
        public bool IsCashOnPickup { get; set; }
        /// <summary>
        /// 付款金額
        /// </summary>
        [JsonProperty("pickupAmount")]
        public int PickupAmount { get; set; }
        /// <summary>
        /// 貨物價值
        /// </summary>
        [JsonProperty("productAmount")]
        public int ProductAmount { get; set; }
        /// <summary>
        /// 貨態列舉
        /// </summary>
        [JsonProperty("deliveryStatus")]
        public byte DeliveryStatus { get; set; }
        /// <summary>
        /// 貨態列舉
        /// </summary>
        [JsonProperty("deliveryStatusDesc")]
        public string DeliveryStatusDesc { get; set; }
        /// <summary>
        /// Dc驗收狀態列舉
        /// </summary>
        [JsonProperty("dcAcceptStatus")]
        public byte DcAcceptStatus { get; set; }
        /// <summary>
        /// 貨態
        /// </summary>
        [JsonProperty("orderHistory")]
        public List<OrderHistoryModel> OrderHistory { get; set; }
    }

    public class OrderHistoryModel
    {
        /// <summary>
        /// 流水號
        /// </summary>
        [JsonProperty("serialNo")]
        public int SerialNo { get; set; }
        /// <summary>
        /// 發生時間
        /// </summary>
        [JsonProperty("deliveryTime")]
        public DateTime DeliveryTime { get; set; }
        /// <summary>
        /// 狀態
        /// </summary>
        [JsonProperty("delvieryStatus")]
        public byte DelvieryStatus { get; set; }
        /// <summary>
        /// memo
        /// </summary>
        [JsonProperty("delvieryDesc")]
        public string DelvieryContent { get; set; }
        /// <summary>
        /// 歷程類型，請參考 ISPConfig.cs -> IspHistoryType
        /// </summary>
        [JsonProperty("historyType")]
        public byte HistoryType { get; set; }
    }
}

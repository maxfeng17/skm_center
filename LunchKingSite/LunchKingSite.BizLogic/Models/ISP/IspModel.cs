﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.ISP
{
    public class UserIspServiceModel
    {
        /// <summary>
        /// 平台使用者名稱
        /// </summary>
        [JsonProperty("user")]
        public string User { get; set; }
        /// <summary>
        /// 到店通路商
        /// </summary>
        [JsonProperty("ispChannels")]
        public List<UserChannel> IspChannels { get; set; }
    }

    public class UserChannel
    {
        /// <summary>
        /// 通路商列舉代號
        /// </summary>
        [JsonProperty("serviceChannelCode")]
        public Byte ServiceChannelCode { get; set; }
        /// <summary>
        /// 通路商名稱
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 通路主廠商代碼
        /// </summary>
        [JsonProperty("serviceType")]
        public List<ChannelSellerCode> ServiceType { get; set; }
    }

    public class ChannelSellerCode
    {
        /// <summary>
        /// 類型名稱
        /// </summary>
        [JsonProperty("typeCode")]
        public byte TypeCode { get; set; }
        /// <summary>
        /// 類型名稱
        /// </summary>
        [JsonProperty("typeName")]
        public string TypeName { get; set; }
        /// <summary>
        /// 通路母代碼
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    /// <summary>
    /// 會員通訊錄
    /// </summary>
    public class MemberContactsData
    {
        public List<Contact> Contacts { get; set; }
        public List<Group> Groups { get; set; }
    }

    /// <summary>
    /// 朋友資料
    /// </summary>
    public class Contact
    {
        public int? Id { get; set; }
        public int? UserId { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public DateTime? Birthday { get; set; }
        public int? GroupId { get; set; }
        public string GroupName { get; set; }

        //是否接受過禮物，曾經按下接受或拒絕皆為True
        //判斷:data=select mgm_gift_version where senderId='自己'
        //data=>x.recevierId==Contact.UserID and x.accept !=0
        public bool IsAcceptGift { get; set; } 
    }

    /// <summary>
    /// 群組
    /// </summary>
    public class Group
    {
        public int? GroupId { get; set; }
        public string GroupName { get; set; }
        public string GroupImagePath { get; set; }
        public int GroupNumber { get; set; }
    }
}

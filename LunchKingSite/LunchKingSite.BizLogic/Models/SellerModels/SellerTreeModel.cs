﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LunchKingSite.BizLogic.Models.SellerModels
{
    
    public class SellerTreeModel
    {
        private static readonly ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();

        #region prop

        public Guid TriggerSellerGuid { get; set; }
        public IEnumerable<SellerTree> SellerTree { get; set; }

        #endregion prop

        public void Initial(Guid sellerGuid)
        {
            TriggerSellerGuid = sellerGuid;
            SellerTree = GetSellerTree(sellerGuid);
        }


        private IEnumerable<SellerTree> GetSellerTree(Guid sellerGuid)
        {
            var trees = new List<SellerTree>();
            var sellers = sp.SellerTreeGetListBySellerGuid(sellerGuid)
                            .GroupBy(x => x.RootSellerGuid).ToList();

            var rootGuids = new List<Guid>();
            if (sellers.Count == 0)
            {
                rootGuids.Add(sellerGuid);
            }
            else
            {
                rootGuids.AddRange(sellers.Select(t => t.Key));
            }

            foreach (var rootGuid in rootGuids)
            {
                var temp = sp.SellerTreeGetDataTableByRoot(rootGuid);

                List<SellerTreeFlattedItem> allFlattedItems = new List<SellerTreeFlattedItem>();
                foreach (DataRow item in temp.Rows)
                {
                    allFlattedItems.Add(new SellerTreeFlattedItem
                    {
                        SellerGuid = (Guid)item["seller_guid"],
                        ParentGuid = (Guid)item["parent_seller_guid"],
                        Sort = (int)item["sort"],
                        StoreStatus = (int)item["store_status"],
                        TempStatus = (int)item["temp_status"],
                        SellerId = item["seller_id"].ToString(),
                        SellerName = item["seller_name"].ToString()
                    });
                }

                var rootNode = GetRootNode(rootGuid);
                var tree = new SellerTree
                {
                    RootNode = rootNode,
                    Children = GetNodes(allFlattedItems, rootGuid, rootNode.Level)
                };
                trees.Add(tree);
            }

            return trees;
        }

        private SellerNode GetRootNode(Guid sellerGuid)
        {
            var sellerTrees = sp.SellerTreeGetListBySellerGuid(sellerGuid);
            if (!sellerTrees.Any())
            {
                var sellerInfo = sp.SellerGet(sellerGuid);

                return new SellerNode { 
                    Id = sellerGuid,
                    Text = string.Format("{0} {1}", sellerInfo.SellerId, sellerInfo.SellerName),
                    TempStatus = sellerInfo.TempStatus,
                    StoreStatus = sellerInfo.StoreStatus,
                    Level = 1
                };
            }

            return null;
        }

        private IEnumerable<SellerNode> GetNodes(List<SellerTreeFlattedItem> allFlattedItems, Guid sellerGuid, int level)
        {
            var flattedChildItems = allFlattedItems.Where(t => t.ParentGuid == sellerGuid)
                   .OrderBy(t => t.Sort)
                   .ToList();
            var nodes = new List<SellerNode>();
            if (flattedChildItems.Count > 0)
            {
                ++level;
                foreach (var item in flattedChildItems)
                {
                    nodes.AddRange(GetNodes(allFlattedItems, item.SellerGuid, level));
                    var node = new SellerNode
                    {
                        Id = item.SellerGuid,
                        Text = item.SellerId + " " + item.SellerName,
                        ParentId = item.ParentGuid,
                        TempStatus = item.TempStatus,
                        StoreStatus = item.StoreStatus,
                        Level = level
                    };
                    nodes.Add(node);
                }
            }
            return nodes;
        }

        private class SellerTreeFlattedItem
        {
            public Guid SellerGuid { get; set; }
            public Guid? ParentGuid { get; set; }
            public string SellerId { get; set; }
            public string SellerName { get; set; }
            public int Sort { get; set; }
            public int TempStatus { get; set; }
            public int StoreStatus { get; set; }
        }
    }

    [Serializable]
    public class SellerTree
    {
        public SellerNode RootNode { get; set; }
        public IEnumerable<SellerNode> Children { get; set; }
    }

    [Serializable]
    public class SellerNode
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public string Text { get; set; }
        public TreeState State{ get; set;}
        public int Sort { get; set; }
        public int TempStatus { get; set; }
        public int StoreStatus { get; set; }
        public int Level { get; set; }
    }

    public enum TreeState
    {
        Closed,
        Open
    }
}

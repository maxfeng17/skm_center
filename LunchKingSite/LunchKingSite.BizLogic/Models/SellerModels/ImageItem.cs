﻿namespace LunchKingSite.BizLogic.Model.SellerModels
{
    public class ImageItem
    {
        public string Url { get; set; }
        public string Title { get; set; }
    }
}

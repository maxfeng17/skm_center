﻿using System;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.BizLogic.Model.SellerModels
{
    public class SellerStoreModel
    {
        public SellerStoreModel()
        {

        }

        public SellerStoreModel(string sellerName, Store store)
        {
            this.StoreGuid = store.Guid;
            this.SellerName = sellerName;
            this.SellerGuid = store.SellerGuid;
            this.BranchName = store.StoreName;
            this.StoreTel = store.Phone;
            this.StoreAddress = store.AddressString;
            this.TownshipId = store.TownshipId;
            this.AddressCityId = store.CityId.GetValueOrDefault(-1);
            this.BusinessHour = store.OpenTime;
            this.CloseDateInformation = store.CloseDate;
            this.CompanyName = store.CompanyName;
            this.CompanyBossName = store.CompanyBossName;
            this.SignCompanyID = store.SignCompanyID;

            SqlGeography geography = LocationFacade.GetGeographyByCoordinate(
                store.Coordinate);
            ApiCoordinates longitudeLatitude = ApiCoordinates.FromSqlGeography(geography);
            this.Longitude = longitudeLatitude.Longitude.ToString();
            this.Latitude = longitudeLatitude.Latitude.ToString();

            this.TrafficMrt = store.Mrt;
            this.TrafficCar = store.Car;
            this.TrafficBus = store.Bus;
            this.TrafficOther = store.OtherVehicles;

            this.WebUrl = store.WebUrl;
            this.FaceBookUrl = store.FacebookUrl;
            this.PlurkUrl = store.PlurkUrl;
            this.BlogUrl = store.BlogUrl;
            this.OtherUrl = store.OtherUrl;

            this.CompanyBankCode = store.CompanyBankCode;
            this.CompanyBranchCode = store.CompanyBranchCode;
            this.CompanyAccount = store.CompanyAccount;
            this.CompanyAccountName = store.CompanyAccountName;
            this.CompanyID = store.CompanyID;
            this.AccountantName = store.AccountantName;
            this.AccountantTel = store.AccountantTel;
            this.CompanyEmail = store.CompanyEmail;
            this.CompanyNotice = store.CompanyNotice;

            this.CreditCardAvailable = store.CreditcardAvailable;
            this.IsOpenBooking = store.IsOpenBooking;
            this.IsOpenReservationSetting = store.IsOpenReservationSetting;
            this.Remark = store.Remarks;

            this.Status = (StoreStatus)store.Status;
            this.IsCloseDown = store.IsCloseDown;
            if (IsCloseDown && store.CloseDownDate.HasValue)
            {
                this.CloseDownDate = store.CloseDownDate.Value.ToString("yyyy/MM/dd");
            }
        }

        public Guid SellerGuid { get; set; }

        public Guid StoreGuid { get; set; }

        public string UserName { get; set; }

        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public StoreStatus Status { get; set; }


        #region 基本資料

        /// <summary>
        /// 分店名
        /// </summary>
        public string BranchName { get; set; }
        /// <summary>
        /// 電話
        /// </summary>
        public string StoreTel { get; set; }
        /// <summary>
        /// 地址的城市ID
        /// </summary>
        public int AddressCityId { get; set; }
        /// <summary>
        /// 地址的鄉鎮市區ID
        /// </summary>
        public int? TownshipId { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string StoreAddress { get; set; }
        /// <summary>
        /// 經緯度
        /// </summary>
        //public ApiCoordinates LongitudeLatitude { get; set; }

        public string Longitude { get; set; }
        public string Latitude { get; set; }

        /// <summary>
        /// 營業時間
        /// </summary>
        public string BusinessHour { get; set; }
        /// <summary>
        /// 公休日
        /// </summary>
        public string CloseDateInformation { get; set; }
        /// <summary>
        /// 簽約公司名稱
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// 負責人
        /// </summary>
        public string CompanyBossName { get; set; }
        /// <summary>
        /// 簽約公司ID/統一編號
        /// </summary>
        public string SignCompanyID { get; set; }

        #endregion

        #region 交通資訊

        /// <summary>
        /// 捷運
        /// </summary>
        public string TrafficMrt { get; set; }

        /// <summary>
        /// 開車
        /// </summary>
        public string TrafficCar { get; set; }

        /// <summary>
        /// 公車
        /// </summary>
        public string TrafficBus { get; set; }

        /// <summary>
        /// 其他交通方是
        /// </summary>
        public string TrafficOther { get; set; }

        #endregion

        #region 網站資訊

        /// <summary>
        /// 官網網址
        /// </summary>
        public string WebUrl { get; set; }

        /// <summary>
        /// FaceBook網址
        /// </summary>
        public string FaceBookUrl { get; set; }

        /// <summary>
        /// Plurk網址
        /// </summary>
        public string PlurkUrl { get; set; }

        /// <summary>
        /// 部落格網址
        /// </summary>
        public string BlogUrl { get; set; }

        /// <summary>
        /// tbOtherUrl
        /// </summary>
        public string OtherUrl { get; set; }

        #endregion

        #region 匯款資料

        /// <summary>
        /// 銀行代號
        /// </summary>
        public string CompanyBankCode { get; set; }
        /// <summary>
        /// 分行代號
        /// </summary>
        public string CompanyBranchCode { get; set; }
        /// <summary>
        /// 帳號
        /// </summary>
        public string CompanyAccount { get; set; }
        /// <summary>
        /// 戶名
        /// </summary>
        public string CompanyAccountName { get; set; }
        /// <summary>
        /// 帳務連絡人
        /// </summary>
        public string AccountantName { get; set; }
        /// <summary>
        /// 受款人ID/統一編號
        /// </summary>
        public string CompanyID { get; set; }
        /// <summary>
        /// 帳務連絡人電話
        /// </summary>
        public string AccountantTel { get; set; }
        /// <summary>
        /// 帳務連絡人電子信箱
        /// </summary>
        public string CompanyEmail { get; set; }
        /// <summary>
        /// 匯款備註
        /// </summary>
        public string CompanyNotice { get; set; }

        #endregion

        #region 其它

        /// <summary>
        /// 刷卡服務
        /// </summary>
        public bool CreditCardAvailable { get; set; }
        /// <summary>
        /// 預約管理
        /// </summary>
        public bool IsOpenBooking { get; set; }
        /// <summary>
        /// 分店是否顯示預約系統設定
        /// </summary>
        public bool IsOpenReservationSetting { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 結束營業狀態
        /// </summary>
        public bool IsCloseDown { get; set; }
        /// <summary>
        /// 結束營業日
        /// </summary>
        public string CloseDownDate { get; set; }
        #endregion

        public string Message { get; set; }


        public void CopyTo(Store store, bool isNew)
        {
            if (isNew == false && (store.Guid == Guid.Empty || this.StoreGuid != store.Guid))
            {
                throw new Exception("SellerStoreModel copyto fail.");
            }
            store.SellerGuid = this.SellerGuid;                
            store.StoreName = this.BranchName;
            store.Phone = this.StoreTel;
            store.AddressString = this.StoreAddress;
            store.TownshipId = this.TownshipId;
            store.CityId = this.AddressCityId;
            store.OpenTime = this.BusinessHour;
            store.CloseDate = this.CloseDateInformation;
            store.CompanyName = this.CompanyName;
            store.CompanyBossName = this.CompanyBossName;
            store.SignCompanyID = this.SignCompanyID;

            ApiCoordinates coordinate = new ApiCoordinates(double.Parse(this.Longitude), double.Parse(this.Latitude));
            store.Coordinate = coordinate.ToSqlGeography().ToString();

            store.Mrt = this.TrafficMrt;
            store.Car = this.TrafficCar;
            store.Bus = this.TrafficBus;
            store.OtherVehicles = this.TrafficOther;

            store.WebUrl = this.WebUrl;
            store.FacebookUrl = this.FaceBookUrl;
            store.PlurkUrl = this.PlurkUrl;
            store.BlogUrl = this.BlogUrl;
            store.OtherUrl = this.OtherUrl;

            store.CompanyBankCode = this.CompanyBankCode;
            store.CompanyBranchCode = this.CompanyBranchCode;
            store.CompanyAccount = this.CompanyAccount;
            store.CompanyAccountName = this.CompanyAccountName;
            store.CompanyID = this.CompanyID;
            store.AccountantName = this.AccountantName;
            store.AccountantTel = this.AccountantTel;
            store.CompanyEmail = this.CompanyEmail;
            store.CompanyNotice = this.CompanyNotice;

            store.CreditcardAvailable = this.CreditCardAvailable;
            store.IsOpenBooking = this.IsOpenBooking;
            store.IsOpenReservationSetting = this.IsOpenReservationSetting;
            store.Remarks = this.Remark;
            store.Status = (int)this.Status;
            store.IsCloseDown = this.IsCloseDown;
            DateTime dt;
            if (this.IsCloseDown && DateTime.TryParse(this.CloseDownDate, out dt))
            {
                store.CloseDownDate = dt;
            }
            else
            {
                store.CloseDownDate = null;
            }
        }
    }


}

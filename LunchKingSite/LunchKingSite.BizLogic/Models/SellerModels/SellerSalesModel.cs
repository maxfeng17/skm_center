﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.BizLogic.Model.SellerSales
{
    public class SellerSalesModel
    {
        public int SalesGroup { get; set; }
        public string SalesMail { get; set; } 
    }

    public class MultipleSalesModel
    {
        private static readonly ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();

        public int SalesGroup { get; set; }
        public int DevelopeSalesId { get; set; }
        public string DevelopeSalesEmail { get; set; }
        public string DevelopeSalesEmpName { get; set; }
        public string DevelopeSalesDeptId { get; set; }
        public string DevelopeSalesDeptName { get; set; }
        public int DevelopeSalesTeamNo { get; set; }
        public int OperationSalesId { get; set; }
        public string OperationSalesEmail { get; set; }
        public string OperationSalesEmpName { get; set; }
        public string OperationSalesDeptId { get; set; }
        public string OperationSalesDeptName { get; set; }
        public int OperationSalesTeamNo { get; set; }
    }

    public class ProposalSalesModel
    {
        private static readonly ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        public string DevelopeSalesEmail { get; set; }
        public string DevelopeSalesEmpName { get; set; }
        public string DevelopeSalesDeptName { get; set; }
        public string OperationSalesEmail { get; set; }
        public string OperationSalesEmpName { get; set; }
        public string OperationSalesDeptName { get; set; }
    }
}

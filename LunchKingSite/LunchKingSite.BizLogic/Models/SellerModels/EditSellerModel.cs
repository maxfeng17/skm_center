﻿using System;
using System.Collections.Generic;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Model.SellerModels
{
    public class EditSellerModel
    {
        public string SellerName { get; set; }
        public bool IsCloseDown { get; set; }

        public DateTime? CloseDownDate { get; set; }
    }

    public class SellerPicsModel
    {
        public IEnumerable<HttpPostedFileBase> PhotoUpload { get; set; }

        public ImageItem[] OriginalPics
        {
            get
            {
                ISerializer json = ProviderFactory.Instance().GetSerializer();
                return json.Deserialize<ImageItem[]>(OriginalPicsToJson);
            }
        }

        public string OriginalPicsToJson { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Model
{
    public class SellerMailInfo
    {
    }

    #region 通知信

    #region 核銷提醒通知信

    public class VerificationNoticeMailInformation
    {
        public string siteUrl { get; set; }
        public string sellerName { get; set; }
        public string tbody_DealList { get; set; }
    }

    #endregion 變動通知信

    #endregion 通知信
}

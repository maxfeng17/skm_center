﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model
{
    /// <summary>
    /// 好康資料簡介，只記錄好康列表功能需要的少量資訊
    /// 用來作為APP或小網頁等，較在意資料傳輸效能的應用
    /// </summary>
    public class PponDealSynopsis : PponDealSynopsisBasic
    {
        public PponDealSynopsis()
        {
            DealTags = new List<int>();
            //預設為非全家
            IsFami = false;
            //預設不顯示兌換價
            ShowExchangePrice = false;
            ExchangePrice = 0;
            Categories = new List<int>();
        }
        /// <summary>
        /// 手機版頁面當作檔次名稱顯示，移除【TravelPlace】。
        /// </summary>
        public string MiniDealName { get; set; }
        /// <summary>
        /// 手機版頁面當作檔次介紹顯示(優惠活動，黑標)。
        /// </summary>
        public string DealEventName { get; set; }
        /// <summary>
        /// 已銷售數量顯示值-已銷售數量*設定的倍數 所得到用於顯示的的數值
        /// </summary>
        public int SoldNumShow { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal OriPrice { get; set; }
        /// <summary>
        /// 旅遊地點
        /// </summary>
        public string TravelPlace { get; set; }
        /// <summary>
        /// 檔次promo圖片,用於瀑布流介面
        /// </summary>
        public string PromoImage { get; set; }
        /// <summary>
        /// 是否為全家檔次
        /// </summary>
        public bool IsFami { get; set; }
        /// <summary>
        /// 是否顯示兌換價
        /// </summary>
        public bool ShowExchangePrice { get; set; }
        /// <summary>
        /// 兌換價
        /// </summary>
        public decimal ExchangePrice { get; set; }
        /// <summary>
        /// 頁簽設定
        /// </summary>
        public List<int> ExternalDealTagType { get; set; }
        /// <summary>
        /// 櫃位
        /// </summary>
        public string Store { get; set; }
        /// <summary>
        /// 館別
        /// </summary>
        public string Department { get; set; }
        /// <summary>
        /// 優惠類型
        /// </summary>
        public int DiscountType { get; set; }
        /// <summary>
        /// 優惠內容
        /// </summary>
        public int Discount { get; set; }
        /// <summary>
        /// 折後價
        /// </summary>
        public decimal? DiscountPrice { get; set; }

        public PponDealSynopsis Clone()
        {
            PponDealSynopsis newOne = new PponDealSynopsis();

            newOne.Bid = this.Bid;
            newOne.DealName = this.DealName;
            newOne.MiniDealName = this.MiniDealName;
            newOne.DealEventName = this.DealEventName;
            newOne.ImagePath = this.ImagePath;
            newOne.SquareImagePath = this.SquareImagePath;
            newOne.SoldNum = this.SoldNum;
            newOne.SoldNumShow = this.SoldNumShow;
            newOne.ShowUnit = this.ShowUnit;
            newOne.Price = this.Price;
            newOne.OriPrice = this.OriPrice;
            newOne.DiscountString = this.DiscountString;
            newOne.DealStartTime = this.DealStartTime;
            newOne.DealEndTime = this.DealEndTime;
            newOne.SoldOut = this.SoldOut;
            newOne.TravelPlace = this.TravelPlace;
            newOne.PromoImage = this.PromoImage;
            newOne.DealTags = this.DealTags;
            newOne.IsFami = this.IsFami;
            newOne.ShowExchangePrice = this.ShowExchangePrice;
            newOne.ExchangePrice = this.ExchangePrice;
            newOne.Categories = this.Categories.Select(x => x).ToList();
            newOne.Seq = this.Seq;
            newOne.ExternalDealTagType = this.ExternalDealTagType;
            newOne.DiscountType = this.DiscountType;
            newOne.Discount = this.Discount;
            newOne.DiscountPrice = this.DiscountPrice;

            return newOne;
        }
    }

    public class TodayDealSynopsis
    {
        #region from deal property
        public Guid Bid { get; set; }
        public string DealPromoImage { get; set; }
        public string AppTitle { get; set; }
        public string ItemName { get; set; }
        public string EventTitle { get; set; }
        public string EventName { get; set; }
        public string DealPromoTitle { get; set; }
        public string CouponUsage { get; set; }
        public DateTime BusinessHourOrderTimeS { get; set; }
        public DateTime BusinessHourOrderTimeE { get; set; }
        public string EventImagePath { get; set; }
        public int OrderedQuantity { get; set; }
        public decimal OrderTotalLimit { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal ItemOrigPrice { get; set; }
        public Guid SellerGuid { get; set; }
        #endregion
        public string DealPromoImageTag { get; set; }
        public string DealIcon { get; set; }
        public string DealIconClass { get; set; }
        public string EventImageTag { get; set; }
        public string SubstringName { get; set; }
        public string SubstringTitle { get; set; }
        public string CountDownString { get; set; }
        public string GroupByCountDown { get; set; }
        public string OrderedQuantityShow { get; set; }
        public string DiscountString { get; set; }
        public bool IsWelfare { get; set; }
        public bool IsSoldOut { get; set; }
        public bool IsCombo { get; set; }
        public int CityId { get; set; }
        public int ChannelId { get; set; }
        public int? ChannelAreaId { get; set; }
        public int Seq { get; set; }
        public int DiscountPercent { get; set; }
        public string CityName { set; get; }
        public string CityName2 { set; get; }
        public string StarRatingAll { set; get; }
        public string EveryDayNewDeal { get; set; }
        /// <summary>
        /// 該Bid擁有的categoryId (type為頻道或區域的)
        /// </summary>
        public List<int> ChannelAndRegionCategories { get; set; }

        public IViewPponDeal PponDeal { get; private set; }

        #region 評價

        /// <summary>
        /// 組出html 
        /// </summary>
        public string RatingString { get; private set; }
        public int RatingNumber { get; private set; }
        public decimal RatingScore { get; private set; }
        public string RatingScoreStr
        {
            get
            {
                return RatingScore.ToString("0.#");
            }
        }
        public bool RatingShow { get; private set; }

        #endregion

        public TodayDealSynopsis(IViewPponDeal deal)
        {
            PponCity pponcity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(deal.CityList.FirstOrDefault());
            Bid = deal.BusinessHourGuid;
            DealPromoImage = deal.DealPromoImage;
            AppTitle = deal.AppTitle;
            ItemName = deal.ItemName;
            EventTitle = deal.EventTitle;
            EventName = deal.EventName;
            DealPromoTitle = deal.DealPromoTitle;
            CouponUsage = deal.CouponUsage;
            BusinessHourOrderTimeS = deal.BusinessHourOrderTimeS;
            BusinessHourOrderTimeE = deal.BusinessHourOrderTimeE;
            EventImagePath = deal.EventImagePath;
            OrderedQuantity = deal.OrderedQuantity ?? 0;
            OrderTotalLimit = deal.OrderTotalLimit ?? 0;
            ItemPrice = deal.ItemPrice == 0 ? (deal.ExchangePrice.HasValue ? deal.ExchangePrice.Value : deal.ItemPrice) : deal.ItemPrice;
            ItemOrigPrice = deal.ItemOrigPrice;
            IsWelfare = Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);
            IsSoldOut = (deal.OrderedQuantity >= deal.OrderTotalLimit);
            IsCombo = (deal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0;
            SellerGuid = deal.SellerGuid;
            DiscountPercent = deal.ItemOrigPrice == 0 ? 0 : deal.ItemPrice == 0 ? (
                deal.ExchangePrice.HasValue && deal.ExchangePrice.Value > 0 ? (int)Math.Round((deal.ItemOrigPrice - deal.ExchangePrice.Value) * 100 / deal.ItemOrigPrice) : 0)
                : (int)Math.Round((deal.ItemOrigPrice - deal.ItemPrice) * 100 / deal.ItemOrigPrice);
            CityName = deal.IsMultipleStores ?
                       "<div class='hover_place_3col'><span class='hover_place_text'><i class='fa fa-map-marker'></i>" + deal.HoverMessage + "</span></div>" : string.Empty; ;
            CityName2 = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                    pponcity, deal.BusinessHourGuid);
            ChannelAndRegionCategories = ViewPponDealManager.DefaultManager.GetCategoryIds(Bid);

            PponDeal = deal;
            decimal ratingScore;
            int ratingNumber;
            bool ratingShow;
            string RatingString = PponFacade.GetDealStarRating(
                deal.BusinessHourGuid, "pc",
                out ratingScore, out ratingNumber, out ratingShow);
            RatingScore = ratingScore;
            RatingNumber = ratingNumber;
            RatingShow = ratingShow;
        }
    }

    public class TodayDealCitySynopsis
    {
        public string CityName { get; set; }
        public int CityId { get; set; }
        public int CountDownTotal { get; set; }
        public DateTime GroupByDate { get; set; }
        public List<TodayDealSynopsis> Deals { get; set; }
    }

    public class TodayDealCityCategorySynopsis
    {
        public TodayDealSynopsis Deal { get; set; }
        public List<int> CategoryIds { get; set; }
    }

    public class SkmPponDealSynopsis
    {
        public SkmPponDealSynopsis()
        {
            DealTags = new List<int>();
            //預設為非全家
            IsFami = false;
            //預設不顯示兌換價
            ShowExchangePrice = false;
            ExchangePrice = 0;
            Categories = new List<int>();
        }
        /// <summary>
        /// 檔次代號
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 手機版頁面當作檔次名稱顯示。
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 手機版頁面當作檔次名稱顯示，移除【TravelPlace】。
        /// </summary>
        public string MiniDealName { get; set; }
        /// <summary>
        /// 手機版頁面當作檔次介紹顯示(優惠活動，黑標)。
        /// </summary>
        public string DealEventName { get; set; }
        /// <summary>
        /// 畫面顯示的圖片路徑，回傳的圖片大小因應參數回傳適當大小。
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// 方形(M版)畫面顯示的圖片路徑
        /// </summary>
        public string SquareImagePath { get; set; }
        /// <summary>
        /// 已銷售數量-查詢時商品已銷售的數量
        /// </summary>
        public int SoldNum { get; set; }
        /// <summary>
        /// 已銷售數量顯示值-已銷售數量*設定的倍數 所得到用於顯示的的數值
        /// </summary>
        public int SoldNumShow { get; set; }
        /// <summary>
        /// 銷售數量顯示的單位
        /// </summary>
        public string ShowUnit { get; set; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal OriPrice { get; set; }
        /// <summary>
        /// 折數顯示用文字"5折"等，特殊檔次可能顯示”特惠”等
        /// </summary>
        public string DiscountString { get; set; }
        /// <summary>
        /// 開始販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealStartTime { get; set; }
        /// <summary>
        /// 結束販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealEndTime { get; set; }
        /// <summary>
        /// 是否已售完 true為售完
        /// </summary>
        public bool SoldOut { get; set; }
        /// <summary>
        /// 旅遊地點
        /// </summary>
        public string TravelPlace { get; set; }
        /// <summary>
        /// 檔次promo圖片,用於瀑布流介面
        /// </summary>
        public string PromoImage { get; set; }
        /// <summary>
        /// 檔次的ICON標示，如破千熱銷之類的ID值
        /// </summary>
        public List<int> DealTags { get; set; }
        /// <summary>
        /// 是否為全家檔次
        /// </summary>
        public bool IsFami { get; set; }
        /// <summary>
        /// 頻道類型
        /// </summary>
        public int BehaviorType { set; get; }
        /// <summary>
        /// 是否顯示兌換價
        /// </summary>
        public bool ShowExchangePrice { get; set; }
        /// <summary>
        /// 兌換價
        /// </summary>
        public decimal ExchangePrice { get; set; }
        /// <summary>
        /// 檔次擁有的分類
        /// </summary>
        public List<int> Categories { get; set; }
        /// <summary>
        /// 序號 推薦的排序順序
        /// </summary>
        public int Seq { get; set; }

        /// <summary>
        /// 體驗商品收藏次數
        /// </summary>
        public string ExperienceDealCollectCount { set; get; }
        /// <summary>
        /// 已兌換人數
        /// </summary>
        public string ExchangeDealCount { set; get; }
        /// <summary>
        /// 是否為體驗商品
        /// </summary>
        public bool isExperience { set; get; }
        /// <summary>
        /// 優惠類型
        /// </summary>
        public int DiscountType { get; set; }
        /// <summary>
        /// 優惠內容
        /// </summary>
        public int Discount { get; set; }
        /// <summary>
        /// 是否為清算檔次
        /// </summary>
        public bool IsSettlementDeal { get; set; }
        /// <summary>
        /// 下次清算時間
        /// </summary>
        public string NextSettlementTime { get; set; }
    }

    /// <summary>
    /// 檔次列表內容
    /// </summary>
    public class PponDealSynopsisBasic
    {
        public PponDealSynopsisBasic()
        {
            Location = string.Empty;
            Bid = new Guid();
            DealName = string.Empty;
            ImagePath = string.Empty;
            SquareImagePath = string.Empty;
            SoldNum = 0;
            ShowUnit = string.Empty;
            Price = 0;
            OriginalPrice = 0;
            DisplayPrice = 0;
            DiscountDisplayType = 0;
            DiscountString = string.Empty;
            DealStartTime = string.Empty;
            DealEndTime = string.Empty;
            SoldOut = false;
            BehaviorType = 0;
            DealTags = new List<int>();
            Categories = new List<int>();
            Seq = 0;
            IsMultiDeal = false;
            DeliveryType = 0;
            FreightAmount = 0;
            EnableShipByIsp = false;
        }

        /// <summary>
        /// 檔次代號
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 使用地點有設定即顯示使用地點 無則city.name
        /// </summary>
        public string Location { get; set; }
        /// <summary>
        /// 手機版頁面當作檔次名稱顯示。
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 畫面顯示的圖片路徑，回傳的圖片大小因應參數回傳適當大小。
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// 方形(M版)畫面顯示的圖片路徑
        /// </summary>
        public string SquareImagePath { get; set; }
        /// <summary>
        /// 已銷售數量-查詢時商品已銷售的數量
        /// </summary>
        public int SoldNum { get; set; }
        /// <summary>
        /// 銷售數量顯示的單位
        /// </summary>
        public string ShowUnit { get; set; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal OriginalPrice { get; set; }
        /// <summary>
        /// 顯示金額
        /// </summary>
        public decimal DisplayPrice { get; set; }
        /// <summary>
        /// 折數顯示用文字"5折"等，特殊檔次可能顯示”特惠”等
        /// </summary>
        public string DiscountString { get; set; }
        /// <summary>
        /// 優惠顯示方式
        /// </summary>
        public int DiscountDisplayType { get; set; }
        /// <summary>
        /// 開始販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealStartTime { get; set; }
        /// <summary>
        /// 結束販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealEndTime { get; set; }
        /// <summary>
        /// 是否已售完 true為售完
        /// </summary>
        public bool SoldOut { get; set; }
        /// <summary>
        /// 檔次的ICON標示，如破千熱銷之類的ID值
        /// </summary>
        public List<int> DealTags { get; set; }
        /// <summary>
        /// 頻道類型
        /// </summary>
        public int BehaviorType { set; get; }
        /// <summary>
        /// 檔次擁有的分類
        /// </summary>
        public List<int> Categories { get; set; }
        /// <summary>
        /// 序號 推薦的排序順序
        /// </summary>
        public int Seq { get; set; }
        /// <summary>
        /// 是否為多檔次
        /// </summary>
        public bool IsMultiDeal { get; set; }
        /// <summary>
        /// 好康類型
        /// </summary>
        public int DeliveryType { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public decimal FreightAmount { get; set; }
        /// <summary>
        /// 是否可超取
        /// </summary>
        public bool EnableShipByIsp { get; set; }
        /// <summary>
        /// 今日強檔壓圖類型
        /// </summary>
        public EveryDayNewDeal EveryDayNewDeal { get; set; }
        /// <summary>
        /// 檔次評價
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DealEvaluate DealEvaluate { get; set; }
        /// <summary>
        /// 折後價
        /// </summary>
        public decimal? DiscountPrice { get; set; }
    }

    /// <summary>
    /// 評等
    /// </summary>
    public class DealEvaluate
    {
        /// <summary>
        /// 評價星等
        /// </summary>
        public decimal EvaluateStar { get; set; }
        /// <summary>
        /// 評價次數
        /// </summary>
        public int EvaluateNumber { get; set; }
    }

    /// <summary>
    /// 給外部搜尋外部服務，提供需要的檢索的內容
    /// </summary>
    public class ViewPponDealSeachModel
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deal"></param>
        public ViewPponDealSeachModel(IViewPponDeal deal)
        {
            this.BusinessHourGuid = deal.BusinessHourGuid;
            this.PicAlt = deal.PicAlt;
            this.EventTitle = deal.EventTitle;
            this.EventName = deal.EventName;
            this.ItemName = deal.ItemName;
            this.AppTitle = deal.AppTitle;
        }
        /// <summary>
        /// 
        /// </summary>
        public string PicAlt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid BusinessHourGuid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EventTitle { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EventName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ItemName { get; set; }
        public string AppTitle { get; set; }
    }

    /// <summary>
    /// 主題企劃
    /// </summary>
    public class AppRandomParagraph
    {
        ///// <summary>
        ///// 類型 0:無 1:頻道 2:單檔 3:策展 4:策展列表
        ///// </summary>
        //public AppRandomParagraphType Type { get; set; }
        /// <summary>
        /// 圖片連結
        /// </summary>
        public string PicUrl { get; set; }
        /// <summary>
        /// 導頁連結
        /// </summary>
        public string NavigatorUrl { get; set; }
    }

}

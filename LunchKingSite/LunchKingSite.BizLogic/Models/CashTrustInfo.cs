﻿using System;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Model
{
    public class CashTrustInfo
    {
        public Guid OrderGuid { get; set; }
        public OrderDetailCollection OrderDetails { get; set; }
        public int CreditCardAmount { get; set; }
        public int SCashAmount { get; set; }
        public int PscashAmount { get; set; }
        public int PCashAmount { get; set; }
        public int BCashAmount { get; set; }
        public int LCashAmount { get; set; }
        public int Quantity { get; set; }
        public int DeliveryCharge { get; set; }
        public DeliveryType DeliveryType { get; set; }
        public int DiscountAmount { get; set; }
        public int AtmAmount { get; set; }
        public int FamilyIspAmount { get; set; }
        public int SevenIspAmount { get; set; }
        public Guid BusinessHourGuid { get; set; }
        public int CheckoutType { get; set; }
        public TrustProvider TrustProvider { get; set; }
        public string ItemName { get; set; }
        public int ItemPrice { get; set; }
        public int ItemOriPrice { get; set; }
        public Member User { get; set; }
        public Dictionary<Core.PaymentType, int> Payments { get; set; }
        public int ThirdPartyCashAmount { set; get; }
        public ThirdPartyPayment ThirdPartySystem { set; get; }
        public int PresentQuantity { get; set; }
        public bool IsGroupCoupon { get; set; }
        public OrderClassification OrderClass { get; set; }
        public GroupCouponDealType GroupCouponType { get; set; }        

        public CashTrustInfo()
        {
            OrderClass = OrderClassification.LkSite;
        }
    }
}

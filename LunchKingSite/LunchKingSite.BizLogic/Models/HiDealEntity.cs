﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Web;

namespace LunchKingSite.BizLogic.Model
{
    public class HiDealEntity
    {
        #region fields
        private IHiDealProvider hdprovider = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        private ISellerProvider sellerProvider = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private bool isNew = false;
        private DateTime? initialStartTime = null;
        private DateTime? initialEndTime = null;
        private bool originalIsAlwaysMain = false;
        private string initialCities = null;
        #endregion

        #region Properties

        private HiDealDeal _deal = null;
        public HiDealDeal Deal
        {
            get { return _deal; }
            set { _deal = value; }
        }


        private HiDealCityCollection _slots = null;
        /// <summary>
        /// provides the current set of deal slots.
        /// </summary>
        public HiDealCityCollection HiDealSlots
        {
            get
            {
                if (_slots == null && Deal.Id != 0)
                {
                    _slots = hdprovider.HiDealCityGetListByDealId(Deal.Id);
                }
                return _slots;
            }
        }
        
        private HiDealRegionCollection _dealRegion = null;
        public HiDealRegionCollection DealRegion
        {
            get
            {
                if(_dealRegion == null && Deal.Id != 0)
                {
                    _dealRegion = hdprovider.HiDealRegionGetList(Deal.Id);
                }
                return _dealRegion;
            }
        }

        private HiDealCategoryCollection _dealCategory = null;
        /// <summary>
        /// Provides the current set of deal categories. To set the categories of the deal, use CategoryIds
        /// </summary>
        public HiDealCategoryCollection DealCategory
        {
            get
            {
                if(_dealCategory == null && Deal.Id != 0)
                {
                    _dealCategory = hdprovider.HiDealDealCategoryGetListByDealId(Deal.Id);
                }
                return _dealCategory;
            }
        }
        
        private List<int> categoryIds = null;
        /// <summary>
        /// Set this list with category ids. The ids need to be from the source [system_code].[code_id], where [system_code].[code_group] = 'HiDealCatg'
        /// </summary>
        public List<int>  CategoryIds
        {
            set { categoryIds = value; }
        }

        private HiDealSpecialDiscountCollection _dealDiscount = null;
        public HiDealSpecialDiscountCollection DealDiscount
        {
            get
            {
                if (_dealDiscount == null && Deal.Id != 0)
                {
                    _dealDiscount = hdprovider.HiDealSpecialDiscountGetListByDealId(Deal.Id);
                }
                return _dealDiscount;
            }
        }

        private List<int> discountIds = null;
        public List<int> DiscountIds
        {
            set { discountIds = value; }
        }
        
        private HiDealContentCollection _dealContents = null;
        public HiDealContentCollection DealContents
        {
            get
            {
                if(_dealContents == null)
                {
                    return this.hdprovider.HiDealContentGetAllByDealId(this.Deal.Id);
                }
                return _dealContents;
            }
        }

        private StoreCollection _branchStores = null;
        /// <summary>
        /// 此檔賣家的所有分店. 
        /// </summary>
        private StoreCollection BranchStores
        {
            get 
            {
                if (_branchStores == null)
                {
                    _branchStores = sellerProvider.StoreGetListBySellerGuid(Deal.SellerGuid) ?? new StoreCollection();
                }
                return _branchStores;
            }
        }
        
        private List<DealBranchStoreSetting> _branchStoreSetting = null;
        /// <summary>
        /// Gets or sets the sequence of the branch stores for this deal.
        /// </summary>
        public List<DealBranchStoreSetting> BranchStoreSettings
        {
            get
            {
                if (_branchStoreSetting == null) 
                {
                    _branchStoreSetting = new List<DealBranchStoreSetting>();
                    HiDealDealStoreCollection storeCollection = hdprovider.HiDealDealsStoreGetListByHiDealId(Deal.Id);
                    foreach (HiDealDealStore store in storeCollection)
                    {
                        _branchStoreSetting.Add(new DealBranchStoreSetting { StoreGuid = store.StoreGuid, Sequence = store.Seq.HasValue ? store.Seq.Value : -1 , UseTime = store.UseTime});
                    }
                }
                return _branchStoreSetting;
            }
            set { _branchStoreSetting = value; }
        }

        private HiDealProductCollection _products = null;
        public HiDealProductCollection Products
        {
            get
            {
                if (_products == null)
                {
                    if (this.Deal.Id != 0)
                    {
                        _products = hdprovider.HiDealProductCollectionGet(this.Deal.Id);
                    }
                    else
                    {
                        _products = new HiDealProductCollection();
                    }
                }
                //_products.Sort(HiDealProduct.Columns.Seq, true);
                return _products;
            }
        }

        #endregion

        #region primary API

        public void Save()
        {
            DateTime processTime = DateTime.Now;
            
            if (isNew)   //generate GUID for creating new deal
            {
                Deal.HiDealGuid = Guid.NewGuid();
            }

            if (isNew)
            {
                Deal.Id = hdprovider.HiDealDealSet(Deal);   //create new deal and return the id of the deal for further processing
            }
            else
            {
                hdprovider.HiDealDealSet(Deal);
            }

            bool dirtyTime = (this.initialStartTime != Deal.DealStartTime || this.initialEndTime != Deal.DealEndTime);
            bool dirtyCities = (Deal.Cities != this.initialCities);

            #region write data to hi_deal_region

            if (dirtyCities || isNew)
            {
                if(Deal.Cities != null)
                    hdprovider.HiDealRegionMerge(Deal.Id, new List<string>(Deal.Cities.Split(',')));
            }

            #endregion

            #region write data to hi_deal_category
            if(this.categoryIds != null)
            {
                hdprovider.HiDealCategoryMerge(Deal.Id, this.categoryIds);
            }

            #endregion

            #region write data to hi_deal_special_discount
            if(this.discountIds != null)
            {
                hdprovider.HiDealSpecialDiscountMerge(Deal.Id, this.discountIds);
            }
            #endregion

            #region write data to hi_deal_city

            bool isMainChanged = originalIsAlwaysMain ^ Deal.IsAlwaysMain;

            if (dirtyTime || dirtyCities || isNew || isMainChanged)
            {
                if (Deal.DealStartTime.HasValue && Deal.DealEndTime.HasValue && Deal.Cities != null)
                    hdprovider.HiDealCityDealTimeSet(Deal.Id, Deal.DealStartTime.Value, Deal.DealEndTime.Value, new List<string>(Deal.Cities.Split(',')), Deal.IsAlwaysMain);
            }

            #endregion
            
            #region write data to hi_deal_deals_store

            HiDealDealStoreCollection storeCollection = hdprovider.HiDealDealsStoreGetListByHiDealId(Deal.Id);

            foreach (HiDealDealStore store in storeCollection)    //delete & modify
            {
                int i = BranchStoreSettings.FindIndex(storeSetting => storeSetting.StoreGuid == store.StoreGuid);
                if (i == -1)
                {
                    hdprovider.HiDealDealsStoreDelete(store);
                }
                else
                {
                    if (store.Seq != BranchStoreSettings[i].Sequence || store.UseTime != BranchStoreSettings[i].UseTime)
                    {
                        store.Seq = BranchStoreSettings[i].Sequence;
                        store.UseTime = BranchStoreSettings[i].UseTime;
                        store.ModifyId = HttpContext.Current.User.Identity.Name;
                        store.ModifyTime = processTime;
                        hdprovider.HiDealDealsStoreSet(store);
                    }
                }
            }

            foreach (DealBranchStoreSetting storeSetting in BranchStoreSettings)  //insert
            {
                bool foundInDB = false;
                foreach (HiDealDealStore store in storeCollection)
                {
                    if (store.StoreGuid == storeSetting.StoreGuid)
                    {
                        foundInDB = true;
                    }
                }
                if (!foundInDB)
                {
                    HiDealDealStore newStore = new HiDealDealStore();
                    newStore.HiDealId = this.Deal.Id;
                    newStore.StoreGuid = storeSetting.StoreGuid;
                    newStore.Seq = storeSetting.Sequence;
                    newStore.UseTime = storeSetting.UseTime;
                    newStore.CreateId = HttpContext.Current.User.Identity.Name;
                    newStore.CreateTime = processTime;
                    hdprovider.HiDealDealsStoreSet(newStore);
                }
            }

            //if(isBranchStoreSequenceUpdated || isNew)
            //{
            //    //delete unlisted items
            //    if( !isNew )
            //    {
            //        foreach(HiDealDealStore store in BranchStoreSequenceSettings)
            //        {
            //            if(!BranchStoreSequence.ContainsValue(store.StoreGuid)) //不在指定內的分店資料要刪掉
            //            {
            //                hdprovider.HiDealDealsStoreDelete(store);
            //            }
            //        }
            //    }

            //    //update or insert new
            //    foreach (KeyValuePair<int, Guid> pair in BranchStoreSequence)
            //    {
            //        HiDealDealStore storeSeq = hdprovider.HiDealDealsStoreGet(Deal.Id, pair.Value);
            //        if(storeSeq.Id == 0)    //no record found
            //        {
            //            storeSeq = new HiDealDealStore
            //                           {
            //                               HiDealId = Deal.Id,
            //                               StoreGuid = pair.Value,
            //                               Seq = pair.Key,
            //                               CreateId = HttpContext.Current.User.Identity.Name,
            //                               CreateTime = DateTime.Now,
            //                               ModifyId = HttpContext.Current.User.Identity.Name,
            //                               ModifyTime = DateTime.Now
            //                           };
            //        }
            //        else
            //        {
            //            storeSeq.Seq = pair.Key;
            //            storeSeq.ModifyId = HttpContext.Current.User.Identity.Name;
            //            storeSeq.ModifyTime = DateTime.Now;
            //        }
            //        hdprovider.HiDealDealsStoreSet(storeSeq);
            //    }
            //}

            #endregion

        }

        public StoreCollection GetAllSellerBranchStores()
        {
            return BranchStores;
        }

        public bool CreateProduct(string productName)
        {
            if (string.IsNullOrEmpty(productName))
                return false;
            HiDealProduct product = new HiDealProduct(true);
            product.Guid = Guid.NewGuid();
            product.SellerGuid = this.Deal.SellerGuid;
            product.Name = productName;
            product.DealId = this.Deal.Id;
            product.CreateId = HttpContext.Current.User.Identity.Name;
            product.CreateTime = DateTime.Now;
            int id = hdprovider.HiDealProductSet(null);
            return true;
        }

        public string GetMediaRelativePictureDirPath(bool isBigPic, bool isSmallPic, bool isProductPics)
        {
            if (isBigPic && isSmallPic && isProductPics)
                return null;
            if (!(isBigPic ^ isSmallPic ^ isProductPics))
                return null;

            //string s = sysCfgProvider.MediaBaseUrl;

            string folder = "";
            if(isBigPic)
                folder = "PrimaryBigPicture/";
            if (isSmallPic)
                folder = "PrimarySmallPicture/";
            if (isProductPics)
                folder = "SecondaryPictures/";

            return string.Format("HiDeal_{0}/{1}", Deal.Id.ToString(), folder); 
        }

        #endregion

        #region static HiDealEntity instance creators
        public static HiDealEntity Create(Guid sid)
        {
            HiDealEntity entity = new HiDealEntity();
            entity.Deal.SellerGuid = sid;
            return entity;
        }

        public static HiDealEntity GetByDealId(int dealId)
        {
            HiDealEntity entity = new HiDealEntity(dealId);
            return entity;
        }

        public static HiDealEntity GetByDealGuid(Guid dealGuid)
        {
            HiDealEntity entity = new HiDealEntity(dealGuid);
            return entity;
        }
        #endregion

        #region .ctors
        private HiDealEntity()
        {
            this.Deal = new HiDealDeal(true);
            this.initialStartTime = Deal.DealStartTime;
            this.initialEndTime = Deal.DealEndTime;
            this.initialCities = Deal.Cities;
            this.originalIsAlwaysMain = Deal.IsAlwaysMain;
            this.isNew = true;
        }

        private HiDealEntity(int dealId)
        {
            HiDealDeal deal = hdprovider.HiDealDealGet(dealId);
            if (deal.Id == 0)
            {
                throw new ArgumentException(string.Format("The provided hi-deal id [{0}] corresponds to no record.", dealId));
            }
            this.Deal = deal;
            this.initialStartTime = Deal.DealStartTime;
            this.initialEndTime = Deal.DealEndTime;
            this.initialCities = Deal.Cities;
            this.originalIsAlwaysMain = Deal.IsAlwaysMain;
        }

        private HiDealEntity(Guid dealGuid)
        {
            HiDealDeal deal = hdprovider.HiDealDealGet(dealGuid);
            if (deal.Id == 0)
            {
                throw new ArgumentException(string.Format("The provided hi-deal guid [{0}] corresponds to no record.", dealGuid));
            }
            this.Deal = deal;
            this.initialStartTime = Deal.DealStartTime;
            this.initialEndTime = Deal.DealEndTime;
            this.initialCities = Deal.Cities;
            this.originalIsAlwaysMain = Deal.IsAlwaysMain;
        }
        #endregion
        
    }

    public class DealBranchStoreSetting
    {
        public Guid StoreGuid { get; set; }
        public int Sequence { get; set; }
        public string UseTime { get; set; }
    }
}

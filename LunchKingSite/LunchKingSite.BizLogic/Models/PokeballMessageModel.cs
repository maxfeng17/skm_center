﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Models
{
    /// <summary>
    /// 訊息中心
    /// </summary>
    public class MessageCollectionModel
    {
        public int Id { get; set; }
        public string IsRead { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string SendTime { get; set; }
        public List<object> PokeballList { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", SendTime, Content);
        }
    }

    /// <summary>
    /// 單檔Pokeball
    /// </summary>
    public class PushBidPokeball
    {
        public string Type { get; set; }
        public Guid BId { get; set; }

        public PushBidPokeball()
        {
            Type = "ShowDealDetail";
        }
    }

    /// <summary>
    /// 頻道Pokeball
    /// </summary>
    public class PushChannelPokeball
    {
        public string Type { get; set; }
        public int? ChannelId { get; set; }
        public int? Area { get; set; }
        public bool Reload { get; set; }

        public PushChannelPokeball()
        {
            Type = "ShowChannel";
        }
    }

    /// <summary>
    /// 優惠券Pokeball
    /// </summary>
    public class PushVourcherPokeball
    {
        public string Type { get; set; }
        public int? VourcherId { get; set; }

        public PushVourcherPokeball()
        {
            Type = "ShowCouponDetail";
        }
    }

    /// <summary>
    /// 賣家Pokeball
    /// </summary>
    public class PushSellerPokeball
    {
        public string Type { get; set; }
        public string SellerId { get; set; }

        public PushSellerPokeball()
        {
            Type = "ShowCouponSeller";
        }
    }

    /// <summary>
    /// 商品主題活動Pokeball
    /// </summary>
    public class PushEventPromoPokeball
    {
        /// <summary>
        /// Pokeball Type
        /// </summary>
        public string Type { get; set; }
        //public int EventType { get; set; }
        public int EventPromoId { get; set; }

        public PushEventPromoPokeball()
        {
            Type = "ShowEventPromo";
        }
    }

    /// <summary>
    /// 策展2.0 Pokeball
    /// </summary>
    public class PushCurationPokeball
    {
        /// <summary>
        /// Pokeball Type
        /// </summary>
        public string Type { get; set; }
        public int EventType { get; set; }
        public int EventId { get; set; }

        public PushCurationPokeball()
        {
            Type = "ShowCurationPromo";
        }
    }

    /// <summary>
    /// URL Pokeball
    /// </summary>
    public class PushCustomUrlPokeball
    {
        /// <summary>
        /// Pokeball Type
        /// </summary>
        public string Type { get; set; }
        public string Url { get; set; }

        public PushCustomUrlPokeball()
        {
            Type = "FullscreenWebView";
        }
    }

    /// <summary>
    /// 顯示商家訂單列表
    /// </summary>
    public class ShowOrdersByVerifyStorePokeball
    {
        /// <summary>
        /// Pokeball Type
        /// </summary>
        public string Type { get; set; }
        
        public Guid VerifyStoreGuid { get; set; }

        public ShowOrdersByVerifyStorePokeball()
        {
            Type = "ShowOrdersByVerifyStore";
        }
    }

    /// <summary>
    /// 顯示商家檔次列表
    /// </summary>
    public class ShowDealsByVerifyStorePokeball
    {
        /// <summary>
        /// Pokeball Type
        /// </summary>
        public string Type { get; set; }

        public Guid VerifyStoreGuid { get; set; }

        public ShowDealsByVerifyStorePokeball()
        {
            Type = "ShowDealsByVerifyStore";
        }
    }
}

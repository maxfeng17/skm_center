﻿
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.BizLogic.Model
{
    #region 通知信  

    #region 電子發票開立通知信的變數

    public class EInvoiceInformation
    {
        /// <summary>
        /// 購買者名稱
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// 購買物品名稱 (主標+商品名稱)
        /// </summary>
        public string PurchasedItemName { get; set; }

        /// <summary>
        /// 發票號碼
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// 發票日期 (格式請設定為: 100年2月20日 )
        /// </summary>
        public string InvoiceDate { get; set; }

        /// <summary>
        /// 買受人
        /// </summary>
        public string InvoiceIndividualName { get; set; }

        /// <summary>
        /// 公司名稱
        /// </summary>
        public string InvoiceCompanyName { get; set; }

        /// <summary>
        /// 公司統一編號
        /// </summary>
        public string InvoiceCompanyId { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string InvoiceAddress { get; set; }

        /// <summary>
        /// 是否為公司發票 (三聯式): 是 => InvoiceCompanyId / InvoiceCompanyName  否 => InvoiceIndividualName  ( 公司發票: [einvoice_main].[invoice_mode] = 18 )
        /// </summary>
        public bool IsCompanyInvoice { get; set; }

        /// <summary>
        /// 是否為商品檔
        /// </summary>
        public bool IsPhysicalProductItem { get; set; }

        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        [XmlArray("Items")]
        [XmlArrayItem("Item")]
        public string[] ItemNames { get; set; }

        /// <summary>
        /// 總金額
        /// </summary>
        public int TotalAmount { get; set; }

        /// <summary>
        /// 大寫數字總金額 (格式: 貳百伍拾)
        /// </summary>
        public string TotalAmountTaiwanFormat { get; set; }

        /// <summary>
        /// 稅率 ( 0 為免稅, > 0 為應稅 )
        /// </summary>
        public decimal TaxRate { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }
    }

    #endregion

    #region 發票中獎通知信的變數

    public class EInvoiceWonNotificationInformation
    {
        /// <summary>
        /// 購買者名稱
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// 購買物品名稱 (主標+商品名稱)
        /// </summary>
        public string PurchasedItemName { get; set; }

        /// <summary>
        /// 發票號碼
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// 發票日期 (格式請設定為: 100年2月20日 )
        /// </summary>
        public string InvoiceDate { get; set; }

        /// <summary>
        /// 買受人
        /// </summary>
        public string InvoiceIndividualName { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string InvoiceAddress { get; set; }

        /// <summary>
        /// 是否為商品檔
        /// </summary>
        public bool IsPhysicalProductItem { get; set; }

        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        [XmlArray("Items")]
        [XmlArrayItem("Item")]
        public string[] ItemNames { get; set; }

        /// <summary>
        /// 總金額
        /// </summary>
        public int TotalAmount { get; set; }

        /// <summary>
        /// 大寫數字總金額 (格式: 貳百伍拾)
        /// </summary>
        public string TotalAmountTaiwanFormat { get; set; }

        /// <summary>
        /// 稅率 ( 0 為免稅, > 0 為應稅 )
        /// </summary>
        public decimal TaxRate { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }
    }

    #endregion

    #region 退貨成功通知信的變數

    public class RefundSuccessNotificationInformation
    {
        /// <summary>
        /// 購買者名稱
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// 退貨物品名稱 (主標+商品名稱)
        /// </summary>
        public string RefundItemName { get; set; }

        /// <summary>
        /// 退貨明細 url
        /// </summary>
        public string RefundDetailUrl { get; set; }

        /// <summary>
        /// 訂單商品(憑證)數
        /// </summary>
        public int OrderItemsCount { get; set; }

        /// <summary>
        /// 本次實際退貨數
        /// </summary>
        public int RefundItemsCount { get; set; }

        /// <summary>
        /// 退款總金額
        /// </summary>
        public int TotalRefundAmount { get; set; }

        /// <summary>
        /// PayEasy購物金抵扣金額
        /// </summary>
        public int PCash { get; set; }

        /// <summary>
        /// 17Life購物金抵扣金額
        /// </summary>
        public int SCash { get; set; }

        /// <summary>
        /// 17Life紅利金抵扣金額
        /// </summary>
        public int BCash { get; set; }

        /// <summary>
        /// 17Life折價券抵扣金額
        /// </summary>
        public int DCash { get; set; }

        /// <summary>
        /// 刷卡支付金額
        /// </summary>
        public int CreditCardCharged { get; set; }

        /// <summary>
        /// ATM支付金額
        /// </summary>
        public int AtmCharged { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }
    }

    #endregion

    #region 退貨退款失敗通知信的變數

    public class RefundFailureNotificationInformation
    {
        /// <summary>
        /// 購買者名稱
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// 退貨物品名稱 (主標+商品名稱)
        /// </summary>
        public string RefundItemName { get; set; }

        /// <summary>
        /// 退貨明細 url
        /// </summary>
        public string RefundDetailUrl { get; set; }

        /// <summary>
        /// 訂單商品(憑證)數
        /// </summary>
        public int OrderItemsCount { get; set; }

        /// <summary>
        /// 本次實際退貨數
        /// </summary>
        public int RefundItemsCount { get; set; }

        /// <summary>
        /// 退貨退款失敗原因
        /// </summary>
        public string FailureReason { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }
    }

    #endregion

    #region 退貨申請書通知信的變數

    public class RefundApplicationFormNotificationInformation
    {
        /// <summary>
        /// 購買者名稱
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// 退貨物品名稱 (主標+商品名稱)
        /// </summary>
        public string RefundItemName { get; set; }

        /// <summary>
        /// 訂單商品(憑證)數
        /// </summary>
        public int OrderItemsCount { get; set; }

        /// <summary>
        /// 本次實際退貨數
        /// </summary>
        public int RefundItemsCount { get; set; }

        /// <summary>
        /// 退貨申請書 Url
        /// </summary>
        public string RefundApplicationUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }
    }

    #endregion

    #region 折讓單通知書的變數
    public class DiscountSingleFormNotificationInformation
    {
        /// <summary>
        /// 購買者名稱
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// 退貨物品名稱 (主標+商品名稱)
        /// </summary>
        public string RefundItemName { get; set; }

        /// <summary>
        /// 訂單商品(憑證)數
        /// </summary>
        public int OrderItemsCount { get; set; }

        /// <summary>
        /// 本次實際退貨數
        /// </summary>
        public int RefundItemsCount { get; set; }

        /// <summary>
        /// 折讓單 Url
        /// </summary>
        public string InvoiceMailbackAllowanceUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }
    }
    #endregion

    #region 退貨申請通知信的變數
    public class ApplyRefundCouponNotificationInformation
    {
        /// <summary>
        /// 購買者名稱
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// 退貨物品名稱 (主標+商品名稱)
        /// </summary>
        public string RefundItemName { get; set; }

        /// <summary>
        /// 訂單商品(憑證)數
        /// </summary>
        public int OrderItemsCount { get; set; }

        /// <summary>
        /// 本次實際退貨數
        /// </summary>
        public int RefundItemsCount { get; set; }

        /// <summary>
        /// 退貨申請書 Url
        /// </summary>
        public string RefundApplicationUrl { get; set; }

        /// <summary>
        /// 發票是否產生，影響折讓單Url是否顯示
        /// </summary>
        public bool IsCreateEinvoice { get; set; }

        /// <summary>
        /// 折讓單 Url
        /// </summary>
        public string InvoiceMailbackAllowanceUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }
    }

    #endregion 退貨申請通知信的變數

    #region 商品退貨申請通知信的變數
    public class ApplyRefundGoodsNotificationInformation
    {
        /// <summary>
        /// 購買者名稱
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// 退貨物品名稱 (主標+商品名稱)
        /// </summary>
        public string RefundItemName { get; set; }

        /// <summary>
        /// 訂單商品(憑證)數
        /// </summary>
        public int OrderItemsCount { get; set; }

        /// <summary>
        /// 本次實際退貨數
        /// </summary>
        public int RefundItemsCount { get; set; }

        /// <summary>
        /// 退貨申請書 Url
        /// </summary>
        public string RefundApplicationUrl { get; set; }

        /// <summary>
        /// 發票是否產生，影響折讓單Url是否顯示
        /// </summary>
        public bool IsCreateEinvoice { get; set; }

        /// <summary>
        /// 折讓單 Url
        /// </summary>
        public string InvoiceMailbackAllowanceUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }
    }

    #endregion 商品退貨申請通知信的變數

    #region 優惠使用提醒通知信的變數

    public class StartUseReminderInformation
    {
        /// <summary>
        /// 購買者名稱
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// 優惠截止日期 (請設定為 yyyy/MM/dd 格式)
        /// </summary>
        public string ExpireDate { get; set; }

        /// <summary>
        /// 購買物品名稱 (主標+商品名稱)
        /// </summary>
        public string PurchasedItemName { get; set; }

        /// <summary>
        /// 訂單明細 url
        /// </summary>
        public string OrderDetailUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }

    }

    #endregion

    #region 優惠到期通知信的變數

    public class ExpireReminderInformation
    {
        /// <summary>
        /// 購買者名稱
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// 優惠截止日期 (請設定為 yyyy/MM/dd 格式)
        /// </summary>
        public string ExpireDate { get; set; }

        /// <summary>
        /// 購買物品名稱 (主標+商品名稱)
        /// </summary>
        public string PurchasedItemName { get; set; }

        /// <summary>
        /// 訂單明細 url
        /// </summary>
        public string OrderDetailUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }
    }

    #endregion

    #region 賣家/分店結束營業通知信的變數

    public class SellerOrStoreCloseDownInformation
    {
        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName { get; set; }

        /// <summary>
        /// 分店名稱
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 結束營業日期 (請用 yyyy/MM/dd 格式)
        /// </summary>
        public string CloseDownDate { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }
    }

    #endregion

    #region 使用期限變更通知信的變數

    public class CouponExpireDateChangedInformation
    {
        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName { get; set; }

        /// <summary>
        /// 分店名稱
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 檔次連結
        /// </summary>
        public string DealUrl { get; set; }

        /// <summary>
        /// 檔名
        /// </summary>
        public string DealName { get; set; }

        /// <summary>
        /// 憑證使用起始日
        /// </summary>
        public string CouponUseStartDate { get; set; }

        /// <summary>
        /// 憑證到期日
        /// </summary>
        public string CouponUseOriginalExpireDate { get; set; }

        /// <summary>
        /// 調整過的到期日
        /// </summary>
        public string ChangedExpireDate { get; set; }

        /// <summary>
        /// 無須設定. 送信物件會設定截止日期是停止使用或者是延長使用
        /// </summary>
        public bool IsExtend { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }

        /// <summary>
        /// 檔號
        /// </summary>
        public int UniqueId { get; set; }
    }

    #endregion

    public class CouponMailInformation
    {
        /// <summary>
        /// Promo Slot Id
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Header.png
        /// </summary>
        public string HDmailHeaderUrl { get; set; }

        /// <summary>
        /// www.17life.com/Themes/HighDeal/images/mail/HDmail_Footer.png
        /// </summary>
        public string HDmailFooterUrl { get; set; }
    }

    #endregion


}

﻿using System.Collections.Generic;
using Vodka.Mongo;

namespace LunchKingSite.BizLogic.Model
{
    /// <summary>
    /// 館別
    /// </summary>
    public class ProvisionDepartment : Entity
    {
        public List<ProvisionCategory> Categorys { get; set; }
        public string Name { get; set; }

        public ProvisionDepartment()
        {
            Categorys = new List<ProvisionCategory>();
        }
    }

    /// <summary>
    /// 類別
    /// </summary>
    public class ProvisionCategory : Entity
    {
        public List<ProvisionItem> Items { get; set; }
        public string Name { get; set; }
        public int Rank { get; set; }

        public ProvisionCategory()
        {
            Items = new List<ProvisionItem>();
        }
    }

    /// <summary>
    /// 項目
    /// </summary>
    public class ProvisionItem : Entity
    {
        public List<ProvisionContent> Contents { get; set; }
        public string Name { get; set; }
        public int Rank { get; set; }

        public ProvisionItem()
        {
            Contents = new List<ProvisionContent>();
        }
    }

    /// <summary>
    /// 內容
    /// </summary>
    public class ProvisionContent : Entity
    {
        public List<ProvisionDesc> ProvisionDescs { get; set; }
        public string Name { get; set; }
        public int Rank { get; set; }

        public ProvisionContent()
        {
            ProvisionDescs = new List<ProvisionDesc>();
        }
    }

    /// <summary>
    /// 服務條款
    /// </summary>
    public class ProvisionDesc : Entity
    {
        public ProvisionDescStatus Status { get; set; }
        public string Description { get; set; }
    }

    #region enum
    public enum ProvisionType
    {
        None,
        Department,
        Category,
        Item,
        Content,
        Description
    }

    public enum ProvisionDescStatus
    {
        /// <summary>
        /// 預設
        /// </summary>
        Default,
        /// <summary>
        /// 必要項目
        /// </summary>
        Necessary,
        /// <summary>
        /// 停用
        /// </summary>
        Disabled,
    }
    #endregion
}

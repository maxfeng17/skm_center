﻿using log4net;
using LunchKingSite.BizLogic.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Web;

namespace LunchKingSite.BizLogic.Models
{
    public class PponDealListCondition
    {
        private ILog logger = LogManager.GetLogger("Troubleshooting");
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public PponDealListCondition()
        {
            this.FilterCategoryIdList = new List<int>();
        }

        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="fullUrl">當前網址</param>        
        public PponDealListCondition(string fullUrl)
        {

            fullUrl = string.IsNullOrWhiteSpace(fullUrl) ? string.Empty : fullUrl.Replace("delivery/", "").ToLower();
            try
            {
                this.PponUri = new Uri(fullUrl);
            }
            catch (Exception ex)
            {
                string inputStr;
                try
                {
                    HttpContext.Current.Request.InputStream.Position = 0;
                    System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Request.InputStream);
                    inputStr = reader.ReadToEnd();
                }
                catch (Exception ex2)
                {
                    inputStr = string.Format("error to get inputStr ({0})", ex2);
                }

                logger.Info(string.Format("UriFormatException occured on initiating Uri:\nfullUrl={0}\ninputStr={1}", fullUrl, inputStr), ex);
                this.PponUri = new Uri("https://www.17life.com");
                if (config.EnableElmahLogUriIsEmpty)
                {
                    throw;
                }
            }
            this.CityId = GetCityIdFromUri();
            this.ChannelId = GetChannelIdByCityId();
            this.AreaId = GetAreaIdFromUri();
            this.SubCategoryId = GetSubcategoryIdFromUri();

            this.NewDealCategoryId = GetNewDealCategoryIdFromUri();
            //this.FilterCategoryIdList = GetFilterCategoryIdListFromUri();
            this.FilterCategoryIdList = GetFilterCategoryIdListFromSession(this.ChannelId);
            
        }

        #region Property

        private Uri PponUri { get; set; }

        public int CityId { get; set; }
        public int ChannelId { get; set; }
        public int AreaId { get; set; }
        public int SubCategoryId { get; set; }

        /// <summary>
        /// 新版分類ID
        /// </summary>
        public int NewDealCategoryId { get; set; }

        /// <summary>
        /// checkbox用條件(ex.即買即用、最後一天、...)
        /// </summary>
        public List<int> FilterCategoryIdList { get; set; }

        public string GetCacheKey(CategorySortType sortType)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("PponDealListCondition:{0},{1},{2},{3},{4},{5},{6}",
                PponUri, CityId, ChannelId, AreaId, SubCategoryId, NewDealCategoryId, sortType);
            bool hadWriteFilterCategoryIdList = false;
            if (FilterCategoryIdList != null && FilterCategoryIdList.Count > 0)
            {
                sb.Append(",(");
                foreach (var cateId in FilterCategoryIdList)
                {
                    if (hadWriteFilterCategoryIdList)
                    {
                        sb.Append('/');
                    }
                    sb.Append(cateId);
                    hadWriteFilterCategoryIdList = true;
                }
                sb.Append(')');
            }
            return sb.ToString();
        }


        #endregion


        #region 分割URL取得各Id
        /// <summary>
        /// 分割Url取得CityId
        /// </summary>
        /// <returns></returns>
        private int GetCityIdFromUri()
        {
            string path = this.PponUri.AbsolutePath.ToLower();
            string parameters = this.PponUri.Query;
            int cityId = CategoryManager.pponCityId.Taipei;

                //ex. 199/{Guid} 或 199/94 
                Match matchGuid = Regex.Match(path, "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                Match matchChar = Regex.Match(path, "[a-z]+", RegexOptions.IgnoreCase | RegexOptions.Compiled);

            if (matchGuid.Success || !matchChar.Success) //"含Guid" 或 "不含字母"
            {
                //ex. /199 、 /199/94 、 /356/95 、 ...
                var splitArray = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                if (splitArray.Length > 0)
                {
                    int.TryParse(splitArray[0], out cityId);
                }
            }
            else if (path.Contains("channel") && !path.Contains(","))
            {
                int channelId = 0;
                Match match = Regex.Match(parameters, @"cid=([^&]*)", RegexOptions.IgnoreCase | RegexOptions.Compiled);

                if (match.Success)
                {
                    string key = match.Groups[1].Value;
                    int.TryParse(key, out cityId);
                }
                else
                {
                    var splitArray = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                    if (splitArray.Length > 1)
                    {
                        Int32.TryParse(splitArray[1], out channelId);
                        cityId = CategoryManager.GetCityIdByChannelId(channelId);
                    }
                }
            }
            else
            {
                Match match = Regex.Match(parameters, @"cid=([^&]*)", RegexOptions.IgnoreCase | RegexOptions.Compiled);

                if (match.Success)
                {
                    string key = match.Groups[1].Value;
                    int.TryParse(key, out cityId);
                }
            }            

            return (cityId == 0) ? CategoryManager.pponCityId.Taipei : cityId;
        }

        /// <summary>
        /// 分割Url取得ChannelId
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private int GetChannelIdByCityId()
        {
            int channelId = 0;

            channelId = CategoryManager.GetChannelIdByCityId(this.CityId);

            return channelId;
        }

        /// <summary>
        /// 分割Url取得AreaId
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private int GetAreaIdFromUri()
        {
            string path = this.PponUri.AbsolutePath;
            int areaId = 0;

            if (!string.IsNullOrWhiteSpace(path) && !path.Contains("ppon/default.aspx"))
            {
                Match matchChar = Regex.Match(path, "[a-z]+", RegexOptions.IgnoreCase | RegexOptions.Compiled);//不含英文字元

                if (!matchChar.Success)
                {
                    var splitArray = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    if (splitArray.Length > 1)
                    {
                        //ex. /199/94 、 /356/95 、 ...
                        int.TryParse(splitArray[1], out areaId);
                    }
                }
                else
                {
                    var splitArray = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    if (splitArray.Length > 1)
                    {
                        if (splitArray[1].Contains(",") && splitArray[0] == "channel")
                        {
                            var splitComma = splitArray[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                            if (splitComma.Length > 1)
                            {
                                Int32.TryParse(splitComma[1], out areaId);
                            }
                        }
                    }
                }
            }

            if (areaId == 0)
            {
                string queryPath = this.PponUri.Query;
                if (string.IsNullOrEmpty(queryPath))
                {
                    areaId = CategoryManager.GetDefaultRegionIdByChannelId(this.ChannelId);
                }
                else
                {
                    string param = HttpUtility.ParseQueryString(queryPath).Get("aid");
                    if (!string.IsNullOrEmpty(param))
                    {
                        Int32.TryParse(param, out areaId);
                    }
                    else
                    {
                        areaId = CategoryManager.GetDefaultRegionIdByChannelId(this.ChannelId);

                    }
                }
            }
            return areaId;
        }

        /// <summary>
        /// 分割Url中問號後面的字串取得SubcategoryId
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private int GetSubcategoryIdFromUri()
        {
            string parameters = this.PponUri.Query;
            int subcategoryId = 0;

            if (parameters != null)
            {
                Match match = Regex.Match(parameters, @"subc=([^&]*)", RegexOptions.IgnoreCase | RegexOptions.Compiled);

                if (match.Success)
                {
                    string key = match.Groups[1].Value;
                    int.TryParse(key, out subcategoryId);
                }
            }

            //如果沒有選子分類，只選到區域，categoryId = regionId
            subcategoryId = (subcategoryId != 0) ? subcategoryId : this.AreaId;

            return subcategoryId;
        }

        /// <summary>
        /// 分割Url取得新版分類Id
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private int GetNewDealCategoryIdFromUri()
        {
            string path = this.PponUri.AbsolutePath;
            int dealCategoryId = 0;
            
            if (!string.IsNullOrWhiteSpace(path) && !path.Contains("ppon/default.aspx"))
            {
                //ex. /Ppon/Default.aspx 或 199/{Guid} 或 199/94 
                Match matchChar = Regex.Match(path, "[a-z]+", RegexOptions.IgnoreCase | RegexOptions.Compiled);

                if (!matchChar.Success)
                {
                    //ex.  /199/94/801 、 /356/95/801 、 ...
                    var splitArray = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    if (splitArray.Length > 2)
                    {
                        int.TryParse(splitArray[2], out dealCategoryId);
                    }
                }
                else
                {
                    var splitArray = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    //ex.  /channel/199,94,801 、 /channel/356,95,801 、 ...(DeepLink)
                    if (splitArray.Length > 1)
                    {
                        if (splitArray[1].Contains(","))
                        {
                            var splitComma = splitArray[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                            if (splitComma.Length > 2)
                            {
                                Int32.TryParse(splitComma[2], out dealCategoryId);
                            }
                        }
                    }
                }
            }

            return dealCategoryId;
        }


        public static List<int> GetFilterCategoryIdListFromSession(int channel)
        {
            string sessKey = string.Format("DefaultPageFilter-{0}", channel);
            List<int> data = new List<int>();
            if (HttpContext.Current.Session[sessKey] != null)
            {
                try
                {
                    data = HttpContext.Current.Session[sessKey].ToString().Split(',')
                        .Select(t => Convert.ToInt32(t)).ToList();
                }
                catch
                {
                    HttpContext.Current.Session.Remove(sessKey);
                }
            }
            //早期的過瀘條件
            if (data.Contains(138))
            {
                data.Remove(138);
            }
            return data;
        }

        public static void SetFilterCategoryIdListFromSession(int channel, string id, bool isChecked)
        {
            string key = string.Format("DefaultPageFilter-{0}", channel);
            string filterStr = HttpContext.Current.Session[key] == null
                ? string.Empty : HttpContext.Current.Session[key].ToString();
            List<string> filterIds = filterStr.Split(new char[] { ',' }, 
                StringSplitOptions.RemoveEmptyEntries).ToList();
            if (isChecked)
            {
                if (filterIds.Contains(id) == false)
                {
                    filterIds.Add(id);
                }
            } else
            {
                if (filterIds.Contains(id))
                {
                    filterIds.Remove(id);
                }
            }
            HttpContext.Current.Session[key] = string.Join(",", filterIds.ToArray());
        }

        public static void SetFilterCategoryIdListFromSession(int channel, string filterStr)
        {
            string key = string.Format("DefaultPageFilter-{0}", channel);
            List<string> filterIds = filterStr.Split(new char[] { ',' }, 
                StringSplitOptions.RemoveEmptyEntries).ToList();

            if (filterIds.Count > 0)
            {
                HttpContext.Current.Session[key] = string.Join(",", filterIds.ToArray());
            }
        }
        
        /// <summary>
        /// 分割URL中問號後面的字串取得特色檔次Id (「即買急用」或「最後一天」等 (checkbox的分類))
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private List<int> GetFilterCategoryIdListFromUri()
        {
            string parameters = this.PponUri.Query;
            string path = this.PponUri.AbsolutePath.ToLower();
            List<int> filterIds = new List<int>();

            if (path.Contains("channel") && !path.Contains(","))
            {
                int channelId = 0;
                var splitArray = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                if (splitArray.Length > 1)
                {
                    Int32.TryParse(splitArray[1], out channelId);
                }

                //即買即用
                if(channelId==(int)ChannelCategory.Immediately)
                {
                    filterIds.Add(137);
                }
            }

            if (parameters != null)
            {
                Match match = Regex.Match(parameters, @"filter=([^&]*)", RegexOptions.IgnoreCase | RegexOptions.Compiled);

                if (match.Success)
                {
                    string key = match.Groups[1].Value;
                    foreach (var item in key.Split(","))
                    {
                        int i;
                        if (int.TryParse(item, out i))
                        {
                            filterIds.Add(i);
                        }
                    }
                }
            }

            return filterIds;
        }
        #endregion
    }
}

﻿using LunchKingSite.BizLogic.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Model
{
    /// <summary>
    /// 自訂Model 非SubSonic產生，用來長分類樹用
    /// </summary>
    [Serializable]
    public class CategoryTreeNode
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region Constructor

        /// <summary>
        /// 空建構子
        /// </summary>
        public CategoryTreeNode()
        {
        }

        /// <summary>
        /// 節點建構子
        /// </summary>
        /// <param name="categoryId">分類ID</param>
        /// <param name="depth">節點深度(從0開始)</param>
        /// <param name="parentChannelId">父節點的channelId</param>
        public CategoryTreeNode(int categoryId, int depth, CategoryTreeNode parentNode, HxRootCategory hxRoot)
        {
            if (categoryId > 0)
            {
                Category category = CategoryManager.GetCategoryByCategoryId(categoryId);

                if (category != null)
                {
                    #region 節點屬性

                    #region [dbo].[category]欄位
                    this.CategoryId = categoryId;
                    this.Name = category.Name;
                    this.Rank = category.Rank;
                    this.Code = category.Code;
                    this.Type = category.Type;
                    this.Status = category.Status;
                    this.IsFinal = category.IsFinal;
                    this.NameInConsole = category.NameInConsole;
                    this.IsShowFrontEnd = category.IsShowFrontEnd;
                    this.IconType = category.IconType;
                    #endregion

                    #region [dbo].[category_region_display_rule]欄位

                    CategoryRegionDisplayRule displayRule = CategoryManager.GetRegionDisplayRuleByCid(categoryId);
                    if (displayRule != null)
                    {
                        this.SpecialRuleFlag = displayRule.SpecialRuleFlag;
                        this.CategoryDisplayName = displayRule.CategoryDisplayName;
                        this.SummaryItemName = displayRule.SummaryItemName;
                        this.SummaryItemCssClass = displayRule.SummaryItemCssClass;
                        this.SubListCssClass = displayRule.SubListCssClass;
                    }

                    #endregion

                    this.Depth = depth;

                    #region 有可能從父節點繼承的Property
                    int pRegionId = 0, pChannelId = 0, pCityId = 0;
                    string pRegionName = null;
                    if (parentNode != null)
                    {
                        pRegionId = parentNode.RegionId;
                        pChannelId = parentNode.ChannelId;
                        pCityId = parentNode.CityId;
                        pRegionName = parentNode.RegionName;
                    }

                    if (pRegionId == 0 && this.Depth == 1)
                    {
                        this.RegionId = this.CategoryId;
                        this.RegionName = this.Name;
                    }
                    else
                    {
                        this.RegionId = pRegionId;
                        this.RegionName = pRegionName;
                    }
                    this.ChannelId = (pChannelId == 0 && category.Type == (int)CategoryType.PponChannel) ? this.CategoryId : pChannelId;
                    this.CityId = (pCityId == 0) ? CategoryManager.GetCityIdByChannelIdAndCategoryId(this.ChannelId, this.CategoryId) : pCityId;
                    #endregion

                    this.DealCount = hxRoot.GetDeals(this.ChannelId, this.RegionId, new List<int>(), this.CategoryId).Count;

                    #region 上下層關係
		            this.Parent = new CategoryTreeNode();
                    this.Children = new List<CategoryTreeNode>();
	                #endregion
                    
                    #endregion


                    #region 節點屬性-加入子節點
                    List<ViewCategoryDependency> dependencies = CategoryManager.GetCategoryDependencyInCacheByPid(categoryId);
                    if (dependencies != null && dependencies.Count() > 0)
                    {
                        dependencies = dependencies.Where(x => x != null && x.CategoryId > 0)
                                                   .OrderBy(x => x.Seq).ToList();
                        foreach (var dependency in dependencies)
                        {
                            this.Children.Add(new CategoryTreeNode(dependency.CategoryId, depth + 1, this, hxRoot));
                        }
                    }
                    #endregion
                }
            }
        }

        #endregion Constructor

        #region Property

        #region Category欄位
        /// <summary>
        /// 分類id (PK)
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// 分類名稱
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 分類排序
        /// </summary>
        public int? Rank { get; set; }

        /// <summary>
        /// 分類代碼
        /// </summary>
        public int? Code { get; set; }

        /// <summary>
        /// 分類類型
        /// </summary>
        public int? Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 是否為最終節點
        /// </summary>
        public bool IsFinal { get; set; }

        /// <summary>
        /// Console顯示名稱
        /// </summary>
        public string NameInConsole { get; set; }

        /// <summary>
        /// 是否在前端顯示
        /// </summary>
        public bool IsShowFrontEnd { get; set; }

        /// <summary>
        /// 圖示類型
        /// </summary>
        public int IconType { get; set; }

        #endregion

        #region CategoryRegionDisplayRule欄位

        /// <summary>
        /// 節點特殊處理的規則flag
        /// </summary>
        public int SpecialRuleFlag { get; set; }

        /// <summary>
        /// 節點要顯示的新名稱
        /// </summary>
        public string CategoryDisplayName { get; set; }

        /// <summary>
        /// 節點底下的總和項目名稱
        /// </summary>
        public string SummaryItemName { get; set; }

        /// <summary>
        /// 定義總和項目css的class
        /// </summary>
        public string SummaryItemCssClass { get; set; }

        /// <summary>
        /// 定義子節點清單css的class  (導覽列的<ul>或最終節點的<ul>樣式)
        /// </summary>
        public string SubListCssClass { get; set; }

        #endregion

        #region 自訂Property
        /// <summary>
        /// 地區的Id (UrlRoute用)
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// depth = 1 的categoryId  (UrlRoute用)
        /// </summary>
        public int RegionId { get; set; }

        /// <summary>
        /// depth = 1 的categoryName  (前端顯示用)
        /// </summary>
        public string RegionName { get; set; }

        /// <summary>
        /// 頻道Id (計算檔次用)
        /// </summary>
        public int ChannelId { get; set; }

        /// <summary>
        /// 節點深度 (由0開始)
        /// </summary>
        public int Depth { get; set; }

        /// <summary>
        /// 該category_id的檔次數
        /// </summary>
        public int DealCount { get; set; }

        /// <summary>
        /// 父節點
        /// </summary>
        public CategoryTreeNode Parent { get; set; }

        /// <summary>
        /// 子節點集合
        /// </summary>
        public List<CategoryTreeNode> Children { get; set; }
        #endregion

        #endregion Property

        #region Method

        /// <summary>
        /// 取得檔次數非零的子節點
        /// </summary>
        /// <returns></returns>
        public List<CategoryTreeNode> GetNonzeroDealCountChildNodes()
        {
            List<CategoryTreeNode> list = new List<CategoryTreeNode>();

            var conf = ProviderFactory.Instance().GetConfig();
            bool showEmptyDealCategory = conf.ShowEmptyDealCategory;//是否要顯示沒有檔次的分類

            if (this.Children != null)
            {
                if (showEmptyDealCategory || this.Depth == 0)
                {
                    //Depth = 0表示是頻道分類，不用過濾DealCount = 0的分類 (換句話說：頻道底下的地區分類，無論有無檔次皆要顯示)
                    list = this.Children;
                }
                else
                {
                    list = this.Children.Where(x => x.DealCount > 0).ToList();
                }
            }

            return list;
        }

        public string GetRoute(bool isSKM=false)
        {
            if (isSKM)
            {
                string route = string.Format("{0}/ppon/skm.aspx?cityId={1}&areaId={2}", config.SiteUrl, this.CityId, this.RegionId);

                return route;

            }
            else
            {
                string route = String.Format("{0}/channel/{3}?cid={1}&aid={2}", config.SiteUrl, this.CityId, this.RegionId, this.ChannelId);

                if (this.Depth > 1 && this.CategoryId != this.RegionId)
                {
                    route += "&subc=" + this.CategoryId;
                }

                return route;

            }
        }

        /// <summary>
        /// [加總項目] 有設CategoryDisplayRuleFlag.DisplaySummaryItem的情況下，要顯示的名稱
        /// </summary>
        /// <returns></returns>
        public string GetSummaryItemNameByRuleFlag()
        {
            //Priority:
            //SummaryName > CategoryDisplayName > Name

            string name = "";
            if (Helper.IsFlagSet(this.SpecialRuleFlag, CategoryDisplayRuleFlag.ReNameSummaryItem))
            {
                name = this.SummaryItemName;
            }
            else if (Helper.IsFlagSet(this.SpecialRuleFlag, CategoryDisplayRuleFlag.ShowDisplayName))
            {
                name = this.CategoryDisplayName;
            }
            else
            {
                name = this.Name;
            }
            return name;
        }

        /// <summary>
        /// [項目] 要顯示的名稱
        /// </summary>
        /// <returns></returns>
        public string GetCategoryNameByRuleFlag()
        {
            string name = "";
            
            if (Helper.IsFlagSet(this.SpecialRuleFlag, CategoryDisplayRuleFlag.ShowDisplayName))
            {
                name = this.CategoryDisplayName;
            }
            else
            {
                name = this.Name;
            }
            return name;
        }

        #endregion Method

    }
}

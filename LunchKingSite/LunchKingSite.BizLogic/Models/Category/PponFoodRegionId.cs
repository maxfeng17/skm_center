﻿using LunchKingSite.BizLogic.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    /// <summary>
    /// 美食頻道各Region的Id (模擬列舉)
    /// </summary>
    /// <remarks>
    /// fix categoryId HardCode的問題
    /// 因為不同DB category.id有可能不同 (流水號)，所以必須用category.code去撈id
    /// </remarks>
    public class PponFoodRegionId
    {
        public PponFoodRegionId()
        {
            this.Taipei = CategoryManager.GetCategoryIdByCategoryCode(6001);
            this.Taoyuan = CategoryManager.GetCategoryIdByCategoryCode(6002);
            this.Hsinchu = CategoryManager.GetCategoryIdByCategoryCode(6003);
            this.Taichung = CategoryManager.GetCategoryIdByCategoryCode(6004);
            this.Tainan = CategoryManager.GetCategoryIdByCategoryCode(6005);
            this.Kaohsiung = CategoryManager.GetCategoryIdByCategoryCode(6006);
        }

        //category:台北       //city:台北市
        public int Taipei { get; set; }

        //category:桃園       //city:桃園市
        public int Taoyuan { get; set; }

        //category:竹苗       //city:新竹市  
        public int Hsinchu { get; set; }

        //category:中彰       //city:台中市
        public int Taichung { get; set; }

        //category:嘉南       //city:台南市
        public int Tainan { get; set; }

        //category:高屏       //city:高雄市  
        public int Kaohsiung { get; set; }

    }
    
}

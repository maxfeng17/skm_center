﻿using LunchKingSite.BizLogic.Component;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    /// <summary>
    /// 各Channel的Id (模擬列舉)
    /// </summary>
    /// <remarks>
    /// fix ChannelId HardCode的問題
    /// 因為不同DB category.id有可能不同 (流水號)，所以必須用category.code去撈id
    /// </remarks>
    public class PponChannelId
    {
        public PponChannelId()
        {
            this.Food = CategoryManager.GetCategoryIdByCategoryCode(5001);
            this.Delivery = CategoryManager.GetCategoryIdByCategoryCode(5002);
            this.Beauty = CategoryManager.GetCategoryIdByCategoryCode(5003);
            this.Travel = CategoryManager.GetCategoryIdByCategoryCode(5004);
            this.DepositCoffee = CategoryManager.GetCategoryIdByCategoryCode(5014);
            this.FamilyMart = CategoryManager.GetCategoryIdByCategoryCode(5005);
            this.Tmall = CategoryManager.GetCategoryIdByCategoryCode(5007);
            this.PiinLife = CategoryManager.GetCategoryIdByCategoryCode(5008);
            this.Skm = CategoryManager.GetCategoryIdByCategoryCode(5009);
            this.Immediately = CategoryManager.GetCategoryIdByCategoryCode(5013);
        }

        [Description("美食")]
        public int Food { get; private set; }
        
        [Description("宅配")]
        public int Delivery { get; private set; }
        
        [Description("玩美‧休閒")]
        public int Beauty { get; private set; }
        
        [Description("旅遊")]
        public int Travel { get; private set; }

        [Description("咖啡寄杯")]
        public int DepositCoffee { get; private set; }

        [Description("全家專區")]
        public int FamilyMart { get; private set; }
        
        [Description("天貓")]
        public int Tmall { get; private set; }
        
        [Description("品生活")]
        public int PiinLife { get; private set; }
        
        [Description("新光三越")]
        public int Skm { get; private set; }

        [Description("即買即用")]
        public int Immediately { get; private set; }

    }

    #region 原本的列舉
    ///// <summary>
    ///// CategoryType = 4的category_id   (HardCode正式站)
    ///// </summary>
    //public enum CategoryChannel
    //{
    //    [Description("美食")]
    //    Food = 87,
    //    [Description("宅配")]
    //    Delivery = 88,
    //    [Description("玩美‧休閒")]
    //    Beauty = 89,
    //    [Description("旅遊")]
    //    Travel = 90,
    //    [Description("全家專區")]
    //    FamilyMart = 91,
    //    [Description("天貓")]
    //    Tmall = 93,
    //    [Description("品生活")]
    //    PiinLife = 148,
    //    [Description("新光三越")]
    //    Skm = 185

    //    //???? = 92	    福利網
    //    //???? = 2160	熟客17來
    //    //???? = 2161	優惠券
    //}	 
	#endregion
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Models
{
    public class CentralCategory
    {
        public CentralCategory(int channelId, int categoryId, string categoryName, CategoryType categoryType, int seq)
        {
            ChannelId = channelId;
            CategoryId = categoryId;
            CategoryName = categoryName;
            CategoryType = (int)categoryType;
            Sequence = seq;
        }

        public int ChannelId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int CategoryType { get; set; }
        public int Sequence { get; set; }
    }
}

﻿using LunchKingSite.BizLogic.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    /// <summary>
    /// 各City的Id (模擬列舉)
    /// </summary>
    /// <remarks>
    /// fix CityId HardCode的問題
    /// 因為不同DB city.id有可能不同 (流水號)，所以必須用city.code去撈id
    /// </remarks>
    public class PponCityId
    {
        public PponCityId()
        {
            this.Tainan = CityManager.GetHeadCityByCode("TN");
            this.Kaohsiung = CityManager.GetHeadCityByCode("GX");
            this.Taipei = CityManager.GetHeadCityByCode("TP");
            this.Hsinchu = CityManager.GetHeadCityByCode("XZ");
            this.Taoyuan = CityManager.GetHeadCityByCode("TY");
            this.Taichung = CityManager.GetHeadCityByCode("TC");
            this.Beauty = CityManager.GetHeadCityByCode("PBU");
            this.Travel = CityManager.GetHeadCityByCode("TRA");
            this.Delivery = CityManager.GetHeadCityByCode("ALL");
            this.FamilyMart = CityManager.GetHeadCityByCode("FAM");
            this.Hour72 = CityManager.GetHeadCityByCode("A72");
            this.PiinLife = CityManager.GetHeadCityByCode("PIN");
            this.Skm = CityManager.GetHeadCityByCode("SKM");
        }

        //city:台南市       //category:嘉南
        public int Tainan { get; private set; }
        //city:高雄市       //category:高屏
        public int Kaohsiung { get; set; }
        //city:台北市       //category:台北
        public int Taipei { get; set; }
        //city:新竹市       //category:竹苗
        public int Hsinchu { get; set; }
        //city:桃園市       //category:桃園
        public int Taoyuan { get; set; }
        //city:台中市       //category:中彰
        public int Taichung { get; set; }
        //city:玩美女人     //category:玩美‧休閒
        public int Beauty { get; set; }
        //city:旅遊渡假     //category:旅遊
        public int Travel { get; set; }
        //city:全國宅配     //category:宅配
        public int Delivery { get; set; }
        //city:咖啡寄杯     //category:咖啡寄杯
        public int DepositCoffee { get; set; }
        //city:全家專區     //category:全家
        public int FamilyMart { get; set; }
        //city:72H到貨      //category:72H到貨  //※例外:同宅配
        public int Hour72 { get; set; }
        //city:品生活       //category:品生活
        public int PiinLife { get; set; }
        //city:新光三越     //category:新光三越
        public int Skm { get; set; }
    }

    #region 原本的列舉
    ///// <summary>
    ///// 有使用在分類選單上的city_id   (HardCode正式站)
    ///// </summary>
    //public enum PponCategoryCity
    //{
    //    [Description("台南市")]//嘉南
    //    Tainan = 116,
    //    [Description("高雄市")]//高屏
    //    Kaohsiung = 155,
    //    [Description("台北市")]//台北
    //    Taipei = 199,
    //    [Description("新竹市")]//竹苗
    //    Hsinchu = 338,
    //    [Description("桃園市")]//桃園
    //    Taoyuan = 356,
    //    [Description("台中市")]//中彰
    //    Taichung = 389,
    //    [Description("玩美女人")]//玩美‧休閒
    //    Beauty = 475,
    //    [Description("旅遊渡假")]//旅遊
    //    Travel = 476,
    //    [Description("全國宅配")]//宅配
    //    Delivery = 477,
    //    [Description("全家專區")]//全家
    //    FamilyMart = 490,
    //    [Description("72H到貨")]//Exception:宅配
    //    Hour72 = 492,
    //    [Description("品生活")]
    //    PiinLife = 494,
    //    [Description("新光三越")]
    //    Skm = 496
    //}
    #endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Models.LineShopping
{
    public class LineProductCategoryForDeal : LineProductData
    {
        public string SubCategory1 { get; set; }

        public LineProductCategoryForDeal(IViewPponDeal deal, string categoryList,
            Dictionary<int, string> lineProductCategoryDic, bool isDiscontinued = true) : base(deal, categoryList,
            isDiscontinued)
        {
            var categoryIds = base.ProductCategoryValue.Split(',');
            SubCategory1 = categoryList.Length > 0 ? lineProductCategoryDic[int.Parse(categoryIds.First())] : "未分類";
        }
    }
}
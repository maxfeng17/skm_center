﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.LineShopping
{
    public class LinePartialUpdateConditionInfo
    {
        public bool IsHiddenInRecommendDeal { get; set; }
        public DateTime ModifyTime { get; set; }
    }
}

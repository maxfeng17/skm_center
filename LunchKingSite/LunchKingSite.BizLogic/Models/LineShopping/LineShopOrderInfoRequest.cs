﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.OrderEntities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LunchKingSite.BizLogic.Models.LineShopping
{
    public class LineShopOrderInfoRequest
    {
        public LineShopOrderInfoRequest(ViewLineshopOrderinfo orderinfo, int vTimestamp, string hash_hamc)
        {

            OrderId = orderinfo.OrderId;
            OrderTime = orderinfo.OrderTime.ToString("yyyy-MM-dd HH:mm:ss");
            OrderTotal = Convert.ToInt32(orderinfo.Ordertotal);
            OrderList = new List<LineShopOrderListItem>();
            Ecid = orderinfo.Ecid;
            Timestamp = vTimestamp;
            Hash = hash_hamc;           
        }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        ///訂單成立時間 
        /// </summary>
        public string OrderTime { get; set; }
        /// <summary>
        /// 訂單交易總額
        /// </summary>
        public int OrderTotal { get; set; }
        /// <summary>
        /// 訂單明細
        /// </summary>
        public List<LineShopOrderListItem> OrderList { get; set; }
        /// <summary>
        /// 用戶識別碼
        /// </summary>
        public string Ecid { get; set; }
        /// <summary>
        /// Post時間
        /// </summary>
        public int Timestamp { get; set; }
        /// <summary>
        /// 驗證碼
        /// </summary>
        public string Hash { get; set; }

    }

    public class LineShopOrderListItem
    {
        [JsonProperty("product")]
        public LineShopOrderListProduct Product { get; set; }
    }

    public class LineShopOrderListProduct
    {
        public LineShopOrderListProduct(LineShopOrderListInfo orderD)
        {
            //查分類
            var getCategoryInfo = PponFacade.GetLineProductCategoryByBid(orderD.Bid);

            ProductName = HttpUtility.UrlEncode(orderD.ItemName);
            ProductType = getCategoryInfo.ProductType;
            ProductId = orderD.Bid;
            //單品總額=amount-(discount+bcash)
            ProductAmount = orderD.OrderAmount.ToString();
            SubCategory1 = getCategoryInfo.SubCategory1 ?? "未分類";
            SubCategory2 = "未分類";
        }
        /// <summary>
        /// 商品名稱
        /// </summary>
        [JsonProperty("product_name")]
        public string ProductName { get; set; }
        /// <summary>
        /// 一般商品(normal)或3C商品(3c)
        /// </summary>
        [JsonProperty("product_type")]
        public string ProductType { get; set; }
        /// <summary>
        /// 商品編號(Bid)
        /// </summary>
        [JsonProperty("product_id")]
        public Guid ProductId { get; set; }
        /// <summary>
        /// 同一Bid付款金額
        /// </summary>
        [JsonProperty("product_amount")]
        public string ProductAmount { get; set; }
        /// <summary>
        /// 主要分類
        /// </summary>
        [JsonProperty("sub_category1")]
        public string SubCategory1 { get; set; }
        /// <summary>
        /// 次要分類
        /// </summary>
        [JsonProperty("sub_category2")]
        public string SubCategory2 { get; set; }
    }
}
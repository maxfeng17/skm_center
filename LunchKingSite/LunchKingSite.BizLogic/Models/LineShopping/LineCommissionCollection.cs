﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.LineShopping
{
    public class LineCommissionCollection
    {
        public LineCommissionCollection()
        {
            SellerWhiteGuidList = new List<Guid>();
            MainDealWhiteGuidList = new List<Guid>();
            SellerBlackGuidList = new List<Guid>();
            MainDealBlackGuidList = new List<Guid>();
        }
        /// <summary>
        /// 商家白名單
        /// </summary>
        public List<Guid> SellerWhiteGuidList { get; set; }
        /// <summary>
        /// 母檔白名單
        /// </summary>
        public List<Guid> MainDealWhiteGuidList { get; set; }
        /// <summary>
        /// 商家黑名單
        /// </summary>
        public List<Guid> SellerBlackGuidList { get; set; }
        /// <summary>
        /// 母檔黑名單
        /// </summary>
        public List<Guid> MainDealBlackGuidList { get; set; }
    }
}

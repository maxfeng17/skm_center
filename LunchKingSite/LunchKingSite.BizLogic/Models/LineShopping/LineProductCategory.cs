﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.LineShopping
{
    public class LineProductCategory
    {
        public LineProductCategory(string categoryName,int categoryId,int categoryValueParent,bool existsDeal)
        {
            CategoryTitle = categoryName;
            CategoryValue = categoryId.ToString();
            CategoryValueParent = categoryValueParent.ToString();
            CategoryFlag = existsDeal ? 1 : 0;
        }
        //商品分類目錄之名稱
        //required
        [JsonProperty("category_title")]
        public string CategoryTitle { get; set; }
        //分類目錄編號(最長50characters)
        //唯一值
        //required
        [JsonProperty("category_value")]
        public string CategoryValue { get; set; }
        //目錄母層編號(最長50characters)
        //若屬於第一層商品分類，數值填0
        //required
        [JsonProperty("category_value_parent")]
        public string CategoryValueParent { get; set; }
        //該目錄，上架=1，下架=0
        [JsonProperty("category_flag")]
        public int CategoryFlag { get; set; }

    }
}

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Models.OrderEntities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LunchKingSite.BizLogic.Models.LineShopping
{
    public class LineShopOrderFinishRequest
    {
        public LineShopOrderFinishRequest(ViewLineshopOrderfinish orderfinish, int vFeetotal,int vTimestamp, string hash_hamc)
        {
            OrderId = orderfinish.OrderId;
            OrderTime = orderfinish.OrderTime.ToString("yyyy-MM-dd HH:mm:ss");
            FeeList = new List <LineShopFeeListItem>();
            FeeTotal = vFeetotal;
            FeeTime = orderfinish.FeeTime.ToString("yyyy-MM-dd HH:mm:ss");
            Ecid = orderfinish.Ecid;
            Timestamp = vTimestamp;
            Hash = hash_hamc;
        }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 訂單成立時間
        /// </summary>
        public string OrderTime { get; set; }
        /// <summary>
        /// 認列明細
        /// </summary>
        public List<LineShopFeeListItem> FeeList { get; set; }
        /// <summary>
        /// 認列付款總額
        /// </summary>
        public int FeeTotal { get; set; }
        /// <summary>
        /// 銷售認列時間
        /// </summary>
        public string FeeTime { get; set; }
        /// <summary>
        /// 用戶識別碼
        /// </summary>
        public string Ecid { get; set; }
        /// <summary>
        /// Post時間
        /// </summary>
        public int Timestamp { get; set; }
        /// <summary>
        /// 驗證碼
        /// </summary>
        public string Hash { get; set; }
    }
    public class LineShopFeeListItem 
    {
        [JsonProperty("product")]
        public LineShopFeeListProduct Product { get; set; }
    }

    public class LineShopFeeListProduct
    {
        public LineShopFeeListProduct(string productName, Guid bid, int productFee)
        {
            //查分類
            var getCategoryInfo = PponFacade.GetLineProductCategoryByBid(bid);

            ProductName = HttpUtility.UrlEncode(productName);
            ProductType = getCategoryInfo.ProductType;
            ProductId = bid;
            //單品總額=amount-(discount+bcash)
            ProductFee = productFee.ToString();
            SubCategory1 = getCategoryInfo.SubCategory1 ?? "未分類";
            SubCategory2 = "未分類";
        }
        /// <summary>
        /// 商品名稱
        /// </summary>
        [JsonProperty("product_name")]
        public string ProductName { get; set; }
        /// <summary>
        /// 一般商品(normal)或3C商品(3c)
        /// </summary>
        [JsonProperty("product_type")]
        public string ProductType { get; set; }
        /// <summary>
        /// 商品編號(Bid)
        /// </summary>
        [JsonProperty("product_id")]
        public Guid ProductId { get; set; }
        /// <summary>
        /// 同一Bid付款金額(已核銷)
        /// </summary>
        [JsonProperty("product_fee")]
        public string ProductFee { get; set; }
        /// <summary>
        /// 主要分類
        /// </summary>
        [JsonProperty("sub_category1")]
        public string SubCategory1 { get; set; }
        /// <summary>
        /// 次要分類
        /// </summary>
        [JsonProperty("sub_category2")]
        public string SubCategory2 { get; set; }
    }
}

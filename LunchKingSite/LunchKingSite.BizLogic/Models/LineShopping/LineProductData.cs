﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Models.LineShopping
{
    public class LineProductData
    {
        private static ISysConfProvider config;

        public LineProductData()
        {
        }
        public LineProductData(IViewPponDeal deal, string categoryList, bool isDiscontinued)
        {
            config = ProviderFactory.Instance().GetConfig();
            var mainBid = deal.MainBid ?? deal.BusinessHourGuid;
            //商品編號
            ProductId = mainBid.ToString();
            //商品名稱
            ProductName = deal.ItemName == null ? deal.ItemName : Regex.Replace(deal.ItemName, "\\p{Cs}", "");
            //商品簡要說明或特色
            Description = deal.EventTitle == null ? deal.EventTitle : Regex.Replace(deal.EventTitle, "\\p{Cs}", "");
            //商品詳細說明
            LDescription = string.Empty;
            //LandingPage
            Link = string.Format("{0}/deal/{1}", config.SSLSiteUrl, mainBid.ToString());
            //商品主圖網址
            ImageLink = string.IsNullOrEmpty(deal.AppDealPic)
                ? PponFacade.GetPponDealFirstImagePath(deal.EventImagePath) 
                : PponFacade.GetPponDealFirstImagePath(deal.AppDealPic);
            //more pictures
            AdditionalImageLink = string.Empty;
            //供應情況
            Availability = !isDiscontinued && deal.OrderedQuantity < deal.OrderTotalLimit ? "in stock" : "discontinued";
            //商品價格(原價)
            Price = (int)deal.ItemOrigPrice;
            //商品分類
            ProductType = deal.CategoryIds.Contains(117) ? "3c" : "normal";
            //目錄編號
            ProductCategoryValue = categoryList;
            //商品折扣價
            SalePrice = (int)deal.ItemPrice;
            //是否為成人商品
            AgeGroup = "normal";
        }
        //商品編號
        //required
        [JsonProperty("product_id")]
        public string ProductId { get; set; }
        //商品名稱
        //required
        [JsonProperty("product_name")]
        public string ProductName { get; set; }
        //商品簡要說明或特色(不允許html,換行符號,tab)
        //required
        [JsonProperty("description")]
        public string Description { get; set; }
        //商品詳細說明(允許使用html格式)
        [JsonProperty("l_description")]
        public string LDescription { get; set; }
        //Landingpage
        //required
        //https
        //特殊字元需經urlEndcode編碼
        [JsonProperty("link")]
        public string Link { get; set; }
        //商品主圖網址
        //required
        [JsonProperty("image_link")]
        public string ImageLink { get; set; }
        //更多圖，最多可有十張，使用逗點分隔
        [JsonProperty("additional_image_link")]
        public string AdditionalImageLink { get; set; }
        //供應狀況
        //預設狀況，discontinued的商品不會被搜尋到，也不會被投遞廣告
        //instock:可立即出貨，preorder:預定，discontinued:商品下架
        //required
        [JsonProperty("availability")]
        public string Availability { get; set; }
        //商品價格(原價)
        //required
        [JsonProperty("price")]
        public int Price { get; set; }
        //商品分類
        //僅填兩種類型(normal/3c)
        //required
        [JsonProperty("product_type")]
        public string ProductType { get; set; }
        //商品對應到目錄更新檔之分類目錄編號
        //最長接受50characters
        //required
        [JsonProperty("product_category_value")]
        public string ProductCategoryValue { get; set; }
        //商品折扣價
        [JsonProperty("sale_price")]
        public int? SalePrice { get; set; }
        //是否為成人商品
        //required
        [JsonProperty("age_group")]
        public string AgeGroup { get; set; }

    }
}

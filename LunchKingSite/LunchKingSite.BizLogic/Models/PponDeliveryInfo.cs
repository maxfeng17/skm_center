﻿using System;
using System.Web;
using System.Web.SessionState;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{
    [Serializable]
    public class ThirdPartyParameters
    {
        public string PayCode { get; set; }
        public bool PayFailure { get; set; }

        public string LineTransactionId { get; set; }
        public bool IsLineCancel { get; set; }
    }
    [Serializable]
    public class PponDeliveryInfo
    {
        /// <summary>
        /// 購買人UserId (通常為登入者Id，但訪客購買時為例外)
        /// </summary>
        public int BuyerUserId { get; set; }
        /// <summary>
        /// 收件地址
        /// </summary>
        public string WriteAddress { get; set; }
        /// <summary>
        /// 收件地址郵遞區號
        /// </summary>
        public string WriteZipCode { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public decimal Freight { get; set; }
        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string WriteName { get; set; }
        /// <summary>
        /// 手機號碼
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 付款方式 (app固定傳1，此欄位應不可拿來做判斷，另新訂義MainPaymentType讓app傳入真正支付方式)
        /// </summary>
        public Core.PaymentType PayType { get; set; }
        /// <summary>
        /// 數量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 紅利點數
        /// </summary>
        public int BonusPoints { get; set; }
        /// <summary>
        /// PayEasy購物金
        /// </summary>
        public int PayEasyCash { get; set; }
        /// <summary>
        /// 17life購物金
        /// </summary>
        public decimal SCash { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        public string Notes { get; set; }
        /// <summary>
        /// 購買商品類別
        /// </summary>
        public DepartmentTypes SellerType { get; set; }
        /// <summary>
        /// 買給朋友
        /// </summary>
        public bool IsForFriend { get; set; }
        /// <summary>
        /// 朋友名稱
        /// </summary>
        public string FriendName { get; set; }
        /// <summary>
        /// 朋友的電子郵件地址
        /// </summary>
        public string FriendEMail { get; set; }
        /// <summary>
        /// 買給朋友時的文字
        /// </summary>
        public string ToFriendNote { get; set; }
        /// <summary>
        /// 送貨日期描述
        /// </summary>
        public string DeliveryDateString { get; set; }
        /// <summary>
        /// 送貨時間描述
        /// </summary>
        public string DeliveryTimeString { get; set; }
        /// <summary>
        /// 買給朋友時登記的屬名
        /// </summary>
        public string Genus { get; set; }
        /// <summary>
        /// 收件地址如果選用MemberDelivery資料的話，紀錄其ID，若為-1表示不使用
        /// </summary>
        public int MemberDeliveryID { get; set; }
        /// <summary>
        /// 寄件地址若不選用MemberDelivery，則須選取Building
        /// </summary>
        public Guid WriteBuildingGuid { get; set; }
        /// <summary>
        /// 配合Building內容的地址資料，Building記錄只到巷，其餘部分的Address紀錄於此
        /// </summary>
        public string WriteShortAddress { get; set; }
        /// <summary>
        /// 判斷消費方式
        /// </summary>
        public DeliveryType DeliveryType { get; set; }

        public int DealAccBusinessGroupId { get; set; }
        public int RootDealType { get; set; }

        public DonationReceiptsType? ReceiptsType { get; set; }

        /// <summary>
        /// 載具類型
        /// </summary>
        public CarrierType CarrierType { get; set; }
        /// <summary>
        /// 載具號碼
        /// </summary>
        public string CarrierId { get; set; }
        /// <summary>
        /// 愛心碼(捐贈發票)
        /// </summary>
        public string LoveCode { get; set; }
        /// <summary>
        /// 發票版本(自 2014/03/01 起加入發票載具及愛心碼資訊)
        /// </summary>
        public InvoiceVersion Version { get; set; }

        /// <summary>
        /// 發票類型，例如:二聯式或三聯式
        /// </summary>
        public string InvoiceType { get; set; }

        /// <summary>
        /// 三聯式發票儲存
        /// </summary>
        public bool IsInvoiceSave { get; set; }

        /// <summary>
        /// 三聯式發票儲存ID
        /// </summary>
        public string InvoiceSaveId { get; set; }

        /// <summary>
        /// 公司抬頭
        /// </summary>
        public string CompanyTitle { get; set; }
        /// <summary>
        /// 統一編號
        /// </summary>
        public string UnifiedSerialNumber { get; set; }
        /// <summary>
        /// 接受電子發票 (true 表示電子 false表示紙本)
        /// </summary>
        public bool IsEinvoice { get; set; }
        /// <summary>
        /// 買受人姓名
        /// </summary>
        public string InvoiceBuyerName { get; set; }
        /// <summary>
        /// 買受人地址
        /// </summary>
        public string InvoiceBuyerAddress { get; set; }

        public Guid StoreGuid { get; set; }
        /// <summary>
        /// DiscountCode編號
        /// </summary>
        public string DiscountCode { get; set; }

        /// <summary>
        /// 代收轉付
        /// </summary>
        public EntrustSellType EntrustSell { get; set; }

        public OrderInstallment CreditCardInstallment { get; set; }

        public bool IsApplePay { get; set; }

        /// <summary>
        /// 取貨方式
        /// </summary>
        public ProductDeliveryType ProductDeliveryType { get; set; }
        /// <summary>
        /// 超取店舖資訊
        /// </summary>
        public PickupStore PickupStore { get; set; }


        #region web 用

        /// <summary>
        /// 如果有值，即有使用折價券，且已確定折抵的金額。
        /// </summary>
        public int DiscountAmount { get; set; }
        /// <summary>
        /// 指扣掉紅利折抵、購物金折抵、折價券等，還需要支付的金額
        /// </summary>
        public int PendingAmount { get; set; }
        /// <summary>
        /// 訂單總額
        /// </summary>
        public int TotalAmount { get; set; }

        public BuyTransPaymentMethod PaymentMethod { get; set; }

        #endregion
    }

    public enum PponPaymentDTOState
    {
        Init = 0,
        Loaded = 1,
        Paying = 2,
        Paid = 3,
        OTP = 4
    }

    public class PponPaymentDTO
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public PponPaymentDTOState State { get; set; }

        /// <summary>
        /// Ticket ID
        /// </summary>
        public string TicketId { get; set; }

        /// <summary>
        /// Transsaction ID
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// order guid
        /// </summary>
        public Guid OrderGuid { get; set; }

        /// <summary>
        /// 預建訂單
        /// </summary>
        public Guid? PrebuiltOrderGuid { get; set; }

        /// <summary>
        /// Session ID
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// payeasy member id
        /// </summary>
        public string PezId { get; set; }

        /// <summary>
        /// bussiness hour guid
        /// </summary>
        public Guid BusinessHourGuid { get; set; }

        /// <summary>
        /// delivery info
        /// </summary>
        public PponDeliveryInfo DeliveryInfo { get; set; }
        /// <summary>
        /// delivery charge
        /// </summary>
        public int DeliveryCharge { get; set; }

        #region 付款資料

        public PponCreditCardInfo CreditCardInfo { get; set; }

        public CreditCardInfo MasterPassInfo { get; set; }

        public string ATMAccount { get; set; }
        public bool IsSaveCreditCardInfo { get; set; }
        public BuyAddressInfo BuyerAddressInfo { get; set; }
        public bool IsVisaDiscountCode { get; set; }
        public bool IsThirdPartyPay { get; set; }

        public ThirdPartyPayment ThirdPartyPaymentSystem { get; set; }

        public ThirdPartyParameters ThirdPartyParameters { get; set; }

        #endregion

        /// <summary>
        /// item options, specified format is
        /// "OptionName|^|OptionGuid,OptionName|^|OptionGuid|#|ItemQuantity||OptionName|^|OptionGuid|#|ItemQuantity", just like
        /// "L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|2||L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|3"
        /// </summary>
        public string ItemOptions { get; set; }
        /// <summary>
        /// 原是訪客購買時，記憶使用者選了哪些選項及其份數的json資料。
        /// 現在記一份到server端，將來導頁後導回可以重現那些選項。
        /// </summary>
        public string ItemOptionsJson { get; set; }
        /// <summary>
        /// 原是訪客購買時，記憶購買者資訊。
        /// 現在記一份到server端，將來導頁後導回可以重現那些選項。
        /// </summary>
        public string BuyerInfoJson { get; set; }
        /// <summary>
        /// 記憶畫面上的付款金額(bcash/scash/discount)、購買方式、發票
        /// </summary>
        public string OtherInfoJson { get; set; }

        ///// <summary>
        ///// 使用 MasterPass
        ///// </summary>
        //public bool IsMasterPass { set; get; }
        ///// <summary>
        ///// MasterPass Data
        ///// </summary>
        //public MasterPassTrans MasterPassTransData { set; get; }




        public void CopyMasterPassInfoToCreditCardInfo()
        {
            if (this.MasterPassInfo == null)
            {
                throw new Exception("CopyMasterPassInfoToCreditCardInfo fail, because MasterPassInfo is null");
            }
            if (this.CreditCardInfo == null)
            {
                this.CreditCardInfo = new PponCreditCardInfo();
            }
            this.CreditCardInfo.ExpireYear = this.MasterPassInfo.ExpiryYear.Substring(2, 2);
            this.CreditCardInfo.ExpireMonth = this.MasterPassInfo.ExpiryMonth.PadLeft(2, '0');
            this.CreditCardInfo.CardNumber = this.MasterPassInfo.CardNumber;
            this.CreditCardInfo.SecurityCode = string.Empty;
            this.CreditCardInfo.TransactionId = this.TransactionId;
            this.CreditCardInfo.Amount = this.DeliveryInfo.PendingAmount;

            //測試時才會用到，正式環境下不需要後3碼，但測試環境需要，於是填測試卡號後3碼
            if (config.IsMasterPassLocalTest && this.CreditCardInfo.CardNumber == "8888880000000001")
            {
                this.CreditCardInfo.SecurityCode = config.DefaultTestCreditcardSecurityCode;
            }
        }

        public PaymentDTO ConvertToPaymentDTO()
        {
            PaymentDTO paymentDTO = new PaymentDTO();
            SessionIDManager sessionManager = new SessionIDManager();
            paymentDTO.SessionId = sessionManager.CreateSessionID(HttpContext.Current);
            paymentDTO.BusinessHourGuid = this.BusinessHourGuid;
            paymentDTO.SelectedStoreGuid = this.DeliveryInfo.StoreGuid;
            paymentDTO.Quantity = this.DeliveryInfo.Quantity;
            paymentDTO.DeliveryInfo = this.DeliveryInfo;
            paymentDTO.BCash = this.DeliveryInfo.BonusPoints;
            paymentDTO.SCash = (int)this.DeliveryInfo.SCash;
            paymentDTO.PCash = 0;
            paymentDTO.DiscountCode = this.DeliveryInfo.DiscountCode;
            paymentDTO.AddressInfo = this.BuyerAddressInfo.ToFlatString();
            paymentDTO.ReferenceId = string.Empty;
            paymentDTO.PezId = this.PezId;
            paymentDTO.IsPaidByATM = this.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.Atm;
            paymentDTO.ItemOptions = this.ItemOptions;
            paymentDTO.PrebuiltOrderGuid = this.PrebuiltOrderGuid;
            paymentDTO.Amount = this.DeliveryInfo.PendingAmount;

            if (this.CreditCardInfo != null && this.CreditCardInfo.IsValid)
            {
                paymentDTO.CreditcardNumber = this.CreditCardInfo.CardNumber;
                paymentDTO.CreditcardExpireYear = this.CreditCardInfo.ExpireYear;
                paymentDTO.CreditcardExpireMonth = this.CreditCardInfo.ExpireMonth;
                paymentDTO.CreditcardSecurityCode = this.CreditCardInfo.SecurityCode;
            }

            return paymentDTO;
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", this.State, this.TicketId);
        }
    }

    public class PponPaymentInfo
    {
        public BuyTransPaymentMethod PaymentMethod { get; set; }
        /// <summary>
        /// 指扣掉紅利折抵、購物金折抵、折價券等，還需要支付的金額
        /// </summary>
        public int PendingAmount { get; set; }
        /// <summary>
        /// 訂單總額
        /// </summary>
        public int TotalAmount { get; set; }
        /// <summary>
        /// DiscountCode編號
        /// </summary>
        public string DiscountCode { get; set; }

        public PponCreditCardInfo CreditCardInfo { get; set; }

        public CreditCardInfo MasterPassInfo { get; set; }

        public string ATMAccount { get; set; }
        public bool IsVisaDiscountCode { get; set; }
        public bool IsApplePay { get; set; }
        public string ApplePayTransId { get; set; }
        public bool IsThirdPartyPay { get; set; }

        public ThirdPartyPayment ThirdPartyPaymentSystem { get; set; }

        public ThirdPartyParameters ThirdPartyParameters { get; set; }
    }

    public enum CheckCreditCardAuthStatus
    {
        Success,
        AlreadyPaid,
        AlreadyFail,
        Fail,
        /// <summary>
        /// 掛失卡
        /// </summary>
        PickupCard,
    }

    public class PponCreditCardInfo : CreditCardAuthObject
    {
        public Guid CreditCardGuid { get; set; }

        public bool IsValid
        {
            get
            {
                if (this.CardNumber == null || this.CardNumber.Length != 16)
                {
                    return false;
                }
                if (this.ExpireYear == null || this.ExpireYear.Length != 2)
                {
                    return false;
                }
                if (this.ExpireMonth == null || this.ExpireMonth.Length != 2)
                {
                    return false;
                }
                return true;
            }
        }
    }

    /// <summary>    
    /// 購買人跟收件人的相關資料
    /// </summary>
    [Serializable]
    public class BuyAddressInfo
    {
        /// <summary>
        /// 姓 LastName
        /// </summary>
        public string FamilyName { get; set; }
        /// <summary>
        /// 名 FirsName
        /// </summary>
        public string GivenName { get; set; }
        /// <summary>
        /// 通訊手機
        /// </summary>
        public string BuyerMobile { get; set; }
        /// <summary>
        /// 通訊Email
        /// </summary>
        public string UserEmail { get; set; }
        public int BuyerCityId { get; set; }
        public int BuyerAreaId { get; set; }
        public Guid BuyerBuildingGuid { get; set; }
        public string BuyerAddress { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverMobile { get; set; }
        public int ReceiverCityId { get; set; }
        public int ReceiverAreaId { get; set; }
        public Guid ReceiverBuildingGuid { get; set; }
        public string ReceiverAddress { get; set; }
        public bool AddReceiverInfo { get; set; }

        public string ToFlatString()
        {
            return string.Format("{0}|{1}|{2}|{3}^{4}|{5}|{6}|{7}",
                FamilyName,
                GivenName,
                BuyerMobile,
                (string.IsNullOrWhiteSpace(BuyerAddress)
                    ? "0|0|" + Guid.Empty + "|"
                    : BuyerCityId + "|" + BuyerAreaId + "|" + Guid.Empty + "|" + BuyerAddress),
                ReceiverName,
                ReceiverMobile,
                (string.IsNullOrWhiteSpace(ReceiverAddress)
                    ? "0|0|" + Guid.Empty + "|"
                    : ReceiverCityId + "|" + ReceiverAreaId + "|" + Guid.Empty + "|" + ReceiverAddress),
                AddReceiverInfo);
        }

        //"孔|雀魚|0939913269|294|308|61ac238e-d0b2-4897-80ea-8dc79962e2d7|中山北路一段11號13樓^子彈飛|0939913269|294|308|61ac238e-d0b2-4897-80ea-8dc79962e2d7|長安東路一段16號7樓|True"
        public static BuyAddressInfo Parse(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }
            string[] segments = str.Split(new char[] { '^' }, StringSplitOptions.RemoveEmptyEntries);

            var result = new BuyAddressInfo();


            if (segments.Length > 0)
            {
                string[] parts = segments[0].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length < 7)
                {
                    return null;
                }
                result.FamilyName = parts[0];
                result.GivenName = parts[1];
                result.BuyerMobile = parts[2];
                result.BuyerCityId = int.Parse(parts[3]);
                result.BuyerAreaId = int.Parse(parts[4]);
                result.BuyerBuildingGuid = Guid.Empty;
                result.BuyerAddress = parts[6];
            }

            if (segments.Length > 1)
            {
                string[] parts = segments[1].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length < 7)
                {
                    return result;
                }
                result.ReceiverName = parts[0];
                result.ReceiverMobile = parts[1];
                result.ReceiverCityId = int.Parse(parts[2]);
                result.ReceiverAreaId = int.Parse(parts[3]);
                result.ReceiverBuildingGuid = Guid.Empty;
                result.ReceiverAddress = parts[5];
                result.AddReceiverInfo = bool.Parse(parts[6]);
            }
            return result;
        }
    }
}

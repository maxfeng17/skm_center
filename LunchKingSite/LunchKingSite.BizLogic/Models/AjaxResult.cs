﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class AjaxResult
    {
        public AjaxResult()
        {
        }

        public AjaxResult(bool success, string message, object value = null)
        {
            this.Success = success;
            this.Message = message;
            this.Value = value;
        }

        public bool Success { get; set; }

        public string Message { get; set; }

        public object Value { get; set; }

    }
}

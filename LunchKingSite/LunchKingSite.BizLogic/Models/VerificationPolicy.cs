﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Model
{
	/// <summary>
	/// 核銷的商業規則
	/// </summary>
	public class VerificationPolicy
	{
		private static ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
		private static IHiDealProvider _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
		private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISellerProvider _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IAccountingProvider _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();



		/// <summary>
		/// 
		/// </summary>
		/// <param name="bizModel"></param>
		/// <param name="merchandiseGuid">好康: bid; 品生活 [hi_deal_product].[guid]</param>
		/// <param name="storeGuid"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public bool IsInVerifyTimeRange(BusinessModel bizModel, Guid merchandiseGuid, Guid? storeGuid, DateTime time, bool checkNextGenTime = false)
		{
			ExpirationDateSelector expirationDateSelector = new ExpirationDateSelector();
			
			switch(bizModel)
			{
				case BusinessModel.Ppon:
					expirationDateSelector.ExpirationDates = PponUsageExpiration.PponUsageExpirationCreator(merchandiseGuid, storeGuid);
					break;
				case BusinessModel.PiinLife:
					expirationDateSelector.ExpirationDates = HiDealUsageExpiration.HiDealUsageExpirationCreator(merchandiseGuid, storeGuid);
					break;
				default:
					throw new InvalidOperationException();
			}

            // 沒設定 "使用截止日期" 的檔次: 無意義的檔次 (品生活會發生)
            // ?? 使用開始日期 < 檔次開賣日期: 無意義的好康檔次 - 需要判斷嗎??
		    DateTime upperLimit = expirationDateSelector.GetInternalExpirationDate() == DateTime.MaxValue
		                              ? DateTime.MinValue
                                      : expirationDateSelector.GetInternalExpirationDate().AddDays(_config.GenToHouseBalanceSheetBuffer);

            var accountInfo = _pp.DealAccountingGet(merchandiseGuid);
            if (accountInfo == null)
            {
                return false;
            }

            //抓取下次產出對帳單時間
            var nextGenTime = time;
            if (checkNextGenTime)
            {
                switch (accountInfo.RemittanceType)
                {
                    case (int)RemittanceType.Weekly:
                    case (int)RemittanceType.AchWeekly:
                    case (int)RemittanceType.ManualWeekly:
                        nextGenTime = time.AddDays(7);
                        break;

                    case (int)RemittanceType.Monthly:
                    case (int)RemittanceType.Flexible:
                    case (int)RemittanceType.ManualMonthly:
                        nextGenTime = time.AddMonths(1).GetFirstDayOfMonth();
                        break;
                }
            }

            return time < upperLimit
                && nextGenTime < upperLimit
                && time > expirationDateSelector.ExpirationDates.DealUseStartDate;
		}

		/// <summary>
		/// 抓出核銷中還需要產對帳單的品生活商品
		/// (參數 frequency 為週對帳單 => 抓出週結的品生活商品; 參數 frequency 為月對帳單 => 抓出月結的品生活商品)
		/// </summary>
		/// <param name="generationTime"></param>
		/// <param name="genFrequency">對帳單的產生頻率, 只有 WeekBalanceSheet 或 MonthBalanceSheet 有意義.</param>
		/// <param name="payFrequency">匯款頻率, 只有 Weekly 或 Monthly 有意義</param>
		/// <returns></returns>
		public IEnumerable<HiDealProduct> GetHiDealVerifyingProductsForBillingSystem(DateTime generationTime, BalanceSheetGenerationFrequency genFrequency, RemittanceFrequency payFrequency)
		{
			if (genFrequency != BalanceSheetGenerationFrequency.WeekBalanceSheet
				&& genFrequency != BalanceSheetGenerationFrequency.MonthBalanceSheet)
            {
				return new List<HiDealProduct>();
            }

			DateTime verificationTime = new DateTime(generationTime.Year, generationTime.Month, generationTime.Day);
			int endBuffer = CalculateEndBuffer(generationTime, genFrequency);

            if (genFrequency == BalanceSheetGenerationFrequency.MonthBalanceSheet
                && payFrequency == RemittanceFrequency.Weekly)
            {
                endBuffer += 7;
            }

			IEnumerable<ViewHiDealExpiration> allProducts = _hp.ViewHiDealExpirationGetList(verificationTime, endBuffer);

			IEnumerable<ViewHiDealExpiration> filteredProducts = FilterByFrequency(allProducts, payFrequency);
            IEnumerable<int> productIds = filteredProducts.Select(x => x.ProductId).Distinct();
			HiDealProductCollection results = _hp.HiDealProductGetList(productIds);
			
			return results;
		}
		
		/// <summary>
		/// 抓出核銷中還需要產對帳單的好康檔次. 
		/// (參數 frequency 為週對帳單 => 抓出週結的好康檔次; 
		/// 參數 frequency 為月對帳單 => 抓出月結的好康檔次;
        /// 參數 frequency 為一次性付款對帳單(商品對帳單) => 抓出 其他付款方式 或 部分暫付 的好康檔次)
		/// </summary>
		/// <param name="generationTime"></param>
		/// <param name="genFrequency">對帳單的產生頻率, 只有 WeekBalanceSheet or MonthBalanceSheet or LumpSumBalanceSheet or FlexibleBalanceSheet 有意義.</param>
        /// <param name="payFrequency">匯款頻率, 只有 Weekly 或 Monthly or LumpSum or Flexible 有意義</param>
        /// <param name="outOfVerificationTimeRange">產出週期是否於核銷區間內,補產擋款週結月對帳單方為true</param>
        /// <param name="deliveryType">檔次類型:憑證or宅配商品</param>
		/// <returns></returns>
        public IEnumerable<BusinessHour> GetPponVerifyingDealsForVendorBillingSystem(DateTime generationTime, BalanceSheetGenerationFrequency genFrequency, RemittanceFrequency payFrequency, 
            bool outOfVerificationTimeRange, DeliveryType deliveryType)
		{
            if (genFrequency != BalanceSheetGenerationFrequency.WeekBalanceSheet && 
				genFrequency != BalanceSheetGenerationFrequency.MonthBalanceSheet &&
                genFrequency != BalanceSheetGenerationFrequency.LumpSumBalanceSheet &&
                genFrequency != BalanceSheetGenerationFrequency.FlexibleBalanceSheet &&
                genFrequency != BalanceSheetGenerationFrequency.FortnightlyBalanceSheet)
            {
				return new List<BusinessHour>();
            }
		    List<Guid> bids;
		    if (!outOfVerificationTimeRange)
            { 
			    DateTime verificationTime = new DateTime(generationTime.Year, generationTime.Month, generationTime.Day);
			    int endBuffer = CalculateEndBuffer(generationTime, genFrequency);
                if (genFrequency == BalanceSheetGenerationFrequency.MonthBalanceSheet && 
                    payFrequency == RemittanceFrequency.Weekly && payFrequency == RemittanceFrequency.Fortnightly)
                {
                    endBuffer += 7;
                }

                var allDeals = deliveryType == DeliveryType.ToHouse
                                ? payFrequency == RemittanceFrequency.LumpSum
                                    ? _pp.ViewPponExpirationGetList(verificationTime)
                                    : _pp.ViewPponExpirationGetList(verificationTime, _config.ShippingBuffer, endBuffer)
                                : _pp.ViewPponExpirationGetList(verificationTime, endBuffer);

                IEnumerable<ViewPponExpiration> filteredDeals = FilterByFrequency(allDeals, payFrequency, genFrequency);
                bids = filteredDeals.Select(x => x.Bid).Distinct().ToList();
            }
            else
            {
                var genTime = generationTime.AddMonths(-1);
                bids = _ap.GetWeekBalanceSheetHasNoMonthBalanceSheet(genTime.Year, genTime.Month)
                            .Select(x=>x.ProductGuid)
                            .ToList();
            }
			BusinessHourCollection result = _sp.BusinessHourGetList(bids);

            return result.Where(bh => !((BusinessHourStatus)bh.BusinessHourStatus).HasFlag(BusinessHourStatus.ComboDealMain)).ToList();
		}



        private IEnumerable<ViewPponExpiration> FilterByFrequency(IEnumerable<ViewPponExpiration> allDeals, RemittanceFrequency frequency, BalanceSheetGenerationFrequency genFrequency)
		{
			switch (frequency)
			{
				case RemittanceFrequency.Weekly:
                    //產出週結月對帳單須排除無須產出月對帳單之新週結對帳單 
			        if (genFrequency == BalanceSheetGenerationFrequency.MonthBalanceSheet)
			        {
			            return allDeals.Where(x =>
			                                  x.VendorBillingModel == (int) VendorBillingModel.BalanceSheetSystem &&
			                                 (x.RemittanceType == (int) RemittanceType.AchWeekly ||
			                                  x.RemittanceType == (int) RemittanceType.ManualWeekly));
			        }
					return allDeals.Where(x =>
                                          x.VendorBillingModel == (int)VendorBillingModel.BalanceSheetSystem &&
                                          (x.RemittanceType == (int)RemittanceType.AchWeekly ||
                                           x.RemittanceType == (int)RemittanceType.ManualWeekly ||
                                           x.RemittanceType == (int)RemittanceType.Weekly));
                case RemittanceFrequency.Fortnightly:
                    return allDeals.Where(x =>
                                          x.VendorBillingModel == (int)VendorBillingModel.BalanceSheetSystem &&
                                          (x.RemittanceType == (int)RemittanceType.Fortnightly));
                case RemittanceFrequency.Monthly:
			        return allDeals.Where(x =>
			                              x.VendorBillingModel == (int) VendorBillingModel.BalanceSheetSystem &&
			                              (x.RemittanceType == (int)RemittanceType.ManualMonthly ||
                                           x.RemittanceType == (int)RemittanceType.Monthly));
                case RemittanceFrequency.LumpSum:
			        return allDeals.Where(x =>
			                              x.VendorBillingModel == (int) VendorBillingModel.BalanceSheetSystem &&
			                             (x.RemittanceType == (int) RemittanceType.ManualPartially ||
                                          x.RemittanceType == (int)RemittanceType.Others));
                case RemittanceFrequency.Flexible:
                    return allDeals.Where(x =>
                                          x.VendorBillingModel == (int)VendorBillingModel.BalanceSheetSystem &&
                                          x.RemittanceType == (int)RemittanceType.Flexible);
				default:
					return allDeals;
			}
		}

		private IEnumerable<ViewHiDealExpiration> FilterByFrequency(IEnumerable<ViewHiDealExpiration> allProducts, RemittanceFrequency frequency)
		{
			switch (frequency)
			{
				case RemittanceFrequency.Weekly:
					return allProducts.Where(x =>
                                             x.VendorBillingModel == (int)VendorBillingModel.BalanceSheetSystem &&
                                            (x.RemittanceType == (int)RemittanceType.AchWeekly || 
										     x.RemittanceType == (int)RemittanceType.ManualWeekly));
				case RemittanceFrequency.Monthly:
					return allProducts.Where(x =>
                                             x.VendorBillingModel == (int)VendorBillingModel.BalanceSheetSystem &&
                                            (x.RemittanceType == (int)RemittanceType.AchMonthly || 
                                             x.RemittanceType == (int)RemittanceType.ManualMonthly));
                case RemittanceFrequency.LumpSum:
                    //品生活不須產商品對帳單
                    return new List<ViewHiDealExpiration>();
				default:
					return allProducts;
			}
		}
		
		private int GetLastSheetBuffer(DateTime generationTime, BalanceSheetGenerationFrequency frequency)
		{
			int result;

			switch (frequency)
			{
				case BalanceSheetGenerationFrequency.WeekBalanceSheet:
					result = 7;
					break;
                case BalanceSheetGenerationFrequency.MonthBalanceSheet:
                case BalanceSheetGenerationFrequency.FlexibleBalanceSheet:
                    result = generationTime.AddMonths(-1).GetCountDaysOfMonth();
                    break;
                case BalanceSheetGenerationFrequency.FortnightlyBalanceSheet:
                    result = 16;
                    break;
                default:
					result = 0;
					break;
			}

			return result;
		}

		private int CalculateEndBuffer(DateTime generationTime, BalanceSheetGenerationFrequency frequency)
		{
			/*
			 * endBuffer = verificationBuffer + lastSheetBuffer
			 * lastSheetBuffer: used for the generation of the last balance sheet
			 * 
			 *                                  generationTime  -------------------------------------------->???????????? 
			 *                                               v                         v                         v                         v                         ?                 
			 * <---------------------|----------------use time-----------------|----verificationBuffer----|-------lastSheetBuffer-------|------>
			 */
			int endBuffer = _config.GenToHouseBalanceSheetBuffer + GetLastSheetBuffer(generationTime, frequency);
			return endBuffer;
		}

		
	}
}

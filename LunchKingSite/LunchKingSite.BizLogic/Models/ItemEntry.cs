﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model
{
    public class ItemEntry
    {
        //private const string _SEPERATOR = "&nbsp";
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int OrderQuantity { get; set; }
        public decimal UnitPrice { get; set; }
        public string Remark { get; set; }
        public Guid StoreGuid { get; set; }

        public List<AccessoryEntry> SelectedAccessory { get; set; }
        public List<ItemEntry> SubItems { get; set; }

        /// <summary>
        /// 回傳考量多選項與成套販售後的名稱
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ToString(" ");
        }


        /// <summary>
        /// 回傳考量多選項與成套販售後的名稱
        /// </summary>
        /// <param name="seperator">指定 ItemName 跟後面 Accessory 的分隔符號</param>
        /// <returns></returns>
        public string ToString(string seperator)
        {
            string opts = GetOptions();
            if (string.IsNullOrEmpty(opts))
            {
                return this.Name;
            }
            else
            {
                return string.Format("{0}{1}{2}", this.Name, seperator, opts);
            }            
        }

        public string GetOptions()
        {
            StringBuilder sb = new StringBuilder();
            if (SubItems.Count > 0)
            {
                List<string> accessoryName = new List<string>();
                foreach (var item in this.SubItems)
                {
                    foreach (var accessory in item.SelectedAccessory)
                    {
                        accessoryName.Add(accessory.Name);
                    }
                }
                foreach (var x in accessoryName.GroupBy(x => x))
                {
                    sb.AppendFormat("({0})*{1}", x.Key, x.Count());
                }
            }
            if (SelectedAccessory.Count > 0)
            {
                foreach (var accessory in this.SelectedAccessory)
                {
                    sb.AppendFormat("({0})", accessory.Name);
                }
                return sb.ToString();
            }
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            ItemEntry castObj = obj as ItemEntry;
            if (castObj == null)
            {
                return false;
            }
            return this.GetHashCode().Equals(castObj.GetHashCode());
        }

        public override int GetHashCode()
        {
            string str = this.ToString();
            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }
            return this.ToString().GetHashCode();
        }

        public ItemEntry()
            : this(Guid.Empty, string.Empty, 0M, 0, null)
        {
            this.SelectedAccessory = new List<AccessoryEntry>();
            this.SubItems = new List<ItemEntry>();
        }

        public ItemEntry(Guid guid, string name, decimal price, int qty, List<AccessoryEntry> accList, string remark, Guid storeGuid)
            : this(guid, name, price, qty, accList)
        {
            Remark = remark;
            StoreGuid = storeGuid;
        }

        public ItemEntry(Guid guid, string name, decimal price, int qty, List<AccessoryEntry> accList)
        {
            this.Id = guid;
            this.Name = name;
            this.UnitPrice = price;
            this.OrderQuantity = qty;
            this.SelectedAccessory = accList;
            this.SubItems = new List<ItemEntry>();
        }
    }

    public class AccessoryEntry
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Guid AccessoryGroupMemberGUID { get; set; }
        private bool _selected;
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
        public AccessoryEntry()
        {
        }

        public AccessoryEntry(string name, Guid accessrotyGroupMemberGuid)
        {
            _name = name;
            AccessoryGroupMemberGUID = accessrotyGroupMemberGuid;
        }
    }
}

﻿using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;


namespace LunchKingSite.BizLogic.Vbs
{
    public class BillRepository
    {
        private static IAccountingProvider _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();


        public static BillEntity BillOfId(int billId)
        {
            BillEntity result = new BillEntity(billId);
            return result;
        }
        
        public static void Save(BillEntity bill, string userName)
        {            
            if (bill.BillId.HasValue)
            {
                Update(bill, userName);
            }
            else
            {
                Insert(bill, userName);
            }
        }


        private static void Insert(BillEntity billEntity, string userName)
        {
            BillInfo info = billEntity.GetModInfo();
            IEnumerable<BillingStmtDistribution> distribution = billEntity.GetModDistribution();
            DateTime now = DateTime.Now;

            Bill bill = new Bill
            {
                BillNumber = info.BillNumber,
                InvoiceDate = info.BillDate.Value,
                InvoiceComId = info.BillingCompanyId,
                BillSum = info.Total,
                BillSumNotaxed = info.Subtotal,
                BillTax = info.Tax,
                Remark = info.Remark,                
                CreateId = userName,
                CreateTime = now, 
                BuyerType = (int)info.BuyerType,
                VendorReceiptType = (int)info.BillType,
                IsInputTaxRequired = info.IsInvoiceTaxRequired,
                BillTypeOtherName = info.BillTypeOtherName
            };

            if (billEntity.CheckIsLumpSumBalanceSheet(distribution.Select(x => x.BalanceSheetId).ToList()))
            {
                bill.FinanceGetDate = now;
            }
                        
            using (TransactionScope scope = new TransactionScope())
            {
                _ap.BillSet(bill);

                BalanceSheetBillRelationshipCollection rels = new BalanceSheetBillRelationshipCollection();
                BalanceSheetWmsBillRelationshipCollection wrels = new BalanceSheetWmsBillRelationshipCollection();
                foreach (var dist in distribution)
                {
                    if (dist.Type == (int)BalanceSheetUseType.Deal)
                    {
                        rels.Add(new BalanceSheetBillRelationship
                        {
                            BalanceSheetId = dist.BalanceSheetId,
                            BillId = bill.Id,
                            BillMoney = dist.Total,
                            BillMoneyNotaxed = dist.Subtotal,
                            BillTax = dist.Tax,
                            Remark = dist.Remark,
                            CreateId = userName,
                            CreateTime = now
                        });
                    }
                    else
                    {
                        wrels.Add(new BalanceSheetWmsBillRelationship
                        {
                            BalanceSheetId = dist.BalanceSheetId,
                            BillId = bill.Id,
                            BillMoney = dist.Total,
                            BillMoneyNotaxed = dist.Subtotal,
                            BillTax = dist.Tax,
                            Remark = dist.Remark,
                            CreateId = userName,
                            CreateTime = now
                        });
                    }

                    
                }

                _ap.BalanceSheetBillRelationshipSetList(rels);
                _ap.BalanceSheetWmsBillRelationshipSetList(wrels);
                SetBillBalanceSheetChangeLog(bill, rels, wrels);
                SetBalanceSheetIsReceiptReceived(rels.Select(x => x.BalanceSheetId));
                scope.Complete();
            }
        }

        private static void Update(BillEntity billEntity, string userName)
        {
            BillInfo info = billEntity.GetModInfo();
            IEnumerable<BillingStmtDistribution> distribution = billEntity.GetModDistribution();
            DateTime now = DateTime.Now;

            Bill bill = _ap.BillGet(billEntity.BillId.Value);
                        
            bill.BillNumber = info.BillNumber;
            bill.InvoiceDate = info.BillDate.Value;
            bill.InvoiceComId = info.BillingCompanyId;
            bill.BillSum = info.Total;
            bill.BillSumNotaxed = info.Subtotal;
            bill.BillTax = info.Tax;
            bill.Remark = info.Remark;
            bill.BuyerType = (int)info.BuyerType;
            bill.ModifyId = userName;
            bill.ModifyTime = now;            

            BalanceSheetBillRelationshipCollection rels = _ap.BalanceSheetBillRelationshipGetListByBillId(billEntity.BillId.Value);
            List<int> dbBsIds = rels.Select(x => x.BalanceSheetId).ToList();

            if (dbBsIds.Any())
            {
                Dictionary<int, BillingStmtDistribution> updDist = distribution
                .Where(x => dbBsIds.Contains(x.BalanceSheetId))
                .ToDictionary(x => x.BalanceSheetId);
                IEnumerable<BillingStmtDistribution> insDist = distribution.Where(x => !dbBsIds.Contains(x.BalanceSheetId));

                if (updDist.Any())
                {
                    foreach (var rel in rels)
                    {
                        BillingStmtDistribution dist = updDist[rel.BalanceSheetId];
                        if (rel.BillMoney != dist.Total
                            || rel.BillMoneyNotaxed != dist.Subtotal
                            || rel.BillTax != dist.Tax
                            || rel.Remark != dist.Remark)
                        {
                            rel.BillMoney = dist.Total;
                            rel.BillMoneyNotaxed = dist.Subtotal;
                            rel.BillTax = dist.Tax;
                            rel.Remark = dist.Remark;
                            rel.ModifyId = userName;
                            rel.ModifyTime = now;
                        }
                    }

                    foreach (var dist in insDist)
                    {
                        rels.Add(new BalanceSheetBillRelationship
                        {
                            BalanceSheetId = dist.BalanceSheetId,
                            BillId = bill.Id,
                            BillMoney = dist.Total,
                            BillMoneyNotaxed = dist.Subtotal,
                            BillTax = dist.Tax,
                            Remark = dist.Remark,
                            CreateId = userName,
                            CreateTime = now
                        });
                    }
                }
                
            }
            

            //wms
            BalanceSheetWmsBillRelationshipCollection wrels = _ap.BalanceSheetWmsBillRelationshipGetListByBillId(billEntity.BillId.Value);
            List<int> dbBswIds = wrels.Select(x => x.BalanceSheetId).ToList();
            if (dbBswIds.Any())
            {
                Dictionary<int, BillingStmtDistribution> updDistw = distribution
                .Where(x => dbBswIds.Contains(x.BalanceSheetId))
                .ToDictionary(x => x.BalanceSheetId);
                IEnumerable<BillingStmtDistribution> insDistw = distribution.Where(x => !dbBswIds.Contains(x.BalanceSheetId));

                if (updDistw.Any())
                {
                    foreach (var rel in wrels)
                    {
                        BillingStmtDistribution dist = updDistw[rel.BalanceSheetId];
                        if (rel.BillMoney != dist.Total
                            || rel.BillMoneyNotaxed != dist.Subtotal
                            || rel.BillTax != dist.Tax
                            || rel.Remark != dist.Remark)
                        {
                            rel.BillMoney = dist.Total;
                            rel.BillMoneyNotaxed = dist.Subtotal;
                            rel.BillTax = dist.Tax;
                            rel.Remark = dist.Remark;
                            rel.ModifyId = userName;
                            rel.ModifyTime = now;
                        }
                    }

                    foreach (var dist in insDistw)
                    {
                        wrels.Add(new BalanceSheetWmsBillRelationship
                        {
                            BalanceSheetId = dist.BalanceSheetId,
                            BillId = bill.Id,
                            BillMoney = dist.Total,
                            BillMoneyNotaxed = dist.Subtotal,
                            BillTax = dist.Tax,
                            Remark = dist.Remark,
                            CreateId = userName,
                            CreateTime = now
                        });
                    }
                }
                
            }
            


            using (TransactionScope scope = new TransactionScope())
            {
                _ap.BillSet(bill);
                _ap.BalanceSheetBillRelationshipSetList(rels);
                _ap.BalanceSheetWmsBillRelationshipSetList(wrels);
                SetBillBalanceSheetChangeLog(bill, rels, wrels);
                SetBalanceSheetIsReceiptReceived(rels.Select(x=>x.BalanceSheetId));
                scope.Complete();
            }
            
        }

        //是否單據已回:用來檢核輸入之單據金額是否滿足對帳單對應之應開立單據金額
        private static void SetBalanceSheetIsReceiptReceived(IEnumerable<int> bsIds)
        {
            var balanceSheets = _ap.BalanceSheetGetListByIds(bsIds.ToList());
            foreach (var bs in balanceSheets)
            {
                bs.IsReceiptReceived = new BalanceSheetModel(bs.Id).DistributedAmountCheck();
                _ap.BalanceSheetSet(bs);
            }
        }

        public static void SetBillBalanceSheetChangeLog(Bill bill, IEnumerable<BalanceSheetBillRelationship> relations, IEnumerable<BalanceSheetWmsBillRelationship> wrelations)
        {
            var logs = new BillBalanceSheetChangeLogCollection();
                
            var log = new BillBalanceSheetChangeLog
            {
                BillId = bill.Id,
                BillNumber = bill.BillNumber,
                InvoiceDate = bill.InvoiceDate,
                InvoiceComId = bill.InvoiceComId,
                BillSum = bill.BillSum,
                BillSumNotaxed = bill.BillSumNotaxed,
                BillSumTax = bill.BillTax,
                BuyerType = bill.BuyerType,
                VendorReceiptType = bill.VendorReceiptType,
                IsInputTaxRequired = bill.IsInputTaxRequired,
                BillTypeOtherName = bill.BillTypeOtherName,
                BillSentDate = bill.BillSentDate,
                FinanceGetDate = bill.FinanceGetDate,
                BillRemark = bill.Remark,
                CreateTime = DateTime.Compare(DateTime.Now.AddSeconds(-2), bill.ModifyTime) > 0 ? bill.CreateTime : bill.ModifyTime,
                CreateId = string.IsNullOrEmpty(bill.ModifyId) ? bill.CreateId : bill.ModifyId
            };

            if (relations != null)
            { 
                foreach (var relation in relations)
                {
                    var relationLog = new BillBalanceSheetChangeLog
                    {
                        BillId = log.BillId,
                        BillNumber = log.BillNumber,
                        InvoiceDate = log.InvoiceDate,
                        InvoiceComId = log.InvoiceComId,
                        BillSum = log.BillSum,
                        BillSumNotaxed = log.BillSumNotaxed,
                        BillSumTax = log.BillSumTax,
                        BuyerType = log.BuyerType,
                        VendorReceiptType = log.VendorReceiptType,
                        IsInputTaxRequired = log.IsInputTaxRequired,
                        BillTypeOtherName = log.BillTypeOtherName,
                        BillSentDate = log.BillSentDate,
                        FinanceGetDate = log.FinanceGetDate,
                        BillRemark = log.BillRemark,
                        BalanceSheetId = relation.BalanceSheetId,
                        BillMoney = relation.BillMoney,
                        BillMoneyNotaxed = relation.BillMoneyNotaxed,
                        BillTax = relation.BillTax,
                        BsRemark = relation.Remark,
                        CreateTime = log.CreateTime,
                        CreateId = log.CreateId
                    };

                    logs.Add(relationLog);
                }
            }

            if (wrelations != null)
            {
                foreach (var wrelation in wrelations)
                {
                    var relationLog = new BillBalanceSheetChangeLog
                    {
                        BillId = log.BillId,
                        BillNumber = log.BillNumber,
                        InvoiceDate = log.InvoiceDate,
                        InvoiceComId = log.InvoiceComId,
                        BillSum = log.BillSum,
                        BillSumNotaxed = log.BillSumNotaxed,
                        BillSumTax = log.BillSumTax,
                        BuyerType = log.BuyerType,
                        VendorReceiptType = log.VendorReceiptType,
                        IsInputTaxRequired = log.IsInputTaxRequired,
                        BillTypeOtherName = log.BillTypeOtherName,
                        BillSentDate = log.BillSentDate,
                        FinanceGetDate = log.FinanceGetDate,
                        BillRemark = log.BillRemark,
                        BalanceSheetId = wrelation.BalanceSheetId,
                        BillMoney = wrelation.BillMoney,
                        BillMoneyNotaxed = wrelation.BillMoneyNotaxed,
                        BillTax = wrelation.BillTax,
                        BsRemark = wrelation.Remark,
                        CreateTime = log.CreateTime,
                        CreateId = log.CreateId
                    };

                    logs.Add(relationLog);
                }
            }

            if (!logs.Any())
            {
                logs.Add(log);
            }

            _ap.BillBanaceSheetChangeLogSetList(logs);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Enumeration;
using System.Web;

namespace LunchKingSite.BizLogic.Models.Game
{
    public class AddCampaignModel
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public int BonusTotal { get; set; }
        public bool EnableBuddyReward { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }


        public bool CheckValid(out string message)
        {
            message = string.Empty;
            if (StartTime == null || EndTime == null)
            {
                message = "資料不完整";
                return false;
            }
            if (StartTime >= EndTime)
            {
                message = "填寫日期有問題";
                return false;
            }
            return true;
        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Enumeration;
using System.Web;

namespace LunchKingSite.BizLogic.Models.Game
{
    public class UpdateCampaignModel
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
        public string Image { get; set; }
        public int BonusTotal { get; set; }
        public bool EnableBuddyReward { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }


        public bool CheckValid(out string message)
        {
            message = string.Empty;
            if (id == 0 || StartTime == null || EndTime == null)
            {
                message = "資料不完整";
                return false;
            }
            if (StartTime >= EndTime)
            {
                message = "填寫日期有問題";
                return false;
            }
            return true;
        }

    }

    public class UpdateActivityModel
    {
        public int ActivityId { get; set; }
        public bool Deleted { get; set; }
        public bool Closed { get; set; }
        public int Sequence { get; set; }
        public List<UpdateActivityGameModel> Games { get; set; }

        public bool CheckValid(out string message)
        {
            message = string.Empty;

            if (ActivityId == 0)
            {
                message = "ActivityId 是必需的";
                return false;
            }
            if (Games == null || Games.Count < 1 || Games.Count > 3)
            {
                message = "遊戲難度的資料筆數不正確，應為1~3筆";
                return false;
            }
            foreach (var game in Games)
            {
                if (game.RuleHours == 0 || game.RulePeople == 0 || game.Maximum == 0)
                {
                    message = "資料錯誤，不預期有值為 0 的資料";
                    return false;
                }
            }
            return true;
        }
    }

    public class UpdateActivityGameModel
    {
        public int GameId { get; set; }
        public GameLevel RuleLevel { get; set; }
        public int RulePeople { get; set; }
        public int RuleHours { get; set; }
        public int Maximum { get; set; }
    }

}

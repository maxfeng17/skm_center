﻿using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Models.Ppon
{
    public class HotTagItem
    {
        public string Keyword { get; set; }
        public string PicUrl { get; set; }
        public string NavigateUrl { get; set; }
    }
    public class HotTagModel
    {
        public List<HotTagItem> Items { get; set; }
    }


    /// <summary>
    /// 熱門搜尋
    /// </summary>
    public class AppHotTag
    {
        /// <summary>
        /// 搜尋關鍵字
        /// </summary>
        public string Keyword { get; set; }
        /// <summary>
        /// 圖片Url
        /// </summary>
        public string PicUrl { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Ppon
{
    public class SolicitEmailBody
    {
        //通用
        public string Address { set; get; }
        public string ContactName { set; get; }
        public string ContactPhoneNo { set; get; }
        public string CallBackTime { set; get; }
        public string Email { set; get; }
        public string SiteUrl { set; get; }

        //公司名稱/團體名稱
        public string CompanyName { set; get; }
        /// <summary>
        /// 統一編號/勸募號
        /// </summary>
        public string CompanyId { set; get; }
        /// <summary>
        /// 品牌
        /// </summary>
        public string BrandName { set; get; }
        /// <summary>
        /// 合作過的網站
        /// </summary>
        public string WorkedSite { set; get; }
        /// <summary>
        /// 合作經驗/網路平台合作經驗
        /// </summary>
        public bool WorkedExperience { set; get; }
        /// <summary>
        /// 曾做過的相關活動
        /// </summary>
        public string WorkedEvent { set; get; }
        /// <summary>
        /// 希望17Life做的事/想要推薦的商品
        /// </summary>
        public string Hope { set; get; }
        /// <summary>
        /// 是否有實體店面
        /// </summary>
        public bool Store { set; get; }
        /// <summary>
        /// 是否連鎖
        /// </summary>
        public bool IsStores { set; get; }
        /// <summary>
        /// 組織型態
        /// </summary>
        public string GroupType { set; get; }
        /// <summary>
        /// 配合方式
        /// </summary>
        public string Solution { set; get; }
        /// <summary>
        /// 電視購物
        /// </summary>
        public string TVBuy { set; get; }

        public SolicitEmailBody(string companyName, string address, string contactName, string contactPhoneNo, string callBackTime, string email)
        {
            CompanyName = companyName;
            Address = address;
            ContactName = contactName;
            ContactPhoneNo = contactPhoneNo;
            CallBackTime = callBackTime;
            Email = email;
        }

        #region Email Utility
        private string GenerateArea(string areaBody)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<fieldset style=\"border: 1px solid gray; width:600px\">");
            sb.AppendLine("<table style=\"font-family:Microsoft JhengHei;\">");
            sb.Append(areaBody);
            sb.AppendLine("</table>");
            sb.AppendLine("</fieldset><br/><br/>");
            return sb.ToString();
        }

        private void TRTDTagString(string title, string content, StringBuilder sb)
        {
            sb.AppendLine(string.Format("<tr><td style=\"padding:15px 8px;\">{0}</td><td>{1}</td></tr>", title, (string.IsNullOrEmpty(content)) ? "N/A" : content));
        }

        private string TRTDTagString(string title, string content)
        {
            return string.Format("<tr><td style=\"padding:15px 8px;\">{0}</td><td>{1}</td></tr>", title, (string.IsNullOrEmpty(content)) ? "N/A" : content);
        }
        #endregion

        #region Business email
        public string GetBusinessMailBody(string hope)
        {
            Hope = hope;
            
            return GenerateArea(GetBusinessNameHtml() + GetContactHtml() + TRTDTagString("希望17life為您做些什麼：", this.Hope));
        }

        private string GetBusinessNameHtml()
        {
            StringBuilder contentSB = new StringBuilder();
            TRTDTagString("提案類型：", "實體商品", contentSB);
            return contentSB.ToString();
        }

        private string GetContactHtml()
        {
            StringBuilder contentSB = new StringBuilder();
            TRTDTagString("店家/公司/團體名稱：", this.CompanyName, contentSB);
            TRTDTagString("地址：", this.Address, contentSB);
            TRTDTagString("聯絡人：", this.ContactName, contentSB);
            TRTDTagString("連絡電話：", this.ContactPhoneNo, contentSB);
            TRTDTagString("合適回覆時間：", this.CallBackTime, contentSB);
            TRTDTagString("E-mail：", this.Email, contentSB);
            return contentSB.ToString();
        }
        #endregion

        #region Market email
        public string GetMarketMailBody(string hope)
        {
            this.Hope = hope;

            return GenerateArea(GetMarketNameHtml() + GetContactHtml() + TRTDTagString("希望17life為您做些什麼：", this.Hope));
        }
        private string GetMarketNameHtml()
        {
            StringBuilder contentSB = new StringBuilder();
            TRTDTagString("提案類型：", "非實體商品", contentSB);
            return contentSB.ToString();
        }

        #endregion

        #region Kind email
        public string GetKindMailBody( string hope)
        {
            this.Hope = hope;

            return GenerateArea(GetKindGroupNameHtml() + GetContactHtml() + TRTDTagString("希望17life為您做些什麼：", this.Hope));
        }

        private string GetKindGroupNameHtml()
        {
            StringBuilder contentSB = new StringBuilder();
            TRTDTagString("提案類型：", "行銷與公益合作", contentSB);
            return contentSB.ToString();
        }
        #endregion
    }
}

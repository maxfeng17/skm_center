﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Ppon
{
    public class HomepageBlockSetupModel
    {

        public Guid guid { get; set; }
        public int type { get; set; }
        public string title { get; set; }
        public object content { get; set; }

        public string contentJson { get; set; }
        public bool enabled { get; set; }
        public int sequence { get; set; }
        public List<HomepageBlockSetupDeal> deals { get; set; }
    }

    public class HomepageBlockSetupDeal
    {
        public string imagePath { get; set; }
        public string title { get; set; }
        public decimal price { get; set; }
        public string bid { get; set; }
    }
}

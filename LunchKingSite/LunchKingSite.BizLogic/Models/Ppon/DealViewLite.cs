﻿using LunchKingSite.BizLogic.Facade;
using System;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Models.Ppon
{
    public class DealViewLite
    {
        public Guid id { get; set; }
        public string title { get; set; }
        public string subTitle { get; set; }
        public string img { get; set; }
        public string imgAlt { get; set; }
        public string sqImg { get; set; }
        public string url { get; set; }
        public bool soldOut { get; set; }
        public string promoImg { get; set; }

        public string salesVolume { get; set; }
        public string origPrice { get; set; }
        public string price { get; set; }
        public string discountPrice { get; set; }
        public string priceTail { get; set; }
        public string discount { get; set; }

        //public List<DealIcon> icons { get; set; }
        public List<DealTag> icons { get; set; }

        public DealStarRating rating { get; set; }

        public bool inCollect { get; set; }

        public List<int> categories { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1}", id, title);
        }
    }

    public class DealStarRating
    {
        public bool show { get; set; }
        public int number { get; set; }
        public decimal score { get; set; }
    }
}

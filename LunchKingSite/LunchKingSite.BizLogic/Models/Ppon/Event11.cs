﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.Ppon
{
    public class Event11
    {
        private static ILog logger = LogManager.GetLogger(typeof(Event11));

        [JsonProperty("start")]
        public DateTime StartTime { get; set; }
        [JsonProperty("end")]
        public DateTime EndTime { get; set; }
        [JsonProperty("m")]
        public string MobileFileName { get; set; }
        [JsonProperty("web")]
        public string WebFileName { get; set; }
        [JsonProperty("data")]
        public string DataSourceFileName { get; set; }

        public static List<Event11> GetDataSource(string baseDir)
        {
            string configPath = Path.Combine(baseDir, "config.json");
            string cacheKey = "Event11.DataSource." + baseDir;
            var result = MemoryCache.Default.Get(cacheKey) as List<Event11>;
            if (result == null)
            {
                if (File.Exists(configPath))
                {
                    try
                    {
                        result = JsonConvert.DeserializeObject<List<Event11>>(File.ReadAllText(configPath));
                        MemoryCache.Default.Set(cacheKey, result, new DateTimeOffset(DateTime.Now.AddMinutes(20)));
                    }
                    catch (Exception ex)
                    {
                        logger.WarnFormat("Event11 GetDataSource fail, configPath={0}, ex={1}", configPath, ex);
                        result = new List<Event11>();
                    }
                }
            }
            return result;
        }
    }
}

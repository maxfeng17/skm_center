﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Ppon
{
    public class BuyPromotionText
    {
        public string Text { get; set; }
        public string Url { get; set; }
    }
}

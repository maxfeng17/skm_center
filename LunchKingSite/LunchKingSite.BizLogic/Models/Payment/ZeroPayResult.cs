﻿using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Model
{
    public class ZeroPayResult
    {
        /// <summary>
        /// WebService Return Code
        /// </summary>
        public ApiReturnCode ReturnCode { set; get; }
        /// <summary>
        /// Web Api Result Code
        /// </summary>
        public ApiResultCode ResultCode { set; get; }
        public string Message { set; get; }
        public ApiFreeDealPayReply Data { set; get; }
        /// <summary> cash_trust_log.trust_id  </summary>
        public IEnumerable<Guid> TrustIds { get; set; }
    }
}

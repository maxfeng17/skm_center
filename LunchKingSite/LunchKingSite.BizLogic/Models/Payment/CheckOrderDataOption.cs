﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Model
{
    public class CheckOrderDataOption
    {
        /// <summary>
        /// 標準檢驗，每單最多訂購量、檔次是否售完、檔次是否過期
        /// </summary>
        public bool CheckStandard { get { return true; } }
        /// <summary>
        /// 每人訂購最多數量檢驗
        /// </summary>
        public bool CheckMaxOrderQty { get; set; }
        /// <summary>
        /// 每人訂購數量是否每日重算
        /// </summary>
        public bool IsDailyRestriction { set; get; }
        /// <summary>
        /// 折價券檢驗
        /// </summary>
        public bool CheckDiscountCode { get; set; }
        /// <summary>
        /// 紅利金檢驗
        /// </summary>
        public bool CheckBonusCash { get; set; }
        /// <summary>
        /// 購物金檢驗
        /// </summary>
        public bool CheckSCash { set; get; }
        /// <summary>
        /// 訂購數量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 折價券編號
        /// </summary>
        public string DiscountCode { set; get; }
        /// <summary>
        /// 紅利金
        /// </summary>
        public int BonusCash { set; get; }
        /// <summary>
        /// 購物金
        /// </summary>
        public decimal SCash { set; get; }
        /// <summary>
        /// 運費
        /// </summary>
        public decimal Freight { set; get; }

        public CheckOrderDataOption()
        {
            Quantity = 1;
            BonusCash = 0;
            SCash = 0;
            Freight = 0;
            DiscountCode = string.Empty;
            CheckMaxOrderQty = IsDailyRestriction = CheckDiscountCode = CheckBonusCash = CheckSCash = false;
        }
    }
}

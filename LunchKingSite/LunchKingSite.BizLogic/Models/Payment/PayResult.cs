﻿using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Models.Payment
{
    public class PayResult
    {
        public PayResult()
        {
            ReturnCode = ApiReturnCode.Ready;
            ResultCode = ApiResultCode.Ready;
            Message = string.Empty;
            Data = null;
        }

        /// <summary>
        /// for WebService PayService.asmx
        /// </summary>
        public ApiReturnCode ReturnCode { set; get; }
        /// <summary>
        /// for WebApi
        /// </summary>
        public ApiResultCode ResultCode { set; get; }
        /// <summary>
        /// Message
        /// </summary>
        public string Message { set; get; }
        /// <summary>
        /// Data
        /// </summary>
        public object Data { get; set; }
    }
}

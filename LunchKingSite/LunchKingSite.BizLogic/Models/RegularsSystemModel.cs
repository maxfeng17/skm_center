﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Model
{
    public class RegularsSystemModel
    {
        public int hidLevel { get; set; }
    }

    public class GetMemberModel
    {
        //test
        public int TotalCount { get; set; }
    }

    public class Members
    {
        //test
        public int? UserId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? SellerMemberId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string SellerMemberLastName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string SellerMemberFirstName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? CardId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public MembershipCardLevel Level { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int OrderCount { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public decimal AmountTotal { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string CardNo { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public GenderType Gender { get; set; }
    }

    public class SellerMemberDetailModel
    {
        public int? UserId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? SellerMemberId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? CardId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? Level { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public float? PaymentPercent { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Instruction { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string OtherPremiums { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public AvailableDateType AvailableDateType { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public int OrderCount { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public decimal AmountTotal { set; get; }
        /// <summary>
        /// 
        /// </summary>
        /// [JsonProperty("time")]
        public string LastUseTime { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string FirstName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string LastName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public GenderType Gender { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Birthday { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Remarks { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string RequestTime { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public string CardNo { set; get; }
    }

    public class MembershipCardQuarterModel
    {
        /// <summary>
        /// 
        /// </summary>
        public MembershipCardQuarterModel()
        {
            IsLoad = false;
        }
        /// <summary>
        /// membership_card_group.id
        /// </summary>
        public int CardGroupId { set; get; }
        /// <summary>
        /// 版本ID membership_card_group_version.id
        /// </summary>
        public int Version { set; get; }
        /// <summary>
        /// 卡片狀態 membership_card.status enum MembershipCardStatus
        /// </summary>
        public int Status { set; get; }
        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName { set; get; }
        /// <summary>
        /// 熟客卡形象圖片
        /// </summary>
        public string SellerImagePath { get; set; }
        /// <summary>
        /// 熟客卡卡片圖片
        /// </summary>
        public string SellerFullCardPath { get; set; }
        /// <summary>
        /// 熟客卡卡片圖片
        /// </summary>
        public string SellerFullCardThumbnailPath { get; set; }
        /// <summary>
        /// 卡片種類
        /// </summary>
        public int SellerCardType { get; set; }
        /// <summary>
        /// 卡片的有效區間(含)
        /// </summary>
        public string VersionOpenTime { set; get; }
        /// <summary>
        /// 卡片的有效區間(含)
        /// </summary>
        public string VersionCloseTime { set; get; }
        /// <summary>
        /// 熟客卡主圖
        /// </summary>
        public string CardImage { set; get; }
        /// <summary>
        /// 熟客卡背景色編號
        /// </summary>
        public int BackgroundColorId { set; get; }
        /// <summary>
        /// 熟客卡背景圖片
        /// </summary>
        public int BackgroundImageId { set; get; }
        /// <summary>
        /// 卡片左上 icon
        /// </summary>
        public int IconImageId { set; get; }
        /// <summary>
        /// 此版本包含的熟客卡
        /// </summary>
        public List<MembershipCardModel> Cards { set; get; }
        /// <summary>
        /// 熟客卡可適用店家數
        /// </summary>
        public int SelectedStoreCount { set; get; }
        /// <summary>
        /// 熟客卡可適用店家資料
        /// </summary>
        public List<OptionStoreModel> Stores { set; get; }
        /// <summary>
        /// 是否有查詢到此筆資料，如果並沒有對應的membership_gruop或membership_group_version則回傳false
        /// </summary>
        public bool IsLoad { get; set; }
        /// <summary>
        /// true表示是否可與店家其他優惠合併使用，預設值為false
        /// </summary>
        public bool CombineUse { get; set; }
        /// <summary>
        /// 卡片適用日期類型
        /// </summary>
        public int AvailableDateType { set; get; }
        /// <summary>
        /// 適用日期中文描述
        /// </summary>
        public string AvailableDateDesc
        {
            get
            {
                return Helper.GetEnumDescription(((AvailableDateType)AvailableDateType));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CardValidDate { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string[] SellerIntro { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageDetail[] SellerServiceImgPath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageDetail[] SellerServiceImgThumbnailPath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageDetail[] SellerSrndgImgPath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageDetail[] SellerSrndgThumbnailPath { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class ImageDetail
    {
        /// <summary>
        /// 
        /// </summary>
        public int Seq { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static explicit operator ImageDetail(ImageDetails c)
        {
            ImageDetail result = new ImageDetail();
            result.Seq = c.Seq;
            result.ImagePath = c.ImagePath;
            return result;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class ImageDetails
    {
        /// <summary>
        /// 
        /// </summary>
        public int Seq { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ImagePath { get; set; }
    }

    public class MembershipCardModel
    {
        /// <summary>
        /// 
        /// </summary>
        public MembershipCardModel() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="card"></param>
        public MembershipCardModel(MembershipCard card)
        {
            CardId = card.Id;
            Level = card.Level;
            PaymentPercent = card.PaymentPercent;
            OtherPremiums = card.Others;
            OrderNeeded = card.OrderNeeded;
            AmountNeeded = (double?)card.AmountNeeded;
            ConditionalLogic = card.ConditionalLogic;
            OpenTime = card.OpenTime.ToString();
            CloseTime = card.CloseTime.ToString();
            Status = (MembershipCardStatus)card.Status;
            ModifyTime = card.ModifyTime.HasValue ? card.ModifyTime.Value.ToString() : card.CreateTime.ToString();
            Instruction = string.IsNullOrWhiteSpace(card.Instruction) ? string.Empty : card.Instruction;
        }
        /// <summary>
        /// membership_card.id
        /// </summary>
        public int? CardId { set; get; }
        /// <summary>
        /// 卡片等級
        /// </summary>
        public int Level { set; get; }
        /// <summary>
        /// 折數
        /// </summary>
        public double PaymentPercent { set; get; }
        /// <summary>
        /// 會員卡加贈
        /// </summary>
        public string OtherPremiums { set; get; }
        /// <summary>
        /// 升等需消費次數
        /// </summary>
        public int? OrderNeeded { set; get; }
        /// <summary>
        /// 升等需銷費金額
        /// </summary>
        public double? AmountNeeded { set; get; }
        /// <summary>
        /// 升等條件邏輯 0:次數與金額擇一達成, 1:金額次數都需滿足條件
        /// </summary>
        public int ConditionalLogic { set; get; }
        /// <summary>
        /// 卡片上架日期
        /// </summary>
        public string OpenTime { set; get; }
        /// <summary>
        /// 卡片下架日期
        /// </summary>
        public string CloseTime { set; get; }
        /// <summary>
        /// 會員卡狀態;int;0：草稿 1：上架 2：作廢
        /// </summary>
        public MembershipCardStatus Status { get; set; }
        /// <summary>
        /// 最後異動時間
        /// </summary>
        public string ModifyTime { get; set; }
        /// <summary>
        /// 限制條件
        /// </summary>
        public string Instruction { set; get; }

    }

    public class OptionStoreModel : StoreModel2
    {
        /// <summary>
        /// 
        /// </summary>
        public OptionStoreModel()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="store"></param>
        /// <param name="selected"></param>
        /// <param name="enabled"></param>
        public OptionStoreModel(Seller store, bool selected, bool enabled) : base(store)
        {
            Selected = selected;
            Enabled = enabled;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Selected { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Enabled { get; set; }
    }

    public class StoreModel2
    {
        /// <summary>
        /// 
        /// </summary>
        public StoreModel2()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="store"></param>
        public StoreModel2(Seller store)
        {
            Guid = store.Guid;
            StoreName = store.SellerName;
            Phone = store.StoreTel;
            Address = (CityManager.CityTownShopStringGet(store.StoreTownshipId == null ? -1 : store.StoreTownshipId.Value) +
                       store.StoreAddress);
            OpenTime = store.OpenTime;
            CloseDate = store.CloseDate;
            Remarks = store.StoreRemark;
            Mrt = store.Mrt;
            Car = store.Car;
            Bus = store.Bus;
            OtherVehicles = store.OtherVehicles;
            WebUrl = store.WebUrl;
            FbUrl = store.FacebookUrl;
            PlurkUrl = string.Empty;
            BlogUrl = store.BlogUrl;
            OtherUrl = store.OtherUrl;
            CreditcardAvailable = store.CreditcardAvailable;
            Coordinates = ApiCoordinates.FromSqlGeography(
                LocationFacade.GetGeographyByCoordinate(store.Coordinate));
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid Guid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OpenTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CloseDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Remarks { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mrt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Car { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Bus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OtherVehicles { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string WebUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FbUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PlurkUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string BlogUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OtherUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool CreditcardAvailable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ApiCoordinates Coordinates { get; set; }
    }

}

    


﻿using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LunchKingSite.BizLogic.Models
{
    public class RegularsBaseViewModel
    {
        private List<VendorRole> _roles { get; set; }
        private List<VbsMembershipPermissionScope> _permissions { get; set; }

        public RegularsBaseViewModel()
        {
            this._roles = (List<VendorRole>)HttpContext.Current.Session[VbsSession.VbsRole.ToString()];
            this._permissions = (List<VbsMembershipPermissionScope>)HttpContext.Current.Session[VbsSession.VbsMembershipPermission.ToString()];
        }

        public bool IsInRole(params VendorRole[] roles)
        {
            return this._roles.Intersect(roles).Count() > 0;
        }

        public bool HasPermission(VbsMembershipPermissionScope scope)
        {
            return _permissions.Contains(scope);
        }
    }
}

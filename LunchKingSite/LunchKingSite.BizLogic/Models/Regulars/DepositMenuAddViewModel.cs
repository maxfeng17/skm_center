﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class DepositMenuAddViewModel : RegularsBaseViewModel
    {
        public List<ViewPcpSellerMenuItem> ItemList { get; set; }

        public List<PcpDepositTemplate> TemplateList { get; set; }
    }
}

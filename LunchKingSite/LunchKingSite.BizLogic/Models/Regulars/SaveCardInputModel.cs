﻿using LunchKingSite.BizLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class SaveCardInputModel
    {
        /// <summary>
        /// 會員卡版本編號
        /// </summary>
        public int VersionId { get; set; }
        /// <summary>
        /// 會員卡列表
        /// </summary>
        public MembershipCardModel Card { get; set; }
        /// <summary>
        /// true表示是否可與店家其他優惠合併使用，預設值為false
        /// </summary>
        public bool CombineUse { get; set; }
        /// <summary>
        /// 卡片適用日期類型
        /// </summary>
        public int AvailableDateType { set; get; }

    }
}

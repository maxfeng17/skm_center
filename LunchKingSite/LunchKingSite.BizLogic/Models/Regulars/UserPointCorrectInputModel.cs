﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.BizLogic.Models
{
    [Bind(Exclude = "SellerGuid, CardGroupId, CreateId")]
    public class UserPointCorrectInputModel
    {
        #region 從後端塞入
        public string SellerGuid { get; set; }
        public int CardGroupId { get; set; }
        public int CreateId { get; set; }
        #endregion

        /// <summary>
        /// 調整的數量(正負數都有可能)
        /// </summary>
        public int Adjust { get; set; }
        public string Description { get; set; }
    }
}

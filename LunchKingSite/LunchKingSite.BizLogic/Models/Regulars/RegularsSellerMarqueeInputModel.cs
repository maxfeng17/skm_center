﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class RegularsSellerMarqueeInputModel
    {
        /// <summary>
        /// 跑馬燈內容與ID
        /// </summary>
        /// <example>["Marquee1###Id1", "Marquee2###Id2"]</example>
        public List<string> Marquees { get; set; }
    }
}

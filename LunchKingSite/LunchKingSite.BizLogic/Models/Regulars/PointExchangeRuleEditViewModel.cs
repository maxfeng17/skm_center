﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class PointExchangeRuleEditViewModel : RegularsBaseViewModel
    {
		public List<PcpPointExchangeRule> ExchangeRules { get; set; }
    }
}

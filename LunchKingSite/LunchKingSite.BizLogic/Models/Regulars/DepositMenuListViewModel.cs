﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class DepositMenuListViewModel : RegularsBaseViewModel
    {

        public List<ViewPcpSellerMenuItem> ItemList { get; set; }

        /// <summary>
        /// 用menuId取的存放寄杯數量的Object
        /// </summary>
        /// <remarks>
        /// 1.menuId -> TotalAmount, RemainAmount
        /// 2.ViewPcpUserDeposit只有TotalAmount與RemainAmount有值
        /// </remarks>
        public Dictionary<int, ViewPcpUserDeposit> MenuIdToAmountsDic { get; set; }

        #region Method
        public Dictionary<int, List<ViewPcpSellerMenuItem>> GetMenuToItemDic()
        {
            var menuToItemDic = this.ItemList.GroupBy(x => x.MenuId).ToDictionary(x => x.Key, x => x.ToList());
            return menuToItemDic;
        }

        public ViewPcpUserDeposit GetAmountsFromDicByMenuId(int menuId)
        {
            ViewPcpUserDeposit item = new ViewPcpUserDeposit();
            if (MenuIdToAmountsDic.ContainsKey(menuId))
            {
                MenuIdToAmountsDic.TryGetValue(menuId, out item);
            }
            return item;
        }
        #endregion

    }
}

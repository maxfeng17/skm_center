﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class DepositCorrectViewModel : RegularsBaseViewModel
    {
        public List<ViewPcpUserDeposit> UserDepositList { get; set; }

        public Dictionary<int, List<ViewPcpUserDeposit>> GetMenuToDepositDic()
        {
            var dictionary = this.UserDepositList.GroupBy(x => x.MenuId).ToDictionary(x => Convert.ToInt32(x.Key), x => x.ToList());
            return dictionary;
        }
    }
    
}

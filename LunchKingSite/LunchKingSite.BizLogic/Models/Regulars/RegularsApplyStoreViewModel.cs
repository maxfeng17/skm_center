﻿using LunchKingSite.BizLogic.Models.SellerModels;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.BizLogic.Models
{
    public class RegularsApplyStoreViewModel : RegularsBaseViewModel
    {
        public RegularsApplyStoreViewModel()
        {
            ApplyStoreList = new List<RegularsApplyStore>();
        }
        public List<RegularsApplyStore> ApplyStoreList { get; set; }
    }


    public class RegularsApplyStore
    {
        public int Index { get; set; }
        public string StoreName { get; set; }
        public Guid StoreGuid { get; set; }
        public string StoreAddress {get;set;}
        public string StoreTel { get; set; }
        public bool IsSelected { get; set; }
    }
}

﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.BizLogic.Models
{
    [Bind(Exclude = "SellerUserId")]
    public class SellerDepositAddMenuInputModel
    {
        public int SellerUserId { get; set; }


        public string Title { get; set; }
        public DateTime STime { get; set; }
        public DateTime ETime { get; set; }
        //public int DefaultAmount { get; set; }
        public List<string> ItemList { get; set; }
    }
}

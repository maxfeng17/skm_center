﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class PointRemarkParentInputModel
    {
        public List<PointRemarkInputModel> RemarkList { get; set; }
    }

    public class PointRemarkInputModel
    {
        public int Id { get; set; }
        public string RemarkText { get; set; }
    }
}

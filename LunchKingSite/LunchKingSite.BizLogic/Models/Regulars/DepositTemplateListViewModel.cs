﻿using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class DepositTemplateListViewModel : RegularsBaseViewModel
    {
        public PcpDepositTemplateType TemplateType { get; set; }

        public List<PcpDepositTemplate> TemplateList { get; set; }
    }
}

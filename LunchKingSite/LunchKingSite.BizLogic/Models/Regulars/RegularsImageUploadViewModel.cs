﻿using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class RegularsImageUploadViewModel : RegularsBaseViewModel
    {
        public PcpImageType ImgType { get; set; }

        public List<string> ImageList { get; set; }

        public string GetTitleByImageType() {
            return Helper.GetDescription(ImgType);
        }
    }
}

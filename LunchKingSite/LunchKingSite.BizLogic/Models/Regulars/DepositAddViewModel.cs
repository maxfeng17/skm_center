﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class DepositAddViewModel : RegularsBaseViewModel
    {
        public List<ViewPcpSellerMenuItem> ItemList { get; set; }

        #region Method
        public Dictionary<int, List<ViewPcpSellerMenuItem>> GetMenuToItemDic()
        {
            var menuToItemDic = this.ItemList.GroupBy(x => x.MenuId).ToDictionary(x => x.Key, x => x.ToList());
            return menuToItemDic;
        }
        #endregion
    }
}

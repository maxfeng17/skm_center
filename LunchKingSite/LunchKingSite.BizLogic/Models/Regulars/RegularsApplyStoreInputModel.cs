﻿using LunchKingSite.BizLogic.Models.SellerModels;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.BizLogic.Models
{
    public class RegularsApplyStoreInputModel
    {
        public List<string> StoreGuidList { get; set; }
    }
}

﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.BizLogic.Models
{
    [Bind(Exclude = "SellerGuid, GroupId, CreateId")]
    public class UserDepositAddItemInputModel
    {
        #region 從後端塞入
        public string SellerGuid { get; set; }
        public int GroupId { get; set; }
        public int CreateId { get; set; }
        #endregion

        public int MenuId { get; set; }
        public int ItemId { get; set; }
        public decimal Price { get; set; }
        public int TotalAmount { get; set; }
        public int RemainAmount { get; set; }
    }
}

﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class SellerDepositEditMenuInputModel
    {
        public int MenuId { get; set; }
        public string Title { get; set; }
        public DateTime STime { get; set; }
        public DateTime ETime { get; set; }
        public int DefaultAmount { get; set; }

        /// <summary>
        /// 品項列表
        /// </summary>
        /// <example>["itemName1###seq1", "itemName2###seq2"]</example>
        public List<string> ItemList { get; set; }
    }
}

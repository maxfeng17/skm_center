﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class PointSettingViewModel : RegularsBaseViewModel
    {
		public PcpPointCollectRule CollectRule { get; set; }

        public List<PcpPointExchangeRule> ExchangeRules { get; set; }

        public List<PcpPointRemark> Remarks { get; set; }
    }
}

﻿using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class DepositTemplateEditInputModel
    {
        public int Id { get; set; }
        public string Template { get; set; }
        public int Type { get; set; }
    }
}

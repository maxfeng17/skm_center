﻿using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.BizLogic.Models
{
    public class RegularsPreviewViewModel : RegularsBaseViewModel
    {
        public RegularsPreviewViewModel()
        {
        }

        public MembershipCardDetail ApiModel { get; set; }

    }
}

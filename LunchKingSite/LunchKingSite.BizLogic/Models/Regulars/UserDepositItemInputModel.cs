﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.BizLogic.Models
{
    [Bind(Exclude = "SellerGuid, GroupId, CreateId")]
    public class UserDepositItemParentInputModel
    {
        #region 從後端塞入
        public string SellerGuid { get; set; }
        public int GroupId { get; set; }
        public int CreateId { get; set; }
        #endregion

        public List<UserDepositItemInputModel> InputList { get; set; }
    }

    public class UserDepositItemInputModel
    {
        public int UserDepositItemId { get; set; }
        public int UseAmount { get; set; }
    }
}

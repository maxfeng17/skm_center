﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.BizLogic.Models
{
    [Bind(Exclude = "SellerGuid, CardGroupId, CreateId")]
    public class UserPointUseInputModel
    {
        #region 從後端塞入
        public string SellerGuid { get; set; }
        public int CardGroupId { get; set; }
        public int CreateId { get; set; }
        #endregion

        public int Count { get; set; }
        public int ExchangeRuleId { get; set; }
        public string ItemName { get; set; }
    }
}

﻿using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.BizLogic.Models
{
    public class CardDashBoardViewModel: RegularsBaseViewModel
    {
        public CardDashBoardViewModel()
        {
            SellerGuidList = new List<SelectListItem>();
            CardGroup = new ViewMembershipCardGroup();
        }

        /// <summary>
        /// 使用者有管理權限的店家列表(有開過熟客卡的店家才會有)
        /// </summary>
        public List<SelectListItem> SellerGuidList { get; set; }
        
        public ViewMembershipCardGroup CardGroup { get; set; }

        /// <summary>
        /// 形象設定圖
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 有幾家分店適用
        /// </summary>
        public int ApplyStoreCount { get; set; }


        #region 項目是否完成的綠勾

        /// <summary>
        /// 商家介紹是否設定
        /// </summary>
        public bool IsSellerIntroSet { get; set; }
        /// <summary>
        /// 適用分店是否設定
        /// </summary>
        public bool IsApplyStoreSet { get; set; }
        /// <summary>
        /// 行銷跑馬燈是否設定
        /// </summary>
        public bool IsMarqueeSet { get; set; }

        #region 圖片
        /// <summary>
        /// 形象圖片是否設定
        /// </summary>
        public bool IsCorporateImageSet { get; set; }
        /// <summary>
        /// 環境圖是否設定
        /// </summary>
        public bool IsEnvironmentImageSet { get; set; }
        /// <summary>
        /// 餐點圖是否設定
        /// </summary>
        public bool IsServiceImageSet { get; set; }
        #endregion

        #region 功能
        public bool IsVipOn { get; set; }
        public bool IsPointOn { get; set; }
        public bool IsDepositOn { get; set; }
        public bool IsVipSet { get; set; }
        public bool IsPointSet { get; set; }
        public bool IsDepositSet { get; set; }
        #endregion

        #endregion


        public bool IsPointFirstSetting { get; set; }
    }
}

﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class PointUseViewModel : RegularsBaseViewModel
    {
        public int UserPoint { get; set; }
		public List<PcpPointExchangeRule> ExchangeRule { get; set; }
    }
}

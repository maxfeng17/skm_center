﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    public class PointRuleChangeLogViewModel : RegularsBaseViewModel
    {
        public List<PcpPointRuleChangeLog> CollectRuleLogs { get; set; }

        public List<PcpPointRuleChangeLog> ExchangeRuleLogs { get; set; }


        #region method
        public Dictionary<string, List<PcpPointRuleChangeLog>> GetCollectRuleTimeToLogsDic()
        {
            var dic = this.CollectRuleLogs.GroupBy(x => string.Format("{0:yyyy/M/d HH:mm:ss}", x.ModifyTime)).ToDictionary(x => x.Key, x => x.ToList());
            return dic;
        }

        public Dictionary<string, List<PcpPointRuleChangeLog>> GetExchangeRuleTimeToLogsDic()
        {
            var dic = this.ExchangeRuleLogs.GroupBy(x => string.Format("{0:yyyy/M/d HH:mm:ss}", x.ModifyTime)).ToDictionary(x => x.Key, x => x.ToList());
            return dic;
        }
        #endregion
    }
}

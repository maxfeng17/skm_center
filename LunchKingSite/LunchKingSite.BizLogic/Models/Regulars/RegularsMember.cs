﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.BizLogic.Models.Regulars
{

    /// <summary>
    /// 消費使用明細
    /// </summary>
    public class ConsumptionDetailModel
    {
        public int UserId { get; set; }
        public int CardGroupId { get; set; }
        /// <summary>
        /// 使用服務(篩選條件)
        /// </summary>
        public int UseTimeRange { get; set; }
        /// <summary>
        /// 使用期間(篩選條件)
        /// </summary>
        public int UseServiceItem { get; set; }
        public List<SelectListItem> UseServiceItemList { get; set; }
        public List<SelectListItem> UseTimeRangeList { get; set; }
        /// <summary>
        /// 寄杯明細格式
        /// </summary>
        public List<ViewPcpUserDepositLog> DepositDetailList { get; set; }

        /// <summary>
        /// 集點明細格式
        /// </summary>
        public ViewPcpUserPointCollection PointDetailList { get; set; }

        /// <summary>
        /// 會員優惠明細格式
        /// </summary>
        public ViewPcpOrderInfoCollection CardDetailList { get; set; }
        /// <summary>
        /// 專注View使用 (會員消費明細下的詳細資料)
        /// </summary>
        public List<DetailViewModel> ViewList { get; set; }

        public ConsumptionDetailModel()
        {
            ViewList = new List<DetailViewModel>();
            CardDetailList = new ViewPcpOrderInfoCollection();
            PointDetailList = new ViewPcpUserPointCollection();
            DepositDetailList = new List<ViewPcpUserDepositLog>();
        }

    }
    
    /// <summary>
    /// 會員消費明細
    /// </summary>
    public class DetailViewModel
    {
        public MembershipService CategoryType { get; set; }
        /// <summary>
        /// 寄杯用
        /// </summary>
        public PcpUserDepositLogType LogType { get; set; }
        /// <summary>
        /// 集點用
        /// </summary>
        public PcpUserPointSetType SetType { get; set; }
        public string TageName { get; set; }
        public string Title { get; set; }
        public string ItemName { get; set; }
        /// <summary>
        /// 購買數量N1
        /// </summary>
        public int OriginalAmount1 { get; set; }
        /// <summary>
        /// 現場兌換N2
        /// </summary>
        public int ChangeAmount2 { get; set; }
        /// <summary>
        /// 剩餘寄杯N3
        /// </summary>
        public int RemainAmount3 { get; set; }

        public DateTime CreateTime { get; set; }
        public string CreateTimeN { get; set; }
        /// <summary>
        /// 適用分店
        /// </summary>
        public string StoreName { get; set; }

        public Guid? StoreGuid { get; set; }
        /// <summary>
        /// 會員姓名(熟客核銷下的詳細資料才會使用)
        /// </summary>
        public string MemberName { get; set; }
        /// <summary>
        /// 核銷帳號(熟客核銷下的詳細資料才會使用)
        /// </summary>
        public string VerifyAccount { get; set; }
        /// <summary>
        /// 核銷姓名(熟客核銷下的詳細資料才會使用)
        /// </summary>
        public string VerifyName { get; set; }

    }

    /// <summary>
    /// 會員資訊(會員列表)
    /// </summary>
    public class SellerMemberModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int? UserId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? SellerMemberId { set; get; }

        public string SellerMemberName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SellerMemberLastName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string SellerMemberFirstName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? CardId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public MembershipCardLevel Level { set; get; }
        /// <summary>
        /// 累積消費次數
        /// </summary>
        public int OrderCount { set; get; }
        /// <summary>
        /// 累積消費金額
        /// </summary>
        public decimal AmountTotal { set; get; }

        /// <summary>
        /// 寄杯總數
        /// </summary>
        public int DepositTotalAmount { get; set; }
        /// <summary>
        /// 總點數
        /// </summary>
        public int TotalPoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CardNo { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public GenderType Gender { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public SellerMemberModel() { }

        public string LastUseTime { get; set; }
        public int CardGroupId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        public SellerMemberModel(ViewVbsSellerMember m)
        {
            UserId = m.UserId;
            SellerMemberId = m.SellerMemberId;
            SellerMemberLastName = string.IsNullOrWhiteSpace(m.SellerMemberLastName) ? string.Empty : m.SellerMemberLastName;
            SellerMemberFirstName = m.UserId == 0
                                    ? "XX"
                                    : string.IsNullOrWhiteSpace(m.SellerMemberFirstName) ? string.Empty : m.SellerMemberFirstName;
            CardId = m.UserMembershipCardId;
            Level = (MembershipCardLevel)m.Level;
            OrderCount = m.OrderCount;
            AmountTotal = m.AmountTotal;
            CardNo = m.CardNo;
            Gender = m.SellerMemberGender.HasValue ? (GenderType)m.SellerMemberGender : GenderType.Unknown;
            LastUseTime = m.LastUseTime.HasValue ? m.LastUseTime.Value.ToString("yyyy/MM/dd HH:mm") : "";
            CardGroupId = m.CardGroupId;
        }

        public SellerMemberModel(ViewVbsSellerMemberConsumption m)
        {
            UserId = m.UserId;
            SellerMemberId = m.SellerMemberId;
            SellerMemberLastName = string.IsNullOrWhiteSpace(m.SellerMemberLastName) ? string.Empty : m.SellerMemberLastName;
            SellerMemberFirstName = m.UserId == 0
                                    ? "XX"
                                    : string.IsNullOrWhiteSpace(m.SellerMemberFirstName) ? string.Empty : m.SellerMemberFirstName;
            CardId = m.UserMembershipCardId;
            Level = (MembershipCardLevel)m.Level;
            CardNo = m.CardNo;
            Gender = m.SellerMemberGender.HasValue ? (GenderType)m.SellerMemberGender : GenderType.Unknown;
            CardGroupId = m.CardGroupId;

            //總集點
            TotalPoint = m.TotalPoints.HasValue ? m.TotalPoints.Value : 0;
            //總剩餘寄杯
            DepositTotalAmount = m.TotalDepositCoups.HasValue ? m.TotalDepositCoups.Value : 0;
            //1.累積消費金額：會員優惠 + 集點
            AmountTotal = m.TotalConsumption.HasValue ? m.TotalConsumption.Value : 0;
            //2.累積消費次數：會員優惠 + 集點 + 寄杯
            OrderCount = m.TotalOrderCount.HasValue ? m.TotalOrderCount.Value : 0;
            //3.最後使用時間：(會員優惠 + 集點 + 寄杯)取得最新一筆時間
            LastUseTime = m.LastUseTime.HasValue ? m.LastUseTime.Value.ToString("yyyy/MM/dd HH:mm") : string.Empty;

        }
    }

    /// <summary>
    /// 會員詳細資料
    /// </summary>
    public class SellerMemberDetailModel : RegularsBaseViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public SellerMemberDetailModel() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        public SellerMemberDetailModel(ViewVbsSellerMember mem)
        {
            UserId = mem.UserId;
            SellerMemberId = mem.SellerMemberId;
            CardId = mem.UserMembershipCardId;
            Level = mem.Level;
            PaymentPercent = (float?)mem.PaymentPercent;
            Instruction = string.IsNullOrWhiteSpace(mem.Instruction) ? string.Empty : mem.Instruction;
            OtherPremiums = string.IsNullOrWhiteSpace(mem.OtherPremiums) ? string.Empty : mem.OtherPremiums;
            AvailableDateType = (AvailableDateType)mem.AvailableDateType;
            OrderCount = mem.OrderCount;
            AmountTotal = mem.AmountTotal;
            LastUseTime = mem.LastUseTime;
            FirstName = string.IsNullOrWhiteSpace(mem.SellerMemberFirstName) ? string.Empty : mem.SellerMemberFirstName;
            LastName = string.IsNullOrWhiteSpace(mem.SellerMemberLastName) ? string.Empty : mem.SellerMemberLastName;
            Mobile = mem.SellerMemberMobile;
            Gender = mem.SellerMemberGender.HasValue ? (GenderType)mem.SellerMemberGender.Value : GenderType.Unknown;
            Birthday = mem.SellerMemberBirthday;
            Remarks = mem.Remarks;
            RequestTime = mem.RequestTime;
            CardNo = string.IsNullOrWhiteSpace(mem.CardNo) ? string.Empty : mem.CardNo;

            if (string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName))
            {
                FirstName = mem.UserEmail;
            }
            CardGroupId = mem.CardGroupId;
        }


        public SellerMemberDetailModel(ViewVbsSellerMemberConsumption mem)
        {
            UserId = mem.UserId;
            SellerMemberId = mem.SellerMemberId;
            CardId = mem.UserMembershipCardId;
            Level = mem.Level;
            PaymentPercent = (float?)mem.PaymentPercent;
            Instruction = string.IsNullOrWhiteSpace(mem.Instruction) ? string.Empty : mem.Instruction;
            OtherPremiums = string.IsNullOrWhiteSpace(mem.OtherPremiums) ? string.Empty : mem.OtherPremiums;
            AvailableDateType = (AvailableDateType)mem.AvailableDateType;

            FirstName = string.IsNullOrWhiteSpace(mem.SellerMemberFirstName) ? string.Empty : mem.SellerMemberFirstName;
            LastName = string.IsNullOrWhiteSpace(mem.SellerMemberLastName) ? string.Empty : mem.SellerMemberLastName;
            Mobile = mem.SellerMemberMobile;
            Gender = mem.SellerMemberGender.HasValue ? (GenderType)mem.SellerMemberGender.Value : GenderType.Unknown;
            Birthday = mem.SellerMemberBirthday;
            Remarks = mem.Remarks;
            RequestTime = mem.RequestTime;
            CardNo = string.IsNullOrWhiteSpace(mem.CardNo) ? string.Empty : mem.CardNo;

            if (string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName))
            {
                FirstName = mem.UserEmail;
            }
            CardGroupId = mem.CardGroupId;

            //總集點
            TotalPoint = mem.TotalPoints.HasValue ? mem.TotalPoints.Value : 0;
            //總剩餘寄杯
            DepositTotalAmount = mem.TotalDepositCoups.HasValue ? mem.TotalDepositCoups.Value : 0;
            //1.累積消費金額：會員優惠 + 集點
            AmountTotal = mem.TotalConsumption.HasValue ? mem.TotalConsumption.Value : 0;
            //2.累積消費次數：會員優惠 + 集點 + 寄杯
            OrderCount = mem.TotalOrderCount.HasValue ? mem.TotalOrderCount.Value : 0;
            //3.最後使用時間：(會員優惠 + 集點 + 寄杯)取得最新一筆時間
            LastUseTime = mem.LastUseTime;
        }
        
        public int Source { get; set; }
        /// <summary>
        /// 寄杯總數
        /// </summary>
        public int DepositTotalAmount { get; set; }
        /// <summary>
        /// 總點數
        /// </summary>
        public int TotalPoint { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="modify"></param>
        /// <param name="totalCount"></param>
        public SellerMemberDetailModel(ViewVbsSellerMember mem, bool modify, int totalCount) : this(mem)
        {
            IsModify = modify;
            SellerMemberCount = totalCount;
        }
        /// <summary>
        /// 
        /// </summary>
        public int UserId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? SellerMemberId { set; get; }
        /// <summary>
        /// membership_card.id
        /// </summary>
        public int? CardId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? Level { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public float? PaymentPercent { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Instruction { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string OtherPremiums { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public AvailableDateType AvailableDateType { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string AvailableDateDesc
        {
            get
            {
                return Helper.GetEnumDescription(AvailableDateType);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OrderCount { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public decimal AmountTotal { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUseTime { set; get; }

        public string MemberName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FirstName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string LastName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public GenderType Gender { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? Birthday { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Remarks { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? RequestTime { set; get; }
        /// <summary>
        /// user_membership_card.card_no (非pkid)
        /// </summary>
        public string CardNo { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsModify { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SellerMemberCount { get; set; }

        public int CardGroupId { get; set; }
    }

    #region 團購 熟客核銷明細
    public class ViewVerificationListModel
    {
        /// <summary>
        /// 團購頁面資料
        /// </summary>
        public VerificationCouponListByStoreModel CouponModel { get; set; }
        /// <summary>
        /// 熟客核銷明細資料
        /// </summary>
        public ConsumptionDetailModel VerifyModel { get; set; }
        public TabShowLogic IsShowCouponTab { get; set; }
        public TabShowLogic IsShowMemberVerifyTab { get; set; }
        public List<SelectListItem> CardGroupLevels { get; set; }
        public List<SelectListItem> StoreLevels { get; set; }
        public string StoreLevelValue { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 資料筆數
        /// </summary>
        public int TotalCount { get; set; }
        public int SellerUserId { get; set; }

        public Guid? SellerGuid { get; set; }
        public int CardGroupId { get; set; }
        public List<SelectListItem> UseServiceItemList { get; set; }
        public int UseServiceItem { get; set; }
        public List<SelectListItem> UseTimeRangeList { get; set; }
        public int UseTimeRange { get; set; }

        public string storeId { get; set; }
        public int? day { get; set; }
        public int? pageNumber { get; set; }
        public string only { get; set; }
        public string mailTo { get; set; }
        public ViewVerificationListModel()
        {
            CardGroupLevels = new List<SelectListItem>();
            StoreLevels = new List<SelectListItem>();
            IsShowCouponTab = TabShowLogic.Hide;
            IsShowMemberVerifyTab = TabShowLogic.Hide;
            CouponModel = new VerificationCouponListByStoreModel();
            VerifyModel = new ConsumptionDetailModel();
            UseServiceItemList = new List<SelectListItem>();
            UseTimeRangeList = new List<SelectListItem>();
            UseServiceItem = 1;
            UseTimeRange = 5;
        }
    }

    public class VerificationCouponListByStoreModel
    {
        public Dictionary<string, string> StoreList { get; set; }
        public Dictionary<string, string> VerifyTime { get; set; }
        public PagerList<VbsVerificationCoupon> Coupons { get; set; }
        public string StoreId { get; set; }
        public bool IsSellerPermission { get; set; }
        public bool IsOnlyAccount { get; set; }
        //public bool IsAllowedExport { get; set; }
        public VerificationCouponListByStoreModel()
        {
            VerifyTime = new Dictionary<string, string>();
            StoreList = new Dictionary<string, string>();
            IsSellerPermission = false;
        }
    }
    #endregion




}

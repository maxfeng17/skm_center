﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Model
{
    public class PponUsageExpiration : IExpirationChangeable
    {
        #region .prop

        /// <summary>
        /// 檔次的開始使用日
        /// </summary>
		public DateTime? DealUseStartDate { get; private set; }

        /// <summary>
        /// 檔次預設的到期日
        /// </summary>
        public DateTime? DealOriginalExpireDate { get; private set; }

        /// <summary>
        /// 檔次變更使用截止日
        /// </summary>
        public DateTime? DealChangedExpireDate { get; private set; }

        /// <summary>
        /// 分店變更使用截止日
        /// </summary>
        public DateTime? StoreChangedExpireDate { get; private set; }

        /// <summary>
        /// 賣家結束營業日
        /// </summary>
        public DateTime? SellerClosedDownDate { get; private set; }

        /// <summary>
        /// 分店結束營業日
        /// </summary>
        public DateTime? StoreClosedDownDate { get; private set; }

        #endregion

        #region .ctor

        public PponUsageExpiration(Guid orderGuid)
        {
            IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            ViewPponCloseDownExpirationChange change = op.ViewPponCloseDownExpirationChangeGet(orderGuid);
            if (change != null)
            {
            	this.DealUseStartDate = change.BhDeliverTimeS;
                this.DealOriginalExpireDate = change.BhDeliverTimeE;
                this.DealChangedExpireDate = change.BhChangedExpireDate;
                this.StoreChangedExpireDate = change.StoreChangedExpireDate;
                this.SellerClosedDownDate = change.SellerCloseDownDate;
                this.StoreClosedDownDate = change.StoreCloseDownDate;
				
            }
        }

        public PponUsageExpiration(Guid orderGuid, Guid bid, Guid storeGuid)
        {
            IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            ViewPponCloseDownExpirationChange change = op.ViewPponCloseDownExpirationChangeGetByBid(bid, storeGuid);
            if (change != null)
            {
                this.DealUseStartDate = change.BhDeliverTimeS;
                this.DealOriginalExpireDate = change.BhDeliverTimeE;
                this.DealChangedExpireDate = change.BhChangedExpireDate;
                this.StoreChangedExpireDate = change.StoreChangedExpireDate;
                this.SellerClosedDownDate = change.SellerCloseDownDate;
                this.StoreClosedDownDate = change.StoreCloseDownDate;

            }
        }

        private PponUsageExpiration(){}

		#endregion
		
		private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
		public static PponUsageExpiration PponUsageExpirationCreator(Guid bid, Guid? storeGuid)
		{
			ViewPponExpirationCollection expirations = pp.ViewPponExpirationGetList(bid);
			PponUsageExpiration result = new PponUsageExpiration();
			
			if(storeGuid == null)
			{
				ViewPponExpiration ex = expirations.FirstOrDefault();
				if(ex != null)
				{
					result.DealUseStartDate = ex.BhDeliverTimeStart;
					result.DealOriginalExpireDate = ex.BhDeliverTimeEnd;
					result.DealChangedExpireDate = ex.BhChangedExpireDate;
					result.StoreChangedExpireDate = null;
					result.SellerClosedDownDate = ex.SellerCloseDownDate;
					result.StoreClosedDownDate = null;
				}
			}
			else
			{
				ViewPponExpiration ex = expirations.Where(exp => exp.StoreGuid == storeGuid).FirstOrDefault();
				if (ex != null)
				{
					result.DealUseStartDate = ex.BhDeliverTimeStart;
					result.DealOriginalExpireDate = ex.BhDeliverTimeEnd;
					result.DealChangedExpireDate = ex.BhChangedExpireDate;
					result.StoreChangedExpireDate = ex.StoreChangedExpireDate;
					result.SellerClosedDownDate = ex.SellerCloseDownDate;
					result.StoreClosedDownDate = ex.StoreCloseDownDate;
				}
			}

			return result;
		}
        
    }
}

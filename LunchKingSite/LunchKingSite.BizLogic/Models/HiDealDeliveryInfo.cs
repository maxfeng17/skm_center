﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.EInvoice;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model
{
    [Serializable]
    public class HiDealDeliveryInfo
    {
        /// <summary>
        /// 購買商品Pid
        /// </summary>
        public int Pid { get; set; }
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string UserName { get; set; }
        public int UserId { get; set; }
        /// <summary>
        /// 17life現金卷編號
        /// </summary>
        public string DiscountCode { get; set; }
        /// <summary>
        /// 17life現金卷金額
        /// </summary>
        public decimal DiscountAmt { get; set; }
        /// <summary>
        /// 17life購物金
        /// </summary>
        public decimal SCashAmt { get; set; }
        /// <summary>
        /// 17life紅利
        /// </summary>
        public decimal BCashAmt { get; set; }
        /// <summary>
        /// Payeasy購物金
        /// </summary>
        public decimal PCashAmt { get; set; }
        /// <summary>
        /// 運費金額
        /// </summary>
        public decimal FreightAmt { get; set; }
        /// <summary>
        /// 刷卡金額
        /// </summary>
        public decimal CreditCardAmt { get; set; }
        /// <summary>
        /// ATM付款金額
        /// </summary>
        public decimal AtmAmt { get; set; }
        /// <summary>
        /// 總消費金額
        /// </summary>
        public decimal TotalAmount { get; set; }
        /// <summary>
        /// 購買商品數量
        /// </summary>
        public int Count { get; set; }
        #region 購買人資訊
        /// <summary>
        /// 購買人姓
        /// </summary>
        public string PurchaserLastName { get; set; }
        /// <summary>
        /// 購買人名
        /// </summary>
        public string PurchaserFirstName { get; set; }
        /// <summary>
        /// 購買人聯絡電話
        /// </summary>
        public string PurchaserPhone { get; set; }
        /// <summary>
        /// 購買人地址
        /// </summary>
        public LkAddressLite PurchaserAddress { get; set; }
        ///// <summary>
        ///// 是否將購買人資料修改到會員資料中
        ///// </summary>
        //public bool IsUpdateMember { get; set; }

        #endregion 購買人資訊
        #region 發票
        /// <summary>
        /// 發票處理方式
        /// </summary>
        public HiDealInvoiceType InvoiceType { get; set; }
        /// <summary>
        /// 發票類型，例如:二聯式或三聯式
        /// </summary>
        public InvoiceMode2 InvoiceMode { get; set; }

        /// <summary>
        /// 公司名稱
        /// </summary>
        public string InvComName { get; set; }
        /// <summary>
        /// 統一編號
        /// </summary>
        public string InvComId { get; set; }
        /// <summary>
        /// 買受人姓名
        /// </summary>
        public string InvBuyerName { get; set; }
        /// <summary>
        /// 發票地址
        /// </summary>
        public string InvAddress { get; set; }
        /// <summary>
        /// 載具類型
        /// </summary>
        public CarrierType CarrierType { get; set; }
        /// <summary>
        /// 載具號碼
        /// </summary>
        public string CarrierId { get; set; }
        /// <summary>
        /// 愛心碼(捐贈發票)
        /// </summary>
        public string LoveCode { get; set; }
        /// <summary>
        /// 發票版本(自 2014/03/01 起加入發票載具及愛心碼資訊)
        /// </summary>
        public InvoiceVersion Version { get; set; }
        #endregion 發票
        #region 收件人
        /// <summary>
        /// 收件人
        /// </summary>
        public int AddresseeType { get; set; }
        /// <summary>
        /// 選取地址
        /// </summary>
        public string SelectedAddressee { get; set; }
        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string AddresseeName { get; set; }
        /// <summary>
        /// 收件人電話
        /// </summary>
        public string AddresseePhone { get; set; }
        /// <summary>
        /// 收件住址
        /// </summary>
        public LkAddressLite AddresseeAddress { get; set; }
        /// <summary>
        /// 是否新增收件人資料
        /// </summary>
        public bool IsNeedInsertAddressee { get; set; }
        #endregion 收件人
        /// <summary>
        /// 扣除17life紅利、17life購物金、PayEasy購物金、17life現金卷後，剩下的金額(以刷卡或ATM轉帳的應付款)
        /// </summary>
        public decimal Cash { get; set; }

        /// <summary>
        /// 選取的選項
        /// </summary>
        public List<HiDealDeliveryInfoCategorySelected> SelectedCategories { get; set; }
        /// <summary>
        /// 使用者選取的取貨方式
        /// </summary>
        public HiDealDeliveryType? DeliveryType { get; set; }
        /// <summary>
        /// 選取的店鋪編號，若無設定則應設為Guid.Empty
        /// </summary>
        public Guid StoreGuid { get; set; }


        public HiDealDeliveryInfo()
        {
            PurchaserAddress = new LkAddressLite();
            //InvAddress = new LkAddress();
            AddresseeAddress = new LkAddressLite();
            IsNeedInsertAddressee = false;
            //IsUpdateMember = false;
            SelectedCategories = new List<HiDealDeliveryInfoCategorySelected>();
        }
    }

    [Serializable]
    public class HiDealDeliveryInfoCategorySelected
    {
        public int Id { get; set; }
        public string CatgName { get; set; }
        public int Selected { get; set; }
        public string SelName { get; set; }

        public HiDealDeliveryInfoCategorySelected()
        {
            Selected = DefaultSelectedId;
        }

        public const int DefaultSelectedId = -1;
    }
}

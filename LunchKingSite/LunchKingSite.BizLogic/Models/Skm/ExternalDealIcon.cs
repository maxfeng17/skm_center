﻿using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.Skm
{
    public class ExternalDealIconStyle
    {
        [JsonProperty("styleId")]
        public int StyleId { get; set; }
        [JsonProperty("Color")]
        public string ForeColor { get; set; }
        [JsonProperty("background-Color")]
        public string BackgroundColor { get; set; }
    }

    public class ExternalDealIconModel: ExternalDealIconStyle
    {
        public int Id { get; set; }
        public string IconName { get; set; }
    }

    /// <summary>
    /// Deal Icon
    /// </summary>
    public class SkmDealIcon
    {
        /// <summary>
        /// 
        /// </summary>
        public SkmDealIcon() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public SkmDealIcon(ExternalDealIconModel model)
        {
            IconName = model.IconName;
            Color = model.ForeColor;
            BackgroundColor = model.BackgroundColor;
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("iconName")]
        public string IconName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("color")]
        public string Color { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("backgroundColor")]
        public string BackgroundColor { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.BizLogic.Models.Skm
{
    /// <summary>
    /// 新光 App Coupon 列表
    /// </summary>
    public class SkmCouponModel
    {
        /// <summary>
        /// 
        /// </summary>
        public SkmCouponModel()
        {
            IsCompanyInvoice = IsBurningEvent = false;
            SkmPayStatus = BurningPoint = 0;
            EcOrderNo = SkmPayStatusDesc = InvoiceNo = string.Empty;
        }

        #region 來源 ApiUserOrder

        /// <summary>
        /// 訂單GUID
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 依據憑證使用與付款產生的訂單狀態
        /// </summary>
        public ApiOrderStatus OrderStatus { get; set; }
        /// <summary>
        /// 母檔BID
        /// </summary>
        public Guid MainBid { get; set; }
        /// <summary>
        /// 檔次BID
        /// </summary>
        public Guid BusinessHourGuid { get; set; }
        /// <summary>
        /// 使用期限-起 字串
        /// </summary>
        public string UseStartDate { get; set; }
        /// <summary>
        /// 使用期限-迄 字串
        /// </summary>
        public string UseEndDate { get; set; }
        /// <summary>
        /// 購買時間
        /// </summary>
        public string BuyDate { get; set; }
        /// <summary>
        /// 是否顯示憑證編號
        /// </summary>
        public bool ShowCoupon { get; set; }
        /// <summary>
        /// 是否為0元優惠活動
        /// </summary>
        public bool IsFreeDeal { get; set; }
        /// <summary>
        /// 新光檔次資料
        /// </summary>
        public SkmPponDeal SkmPponDeal { get; set; }
        /// <summary>
        /// 特殊貨號
        /// </summary>
        public string SpecialItemNo { get; set; }

        #endregion

        #region 來源 ApiCouponData

        /// <summary>
        /// 憑證流水號
        /// </summary>
        public int CouponId { get; set; }
        /// <summary>
        /// 憑證狀態
        /// </summary>
        public int CouponUsedType { get; set; }
        /// <summary>
        /// 憑證確認碼
        /// </summary>
        public string CouponCode { get; set; }
        /// <summary>
        /// 憑證編號
        /// </summary>
        public string CouponSequenceNumber { get; set; }
        /// <summary>
        /// 憑證使用狀態說明
        /// </summary>
        public string CouponUsedTypeDesc { get; set; }
        /// <summary>
        /// 核銷時間
        /// </summary>
        public string UsageVerifiedTime { set; get; }
        /// <summary>
        /// 產品代碼
        /// </summary>
        public string ProductCode { get; set; }
        /// <summary>
        /// 買幾
        /// </summary>
        public int Buy { get; set; }
        /// <summary>
        /// 送幾
        /// </summary>
        public int Free { get; set; }

        #endregion

        #region EC

        /// <summary>
        /// 發票號碼
        /// </summary>
        public string InvoiceNo { get; set; }
        public int SkmPayStatus { get; set; }
        public string SkmPayStatusDesc { get; set; }
        public string EcOrderNo { get; set; }
        public bool IsCompanyInvoice { get; set; }
        public string TradeNo { get; set; }

        #endregion

        #region 燒點

        /// <summary>
        /// 是否燒點活動
        /// </summary>
        public bool IsBurningEvent { get; set; }
        /// <summary>
        /// 需燒幾點
        /// </summary>
        public int BurningPoint { get; set; }

        #endregion

        /// <summary>
        /// 檔次版本
        /// </summary>
        public int DealVersion { get; set; }
        /// <summary>
        /// 退貨日
        /// </summary>
        public string RefundDate { get; set; }
        /// <summary>
        /// 檔次類型
        /// </summary>
        public int ActiveType { get; set; }
    }

    /// <summary>
    /// SKM App 優惠券列表
    /// </summary>
    public class ApiSkmOrder
    {
        public List<SkmCouponModel> OrderList { get; set; }
    }

    /// <summary>
    /// 新光 App EC 訂單列表
    /// </summary>
    public class SkmEcOrder
    {
        public SkmOrderFilterType FilterType { get; set; }
        public string FilterText { get; set; }
        public List<SkmCouponModel> OrderList { get; set; }
    }
    
    /// <summary>
    /// 管理後台 skm pay 訂單與贈品
    /// </summary>
    public class BackSkmPayOrderAndGift
    {
        public BackendSkmPayOrder Order { get; set; }
        public BackendSkmOrderGift Gift { get; set; }
    }

    /// <summary>
    /// 管理後台 skm pay 訂單
    /// </summary>
    public class BackendSkmPayOrder
    {
        /// <summary>
        /// 訂單流水號
        /// </summary>
        public int OrderId { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 會員編號
        /// </summary>
        public string VipNo { get; set; }
        /// <summary>
        /// 授權交易序號
        /// </summary>
        public string TradeNo { get; set; }
        /// <summary>
        /// 退貨交易序號
        /// </summary>
        public string RefundTradeNo { get; set; }
        /// <summary>
        /// 取貨碼
        /// </summary>
        public int? CouponId { get; set; }
        /// <summary>
        /// 訂單狀態
        /// </summary>
        public SkmPayOrderStatus OrderStatus { get; set; }
        /// <summary>
        /// 是否可退貨
        /// </summary>
        public bool IsReturnable { get; set; }
        /// <summary>
        /// 是否為Appos可退貨時間
        /// </summary>
        public bool IsApposRefundTime { get; set; }
        /// <summary>
        /// 訂單狀態描述
        /// </summary>
        public string OrderStatusDesc { get; set; }
        /// <summary>
        /// 取貨狀態
        /// </summary>
        public string VerifyStatusDesc { get; set; }
        /// <summary>
        /// 退款狀態
        /// </summary>
        public string RefundStatus { get; set; }
        /// <summary>
        /// 退款狀態描述
        /// </summary>
        public string RefundStatusDesc { get; set; }

        /// <summary>
        /// 館別代碼
        /// </summary>
        public string ShopCode { get; set; }
        /// <summary>
        /// 館別櫃位
        /// </summary>
        public string ShopName { get; set; }
        /// <summary>
        /// 購買日期
        /// </summary>
        public string OrderTime { get; set; }
        /// <summary>
        /// 取貨日期	
        /// </summary>
        public string VerifyTime { get; set; }
        /// <summary>
        /// 鑑賞截止日
        /// </summary>
        public string FreeReturnDate { get; set; }
        /// <summary>
        /// 臨櫃退貨退款日
        /// </summary>
        public string ReturnDate { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 商品條碼
        /// </summary>
        public string ProductCode { get; set; }
        /// <summary>
        /// 商品貨號
        /// </summary>
        public string ItemNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SpecialItemNo { get; set; }
        /// <summary>
        /// 消費金額
        /// </summary>
        public string OrderAmount { get; set; }
        /// <summary>
        /// 付款明細
        /// </summary>
        public SkmPaymentDetail PaymentDetail { get; set; }
        /// <summary>
        /// 發票資訊
        /// </summary>
        public SkmInvoice Invoice { get; set; }

        public BackendSkmPayOrder()
        {
            OrderId = 0;
            OrderStatus = SkmPayOrderStatus.None;
            PaymentDetail = new SkmPaymentDetail();
            Invoice = new SkmInvoice();
        }
    }

    /// <summary>
    ///  管理後台 skm pay 訂單贈品
    /// </summary>
    public class BackendSkmOrderGift
    {
        public bool IsLoad { get; set; }
        public string OrderNo { get; set; }
        public List<SkmOrderGift> GiftList { get; set; }
        public string RequestXml { get; set; }
        public string ResponseXml { get; set; }

        public BackendSkmOrderGift()
        {
            IsLoad = false;
            GiftList = new List<SkmOrderGift>();
        }
    }

    /// <summary>
    /// skm pay 贈品
    /// </summary>
    public class SkmOrderGift
    {
        /// <summary>
        /// 557901 檔期代號 
        /// </summary>
        public string DealCode { get; set; }
        /// <summary>
        /// 557902 贈品代號 
        /// </summary>
        public string GiftCode { get; set; }
        /// <summary>
        /// 557903 贈品兌換數量
        /// </summary>
        public string GiftExchangeQuantity { get; set; }
        /// <summary>
        /// 557904 贈品名稱
        /// </summary>
        public string GiftName { get; set; }
        /// <summary>
        /// 557905 檔期名稱
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 557906 兌換狀態
        /// </summary>
        public string GiftExchangeStatus { get; set; }
        /// <summary>
        /// 557907 禮券/抵用券流水號
        /// </summary>
        public string GiftCertificateNo { get; set; }
        /// <summary>
        /// 557908 兌換人員姓名
        /// </summary>
        public string RedeemerName { get; set; }
        /// <summary>
        /// 557909 兌換時間 yyyyMMddHHmmss
        /// </summary>
        public string ExchangeTime { get; set; }
        /// <summary>
        /// 557910 贈品類型
        /// </summary>
        public string GiftType { get; set; }
        /// <summary>
        /// 557911 禮券/抵用券金額
        /// </summary>
        public string GiftCertificateAmount { get; set; }
        /// <summary>
        /// 557912 贈品兌換單號
        /// </summary>
        public string GiftExchangeNo { get; set; }
        /// <summary>
        /// 557913 兌換分店名稱
        /// </summary>
        public string ExchangeStoreName { get; set; }
        /// <summary>
        /// 557917 會員卡號
        /// </summary>
        public string MemberNo { get; set; }
        /// <summary>
        /// 557918 卡種編號
        /// </summary>
        public string CardTypeNo { get; set; }
    }

    #region 呼叫 Umall Model

    /// <summary>
    /// 查詢贈禮 Request
    /// </summary>
    public class SkmGiftTransXmlSendData
    {
        public SkmGiftSendData Trans { get; set; }
        public SkmGiftTransXmlSendData()
        {
            Trans = new SkmGiftSendData();
        }
    }

    /// <summary>
    /// 2.31退貨贈品兌換狀態查詢 SendData
    /// </summary>
    public class SkmGiftSendData
    {
        /// <summary>
        /// Message Type ID
        /// 0100 :交易授權 Request
        /// 0110 :交易授權 Response
        /// </summary>
        public string T0100 { get; set; }
        /// <summary>
        /// Processing Code (249000 退貨贈品兌換狀態查詢)
        /// </summary>
        public string T0300 { get; set; }
        /// <summary>
        /// Local time (hhmmss)
        /// </summary>
        public string T1200 { get; set; }
        /// <summary>
        /// Local date (yyyyMMdd)
        /// </summary>
        public string T1300 { get; set; }
        /// <summary>
        /// 端末設備代號 (17 後台填入 IP 不含點號右八位)
        /// </summary>
        public string T4100 { get; set; }
        /// <summary>
        /// 店櫃業者代號 (必須填入使用者所選的店別代號)
        /// </summary>
        public string T4200 { get; set; }
        /// <summary>
        /// 退貨原始銷售交易櫃號
        /// </summary>
        public string T5503 { get; set; }
        /// <summary>
        /// 退貨原始銷售交易收銀機號
        /// </summary>
        public string T5504 { get; set; }
        /// <summary>
        /// 退貨原始銷售交易序號
        /// </summary>
        public string T5505 { get; set; }
        /// <summary>
        /// 原始銷售交易發票號碼
        /// </summary>
        public string T5507 { get; set; }
        /// <summary>
        /// 系統別 (7 : 17 後台)
        /// </summary>
        public string T5509 { get; set; }
        /// <summary>
        /// 退貨原始銷售交易日期 (yyyyMMdd)
        /// </summary>
        public string T5583 { get; set; }
    }



    /// <summary>
    /// 贈品
    /// </summary>
    public class T5579
    {
        /// <summary>
        /// 557901 檔期代號 
        /// </summary>
        public string T557901 { get; set; }
        /// <summary>
        /// 557902 贈品代號 
        /// </summary>
        public string T557902 { get; set; }
        /// <summary>
        /// 557903 贈品兌換數量
        /// </summary>
        public string T557903 { get; set; }
        /// <summary>
        /// 557904 贈品名稱
        /// </summary>
        public string T557904 { get; set; }
        /// <summary>
        /// 557905 檔期名稱
        /// </summary>
        public string T557905 { get; set; }
        /// <summary>
        /// 557906 兌換狀態
        /// </summary>
        public string T557906 { get; set; }
        /// <summary>
        /// 557907 禮券/抵用券流水號
        /// </summary>
        public string T557907 { get; set; }
        /// <summary>
        /// 557908 兌換人員姓名
        /// </summary>
        public string T557908 { get; set; }
        /// <summary>
        /// 557909 兌換時間 yyyyMMddHHmmss
        /// </summary>
        public string T557909 { get; set; }
        /// <summary>
        /// 557910 贈品類型
        /// </summary>
        public string T557910 { get; set; }
        /// <summary>
        /// 557911 禮券/抵用券金額
        /// </summary>
        public string T557911 { get; set; }
        /// <summary>
        /// 557912 贈品兌換單號
        /// </summary>
        public string T557912 { get; set; }
        /// <summary>
        /// 557913 兌換分店名稱
        /// </summary>
        public string T557913 { get; set; }
        /// <summary>
        /// 557917 會員卡號
        /// </summary>
        public string T557917 { get; set; }
        /// <summary>
        /// 557918 卡種編號
        /// </summary>
        public string T557918 { get; set; }
    }

    #endregion

    /// <summary>
    /// 付款明細
    /// </summary>
    public class SkmPaymentDetail
    {
        /// <summary>
        /// 付款方式描述
        /// </summary>
        public string PaymentMethodDesc { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        public SkmPaymentMethod PaymentMethod { get; set; }
        /// <summary>
        /// 付款金額
        /// </summary>
        public string PayAmount { get; set; }
        /// <summary>
        /// 紅利折抵金額
        /// </summary>
        public string RedeemAmount { get; set; }
        /// <summary>
        /// 紅利折抵點數
        /// </summary>
        public string RedeemPoint { get; set; }
        /// <summary>
        /// 燒點點數
        /// </summary>
        public string BurningSkmPoint { get; set; }
    }

    /// <summary>
    /// 發票資訊
    /// </summary>
    public class SkmInvoice
    {
        /// <summary>
        /// 發票開立日
        /// </summary>
        public string InvoiceDate { get; set; }
        /// <summary>
        /// 發票號碼
        /// </summary>
        public string InvoiceNo { get; set; }
        /// <summary>
        /// 發票金額
        /// </summary>
        public string InvoiceAmount { get; set; }
        /// <summary>
        /// 是否捐贈
        /// </summary>
        public bool IsDonate { get; set; }
        /// <summary>
        /// 愛心碼
        /// </summary>
        public string DonateCode { get; set; }
        /// <summary>
        /// 載具類型
        /// </summary>
        public string CarrierType { get; set; }
        /// <summary>
        /// 載具編號
        /// </summary>
        public string CarrierNo { get; set; }
        /// <summary>
        /// 統編
        /// </summary>
        public string CompId { get; set; }
        
    }

    public class SkmPponOrderModel
    {
        public SkmPponOrderModel(ViewSkmPponOrder o)
        {
            Guid = o.Guid;
            SellerGuid = o.SellerGuid;
            BusinessHourGuid = o.BusinessHourGuid;
            UserId = o.UserId;
            OrderStatus = o.OrderStatus;
            CreateTime = o.CreateTime;
            BusinessHourOrderTimeS = o.BusinessHourOrderTimeS;
            BusinessHourOrderTimeE = o.BusinessHourOrderTimeE;
            BusinessHourDeliverTimeS = o.BusinessHourDeliverTimeS;
            BusinessHourDeliverTimeE = o.BusinessHourDeliverTimeE;
        }

        public SkmPponOrderModel(ViewSkmPayPponOrder o)
        {
            Guid = o.Guid;
            SellerGuid = o.SellerGuid;
            BusinessHourGuid = o.BusinessHourGuid;
            UserId = o.UserId;
            OrderStatus = o.OrderStatus;
            CreateTime = o.CreateTime;
            SkmPayOrderStatus = o.Status;
            SkmPayOrderId = o.SkmPayOrderId;
            EcOrderNo = o.OrderNo;
            RefundDate = o.RefundDate;
            SkmTradeNo = o.SkmTradeNo;
        }

        public Guid Guid { get; set; }
        
        public Guid SellerGuid { get; set; }
        
        public Guid BusinessHourGuid { get; set; }
        
        public int UserId { get; set; }
        
        public int OrderStatus { get; set; }
        
        public DateTime CreateTime { get; set; }
        
        public DateTime? BusinessHourOrderTimeS { get; set; }
        
        public DateTime? BusinessHourOrderTimeE { get; set; }
        
        public DateTime? BusinessHourDeliverTimeS { get; set; }
        
        public DateTime? BusinessHourDeliverTimeE { get; set; }

        public int SkmPayOrderStatus { get; set; }

        public int SkmPayOrderId { get; set; }

        public string EcOrderNo { get; set; }

        public DateTime? RefundDate { get; set; }

        public string SkmTradeNo { get; set; }
    }
}

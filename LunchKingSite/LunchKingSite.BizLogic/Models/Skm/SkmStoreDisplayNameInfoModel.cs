﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Skm
{
    /// <summary> SKM檔次顯示櫃位名稱資訊 </summary>
    [Serializable]
    public class SkmStoreDisplayNameInfoModel
    {
        /// <summary> 櫃位名稱編號 </summary>
        public Guid Guid { get; set; }
        /// <summary> 檔次兌換地點類型 </summary>
        public SkmDealExchangeType ExchangeType { get; set; }
        /// <summary> 櫃位名稱編號 </summary>
        public string DisplayName { get; set; }
        /// <summary> 分館編號 </summary>
        public string ShopCode { get; set; }
        /// <summary> 櫃位名稱 </summary>
        public string ShopName { get; set; }
        /// <summary> 分館編號 </summary>
        public Guid SellerGuid { get; set; }
    }
}

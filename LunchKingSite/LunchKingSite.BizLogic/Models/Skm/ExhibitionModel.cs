﻿using System;
using LunchKingSite.Core.Models.Entities;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.Skm
{
    public class DealExhibitionCode
    {
        public DealExhibitionCode() { }

        public DealExhibitionCode(ViewExternalDealExhibitionCode code)
        {
            ExternalDealGuid = code.ExternalDealGuid;
            ExhibitionCodeId = code.ExhibitionCodeId;
            ExhibitionName = code.ExhibitionName;
        }
        
        [JsonProperty("externalDealGuid")]
        public Guid ExternalDealGuid { get; set; }
        [JsonProperty("exhibitionCodeId")]
        public long ExhibitionCodeId { get; set; }
        [JsonProperty("exhibitionName")]
        public string ExhibitionName { get; set; }
    }
}
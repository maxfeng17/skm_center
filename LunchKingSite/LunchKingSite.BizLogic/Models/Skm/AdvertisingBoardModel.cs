﻿using Newtonsoft.Json;
using System;

namespace LunchKingSite.BizLogic.Models.Skm
{
    [Serializable]
    /// <summary> 廣告看板資訊 </summary>
    public class AdvertisingBoardModel
    {
        [JsonProperty("url")]
        /// <summary> 廣告連結 </summary>
        public string Url { get; set; }

        [JsonProperty("image")]
        /// <summary> 圖檔連結 </summary>
        public string Image { get; set; }

        [JsonProperty("title")]
        /// <summary> 廣告看板名稱 </summary>
        public string Title { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Models.Skm
{
    /// <summary>
    /// 新光成本中心
    /// </summary>
    public class SkmBurningEventCostCenter
    {
        public string StoreCode { get; set; }
        public string CostCenterCode { get; set; }
        public string OrderCode { get; set; }
        public int ExChangeQty { get; set; }
    }

    public class SkmBurningEventModel
    {
        public SkmBurningEventModel()
        {
            DealVersion = (int)ExternalDealVersion.Burning;
        }

        public SkmBurningEventModel Clone()
        {
            return new SkmBurningEventModel
            {
                ActiveType = this.ActiveType,
                ExternalGuid = this.ExternalGuid,
                EventName = this.EventName,
                EventId = this.EventId,
                BurningEventSid = this.BurningEventSid,
                DeliveryTimeS = this.DeliveryTimeS,
                DeliveryTimeE = this.DeliveryTimeE,
                BurningPoint = this.BurningPoint,
                ExchangeItemId = this.ExchangeItemId,
                ExchangeQty = this.ExchangeQty,
                CloseProjectStatus = this.CloseProjectStatus,
                CostCenter = this.CostCenter,
                GiftPoint = this.GiftPoint,
                GiftWalletId = this.GiftWalletId,
                DealStartTime = this.DealStartTime,
                DealEndTime = this.DealEndTime,
                PrizeDeal = this.PrizeDeal,
                DealVersion = this.DealVersion,
                IsCostCenterHq = this.IsCostCenterHq
            };
        }

        /// <summary>
        /// SKM檔次類型
        /// </summary>
        public int ActiveType { get; set; }
        /// <summary>
        /// 17Life External Guid
        /// </summary>
        public Guid ExternalGuid { get; set; }
        /// <summary>
        /// 燒點活動名稱
        /// </summary>
        public string EventName { get; set; }
        /// <summary>
        /// SKM燒點活動代號
        /// </summary>
        public string EventId { get; set; }
        /// <summary>
        /// 燒點活動流水號 C000000001
        /// </summary>
        public string BurningEventSid { get; set; }
        /// <summary>
        /// 燒點活動起始日
        /// </summary>
        public DateTime DeliveryTimeS { get; set; }
        /// <summary>
        /// 燒點活動結束日
        /// </summary>
        public DateTime DeliveryTimeE { get; set; }
        /// <summary>
        /// 燃點
        /// </summary>
        public int BurningPoint { get; set; }
        /// <summary>
        /// 兌換商品編號
        /// </summary>
        public string ExchangeItemId { get; set; }
        /// <summary>
        /// 可兌換數量
        /// </summary>
        public int ExchangeQty { get; set; }
        /// <summary>
        /// 活動結案狀態
        /// </summary>
        public bool CloseProjectStatus { get; set; }
        /// <summary>
        /// 成本中心
        /// </summary>
        public List<SkmBurningEventCostCenter> CostCenter { get; set; }
        /// <summary>
        /// 禮券金額
        /// </summary>
        public int GiftPoint { get; set; }
        /// <summary>
        /// 電子券WalletId
        /// </summary>
        public string GiftWalletId { get; set; }
        /// <summary>
        /// 商品檔期起始日
        /// </summary>
        public DateTime DealStartTime { get; set; }
        /// <summary>
        /// 商品檔期結止日
        /// </summary>
        public DateTime DealEndTime { get; set; }
        /// <summary>
        /// 抽獎檔次
        /// </summary>
        public bool PrizeDeal { get; set; }
        /// <summary>
        /// 檔次版本
        /// </summary>
        public int DealVersion { get; set; }
        /// <summary>
        /// 成本中心是否設定為總公司
        /// </summary>
        public bool IsCostCenterHq { get; set; }
        /// <summary>
        /// 歸帳館別
        /// </summary>
        public string CostStoreCode { get; set; }
    }
}

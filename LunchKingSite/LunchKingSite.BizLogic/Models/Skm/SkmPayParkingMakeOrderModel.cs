﻿using LunchKingSite.BizLogic.Model.API;

namespace LunchKingSite.BizLogic.Models.Skm
{
    /// <summary> SKM PAY Parking MakeOrder 回應資訊 </summary>
    public class SkmPayParkingMakeOrderModel 
    {
        /// <summary> API呼叫最多限制次數 </summary>
        public int ApiAccseeLimite { get; set; }

        /// <summary> 交易日期 </summary>
        public string TransDate { get; set; }

        /// <summary> 交易金額 </summary>
        public int TradeAmount { get; set; }

        /// <summary> 刷卡金額 </summary>
        public int CreditCardAmount { get; set; }

        /// <summary> 紅利折抵金額 </summary>
        public int DiscountAmount { get; set; }

        /// <summary> 是否需要OTP驗證 </summary>
        public bool NeedOTP { get; set; }

        /// <summary> OTP驗證連結 </summary>
        public string OTPUrl { get; set; }
    }



    /// <summary>
    /// 查詢停車支付request
    /// </summary>
    public class GetParkingPaymentInfoRequest
    {
        public string MemberCardNo { get; set; }
        public string ShopCode { get; set; }
        public string ParkingTicketNo { get; set; }
        public string ParkingToken { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }

    /// <summary>
    /// 停車支付退貨reqest
    /// </summary>
    public class RefundParkingPaymentRequest
    {
        public string OrderNo { get; set; }
        public string ParkingTicketNo { get; set; }
    }

    /// <summary>
    /// 停車查詢訂單資訊
    /// </summary>
    public class SkmPayParkingOrderModel
    {
        public string MemberCardNo { get; set; }
        public string ShopCode { get; set; }
        public string OrderNo { get; set; }
        public string ParkingTicketNo { get; set; }
        public string ParkingToken { get; set; }
        public string ParkingAreaNo { get; set; }
        public string SellInvoice { get; set; }
        public int Status { get; set; }
        public string CreateDate { get; set; }
        public int OrderAmount { get; set; }
        public string MerchantTradeNo { get; set; }
    }
}

﻿using System.Collections.Generic;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.BizLogic.Models.Skm
{
    public class SkmActivityModel
    {
        public string UserName { get; set; }
        public string CardNo { get; set; }
        public string DeviceType { get; set; }
        public string DeviceCode { get; set; }
        public int MemStatus { get; set; }
        public SkmActivity Act { get; set; }
        public List<SkmActivityRangeModel> RangeList { get; set; }
        public List<SkmActivityItem> ActItems { get; set; }

        public bool ActEnabled { get; set; }
        public string AccessToken { get; set; }
        public string ApiSiteUrl { get; set; }
    }

    public class SkmActivityRangeModel
    {
        public List<int> ShowRangeIds { get; set; }
        public SkmActivityDateRange Range { get; set; }
        public int Qty { get; set; }
    }

    public class SkmActivityJoinResult
    {
        public SkmActivityJoinResult()
        {
            ActRecord = new SkmActivityRangeModel();
        }

        public SkmActivityRangeModel ActRecord { get; set; }
        public int ThisTimeCharge { get; set; }
        public int ThisTimeCost { get; set; }
    }
}

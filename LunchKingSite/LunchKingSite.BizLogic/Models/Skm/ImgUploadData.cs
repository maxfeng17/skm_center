﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Skm
{
    public class ImgUploadData
    {
        public bool SuccessLoadFile { get; set; }
        public List<ImgData> ImgFiles { get; set; }
    }

    public class ImgData
    {
        public ImgData()
        {
            IsExtensionPass = true;
            IsSizePass = true;
            OriFileName = SaveFileName = string.Empty;
        }

        public string OriFileName { get; set; }
        public string SaveFileName { get; set; }
        public bool IsExtensionPass { get; set; }
        public bool IsSizePass { get; set; }
    }
}

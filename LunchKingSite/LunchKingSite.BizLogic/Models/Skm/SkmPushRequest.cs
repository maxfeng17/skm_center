﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Models.Entities;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.Skm
{
    public class SkmPushRequest
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public SkmPushRequest()
        {
            Thread.Sleep(1);
            DateTime now = DateTime.UtcNow;

            ActiveId = ConvertTo36(long.Parse(string.Format("{0:MMddyyHHmmssfff}",now))).PadLeft(10,'0');

            ExpectSendDate = string.Format("{0:yyyyMMdd}", DateTime.Now);
        }

        public SkmPushRequest(ViewSkmCashTrustLogNotVerified data, SkmCashTrustLogNotVerifiedType type)
        {

            if (data == null || data.BusinessHourDeliverTimeE == null)
            {
                return;
            }
            
            Thread.Sleep(1);
            DateTime now = DateTime.UtcNow;
            
            ActiveId = ConvertTo36(long.Parse(string.Format("{0:MMddyyHHmmssfff}",now))).PadLeft(10,'0');

            MerchantId = data.ShopCode;

            ExpectSendDate = string.Format("{0:yyyyMMdd}", DateTime.Now);

            ExpectSendTime = config.SkmCashTrustLogExpectSendTime;

            switch (type)
            {
                case SkmCashTrustLogNotVerifiedType.PreOrder:
                    LocalName = data.IsSkmPay ? "【取貨通知】" : "【優惠券通知】";
                    PushContent = data.IsSkmPay
                        ? string.Format("您的「{0}」已可於「{1:yyyy/MM/dd} ~ {2:yyyy/MM/dd}」來店開始取貨", data.ItemName,
                            data.BusinessHourDeliverTimeS, data.BusinessHourDeliverTimeE)
                        : string.Format("您的「{0}」券已可於「{1:yyyy/MM/dd} ~ {2:yyyy/MM/dd}」開始使用", data.ItemName,
                            data.BusinessHourDeliverTimeS, data.BusinessHourDeliverTimeE);
                    break;
                case SkmCashTrustLogNotVerifiedType.ExpiringOrder:
                    LocalName = data.IsSkmPay ? "【取貨截止通知】" : "【優惠券截止通知】";
                    PushContent = data.IsSkmPay
                        ? string.Format("提醒您，您購買的「{0}」已到貨，請於「{1:yyyy/MM/dd}」前來店領取"
                            , data.ItemName, data.BusinessHourDeliverTimeE)
                        : string.Format("提醒您，您的「{0}」券尚未使用，請於「{1:yyyy/MM/dd}」前使用完畢"
                            , data.ItemName, data.BusinessHourDeliverTimeE);
                    break;
            }

            PushLink = data.IsSkmPay ? Helper.GetEnumDescription(EventType.OrderListNotVerified)
                : Helper.GetEnumDescription(EventType.CouponListNotVerified);

            ProgramType = Helper.GetEnumDescription(SkmPushRequestProgramType.DeepLink);

            Tokens = new List<string>() { data.SkmToken };
        }

        public void AddToken(string token)
        {
            Tokens.Add(token);
        }

        private string ConvertTo36(long i)
        {
            string s = "";
            long j = 0;
            while (i >= 36)
            {
                j = i % 36;
                if (j < 10)
                    s += j.ToString();
                else
                    s += Convert.ToChar(j + 87);
                i = i / 36;
            }
            if (i < 10)
                s += i.ToString();
            else
                s += Convert.ToChar(i + 87);
            Char[] c = s.ToCharArray();
            Array.Reverse(c);
            return Convert.ToString(new string(c)).ToUpper();
        }

        #region prop

        //17Life推播編號(MaxLength:10)
        [JsonProperty("ACTIVE_ID")]
        public string ActiveId { get; set; }
        //館別(MaxLength:4)
        [JsonProperty("MERCHANT_ID")]
        public string MerchantId { get; set; }
        //活動名稱(MaxLength:100)
        [JsonProperty("LOCAL_NAME")]
        public string LocalName { get; set; }
        //預計發送數量(MaxLength:8)
        [JsonProperty("QUOTA")]
        public string Quota
        {
            get { return Tokens.Count.ToString(); }
        }
        //預定發送日期(MaxLength:8)
        [JsonProperty("EXPECT_SEND_DATE")]
        public string ExpectSendDate { get; set; }
        //預定發送時間(MaxLength:6)
        [JsonProperty("EXPECT_SEND_TIME")]
        public string ExpectSendTime { get; set; }
        //推播內容(MaxLength:300)
        [JsonProperty("PUSH_CONTENT")]
        public string PushContent { get; set; }
        //推播連結/頁面(MaxLength:300)
        [JsonProperty("PUSH_LINK")]
        public string PushLink { get; set; }
        //程式類型(與PUSH_LINK搭配)(MaxLength:20)
        [JsonProperty("PROGRAM_TYPE")]
        public string ProgramType { get; set; }
        //會員TOKEN(多筆)(MaxLength:40)
        [JsonProperty("TOKENS")]
        public List<string> Tokens { get; set; }

        #endregion

    }
}

﻿using System;

namespace LunchKingSite.BizLogic.Models.Skm
{

    /// <summary>
    /// 更新新光三越贈品Model
    /// </summary>
    public class SkmGiftInventoryModel
    {
        /// <summary>
        /// 建檔代號
        /// </summary>
        public Guid ExternalDealGuid { get; set; }
        /// <summary>
        /// 館別代號
        /// </summary>
        public string ShopCode { get; set; }
        /// <summary>
        /// 贈品代號
        /// </summary>
        public string ExchangeItemId { get; set; }
        /// <summary>
        /// 活動代號
        /// </summary>
        public string SkmEventId { get; set; }
        /// <summary>
        /// 調整庫存數量
        /// </summary>
        public int AddQty { get; set; }
    }

    /// <summary>
    /// 更新新光三越贈品Model
    /// </summary>
    public class SkmGiftInventoryResponseModel : SkmGiftInventoryModel
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// 失敗訊息
        /// </summary>
        public string Message { get; set; }
    }
}

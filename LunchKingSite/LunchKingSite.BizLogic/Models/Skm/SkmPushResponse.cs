﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Skm
{
    public class SkmPushResponse
    {
        //17Life推播編號(MaxLength:10)
        [JsonProperty("ACTIVE_ID")]
        public string ActiveId { get; set; }
        //UMALL推播編號(MaxLength:10)
        [JsonProperty("PROGRAM_ID")]
        public string ProgramId { get; set; }
        //狀態(MaxLength:3)
        [JsonProperty("STATUS_NO")]
        public string StatusNo { get; set; }
        //訊息，錯誤訊息說明(MaxLength:50)
        [JsonProperty("MESSAGE_TX")]
        public string MessageTx { get; set; }
    }
}

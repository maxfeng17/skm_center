﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Skm
{
    /// <summary>
    /// 新光Token置換Model
    /// </summary>
    public class SkmExchangeTokenModel
    {
        /// <summary>
        /// 身份證(SHA256Base64)
        /// </summary>
        public string ShaMemberId { get; set; }
        /// <summary>
        /// 會員卡號
        /// </summary>
        public string MemberCardNo { get; set; }
        /// <summary>
        /// 原Token
        /// </summary>
        public string OriSkmToken { get; set; }
        /// <summary>
        /// 新Token
        /// </summary>
        public string NewSkmToken { get; set; }
        /// <summary>
        /// 裝置類型
        /// </summary>
        public int? DeviceType { get; set; }
    }

    /// <summary>
    /// 新光站台會員串接 OauthKey
    /// </summary>
    public class SkmSiteOauthKeyModel
    { 
        /// <summary>
        /// 串接網址
        /// </summary>
        public string Oauth { get; set; }
        /// <summary>
        /// 加密KEY
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 加密IV
        /// </summary>
        public string Iv { get; set; }
        /// <summary>
        /// 功能別
        /// </summary>
        public string[] T4100 { get; set; }
    }
}

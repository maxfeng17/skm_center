﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model.API;

namespace LunchKingSite.BizLogic.Models.Skm
{
    /// <summary>
    /// SkmApiDeal
    /// </summary>
    public class SkmApiDeal
    {
        /// <summary>
        /// 檔次代號
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 手機版頁面當作檔次名稱顯示。
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 該檔次所有輪播圖片
        /// </summary>
        public string[] ImagePaths { get; set; }
        /// <summary>
        /// For App圖片
        /// </summary>
        public string AppDealPic { get; set; }
        /// <summary>
        /// 好康名稱-黑標
        /// </summary>
        public string CouponEventName { get; set; }
        /// <summary>
        /// 好康名稱-橘標
        /// </summary>
        public string CouponEventTitle { get; set; }
        /// <summary>
        /// 好康權益說明
        /// </summary>
        public string Introduction { get; set; }
        /// <summary>
        /// 好康權益說明的標籤
        /// </summary>
        public List<string> IntroductionTags { get; set; }
        /// <summary>
        /// 更多權益說明
        /// </summary>
        public string Restrictions { get; set; }
        /// <summary>
        /// 詳細介紹
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 可用商家(好康哪裡找)
        /// </summary>
        public List<ApiPponStore> Availabilities { get; set; }
        /// <summary>
        /// 已銷售數量-查詢時商品已銷售的數量
        /// </summary>
        public int SoldNum { get; set; }
        /// <summary>
        /// 已銷售數量顯示值-已銷售數量*設定的倍數 所得到用於顯示的的數值
        /// </summary>
        public int SoldNumShow { get; set; }
        /// <summary>
        /// 銷售數量顯示的單位
        /// </summary>
        public string ShowUnit { get; set; }
        /// <summary>
        /// 電子禮券/電子抵用金獲得金額
        /// </summary>
        public decimal GiftCash { get; set; }

        /// <summary>
        /// 售價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal OriPrice { get; set; }
        /// <summary>
        /// 折數顯示用文字"5折"等，特殊檔次可能顯示”特惠”等
        /// </summary>
        public string DiscountString { get; set; }
        /// <summary>
        /// 開始販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealStartTime { get; set; }
        /// <summary>
        /// 結束販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealEndTime { get; set; }
        /// <summary>
        /// 是否已售完 true為售完
        /// </summary>
        public bool SoldOut { get; set; }
        /// <summary>
        /// 一姬說好康
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 是否為多檔次
        /// </summary>
        public bool MultiDeal { get; set; }
        /// <summary>
        /// 是否為購物車檔次
        /// </summary>
        public bool ShoppingCart { get; set; }
        /// <summary>
        /// 預設的檔次活動區域名稱
        /// </summary>
        public string DefaultActivityName { get; set; }
        /// <summary>
        /// 旅遊地點
        /// </summary>
        public string TravelPlace { get; set; }
        /// <summary>
        /// 檔次的ICON標示，如破千熱銷之類的ID值
        /// </summary>
        public List<int> DealTags { get; set; }
        /// <summary>
        /// 是否為全家檔次
        /// </summary>
        public bool IsFami { get; set; }
        /// <summary>
        /// 頻道類型
        /// </summary>
        public int BehaviorType { set; get; }
        /// <summary>
        /// 是否顯示兌換價
        /// </summary>
        public bool ShowExchangePrice { get; set; }
        /// <summary>
        /// 兌換價
        /// </summary>
        public decimal ExchangePrice { get; set; }
        /// <summary>
        /// 是否已經收藏
        /// </summary>
        public bool IsCollected { get; set; }
        /// <summary>
        /// 是否可使用折價券
        /// </summary>
        public bool DiscountCodeUsed { get; set; }
        /// <summary>
        /// 是否可用ATM付款
        /// </summary>
        public bool AtmAvailable { get; set; }
        /// <summary>
        /// 品生活的Menu標題內容
        /// </summary>
        public List<ApiPiinlifeMenuTab> PiinlifeMenuTabs { get; set; }
        /// <summary>
        /// 品生活特別說明(無子母檔)
        /// </summary>
        public string PiinlifeRightContent { set; get; }
        /// <summary>
        /// 是否為體驗商品
        /// </summary>
        public bool IsExperience { get; set; }
        /// <summary>
        /// 體驗商品收藏次數
        /// </summary>
        public string ExperienceDealCollectCount { set; get; }
        /// <summary>
        /// 已兌換人數
        /// </summary>
        public string ExchangeDealCount { set; get; }
        /// <summary>
        /// 優惠類型
        /// </summary>
        public int DiscountType { set; get; }
        /// <summary>
        /// 優惠內容
        /// </summary>
        public int Discount { set; get; }
        /// <summary>
        /// 是否燒點檔次
        /// </summary>
        public bool IsBurningDeal { get; set; }
        /// <summary>
        /// 需燒點數
        /// </summary>
        public int BurningPoint { get; set; }
        /// <summary>
        /// 是否已領取過
        /// </summary>
        public bool IsOrdered { get; set; }
        /// <summary>
        /// 買幾
        /// </summary>
        public int Buy { get; set; }
        /// <summary>
        /// 送幾
        /// </summary>
        public int Free { get; set; }
        /// <summary>
        /// 檔次Icon
        /// </summary>
        public SkmDealIcon IconInfo { get; set; }
        /// <summary>
        /// 子檔
        /// </summary>
        public List<SkmApiComboDeal> ComboDeals { get; set; }
        /// <summary>
        /// 是否為skmpay
        /// </summary>
        public bool IsSkmPay { get; set; }
        /// <summary>
        /// 兌換起始日
        /// </summary>
        public string DeliveryStartTime { get; set; }
        /// <summary>
        /// 兌換結束日
        /// </summary>
        public string DeliveryEndTime { get; set; }
        /// <summary>
        /// 優惠類型
        /// </summary>
        public int ActiveType { get; set; }
        /// <summary>
        /// 是否下架
        /// </summary>
        public bool IsOffSale { get; set; }
        /// <summary>
        /// 兌換地點+導航資訊
        /// </summary>
        public List<SkmPponStore> SkmAvailabilities { get; set;}
        /// <summary>
        /// 是否顯示分期資訊
        /// </summary>
        public bool IsInstallment { get; set; }
        /// <summary>
        /// 分期說明頁Url
        /// </summary>
        public string InstallmentInfoUrl { get; set; }

        public SkmApiDeal()
        { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deal"></param>
        public SkmApiDeal(ApiPponDeal deal)
        {
            Bid = deal.Bid;
            DealName = deal.DealName;
            ImagePaths = deal.ImagePaths;
            AppDealPic = deal.AppDealPic;
            CouponEventName = deal.CouponEventName;
            CouponEventTitle = deal.CouponEventTitle;
            Introduction = deal.Introduction;
            IntroductionTags = deal.IntroductionTags;
            Restrictions = deal.Restrictions;
            Description = deal.Description;
            Availabilities = deal.Availabilities;
            SoldNum = deal.SoldNum;
            SoldNumShow = deal.SoldNumShow;
            ShowUnit = deal.ShowUnit;
            Price = deal.Price;
            OriPrice = deal.OriPrice;
            DiscountString = deal.DiscountString;
            DealEndTime = deal.DealEndTime;
            DealStartTime = deal.DealStartTime;
            SoldOut = deal.SoldOut;
            Remark = deal.Remark;
            MultiDeal = deal.MultiDeal;
            ShoppingCart = deal.ShoppingCart;
            DefaultActivityName = deal.DefaultActivityName;
            TravelPlace = deal.TravelPlace;
            DealTags = deal.DealTags;
            IsFami = deal.IsFami;
            BehaviorType = deal.BehaviorType;
            ShowExchangePrice = deal.ShowExchangePrice;
            ExchangePrice = deal.ExchangePrice;
            IsCollected = deal.IsCollected;
            DiscountCodeUsed = deal.DiscountCodeUsed;
            AtmAvailable = deal.AtmAvailable;
            PiinlifeMenuTabs = deal.PiinlifeMenuTabs;
            PiinlifeRightContent = deal.PiinlifeRightContent;
            IsExperience = deal.IsExperience ?? false;
            ExperienceDealCollectCount = deal.ExperienceDealCollectCount;
            ExchangeDealCount = deal.ExchangeDealCount;
            DiscountType = deal.DiscountType;
            Discount = deal.Discount;
            IsOrdered = false;
            IsBurningDeal = false;
            BurningPoint = 0;
            IconInfo = new SkmDealIcon();
            IsSkmPay = false;
            ComboDeals = new List<SkmApiComboDeal>();
            DeliveryEndTime = DeliveryStartTime = string.Empty;
        }
    }

    /// <summary>
    /// 子檔
    /// </summary>
    public class SkmApiComboDeal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deal"></param>
        public SkmApiComboDeal(ViewComboDeal deal)
        {
            Bid = deal.BusinessHourGuid;
            OrderedQuantity = deal.OrderedQuantity;
            OrderTotalLimit = deal.OrderTotalLimit.HasValue ? Convert.ToInt32(deal.OrderTotalLimit) : 0;
            SoldOut = OrderedQuantity >= OrderTotalLimit;
        }

        /// <summary>
        /// 子檔代號
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 館別名稱
        /// </summary>
        public string ShopName { get; set; }
        /// <summary>
        /// 已訂購數量
        /// </summary>
        public int OrderedQuantity { get; set; }
        /// <summary>
        /// 上架數量
        /// </summary>
        public int OrderTotalLimit { get; set; }
        /// <summary>
        /// 是否已售完
        /// </summary>
        public bool SoldOut { get; set; }
        /// <summary>
        /// 限購一次的檔次是否已購買
        /// </summary>
        public bool IsOrdered { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model.API;

namespace LunchKingSite.BizLogic.Models.Skm
{
    public class SkmPponDealDealCheckout
    {
        public SkmPponDealDealCheckout()
        {
            IsBurningEvent = false;
            BurningPoint = 0;
        }

        /// <summary>
        /// 原ApiPponDealCheckOut
        /// </summary>
        public ApiPponDealCheckout PponDealCheckout { get; set; }
        /// <summary>
        /// 是否是燒點活動
        /// </summary>
        public bool IsBurningEvent { get; set; }
        /// <summary>
        /// 需燃點數
        /// </summary>
        public int BurningPoint { get; set; }
        /// <summary>
        /// 是否使用SKM PAY
        /// </summary>
        public bool IsSkmPay { get; set; }
    }
}

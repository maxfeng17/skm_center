﻿using System.Collections.Generic;
using LunchKingSite.BizLogic.Model.API;

namespace LunchKingSite.BizLogic.Models.Skm
{
    /// <summary>
    /// SKM PAY MakeOrder 回應資訊
    /// </summary>
    public class SkmPayMakeOrderModel : ApiFreeDealPayReply
    {
        /// <summary>
        /// API呼叫最多限制次數
        /// </summary>
        public int ApiAccseeLimite { get; set; }
        /// <summary>
        /// 交易日期
        /// </summary>
        public string TransDate { get; set; }
        /// <summary>
        /// 交易金額
        /// </summary>
        public int TradeAmount { get; set; }
        /// <summary>
        /// 刷卡金額
        /// </summary>
        public int CreditCardAmount { get; set; }
        /// <summary>
        /// 紅利折抵金額
        /// </summary>
        public int DiscountAmount { get; set; }
        /// <summary>
        /// 使用會員點數
        /// </summary>
        public int UsedPoint { get; set; }
        /// <summary>
        /// 是否需要OTP驗證
        /// </summary>
        public bool NeedOTP { get; set; }
        /// <summary>
        /// OTP驗證連結
        /// </summary>
        public string OTPUrl { get; set; }
        /// <summary>
        /// 紅利折抵金額
        /// </summary>
        public int RedeemAmt { get; set; }
        /// <summary>
        /// 紅利折抵點數
        /// </summary>
        public int RedeemPoint { get; set; }
        /// <summary>
        /// 剩餘紅利點數
        /// </summary>
        public int PostRedeemPoint { get; set; }
        /// <summary>
        /// 分期期數
        /// </summary>
        public int InstallPeriod { get; set; }
        /// <summary>
        /// 分期首期金額
        /// </summary>
        public int InstallDownPay { get; set; }
        /// <summary>
        /// 分期每期金額
        /// </summary>
        public int InstallEachPay { get; set; }
        /// <summary>
        /// 分期手續費金額
        /// </summary>
        public int InstallFee { get; set; }
    }

    /// <summary>
    /// 查詢SkmPay付款選項 Model
    /// </summary>
    public class GetSkmPaymentOptionRequest
    {
        /// <summary>
        /// 銀行代碼
        /// </summary>
        public string BankNo { get; set; }
        /// <summary>
        /// 聨名卡註記
        /// </summary>
        public string CoBranded { get; set; }
        /// <summary>
        /// 商品售價
        /// </summary>
        public int ItemPrice { get; set; }
        /// <summary>
        /// 檔次代號
        /// </summary>
        public string Bid { get; set; }
    }

    /// <summary>
    /// 回應付款選項 Model
    /// </summary>
    public class GetSkmPaymentOptionResponse
    {
        /// <summary>
        /// 支付選項
        /// </summary>
        public List<int> PaymentOption { get; set; }
        /// <summary>
        /// 分期資訊
        /// </summary>
        public List<SkmPayInstallOption> InstallOption { get; set; }

        public GetSkmPaymentOptionResponse()
        {
            PaymentOption = new List<int>();
            InstallOption = new List<SkmPayInstallOption>();
        }
    }

    /// <summary>
    /// 分期選項
    /// </summary>
    public class SkmPayInstallOption
    {
        /// <summary>
        /// 期數
        /// </summary>
        public int Period { get; set; }
        /// <summary>
        /// 期數說明文字
        /// </summary>
        public string Description { get; set; }
    }

}

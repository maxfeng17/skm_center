﻿using LunchKingSite.Core;
using LunchKingSite.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{
    public class VerificationData
    {
        #region SubClass
        /// <summary>
        /// 核銷展示用的相關資訊
        /// </summary>
        public class DemoInfo : VerificationData
        {
            public static string DemoAlert(string sSeqNo1, string sSeqNo2, string sCode)
            {
                return
                    @"17Life樂園一日遊\n期限：{time}至{time}\n訂購人：一姬來福\n\n憑證號碼：{sequence_1}-{sequence_2}-{code}\n確認核銷此張憑證嗎？"
                    .Replace("{time}", string.Format("{0:yyyy/MM/dd}", DateTime.Today))
                    .Replace("{sequence_1}", sSeqNo1).Replace("{sequence_2}", sSeqNo2)
                    .Replace("{code}", sCode);
            }

            #region 常數參數
            /// <summary>
            /// 模擬的店家資訊物件，共有兩組店家登入帳號，分別代表有檔次以及無檔次之店家
            /// </summary>
            public class Store
            {
                #region 代表有檔次之店家，可登入
                /// <summary>
                /// 代表有檔次之店家帳號
                /// </summary>
                public const string StoreAcct = "DEMO100";
                /// <summary>
                /// 代表有檔次之店家密碼
                /// </summary>
                public const string StorePw = "2431";
                #endregion
                #region 此帳號為無檔次店家代表
                /// <summary>
                /// 無檔次店家帳號
                /// </summary>
                public const string StoreNoDealAcct = "DEMO200";
                /// <summary>
                /// 無檔次店家密碼
                /// </summary>
                public const string StoreNoDealPw = "7014";
                #endregion
            }

            /// <summary>
            /// 四組憑證號碼分別展示四種狀態：可核銷/已核銷/憑證無效/系統錯誤
            /// </summary>
            public class CouponNo
            {
                /// <summary>
                /// 憑證的Sequence No
                /// </summary>
                public const string Sequence = "2521-9131";
                #region 系統錯誤
                /// <summary>
                /// 系統錯誤之Code No.
                /// </summary>
                public const string CodeError = "104410";
                #endregion
                #region 可核銷
                /// <summary>
                /// 可核銷之Code No.
                /// </summary>
                public const string CodeOk = "104411";
                #endregion
                #region 已核銷
                /// <summary>
                /// 已核銷之Code No.
                /// </summary>
                public const string CodeVerified = "104412";
                #endregion
                #region 憑證不在期限內
                /// <summary>
                /// 憑證無效之Code No.
                /// </summary>
                public const string CodeInvalid = "104413";
                #endregion
            }

            public class StoreInfo
            {
                public const string Seller = "17Life - 17Life樂園一日遊";
                public const string Seller_id = "0";
                public const string Store1 = "《承德小屋》 - 17Life樂園一日遊";
                public const string Store1_id = "1";
                public const string Store2 = "《桃園結義》 - 17Life樂園一日遊";
                public const string Store2_id = "2";
            }

            public class DealInfo
            {
                public Dictionary<string, string> deal0 = new Dictionary<string, string>()
                                                               {
                                                                   {"item_name", "17Life樂園一日遊"},
                                                                   {"deal_start", "2011/12/32"},
                                                                   {"deal_end", "2012/12/21"},
                                                                   {"all", "686"},      //全部
                                                                   {"verifed", "380"},  //已核銷
                                                                   {"unverify", "306"}, //未核銷
                                                                   {"return", "0"},     //退貨
                                                                   {"verifed_percent", "55.39%"},
                                                                   {"seller_name", "17Life"},
                                                                   {"store_name", "《17Life》 - 總店"}
                                                               };
                public Dictionary<string, string> deal1 = new Dictionary<string, string>()
                                                               {
                                                                   {"item_name", "溫馨親子四人房"},
                                                                   {"deal_start", "2011/12/32"},
                                                                   {"deal_end", "2012/12/21"},
                                                                   {"all", "340"},      //全部
                                                                   {"verifed", "100"},  //已核銷
                                                                   {"unverify", "240"}, //未核銷
                                                                   {"return", "0"},     //退貨
                                                                   {"verifed_percent", "29.41%"},
                                                                   {"seller_name", "17Life"},
                                                                   {"store_name", "《17Life》 - 承德小屋"}
                                                               };

                public Dictionary<string, string> deal2 = new Dictionary<string, string>()
                                                               {
                                                                   {"item_name", "熱情情侶雙人房"},
                                                                   {"deal_start", "2011/12/32"},
                                                                   {"deal_end", "2012/12/21"},
                                                                   {"all", "346"},      //全部
                                                                   {"verifed", "280"},  //已核銷
                                                                   {"unverify", "66"}, //未核銷
                                                                   {"return", "0"},     //退貨
                                                                   {"verifed_percent", "80.92%"},
                                                                   {"seller_name", "17Life"},
                                                                   {"store_name", "《17Life》 - 桃園結義"}
                                                               };
            }

            public List<VerificationListInfo> GetInfo(int type)
            {
                var infos = new List<VerificationListInfo>();
                var info = new VerificationListInfo(VerificationQueryStatus.Init);
                switch ((VerificationQueryStatus)type)
                {
                    case VerificationQueryStatus.Init:
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0001-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0002-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0003-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0004-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0005-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0006-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0007-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0008-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0009-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0010-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0011-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0012-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0013-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0014-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0015-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0016-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0017-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0018-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0019-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0020-0000", Status = "未核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0021-0000", Status = "未核銷" });
                        break;
                    case VerificationQueryStatus.Verified:
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0022-0000", Time = "2011/12/32 23:61", Status = "已核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0023-0000", Time = "2012/02/30 12:61", Status = "已核銷" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0024-0000", Time = "2012/04/31 08:88", Status = "已核銷" });
                        break;
                    case VerificationQueryStatus.Refund:
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0025-0000", Time = "2010/12/32 23:61", Status = "退貨" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0026-0000", Time = "2011/02/30 12:61", Status = "退貨" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0027-0000", Time = "2011/04/31 08:88", Status = "退貨" });
                        infos.Add(new VerificationListInfo(VerificationQueryStatus.Init) { SequenceCode = "0028-0000", Time = "2011/09/31 16:88", Status = "退貨" });
                        break;
                }
                return infos;
            }

            #endregion
            
        }
        #endregion

        #region Method
        public static string CheckDemoStoreLoginCode()
        {
            return GetCheckCode(DemoInfo.Store.StoreAcct);
        }

        protected static string GetCheckCode(string sKey)
        {
            string retCode = string.Empty;
            string sToday = string.Format("{0:yyyy/MM/dd}", DateTime.Today);
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] b = md5.ComputeHash(Encoding.UTF8.GetBytes("17" + sToday + sKey + "life"));
            retCode = BitConverter.ToString(b).Replace("-", string.Empty);
            return retCode;
        }

        #endregion

    }

    #region Object

    public class VerificationListInfo
    {
        public string SequenceCode { get; set; }
        public string Time { get; set; }
        public string Status { get; set; }
        private VerificationQueryStatus _enumQueryStatus;
        private static readonly string TimeFormat = @"{0:yyyy/MM/dd HH:mm}";

        #region constructor
        public VerificationListInfo(VerificationQueryStatus enumType)
        {
            _enumQueryStatus = enumType;
        }
        #endregion
        #region method
        public void SetValue(string strSequenceCode, DateTime dtVerifiedTime, DateTime dtModifyTime, TrustStatus enumStatus)
        {
            SequenceCode = strSequenceCode;
            switch (_enumQueryStatus)
            {
                case VerificationQueryStatus.Verified:
                    Time = string.Format(TimeFormat, dtVerifiedTime);
                    break;
                case VerificationQueryStatus.Refund:
                    Time = string.Format(TimeFormat, dtModifyTime);
                    break;
                default:
                    Time = string.Empty;
                    break;
            }
            SetStatus(enumStatus);
        }
        public void SetStatus(TrustStatus enumStatus)
        {
            switch (enumStatus)
            {
                case TrustStatus.Verified:
                    Status = "已核銷";
                    break;
                case TrustStatus.Refunded:
                case TrustStatus.Returned:
                    Status = "退貨";
                    break;
                default:
                    Status = "未使用";
                    break;
            }
        }
        
        #endregion
    }

    #endregion
}

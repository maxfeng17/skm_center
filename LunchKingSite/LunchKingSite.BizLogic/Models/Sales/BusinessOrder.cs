﻿using Vodka.Mongo;
using System;
using System.Collections.Generic;
using MongoDB.Bson;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Model.Sales
{
    public class BusinessOrder : Entity
    {
        /// <summary>
        /// 流水號
        /// </summary>
        public int SequenceNumber { get; set; }
        /// <summary>
        /// 工單編號(部門代號+流水號)
        /// </summary>
        public string BusinessOrderId { get; set; }
        /// <summary>
        /// 接續憑證BID
        /// </summary>
        public Guid AncestorSequenceBusinessHourGuid { get; set; }
        /// <summary>
        /// 接續數量BID
        /// </summary>
        public Guid AncestorQuantityBusinessHourGuid { get; set; }
        /// <summary>
        /// bid
        /// </summary>
        public Guid BusinessHourGuid { get; set; }
        /// <summary>
        /// did
        /// </summary>
        public int DealId { get; set; }
        /// <summary>
        /// 檔號
        /// </summary>
        public int UniqueId { get; set; }
        /// <summary>
        /// Seller Guid
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 賣家編號
        /// </summary>
        public string SellerId { get; set; }
        /// <summary>
        /// 上檔日期
        /// </summary>
        public DateTime? BusinessHourTimeS { get; set; }
        /// <summary>
        /// 工單類型
        /// </summary>
        public BusinessType BusinessType { get; set; }
        /// <summary>
        /// 賣家規模
        /// </summary>
        public BusinessOrderSellerLevel SellerLevel { get; set; }
        /// <summary>
        /// 是否為旅遊類型
        /// </summary>
        public bool IsTravelType { get; set; }
        /// <summary>
        /// 關聯工單
        /// </summary>
        public ObjectId CopyBusinessHourGuid { get; set; }
        /// <summary>
        /// 關聯工單編號
        /// </summary>
        public string CopyBusinessOrderId { get; set; }
        /// <summary>
        /// 關聯工單類型
        /// </summary>
        public BusinessCopyType CopyType { get; set; }
        /// <summary>
        /// 接續憑證號碼
        /// </summary>
        public bool IsContinuedSequence { get; set; }
        /// <summary>
        /// 接續銷售數字(前台顯示) 
        /// </summary>
        public bool IsContinuedQuantity { get; set; }
        /// <summary>
        /// 是否已送交合約
        /// </summary>
        public bool IsContractSend { get; set; }
        /// <summary>
        /// 合約未回理由
        /// </summary>
        public string NoContractReason { get; set; }
        /// <summary>
        /// 活動歸屬部門
        /// </summary>
        public string AuthorizationGroup { get; set; }
        /// <summary>
        /// 旅遊區域
        /// </summary>
        public int[] TravelCategory { get; set; }
        /// <summary>
        /// 生活商圈
        /// </summary>
        public Dictionary<int, int[]> CommercialCategory { get; set; }
        /// <summary>
        /// 業務區域ID
        /// </summary>
        public string SalesDeptId { get; set; }
        /// <summary>
        /// 業務區域名稱
        /// </summary>
        public string SalesDeptName { get; set; }
        /// <summary>
        /// 活動區域
        /// </summary>
        public string PponCity { get; set; }
        /// <summary>
        /// 其他活動區域
        /// </summary>
        public string OtherPponCity { get; set; }
        /// <summary>
        /// 業績歸屬館別
        /// </summary>
        public string AccBusinessGroupId { get; set; }
        /// <summary>
        /// 負責業務
        /// </summary>
        public string SalesName { get; set; }
        /// <summary>
        /// 業務電話
        /// </summary>
        public string SalesPhone { get; set; }
        /// <summary>
        /// 搜尋關鍵字
        /// </summary>
        public string Keywords { get; set; }
        /// <summary>
        /// 建議文案重點1
        /// </summary>
        public string Important1 { get; set; }
        /// <summary>
        /// 建議文案重點2
        /// </summary>
        public string Important2 { get; set; }
        /// <summary>
        /// 建議文案重點3
        /// </summary>
        public string Important3 { get; set; }
        /// <summary>
        /// 建議文案重點4
        /// </summary>
        public string Important4 { get; set; }
        /// <summary>
        /// 照片來源
        /// </summary>
        public string PictureSource { get; set; }
        /// <summary>
        /// 行銷活動
        /// </summary>
        public string MarketEvent { get; set; }
        /// <summary>
        /// 名單來源
        /// </summary>
        public string SourceType { get; set; }
        /// <summary>
        /// 核銷方式
        /// </summary>
        public string VerifyType { get; set; }
        /// <summary>
        /// 核銷方式
        /// </summary>
        public PponVerifyType PponVerifyType { get; set; }
        /// <summary>
        /// 是否申請新制行銷資源
        /// </summary>
        public bool IsApplyNewPromotion { get; set; }
        /// <summary>
        /// 是否申請新制行銷資源符合資格
        /// </summary>
        public bool IsApplyNewPromotionConform { get; set; }
        /// <summary>
        /// 是否有刷卡機
        /// </summary>
        public bool IsEDC { get; set; }
        /// <summary>
        /// 有裝設刷卡機意願
        /// </summary>
        public bool IsKownEDC { get; set; }
        /// <summary>
        /// 是否想了解台新銀行刷卡機之優惠
        /// </summary>
        public bool IsKownTaiShinEDC { get; set; }
        /// <summary>
        /// 每月平均營業額
        /// </summary>
        public string EdcDesc1 { get; set; }
        /// <summary>
        /// 每月平均刷卡量
        /// </summary>
        public string EdcDesc2 { get; set; }
        /// <summary>
        /// 原配合銀行
        /// </summary>
        public string EdcDesc3 { get; set; }
        /// <summary>
        /// 原配合銀行手續費
        /// </summary>
        public string EdcDesc4 { get; set; }
        /// <summary>
        /// 方便聯絡時間/聯絡人
        /// </summary>
        public string EdcDesc5 { get; set; }
        /// <summary>
        /// 公單審核狀態
        /// </summary>
        public BusinessOrderStatus Status { get; set; }
        /// <summary>
        /// 品牌名稱
        /// </summary>
        public string SellerName { get; set; }
        /// <summary>
        /// 簽約公司名稱
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// 私人簽約
        /// </summary>
        public bool IsPrivateContract { get; set; }
        /// <summary>
        /// 統一編號
        /// </summary>
        public string GuiNumber { get; set; }
        /// <summary>
        /// 負責人
        /// </summary>
        public string BossName { get; set; }
        /// <summary>
        /// 身分證字號
        /// </summary>
        public string PersonalId { get; set; }
        /// <summary>
        /// 聯絡人
        /// </summary>
        public string ContactPerson { get; set; }
        /// <summary>
        /// 行動電話
        /// </summary>
        public string BossTel { get; set; }
        /// <summary>
        /// 連絡電話
        /// </summary>
        public string BossPhone { get; set; }
        /// <summary>
        /// 傳真電話
        /// </summary>
        public string BossFax { get; set; }
        /// <summary>
        /// 電子信箱
        /// </summary>
        public string BossEmail { get; set; }
        /// <summary>
        /// 簽約公司地址
        /// </summary>
        public string BossAddress { get; set; }
        /// <summary>
        /// 退換貨連絡人
        /// </summary>
        public string ReturnedPerson { get; set; }
        /// <summary>
        /// 退換貨連絡人電話
        /// </summary>
        public string ReturnedTel { get; set; }
        /// <summary>
        /// 退換貨連絡人電子信箱
        /// </summary>
        public string ReturnedEmail { get; set; }
        /// <summary>
        /// 民宿登記證字號
        /// </summary>
        public string HotelId { get; set; }
        /// <summary>
        /// 所在地警察局電話
        /// </summary>
        public string PoliceTel { get; set; }
        /// <summary>
        /// 分店資訊
        /// </summary>
        public List<SalesStore> Store { get; set; }
        /// <summary>
        /// 帳務聯絡人
        /// </summary>
        public string AccountingName { get; set; }
        /// <summary>
        /// 帳務連絡人電話
        /// </summary>
        public string AccountingTel { get; set; }
        /// <summary>
        /// 帳務聯絡人電子信箱
        /// </summary>
        public string AccountingEmail { get; set; }
        /// <summary>
        /// 匯款銀行
        /// </summary>
        public string AccountingBank { get; set; }
        /// <summary>
        /// 銀行代號
        /// </summary>
        public string AccountingBankId { get; set; }
        /// <summary>
        /// 分 行
        /// </summary>
        public string AccountingBranch { get; set; }
        /// <summary>
        /// 分行代號
        /// </summary>
        public string AccountingBranchId { get; set; }
        /// <summary>
        /// 統一編號／身份證字號
        /// </summary>
        public string AccountingId { get; set; }
        /// <summary>
        /// 戶名
        /// </summary>
        public string Accounting { get; set; }
        /// <summary>
        /// 帳號
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 出帳類型
        /// </summary>
        public string RemittanceType { get; set; }
        /// <summary>
        /// 匯款資料
        /// </summary>
        public DealAccountingPayType PayToCompany { get; set; }
        /// <summary>
        /// 出帳方式
        /// </summary>
        public RemittanceType RemittancePaidType { get; set; }
        /// <summary>
        /// 使用代收轉付
        /// </summary>
        public bool IsEntrustSell { get; set; }
        /// <summary>
        /// 提供國旅卡付款
        /// </summary>
        public bool IsTravelcard { get; set; }
        /// <summary>
        /// 信託華泰銀行
        /// </summary>
        public bool IsTrustHwatai { get; set; }
        /// <summary>
        /// 配合墨攻核銷系統
        /// </summary>
        public bool IsMohistVerify { get; set; }
        /// <summary>
        /// 可提供憑證
        /// </summary>
        public Invoice Invoice { get; set; }
        /// <summary>
        /// 發票開立人類型
        /// </summary>
        public InvoiceFounderType InvoiceFounderType { get; set; }
        /// <summary>
        /// 發票開立人-公司名稱
        /// </summary>
        public string InvoiceFounderCompanyName { get; set; }
        /// <summary>
        /// 發票開立人-統編 / ID
        /// </summary>
        public string InvoiceFounderCompanyID { get; set; }
        /// <summary>
        /// 發票開立人-負責人
        /// </summary>
        public string InvoiceFounderBossName { get; set; }
        /// <summary>
        /// 多檔次
        /// </summary>
        public string MultiDeal { get; set; }
        /// <summary>
        /// 好康
        /// </summary>
        public IList<Deal> Deals { get; set; }
        /// <summary>
        /// 宅配商品
        /// </summary>
        public IList<DealProduct> Products { get; set; }
        /// <summary>
        /// 旅遊
        /// </summary>
        public IList<DealTravel> Travels { get; set; }

        /// <summary>
        /// 店名
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public int AddressCityId { get; set; }
        /// <summary>
        /// 鄉鎮市區
        /// </summary>
        public int AddressTownshipId { get; set; }
        /// <summary>
        /// 店址
        /// </summary>
        public string SellerAddress { get; set; }
        /// <summary>
        /// 訂位電話
        /// </summary>
        public string ReservationTel { get; set; }
        /// <summary>
        /// 訂房電話
        /// </summary>
        public string HotelReservationTel { get; set; }
        /// <summary>
        /// 營業時間
        /// </summary>
        public string OpeningTime { get; set; }
        /// <summary>
        /// 公休日
        /// </summary>
        public string CloseDate { get; set; }
        /// <summary>
        /// 交通資訊-捷運
        /// </summary>
        public string Mrt { get; set; }
        /// <summary>
        /// 交通資訊-開車
        /// </summary>
        public string Car { get; set; }
        /// <summary>
        /// 交通資訊-公車
        /// </summary>
        public string Bus { get; set; }
        /// <summary>
        /// 交通資訊
        /// </summary>
        public string VehicleInfo { get; set; }
        /// <summary>
        /// 官網網址
        /// </summary>
        public string WebUrl { get; set; }
        /// <summary>
        /// FaceBook網址	
        /// </summary>
        public string FBUrl { get; set; }
        /// <summary>
        /// Plurk網址
        /// </summary>
        public string PlurkUrl { get; set; }
        /// <summary>
        /// 部落格網址
        /// </summary>
        public string BlogUrl { get; set; }
        /// <summary>
        /// 其他網址
        /// </summary>
        public string OtherUrl { get; set; }
        /// <summary>
        /// 特選店家不露出
        /// </summary>
        public bool IsShowSpecailStore { get; set; }

        /// <summary>
        /// 運費代收單位
        /// </summary>
        public string FreightType { get; set; }
        /// <summary>
        /// 公司配合之貨運公司
        /// </summary>
        public string FreightCompany { get; set; }
        /// <summary>
        /// 貨運公司
        /// </summary>
        public string FreightOtherCompany { get; set; }
        /// <summary>
        /// 離島地區(金/馬/澎)是否配送
        /// </summary>
        public bool IslandsFreight { get; set; }
        /// <summary>
        /// 配送類型
        /// </summary>
        public string FreightMode { get; set; }
        /// <summary>
        /// 商品瑕疵證明(是否須拍照)
        /// </summary>
        public bool IsTakePicture { get; set; }
        /// <summary>
        /// 是否為平行輸入
        /// </summary>
        public bool IsParallelProduct { get; set; }
        /// <summary>
        /// 是否為即期品
        /// </summary>
        public bool IsExpiringItems { get; set; }
        /// <summary>
        /// 即期品有效期限(起)
        /// </summary>
        public string ExpiringItemsDateStart { get; set; }
        /// <summary>
        /// 即期品有效期限(迄)
        /// </summary>
        public string ExpiringItemsDateEnd { get; set; }
        /// <summary>
        /// 保存方式 / 期限
        /// </summary>
        public string ProductPreservation { get; set; }
        /// <summary>
        /// 功能 / 使用方法
        /// </summary>
        public string ProductUse { get; set; }
        /// <summary>
        /// 規格 ( 重量 / 淨重 / 內容量 / 數量 )
        /// </summary>
        public string ProductSpec { get; set; }
        /// <summary>
        /// 主要成份 ( 美妝商品填寫 )
        /// </summary>
        public string ProductIngredients { get; set; }
        /// <summary>
        /// 核準字號 ( 美妝商品填寫 )
        /// </summary>
        public string SanctionNumber { get; set; }
        /// <summary>
        /// 商品檔-最早出貨日上檔後+天數
        /// </summary>
        public int ProductUseDateStartSet { get; set; }
        /// <summary>
        /// 商品檔-最晚出貨日上檔後+天數
        /// </summary>
        public int ProductUseDateEndSet { get; set; }
        /// <summary>
        /// 檢驗規定
        /// </summary>
        public string InspectRule { get; set; }
        /// <summary>
        /// 退貨方式
        /// </summary>
        public ReturnType ReturnType { get; set; }
        /// <summary>
        /// 是否不可使用簡訊憑證
        /// </summary>
        public bool IsNotAccessSms { get; set; }
        /// <summary>
        /// 合約條款
        /// </summary>
        public AdditionalContracts AdditionalContracts { get; set; }
        /// <summary>
        /// 其他附註說明
        /// </summary>
        public string OtherMemo { get; set; }

        /// <summary>
        /// 刷卡機商店代號
        /// </summary>
        public string CreditCardReaderId { get; set; }
        /// <summary>
        /// 平均消費
        /// </summary>
        public string PerCustomerTrans { get; set; }
        /// <summary>
        /// 平均消費描述
        /// </summary>
        public string PerCustomerTransDesc { get; set; }
        /// <summary>
        /// 平均消費-其他
        /// </summary>
        public string PerCustomerTransOthers { get; set; }
        /// <summary>
        /// 選擇彙整
        /// </summary>
        public string PreferentialItems { get; set; }
        /// <summary>
        /// 是否配合雜誌合作
        /// </summary>
        public bool IsMagazineInfo { get; set; }
        /// <summary>
        /// 介紹版位
        /// </summary>
        public string SellerTemplate { get; set; }
        /// <summary>
        /// 介紹版位描述
        /// </summary>
        public string SellerTemplateDesc { get; set; }
        /// <summary>
        /// 店家介紹
        /// </summary>
        public string SellerDescription { get; set; }
        /// <summary>
        /// 需另提供或確認事項
        /// </summary>
        public string MagazineNotice1 { get; set; }
        /// <summary>
        /// 需特別注意或排除事項
        /// </summary>
        public string MagazineNotice2 { get; set; }
        /// <summary>
        /// 雜誌年度
        /// </summary>
        public int MagazineYear { get; set; }
        /// <summary>
        /// 雜誌月份
        /// </summary>
        public List<int> MagazineMonth { get; set; }
        /// <summary>
        /// 優惠折扣項目
        /// </summary>
        public IList<Preferential> Preferential { get; set; }
        /// <summary>
        /// 是否註記歷史紀錄
        /// </summary>
        public bool IsNoteHistory { get; set; }
        /// <summary>
        /// 歷史紀錄
        /// </summary>
        public List<History> HistoryList { get; set; }
        /// <summary>
        /// 好康條款
        /// </summary>
        public Dictionary<ObjectId[], string> ProvisionList { get; set; }
        /// <summary>
        /// 特店折扣
        /// </summary>
        public string PezStore { get; set; }
        /// <summary>
        /// 特店折扣注意事項
        /// </summary>
        public string PezStoreNotice { get; set; }
        /// <summary>
        /// 特店折扣合約起日
        /// </summary>
        public DateTime? PezStoreDateStart { get; set; }
        /// <summary>
        /// 特店折扣合約迄日期
        /// </summary>
        public DateTime? PezStoreDateEnd { get; set; }
        /// <summary>
        /// 建立人員
        /// </summary>
        public string CreateId { get; set; }
        /// <summary>
        /// 建立日期
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 修改人員
        /// </summary>
        public string ModifyId { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 出貨方式
        /// </summary>
        public ShipType ShipType { get; set; }
        /// <summary>
        /// 出貨日期格式
        /// </summary>
        public ShippingDateType ShippingDateType { get; set; }
        /// <summary>
        /// 出貨日期
        /// </summary>
        public int ShippingDate { get; set; }
        /// <summary>
        /// 檔次標籤
        /// </summary>
        public string LabelTagList { get; set; }
        /// <summary>
        /// Icon標籤
        /// </summary>
        public string LabelIconList { get; set; }
        /// <summary>
        /// 新版上檔頻道
        /// </summary>
        public Dictionary<int, List<int>> SelectedChannelCategories { get; set; }

        public Dictionary<int, List<int>> SelectedChannelSpecialRegionCategories { get; set; }

        public Dictionary<int, List<int>> SelectedChannelStandardRegionCategories { get; set; }

        /// <summary>
        /// 是否為app限定購買檔次
        /// </summary>
        public bool IsAppLimitedEdition { set; get; }

        /// <summary>
        /// DealCategory
        /// </summary>
        public List<int> SelectedDealCategories { get; set; }

        /// <summary>
        /// 銷售類別分析
        /// </summary>
        public int? DealType { get; set; }

        /// <summary>
        /// 長期合約
        /// </summary>
        public bool IsLongContract { set; get; }

        public BusinessOrder()
        {
            ProductUseDateStartSet = 5;
            ProductUseDateEndSet = 10;
            ShippingDate = 7;
            CommercialCategory = new Dictionary<int, int[]>();
            Status = BusinessOrderStatus.Temp;
            CopyType = BusinessCopyType.None;
            SellerLevel = BusinessOrderSellerLevel.None;
            InvoiceFounderType = InvoiceFounderType.Company;
            Invoice = new Invoice();
            Deals = new List<Deal>();
            Products = new List<DealProduct>();
            Travels = new List<DealTravel>();
            Preferential = new List<Preferential>();
            HistoryList = new List<History>();
            MagazineMonth = new List<int>();
            ProvisionList = new Dictionary<ObjectId[], string>();
            Store = new List<SalesStore>();
            ReturnType = ReturnType.Access;
            PponVerifyType = Sales.PponVerifyType.OnlineOnly;
            AdditionalContracts = new AdditionalContracts();
            ShipType = Sales.ShipType.Normal;
            ShippingDateType = Sales.ShippingDateType.Special;
            LabelTagList = string.Empty;
            LabelIconList = string.Empty;
            SelectedChannelCategories = new Dictionary<int, List<int>>();
            SelectedChannelSpecialRegionCategories = new Dictionary<int, List<int>>();
            SelectedChannelStandardRegionCategories = new Dictionary<int, List<int>>();
            IsAppLimitedEdition = IsLongContract = false;
            SelectedDealCategories = new List<int>();
        }
    }

    public enum BusinessCopyType
    {
        /// <summary>
        /// 
        /// </summary>
        None,
        /// <summary>
        /// 一般
        /// </summary>
        General,
        /// <summary>
        /// 再次上檔
        /// </summary>
        Again,
    }

    public enum BusinessType
    {
        /// <summary>
        /// 
        /// </summary>
        None,
        /// <summary>
        /// 好康
        /// </summary>
        Ppon,
        /// <summary>
        /// 宅配
        /// </summary>
        Product,
    }

    /// <summary>
    /// 賣家規模
    /// </summary>
    public enum BusinessOrderSellerLevel
    {
        [Localization("SellerLevelBig")]
        Big,
        [Localization("SellerLevelMedium")]
        Medium,
        [Localization("SellerLevelSmall")]
        Small,
        [Localization("SellerLevelA")]
        A,
        [Localization("SellerLevelB")]
        B,
        [Localization("SellerLevelC")]
        C,
        None
    }

    /// <summary>
    /// 照片來源
    /// </summary>
    public enum PictureSourceType
    {
        /// <summary>
        /// 請攝影師前往拍攝
        /// </summary>
        [Localization("PictureSourceTypePhotography")]
        Photography = 0,
        /// <summary>
        /// 使用過往檔次照片
        /// </summary>
        [Localization("PictureSourceTypePastPicture")]
        PastPicture = 1,
        /// <summary>
        /// 店家提供
        /// </summary>
        [Localization("PictureSourceTypeSellerProvide")]
        SellerProvide = 2
    }

    public enum BusinessOrderStatus
    {
        /// <summary>
        /// 草稿
        /// </summary>
        Temp = 0,
        /// <summary>
        /// 送審
        /// </summary>
        Send = 1,
        /// <summary>
        /// 核准
        /// </summary>
        Approve = 2,
        /// <summary>
        /// 已複查
        /// </summary>
        Review = 3,
        /// <summary>
        /// 已排檔
        /// </summary>
        Await = 4,
        /// <summary>
        /// 已建檔
        /// </summary>
        Create = 5,
    }

    public enum InvoiceType
    {
        /// <summary>
        /// 收銀機統一發票
        /// </summary>
        [Localization("BusinessOrderInvoiceTypeGeneral")]
        General = 0,
        /// <summary>
        /// 三聯式統一發票
        /// </summary>
        [Localization("BusinessOrderInvoiceTypeDuplicateUniformInvoice")]
        DuplicateUniformInvoice = 1,
        /// <summary>
        /// 免用統一發票收據
        /// </summary>
        [Localization("BusinessOrderInvoiceTypeNonInvoice")]
        NonInvoice = 2,
        /// <summary>
        /// 其它
        /// </summary>
        [Localization("BusinessOrderInvoiceTypeOther")]
        Other = 3,
        /// <summary>
        /// 已含18%娛樂稅，不開立發票
        /// </summary>
        [Localization("BusinessOrderInvoiceTypeEntertainmentTax")]
        EntertainmentTax = 4,
    }

    public enum StartUseUnitType
    {
        Day = 0,
        Month = 1,
        BeforeDay = 2,
    }

    public enum ReturnType
    {
        /// <summary>
        /// 全程接受退貨
        /// </summary>
        Access = 0,
        /// <summary>
        /// 不接受退貨
        /// </summary>
        NoRefund = 1,
        /// <summary>
        /// 結檔後七天不能退貨
        /// </summary>
        DaysNoRefund = 2,
        /// <summary>
        /// 過期不能退貨
        /// </summary>
        ExpireNoRefund = 3,
        /// <summary>
        /// 演出時間前十日內不能退貨
        /// </summary>
        NoRefundBeforeDays = 4,
    }

    public enum PponVerifyType
    {
        /// <summary>
        /// 即買即用
        /// </summary>
        [Localization("BusinessOrderVerifyTypeOnlineUse")]
        OnlineUse,
        /// <summary>
        /// 線上核銷
        /// </summary>
        [Localization("BusinessOrderVerifyTypeOnlineOnly")]
        OnlineOnly,
        /// <summary>
        /// 紙本清冊搭配專人核銷
        /// </summary>
        [Localization("BusinessOrderVerifyTypeInventoryOnly")]
        InventoryOnly,
    }

    /// <summary>
    /// 核銷方式
    /// </summary>
    public enum VerifyActionType
    {
        None = 0,
        /// <summary>
        /// 一般(正向)核銷
        /// </summary>
        General = 1,
        /// <summary>
        /// 反向核銷
        /// </summary>
        Reverse = 2
    }


    public enum AdditionalContractsType
    {
        /// <summary>
        /// 商品出貨/回報/付款
        /// </summary>
        [Localization("BusinessOrderAdditionalContractsTypeProductRefund")]
        ProductRefund,
    }

    /// <summary>
    /// 出貨方式
    /// </summary>
    public enum ShipType
    {
        /// <summary>
        /// 一般出貨
        /// </summary>
        [Localization("BusinessOrderShipTypeNormal")]
        Normal = -1,
        /// <summary>
        /// 24小時到貨
        /// </summary>
        [Localization("BusinessOrderShipTypeArriveIn24Hrs")]
        ArriveIn24Hrs = 0,
        /// <summary>
        /// 72小時到貨
        /// </summary>
        [Localization("BusinessOrderShipTypeArriveIn72Hrs")]
        ArriveIn72Hrs = 1,
    }

    public enum ShippingDateType
    {
        /// <summary>
        /// 最早出貨天數
        /// </summary>
        [Localization("ShippingDateNormal")]
        Normal = 0,
        /// <summary>
        /// 訂單成立後天數
        /// </summary>
        [Localization("ShippingDateSpecial")]
        Special = 1,

    }

    public class Invoice : Entity
    {
        /// <summary>
        /// 可提供憑證類型
        /// </summary>
        public InvoiceType Type { get; set; }
        /// <summary>
        /// 名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 稅額
        /// </summary>
        public string InTax { get; set; }
        /// <summary>
        /// 其他
        /// </summary>
        public string Desc { get; set; }

        public Invoice()
        {
            Type = InvoiceType.General;
        }
    }

    public class Deal : Entity
    {
        /// <summary>
        /// 賣價
        /// </summary>
        public string ItemPrice { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public string OrignPrice { get; set; }
        /// <summary>
        /// 優惠活動
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// 多重選項
        /// </summary>
        public string MultOption { get; set; }
        /// <summary>
        /// 多重選項參見附件
        /// </summary>
        public bool IsReferenceAttachment { get; set; }
        /// <summary>
        /// 最低門檻數量
        /// </summary>
        public string MinQuentity { get; set; }
        /// <summary>
        /// 最高購買數量
        /// </summary>
        public string MaxQuentity { get; set; }
        /// <summary>
        /// 每人限購
        /// </summary>
        public string LimitPerQuentity { get; set; }
        /// <summary>
        /// 活動使用有效時間
        /// </summary>
        public string StartMonth { get; set; }
        /// <summary>
        /// 活動使用有效時間單位(年/月)
        /// </summary>
        public StartUseUnitType StartUseUnit { get; set; }
        /// <summary>
        /// 是否特殊使用日期限制
        /// </summary>
        public bool IsRestrictionRule { get; set; }
        /// <summary>
        /// 特殊使用日期限制
        /// </summary>
        public string RestrictionRuleDesc { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public string Freight { get; set; }
        /// <summary>
        /// 免運門檻
        /// </summary>
        public string NonFreightLimit { get; set; }
        /// <summary>
        /// 是否貨到付運費
        /// </summary>
        public bool IsFreightToCollect { get; set; }
        /// <summary>
        /// 是否無法配送外島
        /// </summary>
        public bool IsNotDeliveryIslands { get; set; }
        /// <summary>
        /// 使用ATM
        /// </summary>
        public bool IsATM { get; set; }
        /// <summary>
        /// ATM保留份數
        /// </summary>
        public string AtmQuantity { get; set; }
        /// <summary>
        /// 佣金(發票對開)
        /// </summary>
        public int Commission { get; set; }
        /// <summary>
        /// 上架費份數
        /// </summary>
        public string SlottingFeeQuantity { get; set; }
        /// <summary>
        /// 買斷票券
        /// </summary>
        public bool BuyoutCoupon { get; set; }
        /// <summary>
        /// 進貨價格
        /// </summary>
        public string PurchasePrice { get; set; }
        /// <summary>
        /// 含運
        /// </summary>
        public bool IsIncludeFreight { get; set; }
        /// <summary>
        /// 檔次是否配合訂位系統(0:不配合 1:配合憑證 2:配合旅遊)
        /// </summary>
        public int BookingSystemType { get; set; }
        /// <summary>
        /// 訂位系統提前預約天數
        /// </summary>
        public int AdvanceReservationDays { get; set; }
        /// <summary>
        /// 優惠券使用人數(一張一人 Or 一張多人)
        /// </summary>
        public int CouponUsers { get; set; }
        /// <summary>
        /// 住宿類，商家系統提供「鎖定憑證」功能
        /// </summary>
        public bool IsReserveLock { set; get; }

        public Deal()
        {
            IsRestrictionRule = false;
            StartUseUnit = StartUseUnitType.Day;
            BookingSystemType = (int)BookingType.None;
            AdvanceReservationDays = 0;
            CouponUsers = 0;
            IsReserveLock = false;
        }
    }

    public class DealProduct : Entity
    {
        /// <summary>
        /// 優惠活動$
        /// </summary>
        public string ItemPrice { get; set; }
        /// <summary>
        /// 最低門檻數量
        /// </summary>
        public string MinQuentity { get; set; }
        /// <summary>
        /// 最高購買數量
        /// </summary>
        public string MaxQuentity { get; set; }
        /// <summary>
        /// 每人限購
        /// </summary>
        public string LimitPerQuentity { get; set; }
        /// <summary>
        /// 使用ATM
        /// </summary>
        public bool IsATM { get; set; }
        /// <summary>
        /// ATM保留份數
        /// </summary>
        public string AtmQuantity { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public string Freight { get; set; }
        /// <summary>
        /// 階梯式運費
        /// </summary>
        public string FreightSet { get; set; }
        /// <summary>
        /// 免運門檻
        /// </summary>
        public string NonFreightLimit { get; set; }
        /// <summary>
        /// 進貨價格
        /// </summary>
        public string PurchasePrice { get; set; }
        /// <summary>
        /// 含運
        /// </summary>
        public bool IsIncludeFreight { get; set; }

        public DealProduct()
        {

        }
    }

    public class DealTravel : Entity
    {
        /// <summary>
        /// 優惠活動$
        /// </summary>
        public string ItemPrice { get; set; }
        /// <summary>
        /// 最低門檻數量
        /// </summary>
        public string MinQuentity { get; set; }
        /// <summary>
        /// 最高購買數量
        /// </summary>
        public string MaxQuentity { get; set; }
        /// <summary>
        /// 每人限購
        /// </summary>
        public string LimitPerQuentity { get; set; }
        /// <summary>
        /// 使用ATM
        /// </summary>
        public bool IsATM { get; set; }
        /// <summary>
        /// ATM保留份數
        /// </summary>
        public string AtmQuantity { get; set; }
        /// <summary>
        /// 進貨價格
        /// </summary>
        public string PurchasePrice { get; set; }

        public DealTravel()
        {

        }
    }

    public class History : Entity
    {
        /// <summary>
        /// 異動人員
        /// </summary>
        public string ModifyId { get; set; }
        /// <summary>
        /// 異動日期
        /// </summary>
        public DateTime ModifyTime { get; set; }
        /// <summary>
        /// 異動內容
        /// </summary>
        public string Changeset { get; set; }
        /// <summary>
        /// 是否為重要事項
        /// </summary>
        public bool IsImportant { get; set; }

        public History()
        {
            IsImportant = false;
        }
    }

    public class Preferential : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 選擇項目名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 刊登日期
        /// </summary>
        public List<DateTime> PreferentialDateTime { get; set; }
        /// <summary>
        /// 刊登月份
        /// </summary>
        public List<int> PreferentialMonths { get; set; }
        /// <summary>
        /// 循環使用
        /// </summary>
        public bool EnableUse { get; set; }
        /// <summary>
        /// 優惠內容描述1
        /// </summary>
        public string Preferential1 { get; set; }
        /// <summary>
        /// 優惠內容描述2
        /// </summary>
        public string Preferential2 { get; set; }
        /// <summary>
        /// 優惠內容描述3
        /// </summary>
        public string Preferential3 { get; set; }
        /// <summary>
        /// 注意事項1
        /// </summary>
        public bool IsNotice1 { get; set; }
        /// <summary>
        /// 注意事項描述1
        /// </summary>
        public string NoticeDesc1 { get; set; }
        /// <summary>
        /// 注意事項描述內容1
        /// </summary>
        public string NoticeSubDesc1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNotice2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeDesc2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeSubDesc2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNotice3 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeDesc3 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeSubDesc3 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNotice4 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeDesc4 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeSubDesc4 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNotice5 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeDesc5 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeSubDesc5 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNotice6 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeDesc6 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeSubDesc6 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNotice7 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeDesc7 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeSubDesc7 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNotice8 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeDesc8 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeSubDesc8 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNotice9 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeDesc9 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeSubDesc9 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNotice10 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeDesc10 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeSubDesc10 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNotice11 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeDesc11 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeSubDesc11 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNonApplyHoliday { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNonApplyWeekend { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNonApplyOtherDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NonApplyOtherDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNonApplyOther { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NonApplyOther { get; set; }
        /// <summary>
        /// 優惠內容描述
        /// </summary>
        public string PreferentialDesc { get; set; }
        /// <summary>
        /// 使用說明描述
        /// </summary>
        public List<string> NoticeDescList { get; set; }
        /// <summary>
        /// 不適用描述
        /// </summary>
        public List<string> NonApplyDescList { get; set; }

        public Preferential()
        {
            NoticeDescList = new List<string>();
            NonApplyDescList = new List<string>();
            PreferentialMonths = new List<int>();
            PreferentialDateTime = new List<DateTime>();
        }
    }

    public class AdditionalContracts : Entity
    {
        /// <summary>
        /// 商品出貨/回報/付款
        /// </summary>
        public bool IsProductRefund { get; set; }

        public AdditionalContracts()
        {
            IsProductRefund = false;
        }
    }

    public enum BusinessOrderInvoiceTax
    {
        /// <summary>
        /// 免稅
        /// </summary>
        [Localization("BusinessOrderInvoiceTaxNone")]
        None = 0,
        /// <summary>
        /// 應稅(5%營業稅)
        /// </summary>
        [Localization("BusinessOrderInvoiceTaxIn5Tax")]
        In5Tax = 5,
        /// <summary>
        /// 18%娛樂稅
        /// </summary>
        [Localization("BusinessOrderInvoiceTaxIn18Tax")]
        In18Tax = 18,
    }

    public enum BusinessOrderMultiDeal
    {
        /// <summary>
        /// 單一價格－單品項
        /// </summary>
        [Localization("BusinessOrderMultiDealSinglePriceSingleOption")]
        SinglePriceSingleOption = 0,
        /// <summary>
        /// 單一價格－多品項（例如：襪子有紅/黃/綠三種顏色）
        /// </summary>
        [Localization("BusinessOrderMultiDealSinglePriceMultiOption")]
        SinglePriceMultiOption = 1,
        /// <summary>
        /// 多種價格－每一價格選項單一（例如：大閘蟹四入/八入兩種選項）
        /// </summary>
        [Localization("BusinessOrderMultiDealMultiPriceSingleOption")]
        MultiPriceSingleOption = 2,
        /// <summary>
        /// 多種價格－多種價格且產品多樣（例如：男裝/女裝不同價格又有尺寸不同）
        /// </summary>
        [Localization("BusinessOrderMultiDealMultiPriceMultiOption")]
        MultiPriceMultiOption = 3,
    }

    public enum BusinessOrderInspectRule
    {
        /// <summary>
        /// 檢驗證書 (如：電器商品須有電檢證明；兒童商品須有安全檢驗證明；宅配類食品試市場新聞同步提供檢驗證明(如牛肉乾附上瘦肉精檢驗證明等等))
        /// </summary>
        [Localization("BusinessOrderInspectRuleInspectionCertificate")]
        InspectionCertificate = 0,
        /// <summary>
        /// 品牌名稱 / LOGO / 圖片合法使用之授權 (如是代理商、經銷商或大量採購廠商，提供之商品名稱、LOGO和照片需有原廠授權證明)
        /// </summary>
        [Localization("BusinessOrderInspectRuleAuthorizationCertificate")]
        AuthorizationCertificate = 1,
    }

    public enum BusinessOrderDownloadType
    {
        Word,
        Pdf
    }

    /// <summary>
    /// 發票開立人
    /// </summary>
    public enum InvoiceFounderType
    {
        /// <summary>
        /// 同簽約公司
        /// </summary>
        [Localization("BusinessOrderInvoiceFounderTypeCompany")]
        Company,
        /// <summary>
        /// 同受款人
        /// </summary>
        [Localization("BusinessOrderInvoiceFounderTypeAccounting")]
        Accounting,
        /// <summary>
        /// 其他
        /// </summary>
        [Localization("BusinessOrderInvoiceFounderTypeOthers")]
        Others
    }
}

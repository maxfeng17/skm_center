﻿using Vodka.Mongo;

namespace LunchKingSite.BizLogic.Model.Sales
{
    public class Counter : Entity
    {
        public int IdentifyId { get; set; }
        public CounterType Type { get; set;}
    }

    public enum CounterType
    {
        /// <summary>
        /// 業務工單
        /// </summary>
        BusinessOrder = 0,
    }
}

﻿using Vodka.Mongo;
using System;
using MongoDB.Bson;

namespace LunchKingSite.BizLogic.Model.Sales
{
    public class SalesStore : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public ObjectId BusinessOrderId { get; set; }
        /// <summary>
        /// 分店名稱
        /// </summary>
        public string BranchName { get; set; }
        /// <summary>
        /// 電話
        /// </summary>
        public string StoreTel { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public int AddressCityId { get; set; }
        /// <summary>
        /// 鄉鎮市區
        /// </summary>
        public int AddressTownshipId { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string StoreAddress { get; set; }
        /// <summary>
        /// 營業時間
        /// </summary>
        public string OpeningTime { get; set; }
        /// <summary>
        /// 公休日
        /// </summary>
        public string CloseDate { get; set; }
        /// <summary>
        /// 交通資訊-捷運
        /// </summary>
        public string Mrt { get; set; }
        /// <summary>
        /// 交通資訊-開車
        /// </summary>
        public string Car { get; set; }
        /// <summary>
        /// 交通資訊-公車
        /// </summary>
        public string Bus { get; set; }
        /// <summary>
        /// 交通資訊-其他
        /// </summary>
        public string OV { get; set; }
        /// <summary>
        /// 官網網址
        /// </summary>
        public string WebUrl { get; set; }
        /// <summary>
        /// FaceBook網址	
        /// </summary>
        public string FBUrl { get; set; }
        /// <summary>
        /// Plurk網址
        /// </summary>
        public string PlurkUrl { get; set; }
        /// <summary>
        /// 部落格網址
        /// </summary>
        public string BlogUrl { get; set; }
        /// <summary>
        /// 其他網址
        /// </summary>
        public string OtherUrl { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 簽約公司名稱
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 分店簽約公司ID/統一編號
        /// </summary>
        public string StoreSignCompanyID { get; set; }
        /// <summary>
        /// 分店簽約公司地址
        /// </summary>
        public string StoreCompanyAddress { get; set; }
        /// <summary>
        /// 負責人名稱
        /// </summary>
        public string StoreBossName { get; set; }
        /// <summary>
        /// 受款人ID/統編
        /// </summary>
        public string StoreID { get; set; }
        /// <summary>
        /// 受款人銀行代號
        /// </summary>
        public string StoreBankCode { get; set; }
        /// <summary>
        /// 受款人銀行
        /// </summary>
        public string StoreBankDesc { get; set; }
        /// <summary>
        /// 受款人分行代號
        /// </summary>
        public string StoreBranchCode { get; set; }
        /// <summary>
        /// 受款人分行
        /// </summary>
        public string StoreBranchDesc { get; set; }
        /// <summary>
        /// 受款人帳號
        /// </summary>
        public string StoreAccount { get; set; }
        /// <summary>
        /// 受款人戶名
        /// </summary>
        public string StoreAccountName { get; set; }
        /// <summary>
        /// 帳務聯絡人
        /// </summary>
        public string StoreAccountingName { get; set; }
        /// <summary>
        /// 帳務聯絡人電話
        /// </summary>
        public string StoreAccountingTel { get; set; }
        /// <summary>
        /// 店家email
        /// </summary>
        public string StoreEmail { get; set; }
        /// <summary>
        /// 店家備註
        /// </summary>
        public string StoreNotice { get; set; }
        /// <summary>
        /// 數量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CreateId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ModifyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ModifyTime { get; set; }

        public SalesStore()
        {
            Id = ObjectId.GenerateNewId();
        }
    }
}

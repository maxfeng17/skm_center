﻿using Vodka.Mongo;

namespace LunchKingSite.BizLogic.Model.Sales
{
    public class SalesMember : Entity
    {
        public string SalesId { get; set; }
        public string Area { get; set; }
        public Level Level { get; set; }

        public SalesMember()
        {

        }
    }

    public enum Level
    {
        /// <summary>
        /// 專員
        /// </summary>
        Specialist = 0,
        /// <summary>
        /// 區主管
        /// </summary>
        Leader = 1,
        /// <summary>
        /// 協理
        /// </summary>
        Manager = 2,
    }
}

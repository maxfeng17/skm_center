﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.PchomeChannel
{
    /// <summary>
    /// 電子票券商品
    /// </summary>
    public class PChomeETicketProd
    {
        /// <summary>
        /// 商品編號
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 群組編號
        /// </summary>
        public string GroupId { get; set; }
        /// <summary>
        /// 廠商料號
        /// </summary>
        public string VendorPId { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 前端品名(銷售口號)
        /// </summary>
        public string Nick { get; set; }
        /// <summary>
        /// 價格
        /// </summary>
        public ProdPrice Price { get; set; }
        /// <summary>
        /// 圖片位置
        /// </summary>
        public ProdPic Pic { get; set; }
        /// <summary>
        /// 商品數量(可賣量)???
        /// </summary>
        public int Qty { get; set; }
        /// <summary>
        /// 主商館Id
        /// </summary>
        public string StoreId { get; set; }
        /// <summary>
        /// 是否可單賣
        /// </summary>
        public int isSingleton { get; set; }
        /// <summary>
        /// 是否為電子票券
        /// </summary>
        public int isETicket { get; set; }
        /// <summary>
        /// 是否上架
        /// </summary>
        public int isShelf { get; set; }
        /// <summary>
        /// 是否在垃圾桶
        /// </summary>
        public int isArchive { get; set; }
        public string OffShelfReason { get; set; }
    }

    public class ProdPrice
    {
        /// <summary>
        /// 市場價格
        /// </summary>
        public decimal M { get; set; }
        /// <summary>
        /// 實際價格
        /// </summary>
        public decimal P { get; set; }
    }

    public class ProdPic
    {
        public string _360x360 { get; set; }
        public string _120x120 { get; set; }
    }


    /// <summary>
    /// 修改文案
    /// </summary>
    public class ProdEditIntro
    {
        public string GroupId { get; set; }
        public List<IntroDetail> Info { get; set; }
    }

    public class IntroDetail
    {
        public string Pic { get; set; }
        public string Pstn { get; set; }
        public string Intro { get; set; }
    }


    /// <summary>
    /// 修改商品說明
    /// </summary>
    public class ProdEditDesc
    {
        public string GroupId { get; set; }
        public string Stmt { get; set; }
        public string Equip { get; set; }
        public string Remark { get; set; }
        public string Liability { get; set; }
        public string Kword { get; set; }
        public string Slogan { get; set; }
    }

    /// <summary>
    /// 修改上下架
    /// </summary>
    public class ProdEditIsShelf
    {
        public string GroupId { get; set; }
        public int isShelf { get; set; }
        public string OffShelfReason { get; set; }
    }

    /// <summary>
    ///修改價格
    /// </summary>
    public class ProdEditPrice
    {
        public string GroupId { get; set; }
        public ProdPrice Price { get; set; }
    }

    /// <summary>
    ///修改庫存
    /// </summary>
    public class ProdEditQry
    {
        public string Id { get; set; }
        public int Qty { get; set; }
    }

    /// <summary>
    /// 檔案
    /// </summary>
    public class UpdateFile
    {
        public string E7OriginalUrl { get; set; }
        public string E7PhysicalPath { get; set; }
        //public NameValueCollection nvc { get; set; }
        public string E7FileName { get; set; }
        public string PCPath { get; set; }
        public string PCUrl { get; set; }
    }

    /// <summary>
    /// 檔案
    /// </summary>
    public class FileDetail
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
        public string Md5 { get; set; }
    }

    /// <summary>
    ///修改圖片
    /// </summary>
    public class ProdEditPic
    {
        public string Id { get; set; }
        public ProdPic Pic { get; set; }
    }
}

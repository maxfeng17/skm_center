﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.PchomeChannel
{
    /// <summary>
    /// 核銷通知
    /// </summary>
    public class PChomeChannleVerifyNotify
    {
        public string VendorNo { get; set; }
        public string OrderNo { get; set; }
        public string VoucherUId { get; set; }
        public int VoucherStatus { get; set; }
        public int RemainAmount { get; set; }
        public string MacCode { get; set; }
    }
}

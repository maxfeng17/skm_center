﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.PchomeChannel
{
    /// <summary>
    /// 聯絡單列表
    /// </summary>
    public class PChomeContactList
    {
        public int TotalRows { get; set; }
        public List<ContactRow> Rows { get; set; }


    }

    /// <summary>
    /// 聯絡單ID
    /// </summary>
    public class ContactRow
    {
        public string Id { get; set; }
    }

    /// <summary>
    /// 聯絡單明細
    /// </summary>
    public class PChomeContactDetail
    {
        public string Id { get; set; }
        public string OrderId { get; set; }
        public string ProdId { get; set; }
        public string NotifyDate { get; set; }
        public string Status { get; set; }
        public string NotifyType { get; set; }
        public string MsgFromCustomer { get; set; }
        public string MsgFromPchome { get; set; }
        public string ReplyText { get; set; }
        public string ReplyDate { get; set; }
        public Staff Staff { get; set; }

    }

    /// <summary>
    /// 經辦人
    /// </summary>
    public class Staff
    {
        public string Name { get; set; }
        public string Extension { get; set; }
    }


    /// <summary>
    /// 客訴單列表
    /// </summary>
    public class PChomeComplaintList
    {
        public int TotalRows { get; set; }
        public List<ComplaintRow> Rows { get; set; }
    }

    /// <summary>
    /// 聯絡單ID
    /// </summary>
    public class ComplaintRow
    {
        public string UId { get; set; }
    }

    /// <summary>
    /// 客訴單明細
    /// </summary>
    public class PChomeComplaintDetail
    {
        public string Id { get; set; }
        public string OrderId { get; set; }
        public string CreateDate { get; set; }
        public ComplaintType Type { get; set; }
        public int isReply { get; set; }
        public string Question { get; set; }
        public string OrderInfo { get; set; }
        public Staff PChomeContact { get; set; }
    }

    /// <summary>
    /// 客訴單回應資訊
    /// </summary>
    public class PChomeComplaintReply
    {
        public string Id { get; set; }
        public string OrderId { get; set; }
        public List<PChomeComplaintReplyDetail> Reply { get; set; }
    }

    /// <summary>
    /// 客訴單回應資訊
    /// </summary>
    public class PChomeComplaintReplyDetail
    {
        public string Date { get; set; }
        public string Content { get; set; }
        public string Remark { get; set; }
    }

    /// <summary>
    /// 客訴單分類
    /// </summary>
    public class ComplaintType
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    /// <summary>
    /// 客訴單回應
    /// </summary>
    public class ComplaintReply
    {
        public string OrderId { get; set; }
        public string Reply { get; set; }
    }

}

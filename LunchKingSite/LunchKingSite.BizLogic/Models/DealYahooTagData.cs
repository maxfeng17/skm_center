﻿using System;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models
{
    public class DealYahooTagData : TrackTagBase
    {
        public DealYahooTagData() { }

        public DealYahooTagData(TrackTagInputModel tt)
        {
            ProjectId = tt.Config.YahooProjectId;
            Properties = new YahooProperties
            {
                PixelId = tt.Config.YahooPixelId,
                Qstrings = new YahooQstring
                {
                    Ea = tt.YahooEa.ToString(),
                    ProductId = tt.ViewPponDealData.MainBid != null && tt.ViewPponDealData.MainBid != Guid.Empty ? 
                        tt.ViewPponDealData.MainBid.ToString() : tt.ViewPponDealData.BusinessHourGuid.ToString()
                }
            };
        }

        public override string GetJson()
        {
            if (string.IsNullOrEmpty(Properties.PixelId)) return string.Empty;
            var data = string.Format("window.dotq = window.dotq || [];\n window.dotq.push({0});",
                JsonConvert.SerializeObject(this));
            return data;
        }

        [JsonProperty("projectId")]
        public string ProjectId { get; set; }

        [JsonProperty("properties")]
        public YahooProperties Properties { get; set; }
    }

    public class YahooProperties
    {
        [JsonProperty("pixelId")]
        public string PixelId { get; set; }

        [JsonProperty("qstrings")]
        public YahooQstring Qstrings { get; set; }
    }

    public class YahooQstring
    {
        [JsonProperty("et")]
        public string Et
        {
            get { return "custom"; }
        }

        [JsonProperty("ea")]
        public string Ea { get; set; }

        [JsonProperty("product_id")]
        public string ProductId { get; set; }
    }

    public enum YahooEaType
    {
        ViewProduct,
        AddToCart,
        Purchase
    }
}

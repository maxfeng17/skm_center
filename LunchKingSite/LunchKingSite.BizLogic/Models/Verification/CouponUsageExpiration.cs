﻿using System;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model.Verification
{
    public class CouponUsageExpiration : IExpirationChangeable
    {
        #region .prop
		
        public DateTime? DealOriginalExpireDate { get; private set; }

        public DateTime? DealChangedExpireDate { get; private set; }

        public DateTime? StoreChangedExpireDate { get; private set; }

        public DateTime? SellerClosedDownDate { get; private set; }

        public DateTime? StoreClosedDownDate { get; private set; }

        public DateTime? DealUseStartDate { get; private set; }

        #endregion

        #region .ctor

        public CouponUsageExpiration(ViewPponCoupon info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("傳入的 ViewPponCoupon 為 null");
            }
            DealOriginalExpireDate = info.BusinessHourDeliverTimeE;
            DealChangedExpireDate = info.BhChangedExpireDate;
            DealUseStartDate = info.BusinessHourDeliverTimeS;

            #region 目前核銷用不到的 By Ahdaa 2012.07
            StoreChangedExpireDate = info.StoreChangedExpireDate;
            SellerClosedDownDate = info.SellerCloseDownDate;
            StoreClosedDownDate = info.StoreCloseDownDate;
            #endregion
        }

        public CouponUsageExpiration(ViewHiDealVerificationInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("傳入的 ViewHiDealVerificationInfo 為 null");
            }
            // 2012/07 品生活檔次尚無延長兌換日期欄位，所以用UseEndTime代之

            DealOriginalExpireDate = info.UseEndTime;
            DealChangedExpireDate = info.ChangedExpireTime;
            DealUseStartDate = info.UseStartTime;

            #region 目前核銷用不到的 By Ahdaa 2012.07
            StoreChangedExpireDate = DateTime.Today;
            SellerClosedDownDate = DateTime.Today;
            StoreClosedDownDate = DateTime.Today;
            #endregion
        }

        public CouponUsageExpiration(ViewHiDealCoupon info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("傳入的 ViewHiDealCoupon 為 null");
            }
            // 2012/07 品生活檔次尚無延長兌換日期欄位，所以用UseEndTime代之

            DealOriginalExpireDate = info.UseEndTime;
            DealChangedExpireDate = info.ChangedExpireTime;
            DealUseStartDate = info.UseStartTime;

            #region 目前核銷用不到的 By Ahdaa 2012.07
            StoreChangedExpireDate = DateTime.Today;
            SellerClosedDownDate = DateTime.Today;
            StoreClosedDownDate = DateTime.Today;
            #endregion
        }


        #endregion
    }
}

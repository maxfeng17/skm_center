using System;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.Verification
{
    /// <summary>
    /// 核銷查詢時所需要的資料物件<br />
    /// 目的在兜 SQL 用的語法
    /// </summary>
    public class VerificationQueryData
    {
        
        public OrderClassification QueryType { set; get; }
        private Guid _bid;
        public Guid Bid 
        { 
            get { return _bid; }
            set { Guid.TryParse(value.ToString(), out _bid); }
        }

        private int _did;
        public int DealId
        {
            get { return _did; }
            set { _did = value; }
        }

        private int _pid;
        public int ProductId {
            get { return _pid; }
            set { _pid = value; }
        }

        private DateTime _sTime;
        public DateTime StartQueryTime {
            get { return _sTime; }
            set { _sTime = value; }
        }

        private DateTime _eTime;
        public DateTime EndQueryTime
        {
            get { return _eTime; }
            set { _eTime = value; }
        }

        private Guid _seGuid;
        public Guid SellerGuid
        {
            get { return _seGuid; } 
            set { _seGuid = value; }
        }

        private Guid _stGuid;
        public Guid StoreGuid
        {
            get { return _stGuid; } 
            set { _stGuid = value; }
        }
        public bool IsSetCondition { get; set; }
        public bool IsStore { get; set; }

        public VerificationQueryData(OrderClassification enumType)
        {
            QueryType = enumType;
            IsSetCondition = false;
            IsStore = false;
            Bid = Guid.Empty;
            DealId = -1;
            ProductId = -1;
            StartQueryTime = new DateTime();
            EndQueryTime = new DateTime();
            SellerGuid = Guid.Empty;
            StoreGuid = Guid.Empty;
        }

        public void SetQueryCondition(string strBid, string strDid, string strPid, 
                                      DateTime dtStart, DateTime dtEnd,
                                      string strSeGuid, string strStGuid, int iQueryStatus)
        {
            if (ChkStoreQuery(strSeGuid, strStGuid) && ChkDealId(strBid, strDid, strPid))
            {
                IsSetCondition = true;
                if(iQueryStatus != (int)VerificationQueryStatus.Init)
                {
                    ChkQueryTime(dtStart, dtEnd);    
                }
            }
        }
        /// <summary>
        /// 判斷此次查詢是否為 Store
        /// </summary>
        /// <param name="strSeGuid">Seller Guid</param>
        /// <param name="strStGuid">Store Guid</param>
        /// <returns></returns>
        private bool ChkStoreQuery(string strSeGuid, string strStGuid)
        {
            if(string.IsNullOrEmpty(strStGuid) && string.IsNullOrEmpty(strSeGuid))
            {
                return false;
            }
            IsStore = Guid.TryParse(strStGuid, out _stGuid);
            return Guid.TryParse(strSeGuid, out _seGuid);
        }
        private bool ChkDealId(string strBid, string strDid, string strPid)
        {
            bool isChkResult = false;
            switch (QueryType)
            {
                case OrderClassification.LkSite:
                    isChkResult = Guid.TryParse(strBid, out _bid);
                    break;
                case OrderClassification.HiDeal:
                    isChkResult = int.TryParse(strDid, out _did) && int.TryParse(strPid, out _pid);
                    break;
            }
            return isChkResult;
        }
        private void ChkQueryTime(DateTime dtStart, DateTime dtEnd)
        {
            int timeCompare = DateTime.Compare(dtStart, dtEnd);
            StartQueryTime = dtStart;
            if(timeCompare == 0)
            {   
                EndQueryTime = dtEnd;
            } else if(timeCompare > 0)
            {
                StartQueryTime = dtEnd;
                EndQueryTime = dtStart;
            } else
            {
                EndQueryTime = dtEnd;
            }
        }
    }
}
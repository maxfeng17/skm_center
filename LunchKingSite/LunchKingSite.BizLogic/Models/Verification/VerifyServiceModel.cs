﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.VBS;
using LunchKingSite.BizLogic.Models.API;
using LunchKingSite.Core;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.Verification
{
    public class VerifyServiceModel { }

    /// <summary>
    /// QRCode
    /// </summary>
    public class QrActionCode
    {
        /// <summary>
        /// 作用
        /// </summary>
        public QrCodeActionType Action { get; set; }
        /// <summary>
        /// 代碼
        /// </summary>
        public dynamic Code { get; set; }
    }
    
    /// <summary>
    /// 偵測QRCode結果
    /// </summary>
    public class ActionPokeballModel
    {
        /// <summary>
        /// 依不同ActionType回傳不同Pokeball
        /// </summary>
        public object ActionPokeball { get; set; }
    }
 


    
    public class VerifyCouponResultModel
    {
        /// <summary>
        /// 核銷份數
        /// </summary>
        public int VerifyCount { get; set; }
        /// <summary>
        /// 核銷單位
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// 核銷時間
        /// </summary>
        public string UsedDate { get; set; }
        /// <summary>
        /// 核銷分店
        /// </summary>
        public string VerifyStore { get; set; }
        /// <summary>
        /// 商品主圖
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 核銷明細
        /// </summary>
        [JsonIgnore]
        public MultiCouponVerifyResult VerifyResultDetail { get; set; }
    }




}

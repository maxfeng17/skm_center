﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model.Verification
{
    public class VendorBillingAccount
    {
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static readonly object thislock = new object();

        public static string GetPrefix(Guid sellerGuid)
        {
            Seller seller = sp.SellerGet(sellerGuid);
            
            if(!seller.IsLoaded)
            {
                throw new ArgumentException("The provided seller guid cannot retrieve a proper seller.");
            }

            if(!string.IsNullOrWhiteSpace(seller.VbsPrefix))
            {
                return seller.VbsPrefix;
            }
            else
            {
                return CreateVbsPrefix(sellerGuid);
            }
        }
        
        public static int GetPrefixUseCount(string prefix)
        {
            return mp.VbsPrefixUseGetCount(prefix, GetPrefixLength());
        }

        public static bool IsAccountIdExist(string accountId)
        {
            VbsMembership member = mp.VbsMembershipGetByAccountId(accountId);
            if(member.IsLoaded)
            {
                return true;
            }
            return false;
        }


        private static string CreateVbsPrefix(Guid sellerGuid)
        {
            int retryCount = 5;
            while (retryCount > 0)
            {
                try
                {
                    StringBuilder sbResult = new StringBuilder();
                    while (sbResult.Length < GetPrefixLength())
                    {
                        sbResult.Append(Regex.Replace(Path.GetRandomFileName().Replace(".", ""), @"\d", ""));
                        sbResult.Remove(GetPrefixLength() - 1, sbResult.Length - GetPrefixLength());
                    }
                    
                    string result = sbResult.ToString().ToUpper();

                    lock (thislock)
                    {
                        if (int.Equals(0, sp.SellerGetCountByPrefix(result)))
                        {
                            Seller s = sp.SellerGet(sellerGuid);
                            s.VbsPrefix = result;
                            sp.SellerSet(s);
                            return result;
                        }
                    }
                }
                finally
                {
                    retryCount--;
                }
            }

            throw new Exception(string.Format("Failed to generate a prefix for seller: {0}", sellerGuid));
        }
        private static int GetPrefixLength()
        {
            return 6;
        }
       
    }
}

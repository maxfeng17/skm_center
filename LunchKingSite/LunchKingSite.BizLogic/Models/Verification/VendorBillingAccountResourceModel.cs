﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.Verification
{
    /// <summary>
    /// 核銷對帳系統做用，
    /// 根據account_id查詢回傳用的Model
    /// </summary>
    public class VendorBillingAccountResourceModel
    {
        public List<Guid> StoreGuids { get; set; }
        /// <summary>
        /// 如果是P好康，收集 business_hour 的 guid
        /// 如果是品生活回傳hi_deal_product的guid
        /// </summary>
        public List<VendorBillingProductModel> Products { get; set; }
    }
}

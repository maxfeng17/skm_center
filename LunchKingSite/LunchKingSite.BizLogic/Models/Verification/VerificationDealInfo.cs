﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.Verification
{
    public class VerificationDealInfo
    {
        /// <summary>
        /// SellerGuid
        /// </summary>
        public Guid SeG { get; set; }
        /// <summary>
        /// StoreGuid
        /// </summary>
        public string StG { get; set; }
        /// <summary>
        /// StoreName
        /// </summary>
        public string StN { get; set; }
        /// <summary>
        /// Business Hour Guid
        /// </summary>
        public string Bid { get; set; }
        /// <summary>
        /// Deal Id
        /// </summary>
        public int Did { get; set; }
        /// <summary>
        /// Product Id
        /// </summary>
        public int Pid { get; set; }
        /// <summary>
        /// Deal Name
        /// </summary>
        public string Dn { get; set; }
        /// <summary>
        /// Product Name
        /// </summary>
        public string Pn { get; set; }
        /// <summary>
        /// 兌換日期起日
        /// </summary>
        public DateTime StartDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Models
{
    public class DealPriceModel
    {
        public int BusinessHourStatus { get; set; }
        public int? GroupOrderStatus { get; set; }
        public int? DiscountType { get; set; }
        public decimal ItemOrigPrice { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal? ExchangePrice { get; set; }
        public int? DiscountValue { get; set; }
        public bool OnlyNumber { get; set; }
    }

    public class DealDiscountStringModel
    {
        public int DiscountDisplayType { get; set; }
        public string DiscountString { get; set; }
        public decimal DisplayPrice { get; set; }

        public DealDiscountStringModel()
        {
            DisplayPrice = 0;
            DiscountString = string.Empty;
            DiscountDisplayType = 0;
        }
    }
}

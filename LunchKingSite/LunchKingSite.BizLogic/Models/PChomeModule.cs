﻿using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model
{
    public class PChomePponDeal
    {
        #region Field
        /// <summary>
        /// 17Life檔次bid/品生活商品檔次pid;產品編號
        /// </summary>
        [XmlElementName("pk")]
        public string Pk;
        /// <summary>
        /// 產品名稱
        /// </summary>
        [XmlElementName("title")]
        public string Title;
        /// <summary>
        /// 檔次說明
        /// </summary>
        [XmlElementName("desc")]
        public string Desc;
        /// <summary>
        /// 商品分類
        /// (餐廳美食=food、美容保養=beauty、 時尚流行=fashion、旅遊=travel、宅配=delivery)
        /// </summary>
        [XmlElementName("tag")]
        public string Tag;
        /// <summary>
        /// 顯示連結
        /// </summary>
        [XmlElementName("url_disp")]
        public string UrlDisp;
        /// <summary>
        /// 檔次連結
        /// </summary>
        [XmlElementName("url_link")]
        public string UrlLink;
        /// <summary>
        /// JPG格式的商品圖片URL
        /// </summary>
        [XmlElementName("img_link")]
        public string ImgLink;
        /// <summary>
        /// channel
        /// </summary>
        [XmlElementName("channel")]
        public string Channel;
        /// <summary>
        /// 更新時間
        /// </summary>
        [XmlElementName("update")]
        public string Update;
        /// <summary>
        /// 建立時間
        /// </summary>
        [XmlElementName("create")]
        public string Create;
        /// <summary>
        /// 原價
        /// </summary>
        [XmlElementName("va_a")]
        public string VaA;
        /// <summary>
        /// 特價或賣價
        /// </summary>
        [XmlElementName("va_b")]
        public string VaB;
        
        
        #endregion Field
    }
}

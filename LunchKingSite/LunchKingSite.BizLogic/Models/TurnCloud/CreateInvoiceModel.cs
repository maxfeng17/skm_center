﻿using LunchKingSite.Core;
using Newtonsoft.Json;
using System;

namespace LunchKingSite.BizLogic.Models.TurnCloud
{
    #region CreateInvoiceRequest 物件群組
    /// <summary> 開立發票請求資訊 </summary>
    [Serializable]
    public class CreateInvoiceRequest
    {
        /// <summary> 平台代碼 </summary>
        public string PlatFormId { get; set; }
        /// <summary> 交易簽章 </summary>
        public string HashKey { get; set; }
        /// <summary> 裝置編號 </summary>
        [JsonProperty(PropertyName = "pos_id")]
        public string PosId { get; set; }
        /// <summary> 開立發票詳細資訊 </summary>
        [JsonProperty(PropertyName = "rm_detail")]
        public CreateInvoiceDetail[] InvoiceDetails { get; set; }
    }

    /// <summary> 開立發票詳細資訊 </summary>
    [Serializable]
    public class CreateInvoiceDetail
    {
        /// <summary> 商店編號 </summary>
        [JsonProperty(PropertyName = "sell_shopid")]
        public string SellShopId { get; set; }
        /// <summary> 交易序號 </summary>
        [JsonProperty(PropertyName = "sell_no")]
        public string SellNo { get; set; }
        /// <summary> 訂單編號 </summary>
        [JsonProperty(PropertyName = "sell_order_no")]
        public string SellOrderNo { get; set; }
        /// <summary> 收銀機編號 </summary>
        [JsonProperty(PropertyName = "sell_posid")]
        public string PosId { get; set; }
        /// <summary> 銷售日(入帳日)(yyyy/MM/dd) </summary>
        [JsonProperty(PropertyName = "sell_day")]
        public string SellDay { get; set; }
        /// <summary> 銷售時間(HH:mm:ss) </summary>
        [JsonProperty(PropertyName = "sell_time")]
        public string SellTime { get; set; }
        /// <summary> 6X櫃號 </summary>
        [JsonProperty(PropertyName = "store_id")]
        public string StoreId { get; set; }
        /// <summary> 交易日(yyyy/MM/dd) </summary>
        [JsonProperty(PropertyName = "tday")]
        public string TradeDay { get; set; }
        /// <summary> 銷售總金額(含稅) </summary>
        [JsonProperty(PropertyName = "total_amt")]
        public int TotalAmount { get; set; }
        /// <summary> 發票金額 </summary>
        [JsonProperty(PropertyName = "total_sales_amt")]
        public int InvoiceAmount { get; set; }
        /// <summary> 銷售總數量 </summary>
        [JsonProperty(PropertyName = "total_qty")]
        public int TotalQuantity { get; set; }
        /// <summary> 銷售總折扣 </summary>
        [JsonProperty(PropertyName = "disc_amt")]
        public int DiscountAmount { get; set; }
        /// <summary> 發票稅額 </summary>
        [JsonProperty(PropertyName = "rate_amt")]
        public int RateAmount { get; set; }
        /// <summary> 溢收金額 </summary>
        [JsonProperty(PropertyName = "extra_amt")]
        public int ExtraAmount { get; set; }
        /// <summary> 交易型態 </summary>
        [JsonProperty(PropertyName = "sell_transtype")]
        public string SellTransType { get; set; }
        /// <summary> 會員卡號 </summary>
        [JsonProperty(PropertyName = "vip_no")]
        public string VipNo { get; set; }
        /// <summary> 會員 APP 專屬密碼</summary>
        [JsonProperty(PropertyName = "Vip_app_pass")]
        public string VipAppPass { get; set; }
        /// <summary> 統一編號 </summary>
        [JsonProperty(PropertyName = "comp_id")]
        public string CompanyId { get; set; }
        /// <summary> 愛心碼 </summary>
        [JsonProperty(PropertyName = "eui_donate_no")]
        public string EuiDonateNo { get; set; }
        /// <summary> 統一編號 </summary>
        [JsonProperty(PropertyName = "eui_donate_status")]
        public int EuiDonateStatus { get; set; }
        /// <summary> 電子發票載具卡號 </summary>
        [JsonProperty(PropertyName = "eui_vehicle_no")]
        public string EuiVehicleNo { get; set; }
        /// <summary> 是否有載具 </summary>
        [JsonProperty(PropertyName = "eui_universal_status")]
        public int EuiUniversalStatus { get; set; }
        /// <summary> 電子發票載具類別編號 </summary>
        [JsonProperty(PropertyName = "eui_vehicle_type_no")]
        public string EuiVehicleTypeNo { get; set; }
        /// <summary> 車牌號碼(發票明細會顯示) </summary>
        [JsonProperty(PropertyName = "parking_token")]
        public string ParkingToken { get; set; }
        /// <summary> 銷售明細清單 </summary>
        [JsonProperty(PropertyName = "sell_detail")]
        public CreateInvoiceSellDetail[] SellDetails { get; set; }
        /// <summary> 付款方式明細清單 </summary>
        [JsonProperty(PropertyName = "tender_detail")]
        public CreateInvoiceTenderDetail[] TenderDetails { get; set; }
        /// <summary> 預售狀態 </summary>
        [JsonProperty(PropertyName = "pre_sales_status")]
        public string PreDalesStatus { get; set; }
    }

    /// <summary> 銷售明細 </summary>
    public class CreateInvoiceSellDetail
    {
        /// <summary> 銷售編號 </summary>
        [JsonProperty(PropertyName = "sell_id")]
        public string SellId { get; set; }
        /// <summary> 專櫃編號(7X) </summary>
        [JsonProperty(PropertyName = "nd_type")]
        public string BrandCounterCode { get; set; }
        /// <summary> 6X櫃號 </summary>
        [JsonProperty(PropertyName = "store_id")]
        public string StoreId { get; set; }
        /// <summary> 子訂單編號 </summary>
        [JsonProperty(PropertyName = "sell_order_no_s")]
        public string SellDetailNo { get; set; }
        /// <summary> 數量 </summary>
        [JsonProperty(PropertyName = "sell_qty")]
        public int Quantity { get; set; }
        /// <summary> 售價(含稅) </summary>
        [JsonProperty(PropertyName = "sell_price")]
        public int Price { get; set; }
        /// <summary> 折讓金額 </summary>
        [JsonProperty(PropertyName = "sell_discount")]
        public int SellDiscount { get; set; }
        /// <summary> 稅別 </summary>
        [JsonProperty(PropertyName = "sell_rate")]
        public TurnCloudSellRateType SellRate { get; set; }
        /// <summary> 稅額 </summary>
        [JsonProperty(PropertyName = "sell_rate_amt")]
        public int SellRateAmount { get; set; }
        /// <summary> 小計 </summary>
        [JsonProperty(PropertyName = "sell_amt")]
        public int SellAmount { get; set; }
        /// <summary> 商品條碼 </summary>
        [JsonProperty(PropertyName = "product_id")]
        public string ProductId { get; set; }
        /// <summary> 原價 </summary>
        [JsonProperty(PropertyName = "sell_ori_price")]
        public int OriginalPrice { get; set; }
        /// <summary> 商品名稱 </summary>
        [JsonProperty(PropertyName = "goods_name")]
        public string GoodsName { get; set; }
        /// <summary> 折價方式 </summary>
        [JsonProperty(PropertyName = "product_discount_type")]
        public string ProductDiscountType { get; set; }
        /// <summary> 特賣型態 </summary>
        [JsonProperty(PropertyName = "Pmt_type")]
        public string PaymentType { get; set; }
        /// <summary> 促銷會員贈點點數 </summary>
        [JsonProperty(PropertyName = "Point_add")]
        public int AddPoints { get; set; }
        /// <summary> 促銷會員扣點點數 </summary>
        [JsonProperty(PropertyName = "Point_deduct")]
        public int DeductPoints { get; set; }
    }

    /// <summary> 付款方式資訊 </summary>
    public class CreateInvoiceTenderDetail
    {
        /// <summary> 專櫃編號(7X) </summary>
        [JsonProperty(PropertyName = "nd_type")]
        public string BrandCounterCode { get; set; }
        /// <summary> 6X櫃號 </summary>
        [JsonProperty(PropertyName = "store_id")]
        public string StoreId { get; set; }
        /// <summary> 付款方式 </summary>
        [JsonProperty(PropertyName = "sell_tender")]
        public string SellTender { get; set; }
        /// <summary> 付款金額 </summary>
        [JsonProperty(PropertyName = "tender_price")]
        public int TenderPrice { get; set; }
        /// <summary> 付款資訊(依照付款方式來記錄相關資訊，卡號、禮券號等) </summary>
        [JsonProperty(PropertyName = "sec_no")]
        public string SecNo { get; set; }
        /// <summary> 信用卡資訊清單 </summary>
        [JsonProperty(PropertyName = "credit_card_detail")]
        public CreateInvoiceCreditCardDetail CreditCardDetail { get; set; }
        /// <summary> 第三方支付訂單編號</summary>
        [JsonProperty(PropertyName = "Pay_order_no")]
        public string PayOrderNo { get; set; }
        /// <summary> 促銷會員總贈點點數 </summary>
        [JsonProperty(PropertyName = "Point_add_total")]
        public int TotalAddPoint { get; set; }
        /// <summary> 促銷會員總扣點點數 </summary>
        [JsonProperty(PropertyName = "Point_deduct_total")]
        public int TotalDeductPoint { get; set; }
    }

    /// <summary> 信用卡資訊 </summary>
    public class CreateInvoiceCreditCardDetail
    {
        /// <summary> 付款金額 </summary>
        [JsonProperty(PropertyName = "tx_amount")]
        public int TransAmount { get; set; }
        /// <summary> 信用卡號 </summary>
        [JsonProperty(PropertyName = "card_no")]
        public string CardNo { get; set; }
        /// <summary> 分期期數 </summary>
        [JsonProperty(PropertyName = "periods")]
        public int Periods { get; set; }
        /// <summary> 分期的頭期款金額</summary>
        [JsonProperty(PropertyName = "hand_amt")]
        public int FirstAmount { get; set; }
        /// <summary> 分期的每期金額</summary>
        [JsonProperty(PropertyName = "each_amt")]
        public int EachAmount { get; set; }
        /// <summary> 使用紅利(Y,N)</summary>
        [JsonProperty(PropertyName = "use_bonus")]
        public string IsUseBonus { get; set; }
        /// <summary> 信用卡號(Hash) </summary>
        [JsonProperty(PropertyName = "card_no2")]
        public string CardNo2 { get; set; }
        /// <summary> 使用紅利點數 </summary>
        [JsonProperty(PropertyName = "bonus_use_point")]
        public int BonusUsePoint { get; set; }
        /// <summary> 紅利剩餘點數  </summary>
        [JsonProperty(PropertyName = "bonus_point")]
        public int BonusPoint { get; set; }
        /// <summary> 紅利折抵金額 </summary>
        [JsonProperty(PropertyName = "bonus_dis_amt")]
        public int BonusDiscountAmount { get; set; }
    }
    #endregion

    #region CreateInvoiceResponse 物件群組

    /// <summary> 開立發票回應資訊 </summary>
    [Serializable]
    public class CreateInvoiceResponse
    {
        /// <summary> 回應基本資訊 </summary>
        [JsonProperty(PropertyName = "rcrm")]
        public TurnCloudResponseMessage ResponseBase { get; set; }
        /// <summary> 開立發票回應內容 </summary>
        [JsonProperty(PropertyName = "results")]
        public CreateInvoiceResponseData results { get; set; }
    }

    /// <summary> 回應基本資訊 </summary>
    public class TurnCloudResponseMessage
    {
        /// <summary> 回應代號 </summary>
        [JsonProperty(PropertyName = "RC")]
        public string ReturnCode { get; set; }
        /// <summary> 回應訊息 </summary>
        [JsonProperty(PropertyName = "RM")]
        public string ReturnMessage { get; set; }
    }

    /// <summary> 開立發票資訊 </summary>
    public class CreateInvoiceResponseData
    {
        /// <summary> 商店編號 </summary>
        [JsonProperty(PropertyName = "sell_shopid")]
        public string SellShopId { get; set; }

        /// <summary> 訂單編號 </summary>
        [JsonProperty(PropertyName = "sell_order_no")]
        public string SellOrderNo { get; set; }
        /// <summary> 收銀機編號 </summary>
        [JsonProperty(PropertyName = "sell_posid")]
        public string PosId { get; set; }
        /// <summary> 交易序號 </summary>
        [JsonProperty(PropertyName = "sell_no")]
        public string SellNo { get; set; }
        /// <summary> 銷售日(入帳日) </summary>
        [JsonProperty(PropertyName = "sell_day")]
        public DateTime SellDay { get; set; }
        /// <summary> 發票號碼 </summary>
        [JsonProperty(PropertyName = "sell_invoice")]
        public string InvoiceNo { get; set; }
    }

    #endregion

}

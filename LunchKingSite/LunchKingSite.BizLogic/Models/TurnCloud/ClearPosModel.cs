﻿using Newtonsoft.Json;
using System;

namespace LunchKingSite.BizLogic.Models.TurnCloud
{
    /// <summary> 每日上傳銷售總額請求資訊 </summary>
    [Serializable]
    public class ClearPosRequest
    {
        [JsonProperty(PropertyName = "PlatFormId")]
        /// <summary> 平台代碼 </summary>
        public string PlatFormId { get; set; }
        [JsonProperty(PropertyName = "HashKey")]
        /// <summary> 交易簽章 </summary>
        public string HashKey { get; set; }
        /// <summary> 商店編號 </summary>
        [JsonProperty(PropertyName = "sell_shopid")]
        public string ShopId { get; set; }
        /// <summary> 裝置編號 </summary>
        [JsonProperty(PropertyName = "pos_id")]
        public string PosId { get; set; }
        /// <summary> 上傳日期 </summary>
        [JsonProperty(PropertyName = "Tdate")]
        public string UploadDate { get; set; }
        /// <summary> 銷售日期 </summary>
        [JsonProperty(PropertyName = "sales_date")]
        public string SellDate { get; set; }
        /// <summary> 清機資料 </summary>
        [JsonProperty(PropertyName = "clear_pos")]
        public ClearPosDetail[] ClearPosDetails { get; set; }
    }
    /// <summary> 每日銷售總額詳細資訊 </summary>
    [Serializable]
    public class ClearPosDetail
    {
        /// <summary> 清機資料類型 </summary>
        [JsonProperty(PropertyName = "title")]
        public string ClearDataType { get; set; }
        /// <summary> 專櫃編號(6X) </summary>
        [JsonProperty(PropertyName = "store_id")]
        public string BrandCounterCode6X { get; set; }
        /// <summary> 專櫃編號(7X) </summary>
        [JsonProperty(PropertyName = "nd_type")]
        public string BrandCounterCode { get; set; }
        /// <summary> 筆數 </summary>
        [JsonProperty(PropertyName = "sales_qty")]
        public int Quantity { get; set; }
        /// <summary> 金額 </summary>
        [JsonProperty(PropertyName = "sales_amt")]
        public int Amount { get; set; }
    }

    /// <summary> 每日銷售總額回應資訊 </summary>
    [Serializable]
    public class ClearPosResponse
    {
        /// <summary> 回應基本資訊 </summary>
        [JsonProperty(PropertyName = "rcrm")]
        public TurnCloudResponseMessage ResponseBase { get; set; }
        /// <summary> 開立發票回應內容 </summary>
        [JsonProperty(PropertyName = "results")]
        public string Results { get; set; }
    }
}

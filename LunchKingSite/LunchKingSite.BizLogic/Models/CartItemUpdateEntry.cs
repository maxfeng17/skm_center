﻿using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model
{
    public class CartItemUpdateEntry
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _qty;
        public int Quantity
        {
            get { return _qty; }
            set { _qty = value; }
        }
        public DepartmentTypes DeptType { get; set; }

        public CartItemUpdateEntry(int id, int qty)
        {
            _id = id;
            _qty = qty;
        }
        public CartItemUpdateEntry(int id, int qty, DepartmentTypes deptType)
        {
            _id = id;
            _qty = qty;
            this.DeptType = deptType;
        }
    }
}

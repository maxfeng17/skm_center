﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    class PromotionModel
    {
    }

    /// <summary>
    /// 策展折價券
    /// </summary>
    public class CurationDiscountCampaign
    {
        /// <summary>
        /// 折價券Id
        /// </summary>
        public int DiscountId { get; set; }
        /// <summary>
        /// 面額
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 滿額條件
        /// </summary>
        public int MinimumAmount { get; set; }
        /// <summary>
        /// 組數
        /// </summary>
        public int LimitedQty { get; set; }
        /// <summary>
        /// 是否顯示限量
        /// </summary>
        public bool IsShowLimitedQty { get; set; }
        /// <summary>
        /// 狀態
        /// </summary>
        public int StatusType { get; set; }
    }

    /// <summary>
    /// 策展中繼頁資訊
    /// </summary>
    public class CurationRelayPageInfo
    {
        /// <summary>
        /// 活動描述
        /// </summary>
        public string EventDescription { get; set; }
        /// <summary>
        /// 策展折價券
        /// </summary>
        public List<CurationDiscountCampaign> CurationDiscountList { get; set; }
    }
}

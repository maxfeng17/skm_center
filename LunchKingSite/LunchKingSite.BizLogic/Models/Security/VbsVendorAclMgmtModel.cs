﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LunchKingSite.BizLogic.Model
{
    public class VbsVendorAclMgmtModel
    {
        #region public props
        
        public string AccountId { get; set; }
        
        #endregion
        
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        private IVerificationProvider vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        

        //public IEnumerable<VbsVendorAce> GetAclHierarchy(PermissionSearchCondition condition, string searchExpression)
        //{
        //    ViewVbsFlattenedAclCollection permissions = GetPermissionList(condition, searchExpression);
        //    return TransformDbAclToVbsVendorAcl(permissions);
        //}

        #region 權限管理系統

        /// <summary>
        /// 任何條件最後都是以賣家搜尋的方式, 完整的把檔次階層資料回傳.
        /// </summary>
        public IEnumerable<VbsVendorAce> FindAllowableAclHierarchy(PermissionSearchCondition condition, string searchExpression)
        {
            List<VbsVendorAce> allowables = new List<VbsVendorAce>();

            ViewVbsFlattenedAclCollection searchedAcl = GetPermissionList(condition, searchExpression);

            List<VbsVendorAce> transformedAcl = TransformDbAclToVbsVendorAcl(searchedAcl);

            Dictionary<Guid, List<VbsVendorAce>> groupedBySeller = transformedAcl
                .GroupBy(acl => acl.SellerGuid)
                .ToDictionary(g => g.Key, g => g.ToList());

            foreach(KeyValuePair<Guid, List<VbsVendorAce>> sellerHierarchy in groupedBySeller)
            {
                List<VbsVendorAce> currentList = sellerHierarchy.Value;
                if(currentList.Where(acl => acl.IsAllowable).Count() > 0)
                {
                    allowables.AddRange(currentList);
                }
            }

            return allowables;
        }
        
        public IEnumerable<VbsVendorAce> GetAllowedAcl()
        {
            ViewVbsFlattenedAclCollection allowedAcl = mp.ViewVbsFlattenedAclCurrentPermissionsGetList(this.AccountId);
            List<VbsVendorAce> transformedAcl = TransformDbAclToVbsVendorAcl(allowedAcl);
            List<VbsVendorAce> allowedResult = transformedAcl.Where(acl => acl.IsAllowed).ToList();
            return allowedResult;
        }

        #endregion

        #region 權限系統

        /// <summary>
        /// 權限系統: 查帳號擁有的檔次權限
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IVbsVendorAce> GetAllowedDeals()
        {
            return GetAccountVbsVendorAceBaseList(this.AccountId);
        }

        /// <summary>
        /// 權限系統: 查帳號擁有的檔次權限
        /// </summary>
        /// <param name="vbsRight"></param>
        /// <param name="deliveryType"></param>
        /// <param name="onlyEstablishedDeal"></param>
        /// <returns></returns>
        public IEnumerable<IVbsVendorAce> GetAllowedDeals(VbsRightFlag vbsRight, DeliveryType? deliveryType = null, bool onlyEstablishedDeal = true)
        {
            return GetAccountVbsVendorAceBaseList(this.AccountId, deliveryType, vbsRight, onlyEstablishedDeal);
        }

        /// <summary>
        /// 權限系統: 依指定的檔次guid 查帳號擁有的檔次權限
        /// </summary>
        /// <param name="dealGuid"></param>
        /// <param name="deliveryType"></param>
        public IEnumerable<IVbsVendorAce> GetAllowedDeals(Guid dealGuid, DeliveryType? deliveryType = null)
        {
            return GetAccountVbsVendorAceBaseList(this.AccountId, dealGuid, deliveryType);
        }

        /// <summary>
        /// 權限系統: 依指定的檔次guid 查帳號擁有的檔次權限
        /// </summary>
        /// <param name="dealGuid"></param>
        /// <param name="vbsRight"></param>
        /// <param name="deliveryType"></param>
        /// <returns></returns>
        public IEnumerable<IVbsVendorAce> GetAllowedDeals(Guid dealGuid, VbsRightFlag vbsRight, DeliveryType? deliveryType = null)
        {
            return GetAccountVbsVendorAceBaseList(this.AccountId, dealGuid, deliveryType, vbsRight);
        }


        /// <summary>
        /// 權限系統: 依指定的檔次guid 查帳號擁有的檔次分店權限
        ///           抓取顯示於商家系統-核銷查詢及對帳查詢中之憑證檔次
        /// </summary>
        /// <param name="dealGuid"></param>
        /// <param name="deliveryType"></param>
        /// <param name="vbsRight"></param>
        /// <returns></returns>
        public IEnumerable<IVbsVendorAce> GetAllowedDealUnits(Guid? dealGuid, DeliveryType? deliveryType, VbsRightFlag vbsRight)
        {
            var allowedResult = dealGuid.HasValue
                                ? GetAllowedDeals(dealGuid.Value, vbsRight, deliveryType)
                                : GetAllowedDeals(vbsRight, deliveryType);

            return allowedResult;
        }

        /// <summary>
        /// 權限系統: 指定的商品中, 帳號擁有該檔次的分店 guid
        /// </summary>
        /// <param name="merchandiseGuid"></param>
        /// <returns></returns>
        public List<Guid> GetAllowedDealStores(Guid merchandiseGuid)
        {
            var allowedDealUnits = GetAllowedDeals(merchandiseGuid).ToList();

            List<Guid> result = new List<Guid>();

            foreach (var ace in allowedDealUnits)
            {
                if (ace.IsInStore.GetValueOrDefault(false))
                { 
                    result.Add(ace.StoreGuid.Value);
                }
            }

            return result;
        }
        /// <summary>
        /// 權限系統: 帳號擁有的分店 guid
        /// </summary>
        /// <param name="merchandiseGuid"></param>
        /// <returns></returns>
        public List<Guid> GetAllowedDealStores()
        {
            return vp.StoreGuidsGetListByAccountId(this.AccountId);
        }
        
        #endregion

        #region .ctor

        public VbsVendorAclMgmtModel(string accountId)
        {
            this.AccountId = accountId;
        }
        
        #endregion
        
        protected ViewVbsFlattenedAclCollection GetPermissionList(PermissionSearchCondition condition, string searchExpression)
        {
            ViewVbsFlattenedAclCollection result;
            switch (condition)
            {
                case PermissionSearchCondition.Bid:
                    Guid bid;
                    if (Guid.TryParse(searchExpression, out bid))
                    {
                        BusinessHour bh = pp.BusinessHourGet(bid);
                        if (bh != null && bh.IsLoaded)
                        {
                            result = mp.ViewVbsFlattenedAclGetListBySid(this.AccountId, GetRelatedSellerGuid(bh.SellerGuid));
                        }
                        else
                        {
                            result = new ViewVbsFlattenedAclCollection();
                        }
                    }
                    else
                    {
                        result = new ViewVbsFlattenedAclCollection();
                    }
                    break;
                case PermissionSearchCondition.PiinDealId:
                    int dealId;
                    if (int.TryParse(searchExpression, out dealId))
                    {
                        HiDealDeal deal = hp.HiDealDealGet(dealId);
                        if(deal != null && deal.IsLoaded)
                        {
                            result = mp.ViewVbsFlattenedAclGetListBySid(this.AccountId, GetRelatedSellerGuid(deal.SellerGuid));
                        }
                        else
                        {
                            result = new ViewVbsFlattenedAclCollection();
                        }
                    }
                    else
                    {
                        result = new ViewVbsFlattenedAclCollection();
                    }

                    break;
                case PermissionSearchCondition.PiinProductId:
                    int pid;
                    if (int.TryParse(searchExpression, out pid))
                    {
                        HiDealProduct prod = hp.HiDealProductGet(pid);
                        if (prod != null && prod.IsLoaded)
                        {
                            result = mp.ViewVbsFlattenedAclGetListBySid(this.AccountId, GetRelatedSellerGuid(prod.SellerGuid));
                        }
                        else
                        {
                            result = new ViewVbsFlattenedAclCollection();
                        }
                    }
                    else
                    {
                        result = new ViewVbsFlattenedAclCollection();
                    }
                    break;
                case PermissionSearchCondition.PponUniqueId:
                    int uniqueId;
                    if (int.TryParse(searchExpression, out uniqueId))
                    {
                        DealProperty dp = pp.DealPropertyGet(uniqueId);
                        BusinessHour bh = pp.BusinessHourGet(dp.BusinessHourGuid);
                        if (bh != null && bh.IsLoaded)
                        {
                            result = mp.ViewVbsFlattenedAclGetListBySid(this.AccountId, GetRelatedSellerGuid(bh.SellerGuid));
                        }
                        else
                        {
                            result = new ViewVbsFlattenedAclCollection();
                        }
                    }
                    else
                    {
                        result = new ViewVbsFlattenedAclCollection();
                    }

                    break;
                case PermissionSearchCondition.SellerName:
                    result = mp.ViewVbsFlattenedAclGetListLikeSellerName(this.AccountId, searchExpression);
                    break;
                default:
                    result = new ViewVbsFlattenedAclCollection();
                    break;
            }
            
            return result;
        }

        private List<VbsVendorAce> TransformDbAclToVbsVendorAcl(ViewVbsFlattenedAclCollection dbAcl)
        {
            List<VbsVendorAce> transformedAcl = dbAcl.Select(
                acl => new VbsVendorAce()
                {
                    SellerGuid = acl.SellerGuid,
                    SellerName = acl.SellerName,
                    StoreGuid = acl.StoreGuid,
                    StoreName = acl.StoreName,
                    StoreShowStatus = acl.StoreStatus.HasValue ? (StoreStatus) acl.StoreStatus.Value : StoreStatus.Available,
                    IsPiinDeal = acl.IsPiinDeal,
                    IsPponDeal = acl.IsPponDeal,
                    MerchandiseName = acl.MerchandiseName,
                    AppTitle = acl.AppTitle,
                    MerchandiseGuid = acl.MerchandiseGuid,
                    MerchandiseId = acl.MerchandiseId,
                    MerchandiseParentId = acl.MerchandiseParentId,
                    MerchandiseStoreGuid = acl.MerchandiseStoreGuid,
                    MerchandiseStoreResourceGuid = acl.MerchandiseStoreResourceGuid,
                    DealOrderStartTime = acl.DealStartTime,
                    DealOrderEndTime = acl.DealEndTime,
                    DealUseStartTime = acl.DealUseStartTime,
                    DealUseEndTime = acl.DealUseEndTime,
                    DealEstablished = acl.DealEstablished,
                    VendorBillingModel = (VendorBillingModel?)acl.VendorBillingModel,
                    RemittanceType = (RemittanceType?)acl.RemittanceType,
                    IsInStore = acl.IsInStore,
                    IsHomeDelivery = acl.IsHomeDelivery,
                    SellerPermission = acl.SellerPermission,
                    StorePermission = acl.StorePermission,
                    DealStorePermission = acl.DealStorePermission,
                    PponPermission = acl.PponPermission,
                    PiinPermission = acl.PiinPermission,
                    LabelIconList = acl.LabelIconList,
                    VbsRight = acl.VbsRight
                }).ToList();
            return transformedAcl;
        }

        #region VbsVendorAceBase

        private IEnumerable<IVbsVendorAce> GetAccountVbsVendorAceBaseList(string accountId, DeliveryType? deliveryType = null, VbsRightFlag? vbsRight = null, bool onlyEstablishedDeal = true)
        {           
            return onlyEstablishedDeal
                    ? GetDealEstablishedDeals(TransformDTToIVbsVendorAce(mp.VbsVendorAceLiteFnGetDataTable(accountId, vbsRight)), deliveryType)
                    : TransformDTToIVbsVendorAce(mp.VbsVendorAceLiteFnGetDataTable(accountId, vbsRight));
        }

        private IEnumerable<IVbsVendorAce> GetAccountVbsVendorAceBaseList(string accountId, Guid dealGuid, DeliveryType? deliveryType, VbsRightFlag? vbsRight = null)
        {
            return GetDealEstablishedDeals(TransformDTToIVbsVendorAce(mp.VbsVendorAceLiteFnGetDataTable(accountId, dealGuid, vbsRight)), deliveryType);
        }

        private List<IVbsVendorAce> TransformDTToIVbsVendorAce(DataTable dt)
        {
            List<IVbsVendorAce> result = new List<IVbsVendorAce>();

            foreach (DataRow dr in dt.Rows)
            {
                VbsVendorAceLite item = new VbsVendorAceLite();
                item.SellerGuid = (Guid)dr["seller_guid"];
                item.SellerName = (string)dr["seller_name"];
                item.StoreGuid = dr["store_guid"] == DBNull.Value ? (Guid?)null : (Guid)dr[3];
                item.StoreName = (string)dr["store_name"];
                item.StoreShowStatus = dr["store_status"] == DBNull.Value ? StoreStatus.Available : (StoreStatus)(int)dr["store_status"];
                item.MerchandiseGuid = (Guid)dr["merchandise_guid"];
                item.IsPiinDeal = (int)dr["is_piinlife"] == 1;
                item.IsPponDeal = (int)dr["is_ppon"] == 1;
                item.MerchandiseId = dr["merchandise_id"] == DBNull.Value ? (int?)null : (int)dr["merchandise_id"];
                item.MerchandiseParentId = dr["merchandise_parent_id"] == DBNull.Value ? (int?)null : (int)dr["merchandise_parent_id"];
                item.MerchandiseStoreGuid = dr["merchandise_store_guid"] == DBNull.Value ? (Guid?)null : (Guid)dr["merchandise_store_guid"];
                item.MerchandiseStoreResourceGuid = dr["merchandise_store_resource_guid"] == DBNull.Value ? (Guid?)null : (Guid)dr["merchandise_store_resource_guid"];
                item.MerchandiseName = (string)dr["merchandise_name"];
                item.AppTitle = dr["app_title"] == DBNull.Value ? string.Empty : (string)dr["app_title"];
                item.DealOrderStartTime = dr["deal_start_time"] == DBNull.Value ? (DateTime?)null : (DateTime)dr["deal_start_time"];
                item.DealOrderEndTime = dr["deal_end_time"] == DBNull.Value ? (DateTime?)null : (DateTime)dr["deal_end_time"];
                item.DealUseStartTime = dr["deal_use_start_time"] == DBNull.Value ? (DateTime?)null : (DateTime)dr["deal_use_start_time"];
                item.DealUseEndTime = dr["deal_use_end_time"] == DBNull.Value ? (DateTime?)null : (DateTime)dr["deal_use_end_time"];
                item.DealEstablished = dr["deal_established"] == DBNull.Value ? (int?)null : (int)dr["deal_established"];
                item.VendorBillingModel = dr["vendor_billing_model"] == DBNull.Value ? (VendorBillingModel?)null : (VendorBillingModel)dr["vendor_billing_model"];
                item.RemittanceType = dr["remittance_type"] == DBNull.Value ? (RemittanceType?)null : (RemittanceType)dr["vendor_billing_model"];
                item.IsInStore = dr["is_in_store"] == DBNull.Value ? (bool?)null : (bool)dr["is_in_store"];
                item.IsHomeDelivery = dr["is_home_delivery"] == DBNull.Value ? (bool?)null : (bool)dr["is_home_delivery"];
                item.LabelIconList = dr["label_icon_list"] == DBNull.Value ? null : (string)dr["label_icon_list"];
                item.ShipType = dr["ship_type"] == DBNull.Value ? (int?)null : (int)dr["ship_type"];
                item.VbsRight = (int)dr["vbs_right"];
                item.IsSellerPermissionAllow = (bool)dr["seller_permission"];
                item.IsRestrictedStore = (bool)dr["is_restricted_store"];
                item.IsWms = (int)dr["is_wms"] == 1;
                result.Add(item);
            }

            return result;
        }

        private IEnumerable<IVbsVendorAce> GetDealEstablishedDeals(List<IVbsVendorAce> allowedDeals, DeliveryType? deliveryType)
        {
            var now = DateTime.Now;

            return allowedDeals
                .Where(x =>
                {
                    if (deliveryType == DeliveryType.ToHouse)
                    {
                        if (!x.IsHomeDelivery.GetValueOrDefault(false))  //只抓取宅配檔次
                                    {
                            return false;
                        }

                        if (x.VendorBillingModel.HasValue)   //只抓取對帳方式選擇[新版核銷對帳系統]的宅配檔次
                                    {
                            if (!x.VendorBillingModel.Value.Equals(VendorBillingModel.BalanceSheetSystem))
                            {
                                return false;
                            }
                        }
                    }
                    else if (deliveryType == DeliveryType.ToShop)
                    {
                        if (!x.IsInStore.GetValueOrDefault(false))  //只抓取憑證檔次
                        {
                            return false;
                        }
                    }

                    if (!x.DealOrderStartTime.HasValue || !x.DealOrderEndTime.HasValue ||
                        !x.DealUseStartTime.HasValue || !x.DealUseEndTime.HasValue) //不撈取未設定販賣時間及兌換時間之異常檔次
                    {
                        return false;
                    }

                    //開賣時間還沒開始，不顯示
                    if (DateTime.Compare(now, x.DealOrderStartTime.Value) < 0)
                    {
                        return false;
                    }

                    if (x.DealEstablished.HasValue)
                    {
                        return int.Equals(1, x.DealEstablished); //過門檻
                    }

                    if (x.IsPiinDeal.GetValueOrDefault(false))
                    {
                        return true;
                    }

                    return DateTime.Compare(now, x.DealOrderEndTime.Value) < 0; //目前時間未超過結檔時間   
                });            
        }

        #endregion

        private IEnumerable<Guid> GetRelatedSellerGuid(Guid sellerGuid)
        {
            //return SellerFacade.GetSellerTreeSellerGuids(sellerGuid);
            return new List<Guid> { sellerGuid };
        }
    }

    


    

    public enum ObjectResourceType
    {
        Unknown = 0,
        Seller = 1,
        Store = 2,
        HiDealProduct = 3,
        BusinessHour = 4,
        HiDealProductStore = 5,
        PponStore = 6,
    }

    public enum PermissionSearchCondition
    {
        Unknown,
        SellerName,
        Bid,
        PponUniqueId,
        PiinDealId,
        PiinProductId
    }

}

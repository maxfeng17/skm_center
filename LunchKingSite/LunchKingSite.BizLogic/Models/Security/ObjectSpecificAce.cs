﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model
{
    public abstract class ObjectSpecificAce
    {
        public virtual string AccountId { get; set; }
        public virtual Guid ObjectGuid { get; set; }
        public virtual ObjectResourceType ObjectType { get; set; }
        /// <summary>
        /// cummulative permission result:
        /// resource is allowed when 
        /// resource is not denied AND not all are null
        /// i.e. no deny exists in the hierarchy and at least one allow exists in the hierarchy
        /// </summary>
        public virtual bool IsAllowed { get; set; }
        /// <summary>
        /// cummulative permission result:
        /// resource is denied when:
        /// (upper hierarchy has deny) 
        /// OR ( current resource is deny) 
        /// </summary>
        public virtual bool IsDenied { get; set; }
        /// <summary>
        /// resource is allowable when 
        /// (upper hierarchy is null AND (current resource is null OR deny))  
        /// or (upper has no deny AND upper has allow AND current is deny) 
        /// or (upper has deny)    **because upper has deny, upper must be allowable => current resource is allowable**
        /// </summary>
        public virtual bool IsAllowable { get; set; }
        /// <summary>
        /// resource is deniable when 
        /// (upper hierarchy is null AND current resource is allow)
        /// OR (upper hierarchy has no deny and upper hierarchy has allow AND current resource is not deny)
        /// </summary>
        public virtual bool IsDeniable { get; set; }

        /// <summary>
        /// is there an explicit "allow" permission set on this resource
        /// </summary>
        public virtual bool IsPermissionAllowed { get; set; }
        /// <summary>
        /// is there an explicit "deny" permission set on this resource
        /// </summary>
        public virtual bool IsPermissionDenied { get; set; }

        public virtual bool UpperHierarchyHasDeny { get; set; }

        public virtual bool UpperHierarchyHasAllow { get; set; }
    }
}

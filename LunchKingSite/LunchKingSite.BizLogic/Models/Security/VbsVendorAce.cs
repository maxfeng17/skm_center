﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model
{
    public interface IVbsVendorAce
    {
        Guid SellerGuid { get; set; }
        string SellerName { get; set; }
        Guid? StoreGuid { get; set; }
        string StoreName { get; set; }
        StoreStatus StoreShowStatus { get; set; }
        bool? IsPiinDeal { get; set; }
        bool? IsPponDeal { get; set; }
        string MerchandiseName { get; set; }
        string AppTitle { get; set; }
        Guid? MerchandiseGuid { get; set; }
        int? MerchandiseId { get; set; }
        int? MerchandiseParentId { get; set; }
        Guid? MerchandiseStoreGuid { get; set; }
        Guid? MerchandiseStoreResourceGuid { get; set; }
        DateTime? DealOrderStartTime { get; set; }
        DateTime? DealOrderEndTime { get; set; }
        DateTime? DealUseStartTime { get; set; }
        DateTime? DealUseEndTime { get; set; }
        int? DealEstablished { get; set; }
        VendorBillingModel? VendorBillingModel { get; set; }
        RemittanceType? RemittanceType { get; set; }
        bool? IsInStore { get; set; }
        bool? IsHomeDelivery { get; set; }
        string LabelIconList { get; set; }
        int? ShipType { get; set; }
        int? VbsRight { get; set; }
        bool IsSellerPermissionAllow { get; }
        bool IsRestrictedStore { get; set; }
        bool IsWms { get; set; }
    }



    /// <summary>
    /// 商家帳號有哪些可允許檔次權限，提供一個介面同VbsVendorAce，但效能較好的替代品
    /// </summary>
    public class VbsVendorAceLite : IVbsVendorAce
    {
        public Guid SellerGuid { get; set; }
        public string SellerName { get; set; }
        public Guid? StoreGuid { get; set; }
        public string StoreName { get; set; }
        public StoreStatus StoreShowStatus { get; set; }
        public bool? IsPiinDeal { get; set; }
        public bool? IsPponDeal { get; set; }
        public string MerchandiseName { get; set; }
        public string AppTitle { get; set; }
        public Guid? MerchandiseGuid { get; set; }
        public int? MerchandiseId { get; set; }
        public int? MerchandiseParentId { get; set; }
        public Guid? MerchandiseStoreGuid { get; set; }
        public Guid? MerchandiseStoreResourceGuid { get; set; }
        public DateTime? DealOrderStartTime { get; set; }
        public DateTime? DealOrderEndTime { get; set; }
        public DateTime? DealUseStartTime { get; set; }
        public DateTime? DealUseEndTime { get; set; }
        public int? DealEstablished { get; set; }
        public VendorBillingModel? VendorBillingModel { get; set; }
        public RemittanceType? RemittanceType { get; set; }
        public bool? IsInStore { get; set; }
        public bool? IsHomeDelivery { get; set; }
        public string LabelIconList { get; set; }
        public int? ShipType { get; set; }
        public int? VbsRight { get; set; }
        public bool IsSellerPermissionAllow { get; set; }
        public bool IsRestrictedStore { get; set; }
        public bool IsWms { get; set; }
    }

    public class VbsVendorAce : ObjectSpecificAce, IVbsVendorAce
    {
        public override Guid ObjectGuid
        {
            get
            {
                switch (this.ObjectType)
                {
                    case ObjectResourceType.Seller:
                        return this.SellerGuid;
                    case ObjectResourceType.Store:
                        return this.StoreGuid.Value;
                    case ObjectResourceType.BusinessHour:
                    case ObjectResourceType.HiDealProduct:
                        return this.MerchandiseGuid.Value;
                    case ObjectResourceType.PponStore:
                    case ObjectResourceType.HiDealProductStore:
                        return this.MerchandiseStoreResourceGuid.Value;
                    default:
                        throw new Exception("unexpected ResourceType");
                }
            }
        }
        public override ObjectResourceType ObjectType
        {
            get
            {
                if (!this.StoreGuid.HasValue && !this.MerchandiseGuid.HasValue)
                {
                    return ObjectResourceType.Seller;
                }
                if (this.StoreGuid.HasValue && !this.MerchandiseGuid.HasValue)
                {
                    return ObjectResourceType.Store;
                }
                //宅配檔次有設定ppon_store 仍須判斷為檔次權限 而非檔次分店權限
                if (this.MerchandiseGuid.HasValue && this.IsPponDeal.HasValue && this.IsPponDeal.Value && 
                    ((this.IsInStore.HasValue && this.IsInStore.Value && !this.MerchandiseStoreGuid.HasValue) || 
                     (this.IsHomeDelivery.HasValue && this.IsHomeDelivery.Value)))
                {
                    return ObjectResourceType.BusinessHour;
                }
                if (this.MerchandiseGuid.HasValue && this.IsPiinDeal.HasValue && this.IsPiinDeal.Value && !this.MerchandiseStoreGuid.HasValue)
                {
                    return ObjectResourceType.HiDealProduct;
                }
                if (this.MerchandiseStoreGuid.HasValue && this.IsPponDeal.HasValue && this.IsPponDeal.Value)
                {
                    return ObjectResourceType.PponStore;
                }
                if (this.MerchandiseStoreGuid.HasValue && this.IsPiinDeal.HasValue && this.IsPiinDeal.Value)
                {
                    return ObjectResourceType.HiDealProductStore;
                }

                return ObjectResourceType.Unknown;
            }
        }


        public Guid SellerGuid { get; set; }
        public string SellerName { get; set; }
        public Guid? StoreGuid { get; set; }
        public string StoreName { get; set; }
        public StoreStatus StoreShowStatus { get; set; }
        public bool? IsPiinDeal { get; set; }
        public bool? IsPponDeal { get; set; }
        public string MerchandiseName { get; set; }
        public string AppTitle { get; set; }
        public Guid? MerchandiseGuid { get; set; }
        public int? MerchandiseId { get; set; }
        public int? MerchandiseParentId { get; set; }
        public Guid? MerchandiseStoreGuid { get; set; }
        public Guid? MerchandiseStoreResourceGuid { get; set; }
        public DateTime? DealOrderStartTime { get; set; }
        public DateTime? DealOrderEndTime { get; set; }
        public DateTime? DealUseStartTime { get; set; }
        public DateTime? DealUseEndTime { get; set; }
        public int? DealEstablished { get; set; }
        public VendorBillingModel? VendorBillingModel { get; set; }
        public RemittanceType? RemittanceType { get; set; }
        public bool? IsInStore { get; set; }
        public bool? IsHomeDelivery { get; set; }
        public string LabelIconList { get; set; }
        internal int? SellerPermission { get; set; }
        internal int? StorePermission { get; set; }
        internal int? DealStorePermission { get; set; }
        internal int? PponPermission { get; set; }
        internal int? PiinPermission { get; set; }
        public int? VbsRight { get; set; }
        public int? ShipType { get; set; }
        public bool IsRestrictedStore { get; set; }

        public override bool IsAllowable
        {
            get
            {
                switch (this.ObjectType)
                {
                    case ObjectResourceType.Seller:
                        return !IsSellerPermissionAllow;
                    case ObjectResourceType.Store:
                        return (IsSellerPermissionNull && !IsStorePermissionAllow)
                               || (!IsSellerPermissionDeny && IsStorePermissionDeny)
                               || IsSellerPermissionDeny;
                    case ObjectResourceType.BusinessHour:
                    case ObjectResourceType.HiDealProduct:
                        return (IsSellerPermissionNull && !IsMerchandisePermissionAllow)
                               || (!IsSellerPermissionDeny && IsMerchandisePermissionDeny)
                               || IsSellerPermissionDeny;
                    case ObjectResourceType.PponStore:
                    case ObjectResourceType.HiDealProductStore:
                        return (IsSellerPermissionNull && IsStorePermissionNull && !IsMerchandiseStorePermissionAllow)
                               || ((!IsSellerPermissionDeny && !IsStorePermissionDeny)
                                   && (IsSellerPermissionAllow || IsStorePermissionAllow)
                                   && IsMerchandiseStorePermissionDeny)
                               || IsSellerPermissionDeny || IsStorePermissionDeny;
                    default:
                        throw new Exception("unexpected ResourceType");
                }
            }
        }

        public override bool IsDeniable
        {
            get
            {
                switch (this.ObjectType)
                {
                    case ObjectResourceType.Seller:
                        return IsSellerPermissionAllow;
                    case ObjectResourceType.Store:
                        return (IsSellerPermissionNull && IsStorePermissionAllow)
                               || (IsSellerPermissionAllow && !IsStorePermissionDeny);
                    case ObjectResourceType.BusinessHour:
                    case ObjectResourceType.HiDealProduct:
                        return (IsSellerPermissionNull && IsMerchandisePermissionAllow)
                               || (IsSellerPermissionAllow && !IsMerchandisePermissionDeny);
                    case ObjectResourceType.PponStore:
                    case ObjectResourceType.HiDealProductStore:
                        return (IsSellerPermissionNull && IsStorePermissionNull && IsMerchandiseStorePermissionAllow)
                               || ((!IsSellerPermissionDeny && !IsStorePermissionDeny)
                                    && (IsSellerPermissionAllow || IsStorePermissionAllow)
                                    && !IsMerchandiseStorePermissionDeny);
                    default:
                        throw new Exception("unexpected ResourceType");
                }
            }
            set
            {
                base.IsDeniable = value;
            }
        }

        public override bool IsAllowed
        {
            get
            {
                switch (this.ObjectType)
                {
                    case ObjectResourceType.Seller:
                        return IsSellerPermissionAllow;
                    case ObjectResourceType.Store:
                        return !IsDenied && (IsSellerPermissionAllow || IsStorePermissionAllow);
                    case ObjectResourceType.BusinessHour:
                    case ObjectResourceType.HiDealProduct:
                        return !IsDenied && (IsSellerPermissionAllow || IsMerchandisePermissionAllow);
                    case ObjectResourceType.PponStore:
                    case ObjectResourceType.HiDealProductStore:
                        return !IsDenied && (IsSellerPermissionAllow || IsStorePermissionAllow || IsMerchandiseStorePermissionAllow);
                    default:
                        throw new Exception("unexpected ResourceType");
                }
            }
        }

        public override bool IsDenied
        {
            get
            {
                switch (this.ObjectType)
                {
                    case ObjectResourceType.Seller:
                        return IsSellerDenied;
                    case ObjectResourceType.Store:
                        return IsStoreDenied;
                    case ObjectResourceType.BusinessHour:
                    case ObjectResourceType.HiDealProduct:
                        return IsMerchandiseDenied;
                    case ObjectResourceType.PponStore:
                    case ObjectResourceType.HiDealProductStore:
                        return IsMerchandiseStoreDenied;
                    default:
                        throw new Exception("unexpected ResourceType");
                }
            }

        }

        public override bool IsPermissionAllowed
        {
            get
            {
                switch (this.ObjectType)
                {
                    case ObjectResourceType.Seller:
                        return IsSellerPermissionAllow;
                    case ObjectResourceType.Store:
                        return IsStorePermissionAllow;
                    case ObjectResourceType.BusinessHour:
                    case ObjectResourceType.HiDealProduct:
                        return IsMerchandisePermissionAllow;
                    case ObjectResourceType.PponStore:
                    case ObjectResourceType.HiDealProductStore:
                        return IsMerchandiseStorePermissionAllow;
                    default:
                        throw new Exception("unexpected ResourceType");
                }
            }
        }

        public override bool IsPermissionDenied
        {
            get
            {
                switch (this.ObjectType)
                {
                    case ObjectResourceType.Seller:
                        return IsSellerPermissionDeny;
                    case ObjectResourceType.Store:
                        return IsStorePermissionDeny;
                    case ObjectResourceType.BusinessHour:
                    case ObjectResourceType.HiDealProduct:
                        return IsMerchandisePermissionDeny;
                    case ObjectResourceType.PponStore:
                    case ObjectResourceType.HiDealProductStore:
                        return IsMerchandiseStorePermissionDeny;
                    default:
                        throw new Exception("unexpected ResourceType");
                }
            }
        }

        public override bool UpperHierarchyHasDeny
        {
            get
            {
                switch (this.ObjectType)
                {
                    case ObjectResourceType.Seller:
                        return false;
                    case ObjectResourceType.Store:
                        return IsSellerPermissionDeny;
                    case ObjectResourceType.BusinessHour:
                    case ObjectResourceType.HiDealProduct:
                        return IsSellerPermissionDeny;
                    case ObjectResourceType.PponStore:
                    case ObjectResourceType.HiDealProductStore:
                        return IsSellerPermissionDeny || IsStorePermissionDeny;
                    default:
                        throw new Exception("unexpected ResourceType");
                }
            }
        }

        public override bool UpperHierarchyHasAllow
        {
            get
            {
                switch (this.ObjectType)
                {
                    case ObjectResourceType.Seller:
                        return false;
                    case ObjectResourceType.Store:
                        return IsSellerPermissionAllow;
                    case ObjectResourceType.BusinessHour:
                    case ObjectResourceType.HiDealProduct:
                        return IsSellerPermissionAllow;
                    case ObjectResourceType.PponStore:
                    case ObjectResourceType.HiDealProductStore:
                        return IsSellerPermissionAllow || IsStorePermissionAllow;
                    default:
                        throw new Exception("unexpected ResourceType");
                }
            }
        }

        private bool IsSellerDenied
        {
            get { return IsSellerPermissionDeny; }
        }
        private bool IsStoreDenied
        {
            get
            {
                return IsSellerPermissionDeny
                       || IsStorePermissionDeny;
            }
        }
        private bool IsMerchandiseDenied
        {
            get
            {
                return IsSellerPermissionDeny
                       || IsMerchandisePermissionDeny;
            }
        }
        private bool IsMerchandiseStoreDenied
        {
            get
            {
                return (IsSellerPermissionDeny || IsStorePermissionDeny)
                       || (IsMerchandiseStorePermissionDeny);
            }
        }


        public bool IsSellerPermissionAllow
        {
            get
            {
                return this.SellerPermission.HasValue && ((PermissionType)this.SellerPermission.Value).HasFlag(PermissionType.Read);
            }
            set
            {
                this.IsSellerPermissionAllow = value;
            }
        }
        private bool IsSellerPermissionDeny
        {
            get
            {
                return this.SellerPermission.HasValue && !((PermissionType)this.SellerPermission.Value).HasFlag(PermissionType.Read);
            }
        }
        private bool IsSellerPermissionNull
        {
            get { return !this.SellerPermission.HasValue; }
        }

        private bool IsStorePermissionAllow
        {
            get
            {
                return this.StorePermission.HasValue && ((PermissionType)this.StorePermission.Value).HasFlag(PermissionType.Read);
            }
        }
        private bool IsStorePermissionDeny
        {
            get
            {
                return this.StorePermission.HasValue && !((PermissionType)this.StorePermission.Value).HasFlag(PermissionType.Read);
            }
        }
        private bool IsStorePermissionNull
        {
            get
            {
                return !this.StorePermission.HasValue;
            }
        }

        private bool IsMerchandisePermissionAllow
        {
            get
            {
                if (this.ObjectType == ObjectResourceType.BusinessHour)
                {
                    return this.PponPermission.HasValue && ((PermissionType)this.PponPermission.Value).HasFlag(PermissionType.Read);
                }
                return this.PiinPermission.HasValue && ((PermissionType)this.PiinPermission.Value).HasFlag(PermissionType.Read);
            }
        }
        private bool IsMerchandisePermissionDeny
        {
            get
            {
                if (this.ObjectType == ObjectResourceType.BusinessHour)
                {
                    return this.PponPermission.HasValue && !((PermissionType)this.PponPermission.Value).HasFlag(PermissionType.Read);
                }
                return this.PiinPermission.HasValue && !((PermissionType)this.PiinPermission.Value).HasFlag(PermissionType.Read);
            }
        }
        private bool IsMerchandisePermissionNull
        {
            get
            {
                if (this.ObjectType == ObjectResourceType.BusinessHour)
                {
                    return !this.PponPermission.HasValue;
                }
                return !this.PiinPermission.HasValue;
            }
        }

        private bool IsMerchandiseStorePermissionAllow
        {
            get
            {
                return this.DealStorePermission.HasValue && ((PermissionType)this.DealStorePermission.Value).HasFlag(PermissionType.Read);
            }
        }
        private bool IsMerchandiseStorePermissionDeny
        {
            get
            {
                return this.DealStorePermission.HasValue && !((PermissionType)this.DealStorePermission.Value).HasFlag(PermissionType.Read);
            }
        }
        private bool IsMerchandiseStorePermissionNull
        {
            get { return !this.DealStorePermission.HasValue; }
        }

        public bool IsWms { get; set; }
    }
}

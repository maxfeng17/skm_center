﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MasterPass
{
    /// <summary>
    /// APP buy 串接 MasterPass 導 MasterPass頁model
    /// </summary>
    public class GotoMasterPassWithCheckoutInputModel
    {
        /// <summary>
        /// 訂單總金額
        /// </summary>
        public int SubTotal { set; get; }
        /// <summary>
        /// Master Pass Card Id
        /// </summary>
        public string CardId { set; get; }
        /// <summary>
        /// MasterPass Precheckout transaction id
        /// </summary>
        public string PrecheckoutTransactionId { set; get; }
        /// <summary>
        /// MasterPass Wallet name
        /// </summary>
        public string WalletName { set; get; }
        /// <summary>
        /// MasterPass Consumer wallet id
        /// </summary>
        public string ConsumerWalletId { set; get; }
        /// <summary>
        /// 驗證資料
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (SubTotal <= 0 || string.IsNullOrEmpty(CardId) || string.IsNullOrEmpty(PrecheckoutTransactionId) || string.IsNullOrEmpty(WalletName))
                {
                    return false;
                }
                return true;
            }
        }
    }
}

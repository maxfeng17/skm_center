﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model
{
    public class MasterPassCardInfo
    {
        public string CardTitle { set; get; }
        public string CardId { set; get; }

        public MasterPassCardInfo(MasterPassCard card)
        {
            CardTitle = string.Format("{0}-{1}{2}", card.CardHolderName, card.LastFour
                , string.IsNullOrEmpty(card.BrandName) ? string.Empty : string.Format("({0})", card.BrandName));
            CardId = string.Format("{0},{1},{2}", card.CardId, card.ExpiryMonth.ToString().PadLeft(2, '0'),
                card.ExpiryYear.ToString());
        }
    }
}

﻿using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Model
{
    public class MasterPassPreCheckoutData
    {
        public bool IsCheckoutPass { set; get; }
        public string CheckoutFailReason { set; get; }
        public string MasterpassLogoUrl { set; get; }
        public string WalletPartnerLogoUrl { set; get; }
        public string ConsumerWalletId { set; get; }
        public string PrecheckoutTransactionId { set; get; }
        public string WalletName { set; get; }
        public List<MasterPassCard> Cards { set; get; }
    }

    public class MasterPassCard
    {
        public string BrandName { set; get; }
        public string CardAlias { set; get; }
        public string CardHolderName { set; get; }
        public string CardId { set; get; }
        public string LastFour { set; get; }
        public bool SelectedAsDefault { set; get; }
        public int ExpiryMonth { set; get; }
        public int ExpiryYear { set; get; }
    }
}

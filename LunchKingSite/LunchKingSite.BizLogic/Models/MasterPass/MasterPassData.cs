﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model
{
    public class MasterPassData
    {
        public string CallbackUrl { set; get; }
        public string OriginUrl { set; get; }
        public string RequestToken { set; get; }
        public string PairingToken { set; get; }
        public List<string> PairingDataTypes { get; set; }
        public string CheckoutIdentifier { set; get; }
        public string AcceptedCards { set; get; }
        public bool ShippingSuppression { set; get; }
        public bool RewardsProgram { set; get; }
        public bool AuthLevelBasic { set; get; }

        public string MerchantInitRequest { set; get; }
        public string MerchantInitResponse { set; get; }
        public string ShoppingCartResponse { set; get; }
        public bool IsLoaded { set; get; }
        public bool IsLogin { set; get; }
        public bool IsValid { set; get; }

        public MasterPassData ()
        {
            IsValid = true;
            IsLogin = false;
            IsLoaded = false;
            PairingDataTypes = new List<string>();
        }
    }

    public class MasterPassCallBackModel
    {
        /// <summary>
        /// 驗證token
        /// </summary>
        public string AuthToken { set; get; }
        /// <summary>
        /// 驗證碼
        /// </summary>
        public int VerifierCode { set; get; }
        /// <summary>
        /// Master Pass Result
        /// </summary>
        public MasterPassApiResult ResultCode { set; get; }
        /// <summary>
        /// Master Pass Result Code Number
        /// </summary>
        public int ResultCodeNum
        {
            get { return (int) ResultCode; }
        }
        /// <summary>
        /// remark
        /// </summary>
        public string Message { set; get; }
    }
}

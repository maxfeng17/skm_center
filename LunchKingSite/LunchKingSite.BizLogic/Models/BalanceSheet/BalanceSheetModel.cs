﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using Amazon.CloudSearch.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using NPOI.HSSF.Record.Formula.Functions;

namespace LunchKingSite.BizLogic.Vbs.BS
{    
    public class BalanceSheetModel
    {
        #region fields

        private static IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IWmsProvider wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();

        private BalanceSheet _dbBs = null;
        private BalanceSheet DbBs
        {
            get
            {
                if (_dbBs == null)
                {
                    _dbBs = ap.BalanceSheetGet(_id);
                }
                return _dbBs;
            }
        }

        #endregion

        #region properties

        private readonly int _id;
        public int Id
        {
            get
            {
                return _id;
            }
        }

        public int? Year
        {
            get { return this.DbBs.Year; }
        }
        public int? Month
        {
            get { return this.DbBs.Month; }
        }
                
        public int? Day
        {
            get 
            {
                if (this.DbBs.GenerationFrequency == (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet)
                {
                    return this.DbBs.IntervalEnd.Day;
                }
                return null;
            }
        }
        public BalanceSheetType Type
        {
            get { return (BalanceSheetType)this.DbBs.BalanceSheetType; }
        }
        public BusinessModel BizModel
        {
            get { return (BusinessModel)this.DbBs.ProductType; }
        }
        public bool IsConfirmed
        {
            get { return this.DbBs.IsConfirmedReadyToPay; }
        }
        public Guid MerchandiseGuid
        {
            get { return this.DbBs.ProductGuid; }
        }
        public Guid? StoreGuid
        {
            get{ return this.DbBs.StoreGuid; }
        }
        
        private FundTransferInfo _transferInfo;
        /// <summary>
        /// 匯款資訊
        /// </summary>
        public FundTransferInfo TransferInfo
        {
            get
            {
                if(_transferInfo == null)
                {
                    _transferInfo = new FundTransferInfo(this.Id, GetAccountsPayable, this.Type);
                }
                return _transferInfo;
            }
        }

        /// <summary>
        /// 單據可否確認付款
        /// </summary>
        public bool IsBillingConfirmable
        {
            get { return this.GetBillingConfirmationState() == BillingConfirmationState.Confirmable; }
        }

        private List<DistributedBill> _billDistribution = null;
        public IEnumerable<DistributedBill> BillDistribution
        {
            get
            {
                if (_billDistribution == null)
                {
                    List<DistributedBill> result = new List<DistributedBill>();
                    var rels = ap.BalanceSheetBillRelationshipGetListByBalanceSheetId(this._id);
                    foreach (var rel in rels)
                    {
                        result.Add(new DistributedBill(rel.BillId, rel.BillMoney, rel.BillMoneyNotaxed, rel.BillTax, rel.Remark));
                    }
                    _billDistribution = result;
                }
                return _billDistribution;
            }
        }

        #endregion

        #region public methods
        
        public BalanceSheet GetDbBalanceSheet()
        {
            var result = this.DbBs.Clone();
            if (this.DbBs.IsLoaded)
            {
                result.IsNew = false;
                result.IsLoaded = true;            
            }            
            return result;            
        }

        private string _storeName = null;
        public string GetStoreName()
        {
            if (_storeName == null)
            {
                if (this.StoreGuid.HasValue)
                {
                    _storeName = sp.StoreGet(this.StoreGuid.Value).StoreName;
                }
                else
                {
                    _storeName = string.Empty;
                }
            }
            return _storeName;
        }

        /// <summary>
        /// 取對帳單之應付金額
        /// </summary>
        public TotalAmountDetail GetAccountsPayable()
        {
            if (this.DbBs == null || this.DbBs.IsNew)
            {
                throw new InvalidOperationException(string.Format("balance sheet (id={0}) does not exist!", this.Id));
            }

            var bsType = (BalanceSheetType)this.DbBs.BalanceSheetType;
            
            switch(bsType)
            {
                case BalanceSheetType.WeeklyPayWeekBalanceSheet:
                case BalanceSheetType.ManualWeeklyPayWeekBalanceSheet:
                case BalanceSheetType.MonthlyPayBalanceSheet:
                case BalanceSheetType.ManualMonthlyPayBalanceSheet:
                case BalanceSheetType.ManualTriggered:
                case BalanceSheetType.FlexiblePayBalanceSheet:
                case BalanceSheetType.WeeklyPayBalanceSheet:
                    return DirectlyGetAccountsPayable(this.DbBs);
                case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
                    return IndirectlyGetAccountsPayable(this.DbBs);
                case BalanceSheetType.ManualLumpSumPayBalanceSheet:
                case BalanceSheetType.WeeklyToHouseBalanceSheet:
                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                case BalanceSheetType.MonthlyToHouseBalanceSheet:
                case BalanceSheetType.FlexibleToHouseBalanceSheet:
                    return GetToHouseDealAmountByPpon();
                default:
                    throw new InvalidOperationException(string.Format("Data corruption suspected! Unexpected balance sheet type! (id={0}) ", this.Id));
            }
        }

        /// <summary>
        /// 取對帳單已分配到單據內的金額
        /// </summary>
        public int GetDistributedAmount()
        {
            if (this.DbBs == null || this.DbBs.IsNew)
            {
                throw new InvalidOperationException(string.Format("balance sheet (id={0}) does not exist!", this.Id));
            }

            var bsType = (BalanceSheetType)this.DbBs.BalanceSheetType;

            switch (bsType)
            {
                case BalanceSheetType.MonthlyPayBalanceSheet:
                case BalanceSheetType.ManualMonthlyPayBalanceSheet:
                case BalanceSheetType.ManualTriggered:
                case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
                case BalanceSheetType.ManualLumpSumPayBalanceSheet:
                case BalanceSheetType.FlexibleToHouseBalanceSheet:
                case BalanceSheetType.MonthlyToHouseBalanceSheet:
                case BalanceSheetType.WeeklyToHouseBalanceSheet:
                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                case BalanceSheetType.FlexiblePayBalanceSheet:
                case BalanceSheetType.WeeklyPayBalanceSheet:
                    BalanceSheetBillRelationshipCollection bsBills =
                        ap.BalanceSheetBillRelationshipGetListByBalanceSheetId(this.DbBs.Id);
                    return bsBills.Sum(bill => bill.BillMoney);
                default:
                    throw new InvalidOperationException("monthly balance sheet only!");
            }
        }

        /// <summary>
        /// 取得對帳單對應的單據數量
        /// </summary>
        private int GetBillCount()
        {
            if(this.DbBs.IsLoaded)
            {
                return ap.BalanceSheetBillRelationshipGetCount(this.DbBs.Id);
            }
            return 0;
        }
                
        /// <summary>
        /// 改變對帳單可付款狀態
        /// </summary>
        public BillingConfirmationState ConfirmBillingStatement(string user)
        {
            BillingConfirmationState currentState = GetBillingConfirmationState();
            if (currentState == BillingConfirmationState.Confirmable)
            {
                var bsType = (BalanceSheetType)this.DbBs.BalanceSheetType;

                switch (bsType)
                {
                    case BalanceSheetType.ManualMonthlyPayBalanceSheet:
                    case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
                    case BalanceSheetType.MonthlyPayBalanceSheet:
                    case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                    case BalanceSheetType.ManualTriggered:
                    case BalanceSheetType.ManualLumpSumPayBalanceSheet:
                    case BalanceSheetType.FlexibleToHouseBalanceSheet:
                    case BalanceSheetType.MonthlyToHouseBalanceSheet:
                    case BalanceSheetType.WeeklyToHouseBalanceSheet:
                    case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                    case BalanceSheetType.FlexiblePayBalanceSheet:
                    case BalanceSheetType.WeeklyPayBalanceSheet:
                        ConfirmCurrentBs(user);
                        this._dbBs = null;
                        return BillingConfirmationState.Confirmed;
                    case BalanceSheetType.WeeklyPayWeekBalanceSheet:
                    case BalanceSheetType.ManualWeeklyPayWeekBalanceSheet:
                        throw new InvalidOperationException();
                    default:
                        return BillingConfirmationState.UnknownFailure;
                }                   
            }
            else
            {
                return currentState;
            }
        }

        public bool CancelBsConfirmedAndReceiptReceived()
        {
            if (!this.DbBs.IsConfirmedReadyToPay && !this.DbBs.IsReceiptReceived)
            {
                return false;
            }

            var bsType = (BalanceSheetType)this.DbBs.BalanceSheetType; ;
            switch (bsType)
            {
                case BalanceSheetType.ManualMonthlyPayBalanceSheet:
                case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
                case BalanceSheetType.MonthlyPayBalanceSheet:
                case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                case BalanceSheetType.ManualTriggered:
                case BalanceSheetType.ManualLumpSumPayBalanceSheet:
                case BalanceSheetType.FlexibleToHouseBalanceSheet:
                case BalanceSheetType.MonthlyToHouseBalanceSheet:
                case BalanceSheetType.WeeklyToHouseBalanceSheet:
                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                case BalanceSheetType.FlexiblePayBalanceSheet:
                case BalanceSheetType.WeeklyPayBalanceSheet:
                    CancelCurrentBsConfirmedAndReceiptReceived();
                    this._dbBs = null;
                    return true;
                case BalanceSheetType.WeeklyPayWeekBalanceSheet:
                case BalanceSheetType.ManualWeeklyPayWeekBalanceSheet:
                    throw new InvalidOperationException();
                default:
                    return false;
            } 
        }

        /// <summary>
        /// 匯入匯款資料
        /// </summary>
        public void ImportPaymentResult()
        {
            throw new NotImplementedException();
        }
        
        public bool DistributedAmountCheck()
        {
            int payable;
            if (this.Type == BalanceSheetType.WeeklyPayMonthBalanceSheet || 
                this.Type == BalanceSheetType.ManualWeeklyPayMonthBalanceSheet)
            {
                payable = this.TransferInfo.AccountsPaid.GetValueOrDefault(0);
            }
            else
            {
                payable = (int)(this.GetAccountsPayable().ReceiptAmount);
            }

            int distributed = this.GetDistributedAmount();

            if (int.Equals(0, payable) || Math.Abs(payable - distributed) <= GetBillCount())   //每張單據可誤差 1 元 (稅率4捨5入的問題)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 取得宅配對帳單之異動調整金額和折讓金額
        /// </summary>        
        public VendorPaymentAmount GetVendorPaymentAmount()
        {
            return ap.VendorPaymentChangeGetList(_id)
                .GroupBy(x => x.BalanceSheetId)
                .Select(x => new VendorPaymentAmount
                            {
                                Amount = x.Sum(v => v.AllowanceAmount == 0 ? v.Amount : v.AllowanceAmount),
                                AllowanceAmount = x.Sum(v => v.AllowanceAmount)
                            })
                .FirstOrDefault();
        }

        public int GetVendorPaymentOverdueAmount(bool? positive)
        {
            if (positive == null)
                return ap.VendorPaymentOverdueGetListByBsid(_id).Select(x => x.Amount).Sum();
            else if (Convert.ToBoolean(positive))
                return ap.VendorPaymentOverdueGetListByBsid(_id).Where(x=>x.Amount >=0).Select(x => x.Amount).Sum();
            else if (!Convert.ToBoolean(positive))
                return ap.VendorPaymentOverdueGetListByBsid(_id).Where(x => x.Amount < 0).Select(x => x.Amount).Sum();
            else
                return 0;
        }

        /// <summary>
        /// 取得宅配對帳單之出貨份數及應付運費金額
        /// 若為寄倉檔次進貨運費計算規則確定 須調整(或新增)抓取運費規則
        /// </summary>  
        public DeliveryPamentInfo GetDeliveryPaymentInfo()
        {
            var details = ap.BalanceSheetDetailGetListByBalanceSheetId(_id).ToList();
            var cost = GetFirstCost(MerchandiseGuid);
            //由cash_trust_log 取出對應trust_id 以利區分運費 or 商品 計算出貨份數 及 運費金額
            var ctls = mp.CashTrustLogGetList(details.Select(x => x.TrustId));
            //商品的cash_trust_log
            var shippedTrustIds = ctls.Where(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight))
                                      .Select(x => x.TrustId).ToList();
            //運費的cash_trust_log
            var freightDetails = (from detail in details
                                    join ctl in ctls on detail.TrustId equals ctl.TrustId
                                    where Helper.IsFlagSet(ctl.SpecialStatus, TrustSpecialStatus.Freight)
                                    select new
                                    {
                                        detail.Status,
                                        ctl.Amount
                                    }).ToList();

            //應付運費
            var payFreight = (from freight in freightDetails
                            where freight.Status == (int)BalanceSheetDetailStatus.Normal ||
                                freight.Status == (int)BalanceSheetDetailStatus.Undo
                            select freight.Amount).Sum();
            //應付運費份數
            var payFreightCount = (from freight in freightDetails
                                   where freight.Status == (int)BalanceSheetDetailStatus.Normal ||
                                       freight.Status == (int)BalanceSheetDetailStatus.Undo
                                   select freight).Count();
            //退貨運費
            var deductFreight = (from freight in freightDetails
                                where freight.Status == (int)BalanceSheetDetailStatus.Deduction
                                select freight.Amount).Sum();

            //超商物流處理件數
            List<int> ispSevenOrderBeforeCount = new List<int>();//歷史成本資料
            var ispSevenOrderNowCount = 0;                       //目前成本資料
            List<int> ispFamilyOrderBeforeCount = new List<int>();
            var ispFamilyOrderNowCount = 0;

            //若有運費異動日期則分不同運費計算,反之則一種運費
            if (!string.IsNullOrEmpty(config.ISPFamilyFreightsChangeDate))
            {
                List<DateTime> ispFamilyFreightsChangeDateList = BalanceSheetService.GetIspFreightsChangeDateList(ProductDeliveryType.FamilyPickup);
                for (int i = 0; i < ispFamilyFreightsChangeDateList.Count; i++)
                {
                    if (i == ispFamilyFreightsChangeDateList.Count - 1)
                    {
                        //最新運費
                        ispFamilyOrderNowCount = ap.ViewBalanceSheetIspDetailListGetCountByChangeDate(_id, (int)ProductDeliveryType.FamilyPickup, ispFamilyFreightsChangeDateList[i], this.DbBs.IntervalEnd);
                    }
                    else
                    {
                        //舊運費
                        ispFamilyOrderBeforeCount.Add(ap.ViewBalanceSheetIspDetailListGetCountByChangeDate(_id, (int)ProductDeliveryType.FamilyPickup, ispFamilyFreightsChangeDateList[i], ispFamilyFreightsChangeDateList[i + 1]));
                    }
                }
            }
            else
            {
                ispFamilyOrderNowCount = ap.ViewBalanceSheetIspDetailListGetCountByChangeDate(_id, (int)ProductDeliveryType.FamilyPickup, null, null);
            }


            if (!string.IsNullOrEmpty(config.ISPSevenFreightsChangeDate))
            {
                List<DateTime> ispSevenFreightsChangeDateList = BalanceSheetService.GetIspFreightsChangeDateList(ProductDeliveryType.SevenPickup);
                for (int i = 0; i < ispSevenFreightsChangeDateList.Count; i++)
                {
                    if (i == ispSevenFreightsChangeDateList.Count - 1)
                    {
                        //最新運費
                        ispSevenOrderNowCount = ap.ViewBalanceSheetIspDetailListGetCountByChangeDate(_id, (int)ProductDeliveryType.SevenPickup, ispSevenFreightsChangeDateList[i], this.DbBs.IntervalEnd);
                    }
                    else
                    {
                        //舊運費
                        ispSevenOrderBeforeCount.Add(ap.ViewBalanceSheetIspDetailListGetCountByChangeDate(_id, (int)ProductDeliveryType.SevenPickup, ispSevenFreightsChangeDateList[i], ispSevenFreightsChangeDateList[i + 1]));
                    }
                }
            }
            else
            {
                ispSevenOrderNowCount = ap.ViewBalanceSheetIspDetailListGetCountByChangeDate(_id, (int)ProductDeliveryType.SevenPickup, null, null);
            }

            var wmsOrderAmount = wp.WmsOrderFeeSumByBalanceSheet(_id);

            //對帳單明細中未含運費份數
            if (shippedTrustIds.Any())
            { 
                details = details.Where(x => shippedTrustIds.Contains(x.TrustId)).ToList();
            }

            //應付份數
            var payCount = details.Count(detail => detail.Status == (int)BalanceSheetDetailStatus.Normal
                                                || detail.Status == (int)BalanceSheetDetailStatus.Undo);
            //退貨份數
            var deductCount = details.Count(detail => detail.Status == (int)BalanceSheetDetailStatus.Deduction);

            return new DeliveryPamentInfo
            {
                //ActuallyPayCount = payCount > deductCount ? payCount - deductCount : 0, //出貨(本期付款)份數 需扣除上期已付 但因退貨完成 而需扣款份數
                PayCount = payCount,
                DeductCount = deductCount,
                PayAmount = cost * payCount,
                DeductAmount = cost * deductCount,
                PayFreightAmount = payFreight,
                PayFreightCount = payFreightCount,
                DeductFreightAmount = deductFreight,
                ISPFamilyOrderBeforeCount = ispFamilyOrderBeforeCount,
                ISPFamilyOrderNowCount = ispFamilyOrderNowCount,
                ISPSevenOrderBeforeCount = ispSevenOrderBeforeCount,
                ISPSevenOrderNowCount = ispSevenOrderNowCount,
                WmsOrderAmount = wmsOrderAmount

            };
        }

        #endregion

        #region .ctors

        public BalanceSheetModel(int balanceSheetId)
        {
            _id = balanceSheetId;
        }

        #endregion

        #region private methods
        
        private TotalAmountDetail DirectlyGetAccountsPayable(BalanceSheet bs)
        {
            var bizModel = (BusinessModel)bs.ProductType;
            switch(bizModel)
            {
                case BusinessModel.Ppon:
                    return GetAmountByPpon(bs);
                case BusinessModel.PiinLife:
                    return GetAmountByPiinLife(bs);
                default:
                    throw new InvalidDataException(string.Format("invalid product_type in balance_sheet: id={0}", bs.Id.ToString()));
            }
        }
        
        private TotalAmountDetail GetAmountByPiinLife(BalanceSheet bs)
        {
            List<int> ids = new List<int>();
            ids.Add(bs.Id);
            var details = ap.ViewBalanceSheetDetailListGetList(ids);
            var toPay = details
                .Where(detail =>
                    detail.Status == (int)BalanceSheetDetailStatus.Normal
                    || detail.Status == (int)BalanceSheetDetailStatus.Undo)
                    .Sum(detail => detail.Cost.HasValue ? detail.Cost.Value : 0);
            var toDeduct = details
                .Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Deduction)
                .Sum(detail => detail.Cost.HasValue ? detail.Cost.Value : 0);
            return new TotalAmountDetail(toPay, toDeduct, toPay - toDeduct, toPay - toDeduct,0,0,0,0,0,0,0,0);
        }

        private TotalAmountDetail GetAmountByPpon(BalanceSheet bs)
        {
            BalanceSheetDetailCollection details = ap.BalanceSheetDetailGetListByBalanceSheetId(bs.Id);
            
            var toPay = details.Where(detail =>
                        detail.Status == (int)BalanceSheetDetailStatus.Normal
                        || detail.Status == (int)BalanceSheetDetailStatus.Undo).ToList();
            
            var toDeduct = details.Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Deduction).ToList();
            var payAmount = toPay.Count*GetFirstCost(bs.ProductGuid);
            var deductAmount = toDeduct.Count*GetFirstCost(bs.ProductGuid);
            //匯費
            //var remittanceFee = GetRemittanceFee();
            //deductAmount += remittanceFee;
            var totalAmount = payAmount - deductAmount;

            return new TotalAmountDetail(
                payAmount,
                deductAmount,
                totalAmount,
                totalAmount, 0, 0, 0, 0, 0, 0, 0, 0
                );
        }


        private TotalAmountDetail IndirectlyGetAccountsPayable(BalanceSheet bs)
        {
            var bizModel = (BusinessModel)bs.ProductType;
            
            BalanceSheetCollection sheets = ap.BalanceSheetGetWeekBalanceSheetsByMonth(bs.ProductGuid, bs.StoreGuid, bizModel, bs.Year.Value, bs.Month.Value);
            
            switch(bizModel)
            {
                case BusinessModel.Ppon:
                    return GetAmountByPpon(bs, sheets);
                case BusinessModel.PiinLife:
                    return GetAmountByPiinLife(sheets);
                default:
                    throw new InvalidDataException(string.Format("invalid product_type in balance_sheet: id={0}", bs.Id.ToString()));
            }
        }

        private TotalAmountDetail GetAmountByPpon(BalanceSheet mainBs, BalanceSheetCollection sheets)
        {
            if(sheets == null || sheets.Count == 0)
            {
                throw new InvalidOperationException();
            }

            List<int> ids = sheets.Select(sheet => sheet.Id).ToList();

            BalanceSheetDetailCollection details = ap.BalanceSheetDetailGetListByBalanceSheetIds(ids);
            var toPay = details
                .Where(detail =>
                        detail.Status == (int)BalanceSheetDetailStatus.Normal
                        || detail.Status == (int)BalanceSheetDetailStatus.Undo).ToList();
            var toDeduct = details.Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Deduction).ToList();

            return new TotalAmountDetail(
                toPay.Count*GetFirstCost(mainBs.ProductGuid),
                toDeduct.Count*GetFirstCost(mainBs.ProductGuid),
                (toPay.Count - toDeduct.Count) * GetFirstCost(mainBs.ProductGuid),
                (toPay.Count - toDeduct.Count) * GetFirstCost(mainBs.ProductGuid),0,0,0,0,0,0,0,0
                );
        }

        private TotalAmountDetail GetAmountByPiinLife(BalanceSheetCollection sheets)
        {
            if (sheets == null || sheets.Count == 0)
            {
                throw new InvalidOperationException();
            }
            
            List<int> ids = sheets.Select(sheet => sheet.Id).ToList();

            var details = ap.ViewBalanceSheetDetailListGetList(ids);
            var toPay = details
                .Where(detail =>
                        detail.Status == (int)BalanceSheetDetailStatus.Normal
                        || detail.Status == (int)BalanceSheetDetailStatus.Undo)
                .Sum(detail => detail.Cost.HasValue ? detail.Cost.Value : 0);
            var toDeduct = details
                .Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Deduction)
                .Sum(detail => detail.Cost.HasValue ? detail.Cost.Value : 0);
            return new TotalAmountDetail(toPay, toDeduct, toPay - toDeduct, toPay - toDeduct,0,0,0,0,0,0,0, 0);
        }

        /// <summary>
        /// 計算宅配各類金額
        /// </summary>
        /// <returns></returns>
        private TotalAmountDetail GetToHouseDealAmountByPpon()
        {
            var deliveryPaymentInfo = GetDeliveryPaymentInfo();

            //扣掉退貨的運費
            var freightAmounts = deliveryPaymentInfo.PayFreightAmount - deliveryPaymentInfo.DeductFreightAmount;
            //貨款金額
            var toPay = deliveryPaymentInfo.PayAmount;
            //退貨金額
            var toDeduct = deliveryPaymentInfo.DeductAmount;
            
            //異動金額
            var vendorPaymentAmount = GetVendorPaymentAmount();
            var amount = vendorPaymentAmount == null ? 0 : vendorPaymentAmount.Amount;
            var allowanceAmount = vendorPaymentAmount == null ? 0 : vendorPaymentAmount.AllowanceAmount;

            //逾期罰款金額(有正有負)
            int vendorPaymentOverdueAmount = GetVendorPaymentOverdueAmount(null);
            int vendorPositivePaymentOverdueAmount = GetVendorPaymentOverdueAmount(true);
            int vendorNegativePaymentOverdueAmount = GetVendorPaymentOverdueAmount(false);

            //商品對帳單發票金額 : 出貨份數*進貨價 + 消費者付款運費總計 + 廠商折讓金額(對帳單備註) + 逾期扣款
            var receiptAmount = toPay + freightAmounts + allowanceAmount;
            //商品對帳單應付金額 : 出貨份數*進貨價 + 消費者付款運費總計 + 廠商異動金額(對帳單備註) + 逾期扣款 
            toPay = toPay + freightAmounts + amount;

            //匯費
            //var remittanceFee = GetRemittanceFee();
            //toDeduct += remittanceFee;


            //物流處理費(都是負的)
            var ispFamilyOrderAmount = 0;
            var ispSevenOrderAmount = 0;
            
            if (deliveryPaymentInfo.ISPFamilyOrderBeforeCount.Any())
            {
                List<int> ispFamilyBeforeFreightsList = config.ISPFamilyBeforeFreights.Split(",").ToList().Select(freight => Int32.Parse(freight)).ToList();
                for (int i = 0; i < deliveryPaymentInfo.ISPFamilyOrderBeforeCount.Count; i++)
                {
                    ispFamilyOrderAmount += deliveryPaymentInfo.ISPFamilyOrderBeforeCount[i] * ispFamilyBeforeFreightsList[i];
                }
            }
            ispFamilyOrderAmount += (deliveryPaymentInfo.ISPFamilyOrderNowCount * config.ISPFamilyNowFreights);


            if (deliveryPaymentInfo.ISPSevenOrderBeforeCount.Any())
            {
                List<int> ispSevenBeforeFreightsList = config.ISPSevenBeforeFreights.Split(",").ToList().Select(freight => Int32.Parse(freight)).ToList();
                for (int i = 0; i < deliveryPaymentInfo.ISPSevenOrderBeforeCount.Count; i++)
                {
                    ispSevenOrderAmount += deliveryPaymentInfo.ISPSevenOrderBeforeCount[i] * ispSevenBeforeFreightsList[i];
                }
            }
            ispSevenOrderAmount += (deliveryPaymentInfo.ISPSevenOrderNowCount * config.ISPSevenNowFreights);

            //倉儲物流
            var wmsOrderAmount = deliveryPaymentInfo.WmsOrderAmount;


            //出貨份數*進貨價 , 扣除款項 , 出貨份數*進貨價-扣除款項 , 商品對帳單發票金額-扣除款項 , 運費 , 異動金額, 全家物流費, seven物流費, 逾期罰款
            return new TotalAmountDetail(toPay, toDeduct, toPay - toDeduct  - ispFamilyOrderAmount - ispSevenOrderAmount + vendorPaymentOverdueAmount - wmsOrderAmount,
                                         receiptAmount - toDeduct + vendorPositivePaymentOverdueAmount, freightAmounts, amount, ispFamilyOrderAmount,
                                         ispSevenOrderAmount, vendorPaymentOverdueAmount, vendorPositivePaymentOverdueAmount, vendorNegativePaymentOverdueAmount,
                                         wmsOrderAmount);
        }
        

        private decimal? _firstCost = null;
        private decimal GetFirstCost(Guid bid)
        {
            if (!_firstCost.HasValue)
            {
                DealCostCollection costs = pp.DealCostGetList(bid) ?? new DealCostCollection();
                DealProperty dp = pp.DealPropertyGet(bid);
                _firstCost = costs.Max(x => x.Cost) ?? 0;
                //成套票券類型需還原回單價
                _firstCost = _firstCost > 0 && dp.SaleMultipleBase > 0 ? _firstCost / dp.SaleMultipleBase : _firstCost;
            }

            return _firstCost.Value; 
        }
        
        private bool PreviousMbsConfirmedCheck()
        {
            BalanceSheetCollection allMbs = ap.BalanceSheetGetListByProductGuidStoreGuid(this.DbBs.ProductGuid,
                                                                                         this.DbBs.StoreGuid,
                                                                                         BalanceSheetGenerationFrequency
                                                                                             .MonthBalanceSheet);

            List<BalanceSheet> previousUnconfirmed = allMbs
                .Where(mbs => mbs.IntervalEnd < this.DbBs.IntervalEnd
                              && !mbs.IsConfirmedReadyToPay).ToList();

            return int.Equals(0, previousUnconfirmed.Count);
        }

        /// <summary>
        /// 取得對帳單之單據確認狀況
        /// </summary>        
        private BillingConfirmationState GetBillingConfirmationState()
        {
            var bsType = (BalanceSheetType)this.DbBs.BalanceSheetType;

            switch (bsType)
            {
                case BalanceSheetType.ManualMonthlyPayBalanceSheet:
                case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
                case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                    if (this.DbBs.IsConfirmedReadyToPay)
                    {
                        return BillingConfirmationState.AlreadyConfirmed;
                    }
                    if (!PreviousMbsConfirmedCheck())
                    {
                        return BillingConfirmationState.PreviousMonthUnconfirmed;
                    }
                    if (!DistributedAmountCheck())
                    {
                        return BillingConfirmationState.AmountInBillsTooDifferent;
                    }
                    return BillingConfirmationState.Confirmable;
                case BalanceSheetType.ManualTriggered:
                case BalanceSheetType.ManualLumpSumPayBalanceSheet:
                case BalanceSheetType.FlexibleToHouseBalanceSheet:
                case BalanceSheetType.MonthlyToHouseBalanceSheet:
                case BalanceSheetType.WeeklyToHouseBalanceSheet:
                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                case BalanceSheetType.MonthlyPayBalanceSheet:
                case BalanceSheetType.FlexiblePayBalanceSheet:
                case BalanceSheetType.WeeklyPayBalanceSheet:
                    if (this.DbBs.IsConfirmedReadyToPay)
                    {
                        return BillingConfirmationState.AlreadyConfirmed;
                    }
                    if (!DistributedAmountCheck())
                    {
                        return BillingConfirmationState.AmountInBillsTooDifferent;
                    }
                    return BillingConfirmationState.Confirmable;
                case BalanceSheetType.ManualWeeklyPayWeekBalanceSheet:
                case BalanceSheetType.WeeklyPayWeekBalanceSheet:
                    throw new InvalidOperationException();
                default:
                    throw new InvalidDataException();
            }
        }
        
        private BalanceSheetCollection GetWeeklyBs()
        {
            BalanceSheetCollection result = null;

            if (this.DbBs.BalanceSheetType == (int)BalanceSheetType.ManualWeeklyPayMonthBalanceSheet
                || this.DbBs.BalanceSheetType == (int)BalanceSheetType.WeeklyPayMonthBalanceSheet)
            {
                result = ap.BalanceSheetGetListByProductGuidStoreGuid(this.DbBs.ProductGuid, this.DbBs.StoreGuid,
                                                                                        BalanceSheetGenerationFrequency.WeekBalanceSheet);                
            }

            return result ?? new BalanceSheetCollection();
        }

        private BalanceSheetCollection GetWeeklyBsToConfirm(string user, DateTime confirmTime)
        {
            var result = new BalanceSheetCollection();            

            switch (Type)
            {
                case BalanceSheetType.ManualTriggered:
                case BalanceSheetType.MonthlyPayBalanceSheet:
                case BalanceSheetType.ManualMonthlyPayBalanceSheet:
                case BalanceSheetType.ManualLumpSumPayBalanceSheet:
                case BalanceSheetType.FlexibleToHouseBalanceSheet:
                case BalanceSheetType.MonthlyToHouseBalanceSheet:
                case BalanceSheetType.WeeklyToHouseBalanceSheet:
                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                case BalanceSheetType.FlexiblePayBalanceSheet:
                case BalanceSheetType.WeeklyPayBalanceSheet:
                    return result;
                case BalanceSheetType.WeeklyPayMonthBalanceSheet:
                case BalanceSheetType.ManualWeeklyPayMonthBalanceSheet:
                    break;
                default:
                    throw new InvalidOperationException();
            }
            
            List<BalanceSheet> bsToConfirm;

            IEnumerable<BalanceSheet> unconfirmedWeekBalanceSheets = GetWeeklyBs()
                    .Where(x => !x.IsConfirmedReadyToPay);
            
            DateTime? stopTime = BalanceSheetService.GetStopPaymentTime(this.MerchandiseGuid, this.StoreGuid);
            if (stopTime.HasValue)
            {
                bsToConfirm = unconfirmedWeekBalanceSheets
                    .Where(x => x.IntervalEnd <= stopTime).ToList();
            }
            else
            {
                bsToConfirm = unconfirmedWeekBalanceSheets.ToList();
            }

            foreach (BalanceSheet wbs in bsToConfirm)
            {
                wbs.IsConfirmedReadyToPay = true;
                wbs.ConfirmedTime = confirmTime;
                wbs.ConfirmedUserName = user;
                result.Add(wbs);
            }

            return result;
        }


        private void ConfirmCurrentBs(string user)
        {
            DateTime now = DateTime.Now;
            
            this.DbBs.IsDetailFixed = true;
            this.DbBs.IsConfirmedReadyToPay = true;
            this.DbBs.IsReceiptReceived = true;
            this.DbBs.ConfirmedTime = now;
            this.DbBs.ConfirmedUserName = user;

            using (TransactionScope txScope = new TransactionScope(
                TransactionScopeOption.RequiresNew, new TransactionOptions{ IsolationLevel = IsolationLevel.ReadCommitted}))
            {
                ap.BalanceSheetSet(this.DbBs);
                BalanceSheetCollection wbsToUpdate = GetWeeklyBsToConfirm(user, now);
                if (wbsToUpdate.Count > 0)
                {
                    ap.BalanceSheetCollectionSet(wbsToUpdate);
                }
                this._transferInfo = null;
                this._dbBs = null;
                txScope.Complete();                
            }
        }

        private void CancelCurrentBsConfirmedAndReceiptReceived()
        {
            //取消確認:只為了誤key單據且已確認對帳單 在進行刪除單據後 能夠讓營管重新新增單據
            //只須回復月對帳單的確認狀態 以利重新輸入 不須一併將週對帳單也進行取消確認
            //也不須另外檢查(影響) 該月對帳單之前月對帳單or之後月對帳單之確認狀態
            if (this.DbBs.IsConfirmedReadyToPay)
            { 
                this.DbBs.IsDetailFixed = false;
                this.DbBs.IsConfirmedReadyToPay = false;
                this.DbBs.ConfirmedTime = null;
                this.DbBs.ConfirmedUserName = null;
                this.DbBs.DefaultPaymentTime = null;
            }
            //取消單據已回:當進行刪除單據需檢查單據金額是否滿足對帳單應付金額
            if (DistributedAmountCheck())
            {
                if (this.DbBs.IspFamilyAmount != 0 || this.DbBs.IspFamilyAmount != 0 || this.DbBs.OverdueAmount < 0 || this.DbBs.WmsOrderAmount != 0)
                {
                    //發票金額為0但有其他數字時，刪除也需恢復到原狀
                    this.DbBs.IsReceiptReceived = false;
                }
                else
                {
                    this.DbBs.IsReceiptReceived = DistributedAmountCheck();
                }
            }
            else
            {
                this.DbBs.IsReceiptReceived = DistributedAmountCheck();
            }
            

            ap.BalanceSheetSet(this.DbBs);

            this._transferInfo = null;
            this._dbBs = null;
        }

        //private int GetRemittanceFee()
        //{
        //    //先收單據後付款檔次 才需檢查是否收取匯費
        //    if (this.DbBs.BalanceSheetType != (int) BalanceSheetType.FlexiblePayBalanceSheet &&
        //        this.DbBs.BalanceSheetType != (int) BalanceSheetType.FlexibleToHouseBalanceSheet &&
        //        this.DbBs.BalanceSheetType != (int) BalanceSheetType.MonthlyPayBalanceSheet &&
        //        this.DbBs.BalanceSheetType != (int) BalanceSheetType.MonthlyToHouseBalanceSheet &&
        //        this.DbBs.BalanceSheetType != (int) BalanceSheetType.WeeklyPayBalanceSheet &&
        //        this.DbBs.BalanceSheetType != (int) BalanceSheetType.WeeklyToHouseBalanceSheet)
        //    {
        //        return 0;
        //    }

        //    var exceedLimitTime = pp.VerificationStatisticsLogGetListByBalanceSheetId(this.Id).ExceedRemittanceTimeLimit;
        //    return exceedLimitTime*config.AchRemittanceFee;
        //}

        
        #endregion

        public void UpdateDefaultPaymentTime(string DfPaymentTime)
        {
            DateTime? date=null;
            if (DfPaymentTime != "")
            {
                date = Convert.ToDateTime(DfPaymentTime);
            }
            this.DbBs.DefaultPaymentTime = date;
            ap.BalanceSheetSet(this.DbBs);
        }

        public string GetDefaultPaymentDate()
        {
            if (this.DbBs.DefaultPaymentTime != null)
            {
                DateTime? date = null;
                date = Convert.ToDateTime(this.DbBs.DefaultPaymentTime); 
                return Convert.ToString(Convert.ToDateTime(date).ToShortDateString());
            }
            else
            {
                return "";
            }
            
        }
    }

    public struct TotalAmountDetail
    {
        private readonly decimal _totalAmount;
        public decimal TotalAmount
        {
            get { return _totalAmount; }
        }

        private readonly decimal _payAmount;
        public decimal PayAmount
        {
            get { return _payAmount; }
        }

        private readonly decimal _deductAmount;
        public decimal DeductAmount
        {
            get { return _deductAmount; }
        }

        private readonly decimal _receiptAmount;
        public decimal ReceiptAmount
        {
            get { return _receiptAmount; }
        }

        private readonly decimal _freightAmounts;
        public decimal FreightAmounts
        {
            get { return _freightAmounts; }
        }

        private readonly decimal _adjustmentAmount;
        public decimal AdjustmentAmount
        {
            get { return _adjustmentAmount; }
        }

        private readonly decimal _ispFamilyAmount;
        public decimal ISPFamilyAmount
        {
            get { return _ispFamilyAmount; }
        }

        private readonly decimal _ispSevenAmount;
        public decimal ISPSevenAmount
        {
            get { return _ispSevenAmount; }
        }

        private readonly decimal _overdueAmount;
        public decimal OverdueAmount
        {
            get { return _overdueAmount; }
        }

        private readonly decimal _vendorPositivePaymentOverdueAmount;
        public decimal VendorPositivePaymentOverdueAmount
        {
            get { return _vendorPositivePaymentOverdueAmount; }
        }

        private readonly decimal _vendorNegativePaymentOverdueAmount;
        public decimal VendorNegativePaymentOverdueAmount
        {
            get { return _vendorNegativePaymentOverdueAmount; }
        }

        private readonly decimal _wmsOrderAmount;
        public decimal WmsOrderAmount
        {
            get { return _wmsOrderAmount; }
        }

        public TotalAmountDetail(decimal payAmount, decimal deductAmount, decimal totalAmount, decimal receiptAmount, decimal freightAmounts, 
                                 decimal adjustmentAmount, decimal ispFamilyAmount, decimal ispSevenAmount,decimal overdueAmount,
                                 decimal vendorPositivePaymentOverdueAmount, decimal vendorNegativePaymentOverdueAmount, decimal wmsOrderAmount)
        {
            _payAmount = payAmount;
            _deductAmount = deductAmount;
            _totalAmount = totalAmount;
            _receiptAmount = receiptAmount;
            _freightAmounts = freightAmounts;
            _adjustmentAmount = adjustmentAmount;
            _ispFamilyAmount = ispFamilyAmount;
            _ispSevenAmount = ispSevenAmount;
            _overdueAmount = overdueAmount;
            _vendorPositivePaymentOverdueAmount = vendorPositivePaymentOverdueAmount;
            _vendorNegativePaymentOverdueAmount = vendorNegativePaymentOverdueAmount;
            _wmsOrderAmount = wmsOrderAmount;
        }
    }

    public enum BillingConfirmationState
    {
        UnknownFailure = 0,
        Confirmable = 1,
        PreviousMonthUnconfirmed = 2,        
        AmountInBillsTooDifferent = 3,
        AlreadyConfirmed = 4,
        Confirmed = 5
    }

    public class DistributedBill
    {
        public int BillId { get; private set; }
        public int Total { get; private set; }
        public int Subtotal { get; private set; }
        public int Tax { get; private set; }
        public string Remark { get; private set; }

        public DistributedBill(int billId, int total, int subtotal, int tax, string remark)
        {
            BillId = billId;
            Total = total;
            Subtotal = subtotal;
            Tax = tax;
            Remark = remark;
        }
    }

    public class VendorPaymentAmount
    {
        public int Amount { get; set; }
        public int AllowanceAmount { get; set; }

    }

    public class DeliveryPamentInfo
    {
        public int PayCount { get; set; }
        public int DeductCount { get; set; }
        public decimal PayAmount { get; set; }
        public decimal DeductAmount { get; set; }
        public int PayFreightAmount { get; set; }
        public int PayFreightCount { get; set; }
        public int DeductFreightAmount { get; set; }
        public int DebitFreightAmount { get; set; }
        public List<int> ISPFamilyOrderBeforeCount { get; set; }
        public int ISPFamilyOrderNowCount { get; set; }
        public List<int> ISPSevenOrderBeforeCount { get; set; }
        public int ISPSevenOrderNowCount { get; set; }
        public int WmsOrderAmount { get; set; }
    }
}

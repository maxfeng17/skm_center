﻿using Amazon.DataPipeline.Model;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.BalanceSheet
{
    public class FlexibleBalanceSheetModel
    {
        private static readonly IAccountingProvider _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static readonly IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static readonly IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        #region Property

        private readonly Guid _productGuid;

        protected Guid ProductGuid
        {
            get { return _productGuid; }
        }

        private readonly Guid? _storeGuid;

        protected Guid? StoreGuid
        {
            get { return _storeGuid; }
        }

        private readonly DateTime _genDate;

        protected DateTime GenDate
        {
            get { return _genDate; }
        }

        #endregion Property

        #region .ctors

        public FlexibleBalanceSheetModel(Guid productGuid, Guid? storeGuid, DateTime genDate)
        {
            _productGuid = productGuid;
            _storeGuid = storeGuid;
            _genDate = genDate;
        }

        #endregion .ctors

        #region public method

        public FlexibleBalanceSheetInfo CaculateCollectAmount()
        {
            var dealInfo = _pp.ViewPponDealGetByBusinessHourGuid(ProductGuid);
            var accountInfo = _pp.DealAccountingGet(ProductGuid);
            var estAmount = dealInfo.DeliveryType.HasValue
                            ? dealInfo.DeliveryType == (int)DeliveryType.ToHouse
                                ? GetToHouseBalanceSheetAmount()
                                : GetToShopBalanceSheetAmount()
                            :0;
            var balanceSheetInfo = new FlexibleBalanceSheetInfo
            {
                DealName = string.Format("[{0}]{1}", dealInfo.UniqueId, dealInfo.ItemName),
                RemittanceTypeDesc = Helper.GetLocalizedEnum((RemittanceType)accountInfo.RemittanceType),
                EstAmount = estAmount
            };

            return balanceSheetInfo;
        }

        #endregion public method

        #region private method


        /// <summary>
        /// 預算宅配對帳單金額
        /// </summary>
        /// <returns></returns>
        private int GetToHouseBalanceSheetAmount()
        {
            var estAmount = 0;

            //撈取 多檔次母檔對應之子檔 資訊 
            var subDeals = _pp.GetComboDealByBid(ProductGuid, false).Select(x => x.BusinessHourGuid).ToList();
            //檢查是否為多檔次
            if (!subDeals.Any())
            {
                subDeals = new List<Guid> { ProductGuid };
            }
            foreach (var dealGuid in subDeals)
            {
                //取得成功訂單
                var cashTrustLogs = GetAllCashTrustLogs(dealGuid);
                //抓取之trust_id 須將 退貨處理中(無論商家是否標註退貨完成) 退貨單 之 refund trust_id 視為已退貨(剔除出貨份數)
                cashTrustLogs = BalanceSheetManager.GetTrustListWithReturnningToReturned(cashTrustLogs, false);
                //已出貨並過10天鑑賞期之已出貨份數
                var shippedOrders = BalanceSheetManager.GetOverTrialPeriodOrderGuid(dealGuid, GenDate);
                //剔除標註凍結請款狀態之出貨份數
                var payLogs = cashTrustLogs.Where(log => shippedOrders.Contains(log.OrderGuid) &&
                                                       log.Status == (int)TrustStatus.Verified &&
                                                       !Helper.IsFlagSet(log.SpecialStatus, TrustSpecialStatus.Freeze));
                //已列在其他對帳單且是付款的之出貨份數
                var oldBsDetails = GetOldBalanceSheetDetails(dealGuid, null)
                                        .Where(x => x.Status == (int)BalanceSheetDetailStatus.Normal)
                                        .Select(x => x.TrustId);
                //進貨價
                var cost = GetFirstCost(dealGuid);
                //本期要付的訂單
                var payList = payLogs.Where(x => !oldBsDetails.Contains(x.TrustId)).ToList();
                //總額
                var payAmount = cost * payList.Count(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight));
                var freightAmount = payList.Where(x => Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight))
                                    .Sum(x => x.Amount);
                //異動金額
                var changeAmount = _ap.VendorPaymentChangeGetList(dealGuid)
                                .Where(x => !x.BalanceSheetId.HasValue)
                                .Sum(p => p.AllowanceAmount > 0 ? p.AllowanceAmount : p.Amount);
                //合計
                estAmount += (int)(payAmount + freightAmount + changeAmount);
            }

            return estAmount;
        }


        private int GetToShopBalanceSheetAmount()
        {
            var estAmount = 0;
            //已列在其他對帳單之核銷份數
            var vendorBillingHelper = new PponVendorBillingHelper(ProductGuid);
            var oldBsDetails = new List<BalanceSheetDetail>();
            foreach (var storeGuid in vendorBillingHelper.ParticipatingStores)
            {
                oldBsDetails.AddRange(GetOldBalanceSheetDetails(ProductGuid, storeGuid));
            }
            var oldBsTrustIds = oldBsDetails
                                .Where(x => x.Status == (int)BalanceSheetDetailStatus.Normal || 
                                            x.Status == (int)BalanceSheetDetailStatus.NoPay)
                                .Select(x => x.TrustId);
            var payList = GetAllCashTrustLogs(ProductGuid, GenDate)
                            .Where(log => !oldBsTrustIds.Contains(log.TrustId) && 
                                          log.SpecialStatus != (int)TrustSpecialStatus.Freight && 
                                          log.StoreGuid == StoreGuid && 
                                          log.ModifyTime < GenDate &&
                                          (log.Status == (int)TrustStatus.Verified || 
                                          (log.Status == (int)TrustStatus.Returned && Helper.IsFlagSet(log.SpecialStatus, TrustSpecialStatus.ReturnForced)) || 
                                          (log.Status == (int)TrustStatus.Refunded && Helper.IsFlagSet(log.SpecialStatus, TrustSpecialStatus.ReturnForced))));
            var deductList = GetDeductListForNewBalanceSheet(oldBsDetails).ToList();
            int slottingFeeQuantity = GetSlottingFeeQuantity(ProductGuid);
            var slottingFeeCount = GetPreviousBsSlottingFeeCount(oldBsDetails);
            var payCount = payList.Count() - (slottingFeeQuantity > slottingFeeCount ? slottingFeeQuantity - slottingFeeCount : 0);
            payCount = payCount < 0 ? 0 : payCount;
            if (payCount < deductList.Count())
            {
                //沒東西可扣錢就留到以後再扣
                deductList = deductList
                            .OrderBy(detail => detail.BalanceSheetId).ThenBy(detail => detail.UndoTime)
                            .Take(payCount)
                            .ToList();
            }

            estAmount = (int)((payCount - deductList.Count()) * GetFirstCost(ProductGuid));
            
            return estAmount;
        }

        private CashTrustLogCollection GetAllCashTrustLogs(Guid bid)
        {
            return _mp.CashTrustLogGetListByBidWhereOrderIsSuccessful(bid);
        }

        private CashTrustLogCollection GetAllCashTrustLogs(Guid bid,DateTime genDate)
        {
            return _mp.CashTrustLogGetListByBidWhereOrderIsSuccessful(bid, genDate);
        }

        private BalanceSheetDetailCollection GetOldBalanceSheetDetails(Guid bid, Guid? storeGuid)
        {
            return _ap.BalanceSheetDetailGetListByProductGuidAndStoreGuid(bid, storeGuid);
        }

        private IEnumerable<BalanceSheetDetail> GetDeductListForNewBalanceSheet(IEnumerable<BalanceSheetDetail> allDetails)
        {
            var results = new List<BalanceSheetDetail>();

            IEnumerable<BalanceSheetDetail> allUndos = allDetails.Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Undo).ToList();
            IEnumerable<BalanceSheetDetail> allDeducted = allDetails.Where(detail => detail.Status == (int)BalanceSheetDetailStatus.Deduction).ToList();

            foreach (var detail in allUndos)
            {
                BalanceSheetDetail currentDetail = detail;
                int undoCount = allUndos.Where(x => currentDetail.TrustId == x.TrustId).Count();
                int deductCount = allDeducted.Where(x => currentDetail.TrustId == x.TrustId).Count();
                if (undoCount != deductCount)
                {
                    results.Add(currentDetail);
                }
            }

            return results;
        }

        private decimal GetFirstCost(Guid bid)
        {
            var costs = _pp.DealCostGetList(bid) ?? new DealCostCollection();
            DealProperty dp = _pp.DealPropertyGet(bid);
            decimal cost = costs.Max(x => x.Cost) ?? decimal.MaxValue;
            return cost > 0 && dp.SaleMultipleBase > 0 ? cost / dp.SaleMultipleBase.Value : cost;
        }

        private int GetSlottingFeeQuantity(Guid bid)
        {
            var dcc = _pp.DealCostGetList(bid);
            return dcc.Count > 1 ? dcc.Min(x => x.CumulativeQuantity) ?? 0 : 0;
        }

        private int GetPreviousBsSlottingFeeCount(IEnumerable<BalanceSheetDetail> allDetails)
        {
            return allDetails.Count(x => x.Status == (int)BalanceSheetDetailStatus.NoPay);
        }

        #endregion private method

    }
    public class FlexibleBalanceSheetInfo
    {
        public string DealName { get; set; }
        public string RemittanceTypeDesc { get; set; }
        public int EstAmount { get; set; }
    }
}

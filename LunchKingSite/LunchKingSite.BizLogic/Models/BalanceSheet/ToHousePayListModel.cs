﻿using System.Data;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.BizLogic.Model.BalanceSheet
{
    public class ToHousePayListModel
    {
        private static IAccountingProvider _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();

        #region prop

        public Guid DealId { get; set; }
        public string DealName { get; set; }
        public int DealUniqueId { get; set; }
        public string ShipPeriodStart { get; set; }
        public string ShipPeriodEnd { get; set; }
        public string BalanceSheetDesc { get; set; }
        public DateTime IntervalEnd { get; set; }
        public int EstAmount { get; set; }
        public int DeductAmount { get; set; }
        public int PaymentChangeAmount { get; set; }
        public int PaidAmount { get; set; }
        public bool IsTransferComplete { get; set; }
        public int PositivePaymentOverdueAmount { get; set; }
        public int NegativePaymentOverdueAmount { get; set; }
        public int ReceiptAmount { get; set; }
        

        public IEnumerable<DealPayListInfo> DealInfos { get; set; }

        #endregion prop


        public void Initial(Guid productGuid, int bsId)
        {
            try
            {
                var mainDeal = _pp.ViewPponDealGetByBusinessHourGuid(productGuid);

                if (mainDeal.DeliveryType != (int) DeliveryType.ToHouse)
                {
                    return;
                }

                DealId = productGuid;
                DealUniqueId = mainDeal.UniqueId ?? 0;
                DealName = mainDeal.ItemName;
                ShipPeriodStart = string.Format("{0:yyyy/MM/dd}", mainDeal.BusinessHourDeliverTimeS);
                ShipPeriodEnd = string.Format("{0:yyyy/MM/dd}", mainDeal.BusinessHourDeliverTimeE);

                //撈取 多檔次母檔對應之子檔 資訊 
                var subDeals = _pp.GetComboDealByBid(productGuid, false).Select(x => x.BusinessHourGuid).ToList();
                                
                //檢查是否為多檔次
                if (!subDeals.Any())
                {
                    subDeals = new List<Guid> { productGuid };
                }
                //所有對帳單明細 依週/月結/彈性付款 不同出帳方式顯示
                var bsInfos = _ap.ViewBalanceSheetListGetList(subDeals);
                if (!bsInfos.Any())
                {
                    return;
                }
                
                //單筆對帳單檔次付款明細
                var currentBs = bsInfos.FirstOrDefault(x => x.Id == bsId);
                if (currentBs == null)
                {
                    return;
                }

                var balanceSheetDesc = string.Empty;
                var intervalEnd = currentBs.IntervalEnd;
                switch ((BalanceSheetType)currentBs.BalanceSheetType)
                {
                    case BalanceSheetType.FlexibleToHouseBalanceSheet:
                        balanceSheetDesc = string.Format("【{0:yyyy/MM/dd}】彈性對帳單", currentBs.IntervalEnd);
                        break;
                    case BalanceSheetType.MonthlyToHouseBalanceSheet:
                        balanceSheetDesc = string.Format("【{0}/{1}】月對帳單", currentBs.Year, currentBs.Month);
                        break;
                    case BalanceSheetType.WeeklyToHouseBalanceSheet:
                        balanceSheetDesc = string.Format("【{0:yyyy/MM/dd}】週對帳單", currentBs.IntervalEnd.AddDays(-1));
                        intervalEnd = intervalEnd.AddDays(-1);
                        break;
                    case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                        balanceSheetDesc = string.Format("【{0:yyyy/MM/dd}】雙週結對帳單", currentBs.IntervalEnd.AddDays(-1));
                        intervalEnd = intervalEnd.AddDays(-1);
                        break;
                }
                //核銷清單出帳說明
                BalanceSheetDesc = balanceSheetDesc;
                IntervalEnd = intervalEnd;

                var currentBsInfo = bsInfos.Where(x => x.IntervalStart == currentBs.IntervalStart
                                                    && x.IntervalEnd == currentBs.IntervalEnd);
                var dealInfos = new List<DealPayListInfo>();

                if (_config.IsEnableVbsNewShipFile)
                {
                    #region NewVersion
                    //取得品項規格列表
                    var dealIds = currentBsInfo.Select(x => x.UniqueId);
                    var orderProductOptionLists = _op.ViewOrderProductOptionListGetIsCurrentList(dealIds);
                    var orderItemLists = _op.GetOrderProductOptionListNotGroup(orderProductOptionLists)
                                                .Select(x => new
                                                {
                                                    OrderGuid = x.OrderGuid,
                                                    OptionDescription = x.OptionDescription,
                                                    ItemNo = x.ItemNo.Trim(',')
                                                });

                    foreach (var bs in currentBsInfo)
                    {
                        var bsModel = new BalanceSheetModel(bs.Id);
                        var deliveryPaymentInfo = bsModel.GetDeliveryPaymentInfo();
                        var vendorPaymentAmount = bsModel.GetVendorPaymentAmount();
                        var transferInfo = bsModel.TransferInfo;
                        var detailAllInfos = _ap.OrderGetListByBalanceSheetId(bs.Id).AsEnumerable();
                        var detailInfos = detailAllInfos.Where(x => !Helper.IsFlagSet(x.Field<int>("special_status"), (int)TrustSpecialStatus.Freight));
                        var detailFreightInfos = detailAllInfos.Where(x => Helper.IsFlagSet(x.Field<int>("special_status"), (int)TrustSpecialStatus.Freight));
                        var da = _pp.DealAccountingGet(productGuid);

                        bool isTax = da.IsInputTaxRequired;
                        var taxRate = isTax ? 1 + _config.BusinessTax : 1;
                        var itemUnTexCost = bs.Cost == null ? 0m : Math.Round((decimal)bs.Cost / taxRate, 0);
                        var freightAmount = detailFreightInfos.Any() ? detailFreightInfos.Select(x =>
                        x.Field<int>("credit_card") + x.Field<int>("pcash") + x.Field<int>("scash") + x.Field<int>("atm") + x.Field<int>("bcash")).FirstOrDefault() : 0;
                        
                        var payDetail =
                            detailInfos.Where(x => x.Field<int>("detail_status") == (int)BalanceSheetDetailStatus.Normal ||
                                                   x.Field<int>("detail_status") == (int)BalanceSheetDetailStatus.Undo)
                                       .Select(x => new {
                                                           OrderGuid = x.Field<Guid>("order_guid"),
                                                           OrderId = x.Field<string>("order_id"),
                                                           ShipTime = x.Field<DateTime>("ship_time"),
                                                           OrderCreateTime = x.Field<DateTime>("order_create_time")
                                                        })
                                       .Distinct()
                                       .GroupJoin(orderItemLists,
                                                   di => di.OrderGuid,
                                                   oi => oi.OrderGuid,
                                                   (x, y) => new { DeatailInfo = x, OrderItemLists = y })
                                       .SelectMany(x => x.OrderItemLists.DefaultIfEmpty(),
                                                   (x, y) => new { DeatailInfo = x.DeatailInfo, OrderItemLists = y })                                       
                                       .Select(x => new PayListInfo
                                       {
                                           OrderId = x.DeatailInfo.OrderId,
                                           OrderCreateTime = x.DeatailInfo.OrderCreateTime,
                                           ShipTime = x.DeatailInfo.ShipTime,
                                           ItemOption = x.OrderItemLists.OptionDescription ?? string.Empty,
                                           ItemNo = x.OrderItemLists.ItemNo ?? string.Empty,
                                           Count = detailInfos.Where(y => y.Field<string>("order_id") == x.DeatailInfo.OrderId).Count()
                                       }).ToList();
                        
                        var deductDetail =
                            detailInfos.Where(x => x.Field<int>("detail_status") == (int)BalanceSheetDetailStatus.Deduction)
                                        .GroupBy(x => x.Field<string>("order_id"))
                                        .Select(x => new DeductListInfo
                                        {
                                            OrderId = x.Key,
                                            Count = x.Count()
                                        });

                        var changeInfo = _ap.VendorPaymentChangeGetList(bs.Id)
                                            .Select(x => new PaymentChangeInfo
                                            {
                                                Reason = x.Reason,
                                                Amount = x.AllowanceAmount > 0 ? x.AllowanceAmount : x.Amount,
                                                CreateTime = x.CreateTime
                                            });
                        var positiveOverdueInfo = _ap.ViewVendorPaymentOverdueGetListByBsid(bs.Id).Where(x => x.Amount > 0)
                                            .Select(x => new PaymentOverdueInfo
                                            {
                                                Orderid = x.OrderId,
                                                ShipType = x.ShipType.ToString(),///
                                                LashShipDate = x.LastShipDate,
                                                CreateTime = x.CreateTime,
                                                ItemPrice = Convert.ToInt32(x.ItemPrice),
                                                Reason = x.Reason,
                                                Amount = x.Amount
                                            });

                        var negativeOverdueInfo = _ap.ViewVendorPaymentOverdueGetListByBsid(bs.Id).Where(x => x.Amount < 0)
                                            .Select(x => new PaymentOverdueInfo
                                            {
                                                Orderid = x.OrderId,
                                                ShipType = x.ShipType.ToString(),///
                                                LashShipDate = x.LastShipDate,
                                                CreateTime = x.CreateTime,
                                                ItemPrice = Convert.ToInt32(x.ItemPrice),
                                                Reason = x.Reason,
                                                Amount = x.Amount
                                            });
                        

                       

                        List<DateTime> ispFamilyFreightsChangeLastDate = BalanceSheetService.GetIspFreightsChangeDateList(ProductDeliveryType.FamilyPickup);
                        List<DateTime> ispSevenFreightsChangeLastDate = BalanceSheetService.GetIspFreightsChangeDateList(ProductDeliveryType.SevenPickup);

                        //若沒有異動,則讓before.count()=0,反之則抓最新異動之前的
                        var ispFamilyBeforeInfo = _ap.ViewBalanceSheetIspDetailListGetByChangeDate(bs.Id, (int)ProductDeliveryType.FamilyPickup, ispFamilyFreightsChangeLastDate.First(), ispFamilyFreightsChangeLastDate.Last())
                                            .Select(x => new IspInfo
                                            {
                                                OrderId = x.OrderId,
                                                ProductDeliveryType = x.ProductDeliveryType,
                                                ShipToStockTime = x.ShipToStockTime ?? DateTime.MaxValue
                                            });
                        var ispFamilyNowInfo = _ap.ViewBalanceSheetIspDetailListGetByChangeDate(bs.Id, (int)ProductDeliveryType.FamilyPickup, ispFamilyFreightsChangeLastDate.Last(), bs.IntervalEnd)
                                            .Select(x => new IspInfo
                                            {
                                                OrderId = x.OrderId,
                                                ProductDeliveryType = x.ProductDeliveryType,
                                                ShipToStockTime = x.ShipToStockTime ?? DateTime.MaxValue
                                            });
                        var ispSevenBeforeInfo = _ap.ViewBalanceSheetIspDetailListGetByChangeDate(bs.Id, (int)ProductDeliveryType.SevenPickup, ispSevenFreightsChangeLastDate.First(), ispSevenFreightsChangeLastDate.Last())
                                            .Select(x => new IspInfo
                                            {
                                                OrderId = x.OrderId,
                                                ProductDeliveryType = x.ProductDeliveryType,
                                                ShipToStockTime = x.ShipToStockTime ?? DateTime.MaxValue
                                            });
                        var ispSevenNowInfo = _ap.ViewBalanceSheetIspDetailListGetByChangeDate(bs.Id, (int)ProductDeliveryType.SevenPickup, ispSevenFreightsChangeLastDate.Last(), bs.IntervalEnd)
                                            .Select(x => new IspInfo
                                            {
                                                OrderId = x.OrderId,
                                                ProductDeliveryType = x.ProductDeliveryType,
                                                ShipToStockTime = x.ShipToStockTime ?? DateTime.MaxValue
                                            });


                        var wmsOrderTable = _wp.WmsOrderFeeDetailByBalanceSheet(bs.Id);
                        bool isContainRefund = wmsOrderTable.Select("type='refund'").Any();
                        DataTable wmsOrderInfo = wmsOrderTable.AsEnumerable().GroupBy(x => new { UniqueId = x.Field<int>("unique_id"), OrderId = x.Field<string>("order_id"), ItemName = x.Field<string>("item_name"), ShipTime = x.Field<DateTime?>("ship_time") }).Select(y => new {
                            UniqueId = y.Key.UniqueId,
                            OrderId = (isContainRefund ? "*" : "") + y.Key.OrderId,
                            ItemName = y.Key.ItemName,
                            ShipTime = y.Key.ShipTime != null ? y.Key.ShipTime.Value.ToString("yyyy/MM/dd") : "",
                            PackFee = y.Where(a=>a.Field<string>("type")=="pack").Sum(b=>b.Field<int>("fee")),
                            ShipFee = y.Where(a => a.Field<string>("type") == "ship").Sum(b => b.Field<int>("fee")),
                            RefundShipFee = y.Where(a => a.Field<string>("type") == "refund").Sum(b => b.Field<int>("fee")),
                            OrderServFee = y.Where(a => a.Field<string>("type") == "order").Sum(b => b.Field<int>("fee")),
                            TotalFee = y.Sum(a => a.Field<int>("fee"))
                        }).ToDataTable();
                                                    

                        var deal = new DealPayListInfo
                        {
                            DealName = bs.Name,
                            DealUniqueId = bs.UniqueId,
                            EstAmount = bs.EstAmount ?? 0,
                            DeductAmount = deliveryPaymentInfo != null ? (int)deliveryPaymentInfo.DeductAmount + deliveryPaymentInfo.DeductFreightAmount : 0,
                            PaymentChangeAmount = vendorPaymentAmount != null ? vendorPaymentAmount.Amount : 0,
                            AllowanceAmount = vendorPaymentAmount != null ? vendorPaymentAmount.AllowanceAmount : 0,
                            PaidAmount = transferInfo != null ? transferInfo.AccountsPaid : 0,
                            ItemCost = bs.Cost ?? 0m,
                            ItemCostTex = (bs.Cost ?? 0m) - itemUnTexCost,
                            ItemUnTexCost = itemUnTexCost,
                            FreightAmount = freightAmount,
                            PayInfos = payDetail,
                            DeductInfos = deductDetail,
                            ChangeInfos = changeInfo,
                            PositiveOverdueInfos = positiveOverdueInfo,
                            NegativeOverdueInfos = negativeOverdueInfo,
                            ISPFamilyBeforeInfo = ispFamilyBeforeInfo,
                            ISPFamilyNowInfo = ispFamilyNowInfo,
                            ISPSevenBeforeInfo = ispSevenBeforeInfo,
                            ISPSevenNowInfo = ispSevenNowInfo,
                            ISPFamilyOrderAmount = bs.IspFamilyAmount,//全家物流費用份數
                            ISPSevenOrderAmount = bs.IspSevenAmount,//7-11物流費用份數
                            WmsOrderInfo = wmsOrderInfo //PCHOME倉儲費用
                        };
                        dealInfos.Add(deal);
                    }
                    #endregion
                }
                else
                {
                    #region OldVersion
                    foreach (var bs in currentBsInfo)
                    {
                        var bsModel = new BalanceSheetModel(bs.Id);
                        var deliveryPaymentInfo = bsModel.GetDeliveryPaymentInfo();
                        var vendorPaymentAmount = bsModel.GetVendorPaymentAmount();
                        var transferInfo = bsModel.TransferInfo;
                        var detailInfos = _ap.OrderGetListByBalanceSheetId(bs.Id).AsEnumerable()
                                            .Where(x => !Helper.IsFlagSet(x.Field<int>("special_status"), (int)TrustSpecialStatus.Freight));
                        var payDetail =
                            detailInfos.Where(x => x.Field<int>("detail_status") == (int)BalanceSheetDetailStatus.Normal ||
                                                   x.Field<int>("detail_status") == (int)BalanceSheetDetailStatus.Undo)
                                        .GroupBy(x => x.Field<string>("order_id"))
                                        .Select(x => new PayListInfo
                                        {
                                            OrderId = x.Key,
                                            Count = x.Count()
                                        });
                        var deductDetail =
                            detailInfos.Where(x => x.Field<int>("detail_status") == (int)BalanceSheetDetailStatus.Deduction)
                                        .GroupBy(x => x.Field<string>("order_id"))
                                        .Select(x => new DeductListInfo
                                        {
                                            OrderId = x.Key,
                                            Count = x.Count()
                                        });
                        var changeInfo = _ap.VendorPaymentChangeGetList(bs.Id)
                                            .Select(x => new PaymentChangeInfo
                                            {
                                                Reason = x.Reason,
                                                Amount = x.AllowanceAmount > 0 ? x.AllowanceAmount : x.Amount,
                                                CreateTime = x.CreateTime
                                            });
                        var deal = new DealPayListInfo
                        {
                            DealName = bs.Name,
                            DealUniqueId = bs.UniqueId,
                            EstAmount = bs.EstAmount ?? 0,
                            DeductAmount = deliveryPaymentInfo != null ? (int)deliveryPaymentInfo.DeductAmount + deliveryPaymentInfo.DeductFreightAmount : 0,
                            PaymentChangeAmount = vendorPaymentAmount != null ? vendorPaymentAmount.Amount : 0,
                            PaidAmount = transferInfo != null ? transferInfo.AccountsPaid : 0,
                            PayInfos = payDetail,
                            DeductInfos = deductDetail,
                            ChangeInfos = changeInfo
                        };
                        dealInfos.Add(deal);
                    }
                    #endregion
                }
                DealInfos = dealInfos.OrderBy(x => x.DealUniqueId);

                EstAmount = DealInfos.Sum(x => x.EstAmount);
                DeductAmount = DealInfos.Sum(x => x.DeductAmount);
                PaymentChangeAmount = DealInfos.Sum(x => x.PaymentChangeAmount);
                PositivePaymentOverdueAmount = DealInfos.Sum(x => x.PositiveOverdueInfos.Sum(y => y.Amount));
                NegativePaymentOverdueAmount = DealInfos.Sum(x => x.NegativeOverdueInfos.Sum(y => y.Amount));
                PaidAmount = DealInfos.Sum(x => x.PaidAmount ?? 0);
                IsTransferComplete = DealInfos.All(x => x.PaidAmount.HasValue);
                ReceiptAmount = EstAmount - PaymentChangeAmount + DealInfos.Sum(x => x.AllowanceAmount) + DealInfos.Sum(x => x.ISPFamilyOrderAmount) + DealInfos.Sum(x => x.ISPSevenOrderAmount) + Math.Abs(NegativePaymentOverdueAmount);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ex.StackTrace);
            }
        }
    }

    public class DealPayListInfo
    {
        public int DealUniqueId { get; set; }
        public string DealName { get; set; }
        public int EstAmount { get; set; }
        public int DeductAmount { get; set; }
        public int PaymentChangeAmount { get; set; }
        public int AllowanceAmount { get; set; }
        public int? PaidAmount { get; set; }
        public decimal ItemCost { get; set; }
        public decimal ItemCostTex { get; set; }
        public decimal ItemUnTexCost { get; set; }
        public int FreightAmount { get; set; }
        public IEnumerable<PayListInfo> PayInfos { get; set; }
        public IEnumerable<DeductListInfo> DeductInfos { get; set; }
        public IEnumerable<PaymentChangeInfo> ChangeInfos { get; set; }
        public IEnumerable<PaymentOverdueInfo> PositiveOverdueInfos { get; set; }
        public IEnumerable<PaymentOverdueInfo> NegativeOverdueInfos { get; set; }
        public int ISPFamilyOrderAmount { get; set; }
        public int ISPSevenOrderAmount { get; set; }
        public IEnumerable<IspInfo> ISPFamilyBeforeInfo { get; set; }
        public IEnumerable<IspInfo> ISPFamilyNowInfo { get; set; }
        public IEnumerable<IspInfo> ISPSevenBeforeInfo { get; set; }
        public IEnumerable<IspInfo> ISPSevenNowInfo { get; set; }
        public DataTable WmsOrderInfo { get; set; }
    }

    public class DeductListInfo
    {
        public string OrderId { get; set; }
        public int Count { get; set; }
    }

    public class PaymentChangeInfo
    {
        public string Reason { get; set; }
        public int Amount { get; set; }
        public DateTime CreateTime { get; set; }
    }

    public class PaymentOverdueInfo
    {
        public string Orderid { get; set; }
        public string ShipType { get; set; }
        public DateTime? LashShipDate { get; set; }
        public DateTime? CreateTime { get; set; }
        public int ItemPrice { get; set; }
        public int Amount { get; set; }
        public string Reason { get; set; }
    }

    public class IspInfo
    {
        public string OrderId { get; set; }
        public int ProductDeliveryType { get; set; }
        public DateTime ShipToStockTime { get; set; }
    }

    public class PayListInfo
    {
        public DateTime OrderCreateTime { get; set; }
        public string OrderId { get; set; }
        public DateTime ShipTime { get; set; }
        public string ItemOption { get; set; }
        public string ItemNo { get; set; }
        public int Count { get; set; }
    }
}

﻿using Amazon.ElasticLoadBalancing.Model;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LunchKingSite.BizLogic.Model.BalanceSheet
{
    public class BalanceSheetsDeliverModel
    {
        #region interface

        private IPponProvider _pp;
        private ISellerProvider _sp;
        private IAccountingProvider _ap;
        private static ISysConfProvider _config;
        private static IOrderProvider _op;
        private static IVerificationProvider _vp;
        //private static IMemberProvider _mp;

        #endregion

        #region prop

        public Guid DealId { get; set; }
        public string DealName { get; set; }
        public int DealUniqueId { get; set; }
        public string DeliverStartDate { get; set; }
        public string DeliverEndDate { get; set; }
        //應付總金額
        public int DealTotalAmount { get; set; }
        //單據上顯示之總計 轉國字顯示
        public string DealChineseTotalAmount { get; set; }
        //總計尾款
        public int DealTotalFinalAmount { get; set; }
        //總計含運費
        public int DealTotalAmountWithFare { get; set; }
        //總計暫付七成
        public int DealPartialAmount { get; set; }
        //總計運費
        public int DealFareAmount { get; set; }
        //單據上顯示之運費  轉國字顯示
        public string DealChineseFareAmount { get; set; }
        public bool IsTax { get; set; }
        public int DealRemittanceType { get; set; }
        public DateTime? FinalBalanceSheetDate { get; set; }
        //DebitCaseAmount       對帳單扣除金額 allowance_amount=0 就用amount
        //DebitAllowanceAmount  發票扣除金額  只看allowance_amount
        public int DebitCaseAmount { get; set; }
        public int DebitAllowanceAmount { get; set; }
        //顯示明細或是對帳單（發票）
        public bool mIsBalanceCreaDate { get; set; }
        public int mVendorReceiptType { get; set; }
        //營業稅
        public int mDealTaxAmount { get; set; }
        //運費的稅
        public int mDealFareTaxAmount { get; set; }

        public IEnumerable<BalanceSheetsDeliverInfo> DealInfos { get; set; }

        public TransferInfo TransferInfo { get; set; }

        #endregion


        public BalanceSheetsDeliverModel()
        {
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            _config = ProviderFactory.Instance().GetConfig();
        }

        public void ModelInit(Guid productGuid)
        {
            try
            {

                var convertToMainid = _pp.GetComboDealByBid(new List<Guid> {productGuid}).FirstOrDefault();
                Guid mid = convertToMainid == null ? productGuid : convertToMainid.MainBusinessHourGuid ?? new Guid();

                ViewPponDeal mainProduct = _pp.ViewPponDealGetByBusinessHourGuid(mid);

                DealId = mainProduct.BusinessHourGuid;
                DealUniqueId = mainProduct.UniqueId ?? 0;
                DealName = mainProduct.ItemName;
                DeliverStartDate = string.Format("{0:yyyy/MM/dd}", mainProduct.BusinessHourDeliverTimeS);
                DeliverEndDate = string.Format("{0:yyyy/MM/dd}", mainProduct.BusinessHourDeliverTimeE);

                //撈取 多檔次母檔對應之子檔 資訊 
                var subDeals = _pp.GetComboDealByBid(productGuid, false).Select(x => x.BusinessHourGuid).ToList();

                //檢查是否為多檔次
                if (!subDeals.Any())
                {
                    subDeals = new List<Guid> {productGuid};
                }
                var mainDealAccountInfo = _pp.DealAccountingGet(productGuid);
                bool IsMainDealBsCreate = mainDealAccountInfo.BalanceSheetCreateDate.HasValue;
                var bsInfo = _ap.ViewBalanceSheetListGetList(subDeals)
                                .GroupBy(x => x.ProductGuid)
                                .ToDictionary(x => x.Key,
                                              x => x.Count(b => b.GenerationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet));

                DealInfos = SetDealInfos(subDeals);

                //DebitCaseAmount       對帳單扣除金額 allowance_amount=0 就用amount
                //DebitAllowanceAmount  發票扣除金額  只看allowance_amount
                DebitCaseAmount = DealInfos.Sum(x => x.DebitCaseAmount);
                DebitAllowanceAmount = DealInfos.Sum(x => x.DebitAllowanceAmount);
                DealTotalAmount = DealInfos.Sum(x => x.DealTotalAmount);
                DealPartialAmount = DealInfos.Sum(x => x.PartiallyPaymentAmount);
                DealFareAmount = DealInfos.Sum(x => x.FareAmount);
                DealChineseFareAmount = CommonFacade.GetFullChineseAmountString(DealFareAmount);
                DealTotalFinalAmount = DealInfos.Sum(x => x.DealTotalFinalAmount);
                IsTax = DealInfos.Select(x => x.IsTax).First();
                DealRemittanceType = DealInfos.Select(x => x.DealRemittanceType).First();
                mVendorReceiptType = mainDealAccountInfo.VendorReceiptType;
                //全部子檔都要壓對帳單開立日才可以看
                mIsBalanceCreaDate = IsMainDealBsCreate &&
                                     DealInfos.All(x => x.BalanceCreatDate.HasValue &&
                                                        bsInfo.ContainsKey(x.DealGuid) &&
                                                        bsInfo[x.DealGuid] > 0);
                //稅

                //應稅 或 單據開立方式為其他或收據 則運費一起算
                //免稅將運費分開
                decimal taxRate = 1 + _config.BusinessTax;
                DealTotalAmountWithFare = IsTax || mVendorReceiptType == (int) VendorReceiptType.Other ||
                                          mVendorReceiptType == (int) VendorReceiptType.Receipt
                    ? (DealTotalAmount + DealFareAmount + DebitAllowanceAmount)
                    : (DealTotalAmount + DebitAllowanceAmount);
                if (IsTax)
                {
                    mDealTaxAmount = Convert.ToInt32(Math.Round(DealTotalAmountWithFare/taxRate, 0));
                }
                else
                {
                    mDealTaxAmount = 0;
                    mDealFareTaxAmount = Convert.ToInt32(Math.Round(DealFareAmount/taxRate, 0));
                }
                DealChineseTotalAmount = CommonFacade.GetFullChineseAmountString(DealTotalAmountWithFare);

                //匯款資訊以檔號排序取weekly pay account
                var accountInfo = _op.GetWeeklyPayAccount(productGuid, mainProduct.SellerGuid);
                if (accountInfo != null && accountInfo.IsLoaded)
                {
                    TransferInfo = setFundTransferInfo(accountInfo);
                }
                    //都取不到則取賣家資訊
                else
                {
                    TransferInfo = setFundTransferInfo(_sp.SellerGetByBid(productGuid));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ex.StackTrace);
            }
        }

        private TransferInfo setFundTransferInfo(Seller s)
        {
            var transferInfo = new TransferInfo
            {
                ReceiverTitle = s.CompanyAccountName,
                ReceiverCompanyId = s.CompanyID,
                ReceiverAccountBankNo = s.CompanyBankCode,
                ReceiverAccountBranchNo = s.CompanyBranchCode,
                ReceiverAccountNo = s.CompanyAccount,
                ReceiverBossName = s.CompanyBossName
            };

            return transferInfo;
        }

        private TransferInfo setFundTransferInfo(WeeklyPayAccount s)
        {
            var transferInfo = new TransferInfo
            {
                ReceiverTitle = s.AccountName,
                ReceiverCompanyId = s.AccountId,
                ReceiverAccountBankNo = s.BankNo,
                ReceiverAccountBranchNo = s.BranchNo,
                ReceiverAccountNo = s.AccountNo,
                ReceiverBossName = s.SellerName
            };

            return transferInfo;
        }


        private IEnumerable<BalanceSheetsDeliverInfo> SetDealInfos(IEnumerable<Guid> guids)
        {
            var infos = new List<BalanceSheetsDeliverInfo>();

            var deliverSubInfo = from x in _pp.BalanceSheetDeliverSubInfoGetTable(guids).AsEnumerable()
                select new
                {
                    DealGuid = x.Field<Guid>("GUID"),
                    DealUniqueId = x.Field<int>("unique_id"),
                    DealName = x.Field<string>("item_name"),
                    FareAmount = x.Field<int>("fareAmount"),
                    IsTax = x.Field<bool>("is_tax"),
                    PartiallyPaymentDate = x.Field<DateTime?>("partially_payment_date"),
                    RemittanceType = x.Field<int>("remittance_type"),
                    TotalCount = x.Field<int>("totalCount"),
                    DebitCaseAmount = x.Field<int?>("debitCaseAmount") ?? 0,
                    DebitAllowanceAmount = x.Field<int?>("debitAllowanceAmount") ?? 0,
                    BalanceCreateDate = x.Field<DateTime?>("balance_sheet_create_date"),
                    DealCost = Convert.ToInt32(x.Field<decimal>("cost")),
                    GroupOrderStatus = x.Field<int>("status"),
                    Slug = x.Field<int>("slug")
                };

            foreach (var item in deliverSubInfo)
            {
                var result = new BalanceSheetsDeliverInfo
                {
                    DealGuid = item.DealGuid,
                    DealName = item.DealName,
                    DealCost = Convert.ToInt32(item.DealCost),
                    DealUniqueId = item.DealUniqueId,
                    //結檔份數
                    DealResultCount = item.Slug,
                    //出貨份數(查詢balance sheet detail 後 應付金額才可以算乘單價
                    DealCount = item.TotalCount,
                    FareAmount = Convert.ToInt32(item.FareAmount),
                    IsTax = item.IsTax,
                    DebitCaseAmount = item.DebitCaseAmount,
                    DebitAllowanceAmount = item.DebitAllowanceAmount,
                    DealRemittanceType = item.RemittanceType,
                    PartiallyPaymentDate = item.PartiallyPaymentDate,
                    BalanceCreatDate = item.BalanceCreateDate,
                    PartialFail = Helper.IsFlagSet(item.GroupOrderStatus, GroupOrderStatus.PartialFail)
                };

                result.DealTotalAmount = result.DealCost*result.DealCount;

                //區分暫付七成或其他
                //在區分異常或正常
                if (result.DealRemittanceType.Equals((int) RemittanceType.ManualPartially))
                {
                    var desc = result.PartialFail
                        ? "退貨超過三成"
                        : string.Format("{0:yyyy/MM/dd}", result.PartiallyPaymentDate);
                    result.DescPartialPaymentDate = string.Format("[{0}]{1}", result.DealUniqueId, desc);

                    result.PartiallyPaymentAmount = result.PartialFail
                        ? 0
                        : Convert.ToInt32(Math.Ceiling(Convert.ToDouble(item.DealCost*result.DealResultCount*0.7)));
                }
                else if (result.DealRemittanceType.Equals((int) RemittanceType.Others))
                {
                    result.DescPartialPaymentDate = string.Format("[{0}]無暫付", result.DealUniqueId);
                    result.PartiallyPaymentAmount = 0;
                }

                result.DealTotalFinalAmount = result.DealTotalAmount - result.PartiallyPaymentAmount +
                                              result.DebitCaseAmount + result.FareAmount;

                infos.Add(result);
            }

            return infos.OrderBy(x => x.DealUniqueId);
        }
    }

    public class TransferInfo
    {
        /// <summary>
        /// 應付總額
        /// </summary>
        public int AccountsPayable { get; set; }
        /// <summary>
        /// 實付總額
        /// </summary>
        public int? AccountsPaid { get; set; }
        public string Memo { get; set; }
        /// <summary>
        /// 負責人
        /// </summary>
        public string ReceiverBossName { get; set; }
        /// <summary>
        /// 受款戶名
        /// </summary>
        public string ReceiverTitle { get; set; }
        /// <summary>
        /// 受款ID (統編/身分證字號)
        /// </summary>
        public string ReceiverCompanyId { get; set; }
        /// <summary>
        /// 受款帳戶銀行代號
        /// </summary>
        public string ReceiverAccountBankNo { get; set; }
        /// <summary>
        /// 受款帳戶分行代號
        /// </summary>
        public string ReceiverAccountBranchNo { get; set; }
        /// <summary>
        /// 受款帳號
        /// </summary>
        public string ReceiverAccountNo { get; set; }
    }

    public class BalanceSheetsDeliverInfo
    {
        public Guid DealGuid { get; set; }
        public string DealName { get; set; }
        public int DealUniqueId { get; set; }
        public int DealCost { get; set; }
        public int FareAmount { get; set; }
        //出貨份數
        public int DealCount { get; set; }
        //結檔份數
        public int DealResultCount { get; set; }
        public DateTime? PartiallyPaymentDate { get; set; }
        public int PartiallyPaymentAmount { get; set; }
        public int DealTotalAmount { get; set; }
        public int DealTotalFinalAmount { get; set; }
        public bool IsTax { get; set; }
        public int DealRemittanceType { get; set; }
        //DebitCaseAmount       對帳單扣除金額 allowance_amount=0 就用amount
        //DebitAllowanceAmount  發票扣除金額  只看allowance_amount
        public int DebitCaseAmount { get; set; }
        public int DebitAllowanceAmount { get; set; }
        public DateTime? BalanceCreatDate { get; set; }
        public string DescPartialPaymentDate { get; set; }
        public bool PartialFail { get; set; }
    }

}

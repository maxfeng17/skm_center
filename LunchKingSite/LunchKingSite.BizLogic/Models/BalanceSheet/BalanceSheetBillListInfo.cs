﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model.BalanceSheet
{
    public class BalanceSheetBillListInfo
    {
        #region props

        public int Id{ get; set; }
        public int Year { get; set; }
        public int Month{ get; set; }
        public string Name { get; set; }
        public int BillId { get; set; }
        public string BillNumber { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public int BuyerType { get; set; }
        public DateTime? BillSentDate { get; set; }
        public int BillMoney { get; set; }
        public int BillMoneyNotaxed { get; set; }
        public int BillTax { get; set; }
        public string Remark { get; set; }
        public DateTime DealEndTime { get; set; }
        public DateTime UseStartTime { get; set; }
        public DateTime UseEndTime { get; set; }
        public string EmpNo { get; set; }
        public int VendorReceiptType { get; set; }
        public int BalanceSheetUseType { get; set; }
        public string CompanyAccountName { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAccount { get; set; }
        public string CompanyBankCode { get; set; }
        public string CompanyBranchCode { get; set; }
        public string CompanyID { get; set; }
        public string CompanyBossName { get; set; }
        public string AccountantName { get; set; }
        public string AccountantTel { get; set; }
        public Guid ProductGuid { get; set; }
        public BusinessModel BizModel { get; set; }
        public Guid? StoreGuid { get; set; }
        public int RemittanceType { get; set; }
        public int UniqueId { get; set; }
        public int GenerationFrequency { get; set; }
        public string EmpName { get; set; }
        public string StoreName { get; set; }
        public Guid SellerGuid { get; set; }
        public string SellerName { get; set; }
        public bool IsConfirmedReadyToPay { get; set; }
        public DateTime? FinanceGetDate { get; set; }
        public string Message { get; set; }
        public decimal TotalSum { get; set; }
        public int BillSum { get; set; }
        public int BillSumNotaxed { get; set; }
        public int BlTax { get; set; }
        public string BlRemark { get; set; }
        public string InvoiceComId { get; set; }
        public DateTime IntervalEnd { get; set; }
        /// <summary>
        /// 對帳單金額
        /// </summary>
        public int EstAmount { get; set; }
        /// <summary>
        /// 發票總計金額
        /// </summary>
        public int InvoiceTotal { get; set; }
        /// <summary>
        /// 稅率
        /// </summary>
        public decimal Tax { get; set; }
        /// <summary>
        /// 商品服務稅金額
        /// </summary>
        public int GstTotal { get; set; }
        /// <summary>
        /// 小計金額(未含稅)
        /// </summary>
        public int SubTotal { get; set; }
        /// <summary>
        /// 剩餘發票總計金額
        /// </summary>
        public int ResidualInvoiceTotal { get; set; }
        /// <summary>
        /// 剩餘商品服務稅金額
        /// </summary>
        public int ResidualGstTotal { get; set; }
        /// <summary>
        /// 剩餘小計金額(未含稅)
        /// </summary>
        public int ResidualSubTotal { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public int FreightAmounts { get; set; }
        /// <summary>
        /// 異動調整金額
        /// </summary>
        public int AdjustmentAmount { get; set; }
        /// <summary>
        /// +逾期出貨
        /// </summary>
        public int PositivePaymentOverdueAmount { get; set; }
        /// <summary>
        /// -逾期出貨
        /// </summary>
        public int NegativePaymentOverdueAmount { get; set; }
        /// <summary>
        /// 全家物流處理費用總額
        /// </summary>
        public int IspFamilyAmount { get; set; }
        /// <summary>
        /// 7-11物流處理費用總額
        /// </summary>
        public int IspSevenAmount { get; set; }
        /// <summary>
        /// PCHOME物流費用
        /// </summary>
        public int WmsOrderAmount { get; set; }
        /// <summary>
        /// PCHOME倉儲費用
        /// </summary>
        public int WmsAmount { get; set; }
        /// <summary>
        /// 物流對開發票號碼
        /// </summary>
        public string VendorInvoiceNumber { get; set; }
        /// <summary>
        /// 物流對開發票開立日期
        /// </summary>
        public DateTime? VendorInvoiceNumberTime { get; set; }
        /// <summary>
        /// 單據裡該對帳單的部分可否編輯
        /// </summary>
        public bool BillBalanceSheetEditable { get; set; }
        public string CreateId { get; set; }
        public int DeliveryType { get; set; }
        /// <summary>
        /// 預計付款日
        /// </summary>
        public DateTime? DefaultPaymentTime { get; set; }
        /// <summary>
        /// 應稅/免稅
        /// </summary>
        public bool IsTaxRequire { get; set; }
        /// <summary>
        /// 業績歸屬
        /// </summary>
        public string DeptId { get; set; }

        #endregion

    }
}

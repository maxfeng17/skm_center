﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using Amazon.CloudSearch.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using NPOI.HSSF.Record.Formula.Functions;

namespace LunchKingSite.BizLogic.Vbs.BS
{    
    public class BalanceSheetWmsModel
    {
        #region fields

        private static IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        private BalanceSheetWm _dbBs = null;
        private BalanceSheetWm DbBs
        {
            get
            {
                if (_dbBs == null)
                {
                    _dbBs = ap.BalanceSheetWmsGet(_id);
                }
                return _dbBs;
            }
        }

        private FundTransferInfo _transferInfo;
        /// <summary>
        /// 匯款資訊
        /// </summary>
        //public FundTransferInfo TransferInfo
        //{
        //    get
        //    {
        //        if (_transferInfo == null)
        //        {
        //            _transferInfo = new FundTransferInfo(this.Id, GetAccountsPayable, this.Type);
        //        }
        //        return _transferInfo;
        //    }
        //}

        #endregion

        #region properties

        private readonly int _id;
        public int Id
        {
            get
            {
                return _id;
            }
        }

        public int? Year
        {
            get { return this.DbBs.Year; }
        }
        public int? Month
        {
            get { return this.DbBs.Month; }
        }
                
        public int? Day
        {
            get 
            {
                if (this.DbBs.GenerationFrequency == (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet)
                {
                    return this.DbBs.IntervalEnd.Day;
                }
                return null;
            }
        }
        public BalanceSheetType Type
        {
            get { return (BalanceSheetType)this.DbBs.BalanceSheetType; }
        }


        public bool IsConfirmed
        {
            get { return this.DbBs.IsConfirmedReadyToPay; }
        }



        #endregion

        #region public methods

        public BalanceSheetWm GetDbBalanceSheet()
        {
            var result = this.DbBs.Clone();
            if (this.DbBs.IsLoaded)
            {
                result.IsNew = false;
                result.IsLoaded = true;
            }
            return result;
        }

        public int GetWmsAmount()
        {
            return ap.BalanceSheetWmsGet(_id).EstAmount;
        }

        /// <summary>
        /// 改變對帳單可付款狀態
        /// </summary>
        public BillingConfirmationState ConfirmBillingStatement(string user)
        {
            BillingConfirmationState currentState = GetBillingConfirmationState();
            if (currentState == BillingConfirmationState.Confirmable)
            {
                var bsType = (BalanceSheetType)this.DbBs.BalanceSheetType;

                switch (bsType)
                {
                    case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                        ConfirmCurrentBs(user);
                        this._dbBs = null;
                        return BillingConfirmationState.Confirmed;
                    default:
                        return BillingConfirmationState.UnknownFailure;
                }
            }
            else
            {
                return currentState;
            }
        }

        /// <summary>
        /// 取得對帳單之單據確認狀況
        /// </summary>        
        private BillingConfirmationState GetBillingConfirmationState()
        {
            var bsType = (BalanceSheetType)this.DbBs.BalanceSheetType;

            switch (bsType)
            {
                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                    if (this.DbBs.IsConfirmedReadyToPay)
                    {
                        return BillingConfirmationState.AlreadyConfirmed;
                    }
                    if (!DistributedAmountCheck())
                    {
                        return BillingConfirmationState.AmountInBillsTooDifferent;
                    }
                    return BillingConfirmationState.Confirmable;
                default:
                    throw new InvalidDataException();
            }
        }

        /// <summary>
        /// 取對帳單已分配到單據內的金額
        /// </summary>
        public int GetDistributedAmount()
        {
            if (this.DbBs == null || this.DbBs.IsNew)
            {
                throw new InvalidOperationException(string.Format("balance sheet (id={0}) does not exist!", this.Id));
            }

            var bsType = (BalanceSheetType)this.DbBs.BalanceSheetType;

            switch (bsType)
            {
                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                    BalanceSheetWmsBillRelationshipCollection bsBills =
                        ap.BalanceSheetWmsBillRelationshipGetListByBalanceSheetId(this.DbBs.Id);
                    return bsBills.Sum(bill => bill.BillMoney);
                default:
                    throw new InvalidOperationException("monthly balance sheet only!");
            }
        }

        public bool DistributedAmountCheck()
        {
            return true;
        }

        private void ConfirmCurrentBs(string user)
        {
            DateTime now = DateTime.Now;

            this.DbBs.IsConfirmedReadyToPay = true;
            this.DbBs.IsReceiptReceived = true;
            this.DbBs.ConfirmedTime = now;
            this.DbBs.ConfirmedUserName = user;

            using (TransactionScope txScope = new TransactionScope(
                TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                ap.BalanceSheetWmsSet(this.DbBs);
                
                this._transferInfo = null;
                this._dbBs = null;
                txScope.Complete();
            }
        }


        public void UpdateDefaultPaymentTime(string DfPaymentTime)
        {
            DateTime? date = null;
            if (DfPaymentTime != "")
            {
                date = Convert.ToDateTime(DfPaymentTime);
            }
            this.DbBs.DefaultPaymentTime = date;
            ap.BalanceSheetWmsSet(this.DbBs);
        }

        public string GetDefaultPaymentDate()
        {
            if (this.DbBs.DefaultPaymentTime != null)
            {
                DateTime? date = null;
                date = Convert.ToDateTime(this.DbBs.DefaultPaymentTime);
                return Convert.ToString(Convert.ToDateTime(date).ToShortDateString());
            }
            else
            {
                return "";
            }

        }

        public bool CancelBsConfirmedAndReceiptReceived()
        {
            if (!this.DbBs.IsConfirmedReadyToPay && !this.DbBs.IsReceiptReceived)
            {
                return false;
            }

            var bsType = (BalanceSheetType)this.DbBs.BalanceSheetType; ;
            switch (bsType)
            {
                case BalanceSheetType.FortnightlyToHouseBalanceSheet:
                    CancelCurrentBsConfirmedAndReceiptReceived();
                    this._dbBs = null;
                    return true;
                default:
                    return false;
            }
        }
        private void CancelCurrentBsConfirmedAndReceiptReceived()
        {
            //取消確認:只為了誤key單據且已確認對帳單 在進行刪除單據後 能夠讓營管重新新增單據
            //只須回復月對帳單的確認狀態 以利重新輸入 不須一併將週對帳單也進行取消確認
            //也不須另外檢查(影響) 該月對帳單之前月對帳單or之後月對帳單之確認狀態
            if (this.DbBs.IsConfirmedReadyToPay)
            {
                this.DbBs.IsConfirmedReadyToPay = false;
                this.DbBs.ConfirmedTime = null;
                this.DbBs.ConfirmedUserName = null;
                this.DbBs.DefaultPaymentTime = null;
            }
            //取消單據已回:當進行刪除單據需檢查單據金額是否滿足對帳單應付金額
            if (DistributedAmountCheck())
            {
                if (this.DbBs.EstAmount != 0)
                {
                    //發票金額為0但有其他數字時，刪除也需恢復到原狀
                    this.DbBs.IsReceiptReceived = false;
                }
                else
                {
                    this.DbBs.IsReceiptReceived = DistributedAmountCheck();
                }
            }
            else
            {
                this.DbBs.IsReceiptReceived = DistributedAmountCheck();
            }


            ap.BalanceSheetWmsSet(this.DbBs);

            this._transferInfo = null;
            this._dbBs = null;
        }

        #endregion

        #region .ctors

        public BalanceSheetWmsModel(int balanceSheetId)
        {
            _id = balanceSheetId;
        }


        #endregion

      
      
    }
    
}

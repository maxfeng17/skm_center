﻿using Amazon.DataPipeline.Model;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.BalanceSheet
{
    public class BalanceSheetToHouseModel
    {
        private static IAccountingProvider _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        #region prop

        public Guid DealId { get; set; }
        public string DealName { get; set; }
        public int DealUniqueId { get; set; }
        public string ShipPeriodStart { get; set; }
        public string ShipPeriodEnd { get; set; }
        public bool IsTax { get; set; }
        /// <summary>
        /// 檔次的出帳方式
        /// </summary>
        public RemittanceType RemittanceType { get; set; }
        /// <summary>
        /// 對帳單的出帳方式(因有轉換可能不同)
        /// </summary>
        public string BalanceSheetRemittanceTypeDesc { get; set; }
        public VendorReceiptType VendorReceiptType { get; set; }
        //應付總金額
        public int EstAmount { get; set; }
        //總計運費
        public int FreightAmount { get; set; }
        public int PaymentChangeAmount { get; set; }
        public int ReceiptAmount { get; set; }
        public int PositivePaymentOverdueAmount { get; set; }
        public int NegativePaymentOverdueAmount { get; set; }
        

        public int? AccountPaid { get; set; }
        public bool IsTransferComplete { get; set; }
        public IEnumerable<BalanceSheetsToHouseInfo> BsInfos { get; set; }
        public IEnumerable<BalanceSheetsToHouseDealInfo> DealInfos { get; set; }
        public FundTransferInfo FundTransferInfo { get; set; }
        public int ExceedRemittanceTimeLimit { get; set; }


        #endregion

        public void Initial(Guid productGuid, int? bsId)
        {
            try
            {
                var mainDeal = _pp.ViewPponDealGetByBusinessHourGuid(productGuid);

                DealId = productGuid;
                DealUniqueId = mainDeal.UniqueId ?? 0;
                DealName = !string.IsNullOrEmpty(mainDeal.AppTitle) ? mainDeal.AppTitle : mainDeal.ItemName;
                ShipPeriodStart = string.Format("{0:yyyy/MM/dd}", mainDeal.BusinessHourDeliverTimeS);
                ShipPeriodEnd = string.Format("{0:yyyy/MM/dd}", mainDeal.BusinessHourDeliverTimeE);

                var da = _pp.DealAccountingGet(productGuid);
                RemittanceType = (RemittanceType)da.RemittanceType;
                VendorReceiptType = (VendorReceiptType)da.VendorReceiptType;
                IsTax = da.IsInputTaxRequired;

                //撈取 多檔次母檔對應之子檔 資訊 
                var subDeals = _pp.GetComboDealByBid(productGuid, false).Select(x => x.BusinessHourGuid).ToList();

                //檢查是否為多檔次
                if (!subDeals.Any())
                {
                    subDeals = new List<Guid> { productGuid };
                }
                //所有對帳單明細 依週/月結/彈性付款 不同出帳方式顯示
                var bsInfos = _ap.ViewBalanceSheetListGetList(subDeals);
                if (!bsInfos.Any())
                {
                    return;
                }
                BsInfos = bsInfos.GroupBy(x => new { x.IntervalStart, x.IntervalEnd })
                    .Select(x =>
                    {
                        var bsDesc = string.Empty;
                        switch (x.First().RemittanceType)
                        {
                            case (int)RemittanceType.Flexible:
                                bsDesc = string.Format("{0:yyyy/MM/dd}", x.Key.IntervalEnd);
                                break;
                            case (int)RemittanceType.Monthly:
                                bsDesc = string.Format("{0}/{1}", x.Max(b => b.Year), x.Max(b => b.Month));
                                break;
                            case (int)RemittanceType.Weekly:
                                bsDesc = string.Format("{0:yyyy/MM/dd}", x.Key.IntervalEnd.AddDays(-1));
                                break;
                            case (int)RemittanceType.Fortnightly:
                                bsDesc = string.Format("{0:yyyy/MM/dd}", x.Key.IntervalEnd.AddDays(-1));
                                break;
                            default:
                                break;
                        }

                        int positivePaymentOverdueAmount = 0;
                        //這邊似乎慢,可以調教?
                        foreach (int bsid in x.Select(y => y.Id))
                        {
                            var bsModel = new BalanceSheetModel(bsid);
                            positivePaymentOverdueAmount += bsModel.GetVendorPaymentOverdueAmount(true);
                        }
                       

                        return new BalanceSheetsToHouseInfo
                        {
                            BalanceSheetId = x.Max(b => b.Id),
                            BalanceSheetDesc = bsDesc,
                            IntervalEnd = x.Key.IntervalEnd,
                            NotYetConfirmed = x.Any(b => !b.IsConfirmedReadyToPay),
                            IsConfirmed = x.Any(b => b.IsConfirmedReadyToPay && b.ConfirmedUserName != "sys"),
                            NoConfirmed = x.Any(b => b.IsConfirmedReadyToPay && b.ConfirmedUserName == "sys"),
                            IsNoPositive = positivePaymentOverdueAmount == 0 && (x.Sum(b => b.EstAmount) == x.Sum(b => -(b.IspSevenAmount) + -(b.IspFamilyAmount) + b.OverdueAmount)) && x.Sum(b => b.EstAmount) < 0
                        };
                    })
                    .OrderByDescending(x => x.IntervalEnd);
                bsId = bsId ?? BsInfos.First().BalanceSheetId;
                
                
                //單筆對帳單檔次付款明細
                var currentBs = bsInfos.FirstOrDefault(x => x.Id == bsId);
                if (currentBs == null)
                {
                    return;
                }

                var currentBsInfo = bsInfos.Where(x => x.IntervalStart == currentBs.IntervalStart
                                                    && x.IntervalEnd == currentBs.IntervalEnd);
                var dealInfos = new List<BalanceSheetsToHouseDealInfo>();
                var fundTransferInfos = new List<FundTransferInfo>();
                foreach (var bs in currentBsInfo)
                {
                    var bsModel = new BalanceSheetModel(bs.Id);
                    var deliveryPaymentInfo = bsModel.GetDeliveryPaymentInfo();
                    var vendorPaymentAmount = bsModel.GetVendorPaymentAmount();
                    var positiveVendorPaymentOverdueAmount = bsModel.GetVendorPaymentOverdueAmount(true);
                    var negativeVendorPaymentOverdueAmount = bsModel.GetVendorPaymentOverdueAmount(false);
                    var deal = new BalanceSheetsToHouseDealInfo
                    {
                        DealGuid = bs.ProductGuid,
                        DealName = bs.Name,
                        DealUniqueId = bs.UniqueId,
                        BusinessHourOrderTimeS = bs.BusinessHourOrderTimeS.Value.ToString("yyyy/MM/dd"),
                        BusinessHourOrderTimeE = bs.BusinessHourOrderTimeE.Value.ToString("yyyy/MM/dd"),
                        DealCost = bs.Cost ?? 0,
                        EstAmount = bs.EstAmount ?? 0,//最後總額
                        PayAmount = Convert.ToInt32(deliveryPaymentInfo.PayAmount),//應付金額
                        DeductAmount = Convert.ToInt32(deliveryPaymentInfo.DeductAmount),//退貨金額
                        PayCount = deliveryPaymentInfo.PayCount,
                        DeductCount = deliveryPaymentInfo.DeductCount,
                        PayFreightAmount = deliveryPaymentInfo.PayFreightAmount,//應付運費
                        PayFreightCount = deliveryPaymentInfo.PayFreightCount,//應付運費份數
                        DeductFreightAmount = deliveryPaymentInfo.DeductFreightAmount,//退貨運費
                        PaymentChangeAmount = vendorPaymentAmount != null ? vendorPaymentAmount.Amount : 0,
                        AllowanceAmount = vendorPaymentAmount != null ? vendorPaymentAmount.AllowanceAmount : 0,
                        PositivePaymentOverdueAmount = positiveVendorPaymentOverdueAmount,
                        NegativePaymentOverdueAmount = negativeVendorPaymentOverdueAmount,
                        IsConfirmReadyToPay = bs.IsConfirmedReadyToPay,
                        ISPFamilyOrderBeforeCount = deliveryPaymentInfo.ISPFamilyOrderBeforeCount,//全家物流費用份數
                        ISPFamilyOrderNowCount = deliveryPaymentInfo.ISPFamilyOrderNowCount,//全家物流費用份數
                        ISPSevenOrderBeforeCount = deliveryPaymentInfo.ISPSevenOrderBeforeCount,//7-11物流費用異動前份數
                        ISPSevenOrderNowCount = deliveryPaymentInfo.ISPSevenOrderNowCount,//7-11物流費用異動後份數
                        ISPFamilyOrderAmount = bs.IspFamilyAmount,//全家物流總費用
                        ISPSevenOrderAmount = bs.IspSevenAmount,//7-11物流總費用
                        WmsOrderAmount = deliveryPaymentInfo.WmsOrderAmount//PCHOME倉儲物流
                    };

                    
                    dealInfos.Add(deal);
                    fundTransferInfos.Add(bsModel.TransferInfo);
                }
                DealInfos = dealInfos.OrderBy(x => x.DealUniqueId);

                AccountPaid = fundTransferInfos.Sum(x => x.AccountsPaid ?? 0);

                var fundsCount = fundTransferInfos.Where(x => x.AccountsPayable != 0).ToList();
                IsTransferComplete = fundsCount.Count > 0 && fundsCount.All(x => x.AccountsPaid.HasValue);
                FundTransferInfo = IsTransferComplete
                                    ? fundsCount.OrderByDescending(x => x.AccountsPayable)
                                                       .ThenByDescending(x => x.TransferCompletionDescription)
                                                       .ThenBy(x => x.BalanceSheetId).FirstOrDefault()
                                    : fundTransferInfos.Where(x => !x.AccountsPaid.HasValue)
                                                       .OrderByDescending(x => x.AccountsPayable)
                                                       .ThenByDescending(x => x.TransferCompletionDescription)
                                                       .ThenBy(x => x.BalanceSheetId).FirstOrDefault();

                if (FundTransferInfo != null)
                {
                    FundTransferInfo.Memo = string.Join("<br/>", fundTransferInfos.Where(x => !string.IsNullOrEmpty(x.Memo)).Select(x => x.Memo).ToList());
                }
                
                EstAmount = DealInfos.Sum(x => x.EstAmount);
                FreightAmount = DealInfos.Sum(x => x.PayFreightAmount) - DealInfos.Sum(x => x.DeductFreightAmount);
                PaymentChangeAmount = DealInfos.Sum(x => x.PaymentChangeAmount);
                PositivePaymentOverdueAmount = DealInfos.Sum(x => x.PositivePaymentOverdueAmount);
                NegativePaymentOverdueAmount = DealInfos.Sum(x => x.NegativePaymentOverdueAmount);

                //1.免稅:運費/逾期補款要分開算發票
                //2.超取發票不扣除物流費用
                //3.逾期若是負數不用扣發票,正數才算進發票裡,逾期算含稅
                //4.為版面上的第一張發票
                //5.前者為含稅發票，後者為不含稅發票(排除運費和補款)
                ReceiptAmount = IsTax || VendorReceiptType == VendorReceiptType.Other || VendorReceiptType == VendorReceiptType.Receipt
                                ? EstAmount - PaymentChangeAmount + DealInfos.Sum(x => x.AllowanceAmount) + DealInfos.Sum(x => x.ISPFamilyOrderAmount) + DealInfos.Sum(x => x.ISPSevenOrderAmount) + DealInfos.Sum(x => x.WmsOrderAmount) + Math.Abs(NegativePaymentOverdueAmount)
                                : EstAmount - PaymentChangeAmount + DealInfos.Sum(x => x.AllowanceAmount) + DealInfos.Sum(x => x.ISPFamilyOrderAmount) + DealInfos.Sum(x => x.ISPSevenOrderAmount) + DealInfos.Sum(x => x.WmsOrderAmount) + Math.Abs(NegativePaymentOverdueAmount) - Math.Abs(PositivePaymentOverdueAmount) - FreightAmount;

                //宅配對帳單結帳方式
                if (currentBs == null)
                    BalanceSheetRemittanceTypeDesc = "";
                else
                    BalanceSheetRemittanceTypeDesc = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)currentBs.RemittanceType);

                //ExceedRemittanceTimeLimit = DealInfos.Sum(x => x.ExceedRemittanceTimeLimit);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ex.StackTrace);
            }
        }
    
    }

    public class BalanceSheetsToHouseDealInfo
    {
        public Guid DealGuid { get; set; }
        public string DealName { get; set; }
        public int DealUniqueId { get; set; }
        public string BusinessHourOrderTimeS { get; set; }
        public string BusinessHourOrderTimeE { get; set; }
        public decimal DealCost { get; set; }
        //出貨份數
        public int PayCount { get; set; }
        public int DeductCount { get; set; }
        public int PayFreightAmount { get; set; }
        public int PayFreightCount { get; set; }
        public int DeductFreightAmount { get; set; }
        public int EstAmount { get; set; }
        public int PayAmount { get; set; }
        public int DeductAmount { get; set; }
        public int PaymentChangeAmount { get; set; }
        public int AllowanceAmount { get; set; }
        public int PositivePaymentOverdueAmount { get; set; }
        public int NegativePaymentOverdueAmount { get; set; }
        public bool IsConfirmReadyToPay { get; set; }
        public int ExceedRemittanceTimeLimit { get; set; }
        public List<int> ISPFamilyOrderBeforeCount { get; set; }
        public int ISPFamilyOrderNowCount { get; set; }
        public List<int> ISPSevenOrderBeforeCount { get; set; }
        public int ISPSevenOrderNowCount { get; set; }
        public int ISPFamilyOrderAmount { get; set; }
        public int ISPSevenOrderAmount { get; set; }
        public int WmsOrderAmount { get; set; }
    }

    public class BalanceSheetsToHouseInfo
    {
        public int BalanceSheetId { get; set; }
        public string BalanceSheetDesc { get; set; }
        public DateTime IntervalEnd { get; set; }
        public bool IsConfirmed { get; set; }
        public bool NoConfirmed { get; set; }
        public bool NotYetConfirmed { get; set; }
        public string BalanceSheetRemittanceTypeDesc { get; set; }
        public bool IsNoPositive { get; set; }
        

    }

}

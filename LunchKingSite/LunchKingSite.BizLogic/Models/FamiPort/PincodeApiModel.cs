﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.BizLogic.Models.FamiPort
{
    #region Base Class

    public class PincodeApiBase
    {
        /// <summary>
        /// 訂單日期
        /// </summary>
        private DateTime _orderDate;
        /// <summary>
        /// 交易代碼
        /// </summary>
        private FamilyNetTransStatus _transStatus;

        /// <summary>
        /// Famiport 認證ID
        /// </summary>
        [JsonProperty("AUTHID", Order = 1)]
        public string AuthId
        {
            get
            {
                return FamiGroupCoupon.GetFamiportAuthId();
            }
        }

        /// <summary>
        /// 訂單日期
        /// </summary>
        [JsonProperty("ORDER_DATE", Order = 2)]
        public string OrderDate
        {
            get { return _orderDate.ToString("yyyyMMddHHmmss"); }
        }

        /// <summary>
        /// 17Life Order.order_id
        /// </summary>
        [JsonProperty("ORDER_NO", Order = 3)]
        public string OrderNo { get; set; }

        [JsonProperty("STATUS", Order = 4)]
        public string Status
        {
            get { return Convert.ToString((int)_transStatus).PadLeft(2, '0'); }
        }

        /// <summary>
        /// 全家(全網)活動代碼
        /// </summary>
        [JsonProperty("ACT_CODE", Order = 5)]
        public string ActCode { get; set; }

        public void SetOrderDate(DateTime date)
        {
            _orderDate = date;
        }

        public void SetTransStatus(bool isQuery)
        {
            _transStatus = isQuery ? FamilyNetTransStatus.Code23 : FamilyNetTransStatus.Code03;
        }

        public void SetTransStatus(FamilyNetTransStatus status)
        {
            _transStatus = status;
        }
    }

    public class PincodeResultBase
    {
        /// <summary>
        /// 17Life Order.order_id
        /// </summary>
        [JsonProperty("ORDER_NO")]
        public string OrderNo { get; set; }

        /// <summary>
        /// 交易模式
        /// </summary>
        [JsonProperty("STATUS")]
        public string Status { get; set; }

        /// <summary>
        /// 回覆代碼
        /// </summary>
        [JsonProperty("REPLY_CODE")]
        public string ReplyCode { get; set; }

        /// <summary>
        /// 回覆描述
        /// </summary>
        [JsonProperty("REPLY_DESC")]
        public string ReplyDesc { get; set; }

        /// <summary>
        /// Pin碼, 多筆以,分格
        /// </summary>
        [JsonProperty("PINCODE")]
        public string PinCode { get; set; }

        /// <summary>
        /// Pincode 有效期限
        /// </summary>
        [JsonProperty("PIN_EXPIREDATE")]
        public string PinExpiredate { get; set; }

        public FamilyNetReplyCode GetReplyCode()
        {
            return (FamilyNetReplyCode)Convert.ToInt32(ReplyCode);
        }
    }

    #endregion

    #region PinOrder

    public class PinOrderInput : PincodeApiBase
    {
        /// <summary>
        /// 取Pin數量
        /// </summary>
        [JsonProperty("QTY", Order = 6)]
        public string Qty { set; get; }

    }
    
    public class PinOrderResult : PincodeResultBase
    {
        public List<string> GetPincode()
        {
            return PinCode.Split(',').ToList();
        }

        public DateTime? GetPinExpireDate()
        {
            if (string.IsNullOrEmpty(PinExpiredate)) return null;
            try
            {
                DateTime result;
                if (DateTime.TryParse(PinExpiredate, out result))
                {
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    #endregion PinOrder

    #region OrderExg

    public class FamiReturnPincodeInput
    {
        [JsonProperty("Bid")]
        public Guid Bid { get; set; }
        [JsonProperty("Og")]
        public Guid OrderGuid { get; set; }
    }

    public class ExecuteOrderExgInput
    {
        /// <summary>
        /// 加密後的Pincode
        /// </summary>
        [JsonProperty("pcode")]
        public string PezCodeEncode { get; set; }
        /// <summary>
        /// Order.Guid
        /// </summary>
        [JsonProperty("oid")]
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// coupon.id
        /// </summary>
        [JsonProperty("cid")]
        public int CouponId { get; set; }
    }

    public class PincodeOrderExgInput : PincodeApiBase
    {
        public PincodeOrderExgInput()
        {
            Detail = new List<OrderExgInputDetail>();
        }

        /// <summary>
        /// PIN碼
        /// </summary>
        [JsonProperty("PINCODE", Order = 6)]
        public string Pincode { get; set; }
        /// <summary>
        /// Detail
        /// </summary>
        [JsonProperty("DETAIL", Order = 7)]
        public List<OrderExgInputDetail> Detail { get; set; }
    }

    public class OrderExgInputDetail
    {
        /// <summary>
        /// 項次序號
        /// </summary>
        [JsonProperty("SERIAL_NO")]
        public string SerialNo { get; set; }
        /// <summary>
        /// 商品代號
        /// </summary>
        [JsonProperty("ITEMCODE")]
        public string ItemCode { get; set; }
    }

    public class OrderExgResult : PincodeResultBase
    {
        /// <summary>
        /// MMK交易序號
        /// </summary>
        [JsonProperty("TRAN_NO")]
        public string TranNo { get; set; }
        /// <summary>
        /// 活動注意事項1
        /// </summary>
        [JsonProperty("ACT_MEMO1")]
        public string ActMemo1 { get; set; }
        /// <summary>
        /// 活動注意事項2
        /// </summary>
        [JsonProperty("ACT_MEMO2")]
        public string ActMemo2 { get; set; }
        /// <summary>
        /// 活動注意事項3
        /// </summary>
        [JsonProperty("ACT_MEMO3")]
        public string ActMemo3 { get; set; }
        /// <summary>
        /// 活動注意事項4
        /// </summary>
        [JsonProperty("ACT_MEMO4")]
        public string ActMemo4 { get; set; }
        /// <summary>
        /// 全網交易序號
        /// </summary>
        [JsonProperty("FN_TRAN_NO")]
        public string FnTranNo { get; set; }
        /// <summary>
        /// detail
        /// </summary>
        [JsonProperty("DETAIL")]
        public List<OrderExgResultDetail> Detail { get; set; }
    }

    public class OrderExgResultDetail
    {
        /// <summary>
        /// 項次序號 
        /// </summary>
        [JsonProperty("SERIAL_NO")]
        public string SerialNo { get; set; }
        /// <summary>
        /// 商品代號
        /// </summary>
        [JsonProperty("ITEMCODE")]
        public string ItemCode { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        [JsonProperty("PROD_NAME")]
        public string ProdName { get; set; }
        /// <summary>
        /// 第一段條碼 
        /// </summary>
        [JsonProperty("BARCODE1")]
        public string Barcode1 { get; set; }
        /// <summary>
        /// 第二段條碼 
        /// </summary>
        [JsonProperty("BARCODE2")]
        public string Barcode2 { get; set; }
        /// <summary>
        /// 第三段條碼 
        /// </summary>
        [JsonProperty("BARCODE3")]
        public string Barcode3 { get; set; }
        /// <summary>
        /// 第四段條碼 
        /// </summary>
        [JsonProperty("BARCODE4")]
        public string Barcode4 { get; set; }
        /// <summary>
        /// 第五段條碼 
        /// </summary>
        [JsonProperty("BARCODE5")]
        public string Barcode5 { get; set; }
        /// <summary>
        /// 條碼有效期限
        /// </summary>
        [JsonProperty("EXPIREDATE")]
        public string ExpireDate { get; set; }
        /// <summary>
        /// 兌換種類 
        /// </summary>
        [JsonProperty("EXG_KIND")]
        public string ExgKind { get; set; }
        /// <summary>
        /// 兌換點數 
        /// </summary>
        [JsonProperty("EXG_BONUS")]
        public string ExgBonus { get; set; }
        /// <summary>
        /// 折價金額(兌換金額) 
        /// </summary>
        [JsonProperty("DIS_AMT")]
        public string DisAmt { get; set; }
        /// <summary>
        /// 商品注意事項1
        /// </summary>
        [JsonProperty("PROD_MEMO1")]
        public string ProdMemo1 { get; set; }
        /// <summary>
        /// 商品注意事項2
        /// </summary>
        [JsonProperty("PROD_MEMO2")]
        public string ProdMemo2 { get; set; }
        /// <summary>
        /// 商品注意事項3
        /// </summary>
        [JsonProperty("PROD_MEMO3")]
        public string ProdMemo3 { get; set; }
        /// <summary>
        /// 商品注意事項4
        /// </summary>
        [JsonProperty("PROD_MEMO4")]
        public string ProdMemo4 { get; set; }
    }

    #endregion OrderExg

    #region FamilyNet Verify

    public class PincodeVerifyInput
    {
        public PincodeVerifyInput()
        {
            Detail = new List<PincodeInputDetail>();
        }

        /// <summary>
        /// 認證代號
        /// </summary>
        [JsonProperty("AUTHID")]
        public string AuthId { get; set; }
        /// <summary>
        /// 交易模式
        /// </summary>
        [JsonProperty("STATUS")]
        public string Status { get; set; }
        /// <summary>
        /// 核銷明細
        /// </summary>
        [JsonProperty("DETAIL")]
        public List<PincodeInputDetail> Detail { get; set; }
    }

    public class PincodeInputDetail
    {
        /// <summary>
        /// MMK 交易序號
        /// </summary>
        [JsonProperty("TRAN_NO")]
        public string TranNo { get; set; }
        /// <summary>
        /// 項次序號
        /// </summary>
        [JsonProperty("SERIAL_NO")]
        public string SerialNo { get; set; }
        /// <summary>
        /// 活動代碼
        /// </summary>
        [JsonProperty("CD_ACT_CODE")]
        public string CdActCode { get; set; }
        /// <summary>
        /// PIN碼
        /// </summary>
        [JsonProperty("CD_PIN_CODE")]
        public string CdPinCode { get; set; }
        /// <summary>
        /// 商品代碼
        /// </summary>
        [JsonProperty("ITEMCODE")]
        public string ItemCode { get; set; }
        /// <summary>
        /// 結帳時間
        /// </summary>
        [JsonProperty("PAYMENT_TIME")]
        public string PaymentTime { get; set; }
        /// <summary>
        /// 結帳店號
        /// </summary>
        [JsonProperty("STORE_CODE")]
        public string StoreCode { get; set; }
        /// <summary>
        /// 收銀機交易序號
        /// </summary>
        [JsonProperty("TM_TRANS_NO")]
        public string TmTransNo { get; set; }
        /// <summary>
        /// 金額
        /// </summary>
        [JsonProperty("PRICE")]
        public string Price { get; set; }
        /// <summary>
        /// 條碼類別，1:17Life call api 取得 2:消費者透過全家客服進行卡紙補發後的條碼 3:開通Pin碼的資料
        /// </summary>
        [JsonProperty("FLAG")]
        public string Flag { get; set; }

        public PincodeInputDetail()
        {
            Flag = Convert.ToString((int) FamilyNetPincodeDetailFlag.OriginalBarcode);
        }
    }

    public class PincodeVerifyResult
    {
        public PincodeVerifyResult()
        {
            Detail = new List<VerifyResultDetail>();
        }

        /// <summary>
        /// Detail
        /// </summary>
        [JsonProperty("DETAIL")]
        public List<VerifyResultDetail> Detail { get; set; }
    }

    public class VerifyResultDetail
    {
        /// <summary>
        /// MMK 交易序號
        /// </summary>
        [JsonProperty("TRAN_NO")]
        public string TranNo { get; set; }
        /// <summary>
        /// 項次序號
        /// </summary>
        [JsonProperty("SERIAL_NO")]
        public string SerialNo { get; set; }
        /// <summary>
        /// 交易模式
        /// </summary>
        [JsonProperty("STATUS")]
        public string Status { get; set; }
        /// <summary>
        /// 廠商回覆時間
        /// </summary>
        [JsonProperty("REPLY_TIME")]
        public string ReplyTime { get; set; }
        /// <summary>
        /// 回覆代碼
        /// </summary>
        [JsonProperty("REPLY_CODE")]
        public string ReplyCode { get; set; }
        /// <summary>
        /// 回覆描述
        /// </summary>
        [JsonProperty("REPLY_DESC")]
        public string ReplyDesc { get; set; }
    }

    #endregion

    #region FamilyNet Return

    public class FamiDealExpiredReturnPin
    {
        [JsonProperty("In")]
        public PincodeReturnInput Input { get; set; }
        [JsonProperty("Og")]
        public Guid OrderGuid { get; set; }
        [JsonProperty("Ci")]
        public int CouponId { get; set; }
    }

    public class PincodeReturnInput : PincodeApiBase
    {
        public PincodeReturnInput()
        {
            
        }

        /// <summary>
        /// PIN 碼
        /// </summary>
        [JsonProperty("PINCODE")]
        public string PinCode { get; set; }
    }

    /// <summary>
    /// 新版input
    /// </summary>
    public class PincodeReturnInput2
    {
        /// <summary>
        /// 訂單日期
        /// </summary>
        private DateTime _orderDate;
        // <summary>
        /// 交易代碼
        /// </summary>
        private FamilyNetTransStatus _transStatus;
        /// <summary>
        /// Famiport 認證ID
        /// </summary>
        [JsonProperty("AUTHID", Order = 1)]
        public string AuthId
        {
            get
            {
                return FamiGroupCoupon.GetFamiportAuthId();
            }
        }

        /// <summary>
        /// 訂單日期
        /// </summary>
        [JsonProperty("ORDER_DATE", Order = 2)]
        public string OrderDate
        {
            get { return _orderDate.ToString("yyyyMMddHHmmss"); }
        }

        [JsonProperty("STATUS", Order = 4)]
        public string Status
        {
            get { return Convert.ToString((int)_transStatus).PadLeft(2, '0'); }
        }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("DETAIL", Order = 5)]
        public List<PincodeReturnInputDetail> Detail { get; set; }

        public void SetOrderDate(DateTime date)
        {
            _orderDate = date;
        }

        public void SetTransStatus(FamilyNetTransStatus status)
        {
            _transStatus = status;
        }
    }

    public class PincodeReturnInputDetail
    {
        /// <summary>
        /// 流水號
        /// </summary>
        [JsonProperty("SERIAL_NO")]
        public string SerialNo { get; set; }
        /// <summary>
        /// 活動代碼
        /// </summary>
        [JsonProperty("ACT_CODE")]
        public string ActCode { get; set; }
        /// <summary>
        /// PIN 碼
        /// </summary>
        [JsonProperty("PINCODE")]
        public string PinCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CouponId { get; set; }
    }

    public class PincodeReturnResult
    {
        /// <summary>
        /// 交易模式
        /// </summary>
        [JsonProperty("STATUS")]
        public string Status { get; set; }

        /// <summary>
        /// 回覆代碼
        /// </summary>
        [JsonProperty("REPLY_CODE")]
        public string ReplyCode { get; set; }

        /// <summary>
        /// 回覆描述
        /// </summary>
        [JsonProperty("REPLY_DESC")]
        public string ReplyDesc { get; set; }

        /// <summary>
        /// 活動代號
        /// </summary>
        [JsonProperty("ACT_CODE")]
        public string ActCode { get; set; }

        /// <summary>
        /// Pin碼, 多筆以,分格
        /// </summary>
        [JsonProperty("PINCODE")]
        public string PinCode { get; set; }

        public FamilyNetReplyCode GetReplyCode()
        {
            return !string.IsNullOrEmpty(ReplyCode) ? (FamilyNetReplyCode)Convert.ToInt32(ReplyCode) : FamilyNetReplyCode.Initial;
        }
    }

    /// <summary>
    /// 新版回傳
    /// </summary>
    public class PincodeReturnResult2
    {
        /// <summary>
        /// 交易模式
        /// </summary>
        [JsonProperty("STATUS")]
        public string Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("DETAIL")]
        public List<PincodeReturnResultDetail> Detail { get; set; }
    }

    public class PincodeReturnResultDetail : PincodeReturnResult
    {
        /// <summary>
        /// 流水號
        /// </summary>
        [JsonProperty("SERIAL_NO")]
        public string SerialNo { get; set; }
        public int CouponId { get; set; }
    }

    #endregion

    public class ConfirmExchangeAndLockInput
    {
        public Guid OrderGuid { get; set; }
        public List<int> CouponIdList { get; set; }
    }

    public class ConfirmECPayInput
    {
        public Guid OrderGuid { get; set; }
        public string MemberId { get; set; }
        public DataOrm.TaishinECPayTradeResponse TradeData { get; set; }
    }

    public class GetFamiGroupBarCodeInput
    {
        /// <summary>
        /// 訂單GUID
        /// </summary>
        public Guid OrderGuid { get; set; }

        /// <summary>
        /// 兌換張數
        /// </summary>
        public int ExchangeCount { get; set; }
        /// <summary>
        /// 憑證類型
        /// </summary>
        public int CouponType { get; set; }
    }

    public class GetFamiGroupBarCodeByGiftInput
    {
        /// <summary>
        /// 訂單GUID
        /// </summary>
        public Guid OrderGuid { get; set; }

        /// <summary>
        /// 兌換張數
        /// </summary>
        public int ExchangeCount { get; set; }

        /// <summary>
        /// 禮物連結
        /// </summary>
        public string AsCode { get; set; }

        /// <summary>
        /// 憑證類型
        /// </summary>
        public int CouponType { get; set; }
    }


    public class FamiGroupBarcode
    {
        /// <summary>
        /// 憑證編號
        /// </summary>
        public int CouponId { get; set; }

        /// <summary>
        /// 全家Pincode
        /// </summary>
        public string Pincode { get; set; }

        /// <summary>
        /// 全家四段式條碼
        /// </summary>
        public List<string> Barcode { get; set; }
    }
}

﻿using System;
using LunchKingSite.Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model.FamiPort
{
    public class DailyBeaconEffectReportModel
    {

        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public List<DailyFamiportItemTotalTriggerReport> DailyFamiportItemTriggerTotalReportList { get; set; }
        public List<DailyGroupFamiPortTriggerReport> DailyGroupFamiPortTriggerReportList { get; set; }
    }

    public class DailyFamiportItemTotalTriggerReport
    {
        public DailyFamiportItemTotalTriggerReport()
        {
            SubItemList= new List<DailyFamiportItemTriggerReport>();
        }

        private int SubItemListCount()
        {
            return SubItemList.Count;
        }

        public double GetAverageCouponAmount(DailyFamiportItemTotalTriggerReport totalTriggerReport)
        {
            if (totalTriggerReport.SubItemListCount()<=0)
            {
                return 0;
            }
            return Math.Round(((double)CouponAmount / (double)(totalTriggerReport.SubItemListCount())), 2);
        }

        public double GetAveragePayAmount(DailyFamiportItemTotalTriggerReport totalTriggerReport)
        {
            if (totalTriggerReport.SubItemListCount() <= 0)
            {
                return 0;
            }
            return Math.Round(((double)PayAmount / (double)(totalTriggerReport.SubItemListCount())), 2);
        }

        public int FamiportType { get; set; }
        public string FamiportTypeName { get; set; }
        public int BeaconTriggerAmount {
            get
            {
                if (SubItemListCount() <= 0)
                {
                    return 0;
                }
                else
                {
                    return SubItemList.Sum(x=>x.BeaconTriggerAmount);
                }
            }
        }
        public int SoonTriggerAmount {
            get
            {
                if (SubItemListCount() <= 0)
                {
                    return 0;
                }
                else
                {
                    return SubItemList.Sum(x => x.SoonTriggerAmount);
                }
            }
        }
        public int CouponAmount {
            get
            {
                if (SubItemListCount() <= 0)
                {
                    return 0;
                }
                else
                {
                    return SubItemList.Sum(x => x.CouponAmount);
                }
            }
        }
        public int PayAmount {
            get
            {
                if (SubItemListCount() <= 0)
                {
                    return 0;
                }
                else
                {
                    return SubItemList.Sum(x => x.PayAmount);
                }
            }
        }

        public List<DailyFamiportItemTriggerReport> SubItemList { get; set; }
    }

    public class DailyGroupFamiPortTriggerReport
    {
        public DateTime ReportDateTime { get; set; }
        public int LockBeaconTriggerAmount { get; set; }
        public int LockSoonTriggerAmount { get; set; }
        public int LockCouponAmount { get; set; }
        public int LockPayAmount { get; set; }
        public int LockItemCount { get; set; }
        public int UnlockCouponAmount { get; set; }
        public int UnlockPayAmount { get; set; }
        public int UnLockItemCount { get; set; }
        public int GeneralCouponAmount { get; set; }
        public int GeneralPayAmount { get; set; }
        public int GeneralItemCount { get; set; }


    }
}

﻿using System;
using LunchKingSite.Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model.FamiPort
{
    public class DailyBeaconStoreEffectReportModel
    {
        public DailyBeaconStoreEffectReportModel()
        {
            DailyBeaconStoreEffectReportList =new List<DailyBeaconStoreEffectReport>();
        }

        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public List<DailyBeaconStoreEffectReport> DailyBeaconStoreEffectReportList { get; set; }
    }

}

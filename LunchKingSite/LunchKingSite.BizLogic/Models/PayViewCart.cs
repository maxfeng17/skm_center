﻿using System.Collections.Generic;
using LunchKingSite.Core;
using System;

namespace LunchKingSite.BizLogic.Model
{
    public sealed class PayViewCartEntry
    {
        #region props
        private string _id = null;
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public Guid ItemGuid { get; set; }

        private string _itemName = null;
        public string ItemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }

        private decimal _itemUnitPrice = 0M;
        public decimal ItemUnitPrice
        {
            get { return _itemUnitPrice; }
            set { _itemUnitPrice = value; }
        }

        private int _itemQuantity;
        public int ItemQuantity
        {
            get { return _itemQuantity; }
            set { _itemQuantity = value; }
        }

        private string _consumerName = null;
        public string ConsumerName
        {
            get { return _consumerName; }
            set { _consumerName = value; }
        }

        private string _consumerDept = null;
        public string ConsumerGroup
        {
            get { return _consumerDept; }
            set { _consumerDept = value; }
        }

        private string _consumerTeleExt = null;
        public string ConsumerTeleExt
        {
            get { return _consumerTeleExt; }
            set { _consumerTeleExt = value; }
        }

        private OrderDetailStatus _payStatus = OrderDetailStatus.None;
        public OrderDetailStatus PayStatus
        {
            get { return _payStatus; }
            set { _payStatus = value; }
        }

        public decimal Total
        {
            get { return this._itemQuantity * this._itemUnitPrice; }
        }
        #endregion

        #region .ctor
        public PayViewCartEntry() { }

        public PayViewCartEntry(string id)
        {
            _id = id;
        }

        public PayViewCartEntry(string id, Guid itemGuid, string itemName, decimal itemUnitPrice, int itemQuantity, string consumerName, string consumerDept, string consumerTeleExt, OrderDetailStatus payStatus)
        {
            _id = id;
            ItemGuid = itemGuid;
            _itemName = itemName;
            _itemUnitPrice = itemUnitPrice;
            _itemQuantity = itemQuantity;
            _consumerName = consumerName;
            _consumerDept = consumerDept;
            _consumerTeleExt = consumerTeleExt;
            _payStatus = payStatus;
        }

        public PayViewCartEntry(string id, Guid itemGuid ,int itemQuantity, string consumerName, string consumerTeleExt)
        {
            _id = id;
            ItemGuid = itemGuid;
            _itemQuantity = itemQuantity;
            _consumerName = consumerName;
            _consumerTeleExt = consumerTeleExt;
        }
        #endregion
    }

    public class PayViewCart : List<PayViewCartEntry>
    {
        public PayViewCart() : base() { }
        public PayViewCart(int capacity) : base(capacity) { }
    }
}

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.I18N;
using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    /// <summary>
    /// AvailableInformation.cs要在前台顯示時，對應_StoreInformation.cshtml的Model
    /// </summary>
    public class StoreInformation
    {
        private const string googleMapUrl = "https://maps.google.com.tw/maps?f=q&hl=zh-TW&z=16&t=m&iwloc=near&output=&q=";
        private const int urlLimitLength = 55;
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();


        public StoreInformation()
        {
        }

        public StoreInformation(AvailableInformation info)
        {
            this.Name = !string.IsNullOrEmpty(info.U) ? string.Format(@"<a href=""{0}"" target=""_blank"" style=""color:#BF0000;"">{1}</a>", info.U, info.N) : info.N;
            this.Phone = ParseContent(Phrase.Phone, info.P, false);

            this.Address = info.A;
            this.Longitude = info.Longitude;
            this.Latitude = info.Latitude;
            SetMapHtmlByAddressAndLongLat();

            this.SpecialRemarks = info.R;
            this.CloseDate = ParseContent(Phrase.CloseDate, info.CD, false);
            this.MRT = ParseContent(Phrase.Mrt, info.MR, false);
            this.Car = ParseContent(Phrase.Car, info.CA, false);
            this.Bus = ParseContent(Phrase.Bus, info.BU, false);
            this.OtherVehicle = ParseContent(Phrase.OtherVehicle, info.OV, false);
            this.WebSiteUrl = ParseContent(Phrase.Link, info.U, true);
            this.FacebookUrl = ParseContent(Phrase.FbFans, info.FB, true);
            this.BlogUrl = ParseContent(Phrase.BlogFans, info.BL, true);
            this.OtherUrl = ParseContent(Phrase.OtherFans, info.OL, true);
        }

        #region AvailableInformation的Property
        /// <summary>
        /// 店家名稱 (N)
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 電話 (P)
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 地址 (A)
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 經度
        /// </summary>
        public string Longitude { get; set; }
        /// <summary>
        /// 緯度
        /// </summary>
        public string Latitude { get; set; }
        /// <summary>
        /// 開店時間 (OT)
        /// </summary>
        public string OpeningTime { get; set; }
        /// <summary>
        /// 開店天 (OD)
        /// </summary>
        public string OpeningDay { get; set; }
        /// <summary>
        /// 使用時間 (UT)
        /// </summary>
        public string UseTime { get; set; }
        /// <summary>
        /// 網站網址 (U)
        /// </summary>
        public string WebSiteUrl { get; set; }
        /// <summary>
        /// 特殊標記 (R)
        /// </summary>
        public string SpecialRemarks { get; set; }
        /// <summary>
        /// Close Date (CD)
        /// </summary>
        public string CloseDate { get; set; }
        /// <summary>
        /// 捷運 (MR)
        /// </summary>
        public string MRT { get; set; }
        /// <summary>
        /// Car
        /// </summary>
        public string Car { get; set; }
        /// <summary>
        /// Bus
        /// </summary>
        public string Bus { get; set; }
        /// <summary>
        /// Other Vehicles
        /// </summary>
        public string OtherVehicle { get; set; }
        /// <summary>
        /// Facebook網址 (FB)
        /// </summary>
        public string FacebookUrl { get; set; }
        /// <summary>
        /// 噗浪網址 (PL)
        /// </summary>
        public string PlurkUrl { get; set; }
        /// <summary>
        /// 部落格網址 (BL)
        /// </summary>
        public string BlogUrl { get; set; }
        /// <summary>
        /// Other Url
        /// </summary>
        public string OtherUrl { get; set; }

        public bool HasLongLat
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Longitude) ||
                    Longitude.Equals("null", StringComparison.OrdinalIgnoreCase) ||
                    Longitude.Equals("0") || string.IsNullOrWhiteSpace(Latitude) ||
                    string.IsNullOrWhiteSpace(Latitude) ||
                    Latitude.Equals("null", StringComparison.OrdinalIgnoreCase) ||
                    Latitude.Equals("0"))
                {
                    return false;
                }
                return true;
            }
        }
        #endregion

        /// <summary>
        /// 可能是舊的顯示地圖的方法，註解掉觀察是否有使用到
        /// </summary>
        //public string MapDiv
        //{
        //    get
        //    {
        //        return "<div id='map' address='" + this.Address + "' longitude='" + this.Longitude + "' latitude='" + this.Latitude + "' style='width:209px;height:209px;margin-left: -12px;'></div>";
        //    }
        //    set { this.MapDiv = value; }
        //}

        public string MapHtml { get; set; }

        public List<StoreInformation> GetStoreInformationListByAvailability(string availability)
        {
            var list = new List<StoreInformation>();
            try
            {
                var availiableInfoList = new JsonSerializer().Deserialize<List<AvailableInformation>>(availability);
                foreach (var availiableInfo in availiableInfoList)
                {
                    list.Add(new StoreInformation(availiableInfo));
                }
            }
            catch (Exception)
            {
                //ILog logger = LogManager.GetLogger("Troubleshooting");
                //logger.Info(String.Format("Exception occured on deserialize List<AvailableInformation>:\nViewPponDeal = {0}", vpd), ex);
            }
            return list;
        }

        /// <summary>
        /// 根據店家地址、經緯度資訊，組成店家地址要顯示的文字 (含Map的連結)
        /// </summary>
        private void SetMapHtmlByAddressAndLongLat()
        {
            var mapHtml = string.Empty;

            if (config.GoogleMapInDefaultPage)
            {
                if (this.HasLongLat)
                {
                    mapHtml = string.Format("{0}{1}<a target='_blank' href='{2}{3},{4}' class='map_btn' style='background: rgba(0, 0, 0, 0) url(\"https://www.17life.com/Themes/default/images/17Life/G2/newdealdetail.png\") no-repeat scroll -420px -30px;display: inline-block;height: 20px;margin-left: 5px;text-indent: -9999px;width: 36px;'>&nbsp;</a>",
                        Phrase.Address + Phrase.Colon, this.Address, googleMapUrl, this.Latitude, this.Longitude);
                }
                else if (string.IsNullOrEmpty(this.Address) == false)
                {
                    mapHtml = string.Format("{0}{1}<a target='_blank' href='{2}{1}' class='map_btn' style='background: rgba(0, 0, 0, 0) url(\"https://www.17life.com/Themes/default/images/17Life/G2/newdealdetail.png\") no-repeat scroll -420px -30px;display: inline-block;height: 20px;margin-left: 5px;text-indent: -9999px;width: 36px;'>&nbsp;</a>",
                        Phrase.Address + Phrase.Colon, this.Address, googleMapUrl);
                }
            }
            else
            {
                var latitude = string.Empty;
                var longitude = string.Empty;
                var showMapJs = string.Empty;

                if (config.EnableOpenStreetMap)
                {
                    if (this.HasLongLat)
                    {
                        SqlGeography geo = LocationFacade.GetGeography(this.Address);
                        if (geo != null && !geo.IsNull)
                        {
                            if (!geo.Lat.IsNull && !geo.Long.IsNull)
                            {
                                latitude = geo.Lat.ToString();
                                longitude = geo.Long.ToString();
                            }
                        }
                    }
                    else
                    {
                        latitude = this.Latitude;
                        longitude = this.Longitude;
                    }
                }
                else
                {
                    latitude = this.Latitude;
                    longitude = this.Longitude;
                }
                
                if (!string.IsNullOrWhiteSpace(latitude) && !string.IsNullOrWhiteSpace(longitude))
                {
                    showMapJs = string.Format(" onclick='showmap(this, \"{0}\");' class=\"map_btn\" ",
                        config.EnableOpenStreetMap);
                }
                mapHtml = string.Format("{0}:{1}<a address='{1}' longitude='{2}' latitude='{3}' {4} style='cursor:pointer;'>&nbsp;</a>",
                    Phrase.Address, this.Address, longitude, latitude, showMapJs);
            }

            this.Address = mapHtml;
        }

        private string ParseContent(string phrase, string content, bool isUrl) {
            var result = string.Empty;
            if (!string.IsNullOrWhiteSpace(content))
            {
                if (isUrl)
                {
                    result = string.Format("{0}{1}{2}", phrase, Phrase.Colon, string.Format(@"<a href=""{0}"" target=""_blank"" style=""color:Blue;"">{1}</a>", content, (content.Length > urlLimitLength ? content.Substring(0, urlLimitLength) + "..." : content)));
                }
                else
                {
                    result = string.Format("{0}{1}{2}", phrase, Phrase.Colon, content);
                }
            }
            return result;
        }
        
        #region 處理View Render的Html
        /// <summary>
        /// 有使用時間顯示使用時間，否則顯示營業時間
        /// </summary>
        /// <returns></returns>
        public string GetUseTimeOrOpeningTime()
        {
            var showText = string.Empty;
            if (!string.IsNullOrWhiteSpace(this.UseTime))
            {
                showText = Phrase.UseTime + Phrase.Colon + this.UseTime;
            }
            else if (!string.IsNullOrEmpty(this.OpeningTime))
            {
                showText = Phrase.OpeningTime + Phrase.Colon + this.OpeningTime;
            }

            return showText;
        }
        #endregion
        
    }
}

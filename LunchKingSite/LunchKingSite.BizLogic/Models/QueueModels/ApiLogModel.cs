﻿using System;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.QueueModels
{
    
    public class ApiLogModel
    {
        public const string _QUEUE_NAME = "ApiLog";

        /// <summary>
        /// 函示名稱
        /// </summary>
        [JsonProperty("methodName")]
        public string MethodName { get; set; }
        /// <summary>
        /// 呼叫時間
        /// </summary>
        [JsonProperty("requestTime")]
        public DateTime RequestTime { get; set; }
        /// <summary>
        /// 呼叫此服務的apiUserId
        /// </summary>
        [JsonProperty("apiUserId")]
        public string ApiUserId { get; set; }
        /// <summary>
        /// 呼叫的參數
        /// </summary>
        [JsonProperty("parameter")]
        public object Parameter { get; set; }
        /// <summary>
        /// 回傳參數
        /// </summary>
        [JsonProperty("returnValue")]
        public object ReturnValue { get; set; }
        /// <summary>
        /// IP位址
        /// </summary>
        [JsonProperty("ip")]
        public string Ip { get; set; }
        /// <summary>
        /// 使用者的Id
        /// </summary>
        [JsonProperty("user_id")]
        public int? UserId { get; set; }
    }
}

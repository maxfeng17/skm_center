﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.QueueModels
{
    [Serializable]
    public class FamiVerifyPincodeMq
    {
        public const string _QUEUE_NAME = "FamiVerifyPincode";
        [JsonProperty("vid")]
        public int VerifyLogId { get; set; }
        [JsonProperty("pid")]
        public int PeztempId { get; set; }
        [JsonProperty("ono")]
        public int FamilyNetPincodeOrderNo { get; set; }
    }
}

﻿using System;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.QueueModels
{
    [Serializable]
    public class FamiApiLogMq
    {
        public const string _QUEUE_NAME = "FamiApiLog";
       
        [JsonProperty("famiApiLog")]
        public FamiApiLog FamiApiLog { get; set; }
    }
}

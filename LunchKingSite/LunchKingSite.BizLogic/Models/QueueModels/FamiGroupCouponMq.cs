﻿using System;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.QueueModels
{
    [Serializable]
    public class FamiGroupCouponMq
    {
        public const string _QUEUE_NAME = "FamiGroupCoupon";
        /// <summary>
        /// Peztemp.Code Encode
        /// </summary>
        [JsonProperty("pcode")]
        public string PeztempCode { get; set; }
        /// <summary>
        /// Order.Guid
        /// </summary>
        [JsonProperty("oid")]
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// cid
        /// </summary>
        [JsonProperty("cid")]
        public int CouponId { get; set; }
    }
}

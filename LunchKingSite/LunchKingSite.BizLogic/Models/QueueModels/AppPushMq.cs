﻿using System;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using System.Collections.Generic;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.BizLogic.Models.QueueModels
{
    [Serializable]
    public class AppPushMq
    {
        public const string _QUEUE_NAME = "AppPushBooking";

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("aepm")]
        public ActionEventPushMessage Aepm { get; set; }

        [JsonProperty("deviceInfo")]
        public UserDeviceInfoModel DeviceInfo { get; set; }

        [JsonProperty("messageData")]
        public Dictionary<string, object> MessageData { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}

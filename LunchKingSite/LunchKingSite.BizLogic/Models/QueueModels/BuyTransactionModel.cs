﻿using System;
using LunchKingSite.Core;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.QueueModels
{
    public class BuyTransactionModel
    {
        public const string _QUEUE_NAME = "BuyTransaction";

        [JsonProperty("guid")]
        public Guid Guid { get; set; }
        
        [JsonProperty("payment_method")]
        public int PaymentMethod { get; set; }

        [JsonProperty("payment_section")]
        public int PaymentSection { get; set; }
        
        [JsonProperty("create_time")]
        public DateTime CreateTime { get; set; }

        [JsonProperty("user_tracking_guid")]
        public Guid UserTrackingGuid { get; set; }

        [JsonProperty("order_guid")]
        public Guid? OrderGuid { get; set; }

        [JsonProperty("payment_result")]
        public int? PaymentResult { get; set; }

        public BuyTransactionModel(Guid buyTransactionGuid, BuyTransPaymentMethod paymentMethod,
            BuyTransPaymentSection paymentSection, Guid? orderGuid = null, int paymentResult = 0)
        {
            Guid = buyTransactionGuid;
            PaymentMethod = (int)paymentMethod;
            PaymentSection = (int)paymentSection;
            CreateTime = DateTime.Now;
            OrderGuid = orderGuid;
            PaymentResult = paymentResult;
        }
    }
}

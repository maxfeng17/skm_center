﻿using System;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.QueueModels
{
    [Serializable]
    public class UserTracking
    {
        public const string _QUEUE_NAME = "userTracking";

        [JsonProperty("guid")]
        public Guid Guid { get; set; }
        [JsonProperty("sess")]
        public string SessionId { get; set; }
        [JsonProperty("req")]
        public string Request { get; set; }
        [JsonProperty("q")]
        public string QueryString { get; set; }
        [JsonProperty("ty")]
        public string RequestType { get; set; }
        [JsonProperty("ref")]
        public string UrlReferrer { get; set; }
        [JsonProperty("ip")]
        public string UserIp { get; set; }
        [JsonProperty("start")]
        public DateTime StartTime { get; set; }
        [JsonProperty("agent")]
        public string UserAgent { get; set; }
        [JsonProperty("user")]
        public int UserId { get; set; }
        [JsonProperty("elap")]
        public double ElapsedSecs { get; set; }
        [JsonProperty("srvby")]
        public string ServerBy { get; set; }
        [JsonProperty("rsrc")]
        public string ReferrerSourceId { get; set; }
        [JsonProperty("device")]
        public int DeviceType { get; set; }
        [JsonProperty("visitorIdentity")]
        public Guid? VisitorIdentity { get; set; }
        [JsonProperty("bid")]
        public Guid? Bid { get; set; }
        [JsonProperty("https")]
        public bool? IsHttps { get; set; }
    }
}

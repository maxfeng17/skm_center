﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Models.FamiPort;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.QueueModels
{
    [Serializable]
    public class FamiReturnPincodeMq
    {
        public const string _QUEUE_NAME = "FamiReturnPincode";
        [JsonProperty("Bid")]
        public Guid Bid { get; set; }
        [JsonProperty("Og")]
        public Guid OrderGuid { get; set; }
    }
}

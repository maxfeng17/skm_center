﻿using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Models.PchomeWarehouse
{
    /// <summary>
    /// 庫存
    /// </summary>
    public class WmsInventory
    {
        public string Id { get; set; }
        public string VendorPId { get; set; }
        public string Name { get; set; }
        public string Spec { get; set; }
        public List<InventoryStatus> Status { get; set; }

        public class InventoryStatus
        {
            /// <summary>
            /// 訊息
            ///   Checking 待確認
            ///   Good_OnSale 可賣
            ///   Good_Waiting 待上架
            ///   Bad 壞品
            ///   Lost 遺失
            ///   Refund 驗退待還
            /// </summary>
            public string Message { get; set; }
            public int Qty { get; set; }

            public override string ToString()
            {
                return string.Format("{0}: {1}", Message, Qty);
            }
        }

        public override string ToString()
        {
            return string.Format("{0} {1}{2}", Id, Name, string.IsNullOrEmpty(Spec) ? string.Empty : " " + Spec);
        }
    }
}

﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Wms
{
    /// <summary>
    /// 目前出貨進度
    /// </summary>
    public class CurrentWmsOrderProgress
    {
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// wms(Pchome) order id
        /// </summary>
        public string WmsOrderId { get; set; }
        /// <summary>
        /// 撿貨單號
        /// </summary>
        public string PickId { get; set; }
        /// <summary>
        /// 送貨時間
        /// </summary>
        public DateTime? ShipDate { get; set; } 
        /// <summary>
        /// 目前處理狀態
        /// </summary>
        public int CurrentProgressStatus { get; set; }
        /// <summary>
        /// 狀態發生時間
        /// </summary>
        public DateTime? CurrentProgressDate { get; set; } 

        /// <summary>
        /// 已收貨
        /// </summary>
        public bool? IsReceipt { get; set; }

        /// <summary>
        /// 收貨時間
        /// </summary>
        public DateTime? ReceiptDate { get; set; }

        /// <summary>
        /// 出貨物流資訊
        /// </summary>
        public List<WmsLogistic> Logistic { get; set; }
    }
}

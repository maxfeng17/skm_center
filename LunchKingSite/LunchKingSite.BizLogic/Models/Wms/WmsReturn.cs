﻿using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Models.Wms
{
    /// <summary>
    /// 還貨
    /// </summary>
    public class WmsReturn
    {
        /// <summary>
        /// 還貨單編號
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 還貨明細
        /// </summary>
        public List<ReturnDetail> Detail { get; set; }
        /// <summary>
        /// 供應商ID
        /// </summary>
        public string SupplyId { get; set; }
        /// <summary>
        /// 倉庫聯絡人(取件)
        /// </summary>
        public WmsPerson WarehouseContact { get; set; }
        /// <summary>
        /// 收件聯絡人
        /// </summary>
        public WmsPerson ReceiveContact { get; set; }
        /// <summary>
        /// 是否自取
        /// </summary>
        public int isSelfPick { get; set; }
        /// <summary>
        /// 退貨申請時間
        /// </summary>
        public string ApplyDate { get; set; }
        /// <summary>
        /// 備貨完成時間
        /// </summary>
        public string ReadyDate { get; set; }
        /// <summary>
        /// 出貨時間
        /// </summary>
        public string ShipDate { get; set; }
        /// <summary>
        /// 退貨狀態
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string Error { get; set; }

        public override string ToString()
        {
            return Id;
        }
        /// <summary>
        /// 還貨明細
        /// </summary>
        public class ReturnDetail
        {
            /// <summary>
            /// 還貨單序號
            /// </summary>
            public string No { get; set; }
            /// <summary>
            /// 商品ID
            /// </summary>
            public string ProdId { get; set; }
            /// <summary>
            /// 還貨數量
            /// </summary>
            public int Qty { get; set; }
            /// <summary>
            /// 還貨備註
            /// </summary>
            public string Remark { get; set; }
            /// <summary>
            /// 還貨原因
            ///   VendorApply:廠商申請
            ///   Expired:有效期限到期
            ///   BadGoods:後來發現的壞品
            ///   PurchaseReturn:不符進倉規則 (驗退)
            ///   Other:其他
            /// </summary>
            public string Reason { get; set; }

            public override string ToString()
            {
                return string.Format("{0} {1}", this.ProdId, this.Qty);
            }
        }
    }
    /// <summary>
    /// 新增還貨
    /// </summary>
    public class WmsAddReturn
    {
        /// <summary>
        /// 還貨單編號
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 還貨明細
        /// </summary>
        public ReturnDetail Detail { get; set; }
        /// <summary>
        /// 供應商ID
        /// </summary>
        public string SupplyId { get; set; }
        /// <summary>
        /// 收件聯絡人
        /// </summary>
        public WmsPerson ReceiveContact { get; set; }
        /// <summary>
        /// 是否自取
        /// </summary>
        public int isSelfPick { get; set; }
         /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string Error { get; set; }
        public override string ToString()
        {
            return Id;
        }

        /// <summary>
        /// 還貨明細
        /// </summary>
        public class ReturnDetail
        {
            /// <summary>
            /// 商品ID
            /// </summary>
            public string ProdId { get; set; }
            /// <summary>
            /// 還貨數量
            /// </summary>
            public int Qty { get; set; }
            /// <summary>
            /// 還貨備註
            /// </summary>
            public string Remark { get; set; }
            public override string ToString()
            {
                return string.Format("{0} {1}", this.ProdId, this.Qty);
            }
        }
    }
    /// <summary>
    /// 還貨清單
    /// </summary>
    public class WmsReturnQueryList
    {
        public int TotalRows { get; set; }
        public List<ReturnRow> Rows { get; set; }
        public class ReturnRow
        {
            public string Id { get; set; }
        }
    }
}

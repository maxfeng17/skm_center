﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Wms
{
    public class WmsOrderHistoryModel
    {
        /// <summary>
        /// 發生時間
        /// </summary>
        [JsonProperty("deliveryTime")]
        public DateTime DeliveryTime { get; set; }
        /// <summary>
        /// 狀態
        /// </summary>
        [JsonProperty("delvieryDesc")]
        public string DelvieryDesc { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Models.Wms
{
    /// <summary>
    /// 出貨訂單
    /// </summary>
    public class WmsOrder
    {
        public string Id { get; set; }
        public string VendorOrderId { get; set; }
        public List<OrderDetail> Detail { get; set; }
        public WmsPerson ReceiveContact { get; set; }
        public string CreateDate { get; set; }
        public string ShipDate { get; set; }
        public string ShipStatus { get; set; }

        /// <summary>
        /// 出貨訂單明細
        /// </summary>
        public class OrderDetail
        {
            public string No { get; set; }
            public string VendorOrderNo { get; set; }
            public string ProdId { get; set; }
            public int Qty { get; set; }
            public string RefundStatus { get; set; }

            public override string ToString()
            {
                return string.Format("{0} * {1}", VendorOrderNo, Qty);
            }
        }

        public override string ToString()
        {
            return this.VendorOrderId;
        }
    }

    /// <summary>
    /// 出貨進度
    /// </summary>
    public class WmsOrderProgressStatus
    {
        public string OrderId { get; set; }
        /// <summary>
        /// 撿貨單號
        /// </summary>
        public string PickId { get; set; }
        /// <summary>
        /// 出貨進度
        /// </summary>
        public List<ItemProgressStatus> ProgressStatus { get; set; }
        /// <summary>
        /// 狀態資訊
        /// </summary>
        public class ItemProgressStatus
        {
            /// <summary>
            /// GotOrder:收到訂單
            /// Picking:撿貨
            /// Arranging:理貨
            /// Shipping:出貨
            /// Missed:送達未遇
            /// Arrived:已送達
            /// </summary>
            public string Message { get; set; }
            public string Date { get; set; }

            public override string ToString()
            {
                return this.Message;
            }
        }
        public override string ToString()
        {
            return this.OrderId;
        }
    }
    /// <summary>
    /// 出貨物流資訊
    /// </summary>
    public class WmsLogistic
    {
        /// <summary>
        /// 撿貨單號
        /// </summary>
        public string PickId { get; set; }
        /// <summary>
        /// 物流商名稱 (黑貓、大榮)
        /// </summary>
        public string LogisticName { get; set; }
        /// <summary>
        /// 宅配單號
        /// </summary>
        public string ShipId { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1}", LogisticName, PickId);
        }
    }
    /// <summary>
    /// 物流資訊
    /// </summary>
    public class WmsLogisticTrace
    {
        public string OrderId { get; set; }
        public int LogisticQty { get; set; }
        public List<LogisticTraceInfo> Info { get; set; }
        /// <summary>
        /// 配送資訊
        /// </summary>
        public class LogisticTraceInfo
        {
            /// <summary>
            /// 宅配單號
            /// </summary>
            public string ShipId { get; set; }
            /// <summary>
            /// 物流商名稱
            /// </summary>
            public string LogisticName { get; set; }
            /// <summary>
            /// 處理進度
            /// </summary>
            public List<LogisticTraceProgressStatus> ProgressStatus { get; set; }

            public override string ToString()
            {
                return string.Format("{0} {1}", this.LogisticName, this.ShipId);
            }
        }
        /// <summary>
        /// 處理進度
        /// </summary>
        public class LogisticTraceProgressStatus
        {
            /// <summary>
            /// 當地時間
            /// </summary>
            public string Date { get; set; }
            /// <summary>
            /// 進貨點
            /// </summary>
            public string Collection { get; set; }
            /// <summary>
            /// 狀態
            /// </summary>
            public string Message { get; set; }
            public override string ToString()
            {
                return this.Message;
            }
        }

        public override string ToString()
        {
            return this.OrderId;
        }
    }
}

﻿namespace LunchKingSite.BizLogic.Models.Wms
{
    public class WmsPurchaseOrder
    {
        public string Id { get; set; }
        public PurchaseOrderProd Prod { get; set; }
        public string SupplyId { get; set; }
        public string SupplyName { get; set; }
        public WmsPerson PurchaseContact { get; set; }
        public WmsPerson ReturnContact { get; set; }
        public WmsPerson WarehouseContact { get; set; }
        public string ExpireDate { get; set; }
        public string ArrivalDate { get; set; }
        public string EntryDate { get; set; }
        public int ActualQty { get; set; }
        public int RefundQty { get; set; }
        public string ApplyDate { get; set; }
        /// <summary>
        /// Applying:申請中
        /// ValidReceipt:有效單據
        /// Shipping:貨主出貨中
        /// Arrived:到貨待驗
        /// Inventory:進倉入庫
        /// Cancel:單據取消
        /// </summary>
        public string Status { get; set; }

        public class PurchaseOrderProd
        {
            public string Id { get; set; }
            public int Qty { get; set; }
            public int Price { get; set; }

            public override string ToString()
            {
                return string.Format("{0} {1}", Id, Qty);
            }
        }
    }
    public class PurchaseOrderProgressStatus
    {
        public string Message { get; set; }
        public int Qty { get; set; }
        public string Date { get; set; }
        public override string ToString()
        {
            return string.Format("{0}: {1}", Message, Qty);
        }
    }
}

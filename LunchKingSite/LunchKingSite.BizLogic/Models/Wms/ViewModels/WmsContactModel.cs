﻿using LunchKingSite.Core.Models.PponEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Wms.ViewModels
{
    public class WmsContactModel
    {
        public string AccountId { get; set; }

        public string ReturnerName { get; set; }

        public string ReturnerTel { get; set; }

        public string ReturnerMobile { get; set; }

        public string ReturnerEmail { get; set; }

        public string ReturnerZip { get; set; }

        public string ReturnerAddress { get; set; }

        public string WarehousingName { get; set; }

        public string WarehousingTel { get; set; }

        public string WarehousingMobile { get; set; }

        public string WarehousingEmail { get; set; }

        public string WarehousingZip { get; set; }

        public string WarehousingAddress { get; set; }
        public bool IsValid(out string message)
        {
            bool result = true;
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(WarehousingName))
            {
                sb.AppendLine("進倉『聯絡人』必需填寫");
            }
            if (string.IsNullOrEmpty(WarehousingTel))
            {
                sb.AppendLine("進倉『電話』必需填寫");
            }
            if (string.IsNullOrEmpty(WarehousingMobile))
            {
                sb.AppendLine("進倉『手機』必需填寫");
            }
            if (string.IsNullOrEmpty(WarehousingEmail))
            {
                sb.AppendLine("進倉『電子郵件』必需填寫");
            }
            if (string.IsNullOrEmpty(WarehousingZip))
            {
                sb.AppendLine("進倉『郵遞區號』必需填寫");
            }
            if (string.IsNullOrEmpty(WarehousingAddress))
            {
                sb.AppendLine("進倉『地址』必需填寫");
            }
            if (string.IsNullOrEmpty(ReturnerName))
            {
                sb.AppendLine("還貨『聯絡人』必需填寫");
            }
            if (string.IsNullOrEmpty(ReturnerTel))
            {
                sb.AppendLine("還貨『電話』必需填寫");
            }
            if (string.IsNullOrEmpty(ReturnerMobile))
            {
                sb.AppendLine("還貨『手機』必需填寫");
            }
            if (string.IsNullOrEmpty(ReturnerEmail))
            {
                sb.AppendLine("還貨『電子郵件』必需填寫");
            }
            if (string.IsNullOrEmpty(ReturnerZip))
            {
                sb.AppendLine("還貨『郵遞區號』必需填寫");
            }
            if (string.IsNullOrEmpty(ReturnerAddress))
            {
                sb.AppendLine("還貨『地址』必需填寫");
            }
            message = sb.ToString();
            if (sb.Length > 0)
            {
                result = false;
            }
            return result;
        }

        public WmsContact ConvertToWmsContact()
        {
            return new WmsContact
            {
                AccountId = this.AccountId,

            };
        }

        public static WmsContactModel Create(WmsContact contact, string accountId)
        {
            if (contact == null)
            {
                return new WmsContactModel
                {
                    AccountId = accountId
                };
            }
            return new WmsContactModel
            {
                AccountId = contact.AccountId,
                WarehousingName = contact.WarehousingName,
                WarehousingTel = contact.WarehousingTel,
                WarehousingMobile = contact.WarehousingMobile,
                WarehousingEmail = contact.WarehousingEmail,
                WarehousingZip = contact.WarehousingZip,
                WarehousingAddress = contact.WarehousingAddress,
                ReturnerName = contact.ReturnerName,
                ReturnerTel = contact.ReturnerTel,
                ReturnerMobile = contact.ReturnerMobile,
                ReturnerEmail = contact.ReturnerEmail,
                ReturnerZip = contact.ReturnerZip,
                ReturnerAddress = contact.ReturnerAddress,
            };
        }
    }
}

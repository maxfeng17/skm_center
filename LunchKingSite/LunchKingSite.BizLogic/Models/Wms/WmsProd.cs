﻿using Newtonsoft.Json;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Wms
{
    /// <summary>
    /// 商品
    /// </summary>
    public class WmsProd
    {
        public string Id { get; set; }
        public string GroupId { get; set; }
        public string VendorPId { get; set; }
        public string Name { get; set; }
        public string Spec { get; set; }                
        public decimal? Cost { get; set; }
        public decimal? Price { get; set; }
        public int isHaveExpiryDate { get; set; }
        public int? ShelfLife { get; set; }
        public int isSpec { get; set; }
        public string CreateDate { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}{2}", Id, Name, string.IsNullOrEmpty(Spec) ? string.Empty : " " + Spec);
        }
    
    }


    public class WmsProdSpec
    {
        public string Id { get; set; }
        public string GroupId { get; set; }
        public string VendorPId { get; set; }
        public string Spec { get; set; }

        public override string ToString()
        {
            return this.Id;
        }
    }
}

﻿using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Models.Wms
{
    /// <summary>
    /// 退貨
    /// </summary>
    public class WmsRefund
    {
        /// <summary>
        /// 退貨單編號
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 退貨明細
        /// </summary>
        public List<RefundDetail> Detail { get; set; }
        /// <summary>
        /// 退貨聯絡人(取件)
        /// </summary>
        public WmsPerson RefundContact { get; set; }
        /// <summary>
        /// 收件聯絡人(來源廠商)
        /// </summary>
        public WmsPerson ReceiveContact { get; set; }
        /// <summary>
        /// 是否收到商品
        /// </summary>
        public int isReceived { get; set; }
        /// <summary>
        /// 是否需派送物流
        /// </summary>
        public int isLogistic { get; set; }
        /// <summary>
        /// 退貨成立時間
        /// </summary>
        public string RefundDate { get; set; }
        /// <summary>
        /// 退貨狀態
        ///   Init:初始
        ///   Examine:退貨成立
        ///   Processing:退貨處理中
        ///   Delivering:取件配送中
        ///   Arrived:退貨配達(已退回)
        ///   Fail:取不到貨
        ///   NotRefund:客人不退貨
        /// </summary>
        public string State { get; set; }

        public override string ToString()
        {
            return this.OrderId;
        }

        public class RefundDetail
        {
            /// <summary>
            /// 退貨單序號
            /// </summary>
            public string No { get; set; }
            /// <summary>
            /// 訂單序號
            /// </summary>
            public string OrderNo { get; set; }
            /// <summary>
            /// 產品ID
            /// </summary>
            public string ProdId { get; set; }
            /// <summary>
            /// 退貨數量
            /// </summary>
            public int Qty { get; set; }
            /// <summary>
            /// 退貨物流單號
            /// </summary>
            public string RefundShipId { get; set; }

            public override string ToString()
            {
                return this.ProdId;
            }
        }
    }

    /// <summary>
    /// 退貨逆物流資訊
    /// </summary>
    public class WmsRefundTrace
    {
        /// <summary>
        /// 退貨單編號
        /// </summary>
        public string RefundId { get; set; }
        /// <summary>
        /// 配送箱數
        /// </summary>
        public int LogisticQty { get; set; }
        /// <summary>
        /// 配送資訊
        /// </summary>
        public List<LogisticTraceInfo> Info { get; set; }
        /// <summary>
        /// 配送資訊
        /// </summary>
        public class LogisticTraceInfo
        {
            /// <summary>
            /// 宅配單號
            /// </summary>
            public string ShipId { get; set; }
            /// <summary>
            /// 物流商名稱
            /// </summary>
            public string LogisticName { get; set; }
            /// <summary>
            /// 處理進度
            /// </summary>
            public List<LogisticTraceProgressStatus> ProgressStatus { get; set; }

            public override string ToString()
            {
                return string.Format("{0} {1}", this.LogisticName, this.ShipId);
            }
        }
        /// <summary>
        /// 處理進度
        /// </summary>
        public class LogisticTraceProgressStatus
        {
            /// <summary>
            /// 當地時間
            /// </summary>
            public string Date { get; set; }
            /// <summary>
            /// 進貨點
            /// </summary>
            public string Collection { get; set; }
            /// <summary>
            /// 狀態
            /// </summary>
            public string Message { get; set; }
            public override string ToString()
            {
                return this.Message;
            }
        }

        public override string ToString()
        {
            return this.RefundId;
        }
    }
}

﻿using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace LunchKingSite.BizLogic.Model.VBS
{
    class VBSModel
    {
    }

    public class VerifyCode
    {
        public string CouponSerial { get; set; }
        public string CouponCode { get; set; }
        public Guid TrustId { set; get; }
    }


    public class VBSHomeModel
    {
        public VBSHomeModel()
        {
            SellerUserId = 0;
            VBSName = "ErrorString";
        }
        public string VBSName { get; set; }
        public int SellerUserId { get; set; }
        public List<StoreDetail> StoeList { get; set; }
        public string SellerImageUrl { get; set; }
        public int GroupId { get; set; }
        public List<CardGroupInfo> CardGroupList { get; set; }
        public int VbsMemberStatus { get; set; }
        /// <summary>
        /// 累積會員人次的折線圖資料
        /// </summary>
        public LineChart TotalMemberLineChart { get; set; }
        /// <summary>
        /// 每日人次的折線圖資料
        /// </summary>
        public LineChart DailyNewMemberLineChart { get; set; }
    }
    public class CardGroupInfo
    {
        public int CardGroupId { get; set; }
        public string CardGroupName { get; set; }
    }

    public class LineChart
    {
        public LineChart()
        {
            DataList = new List<LineChartPoint>();
        }

        /// <summary>
        /// 說明
        /// </summary>
        public string Description { get; set; }

        public List<LineChartPoint> DataList { get; set; }
    }

    public class LineChartPoint
    {
        public DateTime XAxisDate { get; set; }
        public int YAxisNumber { get; set; }
    }

    public class StoreDetail
    {
        public string StoreName { get; set; }
        public string StoreGuid { get; set; }
    }

    public class VBSQueryModel 
    {
        public int SellerUserId { get; set; }
        public int GroupId { get; set; }
        public string DefaultStoreGuid { get; set; }

        public bool IsValid()
        {
            return true;
        }
    }

    /// <summary>
    /// for mobile app trasnfer Vbs VerifyCoupon data
    /// </summary>
    public class VbsVerifyCheckCouponReplyData
    {
        public VbsApiCouponReturnType VbsApiCouponReturnType { get; set; }
        public IEnumerable<VbsApiVerifyCouponInfo> VbsVerifyCoupons { get; set; }
        public string Message { get; set; }

        public VbsVerifyCheckCouponReplyData()
        {
            this.VbsApiCouponReturnType = VbsApiCouponReturnType.NoneVaileCoupon;
            this.VbsVerifyCoupons = null;
        }
    }

    /// <summary>
    /// 可核銷憑證資訊加一個已過使用期限卻仍在可核銷時限區間內的Boolean值
    /// </summary>
    public class VbsApiVerifyCouponInfo
    {
        public string DealName { get; set; }
        public string DealId { get; set; }
        public BusinessModel DealType { get; set; }
        public string CouponId { get; set; }
        public string CouponCode { get; set; }
        public string ExchangePeriodStartTime { get; set; }
        public string ExchangePeriodEndTime { get; set; }
        public VbsCouponVerifyState VerifyState { get; set; }
        public string ReturnDateTime { get; set; }
        public string PayerName { get; set; }
        public string TrustId { get; set; }
        public bool IsDuInTime { get; set; }
        public bool IsCouponAvailable { get; set; }
        public string ErrMessage { get; set; }

    }

    public class MultiCouponVerifyResult
    {
        public List<ApiResult> VerifyDetail { get; set; }
    }

    public class VbsShipOrderQueryData
    {
        public string queryBeginDate { get; set; }
        public string queryEndDate { get; set; }
        public string queryOrderId { get; set; }
        public string queryDealId { get; set; }
        public int? queryProcessStep { get; set; }
        public VbsShipQueryOption queryOption { get; set; }
        public string queryOptionValue { get; set; }
        public int? pageIndex { get; set; }
        public int? maxPage { get; set; }
    }

    public enum VbsShipQueryOption
    {
        [Description("全部")]
        All = 0,
        [Description("檔號")]
        DealId = 1,
        [Description("訂單編號")]
        OrderId = 2,
        [Description("收件人姓名")]
        Name = 3,
        [Description("收件人電話")]
        Mobile = 4
    }

}
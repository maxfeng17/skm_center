﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model
{
    /// <summary>
    /// 供合作廠商透過API取得檔次資料的內容物件
    /// 紀載單一檔次內容。
    /// </summary>
    public class DealInfoTranslate
    {
        /// <summary>
        /// 好康區域
        /// </summary>
        public List<DealAreaInfo> Area;
        /// <summary>
        /// 好康類別
        /// </summary>
        public List<DealCategory> Category;
        /// <summary>
        /// 好康類型
        /// </summary>
        public string DealType;
        /// <summary>
        /// 好康類型說明
        /// </summary>
        public string DealTypeDesc;
        /// <summary>
        /// 好康唯一代碼值
        /// </summary>
        public int DealId;
        /// <summary>
        /// 好康名稱
        /// </summary>
        public string Title;
        /// <summary>
        /// 好康說明
        /// </summary>
        public string Description;
        /// <summary>
        /// 主視覺方形圖片
        /// </summary>
        public string SquareImagePath;
        /// <summary>
        /// 主圖片
        /// </summary>
        public List<string> PhotoUrls;
        ///// <summary>
        ///// 小圖網址
        ///// </summary>
        //public string SmallPhotoUrl;
        /// <summary>
        /// 好康網址
        /// </summary>
        public string DealUrl;
        /// <summary>
        /// 馬上買網址
        /// </summary>
        public string BuyNowUrl;
        /// <summary>
        /// 原價
        /// </summary>
        public decimal OriginPrice;
        /// <summary>
        /// 售價
        /// </summary>
        public decimal SellPrice;
        /// <summary>
        /// 折扣字串
        /// </summary>
        public string Discount;
        /// <summary>
        /// 上限
        /// </summary>
        public decimal Total;
        /// <summary>
        /// 已銷售數量
        /// </summary>
        public int OrderedQuantity;
        /// <summary>
        /// 成單門檻
        /// </summary>
        public decimal QuantityMin;
        /// <summary>
        /// 開始販售時間 格式:yyyy-MM-ddTHH:mm:ss
        /// </summary>
        public string DealStartTime;
        /// <summary>
        /// 結束時間 格式:yyyy-MM-ddTHH:mm:ss
        /// </summary>
        public string DealEndTime;
        /// <summary>
        /// 詳細使用說明
        /// </summary>
        public string Restriction;
        /// <summary>
        /// 分店
        /// </summary>
        public List<DealStore> Stores;
        /// <summary>
        /// 商品名稱短title
        /// </summary>
        public string ShortTitle;
        public DealInfoTranslate()
        {
            Area = new List<DealAreaInfo>();
            PhotoUrls = new List<string>();
            Stores = new List<DealStore>();
            Category = new List<DealCategory>();
        }
    }

    public class DealAreaInfo
    {
        public int Id;
        public string Name;
        public string Code;
    }

    public class DealCategory
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }

    public class DealStore
    {
        /// <summary>
        /// 店名
        /// </summary>
        public string StoreName;
        /// <summary>
        /// 地址
        /// </summary>
        public string Address;
        /// <summary>
        /// 電話
        /// </summary>
        public string Phone;
        /// <summary>
        /// 營業時間
        /// </summary>
        public string BusinessHours;
        /// <summary>
        /// 網站
        /// </summary>
        public string WebUrl;
        /// <summary>
        /// 交通資訊
        /// </summary>
        public string TrafficInfo;
    }

    public class DealInfoTranslateSaleStatus
    {
        /// <summary>
        /// 好康唯一值
        /// </summary>
        public int DealId { get; set; }
        /// <summary>
        /// 銷售數量
        /// </summary>
        public int OrderedQuantity { get; set; }
        /// <summary>
        /// 好康狀況
        /// off=>未開始
        /// on=>開始
        /// finish=>結束
        /// </summary>
        public string DealStatus { get; set; }
        /// <summary>
        /// 銷售狀況
        /// less=>未成團
        /// success=>已成團
        /// finish=>已售完
        /// </summary>
        public string SaleStatus { get; set; }
    }
}

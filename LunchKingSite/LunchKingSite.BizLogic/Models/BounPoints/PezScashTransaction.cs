﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.BounPoints
{
    public class PezScashTransaction
    {
        public string Operation { get; set; }
        public int DepositId { get; set; }
        public int? WithdrawalId { get; set; }
        public string PezAuthCode { get; set; }        
        public string Title { get; set; }
        public decimal Amount { get; set; }
        public string Creator { get; set; }
        public DateTime TheTime { get; set; }
        public decimal Balance { get; set; }
    }
}

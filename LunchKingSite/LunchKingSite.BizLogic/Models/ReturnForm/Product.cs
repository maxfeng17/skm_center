﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model.Refund
{
    public class Product
    {
        private readonly int _orderProductId;
        public int OrderProductId
        {
            get { return _orderProductId; }
        }

        private readonly string _spec;
        /// <summary>
        /// 組合起來的選項規格 
        /// ex. (紅色,L)
        /// 沒有選項時, 回傳空字串.
        /// </summary>
        public string Spec
        {
            get { return _spec; }
        }

        private readonly List<string> _specOptions;
        /// <summary>
        /// 選項規格
        /// </summary>
        public IList<string> SpecOptions
        {
            get { return _specOptions.AsReadOnly(); }
        }

        private readonly bool _isOriginal;
        /// <summary>
        /// 購買當時的商品 (for 換貨)
        /// </summary>
        public bool IsOriginal
        {
            get { return _isOriginal; }
        }


        private readonly bool _isCurrent;
        /// <summary>
        /// 訂單目前的商品
        /// </summary>
        public bool IsCurrent
        {
            get { return _isCurrent; }
        }

        private readonly bool _isReturning;
        /// <summary>
        /// 退貨中的商品
        /// </summary>
        public bool IsReturning
        {
            get { return _isReturning; }
        }

        private readonly bool _isReturned;
        /// <summary>
        /// 已退貨的商品
        /// </summary>
        public bool IsReturned
        {
            get { return _isReturned; }
        }

        private readonly bool _isExchanging;
        /// <summary>
        /// 換貨中的商品
        /// </summary>
        public bool IsExchanging
        {
            get { return _isExchanging; }
        }

        #region .ctors

        internal Product(OrderProduct dbProd, IEnumerable<OrderProductOption> productOptions, PponOptionCollection allPponOptions)
        {
            _orderProductId = dbProd.Id;
            this._isCurrent = dbProd.IsCurrent;
            this._isOriginal = dbProd.IsOriginal;
            this._isReturning = dbProd.IsReturning;
            this._isReturned = dbProd.IsReturned;
            this._isExchanging = dbProd.IsExchanging;

            _specOptions = new List<string>();
            List<OrderProductOption> options = productOptions.OrderBy(x => x.OptionId).ToList();
            int optionCount = options.Count;

            if (int.Equals(0, optionCount))
            {
                _spec = string.Empty;
                return;
            }

            //舊資料頂多轉入一筆選項
            if (int.Equals(1, optionCount) && options.First().IsOldProduct)
            {
                string oldOption = options.First().OldProductOptions;
                _spec = oldOption;
                _specOptions.Add(oldOption);
                return;
            }

            List<PponOption> pponOptions = allPponOptions
                .OrderBy(x => x.CatgSeq)
                .ThenBy(x => x.OptionSeq)
                .ToList();

            foreach (PponOption pponOption in pponOptions)
            {
                if (options.Any(x => x.OptionId == pponOption.Id))
                {
                    _specOptions.Add(pponOption.OptionName);
                }
            }

            if (_specOptions.Count > 0)
            {
                _spec = string.Format("({0})", string.Join(",", _specOptions));
            }
            else
            {
                _spec = string.Empty;
            }
        }

        internal Product(OrderProduct dbProd, IEnumerable<OrderProductOption> productOptions)
        {
            //品生活
        }

        

        #endregion .ctors
    }
}

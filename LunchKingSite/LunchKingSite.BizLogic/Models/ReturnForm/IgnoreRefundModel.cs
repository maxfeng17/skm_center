﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.ReturnForm
{
    public class IgnoreRefundModel
    {
        /// <summary>
        /// 退貨單號
        /// </summary>
        public int ReturnFormId { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 訂單GUID
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string ItemName { get; set; }
    }
}

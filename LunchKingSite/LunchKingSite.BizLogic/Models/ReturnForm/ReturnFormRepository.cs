﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Transactions;

namespace LunchKingSite.BizLogic.Model.Refund
{
    public class ReturnFormRepository
    {
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IHiDealProvider _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISysConfProvider _config = ProviderFactory.Instance().GetProvider<ISysConfProvider>();
        static ILog logger = LogManager.GetLogger(typeof(ReturnFormRepository));

        #region query

        public static IList<ReturnFormEntity> FindAllByTrustId(Guid trustId)
        {
            var result = new List<ReturnFormEntity>();

            ReturnFormRefundCollection forms = _op.ReturnFormRefundGetList(trustId);
            foreach (ReturnFormRefund form in forms)
            {
                var formEntity = new ReturnFormEntity(form.ReturnFormId);
                result.Add(formEntity);
            }

            return result.OrderBy(x => x.CreateTime).ToList();
        }

        public static IList<ReturnFormEntity> FindAllByOrder(Guid orderGuid)
        {
            var result = new List<ReturnFormEntity>();        
            
            ReturnFormCollection forms = _op.ReturnFormGetListByOrderGuid(orderGuid);
            foreach (ReturnForm form in forms)
            {
                var formEntity =new ReturnFormEntity(form.Id);
                result.Add(formEntity);
            }

            return result.OrderBy(x => x.CreateTime).ToList();
        }

        public static ReturnFormEntity FindById(int returnFormId)
        {
            var result = new ReturnFormEntity(returnFormId);  
            return result;
        }

        public static IList<ReturnFormEntity> FindScheduledForRefunding()
        {
            var result = new List<ReturnFormEntity>();

            List<int> returnFormIds = _op.ReturnFormGetListForScheduledRefund();
            foreach (int formId in returnFormIds)
            {
                var formEntity = new ReturnFormEntity(formId);
                result.Add(formEntity);
            }

            return result;
        }

        /// <summary>
        /// 取得被列入忽略批次退貨的訂單
        /// </summary>
        /// <param name="returnForms"></param>
        /// <returns></returns>
        public static Dictionary<Guid, IViewPponDeal> GetIgnoreBatchRefundOrders(IList<ReturnFormEntity> returnForms)
        {
            var ignoreOrderDic = new Dictionary<Guid, IViewPponDeal>();

            //代銷憑證退貨訂單
            var orderGuids = returnForms.Where(x => x.CreateId == _config.TmallDefaultUserName && x.DeliveryType == DeliveryType.ToShop)
                .Select(x => x.OrderGuid).Distinct().ToList();
            var correspondingOrders = _op.OrderCorrespondingCollectionListGetByOrderGuidList(orderGuids);
            foreach (var co in correspondingOrders)
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(co.BusinessHourGuid);
                if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.PEZevent) || //檢查是否為廠商提供序號活動
                    Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) //成套票券
                )
                {
                    ignoreOrderDic.Add(co.OrderGuid, deal);
                }
            }

            return ignoreOrderDic;
        }

        #endregion query


        public static int Save(ReturnFormEntity form)
        {
            if (form.IsNew)
            {
                return Create(form);
            }
            else
            {
                return Update(form);
            }
        }

        private static int Create(ReturnFormEntity form)
        {
            string orderId;
            int result;

            if (!QueryOrderId(form, out orderId))
            {
                throw new InvalidDataException("Unable to find data for return form!");
            }


            var dbForm = new ReturnForm()
            {
                OrderId = orderId,
                OrderGuid = form.OrderGuid,
                BusinessModel = (int)form.BizModel,
                DeliveryType = (int)form.DeliveryType,
                ProgressStatus = (int)form.ProgressStatus,
                VendorProgressStatus = (int)form.VendorProgressStatus,
                RefundType = (int)form.RefundType,
                ReturnReason = form.ReturnReason,
                IsSystemRefund = form.IsSysRefund,
                Memo = form.Memo,
                IsCreditNoteReceived = false,
                CreateId = form.CreateId,
                CreateTime = DateTime.Now,
                ThirdPartyPaymentSystem = (byte)form.ThirdPartyPaymentSystem,
                OrderFromType = (int)Helper.GetOrderFromType(),
                CreditNoteType = form.CreditNoteType,
                ReceiverName = form.ReceiverName,
                ReceiverAddress = form.ReceiverAddress,
                IsReceive = form.IsReceive,
                NotifyChannel = form.IsNotifyChannel
            };
            
            OrderProductCollection prods = _op.OrderProductGetListByOrderGuid(form.OrderGuid);
            var updProds = new OrderProductCollection();
            foreach (ReturnFormProduct prod in form.ReturnFormProducts)
            {
                OrderProduct p = prods.First(x => x.Id == prod.OrderProductId);
                p.IsReturning = true;
                updProds.Add(p);
            }


            using (var scope = TransactionScopeBuilder.CreateReadCommitted())
            {
                _op.ReturnFormSet(dbForm);  //return_form
                _op.OrderProductSetList(updProds);  // order_product

                result = dbForm.Id;

                var returnFormProducts = new ReturnFormProductCollection();
                IEnumerable<ReturnFormProduct> products = form.ReturnFormProducts;
                foreach (var product in products)
                {
                    product.ReturnFormId = dbForm.Id;
                    returnFormProducts.Add(product);
                }
                _op.ReturnFormProductSetList(returnFormProducts);  // return_form_product


                var returnFormRefunds = new ReturnFormRefundCollection();
                List<Guid> trustIds = form.GetNewRefundTrustIds();
                foreach (var trustId in trustIds)
                {
                    var refund = new ReturnFormRefund();
                    refund.ReturnFormId = dbForm.Id;
                    refund.TrustId = trustId;
                    refund.VendorNoRefund = false;
                    refund.IsFreight = false;
                    returnFormRefunds.Add(refund);
                }

                Guid? freightTrustId = form.GetFreightTrustId();
                if (freightTrustId.HasValue)
                {
                    var refund = new ReturnFormRefund();
                    refund.ReturnFormId = dbForm.Id;
                    refund.TrustId = freightTrustId.Value;
                    refund.VendorNoRefund = false;
                    refund.IsFreight = true;
                    returnFormRefunds.Add(refund);
                }

                _op.ReturnFormRefundSetList(returnFormRefunds); //return_form_refund


                ReturnFormStatusLog log = MakeStatusLog(dbForm, dbForm.CreateId, dbForm.CreateTime);
                _op.ReturnFormStatusLogSet(log);    //return_form_status_log


                ViewOrderMemberBuildingSeller order = _op.ViewOrderMemberBuildingSellerGet(form.OrderGuid);
                //IsReceive=null表示憑證或非24類型
                int refundForm = (int)WmsRefundFrom.None;
                bool receive = form.IsReceive ?? false;
                if (receive)
                {
                    refundForm = (int)WmsRefundFrom.Customer;
                }
                string errMessage = string.Empty;
                bool isSuccess = WmsRefund(form.IsReceive, form.OrderGuid, dbForm.Id, refundForm, form.ReceiverName, order.OrderMobile, order.OrderMobile, order.UserEmail, form.ReceiverAddress, form.CreateId, returnFormProducts, out errMessage);
                if (!isSuccess)
                {
                    logger.Error("order_guid：" + form.OrderGuid.ToString() + " " + errMessage);
                    return 0;
                }
                scope.Complete();
            }

            return result;
        }

        public static bool WmsRefund(bool? isReceive,Guid orderGuid, int returnFormId,int refundFrom, string contactName, string contactTel, string contactMobile, string contactEmail,
                                      string contactAddress,string createId, ReturnFormProductCollection returnFormProducts, out string errMessage)
        {
            try
            {
                //PCHOME退貨單(已送至PC)
                errMessage = string.Empty;
                if (_config.WmsRefundEnable && (string.IsNullOrEmpty(_config.WmsRefundAccount) || (!string.IsNullOrEmpty(_config.WmsRefundAccount) && createId == _config.WmsRefundAccount)))
                {
                    if (isReceive != null)
                    {

                        //消費者收到商品 || 客服壓逆物流 || 出貨前狀態
                        bool receive = isReceive ?? false;
                        WmsOrder wo = _wp.WmsOrderGet(orderGuid);


                        if (wo.IsLoaded && wo.Status == (int)WmsOrderSendStatus.Success)
                        {
                            #region 前台消費者選擇沒收到貨且還在出貨前,直接送逆物流
                            if (!receive && refundFrom == (int)WmsRefundFrom.None)
                            {
                                //API
                                List<Models.Wms.WmsOrderProgressStatus> orderProgressStatus = PchomeWmsAPI.GetOrderProgressStatus(wo.WmsOrderId);
                                //test
                                if (!string.IsNullOrEmpty(_config.WmsProgressStatusAPI))
                                    orderProgressStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.Wms.WmsOrderProgressStatus>>(_config.WmsProgressStatusAPI);

                                if (!orderProgressStatus.Any())
                                {
                                    refundFrom = (int)WmsRefundFrom.Pchome;

                                    ReturnForm rf = _op.ReturnFormGet(returnFormId);
                                    rf.ProgressStatus = (int)ProgressStatus.RetrieveToPC;
                                    rf.VendorProgressStatus = (int)VendorProgressStatus.Retrieving;
                                    _op.ReturnFormSet(rf);
                                }
                                else
                                {
                                    int notShipCount = 0;
                                    foreach (Models.Wms.WmsOrderProgressStatus wmsprogress in orderProgressStatus)
                                    {
                                        List<Models.Wms.WmsOrderProgressStatus.ItemProgressStatus> ProgressStatus = wmsprogress.ProgressStatus.Where(x => x.Date != "").ToList();
                                        if (!ProgressStatus.Any(x => x.Message == "Shipping" || x.Message == "Missed" || x.Message == "Arrived"))
                                            notShipCount++;
                                    }
                                    //若多筆則皆符合才行
                                    if (orderProgressStatus.Count == notShipCount)
                                    {
                                        refundFrom = (int)WmsRefundFrom.Pchome;

                                        ReturnForm rf = _op.ReturnFormGet(returnFormId);
                                        rf.ProgressStatus = (int)ProgressStatus.RetrieveToPC;
                                        rf.VendorProgressStatus = (int)VendorProgressStatus.Retrieving;
                                        _op.ReturnFormSet(rf);
                                    }
                                    else
                                    {
                                        //沒收到貨但PC已出貨,不送逆物流走客服確認
                                    }
                                }

                                contactName = "邱欣儀";
                                contactTel = "(02)2700-0898 #3423";
                                contactMobile = "x";
                                contactEmail = "csdm@17life.com";
                                contactAddress = "333 桃園市龜山區民生北路一段386號";
                            }
                            #endregion

                            if (refundFrom != (int)WmsRefundFrom.None)
                            {
                                List<Models.Wms.WmsRefund.RefundDetail> list = new List<Models.Wms.WmsRefund.RefundDetail>();
                                ViewOrderMemberBuildingSeller order = _op.ViewOrderMemberBuildingSellerGet(orderGuid);
                                WmsContact contact = _wp.WmsContactGetBySellerGuid(order.SellerGuid);

                                //API
                                var wmsOrdersInfo = PchomeWmsAPI.GetOrder(wo.WmsOrderId).Detail;
                                //test
                                if (!string.IsNullOrEmpty(_config.WmsOrderAPI))
                                    wmsOrdersInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Wms.WmsOrder>(_config.WmsOrderAPI).Detail;

                                if (contact == null)
                                {
                                    errMessage = "建立PCHOME逆物流錯誤：商家聯絡資訊有誤";
                                    return false;
                                }

                                #region 查詢產品資訊
                                foreach (ReturnFormProduct rfp in returnFormProducts)
                                {
                                    int optionId = _op.OrderProductOptionGetListByOrderProductIds(new List<int>() { rfp.OrderProductId }).FirstOrDefault().OptionId;
                                    Guid itemGuid = _pp.PponOptionGet(optionId).ItemGuid ?? Guid.Empty;
                                    string pchomeProdId = _pp.ProductItemGet(itemGuid).PchomeProdId;
                                    string orderNo = "";
                                    Models.Wms.WmsOrder.OrderDetail orderDetail = wmsOrdersInfo.Where(x => x.ProdId == pchomeProdId).FirstOrDefault();
                                    if (orderDetail == null)
                                    {
                                        errMessage = "建立PCHOME逆物流錯誤：退貨找無相關產品資訊";
                                        return false;
                                    }
                                    else
                                    {
                                        orderNo = orderDetail.No;
                                    }

                                    Models.Wms.WmsRefund.RefundDetail oldDetail = list.Where(x => x.ProdId == pchomeProdId).FirstOrDefault();
                                    if (oldDetail == null)
                                    {
                                        Models.Wms.WmsRefund.RefundDetail detail = new Models.Wms.WmsRefund.RefundDetail();
                                        detail.OrderNo = orderNo;
                                        detail.ProdId = pchomeProdId;
                                        detail.Qty = 1;
                                        list.Add(detail);
                                    }
                                    else
                                    {
                                        oldDetail.Qty++;
                                    }

                                }
                                #endregion

                                #region 退貨領取地址
                                int zip = 0;
                                if (contactAddress.Length < 3)
                                {
                                    errMessage = "建立PCHOME逆物流錯誤：收件地址不正確";
                                    return false;
                                }
                                Int32.TryParse(contactAddress.Substring(0, 3), out zip);
                                if (zip == 0)
                                {
                                    City city = CityManager.CityGetByStringAddress(contactAddress);
                                    if (city == null)
                                    {
                                        errMessage = "建立PCHOME逆物流錯誤：找無對應郵遞區號";
                                        return false;
                                    }
                                    City district = CityManager.TownshipIdGetByStringAddress(city.Id, contactAddress);
                                    if (district == null)
                                    {
                                        errMessage = "建立PCHOME逆物流錯誤：找無對應郵遞區號";
                                        return false;
                                    }
                                    zip = Convert.ToInt32(district.ZipCode);
                                }
                                #endregion

                                Models.Wms.WmsRefund newRefund = new Models.Wms.WmsRefund
                                {
                                    OrderId = wo.WmsOrderId,
                                    Detail = list,
                                    RefundContact = new Models.Wms.WmsPerson
                                    {
                                        Name = contactName,
                                        Tel = contactTel,
                                        Mobile = contactMobile,
                                        Email = contactEmail,
                                        Zip = zip.ToString(),
                                        Address = contactAddress
                                    },
                                    ReceiveContact = new Models.Wms.WmsPerson
                                    {
                                        Name = contact.ReturnerName,
                                        Tel = contact.ReturnerTel,
                                        Mobile = contact.ReturnerMobile,
                                        Email = contact.ReturnerEmail,
                                        Zip = contact.ReturnerZip,
                                        Address = contact.ReturnerAddress
                                    },
                                    isReceived = Convert.ToInt32(receive)
                                };
                                bool refundAdded = PchomeWmsAPI.AddRefund(newRefund);

                                if (refundAdded)
                                {
                                    WmsRefundOrder rOrder = new WmsRefundOrder();
                                    rOrder.ReturnFormId = returnFormId;
                                    rOrder.PchomeRefundId = newRefund.Id;
                                    rOrder.RefundFrom = refundFrom;
                                    rOrder.IsReceive = receive;
                                    rOrder.Status = (int)WmsRefundStatus.Initial;
                                    rOrder.CreateId = createId;
                                    rOrder.CreateTime = DateTime.Now;
                                    _wp.WmsRefundOrderSet(rOrder);


                                    WmsRefundOrderStatusLog log = WmsFacade.MakeWmsRefundOrderStatusLog(rOrder, createId, DateTime.Now);
                                    _wp.WmsRefundOrderStatusLogSet(log);

                                    #region 寄發通知信
                                    MailToReturnSellerNotify(returnFormId, orderGuid, order);
                                    #endregion 寄發通知信
                                }
                                else
                                {
                                    errMessage = "建立PCHOME逆物流錯誤";
                                    return false;
                                }
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                errMessage = "建立PCHOME逆物流錯誤：" + ex.Message;
                logger.Error(ex);
                return false;
            }
            
        }

        private static void MailToReturnSellerNotify(
            int returnFormId, 
            Guid orderGuid, 
            ViewOrderMemberBuildingSeller order)
        {
            try
            {
                var vbsOrderReturn = _op.ViewOrderReturnFormListGet(returnFormId);
                string mailSubject = string.Format("<重要退貨>17Life【{0}】24H到貨/ 消費者退貨訂單通知", vbsOrderReturn.SellerName);
                Core.Component.Template.WmsReturnOrderNotificationToSeller.WmsReturnOrderListModel wmsReturnOrderListModel = new Core.Component.Template.WmsReturnOrderNotificationToSeller.WmsReturnOrderListModel();

                Dictionary<int, string> orderItemLists = _op.GetReturnOrderProductOptionDesc(new List<int> { returnFormId });
                var specAndQuanty = orderItemLists.ContainsKey(returnFormId) ? orderItemLists[vbsOrderReturn.ReturnFormId] : string.Empty;

                Order dbOrder = _op.OrderGet(orderGuid);
                wmsReturnOrderListModel.ItemList.Add(
                    new Core.Component.Template.WmsReturnOrderNotificationToSeller.WmsReturnOrderModel()
                    {
                        ReturnDate = DateTime.Now.ToString("yyyy/MM/dd"),
                        OrderId = dbOrder.OrderId,
                        UniqueId = vbsOrderReturn.ProductId.ToString(),
                        BuyerName = order.MemberName,
                        ItemName = vbsOrderReturn.DealName,
                        ReturnSpec = specAndQuanty,
                        ReturnReason = vbsOrderReturn.ReturnReason
                    });

                //確認商家收件人
                var checkContacts = CustomerServiceFacade.GetSellerContacts(vbsOrderReturn.SellerGuid, ProposalContact.ReturnPerson);
                if (checkContacts.Key)
                {
                    var mailTo = checkContacts.Value;

                    #region Send mail

                    var template = TemplateFactory.Instance().GetTemplate<Core.Component.Template.WmsReturnOrderNotificationToSeller>();
                    template.SellerName = vbsOrderReturn.SellerName;
                    template.SiteUrl = _config.SiteUrl;
                    template.HttpSiteUrl = _config.SiteUrl;
                    template.SiteServiceUrl = _config.SiteServiceUrl;
                    template.ReturnOrder = wmsReturnOrderListModel;
                    try
                    {
                        string[] mailList = mailTo.Split(",");
                        foreach (string m in mailList)
                        {
                            MailMessage msg = new MailMessage();
                            msg.Subject = string.Format(mailSubject, vbsOrderReturn.SellerName, DateTime.Now.ToString("yyyy/MM/dd"));
                            msg.From = new MailAddress(_config.NewReturnOrExchangepEmailAddress, _config.NewReturnOrExchangeEmailDisplayName);
                            msg.To.Add(m);
                            msg.Body = template.ToString();
                            msg.IsBodyHtml = true;
                            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                        }
                           
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                    }

                    #endregion
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

        }

        private static bool QueryOrderId(ReturnFormEntity form, out string orderId)
        {
            switch (form.BizModel)
            {
                case BusinessModel.Ppon:
                    var pponOrder = _pp.ViewPponOrderGet(form.OrderGuid);
                    if (pponOrder == null || !pponOrder.IsLoaded)
                    {
                        orderId = string.Empty;
                        return false;
                    }
                    orderId = pponOrder.OrderId;
                    return true;
                case BusinessModel.PiinLife:
                default:
                    HiDealOrder hiDealOrder = _hp.HiDealOrderGet(form.OrderGuid);
                    
                    if (hiDealOrder == null || !hiDealOrder.IsLoaded)
                    {
                        orderId = string.Empty;
                        return false;
                    }

                    orderId = hiDealOrder.OrderId;
                    return false;
            }
        }

        private static int Update(ReturnFormEntity form)
        {
            int result;

            using (var scope = TransactionScopeBuilder.CreateReadCommitted())
            {
                //檢查return_form有異動才須update
                if (CheckReturnFormIsChanged(form.ReturnForm))
                {
                    //return_form
                    result = _op.ReturnFormSet(form.ReturnForm);
                    //return_form_status_log
                    ReturnFormStatusLog log = MakeStatusLog(form.ReturnForm, form.ReturnForm.ModifyId, form.ReturnForm.ModifyTime.Value);
                    _op.ReturnFormStatusLogSet(log);
                }
                else
                {
                    result = 0;
                }

                //檢查return_form_product有異動才須update
                if (CheckReturnFormProductIsChanged(form.ReturnFormProducts))
                {
                    _op.ReturnFormProductSetList(form.ReturnFormProducts);
                    result = 1;
                }
                                
                //檢查return_form_refund有異動才須update
                if (CheckReturnRefundIsChanged(form.ReturnFormRefunds))
                {
                    _op.ReturnFormRefundSetList(form.ReturnFormRefunds);
                    result = 1;
                }               

                scope.Complete();
            }

            return result;
        }

        public static ReturnFormStatusLog MakeStatusLog(ReturnForm form, string modifyId, DateTime modifyTime)
        {
            if (form == null)
            {
                return null;
            }

            var result = new ReturnFormStatusLog
                {
                    ReturnFormId = form.Id,
                    ProgressStatus = form.ProgressStatus,
                    VendorProgressStatus = form.VendorProgressStatus,
                    VendorMemo = form.VendorMemo,
                    RefundType = form.RefundType,
                    IsCreditNoteReceived = form.IsCreditNoteReceived,
                    ModifyId = modifyId,
                    ModifyTime = modifyTime
                };

            return result;
        }

        private static bool CheckReturnFormIsChanged(ReturnForm newForm)
        {
            ReturnForm form = _op.ReturnFormGet(newForm.Id);

            if (!form.ProgressStatus.Equals(newForm.ProgressStatus) ||
                !form.VendorProgressStatus.Equals(newForm.VendorProgressStatus) ||
                form.VendorMemo != newForm.VendorMemo ||
                !form.RefundType.Equals(newForm.RefundType) ||
                !form.IsCreditNoteReceived.Equals(newForm.IsCreditNoteReceived))
            {
                return true;
            }

            return false;
        }

        private static bool CheckReturnFormProductIsChanged(ReturnFormProductCollection products)
        {
            return products.Any(x => x.IsDirty || x.IsNew);
                    }

        private static bool CheckReturnRefundIsChanged(ReturnFormRefundCollection refunds)
                    {
            return refunds.Any(x => x.IsDirty || x.IsNew);
        }
    }
}

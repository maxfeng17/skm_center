﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model.Refund
{
    public class PponOrder
    {
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        #region fields

        
        private readonly OrderProductCollection _allOrderProducts;
        private readonly OrderProductOptionCollection _allOrderProductOptions;

        private DealProperty _dealProperty = null;
        private DealProperty DealProperty
        {
            get
            {
                if (_dealProperty == null)
                {
                    _dealProperty = _pp.DealPropertyGet(Bid);
                }
                return _dealProperty;
            }
        }

        #endregion fields



        #region props

        private readonly Guid _orderGuid;
        public Guid OrderGuid
        {
            get { return _orderGuid; }
        }

        private Guid? _bid;
        public Guid Bid
        {
            get
            {
                if (!_bid.HasValue)
                {
                    ViewPponOrder vpo = _pp.ViewPponOrderGet(this.OrderGuid);
                    if (vpo != null && vpo.IsLoaded)
                    {
                        _bid = vpo.BusinessHourGuid;
                    }
                    else
                    {
                        _bid = Guid.Empty;
                    } 
                }
                
                return _bid.Value;
            }
        }
        
        private readonly List<Product> _products;
        public IList<Product> Products 
        {
            get
            {
                return _products
                    .Where(x => x.IsCurrent)
                    .ToList().AsReadOnly();
            }
        }

        public OrderProductCollection AllOrderProducts
        {
            get { return _allOrderProducts.Clone(); }
        }

        #endregion props


        #region public methods
        

        /// <summary>
        /// 取得宅配的商品明細
        /// </summary>
        /// <returns></returns>
        public IList<SummarizedProductSpec> GetToHouseProductSummary()
        {
            return _products
                .GroupBy(x => x.Spec)
                .Select(g => new SummarizedProductSpec
                    {
                        Spec = g.Key,
                        ReturnableCount =
                            g.Count(x => x.IsCurrent && !x.IsExchanging && !x.IsReturned && !x.IsReturning),
                        ReturnedCount = g.Count(x => x.IsCurrent && x.IsReturned),
                        TotalCount = g.Count(x => x.IsCurrent),
                        ProductIds = g.Select(x => x.OrderProductId),
                        ReturnableProductIds = 
                            g.Where(x => x.IsCurrent && !x.IsExchanging && !x.IsReturned && !x.IsReturning)
                             .Select(x => x.OrderProductId)

                    })
                .ToList();
        }

        #endregion public methods

        #region .ctor

        public PponOrder(Guid orderGuid)
        {
            _orderGuid = orderGuid;
            _products = new List<Product>();

            _allOrderProducts = _op.OrderProductGetListByOrderGuid(orderGuid);
            _allOrderProductOptions = _op.OrderProductOptionGetListByOrderProductIds(_allOrderProducts.Select(x => x.Id));

            PponOptionCollection pponOptions = null;
            if (_allOrderProductOptions.Any(x => !x.IsOldProduct && x.OptionId != 0))
            {
                var viewPponOrder = _pp.ViewPponOrderGet(orderGuid);
                if (viewPponOrder != null && viewPponOrder.IsLoaded)
                {
                    Guid bid = viewPponOrder.BusinessHourGuid;
                    pponOptions = _pp.PponOptionGetList(bid, false);
                }
                else
                {
                    pponOptions = new PponOptionCollection();
                }
            }
            else
            {
                pponOptions = new PponOptionCollection();
            }

            foreach (OrderProduct prod in _allOrderProducts)
            {
                IEnumerable<OrderProductOption> productOptions =
                    _allOrderProductOptions.Where(x => x.OrderProductId == prod.Id);

                _products.Add(new Product(prod, productOptions, pponOptions));
            }
        }

        #endregion .ctor


    }


    public class SummarizedProductSpec
    {
        public string Spec { get; set; }
        public int TotalCount { get; set; }
        public int ReturnedCount { get; set; }
        public int ReturnableCount { get; set; }
        public IEnumerable<int> ProductIds { get; set; }
        public IEnumerable<int> ReturnableProductIds { get; set; }
    }


    
}

﻿using System.EnterpriseServices;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.Refund
{
    public class ReturnFormEntity
    {
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static ISysConfProvider _cp = ProviderFactory.Instance().GetConfig();

        #region props

        private readonly bool _isNew = false;
        internal bool IsNew
        {
            get { return _isNew; }
        }

        public int Id { get; private set; }
        public Guid OrderGuid { get; private set; }
        public string OrderId { get; private set; }
        public BusinessModel BizModel { get; private set; }
        public RefundType RefundType { get; private set; }
        public ThirdPartyPayment ThirdPartyPaymentSystem { get; set; }

        private ReturnForm _returnForm = null;
        internal ReturnForm ReturnForm
        {
            get { return _returnForm; }
        }

        private ReturnFormProductCollection _returnFormProducts = null;
        internal ReturnFormProductCollection ReturnFormProducts 
        {
            get { return _returnFormProducts; }
        }

        private ReturnFormRefundCollection _returnFormRefunds = null;
        public ReturnFormRefundCollection ReturnFormRefunds
        {
            get { return _returnFormRefunds; }
        }

        private readonly DeliveryType _deliveryType;
        public DeliveryType DeliveryType
        {
            get { return _deliveryType; }
        }
        
        private string _createId = null;
        /// <summary>
        /// 申請人
        /// </summary>
        public string CreateId
        {
            get { return _createId; }
        }

        private string _returnReason = null;
        /// <summary>
        /// 消費者退貨原因
        /// </summary>
        public string ReturnReason
        {
            get { return _returnReason; }
        }

        /// <summary>
        /// 退貨收件人
        /// </summary>
        private string _receiverName = null;
        public string ReceiverName
        {
            get { return _receiverName; }
        }

        /// <summary>
        /// 退貨收件地址
        /// </summary>
        private string _receiverAddress = null;
        public string ReceiverAddress
        {
            get { return _receiverAddress; }
        }


        /// <summary>
        /// 消費者是否已收到商品
        /// </summary>
        private bool? _isReceive = null;
        public bool? IsReceive
        {
            get { return _isReceive; }
        }

        /// <summary>
        /// 是否已通知代銷商
        /// </summary>
        private int? _isNotifyChannel = null;
        public int? IsNotifyChannel
        {
            get { return _isNotifyChannel; }
        }

        private string _memo = null;
        /// <summary>
        /// 退貨備註
        /// </summary>
        public string Memo
        {
            get { return _memo; }
        }

        /// <summary>
        /// 判斷是否由系統發起退貨
        /// </summary>
        private bool? _isSysRefund = null;
        public bool? IsSysRefund
        {
            get { return _isSysRefund; }
            set 
            {
                _isSysRefund = value;
            }
        }
        
        private bool _isCreditNoteReceived;
        public bool IsCreditNoteReceived
        {
            get { return _isCreditNoteReceived; }
            private set 
            { 
                _isCreditNoteReceived = value;
                _returnForm.IsCreditNoteReceived = value;
            }
        }

        private int _creditNoteType;
        public int CreditNoteType
        {
            get { return _creditNoteType; }
            private set
            {
                _creditNoteType = value;
            }
        }


        public IEnumerable<Product> Products { get; private set; }

        private readonly DateTime _createTime;
        /// <summary>
        /// 申請時間
        /// </summary>
        public DateTime CreateTime
        {
            get { return _createTime; }
        }

        private readonly DateTime _lastProcessingTime;
        /// <summary>
        /// 最後異動時間
        /// </summary>
        public DateTime LastProcessingTime
        {
            get { return _lastProcessingTime; }
        }

        public DateTime? FinishTime
        {
            get { return _returnForm.FinishTime; }
        }

        private readonly DateTime _vendorProcessTime;
        /// <summary>
        /// 廠商處理狀態最後異動時間
        /// </summary>
        public DateTime VendorProcessTime
        {
            get { return _vendorProcessTime; }
        }

        private ProgressStatus _progressStatus;
        public ProgressStatus ProgressStatus
        {
            get { return _progressStatus; }
        }

        private VendorProgressStatus _vendorProgressStatus;
        public VendorProgressStatus VendorProgressStatus
        {
            get { return _vendorProgressStatus; }
            private set { _vendorProgressStatus = value; }
        }

        private string _vendorMemo;
        /// <summary>
        /// 廠商處理備註
        /// </summary>
        public string VendorMemo
        {
            get { return _vendorMemo; }
            private set { _vendorMemo = value; }
        }

        #endregion props
        
        /// <summary>
        /// 改變退貨單進度 (推進進度)
        /// </summary>
        public bool ChangeProgressStatus(ProgressStatus newStatus, string modifyId = "sys")
        {
            if (this.IsNew)
            {
                return false;
            }

            if (this.ProgressStatus == ProgressStatus.Unknown)
            {
                return false;
            }

            if (this.ProgressStatus == newStatus)
            {
                return false;
            }

            switch (newStatus)
            {
                case ProgressStatus.AtmQueueSucceeded:
                    if (this.ProgressStatus != ProgressStatus.AtmQueueing)
                    {
                        return false;
                    }
                    break;
                case ProgressStatus.AtmFailed:
                    if (this.ProgressStatus != ProgressStatus.AtmQueueing
                        && (this.ProgressStatus == ProgressStatus.Processing))  // credit card processing but over one year
                    {
                        return false;
                    }
                    break;
                case ProgressStatus.AtmQueueing:
                case ProgressStatus.Unreturnable:
                    if (this.ProgressStatus != ProgressStatus.Processing
                    && this.ProgressStatus != ProgressStatus.AtmFailed) 
                    {
                        return false;
                    }
                    break;
                case ProgressStatus.Completed:
                    /*
                     * CompletedWithCreditCardQueued 可以改成 Completed
                     */
                    if (this.ProgressStatus != ProgressStatus.CompletedWithCreditCardQueued)
                    {
                        /*
                         * 只有購物金轉刷退才可以直接把退款進度改成完成.
                         * 一般狀況是要透過正常的退款方法, 
                         * 由退款去完整的維護退貨單的退款明細, 訂單貨品退貨狀態, 以及退貨單的進度.
                         */
                        if (this.RefundType != RefundType.ScashToAtm
                            && this.RefundType != RefundType.ScashToCash
                            && this.RefundType != RefundType.ScashToTcash)
                        {
                            return false;
                        }
                    }

                    break;
                case ProgressStatus.CompletedWithCreditCardQueued:
                    /* 
                     * 立即退款時, 
                     * 由於目前並沒有驗證刷退是否成功的機制,
                     * 所以刷退型跑完 Refund 方法後會變成 Completed.
                     * 這狀態是為了要讓客服可以反悔立即退款這個動作
                     */
                    if (this.ProgressStatus != ProgressStatus.Completed
                        && this.RefundType != RefundType.Cash)
                    {
                        return false;
                    }
                    break;
                case ProgressStatus.Canceled:
                    if (!CheckCancelableStatus()) 
                    {
                        return false;
                    }
                    break;
                case ProgressStatus.Processing:
                    /* 
                     * 處理狀態可以回到處理中的唯一情況:
                     * ATM 退款失敗, 需要客服更新ATM退款帳戶的資料後重新退款
                     */
                    if (this.ProgressStatus != ProgressStatus.AtmFailed)
                    {
                        return false;
                    }
                    break;
                default:
                    return false;
            }

            _progressStatus = newStatus;
            _returnForm.ProgressStatus = (int)newStatus;
            _returnForm.ModifyId = modifyId;
            _returnForm.ModifyTime = DateTime.Now;
            
            return true;
        }
        
        public bool IsCancelable()
        {
            if (!CheckCancelableStatus())
            {
                return false;
            }

            if (RefundType == RefundType.Scash)
            {
                //檢查購物金餘額是否充足
                Order order = _op.OrderGet(OrderGuid);
                RefundedAmount refundedAmount = GetRefundedAmount();

                decimal scash;
                decimal pscash;
                OrderFacade.GetSCashSum2(order.UserId, out scash, out pscash);
                if (scash < refundedAmount.SCash)
                {
                    return false;
                }
                if (pscash < refundedAmount.Pscash)
                {
                    return false;
                }
                //檢查紅利金餘額是否充足
                var remainBCash = Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(order.UserId) / 10);
                if (remainBCash < refundedAmount.BCash)
                {
                    return false;
                }
            }

            return true;
        }


        /// <summary>
        /// 已出貨商品訂單 ─ 註記退貨商品收回狀態
        /// </summary>
        /// <param name="progressStatus">廠商退貨進度</param>
        /// <param name="orderProductIds">已收回之商品order_product id</param>
        /// <param name="unReturnCount">不取回(不退款)之商品份數</param>
        /// <param name="progressMemo">退貨處理備註</param>
        /// <param name="modifyId">異動人員</param>
        public bool VendorReturnProcessShipped(VendorProgressStatus progressStatus, IEnumerable<int> orderProductIds, int unReturnCount, string progressMemo, string modifyId)
        {
            if (_returnForm == null)
            {
                return false;
            }
            if (!_returnForm.ProgressStatus.Equals((int)ProgressStatus.Processing) 
                && !_returnForm.ProgressStatus.Equals((int)ProgressStatus.Retrieving)
                && !_returnForm.ProgressStatus.Equals((int)ProgressStatus.RetrieveToPC)
                && !_returnForm.ProgressStatus.Equals((int)ProgressStatus.RetrieveToCustomer)
                && !_returnForm.ProgressStatus.Equals((int)ProgressStatus.ConfirmedForVendor)
                && !_returnForm.ProgressStatus.Equals((int)ProgressStatus.ConfirmedForUnArrival))
            {
                //只限處理 未完成退款 之退貨單
                return false;
            }
            if (_vendorProgressStatus == VendorProgressStatus.CompletedAndNoRetrieving ||
                _vendorProgressStatus == VendorProgressStatus.CompletedAndRetrievied ||
                _vendorProgressStatus == VendorProgressStatus.CompletedAndUnShip ||
                _vendorProgressStatus == VendorProgressStatus.CompletedByCustomerService ||
                _vendorProgressStatus == VendorProgressStatus.Automatic)
            {
                //只限處理 廠商退貨處理進度為處理中 之退貨單
                return false;
            }
            if (unReturnCount > _returnFormRefunds.Count(x => !x.IsFreight))
            {
                //不退款之商品份數 大於 退貨單上可退商品份數 則視為異常 不處理
                return false;
            }
            if (_returnForm.ProgressStatus.Equals((int)ProgressStatus.Retrieving)
                || _returnForm.ProgressStatus.Equals((int)ProgressStatus.RetrieveToPC)
                || _returnForm.ProgressStatus.Equals((int)ProgressStatus.RetrieveToCustomer)
                || _returnForm.ProgressStatus.Equals((int)ProgressStatus.ConfirmedForVendor)
                || _returnForm.ProgressStatus.Equals((int)ProgressStatus.ConfirmedForUnArrival))
                _returnForm.ProgressStatus = (int)ProgressStatus.Processing;

            _returnForm.VendorProgressStatus = progressStatus == VendorProgressStatus.Unknown ? (int)VendorProgressStatus.Processing : (int)progressStatus;
            _returnForm.VendorMemo = progressMemo;
            _returnForm.VendorProcessTime = DateTime.Now;
            _returnForm.ModifyTime = DateTime.Now;
            _returnForm.ModifyId = modifyId;

            //退貨進度標註為 [退貨完成-已收回商品] 時才需異動
            if (progressStatus.Equals(VendorProgressStatus.CompletedAndRetrievied))
            {
                foreach (ReturnFormProduct rfp in _returnFormProducts)
                {
                    rfp.IsCollected = orderProductIds.Contains(rfp.OrderProductId)
                                        ? true
                                        : false;
                }

                foreach (ReturnFormRefund rfr in _returnFormRefunds.Where(x => !x.IsFreight).Take(unReturnCount))
                {
                    rfr.VendorNoRefund = true;
                }

                //有運費之退貨單 : 若不退款商品份數 > 0 則視為非全退，不退回運費 
                if (unReturnCount > 0 && _returnFormRefunds.Any(x => x.IsFreight))
                {
                    ReturnFormRefund rfr = _returnFormRefunds.First(x => x.IsFreight);
                    rfr.VendorNoRefund = true;
                }

                //檢查訂單是否為分期刷退，若部分退回商品，則將刷退改先退購物金，並發信客服處理
                //問題.廠商復原是否復原RefundType?如何判斷申請時的RefundType?
                Order o = _op.OrderGet(OrderGuid);
                if (unReturnCount > 0 && _returnForm.RefundType == (int)RefundType.Cash && o.Installment > 0)
                {
                    _returnForm.RefundType = (int)RefundType.Scash;
                    EmailFacade.SendRefundTypeChangeMail(o.OrderId);
                    CommonFacade.AddAudit(OrderGuid, AuditType.Refund, "退貨份數異常，系統調整此筆分期訂單由[刷退]改[退購物金]", "sys", true);
                }
            }

            return true;
        }

        /// <summary>
        /// 未出貨商品訂單─退貨處理
        /// 不須紀錄商品退回狀態 所以退貨進度設定為 退貨完成
        /// </summary>
        /// <param name="modifyId">異動人員</param>
        public bool VendorReturnProcessUnShip(string modifyId)
        {
            if (_returnForm == null)
            {
                return false;
            }
            if (!_returnForm.ProgressStatus.Equals((int)ProgressStatus.Processing))
            {
                //只限處理 未完成退款 之退貨單
                return false;
            }
           if (_vendorProgressStatus == VendorProgressStatus.CompletedAndNoRetrieving ||
               _vendorProgressStatus == VendorProgressStatus.CompletedAndRetrievied ||
               _vendorProgressStatus == VendorProgressStatus.CompletedAndUnShip ||
               _vendorProgressStatus == VendorProgressStatus.CompletedByCustomerService ||
               _vendorProgressStatus == VendorProgressStatus.Automatic)
            {
                //只限處理 廠商退貨處理進度為處理中 之退貨單
                return false;
            }

            _returnForm.VendorProgressStatus = (int)VendorProgressStatus.CompletedAndUnShip;
            _returnForm.VendorMemo = null;
            _returnForm.VendorProcessTime = DateTime.Now;
            _returnForm.ModifyTime = DateTime.Now;
            _returnForm.ModifyId = modifyId;

            return true;
        }

        /// <summary>
        /// 廠商退貨處理回復為未處理狀態
        /// 清空備註資料 可為null的值改為null
		/// RefundUnprocessed
        /// </summary>
        /// <param name="modifyId">異動人員</param>
        public bool VendorReturnProcessRecover(string modifyId)
        {
            if (_returnForm == null)
            {
                return false;
            }
            if (!_returnForm.ProgressStatus.Equals((int)ProgressStatus.Processing))
            {
                //只限處理 未完成退款 之退貨單
                return false;
            }
            if (_vendorProgressStatus != VendorProgressStatus.CompletedAndNoRetrieving &&
                _vendorProgressStatus != VendorProgressStatus.CompletedAndRetrievied &&
                _vendorProgressStatus != VendorProgressStatus.CompletedAndUnShip)
            {
                //只限處理 廠商退貨處理進度為退貨完成(不包含[系統自動退]及[退貨完成:客服已處理]，這兩種狀態不能使用復原功能) 之退貨單
                return false;
            }

            _returnForm.VendorProgressStatus = (int)VendorProgressStatus.Processing;
            _returnForm.VendorMemo = null;
            _returnForm.VendorProcessTime = DateTime.Now;
            _returnForm.ModifyTime = DateTime.Now;
            _returnForm.ModifyId = modifyId;

            foreach (ReturnFormProduct rfp in _returnFormProducts)
            {
                if (rfp.IsCollected.HasValue)
                {
                    rfp.IsCollected = null;
                }
            }

            foreach (ReturnFormRefund rfr in _returnFormRefunds.Where(x => x.VendorNoRefund.Equals(true)))
            {
                rfr.VendorNoRefund = false;
            }

            return true;
        }

        /// <summary>
        /// 客服標註已處理
        /// </summary>
        public void VendorUnreturnableProcessed(string customerServiceId)
        {
            if (this.IsNew)
            {
                throw new InvalidOperationException();
            }
			
			if (this.VendorProgressStatus != VendorProgressStatus.Unreturnable)
            {
				throw new InvalidOperationException();
			}

            if (this.ProgressStatus != ProgressStatus.Processing)
            {
                throw new InvalidOperationException();
            }

            _returnForm.VendorProgressStatus = (int)VendorProgressStatus.UnreturnableProcessed;
            _returnForm.VendorProcessTime = DateTime.Now;
            _returnForm.ModifyTime = DateTime.Now;
            _returnForm.ModifyId = customerServiceId;
        }

        internal void ImmediateRefundUpdateVendorProgress(string userId)
        {
            if (this.IsNew)
            {
                throw new InvalidOperationException();
            }

            if (this.DeliveryType != DeliveryType.ToHouse)
            {
                return;
            }

            // 廠商處理過的就不用更新
            if (this.VendorProgressStatus == VendorProgressStatus.CompletedAndRetrievied
                || this.VendorProgressStatus == VendorProgressStatus.CompletedAndNoRetrieving
                || this.VendorProgressStatus == VendorProgressStatus.CompletedAndUnShip
                || this.VendorProgressStatus == VendorProgressStatus.Automatic) 
            {
                return;
            }


            this.VendorProgressStatus = VendorProgressStatus.CompletedByCustomerService;
            _returnForm.VendorProgressStatus = (int)VendorProgressStatus.CompletedByCustomerService;
            _returnForm.VendorProcessTime = DateTime.Now;
            _returnForm.ModifyTime = DateTime.Now;
            _returnForm.ModifyId = userId;
        }

        internal void OutOfTimeRangeUpdateVendorProgress()
        {
            if (this.IsNew)
            {
                throw new InvalidOperationException();
            }

            if (this.DeliveryType != DeliveryType.ToHouse)
            {
                return;
            }

            // 非退貨處理中且非廠商未處理之退貨單 就不用處理
            if (this.VendorProgressStatus != VendorProgressStatus.Processing && 
                this.ProgressStatus != ProgressStatus.Processing)
            {
                return;
            }

            this.VendorProgressStatus = VendorProgressStatus.Automatic;
            _returnForm.VendorProgressStatus = (int)VendorProgressStatus.Automatic;
            _returnForm.VendorProcessTime = DateTime.Now;
            _returnForm.ModifyTime = DateTime.Now;
            _returnForm.ModifyId = "sys";
        }
		
        /// <summary>
        /// 取得實際退貨商品品項資訊
        /// 廠商標記退貨完成且不為未取回狀態
        /// </summary>
        public string GetCollectedSpec()
        {
            if (this.DeliveryType == Core.DeliveryType.ToShop)
            {
                return string.Empty;
            }

            if (this.VendorProgressStatus != VendorProgressStatus.CompletedAndNoRetrieving &&
                this.VendorProgressStatus != VendorProgressStatus.CompletedAndRetrievied &&
                this.VendorProgressStatus != VendorProgressStatus.CompletedAndUnShip &&
                this.VendorProgressStatus != VendorProgressStatus.CompletedByCustomerService &&
                this.VendorProgressStatus != VendorProgressStatus.Automatic)
            {
                return string.Empty;
            }

            List<int> collectedProductIds = this.ReturnFormProducts
                                            .Where(x => x.IsCollected.GetValueOrDefault(true))
                                            .Select(x => x.OrderProductId)
                                            .ToList();

            return this.Products
                    .Where(x => collectedProductIds.Contains(x.OrderProductId))
                    .GroupBy(x => x.Spec)
                    .Select(g => new { Spec = g.Key, Count = g.Count() })
                    .Aggregate(new StringBuilder(),
                                (sb, kv) =>
                                {
                                    if (sb.Length > 0)
                                    {
                                        sb.AppendLine();
                                    }
                                    sb.Append(kv.Spec).Append(kv.Count);
                                    return sb;
                                })
                    .ToString();
        }

        /// <summary>
        /// 取得退貨申請商品品項資訊
        /// </summary>
		public string GetReturnSpec()
        {
            string refundedSpec = string.Empty;

            if (this.DeliveryType == Core.DeliveryType.ToHouse)
            {
                StringBuilder aggReturnedSb = this.Products
                    .GroupBy(x => x.Spec)
                    .Select(g => new { Spec = g.Key, Count = g.Count() })
                    .Aggregate(new StringBuilder(),
                                (sb, kv) =>
                                {
                                    if (sb.Length > 0)
                                    {
                                        sb.AppendLine();
                                    }
                                    sb.Append(kv.Spec).Append(kv.Count);
                                    return sb;
                                });

                refundedSpec = aggReturnedSb.ToString();
            }
            else
            {
                if (BizModel == BusinessModel.Ppon)
                {
                    List<Guid> returnTrustIds = _returnFormRefunds.Select(x => x.TrustId).ToList();
                    refundedSpec = GetCouponSequence(returnTrustIds, OrderClassification.LkSite);
                }
            }

            return refundedSpec;
        }

        /// <summary>
        /// 取得退款完成商品品項資訊
        /// </summary>
        public string GetRefundedSpec()
        {
            if (this.ProgressStatus != ProgressStatus.Completed
                && this.ProgressStatus != ProgressStatus.CompletedWithCreditCardQueued)
            {
                if (this.RefundType != RefundType.ScashToAtm
                    && this.RefundType != RefundType.ScashToCash
                    && this.RefundType != RefundType.ScashToTcash)
                {
                return string.Empty;
            }
            }

            string refundedSpec = string.Empty;

            if (this.DeliveryType == Core.DeliveryType.ToHouse)
            {
            List<int> returnedProductIds = this.ReturnFormProducts
                                               .Where(x => x.IsCollected.GetValueOrDefault(true))
                                               .Select(x => x.OrderProductId)
                                               .ToList();

                StringBuilder aggReturnedSb = this.Products
                    .Where(x => x.IsReturned && returnedProductIds.Contains(x.OrderProductId))
                    .GroupBy(x => x.Spec)
                    .Select(g => new { Spec = g.Key, Count = g.Count() })
                    .Aggregate(new StringBuilder(),
                                (sb, kv) =>
                                {
                                    if (sb.Length > 0)
                                    {
                                        sb.AppendLine();
                                    }
                                    sb.Append(kv.Spec).Append(kv.Count);
                                    return sb;
                                });

                refundedSpec = aggReturnedSb.ToString();
            }
            else
            {
                if (BizModel == BusinessModel.Ppon)
                {
                    List<Guid> refundedTrustIds = this.ReturnFormRefunds
                                                      .Where(x => x.IsRefunded.GetValueOrDefault(false))
                                                      .Select(x => x.TrustId)
                                                      .ToList();

                    refundedSpec = GetCouponSequence(refundedTrustIds, OrderClassification.LkSite);
                }
            }

            return refundedSpec;
        }
        
        public void ReceivedCreditNote(string modifier)
        {
            if (this.IsNew)
            {
                throw new InvalidOperationException();
            }

            this.IsCreditNoteReceived = true;
            _returnForm.ModifyId = modifier;
            _returnForm.ModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 購物金轉現金
        /// </summary>
        /// <param name="refundAtm">是否退ATM</param>
        /// <param name="refundTcash"></param>
        /// <param name="modifyId"></param>
        /// <param name="failReason"></param>
        internal void ChangeToCashRefund(bool refundAtm, bool refundTcash, string modifyId, out string failReason)
        {
            if (!CanChangeToCashRefund(out failReason))
            {
                return;
            }
            
            RefundType refundType;

            switch (this.ProgressStatus)
            {
                case ProgressStatus.Processing:
                case ProgressStatus.Retrieving:
                case ProgressStatus.ConfirmedForCS:
                case ProgressStatus.RetrieveToPC:
                case ProgressStatus.RetrieveToCustomer:
                case ProgressStatus.ConfirmedForVendor:
                case ProgressStatus.ConfirmedForUnArrival:
                    refundType = refundAtm
                        ? RefundType.Atm
                        : (ThirdPartyPaymentSystem == ThirdPartyPayment.None) ? RefundType.Cash : RefundType.Tcash;
                    break;
                case ProgressStatus.Completed:
                    refundType = refundAtm 
                        ? RefundType.ScashToAtm
                        : (refundTcash) ? RefundType.ScashToTcash : RefundType.ScashToCash;
                        
                    _progressStatus = ProgressStatus.Processing;
                    this._returnForm.ProgressStatus = (int)ProgressStatus.Processing;
                    break;
                default:
                    throw new InvalidOperationException("unknown state!");
            }

            this.RefundType = refundType;
            this._returnForm.ThirdPartyPaymentSystem = (byte)this.ThirdPartyPaymentSystem;
            this._returnForm.RefundType = (int)refundType;
            this._returnForm.ModifyId = modifyId;
            this._returnForm.ModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 轉退購物金
        /// </summary>
        internal void ChangeToSCashRefund(string modifyId, out string failReason)
        {
            if (CanChangeToSCashRefund(out failReason))
            {
                this.RefundType = RefundType.Scash;
                this._returnForm.RefundType = (int)RefundType.Scash;
                this._returnForm.ModifyId = modifyId;
                this._returnForm.ModifyTime = DateTime.Now;
            }
        }

        /// <summary>
        /// 由購物金轉刷退回復為退購物金完成狀態
        /// 條件限制:購物金轉刷退且未開始進行退款作業 之退貨單方可執行回復
        /// </summary>
        internal void RecoverToRefundScash(string modifyId, out string failReason)
        {
            failReason = string.Empty;

            if (RefundType != RefundType.ScashToCash && RefundType != RefundType.ScashToTcash)
            {
                failReason = "退貨單退款方式非購物金轉刷退";
                return;
            }
            if (ProgressStatus != ProgressStatus.Processing)
            {
                failReason = "退款處理中, 已無法改變退款方式!";
                return;
            }

            //取得最後退購物金完成時間 並將退貨結案時間回復為當時退購物金完成時間
            var refundSCashLog = _op.ReturnFormStatusLogGetListByReturnFormId(this.Id)
                                    .Where(x => x.RefundType == (int)RefundType.Scash &&
                                                x.ProgressStatus == (int)ProgressStatus.Completed)
                                    .OrderByDescending(x => x.ModifyTime)
                                    .FirstOrDefault();
            if (refundSCashLog != null)
            {
                this.RefundType = (RefundType)refundSCashLog.RefundType;
                this._returnForm.RefundType = refundSCashLog.RefundType;
                
                //回復為退購物金 第三方支付標記須取消
                if (this.ThirdPartyPaymentSystem != ThirdPartyPayment.None)
                { 
                    this.ThirdPartyPaymentSystem = ThirdPartyPayment.None;
                    this._returnForm.ThirdPartyPaymentSystem = (byte)this.ThirdPartyPaymentSystem;
                }

                this._returnForm.ProgressStatus = refundSCashLog.ProgressStatus;
                this._returnForm.FinishTime = refundSCashLog.ModifyTime;
                this._returnForm.ModifyId = modifyId;
                this._returnForm.ModifyTime = DateTime.Now;
            }
            else
            {
                failReason = "查無退購物金完成紀錄，無法回復!";
            }
        }

        /// <summary>
        /// 可否轉刷退
        /// </summary>
        public bool CanChangeToCashRefund(out string cannotReason)
        {
            if (RefundType != RefundType.Scash)
            {
                cannotReason = "退貨單退款方式非購物金";
                return false;
            }

            if (ProgressStatus == ProgressStatus.Unreturnable
                || ProgressStatus == ProgressStatus.Canceled)
            {
                cannotReason = "退貨單已失敗";
                return false;
            }

            if (ProgressStatus == ProgressStatus.Processing
                || ProgressStatus == ProgressStatus.Completed
                || ProgressStatus == ProgressStatus.Retrieving
                || ProgressStatus == ProgressStatus.ConfirmedForCS
                || ProgressStatus == ProgressStatus.RetrieveToPC
                || ProgressStatus == ProgressStatus.RetrieveToCustomer
                || ProgressStatus == ProgressStatus.ConfirmedForVendor
                || ProgressStatus == ProgressStatus.ConfirmedForUnArrival)
            {
                Order order = _op.OrderGet(OrderGuid);

                bool hasAtm = ReturnService.OrderHasAtmPayment(order);
                bool hasCreditCard = ReturnService.OrderHasCreditCardPayment(order);
                ThirdPartyPayment tpp;
                bool hasThirdPartyPay = ReturnService.OrderHasThirdPartyPayment(order, out tpp);
                bool hasCashOnDelivery = ReturnService.OrderHasCashOnDelivery(order);

                if (!hasAtm && !hasCreditCard && !hasThirdPartyPay && !hasCashOnDelivery)
                {
                    cannotReason = "沒有刷卡或ATM付款或其他付款方式金額";
                    return false;
                }

                if (hasAtm)
                {
                    if (!ReturnService.AtmRefundAccountExist(OrderGuid))
                    {
                        cannotReason = "ATM單, 需填妥 ATM 退款帳號";
                        return false;
                    }
                }

                if (ReturnService.OldOrderNeedAtm(order, tpp))
                {
                    if (!ReturnService.AtmRefundAccountExist(OrderGuid))
                    {
                        cannotReason = string.Format("{0}天前之信用卡訂單(LinePay{1}天)或台新儲值支付訂單，需填妥 ATM 退款帳號資訊",
                                            _cp.NoneCreditRefundPeriod, _cp.LinePayRefundPeriodLimit);
                        return false;
                    }
                }

                if (ReturnService.InstallmentNeedAtm(order, ReturnFormProducts.Where(x=>x.IsCollected ?? true).Select(x => x.ReturnFormId)))
                {
                    if (!ReturnService.AtmRefundAccountExist(OrderGuid))
                    {
                        cannotReason = "部分退貨，需填妥 ATM 退款帳號資訊";
                        return false;
                    }
                }

                //檢查購物金餘額是否充足
                RefundedAmount refundedAmount = GetRefundedSCashToCashAmount();
                if (OrderFacade.GetSCashSum(order.UserId) < refundedAmount.Atm + refundedAmount.CreditCard + refundedAmount.TCash + refundedAmount.IspPay)
                {
                    cannotReason = "購物金不足, 無法轉退現金";
                    return false;
                }

                cannotReason = string.Empty;
                return true;
            }

            cannotReason = "不明狀態, 請洽技術部!";
            return false;
        }

        /// <summary>
        /// 可否從退現改退購物金
        /// </summary>
        public bool CanChangeToSCashRefund(out string cannotReason)
        {
            if (RefundType != RefundType.Atm
                && RefundType != RefundType.Cash)
            {
                cannotReason = "退貨單退款方式不是刷退/ATM退款";
                return false;
            }

            if (ProgressStatus != ProgressStatus.Processing
                && ProgressStatus != ProgressStatus.Retrieving
                && ProgressStatus != ProgressStatus.ConfirmedForCS
                && ProgressStatus != ProgressStatus.RetrieveToPC
                && ProgressStatus != ProgressStatus.RetrieveToCustomer
                && ProgressStatus != ProgressStatus.ConfirmedForUnArrival
                && ProgressStatus != ProgressStatus.ConfirmedForUnArrival)
            {
                cannotReason = "退款處理中, 已無法改變退款方式!";
                return false;
            }

            cannotReason = string.Empty;
            return true;
        }

        /// <summary>
        /// 取得退貨申請憑證CouponId
        /// </summary>
        public IEnumerable<int> GetReturnCouponIds()
        {
            var returnCouponIds = new List<int>();

            if (this.DeliveryType == Core.DeliveryType.ToShop)
            {
                if (BizModel == BusinessModel.Ppon)
                {
                    List<Guid> returnTrustIds = _returnFormRefunds.Select(x => x.TrustId).ToList();
                    returnCouponIds = _mp.CashTrustLogGetList(returnTrustIds)
                                        .Where(x => x.CouponId.HasValue)
                                        .Select(x => x.CouponId.Value)
                                        .ToList();
                }
            }

            return returnCouponIds;
        }

        #region factory

        /*
         * 退貨單分成三個部分
         * (1) 退貨單屬性 - 處理進度, 退款方式... 儲存的時候建立
         * (2) 退貨單貨物明細
         * (3) 退貨單款項明細
         */


        /// <summary>
        /// 建立宅配型退貨單
        /// </summary>
        internal static ReturnFormEntity Make(Guid orderGuid, BusinessModel bizModel, IEnumerable<int> orderProductIds, RefundType refundType, 
            string returnReason, string createId, string receiverName, string receiverAddress,bool? isReceive,int? isNotifyChannel, ThirdPartyPayment paymentSystem = ThirdPartyPayment.None, 
            bool isAllReturned = false)
        {
            ReturnFormEntity result = new ReturnFormEntity(orderGuid, bizModel, refundType, DeliveryType.ToHouse, returnReason, createId, paymentSystem,
                receiverName, receiverAddress, isReceive, isNotifyChannel);

            if (!SetupOrderProduct(result, orderProductIds))
            {
                return null;
            }
            
            if(!SetupReturnFormRefund(result))
            {
                return null;
            }

            //判斷該張退貨單折讓類型 //宅配目前只會有一張發票
            EinvoiceMain em = _op.EinvoiceMainGetListByOrderGuid(orderGuid).FirstOrDefault();
            if(em != null)
            {
                if (isAllReturned)
                {
                    //宅配當期在未產號碼或未印製的狀況下全退才無須折讓
                    //多次退貨皆走折讓
                    result.CreditNoteType = !em.IsIntertemporal && em.InvoiceStatus != (int)EinvoiceType.D0401 && (em.InvoiceNumber == null || !em.InvoicePapered) ? (int)AllowanceStatus.None : em.AllowanceStatus ?? (int)AllowanceStatus.ElecAllowance;
                }
                else
                {
                    result.CreditNoteType = em.AllowanceStatus ?? (int)AllowanceStatus.ElecAllowance;
                }
            }
            else
            {
                result.CreditNoteType = (int)AllowanceStatus.None;
            }
           

            //訂單上已無可退品項 且 無其他退款處理中之退貨單 才可退運費
            if (ReturnService.GetReturnableProducts(orderGuid).Count == orderProductIds.Count() &&
                !ReturnService.HasProcessingReturnForm(orderGuid) && 
                ReturnService.HasRefundableFreight(orderGuid))
            {
                result.SetFreightTrustId(ReturnService.GetFreight(orderGuid).TrustId);
            }

            return result;
        }

        /// <summary>
        /// 建立憑證型退貨單
        /// </summary>
        internal static ReturnFormEntity MakeCouponTypeRefundForm(
            Guid orderGuid, BusinessModel bizModel, IEnumerable<int> couponIds, RefundType refundType, string returnReason, 
            string createId, OrderClassification classification, bool isSysRefund, ThirdPartyPayment paymentSystem = ThirdPartyPayment.None, bool isAllReturned = false)
        {
            var result = new ReturnFormEntity(orderGuid, bizModel, refundType, DeliveryType.ToShop, returnReason, createId, paymentSystem);

            if (!SetupCouponReturnFormRefund(result, couponIds, classification))
            {
                return null;
            }

            if (!SetupCouponReturnFormProduct(result))
            {
                return null;
            }

            //成套只有一張發票
            bool isGroupCoupon = Helper.IsFlagSet(_mp.GetCouponListMainByOid(orderGuid).BusinessHourStatus, BusinessHourStatus.GroupCoupon);

            var emc = _op.EinvoiceMainGetListByOrderGuid(orderGuid);
            EinvoiceMain em = isGroupCoupon ? emc.Where(x => x.InvoiceStatus == (int)EinvoiceType.C0401).FirstOrDefault() : emc.Where(x => x.InvoiceStatus == (int)EinvoiceType.C0401).FirstOrDefault(x => couponIds.Any(s => s == x.CouponId));

            //憑證當期在未產號碼(成套例外)或未印製的狀況下才無須折讓
            if (em != null)
            {
                //逾期系統退走電折
                if (isSysRefund && em.InvoiceMode2 == (int)InvoiceMode2.Duplicate && (em.AllowanceStatus ?? (int)AllowanceStatus.PaperAllowance) == (int)AllowanceStatus.PaperAllowance && em.OrderTime > _cp.EinvAllowanceStartDate)
                {
                    em.AllowanceStatus = (int)AllowanceStatus.ElecAllowance;
                    EinvoiceFacade.SetEinvoiceMainWithLog(em);
                }

                result.CreditNoteType = !em.IsIntertemporal && (em.InvoiceNumber == null || !em.InvoicePapered) && isAllReturned ? (int)AllowanceStatus.None : em.AllowanceStatus ?? (em.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? (int)AllowanceStatus.PaperAllowance : (int)AllowanceStatus.ElecAllowance);
            }
            else
            {
                //成套需判斷未開立發票時的退貨
                em = isGroupCoupon ? emc.FirstOrDefault() : em;
                //成套部分退貨來決定是否折讓
                result.CreditNoteType = em != null && isGroupCoupon && !isAllReturned ? (em.AllowanceStatus ?? (em.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? (int)AllowanceStatus.PaperAllowance : (int)AllowanceStatus.ElecAllowance)) : (int)AllowanceStatus.None;
            }

            return result;
        }

        #endregion factory

        private static bool SetupCouponReturnFormRefund(ReturnFormEntity returnForm, IEnumerable<int> couponIds, OrderClassification classification)
        {
            var refundTrustIds = new List<Guid>();

            BusinessModel bizModel = returnForm.BizModel;
            if (bizModel == BusinessModel.Ppon)
            {
                CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(returnForm.OrderGuid, classification);
                List<Guid> unrefundableTrustIds = ReturnService.GetUnrefundableReturnFormTrustIds(returnForm.OrderGuid);

                foreach (int couponId in couponIds)
                {
                    CashTrustLog ctlog = ctlogs.First(x => x.CouponId == couponId);
                    if (ctlog == null)
                    {
                        return false;
                    }
                    if (unrefundableTrustIds.Contains(ctlog.TrustId))
                    {
                        return false;
                    }
                    refundTrustIds.Add(ctlog.TrustId);
                }
            }
            else
            {
                
            }

            returnForm.SetNewReturnFormRefund(refundTrustIds);
            return true;
        }

        private static bool SetupCouponReturnFormProduct(ReturnFormEntity returnForm)
        {
            var orderProductIds = new List<int>();
            if (returnForm.BizModel == BusinessModel.Ppon)
            {
                PponOrder order = new PponOrder(returnForm.OrderGuid);
                int itemCount = returnForm.GetNewRefundTrustIds().Count;

                IEnumerable<int> prods = order.AllOrderProducts
                                                       .Where(x => x.IsCurrent
                                                                   && !x.IsReturning
                                                                   && !x.IsReturned)
                                                       .Take(itemCount)
                                                       .Select(x => x.Id);

                orderProductIds.AddRange(prods);
            }
            else
            {
                
            }
            returnForm.SetReturnFormProducts(orderProductIds);
            return true;
        }

        private string GetCouponSequence(IEnumerable<Guid> trustIds, OrderClassification orderClassification)
        {
            CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(this.OrderGuid, orderClassification);

            StringBuilder resultSb = new StringBuilder();

            foreach (Guid trustId in trustIds)
            {
                if (resultSb.Length > 0)
                {
                    resultSb.AppendLine();
                }
                CashTrustLog ctlog = ctlogs.FirstOrDefault(x => x.TrustId == trustId);
                if (ctlog != null)
                {
                    resultSb.Append(ctlog.CouponSequenceNumber);
                }
            }

            return resultSb.ToString();
        }

        /// <summary>
        /// 退款流程跑完
        /// </summary>
        /// <param name="failedTrustIdReasons">未退款成功的 trust_id 及原因</param>
        internal void RefundProcessed(List<Guid> successedTrustIds, Dictionary<Guid, string> failedTrustIdReasons, string createId, bool immediateRefund)
        {
            if (failedTrustIdReasons.Count == _returnFormRefunds.Count)
            {
                _progressStatus = ProgressStatus.Unreturnable;
                _returnForm.ProgressStatus = (int)ProgressStatus.Unreturnable;
                _returnForm.RefundProcessMemo = "沒有退款成功項目";
            }
            else
            {
                _progressStatus = ProgressStatus.Completed;
                _returnForm.ProgressStatus = (int)ProgressStatus.Completed;
            }

            _returnForm.ModifyId = createId;
            _returnForm.ModifyTime = DateTime.Now;


            foreach (ReturnFormRefund refund in _returnFormRefunds)
            {
                if (failedTrustIdReasons.ContainsKey(refund.TrustId))
                {
                    refund.IsRefunded = false;
                    refund.FailReason = failedTrustIdReasons[refund.TrustId];
                }
                else
                {
                    if (successedTrustIds.Contains(refund.TrustId))
                    {
                        refund.IsRefunded = true;
                        refund.FailReason = string.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// 退款流程跑完(for third party payment
        /// </summary>
        /// <param name="successedTrustIds">檢核流程跑完成功或進入retry的 trust id</param>
        /// <param name="retryTrustIds">進入 retry 的 trust id</param>
        /// <param name="failedTrustIdReasons">檢核流程失敗的 trust id</param>
        /// <param name="createId"></param>
        /// <param name="immediateRefund"></param>
        internal void RefundProcessedForTCash(List<Guid> successedTrustIds, List<Guid> retryTrustIds, Dictionary<Guid, string> failedTrustIdReasons, string createId, bool immediateRefund)
        {
            if (failedTrustIdReasons.Count == _returnFormRefunds.Count)
            {
                _progressStatus = ProgressStatus.Unreturnable;
                _returnForm.ProgressStatus = (int)ProgressStatus.Unreturnable;
                _returnForm.RefundProcessMemo = "沒有退款成功項目";
            }
            else if (retryTrustIds.Any())
            {
                _progressStatus = ProgressStatus.Processing;
                _returnForm.ProgressStatus = (int)ProgressStatus.Processing;
            }
            else
            {
                _progressStatus = ProgressStatus.Completed;
                _returnForm.ProgressStatus = (int) ProgressStatus.Completed;
            }

            _returnForm.ModifyId = createId;
            _returnForm.ModifyTime = DateTime.Now;


            foreach (ReturnFormRefund refund in _returnFormRefunds)
            {
                if (failedTrustIdReasons.ContainsKey(refund.TrustId))
                {
                    refund.IsRefunded = false;
                    refund.FailReason = failedTrustIdReasons[refund.TrustId];
                }
                else if (retryTrustIds.Contains(refund.TrustId))
                {
                    refund.IsRefunded = false;
                    refund.IsInRetry = true;
                    refund.FailReason = "退款重試中retry(0).";
                }
                else
                {
                    if (successedTrustIds.Contains(refund.TrustId))
                    {
                        refund.IsRefunded = true;
                        refund.FailReason = string.Empty;
                    }
                }
            }
        }

        internal void RefundProcessedForRetry(Guid trustId, string createId, bool isSuccess, bool canRetry, string failReason)
        {
            var item = _returnFormRefunds.First(x => x.TrustId == trustId);
            if (item != null)
            {
                item.IsRefunded = isSuccess;
                item.FailReason = failReason;
                item.IsInRetry = !isSuccess && canRetry;
            }

            var successCol = _returnFormRefunds.Where(x => x.IsRefunded == true).ToList(); //成功
            var failCol = _returnFormRefunds.Where(x => x.IsRefunded == false && x.IsInRetry == false).ToList(); //已達retry上限仍失敗

            //全部成功，或成功數加上已達 retry 上限仍失敗筆數與退款單一致則算處理完成
            if ((successCol.Count() == _returnFormRefunds.Count) || (successCol.Count() + failCol.Count()) == _returnFormRefunds.Count)
            {
                _progressStatus = ProgressStatus.Completed;
                _returnForm.ProgressStatus = (int) ProgressStatus.Completed;
            }

            _returnForm.ModifyId = createId;
            _returnForm.ModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 取消退貨:更新return_form_refund 為未退款狀態
        /// 若該訂單有運費 已退款運費可能存在於其他退貨單 也需找出來同步更新為未退款
        /// </summary>
        /// <param name="freightTrustId">運費trustId</param>
        internal void UndoReturnFormRefund(Guid? freightTrustId)
        {
            foreach (ReturnFormRefund refund in _returnFormRefunds.Where(x => x.IsRefunded.GetValueOrDefault(false)))
            {
                refund.IsRefunded = false;
            }

            // 更新return_form_refund 已退款運費(存在於其他退貨單中)之退款狀態為未退款
            if (freightTrustId.HasValue &&
                !_returnFormRefunds.Select(x => x.TrustId).Contains(freightTrustId.Value))
            {
                var undoRefundFrieghts = new ReturnFormRefundCollection();
                var freights = _op.ReturnFormRefundGetList(freightTrustId.Value)
                                    .Where(x => x.IsRefunded.GetValueOrDefault(false));
                foreach (var freight in freights)
                {
                    freight.IsRefunded = false;
                    undoRefundFrieghts.Add(freight);
                }
                _op.ReturnFormRefundSetList(undoRefundFrieghts);
            }
        }
        
        /// <summary>
        /// 設定結案時間 (退款成功, 退款失敗, 取消退貨)
        /// </summary>
        /// <param name="time"></param>
        internal void SetFinishTime(DateTime time)
        {
            _returnForm.FinishTime = time;
        }

        /// <summary>
        /// 清除結案時間 (購物金退完後, 轉退現金時使用)
        /// </summary>
        internal void ClearFinishTime()
        {
            _returnForm.FinishTime = null;
        }

        /// <summary>
        /// 取得該退貨單實際退款金額(依refund_type顯示退款方式及金額)
        /// </summary>
        public RefundedAmount GetRefundedAmount()
        {
            var amount = new RefundedAmount();
            List<Guid> trustIds = this.ReturnFormRefunds
                .Where(x => x.IsRefunded.GetValueOrDefault(false))
                .Select(x => x.TrustId)
                .ToList();

            CashTrustLogCollection ctls = _mp.CashTrustLogGetList(trustIds);

            amount.SCash = ctls.Sum(x => x.Scash);
            amount.Pscash = ctls.Sum(x => x.Pscash);
            amount.PCash = ctls.Sum(x => x.Pcash);
            amount.BCash = ctls.Sum(x => x.Bcash);
            if (this.RefundType == RefundType.Scash)
            {
                amount.SCash += ctls.Sum(x => x.CreditCard + x.Atm + x.Tcash + x.FamilyIsp + x.SevenIsp);
            }
            else if (this.RefundType == RefundType.Cash ||
                     this.RefundType == RefundType.ScashToCash)
            {
                amount.CreditCard = ctls.Sum(x => x.CreditCard);
            }
            else if (this.RefundType == RefundType.Atm ||
                     this.RefundType == RefundType.ScashToAtm)
            {
                //超過1年以上之退貨統一由ATM退款,故增加判斷其他付款方式
                amount.Atm = ctls.Sum(x => x.Atm);
                if (amount.Atm == 0)
                {
                    amount.Atm = ctls.Sum(x => x.CreditCard + x.Tcash + x.FamilyIsp + x.SevenIsp);
                }
            }
            else if (RefundType == RefundType.Tcash || RefundType == RefundType.ScashToTcash)
            {
                if (ThirdPartyPaymentSystem == ThirdPartyPayment.LinePay)
                {
                    amount.LinePay = ctls.Sum(x => x.Tcash);
                }
                else if(ThirdPartyPaymentSystem == ThirdPartyPayment.TaishinPay)
                {
                    amount.TaishinPay = ctls.Sum(x => x.Tcash);
                }
                amount.TCash = amount.LinePay + amount.TaishinPay;
            }

            return amount;
        }

        /// <summary>
        /// 取得該退貨單實際退款為購物金中應退現金金額 for購物金轉退現金檢查使用
        /// </summary>
        public RefundedAmount GetRefundedSCashToCashAmount()
        {
            var amount = new RefundedAmount();
            List<Guid> trustIds = this.ReturnFormRefunds
                .Where(x => x.IsRefunded.GetValueOrDefault(false))
                .Select(x => x.TrustId)
                .ToList();

            CashTrustLogCollection ctls = _mp.CashTrustLogGetList(trustIds);            
            if (this.RefundType == RefundType.Scash || this.RefundType == RefundType.ScashToAtm)
            {
                amount.CreditCard = ctls.Sum(x => x.CreditCard);
                amount.Atm = ctls.Sum(x => x.Atm);
                amount.TCash = ctls.Sum(x => x.Tcash);
                amount.IspPay = ctls.Sum(x => x.FamilyIsp + x.SevenIsp);
            }

            return amount;
        }

        #region .ctor

        private ReturnFormEntity(Guid orderGuid, BusinessModel bizModel, RefundType refundType, DeliveryType deliveryType, string returnReason,
            string createId, ThirdPartyPayment paymentSystem = ThirdPartyPayment.None, string receiverName = null, string receiverAddress = null, bool? isReceive = null, int? isNotifyChannel = null)
        {
            _isNew = true;
            this.OrderGuid = orderGuid;
            this.BizModel = bizModel;
            this.RefundType = refundType;
            _deliveryType = deliveryType;
            _returnReason = returnReason;
            _createId = createId;
            _receiverName = receiverName;
            _receiverAddress = receiverAddress;
            _isReceive = isReceive;
            _isNotifyChannel = isNotifyChannel;
            _isCreditNoteReceived = false;
            ThirdPartyPaymentSystem = paymentSystem;
            if (deliveryType == DeliveryType.ToHouse)
            {
                if (BizModel == BusinessModel.Ppon)
                {
                    var vpo = _pp.ViewPponOrderGet(orderGuid);
                    var bh = _pp.BusinessHourGet(vpo.BusinessHourGuid);
                    //如果是超取已付款訂單且未出貨(未取號)，廠商狀態直接壓系統處理
                    var opd = _op.OrderProductDeliveryGetByOrderGuid(orderGuid);

                    if (DateTime.Now < bh.BusinessHourDeliverTimeS || (opd != null && opd.IsLoaded && string.IsNullOrEmpty(opd.PreShipNo) && (opd.ProductDeliveryType == (int)ServiceChannel.FamilyMart || opd.ProductDeliveryType == (int)ServiceChannel.SevenEleven)))
                    {
                        //送貨時間尚未到 || 超取未取編
                        _vendorProgressStatus = VendorProgressStatus.Automatic;
                    }
                    else if (_cp.WmsRefundEnable && (string.IsNullOrEmpty(_cp.WmsRefundAccount) || (!string.IsNullOrEmpty(_cp.WmsRefundAccount) && createId == _cp.WmsRefundAccount)) && opd != null && opd.IsLoaded && opd.ProductDeliveryType == (int)ProductDeliveryType.Wms)
                    {
                        //PCHOME訂單
                        WmsOrder worder = _wp.WmsOrderGet(orderGuid);
                        if (!worder.IsLoaded || worder.Status == (int)WmsOrderSendStatus.Init)
                        {
                            //PCHOME尚未拋過去
                            _vendorProgressStatus = VendorProgressStatus.Automatic;
                        }
                        else if (isReceive == null || !Convert.ToBoolean(isReceive))
                        {
                            //客服建立 || 不在消費者手上
                            _vendorProgressStatus = VendorProgressStatus.ConfirmedForCS;
                        }
                        else
                        {
                            //在消費者手上
                            _vendorProgressStatus = VendorProgressStatus.Retrieving;
                        }
                    }
                    else
                    {
                        _vendorProgressStatus = VendorProgressStatus.Processing;
                    }


                    if (_vendorProgressStatus == VendorProgressStatus.Retrieving)
                        _progressStatus = ProgressStatus.Retrieving;
                    else if (_vendorProgressStatus == VendorProgressStatus.ConfirmedForCS)
                        _progressStatus = ProgressStatus.ConfirmedForCS;
                    else
                        _progressStatus = ProgressStatus.Processing;

                }
                else
                {
                    _vendorProgressStatus = VendorProgressStatus.Processing;
                }
            }
            else
            {
                _progressStatus = ProgressStatus.Processing;
                _vendorProgressStatus = VendorProgressStatus.Unknown;
            }

        }

        public ReturnFormEntity(int returnFormId)
        {
            ReturnForm form = _op.ReturnFormGet(returnFormId);
            if (form == null || !form.IsLoaded)
            {
                _isNew = true;
                return;
            }

            this.Id = returnFormId;
            _isNew = false;
            this.OrderGuid = form.OrderGuid;
            this.OrderId = form.OrderId;
            this.BizModel = (BusinessModel) form.BusinessModel;
            this.RefundType = (RefundType) form.RefundType;
            _deliveryType = (DeliveryType)form.DeliveryType;
            _createId = form.CreateId;
            _createTime = form.CreateTime;
            _lastProcessingTime = form.ModifyTime ?? form.CreateTime;
            _vendorProcessTime = form.VendorProcessTime ?? _lastProcessingTime;
            _progressStatus = (ProgressStatus) form.ProgressStatus;
            _vendorProgressStatus = (VendorProgressStatus)form.VendorProgressStatus.GetValueOrDefault(1);
            _isSysRefund = form.IsSystemRefund;
            _isCreditNoteReceived = form.IsCreditNoteReceived;
            _creditNoteType = form.CreditNoteType ?? 0;
            _returnReason = form.ReturnReason;
            _receiverName = form.ReceiverName;
            _receiverAddress = form.ReceiverAddress;
            _memo = form.Memo;
            _vendorMemo = form.VendorMemo;
            ThirdPartyPaymentSystem = (ThirdPartyPayment)form.ThirdPartyPaymentSystem;

            _returnForm = form;
            _returnFormProducts = _op.ReturnFormProductGetList(returnFormId);
            _returnFormRefunds = _op.ReturnFormRefundGetList(returnFormId);

            OrderProductCollection orderProducts = _op.OrderProductGetList(_returnFormProducts.Select(x => x.OrderProductId));
            OrderProductOptionCollection allOptions =
                _op.OrderProductOptionGetListByOrderProductIds(orderProducts.Select(x => x.Id));

            List<Product> returnFormProducts = new List<Product>();

            if (this.BizModel == BusinessModel.Ppon)
            {
                PponOptionCollection pponOptions = null;
                if (allOptions.Any(x => !x.IsOldProduct && x.OptionId != 0))
                {
                    var viewPponOrder = _pp.ViewPponOrderGet(this.OrderGuid);
                    if (viewPponOrder != null && viewPponOrder.IsLoaded)
                    {
                        Guid bid = viewPponOrder.BusinessHourGuid;
                        pponOptions = _pp.PponOptionGetList(bid, false);
                    }
                    else
                    {
                        pponOptions = new PponOptionCollection();
                    }
                }
                else
                {
                    pponOptions = new PponOptionCollection();
                }

                foreach (OrderProduct orderProduct in orderProducts)
                {
                    IEnumerable<OrderProductOption> productOptions =
                        allOptions.Where(x => x.OrderProductId == orderProduct.Id);
                    returnFormProducts.Add(new Product(orderProduct, productOptions, pponOptions));
                } 

                 
            }

            this.Products = returnFormProducts;
        }

        #endregion

        #region private members

        /// <summary>
        /// 建立宅配型退貨單時, 依據欲退貨的 order_product_id 設定退貨單中的 return_form_product
        /// </summary>
        private static bool SetupOrderProduct(ReturnFormEntity returnForm, IEnumerable<int> orderProductIds)
        {
            if (returnForm.DeliveryType != DeliveryType.ToHouse)
        {
                throw new InvalidOperationException();
            }

            if (!BelongToOrder(returnForm.OrderGuid, returnForm.BizModel, orderProductIds))
            {
                return false;
            }

            if (!QuantityReasonable(returnForm.OrderGuid, returnForm.BizModel, orderProductIds))
                {
                    return false;
                }

            if (!OrderProductReturnable(orderProductIds))
            {
                return false;
            }

            //todo: 品生活的 order product

            return returnForm.SetReturnFormProducts(orderProductIds);
        }

        /// <summary>
        /// 檢查退貨數量是否合理
        /// </summary>
        private static bool QuantityReasonable(Guid orderGuid, BusinessModel bizModel, IEnumerable<int> orderProductIds)
        {
            int returnCount = orderProductIds.Count();
            int distinctCount = orderProductIds.Distinct().Count();

            if (!int.Equals(distinctCount, returnCount))
            {
                return false;
            }

            if (int.Equals(0, returnCount))
            {
                return false;
            }

            if (bizModel == BusinessModel.Ppon)
            {
                return ReturnService.PponComboPackCheck(orderGuid, returnCount);
            }

            return true;
        }

        /// <summary>
        /// 檢查 orderProductIds 是否屬於訂單的
        /// </summary>
        private static bool BelongToOrder(Guid orderGuid, BusinessModel bizModel, IEnumerable<int> orderProductIds)
        {
            if (bizModel == BusinessModel.Ppon)
            {
                var pponOrder = new PponOrder(orderGuid);
                OrderProductCollection allProducts = pponOrder.AllOrderProducts;

                return orderProductIds.All(id => allProducts.Any(y => y.Id == id));
            }

            return false;
        }

        private static bool OrderProductReturnable(IEnumerable<int> orderProductIds)
        {
            Dictionary<int, OrderProduct> products = _op.
                OrderProductGetList(orderProductIds).
                ToDictionary(x => x.Id);
            
            foreach (int id in orderProductIds)
            {
                if (products.ContainsKey(id))
                {
                    OrderProduct prod = products[id];

                    if (!prod.IsCurrent || prod.IsReturning || prod.IsReturned || prod.IsExchanging)
                    {
                return false;
            }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }


        /// <summary>
        /// 設定退貨單的貨品項目
        /// </summary>
        private bool SetReturnFormProducts(IEnumerable<int> orderProductIds)
        {
            if (!this.IsNew)
            {
                return false;
            }

            var returnFormProducts = new ReturnFormProductCollection();

            foreach (int id in orderProductIds)
            {
                var formProd = new ReturnFormProduct
                    {
                        OrderProductId = id
                    };
                returnFormProducts.Add(formProd);
            }

            _returnFormProducts = returnFormProducts;

            return true;
        }
        
        /// <summary>
        /// 建立宅配退貨單裡的退款資訊
        /// </summary>
        private static bool SetupReturnFormRefund(ReturnFormEntity returnForm)
        {
            if (!returnForm.IsNew)
            {
                return false;
            }

            if (returnForm.DeliveryType != DeliveryType.ToHouse)
            {
                throw new InvalidOperationException();
            }

            if (returnForm.BizModel == BusinessModel.Ppon)
            {
                #region 好康

                /* 
                 * 由於並存的訂單雙結構
                 * 舊: order <-> order_detail <-> cash_trust_log
                 * 新("貨"、"款"分離): order <-> order_product;  order <-> cash_trust_log
                 * 
                 * 為了減少舊結構退款與退貨內容的不一致 (款項對應的貨物內容),
                 * 盡量從 order_product 找出可對應的 order_detail 再決定款項.
                 * 但只有在不同 order_detail 是"有差別"的情況下才需要找出這樣的對應.
                 * 
                 * 成套販售: 沒差別
                 * 沒選項: 沒差別
                 * 使用新選項: 有差別
                 * 匯入的舊選項(視為單一選項): 有差別 (可直接對到 order_detail)
                 */

                List<Guid> refundTrustIds = new List<Guid>();   // 要建在退貨單裡的款項
                List<CashTrustLog> refundableCtlogs = ReturnService.GetRefundableCtlogs(returnForm.OrderGuid, returnForm.BizModel);
                

                int comboCount = ReturnService.QueryComboPackCount(returnForm.OrderGuid);
                if (comboCount > 1)  
                {
                    // 成套販售
                    int refundCount = returnForm.ReturnFormProducts.Count/comboCount;
                    refundTrustIds.AddRange(refundableCtlogs.Take(refundCount).Select(x => x.TrustId)); 
                }
                else
                {
                    OrderDetailItemCollection odItems = _op.OrderDetailItemCollectionGetByOrderGuid(returnForm.OrderGuid);
                    var order = new PponOrder(returnForm.OrderGuid);
                
                    foreach (ReturnFormProduct returnProduct in returnForm.ReturnFormProducts)
                    {
                        Product product = order.Products.First(x => x.OrderProductId == returnProduct.OrderProductId);
                        
                        if (string.IsNullOrWhiteSpace(product.Spec))
                        {
                            // 沒有選項 => cash trust log 隨意挑
                            var ctlog = refundableCtlogs.First(x => !refundTrustIds.Contains(x.TrustId));
                            refundTrustIds.Add(ctlog.TrustId);
                        }
                        else
                        {
                            bool foundCtlog = false;

                            // 有選項 => 從 order_detail_item 找出同樣規格的 order_detail_guid 來對應 cash_trust_log
                            foreach (OrderDetailItem item in odItems)
                            {
                                string oldSpec = item.Options;
                                if (product.SpecOptions.All(opt => oldSpec.Contains(opt, StringComparison.OrdinalIgnoreCase)))
                                {
                                    var ctlog = refundableCtlogs.FirstOrDefault(x =>
                                                        x.OrderDetailGuid == item.OrderDetailGuid
                                                        && !refundTrustIds.Contains(x.TrustId));
                                    if (ctlog != null)
                                    {
                                        refundTrustIds.Add(ctlog.TrustId);
                                        foundCtlog = true;
                                        break;
                                    }
                                }
                            }

                            // 如果還是找不到 cash_trust_log, 隨便挑
                            if (!foundCtlog)
                            {
                                var ctlog = refundableCtlogs.First(x => !refundTrustIds.Contains(x.TrustId));
                                refundTrustIds.Add(ctlog.TrustId);
                            }
                        }
                    }
                }

                returnForm.SetNewReturnFormRefund(refundTrustIds);
                return true;

                #endregion 好康
            }
            else
            {
                #region 品生活
                #endregion
            }

            //todo: 找不出 trust_id 怎麼辦? 退貨單應該還是要可以成立....
            return false;
        }

        #region getter/setter for creating new return form

        private List<Guid> _newRefundTrustIds = null;

        internal List<Guid> GetNewRefundTrustIds()
        {
            if (!this._isNew)
            {
                throw new InvalidOperationException(); 
            }

            return _newRefundTrustIds;
        }
        
        private void SetNewReturnFormRefund(IEnumerable<Guid> trustIds)
        {
            if (!this._isNew)
        {
                throw new InvalidOperationException();
            }

            _newRefundTrustIds = trustIds.ToList();
        }

        private Guid? _newFreightTrustId = null;
        internal Guid? GetFreightTrustId()
        {
            if (!this._isNew)
            {
                throw new InvalidOperationException();
            }

            return _newFreightTrustId;
        }

        private void SetFreightTrustId(Guid freightTrustId)
        {
            _newFreightTrustId = freightTrustId;
        }

        #endregion getter/setter for creating new return form

        private bool CheckCancelableStatus()
        {
            if (this.IsNew)
            {
                return false;
            }

            if (this.ProgressStatus == ProgressStatus.Unreturnable
                        || this.ProgressStatus == ProgressStatus.Canceled
                        || this.ProgressStatus == ProgressStatus.AtmQueueing
                        || this.ProgressStatus == ProgressStatus.AtmQueueSucceeded)
            {
                return false;
            }

            if (this.ProgressStatus == ProgressStatus.Completed
                && this.RefundType != RefundType.Scash)
            {
                return false;
            }

            if (RefundType == RefundType.ScashToAtm
                || RefundType == RefundType.ScashToCash
                || RefundType == RefundType.ScashToTcash)
            {
                return false;
            }

            return !ReturnFormRefunds.Any(x => x.IsInRetry);
        }

        #endregion
    }
}

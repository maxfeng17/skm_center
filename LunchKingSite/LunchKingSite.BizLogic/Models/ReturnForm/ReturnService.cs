﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Transactions;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Interface;
using System.Data;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.BizLogic.Model.Refund
{
    public static class ReturnService
    {
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static ISysConfProvider _cp = ProviderFactory.Instance().GetConfig();
        private static IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        private static IFamiportProvider fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
        private static IHiLifeProvider hiLife = ProviderFactory.Instance().GetProvider<IHiLifeProvider>();

        /// <summary>
        /// 更新訂單的退貨狀態
        /// </summary>
        public static void UpdateOrderReturnStatus(Guid orderGuid)
        {
            bool isCancel;
            bool isCanceling;
            bool isPartialCancel;

            OrderProductCollection products = _op.OrderProductGetListByOrderGuid(orderGuid);

            int totalCount = products.Count;
            int returnedCount = products.Count(x => x.IsReturned);
            int returningCount = products.Count(x => x.IsReturning);

            isCancel = returnedCount > 0;
            isCanceling = returningCount > 0;
            isPartialCancel = returnedCount > 0
                                && returnedCount != totalCount;


            Order order = _op.OrderGet(orderGuid);
            if (order == null || !order.IsLoaded)
            {
                return;
            }

            if (Helper.IsFlagSet(order.OrderStatus, OrderStatus.Cancel) != isCancel
                || order.IsCanceling != isCanceling
                || order.IsPartialCancel != isPartialCancel)
            {
                Guid bid = _op.GroupOrderGetByOrderGuid(order.ParentOrderId.GetValueOrDefault()).BusinessHourGuid;
                order.IsCanceling = isCanceling;
                order.IsPartialCancel = isPartialCancel;
                order.OrderStatus = (int)Helper.SetFlag(isPartialCancel, order.OrderStatus, OrderStatus.PartialCancel);

                //取消退貨 才須拿掉 order_status 取消訂單註記
                if (!isCancel)
                {
                    order.OrderStatus = (int)Helper.SetFlag(isCancel, order.OrderStatus, OrderStatus.CancelBeforeClosed);
                    order.OrderStatus = (int)Helper.SetFlag(isCancel, order.OrderStatus, OrderStatus.Cancel);

                    //除退貨完成須更新deal_sales_info外，取消退貨也須更新deal_sales_info
                    PayEvents.OnRefresh(bid);
                }

                _op.OrderSet(order);

                if (isCancel || isPartialCancel)
                {
                    OrderFacade.SetOrderStatus(orderGuid, "sys", OrderStatus.Cancel, true, bid);
                    CommonFacade.AddAudit(orderGuid, AuditType.Order, "訂單取消", "sys", false);
                }
            }
        }


        public static IList<OrderProduct> GetReturnableProducts(Guid orderGuid)
        {
            OrderProductCollection products = _op.OrderProductGetListByOrderGuid(orderGuid);
            return products.Where(x => x.IsCurrent && !x.IsReturning && !x.IsReturned).ToList();
        }

        /// <summary>
        /// 是否允許退貨
        /// </summary>
        public static bool AllowReturn(Guid orderGuid, out string denyReason)
        {
            ViewPponOrder order = _pp.ViewPponOrderGet(orderGuid);

            if (order == null || !order.IsLoaded)
            {
                denyReason = "查不到訂單";
                return false;
            }

            BusinessHour deal = _pp.BusinessHourGet(order.BusinessHourGuid);

            if (!order.Status.HasValue)
            {
                denyReason = "檔次資料有問題";
                return false;
            }

            var groupOrderStats = (GroupOrderStatus)order.Status.Value;

            // 不可退貨
            if (Helper.IsFlagSet(groupOrderStats, GroupOrderStatus.NoRefund)
                && DateTime.Now > deal.BusinessHourOrderTimeS)
            {
                denyReason = "此訂單不接受退貨";
                return false;
            }

            // 過期後不可退貨
            if (Helper.IsFlagSet(groupOrderStats, GroupOrderStatus.ExpireNoRefund)
                && DateTime.Now.Date > deal.BusinessHourDeliverTimeE.Value.Date)
            {
                denyReason = "此訂單已過期";
                return false;
            }

            // 結檔後 N 天不可退貨
            if (Helper.IsFlagSet(groupOrderStats, GroupOrderStatus.DaysNoRefund)
                && DateTime.Now.Date > deal.BusinessHourOrderTimeE.AddDays(_cp.GoodsAppreciationPeriod).Date)
            {
                denyReason = "此訂單已過鑑賞期";
                return false;
            }

            // 演出時間前 N 日內不能退貨
            if (Helper.IsFlagSet(groupOrderStats, GroupOrderStatus.NoRefundBeforeDays)
                && DateTime.Now.Date >= deal.BusinessHourDeliverTimeS.Value.AddDays(-_cp.PponNoRefundBeforeDays).Date)
            {
                denyReason = string.Format("此訂單在演出時間{0}日內", _cp.PponNoRefundBeforeDays);
                return false;
            }

            DealProperty dp = _pp.DealPropertyGet(order.BusinessHourGuid);
            if (dp.DeliveryType == (int)DeliveryType.ToHouse)
            {
                OrderShip os = _op.OrderShipGet(orderGuid);
                bool userPickedUp = true;
                if (os != null && os.IsLoaded && (os.IsReceipt == null || os.IsReceipt == false))
                {
                    userPickedUp = false;
                }
                if (PponOrderManager.CheckIsOverTrialPeriod(orderGuid) && userPickedUp)
                {
                    denyReason = "此訂單已過鑑賞期";
                    return false;
                }
            }

            denyReason = string.Empty;
            return true;
        }

        public static bool HasRefundableFreight(Guid orderGuid)
        {
            CashTrustLog freight = GetFreight(orderGuid);
            if (freight == null
                || freight.Status == (int)TrustStatus.Refunded
                || freight.Status == (int)TrustStatus.Returned)
            {
                return false;
            }

            ReturnFormRefundCollection refunds = _op.ReturnFormRefundGetList(freight.TrustId);

            foreach (var refund in refunds)
            {
                var returnForm = _op.ReturnFormGet(refund.ReturnFormId);
                if (returnForm.ProgressStatus == (int)ProgressStatus.Processing
                    || returnForm.ProgressStatus == (int)ProgressStatus.CompletedWithCreditCardQueued
                    || returnForm.ProgressStatus == (int)ProgressStatus.AtmQueueing
                    || returnForm.ProgressStatus == (int)ProgressStatus.AtmQueueSucceeded
                    || returnForm.ProgressStatus == (int)ProgressStatus.AtmFailed)
                {
                    //運費處理中
                    return false;
                }
            }

            return true;
        }

        internal static CashTrustLog GetFreight(Guid orderGuid)
        {
            CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            return ctlogs.FirstOrDefault(ctlog => Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.Freight));
        }

        /// <summary>
        /// 建立退貨單 (商品類)
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="orderProductIds"></param>
        /// <param name="refundSCash">是否退 17Life 購物金</param>
        /// <param name="createId">執行者</param>
        /// <param name="returnReason">消費者退貨原因</param>
        /// <param name="receiverName">退貨收件人</param>
        /// <param name="receiverAddress">退貨收件地址</param>
        /// <param name="returnFormId">退貨單id</param>
        /// <param name="skipDealSettingCheck">是否要跳過檔次退貨設定的檢查</param>
        /// <returns></returns>
        public static CreateReturnFormResult CreateRefundForm(Guid orderGuid, IEnumerable<int> orderProductIds, bool refundSCash, string createId,
            string returnReason, string receiverName, string receiverAddress, bool? isReceive, int? isNotifyChannel, out int? returnFormId, bool skipDealSettingCheck = false)
        {
            returnFormId = null;
            Order order = _op.OrderGet(orderGuid);

            #region 資料驗證

            if (order == null || !order.IsLoaded)
            {
                throw new InvalidOperationException(string.Format("order not found! order_guid: {0}", orderGuid.ToString()));
            }
            if (!Helper.IsFlagSet(order.OrderStatus, OrderStatus.Complete))
            {
                if ((Helper.IsFlagSet(order.OrderStatus, OrderStatus.FamilyIspOrder) || Helper.IsFlagSet(order.OrderStatus, OrderStatus.SevenIspOrder)) && _config.EnableCancelPaidByIspOrder)
                {
                    return CreateReturnFormResult.IspOrderNotCreate;
                }
                else
                {
                    return CreateReturnFormResult.OrderNotCreate;
                }
            }

            string dummy;
            if (!skipDealSettingCheck
                && !AllowReturn(orderGuid, out dummy))
            {
                return CreateReturnFormResult.DealDeniesRefund;
            }

            if (!ProductsReturnable(orderGuid, orderProductIds))
            {
                return CreateReturnFormResult.ProductsUnreturnable;
            }

            if (HasProcessingReturnForm(orderGuid))
            {
                return CreateReturnFormResult.ProductsIsReturning;
            }

            bool isAllReturned = IsAllReturnedApplication(orderGuid, orderProductIds);

            //仍有換貨處理中商品 可部分退貨 不允許全部退貨
            if (_config.EnabledExchangeProcess &&
                isAllReturned &&
                OrderExchangeUtility.HasProcessingExchangeLog(orderGuid))
            {
                return CreateReturnFormResult.ProductsIsExchanging;
            }

            bool isAtmRefund = false;
            bool isCreditCardRefund = !refundSCash;
            bool isThirdPartyPayRefund = !refundSCash;
            ThirdPartyPayment paymentSystem = ThirdPartyPayment.None;

            if (!refundSCash)
            {
                if (OrderHasAtmPayment(order))
                {
                    if (!AtmRefundAccountExist(orderGuid))
                    {
                        return CreateReturnFormResult.AtmOrderNeedAtmRefundAccount;
                    }
                    isAtmRefund = true;
                }

                CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(order.Guid, OrderClassification.LkSite);
                isCreditCardRefund = OrderHasCreditCardPayment(ctlogs);
                isThirdPartyPayRefund = OrderHasThirdPartyPayment(ctlogs, out paymentSystem);

                //信用卡、LinePay 超過特定期限，台新儲值支付退貨，超取超付，一律走ATM退款
                if (OldOrderNeedAtm(order, paymentSystem))
                {
                    if (!AtmRefundAccountExist(orderGuid))
                    {
                        if (Helper.IsFlagSet(order.OrderStatus, OrderStatus.FamilyIspOrder) || Helper.IsFlagSet(order.OrderStatus, OrderStatus.SevenIspOrder))
                        {
                            return CreateReturnFormResult.IspOrderNeedAtmRefundAccount;
                        }
                        return CreateReturnFormResult.OldOrderNeedAtmRefundAccount;
                    }
                    isAtmRefund = true;
                }
                if (InstallmentNeedAtm(order, orderProductIds))
                {
                    if (!AtmRefundAccountExist(orderGuid))
                    {
                        return CreateReturnFormResult.InstallmentOrderNeedAtmRefundAccount;
                    }
                    isAtmRefund = true;
                }
                if (Helper.IsFlagSet(order.OrderStatus, OrderStatus.FamilyIspOrder) || Helper.IsFlagSet(order.OrderStatus, OrderStatus.SevenIspOrder))
                {
                    if (!AtmRefundAccountExist(orderGuid))
                    {
                        return CreateReturnFormResult.IspOrderNeedAtmRefundAccount;
                    }
                    isAtmRefund = true;
                }
            }

            #endregion

            RefundType refundType;
            if (refundSCash)
            {
                refundType = RefundType.Scash;
            }
            else
            {
                if (isAtmRefund)
                {
                    refundType = RefundType.Atm;
                }
                else
                {
                    if (isCreditCardRefund)
                    {
                        refundType = RefundType.Cash;
                    }
                    else if (isThirdPartyPayRefund)
                    {
                        refundType = RefundType.Tcash;
                    }
                    else
                    {
                        refundType = RefundType.Scash;
                    }
                }
            }

            var returnForm = ReturnFormEntity.Make(orderGuid, BusinessModel.Ppon, orderProductIds, refundType, returnReason, createId,
                receiverName, receiverAddress, isReceive, isNotifyChannel, paymentSystem, isAllReturned);
            if (returnForm == null)
            {
                return CreateReturnFormResult.InvalidArguments;
            }
            else
            {
                returnFormId = ReturnFormRepository.Save(returnForm);
                if (returnFormId == 0)
                {
                    return CreateReturnFormResult.InvalidArguments;
                }
                order.IsCanceling = true;
                _op.OrderSet(order);
            }

            return CreateReturnFormResult.Created;
        }

        /// <summary>
        /// 建立退貨單 (憑證類)
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="couponIds"></param>
        /// <param name="refundSCash"></param>
        /// <param name="createId"></param>
        /// <param name="returnReason"></param>
        /// <param name="returnFormId"></param>
        /// <param name="skipDealSettingCheck"></param>
        /// <param name="isSysRefund"></param>
        /// <param name="tppSystem"></param>
        /// <param name="classification"></param>
        /// <returns></returns>
        public static CreateReturnFormResult CreateCouponReturnForm(
            Guid orderGuid,
            IEnumerable<int> couponIds,
            bool refundSCash,
            string createId,
            string returnReason,
            out int? returnFormId,
            bool skipDealSettingCheck = false,
            bool isSysRefund = false,
            ThirdPartyPayment tppSystem = ThirdPartyPayment.None,
            OrderClassification classification = OrderClassification.LkSite)
        {
            returnFormId = null;
            Order order = _op.OrderGet(orderGuid);

            if (order == null || !order.IsLoaded)
            {
                throw new InvalidOperationException(string.Format("order not found! order_guid: {0}", orderGuid.ToString()));
            }
            if (!Helper.IsFlagSet(order.OrderStatus, OrderStatus.Complete))
            {
                return CreateReturnFormResult.OrderNotCreate;
            }

            string dummy;
            if (!skipDealSettingCheck
                && !AllowReturn(orderGuid, out dummy))
            {
                return CreateReturnFormResult.DealDeniesRefund;
            }

            if (!CouponReturnable(orderGuid, couponIds))
            {
                return CreateReturnFormResult.ProductsUnreturnable;
            }

            if (!UnusedCouponLock(orderGuid, couponIds))
            {
                return CreateReturnFormResult.UnusedCouponLock;
            }

            bool isAtmRefund = false;
            bool isCreditCardRefund = !refundSCash;
            bool isThirdPartyPayRefund = !refundSCash;

            CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(order.Guid, OrderClassification.LkSite);
            bool isAllReturned = couponIds.Count() == ctlogs.Count;

            if (!refundSCash)
            {
                if (OrderHasAtmPayment(order))
                {
                    if (!AtmRefundAccountExist(orderGuid))
                    {
                        return CreateReturnFormResult.AtmOrderNeedAtmRefundAccount;
                    }
                    isAtmRefund = true;
                }

                isCreditCardRefund = OrderHasCreditCardPayment(ctlogs);
                isThirdPartyPayRefund = OrderHasThirdPartyPayment(ctlogs, out tppSystem);

                //信用卡、LinePay 超過特定期限，台新儲值支付退貨，一律走ATM退款
                if (OldOrderNeedAtm(order, tppSystem))
                {
                    if (!AtmRefundAccountExist(orderGuid))
                    {
                        return CreateReturnFormResult.OldOrderNeedAtmRefundAccount;
                    }
                    isAtmRefund = true;
                }

                //分期付款且部份退貨，走ATM退款
                if (InstallmentNeedAtm(order, couponIds))
                {
                    if (!AtmRefundAccountExist(orderGuid))
                    {
                        return CreateReturnFormResult.InstallmentOrderNeedAtmRefundAccount;
                    }
                    isAtmRefund = true;
                }
            }

            RefundType refundType;
            if (refundSCash)
            {
                refundType = RefundType.Scash;
            }
            else
            {
                if (isAtmRefund)
                {
                    refundType = RefundType.Atm;
                }
                else
                {
                    if (isCreditCardRefund)
                    {
                        refundType = RefundType.Cash;
                    }
                    else if (isThirdPartyPayRefund)
                    {
                        refundType = RefundType.Tcash;
                    }
                    else
                    {
                        refundType = RefundType.Scash;
                    }
                }
            }

            var returnForm = ReturnFormEntity.MakeCouponTypeRefundForm(
                                orderGuid, BusinessModel.Ppon, couponIds, refundType, returnReason, createId, classification, isSysRefund, tppSystem, isAllReturned);
            if (returnForm == null)
            {
                return CreateReturnFormResult.InvalidArguments;
            }
            if (refundSCash)
            {
                returnForm.IsSysRefund = isSysRefund;
            }
            returnFormId = ReturnFormRepository.Save(returnForm);
            order.IsCanceling = true;
            _op.OrderSet(order);

            return CreateReturnFormResult.Created;
        }


        /// <summary>
        /// 把退貨單轉現金
        /// </summary>
        public static void RefundScashToCash(int returnFormId, string userId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(returnFormId);

            ProgressStatus originalStatus = returnForm.ProgressStatus;

            Order order = _op.OrderGet(returnForm.OrderGuid);

            ThirdPartyPayment tpp;
            bool useTcash = OrderHasThirdPartyPayment(order, out tpp);
            returnForm.ThirdPartyPaymentSystem = tpp;

            bool useAtm = OrderHasAtmPayment(order) ||
                          OldOrderNeedAtm(order, returnForm.ThirdPartyPaymentSystem) ||
                          InstallmentNeedAtm(order, returnForm.ReturnFormProducts.Where(x => x.IsCollected ?? true).Select(x => x.ReturnFormId));

            string failReason;
            returnForm.ChangeToCashRefund(useAtm, useTcash, userId, out failReason);
            if (string.IsNullOrWhiteSpace(failReason))
            {
                returnForm.ClearFinishTime();
                using (var scope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    ReturnFormRepository.Save(returnForm);

                    if (originalStatus == ProgressStatus.Completed)
                    {
                        /*
                        * 原本已經退成購物金了
                        * 為了避免購物金被使用, 要先跑退款把購物金扣掉.
                        * 再等排程處理現金的退款
                        */
                        Refund(returnForm, userId);
                    }
                    scope.Complete();
                }
            }
            else
            {
                string auditMessage = "購物金轉現金失敗:" + failReason;
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, auditMessage, userId, false);
            }
        }

        /// <summary>
        /// 把退貨單退款方式從退現改成退購物金
        /// </summary>
        public static void RefundCashToSCash(int returnFormId, string userId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(returnFormId);

            string failReason;
            returnForm.ChangeToSCashRefund(userId, out failReason);
            if (string.IsNullOrWhiteSpace(failReason))
            {
                ReturnFormRepository.Save(returnForm);
            }
            else
            {
                string auditMessage = "退現轉退購物金失敗:" + failReason;
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, auditMessage, userId, false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnForm"></param>
        /// <param name="createId"></param>
        /// <param name="immediateRefund">立即退款, 無視廠商進度/折讓單是否回來</param>
        /// <param name="sendRefundCompleteNoticeMail">是否寄送退款完成通知信給消費者</param>
        /// <param name="isForced">是否強制退貨</param>
        public static void Refund(ReturnFormEntity returnForm, string createId = "sys", bool immediateRefund = false, bool sendRefundCompleteNoticeMail = true, bool isForced = false)
        {
            if (!RefundCheck(returnForm, createId, immediateRefund))
            {
                return;
            }

            //檢查全家寄杯是否已退貨完畢
            List<FamilyNetPincode> pincodes = fami.GetFamilyNetListPincode(returnForm.OrderGuid);
            if (pincodes.Any(x => x.Status == (byte)FamilyNetPincodeStatus.Completed && x.ReturnStatus != (int)FamilyNetPincodeReturnStatus.Completed && !x.IsVerified))
            {
                return;
            }

            //檢查萊爾富寄杯是否已退貨完畢
            List<HiLifePincode> hiLifePincodes = hiLife.GetHiLifePincode(returnForm.OrderGuid);
            if (hiLifePincodes.Any(x => x.ReturnStatus != (int)HiLifeReturnStatus.Success && !x.IsVerified))
            {
                return;
            }

            List<Guid> refundedTrustIds;

            try
            {
                using (var scope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    if (immediateRefund && returnForm.DeliveryType == DeliveryType.ToHouse)
                    {
                        returnForm.ImmediateRefundUpdateVendorProgress(createId);
                    }
                    refundedTrustIds = ProcessRefund(returnForm, createId, immediateRefund, isForced);
                    ConcludeReturnForm(returnForm);

                    if (returnForm.DeliveryType == DeliveryType.ToHouse &&
                       returnForm.RefundType != RefundType.ScashToAtm &&
                       returnForm.RefundType != RefundType.ScashToCash &&
                        returnForm.RefundType != RefundType.ScashToTcash)
                    {
                        int memberUniqueId = MemberFacade.GetUniqueId(createId);
                        //更新對帳單請款明細狀態(Undo balance_sheet_detail)
                        foreach (var refundTrustId in refundedTrustIds)
                        {
                            BalanceSheetService.UndoDetail(refundTrustId, memberUniqueId, UndoType.Return);
                        }
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                var msg = new MailMessage();
                msg.From = new MailAddress(_cp.AdminEmail);
                msg.To.Add(new MailAddress(_cp.ItTesterEmail));
                msg.Subject = "Refund exception!";
                msg.Body = string.Format("退貨單(id={0})退款發生問題!{1}{2}",
                    returnForm.Id, Environment.NewLine, ex.StackTrace.Length > 2500 ? ex.StackTrace.Substring(0, 2500) : ex.StackTrace);
                msg.IsBodyHtml = false;
                PostMan.Instance().Send(msg, SendPriorityType.Normal);

                return;
            }

            if (returnForm.BizModel == BusinessModel.Ppon)
            {
                /*
                 * order 和 cash_trust_log 的存取比較容易跟其他功能衝突到, 
                 * 所以不放在 transaction scope 裡
                 */
                UpdateOrderReturnStatusWithExceptionMail(returnForm.OrderGuid);

                //處理 ATM 退款狀態
                if ((returnForm.RefundType == RefundType.Atm ||
                     returnForm.RefundType == RefundType.ScashToAtm) &&
                    (returnForm.ProgressStatus == ProgressStatus.Completed ||
                     returnForm.ProgressStatus == ProgressStatus.Unreturnable))
                {
                    /* 已退款之份數可能因為未分配現金購買 導致未取消atm退款中狀態
                     * 改為只要退貨單狀態為處理完成 則將退貨單上之退貨份數皆取消atm退款處理中狀態 */
                    List<Guid> allTrustIds = returnForm.ReturnFormRefunds.Select(x => x.TrustId).ToList();

                    try
                    {
                        RemoveAtmRefundingFlag(allTrustIds, createId);
                    }
                    catch (Exception)
                    {
                        var msg = new MailMessage();
                        msg.From = new MailAddress(_cp.AdminEmail);
                        msg.To.Add(new MailAddress(_cp.ItTesterEmail));
                        msg.Subject = "cash_trust_log 的 atm_refunding 狀態未拿掉!";
                        msg.Body = string.Format("退貨單(id={0})退款發生問題!{2}嘗試更新 cash_trust_log 時發生錯誤.{2}TrustId={1}",
                            returnForm.Id,
                            string.Join(",", allTrustIds),
                            Environment.NewLine);
                        msg.IsBodyHtml = false;
                        PostMan.Instance().Send(msg, SendPriorityType.Normal);
                    }
                }


                if (returnForm.ProgressStatus == ProgressStatus.Completed
                    || returnForm.ProgressStatus == ProgressStatus.CompletedWithCreditCardQueued)
                {
                    Order order = _op.OrderGet(returnForm.OrderGuid);

                    if (Helper.IsFlagSet(order.OrderStatus, OrderStatus.Cancel) && !order.IsPartialCancel)
                    {
                        MemberFacade.MemberPromotionDeductionWhenOrderCancel(order.Guid, MemberFacade.GetUserName(order.UserId));
                    }

                    ViewPponDeal deal = _pp.ViewPponDealGet(order.ParentOrderId.Value);
                    if (DateTime.Now < deal.BusinessHourOrderTimeE)
                    {
                        // todo: OrderStatus.CancelBeforeClosed 哪裡有在用? 還需要記嗎?
                        OrderFacade.SetOrderStatus(order.Guid, createId, OrderStatus.CancelBeforeClosed, true);
                        CommonFacade.AddAudit(order.Guid, AuditType.Order, "結檔前退貨", createId, false);
                    }

                    if (sendRefundCompleteNoticeMail)
                    {
                        if (RegExRules.CheckEmail(MemberFacade.GetUserEmail(order.UserId)))
                        {
                            if (returnForm.ProgressStatus == ProgressStatus.Completed)
                            {
                                if (order.UserId == ApiLocationDealManager.IDEASMember.UniqueId)
                                {
                                    OrderFacade.SendIDEASMail(IDEASMailContentType.ReturnAll, order, _mp.CashTrustLogGetList(refundedTrustIds));
                                }
                                else if (returnForm.IsSysRefund != null && returnForm.IsSysRefund.Value && returnForm.RefundType == RefundType.Scash)
                                {
                                    OrderFacade.SendCustomerRefundScashCompleteNoticeMail(returnForm, deal);
                                }
                                else
                                {
                                    OrderFacade.SendCustomerRefundCompleteNoticeMail(returnForm, deal);
                                }

                                CommonFacade.AddAudit(order.Guid, AuditType.Order, "寄出處理成功通知", "sys", true);
                            }
                        }
                        else
                        {
                            CommonFacade.AddAudit(order.Guid, AuditType.Order, "消費者Email格式錯誤無法寄出通知", "sys", true);
                        }
                    }
                }
            }
        }

        private static bool RefundCheck(ReturnFormEntity returnForm, string createId, bool immediateRefund)
        {
            if (returnForm.ProgressStatus == ProgressStatus.AtmQueueSucceeded)
            {
                return true;
            }

            if (returnForm.ProgressStatus == ProgressStatus.CompletedWithCreditCardQueued)
            {
                return true;
            }

            if (returnForm.ProgressStatus != ProgressStatus.Processing
                && returnForm.ProgressStatus != ProgressStatus.Retrieving
                && returnForm.ProgressStatus != ProgressStatus.ConfirmedForCS
                && returnForm.ProgressStatus != ProgressStatus.RetrieveToPC
                && returnForm.ProgressStatus != ProgressStatus.RetrieveToCustomer
                && returnForm.ProgressStatus != ProgressStatus.ConfirmedForVendor
                && returnForm.ProgressStatus != ProgressStatus.ConfirmedForUnArrival)
            {
                return false;
            }


            if (!returnForm.IsCreditNoteReceived)
            {
                if (RequireCreditNote(returnForm))
                {
                    //等折讓單寄回後再退款
                    return false;
                }
            }

            //確認可進行退款流程後，才進行退款區間檢查
            if ((returnForm.RefundType == RefundType.Cash
              || returnForm.RefundType == RefundType.Tcash
              || returnForm.RefundType == RefundType.ScashToCash
              || returnForm.RefundType == RefundType.ScashToTcash)
             && returnForm.ProgressStatus == ProgressStatus.Processing)
            {
                Order order = _op.OrderGet(returnForm.OrderGuid);
                if (OldOrderNeedAtm(order, returnForm.ThirdPartyPaymentSystem))
                {
                    if (!AtmRefundAccountExist(returnForm.OrderGuid))
                    {
                        returnForm.ChangeProgressStatus(ProgressStatus.Unreturnable, createId);
                        ReturnFormRepository.Save(returnForm);
                        UpdateOrderProduct(returnForm);

                        string auditMsg = string.Format("退款失敗!{0}天前之信用卡訂單(LinePay{1}天)，請填妥 ATM 退款帳號資訊!", _cp.NoneCreditRefundPeriod.ToString(), _cp.LinePayRefundPeriodLimit);
                        CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, auditMsg, "sys", false);

                        return false;
                    }
                }
                if (InstallmentNeedAtm(order, returnForm.ReturnFormProducts.Where(x => x.IsCollected ?? true).Select(x => x.ReturnFormId)))
                {
                    if (!AtmRefundAccountExist(returnForm.OrderGuid))
                    {
                        returnForm.ChangeProgressStatus(ProgressStatus.Unreturnable, createId);
                        ReturnFormRepository.Save(returnForm);
                        UpdateOrderProduct(returnForm);

                        string auditMsg = string.Format("退款失敗!分期信用卡訂單，部分退貨，請填妥 ATM 退款帳號資訊!", _cp.NoneCreditRefundPeriod.ToString(), _cp.LinePayRefundPeriodLimit);
                        CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, auditMsg, "sys", false);

                        return false;
                    }
                }
            }

            if (returnForm.DeliveryType == DeliveryType.ToHouse)
            {
                if (returnForm.VendorProgressStatus == VendorProgressStatus.Automatic)
                {
                    return true;
                }

                if (returnForm.VendorProgressStatus == VendorProgressStatus.CompletedByCustomerService)
                {
                    return true;
                }

                if (!immediateRefund
                    && (returnForm.VendorProgressStatus != VendorProgressStatus.CompletedAndRetrievied
                        && returnForm.VendorProgressStatus != VendorProgressStatus.CompletedAndNoRetrieving
                        && returnForm.VendorProgressStatus != VendorProgressStatus.CompletedAndUnShip))
                {
                    //等廠商註明完成退貨後再退款
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 退貨單完成後, 要更新訂單貨品明細狀態
        /// </summary>
        private static void ConcludeReturnForm(ReturnFormEntity returnForm)
        {
            if (returnForm.ProgressStatus != ProgressStatus.Completed
                && returnForm.ProgressStatus != ProgressStatus.CompletedWithCreditCardQueued
                && returnForm.ProgressStatus != ProgressStatus.Unreturnable)
            {
                return;
            }

            if (returnForm.RefundType != RefundType.ScashToAtm
                && returnForm.RefundType != RefundType.ScashToCash
                && returnForm.RefundType != RefundType.ScashToTcash)
            {
                UpdateOrderProduct(returnForm);
            }
        }

        private static void UpdateOrderProduct(ReturnFormEntity returnForm)
        {
            if (returnForm.ProgressStatus == ProgressStatus.Unreturnable
                || returnForm.ProgressStatus == ProgressStatus.Canceled)
            {
                OrderProductCollection unreturnableProducts = _op.OrderProductGetList(returnForm.ReturnFormProducts.Select(x => x.OrderProductId));
                foreach (var prod in unreturnableProducts)
                {
                    prod.IsReturning = false;
                    prod.IsReturned = false;
                }

                _op.OrderProductSetList(unreturnableProducts);
                return;
            }

            int refundedCount = returnForm.ReturnFormRefunds.Count(x => x.IsRefunded.GetValueOrDefault(false));
            int productMultiplier = 1;
            if (returnForm.BizModel == BusinessModel.Ppon)
            {
                productMultiplier = QueryComboPackCount(returnForm.OrderGuid);
            }

            List<int> refundedProductIds = returnForm.ReturnFormProducts.
                       Where(x => x.IsCollected.GetValueOrDefault(true)).
                       Take(refundedCount * productMultiplier).
                       Select(x => x.OrderProductId).ToList();

            List<int> unrefundedProductIds = returnForm.ReturnFormProducts.
                       Where(x => refundedProductIds.All(y => y != x.OrderProductId)).
                       Select(x => x.OrderProductId).ToList();

            OrderProductCollection products = _op.OrderProductGetList(returnForm.ReturnFormProducts.Select(x => x.OrderProductId));

            foreach (var prod in products)
            {
                if (refundedProductIds.Any(x => x == prod.Id))
                {
                    prod.IsReturning = false;
                    prod.IsReturned = true;
                }

                if (unrefundedProductIds.Any(x => x == prod.Id))
                {
                    prod.IsReturning = false;
                }
            }

            _op.OrderProductSetList(products);
        }

        private static void UpdateOrderReturnStatusWithExceptionMail(Guid orderGuid)
        {
            try
            {
                UpdateOrderReturnStatus(orderGuid);
            }
            catch (Exception)
            {
                var msg = new MailMessage();
                msg.From = new MailAddress(_cp.AdminEmail);
                msg.To.Add(new MailAddress(_cp.ItTesterEmail));
                msg.Subject = "訂單的退貨狀態未更新!";
                msg.Body = string.Format("{0}/controlroom/maintainance/updateorderreturnstatus?orderGuid={1}", _cp.SiteUrl, orderGuid);
                msg.IsBodyHtml = false;
                PostMan.Instance().Send(msg, SendPriorityType.Normal);
            }
        }


        /// <summary>
        /// 依據退貨單裡的退款方式, 把傳入的 cash_trust_log 退掉並且更新退貨單
        /// </summary>
        /// <param name="returnForm"></param>
        /// <param name="createId"></param>
        /// <param name="immediateRefund">立即退貨</param>
        /// <param name="isForced">強制退款</param>
        /// <returns></returns>
        private static List<Guid> ProcessRefund(ReturnFormEntity returnForm, string createId, bool immediateRefund, bool isForced)
        {
            if (returnForm.ProgressStatus != ProgressStatus.CompletedWithCreditCardQueued)
            {
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Payment, string.Format("退貨單({0}) 開始退款流程", returnForm.Id.ToString()), createId, true);
            }
            bool isInstallment = _op.OrderGet(returnForm.OrderGuid).Installment > 0;
            CashTrustLogCollection refundingCtLogs = GetCashTrustLogsForRefund(returnForm);
            var refundedCtlogs = new CashTrustLogCollection();
            var unrefundedCtlogs = new Dictionary<Guid, string>();
            var retryRefundCtlogs = new List<Guid>();
            var failMessage = string.Empty;

            switch (returnForm.RefundType)
            {
                case RefundType.Scash:

                    #region Scash

                    foreach (CashTrustLog ctlog in refundingCtLogs)
                    {
                        if (!CheckTrustStatus(returnForm.DeliveryType, (TrustStatus)ctlog.Status, isForced, out failMessage))
                        {
                            unrefundedCtlogs.Add(ctlog.TrustId, failMessage);
                            continue;
                        }

                        if (PaymentFacade.CancelPaymentTransactionByCouponForTrust(ctlog, createId, isForced))
                        {
                            refundedCtlogs.Add(ctlog);
                            AuditCtlogRefundSuccess(returnForm.OrderGuid, ctlog, createId);
                        }
                        else
                        {
                            unrefundedCtlogs.Add(ctlog.TrustId, "退購物金失敗");
                        }
                    }

                    /*
                     * CommonFacade.AddAudit 不可放在 ReturnFormRepository.Save 後面!
                     * Nested TransactionScope!! 
                     * 外層有 TransactionScope 且 ReturnFormRepository.Save 內有 TransactionScope,
                     * 若失敗會造成後續 DB 相關的處理失敗.
                     */
                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.OrderDetail, string.Format("本次計 {0} 份退款成功", refundedCtlogs.Count(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).ToString()), createId, true);

                    returnForm.RefundProcessed(refundedCtlogs.Select(x => x.TrustId).ToList(), unrefundedCtlogs, createId, immediateRefund);
                    returnForm.SetFinishTime(DateTime.Now);
                    ReturnFormRepository.Save(returnForm);
                    break;

                #endregion Scash

                case RefundType.Cash:

                    #region Cash

                    if (returnForm.ProgressStatus == ProgressStatus.CompletedWithCreditCardQueued)
                    {
                        CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Payment, string.Format("退貨單({0}) 完成退款流程", returnForm.Id.ToString()), createId, true);

                        /*
                         * Processing - >CompletedWithCreditCardQueued: 讓客服可以取消退貨
                         * CompletedWithCreditCardQueued -> Completed: 退款資料已送出, 無法再取消退貨
                         */
                        returnForm.ChangeProgressStatus(ProgressStatus.Completed, createId);
                        returnForm.SetFinishTime(DateTime.Now);
                        ReturnFormRepository.Save(returnForm);
                        break;
                    }

                    foreach (CashTrustLog ctlog in refundingCtLogs.OrderByDescending(x => x.Status))
                    {
                        if (!CheckTrustStatus(returnForm.DeliveryType, (TrustStatus)ctlog.Status, isForced, out failMessage))
                        {
                            unrefundedCtlogs.Add(ctlog.TrustId, failMessage);
                            continue;
                        }
                        var refundAmount = ctlog.CreditCard;

                        if (isInstallment && unrefundedCtlogs.Count > 0)
                        {
                            unrefundedCtlogs.Add(ctlog.TrustId, "退貨失敗:分期付款未完整退貨");
                        }
                        else
                        {
                            if (PaymentFacade.CancelPaymentTransactionByCouponForTrust(ctlog, createId, isForced))
                            {
                                /*
                                 * 有刷卡金額付款 才需跑刷退流程(有些訂單為使用購物金加刷卡購買 金額分配後可能有些份數無刷卡金)
                                 * 雖無刷退 但仍有購物金退回 需視為退款成功
                                 */
                                if (refundAmount > 0)
                                {
                                    if (PaymentFacade.RefundCreditcardByCouponForTrust(ctlog, createId))
                                    {
                                        refundedCtlogs.Add(ctlog);
                                        AuditCtlogRefundSuccess(returnForm.OrderGuid, ctlog, createId);
                                    }
                                    else
                                    {
                                        unrefundedCtlogs.Add(ctlog.TrustId, "刷退失敗");
                                    }
                                }
                                else
                                {
                                    refundedCtlogs.Add(ctlog);
                                    AuditCtlogRefundSuccess(returnForm.OrderGuid, ctlog, createId);
                                }
                            }
                            else
                            {
                                unrefundedCtlogs.Add(ctlog.TrustId, "刷退前處理:退購物金失敗");
                            }
                        }
                    }

                    /*
                     * CommonFacade.AddAudit 不可放在 ReturnFormRepository.Save 後面!
                     * Nested TransactionScope!! 
                     * 外層有 TransactionScope 且 ReturnFormRepository.Save 內有 TransactionScope,
                     * 若失敗會造成後續 DB 相關的處理失敗.
                     */
                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.OrderDetail, string.Format("本次計 {0} 份退款成功", refundedCtlogs.Count(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).ToString()), createId, true);

                    returnForm.RefundProcessed(refundedCtlogs.Select(x => x.TrustId).ToList(), unrefundedCtlogs, createId, immediateRefund);
                    if (immediateRefund && returnForm.ProgressStatus == ProgressStatus.Completed)
                    {
                        returnForm.ChangeProgressStatus(ProgressStatus.CompletedWithCreditCardQueued, createId);
                    }
                    returnForm.SetFinishTime(DateTime.Now);
                    ReturnFormRepository.Save(returnForm);
                    break;

                #endregion Cash

                case RefundType.Atm:

                    #region Atm

                    if (returnForm.ProgressStatus == ProgressStatus.Processing)
                    {
                        var refundableCtlogs = new CashTrustLogCollection();

                        foreach (CashTrustLog ctlog in refundingCtLogs)
                        {
                            if (!CheckTrustStatus(returnForm.DeliveryType, (TrustStatus)ctlog.Status, isForced, out failMessage))
                            {
                                continue;
                            }

                            refundableCtlogs.Add(ctlog);
                        }

                        PushToAtmQueue(returnForm, refundableCtlogs, createId);
                        break;
                    }

                    if (returnForm.ProgressStatus == ProgressStatus.AtmQueueSucceeded)
                    {
                        foreach (CashTrustLog ctlog in refundingCtLogs)
                        {
                            if (!CheckTrustStatus(returnForm.DeliveryType, (TrustStatus)ctlog.Status, isForced, out failMessage))
                            {
                                unrefundedCtlogs.Add(ctlog.TrustId, failMessage);
                                continue;
                            }

                            var refundAmount = ctlog.CreditCard + ctlog.Atm + ctlog.Tcash + ctlog.FamilyIsp + ctlog.SevenIsp;
                            if (PaymentFacade.CancelPaymentTransactionByCouponForTrust(ctlog, createId, isForced))
                            {
                                /*
                                 * 有現金付款 才需跑退ATM流程(有些訂單為使用購物金加現金購買 金額分配後可能有些份數無現金)
                                 * 雖無退現 但仍有購物金退回 需視為退款成功
                                 */
                                if (refundAmount > 0)
                                {
                                    if (PaymentFacade.RefundAtmByCouponForTrust(ctlog, createId))
                                    {
                                        refundedCtlogs.Add(ctlog);
                                        AuditCtlogRefundSuccess(returnForm.OrderGuid, ctlog, createId);
                                    }
                                    else
                                    {
                                        unrefundedCtlogs.Add(ctlog.TrustId, "ATM退款成功但後續處理失敗");
                                    }
                                }
                                else
                                {
                                    refundedCtlogs.Add(ctlog);
                                    AuditCtlogRefundSuccess(returnForm.OrderGuid, ctlog, createId);
                                }
                            }
                            else
                            {
                                unrefundedCtlogs.Add(ctlog.TrustId, "ATM退款前處理:退購物金失敗");
                            }
                        }

                        /*
                         * CommonFacade.AddAudit 不可放在 ReturnFormRepository.Save 後面!
                         * Nested TransactionScope!! 
                         * 外層有 TransactionScope 且 ReturnFormRepository.Save 內有 TransactionScope,
                         * 若失敗會造成後續 DB 相關的處理失敗.
                         */
                        CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.OrderDetail, string.Format("本次計 {0} 份退款成功", refundedCtlogs.Count(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).ToString()), createId, true);

                        returnForm.RefundProcessed(refundedCtlogs.Select(x => x.TrustId).ToList(), unrefundedCtlogs, createId, immediateRefund);
                        returnForm.SetFinishTime(DateTime.Now);
                        ReturnFormRepository.Save(returnForm);
                    }

                    break;

                #endregion Atm

                case RefundType.Tcash:

                    #region Tcash 第三方

                    foreach (CashTrustLog ctlog in refundingCtLogs)
                    {
                        if (!CheckTrustStatus(returnForm.DeliveryType, (TrustStatus)ctlog.Status, isForced, out failMessage))
                        {
                            unrefundedCtlogs.Add(ctlog.TrustId, failMessage);
                            continue;
                        }
                        var refundAmount = ctlog.Tcash;
                        if (PaymentFacade.CancelPaymentTransactionByCouponForTrust(ctlog, createId, isForced))
                        {
                            /*
                             * 有刷卡金額付款 才需跑刷退流程(有些訂單為使用購物金加刷卡購買 金額分配後可能有些份數無刷卡金)
                             * 雖無刷退 但仍有購物金退回 需視為退款成功
                             */
                            if (refundAmount > 0)
                            {
                                bool refundSuccess;
                                bool canRetry;
                                string failReason = string.Empty;
                                if (PaymentFacade.RefundTcashByCouponForTrust(ctlog, createId, returnForm.Id, out refundSuccess, out canRetry, out failReason, immediateRefund))
                                {
                                    if (refundSuccess)
                                    {
                                        refundedCtlogs.Add(ctlog);
                                        AuditCtlogRefundSuccess(returnForm.OrderGuid, ctlog, createId);
                                    }
                                    else
                                    {
                                        if (canRetry)
                                        {
                                            retryRefundCtlogs.Add(ctlog.TrustId);
                                        }
                                        else
                                        {
                                            unrefundedCtlogs.Add(ctlog.TrustId, string.Format("退{0}失敗, {1}", Helper.GetEnumDescription((ThirdPartyPayment)ctlog.ThirdPartyPayment), failReason));
                                        }
                                    }
                                }
                                else
                                {
                                    unrefundedCtlogs.Add(ctlog.TrustId, string.Format("退{0}失敗", Helper.GetEnumDescription((ThirdPartyPayment)ctlog.ThirdPartyPayment)));
                                }
                            }
                            else
                            {
                                refundedCtlogs.Add(ctlog);
                                AuditCtlogRefundSuccess(returnForm.OrderGuid, ctlog, createId);
                            }
                        }
                        else
                        {
                            unrefundedCtlogs.Add(ctlog.TrustId, string.Format("退{0}前處理:退購物金失敗", Helper.GetEnumDescription((ThirdPartyPayment)ctlog.ThirdPartyPayment)));
                        }
                    }

                    /*
                     * CommonFacade.AddAudit 不可放在 ReturnFormRepository.Save 後面!
                     * Nested TransactionScope!! 
                     * 外層有 TransactionScope 且 ReturnFormRepository.Save 內有 TransactionScope,
                     * 若失敗會造成後續 DB 相關的處理失敗.
                     */
                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.OrderDetail, string.Format("本次計 {0} 份退款成功", refundedCtlogs.Count(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).ToString()), createId, true);
                    returnForm.RefundProcessedForTCash(refundedCtlogs.Select(x => x.TrustId).ToList(), retryRefundCtlogs, unrefundedCtlogs, createId, immediateRefund);
                    if (retryRefundCtlogs.Any())
                    {
                        var tpp = (ThirdPartyPayment)refundingCtLogs.First(x => retryRefundCtlogs.Contains(x.TrustId)).ThirdPartyPayment;
                        foreach (var item in returnForm.ReturnFormRefunds.Where(x => x.IsRefunded != true && x.IsInRetry))
                        {
                            CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.OrderDetail, string.Format("{0}退款失敗, 半小時後自動重試 Trust_id:{1}", Helper.GetEnumDescription(tpp), item.TrustId), createId, true);
                        }
                    }
                    returnForm.SetFinishTime(DateTime.Now);
                    ReturnFormRepository.Save(returnForm);
                    break;

                #endregion Tcash 第三方

                case RefundType.ScashToCash:

                    #region ScashToCash

                    if (returnForm.ProgressStatus == ProgressStatus.Processing)
                    {
                        using (var scope = TransactionScopeBuilder.CreateReadCommitted())
                        {
                            //只須處理實際完成退購物金之份數
                            foreach (CashTrustLog ctlog in refundingCtLogs.Where(x => x.Status == (int)TrustStatus.Returned))
                            {
                                /*
                                 * 有現金付款 才需跑退現流程(有些訂單為使用購物金加現金購買 金額分配後可能有些份數無現金)
                                 * 無現金付款 因已退過購物金 仍算退款成功 無需做失敗處理
                                 */
                                var refundAmount = ctlog.CreditCard;
                                if (refundAmount > 0 && PaymentFacade.RefundCreditcardByCouponForTrust(ctlog, createId))
                                {
                                    refundedCtlogs.Add(ctlog);
                                    AuditCtlogRefundSuccess(returnForm.OrderGuid, ctlog, createId);
                                }
                            }

                            if (refundedCtlogs.Count > 0)
                            {
                                returnForm.ChangeProgressStatus(ProgressStatus.Completed);
                            }
                            else
                            {
                                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Payment, string.Format("退貨單{0} - 轉刷退失敗!", returnForm.Id), createId, true);
                                returnForm.ChangeProgressStatus(ProgressStatus.Unreturnable);
                            }

                            returnForm.SetFinishTime(DateTime.Now);
                            ReturnFormRepository.Save(returnForm);
                            scope.Complete();
                        }
                    }

                    break;

                #endregion ScashToCash

                case RefundType.ScashToAtm:

                    #region ScashToAtm

                    if (returnForm.ProgressStatus == ProgressStatus.Processing)
                    {
                        //只須處理實際完成退購物金之份數
                        foreach (CashTrustLog ctlog in refundingCtLogs.Where(x => x.Status == (int)TrustStatus.Returned))
                        {
                            /*
                             * 有現金付款 才需跑退現流程(有些訂單為使用購物金加現金購買 金額分配後可能有些份數無現金)
                             * 無現金付款 因已退過購物金 仍算退款成功 無需做失敗處理
                             */
                            var refundAmount = ctlog.CreditCard + ctlog.Atm + ctlog.Tcash + ctlog.FamilyIsp + ctlog.SevenIsp;
                            if (refundAmount > 0)
                            {
                                refundedCtlogs.Add(ctlog);
                            }
                        }

                        if (refundedCtlogs.Count > 0)
                        {
                            PushToAtmQueue(returnForm, refundedCtlogs, createId);
                        }
                        else
                        {
                            CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Payment, string.Format("退貨單{0} - 無可轉退 ATM 金額!", returnForm.Id), createId, true);
                            returnForm.ChangeProgressStatus(ProgressStatus.Unreturnable);
                            returnForm.SetFinishTime(DateTime.Now);
                            ReturnFormRepository.Save(returnForm);
                        }
                    }
                    else if (returnForm.ProgressStatus == ProgressStatus.AtmQueueSucceeded)
                    {
                        /* 確定ATM退款結果為成功 才更新cash_trust_log status */
                        //只須處理實際完成退購物金之份數
                        foreach (CashTrustLog ctlog in refundingCtLogs.Where(x => x.Status == (int)TrustStatus.Returned))
                        {
                            var refundAmount = ctlog.CreditCard + ctlog.Atm + ctlog.Tcash + ctlog.FamilyIsp + ctlog.SevenIsp;
                            if (refundAmount > 0 && PaymentFacade.RefundAtmByCouponForTrust(ctlog, createId))
                            {
                                refundedCtlogs.Add(ctlog);
                                AuditCtlogRefundSuccess(returnForm.OrderGuid, ctlog, createId);
                            }
                        }
                        if (refundedCtlogs.Count > 0)
                        {
                            returnForm.ChangeProgressStatus(ProgressStatus.Completed);
                        }
                        else
                        {
                            CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Payment, string.Format("退貨單{0} - 轉退 ATM 失敗!", returnForm.Id), createId, true);
                            returnForm.ChangeProgressStatus(ProgressStatus.Unreturnable);
                        }
                        returnForm.ChangeProgressStatus(ProgressStatus.Completed);
                        returnForm.SetFinishTime(DateTime.Now);
                        ReturnFormRepository.Save(returnForm);
                    }

                    break;

                #endregion ScashToAtm

                case RefundType.ScashToTcash:

                    #region ScashToTcash

                    if (returnForm.ProgressStatus == ProgressStatus.Processing)
                    {
                        using (var scope = TransactionScopeBuilder.CreateReadCommitted())
                        {
                            string failReason = string.Empty;
                            //只須處理實際完成退購物金之份數
                            foreach (CashTrustLog ctlog in refundingCtLogs.Where(x => x.Status == (int)TrustStatus.Returned))
                            {
                                /*
                                 * 有現金付款 才需跑退現流程(有些訂單為使用購物金加現金購買 金額分配後可能有些份數無現金)
                                 * 無現金付款 因已退過購物金 仍算退款成功 無需做失敗處理
                                 */
                                var refundAmount = ctlog.Tcash;
                                bool refundSuccess;
                                bool canRetry;
                                if (refundAmount > 0 && PaymentFacade.RefundTcashByCouponForTrust(ctlog, createId, returnForm.Id, out refundSuccess, out canRetry, out failReason, immediateRefund))
                                {
                                    if (refundSuccess)
                                    {
                                        refundedCtlogs.Add(ctlog);
                                        AuditCtlogRefundSuccess(returnForm.OrderGuid, ctlog, createId);
                                    }
                                    else
                                    {
                                        if (canRetry)
                                        {
                                            retryRefundCtlogs.Add(ctlog.TrustId);
                                        }
                                    }
                                }
                            }

                            if (refundedCtlogs.Count > 0)
                            {
                                returnForm.ChangeProgressStatus(ProgressStatus.Completed);
                            }
                            else if (retryRefundCtlogs.Count > 0)
                            {
                                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Payment, string.Format("退貨單{0} - 轉刷退失敗, 半小時後進行重試!", returnForm.Id), createId, true);
                            }
                            else
                            {
                                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Payment, string.Format("退貨單{0} - 轉刷退失敗({1})!", returnForm.Id, failReason), createId, true);
                                returnForm.ChangeProgressStatus(ProgressStatus.Unreturnable);
                            }

                            returnForm.SetFinishTime(DateTime.Now);
                            ReturnFormRepository.Save(returnForm);
                            scope.Complete();
                        }
                    }

                    break;

                #endregion ScashToTcash

                default:
                    break;
            }

            return refundedCtlogs.Select(x => x.TrustId).ToList();
        }

        private static void PushToAtmQueue(ReturnFormEntity returnForm, CashTrustLogCollection refundingCtLogs, string createId)
        {
            if (QueueAtm(returnForm, refundingCtLogs, createId))
            {
                CashTrustLog shipping = refundingCtLogs.FirstOrDefault(x => Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight));
                if (shipping != null)
                {
                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, "退運費" + shipping.Amount + "元", createId, true);
                }

                int refundCount = refundingCtLogs.Count;
                if (shipping != null)
                {
                    refundCount--;
                }

                if (returnForm.DeliveryType == DeliveryType.ToHouse)
                {
                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, string.Format("共{0}份申請ATM退貨中", refundCount.ToString()), createId, true);
                }
                else
                {
                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, string.Format("共{0}張憑證申請ATM退貨中", refundCount.ToString()), createId, true);
                }

            }
            else
            {
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, "ATM退貨申請失敗 (QueueAtm失敗), 請洽技術部檢查ATM退款帳戶!", createId, true);
            }
        }

        private static bool QueueAtm(ReturnFormEntity returnForm, CashTrustLogCollection refundingCtLogs, string createId)
        {
            CtAtmRefund atmR = _op.CtAtmRefundGetLatest(returnForm.OrderGuid);
            if (atmR.ReturnFormId.HasValue)
            {
                return false;
            }

            var agg = refundingCtLogs.Aggregate(new { Amount = 0, TrustIds = "" }, (seed, ctlog) =>
                    new
                    {
                        Amount = seed.Amount + ctlog.Atm + ctlog.CreditCard + ctlog.Tcash + ctlog.FamilyIsp + ctlog.SevenIsp,
                        TrustIds = seed.TrustIds == "" ?
                            ctlog.TrustId.ToString() :
                            string.Join(";", seed.TrustIds, ctlog.TrustId.ToString())
                    });


            atmR.Amount = agg.Amount;
            atmR.CouponList = agg.TrustIds;
            atmR.Status = (int)AtmRefundStatus.Request;
            atmR.ReturnFormId = returnForm.Id;

            if (returnForm.ChangeProgressStatus(ProgressStatus.AtmQueueing))
            {
                DateTime now = DateTime.Now;
                foreach (CashTrustLog ctlog in refundingCtLogs)
                {
                    ctlog.SpecialStatus = (int)Helper.SetFlag(true, ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding);
                    ctlog.SpecialOperatedTime = now;
                }
                _mp.CashTrustLogCollectionSet(refundingCtLogs);

                _op.CtAtmRefundSet(atmR);
                ReturnFormRepository.Save(returnForm);
                return true;
            }

            return false;
        }

        public static void AuditCtlogRefundSuccess(Guid orderGuid, CashTrustLog ctlog, string createId)
        {
            bool isFreight = Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.Freight);
            bool isDelivery = !isFreight && string.IsNullOrWhiteSpace(ctlog.CouponSequenceNumber);
            bool isCoupon = !isFreight && !isDelivery;

            if (isFreight)
            {
                CommonFacade.AddAudit(orderGuid, AuditType.Refund, "退運費" + ctlog.Amount + "元", createId, true);
            }

            if (isCoupon)
            {
                CommonFacade.AddAudit(orderGuid, AuditType.Refund, "退憑證:#" + ctlog.CouponSequenceNumber, createId, true);
            }

            if (isDelivery)
            {
                CommonFacade.AddAudit(orderGuid, AuditType.Refund, "退款項:" + ctlog.TrustId.ToString(), createId, true);
            }
        }


        /// <summary>
        /// 依據退貨單的資料撈出 cash_trust_log
        /// </summary>
        /// <param name="returnForm"></param>
        /// <returns></returns>
        private static CashTrustLogCollection GetCashTrustLogsForRefund(ReturnFormEntity returnForm)
        {
            CashTrustLogCollection result;
            List<Guid> trustIds;
            if (returnForm.DeliveryType == DeliveryType.ToHouse)
            {
                trustIds = returnForm.ReturnFormRefunds
                                     .Where(x => !x.VendorNoRefund)
                                     .Select(x => x.TrustId)
                                     .ToList();

                result = _mp.CashTrustLogGetList(trustIds);
            }
            else
            {
                trustIds = returnForm.ReturnFormRefunds
                                     .Select(x => x.TrustId)
                                     .ToList();

                result = _mp.CashTrustLogGetList(trustIds);
            }

            return result;
        }

        /// <summary>
        /// 是否有發票 (讓消費者下載折讓單)
        /// </summary>
        /// <param name="returnFormId"></param>
        /// <returns></returns>
        public static bool HasEinvoice(int returnFormId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(returnFormId);

            if (returnForm == null)
            {
                return false;
            }

            List<EinvoiceMain> invoices = _op.EinvoiceMainGetListByOrderGuid(returnForm.OrderGuid).ToList();

            /*
             * 檢查 [einvoice_main].[verified_time] 有沒有值
             * 有值代表近期內一定會開發票
             * 20161128 大米:改成一定要有發票號...所以改抓invoice_number
             */

            if (returnForm.DeliveryType == DeliveryType.ToShop)
            {
                List<int> couponIds = GetCouponIds(returnForm);

                List<EinvoiceMain> relatedInvoices = invoices.
                    Where(x => x.CouponId.HasValue && couponIds.Contains(x.CouponId.Value)).
                    ToList();
                //成套禮券發票無couponId
                relatedInvoices.AddRange(GroupCouponOrder(invoices));
                return relatedInvoices.Where(x => x.InvoiceNumber != null).Where(x => x.InvoiceNumber != "").Any(einvoice => einvoice.VerifiedTime.HasValue);
            }
            else
            {
                //宅配其實只有一張發票 
                return invoices.Where(x => x.InvoiceNumber != null).Where(x => x.InvoiceNumber != "").Any(einvoice => einvoice.VerifiedTime.HasValue);
            }
        }

        /// <summary>
        /// 是否有需要讓消費者下載折讓單(朝令夕改，請小心謹慎)
        /// </summary>
        /// <param name="returnFormId"></param>
        /// <returns></returns>
        public static bool IsPaperAllowance(int returnFormId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(returnFormId);

            if (returnForm == null)
            {
                return false;
            }

            List<EinvoiceMain> invoices = _op.EinvoiceMainGetListByOrderGuid(returnForm.OrderGuid).ToList();

            if (invoices == null)
            {
                return false;
            }

            return (invoices.Any(x => x.AllowanceStatus == (int)AllowanceStatus.ElecAllowance) ? false : true);
        }


        /// <summary>
        /// 是否需要紙本折讓單
        /// </summary>
        /// <returns></returns>
        public static bool RequireCreditNote(ReturnFormEntity returnForm)
        {
            if (returnForm.IsCreditNoteReceived)
            {
                return false;
            }

            //是否需要折讓單 = 檢查所有的發票是否符合折讓規則
            List<EinvoiceMain> invoices = _op.EinvoiceMainGetListByOrderGuid(returnForm.OrderGuid).Where(x => x.InvoiceStatus != (int)EinvoiceType.C0501).ToList();

            bool isPartialCancel = false;

            if (returnForm.DeliveryType == DeliveryType.ToShop)
            {
                List<int> couponIds = GetCouponIds(returnForm);

                List<EinvoiceMain> relatedInvoices = invoices.
                    Where(x => x.CouponId.HasValue && couponIds.Contains(x.CouponId.Value)).
                    ToList();
                //成套禮券發票無couponId
                var groupCoupon = GroupCouponOrder(invoices);
                if (groupCoupon.Count > 0)
                {
                    relatedInvoices.AddRange(groupCoupon);
                    //成套票券需檢查是否部分退貨
                    CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(returnForm.OrderGuid, OrderClassification.LkSite);
                    isPartialCancel = (ctlogs.Count - couponIds.Count) > 0;
                }

                // 沒有發票就不需要折讓單 (return false), 所以要用 Any 擋掉
                return relatedInvoices.Any()
                    && relatedInvoices.All(einvoice => !EInvoiceConformsCreditNotePolicy(einvoice, returnForm.Id, isPartialCancel));
            }
            else
            {
                //檢查是否為部分退貨
                int returnCount = returnForm.ReturnFormProducts.Count(x => x.IsCollected.GetValueOrDefault(true));
                int orderProductCount = _op.OrderProductGetListByOrderGuid(returnForm.OrderGuid)
                                                .Count(x => x.IsCurrent && !x.IsReturned);
                isPartialCancel = (orderProductCount - returnCount) > 0;

                //宅配只有一張發票 
                //沒有發票就不需要折讓單 (return false), 所以要用 Any 擋掉
                return invoices.Any()
                    && invoices.All(einvoice => !EInvoiceConformsCreditNotePolicy(einvoice, returnForm.Id, isPartialCancel));
            }
        }


        /// <summary>
        /// 發票是否符合折讓規則(退貨會判斷 要特別注意)
        /// </summary>
        /// <param name="einvoice"></param>
        /// <returns></returns>
        public static bool EInvoiceConformsCreditNotePolicy(EinvoiceMain einvoice, int returnFormId, bool isPartialCancel = false)
        {
            //todo:未開發票的退貨預期部份退是否還是可以判斷需不需要折讓

            #region 沒開發票可退貨 or 三聯當期未印製 or 上期三聯但在緩衝期間內且未印製

            if (!einvoice.InvoiceNumberTime.HasValue)
            {
                return true;
            }

            if (!isPartialCancel && ((!einvoice.IsIntertemporal && !einvoice.InvoicePaperedTime.HasValue && einvoice.IsEinvoice3) ||
                (einvoice.IsIntertemporal && einvoice.IsEinvoice3 && !einvoice.InvoicePaperedTime.HasValue && !einvoice.IsIntertemporalWithBuffer
                )))
            {
                return true;
            }

            #endregion  沒開發票可退貨 or 三聯當期未印製 or 上期三聯但在緩衝期間內且未印製

            #region 三聯式發票且已印製 => 寄回折讓單才可退貨

            if (einvoice.IsEinvoice3 && einvoice.InvoicePapered)
            {
                return false;
            }

            #endregion 三聯式發票且已印製 => 寄回折讓單才可退貨

            #region 紙本折讓 => 寄回折讓單才可退貨

            if (einvoice.AllowanceStatus.GetValueOrDefault((int)AllowanceStatus.PaperAllowance) == (int)AllowanceStatus.PaperAllowance)
            {
                if (WithinCurrentInvoicePeriod(einvoice.InvoiceNumberTime.Value))
                {
                    #region 當期 => 有索取發票的訂單 or 宅配訂單:部分退貨(因稅額有落差) 需寄回折讓單才可退貨 or 捐贈

                    //todo:強制退貨未判斷折單?
                    if (einvoice.InvoiceRequestTime.HasValue || isPartialCancel)
                    {
                        return false;
                    }

                    if (!string.IsNullOrEmpty(einvoice.LoveCode))
                    {
                        //憑證:退單上的憑證如全部核銷則不需折單 宅配:需折單

                        var list = _op.ViewCashTrustLogRetrnFormListCollectionGet(returnFormId);
                        return list.All(x => x.UsageVerifiedTime != null) && !(list.Any(x => x.DeliveryType == (int)DeliveryType.ToHouse));
                    }

                    #endregion 當期 => 有索取發票的訂單 or 宅配訂單:部分退貨(因稅額有落差) 需寄回折讓單才可退貨
                }
                else
                {
                    #region 跨期 => 寄回折讓單才可退貨

                    return false;

                    #endregion 跨期 => 寄回折讓單才可退貨
                }
            }

            #endregion 紙本折讓 => 寄回折讓單才可退貨

            return true;
        }


        /// <summary>
        /// 檢查退貨數量是否符合成套販售的倍數
        /// </summary>
        public static bool PponComboPackCheck(Guid orderGuid, int returnProductCount)
        {
            int comboCount = QueryComboPackCount(orderGuid);

            return int.Equals(0, returnProductCount % comboCount);
        }

        public static int QueryComboPackCount(Guid orderGuid)
        {
            ViewPponOrder order = _pp.ViewPponOrderGet(orderGuid);

            if (order == null || !order.IsLoaded)
            {
                return 1;
            }

            Guid bid = order.BusinessHourGuid;
            DealProperty dp = _pp.DealPropertyGet(bid);
            if (dp == null || !dp.IsLoaded)
            {
                return 1;
            }

            if (int.Equals(0, dp.ComboPackCount))
            {
                return 1;
            }

            if (dp.ComboPackCount < 0)
            {
                throw new InvalidOperationException("成套販售的數量設定有問題!");
            }

            return dp.ComboPackCount;
        }

        public static bool Cancel(ReturnFormEntity returnForm, string userId)
        {
            if (!returnForm.IsCancelable())
            {
                return false;
            }

            using (var scope = TransactionScopeBuilder.CreateReadCommitted(new TimeSpan(0, 30, 0)))
            {
                // 取得該筆訂單的運費項目
                Guid? freightTrustId = null;
                CashTrustLog freight = GetFreight(returnForm.OrderGuid);
                // 檢查是否已有退款完成的運費 
                if (freight != null &&
                    (freight.Status == (int)TrustStatus.Refunded ||
                     freight.Status == (int)TrustStatus.Returned))
                {
                    freightTrustId = freight.TrustId;
                }

                var refundedTrustIds = new List<Guid>();

                // 已經退款的要把款項取回. ATM 款項取不回來所以例外
                if ((returnForm.ProgressStatus == ProgressStatus.Completed
                        && returnForm.RefundType != RefundType.Atm)
                    || returnForm.ProgressStatus == ProgressStatus.CompletedWithCreditCardQueued)
                {
                    refundedTrustIds = returnForm.ReturnFormRefunds
                                        .Where(x => x.IsRefunded.GetValueOrDefault(false))
                                        .Select(x => x.TrustId)
                                        .ToList();
                    // 取消退貨須將運費取回
                    if (freightTrustId.HasValue &&
                        !refundedTrustIds.Contains(freightTrustId.Value))
                    {
                        refundedTrustIds.Add(freightTrustId.Value);
                    }

                    if (!UndoRefund(refundedTrustIds, userId))
                    {
                        scope.Complete();
                        return false;
                    }
                }

                // ATM 失敗的可以取消
                // ATM 不需要 UndoRefund, 因為 ATM 退款要先成功才會去退其他的款項.
                // 如果是憑證型的話, 需要拿掉標記 ATM 退貨中.
                if (returnForm.ProgressStatus == ProgressStatus.AtmFailed
                    && returnForm.DeliveryType == DeliveryType.ToShop)
                {
                    List<Guid> allTrustIds = returnForm.ReturnFormRefunds.Select(x => x.TrustId).ToList();
                    RemoveAtmRefundingFlag(allTrustIds, userId);
                }

                if (returnForm.ChangeProgressStatus(ProgressStatus.Canceled, userId))
                {
                    returnForm.SetFinishTime(DateTime.Now);

                    // 若廠商處理進度為:[無法退貨:客服未處理]
                    // 按下取消退貨後;需更新狀態為:[無法退貨:客服已處理]
                    if (returnForm.VendorProgressStatus == VendorProgressStatus.Unreturnable)
                    {
                        returnForm.VendorUnreturnableProcessed(userId);
                    }

                    // 更新return_form_refund 退款狀態為未退款
                    returnForm.UndoReturnFormRefund(freightTrustId);

                    ReturnFormRepository.Save(returnForm);
                    UpdateOrderProduct(returnForm);

                    int memberUniqueId = MemberFacade.GetUniqueId(userId);
                    //更新對帳單明細 將balance_sheet_detail status標註為undo且無對應deduction回復為normal
                    foreach (var trustId in refundedTrustIds)
                    {
                        BalanceSheetService.RecoverUndoDetail(trustId, memberUniqueId, "取消退款");
                    }
                }
                else
                {
                    return false;
                }

                scope.Complete();
            }

            UpdateOrderReturnStatusWithExceptionMail(returnForm.OrderGuid);

            return true;
        }

        public static bool RequiresAtm(Guid orderGuid, ThirdPartyPayment tpp)
        {
            Order order = _op.OrderGet(orderGuid);

            if (order == null || !order.IsLoaded)
            {
                throw new InvalidOperationException(string.Format("order not found! order_guid: {0}", orderGuid.ToString()));
            }

            return OrderHasAtmPayment(order) || OldOrderNeedAtm(order, tpp);
        }


        /// <summary>
        /// 退現的發票檢查規則: 檢查發票是否已開立及開立之相關規則
        /// </summary>
        public static bool CashRefundInvoicePolicyCheck(Guid orderGuid)
        {
            /* 參數是否該用退貨單? */

            //宅配只有一張發票, 憑證每張都有發票
            // 1. 如果沒開發票的話 => 可退貨
            // 2. 有開發票的話, 未跨期則發票需寄回, 跨期則需要折讓單

            List<EinvoiceMain> invoices = _op.EinvoiceMainGetListByOrderGuid(orderGuid)
                .Where(x => x.InvoiceNumber != null).ToList();  // 有開立的發票

            if (int.Equals(0, invoices.Count))
            {
                return true;
            }

            OrderReturnList returnForm = _op.OrderReturnListGetbyType(orderGuid, (int)OrderReturnType.Refund);
            List<int?> couponIds = GetCouponIds(returnForm);

            return invoices.All(x =>
            {
                if (couponIds.Contains(x.CouponId))
                {
                    return x.InvoiceMailbackAllowance.GetValueOrDefault(false)  //折讓單有回來
                           || WithinCurrentInvoicePeriod(x.InvoiceNumberTime.Value);  //當期發票
                }
                return true;
            });
        }


        /// <summary>
        /// 補 Ppon 信託資料
        /// </summary>
        public static string PatchCashTrustLog(Guid orderGuid, string createId)
        {
            string msg = string.Empty;
            if (_pp.ViewPponCashTrustLogGetListByOrderGuid(orderGuid).Count == 0)
            {
                ViewPponCouponCollection coupons = _pp.ViewPponCouponGetListRegularOnly(ViewPponCoupon.Columns.OrderGuid, orderGuid);

                if (coupons.Count > 0 && coupons.First().CouponId != null)
                {
                    Member member = _mp.MemberGet(coupons.First().MemberEmail);
                    if (member != null)
                    {
                        IEnumerable<PaymentTransaction> ptCol = _op.PaymentTransactionGetList(orderGuid).Where(x => x.TransType == (int)PayTransType.Authorization);
                        IEnumerable<PaymentTransaction> ptPcash = ptCol.Where(x => x.PaymentType == (int)Core.PaymentType.PCash);
                        IEnumerable<PaymentTransaction> ptBcash = ptCol.Where(x => x.PaymentType == (int)Core.PaymentType.BonusPoint);
                        IEnumerable<PaymentTransaction> ptScash = ptCol.Where(x => x.PaymentType == (int)Core.PaymentType.SCash);
                        PaymentTransaction ptCreditcard = (ptCol.Count() > 0) ? _op.PaymentTransactionGet(ptCol.First().TransId, Core.PaymentType.Creditcard, PayTransType.Authorization) : null;
                        var odBcash = _op.OrderDetailGetList(orderGuid).Where(x => x.Status == (int)OrderDetailStatus.SystemEntry && x.ItemName.Contains("紅利購物金折抵"));

                        int ccash = (ptCreditcard != null) ? Convert.ToInt32(ptCreditcard.Amount) : 0;
                        int pcash = (ptPcash.Count() > 0) ? Convert.ToInt32(ptCol.Where(x => x.PaymentType == (int)Core.PaymentType.PCash).First().Amount) : 0;
                        int bcash = (ptBcash.Count() > 0)
                                    ? Convert.ToInt32(ptCol.Where(x => x.PaymentType == (int)Core.PaymentType.BonusPoint).First().Amount) : 0;

                        //避免payment_transaction因全額紅利折抵 amount為0 or 使用紅利金無payment_transaction紀錄 的狀況 導致bcash誤抓為0
                        //若bcash為0的狀態下 再去order_detail確認看看是否有需補入紅利金資料
                        if (bcash == 0)
                        {
                            bcash = odBcash.Count() > 0 ? Convert.ToInt32((-1) * odBcash.First().Total.GetValueOrDefault(0)) : 0;
                        }

                        int scash = (ptScash.Count() > 0) ? Convert.ToInt32(ptCol.Where(x => x.PaymentType == (int)Core.PaymentType.SCash).First().Amount) - ccash : 0;

                        //抓取發票金額以還原當時未開立發票金額
                        int uninvoiced = 0;
                        var invoices = _op.EinvoiceMainGetListByOrderGuid(orderGuid).Where(x => x.InvoiceStatus != (int)EinvoiceType.C0701).ToList();
                        if (invoices.Any())
                        {
                            uninvoiced = (int)invoices.Sum(x => x.OrderAmount);
                        }
                        else
                        {
                            //舊訂單皆為購買後開立發票 故無需考慮會有未開立發票購物金問題 
                            //若無einvoice_main資料 則未開立發票金額以刷卡金額為主
                            uninvoiced = ccash;
                        }

                        CashTrustLogCollection ctlc = new CashTrustLogCollection();

                        foreach (var coupon in coupons)
                        {
                            OldCashTrustLog ctOld = (coupon.CouponId != null) ? _mp.OldCashTrustLogGetByCouponId(coupon.CouponId.Value) : null;

                            #region 補cash_trust_log

                            CashTrustLog ct = new CashTrustLog();
                            ct.TrustId = Guid.NewGuid();
                            ct.Amount = Convert.ToInt32(coupon.ItemUnitPrice);
                            ct.TrustProvider = (int)TrustProvider.TaiShin;
                            ct.Status = (ctOld != null) ? ctOld.Status : (int)TrustStatus.Initial;
                            ct.CreateTime = coupon.OrderDetailCreateTime;
                            ct.ModifyTime = coupon.OrderDetailCreateTime;
                            ct.CouponSequenceNumber = coupon.SequenceNumber;
                            ct.CouponId = coupon.CouponId;
                            ct.UserId = member.UniqueId;
                            ct.OrderGuid = orderGuid;
                            ct.OrderDetailGuid = (coupon.OrderDetailId != null) ? coupon.OrderDetailId.Value : new Guid();
                            ct.ItemName = coupon.ItemName;
                            ct.OrderClassification = (int)OrderClassification.LkSite;
                            ct.BusinessHourGuid = coupon.BusinessHourGuid;

                            #region 分配款項

                            int total = ct.Amount;
                            if (total > 0 && ccash > 0)
                            {
                                ct.CreditCard = (ccash >= total) ? total : ccash;
                                total -= ct.CreditCard;
                                ccash -= ct.CreditCard;
                            }

                            if (total > 0 && pcash > 0)
                            {
                                ct.Pcash = (pcash >= total) ? total : pcash;
                                total -= ct.Pcash;
                                pcash -= ct.Pcash;
                            }

                            if (total > 0 && bcash > 0)
                            {
                                ct.Bcash = (bcash >= total) ? total : bcash;
                                total -= ct.Bcash;
                                bcash -= ct.Bcash;
                            }

                            if (total > 0 && scash > 0)
                            {
                                ct.Scash = (scash >= total) ? total : scash;
                                total -= ct.Scash;
                                scash -= ct.Scash;
                            }

                            #region 紀錄刷卡未開立發票金額(cash_trust_log建立後 方有ATM付款方式存在)

                            if (uninvoiced > 0)
                            {
                                ct.UninvoicedAmount = (uninvoiced - ct.CreditCard > 0) ? ct.CreditCard : uninvoiced;
                                uninvoiced -= ct.UninvoicedAmount;
                            }

                            #endregion 紀錄刷卡未開立發票金額(cash_trust_log建立後 方有ATM付款方式存在)

                            ctlc.Add(ct);

                            #endregion 分配款項

                            CommonFacade.AddAudit(orderGuid, AuditType.Order, "補入憑證#" + coupon.SequenceNumber + "信託資料", createId, true);

                            #endregion 補cash_trust_log
                        }

                        #region 紀錄購物金未開立發票金額

                        foreach (var ctl in ctlc)
                        {
                            if (uninvoiced > 0)
                            {
                                int newScash = (uninvoiced - ctl.Scash > 0) ? ctl.Scash : uninvoiced;
                                ctl.UninvoicedAmount += newScash;
                                uninvoiced -= newScash;
                            }
                        }

                        #endregion 紀錄購物金未開立發票金額

                        _mp.CashTrustLogCollectionSet(ctlc);
                    }
                    else
                    {
                        msg = "查無會員，無法補入資料";
                        CommonFacade.AddAudit(orderGuid, AuditType.Order, msg, createId, true);
                    }
                }
                else
                {
                    msg = "查無憑證，無法補入資料";
                    CommonFacade.AddAudit(orderGuid, AuditType.Order, msg, createId, true);
                }
            }
            else
            {
                msg = "已有信託資料，無法再補入資料";
                CommonFacade.AddAudit(orderGuid, AuditType.Order, msg, createId, true);
            }

            return msg;
        }


        /// <summary>
        /// 訂單是否需要設定 ATM 退款帳號
        /// </summary>
        public static bool NeedToSetAtmRefundAccount(Guid orderGuid, string requestUserId, ThirdPartyPayment tpp)
        {
            Order order = _op.OrderGet(orderGuid);
            if (order == null || !order.IsLoaded)
            {
                return false;
            }

            return (OrderHasAtmPayment(order) || OldOrderNeedAtm(order, tpp))
                   && !AtmRefundAccountExist(order.Guid);
        }

        public static bool OrderHasCreditCardPayment(Order order)
        {
            CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(order.Guid, OrderClassification.LkSite);
            return ctlogs.Any(ctlog => ctlog.CreditCard > 0);
        }

        public static bool OrderHasCreditCardPayment(CashTrustLogCollection ctlc)
        {
            return ctlc.Any(ctlog => ctlog.CreditCard > 0);
        }

        public static bool OrderHasThirdPartyPayment(Order order, out ThirdPartyPayment paymentSystem)
        {
            CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(order.Guid, OrderClassification.LkSite);
            paymentSystem = ctlogs.Count > 0 ? (ThirdPartyPayment)ctlogs.First().ThirdPartyPayment : ThirdPartyPayment.None;
            return ctlogs.Any(ctlog => ctlog.Tcash > 0);
        }

        public static bool OrderHasThirdPartyPayment(CashTrustLogCollection ctlc, out ThirdPartyPayment paymentSystem)
        {
            paymentSystem = (ThirdPartyPayment)ctlc.First().ThirdPartyPayment;
            return ctlc.Any(ctlog => ctlog.Tcash > 0);
        }

        public static bool OrderHasCashOnDelivery(Order order)
        {
            return Helper.IsFlagSet(order.OrderStatus, OrderStatus.FamilyIspOrder) || Helper.IsFlagSet(order.OrderStatus, OrderStatus.SevenIspOrder);
        }

        public static bool OrderHasAtmPayment(Order order)
        {
            return Helper.IsFlagSet(order.OrderStatus, OrderStatus.ATMOrder);
        }

        /// <summary>
        /// 信用卡、Line 超過特定期限，台新儲值支付退貨，超取超付已取貨，一律走ATM退款
        /// </summary>
        /// <param name="order"></param>
        /// <param name="tpp"></param>
        /// <returns></returns>
        public static bool OldOrderNeedAtm(Order order, ThirdPartyPayment tpp)
        {
            CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(order.Guid, OrderClassification.LkSite);
            if (tpp == ThirdPartyPayment.TaishinPay ||
                (tpp == ThirdPartyPayment.LinePay && order.CreateTime < DateTime.Now.AddDays(-_cp.LinePayRefundPeriodLimit)) ||
                order.CreateTime < DateTime.Now.AddDays(-_cp.NoneCreditRefundPeriod) ||
                ctlogs.Any(ctlog => (ctlog.FamilyIsp + ctlog.SevenIsp > 0) && ctlog.Status <= (int)TrustStatus.Returned)
                )
            {
                return ctlogs.Any(ctlog => ctlog.CreditCard + ctlog.Atm + ctlog.Tcash + ctlog.FamilyIsp + ctlog.SevenIsp > 0);
            }

            return false;
        }

        /// <summary>
        /// 以RefundOrderInfo查詢是否以ATM退款
        /// </summary>
        /// <param name="orderInfo"></param>
        /// <returns></returns>
        public static bool IsAtmRefund(RefundOrderInfo orderInfo)
        {
            //若為 1. ATM單 2.超過360天前以致無法刷退的訂單 3.使用購物金 4.LinePay超過60天,則導到指定帳戶退款頁面以利填寫帳號 
            //     5.TaishinPay退貨api未接上之前, 一律以ATM退
            if (orderInfo == null)
            {
                throw new Exception("OrderInfo is null!");
            }
            if (!orderInfo.Coupons.Any())
            {
                throw new Exception("Coupons is null!");
            }

            var main = orderInfo.Coupons.First();

            if ((main.OrderStatus & (int)OrderStatus.ATMOrder) > 0 ||
                 orderInfo.ThirdPartyPaymentSystem == ThirdPartyPayment.TaishinPay ||
                 (orderInfo.ThirdPartyPaymentSystem == ThirdPartyPayment.LinePay &&
                 main.CreateTime < DateTime.Now.AddDays(-_config.LinePayRefundPeriodLimit)) ||
                 main.CreateTime < DateTime.Now.AddDays(-_config.NoneCreditRefundPeriod) ||
                 (orderInfo.HasPartialCancelOrVerified && orderInfo.IsInstallment) || //部分退且付款方式為分期
                 orderInfo.IsPaidByIsp) //超取超付
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 分期付款且部份退貨，走ATM退款
        /// </summary>
        /// <param name="order"></param>
        /// <param name="returnList"></param>
        /// <returns></returns>
        public static bool InstallmentNeedAtm(Order order, IEnumerable<int> returnList)
        {
            CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(order.Guid);
            PponOrder po = new PponOrder(order.Guid);

            if (order.Installment > 0 &&
                ((ctlogs.Any(x => x.DeliveryType == (int)DeliveryType.ToShop) && ctlogs.Count != returnList.Count()) ||
                 (ctlogs.Any(x => x.DeliveryType == (int)DeliveryType.ToHouse) && po.Products.Count != returnList.Count()) ||
                 order.IsPartialCancel))
            {
                return ctlogs.Any(ctlog => ctlog.CreditCard + ctlog.Atm + ctlog.Tcash + ctlog.FamilyIsp + ctlog.SevenIsp > 0);
            }

            return false;
        }

        public static bool AtmRefundAccountExist(Guid orderGuid)
        {
            CtAtmRefund latestAtm = _op.CtAtmRefundGetLatest(orderGuid);
            return latestAtm.IsLoaded
                   && latestAtm.Status < (int)AtmRefundStatus.AchProcess;
        }

        /// <summary>
        /// 退貨前是否需先填ATM資料
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static bool IsNeedFillAtmInfoBeforeReturn(Guid orderGuid)
        {
            Order order = _op.OrderGet(orderGuid);
            if (!order.IsLoaded) return false;

            if (OrderHasAtmPayment(order))
            {
                if (!AtmRefundAccountExist(order.Guid))
                {
                    return true;
                }
            }

            CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(order.Guid, OrderClassification.LkSite);
            if (!ctlogs.Any()) return false;
            ThirdPartyPayment tppSystem;
            OrderHasThirdPartyPayment(ctlogs, out tppSystem);

            //信用卡、LinePay 超過特定期限，台新儲值支付退貨，一律走ATM退款
            if (OldOrderNeedAtm(order, tppSystem))
            {
                //if (!AtmRefundAccountExist(order.Guid))
                //{
                //    return true;
                //}
                return true;
            }

            //分期付款且部份退貨，走ATM退款
            if (order.Installment > 0)
            {
                if (ctlogs.Any(ctlog => ctlog.CreditCard + ctlog.Atm + ctlog.Tcash + ctlog.FamilyIsp + ctlog.SevenIsp > 0))
                {
                    if (!AtmRefundAccountExist(order.Guid))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static List<CashTrustLog> GetRefundableCtlogs(Guid orderGuid, BusinessModel bizModel)
        {
            OrderClassification classification = bizModel == BusinessModel.Ppon
                                                     ? OrderClassification.LkSite
                                                     : OrderClassification.HiDeal;
            List<CashTrustLog> allCtlogs =
                _mp.CashTrustLogGetListByOrderGuid(orderGuid, classification)
                .Where(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight))
                .ToList();
            List<Guid> unrefundableTrustIds = GetUnrefundableReturnFormTrustIds(orderGuid);

            var result = new List<CashTrustLog>();

            foreach (CashTrustLog ctlog in allCtlogs)
            {
                if (!unrefundableTrustIds.Contains(ctlog.TrustId))
                {
                    result.Add(ctlog);
                }
            }

            return result;
        }

        /// <summary>
        /// 取得憑證訂單中可退貨的 cash_trust_log (退貨狀態非退貨處理中/處理完畢且憑證狀態為未使用且非鎖定中)
        /// </summary>
        public static List<CashTrustLog> GetRefundableCouponCtlogs(Guid orderGuid, BusinessModel bizModel, int? classificationValue = null)
        {
            OrderClassification classification = bizModel == BusinessModel.Ppon
                                                     ? OrderClassification.LkSite
                                                     : OrderClassification.HiDeal;
            classification = classificationValue != null ? (OrderClassification)classificationValue : classification;
            CashTrustLogCollection allCtlogs = _mp.CashTrustLogGetListByOrderGuid(orderGuid, classification);
            List<Guid> unrefundableTrustIds = GetUnrefundableReturnFormTrustIds(orderGuid);

            List<CashTrustLog> result = new List<CashTrustLog>();

            //鎖定中的憑證
            List<int> vpcc = _pp.ViewPponCouponGetListByOrderGuid(orderGuid).Where(x => x.IsReservationLock.Value && x.CouponId != null).Select(s => s.CouponId.Value).ToList();

            foreach (CashTrustLog ctlog in allCtlogs)
            {
                if (!unrefundableTrustIds.Contains(ctlog.TrustId) && !vpcc.Contains(ctlog.CouponId.Value) &&
                    (ctlog.Status == (int)TrustStatus.Initial ||
                     ctlog.Status == (int)TrustStatus.Trusted))
                {
                    result.Add(ctlog);
                }
            }

            return result;
        }



        /// <summary>
        /// 取得不可退貨的 trust_id (退貨處理中/處理完畢的 trust_id)
        /// </summary>
        public static List<Guid> GetUnrefundableReturnFormTrustIds(Guid orderGuid)
        {
            List<Guid> result = new List<Guid>();

            ReturnFormCollection forms = _op.ReturnFormGetListByOrderGuid(orderGuid);
            foreach (ReturnForm form in forms)
            {
                ProgressStatus progressStatus = (ProgressStatus)form.ProgressStatus;
                if (progressStatus != ProgressStatus.Canceled
                    && progressStatus != ProgressStatus.Unknown
                    && progressStatus != ProgressStatus.Unreturnable)
                {
                    ReturnFormRefundCollection refunds = _op.ReturnFormRefundGetList(form.Id);
                    foreach (var refund in refunds)
                    {
                        if (refund.VendorNoRefund
                            && (progressStatus == ProgressStatus.Completed || progressStatus == ProgressStatus.CompletedWithCreditCardQueued))
                        {
                            continue;
                        }
                        result.Add(refund.TrustId);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 取得是否有未完成退款之退貨單
        /// (若returnFormId有值 為排除此退貨單 檢查是否有其他退款中之退貨單)
        /// </summary>
        public static bool HasProcessingReturnForm(Guid orderGuid, int? returnFormId = null)
        {
            return _op.ReturnFormGetListByOrderGuid(orderGuid)
                        .Any(x => x.Id != returnFormId &&
                                 (x.ProgressStatus == (int)ProgressStatus.Processing ||
                                  x.ProgressStatus == (int)ProgressStatus.CompletedWithCreditCardQueued ||
                                  x.ProgressStatus == (int)ProgressStatus.AtmQueueing ||
                                  x.ProgressStatus == (int)ProgressStatus.AtmQueueSucceeded ||
                                  x.ProgressStatus == (int)ProgressStatus.AtmFailed));
        }

        /// <summary>
        /// 取得轉退購物金之退貨單
        /// </summary>
        public static ReturnFormCollection RefundScashReturnForm(Guid orderGuid)
        {
            ReturnFormCollection rfc = _op.ReturnFormGetListByOrderGuidProgressStatusRefundType(orderGuid, ProgressStatus.Completed, RefundType.Scash);
            return rfc;
        }

        /// <summary>
        /// 檢查是否有2聯轉3聯尚未完成
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static bool IsInvoice2To3(Guid orderGuid)
        {
            EinvoiceMainCollection emc = _op.EinvoiceMainCollectionGet("order_guid", orderGuid.ToString(), true);
            if (emc.Any(x => x.InvoiceMode2 == (int)InvoiceMode2.Triplicate && x.InvoiceNumber == null && x.InvoiceStatus == (int)EinvoiceType.C0401))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 由[退購物金轉刷退]回復為[退購物金完成]狀態
        /// 針對已申請購物金轉刷退 因需折讓單 等待標記折讓單已回過程中 消費者使用了購物金 
        /// 導致剩餘購物金不足收回無法進行轉刷退作業狀況之退貨單進行狀態回復(即取消轉刷退)
        /// </summary>
        /// <param name="returnForm"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static void RecoverToRefundScash(ReturnFormEntity returnForm, string userId)
        {
            string failReason;
            returnForm.RecoverToRefundScash(userId, out failReason);

            if (string.IsNullOrWhiteSpace(failReason))
            {
                ReturnFormRepository.Save(returnForm);
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, string.Format("退貨單{0} - 取消購物金轉刷退並回復退購物金完成狀態", returnForm.Id), userId, false);
            }
            else
            {
                string auditMessage = string.Format("退貨單{0} - 回復退購物金完成失敗: {1}", returnForm.Id, failReason);
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, auditMessage, userId, false);
            }
        }

        /// <summary>
        /// 申請退貨前檢查：檢查是否有處理中之換貨單
        /// 有處理中之換貨 仍可允許申請部分退貨 但不允許全部退貨
        /// </summary>
        public static bool HasExchangingLog(Guid orderGuid, int returnCount)
        {
            var hasExchangingLog = _op.OrderReturnListGetListByType(orderGuid, (int)OrderReturnType.Exchange)
                                    .Any(x => x.Status != (int)OrderReturnStatus.ExchangeSuccess &&
                                              x.Status != (int)OrderReturnStatus.ExchangeCancel &&
                                              x.Status != (int)OrderReturnStatus.ExchangeFailure);
            var allProductCount = _op.OrderProductGetListByOrderGuid(orderGuid)
                                    .Count(x => x.IsCurrent && !x.IsExchanging && !x.IsReturned && !x.IsReturning);

            return allProductCount == returnCount
                    ? hasExchangingLog
                    : false;
        }

        /// <summary>
        /// 退貨相關品項發票是否已開立
        /// </summary>
        /// <returns></returns>
        public static bool IsInvoiceCreate(ReturnFormEntity returnForm)
        {
            List<EinvoiceMain> invoices = _op.EinvoiceMainGetListByOrderGuid(returnForm.OrderGuid).ToList();

            if (returnForm.DeliveryType == DeliveryType.ToShop)
            {
                List<int> couponIds = GetCouponIds(returnForm);

                List<EinvoiceMain> relatedInvoices = invoices.
                    Where(x => x.CouponId.HasValue && couponIds.Contains(x.CouponId.Value)).
                    ToList();
                //成套禮券發票無couponId
                relatedInvoices.AddRange(GroupCouponOrder(invoices));

                // 沒有發票(有發票號碼視為已開立發票) (return false)
                return relatedInvoices.Any()
                    && relatedInvoices.Any(einvoice => einvoice.InvoiceNumberTime.HasValue);
            }
            else
            {
                //宅配只有一張發票 
                //沒有發票就不需要折讓單 (return false), 所以要用 Any 擋掉
                return invoices.Any()
                    && invoices.Any(einvoice => einvoice.InvoiceNumberTime.HasValue);
            }
        }

        #region private members

        private static void RemoveAtmRefundingFlag(List<Guid> trustIds, string userId)
        {
            CashTrustLogCollection ctlogs = _mp.CashTrustLogGetList(trustIds);
            CashTrustStatusLogCollection logs = new CashTrustStatusLogCollection();
            DateTime now = DateTime.Now;

            foreach (var ctlog in ctlogs)
            {
                ctlog.SpecialStatus = (int)Helper.SetFlag(false, ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding);
                ctlog.SpecialOperatedTime = now;
                logs.Add(new CashTrustStatusLog
                {
                    TrustId = ctlog.TrustId,
                    TrustProvider = ctlog.TrustProvider,
                    Amount = ctlog.Amount,
                    Status = ctlog.Status,
                    ModifyTime = ctlog.ModifyTime,
                    CreateId = userId,
                    SpecialStatus = ctlog.SpecialStatus,
                    SpecialOperatedTime = now
                });
            }

            _mp.CashTrustLogCollectionSet(ctlogs);
            _mp.CashTrustStatusLogCollectionSet(logs);
        }

        private static bool UndoRefund(List<Guid> trustIds, string userId)
        {
            bool result = false;

            CashTrustLogCollection ctlogs = _mp.CashTrustLogGetList(trustIds);

            foreach (var ctlog in ctlogs)
            {
                result = PaymentFacade.UndoCancelPaymentTransaction(ctlog, userId);
                if (!result)
                {
                    break;
                }
            }

            return result;
        }

        private static bool WithinCurrentInvoicePeriod(DateTime invoiceDate)
        {
            DateTime now = DateTime.Now;
            if (invoiceDate.Year != now.Year)
            {
                return false;
            }

            if (int.Equals(1, now.Month % 2))
            {
                //奇數月份
                return now.Month - invoiceDate.Month == 0;
            }
            else
            {
                //偶數月份
                return now.Month - invoiceDate.Month <= 1;
            }
        }

        private static List<int?> GetCouponIds(OrderReturnList returnForm)
        {
            var result = new List<int?>();

            if (returnForm.CouponIds == null)
            {
                result.Add(null);
            }
            else
            {
                string[] coupons = returnForm.CouponIds.Split(",");

                foreach (string coupon in coupons)
                {
                    int dummy;
                    if (int.TryParse(coupon, out dummy))
                    {
                        result.Add(dummy);
                    }
                    else
                    {
                        throw new InvalidDataException("order_return_list coupon_ids int parsing failed!");
                    }
                }
            }

            return result;
        }

        private static CashTrustLog GetShippingCashTrustLog(List<CashTrustLog> ctlogs)
        {
            return ctlogs.FirstOrDefault(x => Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight));
        }

        private static List<int> GetCouponIds(ReturnFormEntity returnForm)
        {
            List<Guid> trustIds = returnForm.ReturnFormRefunds.Select(x => x.TrustId).ToList();
            CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(returnForm.OrderGuid, OrderClassification.LkSite);

            return ctlogs.Where(x => trustIds.Any(y => y == x.TrustId) && x.CouponId.HasValue)
                         .Select(x => x.CouponId.Value)
                         .ToList();
        }

        public static List<EinvoiceMain> GroupCouponOrder(List<EinvoiceMain> list)
        {
            if (list.Count > 0)
            {
                CashTrustLogCollection ctlogs = _mp.CashTrustLogGetListByOrderGuid(list.FirstOrDefault().OrderGuid, OrderClassification.LkSite);
                foreach (CashTrustLog ctl in ctlogs)
                {
                    if (Helper.IsFlagSet(_pp.BusinessHourGet(ctl.BusinessHourGuid.Value).BusinessHourStatus, BusinessHourStatus.GroupCoupon))
                    {
                        return list;
                    }
                }
            }
            return new List<EinvoiceMain>();
        }

        private static bool ProductsReturnable(Guid orderGuid, IEnumerable<int> orderProductIds)
        {
            PponOrder order = new PponOrder(orderGuid);
            Dictionary<int, Product> products = order.Products.ToDictionary(x => x.OrderProductId);

            return orderProductIds.All(x =>
                                       products[x].IsCurrent
                                       && !products[x].IsExchanging
                                       && !products[x].IsReturning
                                       && !products[x].IsReturned);
        }

        private static bool CouponReturnable(Guid orderGuid, IEnumerable<int> couponIds)
        {
            if (couponIds == null)
            {
                return false;
            }

            if (!couponIds.Any())
            {
                return false;
            }

            CashTrustLogCollection refundingCtlogs = _mp.PponRefundingCashTrustLogGetListByOrderGuid(orderGuid);

            foreach (int couponId in couponIds)
            {
                if (refundingCtlogs.Any(x => x.CouponId == couponId))
                {
                    return false;
                }
            }

            return true;
        }

        private static bool UnusedCouponLock(Guid orderGuid, IEnumerable<int> couponIds)
        {
            var vpcc = _pp.ViewPponCouponGetListByOrderGuid(orderGuid);
            //鎖定中的憑證
            List<int> lockCoupons = vpcc.Where(x => x.IsReservationLock.Value && x.CouponId != null).Select(s => s.CouponId.Value).ToList();
            if (!lockCoupons.Any())
                return true;

            CashTrustLogCollection unusedCtl = _mp.CashTrustLogGetCouponSequencesWithUnused(orderGuid);
            bool isGroupCoupon = vpcc.Any(x => Helper.IsFlagSet((int)BusinessHourStatus.GroupCoupon, x.BusinessHourStatus));

            foreach (int couponId in lockCoupons)
            {
                if (isGroupCoupon && unusedCtl.Any(x => x.CouponId == couponId))
                {
                    return false;
                }
                if (couponIds.Any(x => x == couponId))
                {
                    return false;
                }
            }
            return true;
        }

        private static bool CheckTrustStatus(DeliveryType type, TrustStatus status, bool isForced, out string failMessage)
        {
            var result = true;
            failMessage = string.Empty;

            if (type == DeliveryType.ToShop)
            {
                if (!isForced && status.Equals(TrustStatus.Verified))
                {
                    result = false;
                    failMessage = "憑證已核銷, 無法退貨!";
                }
                else if (isForced && !status.Equals(TrustStatus.Verified))
                {
                    result = false;
                    failMessage = "憑證未核銷, 無須強制退貨!";
                }
            }

            return result;
        }

        private static bool IsAllReturnedApplication(Guid orderGuid, IEnumerable<int> orderProductIds)
        {
            PponOrder order = new PponOrder(orderGuid);
            var productsCount = order.Products.Count(x => x.IsCurrent
                                                       && !x.IsReturning
                                                       && !x.IsReturned);

            return orderProductIds.Count() == productsCount;
        }

        #endregion private members

    }

    public enum CreateReturnFormResult
    {
        None = 0,
        Created = 1,
        /// <summary>
        /// ATM單需要先建立 ATM 退款帳戶
        /// </summary>
        AtmOrderNeedAtmRefundAccount,
        /// <summary>
        /// 特定訂單需要先建立 ATM 退款帳戶
        /// </summary>
        OldOrderNeedAtmRefundAccount,
        /// <summary>
        /// 剩下的貨品不夠退貨
        /// </summary>
        NotEnoughProducts,
        /// <summary>
        /// 需要折讓單
        /// </summary>
        RequireCreditNote,
        /// <summary>
        /// 欲退貨的商品無法退貨
        /// </summary>
        ProductsUnreturnable,
        /// <summary>
        /// 無法成功建立退貨單
        /// </summary>
        InvalidArguments,
        /// <summary>
        /// 檔次不允許退貨
        /// </summary>
        DealDeniesRefund,
        /// <summary>
        /// 有退貨中之商品
        /// </summary>
        ProductsIsReturning,
        /// <summary>
        /// 訂單未完成付款(未成立訂單)
        /// </summary>
        OrderNotCreate,
        /// <summary>
        /// 部分退且是分期訂單需要建立 ATM 退款帳戶
        /// </summary>
        InstallmentOrderNeedAtmRefundAccount,
        /// <summary>
        /// 有換貨中之商品
        /// </summary>
        ProductsIsExchanging,
        /// <summary>
        /// 含有未核銷且鎖定中的憑證
        /// </summary>
        UnusedCouponLock,
        /// <summary>
        /// 超取超付訂單需要先建立 ATM 退款帳戶
        /// </summary>
        IspOrderNeedAtmRefundAccount,
        /// <summary>
        /// 訂單未完成付款, 無法建立退貨單。請改按[取消訂單]，進行訂單取消申請。
        /// </summary>
        IspOrderNotCreate

    }

}

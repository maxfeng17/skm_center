﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{
    public static class HamiDataModel
    {
        public const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss";
        public const string DateFormat = "yyyy-MM-dd";
        public static string SaleStatusByDeal(ViewPponDeal deal)
        {
            var rtn = "L";
            var quantity = deal.OrderedQuantity==null?0:deal.OrderedQuantity.Value;//已銷售數量
            var minimum = deal.BusinessHourOrderMinimum;//門檻
            var total = deal.OrderTotalLimit;
            if(quantity < minimum )
            {
                rtn = "L";
            }
            else if(quantity>=minimum && quantity < total)
            {
                rtn = "S";
            }
            else if(quantity >= total)
            {
                rtn = "F";
            }

            return rtn;
        }

        private static void AddStringToListWithOutContains(this List<string> list , string data)
        {
            if(list == null)
            {
                return;
            }
            if(!list.Contains(data))
            {
                list.Add(data);
            }
        }
        public static string HamiCityStringList(List<int> citys)
        {
            ////////////////////////////////////////////////////
            //01	台北
            //02	桃園
            //03	新竹
            //04	苗栗
            //05	台中
            //06	彰化
            //07	南投
            //08	雲林
            //09	嘉義
            //10	台南
            //11	高雄
            //12	屏東
            //13	基隆
            //14	宜蘭
            //15	花蓮
            //16	台東
            ////////////////////////////////////////////////////
            var hamiCitys = new List<string>();
            foreach(var city in citys)
            {

                if(city == PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId)
                {
                    hamiCitys.AddStringToListWithOutContains("01");
                }
                else if(city == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity.CityId)
                {
                    hamiCitys.AddStringToListWithOutContains("01");
                    hamiCitys.AddStringToListWithOutContains("13");
                }
                else if(city == PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId)
                {
                    hamiCitys.AddStringToListWithOutContains("02");
                }
                else if(city == PponCityGroup.DefaultPponCityGroup.Hsinchu.CityId)
                {
                    hamiCitys.AddStringToListWithOutContains("03");
                }
                else if(city == PponCityGroup.DefaultPponCityGroup.Taichung.CityId)
                {
                    hamiCitys.AddStringToListWithOutContains("05");
                    hamiCitys.AddStringToListWithOutContains("06");
                    hamiCitys.AddStringToListWithOutContains("07");
                }
                else if(city == PponCityGroup.DefaultPponCityGroup.Tainan.CityId)
                {
                    hamiCitys.AddStringToListWithOutContains("08");
                    hamiCitys.AddStringToListWithOutContains("09");
                    hamiCitys.AddStringToListWithOutContains("10");
                }
                else if(city == PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId)
                {
                    hamiCitys.AddStringToListWithOutContains("11");
                    hamiCitys.AddStringToListWithOutContains("12");
                }
                else if(city == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
                {
                    hamiCitys.AddStringToListWithOutContains("04");
                    hamiCitys.AddStringToListWithOutContains("14");
                    hamiCitys.AddStringToListWithOutContains("15");
                    hamiCitys.AddStringToListWithOutContains("16");
                }
            }
            return string.Join(",", hamiCitys.ToArray());
        }

        public static bool CheckRqMac(string queryString , string requestTime,string Mac)
        {
            var config = ProviderFactory.Instance().GetConfig();
            var hmacsha1 = new HMACSHA1(config.HamiHMACSHA1Key.ToBytes());
            var hashData = queryString + requestTime;
            var checkString = Helper.BytesToHex(hmacsha1.ComputeHash(hashData.ToBytes()));
            return checkString.Equals(Mac);
        }

        public static string GetRqDataMac(string queryString, string requestTime)
        {
            var config = ProviderFactory.Instance().GetConfig();
            var hmacsha1 = new HMACSHA1(config.HamiHMACSHA1Key.ToBytes());
            var hashData = queryString + requestTime;
            var checkString = Helper.BytesToHex(hmacsha1.ComputeHash(hashData.ToBytes()));
            return checkString;
        }

    }

    public class HamiPponDeal
    {
        #region Field
        /// <summary>
        /// 商品代碼 Max(16)供應商之商品代碼，須確保唯一
        /// </summary>
        [XmlElementName("ProductId")]
        public string ProductId;

        /// <summary>
        /// 銷售狀態 字串 Max(1)
        /// L:未成團
        /// S:已成團 F:已售完，此狀態之商品將進行下架。
        /// C:加碼，此狀態僅用於已下架之商品，當須追碼銷售時使用，供應商須於商品露出結束日期前1日更新，超過。
        /// </summary>
        [XmlElementName("SaleStatus")] public string SaleStatus;

        /// <summary>
        /// 延展期 數字字串 Max(3) 用以延長商品露出時間，以小時為單位；因此該商品露出結束日期將延長指定時數後才會下架
        /// </summary>
        [XmlElementName("ExpandPeriod")] public int ExpandPeriod;

        /// <summary>
        /// 標題 字串 Max(100) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("Title")] public string Title;

        /// <summary>
        /// 短標題 字串 Max(50) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("ShortTitle")] public string ShortTitle;

        /// <summary>
        /// 描述 字串 Max(100) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("Description")] public string Description;

        /// <summary>
        /// 商品分類 字串 Max(4) 商品露出後，即不可再更新；
        /// </summary>
        [XmlElementName("Category")] public string Category;

        /// <summary>
        /// 商品類型 字串 Max(1)
        /// 1:宅配遞送之商品/服務
        /// 2:到店服務/到店取貨
        /// 3:網路遞送之商品/服務
        /// </summary>
        [XmlElementName("Type")] public string Type;

        /// <summary>
        /// 圖片網址 字串 Max(256) 商品露出後，即不可再更新；請輸入商品圖片網址，圖片格式為JPEG、圖片大小：440*267
        /// </summary>
        [XmlElementName("PhotoUrl")] public string PhotoUrl;

        /// <summary>
        /// 小圖網址 字串 Max(256) 商品露出後，即不可再更新；請輸入商品圖片網址，圖片格式為JPEG、圖片大小：200*133
        /// </summary>
        [XmlElementName("SmallPhotoUrl")] public string SmallPhotoUrl;

        /// <summary>
        /// 商品露出開始時間 日期字串 商品露出後，即不可再更新；格式：yyyy-MM-ddTHH:mm:ss
        /// </summary>
        [XmlElementName("StartDate")] public string StartDateString;

        /// <summary>
        /// 商品露出結束時間 日期字串 商品露出後，即不可再更新；格式：yyyy-MM-ddTHH:mm:ss
        /// </summary>
        [XmlElementName("EndDate")] public string EndDateString;

        /// <summary>
        /// 原價 數字 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("OriginalPrice")] public decimal OriginalPrice;

        /// <summary>
        /// 折扣價 數字 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("DiscountPrice")] public decimal DiscountPrice;

        /// <summary>
        /// 折扣比率	字串	Max(10)	商品露出後，即不可再更新 例如：4.7折
        /// </summary>
        [XmlElementName("DiscountRate")] public string DiscountRateString;

        /// <summary>
        /// 商品數量 數字 商品露出後，即不可再更新，本商品預計販售之數量
        /// </summary>
        [XmlElementName("ProductNum")] public decimal ProductNum;

        /// <summary>
        /// 城市字串 字串 Max(48) 商品露出後，即不可再更新，提供商品或服務之城市，請參考4.3(c)表城市間請以”;”區隔
        /// </summary>
        [XmlElementName("Cities")] public string Cities;

        /// <summary>
        /// 店家代碼 字串 Max(16) 商品露出後，即不可再更新 用以提供店家資訊管理
        /// </summary>
        [XmlElementName("StoreId")] public string StoreId;

        /// <summary>
        /// 店家名稱 字串 Max(50) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("StoreName")] public string StoreName;

        /// <summary>
        /// 店家電話 字串 Max(16) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("StoreTel")] public string StoreTel;

        /// <summary>
        /// 店家描述 字串 Max(32) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("StoreDescription")] public string StoreDescription;

        /// <summary>
        /// 店家網址 字串 Max(256) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("StoreLink")] public string StoreLink;

        /// <summary>
        /// 店家分店子群組
        /// </summary>
        [XmlElementName("Branches")] public List<HamiBranch> Branches;

        /// <summary>
        /// 檔次連結 字串 Max(256) 商品露出後，即不可再更新，商品檔次連結網址
        /// </summary>
        [XmlElementName("Link")] public string Link;

        /// <summary>
        /// 檔次連結 字串 Max(256) 商品露出後，即不可再更新，手機版商品檔次連結網址
        /// </summary>
        [XmlElementName("MobileLink")] public string MobileLink;

        /// <summary>
        /// 標題 字串 Max(100) 促銷用顯示標題 
        /// </summary>
        [XmlElementName("PromotionTitle")] public string PromotionTitle;

        /// <summary>
        /// 短標題 字串 Max(50) 促銷用顯示短標題
        /// </summary>
        [XmlElementName("PromotionShortTitle")] public string PromotionShortTitle;

        /// <summary>
        /// 描述 字串 Max(100 促銷用描述
        /// </summary>
        [XmlElementName("PromotionDesc")] public string PromotionDesc;

        /// <summary>
        /// 售罄時間 日期字串 商品售罄的時間點，SaleStatus＝F時才能更新
        /// </summary>
        [XmlElementName("SoldOutTime")] public string SoldOutTimeString;

        /// <summary>
        /// 銷售數量 數字 目前本商品的銷售總數量
        /// </summary>
        [XmlElementName("SoldNum")] public int SoldNum;

        ///// <summary>
        ///// 結果代碼 字串 Max(3) 請參考表4.3(a) 
        ///// </summary>
        //[XmlElementName("StatusCode")]
        //public string StatusCode;
        ///// <summary>
        ///// 結果說明 字串 Max(256) 請參考表4.3(b)
        ///// </summary>
        //[XmlElementName("StatusDesc")]
        //public string StatusDesc;
        ///// <summary>
        ///// 訊息壓碼 字串Max(32)
        ///// </summary>
        //[XmlElementName("MAC")]
        //public string MAC;

        public HamiPponDeal()
        {
            Branches = new List<HamiBranch>();
        }
        #endregion Field
    }

    public class HamiBranch
    {
        /// <summary>
        /// 分店代碼 字串 Max(16) 商品露出後，即不可再更新 用以提供分店資訊管理
        /// </summary>
        [XmlElementName("BranchId")] public string BranchId;

        /// <summary>
        /// 分店名稱 字串 Max(50) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("BranchName")] public string BranchName;

        /// <summary>
        /// 分店地址 字串 Max(128) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("BranchAddr")] public string BranchAddr;

        /// <summary>
        /// 分店電話 字串 Max(16) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("BranchTel")] public string BranchTel;

        /// <summary>
        /// 分店經度 字串 Max(16) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("BranchLat")] public string BranchLat;

        /// <summary>
        /// 分店緯度 字串 Max(16) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("BranchLng")] public string BranchLng;

        /// <summary>
        /// 分店備註 字串 Max(256) 商品露出後，即不可再更新
        /// </summary>
        [XmlElementName("BranchNote")] public string BranchNote;
    }

}

﻿using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.OAuth
{
    public class TokenResponse
    {
        public const string _RESPONSE_TYPE_BEARER = "Bearer";
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 單位:秒
        /// </summary>
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("token_type")]
        public string TokenType
        {
            get { return _RESPONSE_TYPE_BEARER; }
        }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }

    /// <summary>
    /// 17life自行開發之APP使用，回傳參數名稱不同之CLASS
    /// </summary>
    public class ApiTokenResponse
    {
        public string AccessToken { get; set; }
        public int ExpiresIn { get; set; }
        public string TokenType { get; set; }
        public string RefreshToken { get; set; }

        public static ApiTokenResponse GetApiTokenResponse(TokenResponse token)
        {
            var rtn = new ApiTokenResponse
            {
                AccessToken = token.AccessToken,
                ExpiresIn = token.ExpiresIn,
                TokenType = token.TokenType,
                RefreshToken = token.RefreshToken
            };
            return rtn;
        }
    }
}



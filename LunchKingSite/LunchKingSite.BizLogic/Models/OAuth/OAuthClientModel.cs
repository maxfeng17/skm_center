﻿using System;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model.OAuth
{
    public class OAuthClientModel
    {
        public OauthClient Client { get; set; }

        public OAuthClientModel(OauthClient client, OauthClientPermissionCollection permissions)
        {
            this.Client = client;
            this.Permissions = permissions;
        }
        
        public int Id
        {
            get { return Client.Id; }
            set { Client.Id = value; }
        }

        public string AppId
        {
            get { return Client.AppId; }
            set { Client.AppId = value; }
        }

        public string AppSecret
        {
            get { return Client.AppSecret; }
            set { Client.AppSecret = value; }
        }

        public string ReturnUrl
        {
            get { return Client.ReturnUrl; }
            set { Client.ReturnUrl = value; }
        }

        public string Name
        {
            get { return Client.Name; }
            set { Client.Name = value; }
        }

        public string Description
        {
            get { return Client.Description; }
            set { Client.Description = value; }
        }

        public string Owner
        {
            get { return Client.Owner; }
            set { Client.Owner = value; }
        }

        public DateTime CreatedTime
        {
            get { return Client.CreatedTime; }
            set { Client.CreatedTime = value; }
        }

        public bool Enabled
        {
            get { return Client.Enabled; }
            set { Client.Enabled = value; }
        }

        public OauthClientPermissionCollection Permissions { get; set; }

        public virtual bool HasPermission(TokenScope scope)
        {
            if (Permissions == null)
            {
                return false;
            }
            return Permissions.Any(t => t.ScopeEnum == scope);
        }
    }
}

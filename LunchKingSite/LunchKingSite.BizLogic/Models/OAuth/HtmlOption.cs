﻿namespace LunchKingSite.BizLogic.Model.OAuth
{
    public class HtmlOption
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public bool Checked { get; set; }
    }
}
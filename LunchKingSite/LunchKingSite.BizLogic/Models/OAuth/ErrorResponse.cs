﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.OAuth
{
    public class ErrorResponse
    {
        public const string UnauthorizedClient = "unauthorized_client";
        public const string AccessDenied = "access_denied";
        public const string UnsupportedResponseType = "unsupported_response_type";
        public const string InvalidScope = "invalid_scope";
        public const string ServerError = "server_error";
        public const string TemporarilyUnavailable = "temporarily_unavailable";
        public const string InvalidClientCredentials = "invalid_client_credentials";
        public const string InactiveClient = "inactive_client";
        public const string InvalidReturnUrl = "invalid_return_url";
        public const string InvalidClientId = "invalid_client_id";
        /// <summary>
        /// 無效的使用者
        /// </summary>
        public const string InvalidUser = "invalid_user";
        /// <summary>
        /// 無效的token
        /// </summary>
        public const string InvalidToken = "invalid_token";
        /// <summary>
        /// 輸入錯誤
        /// </summary>
        public const string InputError = "input_error";

        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }

        private static Dictionary<string, string> Data;
        static ErrorResponse()
        {
            Data = new Dictionary<string, string>();
            Data.Add("unauthorized_client", "unauthorized_client");
            Data.Add("access_denied", "access_denied");
            Data.Add("unsupported_response_type", "unsupported_response_type");
            Data.Add("invalid_scope", "invalid_scope");
            Data.Add("server_error", "server_error");
            Data.Add("temporarily_unavailable", "temporarily_unavailable");
            Data.Add(InvalidClientCredentials, "應用程式{0}的私鑰不符合預期");
            Data.Add(InactiveClient, "應用程式{0}目前無法使用.");
            Data.Add(InvalidReturnUrl, "與應用程式設定的回傳網址不符合.");
            Data.Add(InvalidClientId, "應用程式{0}找不到.");
            Data.Add(InvalidUser, "使用者資料錯誤.");
            Data.Add(InputError, "輸入錯誤.");
        }

        public static string GetDescription(string error, params object[] args)
        {
            string desc;
            if (Data.TryGetValue(error, out desc) == false)
            {
                return error;
            }
            if (args != null && args.Length > 0)
            {
                if (Data.ContainsKey(error))
                {
                    return Data[error];
                }
                return string.Format(desc, args);
            }
            return desc;
        }

    }
    /*
unsupported_response_type
invalid_request—HTTPS required
invalid_request—must use HTTP POST
invalid_client_credentials—client secret invalid
invalid_request—secret type not supported
invalid_grant—expired access/refresh token
invalid_grant—IP restricted or invalid login hours
inactive_user—user is inactive
inactive_org—organization is locked, closed, or suspended
rate_limit_exceeded—number of logins exceeded
invalid_scope—requested scope is invalid, unknown, or malformed
     */
}
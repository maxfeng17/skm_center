﻿namespace LunchKingSite.BizLogic.Model.OAuth
{
    public class ErrorMessage
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}

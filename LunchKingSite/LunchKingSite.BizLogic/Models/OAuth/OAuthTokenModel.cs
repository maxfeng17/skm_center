﻿using System;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model.OAuth
{
    public class OAuthTokenModel
    {
        public OauthToken Token { get; set; }

        public OAuthTokenModel(OauthToken token, OauthTokenPermissionCollection permissions)
        {
            this.Token = token;
            this.Permissions = permissions;
        }

        public int Id
        {
            get { return Token.Id; }
            set { Token.Id = value; }
        }

        public string Code
        {
            get { return Token.Code; }
            set { Token.Code = value; }
        }

        public bool IsCodeExchanged
        {
            get { return Token.IsCodeExchanged; }
            set { Token.IsCodeExchanged = value; }
        }

        public string AccessToken
        {
            get { return Token.AccessToken; }
            set { Token.AccessToken = value; }
        }

        public int? UserId
        {
            get { return Token.UserId; }
            set { Token.UserId = value; }
        }

        public string AppId
        {
            get { return Token.AppId; }
            set { Token.AppId = value; }
        }

        public OAuthTargetType Target
        {
            get { return (OAuthTargetType)Token.Target; }
            set { Token.Target = (int)value; }
        }

        public DateTime ExpiredTime
        {
            get { return Token.ExpiredTime; }
            set { Token.ExpiredTime = value; }
        }

        public bool IsExpired
        {
            get { return DateTime.Now > ExpiredTime; }
        }

        public bool IsValid
        {
            get { return Token.IsValid; }
            set { Token.IsValid = value; }
        }

        public DateTime CreatedTime
        {
            get { return Token.CreatedTime; }
            set { Token.CreatedTime = value; }
        }

        public DateTime? ModifiedTime
        {
            get { return Token.ModifiedTime; }
            set { Token.ModifiedTime = value; }
        }

        public string RefreshToken
        {
            get { return Token.RefreshToken; }
            set { Token.RefreshToken = value; }
        }

        public bool IsTokenRefreshed
        {
            get { return Token.IsTokenRefreshed; }
            set { Token.IsTokenRefreshed = value; }
        }

        public OauthTokenPermissionCollection Permissions { get; set; }
    }

    public class TokenExpiredInfo
    {
        public DateTime ExpiredTime { get; set; }
        public int ExpiredSec { get; set; }

        public TokenExpiredInfo()
        {
            // 預設 2 hr
            ExpiredTime = DateTime.Now.AddHours(2);
            ExpiredSec = 7200;
        }

        public TokenExpiredInfo(TokenScope scope)
        {
            switch (scope)
            {
                case TokenScope.LongLived:
                    ExpiredTime = DateTime.Now.AddYears(10);
                    ExpiredSec = 315360000;
                    break;
                case TokenScope.OnedayLived:
                    ExpiredTime = DateTime.Now.AddDays(1);
                    ExpiredSec = 86400;
                    break;
                case TokenScope.OneWeekLived:
                    ExpiredTime = DateTime.Now.AddDays(7);
                    ExpiredSec = 604800;
                    break;
                default:
                    ExpiredTime = DateTime.Now.AddHours(2);
                    ExpiredSec = 7200;
                    break;
            }
        }
    }
}
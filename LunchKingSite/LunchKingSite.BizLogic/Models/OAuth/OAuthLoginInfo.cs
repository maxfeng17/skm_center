﻿namespace LunchKingSite.BizLogic.Model.OAuth
{
    public class OAuthLoginInfo
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public string ResponseType { get; set; }
        public string GrantType { get; set; }
        public string FbToken { get; set; }
        public string PezmemId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string AuthorizeCode { get; set; }
        public string RefreshToken { get; set; }
        public string RedirectUri { get; set; }
        public string Scope { get; set; }
        public string State { get; set; }
    }
}
﻿using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.OAuth
{
    public class OAuthMemberModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("birthday")]
        public string Birthday { get; set; }
        [JsonProperty("fullname")]
        public string DisplayName { get; set; }
    }
}

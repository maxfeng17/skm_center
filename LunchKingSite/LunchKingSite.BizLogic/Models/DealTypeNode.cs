﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Models
{
    [Serializable]
    public class DealTypeNode
    {
		public int CodeId  { get; set; }
		  
		public string CodeName  { get; set; }
		  
		public bool Enabled  { get; set; }
		  
		public int? Seq  { get; set; }
		  
		//public string ShortName  { get; set; }

        public string GoogleCode { get; set; }

        public int Level
        {
            get
            {
                int lvl = 1;
                DealTypeNode p = this;
                while(p.Parent != null)
                {
                    p = p.Parent;
                    lvl++;
                }
                return lvl;
            }
        }

        public DealTypeNode Parent { get; set; }

        public int? ParentCodeId { get; set; }
	
        //[JsonIgnore]
        public List<DealTypeNode> Children { get; set; }

        //標註新增的資料
        public bool IsNew
        {
            get
            {
                return this.CodeId == 0;
            }
        }

        public DealTypeNode()
        {
            this.GoogleCode = string.Empty;
            this.Children = new List<DealTypeNode>();
        }

        public static List<DealTypeNode> ConvertToNestedNodes(IEnumerable<SystemCode> items, int? parentId = null, DealTypeNode parent = null)
        {
            List<DealTypeNode> result = new List<DealTypeNode>();
            foreach (var item in items)
            {
                if (item.ParentCodeId == parentId)
                {
                    var category = Create(parentId, item);
                    category.Parent = parent;
                    category.ParentCodeId = parent == null ? (int?)null : parent.CodeId;
                    result.Add(category);
                    category.Children.AddRange(ConvertToNestedNodes(items, item.CodeId, category));
                }
            }
            return result;
        }

        private static DealTypeNode Create(int? parentId, SystemCode item)
        {
            return new DealTypeNode
            {
                CodeId = item.CodeId,
                CodeName = item.CodeName,
                Enabled = item.Enabled,
                Seq = item.Seq,
                GoogleCode = item.Code ?? string.Empty
            };
        }

        public override string ToString()
        {
            if (this.Enabled)
            {
                return string.Format("{0} {1}", this.CodeId, this.CodeName);
            }
            else
            {
                return string.Format("{0} {1} (關閉)", this.CodeId, this.CodeName);
            }
        }

        public static List<DealTypeNode> ConvertToFlatNodes(List<DealTypeNode> categories)
        {
            List<DealTypeNode> result = new List<DealTypeNode>();

            Action<List<DealTypeNode>> act = null;
            act = delegate(List<DealTypeNode> argCategories)
            {
                foreach (var category in argCategories)
                {
                    result.Add(category);
                    act(category.Children);
                }
            };

            act(categories);

            return result;
        }

        public bool CheckExistByIdOrName(List<DealTypeNode> categories)
        {
            bool match = true;
            List<DealTypeNode> nodes = this.GetParents();
            nodes.Add(this);

            var comparisons = categories;
            foreach(var node in nodes)
            {
                var comparison = comparisons.FirstOrDefault(
                    t => ((t.CodeId == node.CodeId && t.CodeId != 0) ||
                        t.CodeName == node.CodeName && string.IsNullOrEmpty(t.CodeName) == false));
                if (comparison == null)
                {
                    match = false;
                    break;
                }
                comparisons = comparison.Children;
            }
            return match;
        }

        public ModifiedStatusType CheckModifiedStatus(List<DealTypeNode> categories, out string message)
        {
            message = string.Empty;
            bool matchParents = true;
            List<DealTypeNode> nodes = this.GetParents();            

            var comparisons = categories;
            foreach(var node in nodes)
            {
                var comparison = comparisons.FirstOrDefault(
                    t => ((t.CodeId == node.CodeId && t.CodeId != 0) ||
                        t.CodeName == node.CodeName && string.IsNullOrEmpty(t.CodeName) == false));
                if (comparison == null)
                {
                    matchParents = false;
                    break;
                }
                comparisons = comparison.Children;
            }
            if (matchParents == false)
            {
                message = string.Format("{0}{1}父節點資料不正確", this.CodeId, this.CodeName);
                return ModifiedStatusType.Error;
            }

            if (this.CodeId == 0)
            {
                var comparison = comparisons.FirstOrDefault(t => t.CodeName == this.CodeName);
                if (comparison != null)
                {
                    this.CodeId = comparison.CodeId;
                    return ModifiedStatusType.None;
                }
                return ModifiedStatusType.IsNew;
            }
            var comparisonNode = DealTypeFacade.Find(categories, this.CodeId);
            if (comparisonNode == null)
            {
                return ModifiedStatusType.IsNew;
            }
            if (this.Match(comparisonNode))
            {
                return ModifiedStatusType.None;
            }
            return ModifiedStatusType.Modified;
        }

        private bool Match(DealTypeNode comparison)
        {
            return this.CodeId == comparison.CodeId &&
                this.CodeName == comparison.CodeName &&
                this.GoogleCode == comparison.GoogleCode &&
                this.Enabled == comparison.Enabled;
        }

        public bool RoughMatch(int codeId, string codeName)
        {
            if (this.CodeId == 0)
            {
                return this.CodeName.Equals(codeName);
            }
            return this.CodeId == codeId;
        }

        /// <summary>
        /// 取得父節點，從遠到近
        /// </summary>
        /// <returns></returns>
        public List<DealTypeNode> GetParents()
        {
            List<DealTypeNode> result = new List<DealTypeNode>();
            DealTypeNode item = this.Parent;
            while (item != null)
            {
                result.Add(item);
                item = item.Parent;
            }
            result.Reverse();
            return result;
        }

        /// <summary>
        /// 取得父節點，從遠到近
        /// </summary>
        /// <returns></returns>
        public List<DealTypeNode> GetParentsContainSelf()
        {
            List<DealTypeNode> result = new List<DealTypeNode>();
            DealTypeNode item = this;
            while (item != null)
            {
                result.Add(item);
                item = item.Parent;
            }
            result.Reverse();
            return result;
        }
    }

    public enum ModifiedStatusType
    {
        None, IsNew, Modified, Error
    }

    public class ProductCategoryModifiedLog
    {
        public DealTypeNode Target {get;set;}
        public ModifiedStatusType Action { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", this.Action, this.Target.CodeName);
        }
    }
}

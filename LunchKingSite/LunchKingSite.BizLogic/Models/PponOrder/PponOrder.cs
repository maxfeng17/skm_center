﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.PponOrder
{
    public class PponOrder
    {

    }

    /// <summary>
    /// [v3.0] 訂單總覽
    /// </summary>
    public class MemberOrderOverview
    {
        /// <summary>
        /// 主分類
        /// </summary>
        public MemberOrderMainFilterType MainFilterType { get; set; }
        /// <summary>
        /// 主分類名稱
        /// </summary>
        public string MainFilterName { get; set; }
        /// <summary>
        /// 主分類圖片
        /// </summary>
        public string MainFilterImage { get; set; }
        /// <summary>
        /// 主分類訂單筆數
        /// </summary>
        public int OrderCount { get; set; }
    }
    
    /// <summary>
    /// [v3.0] 訂單分類結構
    /// </summary>
    public class OrderFilter
    {
        /// <summary>
        /// 訂單主分類
        /// </summary>
        public MemberOrderMainFilterType MainFilterType { get; set; }
        /// <summary>
        /// 訂單主分類
        /// </summary>
        public string MainFilterTypeDesc { get; set; }
        /// <summary>
        /// 訂單主分類圖示
        /// </summary>
        [JsonIgnore]
        public string MainFilterTypeImagePath { get; set; }
        /// <summary>
        /// 主分類內的訂單數量
        /// </summary>
        public int OrderCount { get; set; }
        /// <summary>
        /// 預設子分類
        /// </summary>
        public MemberOrderSubFilterType DefaultSubFilter { get; set; }
        /// <summary>
        /// 子分類
        /// </summary>
        public List<SubFilter> SubFilters { get; set; }

        public OrderFilter()
        {
            MainFilterTypeDesc = string.Empty;
            MainFilterTypeImagePath = string.Empty;
            OrderCount = 0;
        }
    }

    /// <summary>
    /// [v3.0] 訂單子分類結構
    /// </summary>
    public class SubFilter
    {
        /// <summary>
        /// 訂單子分類
        /// </summary>
        public MemberOrderSubFilterType SubFilterType { get; set; }
        /// <summary>
        /// 訂單子分類名稱
        /// </summary>
        public string SubFilterTypeDesc { get; set; }
        /// <summary>
        /// 子分類內的訂單數量
        /// </summary>
        [JsonIgnore]
        public int OrderCount { get; set; }

        public SubFilter()
        {
            SubFilterTypeDesc = string.Empty;
            OrderCount = 0;
        }
    }
}

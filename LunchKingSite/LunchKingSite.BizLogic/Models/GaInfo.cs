﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{
    public class GaInfo
    {
        public GaDealCollection GaDealData { get; set; }
        public ViewGaDealList ViewGaDealListInfo { get; set; }
        public string Title { get; set; }
        public string JsonData { get; set; }
    }
}

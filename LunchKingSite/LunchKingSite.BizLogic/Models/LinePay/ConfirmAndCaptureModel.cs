﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model
{
    public class ConfirmAndCaptureModel : LinePayBaseOutputModel
    {
        [JsonProperty("info")]
        public ConfirmOutputInfo Info { set; get; }
        /// <summary>
        /// paytype 為 Preapproved 的時後, 以後可直接選用的自動付款金鑰(15位數)
        /// </summary>
        [JsonProperty("regKey")]
        public string RegKey { set; get; }
    }

    public class ConfirmOutputInfo
    {
        /// <summary>
        /// 訂單編號 (17Life Order Guid)
        /// </summary>
        [JsonProperty("orderId")]
        public string OrderId { set; get; }
        /// <summary>
        /// Line 交易序號 Reserve 產生時之序號
        /// </summary>
        [JsonProperty("transactionId")]
        public string TransactionId { set; get; }
        /// <summary>
        /// 可選擇的授權過期日(當付款為Authorization capture=false)
        /// </summary>
        [JsonProperty("authorizationExpireDate")]
        public string AuthorizationExpireDate { set; get; }

        [JsonProperty("payInfo")]
        public ConfirmOutputPayInfo[] PayInfo { set; get; }
    }

    public class ConfirmOutputPayInfo
    {
        /// <summary>
        /// 付款方式(信用卡 CREDIT_CARD, 餘額 BALANCE
        /// </summary>
        [JsonProperty("method")]
        public string Method { set; get; }
        /// <summary>
        /// 付款金額
        /// </summary>
        [JsonProperty("amount")]
        public decimal Amount { set; get; }
        /// <summary>
        /// 信用卡暱稱(用戶自訂/可更改/更改後不會通知商家)
        /// </summary>
        [JsonProperty("creditCardNickname")]
        public string CreditCardNickname { set; get; }
        /// <summary>
        /// 信用卡品牌 VISA/MASTER/AMEX/DINERS/JCB
        /// </summary>
        [JsonProperty("creditCardBrand")]
        public string CreditCardBrand { set; get; }
    }
}

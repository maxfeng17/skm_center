﻿using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model
{
    public class ReserveInputModel
    {
        [JsonProperty("productName")]
        public string ProductName { set; get; }
        [JsonProperty("productImageUrl")]
        public string ProductImageUrl { set; get; }
        [JsonProperty("amount")]
        public int Amount { set; get; }
        [JsonProperty("currency")]
        public string Currency { set; get; }
        [JsonProperty("orderId")]
        public string OrderId { set; get; }
        [JsonProperty("confirmUrl")]
        public string ConfirmUrl { set; get; }
        [JsonProperty("cancelUrl")]
        public string CancelUrl { set; get; }
        [JsonProperty("langCd")]
        public string LangCd { set; get; }
        /// <summary>
        /// 導回時是否必需同一個 browser.
        /// </summary>
        [JsonProperty("checkConfirmUrlBrowser")]
        public bool CheckConfirmUrlBrowser { set; get; }
        /// <summary>
        /// Confirm 時是否直接請款，false : 分開
        /// </summary>
        [JsonProperty("capture")]
        public bool Capture { set; get; }
    }
}

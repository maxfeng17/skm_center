﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model
{
    public class ReserveOutputModel : LinePayBaseOutputModel
    {
        [JsonProperty("info")]
        public ReserveOutputInfo Info { set; get; }
    }

    public class ReserveOutputInfo
    {
        [JsonProperty("transactionId")]
        public string TransactionId { set; get; }
        [JsonProperty("paymentUrl")]
        public Dictionary<string, string> PaymentUrl { set; get; }
    }
}

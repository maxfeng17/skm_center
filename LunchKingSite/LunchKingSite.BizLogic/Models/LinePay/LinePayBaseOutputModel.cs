﻿using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model
{
    public class LinePayBaseOutputModel
    {
        /// <summary>
        /// 結果代碼
        /// </summary>
        [JsonProperty("returnCode")]
        public string ReturnCode { set; get; }
        /// <summary>
        /// 結果訊息/失敗理由
        /// </summary>
        [JsonProperty("returnMessage")]
        public string ReturnMessage { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Model;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model
{
    public class RefundOutputModel : LinePayBaseOutputModel
    {
        [JsonProperty("info")]
        public RefundTransInfo Info { set; get; }
    }

    public class RefundTransInfo
    {
        [JsonProperty("refundTransactionId")]
        public string RefundTransactionId { set; get; }
        [JsonProperty("refundTransactionDate")]
        public string RefundTransactionDate { set; get; }
    }
}

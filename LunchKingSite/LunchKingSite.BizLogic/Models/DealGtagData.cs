﻿using System;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models
{
    public class DealGtagData : TrackTagBase
    {
        public DealGtagData() { }

        public DealGtagData(TrackTagInputModel tt)
        {
            ProductId = tt.ViewPponDealData.BusinessHourGuid.ToString();
            ItemPrice = Convert.ToInt32(tt.ViewPponDealData.ItemPrice).ToString();
            PageType = tt.GtagPage.ToString().ToLower();
            if (tt.ViewPponDealData.DealType != null)
            {
                Category = SystemCodeManager.GetDealTypeName(tt.ViewPponDealData.DealType.Value);
            }
            SendTo = tt.Config.GoogleGtagSendToData;
        }

        public override string GetJson()
        {
            if (string.IsNullOrEmpty(SendTo)) return string.Empty;

            var data = string.Format("gtag('event', 'page_view',{0});",
                JsonConvert.SerializeObject(this));
            return data;
        }

        [JsonProperty("send_to")]
        public string SendTo { get; set; }

        [JsonProperty("ecomm_pagetype")]
        public string PageType { get; set; }

        [JsonProperty("ecomm_prodid")]
        public string ProductId { get; set; }

        [JsonProperty("ecomm_totalvalue")]
        public string ItemPrice { get; set; }

        [JsonProperty("ecomm_category")]
        public string Category { get; set; }
    }

    public class DealGtagConversionData : TrackTagBase
    {
        public DealGtagConversionData() { }

        public DealGtagConversionData(TrackTagInputModel tt):
            this(tt, tt.Config.GoogleGtagConversionSendToData)
        {
        }
        public DealGtagConversionData(TrackTagInputModel tt, string googleGtagConversionSendToData)
        {
            OrderTotal = tt.OrderTotalAmountWithoutDiscount.ToString();
            OrderGuid = tt.OrderGuid.ToString();
            SendTo = googleGtagConversionSendToData;
        }

        public override string GetJson()
        {
            if (string.IsNullOrEmpty(SendTo)) return string.Empty;

            var data = string.Format("gtag('event', 'conversion',{0});",
                JsonConvert.SerializeObject(this));
            return data;
        }

        [JsonProperty("send_to")]
        public string SendTo { get ; set; }

        [JsonProperty("value")]
        public string OrderTotal { get; set; }

        [JsonProperty("currency")]
        public string Currency
        {
            get { return "TWD"; }
        }

        [JsonProperty("transaction_id")]
        public string OrderGuid { get; set; }
    }

    public enum GtagPageType
    {
        Home,
        SearchResults,
        Category,
        Product,
        Cart,
        Purchase,
        Other
    }
}

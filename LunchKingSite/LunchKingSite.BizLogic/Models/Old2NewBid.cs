﻿using System;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models
{
    public class Old2NewBid
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("old")]
        public Guid OldBid { get; set; }
        [JsonProperty("new")]
        public Guid NewBid { get; set; }
        [JsonProperty("code")]
        public string TrackingCode { get; set; }
    }
}

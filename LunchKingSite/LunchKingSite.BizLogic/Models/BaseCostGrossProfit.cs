﻿using System;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{
    public class BaseCostGrossMargin
    {
        public Guid BusinessHourGuid { get; set; }
        public decimal BaseGrossMargin { set; get; }
        public bool IsLoaded { set; get; }
        public decimal MinGrossMargin { set; get; }

        public BaseCostGrossMargin()
        {
            IsLoaded = false;
        }

        public BaseCostGrossMargin(ViewDealBaseGrossMargin item)
        {
            IsLoaded = item.IsLoaded;
            if (!item.IsLoaded)
            {
                BaseGrossMargin = 0;
                BusinessHourGuid = Guid.Empty;
                return;
            }
            BusinessHourGuid = item.BusinessHourGuid;
            BaseGrossMargin = item.GrossMargin ?? 0;
            MinGrossMargin = item.TaxGrossMargin ?? 0;
        }
    }
}
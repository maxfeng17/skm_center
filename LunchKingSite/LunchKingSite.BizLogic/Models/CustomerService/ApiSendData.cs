﻿using System;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class ApiSendData
    {
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("userId")]
        public string UserId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("serviceNo")]
        public string ServiceNo { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("categoryId")]
        public int CategoryId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }
        
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("orderGuid")]
        public string OrderGuid { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("content")]
        public string Content { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("image")]
        public string Image { get; set; }
        [JsonProperty("source")]
        public int Source { get; set; }
        [JsonProperty("IssueFromType")]
        public int IssueFromType { get; set; }
        

        public ApiSendData()
        {
            Source = 1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class ShippingDateModel
    {

        public string ShipTypeDesc { get; set; }
        /// <summary>
        /// 最後出貨日
        /// </summary>
        public DateTime? LastShipDate { get; set; }
    }

    /// <summary>
    /// 最晚出貨日所需參數
    /// </summary>
    public class ShippingDateInputModel
    {
        /// <summary>
        /// 出貨方式
        /// </summary>
        public int? ShipType { get; set; }
        /// <summary>
        /// 依下單日或依結檔日
        /// </summary>
        public int? ShippingdateType { get; set; }

        /// <summary>
        /// 訂單成立(n)個工作天
        /// </summary>
        public int? AfterOrderedDays { get; set; }
        /// <summary>
        /// 上檔後(n)個工作天
        /// </summary>
        public int? AfterDealStartDays { get; set; }
        /// <summary>
        /// 結檔後(n)個工作天
        /// </summary>
        public int? AfterDealEndDays { get; set; }
        
        /// <summary>
        /// 訂單建立日
        /// </summary>
        public DateTime OrderCreateTime { get; set; }
        /// <summary>
        /// 結檔日
        /// </summary>
        public DateTime BusinessHourOrderTimeE { get; set; }
        /// <summary>
        /// 最後出貨日
        /// </summary>
        public DateTime? BusinessHourDeliverTimeE { get; set; }
    }

    //即將逾期出貨通知
    public class OrderShipInfo
    {
        public int ItemNo { get; set; }
        public DateTime BusinessHourOrderTimeS { get; set; }
        public DateTime BusinessHourOrderTimeE { get; set; }
        public string ShipType { get; set; }
        public DateTime? LastShipDate { get; set; }
        public int DealUniqueId { get; set; }
        public string ItemName { get; set; }
        public int OrderCount { get; set; }
        public int NearingExpirationOrderCount { get; set; }
    }

}

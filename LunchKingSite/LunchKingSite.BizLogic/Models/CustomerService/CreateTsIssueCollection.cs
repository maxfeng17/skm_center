﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class CreateTsIssueCollection
    {
        [Required]
        public int CategoryId { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        [Required]
        public string Content { get; set; }

        public string Image { get; set; }

        public string ServiceNo { get; set; }
        
        [Required]
        [Range(1,20)]// 0 = 本站 For新光要大於0
        public int IssueFromType { get; set; }

        [Required]
        public string TsMemberNo { get; set; }
    }

}

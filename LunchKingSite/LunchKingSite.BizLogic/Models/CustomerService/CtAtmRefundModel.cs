﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    /// <summary>
    /// ATM 帳戶資訊
    /// </summary>
    public class CtAtmRefundModel
    {
        /// <summary>
        /// 戶名
        /// </summary>
        public string AccountName { get; set; }
        /// <summary>
        /// 身份證字號
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 銀行代號
        /// </summary>
        public string BankNo { get; set; }
        /// <summary>
        /// 銀行名稱
        /// </summary>
        public string BankName { get; set; }
        /// <summary>
        /// 分行代號
        /// </summary>
        public string BranchNo { get; set; }
        /// <summary>
        /// 分行名稱
        /// </summary>
        public string BranchName { get; set; }
        /// <summary>
        /// 帳號
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// 電話/手機
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 驗證
        /// </summary>
        /// <returns></returns>
        public bool IsAtmInfoValidity(out string errMsg)
        {
            errMsg = string.Empty;

            if (string.IsNullOrEmpty(AccountName))
            {
                errMsg = "戶名錯誤!";
                return false;
            }
            if (string.IsNullOrEmpty(Id) || RegExRules.CheckPersonIdOrCompanyNo(Id) != string.Empty)
            {
                errMsg = "身份證或統編錯誤!";
                return false;
            }
            if (string.IsNullOrEmpty(BankNo))
            {
                errMsg = "銀行代號錯誤!";
                return false;
            }
            if (string.IsNullOrEmpty(BankName))
            {
                errMsg = "銀行名稱錯誤!";
                return false;
            }
            if (string.IsNullOrEmpty(BranchNo))
            {
                errMsg = "分行代號錯誤!";
                return false;
            }
            if (string.IsNullOrEmpty(BranchName))
            {
                errMsg = "分行名稱錯誤!";
                return false;
            }
            if (string.IsNullOrEmpty(AccountNumber) || AccountNumber.Trim().Length > 14 || !AccountNumber.Trim().All(char.IsDigit))
            {
                errMsg = "帳號錯誤!";
                return false;
            }
            if (string.IsNullOrEmpty(Phone))
            {
                errMsg = "手機號碼錯誤!";
                return false;
            }
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class CreateTsOrderIssueCollection
    {
        [Required]
        public int CategoryId { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        [Required]
        public string Content { get; set; }

        public string Image { get; set; }

        public string ServiceNo { get; set; }
        
        [Required]
        [Range(1, 20)]// 0 = 本站 For新光要大於0
        public int IssueFromType { get; set; }

        [Required]
        public string OrderGuid { get; set; }

        [Required]
        public string TsMemberNo { get; set; }
    }

    public class PreTsIssue : CreateTsOrderIssueCollection
    {

        public PreTsIssue(CreateTsOrderIssueCollection data)
        {
            CategoryId = data.CategoryId;
            Name = data.Name;
            Mobile = data.Mobile;
            Email = data.Email;
            Content = data.Content;

            if (string.IsNullOrEmpty(data.Image) == false)
            {
                var index = data.Image.IndexOf(',');
                Image = index > -1 ? data.Image.Substring(index + 1, data.Image.Length - (index + 1)) : data.Image;
            }

            ServiceNo = data.ServiceNo;
            IssueFromType = data.IssueFromType;
            OrderGuid = data.OrderGuid;
            TsMemberNo = data.TsMemberNo;
        }

        public PreTsIssue(CreateTsIssueCollection data)
        {
            CategoryId = data.CategoryId;
            Name = data.Name;
            Mobile = data.Mobile;
            Email = data.Email;
            Content = data.Content;

            if (string.IsNullOrEmpty(data.Image) == false)
            {
                var index = data.Image.IndexOf(',');
                Image = index > -1 ? data.Image.Substring(index + 1, data.Image.Length - (index + 1)) : data.Image;
            }

            ServiceNo = data.ServiceNo;
            IssueFromType = data.IssueFromType;
            OrderGuid = null;
            TsMemberNo = data.TsMemberNo;
        }

        public int MainCategoryId { get; set; }
        public int SubCategoryId { get; set; }

        public void SetCategoryId(int mainId, int subId)
        {
            MainCategoryId = mainId;
            SubCategoryId = subId;
        }
    }
}

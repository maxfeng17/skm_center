﻿using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class CategoryData
    {
        public List<ParentCategory> Category { get; set; }
        public CategoryData()
        {
            Category = new List<ParentCategory>();
        }

    }

    public class ParentCategory
    {
        public int ParentId { get; set; }
        public string ParentName { get; set; }
    }


}

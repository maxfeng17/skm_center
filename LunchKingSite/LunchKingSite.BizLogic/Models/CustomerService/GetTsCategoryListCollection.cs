﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class GetTsCategoryListCollection
    {
        [Required]
        [Range(0,1)]
        public int CategoryType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class SampleModel
    {
        public int id { get; set; }
        public string content { get; set; }
        public string subject { get; set; }
    }
    public class MainModel
    {
        public List<SampleModel> sampleList { get; set; }
    }
}

﻿using System;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class ApiGetData
    {
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("userId")]
        public string UserId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("serviceNo")]
        public string ServiceNo { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("pageCnt")]
        public int PageCnt { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("orderGuid")]
        public string OrderGuid { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("type")]
        public int Type { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("isGetAll")]
        public bool IsGetAll { get; set; }

    }

    public class GetCategoryListInput
    {
        public string UserId { get; set; }
        public int CategoryId { get; set; }
    }
}

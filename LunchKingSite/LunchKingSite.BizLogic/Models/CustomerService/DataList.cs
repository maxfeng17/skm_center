﻿using System.Collections.Generic;
using System;


namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class DataList
    {
        public List<Issue> IssueList { get; set; }
        public bool News { get; set; }
    }

    public class Issue
    {
        /// <summary>
        /// 案件編號
        /// </summary>
        public string ServiceNo { get; set; }
        /// <summary>
        /// 案件類型  / 訂單名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 訂單編號，若是一般客服問題則為空值
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 訂單Guid，若是一般客服問題則為空值
        /// </summary>
        public string OrderGuid { get; set; }
        /// <summary>
        /// 最後修改時間
        /// </summary>
        public string ModifyTime { get; set; }
        /// <summary>
        /// 是否有新訊息，true & false判別
        /// </summary>
        public bool NewMessage { get; set; }
        /// <summary>
        /// 訂單縮圖
        /// </summary>
        public string MainImagePath { get; set; }
        /// <summary>
        /// 最新案件訊息內文
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 接續案件編號
        /// </summary>
        public string ParentServiceNo { get; set; }
        public Issue()
        {
            OrderId = string.Empty;
            OrderGuid = string.Empty;
            ModifyTime = string.Empty;
            MainImagePath = string.Empty;
            Content = string.Empty;
            ParentServiceNo = string.Empty;
        }
    }




}

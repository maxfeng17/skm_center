﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class InsideLogInput
    {
        public string ServiceNo { get; set; }
    }

    public class InsideLogData 
    {
        protected static ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        public InsideLogData(ViewCustomerServiceInsideLog log) 
        {
            ServiceNo = log.ServiceNo;
            Content = log.Content;
            File = string.IsNullOrEmpty(log.File) ? string.Empty : string.Format("{0}/Images/Service/InSide/{1}/{2}", conf.SiteUrl, log.ServiceNo, log.File);
            Status = Helper.GetEnumDescription((statusConvert)log.Status);
            CreateTime = log.CreateTime.ToString("yyyy/MM/dd HH:mm:ss");
            Name = log.Name;
            Memo = string.IsNullOrEmpty(log.Memo) ? string.Empty : log.Memo;
        }
        public string ServiceNo { get; set; }
        public string Content { get; set; }
        public string File { get; set; }
        public string Status { get; set; }
        public string CreateTime { get; set; }
        public string Name { get; set; }
        public string Memo { get; set; }
    }
}

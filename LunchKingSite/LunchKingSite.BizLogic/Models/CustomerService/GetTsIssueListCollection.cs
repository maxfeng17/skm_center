﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using LunchKingSite.Core;
using MongoDB.Bson;

namespace LunchKingSite.BizLogic.Models.CustomerService
{

    public class GetTsIssueListCollection
    {

        [Required]
        [Range(1,20)] // 0 = 本站 For台新要大於0
        public int IssueFromType { get; set; }

        [Required]
        [Range(1,2)]
        public int IssueType { get; set; }

        [Required]
        public string TsMemberNo  { get; set; }
    }
    
}

﻿using System.Collections.Generic;


namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class MainNews
    {
        public List<NewDetail> Main { get; set; }
    }

    public class NewDetail
    {
        public string Category { get; set; }
        public int Count { get; set; }
    }
}

﻿using System.Collections.Generic;


namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class ReplyData
    {
        public string name { get; set; }
        public string account { get; set; }
        public string content { get; set; }
        public string method { get; set; }
        public string date { get; set; }
        public string image { get; set; }
        public string link { get; set; }
    }

}

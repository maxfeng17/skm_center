﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class CaseData
    {

        /// <summary>
        /// 基本資料
        /// </summary>
        public CustomerServiceCase BaseData { get; set; }
        /// <summary>
        /// 客服對話記錄-與商家對話
        /// </summary>
        public List<CustomerServiceConversation> InsideMessage { get; set; }
        /// <summary>
        /// 客服對話記錄-與客戶對話
        /// </summary>
        public List<CustomerServiceConversation> OutsideMessage { get; set; }
        /// <summary>
        /// 問題分類
        /// </summary>
        public List<KeyValuePair<int, string>> Category { get; set; }
        /// <summary>
        /// 主因
        /// </summary>
        public List<KeyValuePair<int, string>> SubCategory { get; set; }

        public CaseData()
        {
            BaseData = new CustomerServiceCase();
            InsideMessage = new List<CustomerServiceConversation>();
            OutsideMessage = new List<CustomerServiceConversation>();
            Category = new List<KeyValuePair<int, string>>();
            SubCategory = new List<KeyValuePair<int, string>>();
        }
    }
/// <summary>
    /// 案件資料
    /// </summary>
    public class CustomerServiceCase
    {
        /// <summary>
        /// 案件編號
        /// </summary>
        public string ServiceNo { get; set; }
        /// <summary>
        /// 案件狀態
        /// </summary>
        public statusConvert Status { get; set; }
        /// <summary>
        /// 案件狀態說明
        /// </summary>
        public string StatusDesc { get; set; }
        /// <summary>
        /// 處理人員1
        /// </summary>
        public string WorkerOne { get; set; }
        /// <summary>
        /// 處理人員2
        /// </summary>
        public string WorkerTwo { get; set; }
        /// <summary>
        /// 問題分類
        /// </summary>
        public int Category { get; set; }
        /// <summary>
        /// 主因
        /// </summary>
        public int SubCategory { get; set; }
        /// <summary>
        /// 問題等級
        /// </summary>
        public int QuestoinLevel { get; set; }
        /// <summary>
        /// 案件來源
        /// </summary>
        public int QuestionFrom { get; set; }
        /// <summary>
        /// 案件處理狀態
        /// </summary>
        public int QuestionDeal { get; set; }
        /// <summary>
        /// 處理人員2候選名單
        /// </summary>
        public List<KeyValuePair<string, string>> WorkerTwoList { get; set; }
        /// <summary>
        /// 是否顯示處理人員1的[通知我]按鈕
        /// </summary>
        public bool IsShowNotify { get; set; }
        /// <summary>
        /// 是否顯示處理人員2的[通知我]按鈕
        /// </summary>
        public bool IsShowSecNotify { get; set; }
        /// <summary>
        /// 是否顯示加入處理人員2
        /// </summary>
        public bool IsShowSelectWorkerTwo { get; set; }

        public CustomerServiceCase()
        {
            WorkerTwoList = new List<KeyValuePair<string, string>>();
        }
    }

    /// <summary>
    /// 客服對話記錄
    /// </summary>
    public class CustomerServiceConversation
    {
        /// <summary>
        /// 留言者
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 狀態
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 內文
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 附加檔案名稱
        /// </summary>
        public string AttachedFilesName { get; set; }
        /// <summary>
        /// 附加檔案連結 
        /// </summary>
        public string AttachedFilesNavigateUrl { get; set; }
        /// <summary>
        /// 建立時間
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        public string Memo { get; set; }
        /// <summary>
        /// 是否為客服留言
        /// </summary>
        public bool IsByCustomerService { get; set; }
    }

    /// <summary>
    /// 回覆案件訊息
    /// </summary>
    public class PushMessageInputModel
    {
        /// <summary>
        /// 案件編號
        /// </summary>
        public string ServiceNo { get; set; }
        /// <summary>
        /// 問題等級
        /// </summary>
        public int Priority { get; set; }
        /// <summary>
        /// 問題類別
        /// </summary>
        public int Category { get; set; }
        /// <summary>
        /// 主因
        /// </summary>
        public int SubCategory { get; set; }
        /// <summary>
        /// 案件狀態
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 回覆內文
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }
    }

}

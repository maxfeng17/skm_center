﻿using System.Collections.Generic;
using System;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.Core.Enumeration;


namespace LunchKingSite.BizLogic.Models.CustomerService
{
    /// <summary>
    /// Web 與 App 共用的對話記錄
    /// </summary>
    public class CommunicateData
    {
        public List<CommunicateDetail> Communicate { get; set; }
        public int Status { get; set; }
        public bool IsGetBanner { get; set; }
        public string Name { get; set; }
        public string OrderId { get; set; }
        public string OrderGuid { get; set; }
        public string ModifyTime { get; set; }
        public string MainImagePath { get; set; }
        public int TotalPage { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
       
        public CommunicateData()
        {
            Communicate = new List<CommunicateDetail>();
            Status = (int)statusConvert.wait;
            IsGetBanner = false;
            Name = string.Empty;
            OrderId = string.Empty;
            OrderGuid = string.Empty;
            ModifyTime = ApiSystemManager.DateTimeToDateTimeString(DateTime.Now); ;
            MainImagePath = string.Empty;
            TotalPage = 0;
            CategoryId = 0;
            CategoryName = string.Empty;
        }
    }
    

    public class CommunicateDetail
    {
        public string ServiceNo { get; set; }
        public string Content { get; set; }
        public string ImageUrl { get; set; }
        public string SmallImageUrl { get; set; }
        public int SmallImageWidth { get; set; }
        public int SmallImageHieght { get; set; }
        public string CreateTime { get; set; }
        public string CreateName { get; set; }
        public int Method { get; set; }
    }



}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.CustomerService
{
    public class GetTsCommunicateListByServiceNoCollection
    {
        [Required]
        public string ServiceNo { get; set; }

        [Required]
        public bool IsGetAll { get; set; }

        [Required]
        [Range(1, 10000)]
        public int PageCount { get; set; }

        [Required]
        public string DeviceType { get; set; }

        [Required]
        public int IssueFromType { get; set; }

        [Required]
        public string TsMemberNo { get; set; }
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Web;

namespace LunchKingSite.BizLogic.Model
{
    public class HiDealProductEntity
    {
        #region fields
        public IHiDealProvider _hdprovider = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        private HiDealProduct _product = null;
        private OptionCatgCollection _options = null;
        private DateTime processTime = DateTime.Now;
        #endregion

        #region Properties
        public HiDealProduct Product
        {
            get { return _product; }
            set { _product = value; }
        }

        private List<ProductBranchStoreSetting> _branchStoreSetting = null;
        /// <summary>
        /// Gets or sets the quantitative information of all branch stores and the sequence of the branch stores for this product.
        /// </summary>
        public List<ProductBranchStoreSetting> BranchStoreSettings 
        { 
            get
            {
                if(_branchStoreSetting == null) //HiDealProductStoreGetByProduct
                {
                    _branchStoreSetting = new List<ProductBranchStoreSetting>();
                    HiDealProductStoreCollection storeCollection = _hdprovider.HiDealProductStoreGetListByProductId(Product.Id);
                    foreach(HiDealProductStore store in storeCollection)
                    {
                        _branchStoreSetting.Add(new ProductBranchStoreSetting{StoreGuid = store.StoreGuid, Sequence = store.Seq.HasValue ? store.Seq.Value : -1, QuantityLimit = store.QuantityLimit.HasValue ? store.QuantityLimit.Value : -1});
                    }
                }
                return _branchStoreSetting;
            }
            set { _branchStoreSetting = value; }
        }
        
        public OptionCatgCollection Options
        {
            get
            {
                if(_options == null)
                {
                    _options = new OptionCatgCollection(Product.Id);
                }
                return _options;
            }
        }

        #endregion

        #region public methods
        
        /// <summary>
        /// 商品 [hi_deal_product], 分店 [hi_deal_product_store], 其他選單 [hi_deal_product_option_category] & [hi_deal_product_option_item] 的資料更新.
        /// </summary>
        public void Save()
        {
            processTime = DateTime.Now;

            _hdprovider.HiDealProductSet(Product);

            #region 分店

            HiDealProductStoreCollection storeCollection = _hdprovider.HiDealProductStoreGetListByProductId(Product.Id);
            
            foreach(HiDealProductStore store in storeCollection)    //delete & modify
            {
                int i = BranchStoreSettings.FindIndex(storeSetting => storeSetting.StoreGuid == store.StoreGuid);
                if( i == -1 )
                {
                    _hdprovider.HiDealProductStoreDelete(store);
                }
                else
                {
                    if (store.Seq != BranchStoreSettings[i].Sequence || store.QuantityLimit != BranchStoreSettings[i].QuantityLimit)
                    {
                        store.Seq = BranchStoreSettings[i].Sequence;
                        if (BranchStoreSettings[i].QuantityLimit == 0)
                        {
                            store.QuantityLimit = null;
                        }
                        else
                        {
                            store.QuantityLimit = BranchStoreSettings[i].QuantityLimit;
                        }
                        store.ModifyId = HttpContext.Current.User.Identity.Name;
                        store.ModifyTime = processTime;
                        _hdprovider.HiDealProductStoreSet(store);
                    }
                }
            }

            foreach (ProductBranchStoreSetting storeSetting in BranchStoreSettings)  //insert
            {
                bool foundInDB = false;
                foreach(HiDealProductStore store in storeCollection)       
                {
                    if(store.StoreGuid == storeSetting.StoreGuid)
                    {
                        foundInDB = true;
                    }
                }
                if(!foundInDB)
                {
                    HiDealProductStore newStore = new HiDealProductStore();
                    newStore.HiDealProductId = this.Product.Id;
                    newStore.StoreGuid = storeSetting.StoreGuid;
                    newStore.Seq = storeSetting.Sequence;
                    if (storeSetting.QuantityLimit == 0)
                    {
                        newStore.QuantityLimit = null;
                    }
                    else
                    {
                        newStore.QuantityLimit = storeSetting.QuantityLimit;
                    }
                    newStore.ResourceGuid = Guid.NewGuid();
                    newStore.CreateId = HttpContext.Current.User.Identity.Name;
                    newStore.CreateTime = processTime;
                    _hdprovider.HiDealProductStoreSet(newStore);
                }
            }
            #endregion

            #region 其他選單

            HiDealProductOptionCategoryCollection dbCatgCollection = _hdprovider.HiDealProductOptionCategoryGetListByProductId(Product.Id);
            
            for (int i = dbCatgCollection.Count; i > dbCatgCollection.Count - (dbCatgCollection.Count - this.Options.Count); i--)
            {
                _hdprovider.HiDealProductOptionCategoryDelete(dbCatgCollection[i - 1].Id);
            }

            for(int m = 0; m < this.Options.Count; m++)
            {
                if (m <= dbCatgCollection.Count -1)
                {
                    UpdateCategory(dbCatgCollection[m], this.Options[m].CatgName);
                    UpdateItems(dbCatgCollection[m].Id, this.Options[m].CatgName);
                }
                else
                {
                    HiDealProductOptionCategory newCatg = new HiDealProductOptionCategory();
                    newCatg.ProductId = this.Product.Id;
                    newCatg.CatgName = this.Options[m].CatgName;
                    newCatg.Seq = this.Options[m].CatgSeq;
                    newCatg.CreateId = HttpContext.Current.User.Identity.Name;
                    newCatg.CreateTime = processTime;
                    int catgId =  _hdprovider.HiDealProductOptionCategorySet(newCatg);

                    foreach (OptionItem item in this.Options[m].Items)
                    {
                        HiDealProductOptionItem newItem = new HiDealProductOptionItem();
                        newItem.CatgId = catgId;
                        newItem.Name = item.ItemName;
                        newItem.Seq = item.ItemSeq;
                        newItem.Quantity = item.Quantity;
                        newItem.Price = item.Price;
                        newItem.CreateId = HttpContext.Current.User.Identity.Name;
                        newItem.CreateTime = processTime;
                        _hdprovider.HiDealProductOptionItemSet(newItem);
                    }
                }
            }

            #endregion
        }

        /// <summary>
        /// 新增商品的運費設定
        /// </summary>
        /// <param name="thresholdAmount">階梯式運費的購買金額界線. 若是第一筆運費, 此參數不會被使用.</param>
        /// <param name="freight">運費金額</param>
        /// <param name="type">設定運費的種類. 給廠商的進貨運費或者是從消費者收取的商品運費</param>
        public void AddFreight( float thresholdAmount, float freight, HiDealFreightType type)
        {
            HiDealFreight latestFreight = _hdprovider.HiDealFreightGetLatest(Product.Id, type);

            if (latestFreight.Id == 0)  //沒資料, 要設定第一筆運費 (start amount必為0, end amount必為 1000000000000 (一兆), 無視 parameter thresholdAmount)
            {
                HiDealFreight newFreight = new HiDealFreight { StartAmount = 0, EndAmount = 1000000000000, FreightAmount = (decimal)freight, ProductId = Product.Id, FreightType = (int)type, CreateId = HttpContext.Current.User.Identity.Name, CreateTime = processTime };
                _hdprovider.HiDealFreightSet(newFreight);
            }

            if (latestFreight.Id != 0)  //第N筆運費
            {
                latestFreight.EndAmount = (decimal)thresholdAmount;
                _hdprovider.HiDealFreightSet(latestFreight);
                HiDealFreight newFreight = new HiDealFreight { CreateId = HttpContext.Current.User.Identity.Name, CreateTime = processTime, StartAmount = (decimal)thresholdAmount, EndAmount = 1000000000000, FreightAmount = (decimal)freight, ProductId = Product.Id, FreightType = (int)type };
                _hdprovider.HiDealFreightSet(newFreight);
            }
        }

        public void DeleteFreight(HiDealFreightType type)
        {
            HiDealFreight latestFreight = _hdprovider.HiDealFreightGetLatest(Product.Id, type);
            _hdprovider.HiDealFreightDelete(latestFreight);
            HiDealFreight newLatestFreight = _hdprovider.HiDealFreightGetLatest(Product.Id, type);
            if (newLatestFreight.Id != 0)  //有資料才需要修改.
            {
                newLatestFreight.EndAmount = 1000000000000;
                _hdprovider.HiDealFreightSet(newLatestFreight);
            }
        }

        public void AddProductPurchaseCost(decimal cost, int quantity)
        {
            HiDealProductCost prodCost = new HiDealProductCost
                                             {ProductId = Product.Id, Cost = cost, Quantity = quantity};
            HiDealProductCost latestProdCost = _hdprovider.HiDealProductCostGetLatest(Product.Id);
            if(latestProdCost.Id == 0)
            {
                prodCost.CumulativeQuantitiy = quantity;
            }
            else
            {
                prodCost.CumulativeQuantitiy = latestProdCost.CumulativeQuantitiy + quantity;
            }
            _hdprovider.HiDealProductCostSet(prodCost);
        }

        public void DeleteProductPurchaseCost()
        {
            HiDealProductCost latestProdCost = _hdprovider.HiDealProductCostGetLatest(Product.Id);
            if(latestProdCost.Id != 0)
                _hdprovider.HiDealProductCostDelete(latestProdCost);
        }
        
        #endregion

        #region private methods

        private void UpdateCategory(HiDealProductOptionCategory dbCatg, string optionCatgName)
        {
            if (dbCatg.CatgName != this.Options[optionCatgName].CatgName || dbCatg.Seq != this.Options[optionCatgName].CatgSeq)
            {
                dbCatg.CatgName = this.Options[optionCatgName].CatgName;
                dbCatg.Seq = this.Options[optionCatgName].CatgSeq;
                dbCatg.ModifyId = HttpContext.Current.User.Identity.Name;
                dbCatg.ModifyTime = processTime;
                _hdprovider.HiDealProductOptionCategorySet(dbCatg);
            }
        }

        private void UpdateItems(int dbCatgId, string optionCatgName)
        {
            HiDealProductOptionItemCollection dbItemCollection = _hdprovider.HiDealProductOptionItemGetListdByCategoryId(dbCatgId);
            
            for (int i = dbItemCollection.Count; i > dbItemCollection.Count - (dbItemCollection.Count - this.Options[optionCatgName].Items.Count); i--)
            {
                _hdprovider.HiDealProductOptionItemDeleteById(dbItemCollection[i - 1].Id);
            }

            for(int m = 0; m < this.Options[optionCatgName].Items.Count; m++)
            {
                if(m <= dbItemCollection.Count - 1)
                {
                    if( dbItemCollection[m].Name != this.Options[optionCatgName].Items[m].ItemName || 
                        dbItemCollection[m].Seq != this.Options[optionCatgName].Items[m].ItemSeq ||
                        dbItemCollection[m].Quantity != this.Options[optionCatgName].Items[m].Quantity ||
                        dbItemCollection[m].Price != this.Options[optionCatgName].Items[m].Price)
                    {
                        dbItemCollection[m].Name = this.Options[optionCatgName].Items[m].ItemName;
                        dbItemCollection[m].Seq = this.Options[optionCatgName].Items[m].ItemSeq;
                        dbItemCollection[m].Quantity = this.Options[optionCatgName].Items[m].Quantity;
                        dbItemCollection[m].Price = this.Options[optionCatgName].Items[m].Price;
                        dbItemCollection[m].ModifyId = HttpContext.Current.User.Identity.Name;
                        dbItemCollection[m].ModifyTime = processTime;
                        _hdprovider.HiDealProductOptionItemSet(dbItemCollection[m]);
                    }
                }
                else
                {
                    HiDealProductOptionItem newItem = new HiDealProductOptionItem();
                    newItem.CatgId = dbCatgId;
                    newItem.Name = this.Options[optionCatgName].Items[m].ItemName;
                    newItem.Seq = this.Options[optionCatgName].Items[m].ItemSeq;
                    newItem.Quantity = this.Options[optionCatgName].Items[m].Quantity;
                    newItem.Price = this.Options[optionCatgName].Items[m].Price;
                    newItem.CreateId = HttpContext.Current.User.Identity.Name;
                    newItem.CreateTime = processTime;
                    _hdprovider.HiDealProductOptionItemSet(newItem);
                }
            }
        }

        #endregion

        #region .ctors
        public HiDealProductEntity(int productId)
        {
            HiDealProduct prod = _hdprovider.HiDealProductGet(productId);
            if (prod.Id == 0)
            {
                throw new ArgumentException(string.Format("The provided hi-deal product id [{0}] corresponds to no record.", productId));
            }
            this.Product = prod;
        }
        #endregion
    }

    public class ProductBranchStoreSetting
    {
        public Guid StoreGuid { get; set; }
        public int Sequence { get; set; }
        public int QuantityLimit { get; set; }
    }

    public class OptionCatgCollection : ICollection<OptionCatg>
    {
        private IHiDealProvider _hdProvider;
        private HiDealProductOptionCategoryCollection _optCatgs = null;
        private List<OptionCatg> catgList = null;
        
        #region .ctor
        public OptionCatgCollection(int productId)
        {
            _hdProvider = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _optCatgs = _hdProvider.HiDealProductOptionCategoryGetListByProductId(productId);
            catgList = new List<OptionCatg>();
            foreach (HiDealProductOptionCategory prodOptCatg in _optCatgs)
            {
                catgList.Add(new OptionCatg{CatgId = prodOptCatg.Id, CatgName = prodOptCatg.CatgName, CatgSeq = prodOptCatg.Seq });
            }
        }
        #endregion

        public OptionCatg this[string catgName]
        {
            get
            {
                int i = catgList.FindIndex(delegate(OptionCatg catg) { return catg.CatgName == catgName; });
                if (i != -1)
                {
                    return catgList[i]; 
                }
                return null; 
            }
            set
            {
                int i = catgList.FindIndex(delegate(OptionCatg catg) { return catg.CatgName == catgName; });
                if (i != -1)
                {
                    catgList.RemoveAt(i);
                    catgList.Insert(i, value);
                }
            }
        }

        public OptionCatg this[int index]
        {
            get
            {
                return catgList[index];
            }
            set
            {
                catgList[index] = value;
            }
        }

        #region IEnumerable implementation
        public IEnumerator<OptionCatg> GetEnumerator()
        {
            return new CatgEnumerator(catgList);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public class CatgEnumerator: IEnumerator<OptionCatg>
        {
            private int curIndex;
            private List<OptionCatg> _catgLst = null;
            private OptionCatg curOptionCatg;

            public CatgEnumerator(List<OptionCatg> list)
            {
                _catgLst = list;
                curIndex = -1;
                curOptionCatg = default(OptionCatg);
            }

            public OptionCatg Current
            {
                get { return curOptionCatg; }
            }

            void IDisposable.Dispose() { }

            object IEnumerator.Current
            {
                get { return Current; }
            }

            public bool MoveNext()
            {
                if(++curIndex >= _catgLst.Count)
                {
                    return false;
                }
                else
                {
                    curOptionCatg = _catgLst[curIndex];
                }
                return true;
            }

            public void Reset()
            {
                curIndex = -1;
            }
        }
        #endregion

        #region ICollection implementation

        public void Add(OptionCatg catg)
        {
            Add(catg.CatgName);
        }

        public void Add(string catgName)
        {
            catgList.Add(new OptionCatg(0, catgList.Count + 1, catgName));
        }

        public void Clear()
        {
            catgList.Clear();
        }

        public bool Contains(OptionCatg catg)
        {
            foreach (OptionCatg optCatg in catgList)
            {
                if (optCatg.CatgName == catg.CatgName)
                    return true;
            }
            return false;
        }

        public void CopyTo(OptionCatg[] array, int arrayIndex)
        {
            throw new Exception("This method is not valid for this implementation.");
        }

        public int Count
        {
            get { return catgList.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(OptionCatg catg)
        {
            int i = catgList.FindIndex(delegate(OptionCatg optCatg) { return optCatg.CatgName == catg.CatgName; });
            if (i != -1)
            {
                catgList[i].Items.Clear();
                catgList.RemoveAt(i);
                return true;
            }
            return false;
        }

        #endregion

    }

    public class OptionItemCollection : ICollection<OptionItem> 
    {
        private IHiDealProvider _hdProvider;
        private HiDealProductOptionItemCollection _optItems = null;
        
        private List<OptionItem> itemList = null;

        #region .ctor
        public OptionItemCollection(int catgId)
        {
            if (itemList == null)
            {
                itemList = new List<OptionItem>();
                _hdProvider = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
                _optItems = _hdProvider.HiDealProductOptionItemGetListdByCategoryId(catgId);
                foreach (HiDealProductOptionItem item in _optItems)
                {
                    itemList.Add(new OptionItem { ItemId = item.Id, ItemName = item.Name, ItemSeq = item.Seq, Price = item.Price, Quantity = item.Quantity });
                }
            }
        }

        #endregion

        internal OptionItem this[string itemName]
        {
            get
            {
                int i = itemList.FindIndex(delegate(OptionItem item) { return item.ItemName == itemName; });
                if (i != -1)
                {
                    return itemList[i];
                }
                return null;
            }
            set
            {
                int i = itemList.FindIndex(delegate(OptionItem item) { return item.ItemName == itemName; });
                if (i != -1)
                {
                    itemList.RemoveAt(i);
                    itemList.Insert(i, value);
                }
            }
        }

        internal OptionItem this[int index]
        {
            get
            {
                return itemList[index];
            }
            set
            {
                itemList[index] = value;
            }
        }

        #region IEnumerable implementation
        public IEnumerator<OptionItem> GetEnumerator()
        {
            return new ItemEnumerator(itemList);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public class ItemEnumerator : IEnumerator<OptionItem>
        {
            private int curIndex;
            private List<OptionItem> _itemLst = null;
            private OptionItem curOptionItem;

            public ItemEnumerator(List<OptionItem> list)
            {
                _itemLst = list;
                curIndex = -1;
                curOptionItem = default(OptionItem);
            }

            public OptionItem Current
            {
                get { return curOptionItem; }
            }

            void IDisposable.Dispose() { }

            object IEnumerator.Current
            {
                get { return Current; }
            }

            public bool MoveNext()
            {
                if (++curIndex >= _itemLst.Count)
                {
                    return false;
                }
                else
                {
                    curOptionItem = _itemLst[curIndex];
                }
                return true;
            }

            public void Reset()
            {
                curIndex = -1;
            }
        }
        #endregion

        #region ICollection implementation
        
        public void Add(OptionItem item)
        {
            itemList.Add(item);
        }

        public void Clear()
        {
            itemList.Clear();
        }

        public bool Contains(OptionItem item)
        {
            foreach(OptionItem i in itemList)
            {
                if (item.ItemName == i.ItemName)
                    return true;
            }
            return false;
        }

        public void CopyTo(OptionItem[] array, int arrayIndex)
        {
            throw new Exception("This method is not valid for this implementation.");
        }

        public int Count
        {
            get { return itemList.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(OptionItem item)
        {
            int i = itemList.FindIndex(delegate(OptionItem optItem) { return optItem.ItemName == item.ItemName; });
            if (i != -1)
            {
                itemList.RemoveAt(i);
                return true;
            }
            return false;
        }

        #endregion
    }

    public class OptionCatg
    {
        public int CatgId { get; set; }
        public int CatgSeq { get; set; }
        public string CatgName { get; set; }
        
        private OptionItemCollection _items = null;
        public OptionItemCollection Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new OptionItemCollection(CatgId);
                }
                return _items;
            }
        }
       
        #region .ctors
        public OptionCatg() { }
        public OptionCatg(int catgId, int seq, string name)
        {
            CatgId = catgId;
            CatgSeq = seq;
            CatgName = name;
        }
        #endregion
    }

    public class OptionItem
    {
        public int ItemId { get; set; }
        public int ItemSeq { get; set; }
        public string ItemName { get; set; }
        public int? Quantity { get; set; }
        public decimal? Price { get; set; }

        #region .ctors
        public OptionItem(){}
        public OptionItem(int itemId, int itemSeq, string itemName, int? quantity, decimal? price)
        {
            ItemId = itemId;
            ItemSeq = itemSeq;
            ItemName = itemName;
            Quantity = quantity;
            Price = price;
        }
        #endregion
    }
    
}

﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Models.LimitedTimeSelection
{
    public class DailySelectionEditLogView
    {
        public const string _ACTION_ADD = "新增";
        public const string _ACTION_SORT= "排序";
        public const string _ACTION_REMOVE= "移除";

        public string Action { get; set; }
        public List<DailySelectionEditLogViewItem> Items { get; set; }
        public string CreateId { get; set; }
        public string CreateTime { get; set; }

        public override string ToString()
        {
            int count = Items == null ? 0 : Items.Count;
            return string.Format("{0}{1}{2}個檔次-{3}", 
                CreateId, Action, count, CreateTime);
        }
    }

    public class DailySelectionEditLogViewItem
    {
        public Guid Bid { get; set; }
        public int Sequence { get; set; }
    }
}

using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.EInvoice
{
    public class GovAPIReply
    {
        [JsonProperty("v")]
        public string Version { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("msg")]
        public string Message { get; set; }

        public const string Error = "500";
        public const string OK = "200";
    }
}
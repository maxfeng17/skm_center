﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.BizLogic.Models.EInvoice
{
    public class EinvoiceTransModel : EinvoiceMain
    {
        const string CarrierTypeCode_Member = "EG0071";
        const string CarrierTypeCode_Phone = "3J0002";
        const string CarrierTypeCode_PersonalCertificate = "CQ0001";
        const string CarrierTypeCode_CreditCard = "EK0002";
        protected static ISysConfProvider config = LunchKingSite.Core.Component.ProviderFactory.Instance().GetConfig();

        private bool IsVendor { get; set; }
       
        private static bool ShowContactInfo
        {
            get
            {
                return DateTime.Now < config.ContactToPayeasy || DateTime.Now >= config.PayeasyToContact;
            }
        }

        private static string Com_Name
        {
            get
            {
                return !ShowContactInfo ? "康迅數位整合股份有限公司" : "康太數位整合股份有限公司";
            }
        }

        private static string Com_Id
        {
            get
            {
                return !ShowContactInfo ? config.PayeasyCompanyId : config.ContactCompanyId;
            }
        }

        private bool IsTriplicate
        {
            get
            {
                return InvoiceMode2 == (int)LunchKingSite.Core.InvoiceMode2.Triplicate;
            }
        }
        private decimal Salesamount
        {
            get
            {
                return Math.Round((OrderAmount / (1 + InvoiceTax)), 0);
            }
        }



        public EinvoiceTransModel(EinvoiceMain main)
        {
            this.CopyFrom(main);
        }

        public EinvoiceTransModel(VendorEinvoiceMain main)
        {
            InvoiceNumber = main.InvoiceNumber;
            InvoiceNumberTime = main.InvoiceNumberTime;
            VerifiedTime = main.InvoiceNumberTime;
            InvoiceMode2 = main.InvoiceMode2;
            OrderAmount = main.OrderAmount;
            InvoiceTax = main.InvoiceTax;
            OrderItem = "物流處理費";
            InvoiceComId = main.InvoiceComId;
            InvoiceComName = main.InvoiceComName;
            OrderIsPponitem = true;
            InvoicePass = main.InvoicePass;
            InvoiceBuyerName = main.AccountantName;
            InvoiceBuyerAddress = main.SellerAddress;
            IsVendor = true;
        }

        public EinvoiceTransModel(PartnerEinvoiceMain main)
        {
            InvoiceNumber = main.InvoiceNumber;
            InvoiceNumberTime = main.InvoiceNumberTime;
            VerifiedTime = main.InvoiceNumberTime;
            InvoiceMode2 = main.InvoiceMode2;
            OrderAmount = main.OrderAmount;
            InvoiceTax = main.InvoiceTax;
            OrderItem = main.OrderItem;
            InvoiceComId = main.InvoiceComId;
            InvoiceComName = main.InvoiceComName;
            OrderId = main.PartnerOrderId;
            OrderIsPponitem = true;
            InvoicePass = main.InvoicePass;
            InvoiceBuyerName = main.InvoiceBuyerName;
            InvoiceBuyerAddress = main.InvoiceBuyerAddress;
            LoveCode = main.LoveCode;
        }

        public EinvoiceTransModel(ViewPartnerEinvoiceMaindetail main)
        {
            InvoiceNumber = main.InvoiceNumber;
            InvoiceNumberTime = main.InvoiceNumberTime;
            VerifiedTime = main.InvoiceNumberTime;
            InvoiceMode2 = main.InvoiceMode2;
            OrderAmount = main.OrderAmount;
            InvoiceTax = main.InvoiceTax;
            OrderItem = main.OrderItem;
            InvoiceComId = main.InvoiceComId;
            InvoiceComName = main.InvoiceComName;
            OrderId = main.PartnerOrderId;
            OrderIsPponitem = true;
            InvoicePass = main.InvoicePass;
            InvoiceBuyerName = main.InvoiceBuyerName;
            InvoiceBuyerAddress = main.InvoiceBuyerAddress;
            LoveCode = main.LoveCode;
        }

        #region 新版 : C0401開立

        public XDocument ReturnC0401()
        {
            KeyValuePair<XNamespace, string> xml_namespaces = EinvoiceFacade.GetXmlNameSpace(EinvoiceType.C0401);
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XDocument(new XDeclaration("1.0", "utf-8", "true"),
                    new XElement(xml_namespaces.Key + "Invoice",
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                    new XAttribute(xsi + "schemaLocation", xml_namespaces.Value),
                    ReturnC0401Main(xml_namespaces.Key), ReturnC0401Details(xml_namespaces.Key), ReturnC0401Amount(xml_namespaces.Key))
                );
        }

        private XElement ReturnC0401Main(XNamespace xmlns)
        {
            XElement main = new XElement(xmlns + "Main",
                             new XElement(xmlns + "InvoiceNumber", InvoiceNumber), //發票號碼
                             new XElement(xmlns + "InvoiceDate", EinvoiceFacade.ReturnInvoiceTime(InvoiceNumberTime.Value, VerifiedTime.Value).ToString("yyyyMMdd")), //發票日期
                             new XElement(xmlns + "InvoiceTime", EinvoiceFacade.ReturnInvoiceTime(InvoiceNumberTime.Value, VerifiedTime.Value).ToString("HH:mm:ss")), //發票時間
                             new XElement(xmlns + "Seller", //賣方資訊
                                 new XElement(xmlns + "Identifier", Com_Id),
                                 new XElement(xmlns + "Name", Com_Name)),
                             new XElement(xmlns + "Buyer", //買方資訊
                                 new XElement(xmlns + "Identifier", IsTriplicate ? InvoiceComId : "00000000"), //三聯式填入統編，二聯式填入8個0
                                 new XElement(xmlns + "Name", IsTriplicate ? EinvoiceFacade.MaxLengthString(InvoiceComName, 60) : UserId.ToString())), //三聯式填入公司抬頭，二聯式填入買方姓名
                             new XElement(xmlns + "InvoiceType", DateTime.Now >= config.EinvoiceMIGv312.AddDays(1) ? "07" : "05"), //發票類別 01三聯:02二聯(item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? "01" : "02") //改為05 不分二聯三聯 // 2015年開始改為07
                             new XElement(xmlns + "DonateMark", string.IsNullOrEmpty(LoveCode) ? 0 : 1) //捐贈註記 0非捐贈，1捐贈                           
                             );
            switch (CarrierType)
            {
                //3J0002 手機條碼 ； EJ0018 17LIFE : 自然人憑證載具 CQ0001
                case (int)LunchKingSite.Core.CarrierType.Member:
                    main.Add(
                         new XElement(xmlns + "CarrierType", CarrierTypeCode_Member), //載具類別號碼
                             new XElement(xmlns + "CarrierId1", UserId), //載具顯碼
                             new XElement(xmlns + "CarrierId2", UserId), //載具隱碼
                             new XElement(xmlns + "PrintMark", "N") //紙本發票已列印註記
                        );
                    break;
                case (int)LunchKingSite.Core.CarrierType.Phone:
                    main.Add(
                        new XElement(xmlns + "CarrierType", CarrierTypeCode_Phone),
                            new XElement(xmlns + "CarrierId1", CarrierId),
                            new XElement(xmlns + "CarrierId2", CarrierId),
                            new XElement(xmlns + "PrintMark", "N")
                       );
                    break;
                case (int)LunchKingSite.Core.CarrierType.PersonalCertificate:
                    main.Add(
                       new XElement(xmlns + "CarrierType", CarrierTypeCode_PersonalCertificate),
                           new XElement(xmlns + "CarrierId1", CarrierId),
                           new XElement(xmlns + "CarrierId2", CarrierId),
                           new XElement(xmlns + "PrintMark", "N")
                      );
                    break;
                case (int)LunchKingSite.Core.CarrierType.None:
                default:
                    if (!string.IsNullOrEmpty(LoveCode))
                    {
                        main.Add(
                       new XElement(xmlns + "PrintMark", "N"), //列印紙本註記，捐贈填N
                       new XElement(xmlns + "NPOBAN", LoveCode) //捐贈對象
                     );
                    }
                    else
                    {
                        main.Add(
                           new XElement(xmlns + "PrintMark", "Y")
                         );
                    }
                    break;
            }
            int invouce_pass;
            if (int.TryParse(InvoicePass, out invouce_pass))
            {
                main.Add(new XElement(xmlns + "RandomNumber", InvoicePass));
            }
            else
            {
                main.Add(new XElement(xmlns + "RandomNumber", new Random(Guid.NewGuid().GetHashCode()).Next(1, 9999).ToString().PadLeft(4, '0')));
            }
            return main;
        }

        private XElement ReturnC0401Details(XNamespace xmlns)
        {
            return new XElement(xmlns + "Details",
                                new XElement(xmlns + "ProductItem",
                                new XElement(xmlns + "Description", InvoiceTax == 0m ? "水果" : (OrderIsPponitem ? EinvoiceFacade.MaxLengthString(OrderItem, 256) : "17Life購物金")), //品名
                                new XElement(xmlns + "Quantity", 1), //數量
                                new XElement(xmlns + "UnitPrice", (int)(IsTriplicate ? Salesamount : OrderAmount)), //單價
                                new XElement(xmlns + "Amount", (int)(IsTriplicate ? Salesamount : OrderAmount)), //金額
                                new XElement(xmlns + "SequenceNumber", 1) //明細項次
                                ));
        }

        private XElement ReturnC0401Amount(XNamespace xmlns)
        {
            return new XElement(xmlns + "Amount",
                                new XElement(xmlns + "SalesAmount", (int)(IsTriplicate ? Salesamount : OrderAmount)), //應稅銷售額合計
                                new XElement(xmlns + "FreeTaxSalesAmount", 0), //免稅銷售額合計
                                new XElement(xmlns + "ZeroTaxSalesAmount", 0), //零稅率銷售額合計
                                new XElement(xmlns + "TaxType", InvoiceTax == 0m ? 3 : 1), //課稅別 1應稅 :3免稅
                                new XElement(xmlns + "TaxRate", InvoiceTax), //稅率 0.05應稅 :免稅
                                new XElement(xmlns + "TaxAmount", (int)(IsTriplicate ? (OrderAmount - Salesamount) : 0)), //營業稅額
                                new XElement(xmlns + "TotalAmount", (int)(OrderAmount)) //總計
                                );
        }

        #endregion

        #region 新版 : C0501作廢發票

        public XDocument ReturnC0501(DateTime cancelTime, string cancelReason = "退貨發票作廢")
        {
            KeyValuePair<XNamespace, string> xml_namespaces = EinvoiceFacade.GetXmlNameSpace(EinvoiceType.C0501);
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XDocument(new XDeclaration("1.0", "utf-8", "true"),
                   new XElement(xml_namespaces.Key + "CancelInvoice",
                   new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                   new XAttribute(xsi + "schemaLocation", xml_namespaces.Value),
                    new XElement(xml_namespaces.Key + "CancelInvoiceNumber", InvoiceNumber), //作廢發票號碼
                    new XElement(xml_namespaces.Key + "InvoiceDate", EinvoiceFacade.ReturnInvoiceTime(InvoiceNumberTime.Value, VerifiedTime.Value).ToString("yyyyMMdd")), //發票日期
                    new XElement(xml_namespaces.Key + "BuyerId", IsTriplicate ? InvoiceComId : "00000000"), //買方統一編號
                    new XElement(xml_namespaces.Key + "SellerId", Com_Id), //賣方統一編號
                    new XElement(xml_namespaces.Key + "CancelDate", cancelTime.ToString("yyyyMMdd")), //作廢時間
                    new XElement(xml_namespaces.Key + "CancelTime", cancelTime.ToString("HH:mm:ss")), //作廢時間
                    new XElement(xml_namespaces.Key + "CancelReason", cancelReason) //作廢原因
                   ));
        }

        #endregion

        #region 新版 : C0701註銷發票
        public XDocument ReturnC0701(DateTime voidTime,string voidReason = "開立錯誤，註銷重開")
        {
            KeyValuePair<XNamespace, string> xml_namespaces = EinvoiceFacade.GetXmlNameSpace(EinvoiceType.C0701);
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XDocument(new XDeclaration("1.0", "utf-8", "true"),
                    new XElement(xml_namespaces.Key + "VoidInvoice",
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                    new XAttribute(xsi + "schemaLocation", xml_namespaces.Value),
                    new XElement(xml_namespaces.Key + "VoidInvoiceNumber", InvoiceNumber),
                    new XElement(xml_namespaces.Key + "InvoiceDate", EinvoiceFacade.ReturnInvoiceTime(InvoiceNumberTime.Value, VerifiedTime.Value).ToString("yyyyMMdd")),
                    new XElement(xml_namespaces.Key + "BuyerId", IsTriplicate ? InvoiceComId : "00000000"),
                    new XElement(xml_namespaces.Key + "SellerId", Com_Id),
                    new XElement(xml_namespaces.Key + "VoidDate", voidTime.ToString("yyyyMMdd")),
                    new XElement(xml_namespaces.Key + "VoidTime", voidTime.ToString("HH:mm:ss")),
                    new XElement(xml_namespaces.Key + "VoidReason", voidReason)
                    )
                );
        }

        #endregion

        #region 新版 : D0401折讓

        public XDocument ReturnD0401(DateTime allowanceTime, decimal detailInvoiceAmount, string allowanceNumber)
        {
            KeyValuePair<XNamespace, string> xml_namespaces = EinvoiceFacade.GetXmlNameSpace(EinvoiceType.D0401);
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XDocument(new XDeclaration("1.0", "utf-8", "true"),
                        new XElement(xml_namespaces.Key + "Allowance",
                        new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                        new XAttribute(xsi + "schemaLocation", xml_namespaces.Value),
                       ReturnD0401Main(allowanceTime, xml_namespaces.Key, allowanceNumber), ReturnD0401Details(detailInvoiceAmount, xml_namespaces.Key), ReturnD0401Amount(detailInvoiceAmount, xml_namespaces.Key))
                   );
        }

        private XElement ReturnD0401Main(DateTime allowanceTime, XNamespace xmlns, string allowanceNumber)
        {
            return new XElement(xmlns + "Main",
                        new XElement(xmlns + "AllowanceNumber", allowanceNumber), //折讓證明單號碼 16碼內 不得重複
                        new XElement(xmlns + "AllowanceDate", allowanceTime.ToString("yyyyMMdd")), //折讓證明單日期
                          new XElement(xmlns + "Seller", new XElement(xmlns + "Identifier", Com_Id), new XElement(xmlns + "Name", Com_Name)), //賣方資訊
                          new XElement(xmlns + "Buyer", //買方資訊
                                 new XElement(xmlns + "Identifier", IsTriplicate ? InvoiceComId : "00000000"), //三聯式填入統編，二聯式填入8個0
                                 new XElement(xmlns + "Name", IsTriplicate ? EinvoiceFacade.MaxLengthString(InvoiceComName, 60) : UserId.ToString())), //三聯式填入公司抬頭，二聯式填入買方姓名
                          new XElement(xmlns + "AllowanceType", "2") //折讓種類 2賣方折讓證明單通知 ； 1買方開立折讓證明單
                        );
        }

        private XElement ReturnD0401Details(decimal detailInvoiceAmount, XNamespace xmlns)
        {
            return new XElement(xmlns + "Details",
                        new XElement(xmlns + "ProductItem",
                            new XElement(xmlns + "OriginalInvoiceDate", EinvoiceFacade.ReturnInvoiceTime(InvoiceNumberTime.Value, VerifiedTime.Value).ToString("yyyyMMdd")), //原發票日期
                            new XElement(xmlns + "OriginalInvoiceNumber", InvoiceNumber), //原發票號碼
                            new XElement(xmlns + "OriginalDescription", InvoiceTax == 0m ? "水果" : (OrderIsPponitem ? EinvoiceFacade.MaxLengthString(OrderItem, 256) : "17Life購物金")), //原品名
                            new XElement(xmlns + "Quantity", 1), //數量
                            new XElement(xmlns + "UnitPrice", (int)(OrderAmount)), //單價
                            new XElement(xmlns + "Amount", (int)Math.Round(InvoiceTax > 0 ? Salesamount : detailInvoiceAmount, 0, MidpointRounding.AwayFromZero)), //金額 未稅的折讓金額
                            new XElement(xmlns + "Tax", InvoiceTax > 0 ? (int)Math.Round((detailInvoiceAmount - Salesamount), 0, MidpointRounding.AwayFromZero) : 0), //營業稅額
                            new XElement(xmlns + "AllowanceSequenceNumber", 1), //折讓證明單明細序號
                            new XElement(xmlns + "TaxType", InvoiceTax == 0m ? 3 : 1) //課稅別
                            ));
        }

        private XElement ReturnD0401Amount(decimal detailInvoiceAmount, XNamespace xmlns)
        {
            return new XElement(xmlns + "Amount",
                            new XElement(xmlns + "TaxAmount", InvoiceTax > 0 ? (int)Math.Round((detailInvoiceAmount - Salesamount), 0, MidpointRounding.AwayFromZero) : 0), //營業稅額合計
                            new XElement(xmlns + "TotalAmount", (int)Math.Round(InvoiceTax > 0 ? Salesamount : detailInvoiceAmount, 0, MidpointRounding.AwayFromZero)) //金額(不含稅之進貨額)
                            );
        }

        #endregion

        /// <summary>
        /// 產生紙本發票
        /// </summary>
        /// <param name="serNo"></param>
        /// <param name="isCopy"></param>
        /// <returns></returns>
        public XElement PaperInvoiceElement(string serNo, bool isCopy)
        {
            bool isTriplicate = InvoiceMode2 == (int)LunchKingSite.Core.InvoiceMode2.Triplicate;
            int index = OrderItem.IndexOf('X') + 1;
            decimal salesamount = Math.Round((OrderAmount / (1 + InvoiceTax)), 0);
            XElement invoice = new XElement("Invoice",
                new XElement("SerNo", serNo),
                new XElement("InvoiceNumber", InvoiceNumber),
                new XElement("InvoiceNumberTime", EinvoiceFacade.ReturnInvoiceTime(InvoiceNumberTime.Value, VerifiedTime.Value).ToString("yyyy/MM/dd HH:mm:ss")),
                                new XElement("InvoiceBuyerName", isTriplicate ? InvoiceComName : string.Empty),
                new XElement("InvoiceComName", isTriplicate ? InvoiceComName : string.Empty),
                new XElement("InvoiceComId", isTriplicate ? InvoiceComId : string.Empty),
                new XElement("IsTriplicate", isTriplicate),
                new XElement("ItemName", (OrderIsPponitem ? OrderItem : string.Format("17Life購物金-{0}",
                    OrderItem.Substring(index, OrderItem.Length - index)))),
                new XElement("InvoiceTax", InvoiceTax),
                new XElement("InvoicePass", InvoicePass),
                new XElement("SaleAmount", isTriplicate ? salesamount.ToString("0.##") : OrderAmount.ToString("0.##")),
                new XElement("TaxAmount", isTriplicate ? (OrderAmount - salesamount).ToString("0.##") : "0"),
                new XElement("Amount", OrderAmount.ToString("0.##")),
                new XElement("OrderId", OrderId),
                new XElement("OrderItem", OrderItem),
                new XElement("Receiver", InvoiceBuyerName),
                new XElement("Address", InvoiceBuyerAddress),
                new XElement("CreditCardTail", CreditCardTail),
                new XElement("IsCopy", isCopy ? "1" : "0"),
                new XElement("IsVendor", IsVendor ? "1" : "0")
                );
            return invoice;
        }
    }
}

using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.EInvoice
{
    public class LoveCodeReply : GovAPIReply
    {
        [JsonProperty("details")]
        public LoveCode[] Details { get; set; }
    }
}
using System;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.EInvoice
{
    public class LoveCode
    {
        [JsonProperty("LoveCode")]
        public string Code { get; set; }
        [JsonProperty("SocialWelfareName")]
        public string FullName { get; set; }
        [JsonProperty("SocialWelfareAbbrev")]
        public string ShortName { get; set; }
        public string Name
        {
            get { return ShortName ?? FullName; }
        }
        public override string ToString()
        {
            return string.Format("{0} {1}", this.Code, this.Name);
        }

        public override int GetHashCode()
        {
            if (Code == null)
            {
                return 0;
            }
            return this.Code.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            LoveCode castObj = obj as LoveCode;
            if (castObj == null)
            {
                return false;
            }
            return GetHashCode() == castObj.GetHashCode();
        }
    }
}
﻿using System;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.EInvoice
{
    public class CodeName
    {
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", Code, Name);
        }
    }
}

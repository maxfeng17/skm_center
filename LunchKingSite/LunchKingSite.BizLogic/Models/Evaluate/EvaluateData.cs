﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.Evaluate
{

    public class EvaluateData
    {
        public List<EvaluateDetailData> DetailData { get; set; }

        public int totalCount { get; set; }

        public int nowPage { get; set; }
        //public EvaluateShowRange ShowRange { get; set; }

        //public EvaluateOrderDesc OrderDesc { get; set; }
    }

    public class EvaluateDetailRating
    {
        public DateTime CreatTime { get; set; }

        public string ItemName { get; set; }
        public string EventName { get; set; }

        public string StoreName { get; set; }

        public DateTime? VerifiedTime { get; set; }

        public int Q1 { get; set; }

        public int Q2 { get; set; }

        public int Q3 { get; set; }

        public int Q4 { get; set; }

        public string CouponSequenceNumber { get; set; }

        public string Comment { get; set; }

    }

    public class EvaluateDetailData
    {
        public string PageNo { get; set; }
        public Guid Bid { get; set; }

        public Guid Sid { get; set; }

        public int UniqueID { get; set; }

        public string EventName { get; set; }

        public bool WithSubStore { get; set; }

        public int DealCount { get; set; }

        public int VerifiedCount { get; set; }

        public DateTime DeliverTimeS { get; set; }

        public DateTime DeliverTimeE { get; set; }

        public string ItemName { get; set; }
        
        public string StoreName { get; set; }

        public int EvaluateCount { get; set; }

        public int CommentCount { get; set; }

        public List<QuestionCount> QuesCount { get; set; }


        public double AvgTotalSatisfactory { get; set; }

        public double AvgComeAgain { get; set; }
        //public List<QuestionDetail> QuesDetail { get; set; }



    }

    public class QuestionDetail
    {
        public Guid TrustID { get; set; }

        public int Qid { get; set; }

        public string QName { get; set; }

        public string QStrValue { get; set; }

        public int QIntValue { get; set; }

        public DateTime CreateTime { get; set; }
    }

    public class QuestionCount
    {
        public int Qid { get; set; }

        public int Counting { get; set; }

        public double AvgStar { get; set; }

        public Dictionary<string, string> RateAndCount { get; set; }
    }

    public class ConditionObj 
    {
        public int Page { get; set; }
        public int Order { get; set; }
        public int Range { get; set; }
    }
}

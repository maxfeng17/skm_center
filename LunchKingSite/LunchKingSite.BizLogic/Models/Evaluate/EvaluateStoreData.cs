﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model.Evaluate
{
    public class EvaluateStoreData
    {
        public List<EvaluateStoreDetail> DetailData { get; set; }
        public int TotalCount { get; set; }
        public int NowPage { get; set; }
        public EvaluateCondition ConditionIndex { get; set; }
        public bool IsResultEmpty { get; set; }

    }

    public class EvaluateStoreDetail
    {
        public Guid? Bid { get; set; }
        public Guid? StoreId { get; set; }
        public int? UniqueID { get; set; }
        public string ItemName { get; set; }
        public int EvaluateCount { get; set; }
        public int DealStatus { get; set; }
        public string SellerId { get; set; }
        public Guid? SellerGuid { get; set; }
        public string SellerName { get; set; }
        public string StoreName { get; set; }
        public DateTime? DeliverTimeS { get; set; }
        public DateTime? DeliverTimeE { get; set; }
        public int StoreCount { get; set; }
        public int VerifiedCount { get; set; }
        public double AvgTotalSatisfactory { get; set; }
        public double AvgComeAgain { get; set; }
        public double AvgEnvironment { get; set; }
        public double AvgService { get; set; }
        public int EnvironmentCount { get; set; }
        public int ServiceCount { get; set; }
        public List<EvaluateDetailRating> Rating { get; set; }
    }

   
}

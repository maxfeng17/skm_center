﻿using LunchKingSite.DataOrm;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Model
{
    public class ProposalMultiDealModel
    {
        public ProposalMultiDealModel()
        {
            Id = 0;
            ItemName = string.Empty;
            ItemNameTravel = string.Empty;
            BrandName = string.Empty;
            OrigPrice = 0;
            ItemPrice = 0;
            CPCBuy = 0;
            CPCPresent = 0;
            GrossMargin = 0;
            Cost = 0;
            SlottingFeeQuantity = 0;
            OrderTotalLimit = 9999;
            AtmMaximum = 20;
            Freights = 0;
            NoFreightLimit = 0;
            Options = string.Empty;
            SellerOptions = string.Empty;
            Sort = 0;
            OrderMaxPersonal = "";
            FreightsId = "";
            QuantityMultiplier = string.Empty;
            Unit = string.Empty;
            Gifts = string.Empty;
            ComboPackCount = string.Empty;
            UniqueId = string.Empty;
            BoxQuantityLimit = string.Empty;
        }

        public int Id { get; set; }
        public string ItemName { get; set; }
        public string ItemNameTravel { get; set; }
        public string BrandName { get; set; }
        public decimal OrigPrice { get; set; }
        public decimal ItemPrice { get; set; }
        public int CPCBuy { get; set; }
        public int CPCPresent { get; set; }
        public decimal GrossMargin { get; set; }
        public decimal Cost { get; set; }
        public int SlottingFeeQuantity { get; set; }
        public decimal OrderTotalLimit { get; set; }
        public int AtmMaximum { get; set; }
        public decimal Freights { get; set; }
        public decimal NoFreightLimit { get; set; }
        public string Options { get; set; }
        public string SellerOptions { get; set; }
        public int Sort { get; set; }
        public string OrderMaxPersonal { get; set; }
        public string FreightsId { get; set; }
        public string QuantityMultiplier { get; set; }
        public string Unit { get; set; }
        public string Gifts { get; set; }
        public string ComboPackCount { get; set; }
        public string UniqueId { get; set; }
        public string BoxQuantityLimit { get; set; }
    }

    public class ProposalMultiDealSort
    {
        public string ItemPrice { get; set; }
        public string Sort { get; set; }
        public int ID { get; set; }
    }

    public class ProposalMarketAnalysis
    {
        public string GomajiPrice { get; set; }
        public string GomajiLink { get; set; }
        public string KuobrothersPrice { get; set; }
        public string KuobrothersLink { get; set; }
        public string CrazymikePrice { get; set; }
        public string CrazymikeLink { get; set; }
        public string EzPricePrice1 { get; set; }
        public string EzPriceLink1 { get; set; }
        public string EzPricePrice2 { get; set; }
        public string EzPriceLink2 { get; set; }
        public string EzPricePrice3 { get; set; }
        public string EzPriceLink3 { get; set; }
        public string PinglePrice1 { get; set; }
        public string PingleLink1 { get; set; }
        public string PinglePrice2 { get; set; }
        public string PingleLink2 { get; set; }
        public string PinglePrice3 { get; set; }
        public string PingleLink3 { get; set; }
        public string SimilarPrice { get; set; }
        public string SimilarLink { get; set; }
        public string OtherShop1 { get; set; }
        public string OtherPrice1 { get; set; }
        public string OtherLink1 { get; set; }
        public string OtherShop2 { get; set; }
        public string OtherPrice2 { get; set; }
        public string OtherLink2 { get; set; }
        public string OtherShop3 { get; set; }
        public string OtherPrice3 { get; set; }
        public string OtherLink3 { get; set; }
    }

    public class ProposalMediaReportLink
    {
        public int Type { get; set; }
        public string Link { get; set; }
    }

    public class Cross
    {
        public string Dept { get; set; }
        public string Team { get; set; }
    }

    public class ProposalEmail
    {
        public string name { get; set; }
        public string email { get; set; }
    }

    public class ProposalRestrictionModel
    {
        public int Id { get; set; }
        public string DeliveryType { get; set; }
        public string DealType { get; set; }
        public string Restriction { get; set; }
        public string Enable { get; set; }
    }

    public class SalesProposalModel
    {
        public Seller SellerProperty { get; set; }
        public ProposalCouponEventContent ContentProperty { get; set; }
    }

    public class ProposalSaleMarketAnalysisDetail
    {
        public int Price { get; set; }
        public string Link { get; set; }
    }
    public class ProposalSaleMarketAnalysis
    {
        public List<ProposalSaleMarketAnalysisDetail> Gomaji { get; set; }
        public List<ProposalSaleMarketAnalysisDetail> Kuobrothers { get; set; }
        public List<ProposalSaleMarketAnalysisDetail> Crazymike { get; set; }
        public List<ProposalSaleMarketAnalysisDetail> EzPrice { get; set; }
        public List<ProposalSaleMarketAnalysisDetail> Other { get; set; }
        public List<ProposalSaleMarketAnalysisDetail> SpecialQC { get; set; }
    }
}

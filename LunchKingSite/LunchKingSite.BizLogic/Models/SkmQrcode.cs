﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace LunchKingSite.BizLogic.Model
{
    /// <summary>
    /// 新光 QRCode 
    /// </summary>
    public class SkmQrcode
    {
        /// <summary>
        /// AES編碼後的憑證資訊
        /// </summary>
        [JsonProperty("trustCode")]
        public string TrustCode { set; get; }
        /// <summary>
        /// 核銷序號
        /// </summary>
        [JsonProperty("couponId")]
        public int CouponId { set; get; }
        /// <summary>
        /// 0:指定商品代碼(自營) 1:限定櫃位使用
        /// </summary>
        [JsonProperty("eventType")]
        public int EventType { set; get; }
        /// <summary>
        /// 商品代碼(eventType = 0才有值, 否則為空值)
        /// </summary>
        [JsonProperty("itemId")]
        public string ItemId { set; get; }
        /// <summary>
        /// 可使用館別櫃位
        /// </summary>
        [JsonProperty("shoppeData")]
        public List<UsingShoppe> ShoppeData { set; get; }
        /// <summary>
        /// 優惠方式 0:折價 1:折數 2:指定金額
        /// </summary>
        [JsonProperty("discountType")]
        public int DiscountType { set; get; }
        /// <summary>
        /// 優惠內容值 ex: 200 or 0.9
        /// </summary>
        [JsonProperty("discountValue")]
        public double DiscountValue { set; get; }
        /// <summary>
        /// 憑證使用日期(起)
        /// </summary>
        [JsonProperty("usingDateS")]
        public string UsingDateS { set; get; }
        /// <summary>
        /// 憑證使用日期(迄)
        /// </summary>
        [JsonProperty("usingDateE")]
        public string UsingDateE { set; get; }
        /// <summary>
        /// 憑證存活期限 (now+10min)
        /// </summary>
        [JsonProperty("expireTime")]
        public string ExpireTime { set; get; }
        /// <summary>
        /// eventName+(string)eventType+(string)discountType+(string)discountValue+(string)usingDateS(月份2碼)+(string)expireTime(秒數2碼)+(string)usingDateE(日2碼)+(string)expireTime(分鐘數2碼) 的字串組合MD5
        /// </summary>
        [JsonProperty("validateCode")]
        public string ValidateCode { set; get; }
    }

    /// <summary>
    /// 可使用櫃位
    /// </summary>
    public class UsingShoppe
    {
        /// <summary>
        /// 館別代碼
        /// </summary>
        [JsonProperty("storeCode")]
        public string StoreCode { set; get; }
        /// <summary>
        /// 使否全櫃位使用
        /// </summary>
        [JsonProperty("isAllShoppe")]
        public bool IsAllShoppe { set; get; }
        /// <summary>
        /// 可使用櫃位
        /// </summary>
        [JsonProperty("shoppe")]
        public string[] Shoppe { set; get; }

        public UsingShoppe()
        {
            Shoppe = new string[] {};
            IsAllShoppe = false;
            StoreCode = string.Empty;
        }
    }

    /// <summary>
    /// 可使用櫃位 Lite
    /// </summary>
    public class UsingShoppeLite
    {
        /// <summary>
        /// 館別代碼
        /// </summary>
        [JsonProperty("o")]
        public string StoreCode { set; get; }
        /// <summary>
        /// 使否全櫃位使用 0:false 1:true
        /// </summary>
        [JsonProperty("p")]
        public int IsAllShoppe { set; get; }
        /// <summary>
        /// 可使用櫃位
        /// </summary>
        [JsonProperty("q")]
        public string[] Shoppe { set; get; }

        public UsingShoppeLite()
        {
            Shoppe = new string[] { };
            IsAllShoppe = 0;
            StoreCode = string.Empty;
        }
    }

    /// <summary>
    /// 新光 QRCode 中 trust code 內容
    /// </summary>
    public class SkmTrustCode
    {
        /// <summary>
        /// 核銷憑證
        /// </summary>
        [JsonProperty("trustId")]
        public Guid TrustId { set; get; }
        /// <summary>
        /// 優惠方式 0:折價 1:折數 2:指定金額
        /// </summary>
        [JsonProperty("discountType")]
        public int DiscountType { set; get; }
        /// <summary>
        /// 優惠內容值 ex: 200 or 0.9
        /// </summary>
        [JsonProperty("discountValue")]
        public double DiscountValue { set; get; }
        /// <summary>
        /// 憑證使用日期(起)
        /// </summary>
        [JsonProperty("usingDateS")]
        public string UsingDateS { set; get; }
        /// <summary>
        /// 憑證使用日期(迄)
        /// </summary>
        [JsonProperty("usingDateE")]
        public string UsingDateE { set; get; }
    }

    /// <summary>
    /// 新光 QrCode BuyGetFree的主要買幾送幾欄位
    /// </summary>
    public class DiscoutSettingLite
    {
        [JsonProperty("w")]
        public int BuyCount { get; set; }
        [JsonProperty("x")]
        public List<string> BuyItemIds { get; set; }
        [JsonProperty("y")]
        public int DiscountCount { get; set; }
        [JsonProperty("z")]
        public List<string> DiscountItemIds { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using Vodka.Mongo;

namespace LunchKingSite.BizLogic.Model
{
    public class Referrer : Entity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public IList<string> AssociatedAccounts { get; set; }
        public IList<Campaign> AdvertisingCampaign { get; set; }
        public string LkGuid { get; set; }

        public Referrer()
        {
            AssociatedAccounts = new List<string>();
            AdvertisingCampaign = new List<Campaign>();
            LkGuid = string.Empty;
        }
    }

    public class Campaign
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int SessionDuration { get; set; }
        public bool OneShotAction { get; set; }
    }

    public class ReferredAction : Entity
    {
        public string ReferrerSourceId { get; set; }
        public ReferrerActionType ActionType { get; set; }
        public string ReferenceId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Department { get; set; }
        public string City { get; set; }
        public string ItemName { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
        public string SubTotal { get; set; }
        public string Remark { get; set; }
        public int DealType { get; set; }
        public int AccBusinessGroupId { get; set; }
        public int BusinessModel { get; set; }
        public bool IsReplicate { get; set; }

        public ReferredAction()
        {
            DealType = -1;
            AccBusinessGroupId = -1;
            BusinessModel = 1;
        }

    }
}

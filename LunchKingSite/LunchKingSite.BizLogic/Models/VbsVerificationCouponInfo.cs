﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model
{
    public class VbsVerificationCouponInfo
    {
        public string CouponId { get; set; }
        public string ProductName { get; set; }
        public string StoreName { get; set; }
        public VbsCouponVerifyState VerifyState { get; set; }
        public DateTime? ReturnDateTime { get; set; }
        public DateTime? VerifiedDateTime { get; set; }
        public string UserName { get; set; }
        public bool? IsBooked { get; set; }
        public bool? IsReservationLock { get; set; }
        

        public class Columns
        {
            public const string CouponId = "CouponId";
            public const string ProductName = "ProductName";
            public const string StoreName = "StoreName";
            public const string VerifyState = "VerifyState";
            public const string ReturnDateTime = "ReturnDateTime";
            public const string VerifiedDateTime = "VerifiedDateTime";
            public const string UserName = "UserName";
            public static List<string> Names { get; set; }
            static Columns()
            {
                Names = new List<string>();
                Names.Add(CouponId);
                Names.Add(ProductName);
                Names.Add(StoreName);
                Names.Add(VerifyState);
                Names.Add(ReturnDateTime);
                Names.Add(VerifiedDateTime);
                Names.Add(UserName);
            }
        }
    }

    public class VerificationCouponInfo
    {
        public string DealName { get; set; }
        public string ItemName { get; set; }
        /// <summary>
        /// ppon: BID; piin: DID
        /// </summary>
        public string DealId { get; set; }
        public BusinessModel DealType { get; set; }
        public string CouponSequence { get; set; }
        public string CouponCode { get; set; }
        public DateTime ExchangePeriodStartTime { get; set; }
        public DateTime ExchangePeriodEndTime { get; set; }
        public VbsCouponVerifyState VerifyState { get; set; }
        public DateTime? ModifyTime { get; set; }
        public string BuyerName { get; set; }
        public Guid TrustId { get; set; }
    }

    public class VbsVerifyCouponInfo
    {
        public string DealName { get; set; }
        public string DealId { get; set; }
        public BusinessModel DealType { get; set; }
        public string CouponId { get; set; }
        public string CouponCode { get; set; }
        public DateTime ExchangePeriodStartTime { get; set; }
        public DateTime ExchangePeriodEndTime { get; set; }
        public VbsCouponVerifyState VerifyState { get; set; }
        public DateTime? ModifyTime { get; set; }
        public string PayerName { get; set; }
        public string TrustId { get; set; }
        public bool IsCouponAvailable { get; set; }
        public bool IsDuInTime { get; set; }
        public string Message { get; set; }
    }

    public class VbsVerificationCoupon : VbsVerificationCouponInfo
    {
        public string DealName { get; set; }
        public bool IsNew { get; set; }
        public int VerifyCount { get; set; }
        public string VerifyAccount { get; set; }
        public string VerifiedStore { get; set; }
    }

    public class VbsVerifyResult
    {
        public bool IsVerifySuccess { get; set; }
        public string Message { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// WebApi回覆前端呼叫時，乘載回傳資訊的類別
    /// </summary>
    public class ApiResult : ApiResult<object> {
    }

    /// <summary>
    /// WebApi回覆前端呼叫時，乘載回傳資訊的類別
    /// </summary>
    public class ApiResult<T> where T : new()
    {
        private string _message;
        /// <summary>
        /// 回傳的結果
        /// </summary>
        public ApiResultCode Code { get; set; }
        /// <summary>
        /// 回傳資料主體
        /// </summary>
        public T Data { get; set; }
        /// <summary>
        /// 回傳的訊息
        /// </summary>
        public string Message {
            get
            {
                return string.IsNullOrEmpty(_message) ?  Helper.GetLocalizedEnum(Code) : _message;
            }
            set
            {
                _message = value;
            }
        }

        public ApiResult()
        {
            Code = ApiResultCode.Ready;
            Data = default(T);
            Message = string.Empty;
        }
    }

    /// <summary>
    /// 需進行版本控管的的API回覆資料用物件，包含 
    /// 供前端資料識別用的 DataTitle
    /// 供判斷版本號碼的 Version
    /// </summary>
    public class ApiVersionResult : ApiResult
    {
        /// <summary>
        /// 版本資訊
        /// </summary>
        public ApiVersionItem Version { get; set; }

        public ApiVersionResult()
        {
            Code = ApiResultCode.Success;
            Data = null;
            Message = string.Empty;
        }
    }
}

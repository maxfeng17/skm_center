﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiComboDealMain
    {
        public ApiComboDealMain()
        {
            ComboDeals = new List<ApiComboDeal>();
        }

        /// <summary>
        /// 代表檔的BID
        /// </summary>
        public Guid MainDealGuid { get; set; }
        /// <summary>
        /// 調整顯示數量與單位
        /// </summary>
        public bool IsAdjusted { get; set; }
        /// <summary>
        /// 已銷售數量顯示值-已銷售數量*設定的倍數 所得到用於顯示的的數值
        /// </summary>
        public int SoldNumShow { get; set; }
        /// <summary>
        /// 銷售數量顯示的單位
        /// </summary>
        public string ShowUnit { get; set; }
        /// <summary>
        /// 子檔資料
        /// </summary>
        public List<ApiComboDeal> ComboDeals { get; set; }
        /// <summary>
        /// 成套禮券
        /// </summary>
        public bool IsGroupCoupon { set; get; }
    }
}

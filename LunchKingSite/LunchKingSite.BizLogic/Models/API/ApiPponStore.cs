﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiPponStore
    {
        /// <summary>
        /// 分店編號
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// 分店名稱
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 電話
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 營業時間
        /// </summary>
        public string OpenTime { get; set; }

        /// <summary>
        /// 公休日
        /// </summary>
        public string CloseDate { get; set; }

        /// <summary>
        /// 備註
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 交通資訊-MRT
        /// </summary>
        public string Mrt { get; set; }

        /// <summary>
        /// 交通資訊-開車
        /// </summary>
        public string Car { get; set; }

        /// <summary>
        /// 交通資訊-公車
        /// </summary>
        public string Bus { get; set; }

        /// <summary>
        /// 交通資訊-其他
        /// </summary>
        public string OtherVehicles { get; set; }

        /// <summary>
        /// 官網
        /// </summary>
        public string WebUrl { get; set; }

        /// <summary>
        /// 官方FB
        /// </summary>
        public string FbUrl { get; set; }

        /// <summary>
        /// 官方噗浪
        /// </summary>
        public string PlurkUrl { get; set; }

        /// <summary>
        /// 官方部落格
        /// </summary>
        public string BlogUrl { get; set; }

        /// <summary>
        /// 其他網路網路資訊
        /// </summary>
        public string OtherUrl { get; set; }

        /// <summary>
        /// 能使用CreditCard
        /// </summary>
        public bool CreditcardAvailable { get; set; }

        /// <summary>
        /// 緯度座標
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 經度座標
        /// </summary>
        public double Longitude { get; set; }


        public ApiPponStore()
        {
            StoreName = string.Empty;
            Phone = string.Empty;
            Address = string.Empty;
            OpenTime = string.Empty;
            CloseDate = string.Empty;
            Remarks = string.Empty;
            Mrt = string.Empty;
            Car = string.Empty;
            Bus = string.Empty;
            OtherVehicles = string.Empty;
            WebUrl = string.Empty;
            FbUrl = string.Empty;
            PlurkUrl = string.Empty;
            BlogUrl = string.Empty;
            OtherUrl = string.Empty;
            CreditcardAvailable = false;
        }
    }
}

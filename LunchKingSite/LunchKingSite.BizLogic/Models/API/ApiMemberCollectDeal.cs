﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Models.Skm;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiMemberCollectDeal
    {

        public ApiMemberCollectDeal()
        {
            //預設不顯示兌換價
            ShowExchangePrice = false;
            ExchangePrice = 0;
            IsBurningEvent = false;
            BurningPoint = 0;
        }

        /// <summary>
        /// 檔次代號
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 手機版頁面當作檔次名稱顯示。
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 畫面顯示的圖片路徑，回傳的圖片大小因應參數回傳適當大小。
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// 優惠類型
        /// </summary>
        public int DiscountType { get; set; }
        /// <summary>
        /// 優惠價格/折抵現金/折數
        /// </summary>
        public int Discount { get; set; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal OriPrice { get; set; }
        
        /// <summary>
        /// 結束販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealEndTime { get; set; }
        /// <summary>
        /// 是否有設定提醒
        /// </summary>
        public bool AppNotice { get; set; }
        /// <summary>
        /// 是否顯示兌換價
        /// </summary>
        public bool ShowExchangePrice { get; set; }
        /// <summary>
        /// 兌換價
        /// </summary>
        public decimal ExchangePrice { get; set; }

        #region 新光App專用

        /// <summary>
        /// 正方型主檔圖片
        /// </summary>
        public string AppPicUrl { get; set; }
        /// <summary>
        /// 體驗商品收藏次數
        /// </summary>
        public string ExperienceDealCollectCount { get; set; }
        /// <summary>
        /// 已兌換人數
        /// </summary>
        public string ExchangeDealCount { get; set; }
        /// <summary>
        /// 是否為體驗商品
        /// </summary>
        public bool? IsExperience { get; set; }
        /// <summary>
        /// 新光可用櫃位
        /// </summary>
        public List<SkmPponStore> SkmAvailabilities { get; set; }
        /// <summary>
        /// 是否為清算檔次
        /// </summary>
        public bool IsSettlementDeal { get; set; }
        /// <summary>
        /// 下次清算時間
        /// </summary>
        public string NextSettlementTime { get; set; }
        /// <summary>
        /// 是否燒點活動
        /// </summary>
        public bool IsBurningEvent { get; set; }
        /// <summary>
        /// 需燒點數
        /// </summary>
        public int BurningPoint { get; set; }
        /// <summary>
        /// 檔次Icon
        /// </summary>
        public SkmDealIcon IconInfo { get; set; }
        /// <summary>
        /// 購滿符合優惠條件的商品:買[幾]送幾
        /// </summary>
        public int? Buy { get; set; }
        /// <summary>
        /// 符合優惠條件的贈品:買幾送[幾]
        /// </summary>
        public int? Free { get; set; }
        /// <summary>
        /// 是否Skm Pay
        /// </summary>
        public bool IsSkmPay { get; set; }

        #endregion

        /// <summary>
        /// 顯示金額
        /// </summary>
        public decimal DisplayPrice { get; set; }
        /// <summary>
        /// 折數顯示用文字"5折"等，特殊檔次可能顯示”特惠”等
        /// </summary>
        public string DiscountString { get; set; }
        /// <summary>
        /// 優惠顯示方式
        /// </summary>
        public int DiscountDisplayType { get; set; }
        /// <summary>
        /// 開始販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealStartTime { get; set; }
        /// <summary>
        /// 是否售完
        /// </summary>
        public bool SoldOut { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiCitySellerSampleCategory
    {
        /// <summary>
        /// 城市編號
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// 商家類型
        /// </summary>
        public List<ApiSellerSampleCategory> SellerCategoryList { get; set; }

        public ApiCitySellerSampleCategory()
        {
            SellerCategoryList = new List<ApiSellerSampleCategory>();
        }
    }
}
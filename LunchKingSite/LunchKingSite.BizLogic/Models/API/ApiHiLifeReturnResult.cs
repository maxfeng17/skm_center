﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// WebApi回覆前端呼叫時，乘載回傳資訊的類別
    /// </summary>
    public class ApiHiLifeReturnResult : ApiHiLifeReturnResult<object> {
    }

    /// <summary>
    /// WebApi回覆前端呼叫時，乘載回傳資訊的類別
    /// </summary>
    public class ApiHiLifeReturnResult<T> where T : new()
    {
        private string _message;
        /// <summary>
        /// 回傳的結果
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// 回傳資料主體
        /// </summary>
        public List<ReturnDetail> PingCodeList { get; set; }
        /// <summary>
        /// 回傳的訊息
        /// </summary>
        public string Message { get; set; }

        public ApiHiLifeReturnResult()
        {
            State = string.Empty;
            PingCodeList = new List<ReturnDetail>();
            Message = string.Empty;
        }
    }

}

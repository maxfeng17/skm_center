﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;


namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// [v2.0] [v3.0] 訂單明細物件
    /// </summary>
    public class ApiUserOrder
    {
        public ApiUserOrder()
        {
            ShowCoupon = true;
            Invoices = new List<ApiInvoiceData>();
            VerifyType = (int)VerifyActionType.General;
            LastShippingDate = string.Empty;
            ActualShippingDate = string.Empty;
        }

        /// <summary>
        /// 訂單GUID
        /// </summary>
        public Guid OrderGuid { get; set; }

        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 檔次BID
        /// </summary>
        public Guid BusinessHourGuid { get; set; }

        /// <summary>
        /// 母檔Bid
        /// </summary>
        public Guid MainBid { get; set; }

        /// <summary>
        /// 使用期限-起 字串
        /// </summary>
        public string UseStartDate { get; set; }

        /// <summary>
        /// 使用期限-迄 字串
        /// </summary>
        public string UseEndDate { get; set; }

        /// <summary>
        /// 購買時間
        /// </summary>
        public string BuyDate { get; set; }

        /// <summary>
        /// 商品列表
        /// </summary>
        public List<ApiUserOrderDetail> OrderDetailList { get; set; }

        /// <summary>
        /// 運費
        /// </summary>
        public decimal Freight { get; set; }

        /// <summary>
        /// 訂單總價
        /// </summary>
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// PCash付款金額
        /// </summary>
        public decimal PCashPay { get; set; }

        /// <summary>
        /// 17life購物金付款金額
        /// </summary>
        public decimal SCashPay { get; set; }

        /// <summary>
        /// 由Payeasy購物金付款金額
        /// </summary>
        public decimal PSCashPay { get; set; }

        /// <summary>
        /// 紅利付款金額
        /// </summary>
        public decimal BCashPay { get; set; }

        /// <summary>
        /// 信用卡付款
        /// </summary>
        public decimal CreditCardPay { get; set; }

        /// <summary>
        /// ATM付款金額
        /// </summary>
        public decimal ATMPay { get; set; }

        /// <summary>
        /// 全家取貨付款金額
        /// </summary>
        public decimal FamilyIspAmount { get; set; }

        /// <summary>
        /// 7-11取貨付款金額
        /// </summary>
        public decimal SevenIspAmount { get; set; }

        /// <summary>
        /// 現金卷付款金額
        /// </summary>
        public decimal DiscountCodePay { get; set; }

        /// <summary>
        /// 付款狀態
        /// </summary>
        public List<ApiPayInfo> PayInfoList { get; set; }

        /// <summary>
        /// 退貨狀態
        /// </summary>
        public List<ApiRefundLog> RefundLogList { get; set; }

        /// <summary>
        /// (好康哪裡找)
        /// </summary>
        public List<ApiPponStore> Availabilities { get; set; }

        /// <summary>
        /// 尚有憑證數
        /// </summary>
        public int PponCouponCount { get; set; }

        /// <summary>
        /// 可用憑證數
        /// </summary>
        public int CanUseCouponCount { get; set; }

        /// <summary>
        /// 取貨方式
        /// </summary>
        public DeliveryType PponDeliveryType { get; set; }

        /// <summary>
        /// 停用簡訊功能
        /// </summary>
        public bool DisableSMS { get; set; }

        /// <summary>
        /// 憑證列表
        /// </summary>
        public List<ApiCouponData> CouponList { get; set; }

        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string AddresseeName { get; set; }

        /// <summary>
        /// 收件人連絡電話
        /// </summary>
        public string AddresseePhone { get; set; }

        /// <summary>
        /// 收件地址
        /// </summary>
        public string AddresseeAddress { get; set; }

        /// <summary>
        /// 是否顯示憑證編號
        /// </summary>
        public bool ShowCoupon { get; set; }

        /// <summary>
        /// 是否為0元優惠活動
        /// </summary>
        public bool IsFreeDeal { get; set; }

        /// <summary>
        /// 是否為全家活動
        /// </summary>
        public bool IsFami { get; set; }

        /// <summary>
        /// 頻道類型
        /// </summary>
        public int BehaviorType { set; get; }

        /// <summary>
        /// 是否為紙本下載：後臺設定項目為「0元或廠商提供序號檔次，提供下載序號」
        /// 對應OrderConfig.PEZeventCouponDownload
        /// </summary>
        public bool IsPaperDownload { get; set; }

        /// <summary>
        /// 訂單已完成(表示已付款)
        /// </summary>
        public bool IsCompleted { get; set; }

        /// <summary>
        /// ATM相關資訊
        /// </summary>
        public ApiOrderAtmData AtmData { get; set; }

        /// <summary>
        /// 已發送簡訊的最小數量
        /// </summary>
        public int SMSCount { get; set; }

        /// <summary>
        /// 憑證類型，包括17life憑證、廠商提供序號、全家pin code、全家bar code等
        /// </summary>
        public DealCouponType CouponType { get; set; }

        /// <summary>
        /// 依據憑證使用與付款產生的訂單狀態
        /// </summary>
        public ApiOrderStatus OrderStatus { get; set; }

        /// <summary>
        /// 發票資訊
        /// </summary>
        public List<ApiInvoiceData> Invoices { get; set; }

        /// <summary>
        /// 成套商品券單價
        /// </summary>
        public int GroupProductUnitPrice { get; set; }

        /// <summary>
        /// 總coupon張數
        /// </summary>
        public int TotalCouponCount { set; get; }

        /// <summary>
        /// 優惠券名稱
        /// </summary>
        public string CouponName { set; get; }

        /// <summary>
        /// 新光檔次
        /// </summary>
        public SkmPponDeal SkmPponDeal { set; get; }

        /// <summary>
        /// 優惠券顯示方式(目前App用)
        /// </summary>
        public GroupCouponType GroupCouponType { set; get; }

        /// <summary>
        /// 檔次圖示(目前App用)
        /// </summary>
        public string MainImagePath { set; get; }

        /// <summary>
        /// 成套商品App頻道
        /// </summary>
        public int GroupCouponAppChannel { set; get; }

        /// <summary>
        /// 訂單狀態說明文字
        /// </summary>
        public string OrderTypeDesc { set; get; }

        /// <summary>
        /// 可填評價數
        /// </summary>
        public int EvaluateCount { set; get; }

        /// <summary>
        /// 成套票券類型
        /// </summary>
        public int GroupCouponDealType { get; set; }

        /// <summary>
        /// 是否可使用換貨申請功能
        /// </summary>
        public bool ExchangeEnable { get; set; }

        /// <summary>
        /// 換貨明細
        /// </summary>
        public List<ApiExchangeLog> ExchangeLogList { get; set; }

        /// <summary>
        /// 核銷方式
        /// </summary>
        public int VerifyType { get; set; }

        /// <summary>
        /// 預計出貨日
        /// </summary>
        public string LastShippingDate { get; set; }

        /// <summary>
        /// 實際出貨日
        /// </summary>
        public string ActualShippingDate { get; set; }

        /// <summary>
        /// 出貨資訊
        /// </summary>
        public ShipInfo ShipInfo { get; set; }
        /// <summary>
        /// 出貨資訊(多筆出貨資訊)
        /// </summary>
        public List<ShipInfo> ShipInfoList { get; set; }
        /// <summary>
        /// 客服回覆訊息數 (大於0列表前方顯示紅點)
        /// </summary>
        public int CsMessageCount { get; set; }
        /// <summary>
        /// 17life 行動支付未申辦行銷文案
        /// </summary>
        public string UnUsed17PayMsg { get; set; }
        /// <summary>
        /// 17life 行動支付已申辦行銷文案
        /// </summary>
        public string Used17PayMsg { get; set; }
        /// <summary>
        /// 是否為24小時到貨檔次訂單
        /// </summary>
        public bool Is24H { get; set; }

        public ApiLinePointsData LinePoint { get; set; }
    }

    /// <summary>
    /// 宅配商品配送狀態
    /// </summary>
    public enum ShippingStatus
    {
        None = 0,
        /// <summary>
        /// 準備出貨
        /// </summary>
        [Description("準備出貨")]
        ReadyToShip = 1,
        /// <summary>
        /// 已出貨
        /// </summary>
        [Description("配送")]
        Shipped = 2,
        /// <summary>
        /// 商品抵達門市
        /// </summary>
        [Description("抵達門市")]
        StoreReceived = 3,
        /// <summary>
        /// 買家取件成功
        /// </summary>
        [Description("取件成功")]
        CustomerReceived = 4,
        /// <summary>
        /// 逾期未取件
        /// </summary>
        [Description("逾期未取件")]
        CustomerOverdueNonePickup = 5
    }

    public class ApiUserOrderDetail
    {
        /// <summary>
        /// 商品明細的GUID
        /// </summary>
        public Guid OrderDetailGuid { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// 好康定價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 數量
        /// </summary>
        public int Quantity { get; set; }
    }

    public class ApiCouponData
    {
        public ApiCouponData()
        {
            ThreeStageCode = new ThreeStageBarcode();
        }
        public int CouponId { get; set; }
        public string ItemName { get; set; }
        public string SequenceNumber { get; set; }
        public string CouponCode { get; set; }
        public ApiCouponUsedType CouponUsedType { get; set; }

        /// <summary>
        /// 憑證使用狀態說明
        /// </summary>
        public string CouponUsedTypeDesc { get; set; }

        /// <summary>
        /// 銀行信託狀控說明
        /// </summary>
        public string BankStatusDesc { get; set; }

        /// <summary>
        /// 分店序號
        /// </summary>
        public string StoreSeq { get; set; }

        /// <summary>
        /// 三段式條碼
        /// </summary>
        public ThreeStageBarcode ThreeStageCode { get; set; }

        /// <summary>
        /// 全家寄杯條碼資訊
        /// </summary>
        public FamilyNetPincode FamilyNetPincode { get; set; }

        /// <summary>
        /// 萊爾富寄杯資訊
        /// </summary>
        public HiLifePincode HiLifePincode { get; set; }

        /// <summary>
        /// 核銷時間
        /// </summary>
        public string UsageVerifiedTime { set; get; }

        /// <summary>
        /// 成套商品的贈品
        /// </summary>
        public bool IsGiveaway { set; get; }

        /// <summary>
        /// 產品代碼
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// 特殊貨號
        /// </summary>
        public string SpecialItemNo { get; set; }

        /// <summary>
        /// 是否可評價
        /// </summary>
        public bool IsToEvaluate { get; set; }

        /// <summary>
        /// 分割後的兌換序號
        /// </summary>
        public string CouponSeparateCode { get; set; }

        /// <summary>
        /// 禮物狀態  0=已送出新禮物（未接收或拒絕），1=已接受禮物 2=已退回禮物 3=取消送禮 4=不是禮物 5=已過期 6=快速領取(效果等同於已接受禮物)
        /// </summary> 
        public int AcceptGiftType { get; set; }
    }

    /// <summary>
    /// 全家寄杯條碼資訊
    /// </summary>
    public class FamilyNetPincode
    {
        /// <summary>
        /// Pincode
        /// </summary>
        public string Pincode { get; set; }
        /// <summary>
        /// 四段條碼
        /// </summary>
        public List<string> Barcode { get; set; }

        /// <summary>
        /// 使用門市-店名
        /// </summary>
        public string UsageStoreName { get; set; }

        /// <summary>
        /// 使用門市-地址
        /// </summary>
        public string UsageStoreAddress { get; set; }

        public FamilyNetPincode()
        {
            Pincode = string.Empty;
            Barcode = new List<string>();
            UsageStoreName = string.Empty;
            UsageStoreAddress = string.Empty;
        }
    }

    public class HiLifePincode
    {
        /// <summary>
        /// 萊爾富兩段式條碼
        /// </summary>
        public List<string> PincodeList { get; set; }

        /// <summary>
        /// 使用門市-店名
        /// </summary>
        public string UsageStoreName { get; set; }

        /// <summary>
        /// 使用門市-地址
        /// </summary>
        public string UsageStoreAddress { get; set; }

        public HiLifePincode()
        {
            PincodeList = new List<string>();
            UsageStoreName = string.Empty;
            UsageStoreAddress = string.Empty;
        }
    }

    public enum ApiCouponUsedType
    {
        /// <summary>
        /// 未達門檻、販賣中
        /// </summary>
        WaitToPass = 0,
        /// <summary>
        /// 未使用
        /// </summary>
        CanUsed,
        /// <summary>
        /// 不能使用-檔次未成立
        /// </summary>
        FailDeal,
        /// <summary>
        /// 退貨
        /// </summary>
        CouponRefund,
        /// <summary>
        /// 憑證過期
        /// </summary>
        CouponExpired,
        /// <summary>
        /// ATM退款中
        /// </summary>
        ATMRefund,
        /// <summary>
        /// 已使用
        /// </summary>
        Used,
        /// <summary>
        /// 退貨中
        /// </summary>
        WaitForRefund,
        /// <summary>
        /// 特殊活動-不能使用
        /// </summary>
        SpecialCoupon,
        /// <summary>
        /// 全家憑證鎖定中
        /// </summary>
        FamiLocked,
    }

    public class ApiPayInfo
    {
        public string PayType { get; set; }
        public decimal PayAmount { get; set; }
        public string GroupName { get; set; }
        public ApiPayInfo(string paytype, decimal payamount, string groupName = "")
        {
            PayType = paytype;
            PayAmount = payamount;
            GroupName = groupName;
        }
    }

    public class ApiRefundLog
    {
        /// <summary>
        /// 申請退貨時間
        /// </summary>
        public string RequestTime { get; set; }
        /// <summary>
        /// 憑證編號
        /// </summary>
        public string Sequence { get; set; }
        /// <summary>
        /// 申請退貨數
        /// </summary>
        public string RequestSpec { get; set; }
        /// <summary>
        /// 實際退貨數
        /// </summary>
        public string ReturnedSpec { get; set; }
        /// <summary>
        /// 退回PCash
        /// </summary>
        public int Pcash { get; set; }
        /// <summary>
        /// 退回紅利
        /// </summary>
        public int Bcash { get; set; }
        /// <summary>
        /// 退回Scash
        /// </summary>
        public int Scash { get; set; }
        /// <summary>
        /// 退回PScash
        /// </summary>
        public int PScash { get; set; }
        /// <summary>
        /// 刷退
        /// </summary>
        public int CreditCardAmount { get; set; }
        /// <summary>
        /// ATM
        /// </summary>
        public int Atm { get; set; }
        /// <summary>
        /// LinePay
        /// </summary>
        public int LinePay { get; set; }
        /// <summary>
        /// TaishinPay
        /// </summary>
        public int TaishinPay { get; set; }
        /// <summary>
        /// 退貨進度
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 是否顯示金額
        /// </summary>
        public bool ShowCash { get; set; }
        public List<ApiPayInfo> RefundInfoList { get; set; }

        public ApiRefundLog(string requesttime, string sequence, int pcash, int bcash, int scash, int ccash, int atm, string message, bool showcash)
        {
            RequestTime = requesttime;
            Sequence = sequence;
            Pcash = pcash;
            Bcash = bcash;
            Scash = scash;
            CreditCardAmount = ccash;
            Atm = atm;
            Message = message;
            ShowCash = showcash;
        }

        public ApiRefundLog(string requestTime, string requestSpec, string returnedSpec, int pcash, int bcash, int scash, int pscash, int ccash, int atm, int linepay, int taishinpay, string message, bool showcash, List<ApiPayInfo> refundInfoList)
        {
            RequestTime = requestTime;
            Sequence = requestSpec;
            RequestSpec = requestSpec;
            ReturnedSpec = returnedSpec;
            Pcash = pcash;
            Bcash = bcash;
            Scash = scash;
            PScash = pscash;
            CreditCardAmount = ccash;
            Atm = atm;
            LinePay = linepay;
            TaishinPay = taishinpay;
            Message = message;
            ShowCash = showcash;
            RefundInfoList = refundInfoList;
        }
    }

    public class ApiInvoiceData
    {
        /// <summary>
        /// 發票開立時間
        /// </summary>
        public string InvoiceTime { get; set; }
        /// <summary>
        /// 發票編號
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// 發票開立金額
        /// </summary>
        public decimal Amount { get; set; }

    }

    public class ApiPponOrderListItem
    {
        public Guid OrderGuid { set; get; }
        public NewCouponListFilterType CountType { set; get; }
        public string CouponName { set; get; }
        public string UseStartDate { set; get; }
        public string UseEndDate { set; get; }
        public string BuyDate { set; get; }
        public int CouponCount { set; get; }
        public string MainImagePath { set; get; }
        public bool DisableSMS { set; get; }
        public string OrderTypeDesc { set; get; }
        public DealCouponType CouponType { set; get; }
        public int? TotalCouponCount { set; get; }
        public int? UnitPrice { set; get; }
        public int GroupCouponAppChannel { set; get; }
    }

    public class SkmPponDeal
    {
        /// <summary>
        /// 母檔Bid
        /// </summary>
        public Guid MainBid { get; set; }
        /// <summary>
        /// 檔次Bid
        /// </summary>
        public Guid BusinessHourGuid { get; set; }
        /// <summary>
        /// Api檔次名稱
        /// </summary>
        public string ApiDealName { get; set; }
        /// <summary>
        /// 主檔圖片
        /// </summary>
        public string PicUrl { get; set; }
        /// <summary>
        /// 正方型主檔圖片
        /// </summary>
        public string AppPicUrl { get; set; }
        /// <summary>
        /// 體驗商品收藏次數
        /// </summary>
        public string ExperienceDealCollectCount { get; set; }
        /// <summary>
        /// 已兌換人數
        /// </summary>
        public string ExchangeDealCount { get; set; }
        /// <summary>
        /// 新光可用櫃位
        /// </summary>
        public List<SkmPponStore> SkmAvailabilities { get; set; }
        /// <summary>
        /// 優惠類型
        /// </summary>
        public int DiscountType { get; set; }
        /// <summary>
        /// 優惠價格/折抵現金/折數
        /// </summary>
        public int Discount { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal ItemOrigPrice { get; set; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal ItemPrice { get; set; }
        /// <summary>
        /// 新光兌換價 
        /// </summary>
        public int? ItemDefaultDailyAmount { get; set; }
        /// <summary>
        /// 是否為體驗商品
        /// </summary>
        public bool? IsExperience { get; set; }
        /// <summary>
        /// 是否為清算檔次
        /// </summary>
        public bool IsSettlementDeal { get; set; }
        /// <summary>
        /// 下次清算時間
        /// </summary>
        public string NextSettlementTime { get; set; }
        /// <summary>
        /// 檔次收藏數量
        /// </summary>
        public int DealMemberCollectCount { get; set; }
    }

    public class SkmPponStore
    {
        private readonly ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        
        public Guid Bid { get; set; }
        /// <summary>
        /// 分店
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 分店名稱
        /// </summary>
        public string SellerName { get; set; }
        /// <summary>
        /// 館別
        /// </summary>
        public string ShopCode { get; set; }
        public string ShopName { get; set; }
        /// <summary>
        /// 櫃位
        /// </summary>
        public string BrandCounterCode { get; set; }
        public string BrandCounterName { get; set; }
        /// <summary>
        /// 樓層
        /// </summary>
        public string Floor { get; set; }
        /// <summary>
        /// for app使用名稱
        /// </summary>
        public string StoreDisplayName { get; set; }
        /// <summary>
        /// 館櫃同步過來的StoreGuid
        /// </summary>
        public Guid StoreGuid { get; set;}
        public Guid BrandGuid { get; set;}
        /// <summary>
        /// 導航連結
        /// </summary>
        public string NavigationLink
        {
            get
            {
                return string.Format("skmapp://www.skm.com/indoornavi?s={0}&f={1}&b={2}",StoreGuid,Floor,BrandGuid);
            }
        }
        /// <summary>
        /// 是否顯示導航資訊
        /// </summary>
        public bool EnableShowNavigation
        {
            get
            {
                var shopcodes = config.NavigationShops.Split(',');
                return StoreGuid != Guid.Empty && BrandGuid != Guid.Empty && !string.IsNullOrEmpty(Floor) && shopcodes.Contains(ShopCode);
            }
        }
    }

    public enum GroupCouponType
    {
        /// <summary>
        /// 非商品券
        /// </summary>
        None = 0,
        /// <summary>
        /// 一般顯示方式
        /// </summary>
        Normal = 1
    }

    public class ApiExchangeLog
    {
        /// <summary>
        /// 換貨申請時間
        /// </summary>
        public string RequestTime { get; set; }
        /// <summary>
        /// 換貨申請原因(換貨內容)
        /// </summary>
        public string ExchangeSpec { get; set; }
        /// <summary>
        /// 換貨進度
        /// </summary>
        public string Message { get; set; }

        public ApiExchangeLog(string requesttime, string exchangeSpec, string message)
        {
            RequestTime = requesttime;
            ExchangeSpec = exchangeSpec;
            Message = message;
        }
    }

    public class  ApiLinePointsData
    {
        public bool IsShow { get; set; }
        /// <summary>
        /// 返饋 LINE Points 的細節
        /// </summary>
        public string Link
        {
            get
            {
                return "https://www.17life.com/join/20190610";
            }
        }
    }
}

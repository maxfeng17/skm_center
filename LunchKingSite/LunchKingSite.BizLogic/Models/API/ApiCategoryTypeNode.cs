﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiCategoryTypeNode
    {
        public ApiCategoryTypeNode(CategoryType nodeType)
        {
            _nodeType = nodeType;
            CategoryNodes = new List<ApiCategoryNode>();
        }

        private readonly CategoryType _nodeType;
        /// <summary>
        /// 分類類別
        /// </summary>
        public CategoryType NodeType
        {
            get { return _nodeType; }
        }
        /// <summary>
        /// 此分類類別之中擁有的分類物件與物件的子類別項目資料
        /// </summary>
        public List<ApiCategoryNode> CategoryNodes { get; set; }

        public ApiCategoryTypeNode Copy()
        {
            ApiCategoryTypeNode rtn = new ApiCategoryTypeNode(_nodeType);
            foreach (var categoryNode in CategoryNodes)
            {
                rtn.CategoryNodes.Add(categoryNode.Copy());
            }
            return rtn;
        }

        /// <summary>
        /// 傳入某個category，檢查此CategoryTypeNode，回傳找到此Id資料經過的每個CategoryId的陣列
        /// 如有多個符合條件的資料，只會回傳第一個符合的
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public List<int> GetCategoryPath(int categoryId)
        {
            foreach (var categoryNode in CategoryNodes)
            {
                List<int> paths = categoryNode.GetCategoryPath(categoryId);
                if (paths.Count > 0)
                {
                    return paths;
                }
            }
            return new List<int>();
        }



        #region public static
        public static ApiCategoryTypeNode CreateApiCategoryTypeNode(CategoryTypeNode node, bool inFrontEnd)
        {
            ApiCategoryTypeNode rtn = new ApiCategoryTypeNode(node.NodeType);
            List<CategoryNode> categoryNodes = node.CategoryNodes;
            if (inFrontEnd)
            {
                categoryNodes = node.CategoryNodes.Where(x => x.IsShowFrontEnd).ToList();
            }

            foreach (var subNodes in categoryNodes)
            {
                rtn.CategoryNodes.Add(ApiCategoryNode.CreateApiCategoryNode(subNodes , inFrontEnd));
            }
            return rtn;
        }
        #endregion 
    }
}

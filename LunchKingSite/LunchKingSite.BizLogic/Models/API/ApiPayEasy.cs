﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// 加密扣款內容(AES Key2)
    /// </summary>
    public class DeductContent
    {
        /// <summary>
        /// 實際扣點
        /// </summary>
        public int DeductPoint;

        /// <summary>
        /// PEZ 交易碼
        /// </summary>
        public string PezAuthCode;
    }

    /// <summary>
    /// 店家交易碼及轉換上限(AES Key2)
    /// </summary>
    public class MerchantContent
    {
        /// <summary>
        /// 店家交易碼
        /// </summary>
        public Guid MerchantDealCode { get; set; }
        /// <summary>
        /// 轉換上線
        /// </summary>
        public string MaxValue { get; set; }
    }


    public class CancelDeductPoint
    {
        public CancelMerchant Merchant { get; set; }
        public CancelDealResult DealResult { get; set; }
    }

    public class CancelMerchant
    {
        /// <summary>
        /// 店家ID
        /// </summary>
        public string MerchantId { get; set; }
        /// <summary>
        /// 店家交易碼
        /// </summary>
        public string MerchantDealCode { get; set; }
    }

    public class CancelDealResult
    {
        /// <summary>
        /// 加密的顧客流水號
        /// </summary>
        public string MemNum { get; set; }
        /// <summary>
        /// 處理狀態
        /// </summary>
        public string ReturnCode { get; set; }
        /// <summary>
        /// 加密的實際扣點
        /// </summary>
        public string CancelPoint { get; set; }
        /// <summary>
        /// 加密的PEZ交易碼
        /// </summary>
        public string PezAuthCode { get; set; }
    }
}

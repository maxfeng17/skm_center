﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiPponOptionCategory
    {
        public ApiPponOptionCategory()
        {
            Options = new List<ApiPponOption>();
        }

        /// <summary>
        /// 分類的識別代號
        /// </summary>
        public Guid CategoryGuid { get; set; }
        public string CategoryName { get; set; }
        public List<ApiPponOption> Options { get; set; }
    }

    public class ApiPponOption
    {
        /// <summary>
        /// 剩餘數量預設值
        /// </summary>
        public const int DefaultQuantity = 99999;

        /// <summary>
        /// 選項的識別代號
        /// </summary>
        public Guid OptionGuid { get; set; }
        /// <summary>
        /// 選項名稱
        /// </summary>
        public string OptionName { get; set; }
        /// <summary>
        /// 剩餘數量
        /// </summary>
        public int Quantity { get; set; }
    }
}

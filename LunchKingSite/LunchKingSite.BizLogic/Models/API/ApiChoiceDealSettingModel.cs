﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.API
{
    public class ApiChoiceDealSettingModel
    {
        public int TotalCount { get; set; }
        public List<ChannelSetting> ChannelList { get;set;}
    }

    public class ChannelSetting
    {
        public int ChannelId { get; set; }
        public int TakeCount { get; set; }
    }
}

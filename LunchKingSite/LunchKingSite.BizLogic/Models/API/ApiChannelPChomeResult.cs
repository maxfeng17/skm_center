﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// PCHOME代銷使用
    /// </summary>
    public class ApiChannelPChomeResult
    {
        private string _msg;
        /// <summary>
        /// 回傳的結果
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 回傳的訊息
        /// </summary>
        public string Msg
        {
            get
            {
                return _msg;
            }
            set
            {
                _msg = value;
            }
        }


        public ApiChannelPChomeResult()
        {
            Code = "00";
            Msg = string.Empty;
        }
    }

    /// <summary>
    /// PCHOME訂購
    /// </summary>
    public class ApiPChomeMakeOrderReturnObject
    {
        /// <summary>
        /// 
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<MakeOrderVouchers> Vouchers { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AppearType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeMsg { get; set; }
        
    }

    public class MakeOrderVouchers
    {
        /// <summary>
        /// 
        /// </summary>
        public string VoucherUId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string VoucherNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AuthCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string VoucherUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string StartTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ExpireTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AmountUnit { get; set; }
    }
    /// <summary>
    /// PCHOME退貨
    /// </summary>
    public class ApiPChomeRefundLionTravelOrderModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<ReturnVouchers> Vouchers { get; set; }
    }

    public class ReturnVouchers
    {
        /// <summary>
        /// 
        /// </summary>
        public int VoucherUId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RefundAmount { get; set; }
    }

    /// <summary>
    /// PCHOME多筆憑證查詢
    /// </summary>
    public class ApiPChomeMultiCouponStatusModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<CouponStatusVouchers> Vouchers { get; set; }
    }

    public class CouponStatusVouchers
    {
        /// <summary>
        /// 
        /// </summary>
        public string VoucherUId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int VoucherStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RemainAmount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UpdateTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RefundStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RefundTime { get; set; }
    }
}

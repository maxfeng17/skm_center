﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiVourcherPromoData
    {
        /// <summary>
        /// 行銷編號
        /// </summary>
        public int PromoId { get; set; }
        /// <summary>
        /// 行銷類型
        /// </summary>
        public VourcherPromoType Type { get; set; }
        /// <summary>
        /// 商家ID
        /// </summary>
        public string SellerId { get; set; }
        /// <summary>
        /// 優惠商家名稱
        /// </summary>
        public string SellerName { get; set; }
        /// <summary>
        /// Promo圖片
        /// </summary>
        public string LogoPic { get; set; }
        /// <summary>
        /// 優惠編號
        /// </summary>
        public int? EventId { get; set; }
        /// <summary>
        /// 關注數
        /// </summary>
        public int AttentionCount { get; set; }
        /// <summary>
        /// 優惠券說明
        /// </summary>
        public string VourcherContent { get; set; }
    }
}

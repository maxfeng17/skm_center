﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiUserCashDetail
    {
        public decimal SCash { get; set; }
        public double BCash { get; set; }
        public decimal PCash { get; set; }
        public List<ApiDiscountCode> DiscountCodes { get; set; }
    }
}

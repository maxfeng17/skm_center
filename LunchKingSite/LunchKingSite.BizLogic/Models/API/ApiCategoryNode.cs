﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiCategoryNode
    {
        private ApiCategoryNode(string name, CategoryType type, string shortName , int id)
        {
            _mainCategory = new Category();
            _mainCategory.Id = id;
            _mainCategory.Name = name;
            _mainCategory.Type = (int)type;
            _mainCategory.IsFinal = false;
            _shortName = shortName;
            //排序序號預設為-1卻表依據序號排序時預設的CategoryNode會排在第一位
            _seq = -1;
            ExistsDeal = false;
            NodeDatas = new List<ApiCategoryTypeNode>();
        }

        public ApiCategoryNode(Category mainCategory)
        {
            _mainCategory = mainCategory;
            NodeDatas = new List<ApiCategoryTypeNode>();
            ExistsDeal = false;//預設不存在
        }

        #region 參數
        private readonly Category _mainCategory;
        /// <summary>
        /// 此節點代表的Category之ID
        /// </summary>
        [JsonProperty("Id")]
        public int CategoryId
        {
            get { return _mainCategory.Id; }
        }
        /// <summary>
        /// 此節點代表的Category之名稱
        /// </summary>
        [JsonProperty("Name")]
        public string CategoryName
        {
            get { return _mainCategory.Name; }
            set { _mainCategory.Name=value; }
        }
        /// <summary>
        /// 此節點代表的Category之類型
        /// </summary>
        [JsonProperty("Type")]
        public CategoryType CategoryType
        {
            get
            {
                if (_mainCategory.Type == null)
                {
                    return CategoryType.None;
                }
                return (CategoryType)_mainCategory.Type;
            }
        }

        /// <summary>
        /// 此節點代表的Category為leaf (最後一個)
        /// </summary>
        public bool IsFinal
        {
            get { return _mainCategory.IsFinal; }
        }

        private string _shortName = string.Empty;
        /// <summary>
        /// short欄位，提供短的 Category名稱，供顯示區塊空間不夠時使用
        /// </summary>
        public string ShortName
        {
            get {
                return string.IsNullOrWhiteSpace(_shortName) ? _mainCategory.Name : _shortName;
            }
            set { _shortName = value; }
        }

        private int? _seq;
        /// <summary>
        /// 此節點代表的Category之後設定的序號，若程式端為特別指定，以Category之Rank欄位為預設值
        /// </summary>
        public int Seq
        {
            get
            {
                if (_seq.HasValue)
                {
                    return _seq.Value;
                }
                if (_mainCategory.Rank.HasValue)
                {

                    return _mainCategory.Rank.Value;
                }
                return int.MaxValue;
            }
            set { _seq = value; }
        }
        /// <summary>
        /// 是否存在檔次資料
        /// </summary>
        public bool ExistsDeal { get; set; }


        private int? _icontype;
        /// <summary>
        /// Icon圖示
        /// </summary>
        public int IconType
        {
            get
            {
                return _mainCategory.IconType;
            }
            set { _icontype = value; }
        }


        /// <summary>
        /// 此分類項目下之子分類
        /// </summary>
        public List<ApiCategoryTypeNode> NodeDatas { get; set; }

        #endregion 參數

        /// <summary>
        /// 取得此節點代表之Category的物件，為建構元傳入之Category的Clone。
        /// </summary>
        /// <returns></returns>
        public Category GetMainCategory()
        {
            return _mainCategory.Clone();
        }

        public ApiCategoryNode Copy()
        {
            ApiCategoryNode rtn = new ApiCategoryNode(_mainCategory.Clone());
            rtn.ExistsDeal = this.ExistsDeal;
            rtn._shortName = this._shortName;
            rtn._seq = this._seq;
            rtn._icontype = this._icontype;
            foreach (var categoryTypeNode in NodeDatas)
            {
                rtn.NodeDatas.Add(categoryTypeNode.Copy());
            }
            return rtn;
        }

        /// <summary>
        /// 由子項目中取出某個CategoryType的CategoryNode List，若無此type或此type無資料，回傳空的CategoryNode List
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<ApiCategoryNode> GetSubCategoryTypeNode(CategoryType type)
        {
            List<ApiCategoryTypeNode> subTypeNodes = NodeDatas.Where(x => x.NodeType == type).ToList();
            if (subTypeNodes.Count > 0)
            {
                ApiCategoryTypeNode subNode = subTypeNodes.First();
                return subNode.CategoryNodes.Select(categoryNode => categoryNode.Copy()).ToList();
            }
            return new List<ApiCategoryNode>();
        }

        /// <summary>
        /// 根據子分類查找CategoryNode
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ApiCategoryNode GetSubCategoryNodeByCategoryId(int categoryId)
        {
            List<ApiCategoryNode> subNodes = NodeDatas.SelectMany(x => x.CategoryNodes).ToList();
            return subNodes.Where(x => x.CategoryId == categoryId).FirstOrDefault();
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", this.CategoryId, this.CategoryName);
        }
        /// <summary>
        /// 傳入某個category，檢查此CategoryNode，回傳找到此Id資料經過的每個CategoryId的陣列
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public List<int> GetCategoryPath(int categoryId)
        {
            if (categoryId == CategoryId)
            {
                List<int> rtnList = new List<int>();
                rtnList.Add(categoryId);
                return rtnList;
            }
            else
            {
                foreach (var categoryTypeNode in NodeDatas)
                {
                    List<int> subData = categoryTypeNode.GetCategoryPath(categoryId);
                    if (subData.Count > 0)
                    {
                        //子項目有符合條件的資料，將本身的CategoryId，插入回傳陣列的第一筆，因為依據路徑來看，越上層的要越上面
                        subData.Insert(0, CategoryId);
                        return subData;
                    }
                }
            }
            return new List<int>();
        }

        #region public static
        public static ApiCategoryNode GetDefaultCategory(string name = "全部", CategoryType type = CategoryType.None, string shortName = "全部", int id = 0)
        {
            return new ApiCategoryNode(name, type, shortName, id);
        }

        public static ApiCategoryNode CreateApiCategoryNode(CategoryNode node, bool inFrontEnd)
        {
            ApiCategoryNode rtn = new ApiCategoryNode(node.GetMainCategory());            
            rtn.Seq = node.Seq;
            rtn.ExistsDeal = false;
            rtn.NodeDatas = new List<ApiCategoryTypeNode>();

            var displayRule = CategoryManager.GetRegionDisplayRuleByCid(rtn.CategoryId);
            if (displayRule != null && Helper.IsFlagSet(displayRule.SpecialRuleFlag, CategoryDisplayRuleFlag.ShowDisplayName))
            {
                rtn.ShortName = displayRule.CategoryDisplayName;
            }
            else
            {
                rtn.ShortName = node.ShortName;
            }

            foreach (var categoryTypeNode in node.NodeDatas)
            {
                rtn.NodeDatas.Add(ApiCategoryTypeNode.CreateApiCategoryTypeNode(categoryTypeNode, inFrontEnd));
            }
            return rtn;
        }
        #endregion public static
    }
}

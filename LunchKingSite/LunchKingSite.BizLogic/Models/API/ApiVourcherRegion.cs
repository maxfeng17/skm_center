﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiVourcherRegion
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public List<string> AreaNameList { get; set; }

        public ApiVourcherRegion()
        {
            AreaNameList = new List<string>();
        }
    }
}

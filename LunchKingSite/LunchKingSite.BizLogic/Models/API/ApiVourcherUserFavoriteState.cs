﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiVourcherUserFavoriteState
    {
        public int EventId { get; set; }
        /// <summary>
        /// 是否收藏
        /// </summary>
        public bool IsCollect { get; set; }
        /// <summary>
        /// 使否已使用過此優惠券
        /// </summary>
        public bool EventIsUsed { get; set; }
        /// <summary>
        /// 會員收藏優惠券數量
        /// </summary>
        public int Count{ get; set; }


        public ApiVourcherUserFavoriteState()
        {
            IsCollect = false;
            EventIsUsed = false;
        }
    }
}

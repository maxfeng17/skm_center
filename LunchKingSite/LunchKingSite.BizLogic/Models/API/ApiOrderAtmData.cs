﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiOrderAtmData
    {
        /// <summary>
        /// 銀行名稱
        /// </summary>
        public string BankName { get; set; }
        /// <summary>
        /// 銀行編號
        /// </summary>
        public string BankCode { get; set; }
        /// <summary>
        /// 銀行帳號
        /// </summary>
        public string BankAccount { get; set; }
        /// <summary>
        /// 匯款期限
        /// </summary>
        public string DeadlineDate { get; set; }
        /// <summary>
        /// 金額
        /// </summary>
        public decimal Amount { get; set; }

    }
}

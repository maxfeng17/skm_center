﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiUserOrderGroup
    {
        /// <summary>
        /// 類型
        /// </summary>
        public PponOrderGroupCountType CountType { get; set; }
        /// <summary>
        /// 數量
        /// </summary>
        public int Count { get; set; }
    }
}

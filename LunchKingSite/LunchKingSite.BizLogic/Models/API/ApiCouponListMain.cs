﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiCouponListMain
    {
        public Guid OrderGuid { get; set; }
        public CouponListFilterType CountType { get; set; }
        public string CouponName { get; set; }
        public string UseStartDate { get; set; }
        public string UseEndDate { get; set; }
        public string MainImagePath { get; set; }
        public int CouponCount { get; set; }
        /// <summary>
        /// 宅配商品已過鑑賞期
        /// </summary>
        public bool PassRefundEndDate { get; set; }
        public string OrderTypeDesc { get; set; }
        /// <summary>
        /// 停用簡訊
        /// </summary>
        public bool DisableSMS { get; set; }

        public string BuyDate { get; set; }
        /// <summary>
        /// 憑證類型，包括17life憑證、廠商提供序號、全家pin code、全家bar code等
        /// </summary>
        public DealCouponType CouponType { get; set; }
    }
}

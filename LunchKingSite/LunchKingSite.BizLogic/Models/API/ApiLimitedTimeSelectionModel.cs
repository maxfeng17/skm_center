﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.BizLogic.Models.API
{
    public class ApiLimitedTimeSelectionResult
    {
        public string BannerImagePath { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }

        public List<PponDealSynopsisBasic> DealList { get; set; }
    }
}

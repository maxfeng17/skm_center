﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiPromoEventMapData : System.ICloneable
    {
        public ApiPromoEventMapData()
        {
            CouponDeals = new List<ApiPponDealMapData>();
            VourcherStores = new List<ApiVourcherStore>();
        }
        /// <summary>
        /// 此行銷活動的名稱,活動顯示的title
        /// </summary>
        public string EventTitle { get; set; }
        /// <summary>
        /// 活動編號,活動識別
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 活動開始時間
        /// </summary>
        public string StartDate { get; set; }
        /// <summary>
        /// 活動截止時間
        /// </summary>
        public string EndDate { get; set; }
        /// <summary>
        /// 檔次版型,檔次版型
        /// </summary>
        public EventPromoTemplateType TemplateType { get; set; }
        /// <summary>
        /// 主題店鋪或景點;物件;先供新光商圈活動使用，顯示地圖上的新光百貨位置，未來可能會改變規則供其他用途
        /// </summary>
        public List<ApiPromoEventMainStore> MainStores { get; set; }
        /// <summary>
        /// 團購券資料;物件陣列;若此活動無團購券資料，為空陣列
        /// </summary>
        public List<ApiPponDealMapData> CouponDeals { get; set; }
        /// <summary>
        /// 優惠券資料;物件陣列;若此活動無優惠資料，為空陣列
        /// </summary>
        public List<ApiVourcherStore> VourcherStores { get; set; }


        public object Clone()
        {
            ApiPromoEventMapData rtn = (ApiPromoEventMapData)this.MemberwiseClone();
            //處理淺層複製無法處理的部分
            rtn.MainStores = new List<ApiPromoEventMainStore>();
            foreach (var store in this.MainStores)
            {
                rtn.MainStores.Add(store.Clone() as ApiPromoEventMainStore);
            }

            rtn.CouponDeals = new List<ApiPponDealMapData>();
            foreach (var deal in this.CouponDeals)
            {
                rtn.CouponDeals.Add(deal.Clone() as ApiPponDealMapData);
            }
            rtn.VourcherStores = new List<ApiVourcherStore>();
            foreach (var vourcher in this.VourcherStores)
            {
                rtn.VourcherStores.Add(vourcher.Clone() as ApiVourcherStore);
            }
            return rtn;
        }
    }

    /// <summary>
    /// API用商品主題活動頁，用來表示活動主題的店家資訊
    /// 一開始是供新光商圈時，標示各間新光百貨資訊
    /// </summary>
    public class ApiPromoEventMainStore : System.ICloneable
    {
        /// <summary>
        /// 店鋪名稱;字串;此標示的分店名稱
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 距離顯示文字;字串;距離最近的分店之距離，由後台編輯成文字後回傳，例如:1.9km
        /// </summary>
        public string DistanceDesc { get; set; }
        /// <summary>
        /// 距離數字;double;距離最近的分店之距離，單位公尺，無法提供數據時預設為0
        /// </summary>
        public double Distance { get; set; }
        /// <summary>
        /// 經緯度;物件;分店位置
        /// </summary>
        public ApiCoordinates Coordinates { get; set; }

        public object Clone()
        {
            ApiPromoEventMainStore rtn = (ApiPromoEventMainStore)this.MemberwiseClone();
            //處理淺層複製無法處理的部分
            rtn.Coordinates = Coordinates == null ? null : Coordinates.Clone() as ApiCoordinates;
            return rtn;
        }
    }
}

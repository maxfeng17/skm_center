﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// 運費設定
    /// </summary>
    public class ApiFreight
    {
        /// <summary>
        /// 商品金額起(含)
        /// </summary>
        public decimal StartAmount { get; set; }
        /// <summary>
        /// 商品金額迄(不含)
        /// </summary>
        public decimal EndAmount { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public decimal FreightAmount { get; set; }
    }
}

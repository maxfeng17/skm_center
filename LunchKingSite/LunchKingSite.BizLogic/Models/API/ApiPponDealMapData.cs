﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// 供API回傳地圖上要顯示的好康資料用的物件
    /// </summary>
    public class ApiPponDealMapData : System.ICloneable
    {
        /// <summary>
        /// 檔次unique id;字串;檔次的識別編號
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 檔次名稱;字串;顯示於畫面的描述
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 檔次說明-黑標;字串;簡短的檔次說明
        /// </summary>
        public string DealEventName { get; set; }
        /// <summary>
        /// 檔次首圖;字串;圖片之url
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// 單價;double;此商品於本服務的售價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 折扣資訊顯示文字;字串;顯示於畫面上的商品折扣比例文字，無折扣或特殊檔次會顯示"特惠"等其他訊息
        /// </summary>
        public string DiscountString { get; set; }
        /// <summary>
        /// 檔次活動截止時間;字串;時間內容已後續格式顯示格式為yyyyMMdd HHmmss Z
        /// </summary>
        public string DealEndTime { get; set; }
        /// <summary>
        /// 是否已銷售完畢;bool;true表示商品已銷售完畢，不可再販售
        /// </summary>
        public bool SoldOut { get; set; }
        /// <summary>
        /// 定位之分店名稱;字串;表示座標所標示的分店
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 距離顯示文字;字串;距離最近的分店之距離，由後台編輯成文字後回傳，例如:1.9km
        /// </summary>
        public string DistanceDesc { get; set; }
        /// <summary>
        /// 距離數字;double;距離最近的分店之距離，單位公尺，無法提供數據時預設為0
        /// </summary>
        public double Distance { get; set; }

        /// <summary>
        /// 經緯度;物件;分店位置
        /// </summary>
        public ApiCoordinates Coordinates { get; set; }


        public object Clone()
        {
            ApiPponDealMapData rtn = (ApiPponDealMapData)this.MemberwiseClone();
            //處理淺層複製無法處理的部分
            rtn.Bid = new Guid(Bid.ToString());
            rtn.Coordinates = Coordinates == null ? null : Coordinates.Clone() as ApiCoordinates;
            return rtn;
        }
    }
}

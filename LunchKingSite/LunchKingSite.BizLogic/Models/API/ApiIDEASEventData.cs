﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiIDEASReturnObject
    {
        /// <summary>
        /// 回傳的結果
        /// </summary>
        public ApiReturnCode code { get; set; }
        /// <summary>
        /// 回傳資料主體
        /// </summary>
        public object data { get; set; }
        /// <summary>
        /// 回傳的訊息
        /// </summary>
        public string message { get; set; }


        public ApiIDEASReturnObject()
        {
            code = ApiReturnCode.Success;
            data = null;
            message = string.Empty;
        }

        public ApiIDEASReturnObject(ApiReturnObject inputData)
        {
            code = inputData.Code;
            data = inputData.Data;
            message = inputData.Message;
        }
    }


    /// <summary>
    /// 資策會雙北APP需要的各站點資料
    /// </summary>
    public class ApiIDEASEventData
    {
        public ApiIDEASEventData()
        {
            specialDeals = new List<ApiIDEASDealBroadcastData>();
            couponDeals = new List<ApiIDEASDealBroadcastData>();
            deliveryDeals = new List<ApiIDEASDealBroadcastData>();
            vourcherStores = new List<ApiIDEASVourcherBroadcastData>();
        }

        /// <summary>
        /// 資料編號
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 名稱
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 播放日期 yyyyMMdd+zz00
        /// </summary>
        public string broadcastDate { get; set; }
        /// <summary>
        /// 特殊檔次（限時限量）;陣列;異常或無資料時為空陣列 字串內容範例 "20130930 120000 +0800~20130930 120000 +0800"
        /// </summary>
        public List<string> specialDealBroadcastTime { get; set; }
        /// <summary>
        /// 嚴選或特別商品
        /// </summary>
        public List<ApiIDEASDealBroadcastData> specialDeals { get; set; }
        /// <summary>
        /// 團購檔次每次播放的分鐘數;int;團購檔次每次播放的分鐘數，撥完後輪下一個類型播放
        /// </summary>
        public int couponDealBroadcastMinutes { get; set; }
        /// <summary>
        /// 團購商品
        /// </summary>
        public List<ApiIDEASDealBroadcastData> couponDeals { get; set; }
        /// <summary>
        /// 宅配檔次每次播放的分鐘數;int;宅配檔次每次播放的分鐘數，撥完後輪下一個類型播放
        /// </summary>
        public int deliveryDealBroadcastMinutes { get; set; }
        /// <summary>
        /// 宅配商品
        /// </summary>
        public List<ApiIDEASDealBroadcastData> deliveryDeals { get; set; }
        /// <summary>
        /// 優惠券每次播放的分鐘數;int;優惠券每次播放的分鐘數，撥完後輪下一個類型播放
        /// </summary>
        public int vourcherStoreBroadcastMinutes { get; set; }
        /// <summary>
        /// 優惠券資料
        /// </summary>
        public List<ApiIDEASVourcherBroadcastData> vourcherStores { get; set; }
    }

    /// <summary>
    /// 提供IDEAS個檔次的輪播時段設定資料物件
    /// </summary>
    public class ApiIDEASDealBroadcastData
    {
        /// <summary>
        /// 檔次編號
        /// </summary>
        public int dealId { get; set; }
    }
    /// <summary>
    /// 提供IDEAS各優惠券的輪播時段設定資料物件
    /// </summary>
    public class ApiIDEASVourcherBroadcastData
    {
        /// <summary>
        /// 優惠券編號
        /// </summary>
        public int eventId { get; set; }
    }

    public class ApiIDEASSubDeal
    {
        /// <summary>
        /// 檔次編號;int
        /// </summary>
        public int dealId { get; set; }
        /// <summary>
        /// 多檔次列表時顯示的字串;string
        /// </summary>
        public string title { get; set; }
    }


    public class ApiIDEASPponStore
    {
        /// <summary>
        /// 分店編號
        /// </summary>
        public Guid guid { get; set; }

        /// <summary>
        /// 分店名稱
        /// </summary>
        public string storeName { get; set; }

        /// <summary>
        /// 電話
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// 營業時間
        /// </summary>
        public string openTime { get; set; }

        /// <summary>
        /// 公休日
        /// </summary>
        public string closeDate { get; set; }

        /// <summary>
        /// 備註
        /// </summary>
        public string remarks { get; set; }

        /// <summary>
        /// 交通資訊-MRT
        /// </summary>
        public string mrt { get; set; }

        /// <summary>
        /// 交通資訊-開車
        /// </summary>
        public string car { get; set; }

        /// <summary>
        /// 交通資訊-公車
        /// </summary>
        public string bus { get; set; }

        /// <summary>
        /// 交通資訊-其他
        /// </summary>
        public string otherVehicles { get; set; }

        /// <summary>
        /// 官網
        /// </summary>
        public string webUrl { get; set; }

        /// <summary>
        /// 官方FB
        /// </summary>
        public string fbUrl { get; set; }

        /// <summary>
        /// 官方噗浪
        /// </summary>
        public string plurkUrl { get; set; }

        /// <summary>
        /// 官方部落格
        /// </summary>
        public string blogUrl { get; set; }

        /// <summary>
        /// 其他網路網路資訊
        /// </summary>
        public string otherUrl { get; set; }

        /// <summary>
        /// 能使用CreditCard
        /// </summary>
        public bool creditcardAvailable { get; set; }

        /// <summary>
        /// 緯度座標
        /// </summary>
        public double latitude { get; set; }
        /// <summary>
        /// 經度座標
        /// </summary>
        public double longitude { get; set; }


        public ApiIDEASPponStore()
        {
            creditcardAvailable = false;
        }
    }


    /// <summary>
    /// 運費設定
    /// </summary>
    public class ApiIDEASFreight
    {
        /// <summary>
        /// 商品金額起(含)
        /// </summary>
        public decimal startAmount { get; set; }
        /// <summary>
        /// 商品金額迄(不含)
        /// </summary>
        public decimal endAmount { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public decimal freightAmount { get; set; }
    }


    public class ApiIDEASPponOptionCategory
    {
        public ApiIDEASPponOptionCategory()
        {
            options = new List<ApiIDEASPponOption>();
        }

        /// <summary>
        /// 分類的識別代號
        /// </summary>
        public Guid categoryGuid { get; set; }
        public string categoryName { get; set; }
        public List<ApiIDEASPponOption> options { get; set; }
    }

    public class ApiIDEASPponOption
    {
        /// <summary>
        /// 剩餘數量預設值
        /// </summary>
        public const int defaultQuantity = 99999;

        /// <summary>
        /// 選項的識別代號
        /// </summary>
        public Guid optionGuid { get; set; }
        /// <summary>
        /// 選項名稱
        /// </summary>
        public string optionName { get; set; }
        /// <summary>
        /// 剩餘數量
        /// </summary>
        public int quantity { get; set; }
    }


    /// <summary>
    /// 提供給IDEAS的檔次資料內容
    /// </summary>
    public class ApiIDEASDeal
    {
        public ApiIDEASDeal()
        {
            photoUrls = new List<string>();
            subDealData = new List<ApiIDEASSubDeal>();
            freightList = new List<ApiIDEASFreight>();
            optionCategories = new List<ApiIDEASPponOptionCategory>();
            stores = new List<ApiIDEASPponStore>();
        }
        /// <summary>
        /// 檔次代號;int
        /// </summary>
        public int dealId { get; set; }
        /// <summary>
        /// 檔次長標題;string ;看板設計稿中的橘色標題
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 看板顯示長標題;string ;看板設計稿中的黑色標題，與17life Web版顯示之黑標同
        /// </summary>
        public string kanbanName { get; set; }
        /// <summary>
        /// 好康券名稱 app現行顯示的名稱;string
        /// </summary>
        public string dealName { get; set; }
        /// <summary>
        /// 簡介
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 圖片列表
        /// </summary>
        public List<string> photoUrls { get; set; }
        /// <summary>
        /// 權益說明
        /// </summary>
        public string introduction { get; set; }
        /// <summary>
        /// 是否為購物車檔次;bool;true表示購買流程為購物車流程，false表示購買流程為一般流程
        /// </summary>
        public bool isShoppingCart { get; set; }
        /// <summary>
        /// 取貨類型 ;int;標記為宅配或到店 說明
        /// </summary>
        public DeliveryType pponDeliveryType { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal originPrice { get; set; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal sellPrice { get; set; }
        /// <summary>
        /// 折扣說明
        /// </summary>
        public string discount { get; set; }
        /// <summary>
        /// 商品總數
        /// </summary>
        public decimal total { get; set; }
        /// <summary>
        /// 目前已售出數量
        /// </summary>
        public int orderedQuantity { get; set; }
        /// <summary>
        /// 設定門檻
        /// </summary>
        public decimal quantityMin { get; set; }
        /// <summary>
        /// 成套商品每套數量 本周不能上線
        /// </summary>
        //public int comboPackCount { get; set; }
        /// <summary>
        /// 團購:兌換起始日  宅配:商品開始配送日 ; string ; 日期格式 yyyyMMdd HHmmss zz00
        /// </summary>
        public string validStartTime { get; set; }
        /// <summary>
        /// 團購:兌換截止日  宅配:最晚配送日 ; string ; 日期格式 yyyyMMdd HHmmss zz00
        /// </summary>
        public string validEndTime { get; set; }
        /// <summary>
        /// 檔次銷售時間起 ; string ; 日期格式 yyyyMMdd HHmmss zz00
        /// </summary>
        public string dealStartTime { get; set; }
        /// <summary>
        /// 檔次銷售時間迄 ; string ; 日期格式 yyyyMMdd HHmmss zz00
        /// </summary>
        public string dealEndTime { get; set; }
        /// <summary>
        /// 好康詳細說明，html字串，濾掉table與iframe等標籤
        /// </summary>
        public string restriction { get; set; }
        /// <summary>
        /// 多檔次狀態 ; int ; 0:非多檔次 1:多檔次代表檔 2:多檔次子檔 說明
        /// </summary>
        public ApiMultiDealType multiDealType { get; set; }
        /// <summary>
        /// 子檔內容，非多檔次代表檔，則為空陣列
        /// </summary>
        public List<ApiIDEASSubDeal> subDealData { get; set; }
        /// <summary>
        /// 是否不配送外島;bool;true為不配送，false為配送
        /// </summary>
        public bool notDeliveryIslands { get; set; }
        /// <summary>
        /// 運費資訊的陣列，非宅配檔次則為空陣列 
        /// </summary>
        public List<ApiIDEASFreight> freightList { get; set; }
        /// <summary>
        /// 商品款式選項,物件陣列,無選項時為空陣列
        /// </summary>
        public List<ApiIDEASPponOptionCategory> optionCategories { get; set; }
        /// <summary>
        /// 分店
        /// </summary>
        public List<ApiIDEASPponStore> stores { get; set; }
    }


    public class ApiIDEASVourcherEventStore : ApiIDEASPponStore
    {
        /// <summary>
        /// 適用店家ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 距離顯示字串
        /// </summary>
        public string distanceDesc { get; set; }
        /// <summary>
        /// 距離(單位公尺)
        /// </summary>
        public double distance { get; set; }
    }


    /// <summary>
    /// 提供給IDEAS的優惠券資料內容
    /// </summary>
    public class ApiIDEASVourcher
    {
        public ApiIDEASVourcher()
        {
            picUrl = new List<string>();
            eventStores = new List<ApiIDEASVourcherEventStore>();
        }
        /// <summary>
        /// 優惠券編號;int
        /// </summary>
        public int eventId { get; set; }
        /// <summary>
        /// 賣家名稱;string
        /// </summary>
        public string sellerName { get; set; }
        /// <summary>
        /// 圖片;string
        /// </summary>
        public string sellerLogoimgPath { get; set; }
        /// <summary>
        /// 賣家說明;string
        /// </summary>
        public string sellerDescription { get; set; }
        /// <summary>
        /// 優惠內容;string
        /// </summary>
        public string vourcherContent { get; set; }

        /// <summary>
        /// 使用方式;string
        /// </summary>
        public string instruction { get; set; }

        /// <summary>
        /// 優惠券限制;string
        /// </summary>
        public string restriction { get; set; }
        /// <summary>
        /// 其他使用說明
        /// </summary>
        public string others { get; set; }
        /// <summary>
        /// 優惠可使用時間起 ; string ; 日期格式 yyyyMMdd HHmmss zz00
        /// </summary>
        public string validStartTime { get; set; }
        /// <summary>
        /// 優惠可使用時間迄 ; string ; 日期格式 yyyyMMdd HHmmss zz00
        /// </summary>
        public string validEndTime { get; set; }

        /// <summary>
        /// 優惠券使用模式; int ; 0:無限制 1:無上限，每人限定一次 2:有上限且每人限用一次 3:有上限，每人不限次數 說明
        /// </summary>
        public VourcherEventMode mode { get; set; }
        /// <summary>
        /// 可最大數量
        /// </summary>
        public int maxQuantity { get; set; }
        /// <summary>
        ///優惠詳圖;string array 無資料為空陣列
        /// </summary>
        public List<string> picUrl { get; set; }
        /// <summary>
        /// 關注數;int
        /// </summary>
        public int attentionCount { get; set; }
        /// <summary>
        /// 平均店消費 ; int ;店消費金額的代號
        /// </summary>
        public SellerConsumptionAvg? consumptionAvg { get; set; }
        /// <summary>
        /// 活動配合分店
        /// </summary>
        public List<ApiIDEASVourcherEventStore> eventStores { get; set; }
    }

    /// <summary>
    /// 提供給雙北APP的檔次與優惠券資料
    /// </summary>
    public class ApiIDEASEventBaseData
    {
        public ApiIDEASEventBaseData()
        {
            products = new List<ApiIDEASDeal>();
            coupons = new List<ApiIDEASVourcher>();
        }
        /// <summary>
        /// 團購檔次資料
        /// </summary>
        public List<ApiIDEASDeal> products { get; set; }
        /// <summary>
        /// 優惠券檔次資料
        /// </summary>
        public List<ApiIDEASVourcher> coupons { get; set; }
    }

    /// <summary>
    /// 雙北APP需要的檔次個別剩下的數量資訊
    /// </summary>
    public class ApiIDEASSurplusQuantityData
    {
        public ApiIDEASSurplusQuantityData()
        {
            products = new List<ApiIDEASDealSurplusQuantity>();
            coupons = new List<ApiIDEASVourcherSurplusQuantity>();
        }
        public List<ApiIDEASDealSurplusQuantity> products { get; set; }
        public List<ApiIDEASVourcherSurplusQuantity> coupons { get; set; }
    }

    public class ApiIDEASDealSurplusQuantity
    {
        public ApiIDEASDealSurplusQuantity()
        {
            optionCategories = new List<ApiIDEASDealOptionCategory>();
            stores = new List<ApiIDEASDealStoreSurplusQuantity>();
        }

        /// <summary>
        /// 檔次編號;int
        /// </summary>
        public int dealId { get; set; }
        /// <summary>
        /// 已銷售數量;int
        /// </summary>
        public int orderedQuantity { get; set; }
        /// <summary>
        /// 剩餘可購買數量;int
        /// </summary>
        public int surplusQuantity { get; set; }
        /// <summary>
        /// 商品選項;無選項之檔次為空陣列
        /// </summary>
        public List<ApiIDEASDealOptionCategory> optionCategories { get; set; }
        public List<ApiIDEASDealStoreSurplusQuantity> stores { get; set; }
    }


    public class ApiIDEASDealOptionCategory
    {
        public ApiIDEASDealOptionCategory()
        {
            options = new List<ApiIDEASDealOption>();
        }

        /// <summary>
        /// 分類的識別代號
        /// </summary>
        public Guid categoryGuid { get; set; }
        public string categoryName { get; set; }
        public List<ApiIDEASDealOption> options { get; set; }
    }

    public class ApiIDEASDealOption
    {
        /// <summary>
        /// 剩餘數量預設值
        /// </summary>
        public const int defaultQuantity = 99999;

        /// <summary>
        /// 選項的識別代號
        /// </summary>
        public Guid optionGuid { get; set; }
        /// <summary>
        /// 選項名稱
        /// </summary>
        public string optionName { get; set; }
        /// <summary>
        /// 剩餘數量
        /// </summary>
        public int surplusQuantity { get; set; }
    }







    public class ApiIDEASDealStoreSurplusQuantity
    {
        /// <summary>
        /// 分店編號;string
        /// </summary>
        public Guid storeGuid { get; set; }
        /// <summary>
        /// 剩餘數量，若無限制一律回傳此檔次的剩餘數量
        /// </summary>
        public int surplusQuantity { get; set; }
    }

    /// <summary>
    /// 提供雙北APP優惠券剩餘數量的物件
    /// </summary>
    public class ApiIDEASVourcherSurplusQuantity
    {
        /// <summary>
        /// 優惠券編號
        /// </summary>
        public int eventId { get; set; }
        /// <summary>
        /// 剩餘數量，若無限制一律回傳-1
        /// </summary>
        public int surplusQuantity { get; set; }

    }

    /// <summary>
    /// 供雙北APP付款用的API
    /// </summary>
    public class ApiIDEASPaymentData
    {
        public ApiIDEASPaymentData()
        {
            discountCode = string.Empty;
        }
        /// <summary>
        /// 購買檔次的編號;int;購買商品的識別編號
        /// </summary>
        public int dealId { get; set; }
        /// <summary>
        /// 購買數量
        /// </summary>
        public int quantity { get; set; }

        /// <summary>
        /// 選取商家編號;string;選取分店的識別編號
        /// </summary>
        public string selectedStoreGuid { get; set; }

        /// <summary>
        ///商品規格選項 ; 物件陣列 ;內容架構為 規格組合與此組合購買的數量，無規格選項的檔次，回傳空陣列
        /// </summary>
        public List<ApiIDEASPaymentOptionCategory> itemOptions { get; set; }
        /// <summary>
        /// 發票捐獻設定
        /// </summary>
        public DonationReceiptsType donationReceiptsType { get; set; }
        /// <summary>
        /// 信用卡編號;string;連續不間斷的16個數字組成之字串
        /// </summary>
        public string creditcardNumber { get; set; }
        /// <summary>
        /// 信用卡背後三碼確認馬;string;三碼數字組成之字串 
        /// </summary>
        public string creditcardSecurityCode { get; set; }
        /// <summary>
        /// 信用卡有效年份;string;年份字串，一律兩碼數字之字串
        /// </summary>
        public string creditcardExpireYear { get; set; }
        /// <summary>
        /// 信用卡有效月份;string;月份字串，一律兩碼數字之字串
        /// </summary>
        public string creditcardExpireMonth { get; set; }
        /// <summary>
        /// 購買人姓名;string;客服聯絡時判定之購買人姓名
        /// </summary>
        public string purchaserName { get; set; }
        /// <summary>
        /// 購買人地址;string;客服聯絡時判定之購買人地址
        /// </summary>
        public string purchaserAddress { get; set; }
        /// <summary>
        /// 購買人手機號碼;string;客服聯絡時判定之購買人手機號碼
        /// </summary>
        public string purchaserPhone { get; set; }
        /// <summary>
        /// 購買人;string;客服聯絡時判定之購買人電子郵件
        /// </summary>
        public string purchaserEMail { get; set; }
        /// <summary>
        /// 宅配檔次之商品收件地址;string;宅配廠商送貨之地址，非宅配檔次設定為空白
        /// </summary>
        public string writeAddress { get; set; }
        /// <summary>
        /// 宅配檔次之商品收件地址郵遞區號;string;三碼郵遞區號，方便宅配廠商識別，非宅配檔次設定為空白
        /// </summary>
        public string writeZipCode { get; set; }
        /// <summary>
        /// 運費;消費者負擔之運費金額，非宅配檔次預設為0
        /// </summary>
        public decimal freight { get; set; }
        /// <summary>
        /// 收件人姓名;string;宅配廠商對收件人之稱呼，非宅配檔次設定為空白
        /// </summary>
        public string writeName { get; set; }
        /// <summary>
        /// 收件人姓名;string;宅配廠商聯絡收件人使用之電話號碼，非宅配檔次設定為空白
        /// </summary>
        public string writePhone { get; set; }
        /// <summary>
        /// 發票類型;int;消費者選擇之開立發票類型
        /// </summary>
        public ApiIDEASInvoiceType invoiceType { get; set; }
        /// <summary>
        /// 三聯式發票之發票抬頭;string;三聯式發票開立時需填寫之發票抬頭內容
        /// </summary>
        public string companyTitle { get; set; }
        /// <summary>
        /// 三聯式發票之統一編號;string;三聯式發票開立時需填寫之統一編號
        /// </summary>
        public string unifiedSerialNumber { get; set; }
        /// <summary>
        /// 三聯式發票之買受人姓名;string;寄送三聯式發票時，收件人之名稱
        /// </summary>
        public string invoiceBuyerName { get; set; }
        /// <summary>
        /// 三聯式發票之買受人地址;string;寄送三聯式發票時，要寄送到的地址
        /// </summary>
        public string invoiceBuyerAddress { get; set; }
        /// <summary>
        /// 優惠券編號
        /// </summary>
        public string discountCode { get; set; }

    }


    public class ApiIDEASPaymentOptionCategory
    {
        public ApiIDEASPaymentOptionCategory()
        {
            options = new List<ApiIDEASPaymentOptionItem>();
        }

        public List<ApiIDEASPaymentOptionItem> options { get; set; }
        public int itemQuantity { get; set; }
    }

    public class ApiIDEASPaymentOptionItem
    {
        public string optionName { get; set; }
        public string optionGuid { get; set; }
    }

    public class ApiIDEASPaymentReply
    {
        public ApiIDEASPaymentReply()
        {
            coupons = new List<ApiIDEASCoupon>();
        }

        /// <summary>
        /// 購買結果
        /// </summary>
        public ApiIDEASPaymentReplyStatus paymentStatus { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string orderId { get; set; }
        /// <summary>
        /// 產出之憑證列表
        /// </summary>
        public List<ApiIDEASCoupon> coupons { get; set; }
        /// <summary>
        /// 結帳金額
        /// </summary>
        public decimal amount { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string message { get; set; }
    }

    public class ApiIDEASCoupon
    {
        /// <summary>
        /// 憑證序號
        /// </summary>
        public int seq { get; set; }
        /// <summary>
        /// 憑證識別ID
        /// </summary>
        public int couponId { get; set; }
        /// <summary>
        /// 憑證編號
        /// </summary>
        public string sequenceNumber { get; set; }
        /// <summary>
        /// 確認碼
        /// </summary>
        public string couponCode { get; set; }
    }

    /// <summary>
    /// 資策會訂單的狀態
    /// </summary>
    public class ApiIDEASOrderProperty
    {
        public ApiIDEASOrderProperty()
        {
            couponIds = new List<ApiIDEASCouponProperty>();
        }

        /// <summary>
        /// 訂單編號
        /// </summary>
        public string orderId { get; set; }
        /// <summary>
        /// 訂單狀態
        /// </summary>
        public ApiIDEASOrderStatus orderStatus { get; set; }
        /// <summary>
        /// 憑證列表
        /// </summary>
        public List<ApiIDEASCouponProperty> couponIds { get; set; }
        /// <summary>
        /// 商品類訂單的的發票
        /// </summary>
        public string goodsInvoiceNumber { get; set; }
        /// <summary>
        /// 發票開立狀況
        /// </summary>
        public ApiInvoiceOfferType invoiceOfferType { get; set; }
    }
    /// <summary>
    /// 資策會憑證的狀態
    /// </summary>
    public class ApiIDEASCouponProperty
    {
        /// <summary>
        /// 憑證編號
        /// </summary>
        public int couponId { get; set; }
        /// <summary>
        /// 憑證狀態
        /// </summary>
        public ApiIDEASCouponStatus status { get; set; }
        /// <summary>
        /// 異動時間
        /// </summary>
        public string modifyTime { get; set; }
        /// <summary>
        /// 發票編號
        /// </summary>
        public string invoiceNumber { get; set; }
        /// <summary>
        /// 發票狀態
        /// </summary>
        public ApiInvoiceOfferType invoiceOfferType { get; set; }
    }


    /// <summary>
    /// 索取紙本發票的結果
    /// </summary>
    public class ApiIDEASInvoiceRequestReply
    {
        public ApiIDEASInvoiceRequestStatus Status { get; set; }
        public string Message { get; set; }
    }

    public class ApiIDEASSendSMSReply
    {
        public ApiIDEASSendSMSReplyStatus Status { get; set; }
        public int Count { get; set; }
        public string Message { get; set; }
    }

    #region IDEAS各車站據點的資料
    public class IDEASStationData
    {
        public int Id { get; set; }
        public string Title { get; set; }
        /// <summary>
        /// 團購檔次每次播放的分鐘數;int;團購檔次每次播放的分鐘數，撥完後輪下一個類型播放
        /// </summary>
        public int CouponDealBroadcastMinutes { get; set; }
        public string CouponEventUrl { get; set; }

        /// <summary>
        /// 宅配檔次每次播放的分鐘數;int;宅配檔次每次播放的分鐘數，撥完後輪下一個類型播放
        /// </summary>
        public int DeliveryDealBroadcastMinutes { get; set; }
        public string DeliveryEventUrl { get; set; }

        public List<BroadcastFormat> SpecialDealBroadcastTime { get; set; }
        public string SpecialEventUrl { get; set; }

        /// <summary>
        /// 優惠券每次播放的分鐘數;int;優惠券每次播放的分鐘數，撥完後輪下一個類型播放
        /// </summary>
        public int VourcherStoreBroadcastMinutes { get; set; }
        public string VourcherEventUrl { get; set; }


        public class BroadcastFormat
        {
            public string StartTimeFormat { get; set; }
            public string EndTimeFormat { get; set; }
        }
    }
    #endregion IDEAS各車站據點的資料

    #region enum

    public enum ApiIDEASInvoiceType
    {
        /// <summary>
        /// 雙聯式發票
        /// </summary>
        DuplexInvoice = 2,
        /// <summary>
        /// 三聯式發票
        /// </summary>
        TriplexInvoice = 3,
    }
    /// <summary>
    /// 付款作業回覆狀態
    /// </summary>
    public enum ApiIDEASPaymentReplyStatus
    {
        Error = 0,
        Success = 1,
        CreditCardError = 2,
        SoldOut = 3,
        ParameterError = 4,
        CouponCreateError = 5,
        PCashError = 6,
        SCashError = 7,
        BCashError = 8,
        DiscountCodeError =9
    }

    public enum ApiIDEASOrderStatus
    {
        /// <summary>
        /// 未付款
        /// </summary>
        Unpaid = 1,
        /// <summary>
        /// 憑證未使用（單一訂單還有未使用憑證歸於此類）
        /// </summary>
        NotUsed = 2,
        /// <summary>
        /// 憑證使用完畢
        /// </summary>
        AllUsed = 3,
        /// <summary>
        /// 宅配商品 
        /// </summary>
        ToHouse = 4,
        /// <summary>
        /// 申請退貨 
        /// </summary>
        InRefund = 5,
        /// <summary>
        /// 退貨完成 
        /// </summary>
        RefundComplete = 6,
        /// <summary>
        /// 已出貨
        /// </summary>
        GoodsShipped = 7,
        /// <summary>
        /// 已過鑑賞期
        /// </summary>
        GoodsExpired = 8,

    }

    public enum ApiIDEASCouponStatus
    {
        /// <summary>
        /// 未使用
        /// </summary>
        NotUsed = 1,
        /// <summary>
        /// 已使用
        /// </summary>
        Used = 2,
        /// <summary>
        /// 退貨處理中
        /// </summary>
        InRefund = 3,
        /// <summary>
        /// 退換完成
        /// </summary>
        RefundComplete = 4,
        /// <summary>
        /// 已過使用期限
        /// </summary>
        Expired = 5,
    }

    public enum ApiIDEASInvoiceRequestStatus
    {
        /// <summary>
        /// 申請成功
        /// </summary>
        Success = 1,
        /// <summary>
        /// 查無訂單或發票
        /// </summary>
        NoOrderOrInvoice = 2,
        /// <summary>
        /// 發票不為該訂單的資料
        /// </summary>
        DataMismatch = 3,
        /// <summary>
        /// 已經申請
        /// </summary>
        AlreadyRequest = 4,
    }

    /// <summary>
    /// 申請簡訊發送是否成功
    /// </summary>
    public enum ApiIDEASSendSMSReplyStatus
    {
        /// <summary>
        /// 成功發送
        /// </summary>
        Success = 1,
        /// <summary>
        /// 訂單不存在
        /// </summary>
        OrderNotExists = 2,
        /// <summary>
        /// 無憑證可發送(可能已退貨或接已核銷完畢)
        /// </summary>
        NoCouponCanSend = 3,
    }
    /// <summary>
    /// 發票開立狀況
    /// </summary>
    public enum ApiInvoiceOfferType
    {
        /// <summary>
        /// 未開立
        /// </summary>
        None = 1,
        /// <summary>
        /// 開立電子發票
        /// </summary>
        Create = 2,
        /// <summary>
        /// 已申請紙本
        /// </summary>
        ApplyPaper = 3,
    }
    #endregion enum
}

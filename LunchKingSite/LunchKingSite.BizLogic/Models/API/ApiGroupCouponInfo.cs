﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// 成套禮券用的資訊
    /// </summary>
    public class ApiGroupCouponInfo
    {
        /// <summary>
        /// 此成套禮券單組消費者實際購買的數量
        /// </summary>
        public int BoughtQuantity { set; get; }
        /// <summary>
        /// 此成套禮券單組贈送給消費者的數量
        /// </summary>
        public int FreeQuantity { set; get; }
        /// <summary>
        /// 單次購買的最大組數
        /// </summary>
        public int MaxQuantity { set; get; }

        public ApiGroupCouponInfo()
        {
            BoughtQuantity = 0;
            FreeQuantity = 0;
            MaxQuantity = 0;
        }
    }
}

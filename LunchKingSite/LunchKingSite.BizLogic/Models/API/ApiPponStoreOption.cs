﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// 供購買時，選取分店用，所以只需列出購買時需要的屬性。
    /// </summary>
    public class ApiPponStoreOption
    {
        /// <summary>
        /// 分店編號
        /// </summary>
        public Guid StoreGuid { get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        public int CityId { get; set; }
        /// <summary>
        /// 鄉鎮市區
        /// </summary>
        public int TownshipId { get; set; }
        /// <summary>
        /// 地址後續字串
        /// </summary>
        public string AddressDesc { get; set; }
        /// <summary>
        /// 完整地址字串
        /// </summary>
        public string CompleteAddress { get; set; }
        /// <summary>
        /// 分店名稱
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 分店分配到的商品總數
        /// </summary>
        public int TotalQuantity { get; set; }
        /// <summary>
        /// 分店商品已銷售總數
        /// </summary>
        public int OrderedQuantity { get; set; }
        /// <summary>
        /// 有限定數量
        /// </summary>
        public bool HaveLimit { get; set; }
        public ApiCoordinates Coordinates { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiComboDeal
    {
        /// <summary>
        /// 檔次代號
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 手機版頁面子檔次的說明顯示。
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 方案說明
        /// </summary>
        public string CouponDescription { get; set; }
        /// <summary>
        /// 已銷售數量-查詢時商品已銷售的數量
        /// </summary>
        public int SoldNum { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal ItemOrigPrice { get; set; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 是否顯示均價
        /// </summary>
        public bool IsShowAveragePrice { get; set; }
        /// <summary>
        /// 平均價
        /// </summary>
        public decimal AveragePrice { get; set; }
        /// <summary>
        /// 折數顯示用文字"5折"等，特殊檔次可能顯示”特惠”等
        /// </summary>
        public string DiscountString { get; set; }
        /// <summary>
        /// 結束販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealEndTime { get; set; }
        /// <summary>
        /// 是否已售完 true為售完
        /// </summary>
        public bool SoldOut { get; set; }
        /// <summary>
        /// 購物車
        /// </summary>
        public bool ShoppingCart { get; set; }
        /// <summary>
        /// 成套票券
        /// </summary>
        public bool IsGroupCoupon { set; get; }
        /// <summary>
        /// 品生活特別說明
        /// </summary>
        public string PiinlifeRightContent { set; get; }
        /// <summary>
        /// 是否提供超取服務
        /// </summary>
        public bool EnableShipByIsp { get; set; }
        /// <summary>
        /// 憑證屬性
        /// </summary>
        public int PponDeliveryType { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public decimal FreightAmount { get; set; }
    }
}

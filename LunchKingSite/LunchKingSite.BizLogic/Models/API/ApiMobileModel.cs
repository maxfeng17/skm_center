﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.API
{
    public class MobileEventAdModel
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string LinkUrl { get; set; }
    }
}

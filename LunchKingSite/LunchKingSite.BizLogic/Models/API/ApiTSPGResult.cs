﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// WebApi回覆前端呼叫時，乘載回傳資訊的類別
    /// </summary>
    public class ApiTSPGResult
    {
        private string _message;
        /// <summary>
        /// 回傳的結果
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 回傳資料主體
        /// </summary>
        public object Data { get; set; }
        /// <summary>
        /// 回傳RedirectUrl
        /// </summary>
        public string RedirectUrl { get; set; }
        /// <summary>
        /// 回傳的訊息
        /// </summary>
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
            }
        }


        public ApiTSPGResult()
        {
            Code = "00";
            Data = new object { };
            Message = string.Empty;
        }
    }
}

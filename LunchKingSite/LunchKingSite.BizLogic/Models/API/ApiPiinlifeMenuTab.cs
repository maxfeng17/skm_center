﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// 品生活單檔的Menu標題&內容
    /// </summary>
    public class ApiPiinlifeMenuTab
    {
        /// <summary>
        /// MenuTab 標題
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        ///  MenuTab 內容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// App顯示Content
        /// </summary>
        public string Url { get; set; }
    }
}

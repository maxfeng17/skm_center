﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// 舊版檔次明細
    /// </summary>
    public class ApiPponDeal
    {
        public ApiPponDeal()
        {
            IntroductionTags = new List<string>();
            Availabilities = new List<ApiPponStore>();
            DealTags = new List<int>();
            //預設為非全家
            IsFami = false;
            //預設不顯示兌換價
            ShowExchangePrice = false;
            ExchangePrice = 0;
            PiinlifeMenuTabs = new List<ApiPiinlifeMenuTab>();
        }

        /// <summary>
        /// 檔次代號
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 手機版頁面當作檔次名稱顯示。
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 該檔次所有輪播圖片
        /// </summary>
        public string[] ImagePaths { get; set; }
        /// <summary>
        /// For App圖片
        /// </summary>
        public string AppDealPic { get; set; }
        /// <summary>
        /// 好康名稱-黑標
        /// </summary>
        public string CouponEventName { get; set; }
        /// <summary>
        /// 好康名稱-橘標
        /// </summary>
        public string CouponEventTitle { get; set; }
        /// <summary>
        /// 好康權益說明
        /// </summary>
        public string Introduction { get; set; }
        /// <summary>
        /// 好康權益說明的標籤
        /// </summary>
        public List<string> IntroductionTags { get; set; }
        /// <summary>
        /// 更多權益說明
        /// </summary>
        public string Restrictions { get; set; }
        /// <summary>
        /// 詳細介紹
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 可用商家(好康哪裡找)
        /// </summary>
        public List<ApiPponStore> Availabilities { get; set; }
        /// <summary>
        /// 已銷售數量-查詢時商品已銷售的數量
        /// </summary>
        public int SoldNum { get; set; }
        /// <summary>
        /// 已銷售數量顯示值-已銷售數量*設定的倍數 所得到用於顯示的的數值
        /// </summary>
        public int SoldNumShow { get; set; }
        /// <summary>
        /// 銷售數量顯示的單位
        /// </summary>
        public string ShowUnit { get; set; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal OriPrice { get; set; }
        /// <summary>
        /// 顯示金額
        /// </summary>
        public decimal DisplayPrice { get; set; }
        /// <summary>
        /// 折數顯示用文字"5折"等，特殊檔次可能顯示”特惠”等
        /// </summary>
        public string DiscountString { get; set; }
        /// <summary>
        /// 優惠顯示方式
        /// </summary>
        public int DiscountDisplayType { get; set; }
        /// <summary>
        /// 結束販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealEndTime { get; set; }
        /// <summary>
        /// 是否已售完 true為售完
        /// </summary>
        public bool SoldOut { get; set; }
        /// <summary>
        /// 一姬說好康
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 是否為多檔次
        /// </summary>
        public bool MultiDeal { get; set; }
        /// <summary>
        /// 是否為購物車檔次
        /// </summary>
        public bool ShoppingCart { get; set; }
        /// <summary>
        /// 預設的檔次活動區域名稱
        /// </summary>
        public string DefaultActivityName { get; set; }
        /// <summary>
        /// 旅遊地點
        /// </summary>
        public string TravelPlace { get; set; }
        /// <summary>
        /// 檔次的ICON標示，如破千熱銷之類的ID值
        /// </summary>
        public List<int> DealTags { get; set; }
        /// <summary>
        /// 是否為全家檔次
        /// </summary>
        public bool IsFami { get; set; }
        /// <summary>
        /// 頻道類型
        /// </summary>
        public int BehaviorType { set; get; }
        /// <summary>
        /// 是否顯示兌換價
        /// </summary>
        public bool ShowExchangePrice { get; set; }
        /// <summary>
        /// 兌換價
        /// </summary>
        public decimal ExchangePrice { get; set; }
        /// <summary>
        /// 是否已經收藏
        /// </summary>
        public bool IsCollected { get; set; }
        /// <summary>
        /// 是否可使用折價券
        /// </summary>
        public bool DiscountCodeUsed { get; set; }
        /// <summary>
        /// 是否可用ATM付款
        /// </summary>
        public bool AtmAvailable { get; set; }
        /// <summary>
        /// 品生活的Menu標題&內容
        /// </summary>
        public List<ApiPiinlifeMenuTab> PiinlifeMenuTabs { get; set; }
        /// <summary>
        /// 品生活特別說明(無子母檔)
        /// </summary>
        public string PiinlifeRightContent { set; get; }
        /// <summary>
        /// 是否為體驗商品
        /// </summary>
        public bool? IsExperience { get; set; }
        /// <summary>
        /// 體驗商品收藏次數
        /// </summary>
        public string ExperienceDealCollectCount { set; get; }
        /// <summary>
        /// 已兌換人數
        /// </summary>
        public string ExchangeDealCount { set; get; }
        /// <summary>
        /// 優惠類型
        /// </summary>
        public int DiscountType { get; set; }
        /// <summary>
        /// 優惠內容
        /// </summary>
        public int Discount { get; set; }
        /// <summary>
        /// 開始販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealStartTime { get; set; }
        /// <summary>
        /// 相關標籤
        /// </summary>
        public List<string> RelatedTags { get; set; }
        /// <summary>
        /// 好康類型
        /// </summary>
        public int DeliveryType { get; set; }
        /// <summary>
        /// 運費
        /// </summary>
        public decimal FreightAmount { get; set; }
        /// <summary>
        /// 是否可超取
        /// </summary>
        public bool EnableShipByIsp { get; set; }
        /// <summary>
        /// 折後價
        /// </summary>
        public decimal? DiscountPrice { get; set; }
        /// <summary>
        /// 分類代號
        /// </summary>
        public List<int> Categories { get; set; }
    }

    public class ApiPponSimpleData
    {
        public string Bid { get; set; }
        public string ItemName { get; set; }
        public string StoreName { get; set; }
        public string StoreLocationLat { get; set; }
        public string StoreLocationLng { get; set; }
    }


    public class ApiPponDealBasic
    {
        /// <summary>
        /// 檔次基本資料
        /// </summary>
        public PponDealSynopsisBasic DealListInfo { get; set; }
        /// <summary>
        /// TOP Banner
        /// </summary>
        public string TopBanner { get; set; }
        /// <summary>
        /// 好康名稱-黑標 (後來以橘標EventTitle取代)
        /// </summary>
        public string CouponEventName { get; set; }
        /// <summary>
        /// 方案說明 (黑標 EventName)
        /// </summary>
        public string CouponDescription { get; set; }
        /// <summary>
        /// 好康權益說明
        /// </summary>
        public string Introduction { get; set; }
        /// <summary>
        /// 好康權益說明的標籤
        /// </summary>
        public List<string> IntroductionTags { get; set; }
        /// <summary>
        /// 詳細介紹v1 (優惠方案、詳細介紹、產品規格)
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 詳細介紹v2
        /// </summary>
        public string DetailDescription { get; set; }
        /// <summary>
        /// 產品規格
        /// </summary>
        public string ProductSpec { get; set; }
        /// <summary>
        /// 一姬說好康
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 是否為購物車檔次
        /// </summary>
        public bool IsShoppingCart { get; set; }
        /// <summary>
        /// 是否已經收藏
        /// </summary>
        public bool IsCollected { get; set; }
        /// <summary>
        /// 是否可使用折價券
        /// </summary>
        public bool CanUseDiscountCode { get; set; }
        /// <summary>
        /// 旅遊地點
        /// </summary>
        public string CityName { get; set; }
        /// <summary>
        /// 品生活的Menu標題&內容
        /// </summary>
        public List<ApiPiinlifeMenuTab> PiinlifeMenuTabs { get; set; }
        /// <summary>
        /// 品生活特別說明(無子母檔)
        /// </summary>
        public string PiinlifeRightContent { set; get; }
        /// <summary>
        /// 可用商家(好康哪裡找)
        /// </summary>
        public List<ApiPponStore> Availabilities { get; set; }
        /// <summary>
        /// 該檔次所有輪播圖片
        /// </summary>
        public string[] ImagePaths { get; set; }
        /// <summary>
        /// 檔次所擁有的策展
        /// </summary>
        public List<DealRelatedCuration> RelatedCurations { get; set; }
        /// <summary>
        /// 關聯檔次
        /// </summary>
        public List<RelatedDealEntity> RelatedDeals { get; set; }
        /// <summary>
        /// 相關標籤
        /// </summary>
        public List<string> RelatedTags { get; set; }

        public ApiPponDealBasic()
        {
            RelatedTags = new List<string>();
            RelatedCurations = new List<DealRelatedCuration>();
        }
    }

    public class RelatedDealEntity
    {
        public Guid Bid { get; set; }
        public string DealName { get; set; }
    }

    public class DealRelatedCuration
    {
        /// <summary>
        /// 策展類型
        /// </summary>
        public EventPromoEventType Type { get; set; }
        /// <summary>
        /// 策展編號
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 檔次內頁前往中繼頁圖片
        /// </summary>
        public string ToRelayPageImageUrl { get; set; }
        /// <summary>
        /// 中繼頁內的檔次相關策展圖
        /// </summary>
        public string RelatedCurationImageUrl { get; set; }
        /// <summary>
        /// 相關商品(檔次)數
        /// </summary>
        public int RelatedDealCount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// App裡切換地圖模式的商店資料
    /// </summary>
    public class ApiStoreMap
    {
        /// <summary>
        /// 檔次代號
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 檔次名稱
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 最低價格
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 經度座標
        /// </summary>
        public double Longitude { get; set; }
        /// <summary>
        /// 緯度座標
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 距離
        /// </summary>
        public double Distance { get; set; }

        public ApiStoreMap()
        {
            Price = 0;
        }
    }
}

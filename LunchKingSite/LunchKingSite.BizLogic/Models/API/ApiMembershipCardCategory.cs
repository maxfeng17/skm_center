﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiMembershipCardCategory
    {
        public ApiMembershipCardCategory()
        {
            SubList = new List<ApiMembershipCardCategory>();
        }
        /// <summary>
        /// 此節點代表的Category之ID
        /// </summary>
        [JsonProperty("Id")]
        public int CategoryId { get; set; }
        /// <summary>
        /// 此節點代表的Category之名稱
        /// </summary>
        [JsonProperty("Name")]
        public string CategoryName { get; set; }
        /// <summary>
        /// 此節點代表的Category之短名稱名稱
        /// </summary>
        public string ShortName { get; set; }
        /// <summary>
        /// 子分類
        /// </summary>
        public List<ApiMembershipCardCategory> SubList { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// 會員基本資料，供會員透過API修改用
    /// </summary>
    public class ApiMemberDetail
    {
        public ApiMemberDetail()
        {
            Address = new LkAddress();
        }


        /// <summary>
        /// 會員名
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// 會員姓
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// 會員地址
        /// </summary>
        public LkAddress Address { get; set; }
        /// <summary>
        /// 手機
        /// </summary>
        public string Mobile { get; set; }
    }

    /// <summary>
    /// 會員基本資料，供會員透過API修改用
    /// </summary>
    public class ApiMemberDetailV2 : ApiMemberDetail
    {
        /// <summary>
        /// 通訊Email
        /// </summary>
        public string UserEmail { get; set; }
        /// <summary>
        /// 會員的大頭照網址
        /// </summary>
        public string PicUrl { get; set; }
    }

    public class ApiMobileMemberRegisterInfo
    {
        /// <summary>
        /// 會員名
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// 會員姓
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// 通訊Email
        /// </summary>
        public string UserEmail { get; set; }
        /// <summary>
        /// 註冊時選擇的地區
        /// </summary>
        public int? CityId { get; set; }
    }


}

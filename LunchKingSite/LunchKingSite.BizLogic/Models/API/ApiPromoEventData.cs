﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiPromoEventData
    {

        public ApiPromoEventData()
        {
            CouponDeals = new List<PponDealSynopsis>();
            VourcherStores = new List<ApiVourcherStore>();
        }
        /// <summary>
        /// 此行銷活動的名稱,活動顯示的title
        /// </summary>
        public string EventTitle { get; set; }
        /// <summary>
        /// 活動編號,活動識別
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 活動開始時間
        /// </summary>
        public string StartDate { get; set; }
        /// <summary>
        /// 活動截止時間
        /// </summary>
        public string EndDate { get; set; }
        /// <summary>
        /// 狀態
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 檔次版型,檔次版型
        /// </summary>
        public EventPromoTemplateType TemplateType { get; set; }
        /// <summary>
        /// 團購券資料;物件陣列;若此活動無團購券資料，為空陣列
        /// </summary>
        public List<PponDealSynopsis> CouponDeals { get; set; }
        /// <summary>
        /// 優惠券資料;物件陣列;若此活動無優惠資料，為空陣列
        /// </summary>
        public List<ApiVourcherStore> VourcherStores { get; set; }
    }
}

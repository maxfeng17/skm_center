﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiCity
    {
        /// <summary>
        /// 城市編號
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 城市名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 是否為外島
        /// </summary>
        public bool Island { get; set; }
        /// <summary>
        /// 鄉鎮市區的列表
        /// </summary>
        public List<ApiTownship> TownshipsList { get; set; } 
    }

    public class ApiTownship
    {
        /// <summary>
        /// 城市編號
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 城市名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 是否為外島
        /// </summary>
        public bool Island { get; set; }
        /// <summary>
        /// 郵遞區號
        /// </summary>
        public string ZipCode { get; set; }
    }
}

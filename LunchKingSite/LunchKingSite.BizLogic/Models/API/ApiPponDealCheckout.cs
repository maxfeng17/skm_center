﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiPponDealCheckout
    {
        public ApiPponDealCheckout()
        {
            DealStage = PponDealStage.NotExist;
            UserPcash = 0;
            MemberAddress = new LkAddress();
            MemberDeliveries = new List<ApiMemberDelivery>();
            OptionCategories = new List<ApiPponOptionCategory>();
            Stores = new List<ApiPponStoreOption>();
            FreightList = new List<ApiFreight>();
            DiscountCodes = new List<ApiDiscountCode>();
            IsBindingMasterPass = false;
        }

        #region 檔次相關資料
        /// <summary>
        /// 檔次Guid
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 檔次目前狀態(開賣、售完、達門檻等)
        /// </summary>
        public PponDealStage DealStage { get; set; }
        /// <summary>
        /// 是否不配送外島 true則不配送，false則為有配送
        /// </summary>
        public bool NotDeliveryIslands { get; set; }
        /// <summary>
        /// 階梯運費的資料
        /// </summary>
        public List<ApiFreight> FreightList { get; set; }
        /// <summary>
        /// 多分店-採取階層選取模式(先選地區在選分店) 若為true表示採用階層選取方式
        /// </summary>
        public bool MultipleBranch { get; set; }
        /// <summary>
        /// 此次交易使用者可選取最大數量
        /// </summary>
        public int DealUserCanBuy { get; set; }
        /// <summary>
        /// 無法購買時顯示的訊息
        /// </summary>
        public string CanNotBuyMessage { get; set; }
        /// <summary>
        /// 檔次所屬館別
        /// </summary>
        public DepartmentTypes PponDepartment { get; set; }
        /// <summary>
        /// 取貨類型 到店或宅配
        /// </summary>
        public DeliveryType PponDeliveryType { get; set; }
        /// <summary>
        /// 宅配取貨選項與購買組數上限
        /// </summary>
        public List<DeliveryOption> DeliveryOptions { get; set; }
        /// <summary>
        /// 不須開立發票
        /// </summary>
        public bool NoInvoiceCreate { get; set; }
        /// <summary>
        /// 商品選項群組
        /// </summary>
        public List<ApiPponOptionCategory> OptionCategories { get; set; }
        /// <summary>
        /// 分店選項
        /// </summary>
        public List<ApiPponStoreOption> Stores { get; set; }
        /// <summary>
        /// 是否為購物車類型的檔次
        /// </summary>
        public bool ShoppingCart { get; set; }
        /// <summary>
        /// 成套商品每套數量(目前為購物車使用)
        /// </summary>
        public int ComboPackCount { get; set; }
        /// <summary>
        /// 是否為全家活動
        /// </summary>
        public bool IsFami { get; set; }
        /// <summary>
        /// 是否為公益檔
        /// </summary>
        public bool IsKindDeal { get; set; }
        /// <summary>
        /// 是否ATM付款
        /// </summary>
        public bool ATMAvailable { get; set; }

        #endregion 檔次相關資料

        #region 系統及購買流程相關
        /// <summary>
        /// 是否允許使用 17LIFE現金卷
        /// </summary>
        public bool DiscountCodeUsed { get; set; }
        /// <summary>
        /// 目前使用的發票設定系統版本
        /// </summary>
        public InvoiceVersion InvoiceVersion { get; set; }
        #endregion 系統及購買流程相關

        #region 會員相關
        /// <summary>
        /// 名
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// 姓
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// 行動電話
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 通訊Email
        /// </summary>
        public string UserEmail { get; set; }
        /// <summary>
        /// 購買人地址資訊
        /// </summary>
        public LkAddress MemberAddress { get; set; }
        /// <summary>
        /// 會員擁有的PCash
        /// </summary>
        public decimal UserPcash { get; set; }
        /// <summary>
        /// 會員擁有的紅利額度(已依據1:10換算為台幣幣值)
        /// </summary>
        public double UserBcash { get; set; }
        /// <summary>
        /// 會員擁有17LIFE購物金
        /// </summary>
        public decimal UserScash { get; set; }
        /// <summary>
        /// 會員最後收件資訊
        /// </summary>
        public LastMemberDelivery LastMemberDelivery { get; set; }
        /// <summary>
        /// 會員聯絡方式列表
        /// </summary>
        public List<ApiMemberDelivery> MemberDeliveries { get; set; }
        /// <summary>
        /// 使用者擁有的折價券列表
        /// </summary>
        public List<ApiDiscountCode> DiscountCodes { get; set; }
        /// <summary>
        /// 會員信用卡列表
        /// </summary>
        public List<MemberCreditCardInfo> MemberCreditCardList { get; set; }
        
        #endregion 會員相關

        #region 成套商品

        /// <summary>
        /// 是否為成套販售
        /// </summary>
        public bool IsGroupCoupon { get; set; }

        public ApiGroupCouponInfo GroupCouponInfo { set; get; }

        #endregion

        #region MasterPass
        /// <summary>
        /// Binding MasterPass
        /// </summary>
        public bool IsBindingMasterPass { get; set; }

        #endregion
    }

    /// <summary>
    /// 取貨選項
    /// </summary>
    public class DeliveryOption
    {
        /// <summary>
        /// 提供取貨方式
        /// </summary>
        public ProductDeliveryType ProductDeliveryType { get; set; }
        /// <summary>
        /// 結帳時的推薦提示
        /// </summary>
        public string PromoTips { get; set; }
        /// <summary>
        /// 取貨數量上限
        /// </summary>
        public int MaxQuantity { get; set; }
    }
}

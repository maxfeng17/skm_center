﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiPromoEvent
    {
        public ApiPromoEvent()
        {
            SubEventList = new List<ApiSubPromoEvent>();
        }

        /// <summary>
        /// 此行銷活動的名稱;string;活動顯示的title
        /// </summary>
        public string EventTitle { get; set; }
        /// <summary>
        /// 活動編號;int;活動識別
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 活動個promo baner顯示的圖片;字串;圖片之url路徑
        /// </summary>
        public string PromoBannerImage { get; set; }
        /// <summary>
        /// 活動於event頁呈現的圖片
        /// </summary>
        public string PromoImage { get; set; }
        /// <summary>
        /// 活動說明;字串;html內容
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 活動說明圖片;字串;圖片的url路徑
        /// </summary>
        public string DescriptionImage { get; set; } 
        /// <summary>
        /// 活動開始時間
        /// </summary>
        public string StartDate { get; set; }
        /// <summary>
        /// 活動截止時間
        /// </summary>
        public string EndDate { get; set; }
        /// <summary>
        /// 是否需要發票輸入;布林;消費者是否需要輸入發票資料，主要為商圈週年慶特製
        /// </summary>
        public bool NeedInvInput { get; set; }
        /// <summary>
        /// 檔次版型;int;檔次版型，請參考參數說明
        /// </summary>
        public EventPromoTemplateType TemplateType { get; set; }
        /// <summary>
        /// 背景圖片的url路徑;字串;顯示於商圈promo頁面的背景圖片
        /// </summary>
        public string BackgroundImage { get; set; }
        /// <summary>
        /// 經緯度;物件;標示地圖檢視商圈時，預設的定位點位置
        /// </summary>
        public ApiCoordinates Coordinates { get; set; }
        /// <summary>
        /// 子活動列表
        /// </summary>
        public List<ApiSubPromoEvent> SubEventList { get; set; }
        /// <summary>
        /// 活動網址
        /// </summary>
        public string EventUrl { get; set; }
        /// <summary>
        /// EventType
        /// </summary>
        public int EventType { get; set; }
    }

    public class ApiSubPromoEvent
    {
        /// <summary>
        /// 商圈名稱;字串;顯示供使用者識別
        /// </summary>
        public string EventTitle { get; set; }
        /// <summary>
        /// 活動編號;int;活動識別
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 背景圖片的url路徑;字串;顯示於商圈promo頁面的背景圖片
        /// </summary>
        public string BackgroundImage { get; set; }
        /// <summary>
        /// 經緯度;物件;標示地圖檢視商圈時，預設的定位點位置
        /// </summary>
        public ApiCoordinates Coordinates { get; set; }
    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using LunchKingSite.Core;
using Newtonsoft.Json;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace LunchKingSite.BizLogic.Model.API
{
    public class HotSearchKeyModel
    {
        public int KeyId { get; set; }
        public string KeyWord { get; set; }
    }

    public class AppSearchResult
    {
        public bool HasResult { get; set; }
        public int ResultCount { get; set; }
        public List<PponDealSynopsis> DealList { get; set; }
    }


    /// <summary>
    /// API分頁用傳入參數
    /// </summary>
    public class ApiPagedOption
    {
        const int DefaultPageSize = 50;
        /// <summary>
        /// GetAll 為 true 時 StartIndex, Length 無作用
        /// </summary>
        public bool GetAll { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int StartIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? PageSize { get; set; }

        public List<T> GetPagedDataList<T>(List<T> dataList)
        {
            if (this.GetAll)
            {
                return dataList;
            }
            int pageSize = PageSize ?? DefaultPageSize;
            return dataList.Skip(StartIndex).Take(pageSize).ToList();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ApiPagedEventPromoDeaList : ApiPagedList<ApiEventPromoDeal>
    {
        /// <summary>
        /// 
        /// </summary>
        public List<ApiEventPromoDeal> DealList
        {
            get { return base.DataList; }
            set { base.DataList = value; }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ApiPagedList<T>
    {
        /// <summary>
        /// 
        /// </summary>
        public ApiPagedList()
        {
            DataList = new List<T>();
        }
        /// <summary>
        /// 
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        protected List<T> DataList { get; set; }
    }

    #region Api promo models

    /// <summary>
    /// 舊版商品主題活動、策展1.0、推播訊息中心GetPokeball使用
    /// </summary>
    public class ApiEventPromo
    {
        /// <summary>
        /// 活動類別
        /// </summary>
        public EventPromoEventType Type { get; set; }
        /// <summary>
        /// EventId 或 BrandId
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 活動名稱
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 頻道Banner出現位置
        /// </summary>
        [JsonIgnore]
        public int BannerType { get; set; }
        /// <summary>
        /// 頻道Banner(搭配BannerType)
        /// </summary>
        [JsonIgnore]
        public string ChannelImageUrl { get; set; }
        /// <summary>
        /// 首頁Banner
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// M版主圖Banner
        /// </summary>
        public string MobileMainPicUrl { get; set; }
        /// <summary>
        /// 分享連結
        /// </summary>
        [JsonIgnore]
        public string ShareUrl { get; set; }
        /// <summary>
        /// 活動描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 總檔次數量
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 分類
        /// </summary>
        [JsonConverter(typeof(ApiEventPromoCategoryListJsonConverter))]
        public List<ApiEventPromoCategory> Categories { get; set; }
        /// <summary>
        /// 活動開始時間
        /// </summary>
        [JsonIgnore]
        public DateTime StartDate { get; set; }
        /// <summary>
        /// 活動結束時間
        /// </summary>
        [JsonIgnore]
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 是否啟用
        /// </summary>
        [JsonIgnore]
        public bool Status { get; set; }
        /// <summary>
        /// 折價券
        /// </summary>
        [JsonIgnore]
        public List<int> DiscountList { get; set; }
        /// <summary>
        /// 檔次內頁領取策展折價券圖片
        /// </summary>
        [JsonIgnore]
        public string RelayImageUrl { get; set; }
    }

    public class ApiEventPromoCategory
    {
        /// <summary>
        /// 分類key
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 分類名稱
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 子類別
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ApiEventPromoCategoryListJsonConverter))]
        public List<ApiEventPromoCategory> SubCategories { get; set; }
        /// <summary>
        /// 分類檔次 (api呼叫時，不顯示此欄位，但仍存在於快取中)
        /// </summary>
        [JsonIgnore]
        public List<ApiEventPromoDeal> DealList { get; set; }

        [JsonIgnore]
        public int Seq { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", this.Key, this.Title);
        }
    }

    public class ApiEventPromoCategoryListJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            List<ApiEventPromoCategory> categories = value as List<ApiEventPromoCategory>;

            writer.WriteStartArray();
            foreach (ApiEventPromoCategory category in categories)
            {
                if (string.IsNullOrEmpty(category.Key))
                {
                    continue;
                }
                serializer.Serialize(writer, category);
            }
            writer.WriteEndArray();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(List<ApiEventPromoCategory>);
        }
    }

    /// <summary>
    /// 策展檔次
    /// </summary>
    public class ApiEventPromoDeal
    {
        public Guid Bid { get; set; }
        public string DealName { get; set; }
        public decimal Price { get; set; }
        public decimal OriginalPrice { get; set; }

        public int Seq { get; set; }
        public string ImagePath { get; set; }
        public string SquareImagePath { get; set; }
        public decimal? ExchangePrice { get; set; }
        public string DealStartTime { get; set; }
        public string DealEndTime { get; set; }
        public int SoldNum { get; set; }

        public string ShowUnit { get; set; }
        public bool SoldOut { get; set; }
        public string TravelPlace { get; set; }

        public decimal DisplayPrice { get; set; }
        public string DiscountString { get; set; }
        public int DiscountDisplayType { get; set; }

        public bool IsMultiDeal { get; set; }
        public int BehaviorType { get; set; }
        public List<int> DealTags { get; set; }
        public List<int> Categories { get; set; }

        public DealEvaluate DealEvaluate { get; set; }
        public decimal? DiscountPrice { get; set; }
        public int DeliveryType { get; set; }
    }

    /// <summary>
    /// 新版商品主題活動、策展1.0、2.0共用
    /// </summary>
    public class ApiCurationInfo
    {
        /// <summary>
        /// 活動類別
        /// </summary>
        public EventPromoEventType Type { get; set; }
        /// <summary>
        /// EventId 或 BrandId
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 活動名稱
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// BannerUrl
        /// </summary>
        public string BannerImageUrl { get; set; }
        /// <summary>
        /// 列表圖片Url
        /// </summary>
        public string ContentImageUrl { get; set; }
        /// <summary>
        /// APP策展圖片顯示位置
        /// </summary>
        public int BannerType { get; set; }
        /// <summary>
        /// 分享連結
        /// </summary>
        public string ShareUrl { get; set; }
        /// <summary>
        /// 活動描述
        /// </summary>
        public string EventDescription { get; set; }
        /// <summary>
        /// 總檔次數量
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 是否有折價券活動
        /// </summary>
        public bool HasAvailableDiscount { get; set; }
        /// <summary>
        /// 中繼圖片
        /// </summary>
        public string RelayImageUrl { get; set; }
        /// <summary>
        /// 分類
        /// </summary>
        [JsonConverter(typeof(ApiEventPromoCategoryListJsonConverter))]
        public List<ApiEventPromoCategory> Categories { get; set; }
        /// <summary>
        /// 活動結束時間
        /// </summary>
        [JsonIgnore]
        public DateTime EndDate { get; set; }
    }

    #region 策展列表

    /// <summary>
    /// 主題策展列表
    /// </summary>
    public class CurationsByTheme
    {
        /// <summary>
        /// 主題編號
        /// </summary>
        public int ThemeMainId { get; set; }
        /// <summary>
        /// 主視覺圖片
        /// </summary>
        public string MainImageUrl { get; set; }
        /// <summary>
        /// M版主視覺圖片
        /// </summary>
        public string MobileMainImageUrl { get; set; }
        /// <summary>
        /// 今日策展列表
        /// </summary>
        public OneDayCuration TodayCurations { get; set; }
        /// <summary>
        /// 全部策展列表
        /// </summary>
        public List<OneDayCuration> AllCurations { get; set; }
    }

    /// <summary>
    /// 主題策展列表
    /// </summary>
    public class CurationEntryInfo
    {
        /// <summary>
        /// 主視覺圖片
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 活動連結
        /// </summary>
        public string LinkUrl { get; set; }
        /// <summary>
        /// M版主視覺圖片
        /// </summary>
        public bool IsOpen { get; set; }
        /// <summary>
        /// 右側小圓圖
        /// </summary>
        public string IconImageUrl { get; set; }
    }

    /// <summary>
    /// 置頂策展列表
    /// </summary>
    public class OneDayCuration
    {
        /// <summary>
        /// 活動是否啟動
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// 活動日
        /// </summary>
        public DateTime EventDate { get; set; }
        /// <summary>
        /// Group名稱
        /// </summary>
        public string GroupName { get; set; }
        /// <summary>
        /// 是否置頂策展
        /// </summary>
        public bool IsTopGroup { get; set; }
        /// <summary>
        /// 活動日策展列表
        /// </summary>
        public List<ThemeCurationInfo> Curations { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public OneDayCuration()
        {
            Curations = new List<ThemeCurationInfo>();
        }
    }

    /// <summary>
    /// 主題策展內容
    /// </summary>
    public class ThemeCurationInfo
    {
        /// <summary>
        /// 活動類別
        /// </summary>
        public EventPromoEventType Type { get; set; }
        /// <summary>
        /// EventId 或 BrandId
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// ImageUrl
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 策展前台是否顯示敬請期待
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// 策展連結
        /// </summary>
        public string CurationLink { get; set; }
        /// <summary>
        /// 是否顯示領取折價券Button (暫時不接此欄位，目前決定直接壓在圖上，未來不確是否會改變)
        /// </summary>
        [JsonIgnore]
        public bool IsShowGetDiscountBtn { get; set; }
        /// <summary>
        /// 折價券壓圖描述 (暫時不接此欄位，目前決定直接壓在圖上，未來不確是否會改變)
        /// 例: "折價券限量500組"
        /// </summary>
        [JsonIgnore]
        public string DiscountDescription { get; set; }
        /// <summary>
        /// 策展是否在Web顯示
        /// </summary>
        [JsonIgnore]
        public bool ShowInWeb { get; set; }
        /// <summary>
        /// 策展是否在App顯示
        /// </summary>
        [JsonIgnore]
        public bool ShowInApp { get; set; }
    }

    #endregion

    public enum ApiCurationInfoUsage
    {
        MainPage,
        Channel,
        SingleEvent,
    }

    public class ApiEventNews
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string LinkUrl { get; set; }
        public int QuietSeconds { get; set; }
        public int AutoCoseSeconds { get; set; }
    }

    #endregion
}

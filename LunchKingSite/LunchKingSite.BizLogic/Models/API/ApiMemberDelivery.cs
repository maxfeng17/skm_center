﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiMemberDelivery
    {
        public ApiMemberDelivery()
        {
            Address = new LkAddress();
            AddressAlias = string.Empty;
            Mobile = string.Empty;
        }
        /// <summary>
        /// 聯絡方式名稱
        /// </summary>
        public string AddressAlias { get; set; }
        /// <summary>
        /// 收件資料ID
        /// </summary>
        public int DeliveryId { get; set; }
        /// <summary>
        /// 收件人名稱
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// 連絡電話(手機)
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 一般宅配地址
        /// </summary>
        public LkAddress Address { get; set; }
    }

    /// <summary>
    /// 最後收件資訊
    /// </summary>
    public class LastMemberDelivery
    {
        /// <summary>
        /// 取貨方式
        /// </summary>
        public int ProductDeliveryType { get; set; }
        /// <summary>
        /// 收件資料ID
        /// </summary>
        public int? DeliveryId { get; set; }
        /// <summary>
        /// 全家店舖資訊
        /// </summary>
        public PickupStore FamilyStoreInfo { get; set; }
        /// <summary>
        /// 7-11店舖資訊
        /// </summary>
        public PickupStore SevenStoreInfo { get; set; }

        public LastMemberDelivery()
        {
            this.ProductDeliveryType = (int)Core.ProductDeliveryType.Normal;
            this.DeliveryId = -1;
            this.FamilyStoreInfo = new PickupStore();
            this.SevenStoreInfo = new PickupStore();
        }
    }
}


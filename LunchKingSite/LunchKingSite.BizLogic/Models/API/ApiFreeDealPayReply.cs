﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{    
    public class ApiFreeDealPayReply
    {
        public int CouponId { get; set; }
        public string CouponCode { get; set; }
        /// <summary>
        /// 三段式條碼的條碼編號物件，只有當CouponType為ThreeStageBarcode時，才會填入資料
        /// </summary>
        public ThreeStageBarcode ThreeStageCode {get; set; }
        /// <summary>
        /// 結帳完成業提供的活動連結網址
        /// </summary>
        public string PromoUrl { get; set; }
        /// <summary>
        /// 結帳完成頁顯示的圖片
        /// </summary>
        public string PromoImage { get; set; }
        /// <summary>
        /// 是否要顯示憑證於結帳完成頁面
        /// </summary>
        public bool ShowCoupon { get; set; }
        /// <summary>
        /// 是否為全家檔次
        /// </summary>
        public bool IsFami { get; set; }

        #region 新光專用
        
        /// <summary>
        /// 是否為新光檔次
        /// </summary>
        public bool IsSkm { get; set; }
        /// <summary>
        /// 新光 QRCode 字串
        /// </summary>
        public string SkmQrCode { get; set; }
        /// <summary>
        /// 新光 QRCode 到期時間
        /// </summary>
        public string SkmExpireTime { get; set; }
        /// <summary>
        /// 產品代碼
        /// </summary>
        public string ProductCode { get; set; }
        /// <summary>
        /// 新光可用櫃位
        /// </summary>
        public List<SkmPponStore> SkmAvailabilities { get; set; }
        /// <summary>
        /// 特殊產品代碼(for 法雅客等非新光自營商品的的合作廠商)
        /// </summary>
        public string SpecialItemNo { get; set; }

        #endregion

        /// <summary>
        /// 憑證是否可以下載後列印使用
        /// </summary>
        public bool IsCouponDownload { get; set; }
        /// <summary>
        /// 憑證類型，包括17life憑證、廠商提供序號、全家pin code、全家bar code等
        /// </summary>
        public DealCouponType CouponType { get; set; }
        public bool IsATM { get; set; }
        public ApiOrderAtmData AtmData { get; set; }
        public Guid OrderGuid { get; set; }
        public string SequenceNumber { get; set; }
        /// <summary>
        /// 17life 行動支付未申辦行銷文案
        /// </summary>
        public string UnUsed17PayMsg { get; set; }
        /// <summary>
        /// 17life 行動支付已申辦行銷文案
        /// </summary>
        public string Used17PayMsg { get; set; }
        public ApiFreeDealPayReply()
        {
            //預設一定要顯示COUPON
            ShowCoupon = true;
            ThreeStageCode = new ThreeStageBarcode();
            OrderGuid = Guid.Empty;
            SequenceNumber = string.Empty;
            AtmData = new ApiOrderAtmData
            {
                //目前只有合作這一家
                //未來有需要在刪掉這個
                BankName = "中國信託商業銀行",
                BankCode = "822",
            };
        }
    }
}

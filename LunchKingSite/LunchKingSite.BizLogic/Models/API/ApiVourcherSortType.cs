﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiVourcherSortType
    {
        public string Text { get; set; }
        public int Value { get; set; }
        public bool NeedLocate { get; set; }
    }
}

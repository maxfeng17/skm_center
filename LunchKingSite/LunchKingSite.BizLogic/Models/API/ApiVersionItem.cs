﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiVersionItem
    {
        public string Item { get; private set; }
        public string Version { get; private set; }

        public ApiVersionItem (string item , string version)
        {
            Item = item;
            Version = version;
        }
    }
}

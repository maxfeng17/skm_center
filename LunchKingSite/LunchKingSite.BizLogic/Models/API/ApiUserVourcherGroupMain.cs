﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// 用來提供webservice回傳各類別優惠券數量
    /// </summary>
    public class ApiUserVourcherGroupMain
    {
        private readonly VourcherGroupMainType _type;
        /// <summary>
        /// 類別型態
        /// </summary>
        public VourcherGroupMainType Type { get { return _type; } }
        /// <summary>
        /// 型態的中文說明
        /// </summary>
        public string TypeDesc { get { return Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager,_type );}}
        /// <summary>
        /// 優惠券數量
        /// </summary>
        public int Count { get; set; }


        public ApiUserVourcherGroupMain(VourcherGroupMainType mainType , int count)
        {
            _type = mainType;
            Count = count;
        }
    }
}

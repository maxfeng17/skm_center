﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiVourcherEventStore : ApiPponStore
    {
        /// <summary>
        /// 適用店家ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 距離顯示字串
        /// </summary>
        public string DistanceDesc { get; set; }
        /// <summary>
        /// 距離(單位公尺)
        /// </summary>
        public double Distance { get; set; }
    }

    //for toyota today vourcher 串接
    [Serializable]
    public class ApiToyotaVourcherEventStore
    {
        /// <summary>
        /// 適用店家ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 分店唯一識別碼
        /// </summary>
        public Guid StoreGuid { get; set; }

        /// <summary>
        /// 分店名稱
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 電話
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 營業時間
        /// </summary>
        public string OpenTime { get; set; }

        /// <summary>
        /// 公休日
        /// </summary>
        public string CloseDate { get; set; }

        /// <summary>
        /// 緯度座標
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// 經度座標
        /// </summary>
        public double Longitude { get; set; }

        
        /// <summary>
        /// 備註
        /// </summary>
        public string Remarks { get; set; }

    }

}

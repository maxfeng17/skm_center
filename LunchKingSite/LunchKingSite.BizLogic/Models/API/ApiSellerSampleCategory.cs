﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiSellerSampleCategory
    {
        /// <summary>
        /// 選項名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 選項代表數值
        /// </summary>
        public int Value { get; set; }
        /// <summary>
        /// 點名稱
        /// </summary>
        public string ShortName { get; set; }
        /// <summary>
        /// 此分類優惠券數量
        /// </summary>
        public int Count { get; set; }
    }
}

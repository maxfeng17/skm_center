﻿using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiPCP
    { }

    public class MembershipCardGroupListQueryModel
    {
        public string UserName { get; set; }
        public int TownshipId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int OrderBy { get; set; }
        public int GroupId { get; set; }
        public int RegionId { get; set; }
        public int Category { get; set; }
        public int AppImageSize { get; set; }
        public bool GetAll { get; set; }
        public int StartIndex { get; set; }

        public MembershipCardGroupListQueryModel()
        {

        }

        public MembershipCardGroupListQueryModel(int regionId, int category, string userName, int orderBy, double latitude, double longitude, int imageSize)
            : this(userName, latitude, longitude, imageSize)
        {
            this.UserName = userName;
            this.RegionId = regionId;
            this.Category = category;
            this.OrderBy = orderBy;
        }

        public MembershipCardGroupListQueryModel(string userName, double latitude, double longitude, int imageSize)
            : this()
        {
            this.UserName = userName;
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.AppImageSize = imageSize;
        }

        public MembershipCardGroupListQueryModel(int groupId, string userName, double latitude, double longitude, int imageSize)
            : this(userName, latitude, longitude, imageSize)
        {
            this.GroupId = groupId;
        }

        public MembershipCardGroupListQueryModel(int regionId, int category, string userName,
            int orderBy, double latitude, double longitude, int imageSize, bool getAll, int startIndex)
            : this(userName, latitude, longitude, imageSize)
        {
            this.StartIndex = startIndex;
            this.GetAll = getAll;
            this.RegionId = regionId;
            this.Category = category;
            this.OrderBy = orderBy;
        }

        public MembershipCardGroupListQueryModel(int regionId, string userName,
            int orderBy, double latitude, double longitude, int imageSize) : this(userName, latitude, longitude, imageSize)
        {
            this.RegionId = regionId;
            this.OrderBy = orderBy;
        }

        public MembershipCardGroupOrderby GetOrderByEnum
        {
            get
            {
                return (MembershipCardGroupOrderby)OrderBy;
            }
        }
        public AppImageSize GetAppImageSizeEnum
        {
            get
            {
                return (AppImageSize)AppImageSize;
            }
        }

        public bool IsValid()
        {
            if (!Enum.IsDefined(typeof(MembershipCardGroupOrderby), OrderBy))
            {
                return false;
            }

            if (!Enum.IsDefined(typeof(AppImageSize), AppImageSize))
            {
                return false;
            }

            return true;
        }
    }

    /// <summary>
    /// 新版熟客頁
    /// </summary>
    public class MembershipCardDetail
    {
        public MembershipCardDetail()
        {
            SellerIntro = new List<string>();
            SellerServiceImgPath = new List<string>();
            SellerServiceImgThumbnailPath = new List<string>();
            SellerSrndgImgPath = new List<string>();
            SellerSrndgThumbnailPath = new List<string>();
            SellerImagePath = new List<string>();
            MarqueePrmo = new List<string>();
            PointActiveContent = new PointActiveContent();
            DepositActiveContent = new DepositActiveContent();
            Remark = new List<string>();
        }

        public int GroupId { get; set; }
        public string SellerName { get; set; }
        public List<string> SellerImagePath { get; set; }
        public string SellerFullCardPath { get; set; }
        public string SellerFullCardThumbnailPath { get; set; }
        public List<string> SellerIntro { get; set; }
        public List<string> SellerServiceImgPath { get; set; }
        public List<string> SellerServiceImgThumbnailPath { get; set; }
        public List<string> SellerSrndgImgPath { get; set; }
        public List<string> SellerSrndgThumbnailPath { get; set; }
        public List<string> MarqueePrmo { get; set; }
        public string OfferDescription { get; set; }
        public string IconImagePath { get; set; }
        public string BackgroundColor { get; set; }
        public string BackgroundImagePath { get; set; }
        public bool IsApply { get; set; }
        public string CardValidDate { get; set; }
        public string LastModifyTime { get; set; }
        public bool IsUseDeposit { get; set; }      //是否設定寄杯服務
        public bool IsUsePoint { get; set; }        //是否設定集點服務
        public bool IsUseOffer { get; set; }        //是否設定熟客服務
        public bool IsVipOn { get; set; }           //是否開啟熟客服務
        public bool IsPointOn { get; set; }         //是否開啟集點服務
        public bool IsDepositOn { get; set; }       //是否開啟寄杯服務
        public int UserPoint { get; set; }          //user 點數
        public int UserDeposit { get; set; }        //user 寄杯數量
        public DepositActiveContent DepositActiveContent { get; set; }
        public PointActiveContent PointActiveContent { get; set; }
        public int CurrentLevel { get; set; }
        public string AvailableService { get; set; }
        public List<string> Remark { get; set; }
        /// <summary>
        /// user登入取目前等級卡片資訊，未登入預設Level1
        /// </summary>
        public CardDetail Card { get; set; }
        /// <summary>
        /// 適用店家
        /// </summary>
        public List<StoreDetail> Stores { get; set; }
        /// <summary>
        /// 相關檔次
        /// </summary>
        public List<PponDealSynopsis> DealList { get; set; }
    }


    public class PointActiveContent
    {
        public PointActiveContent()
        {
            ExchangeRule = new List<string>();
            Remark = new List<string>();
        }

        public string CollectRule { get; set; }//集點rule
        public List<string> ExchangeRule { get; set; } //兌換rule
        public List<string> Remark { get; set; }
    }

    public class DepositActiveContent
    {
        public DepositActiveContent()
        {
            Active = new List<string>();
            Remark = new List<string>();
        }

        public List<string> Active { get; set; }
        public List<string> Remark { get; set; }
    }

    public class MembershipCardOfferModel
    {
        public MembershipCardOfferModel()
        {
            Remark = new List<string>();
        }
        public int OrderCount { get; set; }
        public double AmountTotal { get; set; }
        public int OrderNeeded { get; set; }
        public double AmountNeeded { get; set; }
        public int ConditionalLogic { get; set; }
        public int CurrentLevel { get; set; }
        public string CardValidDate { get; set; }
        public List<string> Remark { get; set; }
        public List<CardDetail> Cards { get; set; }
    }

    public class MembershipCardPointModel
    {
        public MembershipCardPointModel()
        {
            PointActiveContent = new PointActiveContent();
            TransHistory = new List<TransData>();
        }
        public bool IsNew { get; set; }
        public int TotalPoint { get; set; }
        public List<TransData> TransHistory { get; set; }
        public PointActiveContent PointActiveContent { get; set; }
    }

    public class MembershipCardDepositModel
    {
        public MembershipCardDepositModel()
        {
            DepositActiveContent = new DepositActiveContent();
            DepositList = new List<DepositDetail>();
        }
        public int TotalDeposit { get; set; }         
        public List<DepositDetail> DepositList { get; set; }
        public DepositActiveContent DepositActiveContent { get; set; }
    }


    public class DepositDetail
    {
        public DepositDetail()
        {
            TransHistory = new List<TransData>();
        }
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public int TotalAmount { get; set; }
        public int RemainAmount { get; set; }
        public string CreatedDate { get; set; }
        public bool IsNew { get; set; }
        public List<TransData> TransHistory { get; set; }
    }

    public class TransData
    {
        /// <summary>
        /// 購買寄杯:0 使用寄杯:1
        /// 集點:0 兌點:1
        /// 更正數量 : 2
        /// </summary>
        public int Type { get; set; }
        public string TransItem { get; set; }
        public int TransAmount { get; set; }
        public string TransDate { get; set; }
        public string TransDesc { get; set; }
        public string TransStoreName { get; set; }
        public int TransPrice { get; set; }

    }

    public class MembershipCardGroupListResultModel
    {
        //Setting default value. 
        public MembershipCardGroupListResultModel()
        {
            SellerName = "";
            SellerImagePath = "";
            SellerFullCardPath = "";
            SellerCardType = 0;
            SellerIntro = new string[] { };
            SellerServiceImgPath = new string[] { };
            SellerServiceImgThumbnailPath = new string[] { };
            SellerSrndgImgPath = new string[] { };
            SellerSrndgThumbnailPath = new string[] { };
            OfferDescription = "";
            IconImagePath = "";
            BackgroundColor = "";
            BackgroundImagePath = "";
            StoreCount = 0;
            IsApply = false;
            UserCardId = 0;
            HotPoint = 0;
            ReleaseTime = "0001/1/1 00:00:00";
            CompanyName = "";
            Email = "";
            Phone = "";
            Stores = new List<StoreDetailReduced> { };

        }

        [BsonId]
        public int GroupId { get; set; }
        public string SellerName { get; set; }
        public string SellerImagePath { get; set; }
        public string SellerFullCardPath { get; set; }
        public string SellerFullCardThumbnailPath { get; set; }
        public int SellerCardType { get; set; }
        public string[] SellerIntro { get; set; }
        public string[] SellerServiceImgPath { get; set; }
        public string[] SellerServiceImgThumbnailPath { get; set; }
        public string[] SellerSrndgImgPath { get; set; }
        public string[] SellerSrndgThumbnailPath { get; set; }
        public string OfferDescription { get; set; }
        public string IconImagePath { get; set; }
        public string BackgroundColor { get; set; }
        public string BackgroundImagePath { get; set; }
        public int StoreCount { get; set; }
        public bool IsApply { get; set; }
        public int UserCardId { get; set; }
        /// <summary>
        /// 熱門指數
        /// </summary>
        public int HotPoint { get; set; }
        /// <summary>
        /// 發行時間
        /// </summary>
        public string ReleaseTime { get; set; }
        /// <summary>
        /// 公司名稱商家，註冊的公司名稱，用於隱私權政策內文顯示
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 公司的聯絡email，用於隱私權政策內文顯示
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 公司連絡電話，用於隱私權政策內文顯示
        /// </summary>
        public string Phone { get; set; }
        [BsonElement("_Stores")]
        public virtual List<StoreDetailReduced> Stores { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AvailableService { get; set; }
    }

    /// <summary>
    /// 熟客17來首頁使用
    /// </summary>
    public class MembershipCardGroupListModel
    {
        public int TotalCount { get; set; }
        public List<MembershipCardInfo> MembershipCardList { get; set; }
    }

    /// <summary>
    /// 我的熟客卡
    /// </summary>
    public class MultipleUserMemberCardListModel
    {
        public List<UserMembershipCardListModel> UserCardList { get; set; }
        public List<UserMembershipCardListModel> PromoGroupList { get; set; }
    }
    public class UserMembershipCardListModel: MembershipCardInfo
    {
        public int UserCardId { get; set; }
        public int UserSeq { get; set; }
        public double Distance { get; set; }
        public double PaymentPercent { get; set; }
        public DateTime CreateTime { get; set; }
    }

    public class MembershipCardCategoryListModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string DefaultImagePath { get; set; }
        public List<MembershipCardInfo> MembershipCardList { get; set; }
    }

    public class MembershipCardInfo
    {
        public MembershipCardInfo()
        {
            Stores = new List<StoreInfo>();
        }

        public int GroupId { get; set; }
        public string SellerName { get; set; }
        public string SellerFullCardThumbnailPath { get; set; }
        public string AvailableService { get; set; }
        public string OfferDescription { get; set; }
        public string IconImagePath { get; set; }
        public string BackgroundColor { get; set; }
        public string BackgroundImagePath { get; set; }
        public bool IsApply { get; set; }
        public List<StoreInfo> Stores { get; set; }
    }

    public class StoreInfo
    {
        public string StoreName { get; set; }
        public Coordinate Coordinates { get; set; }
        public string DistanceDesc { get; set; }
    }

    public class Coordinate
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }

    public class MembershipCardAboutGroupResultModel : MembershipCardGroupListResultModel
    {
        public string StoreName { get; set; }
        //public Coordinate Coordinates { get; set; }
        public string LastModifyTime { get; set; }
        public List<CardDetail> Cards { get; set; }
        public new List<StoreDetail> Stores { get; set; }
        public string CardValidDate { get; set; }
        public List<PponDealSynopsis> DealList { get; set; }
        public bool IsLoaded { get; set; }

        public MembershipCardAboutGroupResultModel() { }
        public MembershipCardAboutGroupResultModel(bool isLoaded)
        {
            IsLoaded = isLoaded;
        }
    }

    public class CardDetail
    {
        public int CardId { get; set; }
        public int Level { get; set; }
        public bool Enabled { get; set; }
        public double PaymentPercent { get; set; }
        public string Instruction { get; set; }
        public string OtherPremiums { get; set; }
        public int AvailableDateType { get; set; }
        public string AvailableDateDesc { get; set; }
        public int OrderNeeded { get; set; }
        public double AmountNeeded { get; set; }
        public int ConditionalLogic { get; set; }
        public string OpenTime { get; set; }
        public string CloseTime { get; set; }
        public bool CombineUse { get; set; }
    }

    public class StoreDetailReduced
    {
        public string StoreName { get; set; }
        public Coordinate Coordinates { get; set; }
    }

    public class StoreDetail : StoreDetailReduced
    {
        public Guid StoreGuid { get; set; }
        //public string StoreName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string OpenTime { get; set; }
        public string CloseDate { get; set; }
        public string Remarks { get; set; }
        public string Mrt { get; set; }
        public string Car { get; set; }
        public string Bus { get; set; }
        public string OtherVehicles { get; set; }
        public string WebUrl { get; set; }
        public string FbUrl { get; set; }
        public string PlurkUrl { get; set; }
        public string BlogUrl { get; set; }
        public string OtherUrl { get; set; }
        public bool CreditcardAvailable { get; set; }
        //public Coordinate Coordinates { get; set; }
        public string DistanceDesc { get; set; }
    }

    public class UserMembershipCardStore
    {
        public UserMembershipCardStore()
        {
        }

        public Guid StoreGuid { get; set; }
        public string StoreName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string OpenTime { get; set; }
        public string CloseDate { get; set; }
        public string Remarks { get; set; }
        public string Mrt { get; set; }
        public string Car { get; set; }
        public string Bus { get; set; }
        public string OtherVehicles { get; set; }
        public string WebUrl { get; set; }
        public string FbUrl { get; set; }
        public string PlurkUrl { get; set; }
        public string BlogUrl { get; set; }
        public string OtherUrl { get; set; }
        public bool CreditcardAvailable { get; set; }

        public Coordinate Coordinates { get; set; }
        public string DistanceDesc { get; set; }
    }

    public class UserMembershipCardEventLog
    {
        public string Memo { get; set; }
        public string LogTime { get; set; }
    }

    public class SingleUserMembershipCardModel
    {
        public SingleUserMembershipCardModel()
        {
            Stores = new List<UserMembershipCardStore>();
            Logs = new List<UserMembershipCardEventLog>();
            Cards = new List<CardDetail>();
        }

        public SingleUserMembershipCardModel(ViewUserMembershipCard viewUserMembershipCard)
        {
            Stores = new List<UserMembershipCardStore>();
            Logs = new List<UserMembershipCardEventLog>();
            Cards = new List<CardDetail>();
        }
        public string SellerName { get; set; }
        public string SellerImagePath { get; set; }
        public string SellerFullCardPath { get; set; }
        public string SellerFullCardThumbnailPath { get; set; }
        public int SellerCardType { get; set; }
        public string[] SellerIntro { get; set; }
        public string[] SellerServiceImgPath { get; set; }
        public string[] SellerServiceImgThumbnailPath { get; set; }
        public string[] SellerSrndgImgPath { get; set; }
        public string[] SellerSrndgThumbnailPath { get; set; }
        public int GroupId { get; set; }
        public string OfferDescription { get; set; }
        public int UserCardId { get; set; }
        public string UserCardNo { get; set; }
        public int Level { get; set; }
        public bool Enabled { get; set; }
        public double PaymentPercent { get; set; }
        public string Instruction { get; set; }
        public string OtherPremiums { get; set; }
        public string OpenTime { get; set; }
        public string CloseTime { get; set; }
        public int UserSeq { get; set; }
        public int OrderCount { get; set; }
        public double AmountTotal { get; set; }
        public int OrderNeeded { get; set; }
        public double AmountNeeded { get; set; }
        public int ConditionalLogic { get; set; }
        public string IconImagePath { get; set; }
        public string BackgroundImage { get; set; }
        public string BackgroundColor { get; set; }
        public string CardQRCodeUrl { get; set; }
        public string CardValidDate { get; set; }
        public List<CardDetail> Cards { get; set; }
        public List<UserMembershipCardStore> Stores { get; set; }
        public List<UserMembershipCardEventLog> Logs { get; set; }
        public string LastModifyTime { get; set; }
        public List<PponDealSynopsis> DealList { get; set; }
    }

    public class UserMembershipCardModel
    {
        public UserMembershipCardModel()
        {
            Modified = false;
            Stores=new List<StoreInfo>();
        }
        public string SellerName { get; set; }
        public string SellerImagePath { get; set; }
        public string SellerFullCardPath { get; set; }
        public string SellerFullCardThumbnailPath { get; set; }
        public int SellerCardType { get; set; }
        public int UserCardId { get; set; }
        public int MembershipCardId { get; set; }
        public int Level { get; set; }
        public bool Enabled { get; set; }
        public double PaymentPercent { get; set; }
        public string Instruction { get; set; }
        public string OtherPremiums { get; set; }
        public string OpenTime { get; set; }
        public string CloseTime { get; set; }
        public bool HaveRegularsCode { get; set; }
        public bool HaveFavorCode { get; set; }
        public int UserSeq { get; set; }
        public string IconImagePath { get; set; }
        public string BackgroundImage { get; set; }
        public string BackgroundColor { get; set; }
        public double Distance { get; set; }
        public string AvailableService { get; set; }
        public string OfferDescription { get; set; }
        /// <summary>
        /// 近期是否有更新過優惠內容，預設值為false
        /// </summary>
        public bool Modified { get; set; }
        public List<StoreInfo> Stores { get; set; }
    }

    public class PcpImageInfo
    {
        public int SellerUserId { get; set; }
        public int GroupId { get; set; }
        public string ImagePathUrl { get; set; }
        public string ImageCompressedPathUrl { get; set; }
        public PcpImageType ImgType { get; set; }
        public int Seq { get; set; }
    }

    public class PromoGroupUserMembershipCardModel
    {
        [BsonId]
        public int GroupId { get; set; }
        public string SellerName { get; set; }
        public string StoreName { get; set; }
        public string Percent { get; set; }
        public string OfferDescription { get; set; }
        public string IconImagePath { get; set; }
        public string BackgroundImagePath { get; set; }
        public string BackgroundColor { get; set; }
        public string OpenTime { get; set; }
        public string CloseTime { get; set; }
    }

    public class MultipleUserMembershipCardModel
    {
        public MultipleUserMembershipCardModel()
        {
            UserCardList = new List<UserMembershipCardModel>();
            PromoGroupList = new List<MembershipCardGroupListResultModel>();
            IsLogin = false;
        }

        public List<UserMembershipCardModel> UserCardList { get; set; }
        public List<MembershipCardGroupListResultModel> PromoGroupList { get; set; }
        public bool IsLogin { get; set; }
    }

    public class IdentityCodeUrlModel
    {
        public string IdentityCodeUrl { get; set; }
    }

    public class SellerModel
    {
        public SellerModel(string s)
        {
            SellerName = s;
            MembershipCardLevels = "尚未建卡";
        }
        public string SellerName { get; set; }
        public string GroupId { get; set; }
        public string MembershipCardLevels { get; set; }
    }

    /// <summary>
    /// 熟客卡品牌
    /// </summary>
    public class MembershipCardBrandModel
    {
        public int GroupId { get; set; }
        public string BrandName { get; set; }
        public string BrandInfo { get; set; }
        public string BrandImagePath { get; set; }
        public string LogoImagePath { get; set; }
        public int StoreCount { get; set; }
    }

    public class PcpTransResultModel
    {
        /// <summary>
        /// 
        /// </summary>
        public PcpTransResultModel()
        {
            Type = PcpTransactionType.None;
            GroupId = 0;
            Title = string.Empty;
        }


        /// <summary>
        /// 
        /// </summary>
        public string ResultMessage { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public PcpTransactionType Type { set; get; }
        public int GroupId { get; set; }
        public string Title { get; set; }
    }
}


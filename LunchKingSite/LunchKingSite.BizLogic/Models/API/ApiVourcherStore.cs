﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiVourcherStore : System.ICloneable
    {
        public Guid SellerGuid { get; set; }
        public string SellerId { get; set; }
        public string SellerName { get; set; }
        public string SellerLogoimgPath { get; set; }
        public SellerSampleCategory SellerCategory { get; set; }
        public int AttentionCount { get; set; }
        public string DistanceDesc { get; set; }
        public double Distance { get; set; }
        public string VourcherContent { get; set; }
        /// <summary>
        /// VourcherContent顯示內容的eventId，一般為該商家第一個優惠券
        /// </summary>
        public int EventId { get; set; }
        public string StartDate { get; set; }
        public int VourcherCount { get; set; }
        /// <summary>
        /// 緯度座標
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 經度座標
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// 優惠券所處位置，預定取代Latitude Longitude兩屬性
        /// </summary>
        public ApiCoordinates Coordinates { get; set; }

        public object Clone()
        {
            ApiVourcherStore rtn = (ApiVourcherStore)this.MemberwiseClone();
            //處理淺層複製無法處理的部分
            rtn.SellerGuid = new Guid(SellerGuid.ToString());
            rtn.Coordinates = Coordinates == null ? null : Coordinates.Clone() as ApiCoordinates;
            return rtn;
        }
    }
}

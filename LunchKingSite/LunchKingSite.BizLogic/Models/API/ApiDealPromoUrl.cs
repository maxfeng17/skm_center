﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiDealPromoUrl
    {
        public string Url { get; set; }
        public PponDealPromoUrlType UrlType { get; set; }

        public ApiDealPromoUrl()
        {
            UrlType = PponDealPromoUrlType.Error;
        }
    }
}

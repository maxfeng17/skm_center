﻿using System;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System.Collections.Generic;
using System.ComponentModel;
using LunchKingSite.BizLogic.Component;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.API
{
    /// <summary>
    /// [v2.0] 訂單列表物件
    /// </summary>
    public class ApiPponOrder
    {
        /// <summary>
        /// 訂單GUID
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 憑證類型
        /// </summary>
        public NewCouponListFilterType CountType { get; set; }
        /// <summary>
        /// 產品名稱
        /// </summary>
        public string CouponName { get; set; }
        /// <summary>
        /// 使用期間-起
        /// </summary>
        public string UseStartDate { get; set; }
        /// <summary>
        /// 使用期間-迄
        /// </summary>
        public string UseEndDate { get; set; }
        /// <summary>
        ///  購買日
        /// </summary>
        public string BuyDate { get; set; }
        /// <summary>
        /// 可用憑證數
        /// </summary>
        public int CouponCount { get; set; }
        /// <summary>
        /// 訂單呈現主圖
        /// </summary>
        public string MainImagePath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool DisableSMS { get; set; }
        /// <summary>
        /// 訂單狀態說明文字
        /// </summary>
        public string OrderTypeDesc { get; set; }
        /// <summary>
        /// 優惠檔次產出的憑證顯示方式
        /// </summary>
        public DealCouponType CouponType { get; set; }
        /// <summary>
        /// 總憑證數
        /// </summary>
        public int TotalCouponCount { get; set; }
        /// <summary>
        /// 單價
        /// </summary>
        public int UnitPrice { get; set; }
        /// <summary>
        /// 成套商品App頻道 (作用:讓APP顯示成套標籤顏色) (其數值對應的是CategoryId)
        /// </summary>
        public int GroupCouponAppChannel { get; set; }
        /// <summary>
        /// 是為否為成套 (作用:讓APP將訂單程現為成套樣式)
        /// </summary>
        public GroupCouponType GroupCouponType { get; set; }
        /// <summary>
        /// 是否為清算檔次 (新光用)
        /// </summary>
        public bool IsSettlementDeal { get; set; }
        /// <summary>
        /// 可填評價數量
        /// </summary>
        public int EvaluateCount { get; set; }

        /// <summary>
        /// 預計出貨日
        /// </summary>
        public string LastShippingDate { get; set; }
        /// <summary>
        /// 實際出貨日
        /// </summary>
        public string ActualShippingDate { get; set; }
        /// <summary>
        /// 配送類型
        /// </summary>
        public int DeliveryType { get; set; }
        public ApiPponOrder()
        {
            LastShippingDate = string.Empty;
            ActualShippingDate = string.Empty;
        }
    }
    
    /// <summary>
    /// [v3.0] 訂單列表
    /// </summary>
    public class ApiMemberOrders
    {
        public List<ApiMemberOrder> MemberOrders { get; set; }
        public ApiMemberOrders()
        {
            MemberOrders = new List<ApiMemberOrder>();
        }
    }

    /// <summary>
    /// [v3.0] 訂單列表物件
    /// </summary>
    public class ApiMemberOrder
    {
        /// <summary>
        /// 訂單GUID
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        [JsonIgnore]
        public string OrderId { get; set; }
        /// <summary>
        /// 訂單呈現主圖 ([憑證]正方型圖，[咖啡]長方型圖)
        /// </summary>
        public string MainImagePath { get; set; }
        /// <summary>
        /// 產品名稱
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// 購買日
        /// </summary>
        public string BuyDate { get; set; }
        /// <summary>
        /// 客服回覆訊息數 (大於0列表前方顯示紅點)
        /// </summary>
        public int CsMessageCount { get; set; }
        /// <summary>
        /// 會員訂單狀態說明文字
        /// </summary>
        public string OrderStatusDesc { get; set; }
        /// <summary>
        /// 是否顯示訂單狀態
        /// </summary>
        public bool IsShowOrderStatus { get; set; }
        /// <summary>
        /// OrderTags (目前只有咖啡寄杯/成套票券)
        /// </summary>
        public List<int> OrderTags { get; set; }
        /// <summary>
        /// 商品配送類型
        /// </summary>
        public int ProductDeliveryType { get; set; }
        /// <summary>
        /// 是否為24小時到貨檔次訂單
        /// </summary>
        public bool Is24H { get; set; }

        #region 憑證

        /// <summary>
        /// [電子票券] 兌換期限-起
        /// </summary>
        public string PponUseStartDate { get; set; }
        /// <summary>
        /// [電子票券][咖啡] 兌換期限-迄
        /// </summary>
        public string PponUseEndDate { get; set; }
        /// <summary>
        /// [咖啡] 可用憑證數
        /// </summary>
        public int PponCouponCount { get; set; }
        /// <summary>
        /// [咖啡] 總憑證數
        /// </summary>
        public int PponTotalCouponCount { get; set; }
        /// <summary>
        /// [咖啡] 成套標籤顏色 (其數值對應的是CategoryId)
        /// </summary>
        public int PponGroupCouponAppChannel { get; set; }

        #endregion

        #region 宅配
        
        /// <summary>
        /// [宅配] 預計出貨日
        /// </summary>
        public string GoodsLastShippingDate { get; set; }
        /// <summary>
        /// [宅配] 實際出貨日
        /// </summary>
        public string GoodsActualShippingDate { get; set; }
        /// <summary>
        /// [宅配] 出貨狀態
        /// </summary>
        public ShippingStatus GoodsShipStatus { get; set; }
        #endregion

        public ApiMemberOrder()
        {
            MainImagePath = string.Empty;
            ItemName = string.Empty;
            BuyDate = string.Empty;
            CsMessageCount = 0;
            OrderStatusDesc = string.Empty;
            IsShowOrderStatus = false;
            OrderTags = new List<int>();
            ProductDeliveryType = 0;
            PponUseStartDate = string.Empty;
            PponUseEndDate = string.Empty;
            PponCouponCount = 0;
            PponTotalCouponCount = 0;
            PponGroupCouponAppChannel = CategoryManager.Default.Family.CategoryId;
            GoodsLastShippingDate = string.Empty;
            GoodsActualShippingDate = string.Empty;
            GoodsShipStatus = ShippingStatus.None;
        }
    }
    
    /// <summary>
    /// [反向核銷] By商家的訂單列表
    /// </summary>
    public class ApiOrderByVerifyStore
    {
        /// <summary>
        /// 訂單GUID或禮物AccessGuid
        /// </summary>
        public Guid OrderGuidOrAccessCode { get; set; }
        /// <summary>
        /// 品名
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// 呈現主圖
        /// </summary>
        public string MainImagePath { get; set; }
        /// <summary>
        /// 尚有份數
        /// </summary>
        public int CouponCount { get; set; }
        /// <summary>
        /// 總計份數
        /// </summary>
        public int TotalCouponCount { get; set; }
        /// <summary>
        /// 使用期限
        /// </summary>
        public string UseEndDate { get; set; }
        /// <summary>
        /// 是否為禮物
        /// </summary>
        public bool IsGift { get; set; }
        /// <summary>
        /// 遮罩顏色
        /// </summary>
        public int GroupCouponAppChannel { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiMember
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PezId { get; set; }
        /// <summary>
        /// 會員行動電話號碼，可為空直
        /// </summary>
        public string Mobile { get; set; }
        public string FbId { get; set; }
        public string FbPic { get; set; }

        /// <summary>
        /// 進行手機認證的行動電話
        /// </summary>
        public string MobileAuthNumber { get; set; }

        /// <summary>
        /// 是否完成手機認證
        /// </summary>
        public bool IsMobileAuthed { get; set; }
    }

    public class VbsApiMember
    {
        public int Id { get; set; }
        public string AccountId { get; set; }
        public int AccountType { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}

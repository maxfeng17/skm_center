﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiVourcherEvent
    {
        /// <summary>
        /// 優惠券編號
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 優惠券內容
        /// </summary>
        public string Contents { get; set; }
        /// <summary>
        /// 優惠券使用方式
        /// </summary>
        public string Instruction { get;set; }
        /// <summary>
        /// 優惠券限制
        /// </summary>
        public string Restriction { get; set; }
        /// <summary>
        /// 其他說明
        /// </summary>
        public string Others { get; set; }
        /// <summary>
        /// 活動截止日期
        /// </summary>
        public string EndDate { get; set; }
        /// <summary>
        /// 關注數
        /// </summary>
        public int AttentionCount { get; set; }
        /// <summary>
        /// 優惠券類型
        /// </summary>
        public VourcherEventMode Mode { get; set; }
        /// <summary>
        /// 最大數量
        /// </summary>
        public int MaxQuantity { get; set; }
        /// <summary>
        /// 目前數量
        /// </summary>
        public int CurrentQuantity { get; set; }
        /// <summary>
        /// 圖片集
        /// </summary>
        public List<string> PicUrl { get; set; }
        /// <summary>
        /// 商家名稱
        /// </summary>
        public string SellerName { get; set; }
        /// <summary>
        /// 商家說明
        /// </summary>
        public string SellerDescription { get; set; }
        /// <summary>
        /// 活動配合分店
        /// </summary>
        public List<ApiVourcherEventStore> EventStores { get; set; }
        /// <summary>
        /// 平均店消費
        /// </summary>
        public SellerConsumptionAvg? ConsumptionAvg { get; set; }
        /// <summary>
        /// 使用者收藏狀況
        /// </summary>
        public ApiVourcherUserFavoriteState UserFavoriteState { get; set; }

        
        public ApiVourcherEvent()
        {
            PicUrl = new List<string>();
            EventStores = new List<ApiVourcherEventStore>();
        }
    }


    //for toyota today vourcher 串接
    [Serializable]
    public class ApiToyotaVourcherEvent
    {
        /// <summary>
        /// 優惠券編號
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 優惠券內容
        /// </summary>
        public string Contents { get; set; }
        /// <summary>
        /// 優惠券使用方式
        /// </summary>
        public string Instruction { get; set; }
        /// <summary>
        /// 優惠券限制
        /// </summary>
        public string Restriction { get; set; }
        /// <summary>
        /// 其他說明
        /// </summary>
        public string Others { get; set; }
        /// <summary>
        /// 活動截止日期
        /// </summary>
        public string EndDate { get; set; }
        /// <summary>
        /// 關注數
        /// </summary>
        public int AttentionCount { get; set; }
        /// <summary>
        /// 優惠券類型
        /// </summary>
        public VourcherEventMode Mode { get; set; }
        /// <summary>
        /// 最大數量
        /// </summary>
        public int MaxQuantity { get; set; }
        /// <summary>
        /// 目前數量
        /// </summary>
        public int CurrentQuantity { get; set; }
        /// <summary>
        /// 圖片集
        /// </summary>
        public List<string> PicUrl { get; set; }
        /// <summary>
        /// 商家識別碼
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 商家名稱
        /// </summary>
        public string SellerName { get; set; }
        /// <summary>
        /// 優惠券商家分類
        /// </summary>
        public int SellerCategory { get; set; }
        /// <summary>
        /// 商家說明
        /// </summary>
        public string SellerDescription { get; set; }
        /// <summary>
        /// 活動配合分店
        /// </summary>
        public List<ApiToyotaVourcherEventStore> EventStores { get; set; }
        /// <summary>
        /// 平均店消費
        /// </summary>
        public SellerConsumptionAvg? ConsumptionAvg { get; set; }


        public ApiToyotaVourcherEvent()
        {
            PicUrl = new List<string>();
            EventStores = new List<ApiToyotaVourcherEventStore>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Facade;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.BizLogic.Model.API
{
    /// <summary>
    /// API回傳用的經緯度物件
    /// </summary>
    public class ApiCoordinates : System.ICloneable
    {        
        private double _longitude;
        /// <summary>
        /// 經度
        /// </summary>
        public double Longitude { 
            get { return _longitude; }
            set
            {
                if (value > 180 || value < -180)
                {
                    _longitude = 0;
                    throw new Exception("經度限制為正負180");
                }
                else
                {
                    _longitude = value;
                }
            }
        }

        private double _latitude;
        /// <summary>
        /// 緯度
        /// </summary>
        public double Latitude
        {
            get { return _latitude; }
 
            set
            {
                if (value > 90 || value < -90)
                {
                    _latitude = 0;
                    throw new Exception("緯度限制為正負90");
                }
                else
                {
                    _latitude = value;
                }
            }
        } 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="longitude">經度</param>
        /// <param name="latitude">緯度</param>
        public ApiCoordinates(double longitude, double latitude)
        {
            Longitude = longitude;
            Latitude = latitude;
        }

        public SqlGeography ToSqlGeography()
        {
            return LocationFacade.GetGeographicPoint(this);
        }

        public object Clone()
        {
            ApiCoordinates rtn = (ApiCoordinates)this.MemberwiseClone();
            return rtn;
        }


        #region static 

        private static ApiCoordinates _default = new ApiCoordinates(0, 0);
        public static ApiCoordinates  Default
        {
            get { return _default; }
        }

        public static ApiCoordinates FromSqlGeography(SqlGeography geography)
        {
            if (geography == null || geography.IsNull || geography.Lat.IsNull || geography.Long.IsNull)
            {
                return Default;
            }
            return new ApiCoordinates(geography.Long.Value, geography.Lat.Value);
        }

        /// <summary>
        /// 嘗試產生 ApiCoordinates物件，成功回傳true coordinates參數則為轉型後物件，失敗回傳false coordinates參數則為null
        /// </summary>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <param name="coordinates"></param>
        /// <returns></returns>
        public static bool TryParse(string longitude , string latitude , out ApiCoordinates coordinates)
        {
            coordinates = null;
            double lon , lat;
            //檢查經緯度的字串轉型為double是否正常
            if (!double.TryParse(longitude, out lon))
            {
                return false;
            }
            if (!double.TryParse(latitude, out lat))
            {
                return false;
            }

            try
            {
                coordinates = new ApiCoordinates(lon,lat);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion static 
    }
}

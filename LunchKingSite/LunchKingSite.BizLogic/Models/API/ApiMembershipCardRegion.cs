﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiMembershipCardRegion
    {

        /// <summary>
        /// 熟客卡區域編號-現階段與cityId的值一致
        /// </summary>
        [JsonProperty("Id")]
        public int RegionId { get; set; }
        /// <summary>
        /// 熟客卡區域的名稱 如台北、桃園等
        /// </summary>
        [JsonProperty("Name")]
        public string RegionName { get; set; }
    }

    public class ApiMembershipCardRegionListCatch
    {
        public ApiMembershipCardRegionListCatch()
        {
            Version = 0;
            RegionList = new List<ApiMembershipCardRegion>();
        }
        public int Version { get; set; }
        public List<ApiMembershipCardRegion> RegionList { get; set; }
    }
}

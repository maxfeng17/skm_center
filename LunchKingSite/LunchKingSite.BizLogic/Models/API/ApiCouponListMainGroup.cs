﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiCouponListMainGroup
    {
        /// <summary>
        /// 類型
        /// </summary>
        public CouponListFilterType FilterType { get; set; }
        /// <summary>
        /// 數量
        /// </summary>
        public int Count { get; set; }
    }


    public class ApiOrderGrouptMain
    {
        /// <summary>
        /// 憑證類型
        /// </summary>
        public int GroupType { get; set; }
        /// <summary>
        /// 憑證群組
        /// </summary>
        public List<ApiOrderGroup> CouponList { get; set; }
    }

    public class ApiOrderGroup
    {
        /// <summary>
        /// 訂單數量
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 群組類別
        /// </summary>
        public NewCouponListFilterType FilterType { get; set; }
        /// <summary>
        /// 文字說明
        /// </summary>
        public string Text { get; set; }

        public ApiOrderGroup()
        {
            Count = 0;
            Text = string.Empty;
        }
    }
}

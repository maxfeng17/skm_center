﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiDiscountCode
    {
        /// <summary>
        /// 折價券類型
        /// </summary>
        public DiscountCampaignType Type { get; set; }
        /// <summary>
        /// 可折抵金額
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 使用須達成的門檻
        /// </summary>
        public decimal MinimumAmount { get; set; }
        /// <summary>
        /// 折價券使用時間-起(含)
        /// </summary>
        public string StartTime { get; set; }
        /// <summary>
        /// 折價券使用時間-迄(含)
        /// </summary>
        public string EndTime { get; set; }
        public bool Used { get; set; }
        public string Code { get; set; }
        public string ChannelRestrict { get; set; }
    }
}

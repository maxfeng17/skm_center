﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiReturnObject
    {

        /// <summary>
        /// 回傳的結果
        /// </summary>
        public ApiReturnCode Code { get; set; }
        /// <summary>
        /// 回傳資料主體
        /// </summary>
        public object Data { get; set; }
        /// <summary>
        /// 回傳的訊息
        /// </summary>
        public string Message { get; set; }


        public ApiReturnObject()
        {
            Code = ApiReturnCode.Ready;
            Data = null;
            Message = string.Empty;
        }
    }

    public class ApiRefundReturnObject
    {
        /// <summary>
        /// 回傳的結果
        /// </summary>
        public PaymentUtilityRefundType Code { get; set; }
        /// <summary>
        /// 退貨狀態
        /// </summary>
        public ProgressStatus ReturnStatus { get; set; }
        /// <summary>
        /// 退貨最後異動時間
        /// </summary>
        public DateTime? ReturnModifyTime { get; set; }
        /// <summary>
        /// 回傳資料主體
        /// </summary>
        public object Data { get; set; }
        /// <summary>
        /// 回傳的訊息
        /// </summary>
        public string Message { get; set; }

        public ApiRefundReturnObject()
        {
            Code = PaymentUtilityRefundType.Success;
            Data = null;
            Message = string.Empty;
        }
    }

    public class ApiMakeOrderReturnObject
    {
        /// <summary>
        /// 回傳的結果OrderGuid
        /// </summary>
        public string OrderGuid { get; set; }
        /// <summary>
        /// 回傳的結果Id
        /// </summary>
        public string OrderId { get; set; }

        public List<ApiCouponReturnObject> Coupon { get; set; }
        public DateTime UseTimeS { get; set; }
        public DateTime UseTimeE { get; set; }
        /// <summary>
        /// order_detail.create_time tick
        /// </summary>
        public string TimeTicks { get; set; }
    }

    public class ApiRefundMultiReturnObject
    {
        /// <summary>
        /// 回傳的結果OrderGuid
        /// </summary>
        public string OrderGuid { get; set; }
        /// <summary>
        /// 回傳的結果Id
        /// </summary>
        public string Id { get; set; }

        public ApiRefundReturnObject ReturnObject { get; set; }

        public ApiRefundMultiReturnObject()
        {
            OrderGuid = string.Empty;
            Id = string.Empty;
            ReturnObject = new ApiRefundReturnObject();
        }
    }

    public class ApiCouponReturnObject
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        /// <summary>
        /// 回傳的結果Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 回傳的結果SequenceNumber
        /// </summary>
        public string SequenceNumber { get; set; }
        /// <summary>
        /// 回傳的結果Code
        /// </summary>
        public string Code { get; set; }
        public bool IsCustomCode { get; set; }
        public CustomBarcode CustomCode { get; set; }
        public ApiCouponReturnObject(int couponId, string sequenceNumber, string code, IViewPponDeal vpd)
        {
            Id = couponId;
            SequenceNumber = sequenceNumber;
            Code = code;
            IsCustomCode = false;
            if (vpd != null && Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
            {
                if (Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal)
                        && Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.PEZevent))
                {
                    //全家客製
                    IsCustomCode = true;
                    var famip = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
                    var fp = famip.GetFamilyNetPincodeByCouponId(couponId);
                    var fd = famip.GetFamilyNetPincodeDetail(fp.OrderNo, 1);
                    var peztemp = famip.GetPeztemp(fp.PeztempId);
                    CustomCode = new CustomBarcode
                    {
                        CodeType = vpd.CouponCodeType ?? 0,
                        Barcode1 = fd.Barcode2,
                        Pincode = peztemp.PezCode
                    };
                }
                else if (Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.HiLifeDeal)
                    && Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.PEZevent))
                {
                    //萊爾富
                    IsCustomCode = true;
                    CustomCode = new CustomBarcode
                    {
                        CodeType = vpd.CouponCodeType ?? 0,
                        Barcode1 = config.HiLifeFixedFunctionPincode,
                        Barcode2 = sequenceNumber
                    };
                }
                else if (Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.PEZevent))
                {
                    //廠商匯入序號
                    IsCustomCode = true;
                    CustomCode = new CustomBarcode
                    {
                        CodeType = vpd.CouponCodeType ?? 0,
                        Barcode1 = sequenceNumber
                    };
                }
            }
        }
    }

    public class CustomBarcode 
    {
        public int CodeType { get; set; }
        public string Barcode1 { get; set; }
        public string Barcode2 { get; set; }
        public string Barcode3 { get; set; }
        public string Barcode4 { get; set; }
        public string Pincode { get; set; }
    }

    public class ApiOrderInfoReturnObject
    {
        ///<summary>
        /// 回傳的結果OrderGuid
        /// </summary>
        public string OrderGuid { get; set; }
        
        public List<ApiCouponReturnObject> Coupon { get; set; }

        public ApiOrderInfoReturnObject()
        {
            Coupon = new List<ApiCouponReturnObject>();
        }

    }

    public class ApiOrderInfoMultiReturnObject
    {
        /// <summary>
        /// 回傳的結果RelatedOrderGuid
        /// </summary>
        public string RelatedOrderId { get; set; }
        /// <summary>
        /// 回傳的結果電話
        /// </summary>
        public string SmsMobile { get; set; }
        /// <summary>
        /// 是否有資料
        /// </summary>
        public bool HasValue { get; set; }
        public ApiOrderInfoReturnObject ReturnObject { get; set; }

    }
}

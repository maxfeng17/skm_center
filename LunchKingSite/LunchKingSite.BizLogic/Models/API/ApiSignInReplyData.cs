﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model.API
{
    public class ApiSignInReplyData
    {
        public SignInReply ReplyType { get; set; }
        public ApiMember SignInMember { get; set; }
        public string LoginTicket { get; set; }
        public string ExtId { get; set; }
        /// <summary>
        /// OAuth Access Token
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// OAuth Access Token Expired sec
        /// </summary>
        public int AccessTokenExpiredSec { get; set; }
        /// <summary>
        /// 回傳訊息
        /// </summary>
        public string ReplyMessage { get; set; }
        /// <summary>
        /// 是否顯示確認訊息-20141126新增，用以控制前端是否顯示獨立相關公告，預設為false
        /// </summary>
        public bool ShowConfirm { get; set; }

        public ApiSignInReplyData()
        {
            ReplyType = SignInReply.AccountNotExist;
            SignInMember = null;
            LoginTicket = string.Empty;
            ReplyMessage = I18N.Phrase.SignInReplyAccountNotExist;
            ShowConfirm = false;
            AccessToken = string.Empty;
            AccessTokenExpiredSec = 0;
        }
    }

    public class VbsApiSignInReplyData
    {
        public SignInReply ReplyType { get; set; }
        public VbsApiMember SignInMember { get; set; }
        public string LoginTicket { get; set; }
        public string ExtId { get; set; }
        /// <summary>
        /// 回傳訊息
        /// </summary>
        public string ReplyMessage { get; set; }

        /// <summary>
        /// 第一次登入系統
        /// </summary>
        public bool IsFirstLogin { get; set; }

        public VbsApiSignInReplyData()
        {
            ReplyType = SignInReply.AccountNotExist;
            SignInMember = null;
            LoginTicket = string.Empty;
            ReplyMessage = I18N.Phrase.SignInReplyAccountNotExist;
            IsFirstLogin = false;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model
{
    public class PcpPointTransactionMailInfo
    {
    }

    #region 通知信

    #region    PCP點數儲值單通知信
    public class PcpPointTransactionOrderMailInformation
    {
        public string SellerName { get; set; }
        public bool ShowRegularsContent { get; set; }
        public int RegularsPoint { get; set; }
        public int FavorPoint { get; set; }
        }
    
    #endregion PCP點數儲值單通知信


    #region    PCP點數退款單通知信

    public class PcpPointTransactionRefundMailInformation
    {
        public string SellerName { get; set; }
        public int RefundAmount { get; set; }

    }

    
    #endregion PCP點數退款單通知信

    #region    超級紅利金請款單通知信

    public class PcpPointTransactionExchangeOrderMailInformation
    {
        public string SellerName { get; set; }
        public int ExchangeAmount { get; set; }
    }

    #endregion 超級紅利金請款單通知信


    #endregion 通知信

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model
{
    [Serializable]
    public class PponOrderContent
    {
        public string TickedId { get; set; }
        public Guid BusinessHourGuid { get; set; }
        public string Content { get; set; }
        public string AlternativeEmail { get; set; }
        public Guid ParentOrderId { get; set;}
        public string UserName { get; set; }
        public bool IsForFriend { get; set; }
    }
}

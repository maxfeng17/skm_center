﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models
{
    /// <summary>
    /// 會員信用卡資訊
    /// </summary>
    public class MemberCreditCardInfo
    {
        /// <summary>
        /// 信用卡代碼
        /// </summary>
        public Guid CardGuid { get; set; }

        /// <summary>
        /// 信用卡名稱
        /// </summary>
        public string CardName { get; set; }

        /// <summary>
        /// 信用卡後四碼
        /// </summary>
        public string CardTail { get; set; }

        /// <summary>
        /// 有效年 YY
        /// </summary>
        public string ValidYear { get; set; }

        /// <summary>
        /// 有效月 MM
        /// </summary>
        public string ValidMonth { get; set; }

        /// <summary>
        /// 卡別 VISA/MASTER/JCB
        /// </summary>
        public int CardType { get; set; }

        /// <summary>
        /// 銀行編號(非銀行代碼)
        /// </summary>
        public int BankId { get; set; }

        /// <summary>
        /// 銀行名稱
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// 是否為最後使用的卡片
        /// </summary>
        public bool IsLastUsed { get; set; }

        /// <summary>
        /// 是否可以分期
        /// </summary>
        public bool IsInstallment { get; set; }
    }
}

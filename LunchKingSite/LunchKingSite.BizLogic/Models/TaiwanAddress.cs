﻿using System;
using System.IO;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Model
{
    public class TaiwanAddress // 拆解台灣門牌地址字串的Class
    {
        private string mShortAddress;
        private List<string> mAreaTokens = new List<string>();
        private List<TokenAlign> mAreaTokensAlign = new List<TokenAlign>();
        private List<string> mDoorTokens = new List<string>();
        private List<TokenAlign> mDoorTokensAlign = new List<TokenAlign>();

        private enum TokenAlign
        {
            AlignLeft = 0,
            AlignRight = 1
        }

        #region properties
        /// <summary>
        /// 郵遞區號
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 縣市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 鄉鎮市區
        /// </summary>
        public string Town { get; set; }

        /// <summary>
        /// 村里
        /// </summary>
        public string Village { get; set; }

        /// <summary>
        /// 鄰
        /// </summary>
        public string Lin { get; set; }

        /// <summary>
        /// 路街
        /// </summary>
        public string Road { get; set; }

        /// <summary>
        /// 段
        /// </summary>
        public string Sec { get; set; }

        /// <summary>
        /// 巷
        /// </summary>
        public string Lane { get; set; }

        /// <summary>
        /// 弄
        /// </summary>
        public string Alley { get; set; }

        /// <summary>
        /// 衖
        /// </summary>
        public string SubAlley { get; set; }

        /// <summary>
        /// 號
        /// </summary>
        public string No { get; set; }

        /// <summary>
        /// 之號
        /// </summary>
        public string SubNo { get; set; }

        /// <summary>
        /// 樓
        /// </summary>
        public string Floor { get; set; }

        /// <summary>
        /// 樓之
        /// </summary>
        public string SubFloor { get; set; }

        /// <summary>
        /// 室
        /// </summary>
        public string Room { get; set; }
        #endregion

        #region constructor
        public TaiwanAddress(string FullAddress)
        {
            #region stemming & modeling
            if (FullAddress.Contains("F")) FullAddress = FullAddress.Replace("F", "樓");

            if (FullAddress.Contains("f")) FullAddress = FullAddress.Replace("f", "樓");

            if (!FullAddress.Contains("樓")) FullAddress = FullAddress + "1樓";

            if (FullAddress.Contains("-")) FullAddress = FullAddress.Replace("-", "之");

            if (FullAddress.Contains("，")) FullAddress = FullAddress.Replace('，', ',');

            if (FullAddress.IndexOf("之") > FullAddress.IndexOf("號") && FullAddress.IndexOf("之") < FullAddress.IndexOf("樓")
                     && FullAddress.IndexOf(",") > FullAddress.IndexOf("號") && FullAddress.IndexOf(",") < FullAddress.IndexOf("樓"))
            {
                string temp = FullAddress.Substring(FullAddress.IndexOf("之"), FullAddress.IndexOf(",") - FullAddress.IndexOf("之"));

                FullAddress = FullAddress.Remove(FullAddress.IndexOf("之"), FullAddress.IndexOf(",") - FullAddress.IndexOf("之") + 1).Insert(FullAddress.IndexOf("號"), temp);
            }
            #endregion

            Room = string.Empty;
            SubFloor = string.Empty;
            Floor = string.Empty;
            SubNo = string.Empty;
            No = string.Empty;
            SubAlley = string.Empty;
            Alley = string.Empty;
            Lane = string.Empty;
            Sec = string.Empty;
            Road = string.Empty;
            Lin = string.Empty;
            Village = string.Empty;
            Town = string.Empty;
            City = string.Empty;
            Province = string.Empty;

            #region speciality pool
            List<string> specialtowns = new List<string> { "竹東鎮", "東沙群島", "南沙群島", "釣魚台", "桃園區" };
            List<string> specialroads = new List<string> { "新市街","大埔街","和興街","水碓街","富村街","西門街","南村路","尖山路","三村路","水碓路","恭敬路","員山路","嘉里路機場", 
                                                           "香村路","美村路","和興路","大埔路","文山路","三塊厝","三櫃","上大","上內壢","下圭柔山","下茄苳","下罟子","下福村",
                                                           "中埔村","中崙","五守新村","五間厝","下雙溪","大竹圍","大埔","大雁巷", "小埤頭","小崎巷","山子頂","山東里","山腳巷",
                                                           "中屯村","中央第二商場","中坑","仁愛村","公有零售市場","六安里","太平嶺","文山","月眉里","水挸頭","水碓","北苗",
                                                           "古庄","台北榮總西區","台北榮總東區","民生村","玉泉","田中一橫巷","石岡子","同安西巷","尖山","百寧新村","竹仔腳",
                                                           "竹東","竹南","自強里","西門","和興","坡雅頭","東海別墅","松柏林","松埔南巷","波羅汶","牧場","長安村","長坑口",
                                                           "長庚醫護新村","長發工業區","阿柔洋","南洲村","南勢村","柳子林","苗中","倒別牛","員山","埔心村廟後","崁下","恭敬",
                                                           "埤角","埤頭","崎頂","崎腳","崙坪","崙雅巷","清大西院","清華村","許厝庄","頂橋三巷","麻豆口","朝東","渡船巷",
                                                           "栗子崙","泰和里","海寮","烏橋","菜公村","新埔子","溪洲","聖福里","嘉添","墨林村","廣安里","樹林子","橋仔頭","頭份",
                                                           "龍門村","聯大","鴿溪寮","蟠桃","豐榮","雙福","蘇厝寮","警光一村","鹽埕巷","灣內三巷","車路頭街","中路村","市民大道",
                                                           "鎮前街", "鎮東路","鎮南街", "鎮南路","九斗村", "九甲巷", "八里堆", "三十六崙", "三光巷", "三角店" };

            foreach (var list in specialtowns)
            {
                if (FullAddress.IndexOf(list) > FullAddress.IndexOf("縣") || FullAddress.IndexOf(list) > FullAddress.IndexOf("市"))
                {
                    FullAddress = FullAddress.Replace(list, string.Empty);
                    Town = list;
                }
            }

            foreach (var list in specialroads)
            {
                if (FullAddress.IndexOf(list) > FullAddress.IndexOf("縣") || FullAddress.IndexOf(list) > FullAddress.IndexOf("市"))
                {
                    FullAddress = FullAddress.Replace(list, string.Empty);
                    Road = list;
                }
            }

            #endregion

            mAreaTokens.Add("省");
            mAreaTokensAlign.Add(TokenAlign.AlignRight);
            mAreaTokens.Add("縣市");
            mAreaTokensAlign.Add(TokenAlign.AlignRight);
            mAreaTokens.Add("鄉鎮市區");
            mAreaTokensAlign.Add(TokenAlign.AlignRight);
            mAreaTokens.Add("村里");
            mAreaTokensAlign.Add(TokenAlign.AlignRight);
            mAreaTokens.Add("鄰");
            mAreaTokensAlign.Add(TokenAlign.AlignRight);
            mAreaTokens.Add("路街道");
            mAreaTokensAlign.Add(TokenAlign.AlignRight);
            mAreaTokens.Add("段");
            mAreaTokensAlign.Add(TokenAlign.AlignRight);

            mDoorTokens.Add("巷");
            mDoorTokensAlign.Add(TokenAlign.AlignRight);
            mDoorTokens.Add("弄");
            mDoorTokensAlign.Add(TokenAlign.AlignRight);
            mDoorTokens.Add("衖");
            mDoorTokensAlign.Add(TokenAlign.AlignRight);
            mDoorTokens.Add("號");
            mDoorTokensAlign.Add(TokenAlign.AlignRight);
            mDoorTokens.Add("之");
            mDoorTokensAlign.Add(TokenAlign.AlignLeft);
            mDoorTokens.Add("樓");
            mDoorTokensAlign.Add(TokenAlign.AlignRight);
            mDoorTokens.Add("之");
            mDoorTokensAlign.Add(TokenAlign.AlignLeft);
            mDoorTokens.Add("室");
            mDoorTokensAlign.Add(TokenAlign.AlignRight);

            this.ParseAddress(FullAddress);
        }
        #endregion

        #region methods
        private void ParseAddress(string Address)
        {
            mShortAddress = ParseLongAddress(Address);
            ParseShortAddress(mShortAddress);
        }

        private void ParseShortAddress(string Address)
        {
            string sTemp = Address.Trim();
            int iPos = 0;
            string sTokens = null;
            string sSub = null;

            for (int I = 0; I <= mDoorTokens.Count - 1; I++)
            {
                sSub = "";
                sTokens = mDoorTokens[I];
                int iTemp = 0;
                iTemp = I <= 3 ? TownIndexOf(Address, sTokens, iPos) : AddressIndexOf(Address, sTokens, iPos);
                if (iTemp != -1)
                {
                    if (mDoorTokensAlign[I] == TokenAlign.AlignRight)
                    {
                        // 把 iPos 到 iTemp 的文字取出
                        sSub = Address.Substring(iPos, iTemp - iPos + 1);
                        iPos = iTemp + 1;
                    }
                    else
                    {
                        // 把 iTemp 之後的文字取出
                        if (I == mDoorTokens.Count)
                        {
                            //直接取到最後
                            sSub = Address.Substring(iPos);
                        }
                        else
                        {
                            string tmpTokens = mDoorTokens[I + 1];
                            int iTemp2 = 0;

                            iTemp2 = AddressIndexOf(Address, tmpTokens, iTemp + 1);
                            if (iTemp2 != -1)
                            {
                                // 取出 iTemp 到 iTemp2 之間的文字
                                sSub = Address.Substring(iTemp, iTemp2 - iTemp);
                                iPos = iTemp2 - 1;
                            }
                            else
                            {
                                //直接取到最後
                                sSub = Address.Substring(iPos);
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(sSub))
                {
                    // 儲存到對應的變數
                    StoreDoorVariable(I, sSub);
                }
            }
        }

        private void StoreDoorVariable(int iStep, string sPart)
        {
            string str = null;
            string str2 = null;
            if (mDoorTokensAlign[iStep] == TokenAlign.AlignRight)
            {
                //str = sPart.Substring(sPart.Length - 1)
                switch (iStep)
                {
                        //Case 0
                        //    Me.mRoad = sPart
                        //Case 1
                        //    str = CNumToIntStr(sPart)
                        //    Me.mSec = str
                    case 0:
                        // 巷
                        str = CNumToIntStr(sPart);
                        this.Lane = str.Replace("巷", string.Empty);
                        break;
                    case 1:
                        // 弄
                        str = CNumToIntStr(sPart);
                        this.Alley = str.Replace("弄", string.Empty);
                        break;
                    case 2:
                        // 衖
                        str = CNumToIntStr(sPart);
                        this.SubAlley = str.Replace("衖", string.Empty);
                        break;
                    case 3:
                        // 號
                        int iTemp = sPart.IndexOf("之");
                        string sPart2 = null;
                        if (iTemp > 0)
                        {
                            sPart2 = sPart.Substring(0, iTemp);
                            str = CNumToIntStr(sPart2);
                            sPart2 = sPart.Substring(iTemp);
                            str2 = CNumToIntStr(sPart2);
                            //this.No = (str + str2).Replace("號", string.Empty);
                            this.No = str.Replace("號", string.Empty);
                            this.SubNo = str2.Replace("之", string.Empty).Replace("號", string.Empty);
                        }
                        else
                        {
                            //Me.mSubNo = str.Substring(0, str.Length - 1)
                            str = CNumToIntStr(sPart);
                            this.No = str.Replace("號", string.Empty);
                        }

                        No = Regex.Replace(No, "[^0-9]", string.Empty);
                        break;
                    case 4:
                        // 之
                        this.SubNo = sPart.Replace("之", string.Empty);
                        break;
                    case 5:
                        // 樓
                        this.Floor = sPart.Replace("樓", string.Empty);
                        break;
                    case 6:
                        // 之
                        this.SubFloor = sPart.Replace("之", string.Empty);
                        break;
                    case 7:
                        // 室
                        this.Room = sPart.Replace("室", string.Empty);
                        break;
                }
            }
        }

        private string ParseLongAddress(string Address)
        {
            string sTemp = Address.Trim();
            int iPos = 0;
            string sTokens = null;
            string sSub = null;

            for (int I = 0; I <= mAreaTokens.Count - 1; I++)
            {
                sSub = "";
                sTokens = mAreaTokens[I];
                int iTemp = 0;
                iTemp = I <= 3 ? TownIndexOf(Address, sTokens, iPos) : AddressIndexOf(Address, sTokens, iPos);
                if (iTemp != -1)
                {
                    if (mAreaTokensAlign[I] == TokenAlign.AlignRight)
                    {
                        // 把 iPos 到 iTemp 的文字取出
                        sSub = Address.Substring(iPos, iTemp - iPos + 1);
                        iPos = iTemp + 1;
                    }
                    else
                    {
                        // 把 iTemp 之後的文字取出
                        if (I == mAreaTokens.Count)
                        {
                            //直接取到最後
                            sSub = Address.Substring(iPos);
                        }
                        else
                        {
                            string tmpTokens = mAreaTokens[I + 1];
                            int iTemp2 = 0;

                            iTemp2 = AddressIndexOf(Address, tmpTokens, iTemp + 1);
                            if (iTemp2 != -1)
                            {
                                // 取出 iTemp 到 iTemp2 之間的文字
                                sSub = Address.Substring(iTemp, iTemp2 - iTemp);
                                iPos = iTemp2 - 1;
                            }
                            else
                            {
                                //直接取到最後
                                sSub = Address.Substring(iPos);
                            }
                        }
                    }
                }
                if (string.IsNullOrEmpty(sSub)) continue;
                // 儲存到對應的變數
                StoreAreaVariable(I, sSub);
            }
            return Address.Substring(iPos);
        }

        private void StoreAreaVariable(int iStep, string sPart)
        {
            string str = sPart.Trim();
            Regex zip3 = default(Regex);
            Regex zip5 = default(Regex);
            zip3 = new Regex("^[1-9][0-9]{2}", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            zip5 = new Regex("^[1-9][0-9]{4}", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            //If mAreaTokensAlign(iStep) = TokenAlign.AlignRight Then
            //str = sPart.Substring(sPart.Length - 1)
            switch (iStep)
            {
                case 0:
                    // 省
                    if (zip5.IsMatch(str))
                    {
                        // 有包含5碼郵遞區號
                        this.Zip = str.Substring(0, 5);
                        this.Province = str.Substring(5).Trim();
                    }
                    else if (zip3.IsMatch(str))
                    {
                        // 有包含3碼郵遞區號
                        this.Zip = str.Substring(0, 3);
                        this.Province = str.Substring(3).Trim();
                    }
                    else
                    {
                        this.Province = str;
                    }

                    break;
                case 1:
                    //縣市
                    if (zip5.IsMatch(str))
                    {
                        // 有包含5碼郵遞區號
                        this.Zip = str.Substring(0, 5);
                        this.City = str.Substring(5).Trim();
                    }
                    else if (zip3.IsMatch(str))
                    {
                        // 有包含3碼郵遞區號
                        this.Zip = str.Substring(0, 3);
                        this.City = str.Substring(3).Trim();
                    }
                    else
                    {
                        this.City = str;
                    }

                    break;
                case 2:
                    //鄉鎮市區
                    if (zip5.IsMatch(str))
                    {
                        // 有包含5碼郵遞區號
                        this.Zip = str.Substring(0, 5);
                        this.Town = str.Substring(5).Trim();
                    }
                    else if (zip3.IsMatch(str))
                    {
                        // 有包含3碼郵遞區號
                        this.Zip = str.Substring(0, 3);
                        this.Town = str.Substring(3).Trim();
                    }
                    else
                    {
                        this.Town = str;
                    }

                    break;
                case 3:
                    //村里
                    if (zip5.IsMatch(str))
                    {
                        // 有包含5碼郵遞區號
                        this.Zip = str.Substring(0, 5);
                        this.Village = str.Substring(5).Trim();
                    }
                    else if (zip3.IsMatch(str))
                    {
                        // 有包含3碼郵遞區號
                        this.Zip = str.Substring(0, 3);
                        this.Village = str.Substring(3).Trim();
                    }
                    else
                    {
                        this.Village = str;
                    }

                    //Dim regexp As Regex = New Regex("", RegexOptions.Singleline Or RegexOptions.IgnoreCase)
                    int iPos1 = 0;
                    int iPos2 = 0;
                    iPos1 = this.Village.IndexOf("路");
                    if (iPos1 == -1) iPos1 = this.Village.IndexOf("街");
                    if (iPos1 == -1) iPos1 = this.Village.IndexOf("道");
                    if (iPos1 > 0)
                    {
                        this.Road = this.Village.Substring(0, iPos1 + 1);
                        iPos2 = this.Village.IndexOf("段");
                        if (iPos2 > 0)
                        {
                            this.Sec = CNumToIntStr(this.Village.Substring(iPos1 + 1, iPos2 - iPos1));
                            this.Village = this.Village.Substring(iPos2 + 1);
                        }
                        else
                        {
                            this.Village = this.Village.Substring(iPos1 + 1);
                        }
                    }

                    if (zip5.IsMatch(this.Village))
                    {
                        // 有包含5碼郵遞區號
                        this.Zip = this.Village.Substring(0, 5);
                        this.Village = this.Village.Substring(5).Trim();
                    }
                    else if (zip3.IsMatch(this.Village))
                    {
                        // 有包含3碼郵遞區號
                        this.Zip = this.Village.Substring(0, 3);
                        this.Village = this.Village.Substring(3).Trim();
                    }

                    break;
                case 4:
                    //鄰
                    str = CNumToIntStr(sPart);
                    this.Lin = str;
                    break;
                case 5:
                    // 路街
                    if (zip5.IsMatch(str))
                    {
                        // 有包含5碼郵遞區號
                        this.Zip = str.Substring(0, 5);
                        this.Town = str.Substring(5).Trim();
                    }
                    else if (zip3.IsMatch(str))
                    {
                        // 有包含3碼郵遞區號
                        this.Zip = str.Substring(0, 3);
                        this.Road = str.Substring(3).Trim();
                    }
                    else
                    {
                        this.Road = str;
                    }

                    break;
                case 6:
                    // 段
                    str = CNumToIntStr(sPart);
                    this.Sec = str.Replace("段", string.Empty);
                    break;
            }
        }

        // 從 sComp 逐一取出字元, 找出在 sSource 中是否有相符
        private int AddressIndexOf(string sSource, string sComp, [System.Runtime.InteropServices.Optional, System.Runtime.InteropServices.DefaultParameterValue(0)] int iStart)
        {
            int iPos = 0;
            try
            {
                for (int I = 0; I <= sComp.Length - 1; I++)
                {
                    iPos = sSource.IndexOf(sComp[I], iStart);
                    if (iPos != -1)
                    {
                        return iPos;
                    }
                }
                return -1;
            }
            catch
            {
                return -1;
            }
        }

        private int TownIndexOf(string sSource, string sComp, [System.Runtime.InteropServices.Optional, System.Runtime.InteropServices.DefaultParameterValue(0)]  int iStart)
        {
            int temp = -1;
            int result = -1;
            int iPos = 0;
            try
            {
                for (int I = 0; I <= sComp.Length - 1; I++)
                {
                    iPos = sSource.IndexOf(sComp[I], iStart);
                    if (iPos != -1)
                    {
                        if (iPos - iStart < 2)
                        {
                            char ch = sSource[iPos + 1];
                            if (sComp.IndexOf(ch) != -1)
                            {
                                temp = iPos + 1;
                            }
                            else
                            {
                                temp = iPos;
                            }
                        }
                        else
                        {
                            temp = iPos;
                        }
                    }
                    if (result == -1 ||  result > temp)
                    {
                        result = temp;
                    }
                }
                return result;
            }
            catch
            {
                return -1;
            }
        }

        private int CDigitToNum(char cDigit)
        {
            char[] cBigNum = { '○', '一', '二', '三', '四', '五', '六', '七', '八', '九'
                             };
            int J = 0;
            for (J = 0; J <= cBigNum.Length - 1; J++)
            {
                if (cBigNum[J] == cDigit) break; // TODO: might not be correct. Was : Exit For

            }
            if (J == cBigNum.Length)
            {
                return -1;
            }
            return J;
        }

        private int DDigitToNum(char cDigit)
        {
            char[] cBigNum = { '０', '１', '２', '３', '４', '５', '６', '７', '８', '９'
                             };
            int j = 0;
            for (j = 0; j <= cBigNum.Length - 1; j++)
            {
                if (cBigNum[j] == cDigit) break; // TODO: might not be correct. Was : Exit For

            }
            if (j == cBigNum.Length)
            {
                return -1;
            }
            return j;
        }

        private string CNumToIntStr(string sSource)
        {
            string sResult = "";
            int I = 0;
            int Num = 0;
            bool bNum = false;

            while (I < sSource.Length)
            {
                int J = 0;
                J = CDigitToNum(sSource[I]);
                if (J < 0) J = DDigitToNum(sSource[I]);
                if (J >= 0)
                {
                    bNum = true;
                    if (I < sSource.Length - 1)
                    {
                        if (sSource[I + 1] == '十')
                        {
                            Num = J*10;
                            I += 2;
                            goto Cont;
                        }
                    }
                    if (I > 0)
                    {
                        if (sSource[I - 1] == '十')
                        {
                            Num += J;
                        }
                        else
                        {
                            Num = Num*10 + J;
                        }
                        I += 1;
                    }
                    else
                    {
                        Num = J;
                        I += 1;
                    }
                }
                else
                {
                    if (sSource[I] == '十')
                    {
                        bNum = true;
                        if (Num == 0)
                        {
                            Num = 10;
                        }
                        else
                        {
                            Num *= 10;
                        }
                    }
                    else
                    {
                        if (bNum)
                        {
                            sResult += Num.ToString() + sSource.Substring(I);
                            bNum = false;
                            break; // TODO: might not be correct. Was : Exit While
                        }
                        sResult += sSource[I];
                    }
                    I += 1;
                }
                Cont:
                ;
            }
            if (bNum)
            {
                sResult = Num.ToString();
            }
            return sResult;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Model
{
    public class SignInReplyData
    {
        public SignInReplyData(SignInReply reply , Member member ,SingleSignOnSource source , string message )
        {
            Reply = reply;
            SignInMember = member;
            SignOnSource = source;
            ReplyMessage = message;
            ExtId = string.Empty;
        }




        /// <summary>
        /// 登入結果
        /// </summary>
        public SignInReply Reply { get; set; }
        /// <summary>
        /// 登入會員資料
        /// </summary>
        public Member SignInMember { get; set; }
        /// <summary>
        /// 登入類型
        /// </summary>
        public SingleSignOnSource SignOnSource { get; set; }
        /// <summary>
        /// 串街用外部帳戶編號
        /// </summary>
        public string ExtId { get; set; }
        /// <summary>
        /// 回傳訊息
        /// </summary>
        public string ReplyMessage { get; set; }
    }

    public class VbsSignInReplyData
    {
        public VbsSignInReplyData(SignInReply reply , VbsMembership vbsMembership ,SingleSignOnSource source , string message )
        {
            Reply = reply;
            VbsSignInMembership = vbsMembership;
            SignOnSource = source;
            ReplyMessage = string.IsNullOrEmpty(message)?SignInReply.Success.ToString():message;
            ExtId = string.Empty;
        }

        /// <summary>
        /// 登入結果
        /// </summary>
        public SignInReply Reply { get; set; }
        /// <summary>
        /// 登入核銷系統會員資料
        /// </summary>
        public VbsMembership VbsSignInMembership { get; set; }
        /// <summary>
        /// 登入類型
        /// </summary>
        public SingleSignOnSource SignOnSource { get; set; }
        /// <summary>
        /// 串街用外部帳戶編號
        /// </summary>
        public string ExtId { get; set; }
        /// <summary>
        /// 回傳訊息
        /// </summary>
        public string ReplyMessage { get; set; }
    }
}

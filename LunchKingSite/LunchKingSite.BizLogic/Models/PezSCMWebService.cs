﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model
{
    public class SCMWebServiceLogin
    {
        public string clientId { get; set; }
        public string clientSecret { get; set; }
    }

    public class SCMWebServiceMerchant
    {
        public string usrNum { get; set; }
        public string venNum { get; set; }
    }

    public class SCMWebServiceReturnGoods
    {
        public SCMWebServiceMerchant merchant { get; set; }
        public ReturnGoodsData data { get; set; }
    }

    

    public class ReturnGoodsData
    {
        public string ordId { get; set; }
    }

    public class SCMWebServiceResult
    {
        public int timestamp { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public string path { get; set; }
        public bool success { get; set; }
    }

    public class SCMWebServiceSupplierData
    {
        public string shipStartDate { get; set; }
        public string shipEndDate { get; set; }
        public int dataIndex { get; set; }
        public string ordId { get; set; }
        public string orderKindType { get; set; }
        public string orderType { get; set; }
        public string orderDealType { get; set; }
    }

    public class SCMWebServiceSupplier
    {
        public SCMWebServiceMerchant merchant { get; set; }
        public SCMWebServiceSupplierData data { get; set; }
    }

    public class SCMWebServiceSupplierResult
    {
        public bool success { get; set; }
        public int totalSize { get; set; }
        public List<SCMWebServiceSupplierDataResult> dataResults { get; set; }
    }

    public class SCMWebServiceSupplierDataResult
    {
        public string orderStatusType { get; set; }
        public string shipDate { get; set; }
        public string ordId { get; set; }
        public bool act { get; set; }
        public string actName { get; set; }
        public List<SCMWebServiceSupplierDataResultItem> supplierItemResults { get; set; }
        public string osiCname { get; set; }
        public string osiCzip { get; set; }
        public string osiCity { get; set; }
        public string osiCaddr { get; set; }
        public string osiCdtel { get; set; }
        public string osiCntel { get; set; }
        public string osiCmtel { get; set; }
        public string osiRemark { get; set; }
        public string packageNo { get; set; }
        public int? srpCarriertype { get; set; }
        public string srpRemark { get; set; }
        public int ordAccountPayment { get; set; }
        public int ordGoodsPayment { get; set; }
        public string orderKindType { get; set; }
    }

    public class SCMWebServiceSupplierDataResultItem
    {
        public int proNum { get; set; }
        public string pidName { get; set; }
        public string proQc { get; set; }
        public int iteQty { get; set; }
        public List<string> combineChildrens { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Models.MGM;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class CardMessageData
    {
        public CardData CardData { get; set; }
        public string ReceiveName { get; set; }
    }

    public class CardData
    {
        public int Id { get; set; }
        public int SenderId { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
        public DateTime SendTime { get; set; }
        public DateTime? ReadTime { get; set; }
        public int? MessageType { get; set; }
        public int? ReceiverId { get; set; }
        public DateTime? ReceiveTime { get; set; }
        public bool? IsDeleted { get; set; }
        public int? ReferMessageId { get; set; }
        public Guid? ReferTargetGuid { get; set; }
        public string SenderName { get; set; }
        public TemplateCard TemplateCard { get; set; }
    }
}

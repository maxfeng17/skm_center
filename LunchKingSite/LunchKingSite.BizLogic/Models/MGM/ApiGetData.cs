﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class MgmGetData
    {
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("userId")]
        public string UserId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("asCode")]
        public string AsCode { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("msgId")]
        public int MsgId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("msg")]
        public string Msg { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("sendName")]
        public string SendName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("cardStyleId")]
        public int CardStyleId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("type")]
        public int Type { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("getAll")]
        public bool GetAll { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("page")]
        public int Page { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("sendTime")]
        public string SendTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("orderGuid")]
        public Guid OrderGuid { get; set; }

    }
}

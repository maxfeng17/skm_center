﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class WhereItemList
    {
        public GiftData GiftData { get; set; }
        public int GiftId { get; set; }
        public string SenderName { get; set; }
        public int SendMsgId { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal ItemOrigPrice { get; set; }
        public string DiscountString { get; set; }
        public List<WhereItem> ItemList { get; set; }

        public WhereItemList()
        {
            GiftData = new GiftData();
            GiftId = 0;
            SenderName = string.Empty;
            SendMsgId = 0;
            ItemPrice = 0;
            ItemOrigPrice = 0;
            DiscountString = string.Empty;
            ItemList = new List<WhereItem>();
        }
    }
}

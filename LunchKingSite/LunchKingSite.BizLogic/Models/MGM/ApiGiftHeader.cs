﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class ApiGiftHeader
    {
        public string Content { get; set; }
        public string TextColor { get; set; }
        public string ImageUrl { get; set; }
    }
}

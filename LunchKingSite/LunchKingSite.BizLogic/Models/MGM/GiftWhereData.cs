﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class GiftWhereData
    {
        public GiftData GiftData { get; set; }
        public string OrderId { get; set; }
        public Guid OrderGuid { get; set; }
        public string ReceiverStr { get; set; }
        public int NotReceiveCount { get; set; }
        public DateTime SendTime { get; set; }
        public DateTime ReceiveTime { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class SendGiftData
    {
        public GiftData GiftData { get; set; }
        public int RemainCount { get; set; }
        public decimal OrderAmount { get; set; }
        public string OrderId { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal ItemOrigPrice { get; set; }
        public string DiscountString { get; set; }
    }
}

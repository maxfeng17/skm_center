﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class ApiReturnGiftList
    {
        /// <summary>
        /// 
        /// </summary>
        public List<SendGiftData> GiftList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalCount { get; set; }

    }
}

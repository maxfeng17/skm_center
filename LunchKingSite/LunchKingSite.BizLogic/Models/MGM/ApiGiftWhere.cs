﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class ApiGiftWhere
    {
        public GiftData GiftData { get; set; }
        public List<ApiReceive> ReceiverList { get; set; }
        public int ReceiveCount { get; set; }
        public DateTime SendTime { get; set; }
        public Guid OrderGuid { get; set; }
        public ApiGiftWhere()
        {
            GiftData = new GiftData();
            ReceiverList = new List<ApiReceive>();
            ReceiveCount = 0;
            SendTime = DateTime.Now;
            OrderGuid = Guid.Empty;
        }
    }
    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class GiftData
    {
        public Guid Bid { get; set; }
        public string PicUrl { get; set; }
        public string AppPicUrl { get; set; }
        public string ItemName { get; set; }
        public string Name { get; set; }
        public DateTime OrderTimeE { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class ApiReceive
    {
        public string Name { get; set; }
        public string ImgUrl { get; set; }
        public string AccountNumber { get; set; }
        public ApiReceive()
        {
            Name = string.Empty;
            ImgUrl = string.Empty;
            AccountNumber = string.Empty;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class GiftCardListData
    {
        public string ImageType { get; set; }
        public string ReceiverName { get; set; }
        public string SenderName { get; set; }
        public string SendTime { get; set; }
        public string Message { get; set; }
        public int MessageType { get; set; }
        public bool IsRead { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class GiftListData
    {
        public GiftData GiftData { get; set; }
        public int GiftId { get; set; }
        public string SenderName { get; set; }
        public int SendMsgId { get; set; }
        public int? ReplyMsgId { get; set; }
        public Guid? AccessCode { get; set; }
        public DateTime SendTime { get; set; }
        public int Accept { get; set; }
        public bool IsUsed { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Models.MGM
{
    /// <summary>
    /// 禮物明細
    /// </summary>
    public class ApiGiftItem
    {
        public int Id { get; set; }
        public string UseStartDate { get; set; }
        public string UseEndDate { get; set; }
        public string CouponName { get; set; }
        public Guid CouponBid { get; set; }
        public Guid OrderGuid { get; set; }
        public int CouponType { get; set; }
        public int GroupCouponType { get; set; }
        public string MainImagePath { get; set; }
        public int GroupCouponAppChannel { get; set; }
        public List<ApiPponStore> Availabilities { get; set; }
        public List<ApiCouponData> CouponList { get; set; }
        /// <summary>
        /// 核銷方式
        /// </summary>
        public int VerifyType { get; set; }
    }
}
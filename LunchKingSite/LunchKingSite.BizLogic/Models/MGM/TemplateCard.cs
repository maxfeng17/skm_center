﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.MGM
{
    public class TemplateCard
    {
        public int Id { get; set; }
        public string ImagePic { get; set; }
        public string Title { get; set; }
        public string BackgroundColor { get; set; }
    }
}

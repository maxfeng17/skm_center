﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class GiftCard
    {

        public string Message { get; set; }
        public int ReceiverId { get; set; }
        public int SenderId { get; set; }
        public string SenderName { get; set; }
        public int CardStyleId { get; set; }
        public string OrderId { get; set; }
        public IViewPponDeal Deal { get; set; }
        public string CouponUsage { get; set; }
        public List<MatchMember> MatchMembers { get; set; }
        public List<MatchMemberGroup> MatchMemberGroup { get; set; }
    }

    /// <summary>
    /// 聯絡方式 0:email 1:手機 2:FB 
    /// AccessCode:禮物連結
    /// </summary>
    public class MatchMember
    {
        public string Name { get; set; }
        public int Type { get; set; }
        public string Value { get; set; }
    }

    public class MatchMemberGroup
    {
        public string Name { get; set; }
        public int Type { get; set; }
        public string Value { get; set; }
        public int Count { get; set; }
        public string AccessCode { get; set; }
    }
    
}

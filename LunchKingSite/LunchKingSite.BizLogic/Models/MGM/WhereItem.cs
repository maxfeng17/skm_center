﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class WhereItem
    {
        public string ReceiverName { get; set; }
        public string MatchValue { get; set; }
        public int? CouponId { get; set; }
        public string Code { get; set; }
        public string CouponSequence { get; set; }
        public DateTime? ReceiveTime { get; set; }
        public DateTime? UsedTime { get; set; }
        public int Accept { get; set; }
        public int? ReplyMsgId { get; set; }
    }
}

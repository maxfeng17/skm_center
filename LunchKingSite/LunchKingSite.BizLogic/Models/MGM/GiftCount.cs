﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class GiftCount
    {
        public int NewGiftCount { get; set; }
        public int ReplyCardCount { get; set; }
        public int AsCount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class ApiGiftWhereItem
    {
        public GiftData GiftData { get; set; }
        public int GiftId { get; set; }
        public int SendMsgId { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal ItemOrigPrice { get; set; }
        public string DiscountString { get; set; }
        public List<ApiWhereItem> ReceiverList { get; set; }

        public ApiGiftWhereItem()
        {
            GiftData = new GiftData();
            GiftId = 0;
            SendMsgId = 0;
            ItemPrice = 0;
            ItemOrigPrice = 0;
            DiscountString = string.Empty;
            ReceiverList = new List<ApiWhereItem>();
        }
    }

    public class ApiWhereItem
    {
        public ApiReceive Receive { get; set; }
        public int? CouponId { get; set; }
        public string CouponSequence { get; set; }
        public DateTime? ReceiveTime { get; set; }
        public DateTime? UsedTime { get; set; }
        public int Accept { get; set; }
        public int? ReplyMsgId { get; set; }
        public string AsCode { get; set; }
        

        public ApiWhereItem()
        {
            Receive= new ApiReceive();
            CouponSequence = string.Empty;
            Accept = 0;
        }
    }

    public class ApiBarCode
    {
        public string Image { get; set; }
        public string IsReceive { get; set; }
        public string IsUsed { get; set; }
        public string UsedTime { get; set; }
        public string ReceiveTime { get; set; }
        public string SequenceNumber { get; set; }
        public string CouponCode { get; set; }
        public string CodeType { get; set; }


    }
}

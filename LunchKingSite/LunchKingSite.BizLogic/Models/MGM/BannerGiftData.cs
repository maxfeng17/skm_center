﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.MGM
{
    public class BannerGiftData
    {
        public string OrderId { get; set; }
        public int GiftId { get; set; }
        public Guid Bid { get; set; }
        public int PponCount { get; set; }
        public string PponTitle { get; set; }
        public string PponContent { get; set; }
        public string SenderName { get; set; }
        public DateTime SendTime { get; set; }
        public string PponImg { get; set; }
    }
}

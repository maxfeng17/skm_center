﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Model
{
    public class ComboData
    {
        [JsonProperty("mainDeal")]
        public ComboItem MainDeal { get; set; }
        [JsonProperty("subDeals")]
        public List<ComboItem> SubDeals { get; set; }
    }
    public class ComboItem
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("bid")]
        public Guid Bid { get; set; }

        public override string ToString()
        {
            return Title;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            ComboItem castObj = obj as ComboItem;
            if (castObj == null) return false;
            return this.GetHashCode() == castObj.GetHashCode();
        }

        public override int GetHashCode()
        {
            return string.Format("{0}{1}", Bid, Title).GetHashCode();
        }
    }
}

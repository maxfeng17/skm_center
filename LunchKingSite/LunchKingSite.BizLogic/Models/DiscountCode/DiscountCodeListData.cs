﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Models.DiscountCode
{
    public class DiscountCodeListData
    {
        public string EventName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Page { get; set; }
        
    }

}

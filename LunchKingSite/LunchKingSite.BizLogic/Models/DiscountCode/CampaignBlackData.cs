﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.DiscountCode
{
    public class CampaignBlackData
    {
        public int BlackDealType { get; set; }
        public int ChannelType { get; set; }
        public int SearchType { get; set; }
        public string SearchData { get; set; }
    }
}

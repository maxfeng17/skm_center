﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Models.DiscountCode
{
    public class CreateDiscountCodeData
    {

        public string EventName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime EventDateS { get; set; }
        public DateTime EventDateE { get; set; }
        public int DiscountAmount { get; set; }
        public int MinimumAmount { get; set; }
        public int Quantity { get; set; }
        public int FreeGiftDiscount { get; set; }
        public int MinimumGrossMargin { get; set; }
        public int DiscountType { get; set; }
        public int DiscountGiftType { get; set; }
        public int UseOnlyType { get; set; }
        public int AppOnly { get; set; }
        public int CanReused { get; set; }
        public int ChannelType { get; set; }
        public string CategorieId { get; set; }
        public string EventId { get; set; }
        public string BrandId { get; set; }
        public string CustomCode { get; set; }
        /// <summary>
        /// 特別指定，提供前臺最低均價計算
        /// </summary>
        public bool SpecifiedForDiscountPrice { get; set; }
    }

}

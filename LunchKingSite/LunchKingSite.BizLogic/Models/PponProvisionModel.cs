﻿using System.Collections.Generic;
using Vodka.Mongo;

namespace LunchKingSite.BizLogic.Model
{
    /// <summary>
    /// 館別
    /// </summary>
    public class ProvisionDepartmentModel
    {
        public string Id { get; set; }
        public List<ProvisionCategoryModel> Categorys { get; set; }
        public string Name { get; set; }

        public ProvisionDepartmentModel()
        {
            Categorys = new List<ProvisionCategoryModel>();
        }
    }

    /// <summary>
    /// 類別
    /// </summary>
    public class ProvisionCategoryModel
    {
        public string Id { get; set; }
        public List<ProvisionItemModel> Items { get; set; }
        public string Name { get; set; }
        public int Rank { get; set; }

        public ProvisionCategoryModel()
        {
            Items = new List<ProvisionItemModel>();
        }
    }

    /// <summary>
    /// 項目
    /// </summary>
    public class ProvisionItemModel
    {
        public string Id { get; set; }
        public List<ProvisionContentModel> Contents { get; set; }
        public string Name { get; set; }
        public int Rank { get; set; }

        public ProvisionItemModel()
        {
            Contents = new List<ProvisionContentModel>();
        }
    }

    /// <summary>
    /// 內容
    /// </summary>
    public class ProvisionContentModel
    {
        public string Id { get; set; }
        public List<ProvisionDescModel> ProvisionDescs { get; set; }
        public string Name { get; set; }
        public int Rank { get; set; }

        public ProvisionContentModel()
        {
            ProvisionDescs = new List<ProvisionDescModel>();
        }
    }

    /// <summary>
    /// 服務條款
    /// </summary>
    public class ProvisionDescModel
    {
        public string Id { get; set; }
        public ProvisionDescStatus Status { get; set; }
        public string Description { get; set; }
    }
}

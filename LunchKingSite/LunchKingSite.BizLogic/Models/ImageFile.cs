﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model
{
    public class ImageFile
    {
        public string FileName { get; set; }
        public string Url { get; set; }
        public DateTime AcessTime { get; set; }
    }
}

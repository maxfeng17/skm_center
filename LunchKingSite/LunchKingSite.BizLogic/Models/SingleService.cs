﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Interface;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using System.Threading;

namespace LunchKingSite.BizLogic.Model
{
    public class SingleService : ISingleService
    {
        public void GenCouponService(ViewPponDeal i_PponDeal, ViewPponCouponCollection i_CC, GroupOrder i_GO, Guid i_Guid, Boolean i_Atm, String i_Hami)
        {
            var data = new Dictionary<string, object>
                        {
                            {"i_Deal", i_PponDeal},
                            {"i_CouponList", i_CC},
                            {"i_GroupOrder", i_GO},
                            {"i_OrderGuid", i_Guid},
                            {"i_Atm", i_Atm},
                            {"i_Hami", i_Hami}
                        };
            //var workflowApplication = new WorkflowApplication(new GenCoupon(), data);
            //workflowApplication.SynchronizationContext = new SynchronizationContext();
            //workflowApplication.Run();
        }

        public bool HiDealSetCouponDataService(HiDealOrder order, HiDealOrderDetail orderDetail, string memberUserName)
        {
            return HiDealCouponManager.SetCouponData(order, orderDetail, memberUserName);
        }
    }
}

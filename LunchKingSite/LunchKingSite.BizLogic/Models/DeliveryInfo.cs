﻿using System;
using LunchKingSite.Core;

namespace LunchKingSite.BizLogic.Model
{
    public sealed class DeliveryInfo
    {
        /// <summary>
        /// 取貨方式
        /// </summary>
        public ProductDeliveryType ProductDeliveryType { get; set; }
        /// <summary>
        /// 收件人id
        /// </summary>
        public int DeliveryId { get; set; }
        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string CustomerName { get; set; }
        /// <summary>
        /// 收件人電話
        /// </summary>
        public string CustomerMobile { get; set; }
        /// <summary>
        /// 收件人地址
        /// </summary>
        public string DeliveryAddress { get; set; }
        /// <summary>
        /// 超取店舖資訊
        /// </summary>
        public PickupStore PickupStore { get; set; }

        public string CustomerLandLine { get; set; }
        public string LandLineExtension { get; set; }
        public int DeliveryCharge { get; set; }

        public DeliveryInfo() { }
        public DeliveryInfo(string mobile, string address, string landline, string landlineExt, int deliveryCharge = 0)
        {
            CustomerMobile = mobile;
            DeliveryAddress = address;
            CustomerLandLine = landline;
            LandLineExtension = landlineExt;
            DeliveryCharge = deliveryCharge;
        }

        public DeliveryInfo(string name, string mobile, string address, int deliveryCharge = 0)
        {
            CustomerName = name;
            CustomerMobile = mobile;
            DeliveryAddress = address;
            DeliveryCharge = deliveryCharge;
        }
    }
    
    public class PickupStore
    {
        /// <summary>
        /// 店舖代碼
        /// </summary>
        public string StoreId { get; set; }
        /// <summary>
        /// 店舖名稱
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 店舖電話
        /// </summary>
        public string StoreTel { get; set; }
        /// <summary>
        /// 店舖地址
        /// </summary>
        public string StoreAddr { get; set; }

        public PickupStore()
        {
            this.StoreId = string.Empty;
            this.StoreName = string.Empty;
            this.StoreTel = string.Empty;
            this.StoreAddr = string.Empty;
        }
    }

    /// <summary>
    /// buy頁使用，收集從電子地圖帶回來的超取分店資料
    /// </summary>
    public class PponPickupStore : PickupStore
    {
        public ProductDeliveryType ProductDeliveryType { get; set; }
    }
    /// <summary>
    /// buy頁使用，帶入會員預設(上一次)的商取分店資料
    /// </summary>
    public class PponPickupStores
    {
        public PickupStore FamilyStore { get; set; }
        public PickupStore SevenStore { get; set; }
    }
}

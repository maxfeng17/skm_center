﻿using System;
using LunchKingSite.Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Model.BookingSystem
{
    [Serializable]
    public class ReserveCouponInfo
    {
        public int CouponId { get; set; }
        public string CouponSequence { get; set; }
        public string CouponUsage { get; set; }//訂單短標
        public string EventName { get; set; }
        public string MemberName { get; set; }
        public bool IsReservatoinLock { get; set; }
        public BusinessModel DealType { get; set; }        
        public DateTime? LastModifityTime { get; set; }
        public ReserveCouponStatus ReserveCouponStatus { get; set; }
        public int DealUniqueId { get; set; }
        public DateTime? CouponReservationDate { get; set; }
        public DateTime ExchangePeriodStartTime { get; set; }
        public DateTime ExchangePeriodEndTime { get; set; }
    }

    public enum ReserveCouponStatus
    {
        Valid = 0,
        Lock = 1,
        UnLock = 2,
        Returning = 3,
        Returned = 4,
        Invalid = 5,
    }
}

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Model
{
    public class Creditcard
    {
        private const string TimeFormat = @"{0:yyyy/MM/dd HH:mm}";
        private readonly static IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private readonly static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private readonly static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        #region 物件變數

        public int Id { get; set; }

        /// <summary>
        /// 是否照會銀行
        /// </summary>
        public bool IsReference { get; set; }

        /// <summary>
        /// 信用卡片到期日
        /// </summary>
        public string ExpDate { get; set; }

        /// <summary>
        /// 會員姓名
        /// </summary>
        public string MemberName { get; set; }

        /// <summary>
        /// 會員電話
        /// </summary>
        public string MemberPhone { get; set; }

        /// <summary>
        /// 會員手機
        /// </summary>
        public string MemberMobile { get; set; }
        /// <summary>
        /// 外送地址
        /// </summary>
        public string DeliveryAddress { get; set; }

        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderGuid { get; set; }

        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName { get; set; }

        /// <summary>
        /// 訂單金額
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// IP
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// 此卡號是否為黑名單
        /// </summary>
        public bool IsLocked { get; set; }

        /// <summary>
        /// 訂單時間
        /// </summary>
        public string OrderTime { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// 檔名
        /// </summary>
        public string ItemName { get; set; }

        /// <summary>
        /// 此會員是否被設定為黑名單
        /// </summary>
        public bool IsMemBlacklist { get; set; }

        /// <summary>
        /// 新信用卡號
        /// </summary>
        public string CreditcardInfo { get; set; }

        /// <summary>
        /// 黑名單類型
        /// </summary>
        public int? BlackListType { get; set; }

        /// <summary>
        /// 加入人員
        /// </summary>
        public string CreateId { get; set; }

        /// <summary>
        /// 最後修改時間
        /// </summary>
        public string LastTime { get; set; }

        /// <summary>
        /// 最後修改ID
        /// </summary>
        public string LastID { get; set; }

        #endregion 物件變數

        public Creditcard()
        {
        }

        #region method

        /// <summary>
        /// 將訂單時間設定為文字格式
        /// </summary>
        /// <param name="dtOrderTime">給予日期格式，輸出成字串</param>
        /// <returns></returns>
        public void SetOrderTime(DateTime dtOrderTime)
        {
            OrderTime = string.Format(TimeFormat, dtOrderTime);
        }

        /// <summary>
        /// 給予銀行端之回傳結果設定狀態
        /// </summary>
        /// <param name="iResult">int, 銀行端傳回結果</param>
        public void SetResult(int iResult)
        {
            CreditcardProcessor cp = new CreditcardProcessor();
            Result = cp.GetResultName(iResult);
        }

        public static List<Creditcard> GetCreditcardList(CreditcardOrderCollection objList)
        {
            Security sec = new Security(SymmetricCryptoServiceProvider.AES);
            List<Creditcard> rtnList = new List<Creditcard>();
            foreach (CreditcardOrder info in objList)
            {
                Creditcard creditcard = new Creditcard();
                Member mem = _mp.MemberGetbyUniqueId(info.MemberId);
                Order order = _op.OrderGet(info.OrderGuid);
                ViewPponDealCollection deals = _pp.ViewPponDealGetListPaging(1, -1, ViewPponDeal.Columns.BusinessHourOrderTimeE,
                            ViewPponDeal.Columns.OrderGuid + " = " + order.ParentOrderId);
                string fastget = "";
                if (deals != null && deals.Count>0)
                {
                    if (!string.IsNullOrWhiteSpace(deals[0].LabelIconList))
                    {
                        foreach (var s in deals[0].LabelIconList.Split(','))
                        {
                            if (s == ((int)DealLabelSystemCode.ArriveIn24Hrs).ToString() ||
                                s == ((int)DealLabelSystemCode.ArriveIn72Hrs).ToString())
                            {
                                fastget += string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(Convert.ToInt32(s)));
                            }
                        }
                    }
                }

                creditcard.Id = info.Id;
                creditcard.IsReference = info.IsReference;
                creditcard.ExpDate = info.ExpDate;
                creditcard.MemberName = mem.DisplayName;
                creditcard.MemberMobile = mem.CompanyTel;
                creditcard.MemberMobile = mem.Mobile;
                creditcard.DeliveryAddress = order.DeliveryAddress;
                creditcard.OrderGuid = order.OrderId;
                creditcard.OrderTime = string.Format(TimeFormat, info.CreateTime);
                creditcard.SellerName = order.SellerName;
                creditcard.Amount = info.CreditcardAmount;
                creditcard.SetResult(info.Result);
                creditcard.ItemName = fastget + (deals.Count > 0 ? deals[0].ItemName : "");
                creditcard.IP = info.ConsumerIp;
                creditcard.IsLocked = info.IsLocked;
                creditcard.IsMemBlacklist = Helper.IsFlagSet(mem.Status, (int)MemberStatusFlag.IsBlacklisted);
                creditcard.CreditcardInfo = sec.Decrypt(info.CreditcardInfo);
                creditcard.BlackListType = info.BlackListType;
                creditcard.CreateId = info.CreateId;

                if (info.CreateTime != null && info.LockedTime == null && info.UnlockedTime == null &&
                    info.CreateTime.ToString().Trim() != "" && info.LockedTime.ToString().Trim() == "" && info.UnlockedTime.ToString().Trim() == "")
                {
                    creditcard.LastTime = info.CreateTime.ToString().Trim();
                    creditcard.LastID = info.CreateId;
                }
                else if (info.CreateTime != null && info.LockedTime != null && info.UnlockedTime == null &&
                   info.CreateTime.ToString().Trim() != "" && info.LockedTime.ToString().Trim() != "" && info.UnlockedTime.ToString().Trim() == "")
                {
                    if (info.CreateTime >= info.LockedTime)
                    {
                        creditcard.LastTime = info.CreateTime.ToString().Trim();
                        creditcard.LastID = info.CreateId;
                    }
                    else
                    {
                        creditcard.LastTime = info.LockedTime.ToString().Trim();
                        creditcard.LastID = info.LockerId;
                    }
                }
                else if (info.CreateTime != null && info.LockedTime == null && info.UnlockedTime != null &&
                    info.CreateTime.ToString().Trim() != "" && info.LockedTime.ToString().Trim() == "" && info.UnlockedTime.ToString().Trim() != "")
                {
                    if (info.CreateTime >= info.UnlockedTime)
                    {
                        creditcard.LastTime = info.CreateTime.ToString().Trim();
                        creditcard.LastID = info.CreateId;
                    }
                    else
                    {
                        creditcard.LastTime = info.UnlockedTime.ToString().Trim();
                        creditcard.LastID = info.UnlockerId;
                    }
                }
                else if (info.CreateTime != null && info.LockedTime != null && info.UnlockedTime != null &&
                    info.CreateTime.ToString().Trim() != "" && info.LockedTime.ToString().Trim() != "" && info.UnlockedTime.ToString().Trim() != "")
                {
                    if ((info.CreateTime >= info.LockedTime) && (info.CreateTime >= info.UnlockedTime))
                    {
                        creditcard.LastTime = info.CreateTime.ToString().Trim();
                        creditcard.LastID = info.CreateId;
                    }
                    else if ((info.LockedTime >= info.CreateTime) && (info.LockedTime >= info.UnlockedTime)) 
                    {
                        creditcard.LastTime = info.LockedTime.ToString().Trim();
                        creditcard.LastID = info.LockerId;
                    }
                    else if ((info.UnlockedTime >= info.CreateTime) && (info.UnlockedTime >= info.LockedTime))
                    {
                        creditcard.LastTime = info.UnlockedTime.ToString().Trim();
                        creditcard.LastID = info.UnlockerId;
                    }
                }
                else
                {
                    creditcard.LastTime = info.CreateTime.ToString().Trim();
                    creditcard.LastID = info.CreateId;
                }

                if (creditcard.Result != "未定義狀態")
                {
                    rtnList.Add(creditcard);
                }
            }

            return rtnList;
        }

        #endregion method
    }

    public class ObjectCreditcardResult
    {
        /// <summary>
        /// 編號
        /// </summary>
        public int Id;

        /// <summary>
        /// 回傳原因(英文)
        /// </summary>
        public string Name;

        /// <summary>
        /// 回傳說明(中文)
        /// </summary>
        public string Title;
    }

    public class CreditcardFraudData
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Ip { get; set; }
        public string Address { get; set; }
        public string Type { get; set; }
        public string CardNumber { get; set; }
        public string CreateId { get; set; }
        public string CreateTime { get; set; }
        public string ModifyId { get; set; }
        public string ModifyTime { get; set; }
        public string RelateTime { get; set; }
    }

    public class CheckAccount
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string CreateId { get; set; }
        public string CreateTime { get; set; }
    }
}
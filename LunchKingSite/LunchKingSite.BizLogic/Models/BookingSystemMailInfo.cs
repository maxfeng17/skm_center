﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Model
{
    public class BookingSystemMailInfo
    {
    }

    #region 通知信

    #region 訂位成功通知信變數

    public class ReservationSuccessMailInfomation
    {
        public string MemberName { get; set; }
        public DateTime ReservationDate { get; set; }
        public DateTime ReservationTimeSlot { get; set; }
        public int NumberOfPeople { get; set; }
        public string MemberEmail { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string Remark { get; set; }
        public List<string> CouponsSequence { get; set; }
        public string CouponInfo { get; set; }
        public string CouponLink { get; set; }
        public string SellerName { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
    }

    #endregion 訂位成功通知信變數

    #region 訂房成功通知信變數

    public class TravelSuccessMailInfomation
    {
        public string MemberName { get; set; }
        public DateTime ReservationDate { get; set; }
        public int NumberOfPeople { get; set; }
        public string MemberEmail { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string Remark { get; set; }
        public List<string> CouponsSequence { get; set; }
        public string CouponInfo { get; set; }
        public string CouponLink { get; set; }
        public string SellerName { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
    }

    #endregion 訂房成功通知信變數

    #region 預約取消通知信

    public class ReservationCancelMailInfomation
    {
        public string MemberName { get; set; }
        public string SellerName { get; set; }
        public string StoreName { get; set; }
    }

    #endregion 預約取消通知信

    #region 訂位提醒通知信

    public class ReservationRemindMailInfomation
    {
        public string MemberName { get; set; }
        public DateTime ReservationDate { get; set; }
        public DateTime ReservationTimeSlot { get; set; }
        public int NumberOfPeople { get; set; }
        public string MemberEmail { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string Remark { get; set; }
        public List<string> CouponsSequence { get; set; }
        public string CouponInfo { get; set; }
        public string CouponLink { get; set; }
        public string SellerName { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
    }


    #endregion 訂位提醒通知信

    #region 訂房提醒通知信

    public class TravelRemindMailInfomation
    {
        public string MemberName { get; set; }
        public DateTime ReservationDate { get; set; }
        public int NumberOfPeople { get; set; }
        public string MemberEmail { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string Remark { get; set; }
        public List<string> CouponsSequence { get; set; }
        public string CouponInfo { get; set; }
        public string CouponLink { get; set; }
        public string SellerName { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
    }

    #endregion 訂房提醒通知信

    #region 變動通知信

    public class BookingSettingChangeLogMailInfomation
    {
        public string VbsAccount { get; set; }
        public string SellerName { get; set; }
        public string StoreName { get; set; }
        public int BookingSystemStoreId { get; set; }
        public int BookingSystemDateId { get; set; }
        public DateTime ModifyTime { get; set; }
        public List<KeyValuePair<string, string>> ChangeSettingLogs { get; set; }
    }

    #endregion 變動通知信

    #endregion 通知信
}

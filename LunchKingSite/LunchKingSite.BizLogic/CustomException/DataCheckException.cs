﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.CustomException
{
    public class DataCheckException<T> : Exception where T:struct
    {
        public T ErrorType { get; private set; }
        public DataCheckException(string message, T eEnum)
            : base(message)
        {
            ErrorType = eEnum;
        }
    }
}

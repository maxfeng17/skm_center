﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Xml.Xsl;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using Winnovative.WnvHtmlConvert;
using log4net;
using SubSonic;

namespace LunchKingSite.BizLogic.Facade
{
    public class HiDealMailFacade
    {
        private static readonly ILog _log;
        private static readonly ISysConfProvider _config;
        private static readonly IHiDealProvider _hdp;
        private static readonly IMemberProvider _mp;
        private static readonly HiDealMailAgent _hdma;

        private static readonly string _unSubscription =
            @"                                若您不希望再收到此訊息，請按此<a href=""%%UNSUBSCRIPTBE%%"" target=""_blank"" style=""color: #FC7D49;"">取消訂閱</a>。";

        static HiDealMailFacade()
        {
            _log = LogManager.GetLogger(typeof(HiDealMailFacade));
            _config = ProviderFactory.Instance().GetConfig();
            _hdp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _hdma = new HiDealMailAgent();
        }

        #region 通知信

        #region 退貨成功通知信

        public static bool SendRefundSuccessNotification(string emailTo, RefundSuccessNotificationInformation info)
        {
            try
            {
                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetHiDealNotificationXsl(PiinlifeNotificationType.RefundSuccessNotification);
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("ServiceCenterUrl", "", _config.HiDealServiceCenterUrl);
                transformer.XslParameters = args;

                string msgBody = transformer.Transform();
                _hdma.SendWithPostMan(_config.PiinlifeServiceEmail, emailTo, "退貨成功通知", msgBody, _config.HiDealServiceName);
            }
            catch (Exception e)
            {
                string errMsg = string.Format("寄給 {0} 的系統通知信 (退貨成功通知) 發送失敗.", emailTo);
                _log.Error(errMsg, e);
                return false;
            }

            return true;
        }

        #endregion

        #region 退貨退款失敗通知信

        public static bool SendRefundFailureNotification(string emailTo, RefundFailureNotificationInformation info)
        {
            try
            {
                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetHiDealNotificationXsl(PiinlifeNotificationType.RefundFailureNotification);
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("ServiceCenterUrl", "", _config.HiDealServiceCenterUrl);
                transformer.XslParameters = args;

                string msgBody = transformer.Transform();
                _hdma.SendWithPostMan(_config.PiinlifeServiceEmail, emailTo, "退貨退款失敗通知", msgBody, _config.HiDealServiceName);
            }
            catch (Exception e)
            {
                string errMsg = string.Format("寄給 {0} 的系統通知信 (退貨退款失敗通知) 發送失敗.", emailTo);
                _log.Error(errMsg, e);
                return false;
            }

            return true;
        }

        #endregion

        #region 退貨通知書
        public static bool SendRefundApplicationFormNotification(string emailTo, RefundApplicationFormNotificationInformation info)
        {
            try
            {
                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetHiDealNotificationXsl(PiinlifeNotificationType.RefundApplicationFormNotification);
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("ServiceCenterUrl", "", _config.HiDealServiceCenterUrl);
                transformer.XslParameters = args;

                string msgBody = transformer.Transform();
                _hdma.SendWithPostMan(_config.PiinlifeServiceEmail, emailTo, "退貨申請書", msgBody, _config.HiDealServiceName);
            }
            catch (Exception e)
            {
                string errMsg = string.Format("寄給 {0} 的系統通知信 (退貨申請書) 發送失敗.", emailTo);
                _log.Error(errMsg, e);
                return false;
            }

            return true;
        }
        #endregion

        #region 折讓單通知書
        public static bool SendDiscountSingleFormNotification(string emailTo, DiscountSingleFormNotificationInformation info)
        {
            try
            {
                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetHiDealNotificationXsl(PiinlifeNotificationType.DiscountSingleFormNotification);
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("ServiceCenterUrl", "", _config.HiDealServiceCenterUrl);
                transformer.XslParameters = args;

                string msgBody = transformer.Transform();
                _hdma.SendWithPostMan(_config.PiinlifeServiceEmail, emailTo, "退回或折讓單", msgBody, _config.HiDealServiceName);
            }
            catch (Exception e)
            {
                string errMsg = string.Format("寄給 {0} 的系統通知信 (退回或折讓單) 發送失敗.", emailTo);
                _log.Error(errMsg, e);
                return false;
            }

            return true;
        }
        #endregion

        #region (憑證)退貨申請通知

        public static bool SendRefundCouponNotification(string emailTo, ApplyRefundCouponNotificationInformation info)
        {
            try
            {
                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetHiDealNotificationXsl(PiinlifeNotificationType.RefundCouponNotification);
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("ServiceCenterUrl", "", _config.HiDealServiceCenterUrl);
                transformer.XslParameters = args;

                string msgBody = transformer.Transform();
                _hdma.SendWithPostMan(_config.PiinlifeServiceEmail, emailTo, "退貨申請通知", msgBody, _config.HiDealServiceName);
            }
            catch (Exception e)
            {
                string errMsg = string.Format("寄給 {0} 的系統通知信 (退貨申請通知) 發送失敗.", emailTo);
                _log.Error(errMsg, e);
                return false;
            }

            return true;
        }

        #endregion

        #region 商品退貨申請通知

        public static bool SendRefundGoodsNotification(string emailTo, ApplyRefundGoodsNotificationInformation info)
        {
            try
            {
                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetHiDealNotificationXsl(PiinlifeNotificationType.RefundGoodsNotification);
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("ServiceCenterUrl", "", _config.HiDealServiceCenterUrl);
                transformer.XslParameters = args;

                string msgBody = transformer.Transform();
                _hdma.SendWithPostMan(_config.PiinlifeServiceEmail, emailTo, "商品退貨申請通知", msgBody, _config.HiDealServiceName);
            }
            catch (Exception e)
            {
                string errMsg = string.Format("寄給 {0} 的系統通知信 (商品退貨申請通知) 發送失敗.", emailTo);
                _log.Error(errMsg, e);
                return false;
            }

            return true;
        }

        #endregion

        #region 發票中獎通知
        public static bool SendEInvoiceWonNotification(string emailTo, EInvoiceWonNotificationInformation info)
        {
            try
            {
                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetHiDealNotificationXsl(PiinlifeNotificationType.EInvoiceWonNotification);
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("ServiceCenterUrl", "", _config.HiDealServiceCenterUrl);
                transformer.XslParameters = args;

                string msgBody = transformer.Transform();
                _hdma.SendWithPostMan(_config.PiinlifeServiceEmail, emailTo, "發票中獎通知", msgBody, _config.HiDealServiceName);
            }
            catch (Exception e)
            {
                string errMsg = string.Format("寄給 {0} 的系統通知信 (發票中獎通知) 發送失敗.", emailTo);
                _log.Error(errMsg, e);
                return false;
            }
            return true;
        }
        #endregion

        #region 電子發票開立通知信
        public static bool SendEInvoice(string emailTo, EInvoiceInformation info)
        {
            try
            {
                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetHiDealNotificationXsl(PiinlifeNotificationType.EInvoice);
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("ServiceCenterUrl", "", _config.HiDealServiceCenterUrl);
                transformer.XslParameters = args;

                string msgBody = transformer.Transform();
                _hdma.SendWithPostMan(_config.PiinlifeServiceEmail, emailTo, "電子發票開立通知", msgBody, _config.HiDealServiceName);
            }
            catch (Exception e)
            {
                string errMsg = string.Format("寄給 {0} 的系統通知信 (電子發票開立通知) 發送失敗.", emailTo);
                _log.Error(errMsg, e);
                return false;
            }
            return true;
        }
        #endregion

        #region 優惠使用提醒
        public static bool SendStartUseReminder(string emailTo, StartUseReminderInformation info)
        {
            try
            {
                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetHiDealNotificationXsl(PiinlifeNotificationType.StartUseReminder);
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("ServiceCenterUrl", "", _config.HiDealServiceCenterUrl);
                transformer.XslParameters = args;

                string msgBody = transformer.Transform();
                string subject = string.Format("【{0}】優惠使用提醒", info.PurchasedItemName);
                _hdma.SendWithPostMan(_config.PiinlifeServiceEmail, emailTo, subject, msgBody, _config.HiDealServiceName);
            }
            catch (Exception e)
            {
                string errMsg = string.Format("寄給 {0} 的系統通知信 ({1} 優惠使用提醒) 發送失敗.", emailTo, info.PurchasedItemName);
                _log.Error(errMsg, e);
                return false;
            }
            return true;
        }
        #endregion

        #region 優惠到期提醒
        public static bool SendExpireReminder(string emailTo, ExpireReminderInformation info)
        {
            try
            {
                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetHiDealNotificationXsl(PiinlifeNotificationType.ExpireReminder);
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("ServiceCenterUrl", "", _config.HiDealServiceCenterUrl);
                transformer.XslParameters = args;

                string msgBody = transformer.Transform();
                string subject = string.Format("【{0}】優惠到期提醒", info.PurchasedItemName);
                _hdma.SendWithPostMan(_config.PiinlifeServiceEmail, emailTo, subject, msgBody, _config.HiDealServiceName);
            }
            catch (Exception e)
            {
                string errMsg = string.Format("寄給 {0} 的系統通知信 ({1} 優惠到期提醒) 發送失敗.", emailTo, info.PurchasedItemName);
                _log.Error(errMsg, e);
                return false;
            }
            return true;
        }
        #endregion

        #region 賣家/分店 倒店 - 公司內部通知信

        public static bool SendSellerOrStoreCloseDownNotification(string emailSender, string emailSenderName, SellerOrStoreCloseDownInformation info)
        {
            try
            {
                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetSellerOrStoreCloseDownXsl();
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                string msgBody = transformer.Transform();
                string subject;
                if (string.IsNullOrWhiteSpace(info.StoreName))
                {
                    subject = string.Format("【倒店通知】{0} 結束營業", info.SellerName);
                }
                else
                {
                    subject = string.Format("【倒店通知】{0} {1}結束營業", info.SellerName, info.StoreName);
                }

                List<string> mailToList = new List<string>(_config.ExpirationChangeEmail.Split(','));
                mailToList.Add(emailSender);

                foreach (string mail in mailToList)
                {
                    _hdma.SendWithPostMan(emailSender, mail.Trim(), subject, msgBody, emailSenderName);
                }
            }
            catch (Exception e)
            {
                string errMsg = string.Format("由 {0} 寄的系統通知信 ( {1} 倒店內部人員通知信) 發送失敗.", emailSender, string.Concat(info.SellerName, " ", info.StoreName));
                _log.Error(errMsg, e);
                return false;
            }
            return true;
        }

        #endregion

        #region 檔次/分店 - 使用期限調整公司內部通知信

        public static bool SendCouponExpireDateChangedNotification(string emailSender, string emailSenderName, CouponExpireDateChangedInformation info)
        {
            try
            {
                DateTime originalDate;
                DateTime changedDate;
                DateTime.TryParse(info.CouponUseOriginalExpireDate, out originalDate);
                DateTime.TryParse(info.ChangedExpireDate, out changedDate);
                string subject;

                if (changedDate < originalDate)
                {
                    subject = string.Format("【憑證停止使用】{0} 檔號：{1}  憑證停止使用", info.SellerName, info.UniqueId);
                    info.IsExtend = false;
                }
                else
                {
                    subject = string.Format("【延長使用期限】{0} 檔號：{1}  延長使用期限", info.SellerName, info.UniqueId);
                    info.IsExtend = true;
                }

                XslTransformer transformer = new XslTransformer();
                transformer.XslStylesheet = HiDealMailAgent.GetCouponExpireDateChangedNotificationXsl();
                transformer.XmlData = XslTransformer.SerializeObjectToXml(info);

                string msgBody = transformer.Transform();

                List<string> mailToList = new List<string>(_config.ExpirationChangeEmail.Split(','));
                mailToList.Add(emailSender);

                foreach (string mail in mailToList)
                {
                    _hdma.SendWithPostMan(emailSender, mail.Trim(), subject, msgBody, emailSenderName);
                }
            }
            catch (Exception e)
            {
                string errMsg = string.Format("由 {0} 寄的系統通知信 ( 檔號：{1} - 憑證使用期限調整內部人員通知信) 發送失敗.", emailSender, info.UniqueId);
                _log.Error(errMsg, e);
                return false;
            }
            return true;
        }

        #endregion

        /// <summary>
        /// emailTo 可以跟 userName 不同，會員可以指定寄到任意合法的電子信箱
        /// </summary>
        /// <param name="emailTo"></param>
        /// <param name="userName"></param>
        /// <param name="couponId"></param>
        public static void SendCouponToMember(string emailTo, string userName, int couponId)
        {
            string requestUrl = WebUtility.GetSiteRoot() + "/piinlife/member/CouponPrint.aspx?s=1&";
            ViewHiDealCoupon coupon = ProviderFactory.Instance().GetProvider<IHiDealProvider>().ViewHiDealCouponGet(couponId);
            //ViewHiDealCouponGet 用 LoadByParam 來撈資料，無資料亦傳回 IsNew = true, IsLoaded = true
            if (coupon.CouponId == 0)
            {
                throw new Exception("取得憑證失敗,可能是憑證已不存在. 編號 " + couponId);
            }
            if (string.Equals(coupon.UserName, userName, StringComparison.OrdinalIgnoreCase) == false)
            {
                throw new Exception(string.Format("取得憑證失敗,您沒有取得憑證的權限. 編號 {0}, 使用者: {1} ", couponId, userName));
            }

            IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            Member member = mp.MemberGet(userName);
            string userDsplayName = null;
            if (member.IsLoaded)
            {
                userDsplayName = member.DisplayName;
            }

            requestUrl += "cid=" + couponId + "&u=" + userName;

            PdfConverter pdfc = new PdfConverter();
            pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
            pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;

            // disable unnecessary features to enhance performance
            pdfc.ScriptsEnabled = pdfc.ScriptsEnabledInImage = false;
            pdfc.ActiveXEnabled = pdfc.ActiveXEnabledInImage = false;
            pdfc.PdfDocumentOptions.JpegCompressionEnabled = false;

            byte[] b = pdfc.GetPdfFromUrlBytes(requestUrl);
            MemoryStream ms = new MemoryStream(b);
            ms.Position = 0;
            List<Attachment> attachments = new List<Attachment>();

            Attachment attachCoupon = new Attachment(ms, coupon.Prefix + coupon.Sequence, "application/pdf");
            attachments.Add(attachCoupon);
            string subject = string.Format("優惠憑證:{0}{1} {2}", coupon.Prefix, coupon.Sequence, coupon.Name);

            XslTransformer transformer = new XslTransformer();
            transformer.XslStylesheet = HiDealMailAgent.GetHiDealNotificationXsl(PiinlifeNotificationType.CouponMail);
            transformer.XmlData = XslTransformer.SerializeObjectToXml(
                new CouponMailInformation { 
                    UserName = userDsplayName,
                    HDmailHeaderUrl = _config.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Header.png",
                    HDmailFooterUrl = _config.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Footer.png"
                }
            );

            string msgBody = transformer.Transform();


            _hdma.SendWithPostMan(_config.PiinlifeServiceEmail, emailTo, subject,
                msgBody,
                _config.HiDealServiceName, true, attachments);
        }

        #endregion

        #region 訂閱信
        public static bool IsHiDealSubscribe(string userName)
        {
            return _hdp.HiDealSubscriptionGetListByUser(userName).FirstOrDefault() != null;
        }

        public static HiDealSubscription HiDealSubscribeCheckSet(string userName, int regionCodeId, int cityId, bool isSubscribe)
        {
            HiDealSubscription subscription = new HiDealSubscription();
            if (!regionCodeId.Equals(0))
            {
                subscription = _hdp.HiDealSubscriptionGetByUserRegionCodeId(userName, regionCodeId);
            }
            else if (!cityId.Equals(0))
            {
                subscription = _hdp.HiDealSubscriptionGetByUserCityId(userName, cityId);
            }

            if (isSubscribe)
            {
                if (!subscription.IsLoaded)
                {
                    // 訂閱
                    subscription.Email = userName;
                    subscription.Flag = Convert.ToInt32(PiinlifeSubscriptionFlag.General);
                    if (!regionCodeId.Equals(0))
                    {
                        subscription.RegionCodeId = regionCodeId;
                    }

                    if (!cityId.Equals(0))
                    {
                        subscription.City = cityId;
                    }

                    subscription.CreateTime = DateTime.Now;
                    _hdp.HiDealSubscriptionSet(subscription);

                    // 預設訂閱總覽
                    if (!regionCodeId.Equals((int)HiDealRegionManager.DealOverviewRegion))
                    {
                        HiDealSubscribeCheckSet(userName, (int)HiDealRegionManager.DealOverviewRegion, 0, true);
                    }
                }
                else if (subscription.Flag.Equals((int)PiinlifeSubscriptionFlag.Cancel))
                {
                    // 重新訂閱
                    subscription.Flag = Convert.ToInt32(PiinlifeSubscriptionFlag.Resubscribe);
                    _hdp.HiDealSubscriptionSet(subscription);
                }
            }
            else if (subscription.IsLoaded && !subscription.Flag.Equals((int)PiinlifeSubscriptionFlag.Cancel))
            {
                // 取消訂閱
                subscription.Flag = Convert.ToInt32(PiinlifeSubscriptionFlag.Cancel);
                subscription.CancelTime = DateTime.Now;
                _hdp.HiDealSubscriptionSet(subscription);
            }
            return subscription;
        }

        /// <summary>
        /// 訂閱電子報
        /// </summary>
        /// <param name="userId"></param>
        public static void SubscribeEdm(int userId)
        {
            string userEmail = _mp.MemberGetbyUniqueId(userId).UserEmail;
            HiDealSubscribeCheckSet(userEmail, HiDealRegionManager.DealOverviewRegion, 0, true);
        }

        /// <summary>
        /// 訂閱電子報且為 Visa Member
        /// </summary>
        /// <param name="userId"></param>
        public static void SubscribeVisaMember(int userId, int visaCardType)
        {
            string userEmail = _mp.MemberGetbyUniqueId(userId).UserEmail;
            HiDealSubscription subscription = _hdp.HiDealSubscriptionGetByUserRegionCodeId(
                userEmail, HiDealRegionManager.DealOverviewRegion);
            if (subscription.IsLoaded == false)
            {
                subscription.RegionCodeId = HiDealRegionManager.DealOverviewRegion;
                subscription.Email = userEmail;
                subscription.CreateTime = DateTime.Now;
                subscription.Flag = 1;
                subscription.VisaMemberType = visaCardType;
                _hdp.HiDealSubscriptionSet(subscription);
            }
            else if (subscription.Flag == 0 || subscription.VisaMemberType != visaCardType)
            {
                subscription.Flag = 1;
                subscription.VisaMemberType = visaCardType;
                _hdp.HiDealSubscriptionSet(subscription);
            }
        }
        #endregion

        #region 大量通知信



        #endregion
    }

}

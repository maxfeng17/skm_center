﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Transactions;
using LunchKingSite.Core.Component;
using log4net;
using System.Net.Mail;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models.QueueModels;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.BizLogic.Facade
{
    public class CouponFacade
    {
        private static object thisLock = new object();
        private static ILog logger = LogManager.GetLogger(typeof(CouponFacade).Name);
        private static IOrderProvider op;
        private static IPponProvider pp;
        private static IMemberProvider mp;
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IFamiportProvider fp;
        private static IMGMProvider mgmp;
        private static readonly IVerificationProvider vp;
        //for 富利核銷
        private static List<FailedCoupon> failedCoupons;
        private static List<SucceedCoupon> succeedCoupons;
        private static bool isJFVerifySucceeed = true;

        static CouponFacade()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            fp = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
            mgmp = ProviderFactory.Instance().GetProvider<IMGMProvider>();
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
        }

        public static List<Coupon> GenCouponWithSmsWithGeneratePeztemp(Order order, IViewPponDeal deal, bool isPaidByAtm)
        {
            bool dealOn;
            bool disabledSms;
            int itemquantity;
            List<Coupon> returnCoupons = GenCouponWithGeneratePeztemp(order, deal, false, out dealOn, out disabledSms, out itemquantity);
            if (returnCoupons.Count > 0)
            {
                if (returnCoupons.Count == itemquantity)
                {
                    // config.CouponSmsEnabled 所有檔次是否要發簡訊的設定
                    if (!disabledSms && config.CouponSmsEnabled)
                    {
                        string mobile = MemberFacade.GetUserMobile(order.UserId);
                        if (RegExRules.CheckMobile(mobile))
                        {
                            SendSms(returnCoupons, deal, order.UserId, mobile, isPaidByAtm, dealOn);
                        }
                    }
                }
                else
                {
                    logger.Error("couponfacade error: the quantity of return coupons is not equal to itemquantity" + Helper.Object2String(order, "\n"));
                }
            }
            return returnCoupons;
        }

        /// <summary>
        /// 此為雄獅檔次使用 
        /// 因為簡訊需要外部傳入的手機號碼 簡訊內容也需要刪除17life字樣 僅產生憑證 簡訊另外處理
        /// </summary>
        /// <param name="order"></param>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static List<Coupon> GenCouponWithoutSms(Order order, IViewPponDeal deal, bool couponSequenceBetaVersion = false)
        {
            bool dealOn;
            bool disabledSms;
            int itemquantity;
            List<Coupon> returnCoupons = GenCouponWithGeneratePeztemp(order, deal, couponSequenceBetaVersion, out dealOn, out disabledSms, out itemquantity);
            return returnCoupons;
        }

        private static List<Coupon> GenCouponWithGeneratePeztemp(Order order, IViewPponDeal deal, bool couponSequenceBetaVersion,
            out bool dealOn, out bool disabledSms, out int itemquantity)
        {
            GroupOrder groupOrder = op.GroupOrderGet(deal.GroupOrderGuid.Value);
            ViewPponCouponCollection coupons = pp.ViewPponCouponGetNoCouponList(ViewPponOrderDetail.Columns.OrderGuid, order.Guid);
            disabledSms = Helper.IsFlagSet(groupOrder.Status ?? 0, GroupOrderStatus.DisableSMS);
            //APP訂單不再發送憑證簡訊
            disabledSms = (order.OrderFromType == (int)OrderFromType.ByIOS || order.OrderFromType == (int)OrderFromType.ByAndroid)
                && !Helper.IsFlagSet(groupOrder.Status ?? 0, GroupOrderStatus.FamiDeal) ? true : disabledSms;
            itemquantity = 0;
            List<Coupon> returnCoupons = new List<Coupon>();
            if (dealOn = (deal.OrderedQuantity >= deal.BusinessHourOrderMinimum))
            {
                CommunicationFacade.SmsLogSetToQueue(deal.BusinessHourGuid);
            }

            //判斷gorup_order狀態是否已為PponCouponGenerated
            if (!Helper.IsFlagSet(groupOrder.Status ?? 0, GroupOrderStatus.PponCouponGenerated))
            {
                //變更為已產生憑證
                OrderFacade.SetDealGenerated(groupOrder);
            }

            //憑證檔次 產生憑證 訂單未取消
            if (deal.DeliveryType.HasValue && deal.DeliveryType.Value == (int)DeliveryType.ToShop
                    && !Helper.IsFlagSet(order.OrderStatus, OrderStatus.Cancel))
            {
                //預產憑證的檔次判斷
                string pezExternalUserId = MemberFacade.CheckIsPEZUserID(order.UserId);
                bool isPezEvent = Helper.IsFlagSet(groupOrder.Status ?? 0, GroupOrderStatus.PEZevent);
                bool isPezEventWithUserId = Helper.IsFlagSet(groupOrder.Status ?? 0, GroupOrderStatus.PEZeventWithUserId);
                bool isRealtimeGenPeztemp = false;
                List<string> realtimePeztempCol = new List<string>();
                if (isPezEvent
                    && Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
                {
                    if ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)
                    {
                        realtimePeztempCol = FamiGroupCoupon.GetFamiportPincodeByOrderGuid(order.Guid);
                        isRealtimeGenPeztemp = true;
                    }
                }

                IEnumerable<ViewPponCoupon> incompletedCoupons =
                    coupons.Where(x => x.OrderDetailStatus != (int)OrderDetailStatus.SystemEntry);
                foreach (var item in incompletedCoupons)
                {
                    List<Coupon> inside_coupons = new List<Coupon>();
                    ViewPponCoupon coupon = item;
                    int saleMultipleBase = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) && config.IsGroupCouponOn ? deal.SaleMultipleBase ?? 0 : 0;
                    coupon.ItemQuantity = saleMultipleBase > 0 ? saleMultipleBase : coupon.ItemQuantity;
                    itemquantity += coupon.ItemQuantity;
                    if (OrderFacade.CheckCouponCountByOrderDetail(coupon.OrderDetailId.Value) == 0)
                    {
                        for (int i = 0; i < coupon.ItemQuantity; i++)
                        {
                            Coupon c = new Coupon();
                            try
                            {
                                lock (thisLock)
                                {
                                    using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                                    {
                                        c.OrderDetailId = coupon.OrderDetailId.Value;
                                        //預產憑證的檔次
                                        if (isPezEvent)
                                        {
                                            Peztemp pez;
                                            if (isRealtimeGenPeztemp)
                                            {
                                                pez = new Peztemp
                                                {
                                                    PezCode = FamiGroupCoupon.DecryptPincode(realtimePeztempCol[i]),
                                                    Bid = deal.BusinessHourGuid,
                                                    UserId = order.UserId.ToString(),
                                                    IsUsed = false,
                                                    ServiceCode = (int)PeztempServiceCode.FamilynetPincode,
                                                    IsTest = false,
                                                    IsBackupCode = false,
                                                };
                                            }
                                            else if (isPezEventWithUserId)
                                            {
                                                pez = OrderFacade.GetPezTemp(deal.BusinessHourGuid, pezExternalUserId);
                                            }
                                            else
                                            {
                                                pez = OrderFacade.GetPezTemp(deal.BusinessHourGuid);
                                            }
                                            c.Code = c.SequenceNumber = pez.PezCode;
                                            pez.OrderDetailGuid = c.OrderDetailId;
                                            pez.OrderGuid = coupon.OrderGuid;
                                            pez.Status = (int)PeztempStatus.Using;
                                            OrderFacade.SetPezTemp(pez);
                                        }
                                        else
                                        { //取coupon字數排列與編號
                                            int si;
                                            if (couponSequenceBetaVersion == false)
                                            {
                                                si = OrderFacade.GetLastCouponSi(deal.BusinessHourGuid.ToString(), coupon.BusinessHourStatus);
                                                if (int.Equals(1, si))
                                                {
                                                    si += OrderFacade.GetAncestorCouponCount(deal.BusinessHourGuid);
                                                }//產生CouponSequence碼
                                            }
                                            else
                                            {
                                                si = OrderFacade.GetLastCouponSiV2(deal.BusinessHourGuid.ToString());
                                            }
                                            
                                            c.SequenceNumber = OrderFacade.GenerateCouponSequence(coupon.ParentOrderId.ToString(), coupon.MemberEmail, si);
                                            c.Code = OrderFacade.GenerateCouponVerification(c.SequenceNumber, coupon.ParentOrderId.ToString());

                                            if (coupon.StoreGuid != null)
                                            {
                                                c.StoreSequence = OrderFacade.GenerateStoreCouponSequence(deal.BusinessHourGuid.ToString(), coupon.StoreGuid.ToString());
                                            }

                                        }
                                        c.Id = OrderFacade.GetCouponID(c);    //get coupon_id for SMS
                                        ts.Complete();
                                    }
                                }
                                returnCoupons.Add(c);
                                inside_coupons.Add(c);
                                if (isRealtimeGenPeztemp && config.ActiveMQEnable)
                                {
                                    IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
                                    var famiMq = new FamiGroupCouponMq
                                    {
                                        PeztempCode = realtimePeztempCol[i],
                                        OrderGuid = order.Guid,
                                        CouponId = c.Id
                                    };
                                    mqp.Send(FamiGroupCouponMq._QUEUE_NAME, famiMq);
                                }
                            }
                            catch (Exception ex)
                            {
                                logger.Error("GenCoupon error. " + Helper.Object2String(c, "\n"), ex);
                            }
                        }

                        OrderClassification oc = (deal.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0 ? OrderClassification.Skm : OrderClassification.LkSite;
                        CashTrustLogCollection cashtrustlogList = OrderFacade.GetTrustByOrderDetail(coupon.OrderDetailId.Value, oc);
                        try
                        {
                            if (cashtrustlogList.Count == inside_coupons.Count)
                            {
                                using (TransactionScope ts2 = new TransactionScope())
                                {
                                    for (int y = 0; y < cashtrustlogList.Count(); y++)
                                    {
                                        OrderFacade.SaveTrustCoupon(cashtrustlogList[y], inside_coupons[y].Id, inside_coupons[y].SequenceNumber);
                                    }
                                    ts2.Complete();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error("Something wrong while trying to fill coupon_id in cash_trust_log: " +
                                Helper.Object2String(cashtrustlogList, "\n"), ex);
                        }
                        //檢查憑證數量 刪除多餘的憑證
                        //if (OrderFacade.CheckCouponCountByOrderDetail(coupon.OrderDetailId.Value) > coupon.ItemQuantity && returnCoupons != null)
                        //{
                        //    logger.Warn("好奇這段code有可能執行到嗎? CouponFacade-01");
                        //    returnCoupons.Skip(coupon.ItemQuantity).ForEach(x =>
                        //    {
                        //        OrderFacade.ClearCouponByCouponID(x.Id);
                        //    });
                        //    returnCoupons = returnCoupons.Take(coupon.ItemQuantity).ToList();
                        //}
                    }
                }
            }
            return returnCoupons;
        }

        private static void SendSms(List<Coupon> coupons, IViewPponDeal deal, int userId, string mobile, bool isPaidByAtm, bool dealOn)
        {
            SMS sms = new SMS();
            SmsContent sc = OrderFacade.SMSContentGet(deal.BusinessHourGuid);
            string[] msgs = sc.Content.Split('|');

            SmsSystem provider = CommunicationFacade.GetSmsSystemByRatio();
            OrderDetail od = op.OrderDetailGet(coupons.FirstOrDefault().OrderDetailId);
            string changedExpireDate = GetFinalExpireDateContent(od.OrderGuid, Guid.Empty, Guid.Empty).Replace("/", "");
            foreach (Coupon coupon in coupons.Where(x => x.Status != (int)CouponStatus.Locked))
            {
                string branch = OrderFacade.SmsGetPhoneFromWhere(coupon.OrderDetailId, deal.BusinessHourGuid);    //分店資訊 & 多重選項
                string sequenceNumber = coupon.SequenceNumber;
                //是否為廠商提供序號活動
                if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.PEZevent))
                {
                    var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(deal.BusinessHourGuid, true);
                    //是否為自訂簡訊格式  
                    if (vpd != null && vpd.CouponSeparateDigits > 0)
                    {
                        sequenceNumber = GetCouponSeparateCode(coupon.SequenceNumber, (int)vpd.CouponSeparateDigits);
                    }
                }
                string msg = string.Format(msgs[0] + msgs[1] + branch + msgs[2],
                sequenceNumber,
                (coupon.SequenceNumber == coupon.Code) ? string.Empty : coupon.Code,
                deal.BusinessHourDeliverTimeS.Value.ToString("yyyyMMdd"),
                string.IsNullOrEmpty(changedExpireDate) ? deal.BusinessHourDeliverTimeE.Value.ToString("yyyyMMdd") : changedExpireDate,
                !coupon.StoreSequence.HasValue ? string.Empty : "，序號" + coupon.StoreSequence.Value);

                if (coupon.SequenceNumber == coupon.Code)
                {
                    msg = msg.Replace("確認碼，", string.Empty);
                }

                if (isPaidByAtm)
                {
                    sms.QueueMessageForPayByAtm(msg, Core.SmsType.Ppon, userId.ToString(), mobile,
                        deal.BusinessHourGuid, coupon.Id.ToString(), provider);
                }
                else
                {
                    if (dealOn)
                    {
                        sms.QueueMessage(msg, Core.SmsType.Ppon, userId.ToString(), mobile,
                            deal.BusinessHourGuid, coupon.Id.ToString(), provider);
                    }
                    else
                    {
                        sms.QueueMessageForPponNotOnDeal(msg, Core.SmsType.Ppon, userId.ToString(), mobile,
                            deal.BusinessHourGuid, coupon.Id.ToString(), provider);
                    }
                }
            }
        }

        /// <summary>
        /// 取得訂單所屬分店有設訂位系統且該檔次有配合訂位服務
        /// </summary>
        /// <returns></returns>
        private static bool IsEnableBookingSystem(Guid orderGuid, BookingType bookingType)
        {
            ViewPponOrderDetail vpod = op.ViewPponOrderDetailGetListByOrderGuid(orderGuid).FirstOrDefault(x => x.OrderDetailStatus == (int)OrderDetailStatus.None);

            bool IsNoRestrictedStore = false;   //通用券
            if (vpod.BusinessHourGuid != null)
            {
                ViewPponDeal vpd = new ViewPponDeal();
                vpd = pp.ViewPponDealGetByBusinessHourGuid(vpod.BusinessHourGuid.Value);
                IsNoRestrictedStore = Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
            }
            if (vpod.StoreGuid != null)
            {
                var dealProperty = pp.DealPropertyGetByOrderGuid(orderGuid);
                if (BookingSystemFacade.GetBookingSystemStoreSettingByStoreGuid((Guid)vpod.StoreGuid, bookingType) && dealProperty.BookingSystemType > 0)
                {
                    return true;
                }
            }
            else if (IsNoRestrictedStore)
            {
                //通用券
                var dealProperty = pp.DealPropertyGetByOrderGuid(orderGuid);
                Dictionary<string, string> sellers = new Dictionary<string, string>();
                CashTrustLog vpct = mp.CashTrustLogGetListByOrderGuid(orderGuid).Where(x => x.BusinessHourGuid != null).FirstOrDefault();
                ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(vpct.BusinessHourGuid.Value);
                IEnumerable<Seller> ss = SellerFacade.GetSellerTreeSellers(vpd.SellerGuid);
                foreach (Seller seller in ss)
                {
                    if (BookingSystemFacade.GetBookingSystemStoreSettingByStoreGuid(seller.Guid, bookingType) && dealProperty.BookingSystemType > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 顯示0元檔次且為全家優惠的內容
        /// </summary>
        /// <param name="CodeType">BarCode型態</param>
        /// <returns></returns>
        private static string GetZeroDealTagContent(CouponCodeType CodeType)
        {
            string tagContent = string.Empty;
            if (config.EnableFami3Barcode)
            {
                tagContent = @"<div class='tag tag-fami-barcode'>優惠條碼</div>
                            <span class='tag-note'>請直接於櫃檯出示結帳</sapn>";
            }
            else
            {
                switch (CodeType)
                {
                    case CouponCodeType.Pincode:
                        tagContent = @"<div class='tag tag-fami-pincode'>紅利Pin碼</div>
                            <span class='tag-note'>請於FamiPort列印條碼</sapn>";
                        break;
                    case CouponCodeType.Barcode128:
                    case CouponCodeType.BarcodeEAN13:
                        tagContent = @"<div class='tag tag-fami-barcode'>優惠條碼</div>
                            <span class='tag-note'>請直接於櫃檯出示結帳</sapn>";
                        break;
                    default:
                        break;
                }
            }

            return tagContent;
        }

        /// <summary>
        /// 處理欄位資料
        /// </summary>
        /// <param name="couponData"></param>
        /// <returns></returns>
        private static BaseCouponsModel ProcessCouponsColumnData(ViewCouponOnlyListMain couponData)
        {
            #region Declare
            ExpirationDateSelector selector = new ExpirationDateSelector();
            selector.ExpirationDates = new PponUsageExpiration(couponData.Guid);

            bool isShowBookingBtn = false;
            bool isShowCouponDetailBtn = false;
            bool isEnableCouponEaluateBtn = true;
            bool isShowCouponEaluateBtn = false;
            string priceHtml = string.Empty;
            string finalExpDateHtml = string.Empty;
            string couponStatus = string.Empty;
            string orderStatus = string.Empty;
            string couponDetailEventHtml = string.Empty;
            string bookingBtnEventHtml = string.Empty;
            string bookingBtnText = string.Empty;

            bool isOrderCancel = (couponData.OrderStatus & (int)OrderStatus.Cancel) > 0;
            bool isPartialCancel = (couponData.TotalCount > mp.CashTrustLogGetListByOrderGuid(couponData.Guid, OrderClassification.LkSite).Where(x => (x.Status == (int)TrustStatus.Returned || x.Status == (int)TrustStatus.Refunded) && !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).Count());
            bool isZeroPponDeal = decimal.Equals(0m, couponData.ItemPrice);
            bool isATMNotPayYet = ((couponData.OrderStatus & (int)OrderStatus.ATMOrder) > 0 && (couponData.OrderStatus & (int)OrderStatus.Complete) == 0);
            bool isPpon = (!couponData.DeliveryType.HasValue || int.Equals((int)DeliveryType.ToShop, couponData.DeliveryType.Value));
            bool isFamiDeal = (couponData.Status & (int)GroupOrderStatus.FamiDeal) > 0;
            bool isKindDeal = (couponData.Status & (int)GroupOrderStatus.KindDeal) > 0;
            bool isPEZevent = (couponData.Status & (int)GroupOrderStatus.PEZevent) > 0;
            string orderNameUrl = (isFamiDeal ? ("/" + PponCityGroup.DefaultPponCityGroup.Family.CityId) : string.Empty) + "/" + couponData.BusinessHourGuid;
            #endregion

            #region 是否顯示訂位/房按鈕
            //憑證使用時間及提前天數預約檢查，是否顯示按鈕 start
            int arDays = 0;
            var dealProperty = pp.DealPropertyGetByOrderGuid(couponData.Guid);
            if (dealProperty != null && dealProperty.IsLoaded)
            {
                arDays = dealProperty.AdvanceReservationDays;
            }
            //憑證使用時間及提前天數預約檢查，是否顯示按鈕 end
            bookingBtnText = (BookingType)couponData.BookingSystemType == BookingType.Travel ? "訂房" : "訂位";
            bool isBooking = DateTime.Today <= selector.GetExpirationDate().AddDays(-arDays);
            bool isCheckCouponsStatus = BookingSystemFacade.CheckCouponsStatus(Convert.ToString(couponData.Guid), (BookingType)couponData.BookingSystemType);
            isShowBookingBtn = IsEnableBookingSystem(couponData.Guid, (BookingType)couponData.BookingSystemType) && isBooking;

            if ((!isOrderCancel && isPpon && couponData.TotalCount != 0 && isShowBookingBtn) ||
                (isOrderCancel && isPpon && isPartialCancel && couponData.TotalCount != 0 && isShowBookingBtn))
            {
                bookingBtnEventHtml = string.Format("openBookingSystem('{0}','{1}')", isCheckCouponsStatus ? Convert.ToString(couponData.Guid) : string.Empty, Helper.Encrypt("17Life"));
                bookingBtnText = isCheckCouponsStatus ? bookingBtnText : "查詢" + bookingBtnText;
            }
            #endregion

            #region 是否顯示好康名稱下方資訊            
            //好康名稱下方
            //公益檔次不顯示
            if (!isKindDeal)
            {
                bool isPriceZeroShowOriginalPrice = (couponData.BusinessHourStatus & (int)BusinessHourStatus.PriceZeorShowOriginalPrice) > 0;

                if (isZeroPponDeal && isFamiDeal)
                {
                    priceHtml = GetZeroDealTagContent((CouponCodeType)couponData.CouponCodeType);
                }
                else
                {
                    priceHtml = "好康價:" + (isPriceZeroShowOriginalPrice ? couponData.ItemOrigPrice.ToString("F0") : couponData.ItemPrice.ToString("F0"));
                }
            }
            #endregion

            #region 兌換日期
            //兌換日期
            string expDate = (!couponData.BusinessHourDeliverTimeE.HasValue) ? string.Empty : (couponData.ChangedExpireDate.HasValue ? couponData.ChangedExpireDate.Value.ToString("yyyy/MM/dd") : couponData.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd"));

            if (!(couponData == null || selector == null || couponData.Department > 3 || !Helper.IsFlagSet(couponData.BusinessHourStatus, BusinessHourStatus.FinalExpireTimeChange)))
            {
                //若結束營業日&截止日期調整兩者相同, 結束營業優先
                DateTime expireDate = selector.GetExpirationDate();
                if (expireDate < selector.ExpirationDates.DealOriginalExpireDate)
                {
                    if (DateTime.Equals(expireDate, selector.ExpirationDates.SellerClosedDownDate ?? DateTime.MaxValue) ||
                        DateTime.Equals(expireDate, selector.ExpirationDates.StoreClosedDownDate ?? DateTime.MaxValue))
                    {
                        finalExpDateHtml = string.Format("(至{0}停止營業)", expireDate.ToString("yyyy/MM/dd"));
                    }
                    finalExpDateHtml = string.Format("(至{0}憑證停止使用)", expireDate.ToString("yyyy/MM/dd"));
                }
            }

            #endregion

            #region 憑證狀態
            //全家優惠事件
            string famiClickEvent = (isZeroPponDeal && isFamiDeal) ? string.Format("openFamiCoupon({0})", mp.GetViewCouponListByOrderGuid(couponData.Guid).FirstOrDefault().Id) : string.Empty;
            //憑證兌換事件
            string couponClickEvent = string.Format("ShowDetail('{0}')", couponData.Guid);

            //AMT尚未付款的話不顯示
            if (!isATMNotPayYet)
            {
                //零元檔次
                if (isZeroPponDeal)
                {
                    if (isPEZevent || isFamiDeal)
                    {
                        if (couponData.PinType == (int)PinType.Single)
                        {
                            //如果只有一組序號則不顯示使用狀態
                            couponStatus = "可使用";
                            isShowCouponDetailBtn = true;
                        }
                        else if (couponData.PrintCount == 0)
                        {
                            couponStatus = couponData.CouponCodeType == (int)CouponCodeType.Pincode ? "未使用" : "可使用";
                            isShowCouponDetailBtn = true;
                        }
                        else
                        {
                            couponStatus = "使用完畢";
                        }
                    }
                }
                else
                {
                    if ((!isOrderCancel && isPpon && couponData.TotalCount != 0) ||
                        (isOrderCancel && isPartialCancel && isPpon && couponData.TotalCount != 0))
                    {
                        couponStatus = string.Format("未使用:{0}", couponData.RemainCount);
                        isShowCouponDetailBtn = true;
                    }
                }

                if (isKindDeal)
                {
                    couponStatus = "公益檔次";
                }
            }

            couponDetailEventHtml = isShowCouponDetailBtn ? ((isZeroPponDeal && isFamiDeal) ? famiClickEvent : couponClickEvent) : string.Empty;
            #endregion

            #region 訂單狀態
            int evaluateCount = new EvaluateFacade().GetEvaluateCount(couponData.Guid);
            bool isCancel = false;
            isShowCouponEaluateBtn = evaluateCount > 0;

            if (isOrderCancel)
            {
                isCancel = true;
                orderStatus = isPartialCancel ? "部分退貨" : "訂單已取消";
                isEnableCouponEaluateBtn = evaluateCount > 0;
                isShowCouponEaluateBtn = evaluateCount <= 0;
            }
            else if (couponData.BusinessHourOrderTimeE <= DateTime.Now)
            {
                isEnableCouponEaluateBtn = (evaluateCount > 0);
            }
            else
            {
                isEnableCouponEaluateBtn = (evaluateCount > 0);
            }

            #region 退貨條件顯示
            //不接受退貨、結檔後七天不接受退貨、過期不接受退貨、演出時間前十日過後不能退貨
            if (((couponData.Status & (int)GroupOrderStatus.NoRefund) > 0) ||
                ((couponData.Status & (int)GroupOrderStatus.DaysNoRefund) > 0 && couponData.BusinessHourOrderTimeE.AddDays(config.ProductRefundDays) < DateTime.Now) ||
                ((couponData.Status & (int)GroupOrderStatus.ExpireNoRefund) > 0 && (couponData.BusinessHourDeliverTimeE != null && couponData.BusinessHourDeliverTimeE.Value.AddDays(1) < DateTime.Now) && !isCancel) ||
                ((couponData.Status & (int)GroupOrderStatus.NoRefundBeforeDays) > 0 && DateTime.Now.Date >= ((DateTime)couponData.BusinessHourDeliverTimeS).AddDays(-config.PponNoRefundBeforeDays).Date))
            {
                orderStatus = I18N.Phrase.NoRefund;
            }
            #endregion
            #endregion

            #region 資料整理(Web)
            CouponsWebModel model = new CouponsWebModel();
            model.OrderDate = couponData.CreateTime.ToString("yyyy/MM/dd");
            model.OrderSerial = couponData.OrderId;
            model.OrderName = couponData.ItemName.TrimToMaxLength(17, "...");
            model.OrderAmount = (couponData.TotalCount != 0 ? Convert.ToString(couponData.TotalCount) : string.Empty);
            model.OrderExp = expDate;
            model.OrderGuid = Convert.ToString(couponData.Guid);
            model.EvaluateCount = evaluateCount;
            model.IsOrderSerialLink = !isZeroPponDeal;
            model.IsShowServiceBtn = (config.PponServiceButtonVisible && !isZeroPponDeal);
            model.IsShowBookingBtn = isShowBookingBtn;
            model.IsShowCouponDetailBtn = isShowCouponDetailBtn;
            model.IsShowAtmPaycontinueBtn = isATMNotPayYet && couponData.CreateTime.Date.Equals(DateTime.Now.Date);
            model.IsShowCouponEaluateBtn = isShowCouponEaluateBtn;
            model.IsEnableCouponEaluateBtn = isEnableCouponEaluateBtn;
            model.IsFamiDeal = isFamiDeal;
            model.PriceHtml = priceHtml;
            model.FinalExpDateHtml = finalExpDateHtml;
            model.CouponStatus = couponStatus;
            model.OrderStatus = orderStatus;
            model.CouponDetailEventHtml = couponDetailEventHtml;
            model.OrderNameUrl = orderNameUrl;
            model.BookingBtnEventHtml = bookingBtnEventHtml;
            model.BookingBtnText = bookingBtnText;
            #endregion

            return model;
        }

        public static string SmsMessage(Guid bid, int? groupOrderStatus, string couponCode, string branch, string[] msgs, string sequenceNumber, DateTime? businessHourDeliverTimeS, DateTime finalExpireDate, int? couponStoreSequence)
        {
            string msg = string.Empty;
            //是否為廠商提供序號活動
            if (Helper.IsFlagSet(groupOrderStatus ?? 0, GroupOrderStatus.PEZevent))
            {
                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
                msgs[2] = msgs[2].Replace("-", "");
                //是否為自訂簡訊格式  
                if (vpd != null && vpd.CouponSeparateDigits > 0)
                {
                    var coupon = GetCouponSeparateCode(sequenceNumber, (int)vpd.CouponSeparateDigits);
                    msg = string.Format(msgs[0] + msgs[1] + branch + msgs[2], coupon, "", businessHourDeliverTimeS.Value.ToString("yyyyMMdd") + "-", finalExpireDate.ToString("yyyyMMdd"), (couponStoreSequence != null && couponStoreSequence != 0) ? "，序號" + couponStoreSequence.Value.ToString("0000") : string.Empty);
                }
                else//不顯示確認碼
                {
                    msg = string.Format(msgs[0] + msgs[1] + branch + msgs[2], sequenceNumber.Replace("-", "－"), "", businessHourDeliverTimeS.Value.ToString("yyyyMMdd"), finalExpireDate.ToString("yyyyMMdd"), (couponStoreSequence != null && couponStoreSequence != 0) ? "，序號" + couponStoreSequence.Value.ToString("0000") : string.Empty);
                }
            }
            else
            {
                //因為Sony手機的簡訊, 在碰到 - 8 這樣的字元組合時, 會替換成音符..故把半行的 dash 改換為全形 dash
                msg = string.Format(msgs[0] + msgs[1] + branch + msgs[2], sequenceNumber.Replace("-", "－"), couponCode, businessHourDeliverTimeS.Value.ToString("yyyyMMdd"), finalExpireDate.ToString("yyyyMMdd"), (couponStoreSequence != null && couponStoreSequence != 0) ? "，序號" + couponStoreSequence.Value.ToString("0000") : string.Empty);
            }

            return msg;
        }

        public static string GetCouponSeparateCode(string couponCode, int digits)
        {
            couponCode = couponCode.Trim();
            string coupon = string.Empty;
            for (int i = 1; i <= couponCode.Length; i++)
            {
                if (i % digits == 0 && i != couponCode.Length)
                {
                    coupon += couponCode[i - 1] + "－";
                }
                else
                {
                    coupon += couponCode[i - 1];
                }
            }
            return coupon;
        }

        /// <summary>
        /// 取得前端頁面用的憑證資料(只有憑證)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<CouponsWebModel> GetCouponsDataForWebView(ViewCouponOnlyListMainCollection data)
        {
            List<CouponsWebModel> couponsList = new List<CouponsWebModel>();
            foreach (ViewCouponOnlyListMain row in data)
            {
                couponsList.Add((CouponsWebModel)ProcessCouponsColumnData(row));
            }

            return couponsList;
        }

        /// <summary>
        /// 取得憑證未使用數量
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static int GetCouponCanUsedCount(ViewCouponListMain main)
        {
            if (main.RemainCount == null)
            {
                return 0;
            }

            if (((int)main.RemainCount) == 0)
            {
                return 0;
            }

            //MGM 數量
            var gifts = mgmp.GiftGetByOid(main.Guid).Where(x => x.IsActive == true && x.IsUsed == false).ToList();
            var giftCount = gifts.Any() ? gifts.Count : 0; //正在送禮階段數量

            var couponType = PponFacade.GetDealCouponType(main);
            if (couponType == DealCouponType.FamilyNetPincode)
            {
                //全家被鎖定憑證
                var lockedCount = fp.GetFamiCouponsByLocked(main.Guid);

                //對方已收禮
                var acceptedGifts = gifts.Where(g => g.Accept == (int)MgmAcceptGiftType.Accept || g.Accept == (int)MgmAcceptGiftType.Quick).ToList();

                //對方已收禮並Lock
                var acceptedGiftLockCount = lockedCount.Join(acceptedGifts, f => f.CouponId, g => g.CouponId, (f, g) => new { CouponId = f.CouponId }).Count();

                //自己lock數量
                var selfLockCount = lockedCount.Count - acceptedGiftLockCount;

                //未使用 = 剩餘憑證總數 - 自己Lock總數 - 送禮總數 
                int canUsedCount = (int)main.RemainCount - selfLockCount - giftCount;

                return canUsedCount < 0 ? 0 : canUsedCount;
            }

            return (int)main.RemainCount - giftCount;

        }

        /// <summary>
        /// 取得憑證狀態
        /// </summary>
        /// <param name="ctlog"></param>
        /// <param name="returningCtlog"></param>
        /// <returns></returns>
        public static string GetCouponStatus(CashTrustLog ctlog, CashTrustLog returningCtlog)
        {
            if (ctlog != null)
            {
                if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                {
                    return "ATM退款中，無法退貨";
                }

                switch ((TrustStatus)ctlog.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        if (returningCtlog != null)
                        {
                            return "已在退貨單上，無法退貨";
                        }
                        return "未核銷，可進行退貨";
                    case TrustStatus.Verified:
                        DateTime modifyDT = ctlog.ModifyTime;
                        string modifyTime = string.Format("{0:yyyy/MM/dd HH:mm} ", modifyDT);

                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.VerificationForced))
                        {
                            return modifyTime + "已強制核銷";
                        }

                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.VerificationLost))
                        {
                            return modifyTime + "已核銷，清冊遺失";
                        }

                        return modifyTime + "已核銷";
                    case TrustStatus.Returned:
                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            return "已強制退貨";
                        }

                        return "已退購物金，無法再退貨";
                    case TrustStatus.Refunded:
                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            return "已強制退貨";
                        }

                        return "已轉退現金，無法再退貨";
                    case TrustStatus.ATM:
                        return "未付款，無法退貨";
                    default:
                        return "查無即時核銷資訊";
                }
            }

            return "查無即時核銷資訊";
        }

        public static string GetFinalExpireDateContent(Guid orderGuid, Guid bid, Guid storeGuid)
        {
            if (orderGuid == Guid.Empty && bid == Guid.Empty && storeGuid == Guid.Empty)
            {
                return string.Empty;
            }

            string expireDateContent = GetFinalExpireDate(orderGuid, bid, storeGuid);
            if (expireDateContent.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return expireDateContent;
            }
        }

        private static string GetFinalExpireDate(Guid orderGuid, Guid bid, Guid storeGuid)
        {
            if (orderGuid == Guid.Empty && bid == Guid.Empty && storeGuid == Guid.Empty)
            {
                return string.Empty;
            }

            ExpirationDateSelector selector = GetExpirationComponent(orderGuid, bid, storeGuid);

            DateTime expireDate = selector.GetExpirationDate();
            if (expireDate != selector.ExpirationDates.DealOriginalExpireDate)  //selector.ExpirationDates.DealOriginalExpireDate
            {
                if (DateTime.Equals(expireDate, selector.ExpirationDates.SellerClosedDownDate ?? DateTime.MaxValue) ||
                    DateTime.Equals(expireDate, selector.ExpirationDates.StoreClosedDownDate ?? DateTime.MaxValue))  //若結束營業日&截止日期調整兩者相同, 結束營業優先
                {
                    return expireDate.ToString("yyyy/MM/dd");
                }
                return expireDate.ToString("yyyy/MM/dd");
            }

            return string.Empty;
        }

        private static ExpirationDateSelector GetExpirationComponent(Guid orderGuid, Guid bid, Guid storeGuid)
        {
            Dictionary<Guid, ExpirationDateSelector> expirationComponents = new Dictionary<Guid, ExpirationDateSelector>();
            if (bid == Guid.Empty)
            {
                if (expirationComponents.ContainsKey(orderGuid))
                {
                    return expirationComponents[orderGuid];
                }

                ExpirationDateSelector selector = new ExpirationDateSelector();
                selector.ExpirationDates = new PponUsageExpiration(orderGuid);

                expirationComponents.Add(orderGuid, selector);
                return selector;
            }
            else
            {
                if (expirationComponents.ContainsKey(bid))
                {
                    return expirationComponents[bid];
                }

                ExpirationDateSelector selector = new ExpirationDateSelector();
                selector.ExpirationDates = new PponUsageExpiration(orderGuid, bid, storeGuid);

                expirationComponents.Add(bid, selector);
                return selector;
            }
        }

        #region 富利核銷

        public static string AutoVerifyCouponJob()
        {
            isJFVerifySucceeed = true;

            var ftp = new SimpleFtpClient(new Uri(config.PizzaHutFtpUri));

            var directories = ftp.GetDirectoryList(config.PizzaHutFtpPath);

            var trnsfrPathList = DownLoadCsvGetTrnsfrPathList(ftp, directories);

            var coupons = Csv2CouponList(trnsfrPathList);

            VerifyCoupon(coupons);

            string result = CheckFileSize(trnsfrPathList);

            #region 核銷結果(PizzaHut)

            var content = "=======================<br/>";

            content += "PizzaHut_" + DateTime.Now.ToString("yyyy/MM/dd HH:mm") + "<br/>";
            if (trnsfrPathList.Count != 0)
            {
                content += result;
                content += "======核銷憑證統計======<br/>";
                content += string.Format("總共:{0}筆<br/>", GetCodeTotal(coupons));
                content += string.Format("成功:{0}筆<br/>", succeedCoupons.Count);
                content += string.Format("失敗:{0}筆<br/>", failedCoupons.Count);
                content += "=======================<br/>";
            }
            else
            {
                content += "======核銷憑證統計======<br/>";
                content += "無任何核銷資料<br/>";
                content += "=======================<br/>";
            }

            if (failedCoupons.Any())
            {
                isJFVerifySucceeed = false;
                foreach (var failedCoupon in failedCoupons)
                {
                    content += string.Format("Path:{0},Code:{1},Message{2}<br/>"
                        , failedCoupon.Path, failedCoupon.Code, failedCoupon.Message);
                }
            }

            #endregion

            ftp = new SimpleFtpClient(new Uri(config.KfcFtpUri));

            directories = ftp.GetDirectoryList(config.KfcFtpPath);

            trnsfrPathList = DownLoadCsvGetTrnsfrPathList(ftp, directories);

            coupons = Csv2CouponList(trnsfrPathList);

            VerifyCoupon(coupons);

            result = CheckFileSize(trnsfrPathList);

            #region 核銷結果(KFC)

            content += "KFC_" + DateTime.Now.ToString("yyyy/MM/dd HH:mm") + "<br/>";

            if (trnsfrPathList.Count != 0)
            {
                content += result;
                content += "======核銷憑證統計======<br/>";
                content += string.Format("總共:{0}筆<br/>", GetCodeTotal(coupons));
                content += string.Format("成功:{0}筆<br/>", succeedCoupons.Count);
                content += string.Format("失敗:{0}筆<br/>", failedCoupons.Count);
                content += "=======================<br/>";
            }
            else
            {
                content += "======核銷憑證統計======<br/>";
                content += "無任何核銷資料<br/>";
                content += "=======================<br/>";
            }

            if (failedCoupons.Any())
            {
                isJFVerifySucceeed = false;
                foreach (var failedCoupon in failedCoupons)
                {
                    content += string.Format("Path:{0},Code:{1},Message{2}<br/>"
                        , failedCoupon.Path, failedCoupon.Code, failedCoupon.Message);
                }
            }

            #endregion

            #region Mail

            try
            {
                MailMessage msg = new MailMessage();

                msg.From = new MailAddress(config.SystemEmail);

                foreach (var mailTo in config.JfMailTo.Split(','))
                {
                    msg.To.Add(mailTo);
                }

                msg.Subject = config.JfMailSubject + DateTime.Now.ToString("yyyy/MM/dd") + (isJFVerifySucceeed ? "[成功]" : "[有異常項目,請確認]");
                msg.Body = content;
                msg.IsBodyHtml = true;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);

            }
            catch (Exception ex)
            {
                logger.Error("富利核銷資料寄送失敗:" + ex.Message + "<br/>" + ex.StackTrace);
            }

            #endregion

            return content;

        }

        private static string CheckFileSize(List<string> trnsfrPathList)
        {
            string result = string.Empty;
            foreach (var data in trnsfrPathList)
            {
                var file = new FileInfo(data);
                if (file.Length == 0)
                {
                    file.Delete();
                    result += string.Format("檔案上傳失敗(大小0kb):{0}<br/>", data);
                    isJFVerifySucceeed = false;
                }
                else
                {
                    result += string.Format("今日核銷檔案:{0}<br/>", data);
                }
            }

            return result;
        }

        private static List<string> DownLoadCsvGetTrnsfrPathList(SimpleFtpClient ftp, List<string> directories)
        {
            var trnsfrPathList = new List<string>();

            using (WebClient ftpClient = new WebClient())
            {
                ftpClient.Credentials = new System.Net.NetworkCredential(ftp.Config.UserId, ftp.Config.Password);

                for (int i = 0; i <= directories.Count - 1; i++)
                {
                    if (directories[i].Contains(".csv"))
                    {
                        string path = "ftp://" + ftp.Config.Host + "/" + directories[i].ToString();
                        string trnsfrPath = Path.Combine(config.WebTempPath, "JF", directories[i].ToString());
                        string localPath = Path.Combine(config.WebTempPath, "JF", directories[i].ToString().Split('/')[0]);

                        if (!Directory.Exists(localPath))
                        {
                            Directory.CreateDirectory(localPath);
                        }

                        if (File.Exists(trnsfrPath))
                        {
                            continue;
                        }

                        ftpClient.DownloadFile(path, trnsfrPath);

                        trnsfrPathList.Add(trnsfrPath);

                    }
                }
            }

            return trnsfrPathList;
        }

        private static void VerifyCoupon(List<Coupons> coupons)
        {

            failedCoupons = new List<FailedCoupon>();
            succeedCoupons = new List<SucceedCoupon>();

            foreach (var item in coupons)
            {
                foreach (var code in item.Codes)
                {
                    var cashTrustLog = mp.CashTrustLogGetByCouponSequenceNumber(code);

                    var viewCoupon = mp.GetViewCouponListByOrderGuid(cashTrustLog.OrderGuid).FirstOrDefault(p => p.SequenceNumber == cashTrustLog.CouponSequenceNumber);
                    if (viewCoupon != null)
                    {
                        var accessibleCoupons = GetAccessibleCoupons(viewCoupon.SequenceNumber, viewCoupon.Code, cashTrustLog.TrustId).Select(info => new VbsVerifyCouponInfo
                        {
                            DealName = info.ItemName,
                            DealId = info.DealId,
                            DealType = info.DealType,
                            CouponId = info.CouponSequence,
                            CouponCode = info.CouponCode,
                            ExchangePeriodStartTime = info.ExchangePeriodStartTime,
                            ExchangePeriodEndTime = info.ExchangePeriodEndTime,
                            VerifyState = info.VerifyState,
                            ModifyTime = info.ModifyTime,
                            PayerName = VerificationFacade.SetPayerNameHidden(info.BuyerName),
                            TrustId = info.TrustId.ToString(),
                            Message = string.Empty
                        }).ToList();
                        var vbsAccessibleCoupons = VerificationFacade.GetVbsAccessibleCoupon(accessibleCoupons);

                        if (vbsAccessibleCoupons.Any())
                        {
                            var vbsAccessibleCoupon = vbsAccessibleCoupons.FirstOrDefault();

                            if (vbsAccessibleCoupon.IsCouponAvailable)
                            {
                                var verifyResult = VerifyCoupon(vbsAccessibleCoupon);

                                if (verifyResult.IsVerifySuccess)
                                {
                                    succeedCoupons.Add(new SucceedCoupon()
                                    {
                                        Code = vbsAccessibleCoupon.CouponCode,
                                        Path = item.Path,
                                        Message = verifyResult.Message
                                    });
                                }
                                else
                                {
                                    failedCoupons.Add(new FailedCoupon()
                                    {
                                        Code = vbsAccessibleCoupon.CouponCode,
                                        Path = item.Path,
                                        Message = verifyResult.Message
                                    });
                                }
                            }
                            else
                            {
                                failedCoupons.Add(new FailedCoupon()
                                {
                                    Code = vbsAccessibleCoupon.CouponCode,
                                    Path = item.Path,
                                    Message = vbsAccessibleCoupon.Message
                                });
                            }
                        }
                        else
                        {
                            failedCoupons.Add(new FailedCoupon()
                            {
                                Code = cashTrustLog.CouponSequenceNumber,
                                Path = item.Path,
                                Message = "查無此憑證(vbsAccessibleCoupons.Any()==false)"
                            });
                        }
                    }
                    else
                    {
                        failedCoupons.Add(new FailedCoupon()
                        {
                            Code = code,
                            Path = item.Path,
                            Message = "查無此憑證(viewCoupon==null)"
                        });
                    }
                }
            }
        }

        private static VbsVerifyResult VerifyCoupon(VbsVerifyCouponInfo vbsAccessibleCoupon)
        {
            var status = OrderFacade.VerifyCoupon(Guid.Parse(vbsAccessibleCoupon.TrustId), "sys", true);

            VbsVerifyResult verifyResult = VerificationFacade.GetVbsVerifyResult(status, vbsAccessibleCoupon.TrustId);

            return verifyResult;
        }

        private static IEnumerable<VerificationCouponInfo> GetAccessibleCoupons(string couponSequence, string couponCode, Guid trustId)
        {
            #region initialize CouponInfo

            var pponCouponInfos = vp.GetVerifiedCouponDataWithPpon(couponSequence, couponCode, trustId, false).ToList();
            var piinCouponInfos = vp.GetVerifiedCouponDataWithPiinLife(couponSequence, couponCode, trustId, false).ToList();

            #endregion

            var verifyState = VbsCouponVerifyState.Null;

            var couponInfos = new List<VerificationCouponInfo>();

            #region Ppon

            foreach (VerifiedCouponData couponInfo in pponCouponInfos)
            {
                switch (couponInfo.Status)
                {
                    case ((int)TrustStatus.Initial):
                    case ((int)TrustStatus.Trusted):
                        verifyState = VbsCouponVerifyState.UnUsed;
                        break;
                    case ((int)TrustStatus.Verified):
                        verifyState = VbsCouponVerifyState.Verified;
                        break;
                    case ((int)TrustStatus.Refunded):
                    case ((int)TrustStatus.Returned):
                        verifyState = VbsCouponVerifyState.Return;
                        break;
                    case ((int)TrustStatus.ATM):
                        break;
                }
                //強制退貨 視為已核銷
                if ((couponInfo.SpecialStatus & (int)TrustSpecialStatus.ReturnForced) > 0)
                {
                    verifyState = VbsCouponVerifyState.Verified;
                }

                //Atm退款中 視為已退貨
                if ((couponInfo.SpecialStatus & (int)TrustSpecialStatus.AtmRefunding) > 0)
                {
                    verifyState = VbsCouponVerifyState.Return;
                }

                var info = new VerificationCouponInfo
                {
                    DealName = couponInfo.DealName,
                    ItemName = couponInfo.ItemName.Replace("&nbsp", " "),  //避免App顯示  &nbsp 字元
                    DealId = couponInfo.DealId.ToString(),
                    DealType = BusinessModel.Ppon,
                    CouponSequence = couponInfo.CouponSequence,
                    CouponCode = couponInfo.CouponCode,
                    ExchangePeriodStartTime = couponInfo.UseStartTime,
                    ExchangePeriodEndTime = couponInfo.UseEndTime,
                    VerifyState = verifyState,
                    ModifyTime = couponInfo.ModifyTime,
                    BuyerName = couponInfo.MemberName,
                    TrustId = couponInfo.TrustId
                };

                couponInfos.Add(info);
            }

            #endregion

            #region 品生活

            foreach (VerifiedCouponData couponInfo in piinCouponInfos)
            {
                switch (couponInfo.Status)
                {
                    case ((int)TrustStatus.Initial):
                    case ((int)TrustStatus.Trusted):
                        verifyState = VbsCouponVerifyState.UnUsed;
                        break;
                    case ((int)TrustStatus.Verified):
                        verifyState = VbsCouponVerifyState.Verified;
                        break;
                    case ((int)TrustStatus.Refunded):
                    case ((int)TrustStatus.Returned):
                        verifyState = VbsCouponVerifyState.Return;
                        break;
                    case ((int)TrustStatus.ATM):
                        break;
                }
                //強制退貨數視為已核銷
                if ((couponInfo.SpecialStatus & (int)TrustSpecialStatus.ReturnForced) > 0)
                {
                    verifyState = VbsCouponVerifyState.Verified;
                }

                var info = new VerificationCouponInfo
                {
                    DealName = couponInfo.DealName,
                    ItemName = couponInfo.ItemName.Replace("&nbsp", " "), //避免App顯示  &nbsp 字元
                    DealId = couponInfo.DealId.ToString(),
                    DealType = BusinessModel.PiinLife,
                    CouponSequence = couponInfo.CouponSequence,
                    CouponCode = couponInfo.CouponCode,
                    ExchangePeriodStartTime = couponInfo.UseStartTime,
                    ExchangePeriodEndTime = couponInfo.UseEndTime,
                    VerifyState = verifyState,
                    ModifyTime = couponInfo.ModifyTime,
                    BuyerName = couponInfo.MemberName,
                    TrustId = couponInfo.TrustId
                };

                couponInfos.Add(info);
            }

            #endregion

            return couponInfos;
        }

        private static List<Coupons> Csv2CouponList(List<string> trnsfrPathList, int ignoreRow = 1)
        {
            var result = new List<Coupons>();

            foreach (var path in trnsfrPathList)
            {
                var codes = new List<string>();

                int ignoreRowCount = 0;
                var lines = File.ReadAllLines(path);
                foreach (var line in lines)
                {
                    ignoreRowCount++;
                    if (ignoreRowCount <= ignoreRow)
                    {
                        continue;
                    }
                    if (line == null || line.Contains(",") == false)
                    {
                        continue;
                    }
                    string code = line.Split(',')[0].Trim();
                    codes.Add(code);
                }

                result.Add(new Coupons()
                {
                    Path = path,
                    Codes = codes
                });
            }

            return result;
        }

        private static int GetCodeTotal(List<Coupons> coupons)
        {
            var total = 0;

            foreach (var coupon in coupons)
            {
                total += coupon.Codes.Count;
            }

            return total;
        }

        private class FailedCoupon
        {
            public string Code { get; set; }
            public string Path { get; set; }
            public string Message { get; set; }
        }

        private class SucceedCoupon
        {
            public string Code { get; set; }
            public string Path { get; set; }
            public string Message { get; set; }
        }

        private class Coupons
        {
            public string Path { get; set; }
            public List<string> Codes { get; set; }
        }

        #endregion


    }
}

﻿using System;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Facade
{
    public class CmsContentFacade
    {
        private static ICmsProvider cp;

        static CmsContentFacade()
        {
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        }

        public static string GetDataFromCache(string contentName)
        {
            return (string)HttpRuntime.Cache[contentName];
        }

        private static void PutDataToCache(string contentName, int durationMinutes, string data)
        {
            HttpRuntime.Cache.Remove(contentName);
            HttpRuntime.Cache.Insert(contentName, data, null, DateTime.Now.AddMinutes(durationMinutes),
                TimeSpan.Zero, CacheItemPriority.NotRemovable, null);
        }

        public static string GetContentNameByContentId(string contentId, string path)
        {
            string contentName = string.Empty;
            if (string.Equals("paragraph1", contentId))
            {
                contentName = "/ppon/default.aspx_paragraph1";
            }
            else if (string.Equals("headerscript", contentId))
            {
                contentName = "/ppon/default.aspx_headerscript";
            }
            else
            {
                contentName = path + "_" + contentId;
            }

            return contentName;
        }

        public static void SetDataToCache(string contentId, string path)
        {
            bool initializeCache = true;
            int durationMinutes = 1440;
            string contentName = GetContentNameByContentId(contentId, path);
            string contentBody = string.Empty;
            string[] UserRole = Roles.GetRolesForUser();
            CmsContent cmsContent = cp.CmsContentGet(contentName);

            if (!cmsContent.IsLoaded)
            {
                if (UserRole != null && !string.IsNullOrEmpty(Array.Find(UserRole, (a) => a == MemberRoles.Editor.ToString("g"))))
                {
                    cmsContent = new CmsContent();
                    cmsContent.ContentName = contentName;
                    cmsContent.CreatedBy = HttpContext.Current.User.Identity.Name;
                    cmsContent.CreatedOn = DateTime.Now;
                    cmsContent.Locale = "zh_TW";
                    cp.CmsContentSet(cmsContent);
                }
                else
                {
                    initializeCache = false;
                }
            }
            else
            {
                contentBody = cmsContent.Body;
            }

            if (initializeCache)
            {
                PutDataToCache(contentName, durationMinutes, contentBody);
            }
        }
    }
}

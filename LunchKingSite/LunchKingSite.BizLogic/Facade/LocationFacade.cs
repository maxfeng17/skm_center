using System.IO;
using System.Net;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;
using System.Data.SqlTypes;
using System.Threading;
using log4net;
using LunchKingSite.DataOrm;
using System.Linq;
using System.Runtime.CompilerServices;
using LunchKingSite.BizLogic.Model;


namespace LunchKingSite.BizLogic.Facade
{
    /// <summary>
    /// 與空間座標、地理區域、行政區相關之作業
    /// </summary>
    public class LocationFacade
    {
        protected static ILog logger = LogManager.GetLogger("LocationFacade");

        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();

        /// <summary>
        /// 4326為一空間座標參照之系統代碼，此代號所表示的系統為以經緯度表示位置之系統
        /// </summary>
        public const int CoordinatesSrid = 4326;

        /// <summary>
        /// 輸入經緯度字串即可回傳一個形式為點的SqlGeography物件
        /// </summary>
        /// <param name="latitude">緯度</param>
        /// <param name="longitude">經度</param>
        /// <returns>SqlGeography</returns>
        public static SqlGeography GetGeographicPoint(string latitude, string longitude)
        {
            double lat;
            double lon;
            if (!double.TryParse(latitude, out lat))
            {
                return null;
            }
            if (!double.TryParse(longitude, out lon))
            {
                return null;
            }

            ApiCoordinates coordinates = new ApiCoordinates(lon, lat);

            return GetGeographicPoint(coordinates);
        }

        /// <summary>
        /// 傳入ApiCoordinates依據其經緯度建立SqlGeography物件，若傳入物件不存在或有異常，回傳NULL
        /// </summary>
        /// <param name="coordinates"></param>
        /// <returns></returns>
        public static SqlGeography GetGeographicPoint(ApiCoordinates coordinates)
        {
            //傳入物件不存在
            if (coordinates == null)
            {
                return null;
            }

            SqlGeographyBuilder builder = new SqlGeographyBuilder();

            builder.SetSrid(CoordinatesSrid);
            builder.BeginGeography(OpenGisGeographyType.Point);
            builder.BeginFigure(coordinates.Latitude, coordinates.Longitude);
            builder.EndFigure();
            builder.EndGeography();
            return builder.ConstructedGeography;
        }

        /// <summary>
        /// 輸入經緯度字串即可回傳一個Well-Known Text的字串方便利用SubSonic model存入geography資料類別的欄位
        /// 
        /// ex:
        ///    Building bd = lp.BuildingGet(new Guid("D55AEC05-C274-4A2A-941D-0C583A4F9273"));
        ///    bd.Coordinate = LocationFacade.GetGeographyWKT("25.039247", "121.524542");
        ///    lp.BuildingSet(bd);
        /// </summary>
        /// <param name="latitude">緯度</param>
        /// <param name="longitude">經度</param>
        /// <returns>String</returns>
        public static String GetGeographyWKT(string latitude, string longitude)
        {
            SqlGeography geo = GetGeographicPoint(latitude, longitude);
            if (geo != null)
            {
                return geo.ToString();
            }
            else
            {
                return null;
            }
        }

        public static String GetGoogleMapsAPIKey()
        {
            return ProviderFactory.Instance().GetConfig().GoogleMapsAPIKey;
        }
        /// <summary>
        /// 根據TownshipId及地址查找GoogleAPI取得經緯度(key=Lat, value=long)
        /// </summary>
        /// <param name="township_id">TownshipId</param>
        /// <param name="address">地址</param>
        /// <returns></returns>
        public static KeyValuePair<string, string> GetAreaWithGoogleMap(string address, int failTry = 0)
        {
            KeyValuePair<string, string> coordinate = new KeyValuePair<string, string>();
            if (string.IsNullOrWhiteSpace(address))
            {
                return coordinate;
            }

            GoogleGeoCodeResponse geoCodes = GetGoogleGeoCodeResponse(address);

            // 最高限制嘗試3次請求，若超過此限則視為例外
            if (failTry < 3 && geoCodes.status == "OVER_QUERY_LIMIT")
            {
                failTry++;
                // 停頓2秒重新發送請求 https://developers.google.com/maps/documentation/business/articles/usage_limits#limitexceeded
                Thread.Sleep(2000);
                coordinate = GetAreaWithGoogleMap(address, failTry);
            }
            else if (geoCodes.status == "OK" && geoCodes.results.Length > 0)
            {
                //location_type有四種(ROOFTOP、RANGE_INTERPOLATED、GEOMETRIC_CENTER、APPROXIMATE)，只有ROOFTOP傳回的經緯度最準確，其他皆會有誤差
                if (geoCodes.results[0].geometry.location_type == "ROOFTOP")
                {
                    Geometry geo = geoCodes.results[0].geometry;
                    coordinate = new KeyValuePair<string, string>(geo.location.lat, geo.location.lng);
                }
            }
            else
            {
                logger.Info("GetArea by GoogleAPIs Geocode error: " + geoCodes);
            }
            return coordinate;
        }
        /// <summary>
        /// 根據TownshipId及地址查找GoogleAPI取得經緯度和精確度(location_type,(Lat,long))
        /// </summary>
        /// <param name="township_id">TownshipId</param>
        /// <param name="address">地址</param>
        /// <returns></returns>
        public static KeyValuePair<string, KeyValuePair<string, string>> GetAreaWithGoogleMap2(string address, int failTry = 0)
        {
            KeyValuePair<string, string> coordinate = new KeyValuePair<string, string>();
            KeyValuePair<string, KeyValuePair<string, string>> temp = new KeyValuePair<string, KeyValuePair<string, string>>();
            if (string.IsNullOrWhiteSpace(address))
            {
                return temp;
            }

            GoogleGeoCodeResponse geoCodes = GetGoogleGeoCodeResponse(address);

            // 最高限制嘗試3次請求，若超過此限則視為例外
            if (failTry < 3 && geoCodes.status == "OVER_QUERY_LIMIT")
            {
                failTry++;
                // 停頓2秒重新發送請求 https://developers.google.com/maps/documentation/business/articles/usage_limits#limitexceeded
                Thread.Sleep(2000);
                temp = GetAreaWithGoogleMap2(address, failTry);
            }
            else if (geoCodes.status == "OK" && geoCodes.results.Length > 0)
            {
                //location_type有四種(ROOFTOP、RANGE_INTERPOLATED、GEOMETRIC_CENTER、APPROXIMATE)，不管誤差皆回傳
                Geometry geo = geoCodes.results[0].geometry;
                coordinate = new KeyValuePair<string, string>(geo.location.lat, geo.location.lng);
                temp = new KeyValuePair<string, KeyValuePair<string, string>>(geoCodes.results[0].geometry.location_type, coordinate);
            }
            else
            {
                logger.Info("GetAreaWithGoogleMap2 by GoogleAPIs Geocode error: " + geoCodes);
            }
            return temp;
        }

        public static SqlGeography GetGeography(string address)
        {
            if (config.EnableBingMapAPI)
            {
                KeyValuePair<string, string> coordinate = GetAreaWithBingMap(address);
                return GetGeographicPoint(coordinate.Key, coordinate.Value);
            }
            else
            {
                return GetGeographyWithGoogleMap(address);
            }
        }

        public static SqlGeography GetGeographyWithGoogleMap(string address, int failTry = 0)
        {
            SqlGeography rtn = null;
            if (string.IsNullOrWhiteSpace(address))
            {
                return null;
            }

            GoogleGeoCodeResponse geoCodes = GetGoogleGeoCodeResponse(address);

            // 最高限制嘗試3次請求，若超過此限則視為例外
            if (failTry < 3 && geoCodes.status == "OVER_QUERY_LIMIT")
            {
                failTry++;
                // 停頓2秒重新發送請求 https://developers.google.com/maps/documentation/business/articles/usage_limits#limitexceeded
                Thread.Sleep(2000);
                rtn = GetGeographyWithGoogleMap(address, failTry);
            }
            else if (geoCodes.status == "OK" && geoCodes.results.Length > 0)
            {
                Geometry geo = geoCodes.results[0].geometry;
                rtn = GetGeographicPoint(geo.location.lat, geo.location.lng);
            }
            else
            {
                logger.Info("GetGeography by GoogleAPIs Geocode error: " + geoCodes);
            }
            return rtn;
        }
        
        private static GoogleGeoCodeResponse GetGoogleGeoCodeResponse(string address)
        {
            ProposalFacade.ProposalPerformanceLogSet("GetGoogleGeoCodeResponse", address, "sys");
            string output = "json";
            string url = string.Format("http://maps.googleapis.com/maps/api/geocode/{0}?address={1}&language=zh-TW", output, address); //2016/03/24版本已不需要sensor參數
            // 送出要求  
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            // 取得回應  
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // 讀取結果 
            StreamReader sr = new StreamReader(response.GetResponseStream());
            // 解析
            GoogleGeoCodeResponse geoCodes = new JsonSerializer().Deserialize<GoogleGeoCodeResponse>(sr.ReadToEnd());
            return geoCodes;
        }
        private static GoogleGeoCodeResponse GetGoogleGeoCodeResponse(double latitude, double longitude)
        {
            string output = "json";
            string url = string.Format("http://maps.googleapis.com/maps/api/geocode/{0}?latlng={1},{2}&language=zh-tw", output, latitude, longitude); //2016/03/24版本已不需要sensor參數
            // 送出要求  
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            // 取得回應  
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // 讀取結果 
            StreamReader sr = new StreamReader(response.GetResponseStream());
            // 解析
            GoogleGeoCodeResponse geoCodes = new JsonSerializer().Deserialize<GoogleGeoCodeResponse>(sr.ReadToEnd());
            return geoCodes;
        }

        public static SqlGeography GetGeographyByCoordinate(string coordinate)
        {
            return SqlGeography.STPointFromText(new SqlChars(!string.IsNullOrWhiteSpace(coordinate) ? coordinate : "POINT EMPTY"), CoordinatesSrid);
        }

        public static KeyValuePair<double, double> GetLatLongitudeByGeography(string coordinate)
        {
            SqlGeography geo = GetGeographyByCoordinate(coordinate);            
            return geo == null ? new KeyValuePair<double, double>(0, 0)  : new KeyValuePair<double, double>((double)geo.Lat, (double)geo.Long);
        }

        /// <summary>
        /// 回傳from到to間的距離，單位為公尺
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static double GetDistance(SqlGeography from, SqlGeography to)
        {
            if (to.Lat.IsNull || to.Long.IsNull)
            {
                return 0;
            }

            return from.STDistance(to).Value;
        }

        /// <summary>
        /// 傳入距離(單位公尺)，依據此參數回傳距離表示用的字串，例如 1000 表示為 1.0km
        /// </summary>
        /// <param name="distance">距離的值，單位公尺</param>
        /// <returns></returns>
        public static string GetDistanceDesc(double distance)
        {
            return string.Format("{0:0.0km}", distance / 1000);
        }

        /// <summary>
        /// 為避免GoogleMap服務失效，提供BingMap作為切換
        /// </summary>
        /// <param name="township_id"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        public static KeyValuePair<string, string> GetArea(string address)
        {
            if (config.EnableBingMapAPI)
            {
                return GetAreaWithBingMap(address);
            }
            else
            {
                return GetAreaWithGoogleMap(address);
            }
        }

        /// <summary>
        /// GoogleMap取經緯資料和精確度
        /// </summary>
        /// <param name="township_id"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        public static KeyValuePair<string, KeyValuePair<string, string>> GetArea2(string address)
        {
            return GetAreaWithGoogleMap2(address);
        }

        public static ApiCoordinates GetApiCoordinates(int townshipId, string address)
        {
            var pair = GetArea(CityManager.CityTownShopStringGet(townshipId) + address);
            ApiCoordinates coordinate;
            if (ApiCoordinates.TryParse(pair.Value, pair.Key, out coordinate) == false)
            {
                coordinate = ApiCoordinates.Default;
            }
            return coordinate;
        }

        public static KeyValuePair<string, string> GetAreaWithBingMap(string address)
        {
            KeyValuePair<string, string> coordinate = new KeyValuePair<string, string>();
            string key = config.BingMapApiKey;
            try
            {
                string output = "json";
                Uri uri = new Uri(string.Format("http://dev.virtualearth.net/REST/v1/Locations?o={0}&q={1},TW&key={2}", output, address, key));
                // 送出要求  
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.UseDefaultCredentials = true;
                // 取得回應  
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                // 讀取結果 
                StreamReader sr = new StreamReader(response.GetResponseStream());
                // 解析
                dynamic result = new JsonSerializer().Deserialize<dynamic>(sr.ReadToEnd());
                if (result.resourceSets[0].estimatedTotal.Value > 0)
                {
                    coordinate = new KeyValuePair<string, string>(
                        result.resourceSets[0].resources[0].point.coordinates[0].Value.ToString(),
                        result.resourceSets[0].resources[0].point.coordinates[1].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("Bing Map REST Services get geocode error!! address: {0} <br /> error message: {1}", address, ex);
                throw;
            }
            return coordinate;
        }

        public static PponCity GetAreaCityWithBingMap(double latitude, double longitude, out City actual_city)
        {
            string key = config.BingMapApiKey;
            try
            {
                StreamReader sr;
                try
                {
                    Uri uri = new Uri(string.Format(@"http://dev.virtualearth.net/REST/v1/Locations/{0},{1}?includeEntityTypes=Postcode1&o=json&key={2}", latitude, longitude, key));
                    // 送出要求  
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                    // 取得回應  
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    // 讀取結果 
                    sr = new StreamReader(response.GetResponseStream());
                    // 解析

                }
                catch (WebException ex)
                {
                    logger.DebugFormat("Bing Map REST Services get geocode error!! coordinate: {0}/{1} <br /> error message: {2}", latitude, longitude, ex);
                    actual_city = CityManager.CityGetById(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
                    return PponCityGroup.DefaultPponCityGroup.TaipeiCity;
                }

                // 解析
                dynamic result = new JsonSerializer().Deserialize<dynamic>(sr.ReadToEnd());
                //resources null 情況
                if (result.resourceSets[0].resources.Count == 0)
                {
                    //logger.DebugFormat("Bing Map REST Services get geocode error!! coordinate: {0}/{1}<br />error message:(result.resourceSets[0].resources.Count==0)", latitude, longitude);
                    actual_city = CityManager.CityGetById(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
                    return PponCityGroup.DefaultPponCityGroup.TaipeiCity;
                }
                else if (object.ReferenceEquals(null, result.resourceSets[0].resources[0].address) || object.ReferenceEquals(null, result.resourceSets[0].resources[0].address.postalCode))
                {
                    //logger.DebugFormat("Bing Map REST Services get geocode error!! coordinate: {0}/{1}<br />error message:(Object.ReferenceEquals(null, result.resourceSets[0].resources[0].address.postalCode))", latitude, longitude);
                    actual_city = CityManager.CityGetById(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
                    return PponCityGroup.DefaultPponCityGroup.TaipeiCity;
                }

                string zip_code = result.resourceSets[0].resources[0].address.postalCode.Value;
                actual_city = CityManager.CityGetByZipCode(zip_code);
                if (actual_city != null)
                {
                    return PponCityGroup.GetLargeAreaPponCityByCityId(actual_city.Code);
                }
                else
                {
                    actual_city = CityManager.CityGetById(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
                    return PponCityGroup.DefaultPponCityGroup.TaipeiCity;
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("Bing Map REST Services get geocode error!! coordinate: {0}/{1} <br /> error message: {2}", latitude, longitude, ex);
                actual_city = CityManager.CityGetById(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
                return PponCityGroup.DefaultPponCityGroup.TaipeiCity;
            }
        }

        public static PponCity GetAreaCityWithGoogleMap(double latitude, double longitude, out City actual_city)
        {
            GoogleGeoCodeResponse geoCodes = GetGoogleGeoCodeResponse(latitude, longitude);
            if (geoCodes.status == "OK" && geoCodes.results.Length > 0)
            {
                string zip_code = geoCodes.results[0].address_components[geoCodes.results[0].address_components.GetLength(0) - 1].long_name;//取郵遞區號
                actual_city = CityManager.CityGetByZipCode(zip_code);
                if (actual_city != null)
                {
                    return PponCityGroup.GetLargeAreaPponCityByCityId(actual_city.Code);
                }
                else
                {
                    logger.Info("GetAreaCityWithGoogleMap, status_OK, by GoogleAPIs Geocode error: " + geoCodes);
                    actual_city = CityManager.CityGetById(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
                    return PponCityGroup.DefaultPponCityGroup.TaipeiCity;
                }
            }
            else
            {
                logger.Info("GetAreaCityWithGoogleMap by GoogleAPIs Geocode error: " + geoCodes);
                actual_city = CityManager.CityGetById(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
                return PponCityGroup.DefaultPponCityGroup.TaipeiCity;
            }
        }

        public static PponCity GetPponCityByCityName(string cityName)
        {
            CityGroup cityGroup = CityManager.CityGroupGetByCityName(cityName, CityGroupType.Subscription);
            if (cityGroup != null)
            {
                return PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityGroup.RegionId);
            }
            else
            {
                return null;
            }
        }

        public static IEnumerable<dynamic> StoreNearMRTLocation(double latitude, double longitude)
        {
            MrtLocationInfoCollection mrtLocationInfos = lp.MrtLocationInfoCollectionGetNearByLocation(latitude, longitude);

            var result =
                mrtLocationInfos.Select(
                    x => new {name = x.Name, code = x.Code, type = (int) MrtReleaseshipStoreSettingType.System});

            //var result = new[] { new { name = "test01", code = 4, type = 1 }, new { name = "test02", code = 2, type = 1 }, new { name = "test02", code = 2, type = 1 }, new { name = "test02", code = 2, type = 1 }, new { name = "test02", code = 2, type = 1 } };

            return result;
        }

    }

    #region Google GeoCode Response
    public class GoogleGeoCodeResponse
    {

        public string status { get; set; }
        public Results[] results { get; set; }
    }

    public class Results
    {
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string[] types { get; set; }
        public AddressComponent[] address_components { get; set; }
    }

    public class Geometry
    {
        public string location_type { get; set; }
        public Location location { get; set; }
    }

    public class Location
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public string[] types { get; set; }
    }
    #endregion
}

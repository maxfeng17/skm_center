﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using SubSonic;


namespace LunchKingSite.BizLogic.Facade
{
    public class VBSShipFacade
    {
        #region interface
        private IPponProvider _pp;
        private IOrderProvider _op;
        #endregion


        public VBSShipFacade()
        {
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        public Dictionary<string, int> GetMergeProductStatistics(List<ViewOrderProductOptionList> infos)
        {
            if (infos.FirstOrDefault() != null)
            {
                    var result = from x in infos
                                 where !x.IsReturned && !x.IsReturning
                                 orderby x.CatgSeq
                                 group x by new { x.ItemName,x.OrderProductId,x.QuantityMultiplier }
                                     into g
                                     orderby g.Key.OrderProductId
                                     select new
                                     {
                                         name = g.Key.ItemName,
                                         optionName =
                                             g.Where(r => r.OrderProductId == g.Key.OrderProductId)
                                                 .Select(s => s.CatgName + s.OptionName)
                                                 .ToList()
                                                 .Aggregate((a, b) => string.Join(" ， ", b, a)),
                                         combo_quantity = (g.Key.QuantityMultiplier != null && g.Any(s => s.OptionName == null)) ? g.Key.QuantityMultiplier.Value : 1
                                     };

                    return
                        result.GroupBy(x => new { g_name = (x.name.IndexOf("（") > 0) ? x.name.Substring(0, x.name.IndexOf("（")) : x.name, g_optionName = x.optionName })
                        .Select(x => new { s_name = x.Key.g_name, s_optionName = x.Key.g_optionName , count = x.Sum(w=> w.combo_quantity) })
                        .ToDictionary(item => (!string.IsNullOrWhiteSpace(item.s_optionName)) ? item.s_name + "：" + item.s_optionName : item.s_name , item => item.count);
            }
            return new Dictionary<string, int>();
        }

        public Dictionary<string, int> GetMergeProductStatisticsNullShip(int IsMergeCount, IEnumerable<int> bids)
        {
            var infos = _op.ViewOrderProductOptionListGetReadyItems(bids);

            switch (IsMergeCount)
            {
                case 1:
                    return GetMergeProductStatistics(infos);
                case 0:
                    return GetIndividualProductStatistics(infos);
            }
            return new Dictionary<string, int>();
        }

        public Dictionary<string, int> GetIndividualProductStatistics(List<ViewOrderProductOptionList> infos)
        {
            return
                infos.Where(x => !x.IsReturned && !x.IsReturning)
                    .GroupBy(x => new { x.CatgName, x.OptionName })
                    .ToDictionary(item => item.Key.CatgName + " " + item.Key.OptionName, item => item.Count());

        }
    }
}

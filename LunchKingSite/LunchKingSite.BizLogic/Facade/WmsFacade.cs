﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models.Wms;
using LunchKingSite.BizLogic.Models.Wms.ViewModels;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using WmsModel = LunchKingSite.BizLogic.Models.Wms;
using System.Text;
using LunchKingSite.BizLogic.Model.API;
using log4net;
using System.IO;
using System.Web.Hosting;
using System.Diagnostics;
using System.Net;
using System.Web;

namespace LunchKingSite.BizLogic.Facade
{
    public class WmsHelper
    {
        private static readonly Security PchomeWmsAES;
        static ISysConfProvider config = ProviderFactory.Instance().GetConfig();


        static WmsHelper()
        {
            PchomeWmsAES = new Security(SymmetricCryptoServiceProvider.AES,
                Encoding.ASCII.GetBytes(config.PchomeWmsAESKey), Encoding.ASCII.GetBytes(config.PchomeWmsAESIV), CipherMode.CBC, PaddingMode.Zeros)
            {
                StringStyle = EncryptedStringStyle.Hex
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="plainStr"></param>
        /// <returns></returns>
        public static string EncryptStr(string plainStr)
        {
            if (string.IsNullOrEmpty(plainStr))
            {
                return string.Empty;
            }
            string encryptedStr = PchomeWmsAES.Encrypt(plainStr);
            return encryptedStr;
        }
        public static string DecryptStr(string encryptedStr)
        {
            if (string.IsNullOrEmpty(encryptedStr))
            {
                return string.Empty;
            }
            string plainStr = PchomeWmsAES.Decrypt(encryptedStr);
            return plainStr;
        }
        /// <summary>
        /// 呼叫API時，加密聯絡人資料用
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public static WmsPerson Encrypt(WmsPerson person)
        {
            if (person == null)
            {
                return null;
            }
            return new WmsPerson
            {
                Name = EncryptStr(person.Name),
                Tel = EncryptStr(person.Tel),
                Mobile = EncryptStr(person.Mobile),
                Email = EncryptStr(person.Email),
                Zip = EncryptStr(person.Zip),
                Address = EncryptStr(person.Address)
            };
        }

        public static WmsPerson Decrypt(WmsPerson person)
        {
            if (person == null)
            {
                return null;
            }
            return new WmsPerson
            {
                Name = DecryptStr(person.Name),
                Tel = DecryptStr(person.Tel),
                Mobile = DecryptStr(person.Mobile),
                Email = DecryptStr(person.Email),
                Zip = DecryptStr(person.Zip),
                Address = DecryptStr(person.Address)
            };
        }
    }
    public class WmsFacade
    {
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        static IPponProvider _pp = ProviderFactory.Instance().GetDefaultProvider<IPponProvider>();

        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();


        static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        static IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ILog logger = LogManager.GetLogger("WmsFacade");
        private static ICacheProvider _cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
        private static ISellerProvider _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();

        /// <summary>
        /// 更新進倉/收退貨聯絡人資料
        /// </summary>
        /// <param name="model"></param>
        /// <param name="accountId"></param>
        /// <param name="createId"></param>
        public static bool SaveWmsContact(WmsContactModel model, string accountId, string createId, out string message)
        {
            message = string.Empty;
            if (model == null || model.AccountId != accountId)
            {
                message = string.Format("SaveWmsContact error. 要存入的聯絡人資料，與帳號不一致 {0}, {1}",
                    model.AccountId, accountId);
                return false;
            }
            if (model.IsValid(out message) == false)
            {
                return false;
            }
            WmsContact contact = _wp.WmsContactGet(accountId);
            if (contact == null)
            {
                contact = new WmsContact();
                contact.AccountId = model.AccountId;
            }
            if (RegExRules.CheckEmail(model.ReturnerEmail.Trim()) == false)
            {
                message = "退還貨聯絡人email格式不符，請重新輸入";
                return false;
            }
            contact.WarehousingName = model.WarehousingName.Trim();
            contact.WarehousingTel = model.WarehousingTel.Trim();
            contact.WarehousingMobile = model.WarehousingMobile.Trim();
            contact.WarehousingEmail = model.WarehousingEmail.Trim();
            contact.WarehousingZip = model.WarehousingZip.Trim();
            contact.WarehousingAddress = model.WarehousingAddress.Trim();
            contact.ReturnerName = model.ReturnerName.Trim();
            contact.ReturnerTel = model.ReturnerTel.Trim();
            contact.ReturnerMobile = model.ReturnerMobile.Trim();
            contact.ReturnerEmail = model.ReturnerEmail.Trim();
            contact.ReturnerZip = model.ReturnerZip.Trim();
            contact.ReturnerAddress = model.ReturnerAddress.Trim();

            _wp.WmsContactSave(contact, createId);
            return true;
        }
        /// <summary>
        /// 取得賣家進倉/收退貨聯絡人資料
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public static WmsContact GetWmsContact(string accountId)
        {
            return _wp.WmsContactGet(accountId);
        }

        /// <summary>
        /// 呼叫pchomeAPI，取得prodId後更新product_item
        /// </summary>
        /// <param name="did"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool RequestWmsAPIToAddProd(Guid did, string createId, out string message, out string pchomeProdId)
        {
            message = string.Empty;
            pchomeProdId = string.Empty;
            ViewProductItem vpi = _pp.ViewProductItemGet(did);
            if (vpi == null || vpi.IsLoaded == false)
            {
                message = "無資料";
                return false;
            }
            if (string.IsNullOrEmpty(vpi.PchomeProdId) == false)
            {
                message = "倉儲資料已建立";
                return false;
            }

            WmsProd nProd = null;
            if (vpi.IsMulti.GetValueOrDefault() == false)
            {
                message = "不支援單一規格";
                return false;
            }
            else
            {
                nProd = new WmsProd
                {
                    GroupId = vpi.InfoGuid == null ? string.Empty : vpi.InfoGuid.Value.ToString(),
                    VendorPId = vpi.ItemGuid.ToString()
                };
                if (string.IsNullOrEmpty(vpi.ProductBrandName))
                {
                    nProd.Name = string.Format("{0} {1}", vpi.ProductName, vpi.ItemName);
                }
                else
                {
                    nProd.Name = string.Format("{0}-{1} {2}", vpi.ProductBrandName, vpi.ProductName, vpi.ItemName);
                }
            }
            if (vpi.Price.GetValueOrDefault() == 0)
            {
                return false;
            }
            nProd.Cost = nProd.Price = vpi.Price.Value;
            if (vpi.ShelfLife == null)
            {
                nProd.isHaveExpiryDate = 0;
            }
            else
            {
                nProd.isHaveExpiryDate = 1;
                nProd.ShelfLife = vpi.ShelfLife.Value;
            }
            bool result = PchomeWmsAPI.AddProd(nProd);
            if (result)
            {
                ProductItem pi = _pp.ProductItemGet(did);
                if (pi != null && pi.IsLoaded)
                {
                    pi.PchomeProdId = nProd.Id;
                    pi.ModifyId = createId;
                    pi.ModifyTime = DateTime.Now;
                    _pp.ProductItemSet(pi);
                }
            }
            pchomeProdId = nProd.Id;
            return result;
        }

        public static IList<WmsModel.WmsOrder> GetReadyTransferOfWmsOrders()
        {
            var vwoList = _wp.GetSuccessfulViewWmsOrdersBySendStatus(WmsOrderSendStatus.Init).ToList();
            var viewWmsOrdersGroupByOrderId = vwoList.GroupBy(x => x.OrderId);

            List<WmsModel.WmsOrder> wmsOrders = new List<WmsModel.WmsOrder>();
            foreach (var viewWmsOrders in viewWmsOrdersGroupByOrderId)
            {
                var wmsOrder = new WmsModel.WmsOrder();
                ViewWmsOrder viewWmsOrder = viewWmsOrders.FirstOrDefault();
                wmsOrder.VendorOrderId = viewWmsOrder.OrderGuid.ToString("N");

                wmsOrder.Detail = viewWmsOrders.GroupBy(g => new { g.VendorOrderNo, g.PchomeProdId })
                    .Select(v => new WmsModel.WmsOrder.OrderDetail()
                    {
                        VendorOrderNo = v.Key.VendorOrderNo.ToString(),
                        ProdId = v.Key.PchomeProdId,
                        Qty = v.Count()
                    }).ToList();

                wmsOrder.ReceiveContact = new WmsPerson()
                {
                    Name = viewWmsOrder.MemberName,
                    Tel = viewWmsOrder.PhoneNumber,
                    Mobile = viewWmsOrder.MobileNumber,
                    Email = viewWmsOrder.MemberEmail,
                    Zip = GetZipCode(viewWmsOrder.DeliveryAddress),
                    Address = viewWmsOrder.DeliveryAddress,
                };

                wmsOrders.Add(wmsOrder);
            }

            return wmsOrders;
        }

        public static string GetZipCode(string deliveryAddress)
        {
            string zipCode = string.Empty;
            if (string.IsNullOrEmpty(deliveryAddress))
            {
                return zipCode;
            }

            foreach (char c in deliveryAddress)
            {
                if (!char.IsDigit(c))
                {
                    break;
                }

                zipCode += c;
            }

            return zipCode;
        }

        /// <summary>
        /// 該商家是否同意WMS
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static bool isConfirmWms(Guid? sellerGuid, string accountId)
        {
            if (sellerGuid != null)
            {
                ResourceAclCollection acl = _mp.ResourceAclGetListByResourceGuidList(new List<Guid>() { (Guid)sellerGuid });
                if (acl.Any())
                {
                    IEnumerable<Guid> resources = acl.Select(x => x.ResourceGuid);
                    var acls = _mp.ResourceAclGetListByResourceGuidList(resources)
                                .Where(x => ((PermissionType)x.PermissionType).HasFlag(PermissionType.Read)
                                        && ((PermissionType)x.PermissionSetting).HasFlag(PermissionType.Read))
                                .GroupBy(x => new { x.AccountId, x.ResourceGuid });

                    bool isConfirm = false;
                    foreach (var a in acls)
                    {
                        VbsMembership mem = _mp.VbsMembershipGetByAccountId(a.Key.AccountId);
                        if (mem.ViewWmsRight && !string.IsNullOrEmpty(mem.WmsContract))
                        {
                            isConfirm = true;
                            break;
                        }
                    }
                    return isConfirm;
                }
                else
                {
                    return false;
                }
            }
            else if (!string.IsNullOrEmpty(accountId))
            {
                VbsMembership mem = _mp.VbsMembershipGetByAccountId(accountId);
                return mem.ViewWmsRight && !string.IsNullOrEmpty(mem.WmsContract);
            }
            else
            {
                return false;
            }
        }

        public static IList<DataOrm.WmsOrder> GetReadySyncShipStatusOfWmsOrders()
        {
            return _wp.GetReadySyncShipStatusOfWmsOrders(DateTime.Now.AddDays(-20)).ToList(); //出貨7天後就不更新出貨狀態
        }

        public static void UpdateWmsOrderByWmsOrderInfo(Guid orderGuid, int wmsShipStatus, DateTime? wmsShipDates, bool wantToSend)
        {
            var wmsOrder = _wp.WmsOrderGet(orderGuid);
            wmsOrder.ShipStatus = wmsShipStatus;
            wmsOrder.ShipDate = wmsShipDates;
            if (wantToSend)
            {
                wmsOrder.SendNotification = true;
            }
            _wp.WmsOrderSet(wmsOrder);
        }

        private static Dictionary<string, int> GetWmsCompanyNameIdMapping()
        {
            return new Dictionary<string, int>
            {
                 {"黑貓", 2},
                 {"大榮", 5},
            };
        }

        public static void UpdateWmsOrderShipByWmsOrderShipInfo(List<CurrentWmsOrderProgress> currentWmsOrderProgresses)
        {
            foreach (var item in currentWmsOrderProgresses)
            {
                foreach (var logistic in item.Logistic)
                {

                    OrderShip orderShip = _wp.OrderShipGetByshipNo(item.OrderGuid, logistic.PickId, logistic.ShipId);

                    orderShip.ProgressStatus = item.CurrentProgressStatus;
                    orderShip.ProgressDate = item.CurrentProgressDate;
                    orderShip.ShipTime = item.ShipDate;
                    orderShip.ShipCompanyId = GetShipCompanyId(logistic);
                    orderShip.ShipNo = logistic.ShipId;
                    orderShip.IsReceipt = item.IsReceipt;
                    orderShip.ReceiptTime = item.ReceiptDate;
                    orderShip.ModifyId = "sys";
                    orderShip.ModifyTime = DateTime.Now;

                    if (orderShip.Id == 0)
                    {
                        orderShip.OrderGuid = item.OrderGuid;
                        orderShip.PickId = item.PickId;

                        orderShip.CreateId = "sys";
                        orderShip.CreateTime = DateTime.Now;

                        //跟Wms無關的資訊
                        orderShip.Type = 1;
                        orderShip.OrderClassification = 1;

                        _wp.OrderShipSet(orderShip);
                    }
                    else
                    {
                        _wp.OrderShipSave(orderShip);
                    }
                }
            }
        }

        public static List<CurrentWmsOrderProgress> GetWmsOrderProgresses(List<WmsOrderProgressStatus> orderProgressStatus, List<WmsLogistic> wmsLogistics, Guid orderGuid)
        {
            var result = new List<CurrentWmsOrderProgress>();
            foreach (var item in wmsLogistics)
            {
                //取得出貨時間
                var shipProgressStatus = orderProgressStatus.FirstOrDefault().ProgressStatus.FirstOrDefault(x => x.Message == WmsDeliveryStatus.Shipping.ToString());
                DateTime? shipDate = shipProgressStatus != null && !string.IsNullOrEmpty(shipProgressStatus.Date)
                    ? DateTime.ParseExact(shipProgressStatus.Date, "yyyy/MM/dd HH:mm:ss", null) : (DateTime?)null;              
                //取得收貨資訊
                var currentArrivedStatus = orderProgressStatus.FirstOrDefault().ProgressStatus.FirstOrDefault(x => x.Message == WmsDeliveryStatus.Arrived.ToString());
                DateTime? arrivedDate = currentArrivedStatus != null && !string.IsNullOrEmpty(currentArrivedStatus.Date)
                    ? DateTime.ParseExact(currentArrivedStatus.Date, "yyyy/MM/dd HH:mm:ss", null) : (DateTime?)null;

                WmsOrderProgressStatus.ItemProgressStatus currentProgressStatus = GetCurrentProgressStatus(orderProgressStatus.FirstOrDefault());
                //目前物流狀態type
                WmsDeliveryStatus currentProgressStatusType = (WmsDeliveryStatus)Enum.Parse(typeof(WmsDeliveryStatus), currentProgressStatus.Message);

                result.Add(new CurrentWmsOrderProgress
                {
                    OrderGuid = orderGuid,
                    WmsOrderId = orderProgressStatus.FirstOrDefault().OrderId,
                    PickId = item.PickId,
                    ShipDate = shipDate,
                    CurrentProgressStatus = (int)currentProgressStatusType,
                    CurrentProgressDate = !string.IsNullOrEmpty(currentProgressStatus.Date) ? DateTime.Parse(currentProgressStatus.Date) : (DateTime?)null,
                    Logistic= wmsLogistics,
                    IsReceipt = (currentProgressStatusType == WmsDeliveryStatus.Arrived) ? true : (bool?)null, //已送達是最終狀態
                    ReceiptDate = arrivedDate,
                });
            }

            return result;
        }

        public static WmsOrderProgressStatus.ItemProgressStatus GetCurrentProgressStatus(WmsOrderProgressStatus item)
        {
            //取最新時間的狀態當作目前狀態
            return item.ProgressStatus.Where(s => !string.IsNullOrEmpty(s.Date))
                .OrderByDescending(x => DateTime.ParseExact(x.Date, "yyyy/MM/dd HH:mm:ss", null)).FirstOrDefault() ??
                new WmsOrderProgressStatus.ItemProgressStatus { Message = WmsDeliveryStatus.Processing.ToString() };
        }

        private static int? GetShipCompanyId(WmsLogistic currentShipInfo)
        {
            var wmsCompanyNameIdMapping = GetWmsCompanyNameIdMapping();

            if (currentShipInfo == null || string.IsNullOrEmpty(currentShipInfo.LogisticName))
            {
                return null;
            }

            int result;
            if (wmsCompanyNameIdMapping.TryGetValue(currentShipInfo.LogisticName, out result))
            {
                return result;
            }
            else
            {
                logger.Warn(string.Format("沒有相對應的Pchome物流配送廠商, 名稱: {0}", currentShipInfo.LogisticName));
                return 99; //其他
            }
        }

        public static ShippingStatus GetShipStatusMapping(WmsDeliveryStatus wmsDeliveryStatus)
        {
            switch (wmsDeliveryStatus)
            {
                case WmsDeliveryStatus.Processing:
                case WmsDeliveryStatus.GotOrder:
                case WmsDeliveryStatus.Picking:
                case WmsDeliveryStatus.Arranging:
                    return ShippingStatus.ReadyToShip;
                case WmsDeliveryStatus.Shipping:
                    return ShippingStatus.Shipped;
                case WmsDeliveryStatus.Missed:
                    return ShippingStatus.Shipped;
                case WmsDeliveryStatus.Arrived:
                    return ShippingStatus.StoreReceived;
                default:
                    return ShippingStatus.ReadyToShip;

            }
        }

        public static bool ProgressStatusNotChanged(Guid orderGuid, List<WmsOrderProgressStatus> apiOrderProgresssStatus)
        {
            foreach (var item in apiOrderProgresssStatus)
            {
                OrderShip orderShip = _wp.OrderShipGet(orderGuid, item.PickId);
                WmsOrderProgressStatus.ItemProgressStatus currentProgressStatus = GetCurrentProgressStatus(item);

                if (!orderShip.ProgressStatus.HasValue)
                    return false;

                WmsDeliveryStatus currentApiProgressStatusType = (WmsDeliveryStatus)Enum.Parse(typeof(WmsDeliveryStatus), currentProgressStatus.Message);
                if ((WmsDeliveryStatus)orderShip.ProgressStatus.Value != currentApiProgressStatusType)
                {
                    return false;
                }
            }

            return true;
        }



        public static string DownloadWmsFeeFile(DateTime time, string type, string createId, out bool isHasData)
        {
            string message = string.Empty;
            isHasData = false;
            List<string> feeTypeList = new List<string>();
            int totalCount = 0;

            if (type == "all")
            {
                string[] feeTypeArray = { "purchaseservfee", "stockfee", "returnservfee", "returnshipfee", "returnpackfee", "orderservfee", "shipfee", "packfee", "refundshipfee" };
                feeTypeList.AddRange(feeTypeArray);
            }
            else
            {
                string[] feeTypeArray = { type };
                feeTypeList.AddRange(feeTypeArray);
            }
            string feeType = string.Empty;
            //string baseDir = HttpContext.Current.Server.MapPath("~/WmsFile");//抓不到
            //string baseDir = string.IsNullOrEmpty(config.WmsFeeFileDir) ? HostingEnvironment.MapPath("~/cache/WmsFeeFile/") : config.WmsFeeFileDir;
            string baseDir = string.IsNullOrEmpty(config.WebAppDataPath) ? HostingEnvironment.MapPath("~/cache/WmsFeeFile/") : Path.Combine(config.WebAppDataPath, "WmsFeeFile");

            if (!Directory.Exists(baseDir))
                Directory.CreateDirectory(baseDir);


            var timer = new Stopwatch();
            timer.Start();


            #region 呼叫API下載檔案
            if (config.EnableWms)
            {
                //API下載
                for (int i = 0; i < feeTypeList.Count; i++)
                {
                    try
                    {
                        feeType = feeTypeList[i];

                        string url = PchomeWmsAPI.GetWmsFee(feeType, time.Date);
                        if (!string.IsNullOrEmpty(url))
                        {
                            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(url);
                            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();

                            System.IO.Stream dataStream = httpResponse.GetResponseStream();
                            byte[] buffer = new byte[8192];

                            FileStream fs = new FileStream(Path.Combine(baseDir, feeType + "_" + time.ToString("yyyyMMdd") + ".csv"), FileMode.Create, FileAccess.Write);
                            int size = 0;
                            do
                            {
                                size = dataStream.Read(buffer, 0, buffer.Length);
                                if (size > 0)
                                    fs.Write(buffer, 0, size);
                            } while (size > 0);
                            fs.Close();

                            httpResponse.Close();

                        }

                        //等一下下載檔案
                        System.Threading.Thread.Sleep(1000);
                    }
                    catch (Exception ex)
                    {
                        message += feeType + "：" + ex.ToString() + "<br/>";
                    }

                }
            }
            else
            {
                //自產測試
            }
            #endregion


            var downloadFile = timer.Elapsed;


            #region 讀取檔案寫入DB
            for (int i = 0; i < feeTypeList.Count; i++)
            {
                try
                {
                    feeType = feeTypeList[i];
                    string FilePath = feeType + "_" + time.ToString("yyyyMMdd") + ".csv";
                    string AllPath = Path.Combine(baseDir, FilePath);

                    if (!File.Exists(AllPath))
                    {
                        message += AllPath + "不存在 <br/>";
                        continue;

                    }
                    FileStream fs = new FileStream(AllPath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    StreamReader sr = new StreamReader(fs, Encoding.UTF8);
                    string strLine = "";
                    string[] aryLine = null;
                    bool IsFirst = true;//是否為第一行
                    int fee = 0;
                    int qty = 0;
                    double unitFee = 0;
                    double volume = 0;
                    DateTime accountingDate = DateTime.MaxValue;
                    DateTime returnDate = DateTime.MaxValue;
                    DateTime orderDate = DateTime.MaxValue;
                    

                    if (feeType == "purchaseservfee")
                    {
                        #region 進倉作業處理費明細 purchaseservfee
                        if (_wp.GetWmsPurchaseServFeeByAccountingDate(time.Date) == 0)
                        {
                            WmsPurchaseServFeeCollection purchaseServFeeC = new WmsPurchaseServFeeCollection();
                            while ((strLine = sr.ReadLine()) != null)
                            {
                                if (IsFirst == true)
                                {
                                    IsFirst = false;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(strLine.Replace("\t", "")))
                                        break;
                                    aryLine = strLine.Split('\t');

                                    WmsPurchaseServFee purchaseServFee = new WmsPurchaseServFee();
                                    purchaseServFee.PurchaseOrderId = aryLine[0];
                                    purchaseServFee.AcceptId = aryLine[1];
                                    purchaseServFee.Fee = int.TryParse(aryLine[2], out fee) ? fee : 0;
                                    purchaseServFee.AccountingDate = DateTime.TryParse(aryLine[3], out accountingDate) ? accountingDate : accountingDate;
                                    purchaseServFee.VendorPid = aryLine[4];
                                    purchaseServFee.ProdId = aryLine[5];
                                    purchaseServFee.ProdName = aryLine[6].Replace("\"", "");
                                    purchaseServFee.ProdSpec = aryLine[7].Replace("\"", "").Replace(" ", "");
                                    purchaseServFee.UnitFee = double.TryParse(aryLine[8], out unitFee) ? unitFee : 0;
                                    purchaseServFee.Qty = int.TryParse(aryLine[9], out qty) ? qty : 0;
                                    purchaseServFee.CreateId = createId;
                                    purchaseServFee.CreateTime = DateTime.Now;
                                    purchaseServFeeC.Add(purchaseServFee);
                                }
                            }

                            message += feeType + "：" + purchaseServFeeC.Count + "筆<br/>";
                            _wp.WmsPurchaseServFeeSet(purchaseServFeeC);
                            totalCount = totalCount + purchaseServFeeC.Count;
                        }
                        else
                        {
                            message += feeType + "：" + "已匯過<br/>";
                        }
                        #endregion

                    }
                    else if (feeType == "stockfee")
                    {
                        #region 倉儲費明細 stockfee
                        if (_wp.GetWmsStockFeeByAccountingDate(time.Date) == 0)
                        {
                            if (time < Convert.ToDateTime("2019/07/20")) //前6個月優惠
                            {
                                message += feeType + "：優惠中<br/>";
                                continue;
                            }
                            WmsStockFeeCollection stockFeeC = new WmsStockFeeCollection();
                            while ((strLine = sr.ReadLine()) != null)
                            {
                                if (IsFirst == true)
                                {
                                    IsFirst = false;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(strLine.Replace("\t", "")))
                                        break;
                                    aryLine = strLine.Split('\t');

                                    WmsStockFee stockFee = new WmsStockFee();
                                    stockFee.AccountingDate = DateTime.TryParse(aryLine[0], out accountingDate) ? accountingDate : accountingDate;
                                    stockFee.VendorPid = aryLine[1];
                                    stockFee.ProdId = aryLine[2];
                                    stockFee.Fee = int.TryParse(aryLine[3], out fee) ? fee : 0;
                                    stockFee.ProdName = aryLine[4].Replace("\"", "");
                                    stockFee.ProdSpec = aryLine[5].Replace("\"", "").Replace(" ", "");
                                    stockFee.Volume = double.TryParse(aryLine[6], out volume) ? volume : 0;
                                    stockFee.UnitFee = double.TryParse(aryLine[7], out unitFee) ? unitFee : 0;
                                    stockFee.Qty = int.TryParse(aryLine[8], out qty) ? qty : 0;
                                    stockFee.CreateId = createId;
                                    stockFee.CreateTime = DateTime.Now;
                                    stockFeeC.Add(stockFee);
                                }
                            }

                            message += feeType + "：" + stockFeeC.Count + "筆<br/>";
                            _wp.WmsStockFeeSet(stockFeeC);
                            totalCount = totalCount + stockFeeC.Count;
                        }
                        else
                        {
                            message += feeType + "：" + "已匯過<br/>";
                        }
                        #endregion
                    }
                    else if (feeType == "returnservfee")
                    {
                        #region 還貨作業處理費明細 returnservfee
                        if (_wp.GetWmsReturnServFeeByAccountingDate(time.Date) == 0)
                        {
                            WmsReturnServFeeCollection returnServFeeC = new WmsReturnServFeeCollection();
                            while ((strLine = sr.ReadLine()) != null)
                            {
                                if (IsFirst == true)
                                {
                                    IsFirst = false;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(strLine.Replace("\t", "")))
                                        break;
                                    aryLine = strLine.Split('\t');

                                    WmsReturnServFee returnServFee = new WmsReturnServFee();
                                    returnServFee.ReturnDate = DateTime.TryParse(aryLine[0], out returnDate) ? returnDate : returnDate;
                                    returnServFee.ReturnId = aryLine[1];
                                    returnServFee.Fee = int.TryParse(aryLine[2], out fee) ? fee : 0;
                                    returnServFee.AccountingDate = DateTime.TryParse(aryLine[3], out accountingDate) ? accountingDate : accountingDate;
                                    returnServFee.VendorPid = aryLine[4];
                                    returnServFee.ProdId = aryLine[5];
                                    returnServFee.ProdName = aryLine[6].Replace("\"", "");
                                    returnServFee.ProdSpec = aryLine[7].Replace("\"", "").Replace(" ", "");
                                    returnServFee.UnitFee = double.TryParse(aryLine[8], out unitFee) ? unitFee : 0;
                                    returnServFee.Qty = int.TryParse(aryLine[9], out qty) ? qty : 0;
                                    returnServFee.CreateId = createId;
                                    returnServFee.CreateTime = DateTime.Now;
                                    returnServFeeC.Add(returnServFee);
                                }
                            }

                            message += feeType + "：" + returnServFeeC.Count + "筆<br/>";
                            _wp.WmsReturnServFeeSet(returnServFeeC);
                            totalCount = totalCount + returnServFeeC.Count;
                        }
                        else
                        {
                            message += feeType + "：" + "已匯過<br/>";
                        }
                        #endregion
                    }
                    else if (feeType == "returnshipfee")
                    {
                        #region returnshipfee 還貨運費明細
                        if (_wp.GetWmsReturnShipFeeByAccountingDate(time.Date) == 0)
                        {
                            WmsReturnShipFeeCollection returnShipFeeC = new WmsReturnShipFeeCollection();
                            while ((strLine = sr.ReadLine()) != null)
                            {
                                if (IsFirst == true)
                                {
                                    IsFirst = false;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(strLine.Replace("\t", "")))
                                        break;
                                    aryLine = strLine.Split('\t');

                                    WmsReturnShipFee returnShipFee = new WmsReturnShipFee();
                                    returnShipFee.ReturnDate = DateTime.TryParse(aryLine[0], out returnDate) ? returnDate : returnDate;
                                    returnShipFee.ReturnId = aryLine[1];
                                    returnShipFee.Fee = int.TryParse(aryLine[2], out fee) ? fee : 0;
                                    returnShipFee.AccountingDate = DateTime.TryParse(aryLine[3], out accountingDate) ? accountingDate : accountingDate;
                                    returnShipFee.VendorPid = aryLine[4];
                                    returnShipFee.ProdId = aryLine[5];
                                    returnShipFee.ProdName = aryLine[6].Replace("\"", "");
                                    returnShipFee.ProdSpec = aryLine[7].Replace("\"", "").Replace(" ", "");
                                    returnShipFee.Qty = int.TryParse(aryLine[8], out qty) ? qty : 0;
                                    returnShipFee.ShipId = aryLine[9];
                                    returnShipFee.CreateId = createId;
                                    returnShipFee.CreateTime = DateTime.Now;
                                    returnShipFeeC.Add(returnShipFee);
                                }
                            }

                            message += feeType + "：" + returnShipFeeC.Count + "筆<br/>";
                            _wp.WmsReturnShipFeeSet(returnShipFeeC);
                            totalCount = totalCount + returnShipFeeC.Count;
                        }
                        else
                        {
                            message += feeType + "：" + "已匯過<br/>";
                        }
                        #endregion
                    }
                    else if (feeType == "returnpackfee")
                    {
                        #region returnpackfee 還貨包材費明細(暫不收費)
                        if (_wp.GetWmsReturnPackFeeByAccountingDate(time.Date) == 0)
                        {
                            WmsReturnPackFeeCollection returnPackFeeC = new WmsReturnPackFeeCollection();
                            while ((strLine = sr.ReadLine()) != null)
                            {
                                if (IsFirst == true)
                                {
                                    IsFirst = false;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(strLine.Replace("\t", "")))
                                        break;
                                    aryLine = strLine.Split('\t');

                                    WmsReturnPackFee returnPackFee = new WmsReturnPackFee();
                                    returnPackFee.ReturnDate = DateTime.TryParse(aryLine[0], out returnDate) ? returnDate : returnDate;
                                    returnPackFee.ReturnId = aryLine[1];
                                    returnPackFee.Fee = int.TryParse(aryLine[2], out fee) ? fee : 0;
                                    returnPackFee.AccountingDate = DateTime.TryParse(aryLine[3], out accountingDate) ? accountingDate : accountingDate;
                                    returnPackFee.VendorPid = aryLine[4];
                                    returnPackFee.ProdId = aryLine[5];
                                    returnPackFee.ProdName = aryLine[6].Replace("\"", "");
                                    returnPackFee.ProdSpec = aryLine[7].Replace("\"", "").Replace(" ", "");
                                    returnPackFee.Qty = int.TryParse(aryLine[8], out qty) ? qty : 0;
                                    returnPackFee.CreateId = createId;
                                    returnPackFee.CreateTime = DateTime.Now;
                                    returnPackFeeC.Add(returnPackFee);
                                }
                            }

                            message += feeType + "：" + returnPackFeeC.Count + "筆<br/>";
                            _wp.WmsReturnPackFeeSet(returnPackFeeC);
                            totalCount = totalCount + returnPackFeeC.Count;
                        }
                        else
                        {
                            message += feeType + "：" + "已匯過<br/>";
                        }
                        #endregion
                    }
                    else if (feeType == "orderservfee")
                    {
                        #region orderservfee 出倉作業處理費明細
                        if (_wp.GetWmsOrderServFeeByAccountingDate(time.Date) == 0)
                        {
                            WmsOrderServFeeCollection orderServFeeC = new WmsOrderServFeeCollection();
                            while ((strLine = sr.ReadLine()) != null)
                            {
                                if (IsFirst == true)
                                {
                                    IsFirst = false;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(strLine.Replace("\t", "")))
                                        break;
                                    aryLine = strLine.Split('\t');

                                    Guid result = Guid.Empty;
                                    Guid.TryParse(aryLine[1].Split("-")[0], out result);
                                    WmsOrderServFee orderServFee = new WmsOrderServFee();
                                    orderServFee.OrderDate = DateTime.TryParse(aryLine[0], out orderDate) ? orderDate : orderDate;
                                    orderServFee.VendorOrderId = aryLine[1];
                                    orderServFee.OrderId = aryLine[2];
                                    orderServFee.Fee = int.TryParse(aryLine[3], out fee) ? fee : 0;
                                    orderServFee.AccountingDate = DateTime.TryParse(aryLine[4], out accountingDate) ? accountingDate : accountingDate;
                                    orderServFee.VendorPid = aryLine[5];
                                    orderServFee.ProdId = aryLine[6];
                                    orderServFee.ProdName = aryLine[7].Replace("\"", "");
                                    orderServFee.ProdSpec = aryLine[8].Replace("\" \"", "");
                                    orderServFee.UnitFee = double.TryParse(aryLine[9], out unitFee) ? unitFee : 0;
                                    orderServFee.Qty = int.TryParse(aryLine[10], out qty) ? qty : 0;
                                    orderServFee.OrderGuid = result;
                                    orderServFee.CreateId = createId;
                                    orderServFee.CreateTime = DateTime.Now;
                                    orderServFeeC.Add(orderServFee);
                                }
                            }

                            message += feeType + "：" + orderServFeeC.Count + "筆<br/>";
                            _wp.WmsOrderServFeeSet(orderServFeeC);
                            totalCount = totalCount + orderServFeeC.Count;
                        }
                        else
                        {
                            message += feeType + "：" + "已匯過<br/>";
                        }
                        #endregion
                    }
                    else if (feeType == "shipfee")
                    {
                        #region shipfee 運費明細
                        if (_wp.GetWmsShipFeeByAccountingDate(time.Date) == 0)
                        {
                            WmsShipFeeCollection shipFeeC = new WmsShipFeeCollection();
                            while ((strLine = sr.ReadLine()) != null)
                            {
                                if (IsFirst == true)
                                {
                                    IsFirst = false;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(strLine.Replace("\t", "")))
                                        break;
                                    aryLine = strLine.Split('\t');

                                    Guid result = Guid.Empty;
                                    Guid.TryParse(aryLine[1].Split("-")[0], out result);

                                    WmsShipFee shipFee = new WmsShipFee();
                                    shipFee.OrderDate = DateTime.TryParse(aryLine[0], out orderDate) ? orderDate : orderDate;
                                    shipFee.VendorOrderId = aryLine[1];
                                    shipFee.OrderId = aryLine[2];
                                    shipFee.Fee = int.TryParse(aryLine[3], out fee) ? fee : 0;
                                    shipFee.AccountingDate = DateTime.TryParse(aryLine[4], out accountingDate) ? accountingDate : accountingDate;
                                    shipFee.VendorPid = aryLine[5];
                                    shipFee.ProdId = aryLine[6];
                                    shipFee.ProdName = aryLine[7].Replace("\"", "");
                                    shipFee.ProdSpec = aryLine[8].Replace("\"", "").Replace(" ", "");
                                    shipFee.Qty = int.TryParse(aryLine[9], out qty) ? qty : 0;
                                    shipFee.ShipId = aryLine[10];
                                    shipFee.OrderGuid = result;
                                    shipFee.CreateId = createId;
                                    shipFee.CreateTime = DateTime.Now;
                                    shipFeeC.Add(shipFee);
                                }
                            }

                            message += feeType + "：" + shipFeeC.Count + "筆<br/>";
                            _wp.WmsShipFeeSet(shipFeeC);
                            totalCount = totalCount + shipFeeC.Count;
                        }
                        else
                        {
                            message += feeType + "：" + "已匯過<br/>";
                        }
                        #endregion
                    }
                    else if (feeType == "packfee")
                    {
                        #region packfee 包材費明細
                        if (_wp.GetWmsPackFeeByAccountingDate(time.Date) == 0)
                        {
                            WmsPackFeeCollection packFeeC = new WmsPackFeeCollection();
                            while ((strLine = sr.ReadLine()) != null)
                            {
                                if (IsFirst == true)
                                {
                                    IsFirst = false;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(strLine.Replace("\t", "")))
                                        break;
                                    aryLine = strLine.Split('\t');

                                    Guid result = Guid.Empty;
                                    Guid.TryParse(aryLine[1].Split("-")[0], out result);
                                    WmsPackFee packFee = new WmsPackFee();
                                    packFee.OrderDate = DateTime.TryParse(aryLine[0], out orderDate) ? orderDate : orderDate;
                                    packFee.VendorOrderId = aryLine[1];
                                    packFee.OrderId = aryLine[2];
                                    packFee.Fee = int.TryParse(aryLine[3], out fee) ? fee : 0;
                                    packFee.AccountingDate = DateTime.TryParse(aryLine[4], out accountingDate) ? accountingDate : accountingDate;
                                    packFee.VendorPid = aryLine[5];
                                    packFee.ProdId = aryLine[6];
                                    packFee.ProdName = aryLine[7].Replace("\"", "");
                                    packFee.ProdSpec = aryLine[8].Replace("\"", "").Replace(" ", "");
                                    packFee.Qty = int.TryParse(aryLine[9], out qty) ? qty : 0;
                                    packFee.OrderGuid = result;
                                    packFee.CreateId = createId;
                                    packFee.CreateTime = DateTime.Now;
                                    packFeeC.Add(packFee);
                                }
                            }

                            message += feeType + "：" + packFeeC.Count + "筆<br/>";
                            _wp.WmsPackFeeSet(packFeeC);
                            totalCount = totalCount + packFeeC.Count;
                        }
                        else
                        {
                            message += feeType + "：" + "已匯過<br/>";
                        }
                        #endregion
                    }
                    else if (feeType == "refundshipfee")
                    {
                        #region refundshipfee 退貨運費明細
                        if (_wp.GetWmsRefundShipFeeByAccountingDate(time.Date) == 0)
                        {
                            WmsRefundShipFeeCollection refundShipFeeC = new WmsRefundShipFeeCollection();
                            while ((strLine = sr.ReadLine()) != null)
                            {
                                if (IsFirst == true)
                                {
                                    IsFirst = false;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(strLine.Replace("\t", "")))
                                        break;
                                    aryLine = strLine.Split('\t');

                                    Guid result = Guid.Empty;
                                    Guid.TryParse(aryLine[0].Split("-")[0], out result);
                                    WmsRefundShipFee refundShipFee = new WmsRefundShipFee();
                                    refundShipFee.VendorOrderId = aryLine[0];
                                    refundShipFee.OrderId = aryLine[1];
                                    refundShipFee.Fee = int.TryParse(aryLine[2], out fee) ? fee : 0;
                                    refundShipFee.AccountingDate = DateTime.TryParse(aryLine[3], out accountingDate) ? accountingDate : accountingDate;
                                    refundShipFee.RefundShipId = aryLine[4];
                                    refundShipFee.OrderGuid = result;
                                    refundShipFee.CreateId = createId;
                                    refundShipFee.CreateTime = DateTime.Now;
                                    refundShipFeeC.Add(refundShipFee);
                                }
                            }

                            message += feeType + "：" + refundShipFeeC.Count + "筆<br/>";
                            _wp.WmsRefundShipFeeSet(refundShipFeeC);
                            totalCount = totalCount + refundShipFeeC.Count;
                        }
                        else
                        {
                            message += feeType + "：" + "已匯過<br/>";
                        }
                        #endregion
                    }
                    sr.Close();
                    fs.Close();
                }
                catch (Exception ex)
                {
                    message += feeType + " Error：" + ex.ToString() + "<br/>";
                }

            }
            #endregion

            var writeDB = timer.Elapsed.Subtract(downloadFile);
            timer.Stop();


            if (totalCount > 0)
            {
                isHasData = true;
            }
            message += "總花費時間" + timer.Elapsed + "下載檔案" + downloadFile + "寫入DB" + writeDB + "<br/>";
            return message;
        }


        public static void SaveWmsPurchaseOrderLog(List<Guid> wmsPurchaseOrderGuids, int status, string userName)
        {
            DataOrm.WmsPurchaseOrderLogCollection orderLogs = new WmsPurchaseOrderLogCollection();
            foreach (var guid in wmsPurchaseOrderGuids)
            {
                DataOrm.WmsPurchaseOrderLog newOrderLog = new DataOrm.WmsPurchaseOrderLog();
                newOrderLog.WmsPurchaseOrderGuid = guid;
                newOrderLog.Status = status;
                newOrderLog.CreateId = userName;
                newOrderLog.CreateTime = DateTime.Now;
                orderLogs.Add(newOrderLog);
            }

            _wp.WmsPurchaseOrderLogSet(orderLogs);
        }

        public static void SaveWmsReturnOrderLog(List<Guid> wmsReturnOrderGuids, int status, string userName)
        {
            DataOrm.WmsReturnOrderLogCollection orderLogs = new WmsReturnOrderLogCollection();
            foreach (var guid in wmsReturnOrderGuids)
            {
                DataOrm.WmsReturnOrderLog newOrderLog = new DataOrm.WmsReturnOrderLog();
                newOrderLog.WmsReturnOrderGuid = guid;
                newOrderLog.Status = status;
                newOrderLog.CreateId = userName;
                newOrderLog.CreateTime = DateTime.Now;
                orderLogs.Add(newOrderLog);
            }

            _wp.WmsReturnOrderLogSet(orderLogs);
        }

        public static List<WmsOrderHistoryModel> GetWmsOrderHistory(Guid orderGuid,string shipNo)
        {
            string cacheKey = string.Format("WmsOrderHistory.{0}.{1}", orderGuid,shipNo);
            var result = _cache.Get<List<WmsOrderHistoryModel>>(cacheKey);
            if (result != null)
            {
                return result;
            }

            result = new List<WmsOrderHistoryModel>();
            var wo = _wp.WmsOrderGet(orderGuid);
            if (wo.IsLoaded && !string.IsNullOrEmpty(wo.WmsOrderId))
            {
                List<WmsOrderProgressStatus> orderProgressStatus = PchomeWmsAPI.GetOrderProgressStatus(wo.WmsOrderId);
                WmsLogisticTrace logisticTrace = PchomeWmsAPI.GetLogisticTrace(wo.WmsOrderId);

                //取第一筆出貨單作為訂單狀態
                var firstOrderProgressStatus = orderProgressStatus.FirstOrDefault();

                foreach (var item in firstOrderProgressStatus.ProgressStatus.Where(s => !string.IsNullOrEmpty(s.Date)))
                {
                    result.Add(new WmsOrderHistoryModel
                    {
                        DelvieryDesc = Helper.GetEnumDescription((WmsDeliveryStatus)Enum.Parse(typeof(WmsDeliveryStatus), item.Message)),
                        DeliveryTime = DateTime.ParseExact(item.Date, "yyyy/MM/dd HH:mm:ss", null),
                    });
                }

                //這邊要依據ShipNo取得自身的運送資料
                var firstLogisticTraceInfo = logisticTrace.Info.Where(p => p.ShipId == shipNo).FirstOrDefault();

                if (firstLogisticTraceInfo != null)
                {
                    foreach (var item in firstLogisticTraceInfo.ProgressStatus)
                    {
                        result.Add(new WmsOrderHistoryModel
                        {
                            DelvieryDesc = item.Collection + " " + item.Message,
                            DeliveryTime = DateTime.ParseExact(item.Date, "yyyy/MM/dd HH:mm:ss", null),
                        });
                    }
                }
            }

            result = result.OrderBy(x => x.DeliveryTime).ToList();
            _cache.Set(cacheKey, result, TimeSpan.FromMinutes(30));

            return result;
        }

        public static void SendShipmentNotification(Guid orderGuid, CurrentWmsOrderProgress wmsOrderProgresses)
        {
            Order od = _op.OrderGet(orderGuid);

            foreach (var item in wmsOrderProgresses.Logistic)
            {
                string message = string.Format("您的訂單{0}已出貨（物流資訊：{1}{2}）", od.OrderId
                    , _op.ShipCompanyGet(GetShipCompanyId(item)?? 0).ShipCompanyName, item.ShipId);

                MemberFacade.Send24HOrderShipmentNotification(od, message);
            }
        }

        public static bool NeedNotifiedByProgressStatus(WmsOrderProgressStatus wmsOrderProgressStatus)
        {
            var currentProgressStatus = (WmsDeliveryStatus)Enum.Parse(typeof(WmsDeliveryStatus), WmsFacade.GetCurrentProgressStatus(wmsOrderProgressStatus).Message);
            bool isShippingOrMissed = (currentProgressStatus == WmsDeliveryStatus.Shipping || currentProgressStatus == WmsDeliveryStatus.Missed);

            if (isShippingOrMissed)
            {
                return true;
            }

            return false;
        }

        public static WmsRefundOrderStatusLog MakeWmsRefundOrderStatusLog(WmsRefundOrder refund, string modifyId, DateTime modifyTime)
        {
            if (refund == null)
            {
                return null;
            }

            var result = new WmsRefundOrderStatusLog
            {
                WmsRefundOrderId = refund.Id,
                HasPickupTime = refund.HasPickupTime,
                ArrivalTime = refund.ArrivalTime,
                CsConfirmedTime = refund.CsConfirmedTime,
                Status = refund.Status,
                ModifyId = modifyId,
                ModifyTime = modifyTime
            };

            return result;
        }
    }
}

﻿using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.ISP;
using Newtonsoft.Json;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;
using log4net;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.BizLogic.Facade
{
    public class ISPFacade
    {
        private static ISysConfProvider cp;
        private static ISellerProvider sp;
        private static IOrderProvider op;
        private static Dictionary<IspChannelServiceType, string> _uid;
        private static IMemberProvider mp;
        private static readonly ILog logger;

        static ISPFacade()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            cp = ProviderFactory.Instance().GetConfig();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _uid = new Dictionary<IspChannelServiceType, string>();
            logger = LogManager.GetLogger(typeof(ISPFacade));

        }

        public static bool IsExistServiceChannelStoreData(string storeId, ServiceChannel serviceChannel)
        {
            //詢問MemberCheckoutInfo 使用者紀錄使用過的店舖資料
            var memberCheckInfo = mp.MemberCheckoutInfoGetByStoreId(storeId, serviceChannel);
            return memberCheckInfo.IsLoaded;
        }

        /// <summary>
        /// 跟applicationServer詢問店鋪資料 
        /// </summary>
        /// <param name="storeId">超商編號</param>
        /// <param name="serviceChannel">店取通路商</param>
        /// <returns></returns>
        public static string IspGetServiceChannelStoreData(string storeId , ServiceChannel serviceChannel)
        {
            var apiName = string.Empty;
            switch (serviceChannel)
            {
                case ServiceChannel.FamilyMart:
                    apiName = string.IsNullOrEmpty(storeId) ? "GetFamiStore" : "GetFamiStoreByStoreId";
                    break;
                case ServiceChannel.SevenEleven:
                    apiName = string.IsNullOrEmpty(storeId) ? "GetSevenStore" : "GetSevenStoreByStoreId";
                    break;
            }
            
            var result = ISPApiRequest("", cp.ISPApiUrl + apiName);
            if (result.Code == ApiResultCode.Success)
            {
                return result.Data.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// 同步訂單facade
        /// </summary>
        /// <param name="ordeGuids"></param>
        public static void IspMakeOrder(List<Guid> ordeGuids)
        {
            logger.Info("IspMakeOrder.parameter:" + JsonConvert.SerializeObject(ordeGuids));
            List<ViewIspMakeOrderInfo> orderList = new List<ViewIspMakeOrderInfo>();

            if (ordeGuids == null)
            {
                //撈出ViewIspMakeOrderInfo 預設與失敗要retry的 (成功不再撈)
                orderList = op.GetViewIspMakeOrderInfoCollection().ToList();
            }
            else
            {
                orderList = op.GetViewIspMakeOrderInfo(ordeGuids).ToList();
            }
            
            List<ISPOrderData> sentToMakeOrderApiData = new List<ISPOrderData>();
            foreach (var i in orderList)
            {
                ISPOrderData newOrder = new ISPOrderData()
                {
                    ChannelStoreCode = string.IsNullOrEmpty(i.ChannelStoreCode) ? "not found" : ((i.ChannelStoreCode.Length > 6) ? i.ChannelStoreCode.Substring(1, 6) : i.ChannelStoreCode),//通路代碼(ex: fami_store_id)
                    IsCashOnPickup = (i.IsCashOnPickup == 1),
                    OrderKey = i.OrderKey,
                    PickupAmount = i.PickupAmount.HasValue ? i.PickupAmount.Value : 0,
                    ProductAmount = Convert.ToInt32(i.ProductAmount),
                    ReceiverMPhone = i.ReceiverMPhone,
                    ReceiverName = i.ReceiverName,
                    SenderName = "17Life",
                    ServiceChannel = i.ServiceChannel.ToString(),
                    StoreKey = i.StoreKey.ToString(),
                    OrderType = (int)IspOrderType.B2C,
                    ReceiverEmail = i.ReceiverEmail
                };
                sentToMakeOrderApiData.Add(newOrder);
            }
            
            try
            {
                // call make_order api
                var json = JsonConvert.SerializeObject(sentToMakeOrderApiData);
                logger.Info("IspMakeOrder.parameter:" + json);
                var result = ISPApiRequest(json, cp.ISPApiUrl + "MakeOrder");
                if (result.Code == ApiResultCode.Success)
                {
                    var resultData = JsonConvert.DeserializeObject<ISPMakeOrderReturnModel>(result.Data.ToString());
                    //回寫狀態order_product_delivery.is_application_server_connected
                    if (resultData.failData.Any())
                    {
                        logger.Info("IspMakeOrder.failData result:" + JsonConvert.SerializeObject(result));
                    }

                    UpdateOrderProductDeliverIsApplicationServerConnectedStatus(resultData.successData, orderList, IspIsApplicationServerConnectedStatus.SuccessConnected);
                    UpdateOrderProductDeliverIsApplicationServerConnectedStatus(resultData.failData, orderList, IspIsApplicationServerConnectedStatus.FailConnected);
                }
                else
                {
                    logger.Info("IspMakeOrder.error result:" +JsonConvert.SerializeObject(result));
                    UpdateOrderProductDeliverIsApplicationServerConnectedStatus(orderList.Select(p=>p.OrderGuid).ToList(), IspIsApplicationServerConnectedStatus.FailConnected);
                }
            }
            catch (Exception e)
            {
                logger.Info("IspMakeOrder.exception result:" + e.Message);
                UpdateOrderProductDeliverIsApplicationServerConnectedStatus(orderList.Select(p => p.OrderGuid).ToList(), IspIsApplicationServerConnectedStatus.FailConnected);
            }
        }

        private static void UpdateOrderProductDeliverIsApplicationServerConnectedStatus(List<ISPMakeOrderReturnItem> apiData, List<ViewIspMakeOrderInfo> orderList, IspIsApplicationServerConnectedStatus status)
        {
            List<Guid> orderGuidList = new List<Guid>();
            foreach (var item in apiData)
            {
                orderGuidList.Add(orderList.Where(p=>p.OrderKey == item.orderKey).Select(p=>p.OrderGuid).LastOrDefault());
            }
            op.OrderProductDeliveryUpdateIsApplicationServerConnectedStatus(status, orderGuidList);
        }

        private static void UpdateOrderProductDeliverIsApplicationServerConnectedStatus(List<Guid> orderGuidList, IspIsApplicationServerConnectedStatus status)
        {
            op.OrderProductDeliveryUpdateIsApplicationServerConnectedStatus(status, orderGuidList);
        }

        public static void ISPApply(Guid sellerGuid, ServiceChannel channel, int returnCycle, int returnType, string returnOther, string userName,string sellerAddress, string contactName, string contactTel,string contactMobile, string email, out string message, bool isUpdate)
        {
            var isp = sp.VbsInstorePickupGet(sellerGuid, (int)channel);
            isp.ReturnCycle = returnCycle;
            isp.ReturnType = returnType;
            isp.ReturnOther = returnOther;
            isp.SellerAddress = sellerAddress;
            isp.Contacts = new JsonSerializer().Serialize(new { ContactPersonName = contactName, SellerTel = contactTel, SellerMobile = contactMobile, ContactPersonEmail = email });
            isp.ModifyTime = DateTime.Now;
            isp.ModifyId = userName;
            sp.VbsInstorePickupSet(isp);

            var vvip = sp.ViewVbsInstorePickupCollectionGet(sellerGuid).FirstOrDefault(x => x.ServiceChannel == (int)channel);


            string contactEmail = string.Empty;
            string firstContact = string.Empty;
            string vendorTell = string.Empty;
            string returnAddress = string.Empty;

            if (cp.EnableSevenIsp)
            {

                if (!string.IsNullOrEmpty(contactTel))
                {
                    try
                    {
                        var firstTell = contactTel.Split(',')[0].Split(';')[0].Trim();
                        var firstTellNum = decimal.Parse(Regex.Replace(firstTell, "[^0-9]+", string.Empty));
                        var extension = firstTell.IndexOf('#') > 0 ? firstTell.Split('#')[1] : "";

                        string _format = string.Format("0#-#######{0}-{1}", "".PadLeft(firstTellNum.ToString().Length - (!string.IsNullOrEmpty(extension) ? extension.Length : 0) - 8, '#'), "".PadRight(extension.Length, '#'));
                        contactTel = string.Format("{0:" + _format + "}", firstTellNum);
                    }
                    catch(Exception ex)
                    {
                        message = "電話格式不正確";
                        return;
                    }
                    
                }


                contactEmail = email;
                firstContact = contactName;
                vendorTell = contactTel;
                returnAddress = sellerAddress;
            }
            else
            {
                var contacts = new Seller.MultiContracts();
                contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(vvip.Contacts).Where(x => x.Type == ((int)ProposalContact.ReturnPerson).ToString()).FirstOrDefault();

                if (!string.IsNullOrEmpty(contacts.SellerTel))
                {
                    var firstTell = contacts.SellerTel.Split(',')[0].Split(';')[0].Trim();
                    var firstTellNum = decimal.Parse(Regex.Replace(firstTell, "[^0-9]+", string.Empty));
                    var extension = firstTell.IndexOf('#') > 0 ? firstTell.Split('#')[1] : "";

                    string _format = string.Format("0#-#######{0}-{1}", "".PadLeft(firstTellNum.ToString().Length - (!string.IsNullOrEmpty(extension) ? extension.Length : 0) - 8, '#'), "".PadRight(extension.Length, '#'));
                    contacts.SellerTel = string.Format("{0:" + _format + "}", firstTellNum);
                }


                contactEmail = contacts.ContactPersonEmail.Split(',')[0].Split(';')[0];
                firstContact = contacts.ContactPersonName.Split(',')[0].Split(';')[0];
                vendorTell = contacts.SellerTel;
                returnAddress = vvip.SellerAddress;
            }


            var data = new
            {
                serviceChannelCode = vvip.ServiceChannel,
                storeKey = vvip.SellerGuid,
                vendorName = vvip.SellerName.Split(',')[0].Split(';')[0],
                contactEmail = contactEmail,
                firstContact = firstContact,
                vendorTell = vendorTell,
                returnCycle = vvip.ReturnCycle != (int)RrturnCycle.Daily ? 0 : 1,
                returnDay = vvip.ReturnCycle,
                returnMode = vvip.ReturnType,
                returnModeOther = vvip.ReturnOther,
                returnAddress = returnAddress,
            };

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            string url = cp.ISPApiUrl + (isUpdate ? "UpdateStore" : "ApplyStore");
            var result = ISPApiRequest(json, url);
            message = "";

            if (result.Code == ApiResultCode.Success)
            {
                isp.Status = !isUpdate ? (int)ISPStatus.StoreCreate : isp.Status;
                isp.VerifyTime = DateTime.Now;
                if (!isUpdate && result.Data != null)
                {
                    isp.StoreSubCode = result.Data.ToString();//審核過後就先帶回子代碼
                }
            }
            else
            {
                isp.VerifyMemo = result.Message;
            }
            sp.VbsInstorePickupSet(isp);

            message = result.Message;
        }

        public static string GetApiTrackingNumber(List<Core.ModelPartial.ShipmentOrderModel> orderList)
        {
            logger.Info("GetApiTrackingNumber.parameter=" + Newtonsoft.Json.JsonConvert.SerializeObject(orderList));
            List<object> list = new List<object>();
            if (cp.EnableSevenIsp)
            {
                foreach (var order in orderList)
                {
                    list.Add(new
                    {
                        orderType = (int)order.OrderType,
                        channelType = order.channelType,
                        isReTag = order.isReTag,
                        storeKey = order.StoreKey,
                        orderList = order.OrderList,
                    });
                }
            }
            else
            {
                foreach (var order in orderList)
                {
                    list.Add(new
                    {
                        orderType = (int)order.OrderType,
                        storeKey = order.StoreKey,
                        orderList = order.OrderList
                    });
                }
            }

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(list);
            logger.Info("GetApiTrackingNumber.json=" + json);

            string url = cp.ISPApiUrl + "GetShipmentNumber";
            var result = ISPApiRequest(json, url);


            logger.Info("GetApiTrackingNumber.result=" + Newtonsoft.Json.JsonConvert.SerializeObject(result));
            if (result.Code == ApiResultCode.Success)
            {
                return result.Data.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetApiShipmentPDF(List<Core.ModelPartial.ShipmentOrderModel> orderList)
        {
            logger.Info("GetApiShipmentPDF.parameter=" + Newtonsoft.Json.JsonConvert.SerializeObject(orderList));
            List<object> list = new List<object>();

            if (cp.EnableSevenIsp)
            {
                foreach (var order in orderList)
                {
                    list.Add(new
                    {
                        orderType = (int)order.OrderType,
                        channelType = order.channelType,
                        isReTag = order.isReTag,
                        storeKey = order.StoreKey,
                        orderList = order.OrderList,
                    });
                }
            }
            else
            {
                foreach (var order in orderList)
                {
                    list.Add(new
                    {
                        orderType = (int)order.OrderType,
                        storeKey = order.StoreKey,
                        orderList = order.OrderList
                    });
                }
            }

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(list);
            logger.Info("GetApiShipmentPDF.json=" + json);

            string url = cp.ISPApiUrl + "GetShipmentPDF";
            var result = ISPApiRequest(json, url);

            logger.Info("GetApiShipmentPDF.result=" + Newtonsoft.Json.JsonConvert.SerializeObject(result));
            if (result.Code == ApiResultCode.Success)
            {
                return result.Data.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public static string ShippingDelivery(List<Core.ModelPartial.ShipmentOrderModel> orderList)
        {
            logger.Info("ShippingDelivery.parameter=" + Newtonsoft.Json.JsonConvert.SerializeObject(orderList));
            List<object> list = new List<object>();
            foreach (var order in orderList)
            {
                list.Add(new
                {
                    orderType = (int)order.OrderType,
                    storeKey = order.StoreKey,
                    orderList = order.OrderList
                });
            }

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(list);
            logger.Info("ShippingDelivery.json=" + json);

            string url = cp.ISPApiUrl + "ShippingDelivery";
            var result = ISPApiRequest(json, url);

            logger.Info("ShippingDelivery.result=" + Newtonsoft.Json.JsonConvert.SerializeObject(result));
            if (result.Code == ApiResultCode.Success)
            {
                return result.Data.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public static string UpdateStoreApplicationStatus(Guid storeKey, int channel, int applicationStatus)
        {

            var data = new
            {
                storeKey = storeKey,
                serviceChannel = channel,
                ApplicationStatus = applicationStatus
            };

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            logger.Info("UpdateStoreApplicationStatus.json=" + json);

            string url = cp.ISPApiUrl + "UpdateStoreApplicationStatus";
            var result = ISPApiRequest(json, url);


            logger.Info("UpdateStoreApplicationStatus.result=" + Newtonsoft.Json.JsonConvert.SerializeObject(result));
            if (result.Code == ApiResultCode.Success)
            {
                return result.Code.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public static void UpdateStoreStatus(VbsInstorePickup vip)
        {
            var query = new
            {
                storeKey = vip.SellerGuid.ToString()
            };
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(query);
            string url = cp.ISPApiUrl + "QueryStoreStatus";
            var result = ISPApiRequest(json, url);
            DateTime now = DateTime.Now;
            if (result.Code == ApiResultCode.Success)
            {
                var data = Newtonsoft.Json.JsonConvert.DeserializeObject<ISPServiceData>(result.Data.ToString());
                var serviceStoreData = data.service.FirstOrDefault(x => x.serviceChannelCode == vip.ServiceChannel);//api回傳兩筆,只抓要更新的
                Guid sellerGuid = Guid.Parse(data.storeKey);

                if (serviceStoreData != null)
                {
                    switch (serviceStoreData.applicationStatusCode)
                    {
                        case (int)ApplicationStatusCode.Verifying:
                            if (vip.Status == (int)ISPStatus.StoreCreate)
                            {
                                vip.Status = (int)ISPStatus.Checking;
                                vip.StoreCreateTime = now;
                                vip.StoreSubCode = serviceStoreData.serviceChannelProperty != null ? serviceStoreData.serviceChannelProperty.eshopId : null;
                                sp.VbsInstorePickupSet(vip);

                                var verifyingMailTo = GetSellerContractsEmailList(sellerGuid);
                                if (verifyingMailTo.Any())
                                {
                                    SendApplicationVerifyingEmail(verifyingMailTo);
                                }
                            }
                            break;
                        case (int)ApplicationStatusCode.Pass:
                            vip.Status = (int)ISPStatus.Complete;
                            vip.CheckStatus = (int)CheckLableCode.Pass;
                            vip.FinishTime = now;
                            vip.IsEnabled = true;

                            if (vip.ServiceChannel == (int)ServiceChannel.SevenEleven && vip.StoreCreateTime == null)
                            {
                                vip.StoreCreateTime = now;
                                vip.StoreSubCode = serviceStoreData.serviceChannelProperty != null ? serviceStoreData.serviceChannelProperty.eshopId : null;
                            }
                            sp.VbsInstorePickupSet(vip);
                            var passMailTo = GetSellerContractsEmailList(sellerGuid);
                            if (passMailTo.Any())
                            {
                                SendApplicationPassEmail(passMailTo);
                            }
                            break;
                        case (int)ApplicationStatusCode.Fail:
                            if (vip.Status != (int)ISPStatus.Failed)
                            {
                                vip.Status = (int)ISPStatus.Failed;
                                vip.CheckStatus = (int)CheckLableCode.Fail;
                                sp.VbsInstorePickupSet(vip);
                            }
                            break;
                        case (int)ApplicationStatusCode.Reject:
                            vip.Status = (int)ISPStatus.Reject;
                            vip.VerifyMemo = "資料不完整";
                            sp.VbsInstorePickupSet(vip);
                            break;
                    }
                }
            }
        }

        public static void SendApplicationPassEmail(List<string> mailTo)
        {
            var docUrl = cp.SSLSiteUrl + "/vbs/VbsDocumentDownloadSys?t=1&d=2";
            var systemUrl = cp.SSLSiteUrl + "/vbs/Login";

            EmailFacade.SendNowCommonNotification("【17Life通知】超商取貨_功能開通完成"
                , "親愛的供應商夥伴 您好"
                , string.Format(@"恭喜您，超商取貨資料+測試標籤皆審核通過，已為您開通超商取貨設定欄位，您需於提報新提案單時輸入『超商取貨限制』；
                    亦可將系統內既有提案單啟動編輯補輸入『超商取貨限制』，提案單17Life審核通過後，商品會即時開通超商取貨服務，教學步驟如下，
                    若操作上有任何不清楚的地方，歡迎來信或來電詢問，謝謝。<br><br>
                    【新提案_超商取貨限制設定】<a href='{0}' trage='_blank'>教學文件下載</a>
                    <br><br>
                    Step1. 登入<a href='{1}' trage='_blank'>商家系統</a> ，點選『提案功能』。<br>
                    Step2. 新增新提案，page2銷售方案，每個方案皆可依開放消費者使用超取需求，選填輸入『超商取貨限制』數量。<br>
                    Step3. 請留意超商取貨的包裝規範。<br>
                    Steo4. 請留意超商取貨的訂單相關資訊(出貨/回報/刷退&驗退)。", docUrl, systemUrl)
                , mailTo);
        }

        public static void SendApplicationVerifyingEmail(List<string> mailTo)
        {
            var docUrl = cp.SSLSiteUrl + "/vbs/VbsDocumentDownloadSys?t=1&d=2";
            var systemUrl = cp.SSLSiteUrl + "/vbs/Login";

            EmailFacade.SendNowCommonNotification("【17Life通知】超商取貨_標籤測試"
                , "親愛的供應商夥伴 您好"
                , string.Format(@"很高興通知您， 貴司申請超商取貨資料 第一關審核通過，<font color='blue'>請儘速進行下一階段【測試標籤驗證作業】</font>。
                    此段作業為驗證包裹外觀黏貼的配送標籤，超商可順利掃瞄辨識無誤，教學步驟如下，
                    若操作上有任何不清楚的地方，歡迎來信或來電詢問，謝謝。<br><br>
                    【測試標籤驗證作業】<a href='{0}' trage='_blank'/>教學文件下載</a><br>
                    Step1.登入<a href='{1}' trage='_blank'/>商家系統</a>，點選『超商申請作業』。<br>
                    Step2.點選測試標籤<font color='red'>『下載』</font>，使用<font color='red'>『雷射印表機』</font>列印紙本。<br>
                    Step3.請將紙本測試標籤<font color='red'>『擇一』</font>下列方式進行驗證：<br><br>
                    • 掃描：Email寄回電子檔。信件格式如下<br>
                    主旨：[測試標籤]＋公司名稱 (ex: [測試標籤] 康太數位整合股份有限公司)<br>
                    收件人：apply@17Life.com<br>
                    附檔：掃描(測試標籤)電子檔(PDF)<br><br>
 
                    • 紙本：掛號郵寄。<br>
                    收件地址:台北市中山區中山北路一段11號13樓<br>
                    收件人:康太數位整合股份有限公司(宅配)收。", docUrl, systemUrl)
                , mailTo);
        }

        public static List<string> GetSellerContractsEmailList(Guid sellerGuid)
        {
            var seller = SellerFacade.SellerGet(sellerGuid);
            if (!seller.IsLoaded)
            {
                return new List<string>();
            }

            List<string> contractsType = new List<string> { ((int)ProposalContact.Normal).ToString(), ((int)ProposalContact.ReturnPerson).ToString() };

            var contracts = new JsonSerializer()
                .Deserialize<List<Seller.MultiContracts>>(seller.Contacts)
                .Where(x => contractsType.Contains(x.Type));

            List<string> mailTo = new List<string>();

            foreach (var email in contracts.Select(x => x.ContactPersonEmail))
            {
                if (email.Split(';').Length > 1)
                {
                    mailTo.AddRange(email.Split(';'));
                }
                else if (email.Split(',').Length > 1)
                {
                    mailTo.AddRange(email.Split(','));
                }
                else
                {
                    mailTo.Add(email);
                }
            }

            return mailTo;
        }

        public static List<OrderHistoryModel> GetIspOrderHistory(Guid orderGuid)
        {
            var opd = op.OrderProductDeliveryGetByOrderGuid(orderGuid);
            if (opd.IsLoaded && !string.IsNullOrEmpty(opd.GoodStatusHistory))
            {
                return new JsonSerializer().Deserialize<List<OrderHistoryModel>>(opd.GoodStatusHistory);
            }

            return new List<OrderHistoryModel>();
        }

        public static void RefreshOrder()
        {
            Dictionary<string, KeyValuePair<int, int>> orderIds = op.ViewOrderProductDeliveryGetUndoneList()
                .ToDictionary(x => x.OrderId, x=> new KeyValuePair<int, int>(x.Id, x.OrderShipId ?? 0));
            
            int idx = 0;
            while (idx <= orderIds.Count - 1)
            {
                var exeOrderIds = orderIds.Skip(idx).Take(200).Select(x=>x.Key);
                var result = ISPApiRequest(new JsonSerializer().Serialize(new { orderList = exeOrderIds }), cp.ISPApiUrl + "QueryOrder");
                if (result.Code == ApiResultCode.Success && result.Data != null)
                {
                    var js = new JsonSerializer();
                    List<QueryOrderResultItem> ispOrders = js.Deserialize<List<QueryOrderResultItem>>(js.Serialize(result.Data));
                    foreach (var o in ispOrders)
                    {
                        var opd = op.OrderProductDeliveryGet(orderIds[o.OrderKey].Key);
                        var os = orderIds[o.OrderKey].Value == 0 ? new OrderShip() : op.GetOrderShipById(orderIds[o.OrderKey].Value);

                        var goodsStatusData = o.OrderHistory.Where(x => x.HistoryType == (byte)IspHistoryType.DeliveryStatus)
                            .OrderByDescending(t => t.SerialNo).First();
                        var sellerGoodsStatusData = o.OrderHistory.OrderByDescending(t => t.SerialNo).First();
                        var order = op.OrderGet(opd.OrderGuid);

                        opd.DcAcceptStatus = o.DcAcceptStatus;
                        opd.GoodsStatus = goodsStatusData.DelvieryStatus;
                        opd.GoodsStatusMessage = Helper.GetEnumDescription((GoodsStatus) opd.GoodsStatus);
                        opd.SellerGoodsStatus = sellerGoodsStatusData.DelvieryStatus;
                        opd.SellerGoodsStatusMessage = Helper.GetEnumDescription((GoodsStatus)opd.SellerGoodsStatus);
                        opd.GoodStatusHistory = new JsonSerializer().Serialize(o.OrderHistory);
                        opd.ModifyTime = DateTime.Now;
                        
                        foreach (var h in o.OrderHistory)
                        {
                            switch ((GoodsStatus) h.DelvieryStatus)
                            {
                                case GoodsStatus.StoreReceived:
                                    if (os.IsLoaded && !os.SentArrivalNoticeSms)
                                    {
                                        string title = string.Empty;
                                        string msg = string.Empty;
                                        switch (opd.ProductDeliveryType)
                                        {
                                            case (int)ProductDeliveryType.SevenPickup:
                                                //17Life商品到店通知：物流編號(物流編號ex:1213131231)已送達(7-ELEVEN門市名稱)，請速攜帶身份證件前往領取。(門市進貨日＋7天ex:20171007)內未取貨，將自動辦理退貨。
                                                title = "7-11到貨通知";
                                                msg = string.Format("17Life商品到店通知：物流編號{0}已送達7-11{1}，請速攜帶身份證件前往領取。{2:yyyy/MM/dd}內未取貨，將自動辦理退貨。"
                                                    , os.ShipNo, opd.SevenStoreName, h.DeliveryTime.AddDays(7));
                                                break;
                                            case (int)ProductDeliveryType.FamilyPickup:
                                                title = "全家到貨通知";
                                                msg = string.Format("17Life商品到店通知：物流編號{0}已送達全家{1}，請速攜帶身份證件前往領取。{2:yyyy/MM/dd}內未取貨，將自動辦理退貨。"
                                                    , os.ShipNo, opd.FamilyStoreName, h.DeliveryTime.AddDays(7));
                                                break;

                                        }
                                        SMS sms = new SMS();
                                        sms.SendMessage(title, msg, string.Empty, order.MobileNumber,
                                            cp.SystemEmail, SmsType.Ppon);
                                        os.SentArrivalNoticeSms = true;
                                    }

                                    if (os.IsLoaded && !os.SentArrivalNoticePush)
                                    {
                                        //丟至mq直至上班時間做推播通知
                                        var storeReceivedMsg = string.Format("17Life商品(訂單：{0})已送達{1}囉", order.OrderId, opd.SevenStoreName);
                                        NotificationFacade.PushMemberOrderMessageOnWorkTime(order.UserId, storeReceivedMsg, order.Guid);
                                        logger.Info("AppPushMQ: 超取已抵達門市 RefreshOrder.PushMemberOrderMessageOnWorkTime: " +
                                                    string.Format("UserId={0}, pmsg={1}, Guid={2}", order.UserId, storeReceivedMsg, order.OrderId));
                                        os.SentArrivalNoticePush = true;
                                    }

                                    break;
                                case GoodsStatus.DcReceiveSuccess:
                                    if (os.ShipToStockTime != null && os.ShipToStockTime != h.DeliveryTime)
                                    {
                                        logger.Warn("超取訂單進倉日變更,請確認對帳單與發票有無影響 oGuid=" + os.OrderGuid.ToString());
                                    }
                                    os.ShipToStockTime = h.DeliveryTime;
                                    break;
                                case GoodsStatus.CustomerReceived:
                                    if (os.IsLoaded)
                                    {
                                        os.ModifyTime = h.DeliveryTime;
                                        os.ReceiptTime = h.DeliveryTime;
                                        os.IsReceipt = true;
                                        opd.IsCompleted = true;
                                        OrderFacade.CompleteISPOrder(os.OrderGuid); //完成訂單
                                    }
                                    if (order.IsLoaded && order.IsCanceling && cp.EnableCancelPaidByIspOrder)
                                    {
                                        //原被取消買家又領取者,恢復訂單取消狀態
                                        order.IsCanceling = false;
                                        OrderFacade.SendOrderCancelMail(order.Guid, false, false);
                                        CommonFacade.AddAudit(order.Guid, AuditType.Order, "原取消訂單又領取包裹，訂單成立", "sys", false);
                                    }

                                    break;
                                case GoodsStatus.DcReceiveReturn:
                                case GoodsStatus.GoodsLost:
                                    opd.IsCancel = true;
                                    opd.CancelReason = h.DelvieryContent;
                                    break;
                                case GoodsStatus.DcReturn:
                                    os.IsReceipt = false;
                                    ReturnForm rf = op.ReturnFormGetListByOrderGuid(os.OrderGuid).Where(x => x.ProgressStatus == (int)ProgressStatus.Processing).FirstOrDefault();
                                    if (rf != null && rf.IsLoaded)
                                    {
                                        rf.VendorProgressStatus = (int)VendorProgressStatus.Automatic;
                                        op.ReturnFormSet(rf);
                                    }
                                    opd.DcReturn = 1;
                                    opd.DcReturnTime = h.DeliveryTime;
                                    opd.DcReturnReason = h.DelvieryContent;
                                    //取消訂單會有JOB處理
                                    break;
                                case GoodsStatus.ReverseStoreReceive:
                                    opd.DcReturn = 1;
                                    opd.DcReturnTime = h.DeliveryTime;
                                    opd.DcReturnReason = h.DelvieryContent;
                                    break;
                                case GoodsStatus.DcReceiveFail:
                                    opd.DcReceiveFail = 1;
                                    opd.DcReceiveFailTime = h.DeliveryTime;
                                    opd.DcReceiveFailReason = h.DelvieryContent;
                                    break;
                            }
                        }

                        op.OrderProductDeliverySet(opd);
                        if (os.IsLoaded)
                        {
                            op.OrderShipSet(os);
                        }
                        if (order.IsLoaded)
                        {
                            op.OrderSet(order);
                        }
                    }
                }
                
                idx += 200;
            }
        }

        public static string GetUidFromIspChannelServiceType(bool isB2C)
        {
            IspChannelServiceType serviceType = isB2C ? IspChannelServiceType.B2C : IspChannelServiceType.C2C;
            var uid = _uid.ContainsKey(serviceType) ? _uid[serviceType] : string.Empty;
            if (string.IsNullOrEmpty(uid))
            {
                uid = GetUidFromUserIspService(isB2C);
                if (!string.IsNullOrEmpty(uid))
                {
                    _uid.Add(serviceType, uid);
                }
            }
            return uid;
        }

        public static string GetUidFromUserIspService(bool b2C = true)
        {
            var ispApiResult = ISPApiRequest(string.Empty, cp.ISPApiUrl + "GetUserIspService");
            if (ispApiResult.Code != ApiResultCode.Success)
            {
                return string.Empty;
            }

            var data = JsonConvert.DeserializeObject<UserIspServiceModel>(ispApiResult.Data.ToString());
            var seven = data.IspChannels.FirstOrDefault(x => x.ServiceChannelCode == (int)ServiceChannel.SevenEleven);
            if (seven == null)
            {
                return string.Empty;
            }

            var serviceType = seven.ServiceType.FirstOrDefault(x => x.TypeCode == (b2C ? (byte)IspChannelServiceType.B2C : (byte)IspChannelServiceType.C2C));
            return serviceType == null ? string.Empty : serviceType.Uid;
        }

        public static ServiceChannelProperty GetServiceChannelByStoreGuid(Guid sellerGuid, ServiceChannel channel)
        {
            var json = JsonConvert.SerializeObject(new
            {
                storeKey = sellerGuid
            });
            
            var apiResult = ISPApiRequest(json, cp.ISPApiUrl + "QueryStoreStatus");
            if (apiResult.Code == ApiResultCode.Success)
            {
                var data = JsonConvert.DeserializeObject<ISPServiceData>(apiResult.Data.ToString());
                var serviceChannel = data.service.FirstOrDefault(x => x.serviceChannelCode == (int) channel);
                if (serviceChannel != null)
                {
                    return serviceChannel.serviceChannelProperty;
                }
            }

            return new ServiceChannelProperty();
        }

        public static ApiResult ISPApiRequest(string jsonData, string url,bool hasRoot = true)
        {
            byte[] dataBytes = Encoding.UTF8.GetBytes(jsonData);
            url = hasRoot ? url : cp.ISPApiUrl + url;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Headers.Add("Authorization", "Bearer " + cp.ISPOAuthToken);
            request.ContentType = "application/json";
            request.ContentLength = dataBytes.Length;
            request.Method = WebRequestMethods.Http.Post;

            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(dataBytes, 0, dataBytes.Length);
                requestStream.Flush();
            }

            string result = "";
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        result = reader.ReadToEnd();
                    }
                }
            }

            return Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(result);
        }

        /// <summary>
        /// 取得店取電子地圖URL
        /// </summary>
        /// <param name="type"></param>
        /// <param name="ticketId"></param>
        /// <param name="sellerGuid"></param>
        /// <param name="storeId"></param>
        /// <param name="b2C"></param>
        /// <returns></returns>
        public static string GetIspEmapUrl(ProductDeliveryType type, string ticketId, string storeId)
        {
            string queryString;
            switch (type)
            {
                case ProductDeliveryType.FamilyPickup:
                    queryString = string.Format("cvsname={0}&cvsid={1}&cvstemp=&exchange=true", new Uri(cp.SiteUrl).Host, ticketId);
                    if (cp.EnableFamiMap && !cp.EnableFamiMapTestMode)
                    {
                        return string.Format("http://mfme.map.com.tw/default.aspx?{0}", queryString);
                    }
                    else
                    {
                        return string.Format("/FamiMapTest.aspx?{0}", queryString);
                    }
                case ProductDeliveryType.SevenPickup:
                    queryString = string.Format("tempvar={0}&storeId={1}&app=false", ticketId, storeId);
                    return string.Format("{0}/ISP/SevenEMap?{1}", cp.SSLSiteUrl, queryString);
                default:
                    return string.Empty;
            }
        }

        private class ISPServiceData
        {
            public string storeKey { get; set; }
            public List<ServiceStoreData> service { get; set; }
        }

        private class ServiceStoreData
        {
            public string serviceChannel { get; set; }
            public int serviceChannelCode { get; set; }
            public string applicationStatus { get; set; }
            public int applicationStatusCode { get; set; }
            public string serviceType { get; set; }
            public string vendorName { get; set; }
            public string contactEmail { get; set; }
            public string firstContact { get; set; }
            public string vendorTell { get; set; }
            public string returnCycle { get; set; }
            public string returnDay { get; set; }
            public string returnMode { get; set; }
            public string returnModeOther { get; set; }
            public string returnAddress { get; set; }
            public string checkLabel { get; set; }
            public int checkLabelCode { get; set; }
            public bool enabled { get; set; }
            public ServiceChannelProperty serviceChannelProperty { get; set; }
        }

        public class ServiceChannelProperty
        {
            public string parentId { get; set; }
            public string eshopId { get; set; }
        }

        private class ISPOrderData
        {
            [JsonProperty("channelStoreCode")]
            public string ChannelStoreCode { get; set; }
            [JsonProperty("isCashOnPickup")]
            public bool IsCashOnPickup { get; set; }
            [JsonProperty("orderKey")]
            public string OrderKey { get; set; }
            [JsonProperty("pickupAmount")]
            public int PickupAmount { get; set; }
            [JsonProperty("productAmount")]
            public int ProductAmount { get; set; }
            [JsonProperty("receiverName")]
            public string ReceiverName { get; set; }
            [JsonProperty("senderName")]
            public string SenderName { get; set; }
            [JsonProperty("serviceChannel")]
            public string ServiceChannel { get; set; }
            [JsonProperty("storeKey")]
            public string StoreKey { get; set; }
            [JsonProperty("receiverMPhone")]
            public string ReceiverMPhone { get; set; }
            [JsonProperty("orderType")]
            public int OrderType { get; set; }
            [JsonProperty("receiverEmail")]
            public string ReceiverEmail { get; set; }
        }

        public class ISPMakeOrderReturnModel
        {
            public List<ISPMakeOrderReturnItem> successData { get; set; }
            public List<ISPMakeOrderReturnItem> failData { get; set; }

        }

        public class ISPMakeOrderReturnItem
        {
            public string orderKey { get; set;}
            public string orderToken { get; set; }
            public string message { get; set; }
        }
    }
}

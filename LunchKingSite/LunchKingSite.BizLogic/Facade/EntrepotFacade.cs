﻿using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Facade
{
    public class EntrepotFacade
    {
        #region Property

        private static ISysConfProvider _config;
        private static IEntrepotProvider _ep;
        private static IMemberProvider _mp;

        #endregion property

        static EntrepotFacade()
        {
            _config = ProviderFactory.Instance().GetConfig();
            _ep = ProviderFactory.Instance().GetProvider<IEntrepotProvider>();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        /// <summary>
        /// Get entrepot member by token
        /// </summary>
        /// <param name="token"></param>
        /// <param name="isShared"></param>
        /// <returns></returns>
        public static EntrepotMember MemberGetByToken(string token, int oauthClientId)
        {
            EntrepotMember member = _ep.EntrepotMemberGet(token, oauthClientId);
            if (member.Id > 0)
            {
                return member;
            }

            member.EntrepotToken = token;
            member.OauthClientId = oauthClientId;
            member.CreateTime = DateTime.Now;

            EntrepotMemberRegister memberRegist = new EntrepotMemberRegister(member);
            MemberRegisterReplyType reply = memberRegist.Process(false);
            if (reply == MemberRegisterReplyType.MemberExisted)
            {
                _ep.EntrepotMemberSet(member);
                member = _ep.EntrepotMemberGet(token, oauthClientId);
                if (member.UserId == 0)
                {
                    var e7LifeMember = MemberFacade.GetMember(memberRegist.GetMemberRegisterName());
                    if (e7LifeMember != null && e7LifeMember.IsLoaded)
                    {
                        FixEntrepotMemberLink(member, e7LifeMember);
                    }
                }
            }

            return member;
        }

        public static void FixEntrepotMemberLink(EntrepotMember entrepotMember, Member e7Member)
        {
            entrepotMember.UserId = e7Member.UniqueId;
            var ml = _mp.MemberLinkGet(e7Member.UniqueId, SingleSignOnSource.Entrepot);
            if (ml.IsLoaded)
            {
                ml.ExternalUserId = entrepotMember.Id.ToString();
                _mp.MemberLinkSet(ml);
            }
            else
            {
                MemberFacade.AddMemberLink(
                    e7Member.UniqueId,
                    SingleSignOnSource.Entrepot,
                    entrepotMember.Id.ToString(), e7Member.UserName);
            }
            _ep.EntrepotMemberSet(entrepotMember);
        }
    }
}

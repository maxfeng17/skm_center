﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.Refund;
using System.Dynamic;
using System.Runtime.Serialization;
using System.Security.Permissions;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.CustomerService;

namespace LunchKingSite.BizLogic.Facade
{
    public class UserRefundFacade
    {
        protected static IMemberProvider mp;
        protected static IPponProvider pp;
        protected static IOrderProvider op;
        protected static ISysConfProvider confProv;

        static UserRefundFacade()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            confProv = ProviderFactory.Instance().GetConfig();
        }

        /// <summary>
        /// 把BankInfo轉成可以序列化的List
        /// </summary>
        /// <param name="bks"></param>
        /// <returns></returns>
        private static dynamic BankInfoToList(BankInfoCollection bks)
        {
            List<dynamic> data = new List<dynamic>();

            foreach (var item in bks)
            {
                dynamic d = new DynamicContext();
                d.Address = item.Address;
                d.BankName = item.BankName;
                d.BankNo = item.BankNo;
                d.BranchName = item.BranchName;
                d.BranchNo = item.BranchNo;
                d.Id = item.Id;
                d.Telephone = item.Telephone;
                data.Add(d);
            }

            return data;
        }

        /// <summary>
        /// 取得銀行資料
        /// </summary>
        /// <returns></returns>
        public static dynamic BankInfoGetMainList()
        {
            BankInfoCollection bks = op.BankInfoGetMainList();
            return BankInfoToList(bks);
        }

        /// <summary>
        /// 取得分行資料
        /// </summary>
        /// <param name="bankNo"></param>
        /// <returns></returns>
        public static dynamic BankInfoGetBranchList(string bankNo)
        {
            BankInfoCollection bks = op.BankInfoGetBranchList(bankNo);
            return BankInfoToList(bks);
        }

        /// <summary>
        /// 取得訂單資訊
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public static RefundOrderInfo GetOrderInfo(int uniqueId, string orderId)
        {
            ViewCouponListMainCollection coupons = mp.GetCouponListMainListByOrderID(uniqueId, orderId);

            RefundOrderInfo orderinfo = new RefundOrderInfo
            {
                Coupons = coupons,
                ThirdPartyPaymentSystem = ThirdPartyPayment.None,
                IsThridPartyPay = false
            };

            if (coupons.Count > 0)
            {
                var coupon = coupons.First();
                orderinfo.OrderGuid = coupon.Guid;
                CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(orderinfo.OrderGuid, OrderClassification.LkSite);
                if (ctCol.Any(x => x.CreditCard + x.Atm + x.Tcash + x.FamilyIsp + x.SevenIsp > 0))
                {
                    orderinfo.IsPaidByCash = true;
                    orderinfo.IsRefundCashOnly = PaymentFacade.IsRefundCashOnly(ctCol.First());
                }
                orderinfo.IsIspPay = ctCol.Any(x => x.FamilyIsp + x.SevenIsp > 0);
                orderinfo.IsReturnable = ReturnService.GetReturnableProducts(orderinfo.OrderGuid).Any();
                orderinfo.HasProcessingReturnForm = ReturnService.HasProcessingReturnForm(orderinfo.OrderGuid);
                orderinfo.IsPpon = coupon.DeliveryType == (int)DeliveryType.ToShop;
                orderinfo.IsIsp = !orderinfo.IsPpon && coupon.ProductDeliveryType != (int)ProductDeliveryType.Normal;
                DealProperty dp = pp.DealPropertyGetByOrderGuid(orderinfo.OrderGuid);
                orderinfo.IsEntrustSell = (EntrustSellType)dp.EntrustSell != EntrustSellType.No;

                ViewPponCouponCollection vpcCol = pp.ViewPponCouponGetList("order_Guid", coupon.Guid);
                //只有擋未核銷鎖定
                if (vpcCol.Any(x => x.IsReservationLock.HasValue && x.IsReservationLock == true && ctCol.Any(c => c.CouponId == x.CouponId && (c.Status == (int)TrustStatus.Initial || c.Status == (int)TrustStatus.Trusted))))
                {
                    orderinfo.IsReservationLock = true;
                }
                orderinfo.IsThridPartyPay = ctCol.Any(x => x.Tcash > 0);
                if (orderinfo.IsThridPartyPay)
                {
                    orderinfo.ThirdPartyPaymentSystem = (ThirdPartyPayment)ctCol.First(x => x.Tcash > 0).ThirdPartyPayment;
                }
                if (ctCol.Any(x => x.FamilyIsp + x.SevenIsp > 0))
                {
                    orderinfo.IsPaidByIsp = true;
                }

                orderinfo.HasPartialCancelOrVerified =
                    ctCol.Any(x => x.Status == (int)TrustStatus.Verified && x.DeliveryType == (int)DeliveryType.ToShop);
            }
            
            #region 檢查訂單狀態

            orderinfo.IsExchangeProcessing = OrderExchangeUtility.HasProcessingExchangeLog(orderinfo.OrderGuid);
            Order o = op.OrderGet(Order.Columns.OrderId, orderId);
            orderinfo.HasPartialCancelOrVerified = orderinfo.HasPartialCancelOrVerified ? true : o.IsPartialCancel;
            orderinfo.IsInstallment = o.Installment > 0;
            
            #endregion 檢查訂單狀態

            #region 檢查發票是否正在2聯轉3聯
            EinvoiceMainCollection emc = op.EinvoiceMainCollectionGetByOrderId(orderId);
            if (emc.Count > 0)
            {
                if (emc.Where(x => x.InvoiceMode2 == (int)InvoiceMode2.Triplicate && x.InvoiceNumber == null && x.InvoiceStatus == (int)EinvoiceType.C0401).ToList().Count > 0)
                {
                    orderinfo.IsInvoice2To3 = true;
                }
            }
            #endregion

            return orderinfo;
        }

        /// <summary>
        /// 取得預計退款的金額
        /// </summary>
        /// <param name="unique_id"></param>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static decimal? GetOrderRefundAmount(int unique_id, Guid orderGuid)
        {
            CashTrustLogCollection ctlc = mp.CashTrustLogGetListByOrderGuid(orderGuid);
            Order o = op.OrderGet(orderGuid);
            decimal? refundAmount = null;
            if (ctlc != null && o !=null)
            {
                refundAmount = ctlc.Where(x => x.Status == (int)TrustStatus.Initial || x.Status == (int)TrustStatus.Trusted).Sum(s => s.Amount - s.DiscountAmount);
                refundAmount = ctlc.Any(x => x.UserId != unique_id) ? null : refundAmount;
            }
            return refundAmount;
        }

        /// <summary>
        /// 設定訂單資訊
        /// </summary>
        /// <param name="coupons"></param>
        /// <param name="isReturnable"></param>
        /// <param name="hasProcessingReturnForm"></param>
        /// <param name="isEtrustSell"></param>
        /// <param name="isReservationLock"></param>
        /// <param name="isExchangeProcessing"></param>
        /// <returns></returns>
        public static dynamic SetOrderInfo(ViewCouponListMainCollection coupons, bool isReturnable, bool hasProcessingReturnForm, bool isEtrustSell, bool isReservationLock, bool isExchangeProcessing)
        {
            dynamic obj = new DynamicContext();

            string OrderStatusMessage = string.Empty;
            Guid OrderGuid = Guid.Empty;
            string OrderID = string.Empty;
            int? OrderStatus = null;
            bool? IsEntrustSell = null;
            //用來判斷退貨商品是否為憑證或宅配
            bool? isPpon = null;
            bool? IsCoupon = null;
            bool? gonext = null;
            DateTime? OrderCreateTime = null;


            DateTime now = DateTime.Now;
            int slug;

            if (coupons.Count == 0)
            {
                OrderStatusMessage = "查無此訂單。";
            }
            else
            {
                #region 頁面填值

                ViewCouponListMain main = coupons.First();
                OrderGuid = main.Guid;
                OrderID = main.OrderId;
                OrderStatus = main.OrderStatus;
                IsEntrustSell = isEtrustSell;
                //用來判斷退貨商品是否為憑證或宅配
                isPpon = main.DeliveryType.Value == (int)DeliveryType.ToShop;
                IsCoupon = isPpon;

                #endregion

                OrderCreateTime = (DateTime)main.CreateTime;
                gonext = true;

                //新版退貨判斷
                if (hasProcessingReturnForm)
                {
                    //全數退貨中
                    gonext = false;
                    OrderStatusMessage = "已申請退貨，無法再申請。";
                }
                else if (isPpon != null && !isPpon.Value && isExchangeProcessing)
                {
                    gonext = false;
                    OrderStatusMessage = "您已申請換貨處理中，若有需要協助，請洽客服人員。";
                }
                else if (!isReturnable)
                {
                    //全數退貨完成
                    gonext = false;
                    OrderStatusMessage = "訂單已全數退貨，無法再申請。";
                }
                else if (main.BusinessHourOrderTimeE <= now && int.TryParse(main.Slug, out slug) &&
                         slug < main.BusinessHourOrderMinimum)
                {
                    //已結檔且未達門檻
                    OrderStatusMessage = "未達門檻不需退貨。";
                    if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 &&
                        (main.OrderStatus & (int)Core.OrderStatus.Complete) > 0) //ATM已付款，未達門檻顯示退款
                    {
                        OrderStatusMessage =
                            @"此好康未達門檻，請至<a href='coupon_list.aspx' style='color:blue;'>【會員專區】</a>進行指定帳戶退款";
                    }
                }
                else
                {
                    if (isPpon != null && isPpon.Value)
                    {
                        //憑證
                        if (main.TotalCount != 0)
                        {
                            gonext = (main.RemainCount > 0);
                            OrderStatusMessage = ((main.RemainCount == 0)
                                                               ? " 憑證已使用完畢，無法退貨。"
                                                               : ("此好康未使用:" + main.RemainCount));
                        }
                    }
                    else
                    {
                        //商品檔鑑賞期檢查
                        if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                        {
                            gonext = false;
                            OrderStatusMessage = "商品已過鑑賞期，無法退貨。";
                        }
                    }
                }

                #region ATM 檢核

                if ((main.OrderStatus & (int)Core.OrderStatus.Complete) == 0)
                {
                    if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0)
                    {
                        gonext = false;
                        OrderStatusMessage = main.CreateTime.Date.Equals(DateTime.Now.Date)
                                                        ? "尚未完成ATM付款，無需退貨。"
                                                        : "ATM 逾期未付款，無法退貨。";
                    }
                    else
                    {
                        OrderStatusMessage = "未完成付款的訂單。";
                    }
                }

                #endregion

                #region 不得退貨條件

                string noRefundString = "此訂單不接受退貨申請。";

                //不接受退貨
                if ((main.Status & (int)GroupOrderStatus.NoRefund) > 0)
                {
                    gonext = false;
                    OrderStatusMessage = noRefundString;
                }

                //結檔後七天不接受退貨
                if ((main.Status & (int)GroupOrderStatus.DaysNoRefund) > 0 && main.BusinessHourOrderTimeE.AddDays(confProv.ProductRefundDays) < DateTime.Now)
                {
                    gonext = false;
                    OrderStatusMessage = noRefundString;
                }

                //過期不接受退貨
                if ((main.Status & (int)GroupOrderStatus.ExpireNoRefund) > 0)
                {
                    if (main.BusinessHourDeliverTimeE != null && main.BusinessHourDeliverTimeE.Value.AddDays(1) < now)
                    {
                        gonext = false;
                        OrderStatusMessage = noRefundString;
                    }
                }

                //演出時間前十日過後不能退貨
                if ((main.Status & (int)GroupOrderStatus.NoRefundBeforeDays) > 0 && now.Date >= ((DateTime)main.BusinessHourDeliverTimeS).AddDays(-confProv.PponNoRefundBeforeDays).Date)
                {
                    gonext = false;
                    OrderStatusMessage = noRefundString;
                }

                #endregion

                #region 零元檔，全家0元檔，捐款檔，公益檔

                //0元好康
                if (main.ItemPrice.Equals(0) || (main.Status & (int)GroupOrderStatus.KindDeal) > 0)
                {
                    gonext = false;
                    OrderStatusMessage = noRefundString;
                }

                #endregion

                #region 其它條件

                //4月1號以前
                if (main.BusinessHourOrderTimeS < new DateTime(2011, 4, 1))
                {
                    OrderStatusMessage = "此好康不提供退貨服務。";
                    gonext = false;
                }

                if (gonext != null && (gonext.Value && isReservationLock))
                {
                    OrderStatusMessage = "訂單已預約，無法申請退貨。";
                    gonext = false;
                }

                #endregion



            }

            obj.OrderGuid = OrderGuid;
            obj.OrderID = OrderID;
            obj.OrderStatus = OrderStatus;
            obj.IsEntrustSell = IsEntrustSell;
            obj.IsPpon = isPpon;
            obj.IsCoupon = isPpon;
            obj.OrderCreateTime = OrderCreateTime;
            obj.GoNext = gonext;
            obj.OrderStatusMessage = OrderStatusMessage;

            return obj;
        }

        /// <summary>
        /// 處理ATM退款
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="userName"></param>
        /// <param name="isCoupon"></param>
        /// <param name="isPaidByCash"></param>
        /// <param name="data"></param>
        /// <param name="receiverName"></param>
        /// <param name="receiverAddress"></param>
        /// <returns></returns>
        public static dynamic RequestRefundATM(Guid orderGuid, string userName, bool isCoupon, bool isPaidByCash, 
            KeyValuePair<CtAtmRefund, KeyValuePair<OrderStatusLog, RefundType>> data, string receiverName, string receiverAddress)
        {
            //本來這邊的程式碼是專門給ATM 訂單用的, 所以PaymentTransaction的條件也要是ATM. 但因為 "訂購超過一年" 的訂單
            //無法刷退, 所以也規劃走ATM 退款模式. 因此在這裡把ATM 判斷取消

            PaymentTransactionCollection pt = op.PaymentTransactionGetList(orderGuid);

            PaymentTransaction pt_atm = (pt.Count() > 0) ? op.PaymentTransactionGetListPaging(-1, 
                                                                                              -1, 
                                                                                            null,
                                                                         PayTransStatusFlag.None, 
                                                                                               0,
                        PaymentTransaction.Columns.TransId + " = " + pt.FirstOrDefault().TransId,
                 PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Authorization)
                                                                                               .FirstOrDefault() 
                                                         : null;

            if (pt_atm != null)
            {
                CtAtmRefund ctatmrefund = data.Key;
                ctatmrefund.PaymentId = pt_atm.TransId;
                ctatmrefund.Amount = 0;
                op.CtAtmRefundSet(ctatmrefund);
                op.CtAtmUpdateStatus((int)AtmStatus.RefundRequest, orderGuid);
            }

            return RequestRefund(orderGuid, userName, isCoupon, isPaidByCash, data.Value, receiverName, receiverAddress);
        }

        /// <summary>
        /// 處理ATM退款的退貨申請
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="userName"></param>
        /// <param name="isCoupon"></param>
        /// <param name="isPaidByCash"></param>
        /// <param name="data"></param>
        /// <param name="receiverName"></param>
        /// <param name="receiverAddress"></param>
        /// <returns></returns>
        public static dynamic RequestRefund(Guid orderGuid, string userName, bool isCoupon, bool isPaidByCash, 
            KeyValuePair<OrderStatusLog, RefundType> data, string receiverName, string receiverAddress)
        {
            dynamic obj = new DynamicContext();

            EinvoiceMainCollection einvoice = op.EinvoiceMainGetListByOrderGuid(orderGuid);
            bool isRefundCash = (data.Value == RefundType.Atm || data.Value == RefundType.Cash) ? true : false; //是否刷退 或 退款至帳戶
            
            bool requireCreditNote = false;
            CreateReturnFormResult result;
            int? dummy;

            if (isCoupon)
            {
                //憑證退貨申請單
                IEnumerable<int> refundableCouponIds = ReturnService.GetRefundableCouponCtlogs(data.Key.OrderGuid, BusinessModel.Ppon).Select(x => (int)x.CouponId).ToList();
                if (refundableCouponIds.Any())
                {
                    result = ReturnService.CreateCouponReturnForm(data.Key.OrderGuid, refundableCouponIds, !isRefundCash, userName, data.Key.Reason, out dummy, false);

                    //檢查是否需折讓單:已開立或準備開立發票、有使用現金購買及申請退現金
                    requireCreditNote = einvoice.Any(x => x.CouponId.HasValue &&
                                                          refundableCouponIds.Contains(x.CouponId.Value) &&
                                                          x.VerifiedTime.HasValue)
                                        && isPaidByCash
                                        && isRefundCash;
                }
                else
                {
                    result = CreateReturnFormResult.ProductsUnreturnable;
                }
            }
            else
            {
                //宅配退貨申請單，抓出 1.非退貨中 2.非已退貨 3.非換貨中 
                List<int> orderProductIds = op.OrderProductGetListByOrderGuid(data.Key.OrderGuid).Where(x => x.IsCurrent && !x.IsReturning && !x.IsReturned && !x.IsExchanging).Select(x => x.Id).ToList();
                if (orderProductIds.Any())
                {
                    result = ReturnService.CreateRefundForm(data.Key.OrderGuid, orderProductIds, !isRefundCash, userName, data.Key.Reason,
                        receiverName, receiverAddress, null, null, out dummy, false);
                }
                else
                {
                    result = CreateReturnFormResult.ProductsUnreturnable;
                }
            }

            if (result == CreateReturnFormResult.Created)
            {
                int returnFormId = op.ReturnFormGetListByOrderGuid(data.Key.OrderGuid)
                      .OrderByDescending(x => x.Id)
                      .First().Id;

                string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId, data.Key.Reason);
                CommonFacade.AddAudit(data.Key.OrderGuid, AuditType.Refund, auditMessage, userName, false);

                OrderFacade.SendCustomerReturnFormApplicationMail(returnFormId);
                if (!isCoupon)
                {
                    EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Refund, returnFormId);
                }
            }
            //else
            //{
            //    //建立退貨單失敗
            //    //View.ReturnPanel("RefundFail", requireCreditNote);

            //    obj.RequireCreditNote = requireCreditNote;

            //    return obj;
            //}

            obj.RequireCreditNote = requireCreditNote;
            obj.IsRefundCash = isRefundCash;
            obj.IsCoupon = isCoupon;
            obj.Result = result;
            obj.ResultMessage = GetTextFromEnum(result);

            return obj;
            #region Mark
            /*
            if (isRefundCash)
            {
                //刷退或是指定帳戶退款
                if (isCoupon)
                {
                    //憑證
                    if (data.Value == RefundType.Atm)
                    {
                        //退款至帳戶
                        //View.ReturnPanel("RefundATMSuccess_ToShopOrder", requireCreditNote);
                        return obj;
                    }
                    else
                    {
                        //刷退
                        //View.ReturnPanel("RefundCashSuccess_ToShopOrder", requireCreditNote);
                        return obj;
                    }
                }
                else
                {
                    //宅配
                    if (data.Value == RefundType.Atm)
                    {
                        //退款至帳戶
                        //View.ReturnPanel("RefundATMSuccess_ToHouseOrder", requireCreditNote);
                        return obj;
                    }
                    else
                    {
                        //刷退
                        //View.ReturnPanel("RefundCashSuccess_ToHouseOrder", requireCreditNote);
                        return obj;
                    }
                }
            }
            else
            {
                //只退購物金
                if (DateTime.Compare(DateTime.Today, deal.BusinessHourDeliverTimeS.Value.Date) < 0)
                {
                    //兌換開始日前
                    //View.ReturnPanel("RefundCashPointSuccess_BeforeUseStartTime", requireCreditNote);
                    return obj;
                }
                else
                {
                    //兌換開始日後
                    if (isCoupon)
                    {
                        //憑證
                        //View.ReturnPanel("RefundCashPointSuccess_AfterUseStartTime", requireCreditNote);
                        return obj;
                    }
                    else
                    {
                        //宅配
                        //View.ReturnPanel("RefundCashPointSuccess_AfterUseStartTime_ToHouseOrder", requireCreditNote);
                        return obj;
                    }
                }
            }*/
            #endregion
        }

        /// <summary>
        /// 退貨申請原因
        /// </summary>
        /// <param name="isToShopOrder">憑證訂單才需顯示</param>
        /// <returns></returns>
        public static dynamic RefundReason(bool isToShopOrder)
        {
            List<dynamic> list = new List<dynamic>();

            if (isToShopOrder) // 憑證訂單才需顯示
            {
                list.Add(new
                {
                    ReasonText = "服務/店家品質不佳",
                    IsNeedOtherReason = false
                });
                list.Add(new
                {
                    ReasonText = "訂不到位置",
                    IsNeedOtherReason = false
                });
                list.Add(new
                {
                    ReasonText = "忘記使用",
                    IsNeedOtherReason = false
                });
            }
            else
            {
                list.Add(new
                {
                    ReasonText = "商品品質不佳",
                    IsNeedOtherReason = false
                });
                list.Add(new
                {
                    ReasonText = "商品與描述、預期不符",
                    IsNeedOtherReason = false
                });
                list.Add(new
                {
                    ReasonText = "商品瑕疵",
                    IsNeedOtherReason = false
                });
                list.Add(new
                {
                    ReasonText = "商品錯發、漏發",
                    IsNeedOtherReason = false
                });
                list.Add(new
                {
                    ReasonText = "延遲出貨",
                    IsNeedOtherReason = false
                });
            }
            list.Add(new
            {
                ReasonText = "看到更優惠的商品",
                IsNeedOtherReason = false
            });
            list.Add(new
            {
                ReasonText = "其它原因與建議",
                IsNeedOtherReason = true
            });

            return list;
        }

        /// <summary>
        /// 提供其他程式呼叫，因dynamic編譯後呼叫是internal級別，無法跨程式使用
        /// </summary>
        /// <param name="isToShopOrder"></param>
        /// <returns></returns>
        public static Dictionary<string,bool> RefundReasonForOther(bool isToShopOrder)
        {
            return (RefundReason(isToShopOrder) as List<dynamic>).ToDictionary(key => (string)key.ReasonText, val => (bool)val.IsNeedOtherReason);
        }

        /// <summary>
        /// 是否允許退貨
        /// </summary>
        /// <param name="orderInfo"></param>
        /// <param name="reson"></param>
        /// <returns></returns>
        public static bool IsAllowRefund(RefundOrderInfo orderInfo, out string reson)
        {
            reson = string.Empty;
            if (orderInfo.Coupons.Count == 0)
            {
                reson = "查無此訂單。"; //I18N.Phrase.NoOrder;
                return false;
            }

            ViewCouponListMain main = orderInfo.Coupons.First();
            bool isPpon = main.DeliveryType == (int)DeliveryType.ToShop;

            //超付 未成單
            if (orderInfo.IsPaidByIsp && !Helper.IsFlagSet(main.OrderStatus, (int)OrderStatus.Complete))
            {
                if (!confProv.EnableCancelPaidByIspOrder)
                {
                    return false;
                }
                if (Helper.IsFlagSet(main.OrderStatus, (int)OrderStatus.Cancel))
                {
                    reson = "訂單已取消";
                    return false;
                }
                if (main.IsCanceling)
                {
                    reson = "已申請取消訂單，無法再申請。";
                    return false;
                }
                if (!confProv.AllowsIspReturnForShipping && !string.IsNullOrEmpty(main.PreShipNo))
                {
                    reson = "超商取貨付款訂單於運送期間無法取消訂單。";
                    return false;
                }
            }

            if (orderInfo.HasProcessingReturnForm)
            {
                reson = "已申請退貨，無法再申請。";
                return false;
            }

            if (!isPpon && orderInfo.IsExchangeProcessing)
            {
                reson = "您已申請換貨處理中，若有需要協助，請洽客服人員。";
                return false;
            }

            if (!orderInfo.IsReturnable)
            {
                reson = "訂單已全數退貨，無法再申請。";
                return false;
            }

            int slug;
            if (main.BusinessHourOrderTimeE <= DateTime.Now && int.TryParse(main.Slug, out slug) &&
                         slug < main.BusinessHourOrderMinimum)
            {
                reson = "未達門檻不需退貨。";
                return false;
            }

            if (orderInfo.IsInvoice2To3)
            {
                reson = "發票二聯改三聯處理中，無法申請退貨，若有需要協助，請洽客服人員。";
                return false;
            }

            if (isPpon)
            {
                if (main.TotalCount != 0)
                {
                    if (main.RemainCount <= 0)
                    {
                        reson = "憑證已使用完畢，無法退貨。";
                        return false;
                    }
                }
            }
            else
            {
                if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                {
                    reson = "商品已過鑑賞期，無法退貨。";
                    return false;
                }
            }

            if ((main.OrderStatus & (int)OrderStatus.Complete) == 0 &&
                (main.OrderStatus & (int)OrderStatus.ATMOrder) > 0)
            {
                if (main.CreateTime.Date.Equals(DateTime.Now.Date))
                {
                    reson = "尚未完成ATM付款，無需退貨。";
                    return false;
                }

                reson = "ATM 逾期未付款，無法退貨。";
                return false;
            }

            if (((main.Status & (int)GroupOrderStatus.NoRefund) > 0)
             || ((main.Status & (int)GroupOrderStatus.DaysNoRefund) > 0 &&
                  main.BusinessHourOrderTimeE.AddDays(confProv.ProductRefundDays) < DateTime.Now)
             || ((main.Status & (int)GroupOrderStatus.ExpireNoRefund) > 0 &&
                  main.BusinessHourDeliverTimeE != null && main.BusinessHourDeliverTimeE.Value.AddDays(1) < DateTime.Now)
             || ((main.Status & (int)GroupOrderStatus.NoRefundBeforeDays) > 0 &&
                  main.BusinessHourDeliverTimeS != null &&
                  DateTime.Now.Date >= ((DateTime)main.BusinessHourDeliverTimeS).AddDays(-confProv.PponNoRefundBeforeDays).Date)
             || main.ItemPrice.Equals(0)
             || (main.Status & (int)GroupOrderStatus.KindDeal) > 0
             || (main.BusinessHourOrderTimeS < new DateTime(2011, 4, 1))
               )
            {
                reson = "此訂單不接受退貨申請。";
                return false;
            }

            if (orderInfo.IsReservationLock)
            {
                reson = "訂單已預約，無法申請退貨。";
                return false;
            }

            var familyLockCnt = FamiGroupCoupon.GetFamiCouponLockedCount(main);
            if (familyLockCnt > 0)
            {
                reson = "目前部分商品使用狀態資料處理中，待資料處理完畢後再重新按下退貨申請。";
                return false;
            }

            return true;
        }

        /// <summary>
        /// 由API觸發退貨申請
        /// </summary>
        /// <param name="kp"></param>
        /// <param name="userName"></param>
        /// <param name="orderInfo"></param>
        /// <returns></returns>
        public static KeyValuePair<CreateReturnFormResult, bool> RequestRefund(KeyValuePair<OrderStatusLog, RefundType> kp, string userName, 
            RefundOrderInfo orderInfo)
        {
            CreateReturnFormResult result = CreateReturnFormResult.None;
            bool requireCreditNote = false;

            OrderStatusLog orderStatusLog = kp.Key;
            RefundType refundType = kp.Value;
            
            var orderGuid = orderStatusLog.OrderGuid;
            
            var einvoice  = op.EinvoiceMainGetListByOrderGuid(orderGuid);
            var order = op.OrderGet(orderGuid);

            if (einvoice.Count > 0)
            {
                //電子折讓單上線 : 除三聯紙本發票 申請退貨 皆視為走電子折讓
                //2016以前的只能走紙本，主要影響成套、宅配，一般憑證不影響
                einvoice.Where(x => x.InvoiceMode2 == (int)InvoiceMode2.Duplicate &&
                                    x.AllowanceStatus.GetValueOrDefault((int)AllowanceStatus.PaperAllowance) == (int)AllowanceStatus.PaperAllowance && x.OrderTime > confProv.EinvAllowanceStartDate)
                        .ToList().ForEach(i => i.AllowanceStatus = (int)AllowanceStatus.ElecAllowance);
                EinvoiceFacade.SetEinvoiceMainCollectionWithLog(einvoice, userName);
            }

            if (order.IsLoaded && order.ParentOrderId != null)
            {
                //是否刷退 或 退款至帳戶
                bool isRefundCash = refundType == RefundType.Atm || refundType == RefundType.Cash || refundType == RefundType.Tcash;
                int? dummy;

                var main = orderInfo.Coupons.First();
                bool isPpon = main.DeliveryType == (int)DeliveryType.ToShop;
                List<int> returnItemList = new List<int>();

                #region 退憑證

                if (isPpon)
                {
                    var ctls = ReturnService.GetRefundableCouponCtlogs(orderStatusLog.OrderGuid, BusinessModel.Ppon);
                    var refundableCouponIds = ctls.Where(x => x.CouponId != null).Select(x => (int)x.CouponId).ToList();

                    if (refundableCouponIds.Any())
                    {
                        result = ReturnService.CreateCouponReturnForm(
                        orderStatusLog.OrderGuid, refundableCouponIds, !isRefundCash, userName, orderStatusLog.Reason, out dummy, false);
                        requireCreditNote = einvoice.Any(x => x.CouponId.HasValue &&
                                                          refundableCouponIds.Contains(x.CouponId.Value) &&
                                                          x.VerifiedTime.HasValue)
                                        && orderInfo.IsPaidByCash
                                        && isRefundCash;
                        returnItemList = refundableCouponIds;
                    }
                }

                #endregion
                #region 退宅配

                else
                {
                    //宅配退貨申請單，抓出 1.非退貨中 2.非已退貨 3.非換貨中 
                    List<int> orderProductIds = op.OrderProductGetListByOrderGuid(orderStatusLog.OrderGuid)
                            .Where(x => x.IsCurrent && !x.IsReturning && !x.IsReturned && !x.IsExchanging).Select(x => x.Id).ToList();
                    if (orderProductIds.Any())
                    {
                        result = ReturnService.CreateRefundForm(orderStatusLog.OrderGuid, orderProductIds, !isRefundCash, userName,
                            orderStatusLog.Reason, orderStatusLog.ReceiverName, orderStatusLog.ReceiverAddress, orderStatusLog.IsReceive, null, out dummy, false);
                        returnItemList = orderProductIds;
                    }
                }

                #endregion

                if (result == CreateReturnFormResult.Created)
                {
                    if (einvoice.Count > 0 && einvoice.Any(x => x.InvoiceRequestTime.HasValue))
                    {
                        EinvocieCancelRequestProcess(einvoice, orderGuid, returnItemList, isPpon ? DeliveryType.ToShop : DeliveryType.ToHouse, userName);
                    }

                    int returnFormId = op.ReturnFormGetListByOrderGuid(orderStatusLog.OrderGuid)
                          .OrderByDescending(x => x.Id)
                          .First().Id;

                    string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId, orderStatusLog.Reason);
                    CommonFacade.AddAudit(orderStatusLog.OrderGuid, AuditType.Refund, auditMessage, userName, false);

                    OrderFacade.SendCustomerReturnFormApplicationMail(returnFormId);
                    if (!isPpon)
                    {
                        EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Refund, returnFormId);
                    }

                    return new KeyValuePair<CreateReturnFormResult, bool>(result, requireCreditNote);
                }
            }
            return new KeyValuePair<CreateReturnFormResult, bool>(CreateReturnFormResult.InvalidArguments, requireCreditNote);
        }
        
        public static void SetCtAtmRefund(CtAtmRefund ct, Guid orderGuid)
        {
            var pt = op.PaymentTransactionGetList(orderGuid).FirstOrDefault();
            if (pt != null)
            {
                var ptAtm = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                        PaymentTransaction.Columns.TransId + " = " + pt.TransId,
                        PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Authorization)
                        .FirstOrDefault();
                if (ptAtm != null)
                {
                    CtAtmRefund ctatmrefund = ct;
                    ctatmrefund.PaymentId = ptAtm.TransId;
                    ctatmrefund.Amount = 0;
                    op.CtAtmRefundSet(ctatmrefund);
                    op.CtAtmUpdateStatus((int)AtmStatus.RefundRequest, orderGuid);
                }
            }
        }

        public static bool SetCtAtmRefundModel(CtAtmRefundModel atmForm, Guid orderGuid, int buyerUserId)
        {
            CtAtmRefund ct = new CtAtmRefund();
            CtAtmRefund beforeCt = op.CtAtmRefundGetLatest(orderGuid);
            if (beforeCt.IsLoaded && beforeCt.Status < (int)AtmRefundStatus.AchProcess)
            {
                ct = beforeCt;
            }
            else
            {
                ct.UserId = buyerUserId;
                ct.OrderGuid = orderGuid;
                ct.Status = (int)AtmRefundStatus.Initial;
            }

            ct.AccountName = atmForm.AccountName;
            ct.Id = atmForm.Id;
            ct.BankName = atmForm.BankName;
            ct.BranchName = atmForm.BranchName;
            ct.BankCode = atmForm.BankNo + atmForm.BranchNo;
            ct.AccountNumber = atmForm.AccountNumber.Replace(" ", string.Empty).PadLeft(14, '0');
            ct.Phone = atmForm.Phone;
            ct.CreateTime = DateTime.Now;

            SetCtAtmRefund(ct, orderGuid);

            return ct.IsNew;
        }

        /// <summary>
        /// 退貨明細是否顯示折讓單
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static bool IsShowCreditNote(int userId, Guid orderGuid)
        {
            var ctl = mp.CashTrustLogGetListByOrderGuid(orderGuid);
            if (ctl.Any(x => x.CreditCard + x.Atm > 0 && x.Status < (int)TrustStatus.Returned))
            {
                var couponListMain = MemberFacade.ViewCouponListMainGetByOrderGuid(userId, orderGuid).FirstOrDefault();
                if (couponListMain != null)
                {
                    if (couponListMain.ReturnStatus == (int)ProgressStatus.Processing)
                    {
                        var einvoices = op.EinvoiceMainGetListByOrderGuid(orderGuid);
                        var receipt = op.EntrustSellReceiptGetByOrder(orderGuid);
                        var receiptCodeGenerated = receipt.IsLoaded && receipt.ReceiptCode != null;

                        if (einvoices.Any(x => x.VerifiedTime.HasValue) || receiptCodeGenerated)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 判斷申請退貨時是否取消發票的紙本申請
        /// </summary>
        public static void EinvocieCancelRequestProcess(EinvoiceMainCollection emc, Guid orderGuid, List<int> returnItemList, DeliveryType type, string userName)
        {
            var vclm = mp.GetCouponListMainByOid(orderGuid);
            if (type == DeliveryType.ToShop)
            {
                bool isGroupCoupon = Helper.IsFlagSet(vclm.BusinessHourStatus, BusinessHourStatus.GroupCoupon);

                if (!isGroupCoupon)
                {
                    foreach (var id in returnItemList)
                    {
                        EinvoiceMain em = emc.FirstOrDefault(x => x.CouponId.Value == id && (x.InvoiceStatus == (int)EinvoiceType.C0401 || x.InvoiceStatus == (int)EinvoiceType.Initial || x.InvoiceStatus == (int)EinvoiceType.NotComplete));
                        //未印製才可取消
                        if (em != null && em.InvoicePaperedTime == null)
                        {
                            emc.FirstOrDefault(x => x.Id == em.Id).InvoiceRequestTime = null;
                        }
                    }
                }
                //成套視為宅配處理
                else
                {
                    CashTrustLogCollection ctlogs = mp.CashTrustLogGetListByOrderGuid(orderGuid);
                    //判斷部分OR全退
                    bool isAllReturn = !ctlogs.Any(x => x.Status == (int)TrustStatus.Verified);
                    //只會有一筆開立，但二轉三可能會有多筆資料
                    //即買即退未產號
                    EinvoiceMain em = emc.FirstOrDefault(x => (x.InvoiceStatus == (int)EinvoiceType.C0401 || x.InvoiceStatus == (int)EinvoiceType.Initial || x.InvoiceStatus == (int)EinvoiceType.NotComplete) && x.InvoicePaperedTime == null);
                    if (isAllReturn && em != null)
                    {
                        emc.FirstOrDefault(x => x.Id == em.Id).InvoiceRequestTime = null;
                    }
                }
            }
            else
            {
                CashTrustLogCollection ctlogs = mp.CashTrustLogGetListByOrderGuid(orderGuid);
                int comboPackCount = vclm.ComboPackCount == 0 ? 1 : vclm.ComboPackCount;
                bool isAllReturn = (ctlogs.Where(x => x.SpecialStatus != (int)TrustSpecialStatus.Freight).Count() * comboPackCount) == returnItemList.Count();

                EinvoiceMain em = emc.FirstOrDefault(x => (x.InvoiceStatus == (int)EinvoiceType.C0401 || x.InvoiceStatus == (int)EinvoiceType.Initial || x.InvoiceStatus == (int)EinvoiceType.NotComplete) && x.InvoicePaperedTime == null);
                if (isAllReturn && em != null)
                {
                    emc.FirstOrDefault(x => x.Id == em.Id).InvoiceRequestTime = null;
                }
            }
            
            EinvoiceFacade.SetEinvoiceMainCollectionWithLog(emc, userName);
        }

        /// <summary>
        /// 處理上期未完成退貨而需調整的折讓狀態
        /// </summary>
        /// <param name="date_start"></param>
        /// <param name="date_end"></param>
        public static void ProcessReturnFormCreditNoteType(DateTime date_start, DateTime date_end)
        {
            //2聯
            ReturnFormCollection rfc_d = op.GetNoCompleteAndRequireCreditNote(date_start, date_end, InvoiceMode2.Duplicate, AllowanceStatus.ElecAllowance);
            rfc_d.ForEach(x =>
            {
                x.CreditNoteType = (int)AllowanceStatus.ElecAllowance;
                op.ReturnFormSet(x);
                string auditMessage = string.Format("因超過發票當期處理時間，系統將退貨單({0})調整為電子折讓", x.Id);
                CommonFacade.AddAudit(x.OrderGuid, AuditType.Refund, auditMessage, "sys", false);
            });
            //3聯
            ReturnFormCollection rfc_t = op.GetNoCompleteAndRequireCreditNote(date_start, date_end, InvoiceMode2.Triplicate, AllowanceStatus.PaperAllowance);
            rfc_t.ForEach(x =>
            {
                x.CreditNoteType = (int)AllowanceStatus.PaperAllowance;
                op.ReturnFormSet(x);
                string auditMessage = string.Format("因超過發票當期處理時間，系統將退貨單({0})調整為紙本折讓", x.Id);
                CommonFacade.AddAudit(x.OrderGuid, AuditType.Refund, auditMessage, "sys", false);
            });
        }

        /// <summary>
        /// 取得列舉的文字說明
        /// </summary>
        /// <param name="crfr"></param>
        /// <returns></returns>
        private static string GetTextFromEnum(CreateReturnFormResult crfr)
        {
            switch (crfr)
            {
                case CreateReturnFormResult.Created:
                    return "Created";
                case CreateReturnFormResult.AtmOrderNeedAtmRefundAccount:
                    return I18N.Phrase.AtmOrderNeedAtmRefundAccount;
                case CreateReturnFormResult.DealDeniesRefund:
                    return I18N.Phrase.DealDeniesRefund;
                case CreateReturnFormResult.InvalidArguments:
                    return I18N.Phrase.InvalidArguments;
                case CreateReturnFormResult.NotEnoughProducts:
                    return I18N.Phrase.NotEnoughProducts;
                case CreateReturnFormResult.OldOrderNeedAtmRefundAccount:
                    return I18N.Phrase.OldOrderNeedAtmRefundAccount;
                case CreateReturnFormResult.ProductsIsReturning:
                    return I18N.Phrase.ProductsIsReturning;
                case CreateReturnFormResult.ProductsUnreturnable:
                    return I18N.Phrase.ProductsUnreturnable;
                case CreateReturnFormResult.RequireCreditNote:
                    return I18N.Phrase.RequireCreditNote;
                case CreateReturnFormResult.OrderNotCreate:
                    return "訂單尚未完成付款，無須退貨";
                default:
                    return string.Empty;
            }
        }
    }

    /// <summary>
    /// Serializable DynamicObject
    /// </summary>
    [Serializable]
    public class DynamicContext : DynamicObject, ISerializable
    {
        private Dictionary<string, object> dynamicContext = new Dictionary<string, object>();

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return (dynamicContext.TryGetValue(binder.Name, out result));
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            dynamicContext.Add(binder.Name, value);
            return true;
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            foreach (KeyValuePair<string, object> kvp in dynamicContext)
            {
                info.AddValue(kvp.Key, kvp.Value);
            }
        }

        public DynamicContext()
        {
        }

        protected DynamicContext(SerializationInfo info, StreamingContext context)
        {
            // TODO: validate inputs before deserializing. See http://msdn.microsoft.com/en-us/library/ty01x675(VS.80).aspx
            foreach (SerializationEntry entry in info)
            {
                dynamicContext.Add(entry.Name, entry.Value);
            }
        }

    }
}

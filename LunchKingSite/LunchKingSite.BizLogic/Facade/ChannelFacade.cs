﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.BizLogic.Models.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Models.Channel;
using LunchKingSite.BizLogic.Models.LineShopping;
using LunchKingSite.BizLogic.Model.API;
using log4net;
using System.Text;
using System.Security.Cryptography;
using LunchKingSite.BizLogic.Models.PchomeChannel;
using LunchKingSite.BizLogic.Jobs;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.BizLogic.Facade
{
    public class ChannelFacade
    {
        private static ISysConfProvider _config;
        private static IOAuthProvider op;
        private static IChannelProvider cp;
        private static ISellerProvider sp;
        private static ISystemProvider _sysp;
        private static IOrderProvider _orderProvider;
        private static IPponProvider pp;
        private static IFamiportProvider fami;
        private static ILog logger = LogManager.GetLogger("ChannelFacade");
        public static ConcurrentDictionary<Guid, List<SellerTreeModel>> SellerTree;
        public static ConcurrentDictionary<Guid, List<Guid>> ParentSellerTree;
        private static JsonSerializer jsonSerializer;

        static ChannelFacade()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOAuthProvider>();
            cp = ProviderFactory.Instance().GetProvider<IChannelProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _sysp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _orderProvider = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            SellerTree = new ConcurrentDictionary<Guid, List<SellerTreeModel>>();
            ParentSellerTree = new ConcurrentDictionary<Guid, List<Guid>>();
            SellerRoleAddOrUpdate();
            jsonSerializer = new JsonSerializer();
        }

        #region 導購

        public static void RefreshShoppingGuideCache()
        {
            foreach (ShoppingGuideDealType dealType in Enum.GetValues(typeof(ShoppingGuideDealType)))
            {
                var c = cp.ShoppingGuideChannelGet(dealType);
                if (c.IsLoaded && c.ModifyTime > DateTime.Now.AddMinutes(-360))
                {
                    continue;
                }

                ConcurrentDictionary<Guid, List<DealCategory>> dealCategoryCol;
                PponDealSpecialCityType cityType = PponDealSpecialCityType.PponAndPiinlife;

                switch (dealType)
                {
                    case ShoppingGuideDealType.Piinlife:
                        cityType = PponDealSpecialCityType.Piinlife;
                        break;
                    case ShoppingGuideDealType.BeautyAndLeisure:
                    case ShoppingGuideDealType.Coupon:
                    case ShoppingGuideDealType.Delivery:
                    case ShoppingGuideDealType.Food:
                    case ShoppingGuideDealType.Travel:
                        cityType = PponDealSpecialCityType.WithOutSpecialCity;
                        break;
                }

                GetDealBidByShoppingGuideDealType(dealType, DateTime.Today, out dealCategoryCol, cityType);
            }

            cp.ShoppingGuideQueryDelete(0);
        }

        public static List<Guid> GetDealBidByShoppingGuideDealType(ShoppingGuideDealType type, DateTime queryDate
            , out ConcurrentDictionary<Guid, List<DealCategory>> dealCategoryCol, PponDealSpecialCityType cityType = PponDealSpecialCityType.WithOutSpecialCity)
        {
            List<Guid> bidlist = pp.ViewPponDealGetBidListByDayWithNoLock(queryDate, queryDate.AddHours(24), cityType, true);
            var vadcList = pp.ViewAllDealCategoryGetListByBidList(bidlist).ToList();

            if (type != ShoppingGuideDealType.Overall)
            {
                var keepBid = PponFacade.GetShoppingGuideKeepBid(vadcList, type);
                vadcList = vadcList.Where(x => keepBid.Contains(x.Bid)).ToList();
            }

            dealCategoryCol = new ConcurrentDictionary<Guid, List<DealCategory>>();

            foreach (var vadc in vadcList)
            {
                dealCategoryCol.AddOrUpdate(vadc.Bid,
                    new List<DealCategory> { new DealCategory { Id = vadc.Cid, Name = vadc.Name } },
                    (key, oldValue) =>
                    {
                        oldValue.Add(new DealCategory { Id = vadc.Cid, Name = vadc.Name });
                        return oldValue;
                    });
            }

            //濾除掉沒有分類且過期的檔次 (ViewAllDealCategory 有下當日上檔時間區間條件)
            bidlist = bidlist.Intersect(dealCategoryCol.Keys.ToList()).ToList();

            //Save Db Cache
            cp.ShoppingGuideChannelSaveDetail(type, bidlist);
            if (type == ShoppingGuideDealType.Overall)
            {
                var saveCategoryCol = dealCategoryCol.ToDictionary(c => c.Key, c => jsonSerializer.Serialize(c.Value));
                cp.ShoppingGuideCategorySave(saveCategoryCol);
            }

            return bidlist;
        }

        public static List<Guid> GetDealBidFromDbCache(ShoppingGuideDealType type, DateTime queryDate,
            out ConcurrentDictionary<Guid, List<DealCategory>> dealCategoryCol, bool newFilterLowGM = false
            , decimal couponGrossMargin = 1.0m, decimal deliveryGrossMargin = 1.0m)
        {
            dealCategoryCol = new ConcurrentDictionary<Guid, List<DealCategory>>();
            var c = cp.ShoppingGuideChannelGet(type);
            var bids = new List<Guid>();
            if (c.IsLoaded && c.ModifyTime.ToShortDateString() == queryDate.ToShortDateString())
            {
                bids = newFilterLowGM ? cp.GetFilterLowGrossMarginDeal(c.ChannelId, c.BatchId, couponGrossMargin, deliveryGrossMargin)
                    : cp.ShoppingGuideChannelDetailGet(type, c.BatchId);
                dealCategoryCol = GetDealCategoryColByBid(bids);
            }

            return bids;
        }

        public static ConcurrentDictionary<Guid, List<DealCategory>> GetDealCategoryColByBid(List<Guid> bids)
        {
            ConcurrentDictionary<Guid, List<DealCategory>> result =
                new ConcurrentDictionary<Guid, List<DealCategory>>();
            var categoryTemp = cp.ShoppingGuideDealCategoryGet(bids);
            foreach (var ct in categoryTemp)
            {
                var data = jsonSerializer.Deserialize<List<DealCategory>>(ct.DealCategoryJson);
                result.TryAdd(ct.Bid, data);
            }

            return result;
        }

        public static List<Guid> GetFilterLowGrossMarginDeal(ShoppingGuideDealType type, DateTime queryDate,
            decimal couponGrossMargin = 1.0m, decimal deliveryGrossMargin = 1.0m)
        {
            var c = cp.ShoppingGuideChannelGet(type);
            var bids = new List<Guid>();
            if (c.IsLoaded && c.ModifyTime.ToShortDateString() == queryDate.ToShortDateString())
            {
                bids = cp.GetFilterLowGrossMarginDeal(c.ChannelId, c.BatchId, couponGrossMargin, deliveryGrossMargin);
            }

            return bids;
        }

        #endregion 導購

        #region 代銷

        /// <summary>
        /// 取得通路列表
        /// </summary>
        /// <returns>(string)通路name, ((int)OrderClassification,AppId)</returns>
        public static Dictionary<string, KeyValuePair<int, string>> GetChannelList()
        {
            var result = new Dictionary<string, KeyValuePair<int, string>>();
            var oauthClientCol = op.OauthClientGetListByChannelClient();
            foreach (var oauthClient in oauthClientCol)
            {
                var company = GetOrderClassificationByName(oauthClient.Name);

                if (AgentChannel.NONE != company)
                {
                    if (oauthClient.Name != "PEZDelivery")
                        result.Add(Helper.GetEnumDescription(company), new KeyValuePair<int, string>((int)company, oauthClient.AppId));

                }
            }
            return result;
        }

        public static AgentChannel GetOrderClassificationByRequestType()
        {
            if (HttpContext.Current.Request.QueryString["type"] == null)
            {
                return AgentChannel.LionTravelOrder;
            }

            List<int> ocList = Enum.GetValues(typeof(AgentChannel)).Cast<AgentChannel>().Except(new AgentChannel[] { AgentChannel.NONE }).Select(p => (int)p).ToList();

            int oc;
            if (!int.TryParse(HttpContext.Current.Request.QueryString["type"], out oc) || !ocList.Contains(oc))
            {
                oc = (int)AgentChannel.LionTravelOrder;
            }
            return (AgentChannel)oc;
        }

        #region 通路商列舉

        public static AgentChannel GetOrderClassificationByName(string name)
        {
            AgentChannel resultType = AgentChannel.NONE;
            if (string.IsNullOrEmpty(name))
            {
                return resultType;
            }

            var setting = jsonSerializer.Deserialize<AgentOrderClassJsonModel>(_sysp.SystemDataGet("AgentOrderClassification").Data);
            //setting.ExceptionName  消創會客服(免消除'@'前的字樣)
            if (name.IndexOf('@') > 0 && !setting.ExceptionName.Contains(name))
            {
                name = name.Substring(name.IndexOf('@'), name.Length - name.IndexOf('@'));
            }
            var iKey = 0;
            foreach (var an in setting.AllowName)
            {
                if (an.Value.Any(n => string.Equals(n, name, StringComparison.CurrentCultureIgnoreCase)))
                {
                    iKey = an.Key;
                }
                if (iKey != 0)
                {
                    break;
                }
            }
            Enum.TryParse(iKey.ToString(), out resultType);
            return resultType;
        }

        /// <summary>
        /// 依 Oauth Token 取得通路 Agent
        /// </summary>
        /// <returns></returns>
        public static AgentChannel GetOrderClassificationByHeaderToken()
        {
            if (HttpContext.Current == null)
            {
                return AgentChannel.NONE;
            }

            int checkAg;
            var oc = GetOauthClientByHeaderToken();
            if (oc == null)
            {
                HttpCookie agentCookie = HttpContext.Current.Request.Cookies.Get(LkSiteCookie.ChannelAgent.ToString("g"));
                if (agentCookie != null && int.TryParse(agentCookie.Value, out checkAg))
                {
                    return (AgentChannel)int.Parse(agentCookie.Value);
                }

                return AgentChannel.NONE;
            }

            var result = GetOrderClassificationByName(oc.Name);

            HttpCookie agCookie = HttpContext.Current.Request.Cookies.Get(LkSiteCookie.ChannelAgent.ToString("g"));
            if (agCookie == null || !int.TryParse(agCookie.Value, out checkAg) || DateTime.Now.AddDays(15) > agCookie.Expires)
            {
                agCookie = CookieManager.NewCookie(LkSiteCookie.ChannelAgent.ToString("g"));
                agCookie.Value = Convert.ToString((int)result);
                agCookie.Expires = DateTime.Now.AddDays(30);
                HttpContext.Current.Response.SetCookie(agCookie);
            }

            return result;
        }

        /// <summary>
        /// 依 Oauth Token 取得 Oauth_Client.id
        /// </summary>
        /// <returns></returns>
        public static int GetOauthClientIdByHeaderToken()
        {
            var oc = GetOauthClientByHeaderToken();
            return oc == null ? 0 : oc.Id;
        }

        /// <summary>
        /// 依 Oauth Token 取得 Oauth_Client
        /// </summary>
        /// <returns></returns>
        public static OauthClient GetOauthClientByHeaderToken()
        {
            var token = GetHeaderToken();
            if (string.IsNullOrEmpty(token))
            {
                return null;
            }

            return op.OauthClientGetByAccessToken(token).FirstOrDefault();
        }

        /// <summary>
        /// 取得Header Authorization Token
        /// </summary>
        /// <returns></returns>
        public static string GetHeaderToken()
        {
            if (HttpContext.Current == null)
            {
                return null;
            }

            var authHeader = HttpContext.Current.Request.Headers["Authorization"];
            if (authHeader == null)
            {
                if (HttpContext.Current.Request["access_token"] != null)
                {
                    return HttpContext.Current.Request["access_token"];
                }
                else if (HttpContext.Current.Request["accessToken"] != null)
                {
                    return HttpContext.Current.Request["accessToken"];
                }
                else if (HttpContext.Current.Request.RequestContext.RouteData.Values["accessToken"] != null) //RouteData
                {
                    return HttpContext.Current.Request.RequestContext.RouteData.Values["accessToken"].ToString();
                }
                else
                {
                    return null;
                }
            }
            var authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);
            return OAuthFacade.ExtractToken(authHeaderVal);
        }

        #endregion 通路商列舉

        public static bool IsOverLowGrossmarginDeals(List<Guid> bidList, decimal minGrossmargin = 0.1m)
        {
            return bidList.Select(bid => ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(bid)).Any(cost => Math.Round(cost.MinGrossMargin, 4) < minGrossmargin);
        }

        public static bool IsDealSoldOut(Guid bid)
        {
            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            if (!vpd.IsLoaded)
            {
                return true;
            }

            if (vpd.OrderedQuantity >= vpd.OrderTotalLimit.GetValueOrDefault())
            {
                return true;
            }

            if (vpd.ComboDelas != null && vpd.ComboDelas.Any()) //多檔次全子檔售完即排除
            {
                return IsComboDealsSoldOut(vpd.ComboDelas.Select(x => x.BusinessHourGuid).ToList());
            }

            return false;
        }

        public static bool IsComboDealsSoldOut(List<Guid> bids)
        {
            int soldOutCnt = 0;

            foreach (var bid in bids)
            {
                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
                if (vpd.IsLoaded)
                {
                    if (vpd.OrderedQuantity >= vpd.OrderTotalLimit.GetValueOrDefault())
                    {
                        soldOutCnt++;
                    }
                }
            }

            return soldOutCnt == bids.Count;
        }

        public static bool IsOverLowGrossmarginDealsForShoppingGuide(List<Guid> bidList, decimal checkGrossMargin = 1.0m)
        {
            var costData = ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(bidList);
            return costData.Any(x => Math.Round(x.MinGrossMargin, 4) < checkGrossMargin);
        }

        /// <summary>
        /// 過濾檔次條件
        /// </summary>
        /// <param name="deals"></param>
        /// <param name="channelClientProperty"></param>
        /// <param name="channelIdList"></param>
        /// <returns></returns>
        public static List<Guid> ChannelExcludedGuid(List<IViewPponDeal> deals, ChannelClientProperty channelClientProperty, List<int> channelIdList, int channel)
        {
            var excludedGuid = new List<Guid>();

            foreach (var item in deals)
            {
                List<int> categoryList = PponDealPreviewManager.GetDealCategoryIdList(item.BusinessHourGuid);
                categoryList = categoryList.Except(channelIdList).ToList();

                //排除廠商自定序號
                if (channel != (int)AgentChannel.PayEasyOrder 
                    && Helper.IsFlagSet(item.GroupOrderStatus ?? 0, GroupOrderStatus.PEZevent)
                    && !Helper.IsFlagSet(item.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
                {
                    excludedGuid.Add(item.BusinessHourGuid);
                    continue;
                }

                //排除全家檔次
                if (Helper.IsFlagSet(item.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal)
                    && !Helper.IsFlagSet(item.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
                {
                    excludedGuid.Add(item.BusinessHourGuid);
                    continue;
                }

                //排除公益檔
                if (Helper.IsFlagSet(item.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
                {
                    excludedGuid.Add(item.BusinessHourGuid);
                    continue;
                }

                //排除APP限定檔次
                if (categoryList.Any(x => x == CategoryManager.Default.AppLimitedEdition.CategoryId))
                {
                    excludedGuid.Add(item.BusinessHourGuid);
                    continue;
                }

                //排除低毛利檔次
                decimal channelMinGrossMargin = 0.1m; //預設低毛利門檻
                BaseCostGrossMargin cost = ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(item.BusinessHourGuid);
                if (cost.MinGrossMargin == 0 || channelClientProperty.MinGrossMargin == 0 || channelClientProperty.MinGrossMargin == null)
                {
                    if (Math.Round(cost.BaseGrossMargin, 4) < channelMinGrossMargin)
                    {
                        excludedGuid.Add(item.BusinessHourGuid);
                    }
                }
                else
                {
                    channelMinGrossMargin = (decimal)channelClientProperty.MinGrossMargin;
                    if (Math.Round(cost.MinGrossMargin, 4) <= channelClientProperty.MinGrossMargin)
                    {
                        excludedGuid.Add(item.BusinessHourGuid);
                    }
                }
                //排除子檔內含低毛利檔次
                if (item.ComboDelas != null && item.ComboDelas.Any())
                {
                    if (ChannelFacade.IsOverLowGrossmarginDeals(item.ComboDelas.Select(y => y.BusinessHourGuid).ToList(), channelMinGrossMargin))
                    {
                        excludedGuid.Add(item.BusinessHourGuid);
                    }
                }

                //排除成套商品
                if (Helper.IsFlagSet(item.BusinessHourStatus, BusinessHourStatus.GroupCoupon) 
                        && !IsEnableChannelGroupCoupon(item, channelClientProperty))
                {
                    excludedGuid.Add(item.BusinessHourGuid);
                    continue;
                }

                //排除憑證消費多重選項
                if (item.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToShop, item.DeliveryType.Value))
                {
                    if (item.ComboDelas != null && item.ComboDelas.Any())
                    {
                        foreach (var subdeal in item.ComboDelas)
                        {
                            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(subdeal.BusinessHourGuid);
                            if (vpd.ShoppingCart == true)
                            {
                                excludedGuid.Add(item.BusinessHourGuid);
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (item.ShoppingCart == true)
                        {
                            excludedGuid.Add(item.BusinessHourGuid);
                        }
                    }
                }

                //PChome憑證排除多分店/限紙本憑證/兌換時間限制
                if (channel == (int)AgentChannel.PChome && item.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToShop, item.DeliveryType.Value))
                {
                    if (item.ComboDelas != null && item.ComboDelas.Any())
                    {
                        foreach (var subdeal in item.ComboDelas)
                        {
                            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(subdeal.BusinessHourGuid);
                            if (vpd.IsMultipleStores)
                            {
                                excludedGuid.Add(item.BusinessHourGuid);
                                break;
                            }

                            if (vpd.LabelTagList != null && vpd.LabelTagList.Contains(((int)DealLabelSystemCode.OnlyPaperFormatCoupon).ToString()))
                            {
                                excludedGuid.Add(item.BusinessHourGuid);
                                break;
                            }

                            //日子過了可以拔掉
                            DateTime useEndTime = vpd.ChangedExpireDate.HasValue ? vpd.ChangedExpireDate.Value : vpd.BusinessHourDeliverTimeE.Value;
                            if (useEndTime < Convert.ToDateTime("2020/03/15") || vpd.BusinessHourOrderTimeE < Convert.ToDateTime("2019/12/01"))
                            {
                                excludedGuid.Add(item.BusinessHourGuid);
                            }
                        }
                    }
                    else
                    {
                        if (item.IsMultipleStores)
                        {
                            excludedGuid.Add(item.BusinessHourGuid);
                        }

                        if (item.LabelTagList != null && item.LabelTagList.Contains(((int)DealLabelSystemCode.OnlyPaperFormatCoupon).ToString()))
                        {
                            excludedGuid.Add(item.BusinessHourGuid);
                        }

                        DateTime useEndTime = item.ChangedExpireDate.HasValue ? item.ChangedExpireDate.Value : item.BusinessHourDeliverTimeE.Value;
                        if (useEndTime < Convert.ToDateTime("2020/03/15") || item.BusinessHourOrderTimeE < Convert.ToDateTime("2019/12/01"))
                        {
                            excludedGuid.Add(item.BusinessHourGuid);
                        }

                    }
                }
                //代銷通路
                List<int> itemAgentChannels = ProposalFacade.GetAgentChannels(item.AgentChannels);
                if (!itemAgentChannels.Contains(channel))
                {
                    excludedGuid.Add(item.BusinessHourGuid);
                }
            }

            return excludedGuid;
        }

        public static List<MultipleMainDealPreview> ChannelComboDealRemoveMulti(List<MultipleMainDealPreview> deals)
        {

            List<Guid> overGroup = pp.PponOptionGetOverGroup();
            MultipleMainDealPreview[] tempDeals = deals.ToArray();
            foreach (var deal in tempDeals)
            {
                try
                {
                    if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
                    {
                        deals.Remove(deal);
                    }
                    else
                    {
                        ViewComboDeal[] tempCombo = deal.PponDeal.ComboDelas.ToArray();

                        foreach (var subdeal in tempCombo)
                        {
                            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(subdeal.BusinessHourGuid);
                            if (overGroup.Contains(subdeal.BusinessHourGuid) || vpd.IsExpiredDeal.GetValueOrDefault(false) || vpd.ComboPackCount > 1 ||
                                Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
                            {
                                if (deal.PponDeal.ComboDelas.Contains(subdeal))
                                {
                                    deal.PponDeal.ComboDelas.Remove(subdeal);
                                }

                            }
                        }

                        if (!deal.PponDeal.ComboDelas.Any())
                        {
                            deals.Remove(deal);
                        }
                    }

                }
                catch (Exception ex)
                {
                    logger.Error("ChannelComboDealRemoveMulti ERROR bid:" + deal.DealId, ex);
                }

            }

            return deals;
        }

        public static List<MultipleMainDealPreview> ChannelComboDealBlackWhiteList(List<MultipleMainDealPreview> deals, int? channelid, int? areaid, string categoryid, int channel, int delivery_type, ref List<Guid> excludedGuid)
        {

            //黑名單
            ChannelCommissionListCollection sellerBlack = cp.ChannelCommissionListGetBySeller(channel, 1);
            ChannelCommissionListCollection mainBidBlack = cp.ChannelCommissionListGetByMainDeal(channel, 1);
            ChannelCommissionListCollection subBidBlack = cp.ChannelCommissionListGetBySubDeal(channel, 1);
            ChannelCommissionListCollection bidBlack = cp.ChannelCommissionListGetByDeal(channel, 1);
            //白名單
            ChannelCommissionListCollection sellerWhite = cp.ChannelCommissionListGetBySeller(channel, 2);
            ChannelCommissionListCollection mainBidWhite = cp.ChannelCommissionListGetByMainDeal(channel, 2);
            ChannelCommissionListCollection subBidWhite = cp.ChannelCommissionListGetBySubDeal(channel, 2);
            ChannelCommissionListCollection bidWhite = cp.ChannelCommissionListGetByDeal(channel, 2);
            DateTime now = DateTime.Now;


            //黑名單-商家
            foreach (ChannelCommissionList s in sellerBlack)
            {
                List<MultipleMainDealPreview> exclude = deals.Where(x => x.PponDeal.SellerGuid == s.SellerGuid).ToList();

                foreach (MultipleMainDealPreview e in exclude)
                {
                    //沒有該商家的白名單->直接刪
                    if (!mainBidWhite.Where(x => x.MainBid == e.DealId).Any() && !subBidWhite.Where(x => x.MainBid == e.DealId).Any() && !bidWhite.Where(x => x.Bid == e.DealId).Any())
                    {
                        deals.Remove(e);
                    }


                    List<ChannelCommissionList> mList = mainBidWhite.Where(x => x.SellerGuid == e.PponDeal.SellerGuid && x.MainBid == e.DealId).ToList();
                    List<ChannelCommissionList> bList = subBidWhite.Where(x => x.SellerGuid == e.PponDeal.SellerGuid && x.MainBid == e.DealId).ToList();
                    List<ChannelCommissionList> dList = bidWhite.Where(x => x.SellerGuid == e.PponDeal.SellerGuid && x.Bid == e.DealId).ToList();

                    //白名單有子檔,不移除
                    foreach (ChannelCommissionList b in bList)
                    {
                        ViewComboDeal[] tempCombo = e.PponDeal.ComboDelas.ToArray();
                        foreach (var subdeal in tempCombo)
                        {

                            if (b.Bid != subdeal.BusinessHourGuid && !mList.Select(x => x.MainBid).Contains(subdeal.MainBusinessHourGuid))
                            {
                                e.PponDeal.ComboDelas.Remove(subdeal);
                            }
                        }
                    }

                    //白名單有母檔,不移除
                    foreach (ChannelCommissionList m in mList)
                    {
                        if (e.DealId != m.MainBid)
                        {
                            deals.Remove(e);
                        }
                    }


                    //白名單有單檔,不移除
                    foreach (ChannelCommissionList d in dList)
                    {
                        if (e.DealId != d.Bid)
                        {
                            deals.Remove(e);
                        }
                    }

                }
            }

            //黑名單-母檔
            foreach (ChannelCommissionList m in mainBidBlack)
            {
                List<MultipleMainDealPreview> exclude = deals.Where(x => x.PponDeal.BusinessHourGuid == m.MainBid).ToList();

                foreach (MultipleMainDealPreview e in exclude)
                {
                    //沒有該母檔的白名單->直接刪
                    if (!subBidWhite.Where(x => x.MainBid == e.DealId).Any())
                    {
                        deals.Remove(e);
                    }

                    List<ChannelCommissionList> bList = subBidWhite.Where(x => x.MainBid == e.DealId).ToList();

                    //白名單有子檔,不移除
                    foreach (ChannelCommissionList b in bList)
                    {

                        ViewComboDeal[] tempCombo = e.PponDeal.ComboDelas.ToArray();
                        foreach (var subdeal in tempCombo)
                        {
                            if (b.Bid != subdeal.BusinessHourGuid)
                            {
                                e.PponDeal.ComboDelas.Remove(subdeal);
                            }
                        }
                    }
                }
            }


            //黑名單-子檔
            foreach (ChannelCommissionList b in subBidBlack)
            {
                List<MultipleMainDealPreview> exclude = deals.Where(x => x.PponDeal.BusinessHourGuid == b.MainBid).ToList();

                foreach (MultipleMainDealPreview e in exclude)
                {
                    //只移除黑名單的某子檔
                    ViewComboDeal[] tempCombo = e.PponDeal.ComboDelas.ToArray();
                    foreach (var subdeal in tempCombo)
                    {
                        if (b.Bid == subdeal.BusinessHourGuid)
                        {
                            e.PponDeal.ComboDelas.Remove(subdeal);
                        }
                    }
                }
            }

            //黑名單-單檔
            foreach (ChannelCommissionList b in bidBlack)
            {
                List<MultipleMainDealPreview> exclude = deals.Where(x => x.PponDeal.BusinessHourGuid == b.Bid).ToList();

                foreach (MultipleMainDealPreview e in exclude)
                {
                    deals.Remove(e);
                }
            }


            //List<IViewPponDeal> GetOnlineDealsBySeller(Guid sellerGuid)
            //IViewPponDeal ViewPponDealGetByBid(Guid businessHourGuid)
            //MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(0, limitedTimeSelection.Sequence, ppdeal,new List<int>(), DealTimeSlotStatus.Default, 0);
            //MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(0, vbc.Seq, ppdeal, new List<int>(), DealTimeSlotStatus.Default, 0);

            //IViewPponDeal ppdeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vbc.BusinessHourGuid, true);
            //MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(0, vbc.Seq, ppdeal, new List<int>(), DealTimeSlotStatus.Default, 0);
            //dealList.Add(mainDealPreview);


            //MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(0, vbc.Seq, ppdeal, new List<int>(), DealTimeSlotStatus.Default, 0);
            //dealList.Add(mainDealPreview);


            //白名單-商家
            foreach (ChannelCommissionList s in sellerWhite)
            {
                List<IViewPponDeal> sWhiteList = ViewPponDealManager.DefaultManager.GetOnlineDealsBySeller((Guid)s.SellerGuid, Guid.Empty);
                foreach (IViewPponDeal sWhite in sWhiteList)
                {
                    //檢核檔期
                    if (sWhite.BusinessHourOrderTimeS > now || sWhite.BusinessHourOrderTimeE < now)
                    {
                        continue;
                    }

                    //檢核分類
                    if (!string.IsNullOrEmpty(categoryid) && !sWhite.CategoryIds.Contains(Convert.ToInt32(categoryid)))
                    {
                        continue;
                    }
                    //檢核頻道
                    bool checkChannelid = false;
                    if (channelid != null)
                    {
                        foreach (int cityid in sWhite.CityIds)
                        {
                            PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityid);
                            if (city.ChannelId != null && city.ChannelId == channelid)
                            {
                                checkChannelid = true;
                            }
                        }
                    }
                    else
                    {
                        checkChannelid = true;
                    }
                    if (!checkChannelid)
                        continue;

                    //檢核地區
                    if (areaid != null && !sWhite.CategoryIds.Contains(Convert.ToInt32(areaid)))
                    {
                        continue;
                    }

                    //沒有該商家的黑名單->直接加
                    if (!mainBidBlack.Where(x => x.SellerGuid == sWhite.SellerGuid).Any() && !subBidBlack.Where(x => x.SellerGuid == s.SellerGuid).Any() && !bidBlack.Where(x => x.SellerGuid == s.SellerGuid).Any())
                    {
                        MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(88, 0, sWhite, new List<int>(), DealTimeSlotStatus.Default, 0);
                        if (excludedGuid.Contains(mainDealPreview.DealId))
                        {
                            excludedGuid.RemoveAll(x => x == mainDealPreview.DealId);
                        }
                        if (!deals.Contains(mainDealPreview) && mainDealPreview.PponDeal.DeliveryType == delivery_type)
                        {
                            deals.Add(mainDealPreview);
                        }

                    }



                    List<ChannelCommissionList> mList = mainBidBlack.Where(x => x.SellerGuid == sWhite.SellerGuid).ToList();
                    List<ChannelCommissionList> bList = subBidBlack.Where(x => x.SellerGuid == sWhite.SellerGuid).ToList();
                    List<ChannelCommissionList> dList = bidBlack.Where(x => x.SellerGuid == sWhite.SellerGuid).ToList();


                    //黑名單有母檔
                    foreach (ChannelCommissionList m in mList)
                    {
                        if (sWhite.BusinessHourGuid != m.MainBid)
                        {
                            MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(88, 0, sWhite, new List<int>(), DealTimeSlotStatus.Default, 0);
                            if (excludedGuid.Contains(mainDealPreview.DealId))
                            {
                                excludedGuid.RemoveAll(x => x == mainDealPreview.DealId);
                            }
                            if (!deals.Contains(mainDealPreview) && mainDealPreview.PponDeal.DeliveryType == delivery_type)
                                deals.Add(mainDealPreview);
                        }
                    }


                    //黑名單有子檔
                    foreach (ChannelCommissionList b in bList)
                    {
                        if (!mList.Select(x => x.MainBid).Contains(b.MainBid))
                        {
                            MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(88, 0, sWhite, new List<int>(), DealTimeSlotStatus.Default, 0);
                            ViewComboDeal[] tempCombo = mainDealPreview.PponDeal.ComboDelas.ToArray();
                            foreach (var subdeal in tempCombo)
                            {
                                if (subdeal.BusinessHourGuid == b.Bid)
                                {
                                    mainDealPreview.PponDeal.ComboDelas.Remove(subdeal);
                                }
                            }

                            if (excludedGuid.Contains(mainDealPreview.DealId))
                            {
                                excludedGuid.RemoveAll(x => x == mainDealPreview.DealId);
                            }
                            if (!deals.Contains(mainDealPreview) && mainDealPreview.PponDeal.DeliveryType == delivery_type)
                                deals.Add(mainDealPreview);

                            if (deals.Contains(mainDealPreview) && mainDealPreview.PponDeal.DeliveryType == delivery_type)
                            {
                                //若有異動，則須更新
                                deals.Remove(mainDealPreview);
                                deals.Add(mainDealPreview);
                            }
                        }
                    }

                    //黑名單有單檔
                    foreach (ChannelCommissionList d in dList)
                    {
                        if (sWhite.BusinessHourGuid != d.Bid)
                        {
                            MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(88, 0, sWhite, new List<int>(), DealTimeSlotStatus.Default, 0);
                            if (excludedGuid.Contains(mainDealPreview.DealId))
                            {
                                excludedGuid.RemoveAll(x => x == mainDealPreview.DealId);
                            }
                            if (!deals.Contains(mainDealPreview) && mainDealPreview.PponDeal.DeliveryType == delivery_type)
                                deals.Add(mainDealPreview);
                        }
                    }
                }

            }

            //白名單-母檔
            foreach (ChannelCommissionList m in mainBidWhite)
            {
                List<ChannelCommissionList> bList = subBidBlack.Where(x => x.MainBid == m.MainBid).ToList();
                IViewPponDeal mWhite = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)m.MainBid);

                //檢核檔期
                if (mWhite.BusinessHourOrderTimeS > now || mWhite.BusinessHourOrderTimeE < now)
                {
                    continue;
                }

                //檢核分類
                if (!string.IsNullOrEmpty(categoryid) && !mWhite.CategoryIds.Contains(Convert.ToInt32(categoryid)))
                {
                    continue;
                }

                //檢核頻道
                bool checkChannelid = false;
                if (channelid != null)
                {
                    foreach (int cityid in mWhite.CityIds)
                    {
                        PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityid);
                        if (city.ChannelId != null && city.ChannelId == channelid)
                        {
                            checkChannelid = true;
                        }
                    }
                }
                else
                {
                    checkChannelid = true;
                }
                if (!checkChannelid)
                    continue;

                //檢核地區
                if (areaid != null && !mWhite.CategoryIds.Contains(Convert.ToInt32(areaid)))
                {
                    continue;
                }

                MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(88, 0, mWhite, new List<int>(), DealTimeSlotStatus.Default, 0);
                ViewComboDeal[] tempCombo = mainDealPreview.PponDeal.ComboDelas.ToArray();
                foreach (var subdeal in tempCombo)
                {
                    if (bList.Select(x => x.Bid).Contains(subdeal.BusinessHourGuid))
                    {
                        mainDealPreview.PponDeal.ComboDelas.Remove(subdeal);
                    }
                }

                if (excludedGuid.Contains(mainDealPreview.DealId))
                {
                    excludedGuid.RemoveAll(x => x == mainDealPreview.DealId);
                }
                if (!deals.Contains(mainDealPreview) && mainDealPreview.PponDeal.DeliveryType == delivery_type)
                    deals.Add(mainDealPreview);

                if (deals.Contains(mainDealPreview) && mainDealPreview.PponDeal.DeliveryType == delivery_type)
                {
                    //若有異動，則須更新
                    deals.Remove(mainDealPreview);
                    deals.Add(mainDealPreview);
                }
            }

            //白名單-子檔
            if (subBidWhite.Any())
            {
                //先把同母檔分組
                Dictionary<Guid, List<Guid>> dicSubBidWhite = subBidWhite.GroupBy(x => x.MainBid).ToDictionary(key => (Guid)key.Key, value => value.Select(x => (Guid)x.Bid).ToList());

                foreach (KeyValuePair<Guid, List<Guid>> kvp in dicSubBidWhite)
                {
                    IViewPponDeal mWhite = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)kvp.Key);

                    //檢核檔期
                    if (mWhite.BusinessHourOrderTimeS > now || mWhite.BusinessHourOrderTimeE < now)
                    {
                        continue;
                    }

                    //檢核分類
                    if (!string.IsNullOrEmpty(categoryid) && !mWhite.CategoryIds.Contains(Convert.ToInt32(categoryid)))
                    {
                        continue;
                    }

                    //檢核頻道
                    bool checkChannelid = false;
                    if (channelid != null)
                    {
                        foreach (int cityid in mWhite.CityIds)
                        {
                            PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityid);
                            if (city.ChannelId != null && city.ChannelId == channelid)
                            {
                                checkChannelid = true;
                            }
                        }
                    }
                    else
                    {
                        checkChannelid = true;
                    }
                    if (!checkChannelid)
                        continue;

                    //檢核地區
                    if (areaid != null && !mWhite.CategoryIds.Contains(Convert.ToInt32(areaid)))
                    {
                        continue;
                    }

                    MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(88, 0, mWhite, new List<int>(), DealTimeSlotStatus.Default, 0);
                    //該母檔的原子檔們
                    ViewComboDeal[] tempCombo = mainDealPreview.PponDeal.ComboDelas.ToArray();
                    //白名單的子檔們
                    List<Guid> subBidWhiteList = dicSubBidWhite[kvp.Key];
                    foreach (var subdeal in tempCombo)
                    {
                        if (!subBidWhiteList.Contains(subdeal.BusinessHourGuid))
                        {
                            //沒有包含在白名單就移除
                            mainDealPreview.PponDeal.ComboDelas.Remove(subdeal);
                        }
                    }

                    if (excludedGuid.Contains(mainDealPreview.DealId))
                    {
                        excludedGuid.RemoveAll(x => x == mainDealPreview.DealId);
                    }
                    if (!deals.Contains(mainDealPreview) && mainDealPreview.PponDeal.DeliveryType == delivery_type)
                        deals.Add(mainDealPreview);

                    if (deals.Contains(mainDealPreview) && mainDealPreview.PponDeal.DeliveryType == delivery_type)
                    {
                        //若有異動，則須更新
                        deals.Remove(mainDealPreview);
                        deals.Add(mainDealPreview);
                    }
                }

            }

            //白名單-單檔
            foreach (ChannelCommissionList b in bidWhite)
            {
                IViewPponDeal mWhite = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)b.Bid);

                //檢核檔期
                if (mWhite.BusinessHourOrderTimeS > now || mWhite.BusinessHourOrderTimeE < now)
                {
                    continue;
                }

                //檢核分類
                if (!string.IsNullOrEmpty(categoryid) && !mWhite.CategoryIds.Contains(Convert.ToInt32(categoryid)))
                {
                    continue;
                }

                //檢核頻道
                bool checkChannelid = false;
                if (channelid != null)
                {
                    foreach (int cityid in mWhite.CityIds)
                    {
                        PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityid);
                        if (city.ChannelId != null && city.ChannelId == channelid)
                        {
                            checkChannelid = true;
                        }
                    }
                }
                else
                {
                    checkChannelid = true;
                }
                if (!checkChannelid)
                    continue;

                //檢核地區
                if (areaid != null && !mWhite.CategoryIds.Contains(Convert.ToInt32(areaid)))
                {
                    continue;
                }

                MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(88, 0, mWhite, new List<int>(), DealTimeSlotStatus.Default, 0);

                if (excludedGuid.Contains(mainDealPreview.DealId))
                {
                    excludedGuid.RemoveAll(x => x == mainDealPreview.DealId);
                }
                if (!deals.Contains(mainDealPreview) && mainDealPreview.PponDeal.DeliveryType == delivery_type)
                    deals.Add(mainDealPreview);
            }


            return deals;
        }

        /// <summary>
        /// 是否有在白名單，強制允許代銷行為
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="dealGuid"></param>
        /// <returns></returns>
        public static bool IsChannleForcePass(AgentChannel channel, IViewPponDeal dealGuid)
        {
            bool isForcePass = false;

            //有在白名單(單檔)
            List<ChannelCommissionList> bidWhite = cp.ChannelCommissionListGetByDeal((int)channel, 2).Where(x => x.Bid == dealGuid.BusinessHourGuid).ToList();
            if (bidWhite.Any())
            {
                return true;
            }

            //有在白名單(子檔)
            List<ChannelCommissionList> subWhite = cp.ChannelCommissionListGetBySubDeal((int)channel, 2).Where(x => x.Bid == dealGuid.BusinessHourGuid).ToList();
            if (subWhite.Any())
            {
                return true;
            }

            //有在白名單(母檔)且不在黑名單(子檔)
            List<ChannelCommissionList> mainBidWhite = cp.ChannelCommissionListGetByMainDeal((int)channel, 2).Where(x => x.MainBid == dealGuid.MainBid).ToList();
            if (mainBidWhite.Any())
            {
                List<ChannelCommissionList> bidBlack = cp.ChannelCommissionListGetBySubDeal((int)channel, 1).Where(x => x.Bid == dealGuid.BusinessHourGuid).ToList();
                if (bidBlack.Any())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            //有在白名單(商家)且不在黑名單(母子檔)
            List<ChannelCommissionList> sellerWhite = cp.ChannelCommissionListGetBySeller((int)channel, 2).Where(x => x.SellerGuid == dealGuid.SellerGuid).ToList();
            if (sellerWhite.Any())
            {
                List<ChannelCommissionList> mainBidBlack = cp.ChannelCommissionListGetByMainDeal((int)channel, 1).Where(x => x.MainBid == dealGuid.MainBid).ToList();
                List<ChannelCommissionList> subBlack = cp.ChannelCommissionListGetBySubDeal((int)channel, 1).Where(x => x.Bid == dealGuid.BusinessHourGuid).ToList();
                List<ChannelCommissionList> bidBlack = cp.ChannelCommissionListGetByDeal((int)channel, 1).Where(x => x.Bid == dealGuid.BusinessHourGuid).ToList();

                if (mainBidBlack.Any() || subBlack.Any() || bidBlack.Any())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }


            return isForcePass;


        }

        public static List<PiinLifeDescription> GetPiinLifeDescription(string remark)
        {
            List<PiinLifeDescription> description = new List<PiinLifeDescription>();
            string[] details = string.IsNullOrEmpty(remark) ? new string[0] : remark.Split(new string[] { "||" }, StringSplitOptions.None);
            if (details.Length >= 5)
            {
                for (int i = 3; i < details.Length; i++)
                {
                    string[] content = details[i].Split(new string[] { "##" }, StringSplitOptions.None);
                    if (content.Length == 2 && !string.IsNullOrEmpty(content[0]))
                    {
                        if (!string.IsNullOrEmpty(content[1]))
                        {
                            description.Add(new PiinLifeDescription() { Title = (content[0] == "右邊" ? "優惠內容" : content[0]), Content = content[1] });
                        }
                    }
                }
            }

            return description;
        }

        public static string GetPiinLifeMenu(string remark)
        {
            if (!string.IsNullOrEmpty(remark))
            {
                string[] details = remark.Split(new string[] { "||" }, StringSplitOptions.None);
                if (details.Length >= 4)
                {
                    string[] content = details[3].Split(new string[] { "##" }, StringSplitOptions.None);
                    if (content.Length == 2 && !string.IsNullOrEmpty(content[0]))
                    {
                        if (content[0] == "右邊")
                        {
                            return content[1];
                        }
                    }
                }
            }
            return string.Empty;
        }

        public static string GetEGDescription(string remark)
        {
            if (!string.IsNullOrEmpty(remark))
            {
                string[] details = remark.Split(new string[] { "||" }, StringSplitOptions.None);
                if (details.Length >= 2)
                {
                    return details[2];
                }
            }
            return string.Empty;
        }

        public static List<string> GetDealLabelTagList(string labeltaglist, string customtag)
        {
            List<string> labeltag_list = new List<string>();
            if (!string.IsNullOrEmpty(labeltaglist))
            {
                int labeltag_id;
                string[] labeltag_split_list = labeltaglist.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in labeltag_split_list)
                {
                    if (int.TryParse(item, out labeltag_id))
                    {
                        labeltag_list.Add(SystemCodeManager.GetDealLabelCodeName(labeltag_id));
                    }
                }
            }
            if (!string.IsNullOrEmpty(customtag))
            {
                labeltag_list.Add(customtag);
            }

            return labeltag_list;
        }

        public static List<string> GetDealIconList(IViewPponDeal vpd)
        {
            List<string> deal_icon_list = new List<string>();

            TimeSpan soldEndDays = new TimeSpan(vpd.BusinessHourOrderTimeE.Ticks - DateTime.Now.Ticks);
            if (0 <= soldEndDays.TotalDays && soldEndDays.TotalDays < 1)
            {
                //檢查 一天內結檔=>最後一天
                deal_icon_list.Add("最後一天");
            }

            //排除公益檔次的破千破萬 Icon
            if (!Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
            {
                QuantityAdjustment adj = vpd.GetAdjustedOrderedQuantity();

                //熱銷 Icon
                if (adj.Quantity >= 1000)
                {
                    deal_icon_list.Add("熱銷");
                }
            }

            if (!string.IsNullOrEmpty(vpd.LabelIconList))
            {
                List<int> iconCodeIdList = vpd.LabelIconList.Split(',').Select(int.Parse).ToList();
                iconCodeIdList.Sort();
                foreach (var iconCodeId in iconCodeIdList)
                {
                    switch ((DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), iconCodeId))
                    {
                        case DealLabelSystemCode.ArriveIn24Hrs:
                            deal_icon_list.Add("24h到貨");
                            break;
                        case DealLabelSystemCode.ArriveIn72Hrs:
                            deal_icon_list.Add("72h到貨");
                            break;
                        case DealLabelSystemCode.CanBeUsedImmediately:
                            deal_icon_list.Add("即買即用");
                            break;
                        case DealLabelSystemCode.FiveStarHotel:
                        case DealLabelSystemCode.FourStarHotel:
                            break;
                        case DealLabelSystemCode.AppLimitedEdition:
                            deal_icon_list.Add("APP限定");
                            break;
                        case DealLabelSystemCode.PiinlifeValentinesEvent:
                            deal_icon_list.Add("情人專屬");
                            break;
                        case DealLabelSystemCode.PiinlifeFatherEvent:
                            deal_icon_list.Add("父親節精選");
                            break;
                        case DealLabelSystemCode.PiinlifeMoonEvent:
                            deal_icon_list.Add("賞悅中秋");
                            break;
                        case DealLabelSystemCode.PiinlifeRecommend:
                            deal_icon_list.Add("每月推薦");
                            break;
                    }
                }
            }

            return deal_icon_list;
        }

        public static bool IsNeedBooking(IViewPponDeal vpd)
        {
            List<Guid> bids = new List<Guid>();

            if (vpd.ComboDelas != null && vpd.ComboDelas.Any())
            {
                bids.AddRange(vpd.ComboDelas.Select(x => x.BusinessHourGuid).ToList());
            }
            bids.Add(vpd.BusinessHourGuid);

            return bids.Any(bid => ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true).BookingSystemType > 0);
        }

        #endregion 代銷

        /// <summary>
        /// 通知代銷商核銷/取消核銷
        /// </summary>
        /// <param name="type"></param>
        /// <param name="couponId"></param>
        /// <param name="couponCode"></param>
        public static void NotifyVerify(Guid? bid, Guid orderGuid, int couponId, int amount, NotifyVoucherStatus voucherStatus, 
            string sequenceNumber, string couponCode, Guid? verifiedStoreGuid)
        {
            try
            {
                OrderCorresponding oc = _orderProvider.OrderCorrespondingListGetByOrderGuid(orderGuid);
                if (oc.IsLoaded)
                {
                    if (oc.Type == (int)AgentChannel.PChome)
                    {
                        PChomeChannleVerifyNotify notify = new PChomeChannleVerifyNotify();
                        notify.VendorNo = _config.PChomeChannelVendorNo;
                        notify.OrderNo = oc.OrderId;
                        notify.VoucherUId = couponId.ToString();
                        notify.VoucherStatus = (int)voucherStatus;//1:已使用, 2:取消使用
                        notify.RemainAmount = amount;

                        string macCode = _config.PChomeChannelVendorNo + oc.OrderId + couponId.ToString() + ((int)voucherStatus).ToString() + amount + _config.PChomeChannelVendorNo;
                        notify.MacCode = Security.GetSHA1Hash(macCode);
                        PChomeChannelAPI.NotifyVerify(notify);
                    }
                    else if (oc.Type == (int)AgentChannel.LifeEntrepot)
                    {
                        string verifiedStoreId = string.Empty;
                        if (bid != null)
                        {
                            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid.Value);
                            if(Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal))
                            {
                                DealProperty dp = pp.DealPropertyGet(bid.Value);
                                if (dp != null && dp.IsLoaded)
                                {
                                    if (dp.IsDepositCoffee)
                                    {
                                        //全家咖啡寄杯
                                        Peztemp peztemp = pp.PeztempGet(sequenceNumber);
                                        if (peztemp != null)
                                        {
                                            LunchKingSite.Core.Models.Entities.FamilyNetPincode familyNetPincode = fami.GetVerifiedFamilyNetPincodeByPeztempId(peztemp.Id);
                                            if(familyNetPincode != null)
                                            {
                                                verifiedStoreId = familyNetPincode.StoreCode;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        LifeEntrepotApi.VerifyNotifity(couponId, sequenceNumber, couponCode, voucherStatus, verifiedStoreGuid, verifiedStoreId);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error("NotifyVerify Error:orderGuid=" + orderGuid.ToString() + "couponId=" + couponId.ToString(), ex);
            }

        }



        #region ODM (Skm..)

        #region seller role

        /// <summary>
        /// 清除新光權限快取
        /// </summary>
        public static void ResetSellerRole()
        {
            SellerTree.Clear();
            ParentSellerTree.Clear();
            SellerRoleAddOrUpdate();
        }

        /// <summary>
        /// 更新SellerTree
        /// </summary>
        private static void SellerRoleAddOrUpdate()
        {
            var sellerRoots = cp.ChannelOdmCustomerGetAllSellerRootGuid();
            foreach (var sg in sellerRoots)
            {
                List<SellerTreeModel> st = sp.SellerTreeGetListByRootGuid(sg).ToList();
                SellerTree.AddOrUpdate(sg, st, (k, oldValue) => st);
            }
        }

        /// <summary>
        /// 取得Seller Guid下所以子Seller Guid & store_status = Available
        /// </summary>
        /// <param name="parentSellerGuid"></param>
        /// <param name="rootSellerGuid"></param>
        /// <returns></returns>
        public static List<Guid> GetAvailableChildrenSellerGuid(Guid parentSellerGuid, Guid rootSellerGuid)
        {
            List<Guid> result = new List<Guid>();
            List<SellerTreeModel> treeCol = GetSellerTreeModel(rootSellerGuid);

            if (!treeCol.Any())
            {
                return result;
            }

            List<SellerTreeModel> parentTree = treeCol.Where(x => x.ParentSellerGuid == parentSellerGuid).ToList();
            if (!parentTree.Any())
            {
                return result;
            }

            foreach (var children in parentTree)
            {
                result.Add(children.SellerGuid);
                result.AddRange(GetAvailableChildrenSellerGuid(children.SellerGuid, rootSellerGuid));
            }

            return result;
        }

        public static List<Guid> GetSellerChildrenGuidList(Guid parentGuid, Guid rootGuid)
        {
            return ParentSellerTree.GetOrAdd(parentGuid, delegate
            {
                return GetAvailableChildrenSellerGuid(parentGuid, rootGuid);
            });
        }

        public static List<Guid> GetRootOneDownSeller(Guid rootGuid)
        {
            List<SellerTreeModel> childrenCol = GetSellerTreeModel(rootGuid);
            var result = childrenCol.Where(x => x.ParentSellerGuid == x.RootSellerGuid).ToList();
            return result.Any() ? result.Select(n => n.SellerGuid).ToList() : new List<Guid>();
        }

        private static List<SellerTreeModel> GetSellerTreeModel(Guid rootGuid)
        {
            return SellerTree.GetOrAdd(rootGuid, delegate
            {
                return sp.SellerTreeGetListByRootGuid(rootGuid).ToList();
            });
        }

        #endregion seller role

        public static IEnumerable<Guid> GetOdmSellerRoleGuidsByUserId(int userId, Guid rootGuid)
        {
            bool administrator = false;

            return GetOdmSellerRoleGuidsByUserId(userId, rootGuid, ref administrator);
        }

        public static IEnumerable<Guid> GetOdmSellerRoleGuidsByUserId(int userId, Guid rootGuid, ref bool administrator)
        {
            //check root exist.
            List<SellerTreeModel> rootSellerTree = GetSellerTreeModel(rootGuid);
            List<Guid> sellerGuids = new List<Guid>();
            ChannelOdmSellerRoleCollection sellerRoles = new ChannelOdmSellerRoleCollection();
            var accountUserName = MemberFacade.GetMember(userId).UserName.ToLower();
            if (accountUserName.IndexOf("skm", StringComparison.Ordinal) < 0 && accountUserName.IndexOf("@17life.com", StringComparison.Ordinal) > -1)
            {
                //17Life 帳號預設給予最高權限
                var odmRootGuids = cp.ChannelOdmCustomerGetAllSellerRootGuid();
                foreach (var root in odmRootGuids)
                {
                    sellerRoles = new ChannelOdmSellerRoleCollection
                    {
                        new ChannelOdmSellerRole
                        {
                            UserId = userId,
                            SellerGuid = root,
                            CreateUser = userId,
                            CreateTime = DateTime.Now
                        }
                    };
                }

                administrator = true;
            }
            else
            {
                sellerRoles = cp.ChannelOdmSellerRoleGetByUserId(userId);

                administrator = false;
            }


            foreach (var sellerRole in sellerRoles)
            {
                var seller = sp.SellerGet(sellerRole.SellerGuid);
                if (!seller.IsLoaded || seller.StoreStatus != (int)StoreStatus.Available)
                {
                    continue;
                }

                //若權限設定的seller guid 不屬於傳入的root tree 則略過
                if (sellerRole.SellerGuid != rootGuid && rootSellerTree.All(x => x.SellerGuid != sellerRole.SellerGuid))
                {
                    continue;
                }

                sellerGuids.Add(sellerRole.SellerGuid);
                sellerGuids.AddRange(GetSellerChildrenGuidList(sellerRole.SellerGuid, rootGuid));
            }

            return sellerGuids;
        }

        /// <summary>
        /// 取得擁有的Root底下第一層Seller權限
        /// </summary>
        /// <param name="sellerRoles"></param>
        /// <param name="rootSellerGuid"></param>
        /// <returns></returns>
        public static List<Guid> GetOwnOneDownSeller(List<Guid> sellerRoles, Guid rootSellerGuid)
        {
            //get all one down guid by root seller guid
            var oneDowns = GetRootOneDownSeller(rootSellerGuid);
            //get seller tree data by root
            List<SellerTreeModel> childrenCol = GetSellerTreeModel(rootSellerGuid);
            //get own tree parent guid
            List<Guid> parentGuids =
                childrenCol.Where(x => sellerRoles.Contains(x.SellerGuid)).Select(x => x.ParentSellerGuid).ToList();
            //將擁有的所有parent seller tree與 all one down 做交集比對
            return oneDowns.Intersect(parentGuids).ToList();
        }

        public static List<Guid> GetGrandparentsSellerGuids(List<Guid> sellerGuids, Guid rootSellerGuid)
        {
            return GetParentSellerTreeModel(sellerGuids, rootSellerGuid).Select(x => x.ParentSellerGuid).ToList();
        }

        public static List<SellerTreeModel> GetParentSellerTreeModel(List<Guid> sellerGuids, Guid rootSellerGuid)
        {
            List<SellerTreeModel> childrenCol = GetSellerTreeModel(rootSellerGuid);
            //取爸爸
            var parentGuids = childrenCol.Where(x => sellerGuids.Contains(x.ParentSellerGuid)).Select(x => x.ParentSellerGuid).ToList();
            return childrenCol.Where(x => parentGuids.Contains(x.SellerGuid)).ToList();
        }

        public static SellerTreeModel GetParentSeller(Guid sellerGuid, Guid rootSellerGuid)
        {
            List<SellerTreeModel> temp = GetParentSellerTreeModel(new List<Guid> { sellerGuid }, rootSellerGuid).ToList();
            if (temp.Any())
            {
                return temp.First();
            }
            return new SellerTreeModel();
        }

        #endregion ODM (Skm..)

        #region 台新商城

        public static List<TaishinMallBanner> GetTaishinMallBanner(bool isAll = false)
        {
            try
            {
                var tsmb = _sysp.SystemDataGet("TaishinMallBanner");
                if (tsmb == null || tsmb.Data == null)
                {
                    return new List<TaishinMallBanner>();
                }
                var js = isAll ?
                    jsonSerializer.Deserialize<List<TaishinMallBanner>>(tsmb.Data) :
                    jsonSerializer.Deserialize<List<TaishinMallBanner>>(tsmb.Data).Where(x => x.StartDate <= DateTime.Now
                                && x.EndDate >= DateTime.Now).ToList();
                return js;
            }
            catch
            {
                return new List<TaishinMallBanner>();
            }
        }

        #endregion

        #region MasterpassBankLog

        public static ViewMasterpassBankLogCollection GetMasterpassBankLogCol(int? bankId)
        {
            return cp.MasterpassBankLogCollectionGet(bankId);
        }

        #endregion

        #region Masterpass

        public static MasterpassCardInfoLog GetMasterpassCardInfoLog(int verifierCode, string authToken)
        {
            MasterpassCardInfoLog mpclog = _orderProvider.MasterpassCardInfoLogGet(verifierCode, authToken);
            mpclog.CardNumber = MemberFacade.DecryptCreditCardInfo(mpclog.CardNumber, mpclog.AuthToken.Substring(0, 16));
            return mpclog;
        }

        #endregion

        #region LineShoppingGuide

        public static List<LineProductCategory> GetLineProductCategoryList()
        {
            ApiCategoryTypeNode typeNode = PponDealPreviewManager.GetPponChannelCategoryTree();
            var resultList = new List<LineProductCategory>();
            foreach (var item in typeNode.CategoryNodes)
            {
                //取得第一層type6的子分類
                List<ApiCategoryNode> first = item.GetSubCategoryTypeNode(CategoryType.DealCategory);
                if (first.Any())
                {
                    foreach (var firstNode in first)
                    {
                        if (firstNode.Seq == -1) continue;
                        resultList.Add(new LineProductCategory(
                            firstNode.CategoryName, firstNode.CategoryId,
                            0, firstNode.ExistsDeal));
                        //取得第二層type6的子分類
                        List<ApiCategoryNode> second = firstNode.GetSubCategoryTypeNode(CategoryType.DealCategory);
                        if (second.Any())
                        {
                            foreach (var secondNode in second)
                            {
                                if (secondNode.Seq == -1) continue;
                                resultList.Add(new LineProductCategory(
                                    secondNode.CategoryName, secondNode.CategoryId,
                                    firstNode.CategoryId, firstNode.ExistsDeal));
                            }
                        }
                    }
                }
            }
            return resultList;
        }

        private static ApiCategoryTypeNode typeNode = PponDealPreviewManager.GetPponChannelCategoryTree();
        public static List<LineProductCategory> GetLineProductCategoryList2()
        {
            var result = new List<LineProductCategory>();
            if (typeNode.CategoryNodes.Any())
            {
                foreach (var node in typeNode.CategoryNodes)
                {
                    if (node.NodeDatas.Any())
                    {
                        foreach (var item in node.NodeDatas)
                        {
                            if (item.NodeType != CategoryType.DealCategory) continue;
                            TreeView(item, ref result);
                        }
                    }
                }
            }

            return result;
        }
        public static void TreeView(ApiCategoryTypeNode nodes, ref List<LineProductCategory> result)
        {

            if (nodes.CategoryNodes.Any())
            {
                foreach (var node in nodes.CategoryNodes)
                {

                    if (node.Seq == -1) continue;

                    List<int> parentIds = typeNode.GetCategoryPath(node.CategoryId);
                    int index = (parentIds.Count - 2);
                    int parentId = index == 0 ? 0 : parentIds[index];

                    result.Add(new LineProductCategory(node.CategoryName, node.CategoryId, parentId, node.ExistsDeal));

                    if (node.NodeDatas.Any())
                    {
                        foreach (var item in node.NodeDatas)
                        {
                            if (item.NodeType != CategoryType.DealCategory) continue;
                            TreeView(item, ref result);
                        }
                    }

                }
            }
        }

        #endregion

        public static string PChomeChannelEncryptStr(string plainStr)
        {
            Security PchomeChannelAES = new Security(SymmetricCryptoServiceProvider.AES,
                Encoding.ASCII.GetBytes(_config.PChomeChannelAESKey), Encoding.ASCII.GetBytes(_config.PChomeChannelAESIV), CipherMode.CBC, PaddingMode.PKCS7)
            {
                StringStyle = EncryptedStringStyle.Hex
            };

            if (string.IsNullOrEmpty(plainStr))
            {
                return string.Empty;
            }
            string encryptedStr = PchomeChannelAES.Encrypt(plainStr);
            return encryptedStr;
        }
        public static string PChomeChannelDecryptStr(string encryptedStr)
        {
            Security PchomeChannelAES = new Security(SymmetricCryptoServiceProvider.AES,
                Encoding.ASCII.GetBytes(_config.PChomeChannelAESKey), Encoding.ASCII.GetBytes(_config.PChomeChannelAESIV), CipherMode.CBC, PaddingMode.PKCS7)
            {
                StringStyle = EncryptedStringStyle.Hex
            };

            if (string.IsNullOrEmpty(encryptedStr))
            {
                return string.Empty;
            }
            string plainStr = PchomeChannelAES.Decrypt(encryptedStr);
            return plainStr;
        }

        public static bool IsPChomeChannelProd(Guid bid, out PchomeChannelProd pchomeProd)
        {
            pchomeProd = cp.PChomeChannelProdGetByBid(bid);
            return pchomeProd.IsLoaded;
        }

        public static string GetPChomeSlogan(string remark, DateTime useStartTime, DateTime useEndTime)
        {
            string slogan = "";
            string useStart = useStartTime < DateTime.Now ? "即日起" : useStartTime.ToString("yyyy/MM/dd");
            string useEnd = useEndTime.ToString("yyyy/MM/dd");

            slogan = ChannelFacade.GetEGDescription(remark);
            slogan += "<br /><span style=\"color: red; \">●兌換期限：" + useStart + " ~ " + useEnd + "</span>";
            slogan = slogan.Replace(System.Environment.NewLine, string.Empty);
            string line1 = slogan.Split("<br />")[0].ToString();
            if (line1.Contains("折價碼"))
            {
                //折價碼相關字眼不能帶入PChome
                slogan = slogan.Replace(line1 + "<br />", string.Empty);
            }

            return slogan;
        }

        public static bool SyncPChomePrice(PchomeChannelProd pchomeProd, decimal itemPrice, decimal itemOriPrice, out string errMessage)
        {
            errMessage = "";
            //同步價格
            ProdEditPrice paramEditPrice = new ProdEditPrice();
            ProdPrice paramPriceDetail = new ProdPrice();

            paramPriceDetail.M = itemOriPrice;
            paramPriceDetail.P = itemPrice;
            paramEditPrice.GroupId = pchomeProd.ProdGroupId;
            paramEditPrice.Price = paramPriceDetail;
            bool result = PChomeChannelAPI.EditPrice(new List<ProdEditPrice> { paramEditPrice });
            if (!result)
            {
                logger.Error("PChome SyncPChomePrice錯誤 bid=" + pchomeProd.Bid.ToString());
                errMessage = "PChome同步價格失敗\\n";
            }
            return result;
        }

        public static bool SyncPChomeEG(PchomeChannelProd pchomeProd, string remark, DateTime useStartTime, DateTime useEndTime, out string errMessage)
        {
            errMessage = "";
            //同步一姬(slogan)
            ProdEditDesc paramEditDesc = new ProdEditDesc();
            paramEditDesc.GroupId = pchomeProd.ProdGroupId;
            paramEditDesc.Slogan = ChannelFacade.GetPChomeSlogan(remark, useStartTime, useEndTime);
            bool result = PChomeChannelAPI.EditDesc(paramEditDesc);
            if (!result)
            {
                logger.Error("PChome SyncPChomeEG錯誤 bid=" + pchomeProd.Bid.ToString());
                errMessage = "PChome同步slogan(一姬說好康)失敗\\n";
            }
            return result;
        }

        public static bool SyncPChomeProd(PchomeChannelProd pchomeProd, string nick, out string errMessage)
        {
            errMessage = "";
            //同步銷售口號(Nick)
            PChomeETicketProd Eprod = PChomeChannelAPI.GetProd(pchomeProd.ProdId).FirstOrDefault();
            PChomeETicketProd paramEditProd = new PChomeETicketProd();
            paramEditProd.GroupId = pchomeProd.ProdGroupId;
            paramEditProd.Nick = nick;
            paramEditProd.StoreId = Eprod.StoreId;//照舊
            paramEditProd.isSingleton = Eprod.isSingleton;//照舊

            bool result = PChomeChannelAPI.EditProd(paramEditProd);
            if (!result)
            {
                logger.Error("PChome SyncPChomeProd錯誤 bid=" + pchomeProd.Bid.ToString());
                errMessage = "PChome同步銷售口號(短標)失敗\\n";
            }
            return result;
        }

        public static bool SyncPChomeQty(PchomeChannelProd pchomeProd, int qty, out string errMessage)
        {
            errMessage = "";
            //同步庫存
            ProdEditQry paramEditQty = new ProdEditQry();
            paramEditQty.Id = pchomeProd.ProdId;
            paramEditQty.Qty = qty;
            if (paramEditQty.Qty > 1200)
                paramEditQty.Qty = 1200;
            bool result = PChomeChannelAPI.EditQty(new List<ProdEditQry> { paramEditQty });
            if (!result)
            {
                logger.Error("PChome SyncPChomeQty錯誤 bid=" + pchomeProd.Bid.ToString());
                errMessage = "PChome同步庫存失敗\\n";
            }
            return result;
        }

        public static bool SyncPChomePic(PchomeChannelProd pchomeProd, string appDealPic, out string errMessage)
        {
            errMessage = "";
            //同步大圖
            if (string.IsNullOrEmpty(appDealPic))
            {
                logger.Error("PChome上傳大圖File錯誤,無大圖,bid=" + pchomeProd.Bid.ToString());
                errMessage = "PChome同步大圖失敗\\n";
                return false;
            }

            #region 上傳大圖
            List<UpdateFile> filePic;
            bool uploadPicResult;
            uploadPicResult = PChomeChannelAddProdJob.UploadPicFile(pchomeProd.Bid, appDealPic, out filePic);
            #endregion

            if (uploadPicResult)
            {
                ProdEditPic paramEditPic = new ProdEditPic();
                ProdPic pic = new ProdPic();
                pic._360x360 = filePic.Count > 0 ? filePic[0].PCPath : "";
                pic._120x120 = filePic.Count > 1 ? filePic[1].PCPath : "";

                paramEditPic.Id = pchomeProd.ProdId;
                paramEditPic.Pic = pic;
                bool result = PChomeChannelAPI.EditPic(new List<ProdEditPic> { paramEditPic });
                if (!result)
                {
                    logger.Error("PChome SyncPChomePic錯誤 bid=" + pchomeProd.Bid.ToString());
                    errMessage = "PChome同步大圖失敗\\n";
                }
                return result;
            }
            errMessage = "PChome同步大圖失敗\\n";
            return false;
        }

        public static bool SyncDesc(PchomeChannelProd pchomeProd, string description, string restrictions, string availability, out string errMessage)
        {
            errMessage = "";
            //同步文案(詳細介紹) + 權益說明
            ProdEditIntro paramEditIntro = new ProdEditIntro();
            List<IntroDetail> paramIntroDetailList = new List<IntroDetail>();

            #region 上傳圖文
            bool uploadIntroResult;
            uploadIntroResult = PChomeChannelAddProdJob.UploadIntroFile(pchomeProd.Bid, description, restrictions, availability, out paramIntroDetailList);
            #endregion

            if (uploadIntroResult)
            {
                paramEditIntro.GroupId = pchomeProd.ProdGroupId;
                paramEditIntro.Info = paramIntroDetailList;
                bool result = PChomeChannelAPI.EditIntro(paramEditIntro);
                if (!result)
                {
                    logger.Error("PChome SyncDesc錯誤 bid=" + pchomeProd.Bid.ToString());
                    errMessage = "PChome同步文案失敗\\n";
                }
                return result;
            }
            errMessage = "PChome同步文案失敗\\n";
            return false;
        }

        public static ChannelCouponType GetChannelCouponType(IViewPponDeal vpd) 
        {
            if (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon)) 
            {
                if (Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal)
                    && Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.PEZevent))
                {
                    return ChannelCouponType.FamiGroupCoupon;
                } 
                else if (Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.HiLifeDeal)
                  && Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.PEZevent))
                {
                    return ChannelCouponType.HiLifeGroupCoupon;
                }
                else if (Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.PEZevent)) 
                {
                    return ChannelCouponType.PezGroupCoupon;
                }
                else
                {
                    return ChannelCouponType.GroupCoupon;
                }
            }
            return ChannelCouponType.Default;
        }

        public static bool IsEnableChannelGroupCoupon(IViewPponDeal vpd, ChannelClientProperty ccp) 
        {
            if (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
            {
                if (ccp.FamiGroupCoupon //全家寄杯
                    && Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal)
                    && Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.PEZevent))
                {
                    return true;
                }
                else if (ccp.HiLifeGroupCoupon //萊爾富
                    && Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.HiLifeDeal)
                    && Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.PEZevent))
                {
                    return true;
                }
                else if (ccp.PezGroupCoupon  //自訂序號寄杯 (ex:丹堤)
                    && Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.PEZevent))
                {
                    return true;
                }
                else if (ccp.GroupCoupon) //Default 成套票券
                {
                    return true;
                }
            }
            return false;
        }
    }
}
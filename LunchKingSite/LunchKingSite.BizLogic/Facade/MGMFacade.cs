﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.MGM;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Models.MGM;
using System.Transactions;
using log4net;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.ModelCustom;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Imaging;
using com.google.zxing;
using com.google.zxing.common;
using System.IO;
using System.Drawing.Drawing2D;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.BizLogic.Models.API;

namespace LunchKingSite.BizLogic.Facade
{
    public class MGMFacade
    {
        private static IMGMProvider mgmp;
        private static IMemberProvider mp;
        private static ISysConfProvider config;
        private static IPponProvider pp;
        private static IOrderProvider op;
        private static IFamiportProvider fami;
        private static IPponProvider pponProvider;
        private static IHiLifeProvider hiLife;
        private static ILog logger = LogManager.GetLogger("MGMFacade");
        const string MGMEnabledSessionKey = "LunchKingSite.BizLogic.Facade.MGMFacade.MGMEnabledSessionKey";

        static MGMFacade()
        {
            mgmp = ProviderFactory.Instance().GetProvider<IMGMProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
            pponProvider = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
            hiLife = ProviderFactory.Instance().GetProvider<IHiLifeProvider>();
        }


        /// <summary>
        /// 判斷MGM功能是否啟用
        /// </summary>
        public static bool IsEnabled
        {
            get
            {
                if (ProviderFactory.Instance().GetConfig().MGMEnabled)
                {
                    return true;
                }
                if (HttpContext.Current != null && HttpContext.Current.Session != null &&
                    string.Equals(HttpContext.Current.Session[MGMEnabledSessionKey], "1"))
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// 暫時的開啟MGM測試功能
        /// 如果小web.config裡的設定已開啟，則這邊是否呼叫都不影響
        /// http://localhost/WebService/systemservice.asmx/Mgm?enable=1
        /// </summary>
        /// <param name="enabled"></param>
        public static void EnableModuleTemporarily(bool enabled)
        {
            if (enabled)
            {
                HttpContext.Current.Session[MGMEnabledSessionKey] = "1";
            }
            else
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpContext.Current.Session.Remove(MGMEnabledSessionKey);
                }
            }
        }


        #region 送禮

        /// <summary>
        /// 取得訂單列表
        /// </summary>
        /// <returns></returns>
        public static List<SendGiftData> GetGiftOrderListByUser(int userId)
        {
            var data = mp.GetGiftOrderListByUser(userId).OrderByDescending(x => x.CreateTime);
            var rtnData = new List<SendGiftData>();
            foreach (var d in data)
            {
                var orderTimeE = d.ChangedExpireDate ?? d.BusinessHourDeliverTimeE;
                var giftCount = mgmp.SendGiftCountGetByOrderGuid(d.Guid);
                var tmp = new SendGiftData();

                #region 檔次圖示

                string imagePathData = d.ImagePath;
                //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                if ((d.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                {
                    //多檔次子檔訂單需取得主檔的圖片
                    CouponEventContent mainDealContent = pponProvider.CouponEventContentGetBySubDealBid(d.BusinessHourGuid);
                    if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                    {
                        imagePathData = mainDealContent.ImagePath;
                    }
                }
                string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);
                #endregion
                //成套取母檔標頭
                var mainDeal = pp.GetViewComboDealAllByBid(d.BusinessHourGuid);
                tmp.GiftData = new GiftData();
                tmp.GiftData.Bid = d.BusinessHourGuid;
                tmp.GiftData.Name = d.Name;
                tmp.GiftData.OrderTimeE = orderTimeE.Value;
                tmp.GiftData.ItemName = mainDeal.Any() ? mainDeal.First().CouponUsage : d.ItemName;
                tmp.ItemOrigPrice = d.ItemOrigPrice;
                tmp.ItemPrice = d.ItemPrice;
                tmp.OrderId = d.OrderId;
                tmp.OrderAmount = d.Total;
                tmp.DiscountString = (d.ItemPrice / d.ItemOrigPrice * 10).ToString("f1") + "折";
                tmp.RemainCount = giftCount;
                tmp.GiftData.PicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                tmp.GiftData.AppPicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                rtnData.Add(tmp);
            }
            return rtnData;
        }


        public static bool GiftMessageSet(int sendId, int receiverId, string message,
            GiftMessageType messageType, DateTime sendTime)
        {
            MemberMessage memberMsg = new MemberMessage()
            {
                //SenderName = giftCard.SenderName, //屬名
                MessageType = (int)messageType,
                ReceiverId = receiverId,
                SenderId = sendId,
                Message = message,
                IsRead = false,
                SendTime = sendTime,
            };

            return mp.MemberMsgSet(memberMsg);
        }

        /// <summary>
        /// 建立寄送卡片、訊息 
        /// </summary>
        /// <param name="giftCard"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public static bool CreateGift(ref GiftCard giftCard, ref string errorMsg)
        {
            bool isComplete = false;

            #region 檢查憑證

            if (!giftCard.MatchMemberGroup.Any())
            {
                errorMsg = GiftStatus.NoneMember.GetAttributeOfType<DescriptionAttribute>().Description;
                return isComplete;
            }

            int total = giftCard.MatchMemberGroup.Sum(x => x.Count); //取得送禮名單總數
            Guid orderGuid = op.OrderGuidGetById(giftCard.OrderId); //取得order guid
            var activeCoupons = mgmp.ActiveGiftGetByOrderGuid(orderGuid)
                .Take(total).ToList(); //撈出可用的憑證

            if (!activeCoupons.Any())
            {
                errorMsg = GiftStatus.NoneCopon.GetAttributeOfType<DescriptionAttribute>().Description;
                return isComplete;
            }

            if (total > activeCoupons.Count) //發送名單數大於可用憑證數
            {
                errorMsg = GiftStatus.OverCoponCount.GetAttributeOfType<DescriptionAttribute>().Description;
                return isComplete;
            }
            #endregion

            //成套取母檔標頭
            var mainDeal = pp.GetViewComboDealAllByBid(activeCoupons[0].BusinessHourGuid.Value);
            giftCard.Deal =
                ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(activeCoupons[0].BusinessHourGuid.Value);
            giftCard.CouponUsage = mainDeal.Any() ? mainDeal.First().CouponUsage : giftCard.Deal.ItemName;

            try
            {
                using (var tran = TransactionScopeBuilder.CreateReadCommitted())
                {
                    int index = 0;
                    DateTime snedTime = DateTime.Now;
                    foreach (var member in giftCard.MatchMemberGroup) // 多人發送
                    {
                        #region 建立禮物

                        //member_message 
                        MemberMessage memberMsg = new MemberMessage()
                        {
                            SenderId = giftCard.SenderId,
                            SenderName = giftCard.SenderName, //屬名
                            MessageType = (int)GiftMessageType.Send,
                            Message = giftCard.Message,
                            TemplateId = giftCard.CardStyleId,
                            IsRead = false,
                            SendTime = snedTime,
                            IsDeleted = false
                        };
                        mp.MemberMsgSet(memberMsg);

                        //群組禮物資料建立
                        var accessCode = Guid.NewGuid();  //產生禮物accessCode
                        var sequenceNumber = accessCode.ToString().Substring(0, 5);

                        for (int giftIndex = 0; giftIndex < member.Count; giftIndex++) //幾份禮物
                        {
                            MgmGift gift = new MgmGift()
                            {
                                IsUsed = false,
                                OrderGuid = activeCoupons[index].OrderGuid,
                                CouponId = activeCoupons[index].CouponId.Value,
                                Bid = activeCoupons[index].BusinessHourGuid ?? Guid.Empty,
                                Guid = Guid.NewGuid(),
                                AccessCode = accessCode,
                                SequenceNumber = sequenceNumber
                            };
                            mgmp.MgmGiftSet(gift);


                            //mgm_gift_version 如果gift已存在DB，giftId一樣
                            MgmGiftVersion giftGiftVersion = new MgmGiftVersion()
                            {
                                MgmGiftId = gift.Id,
                                MgmMatchId = 0,
                                ReceiverName = member.Name,
                                SendMessageId = memberMsg.Id,
                                SenderId = giftCard.SenderId,
                                SendTime = snedTime,
                                Accept = 0,
                                IsActive = true
                            };

                            mgmp.MgmGiftVersionSet(giftGiftVersion);

                            //mgm_gift_match
                            MgmGiftMatch giftmatch = new MgmGiftMatch()
                            {
                                MatchType = member.Type, // 0:email,1:mobile,2:fb
                                MatchValue = member.Value,
                                MgmGiftVersionId = giftGiftVersion.Id
                            };
                            mgmp.MgmGiftMatchSet(giftmatch);

                            //update mgm_gift_version.MgmMatchId
                            giftGiftVersion.MgmMatchId = giftmatch.Id;
                            mgmp.MgmGiftVersionSet(giftGiftVersion);

                            //coupon lock 發送禮物後，該憑證lock住無法被核銷
                            var couponId = activeCoupons[index].CouponId;
                            if (couponId != null)
                            {
                                Coupon cp = pp.CouponGet(couponId.Value);
                                cp.Status = (int)CouponStatus.Locked;
                                pp.CouponSet(cp);
                            }
                            index += 1;
                        }

                        //如果已經是本會員了 加一筆通知
                        int mUserId = 0;
                        if (IsMember(member, ref mUserId))
                        {

                            GiftMessageSet(giftCard.SenderId, mUserId, accessCode.ToString(),
                                GiftMessageType.GiftCode, DateTime.Now);

                            if (mUserId != giftCard.SenderId) //自己送自己就不加上訊息通知
                            {
                                GiftMessageCountSet(mUserId, (int)GiftMessageType.GiftCode);
                            }
                        }

                        //add access_code，等等發訊息用
                        member.AccessCode = accessCode.ToString();

                        #endregion
                    }

                    tran.Complete();
                    isComplete = true;
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }

            return isComplete;
        }

        /// <summary>
        /// 查看這fbUserId是否為會員
        /// </summary>
        /// <param name="fbUserId"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        public static bool IsMemeberByFb(string fbUserId, ref Member member)
        {
            MemberLink link = mp.MemberLinkGetByExternalId(fbUserId, SingleSignOnSource.Facebook);
            if (link.IsLoaded)  //判斷此fb會員是否已是本會員
            {
                Member mem = mp.MemberGet(link.UserId);
                //mem.DisplayName
                if (mem.IsLoaded)
                {
                    member = mem;
                    return true;
                }
            }
            return false;
        }


        /// <summary>
        /// 查詢email或手機是否為會員
        /// </summary>
        /// <param name="matchMember"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private static bool IsMember(MatchMemberGroup matchMember, ref int userId)
        {
            bool isCheck = false;
            Member m = null;
            switch (matchMember.Type)
            {
                case (int)MatchType.Email:
                    m = MemberFacade.GetMember(matchMember.Value);
                    if (m.IsLoaded)
                    {
                        isCheck = true;
                    }
                    break;

                case (int)MatchType.Mobile:
                    m = MemberFacade.GetMember(matchMember.Value);
                    if (m.IsLoaded)
                    {
                        isCheck = true;
                    }
                    break;
            }

            if (m != null)
            {
                userId = m.UniqueId;
            }

            return isCheck;
        }

        #endregion

        #region 收禮

        /// <summary>
        /// 綁定禮物設定
        /// </summary>
        /// <returns></returns>
        public static bool GiftBinding(MgmGiftVersion giftVersion, MemberMessage asMsg, int userId)
        {
            if (giftVersion.ReceiverId != null)
            {
                return false;
            }

            //擋自己送給自己，不給歸戶
            if (userId == giftVersion.SenderId)
            {
                return false;
            }

            giftVersion.ReceiverId = userId;
            mgmp.MgmGiftVersionSet(giftVersion);
            if (asMsg.Id != 0)
            {
                asMsg.IsRead = true;
                asMsg.ReadTime = DateTime.Now;
                asMsg.ReceiverId = userId;
                mgmp.MemberMessageSet(asMsg);
            }
            var Msg = mgmp.GiftCardGetById(giftVersion.SendMessageId);
            if (Msg.Id != 0)
            {
                Msg.ReceiverId = userId;
                Msg.IsRead = false;
                mgmp.MemberMessageSet(Msg);
            }
            return true;
        }

        /// <summary>
        /// 領取禮物設定
        /// </summary>
        /// <returns></returns>
        public static int GiftUserIdSet(string asCode, int userId)
        {
            int tmpMsg = 0;
            var giftDatalist = mgmp.GiftGetByAccessCode(asCode);
            foreach (var giftData in giftDatalist)
            {
                var giftVersion = mgmp.GiftVersionGetByGiftiId(giftData.Id);
                var msg = mgmp.GiftMsgByAccessCode(asCode);
                if (GiftBinding(giftVersion, msg, userId))
                {
                    tmpMsg = giftVersion.MgmGiftId;
                }

            }
            return tmpMsg;
        }

        /// <summary>
        /// 領取禮物設定
        /// </summary>
        /// <returns></returns>
        public static int GiftUserIdSet(MemberMessage msg, int userId)
        {
            int tmpMsg = 0;
            var giftDatalist = mgmp.GiftGetByAccessCode(msg.Message.Trim());
            foreach (var giftData in giftDatalist)
            {
                var giftVersion = mgmp.GiftVersionGetByGiftiId(giftData.Id);
                if (GiftBinding(giftVersion, msg, userId))
                {
                    tmpMsg = giftVersion.MgmGiftId;
                }
            }
            return tmpMsg;
        }

        /// <summary>
        /// 取得禮物
        /// </summary>
        /// <returns></returns>
        public static MemberFastInfo MemberFastInfoGetUserId(int userId)
        {
            return mgmp.MemberFastInfoGetByUserId(userId);
        }

        /// <summary>
        /// 取得禮物明細
        /// </summary>
        /// <returns></returns>
        public static ViewMgmGiftCollection GiftViweGetByAsCode(string asCode, int userId)
        {
            return mgmp.GiftViweGetByAsCode(asCode, userId);
        }

        /// <summary>
        /// FB取得禮物，條件:requestId+userId
        /// </summary>
        /// <param name="recipientUserId">{requestObjectId}_{userId}</param>
        /// <returns></returns>
        public static ViewMgmGift GiftGetByFbRecipientId(string recipientUserId)
        {
            ViewMgmGift gift = null;
            int matchId = mgmp.MgmGiftMatchIdGet(recipientUserId);
            if (matchId != 0)
            {
                gift = mgmp.GiftGetByGetByMatchId(matchId);
            }

            return gift;
        }

        /// <summary>
        /// FB取得禮物，條件:requestId
        /// </summary>
        /// <param name="requestId">{requestObjectId}</param>
        /// <returns></returns>
        public static ViewMgmGift GiftGetByFbRequestId(string requestId)
        {
            ViewMgmGift gift = null;
            int matchId = mgmp.MatchIdGetByContainsRequestId(requestId);
            if (matchId != 0)
            {
                gift = mgmp.GiftGetByGetByMatchId(matchId);
            }

            return gift;
        }

        /// <summary>
        /// 取得禮物序號
        /// </summary>
        /// <returns></returns>
        public static MemberMessageCollection GiftAccessGetByUserId(int userId)
        {
            return mgmp.GiftAccessGetByUserId(userId);
        }

        /// <summary>
        /// 取得禮物列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static List<GiftListData> GetGiftByReceiverIdAndSellerGuid(int userId, Guid sellerGuid)
        {
            var giftCol = mgmp.GiftGetByReceiverIdAndSellerGuid(userId, sellerGuid);
            return GetGiftListDataFromViewMgmGiftCol(giftCol);
        }

        /// <summary>
        /// By商家查詢禮物列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sellerGuid"></param>
        /// <param name="reverseVerifyOnly"></param>
        /// <returns></returns>
        public static List<ApiOrderByVerifyStore> GetGiftOrdersBySellerGuid(int userId, Guid sellerGuid, bool reverseVerifyOnly = false)
        {
            var giftOrders = new List<ApiOrderByVerifyStore>();
            var viewMgmGiftCol = mgmp.GiftGetByReceiverIdAndSellerGuid(userId, sellerGuid);
            List<GiftListData> giftData = GetGiftListDataFromViewMgmGiftCol(viewMgmGiftCol);
            
            var giftDic = giftData
                .Select(x => x.AccessCode).Distinct().ToDictionary(
                accessCode => accessCode,
                accessCode => giftData.Where(x => x.AccessCode == accessCode).ToList()
                );
            
            foreach (var gift in giftDic)
            {
                GiftListData giftItem = gift.Value[0];
                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(giftItem.GiftData.Bid);
                if (vpd == null || (reverseVerifyOnly && vpd.VerifyActionType != (int)VerifyActionType.Reverse))
                { 
                    continue;
                }

                if (gift.Key != null)
                {
                    var count = gift.Value.Where(x => !x.IsUsed).ToList().Count;
                    var giftOrder = new ApiOrderByVerifyStore();
                    giftOrder.OrderGuidOrAccessCode = (Guid)gift.Key;
                    giftOrder.ItemName = giftItem.GiftData.ItemName;
                    giftOrder.MainImagePath = giftItem.GiftData.AppPicUrl;
                    giftOrder.UseEndDate = ApiSystemManager.DateTimeToDateTimeString(giftItem.GiftData.OrderTimeE);
                    giftOrder.CouponCount = count;
                    giftOrder.TotalCouponCount = gift.Value.Count;
                    giftOrder.IsGift = true;
                    giftOrder.GroupCouponAppChannel = vpd.GroupCouponAppStyle ?? 0;
                    giftOrders.Add(giftOrder);
                }
            }

            return giftOrders;
        }
        
        /// <summary>
        /// 取得禮物列表
        /// </summary>
        /// <returns></returns>
        public static List<GiftListData> GiftGetByReceiverId(int userId)
        {
            var data = mgmp.GiftGetByReceiverId(userId);
            return GetGiftListDataFromViewMgmGiftCol(data);
        }
        
        public static List<GiftListData> GetGiftListDataFromViewMgmGiftCol(ViewMgmGiftCollection data)
        {
            var rtnData = new List<GiftListData>();
            foreach (var d in data)
            {
                //成套取母檔標頭
                var mainDeal = pp.GetViewComboDealAllByBid(d.Bid);
                #region 檔次圖示

                string imagePathData = d.ImagePath;
                //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                if ((d.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                {
                    //多檔次子檔訂單需取得主檔的圖片
                    CouponEventContent mainDealContent = pponProvider.CouponEventContentGetBySubDealBid(d.Bid);
                    if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                    {
                        imagePathData = mainDealContent.ImagePath;
                    }
                }


                string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);
                #endregion

                var orderTimeE = d.ChangedExpireDate ?? d.BusinessHourDeliverTimeE;
                var tmp = new GiftListData();

                tmp.GiftData = new GiftData();
                tmp.GiftData.Bid = d.Bid;
                tmp.GiftData.Name = d.Name;
                tmp.GiftData.OrderTimeE = orderTimeE.Value;
                tmp.GiftData.ItemName = mainDeal.Any() ? mainDeal.First().CouponUsage : d.ItemName;
                tmp.GiftData.PicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                tmp.GiftData.AppPicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                tmp.GiftId = d.Id;
                tmp.SendTime = d.SendTime;
                tmp.Accept = d.Accept;
                tmp.AccessCode = d.AccessCode;
                tmp.IsUsed = d.IsUsed;
                tmp.ReplyMsgId = d.ReplyMessageId;
                tmp.SendMsgId = d.SendMessageId;
                tmp.SenderName = d.LastName + d.FirstName;

                rtnData.Add(tmp);
            }
            return rtnData;
        }

        /// <summary>
        /// 禮物去哪列表
        /// </summary>
        /// <returns></returns>
        public static List<GiftWhereData> GiftWhereGetBySenderId(int userId)
        {
            var rtnData = new List<GiftWhereData>();
            var dataList = mgmp.GiftGetBySenderId(userId).ToLookup(x => x.SendTime);
            foreach (var data in dataList)
            {
                int receiverCount = 0;
                int noReceiverCount = 0;
                var tmp = new GiftWhereData { GiftData = new GiftData() };
                var tmpMatchValue = "";

                foreach (var d in data)
                {
                    if (receiverCount == 0 && noReceiverCount == 0)
                    {
                        var orderTimeE = d.ChangedExpireDate ?? d.BusinessHourDeliverTimeE;
                        #region 檔次圖示

                        string imagePathData = d.ImagePath;
                        //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                        if ((d.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                        {
                            //多檔次子檔訂單需取得主檔的圖片
                            CouponEventContent mainDealContent = pponProvider.CouponEventContentGetBySubDealBid(d.Bid);
                            if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                            {
                                imagePathData = mainDealContent.ImagePath;
                            }
                        }


                        string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);
                        #endregion
                        //成套取母檔標頭
                        var mainDeal = pp.GetViewComboDealAllByBid(d.Bid);
                        tmp.GiftData.Bid = d.Bid;
                        tmp.GiftData.Name = d.Name;
                        tmp.GiftData.OrderTimeE = orderTimeE.Value;
                        tmp.GiftData.ItemName = mainDeal.Any() ? mainDeal.First().CouponUsage : d.ItemName;
                        tmp.GiftData.PicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                        tmp.GiftData.AppPicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                        tmp.OrderId = pp.ViewPponOrderGet(d.OrderGuid).OrderId;
                        tmp.OrderGuid = d.OrderGuid;
                        tmp.SendTime = d.SendTime;
                        tmp.ReceiveTime = d.SendTime.AddDays(config.MGMGiftLockDay);
                    }

                    var matchValue = mgmp.GiftMatchGetById(d.MgmMatchId).MatchValue;

                    //接收者小於三則顯示名稱
                    if (receiverCount <= 3)
                    {
                        if (tmpMatchValue != matchValue)
                        {
                            if (receiverCount == 0)
                            {
                                tmp.ReceiverStr = d.ReceiverName;
                            }
                            else
                            {
                                tmp.ReceiverStr = tmp.ReceiverStr + "、" + d.ReceiverName;
                            }
                        }
                    }

                    if (tmpMatchValue != matchValue)
                    {
                        if (d.Accept != 1)
                        {
                            noReceiverCount++;
                        }
                        receiverCount++;
                        tmpMatchValue = matchValue;
                    }
                }
                //接收者大於三則顯示前三人+數量
                if (receiverCount > 3)
                {
                    tmp.ReceiverStr = tmp.ReceiverStr + "等" + receiverCount + "人";
                }
                tmp.NotReceiveCount = noReceiverCount;
                rtnData.Add(tmp);
            }

            return rtnData;
        }

        /// <summary>
        /// 禮物去哪明細
        /// </summary>
        /// <returns></returns>
        public static WhereItemList GiftWhereItemGetByOid(Guid oid, DateTime sendTime)
        {
            var rtnData = new WhereItemList();
            rtnData.GiftData = new GiftData();
            rtnData.ItemList = new List<WhereItem>();
            var tmpData = mgmp.GiftGetByOid(oid).Where(x => x.SendTime > sendTime && x.SendTime < sendTime.AddMilliseconds(999));
            var dataList = new List<WhereItem>();

            foreach (var d in tmpData)
            {
                if (dataList.Count == 0)
                {
                    var orderTimeE = d.ChangedExpireDate ?? d.BusinessHourDeliverTimeE;
                    var orderData = mp.GetCouponListMainByOid(d.OrderGuid);
                    #region 檔次圖示

                    string imagePathData = d.ImagePath;
                    //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                    if ((d.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                    {
                        //多檔次子檔訂單需取得主檔的圖片
                        CouponEventContent mainDealContent = pponProvider.CouponEventContentGetBySubDealBid(d.Bid);
                        if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                        {
                            imagePathData = mainDealContent.ImagePath;
                        }
                    }
                    string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);

                    #endregion
                    //成套取母檔標頭
                    var mainDeal = pp.GetViewComboDealAllByBid(d.Bid);
                    rtnData.GiftData.Name = d.Name;
                    rtnData.GiftData.ItemName = mainDeal.Any() ? mainDeal.First().CouponUsage : d.ItemName;
                    rtnData.GiftData.PicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                    rtnData.GiftData.AppPicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                    rtnData.GiftData.Bid = d.Bid;
                    rtnData.GiftData.OrderTimeE = orderTimeE.Value;
                    rtnData.GiftId = d.Id;
                    rtnData.SenderName = d.FirstName;
                    rtnData.SendMsgId = d.SendMessageId;
                    rtnData.ItemPrice = orderData.ItemPrice;
                    rtnData.ItemOrigPrice = orderData.ItemOrigPrice;
                    rtnData.DiscountString = (rtnData.ItemPrice / rtnData.ItemOrigPrice * 10).ToString("f1");
                }

                var tmp = new WhereItem();
                var tmpVersion = mgmp.GiftVersionGetByMsgId(d.SendMessageId);
                tmp.Accept = d.Accept;
                tmp.ReceiveTime = mgmp.GiftCardGetById(d.SendMessageId).ReadTime;
                tmp.ReceiverName = d.ReceiverName;
                tmp.ReplyMsgId = d.ReplyMessageId;
                tmp.UsedTime = d.UsedTime;
                tmp.CouponId = d.CouponId;
                tmp.Code = d.Code;
                tmp.CouponSequence = d.CouponSequenceNumber;
                tmp.MatchValue = mgmp.GiftMatchGetById(tmpVersion.First().MgmMatchId).MatchValue;
                dataList.Add(tmp);
            }
            rtnData.ItemList = dataList;

            return rtnData;
        }

        /// <summary>
        /// 憑證列表頁取得憑證可用數量
        /// </summary>
        /// <returns></returns>
        public static int GetGiftCountByOrderId(Guid oid)
        {
            return mgmp.SendGiftCountGetByOrderGuid(oid); ;
        }

        public static int GetMgmNewGiftCountByReceiverId(int userId)
        {
            return mgmp.MgmNewGiftCountGetByReceiverId(userId);
        }

        #endregion


        #region 卡片

        /// <summary>
        /// 取範例卡片
        /// </summary>
        /// <returns></returns>
        public static MemberMessageTemplateCollection MessageTemplateGet()
        {
            return mgmp.MessageTemplateGet();
        }

        /// <summary>
        /// 取卡片列表
        /// </summary>
        /// <returns></returns>
        public static List<GiftCardListData> CardListGet(int userId)
        {
            var data = mgmp.GiftCardListGetByUserId(userId);
            var cardList = new List<GiftCardListData>();
            var tmpData = data.Where(x => x.MessageType == (int)GiftMessageType.Send && x.SenderId == userId);
            var memberMessages = tmpData as IList<MemberMessage> ?? tmpData.ToList();
            foreach (var tmp in memberMessages.ToLookup(x => x.SendTime))
            {
                string name = "";
                bool tmpType = true;
                foreach (var d in tmp)
                {
                    if (d.ReceiverId == null)
                    {
                        var tmpVersion = mgmp.GiftVersionGetByMsgId(d.Id);
                        var tmpName = mgmp.GiftMatchGetById(tmpVersion.First().MgmMatchId).MatchValue;
                        name = name == "" ? name + tmpName : name + "、" + tmpName;
                    }
                    else
                    {
                        var tmpName = mp.MemberGetbyUniqueId(d.ReceiverId.Value).FirstName;
                        name = name == "" ? name + tmpName : name + "、" + tmpName;
                    }

                    var ReplyMsg = mgmp.ReplyMsgByMsgId(d.Id);
                    if (ReplyMsg != null && ReplyMsg.Id != 0)
                    {
                        tmpType = tmpType & ReplyMsg.ReadTime != null;
                    }

                }

                var tmpd = new GiftCardListData
                {
                    ImageType = mgmp.GiftCardTemplateGetById(tmp.First().TemplateId ?? 1).LogoPic,
                    IsRead = tmpType,
                    Message = tmp.First().Message,
                    MessageType = (int)GiftMessageType.Send,
                    ReceiverName = name,
                    SendTime = tmp.First().SendTime.Date.ToString("yyyy/MM/dd")
                };
                cardList.Add(tmpd);
            }
            tmpData = data.Where(x => x.MessageType == (int)GiftMessageType.Reply && x.ReceiverId == userId);
            foreach (var d in tmpData)
            {
                var tmpd = new GiftCardListData
                {
                    ImageType = mgmp.GiftCardTemplateGetById(d.TemplateId ?? 1).LogoPic,
                    IsRead = d.ReadTime != null,
                    Message = d.Message,
                    MessageType = (int)GiftMessageType.Reply,
                    SenderName = d.SenderName,
                    SendTime = d.SendTime.ToString("yyyy/MM/dd")
                };
                cardList.Add(tmpd);
            }
            return cardList;
        }

        /// <summary>
        /// 收禮者取禮物卡片
        /// </summary>
        /// <returns></returns>
        public static CardMessageData GiftCardGet(int userId, int msgId, int isRead, ref string errMsg)
        {
            var tmpData = mgmp.GiftCardGetById(msgId);
            var giftVersions = mgmp.GiftVersionGetByMsgId(tmpData.Id);

            #region 檢查卡片是否可被讀取

            if (userId != tmpData.SenderId && userId != tmpData.ReceiverId)
            {
                errMsg = "無法開啟卡片，目前登入帳號非該卡片擁有者";
                return new CardMessageData();
            }
            //是否被對方取消
            if (giftVersions.Any() && giftVersions.First().Accept == (int)MgmAcceptGiftType.Cancel)
            {
                errMsg = "卡片已被取消，無法讀取卡片";
                return new CardMessageData();
            }

            #endregion

            var tempCard = MessageTemplateGet().First(x => x.Id == tmpData.TemplateId);
            TemplateCard tempC = new TemplateCard()
            {
                Id = tempCard.Id,
                Title = tempCard.Title,
                ImagePic = config.SiteUrl + "/Themes/MGM/images/" + tempCard.LogoPic,
                BackgroundColor = tempCard.BackgroundColor
            };


            var cardData = new CardData
            {
                Id = tmpData.Id,
                ReceiverId = tmpData.ReceiverId,
                IsDeleted = tmpData.IsDeleted,
                IsRead = tmpData.IsRead,
                Message = tmpData.Message,
                MessageType = tmpData.MessageType,
                ReadTime = tmpData.ReadTime,
                ReceiveTime = tmpData.ReceiveTime,
                ReferMessageId = tmpData.ReferMessageId,
                ReferTargetGuid = tmpData.ReferTargetGuid,
                SendTime = tmpData.SendTime,
                SenderName = tmpData.SenderName,
                SenderId = tmpData.SenderId,
                TemplateCard = new TemplateCard()
            };

            cardData.TemplateCard = tempC;

            string receiveName = "";
            if (cardData.ReceiverId != null)
            {
                if (!giftVersions.Any())
                {
                    receiveName = mp.MemberGetbyUniqueId(cardData.ReceiverId.Value).FirstName;
                }
                else
                {
                    receiveName = giftVersions.First().ReceiverName;
                }
            }

            CardMessageData data = new CardMessageData
            {
                CardData = cardData,
                ReceiveName = receiveName
            };

            if (tmpData.ReadTime == null)
            {
                if (isRead == 1)
                {
                    if (tmpData.ReceiverId != null)
                    {
                        GiftMessageCountSet(tmpData.ReceiverId.Value, (int)GiftMessageType.Reply, true);
                        cardData.ReadTime = DateTime.Now;
                    }
                }
                cardData.IsRead = true;
                mgmp.MemberMessageSet(tmpData);
            }

            return data;
        }

        /// <summary>
        /// 送禮者看卡片
        /// </summary>
        /// <param name="senderId"></param>
        /// <param name="sendTime"></param>
        /// <returns></returns>
        public static CardMessageData GiftCardGetBySender(int senderId, DateTime sendTime)
        {
            var tmpData = mgmp.GiftCardListGetBySenderId(senderId).FirstOrDefault(x => x.SendTime > sendTime && x.SendTime < sendTime.AddMilliseconds(999));

            if (tmpData == null)
            {
                return new CardMessageData();
            }

            var tempCard = MessageTemplateGet().First(x => x.Id == tmpData.TemplateId);
            TemplateCard tempC = new TemplateCard()
            {
                Id = tempCard.Id,
                Title = tempCard.Title,
                ImagePic = config.SiteUrl + "/Themes/MGM/images/" + tempCard.LogoPic,
                BackgroundColor = tempCard.BackgroundColor
            };

            var cardData = new CardData
            {
                Id = tmpData.Id,
                ReceiverId = tmpData.ReceiverId,
                IsDeleted = tmpData.IsDeleted,
                IsRead = tmpData.IsRead,
                Message = tmpData.Message,
                MessageType = tmpData.MessageType,
                ReadTime = tmpData.ReadTime,
                ReceiveTime = tmpData.ReceiveTime,
                ReferMessageId = tmpData.ReferMessageId,
                ReferTargetGuid = tmpData.ReferTargetGuid,
                SendTime = tmpData.SendTime,
                SenderName = tmpData.SenderName,
                SenderId = tmpData.SenderId,
                TemplateCard = new TemplateCard()
            };

            cardData.TemplateCard = tempC;

            string receiveName = "";
            CardMessageData data = new CardMessageData
            {
                CardData = cardData,
                ReceiveName = receiveName
            };

            return data;
        }


        /// <summary>
        /// 取卡片明細
        /// </summary>
        /// <returns></returns>
        public static GiftCardListData CardItemGet(int msgId, int type)
        {
            var giftVersion = mgmp.GiftVersionGetByMsgId(msgId);
            if (giftVersion.Count > 0)
            {
                var tmpd = mgmp.GiftGetByGiftId(giftVersion.First().MgmGiftId);
            }

            var cardList = new List<GiftCardListData>();
            return new GiftCardListData();
        }

        /// <summary>
        /// 更新卡片資訊
        /// </summary>
        /// <returns></returns>
        public static bool UpDateGift(string asCode, int type, ref int error)
        {
            var giftList = mgmp.GiftGetByAccessCode(asCode);

            #region 檢查禮物是否可更新

            if (!giftList.Any())
            {
                return false;
            }

            var giftCheck = mgmp.GiftVersionGetByGiftiId(giftList.First().Id);
            // 下線的禮物 or 已被領取無法再次領取 or 快速領取
            if (!giftCheck.IsActive || giftCheck.Accept == (int)MgmAcceptGiftType.Accept || giftCheck.Accept == (int)MgmAcceptGiftType.Quick)
            {
                error = giftCheck.Accept;
                return false;
            }

            //TODO:要驗此asCode 的訂單是否跟是持有人可以取消。  驗version 中的reciverId 可否接受或拒絕
            #endregion

            var tmpData = new MgmGiftVersion();
            foreach (var gift in giftList)
            {
                var giftVersion = mgmp.GiftVersionGetByGiftiId(gift.Id);
                giftVersion.ReceiveTime = DateTime.Now;
                giftVersion.Accept = type;

                //active=0 
                if (type == (int)MgmAcceptGiftType.Reject || type == (int)MgmAcceptGiftType.Cancel)
                {
                    giftVersion.IsActive = false;
                }

                mgmp.MgmGiftVersionSet(giftVersion);
                tmpData = giftVersion;

                var couponId = gift.CouponId;
                if (couponId != 0)
                {
                    Coupon cp = pp.CouponGet(couponId);
                    cp.Status = (int)CouponStatus.None;
                    pp.CouponSet(cp);
                }
            }

            var cardData = mgmp.GiftCardGetById(tmpData.SendMessageId);
            cardData.ReadTime = DateTime.Now;
            mp.MemberMsgSet(cardData);
            if (tmpData.ReceiverId != null)
            {
                GiftMessageCountSet(tmpData.ReceiverId.Value, (int)GiftMessageType.Send, true);
            }


            return true;
        }

        /// <summary>
        /// 卡片回復
        /// </summary>
        /// <returns></returns>
        public static bool CardReply(int Msgid, string msg, string sendName, int cardType, int userId)
        {
            var giftVersionList = mgmp.GiftVersionGetByMsgId(Msgid);
            var rMsg = new MemberMessage();

            if (!giftVersionList.Any())
            {
                return false;
            }

            foreach (var giftVersion in giftVersionList)
            {
                if (giftVersion.ReceiverId != userId)
                {
                    return false;
                }
                if (rMsg.Id == 0)
                {
                    rMsg.SenderId = userId;
                    rMsg.Message = msg;
                    rMsg.SendTime = DateTime.Now;
                    rMsg.MessageType = (int)GiftMessageType.Reply;
                    rMsg.TemplateId = cardType;
                    rMsg.ReceiverId = giftVersion.SenderId;
                    rMsg.SenderName = sendName;
                    rMsg.IsRead = false;
                    rMsg.ReferMessageId = Msgid;
                    GiftMessageCountSet(giftVersion.SenderId, (int)GiftMessageType.Reply);
                    mgmp.MemberMessageSet(rMsg);
                    //推播
                    NotificationFacade.PushMemberMessageForMessageCenter(
                        giftVersion.SenderId, "您的朋友 " + sendName + "回覆您一張卡片囉!", cardType, rMsg.Id.ToString());
                }

                giftVersion.ReplyMessageId = rMsg.Id;
                giftVersion.ReceiveTime = DateTime.Now;
                mgmp.MgmGiftVersionSet(giftVersion);



            }

            return true;
        }

        #endregion


        #region Banner

        /// <summary>
        /// Banner訂單列表
        /// </summary>
        /// <returns></returns>
        public static List<BannerGiftData> BannerSendGiftByUser(int userId)
        {
            var data = mp.GetGiftOrderListByUser(userId).OrderByDescending(x => x.CreateTime);
            var rtnData = new List<BannerGiftData>();
            int count = 0;
            foreach (var d in data)
            {
                if (count == 2)
                {
                    break;
                }
                if (d.Name.Length > 50)
                {
                    d.Name = d.Name.Substring(0, 50) + "...";
                }
                var tmp = new BannerGiftData();
                tmp.OrderId = d.OrderId;
                tmp.Bid = d.BusinessHourGuid;
                tmp.PponCount = mgmp.SendGiftCountGetByOrderGuid(d.Guid);
                tmp.PponTitle = d.ItemName;
                tmp.PponContent = d.Name;
                tmp.PponImg = ImageFacade.GetMediaPathsFromRawData(d.ImagePath, MediaType.PponDealPhoto)
                        .DefaultIfEmpty(@"https://www.17life.com/Themes/default/images/17Life/G2/ppon-M1_pic.jpg").First();
                if (tmp.PponCount > 0)
                {
                    rtnData.Add(tmp);
                    count++;
                }

            }
            return rtnData;
        }

        /// <summary>
        /// Banner禮物列表
        /// </summary>
        /// <returns></returns>
        public static List<BannerGiftData> BannerGiftListByUser(int userId)
        {
            var rtnData = new List<BannerGiftData>();
            var data = mgmp.GiftGetByReceiverId(userId).Where(x => x.SendTime.AddDays(config.MGMGiftLockDay) > DateTime.Now && x.Accept == 0).ToLookup(x => x.AccessCode).ToDictionary(x => x.First(), x => x.Count()).Take(2);
            foreach (var d in data)
            {

                if (d.Key.Name.Length > 50)
                {
                    d.Key.Name = d.Key.Name.Substring(0, 50) + "...";
                }
                var tmp = new BannerGiftData();
                tmp.Bid = d.Key.Bid;
                tmp.GiftId = d.Key.Id;
                tmp.PponCount = mgmp.SendGiftCountGetByOrderGuid(d.Key.Guid);
                tmp.PponTitle = d.Key.ItemName;
                tmp.PponContent = d.Key.Name;
                tmp.SenderName = d.Key.LastName + d.Key.FirstName;
                tmp.SendTime = d.Key.SendTime;
                tmp.PponImg = ImageFacade.GetMediaPathsFromRawData(d.Key.ImagePath, MediaType.PponDealPhoto)
                                .DefaultIfEmpty(@"https://www.17life.com/Themes/default/images/17Life/G2/ppon-M1_pic.jpg").First();
                rtnData.Add(tmp);
            }

            return rtnData;
        }
        /// <summary>
        /// Banner卡片列表
        /// </summary>
        /// <returns></returns>
        public static List<GiftCardListData> BannerGiftCardByUser(int userId)
        {
            var data = mgmp.GiftCardListGetByUserId(userId);
            var tmpData = data.Where(x => x.MessageType == (int)GiftMessageType.Reply && x.ReceiverId == userId).Take(5);
            var cardList = new List<GiftCardListData>();
            foreach (var d in tmpData)
            {
                var tmpd = new GiftCardListData
                {
                    ImageType = mgmp.GiftCardTemplateGetById(d.TemplateId ?? 1).LogoPic,
                    IsRead = d.IsRead,
                    Message = d.Message,
                    MessageType = (int)GiftMessageType.Reply,
                    SenderName = d.SenderName,
                    SendTime = d.SendTime.ToString("yyyy/MM/dd")
                };
                cardList.Add(tmpd);
            }
            return cardList;
        }

        #endregion


        #region Api相關

        /// <summary>
        /// Api取得禮物明細
        /// </summary>
        /// <returns></returns>
        public static ApiGiftItem ApiGiftViweGetByAsCode(string asCode, int userId)
        {
            ApiGiftItem giftItem = new ApiGiftItem();
            var gift = mgmp.GiftViweGetByAsCode(asCode, userId);

            if (gift.Count <= 0)
            {
                return null;
            }

            CashTrustLogCollection cashTrustLogList = mp.CashTrustLogGetListByOrderGuid(gift.First().OrderGuid);

            Guid bid = Guid.Empty;
            if (cashTrustLogList.Count > 0)
            {
                var firstCoupon = cashTrustLogList.First();
                if (firstCoupon.BusinessHourGuid == null)
                {
                    return null;
                }
                else
                {
                    bid = firstCoupon.BusinessHourGuid.Value;
                }
            }

            Order order = op.OrderGet(gift.First().OrderGuid);

            //憑證資料
            ViewCouponListSequenceCollection couponList = mp.GetCouponListSequenceListByOid(gift.First().OrderGuid);

            CashTrustLogCollection returnFormCtlogs = mp.PponRefundingCashTrustLogGetListByOrderGuid(gift.First().OrderGuid);
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);

            int slug;
            if (!int.TryParse(vpd.Slug, out slug))
            {
                slug = 0;
            }

            #region CouponList

            var apiCouponList = new List<ApiCouponData>();
            ViewCouponListMain couponListMain = mp.GetCouponListMainByOid(gift.First().OrderGuid);
            //成套取母檔標頭
            var mainDeal = pp.GetViewComboDealAllByBid(couponListMain.BusinessHourGuid);

            //CouponType 
            giftItem.CouponType = (int)PponFacade.GetDealCouponType(couponListMain);

            #region 全家 Pincode Barcode

            var famiBarcodeInfoDic = new Dictionary<int, FamiUserCouponBarcodeInfo>();
            if (giftItem.CouponType == (int)DealCouponType.FamilyNetPincode || giftItem.CouponType == (int)DealCouponType.FamiSingleBarcode)
            {
                famiBarcodeInfoDic = FamiGroupCoupon.GetUserCouponBarcodeInfoListByOrderGuid(order.Guid);
            }

            #endregion
            foreach (ViewCouponListSequence coupon in couponList)
            {
                ApiCouponData apiCoupon = new ApiCouponData();

                if (gift.Any(x => x.CouponId == coupon.Id))
                {
                    DateTime now = DateTime.Now;
                    apiCoupon.CouponId = coupon.Id;
                    apiCoupon.ItemName = mainDeal.Any() ? mainDeal.First().CouponUsage : coupon.ItemName;
                    apiCoupon.SequenceNumber = coupon.SequenceNumber;
                    apiCoupon.CouponCode = coupon.Code;
                    if (apiCoupon.SequenceNumber == coupon.Code && (vpd.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) == 0)
                    {
                        apiCoupon.CouponCode = string.Empty;
                    }

                    if (coupon.StoreSequence == null)
                    {
                        apiCoupon.StoreSeq = string.Empty;
                    }
                    else
                    {
                        apiCoupon.StoreSeq = coupon.StoreSequence.Value.ToString();
                    }

                    #region 憑證狀態

                    if (coupon.BankStatus == 0)
                    {
                        apiCoupon.BankStatusDesc = "信託準備中";
                    }
                    else if (coupon.BankStatus == 1)
                    {
                        apiCoupon.BankStatusDesc = "已信託";
                    }
                    else if (coupon.BankStatus == 3)
                    {
                        apiCoupon.BankStatusDesc = "信託已核銷";
                    }


                    if (coupon.Status < 0)//no trust data
                    {
                        //沒有 cashTrustLog資料
                        if ((order.OrderStatus & (int)Core.OrderStatus.Cancel) > 0)
                        {
                            apiCoupon.CouponUsedType = ApiCouponUsedType.CouponRefund;
                            apiCoupon.CouponUsedTypeDesc = "已退貨";
                        }
                        else if (vpd.BusinessHourOrderTimeE < now)
                        {
                            //已結檔
                            if (slug >= vpd.BusinessHourOrderMinimum)
                            {
                                if (PponFacade.IsPastFinalExpireDate(coupon))
                                {
                                    apiCoupon.CouponUsedType = ApiCouponUsedType.CouponExpired;
                                    apiCoupon.CouponUsedTypeDesc = "憑證過期";
                                }
                                else
                                {
                                    apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                                    apiCoupon.CouponUsedTypeDesc = "未使用";
                                }
                            }
                            else
                            {
                                apiCoupon.CouponUsedType = ApiCouponUsedType.FailDeal;
                                apiCoupon.CouponUsedTypeDesc = "未達門檻";
                            }
                        }
                        //檔次未結束
                        else if ((vpd.BusinessHourOrderTimeS <= now) &&
                                 (vpd.BusinessHourOrderTimeE > now))
                        {
                            //目前銷售數量
                            int currentQuantity = vpd.OrderedQuantity == null ? 0 : vpd.OrderedQuantity.Value;

                            if (currentQuantity < vpd.BusinessHourOrderMinimum && currentQuantity != 0)
                            {
                                //未達門檻，販賣中
                                apiCoupon.CouponUsedType = ApiCouponUsedType.WaitToPass;
                                apiCoupon.CouponUsedTypeDesc = "";
                            }
                            else
                            {
                                apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                                apiCoupon.CouponUsedTypeDesc = "未使用";
                            }
                        }
                        else
                        {
                            apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                            apiCoupon.CouponUsedTypeDesc = "未使用";
                        }

                    }
                    else
                    {
                        if (Helper.IsFlagSet((long)coupon.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                        {
                            apiCoupon.CouponUsedType = ApiCouponUsedType.ATMRefund;
                            apiCoupon.CouponUsedTypeDesc = "ATM退款中";
                        }
                        else
                        {
                            //憑證未使用
                            if (CouponIsNotUsed((TrustStatus)coupon.Status))
                            {
                                //已結檔
                                if (vpd.BusinessHourOrderTimeE < now)
                                {
                                    if (slug >= vpd.BusinessHourOrderMinimum)
                                    {
                                        if (PponFacade.IsPastFinalExpireDate(coupon))
                                        {
                                            apiCoupon.CouponUsedType = ApiCouponUsedType.CouponExpired;
                                            apiCoupon.CouponUsedTypeDesc = "憑證過期";
                                        }
                                        else
                                        {
                                            apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                                            apiCoupon.CouponUsedTypeDesc = "未使用";
                                        }
                                    }
                                    else
                                    {
                                        apiCoupon.CouponUsedType = ApiCouponUsedType.FailDeal;
                                        apiCoupon.CouponUsedTypeDesc = "未達門檻";
                                    }
                                }
                                //檔次未結束
                                else if ((vpd.BusinessHourOrderTimeS <= now) &&
                                         (vpd.BusinessHourOrderTimeE > now))
                                {
                                    //檔次進行中，可以使用ViewPponDealManager來取得資料
                                    //ViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                                    //目前銷售數量
                                    int currentQuantity = vpd.OrderedQuantity == null
                                                              ? 0
                                                              : vpd.OrderedQuantity.Value;

                                    if (currentQuantity < vpd.BusinessHourOrderMinimum &&
                                        currentQuantity != 0)
                                    {
                                        //為達門檻
                                        apiCoupon.CouponUsedType = ApiCouponUsedType.WaitToPass;
                                        apiCoupon.CouponUsedTypeDesc = "";
                                    }
                                    else
                                    {
                                        apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                                        apiCoupon.CouponUsedTypeDesc = "未使用";
                                    }
                                }
                                else
                                {
                                    apiCoupon.CouponUsedType = ApiCouponUsedType.CanUsed;
                                    apiCoupon.CouponUsedTypeDesc = "未使用";
                                }

                            }
                            else if ((TrustStatus)coupon.Status == TrustStatus.Verified)
                            {
                                apiCoupon.UsageVerifiedTime =
                                    coupon.UsageVerifiedTime != null ? ApiSystemManager.DateTimeToDateTimeString((DateTime)coupon.UsageVerifiedTime)
                                    : string.Empty;
                                apiCoupon.CouponUsedType = ApiCouponUsedType.Used;
                                apiCoupon.CouponUsedTypeDesc = "已使用";
                            }
                            else
                            {
                                //憑證cashTrustLog狀態不為 已核銷 或 未使用 類型的其他狀態
                                if ((order.OrderStatus & (int)Core.OrderStatus.Cancel) > 0)
                                {
                                    apiCoupon.CouponUsedType = ApiCouponUsedType.CouponRefund;
                                    apiCoupon.CouponUsedTypeDesc = "已退貨";
                                }
                                else if ((TrustStatus)coupon.Status != TrustStatus.ATM)
                                {
                                    apiCoupon.CouponUsedType = ApiCouponUsedType.WaitForRefund;
                                    apiCoupon.CouponUsedTypeDesc = "退貨中";
                                }
                            }
                        }
                    }

                    //處理coupon.Status=0但是已經辦理退貨中
                    CashTrustLog returningCtlog = returnFormCtlogs.FirstOrDefault(x => x.CouponId == coupon.Id);
                    if (returningCtlog != null && apiCoupon.CouponUsedType == ApiCouponUsedType.CanUsed)
                    {
                        apiCoupon.CouponUsedType = ApiCouponUsedType.WaitForRefund;
                        apiCoupon.CouponUsedTypeDesc = "退貨中";
                    }

                    //全家四段式條碼及鎖定狀態
                    var userCouponBarcodeInfo = new FamiUserCouponBarcodeInfo();
                    if (giftItem.CouponType == (int)DealCouponType.FamilyNetPincode || giftItem.CouponType == (int)DealCouponType.FamiSingleBarcode)
                    {
                        if (famiBarcodeInfoDic.Any())
                        {
                            userCouponBarcodeInfo = famiBarcodeInfoDic[coupon.Id];
                        }
                    }

                    //全家寄杯憑證鎖定中
                    if (userCouponBarcodeInfo != null && userCouponBarcodeInfo.IsLock)
                    {
                        apiCoupon.CouponUsedType = ApiCouponUsedType.FamiLocked;
                        apiCoupon.CouponUsedTypeDesc = "資料處理中";
                    }

                    #endregion

                    //全家四段式條碼 & 一段碼 (在已使用或憑證鎖定中時才顯示)
                    if (userCouponBarcodeInfo != null
                        && (apiCoupon.CouponUsedType == ApiCouponUsedType.Used || apiCoupon.CouponUsedType == ApiCouponUsedType.FamiLocked))
                    {
                        if (apiCoupon.CouponUsedType == ApiCouponUsedType.FamiLocked && userCouponBarcodeInfo.LockTime != null)
                        {
                            apiCoupon.UsageVerifiedTime = ApiSystemManager.DateTimeToDateTimeString((DateTime)userCouponBarcodeInfo.LockTime);
                        }

                        var familyNetPincode = new LunchKingSite.BizLogic.Model.API.FamilyNetPincode();
                        familyNetPincode.Pincode = userCouponBarcodeInfo.Pincode;

                        if (giftItem.CouponType == (int)DealCouponType.FamilyNetPincode)
                        {
                            familyNetPincode.Barcode.Add(userCouponBarcodeInfo.Barcode1);
                            familyNetPincode.Barcode.Add(userCouponBarcodeInfo.Barcode2);
                            familyNetPincode.Barcode.Add(userCouponBarcodeInfo.Barcode3);
                            familyNetPincode.Barcode.Add(userCouponBarcodeInfo.Barcode4);
                        }
                        else if(giftItem.CouponType == (int)DealCouponType.FamiSingleBarcode)
                        {
                            familyNetPincode.Barcode.Add(userCouponBarcodeInfo.Barcode2);
                        }

                        familyNetPincode.UsageStoreName = userCouponBarcodeInfo.StoreName;
                        familyNetPincode.UsageStoreAddress = userCouponBarcodeInfo.StoreAddress;
                        apiCoupon.FamilyNetPincode = familyNetPincode;
                    }


                    //PEZ活動
                    if ((vpd.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0 
                        && !((vpd.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)
                        && !((vpd.BusinessHourStatus & (int)BusinessHourStatus.GroupCoupon) > 0))
                    {
                        apiCoupon.CouponUsedType = ApiCouponUsedType.SpecialCoupon;
                    }

                    //全家寄杯鎖定
                    if (fami.GetFamilyNetListPincode(gift.First().OrderGuid).Where(p => p.IsLock).Any(x => x.CouponId == coupon.Id))
                    {
                        apiCoupon.CouponUsedType = ApiCouponUsedType.FamiLocked;
                    }

                    //成套商品贈品
                    apiCoupon.IsGiveaway = false;

                    //是否可評價
                    apiCoupon.IsToEvaluate =
                        coupon.Status == (int)TrustStatus.Verified && //已核銷
                        mp.EvaluateGetMainID(mp.CashTrustLogGetByCouponId(coupon.Id).TrustId) <= 0 && //未評價
                        !(coupon.UsageVerifiedTime == null && coupon.UsageVerifiedTime.Value.AddDays(config.MGMGiftLockDay) < DateTime.Now); //未逾期

                    apiCouponList.Add(apiCoupon);
                }
            }

            giftItem.CouponList = apiCouponList;
            #endregion

            if (giftItem.CouponList.Count > 0)
            {
                OrderDetailCollection orderDetailList = op.OrderDetailGetList(gift.First().OrderGuid);


                Guid selectStoreGuid = Guid.Empty;
                if (orderDetailList.Count > 0)
                {
                    var storeGuid = orderDetailList.First().StoreGuid;
                    if (storeGuid != null)
                    {
                        selectStoreGuid = storeGuid.Value;
                    }
                }



                ViewPponStoreCollection pponStores;
                if (selectStoreGuid != null && selectStoreGuid != Guid.Empty)
                {
                    pponStores =
                        pp.ViewPponStoreGetListWithNoLock(ViewPponStore.Columns.BusinessHourGuid + " = " + bid,
                            ViewPponStore.Columns.StoreGuid + " = " + selectStoreGuid);
                }
                else
                {
                    pponStores = pp.ViewPponStoreGetListByBidWithNoLock(bid, VbsRightFlag.Location);
                }

                List<ApiPponStore> stores = new List<ApiPponStore>();
                foreach (var pponStore in pponStores)
                {
                    var store = new ApiPponStore
                    {
                        StoreName = pponStore.StoreName,
                        Phone = NoHTML(pponStore.Phone),
                        Address =
                            pponStore.TownshipId == null
                                ? ""
                                : CityManager.CityTownShopStringGet(pponStore.TownshipId.Value) +
                                  pponStore.AddressString,
                        OpenTime = pponStore.OpenTime,
                        CloseDate = pponStore.CloseDate,
                        Remarks = pponStore.Remarks,
                        Mrt = pponStore.Mrt,
                        Car = pponStore.Car,
                        Bus = pponStore.Bus,
                        OtherVehicles = pponStore.OtherVehicles,
                        WebUrl = pponStore.WebUrl,
                        FbUrl = pponStore.FacebookUrl,
                        PlurkUrl = pponStore.PlurkUrl,
                        BlogUrl = pponStore.BlogUrl,
                        OtherUrl = pponStore.OtherUrl
                    };
                    stores.Add(store);
                }

                giftItem.Id = gift.First().Id;
                giftItem.CouponName = mainDeal.Any() ? mainDeal.First().CouponUsage : gift.First().ItemName;
                giftItem.CouponBid = gift.First().Bid;
                giftItem.OrderGuid = gift.First().OrderGuid;

                #region CouponType、GroupCouponType

                List<NewCouponListFilterType> filterTypes = MemberFacade.GetViewCouponListMainGetCountNewFilterTypes(couponListMain);
                giftItem.GroupCouponType = (int)PponDealApiManager.GetGroupCouponType(filterTypes);
                
                #endregion

                #region 成套檔次圖示
                string imagePathData = couponListMain.ImagePath;
                //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                if ((couponListMain.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                {
                    //多檔次子檔訂單需取得主檔的圖片
                    CouponEventContent mainDealContent = pp.CouponEventContentGetBySubDealBid(couponListMain.BusinessHourGuid);
                    if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                    {
                        imagePathData = mainDealContent.ImagePath;
                    }
                }
                string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);
                giftItem.MainImagePath = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                giftItem.GroupCouponAppChannel = couponListMain.GroupCouponAppStyle; //成套商品App頻道

                #endregion

                if (vpd.BusinessHourDeliverTimeS != null)
                {
                    giftItem.UseStartDate = ApiSystemManager.DateTimeToDateTimeString((DateTime)vpd.BusinessHourDeliverTimeS);
                }
                if (vpd.BusinessHourDeliverTimeE != null)
                {
                    var deliverTimeE = vpd.ChangedExpireDate ?? vpd.BusinessHourDeliverTimeE;
                    giftItem.UseEndDate = ApiSystemManager.DateTimeToDateTimeString((DateTime)deliverTimeE);
                }

                giftItem.Availabilities = stores;
                giftItem.VerifyType = vpd.VerifyActionType ?? (int)VerifyActionType.None;

                return giftItem;
            }
            else
            {
                return null;
            }

        }

        /// <summary>
        /// Api取得禮物去哪列表
        /// </summary>
        /// <returns></returns>
        public static List<ApiGiftWhere> ApiGiftWhereGetBySenderId(int userId)
        {
            var rtnData = new List<ApiGiftWhere>();
            var dataList = mgmp.GiftGetBySenderId(userId).ToLookup(x => x.SendTime);
            foreach (var data in dataList)
            {
                int receiverCount = 0;
                var tmp = new ApiGiftWhere();
                tmp.GiftData = new GiftData();
                tmp.ReceiverList = new List<ApiReceive>();
                int tmpId = 0;
                var tmpMatchValue = "";

                foreach (var d in data)
                {
                    //成套取母檔標頭
                    var mainDeal = pp.GetViewComboDealAllByBid(d.Bid);
                    if (receiverCount == 0)
                    {
                        var orderTimeE = d.ChangedExpireDate ?? d.BusinessHourDeliverTimeE;
                        #region 檔次圖示

                        string imagePathData = d.ImagePath;
                        //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                        if ((d.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                        {
                            //多檔次子檔訂單需取得主檔的圖片
                            CouponEventContent mainDealContent = pponProvider.CouponEventContentGetBySubDealBid(d.Bid);
                            if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                            {
                                imagePathData = mainDealContent.ImagePath;
                            }
                        }


                        string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);
                        #endregion
                        tmp.GiftData.Bid = d.Bid;
                        tmp.GiftData.Name = d.Name;
                        tmp.GiftData.OrderTimeE = orderTimeE.Value;
                        tmp.GiftData.ItemName = mainDeal.Any() ? mainDeal.First().CouponUsage : d.ItemName;
                        tmp.GiftData.PicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                        tmp.GiftData.AppPicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                        tmp.OrderGuid = d.OrderGuid;
                        tmp.SendTime = d.SendTime;
                    }

                    if (d.ReceiverId != null)
                    {
                        if (tmpId != d.ReceiverId)
                        {
                            if (receiverCount <= 3)
                            {
                                var Receiverdata = new ApiReceive();
                                var tmpMember = mp.MemberGetbyUniqueId(d.ReceiverId.Value);
                                string name = tmpMember.LastName + tmpMember.FirstName;

                                if (string.IsNullOrEmpty(name))
                                {
                                    var mgmMatch = mgmp.GiftMatchGetById(d.MgmMatchId);
                                    if (mgmMatch.MatchType == (int)MatchType.Fb)
                                    {
                                        name = "FB的朋友";
                                    }
                                    else if (mgmMatch.MatchType == (int) MatchType.Line)
                                    {
                                        name = "Line的朋友";
                                    }
                                    else
                                    {
                                        name = "你的朋友";
                                    }
                                }

                                Receiverdata.Name = name;
                                Receiverdata.ImgUrl = MemberFacade.GetMemberPicUrl(tmpMember);
                                Receiverdata.AccountNumber = tmpMember.UserName.Contains("@mobile") == true ? tmpMember.Mobile : tmpMember.UserName;
                                tmp.ReceiverList.Add(Receiverdata);
                            }
                            receiverCount++;
                        }
                    }
                    else
                    {
                        var tmpVersion = mgmp.GiftVersionGetByMsgId(d.SendMessageId).First();
                        //如果是快速領取的話
                        if (d.Accept == 6)
                        {
                            if (tmpMatchValue != tmpVersion.QuickInfo)
                            {
                                if (receiverCount <= 3)
                                {
                                    var Receiverdata = new ApiReceive();
                                    Receiverdata.Name = GetMask(tmpVersion.QuickInfo, "*", true);
                                    Receiverdata.AccountNumber = GetMask(tmpVersion.QuickInfo, "*", true);
                                    tmp.ReceiverList.Add(Receiverdata);
                                }
                                receiverCount++;
                                tmpMatchValue = tmpVersion.QuickInfo;
                            }
                        }
                        else
                        {
                            var matchValue = mgmp.GiftMatchGetById(d.MgmMatchId).MatchValue;
                            if (tmpMatchValue != matchValue)
                            {
                                if (receiverCount <= 3)
                                {
                                    var Receiverdata = new ApiReceive();
                                    Receiverdata.Name = d.ReceiverName;
                                    Receiverdata.AccountNumber = matchValue;
                                    tmp.ReceiverList.Add(Receiverdata);
                                }
                                receiverCount++;
                                tmpMatchValue = matchValue;
                            }
                        }
                    }

                    tmpId = d.ReceiverId ?? 0;
                }

                tmp.ReceiveCount = receiverCount;

                rtnData.Add(tmp);
            }
            return rtnData;
        }

        /// <summary>
        /// Api取得禮物去哪列表明細
        /// </summary>
        /// <returns></returns>
        public static ApiGiftWhereItem ApiGiftWhereItemGetBySenderId(Guid oid, DateTime sendTime)
        {

            var rtnData = new ApiGiftWhereItem();
            var tmpData = mgmp.GiftGetByOid(oid).Where(x => x.SendTime >= sendTime && x.SendTime <= sendTime.AddMilliseconds(999));
            var dataList = new List<ApiWhereItem>();

            foreach (var d in tmpData)
            {
                if (dataList.Count == 0)
                {
                    //成套取母檔標頭
                    var mainDeal = pp.GetViewComboDealAllByBid(d.Bid);
                    var orderTimeE = d.ChangedExpireDate ?? d.BusinessHourDeliverTimeE;
                    var orderData = mp.GetCouponListMainByOid(d.OrderGuid);
                    #region 檔次圖示

                    string imagePathData = d.ImagePath;
                    //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                    if ((d.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                    {
                        //多檔次子檔訂單需取得主檔的圖片
                        CouponEventContent mainDealContent = pponProvider.CouponEventContentGetBySubDealBid(d.Bid);
                        if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                        {
                            imagePathData = mainDealContent.ImagePath;
                        }
                    }
                    string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);

                    #endregion
                    rtnData.GiftData.Name = d.Name;
                    rtnData.GiftData.ItemName = mainDeal.Any() ? mainDeal.First().CouponUsage : d.ItemName;
                    rtnData.GiftData.AppPicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                    rtnData.GiftData.PicUrl = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                    rtnData.GiftData.Bid = d.Bid;
                    rtnData.GiftData.OrderTimeE = orderTimeE.Value;
                    rtnData.GiftId = d.Id;
                    rtnData.SendMsgId = d.SendMessageId;
                    rtnData.ItemPrice = orderData.ItemPrice;
                    rtnData.ItemOrigPrice = orderData.ItemOrigPrice;
                    rtnData.DiscountString = (rtnData.ItemPrice / rtnData.ItemOrigPrice * 10).ToString("f1") + "折";
                }

                var tmp = new ApiWhereItem();
                var tmpVersion = mgmp.GiftVersionGetByMsgId(d.SendMessageId).First();

                //因為現在多了快速領取狀態為6，若不後端修正即需要前端接收修正
                tmp.Accept = d.Accept == 6 ? 1 : d.Accept;

                //如果是快速領取的話，直接抓取ReceiveTime
                if (d.Accept == 6)
                {
                    tmp.ReceiveTime = d.ReceiveTime;
                    tmp.Receive.Name = GetMask(tmpVersion.QuickInfo, "*", true);
                    tmp.Receive.AccountNumber= GetMask(tmpVersion.QuickInfo, "*", true);
                }
                else
                {
                    tmp.ReceiveTime = mgmp.GiftCardGetById(d.SendMessageId).ReadTime;
                    tmp.Receive.Name = tmpVersion.ReceiverName;
                    tmp.Receive.AccountNumber = mgmp.GiftMatchGetById(d.MgmMatchId).MatchValue;
                }

                if (d.ReceiverId != null)
                {
                    var tmpMember = mp.MemberGetbyUniqueId(d.ReceiverId.Value);
                    tmp.Receive.ImgUrl = MemberFacade.GetMemberPicUrl(tmpMember);

                    string name = tmpMember.LastName + tmpMember.FirstName;
                    if (string.IsNullOrEmpty(name))
                    {
                        var mgmMatch = mgmp.GiftMatchGetById(d.MgmMatchId);
                        if (mgmMatch.MatchType == (int)MatchType.Fb)
                        {
                            name = "FB的朋友";
                        }
                        else if (mgmMatch.MatchType == (int)MatchType.Line)
                        {
                            name = "Line的朋友";
                        }
                        else
                        {
                            name = "你的朋友";
                        }
                    }

                    tmp.Receive.Name = name;
                    tmp.Receive.AccountNumber = tmpMember.UserName.Contains("@mobile") == true ? tmpMember.Mobile : tmpMember.UserName;
                }

                tmp.ReplyMsgId = d.ReplyMessageId;
                tmp.UsedTime = d.UsedTime;
                tmp.CouponId = d.CouponId;
                tmp.CouponSequence = d.CouponSequenceNumber;
                tmp.AsCode = d.AccessCode.ToString();
                dataList.Add(tmp);
            }
            rtnData.ReceiverList = dataList;


            return rtnData;
        }

        public static List<ApiGiftHeader> ApiGiftHeaderGet()
        {
            var data = mgmp.MgmGiftHeaderCollectionGet();

            return data.Select(h => new ApiGiftHeader()
            {
                Content = h.Content,
                ImageUrl = config.SiteUrl + "/Images/MGM/header/" + h.ImageUrl,
                TextColor = h.TextColor
            }).ToList();
        }


        /// <summary>
        /// 去除HTML标记
        /// </summary>
        /// <param name="htmlstring">包括HTML </param>
        /// <returns>已经去除后的文字 </returns>
        public static string NoHTML(string htmlstring)
        {
            //檢查輸入的資料，若為空白或null，直接回傳空字串
            if (string.IsNullOrWhiteSpace(htmlstring))
            {
                return string.Empty;
            }
            int startIndex = htmlstring.IndexOf("<");
            int endIndex = htmlstring.IndexOf(">");
            while (startIndex >= 0 && endIndex > startIndex)
            {
                int length = endIndex - startIndex + 1;
                htmlstring = htmlstring.Remove(startIndex, length);
                startIndex = htmlstring.IndexOf("<");
                endIndex = htmlstring.IndexOf(">");
            }
            htmlstring = htmlstring.Replace("\t", "");
            htmlstring = htmlstring.Replace("\r\n\r\n", "\r\n");
            if (htmlstring.IndexOf("\r\n") == 0)
            {
                htmlstring = htmlstring.Remove(0, 2);
            }
            //轉換特殊字元
            htmlstring = HttpUtility.HtmlDecode(htmlstring);

            return htmlstring;
        }

        /// <summary>
        /// 驗證cashTrustLog的status欄位，判斷憑證是否未使用
        /// </summary>
        /// <param name="status"></param>
        private static bool CouponIsNotUsed(TrustStatus status)
        {
            //憑證狀態若為信託準備中或已信託，則表示憑證未使用
            if (status == TrustStatus.Initial || status == TrustStatus.Trusted)
            {
                return true;
            }
            else
            {
                //其他類型為已使用、宅配、或退貨的狀態
                return false;
            }
        }

        #endregion

        /// <summary>
        /// 退還超過期限未被接受的禮物，並且解鎖對應的coupon
        /// </summary>
        /// <returns></returns>
        public static bool ReturnUnacceptedExpiredGiftAndUnlockCoupon()
        {
            try
            {
                using (TransactionScope trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    var nowTime = DateTime.Now;
                    ViewMgmGiftCollection mgmGiftList = mgmp.GetUnacceptedExpiredGiftList(nowTime);
                    var couponIds = mgmGiftList.Where(x => x.CouponId > 0)
                                               .Select(x => Convert.ToInt32(x.CouponId)).ToList<int>();

                    pp.UnlockCouponStatusByCouponIds(couponIds);//解鎖coupon
                    mgmp.ReturnUnacceptedExpiredGiftByNowTime(nowTime);//退還禮物

                    #region Log
                    MgmGiftExpiredLogCollection gLogs = new MgmGiftExpiredLogCollection();
                    foreach (var g in mgmGiftList)
                    {
                        string matchValue = mgmp.GiftMatchGetById(g.MgmMatchId).MatchValue;
                        MgmGiftExpiredLog log = new MgmGiftExpiredLog()
                        {
                            CreatedTime = DateTime.Now,
                            GiftAccessCode = g.AccessCode.Value,
                            SenderId = g.SenderId,
                            SendTime = g.SendTime,
                            MatchValue = matchValue ?? ""
                        };
                        gLogs.Add(log);
                    }

                    mgmp.MgmGiftExpiredLogSet(gLogs);
                    #endregion

                    trans.Complete();
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("退還超過期限未被接受的禮物Job發生錯誤：{0}", ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// 禮物數量和卡片數量設定
        /// </summary>
        /// <returns></returns>
        public static bool GiftMessageCountSet(int userId, int msgType, bool isRead = false)
        {
            MemberFastInfo memberFastInfo = mp.MemberFastInfoGet(userId);
            memberFastInfo.UserId = userId;
            if (msgType == (int)GiftMessageType.Send)
            {
                if (isRead)
                {
                    if (memberFastInfo.GiftCardCount != null)
                    {
                        memberFastInfo.GiftCardCount = memberFastInfo.GiftCardCount == 0 ? 0 : memberFastInfo.GiftCardCount - 1;
                    }

                }
                else
                {
                    memberFastInfo.GiftCardCount = memberFastInfo.GiftCardCount != null ? memberFastInfo.GiftCardCount + 1 : 1;
                    memberFastInfo.MessageCount = memberFastInfo.MessageCount == 0 ? 0 : memberFastInfo.MessageCount - 1;
                }
            }
            else if (msgType == (int)GiftMessageType.Reply)
            {
                if (isRead)
                {
                    if (memberFastInfo.ReplyCardCount != null)
                    {
                        memberFastInfo.ReplyCardCount = memberFastInfo.ReplyCardCount == 0 ? 0 : memberFastInfo.ReplyCardCount - 1;
                    }
                }
                else
                {
                    memberFastInfo.ReplyCardCount = memberFastInfo.ReplyCardCount != null ? memberFastInfo.ReplyCardCount + 1 : 1;
                }
            }
            else if (msgType == (int)GiftMessageType.GiftCode)
            {
                memberFastInfo.MessageCount = memberFastInfo.MessageCount + 1;
            }
            mp.MemberFastInfoSet(memberFastInfo);
            return true;
        }

        /// <summary>
        /// 使用accessCode取得所有相關couponId
        /// </summary>
        /// <returns></returns>
        public static List<int?> GetCouponIdByAsCode(string asCode)
        {
            return mgmp.GiftViweGetByAsCodeWithNoUserId(asCode);
        }

        /// <summary>
        /// 使用couponId回傳BarCode以及使用狀況
        /// </summary>
        /// <returns></returns>
        public static ApiBarCode GetBarcode(int? couponId)
        {
            ApiBarCode result = new ApiBarCode();
            ViewPponCoupon coupon = null;
            bool EnableFami3Barcode = config.EnableFami3Barcode;
            CouponCodeType codeType = CouponCodeType.Pincode;
            string Image = string.Empty;
            string sequenceNumber = string.Empty;
            string CodeType = "0";//表示一般
            
            coupon = pp.ViewPponCouponGet(couponId ?? 0);

            sequenceNumber = coupon.SequenceNumber;

            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(coupon.BusinessHourGuid);

            string CouponCode = coupon.CouponCode;

            //檢查是否為廠商提供序號活動
            if (Helper.IsFlagSet(coupon.GroupOrderStatus ?? 0, GroupOrderStatus.PEZevent))
            {
                CouponCode = "--------";
            }

            string itemName = string.Empty;

            DateTime? expireDate = null;

            //檢查有沒有延長使用期限
            if (vpd.ChangedExpireDate == null)
            {
                expireDate = vpd.BusinessHourDeliverTimeE;
            }
            else
            {
                expireDate = vpd.ChangedExpireDate;
            }

            //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
            if ((coupon.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
            {
                //多檔次子檔訂單需取得主檔的圖片
                Item mainDealContent = pponProvider.ItemGetBySubDealBid(coupon.BusinessHourGuid);
                if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                {
                    itemName = mainDealContent.ItemName;
                }
            }
            else
            {
                itemName = coupon.ItemName;
            }

            Dictionary<string, System.Drawing.Image> BarcodeList = new Dictionary<string, System.Drawing.Image>();

            if (Helper.IsFlagSet((GroupOrderStatus)coupon.GroupOrderStatus.Value, GroupOrderStatus.FamiDeal))
            {
                //全家咖啡寄杯(成套票券)
                if ((Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon) && (vpd.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0))
                {
                    Core.Models.Entities.FamilyNetEvent ed = fami.GetFamilyNetEvent(vpd.BusinessHourGuid);
                    if (ed != null && ed.Version == (int)FamilyBarcodeVersion.FamiSingleBarcode)
                    {
                        codeType = CouponCodeType.FamiSingleBarcode; //一段
                    }
                    else
                    {
                        codeType = CouponCodeType.FamiItemCode;
                    }
                    CodeType = "1";
                }

                switch (codeType)
                {
                    case CouponCodeType.Pincode://3 Barcode
                        if (EnableFami3Barcode)
                        {
                            var famipos = OrderFacade.FamiposBarcodeGetCreate(coupon.OrderDetailGuid, vpd.BusinessHourGuid, vpd);
                            if (famipos.IsLoaded)
                            {
                                if (!string.IsNullOrEmpty(famipos.Barcode1))
                                {
                                    BarcodeList.Add(famipos.Barcode1, new LunchKingSite.Core.Component.BarCode39(famipos.Barcode1, false, false).Paint());
                                }

                                if (!string.IsNullOrEmpty(famipos.Barcode2))
                                {
                                    BarcodeList.Add(famipos.Barcode2, new LunchKingSite.Core.Component.BarCode39(famipos.Barcode2, false, false).Paint());
                                }

                                if (!string.IsNullOrEmpty(famipos.Barcode3))
                                {
                                    BarcodeList.Add(famipos.Barcode3, new LunchKingSite.Core.Component.BarCode39(famipos.Barcode3, false, false).Paint());
                                }

                                Image = MergeImage(BarcodeList, coupon.CouponCode, itemName, expireDate.Value);
                            }
                        }
                        break;
                    case CouponCodeType.FamiItemCode://4 Barcode
                        var famiNetPincode = fami.GetFamilyNetPincodeInfo(coupon.OrderGuid, coupon.CouponId.Value);
                        if (famiNetPincode != null)
                        {
                            if (!string.IsNullOrEmpty(famiNetPincode.Barcode1))
                            {
                                BarcodeList.Add(famiNetPincode.Barcode1, new LunchKingSite.Core.Component.BarCodeEAN13(famiNetPincode.Barcode1).Paint(2.5f, 350, 80, 21));
                            }

                            if (!string.IsNullOrEmpty(famiNetPincode.Barcode2))
                            {
                                BarcodeList.Add(famiNetPincode.Barcode2, new LunchKingSite.Core.Component.BarCode39(famiNetPincode.Barcode2, false, false).Paint());
                            }

                            if (!string.IsNullOrEmpty(famiNetPincode.Barcode3))
                            {
                                BarcodeList.Add(famiNetPincode.Barcode3, new LunchKingSite.Core.Component.BarCode39(famiNetPincode.Barcode3, false, false).Paint());
                            }

                            if (!string.IsNullOrEmpty(famiNetPincode.Barcode4))
                            {
                                BarcodeList.Add(famiNetPincode.Barcode4, new LunchKingSite.Core.Component.BarCode39(famiNetPincode.Barcode4, false, false).Paint());
                            }

                            Image = MergeImage(BarcodeList, coupon.CouponCode, itemName, expireDate.Value);
                        }
                        break;
                    case CouponCodeType.FamiSingleBarcode: //全家一段式條碼
                        var famiSinglePincode = fami.GetFamilyNetPincodeInfo(coupon.OrderGuid, coupon.CouponId.Value);
                        if (famiSinglePincode != null)
                        {
                            if (!string.IsNullOrEmpty(famiSinglePincode.Barcode2))
                            {
                                BarcodeList.Add(famiSinglePincode.Barcode2, new Core.Component.BarCode39(famiSinglePincode.Barcode2, false, false).Paint());
                            }

                            Image = MergeImage(BarcodeList, coupon.CouponCode, itemName, expireDate.Value);
                        }
                        break;
                    default:
                        Image = CreateQRCode(coupon.CouponCode, 600, 600, sequenceNumber, CouponCode, itemName, expireDate.Value);
                        break;
                }
            }
            else if (Helper.IsFlagSet((GroupOrderStatus)coupon.GroupOrderStatus.Value, GroupOrderStatus.HiLifeDeal))
            {
                var p = hiLife.GetHiLifePincode(coupon.SequenceNumber);
                if (p != null)
                {
                    if (!string.IsNullOrEmpty(config.HiLifeFixedFunctionPincode))
                        BarcodeList.Add(config.HiLifeFixedFunctionPincode, new LunchKingSite.Core.Component.BarCode39(config.HiLifeFixedFunctionPincode, false, false).Paint());

                    if (!string.IsNullOrEmpty(p.Pincode.ToUpper()))
                        BarcodeList.Add(p.Pincode.ToUpper(), new LunchKingSite.Core.Component.BarCode39(p.Pincode.ToUpper(), false, false).Paint());

                    Image = MergeImage(BarcodeList, coupon.CouponCode, itemName, expireDate.Value);
                }
            }
            else
            {
                string FirstCode = string.Empty;
                string LastCode = string.Empty;
                string SecurityCode = string.Empty;
                string FullCode = string.Empty;

                //全家檔次
                if ((coupon.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)
                {
                    //憑證碼和驗證碼會相同
                    FirstCode = coupon.SequenceNumber;
                    LastCode = string.Empty;
                    SecurityCode = coupon.CouponCode;
                    FullCode = coupon.CouponCode;
                }
                else if ((coupon.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
                {
                    if (Regex.IsMatch(coupon.CouponCode, "[0-9]{4}-[0-9]{4}-[0-9]{6}"))
                    {
                        FirstCode = coupon.CouponCode.Substring(0, 4);
                        LastCode = coupon.CouponCode.Substring(5, 4);
                        SecurityCode = coupon.CouponCode.Substring(10, 6);
                        FullCode = string.Format("{0}-{1}-{2}", FirstCode, LastCode, SecurityCode);
                    }
                    else
                    {
                        FirstCode = coupon.SequenceNumber;
                        LastCode = string.Empty;
                        SecurityCode = coupon.CouponCode;
                        FullCode = coupon.CouponCode;
                    }
                }
                else
                {
                    string[] parts = coupon.SequenceNumber.Split('-');
                    FirstCode = parts[0];
                    LastCode = parts[1];
                    SecurityCode = coupon.CouponCode;
                    FullCode = string.Format("{0}-{1}-{2}", FirstCode, LastCode, SecurityCode);
                }
                Image = CreateQRCode(FullCode, 600, 600, sequenceNumber, CouponCode, itemName, expireDate.Value);
            }
            bool isReceive = false;
            string receiveTime = "----/--/--";
            bool isUsed = false;
            string usedTime = "----/--/--";

            var viewMgmGift = mgmp.GiftGetByCouponId(couponId ?? 0);
            if (viewMgmGift.IsLoaded)
            {
                if (viewMgmGift.ReceiveTime != null)
                {
                    isReceive = true;
                    receiveTime = viewMgmGift.ReceiveTime.Value.ToString("yyyy/MM/dd");
                }

                if (viewMgmGift.UsedTime != null)
                {
                    isUsed = true;
                    usedTime = viewMgmGift.UsedTime.Value.ToString("yyyy/MM/dd");
                }
            }




            result.Image = Image;
            result.IsReceive = isReceive==true?"已領取":"未領取";
            result.ReceiveTime = receiveTime;

            //檢查有沒有過期且尚未使用
            if (expireDate < DateTime.Now && !isUsed)
            {
                result.IsUsed = "已過兌換期限";
                result.UsedTime = usedTime;
            }
            else
            {
                result.IsUsed = isUsed == true ? "已使用" : "未使用";
                result.UsedTime = usedTime;
            }
            
            result.CouponCode = CouponCode;
            result.SequenceNumber = sequenceNumber;
            result.CodeType = CodeType;


            return result;
        }

        /// <summary>
        /// 產生QRcode Function
        /// </summary>
        /// <param name="code">QRCode產檔內容</param>
        /// <param name="dQRHeight">QRCode高</param>
        /// <param name="dQRWeight">QRCode寬</param>
        /// <param name="sequenceNumber">兌換序號</param>
        /// <param name="couponCode">確認碼</param>
        /// <param name="itemName">商品短標</param>
        /// <param name="expireDate">兌換期限</param>
        /// <returns></returns>
        protected static string CreateQRCode(string code, int dQRHeight, int dQRWeight,string sequenceNumber,string couponCode,string itemName,DateTime expireDate)
        {
            ByteMatrix byteMatrix = new MultiFormatWriter().encode(code, BarcodeFormat.QR_CODE, dQRHeight, dQRWeight);

            int width = byteMatrix.Width;
            int height = byteMatrix.Height;

            int item_Font_Width = 0;
            int second_Font_Width = 0;
            int expire_Width = 0;
            bool oneLine = true;

            int preHigh = 0;

            string base64string = string.Empty;

            Font pinf = new System.Drawing.Font("細明體", 20, true ? System.Drawing.FontStyle.Bold : System.Drawing.FontStyle.Regular); //文字字型
            Font f = new System.Drawing.Font("細明體", 16, System.Drawing.FontStyle.Regular); //文字字型
            Font desf = new System.Drawing.Font("細明體", 12, System.Drawing.FontStyle.Regular); //文字字型

            Brush b = new System.Drawing.SolidBrush(Color.Black); //文字顏色
            Brush pinb = new System.Drawing.SolidBrush(Color.Black); //文字顏色

            string path = HttpContext.Current.Server.MapPath("~/Themes/MGM/images/logo.png");

            int maxItemName = 20;
            int addLineSpace = 40;
            int addSpace = 30;

            #region 製作QRCode

            System.Drawing.Bitmap bmap = new System.Drawing.Bitmap(width, height, PixelFormat.Format32bppArgb);

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    bmap.SetPixel(x, y, byteMatrix.get_Renamed(x, y) != -1
                                            ? System.Drawing.ColorTranslator.FromHtml("Black")
                                            : Color.White);
                }
            }

            #endregion
            //return bmap.Clone(new Rectangle(16, 16, 68, 68), PixelFormat.Format32bppArgb);
            /*
            int NewWidth = 500;
            int NewHeight = (500 / bmap.Width) * bmap.Height;
            Bitmap bmapOut = new Bitmap(bmap, NewWidth, NewHeight);
            */
            //return bmap;
            bmap= bmap.Clone(new Rectangle(50, 50, 500, 500), PixelFormat.Format32bppArgb);
            Bitmap mybmp = null;
            if (itemName.Count() > maxItemName)
            {
                mybmp = new Bitmap(bmap.Width, bmap.Height + 290);
            }
            else
            {
                mybmp = new Bitmap(bmap.Width, bmap.Height + 250);
            }


            Graphics gr = Graphics.FromImage(mybmp);

            #region 製作底圖疊合

            Bitmap bgImage = null;
            if (itemName.Count() > maxItemName)
            {
                bgImage = new Bitmap(bmap.Width, bmap.Height + 290);
            }
            else
            {
                bgImage = new Bitmap(bmap.Width, bmap.Height + 250);
            }

            using (Graphics g = Graphics.FromImage(bgImage))
            {
                g.Clear(Color.White);
                g.Dispose();
            }

            gr.DrawImage(bgImage, 0, 0);

            #endregion



            #region Logo
            //合併圖片
            gr.DrawImage(Image.FromFile(path), bmap.Width / 2 - Image.FromFile(path).Width / 2, 0);

            preHigh = preHigh + Image.FromFile(path).Height;
            #endregion

            #region 商品敘述

            //圖片產生
            Bitmap pro_Image = null;
            if (itemName.Count() > maxItemName)
            {
                pro_Image = new Bitmap(bmap.Width, Image.FromFile(path).Height + addLineSpace * 2);
            }
            else
            {
                pro_Image = new Bitmap(bmap.Width, Image.FromFile(path).Height + addLineSpace);
            }

            //填滿顏色並透明
            using (Graphics g = Graphics.FromImage(pro_Image))
            {
                g.Clear(Color.Transparent);
                g.Dispose();
            }

            //計算文字長寬
            using (Graphics pgr = Graphics.FromImage(new Bitmap(1, 1)))
            {
                if (itemName.Count() > maxItemName)
                {
                    SizeF itemsize = pgr.MeasureString(itemName.Substring(0, maxItemName), f);
                    item_Font_Width = Convert.ToInt32(itemsize.Width);

                    SizeF secondsize = pgr.MeasureString(itemName.Substring(maxItemName, itemName.Count()- maxItemName), f);
                    second_Font_Width = Convert.ToInt32(secondsize.Width);

                    oneLine = false;
                }
                else
                {
                    SizeF itemsize = pgr.MeasureString(itemName, f);
                    item_Font_Width = Convert.ToInt32(itemsize.Width);
                }
                pgr.Dispose();
            }

            //文字寫入
            using (Graphics g = Graphics.FromImage(pro_Image))
            {
                g.InterpolationMode = InterpolationMode.High; //設定高品質插值法
                g.SmoothingMode = SmoothingMode.HighQuality; //設定高品質,低速度呈現平滑程度
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                if (oneLine)
                {
                    g.DrawString(itemName, f, b, bmap.Width / 2 - item_Font_Width / 2, preHigh);
                }
                else
                {
                    g.DrawString(itemName.Substring(0, maxItemName), f, b, bmap.Width / 2 - item_Font_Width / 2, preHigh);
                    preHigh = preHigh + addLineSpace;
                    g.DrawString(itemName.Substring(maxItemName, itemName.Count()- maxItemName), f, b, bmap.Width / 2 - second_Font_Width / 2, preHigh);
                }

                g.Dispose();
            }

            //合併圖片
            gr.DrawImage(pro_Image, 0, 0);

            preHigh = preHigh + addSpace;

            #endregion

            #region QRCode
            //合併QRCode
            gr.DrawImage(bmap, 0, preHigh);

            preHigh = preHigh + bmap.Height;

            #endregion

            #region 兌換序號寫入

            //圖片產生
            Bitmap pin_Image = new Bitmap(bmap.Width, 100);

            //填滿顏色並透明
            using (Graphics g = Graphics.FromImage(pin_Image))
            {
                g.Clear(Color.Transparent);
                g.Dispose();
            }

            //計算文字長寬
            using (Graphics gra = Graphics.FromImage(new Bitmap(1, 1)))
            {
                SizeF size = gra.MeasureString("使用期限:" + expireDate.ToString("yyyy/MM/dd"), pinf);
                expire_Width = Convert.ToInt32(size.Width);
                gra.Dispose();
            }

            //文字寫入
            using (Graphics g = Graphics.FromImage(pin_Image))
            {
                g.InterpolationMode = InterpolationMode.High; //設定高品質插值法
                g.SmoothingMode = SmoothingMode.HighQuality; //設定高品質,低速度呈現平滑程度
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.DrawString("使用期限:" + expireDate.ToString("yyyy/MM/dd"), pinf, pinb, bmap.Width/2- expire_Width/2, 0);
                g.DrawString("兌換序號:" + sequenceNumber, pinf, pinb, 0, 40);
                g.DrawString("確認碼:" + couponCode, pinf, pinb, 0, 70);
                g.Dispose();
            }

            //合併圖片
            gr.DrawImage(pin_Image, 0, preHigh);

            gr.Dispose();

            #endregion



            #region 轉換Base64

            MemoryStream ms = new MemoryStream();

            mybmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            byte[] imageBytes = ms.ToArray();

            base64string = Convert.ToBase64String(imageBytes);

            #endregion

            return base64string;

           

        }


        /// <summary>
        /// 多圖檔垂直合併
        /// </summary>
        /// <param name="barcodeList">Barcode圖檔List</param>
        /// <param name="couponCode">紅利Pin碼</param>
        /// <param name="itemName">優惠券短標</param>
        /// <param name="expireDate">兌換期限</param>
        /// <returns></returns>
        protected static string MergeImage(Dictionary<string, System.Drawing.Image> barcodeList, string couponCode,string itemName, DateTime expireDate)
        {
            int wide = 0;
            int high = 0;
            int preHigh = 0;

            int font_Width = 0;
            int font_Height = 0;
            int item_Font_Width = 0;
            int expire_Width=0;
            int second_Font_Width = 0;
            bool oneLine = true;

            Font desf = new System.Drawing.Font("細明體", 12, System.Drawing.FontStyle.Regular); //文字字型
            Font f = new System.Drawing.Font("細明體",16,System.Drawing.FontStyle.Regular); //文字字型
            Font pinf = new System.Drawing.Font("細明體", 20, true ? System.Drawing.FontStyle.Bold : System.Drawing.FontStyle.Regular); //文字字型

            Brush b = new System.Drawing.SolidBrush(Color.Black); //文字顏色           
            Brush pinb = new System.Drawing.SolidBrush(Color.Black); //文字顏色

            int maxItemName = 20;
            int addLineSpace = 40;
            int addSpace = 20;

            string path = HttpContext.Current.Server.MapPath("~/Themes/MGM/images/logo.png");

            //計算Barcode寬度大小            
            foreach (var item in barcodeList)
            {
                high = high + item.Value.Height;
                if (wide < item.Value.Width)
                {
                    wide = item.Value.Width;
                }
            }

            wide = wide + addSpace;

            //Barcode+條碼(數字)+額外的文字總高
            high = high + barcodeList.Count() * 50 + 320;
            if (itemName.Count() > maxItemName)
            {
                high=high + addLineSpace;
            }


            //建立圖片
            Bitmap mybmp = new Bitmap(wide, high);
            Graphics gr = Graphics.FromImage(mybmp);

            #region 製作底圖疊合

            Bitmap bgImage = new Bitmap(wide, high);

            using (Graphics g = Graphics.FromImage(bgImage))
            {
                g.Clear(Color.White);
                g.Dispose();
            }

            gr.DrawImage(bgImage, 0, 0);

            #endregion

            #region Logo
            //合併Logo圖片
            gr.DrawImage(Image.FromFile(path), wide / 2 - Image.FromFile(path).Width / 2, 0);

            preHigh = preHigh + Image.FromFile(path).Height;
            #endregion

            #region 商品敘述

           //圖片產生
            Bitmap pro_Image = null;
            if (itemName.Count() > maxItemName)
            {
                pro_Image = new Bitmap(wide, Image.FromFile(path).Height + addLineSpace*2);
            }
            else
            {
                pro_Image = new Bitmap(wide, Image.FromFile(path).Height+ addLineSpace);
            }


            //填滿顏色並透明
            using (Graphics g = Graphics.FromImage(pro_Image))
            {
                g.Clear(Color.Transparent);
                g.Dispose();
            }

            //計算文字長寬
            using (Graphics pgr = Graphics.FromImage(new Bitmap(1, 1)))
            {
                if (itemName.Count() > maxItemName)
                {
                    SizeF itemsize = pgr.MeasureString(itemName.Substring(0, maxItemName), f);
                    item_Font_Width = Convert.ToInt32(itemsize.Width);

                    SizeF secondsize = pgr.MeasureString(itemName.Substring(maxItemName, itemName.Count() - maxItemName), f);
                    second_Font_Width = Convert.ToInt32(secondsize.Width);

                    oneLine = false;
                }
                else
                {
                    SizeF itemsize = pgr.MeasureString(itemName, f);
                    item_Font_Width = Convert.ToInt32(itemsize.Width);
                }
                pgr.Dispose();
            }

            //文字寫入
            using (Graphics g = Graphics.FromImage(pro_Image))
            {
                g.InterpolationMode = InterpolationMode.High; //設定高品質插值法
                g.SmoothingMode = SmoothingMode.HighQuality; //設定高品質,低速度呈現平滑程度
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                if (oneLine)
                {
                    g.DrawString(itemName, f, b, wide / 2 - item_Font_Width / 2, preHigh);
                }
                else
                {
                    g.DrawString(itemName.Substring(0, maxItemName), f, b, wide / 2 - item_Font_Width / 2, preHigh);
                    preHigh = preHigh + addLineSpace;
                    g.DrawString(itemName.Substring(maxItemName, itemName.Count() - maxItemName), f, b, wide / 2 - second_Font_Width / 2, preHigh);
                }

                g.Dispose();
            }

            //合併圖片
            gr.DrawImage(pro_Image, 0, 0);

            preHigh = preHigh + addLineSpace;

            #endregion

            foreach (var item in barcodeList)
            {
                gr.DrawImage(item.Value, wide / 2 - item.Value.Width / 2, preHigh + addSpace);

                preHigh = preHigh + item.Value.Height + addSpace;

                //計算文字長寬
                using (Graphics gra = Graphics.FromImage(new Bitmap(1, 1)))
                {
                    SizeF size = gra.MeasureString(item.Key, f);
                    font_Width = Convert.ToInt32(size.Width);
                    font_Height = Convert.ToInt32(size.Height);
                    gra.Dispose();
                }

                //圖片產生
                Bitmap image = new Bitmap(font_Width, font_Height);

                //填滿顏色並透明
                using (Graphics g = Graphics.FromImage(image))
                {
                    g.Clear(Color.Transparent);
                    g.Dispose();
                }

                //文字寫入
                using (Graphics g = Graphics.FromImage(image))
                {
                    g.InterpolationMode = InterpolationMode.High; //設定高品質插值法
                    g.SmoothingMode = SmoothingMode.HighQuality; //設定高品質,低速度呈現平滑程度
                    g.CompositingQuality = CompositingQuality.HighQuality;
                    g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                    g.DrawString(item.Key, f, b, 0, 0);
                    g.Dispose();
                }

                //合併圖片
                gr.DrawImage(image, wide / 2 - font_Width / 2, preHigh);

                preHigh = preHigh + font_Height;
            }

            #region Pin碼 & 說明敘述寫入


            //圖片產生
            Bitmap des_Image = new Bitmap(wide, 200);

           
            //填滿顏色並透明
            using (Graphics g = Graphics.FromImage(des_Image))
            {
                g.Clear(Color.Transparent);
                g.Dispose();
            }

            //計算文字長寬
            using (Graphics pgr = Graphics.FromImage(new Bitmap(1, 1)))
            {
                SizeF size = pgr.MeasureString("紅利PIN碼:" + couponCode, pinf);
                SizeF sizeDateTime = pgr.MeasureString("兌換期限:" + expireDate.ToString("yyyy/MM/dd"), pinf);
                font_Width = Convert.ToInt32(size.Width);
                expire_Width = Convert.ToInt32(sizeDateTime.Width);
                pgr.Dispose();
            }

            //文字寫入
            using (Graphics g = Graphics.FromImage(des_Image))
            {
                g.InterpolationMode = InterpolationMode.High; //設定高品質插值法
                g.SmoothingMode = SmoothingMode.HighQuality; //設定高品質,低速度呈現平滑程度
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.DrawString("兌換期限:" + expireDate.ToString("yyyy/MM/dd"), pinf, pinb, wide / 2 - expire_Width / 2, 0);
                g.DrawString("紅利PIN碼:" + couponCode, pinf, pinb, wide / 2 - font_Width / 2, 40);
                g.DrawString("如遇到無法兌換情況請依照下列步驟:", desf, b, 10, 80);
                g.DrawString("1.撥打全家客服:0800-071-999，告知客服此杯紅利PIN碼", desf, b, 10, 110);
                g.DrawString("2.完成告知後可用全家FamiPort輸入此杯紅利PIN碼並列印單據至櫃台兌換", desf, b, 10, 140);
                g.Dispose();
            }
            //合併圖片
            gr.DrawImage(des_Image, 0, preHigh + addSpace);
            #endregion



            gr.Dispose();

            #region 轉換成Base64

            MemoryStream ms = new MemoryStream();

            mybmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            byte[] imageBytes = ms.ToArray();

            string base64String = Convert.ToBase64String(imageBytes);

            #endregion

            return base64String;
            
        }

        /// <summary>
        /// 資料安全遮罩
        /// </summary>
        /// <param name="oriString">原文字</param>
        /// <param name="maskString">遮罩文字</param>
        /// <param name="isDis">false時，遮罩頭尾</param>
        /// <returns></returns>
        private static string GetMask(string oriString, string maskString, bool isDis)
        {
            string result = "";
            oriString = oriString.Trim();
            char[] oStrArry = oriString.ToCharArray();
            int[] oArray = new int[] { 0, oriString.Trim().Length - 1 };

            if (Regex.IsMatch(oriString.Trim(), @"^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$"))
            {
                result += GetMask(oriString.Split('@')[0], maskString, true) + "@";

                result += oriString.Split('@')[1].ToString();

                return result;
            }
            else if (Regex.IsMatch(oriString.Trim(), "^(09([0-9]){8})$"))
            {
                oArray = new int[] { 0, 1, 2, 7, 8, 9 };
            }
            else if (Regex.IsMatch(oriString.Trim(), "^[a-zA-Z][0-9]{9}$"))
            {
                oArray = new int[] { 0, 1, 2, 3, 9 };
            }

            for (int i = 0; i < oStrArry.Length; i++)
            {
                if (isDis)
                {
                    result += oArray.Contains(i) ? oStrArry[i].ToString() : maskString;
                }
                else
                {
                    result += oArray.Contains(i) ? maskString : oStrArry[i].ToString();
                }
            }
            return result;
        }


        /// <summary>
        /// 回寫快速領取狀況
        /// </summary>
        /// <param name="asCode">領取禮物的accessCode</param>
        /// <param name="couponId">優惠券Id</param>
        /// <param name="info">使用者填寫的資料</param>
        /// <returns></returns>
        public static void QuickReceive(string asCode,int? couponId, string info)
        {
            int giftId = 0;
            int matchId = 0;

            string QuickInfo = string.Empty;


            //先取ViewMgmGift
            var viewMgmGift=mgmp.GiftGetByCouponIdAndAsCode(couponId.Value,asCode);

            if (viewMgmGift.IsLoaded)
            {
                giftId = viewMgmGift.Id;
                matchId = viewMgmGift.MgmMatchId;

                //解鎖Coupon
                couponId = couponId ?? 0;
                if (couponId != 0)
                {
                    Coupon cp = pp.CouponGet(couponId.Value);
                    cp.Status = (int)CouponStatus.None;
                    pp.CouponSet(cp);
                }

            }

            var mgmGiftMatch = mgmp.GiftMatchGetById(matchId);
            if(mgmGiftMatch.IsLoaded)
            {
                if (!string.IsNullOrEmpty(info) && info!= "undefined")
                {
                    QuickInfo = info;
                }
                else
                {
                    QuickInfo = mgmGiftMatch.MatchValue;
                }
            }


            var mgmGiftVersion = mgmp.GiftVersionGetByGiftiId(giftId);

            if (mgmGiftVersion.IsLoaded)
            {
                mgmGiftVersion.ReceiveTime = DateTime.Now;
                mgmGiftVersion.Accept = (int)MgmAcceptGiftType.Quick;
                mgmGiftVersion.IsActive = true;
                mgmGiftVersion.QuickInfo = QuickInfo;

                mgmp.MgmGiftVersionSet(mgmGiftVersion);
            }

        }

    }
}

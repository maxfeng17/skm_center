﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.BizLogic.Facade
{
    public class BeaconFacade
    {
        private static IBeaconProvider bp;
        private static IOAuthProvider op;
        private static IPponProvider pp;
        private static IEventProvider ep;

        static BeaconFacade()
        {
            bp = ProviderFactory.Instance().GetProvider<IBeaconProvider>();
            op = ProviderFactory.Instance().GetProvider<IOAuthProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        }

        /// <summary>
        /// 候選eventIds為指定客群的, 判斷user_token是否符合
        /// </summary>
        /// <param name="eventIds"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static List<int> GetEventIdInTargets(List<int> eventIds, string accessToken)
        {
            var targetEvents = bp.GetBeaconEventTargetList(eventIds);
            foreach(var i in targetEvents)
            {
                if (i.TargetUserToken != accessToken)
                {
                    eventIds.Remove(i.EventId);
                }
            }

            return eventIds;
        }

        public static List<string> getFloor(string f)
        {
            List<string> fl = new List<string>();
            fl.Add(string.Format("{0}", f));

            if (f.Substring(0, 1).ToLower() == "b")
            {
                fl.Add(string.Format("{0}", f.Replace("F", "")));
            }
            else if (f.Substring(0, 1) == "0")
            {
                fl.Add(string.Format("{0}", f.Substring(1, f.Length - 1)));
            }
            return fl;
        }

        /// <summary>
        /// 判斷子訊息ID若為商品檔次, 當下必須符合檔次上檔時間
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public static List<int> GetBeaconSubeventIds(int eventId)
        {
            List<BeaconSubevent> subEventList = bp.GetBeaconSubeventList(eventId);
            var temp = subEventList;
            foreach (var i in temp)
            {
                if (i.EventType == (int)EventType.Bid)
                {
                    ViewExternalDeal ved = pp.ViewExternalDealGetByBid(i.ActionBid);
                    if (!ved.IsLoaded)
                    {
                        subEventList.Remove(i);
                    }
                }
            }
            return subEventList.Select(p => p.SubeventId).ToList();  
        }

        /// <summary>
        /// 取得事件型態
        /// </summary>
        /// <param name="targerEvent"></param>
        /// <returns></returns>
        public static Core.ModelCustom.Action GetPokeBallActionParameter(BeaconEvent targerEvent)
        {
            Core.ModelCustom.Action action = new Core.ModelCustom.Action();
            action.MessageId = targerEvent.EventId.ToString();
            action.Message = targerEvent.Content;
            action.MessageTitle = targerEvent.Subject;
            action.TriggerType = PokeballTriggerType.BeaconMessageRead;

            switch (targerEvent.EventType)
            {
                case (int)EventType.SubEvent:
                    action.ActionType = Helper.GetEnumDescription(PokeballActionType.GroupMessage);
                    break;
                case (int)EventType.Url:
                    action.ActionType = Helper.GetEnumDescription(PokeballActionType.FullscreenWebView);
                    action.Url = targerEvent.ActionUrl;
                    break;
                case (int)EventType.Message:
                    action.ActionType = Helper.GetEnumDescription(PokeballActionType.PureText);
                    break;
                case (int)EventType.Bid:
                    action.ActionType = Helper.GetEnumDescription(PokeballActionType.ShowDealDetail);
                    action.Bid = targerEvent.ActionBid.ToString();
                    break;
                default:
                    break;
            }

            return action;
        }
        
        public static List<BeaconLog> GetUserBeaconLog(int triggerAppId, int userId, int appDeviceId)
        {
            var UserBeaconLogs = new List<BeaconLog>();
            try
            {
                if (appDeviceId != 0)
                {
                    if (userId == 0)//沒有身分就是用裝置編號撈取
                    {
                        UserBeaconLogs = bp.GetBeaconLogList(triggerAppId, 0, appDeviceId, DateTime.Today);
                    }
                    else
                    {
                        //先撈出裝置下無會員身分的訊息, 並update成此member
                        UserBeaconLogs = bp.GetBeaconLogList(triggerAppId, 0, appDeviceId, DateTime.Today);
                        if (UserBeaconLogs.Any())
                        {
                            foreach (var item in UserBeaconLogs)
                            {
                                item.UserId = userId;
                                bp.InsertOrUpdateBeaconLog(item);
                            }
                        }
                        //將訊息歸戶後, 重新以userID撈取即可
                        UserBeaconLogs = bp.GetBeaconLogList(triggerAppId, userId, null, DateTime.Today);
                    }
                }
            }
            catch(Exception e)
            {
                var item = new PromoItem()
                {
                    Code = "SetSellerBeaconLog",
                    UseTime = DateTime.Now,
                    Memo = string.Format("triggerAppId:{0},userId:{1},appDeviceId:{2}, log:{3}", triggerAppId, userId, appDeviceId, "GetUserBeaconLog(" + triggerAppId + "/" + userId + "/" + appDeviceId + ").Exception=" + e.Message),
                };
                ep.PromoItemSet(item);
            }
            return UserBeaconLogs;

        }

        public static List<string> GetBeaconTriggerAppUuidList(int triggerAppId)
        {
            List<string> uuids = new List<string>();
            try
            {
                var uuidList = bp.GetBeaconTriggerAppUuidList(triggerAppId);
                if (uuidList.Any())
                {
                    uuids = uuidList.Select(p => p.ElectricUuid.ToString()).ToList();
                    uuids.AddRange(uuidList.Select(p => p.SellerUuid.ToString()).ToList());
                }
            }
            catch (Exception)
            {

            }
            return uuids.GroupBy(p=>p).Select(p=>p.First()).ToList();//distinct 
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.API;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Facade
{
    /// <summary>
    /// 搜尋檔次功能相關邏輯
    /// </summary>
    public class SearchFacade
    {
        private static ISysConfProvider _config;
        private static IOrderProvider _op;

        static SearchFacade()
        {
            _config = ProviderFactory.Instance().GetConfig();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        /// <summary>
        /// 以關鍵字取得搜尋的檔次BusinessHourGuid列表
        /// </summary>
        /// <param name="queryString">關鍵字</param>
        /// <param name="searchEngine"></param>
        /// <param name="queryLog">搜尋過程的紀錄</param>
        /// <param name="indexList">要搜尋的index資料夾名稱(optional)</param>
        /// <returns></returns>
        public static List<IViewPponDeal> GetPponMainDealsByQueryString(string queryString, int? searchEngine, out string queryLog, List<string> indexList = null)
        {
            List<Guid> searchedBids = new List<Guid>();
            queryLog = string.Empty;
            string externalSearchUrl1 = Helper.CombineUrl(_config.SearchApiUrl, "/api/rudelySearch");
            string externalSearchUrl2 = Helper.CombineUrl(_config.SearchApiUrl, "/api/standardSearch");
            string externalSearchUrl3 = Helper.CombineUrl(_config.SearchApiUrl, "/api/standardSearch2");
            searchEngine = searchEngine ?? _config.SearchEngine;
            if (searchEngine == 0)
            {
                throw new NotSupportedException();
            }
            else if (searchEngine == 1)
            {
                using (var client = new HttpClient())
                {
                    try
                    {
                        client.Timeout = new TimeSpan(0, 0, 30);
                        string apiFullUrl = string.Format("{0}?q={1}&s={2}&d={3}",
                            externalSearchUrl1, HttpUtility.UrlEncode(queryString), _config.dealCountLimit,
                            Helper.GetOrderFromType());
                        HttpResponseMessage response = client.GetAsync(apiFullUrl).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            ApiResult<List<Guid>> content = JsonConvert.DeserializeObject<ApiResult<List<Guid>>>(
                                response.Content.ReadAsStringAsync()
                                    .Result);
                            foreach (Guid bid in content.Data)
                            {
                                searchedBids.Add(bid);
                            }
                        }
                    } catch (Exception ex)
                    {
                        
                    }
                }
            }
            else if (searchEngine == 2)
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiFullUrl = string.Format("{0}?q={1}&s={2}&d={3}",
                        externalSearchUrl2, HttpUtility.UrlEncode(queryString), _config.dealCountLimit,
                        Helper.GetOrderFromType());
                    HttpResponseMessage response = client.GetAsync(apiFullUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        ApiResult<List<Guid>> content = JsonConvert.DeserializeObject<ApiResult<List<Guid>>>(
                            response.Content.ReadAsStringAsync()
                                .Result);
                        foreach (Guid bid in content.Data)
                        {
                            searchedBids.Add(bid);
                        }
                    }
                }
            }
            else if (searchEngine == 3)
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = new TimeSpan(0, 0, 30);
                    string apiFullUrl = string.Format("{0}?q={1}&s={2}&d={3}",
                        externalSearchUrl3, HttpUtility.UrlEncode(queryString), _config.dealCountLimit,
                        Helper.GetOrderFromType());
                    HttpResponseMessage response = client.GetAsync(apiFullUrl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        ApiResult<List<Guid>> content = JsonConvert.DeserializeObject<ApiResult<List<Guid>>>(
                            response.Content.ReadAsStringAsync()
                                .Result);
                        foreach (Guid bid in content.Data)
                        {
                            searchedBids.Add(bid);
                        }
                    }
                }
            }



            #region 活動時程表「有開賣」或「沒設隱藏」的檔次
            List<IViewPponDeal> result = new List<IViewPponDeal>();
            List<IDealTimeSlot> timeSlots = ViewPponDealManager.DefaultManager.GetDealTimeSlotInEffectiveTime();
            //1.過濾沒開賣 (與DealTimeSlot取交集)
            var effectiveBids = from x in searchedBids
                                join y in timeSlots on x equals y.BusinessHourGuid
                                select x;
            //2.過濾設隱藏
            List<Guid> hiddenBids = timeSlots.Where(x => x.Status == (int)DealTimeSlotStatus.NotShowInPponDefault)
                                               .Select(x => x.BusinessHourGuid).ToList();
            searchedBids = effectiveBids.Except(hiddenBids).ToList();
            HashSet<Guid> duplicatedDealSet = new HashSet<Guid>();
            foreach(var searchBid in searchedBids)
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(searchBid, true);
                if (deal == null || deal.IsLoaded == false)
                {
                    continue;
                }
                if (duplicatedDealSet.Contains(deal.BusinessHourGuid)) {
                    continue;
                }
                if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
                {
                    continue;
                }
                duplicatedDealSet.Add(deal.BusinessHourGuid);
                result.Add(deal);
            }
            #endregion

            return result;
        }

        public static bool TryGetDiscountCampaignIdByCode(int userId, string querystring, out DiscountCampaign campaign)
        {
            campaign = null;
            if (string.IsNullOrWhiteSpace(querystring))
            {
                return false;
            }
            if (querystring.StartsWith("code:") == false)
            {
                return false;
            }
            querystring = querystring.Substring(5);

            DiscountCode dc = _op.DiscountCodeGetByCode(querystring);
            if (dc.IsLoaded == false || dc.CampaignId == null || dc.CampaignId == 0) {
                return false;
            }

            campaign = _op.DiscountCampaignGet(dc.CampaignId.Value);
            if (campaign.IsLoaded == false)
            {
                return false;
            }
            
            if ((campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) > 0 &&
                (campaign.Flag & (int)DiscountCampaignUsedFlags.OwnerUseOnly) ==  0)
            {
                return true;
            }

            if ((campaign.Flag & (int)DiscountCampaignUsedFlags.InstantGenerate) > 0 &&
                (campaign.Flag & (int)DiscountCampaignUsedFlags.OwnerUseOnly)>  0 &&
                dc.Owner == userId
                )
            {
                return true;
            }

            if (userId == 0)
            {
                return false;
            }


            return true;
        }

        /// <summary>
        /// 以關鍵字取得搜尋的檔次列表
        /// </summary>
        /// <param name="querystring">關鍵字</param>
        /// <param name="sortType">排序依據(預設以銷售量排序)</param>
        /// <param name="searchEngine"></param>
        /// <returns></returns>
        public static List<IViewPponDeal> GetSearchDeals(string querystring, string sortType, int? searchEngine)
        {
            DateTime now = DateTime.Now;
            CategorySortType sort = CategorySortType.TryParse(sortType, out sort) ? sort : CategorySortType.TopOrderTotal;
            string queryLog = string.Empty;

            List<IViewPponDeal> dealList = SearchFacade.GetPponMainDealsByQueryString(querystring, searchEngine, out queryLog);
            
            dealList = dealList.OrderBySortType(sort);

            return dealList;
        }

        public static List<IViewPponDeal> GetSearchDealsByDiscountCode(
            string querystring, string sortType, DiscountCampaign campaign, int takeSize)
        {
            var campaignRule = DiscountManager.GetCampaignRule(campaign.Id);
            var deals = ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true);
            List<Guid> bids = new List<Guid>();
            foreach(var deal in deals)
            {
                var dealRule = DiscountManager.GetDealDiscountTerm(deal.BusinessHourGuid);
                if (campaignRule.Match(dealRule))
                {
                    bids.Add(deal.BusinessHourGuid);
                    if (bids.Count >= takeSize)
                    {
                        break;
                    }
                }
            }
            
            CategorySortType sort = CategorySortType.TryParse(sortType, out sort) ? sort : CategorySortType.TopOrderTotal;            
            
            List<IViewPponDeal> dealList = ViewPponDealManager.DefaultManager.ViewPponDealGetListByGuidList(bids).ToList();
            dealList = dealList.OrderBySortType(sort);

            return dealList;
        }
    }

    public static class SearchExtension
    {
        /// <summary>
        /// 檔次列表依照類型排序
        /// </summary>
        /// <param name="dealList">檔次列表</param>
        /// <param name="sortType">排序類型</param>
        /// <returns></returns>
        public static List<IViewPponDeal> OrderBySortType(this List<IViewPponDeal> dealList, CategorySortType sortType)
        {
            switch (sortType)
            {
                case CategorySortType.TopNews:
                    dealList = dealList.OrderByDescending(x => x.BusinessHourOrderTimeS).ToList();
                    break;
                case CategorySortType.TopOrderTotal:
                    var soldOut = dealList.Where(x => x.OrderTotalLimit.GetValueOrDefault(0) == 0);
                    dealList = dealList.Where(x=>x.OrderTotalLimit.GetValueOrDefault(0) > 0)
                        .OrderByDescending(x => x.IsWms)
                        .ThenByDescending(x => x.GetAdjustedOrderedQuantity().Quantity).ToList();

                    dealList.AddRange(soldOut);
                    break;
                case CategorySortType.DiscountAsc:
                    dealList = dealList.OrderBy(x => x.ItemPrice / (x.ItemOrigPrice != 0 ? x.ItemOrigPrice : 1)).ToList();
                    break;
                case CategorySortType.PriceDesc:
                    dealList = dealList.OrderByDescending(x => x.ItemPrice).ToList();
                    break;
                case CategorySortType.PriceAsc:
                    dealList = dealList.OrderBy(x => x.ItemPrice).ToList();
                    break;
                default:
                    break;
            }

            return dealList;
        }
    }
}

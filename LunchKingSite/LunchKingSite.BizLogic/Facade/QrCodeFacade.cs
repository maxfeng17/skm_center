﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using com.google.zxing;
using com.google.zxing.qrcode.decoder;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Models.Verification;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Facade
{
    public class QrCodeFacade
    {
        private static ISellerProvider sp;
        static QrCodeFacade()
        {
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        }

        #region 產生 QRCode

        /// <summary>
        /// 產生QRCode
        /// </summary>
        /// <param name="content">QRCode內容</param>
        /// <param name="words">QRCode上方說明文字(可不填)</param>
        /// <param name="qrWidth">QRCode寬</param>
        /// <param name="qrHeight">QRCode高</param>
        /// <param name="fontFamilyName">字型(可不填)</param>
        /// <param name="fontSize">說明文字大小(可不填)</param>
        /// <returns></returns>
        public static byte[] GenerateQrCode(string content, string words, int qrWidth, int qrHeight, string fontFamilyName = "Comic Sans MS",
            int fontSize = 20)
        {
            //QRCode圖形
            Bitmap image = GetQrCdoeBitmap(content, qrWidth, qrHeight);
            Graphics g = Graphics.FromImage(image);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            //附加說明文字
            g.DrawString(words, new Font(fontFamilyName, fontSize), Brushes.Black, new RectangleF(25, 0, 430, 100));
            g.Flush();

            return GetImageBytes(image);
        }

        /// <summary>
        /// 畫出QRCode
        /// </summary>
        /// <param name="content">QRCode內文</param>
        /// <param name="width">寬</param>
        /// <param name="height">高</param>
        /// <returns></returns>
        private static Bitmap GetQrCdoeBitmap(string content, int width, int height)
        {
            Hashtable hints = new Hashtable();
            //hints.Add(EncodeHintType.CHARACTER_SET, "UTF-8");
            hints.Add(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

            Writer writer = new MultiFormatWriter();
            var matrix = writer.encode(content, BarcodeFormat.QR_CODE, width, height, hints);

            Bitmap result;
            using (Bitmap bmap = new Bitmap(width, height, PixelFormat.Format32bppArgb))
            {
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        bmap.SetPixel(x, y,
                            matrix.get_Renamed(x, y) != -1
                                ? ColorTranslator.FromHtml("Black")
                                : ColorTranslator.FromHtml("White"));
                    }
                }
                Rectangle cloneRect = new Rectangle(0, 0, width, height);
                PixelFormat format = bmap.PixelFormat;
                result = bmap.Clone(cloneRect, format);
            }
            return result;
        }

        /// <summary>
        /// 取得QRCode圖檔(byte)
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        private static byte[] GetImageBytes(Image image)
        {
            ImageCodecInfo codec = null;
            foreach (ImageCodecInfo e in ImageCodecInfo.GetImageEncoders())
            {
                if (e.MimeType == "image/png")
                {
                    codec = e;
                    break;
                }
            }

            using (EncoderParameters ep = new EncoderParameters())
            {
                ep.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, codec, ep);
                    return ms.ToArray();
                }
            }
        }

        #endregion

        #region 掃瞄QRCode的對應Action

        /// <summary>
        /// 取得QRCode對應Action
        /// </summary>
        /// <param name="qrCode"></param>
        /// <returns></returns>
        public static QrActionCode GetQrActionAndCode(string qrCode)
        {
            //新編碼QRCode {action}_{code} 
            var qrArray = qrCode.Split('|');
            if (qrArray.Length == 2)
            {
                var actionStr = qrArray[0];
                var codeStr = qrArray[1];

                if (!string.IsNullOrEmpty(actionStr))
                {
                    QrCodeActionType action;
                    if (Enum.TryParse(actionStr, out action) && Enum.IsDefined(typeof(QrCodeActionType), action))
                    {
                        switch (action)
                        {
                            case QrCodeActionType.VerifyStore:
                                if (!string.IsNullOrEmpty(codeStr))
                                {
                                    //TODO: 若code要改用Token，原本call解密就改呼叫以Token取store Guid
                                    return new QrActionCode { Action = action, Code = DecryptVerifyStore(codeStr) };
                                }
                                break;
                                //有其他新編碼由此擴充
                        }
                    }
                }
                else
                {
                    return new QrActionCode { Action = QrCodeActionType.Undefined, Code = qrCode };
                }
            }

            //查無 Action return 原值
            return new QrActionCode { Action = QrCodeActionType.Undefined, Code = qrCode };
        }

        /// <summary>
        /// 偵測 QRCode 回傳對應Pokeball
        /// </summary>
        /// <returns></returns>
        public static ActionPokeballModel GetQrActionPokeball(string code, int uniqueId)
        {
            var pokeModel = new ActionPokeballModel();
            QrActionCode qrActionCode = GetQrActionAndCode(code);

            if (qrActionCode.Code == null)
            {
                return pokeModel;
            }

            switch (qrActionCode.Action)
            {
                case QrCodeActionType.VerifyStore:
                    var verifyStoreGuid = qrActionCode.Code;
                    if (verifyStoreGuid != null && verifyStoreGuid != Guid.Empty)
                    {
                        var rootSellerGuid = sp.SellerTreeGetRootSellerGuidBySellerGuid((Guid)verifyStoreGuid);
                        if (rootSellerGuid != null)
                        {
                            var orderCount = MemberFacade.ViewCouponListMainGetCountBySeller(uniqueId, (Guid)rootSellerGuid);
                            if (orderCount > 0)
                            {
                                pokeModel.ActionPokeball = new ShowOrdersByVerifyStorePokeball { VerifyStoreGuid = qrActionCode.Code };
                            }
                            else
                            {
                                pokeModel.ActionPokeball = new ShowDealsByVerifyStorePokeball { VerifyStoreGuid = qrActionCode.Code };
                            }
                        }
                    }
                    break;
            }

            return pokeModel;
        }

        #endregion

        #region 分店 QRCode 相關

        /// <summary>
        /// 加密核銷分店
        /// </summary>
        /// <param name="verifyStore"></param>
        /// <returns></returns>
        public static string EncryptVerifyStore(Guid verifyStore)
        {
            var sec = new Security(SymmetricCryptoServiceProvider.AES);
            return sec.Encrypt(verifyStore.ToString());
        }

        /// <summary>
        /// 解密核銷分店
        /// </summary>
        /// <param name="sourceInfo"></param>
        /// <returns></returns>
        public static Guid? DecryptVerifyStore(string sourceInfo)
        {
            var sec = new Security(SymmetricCryptoServiceProvider.AES);
            string strDecryptCardInfo;
            try
            {
                strDecryptCardInfo = sec.Decrypt(sourceInfo);
            }
            catch
            {
                return null;
            }

            Guid verifyStore;
            Guid.TryParse(strDecryptCardInfo, out verifyStore);
            return verifyStore;
        }

        /// <summary>
        /// 取得分店QRCode
        /// </summary>
        /// <param name="storeGuid"></param>
        /// <param name="storeName"></param>
        /// <returns></returns>
        public static byte[] GetStoreQrCode(Guid storeGuid, string storeName)
        {
            string code = string.Format("{0}|{1}", (int)QrCodeActionType.VerifyStore, EncryptVerifyStore(storeGuid));
            string content = string.Format("https://www.17life.com/App/Open?code={0}", code);
            return GenerateQrCode(content, storeName, 480, 480);
        }

        #endregion
    }
}

﻿using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.BizLogic.Facade
{
    public class AnalyticsFacade
    {
        private static ISysConfProvider config;
        private static ISystemProvider ss;
        private static IMarketingProvider mkp;
        private static ILog Logger = LogManager.GetLogger("AnalyticsFacade");
        static AnalyticsFacade()
        {
            config = ProviderFactory.Instance().GetConfig();
            mkp = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
            ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        }

        public static bool GaUpdate(string vid, string startDate, string endDate, string metrics)
        {
            string returnvalue = string.Empty;
            //string args = "2015-03-01 2015-03-31 users,newUsers,percentNewSessions,sessionsPerUser";
            //string filePath = @"C:\project\Gaia\GoogleAnalytics\ConsoleApplicationGA\bin\Debug\ConsoleApplicationGA.exe";
            Process myProcess = new Process();
            string filePath = config.GAConsolePath;
            //Logger.Info("GaUpdate filePaht:" + filePath);
            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = Path.Combine(filePath, "ConsoleApplicationGA.exe");
            info.WorkingDirectory = Path.GetDirectoryName(filePath);
            info.UseShellExecute = false;
            info.RedirectStandardInput = true;
            info.RedirectStandardOutput = true;
            info.CreateNoWindow = true;

            GaViewIDMap idMapping;
            if (!Enum.TryParse(vid, out idMapping)) { return false; }

            DateTime sDate, eDate;
            if (!DateTime.TryParse(startDate, out sDate)) { return false; }
            if (!DateTime.TryParse(endDate, out eDate)) { return false; }

            var gaCol = new GaOverviewCollection();

            gaCol = mkp.GaOverviewGetList(sDate.Date, eDate.Date, (int)idMapping);

            while (sDate <= eDate)
            {
                info.Arguments = string.Format("{0} {1:yyyy-MM-dd} {2:yyyy-MM-dd} {3}", (int)idMapping, sDate, sDate, metrics);
                try
                {
                    using (Process process = Process.Start(info))
                    {
                        StreamReader sr = process.StandardOutput;
                        returnvalue = sr.ReadToEnd();
                    }
                    //myProcess.StartInfo = info;
                    //myProcess.Start();
                    //StreamReader sr = myProcess.StandardOutput;
                    //returnvalue = sr.ReadToEnd();
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message + ex.StackTrace);
                }

                if (string.IsNullOrWhiteSpace(returnvalue)) { return false; }

                string[] stringSeparators = new string[] { "\r\n" };
                var gaStringData = returnvalue.Split(stringSeparators, StringSplitOptions.None);

                var gaData = gaStringData
                    .Where(x => x.IndexOf(',') > 0)
                    .Select(x => new { k = x.Split(',')[0], v = x.Split(',')[1] })
                    .ToDictionary(g => g.k, g => g.v);

                //foreach (var item in gaData)
                //{
                //    Logger.Info(string.Format("{0}{1}",item.Key,item.Value));
                //}

                var gaExist = gaCol.Where(x => x.QueryDate == sDate.Date && x.ViewId == (int)idMapping).FirstOrDefault();

                if (gaExist == null)
                {
                    var newGaOverview = new GaOverview
                    {
                        QueryDate = sDate,
                        ViewId = (int)idMapping,
                        ViewName = Helper.GetEnumDescription(idMapping)
                    };

                    UpdateGAMatrics(ref newGaOverview, gaData);

                    gaCol.Add(newGaOverview);
                }
                else
                {
                    UpdateGAMatrics(ref gaExist, gaData);
                }

                sDate = sDate.AddDays(1);
            }

            return mkp.GaOverviewSet(gaCol);
        }

        private static void UpdateGAMatrics(ref GaOverview gaExist, Dictionary<string, string> gaData)
        {
            //foreach (var item in typeof(GaQueryMatrics).GetProperties())
            //{
            //    if (gaExist.GetType().GetProperty(item.Name).GetValue(gaExist, null) != null && gaData.ContainsKey(item.Name))
            //    {
            //        gaExist.GetType().GetProperty(item.Name).SetValue(gaExist, gaData[item.Name], null);
            //    }
            //}

            foreach (var item in typeof(GaQueryMatrics).GetProperties())
            {
                if (!gaData.ContainsKey(item.Name)) { continue; }
                var parseValue = gaData[item.Name];

                var resultObj = gaExist.GetType().GetProperties().Where(x => x.Name == item.Name).FirstOrDefault();
                if (resultObj == null) { continue; }

                switch (Type.GetTypeCode(resultObj.PropertyType))
                {
                    case TypeCode.Int32:
                        int tmp;
                        int.TryParse(parseValue.ToString(), out tmp);
                        gaExist.GetType().GetProperty(item.Name).SetValue(gaExist, tmp, null);
                        break;
                }
            }
        }

        private static SystemCode CheckGaToken()
        {
            bool check = true;
            SystemCode ga_token_systemcode = ss.SystemCodeGetByCodeGroupId(SingleOperator.GoogleAnalytics.ToString(), 1);
            if (!string.IsNullOrEmpty(ga_token_systemcode.CodeGroup))
            {
                if (ga_token_systemcode.ExpiredTime.HasValue && ga_token_systemcode.ExpiredTime.Value > DateTime.Now)
                {
                    return ga_token_systemcode;
                }
                else
                {
                    check = false;
                }
            }
            else
            {
                check = false;
            }
            if (!check)
            {
                Process myProcess = new Process();
                string filePath = config.GAConsolePath;
                //Logger.Info("GaUpdate filePaht:" + filePath);
                ProcessStartInfo info = new ProcessStartInfo();
                info.FileName = Path.Combine(filePath, "ConsoleApplicationGA.exe");
                info.WorkingDirectory = Path.GetDirectoryName(filePath);
                info.UseShellExecute = false;
                info.RedirectStandardInput = true;
                info.RedirectStandardOutput = true;
                info.CreateNoWindow = true;
                info.Arguments = string.Format("{0} {1}", config.SiteUrl, filePath);
                using (Process process = Process.Start(info))
                {
                }
            }
            Thread.Sleep(5000);
            ga_token_systemcode = ss.SystemCodeGetByCodeGroupId(SingleOperator.GoogleAnalytics.ToString(), 1);
            return ga_token_systemcode;
        }

        private static string Get17LifeGaApiUrl(string access_token, Guid bid, DateTime start_date, DateTime end_date)
        {
            string api_url = @"https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A{0}&start-date={1}&end-date={2}&metrics=ga%3Ausers%2Cga%3Aentrances%2Cga%3AentranceRate%2Cga%3Apageviews%2Cga%3AuniquePageviews%2Cga%3AavgTimeOnPage%2Cga%3Aexits%2Cga%3AexitRate%2Cga%3Asessions&dimensions=ga%3ApagePath&filters=ga%3ApagePath%3D~%5E((%5B%5Cd*%5D%2B%2F)%7C%2F)%2B{3}*&access_token={4}";
            return string.Format(api_url, ((int)GaViewIDMap.Lunchking_17LIFE).ToString(), start_date.ToString("yyyy-MM-dd"), end_date.ToString("yyyy-MM-dd"), bid, access_token);
        }

        private static string Get17LifeGaApiUrl(string access_token, DateTime start_date, DateTime end_date, GaViewIDMap site)
        {
            string api_url = @"https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A{0}&start-date={1}&end-date={2}&metrics=ga%3Ausers%2Cga%3Aentrances%2Cga%3AentranceRate%2Cga%3Apageviews%2Cga%3AuniquePageviews%2Cga%3AavgTimeOnPage%2Cga%3Aexits%2Cga%3AexitRate%2Cga%3Asessions&access_token={3}";
            return string.Format(api_url, ((int)site).ToString(), start_date.ToString("yyyy-MM-dd"), end_date.ToString("yyyy-MM-dd"), access_token);
        }

        private static void GetGaByDeal(string access_token, Guid bid, DateTime start_date, DateTime end_date, int gid, GaViewIDMap site = GaViewIDMap.Lunchking_17LIFE, int sort = 0)
        {
            try
            {
                string result = string.Empty;
                //17life ga:6098270      17life app ga:69858944
                //抓整個網站
                //https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A6098270&start-date=2015-05-23&end-date=2015-05-23&metrics=ga%3Ausers%2Cga%3Aentrances%2Cga%3AentranceRate%2Cga%3Apageviews%2Cga%3AuniquePageviews%2Cga%3AavgTimeOnPage%2Cga%3Aexits%2Cga%3AexitRate%2Cga%3Asessions
                //單檔
                //https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A6098270&start-date={0}&end-date={1}&metrics=ga%3Ausers%2Cga%3Aentrances%2Cga%3AentranceRate%2Cga%3Apageviews%2Cga%3AuniquePageviews%2Cga%3AavgTimeOnPage%2Cga%3Aexits%2Cga%3AexitRate%2Cga%3Asessions&dimensions=ga%3ApagePath&filters=ga%3ApagePath%3D~%5E((%5B%5Cd*%5D%2B%2F)%7C%2F)%2B{2}*&access_token={3}

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(bid == Guid.Empty ? Get17LifeGaApiUrl(access_token, start_date, end_date, site) : Get17LifeGaApiUrl(access_token, bid, start_date, end_date));
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream response_stream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(response_stream))
                        {
                            result = reader.ReadToEnd();
                        }
                    }
                }
                JObject j = JObject.Parse(result);
                GaDealCollection rows = new GaDealCollection();
                JArray array = (JArray)(j["rows"]);
                bool reliable;
                if (bid != Guid.Empty)
                {
                    if (array != null)
                    {
                        foreach (var item in array)
                        {
                            List<string> innitem = item.ToObject<List<string>>();
                            GaDeal row = ParseJsonToGaDeal(innitem, out reliable);
                            if (reliable)
                            {
                                row.Type = (int)GaDealType.ByPath;
                                row.Bid = bid;
                                row.StartDate = start_date;
                                row.EndDate = end_date.AddDays(1);
                                row.Gid = gid;
                                row.Sort = sort;
                                rows.Add(row);
                            }
                        }
                    }
                }
                var summary = j["totalsForAllResults"];
                List<string> summary_item = new List<string>();
                summary_item.Add(string.Empty);
                summary_item.Add(summary["ga:users"].ToString());
                summary_item.Add(summary["ga:entrances"].ToString());
                summary_item.Add(summary["ga:entranceRate"].ToString());
                summary_item.Add(summary["ga:pageviews"].ToString());
                summary_item.Add(summary["ga:uniquePageviews"].ToString());
                summary_item.Add(summary["ga:avgTimeOnPage"].ToString());
                summary_item.Add(summary["ga:exits"].ToString());
                summary_item.Add(summary["ga:exitRate"].ToString());
                summary_item.Add(summary["ga:sessions"].ToString());
                GaDeal summary_row = ParseJsonToGaDeal(summary_item, out reliable);
                if (reliable)
                {
                    summary_row.Type = bid == Guid.Empty ? (int)GaDealType.BySite : (int)GaDealType.Summary;
                    summary_row.Bid = bid;
                    summary_row.StartDate = start_date;
                    summary_row.EndDate = end_date.AddDays(1);
                    summary_row.Gid = gid;
                    summary_row.Sort = sort;
                    rows.Add(summary_row);
                }
                mkp.GaDealSet(rows);
            }
            catch (Exception error)
            {
                LogManager.GetLogger("LogToDb").Error(string.Format("Ga Deal Error!{0},{1},{2}", bid, start_date, end_date), error);
            }
        }

        public static void GetGaBySite(GaViewIDMap site, DateTime date)
        {
            SystemCode ga_token_systemcode = CheckGaToken();
            if (!string.IsNullOrEmpty(ga_token_systemcode.CodeGroup))
            {
                if (ga_token_systemcode.ExpiredTime.HasValue && ga_token_systemcode.ExpiredTime.Value > DateTime.Now)
                {
                    GetGaByDeal(ga_token_systemcode.Code, Guid.Empty, date, date, (int)site, site);
                }
                else
                {
                    ga_token_systemcode = CheckGaToken();
                    GetGaByDeal(ga_token_systemcode.Code, Guid.Empty, date, date, (int)site, site);
                }
            }
        }

        public static void GetGaByDeal(int id)
        {
            SystemCode ga_token_systemcode = CheckGaToken();
            if (!string.IsNullOrEmpty(ga_token_systemcode.CodeGroup))
            {
                if (!ga_token_systemcode.ExpiredTime.HasValue && ga_token_systemcode.ExpiredTime.Value > DateTime.Now)
                {
                    ga_token_systemcode = CheckGaToken();
                }
                GaDealList ga = mkp.GetGaDealListById(id);
                int sort = 0;
                GetGaByDeal(ga_token_systemcode.Code, ga.Bid, ga.StartDate, ga.EndDate.AddDays(-1), id, sort: sort);
                sort++;
                int diff_days = (ga.EndDate - ga.StartDate).Days;
                for (int i = 0; i < diff_days; i++)
                {
                    GetGaByDeal(ga_token_systemcode.Code, ga.Bid, ga.StartDate.AddDays(i), ga.StartDate.AddDays(i), id, sort: sort);
                    sort++;
                }
            }
        }
        public static void GetGaByList(DateTime date)
        {
            SystemCode ga_token_systemcode = CheckGaToken();
            DateTime now = DateTime.Now;
            int sort = (now.Year - 2010) * 10000 + int.Parse(now.ToString("MMdd"));
            if (!string.IsNullOrEmpty(ga_token_systemcode.CodeGroup))
            {
                GaDealListCollection ga_list = mkp.GetGaDealListByDate(date);
                foreach (var item in ga_list)
                {
                    if (ga_token_systemcode.ExpiredTime.HasValue && ga_token_systemcode.ExpiredTime.Value > DateTime.Now)
                    {
                        GetGaByDeal(ga_token_systemcode.Code, item.Bid, date, date, item.Id, sort: sort);
                    }
                    else
                    {
                        ga_token_systemcode = CheckGaToken();
                        GetGaByDeal(ga_token_systemcode.Code, item.Bid, date, date, item.Id, sort: sort);
                    }
                }
            }
        }

        //public static void GetGaByOnlineDeal(DateTime date)
        //{
        //    //僅抓7天內開檔的
        //    List<Guid> bids = ViewPponDealManager.DefaultManager.GetDistinctBusinessHourGuid(date.AddDays(-7));
        //    SystemCode ga_token_systemcode = CheckGaToken();
        //    DateTime now = DateTime.Now;
        //    int sort = (now.Year - 2010) * 10000 + int.Parse(now.ToString("MMdd"));
        //    if (!string.IsNullOrEmpty(ga_token_systemcode.CodeGroup))
        //    {
        //        foreach (var item in bids)
        //        {
        //            if (ga_token_systemcode.ExpiredTime.HasValue && ga_token_systemcode.ExpiredTime.Value > DateTime.Now)
        //            {
        //                GetGaByDeal(ga_token_systemcode.Code, item, date, date, 0, sort: sort);
        //            }
        //            else
        //            {
        //                ga_token_systemcode = CheckGaToken();
        //                GetGaByDeal(ga_token_systemcode.Code, item, date, date, 0, sort: sort);
        //            }
        //        }
        //    }
        //}


        private static GaDeal ParseJsonToGaDeal(List<string> item, out bool reliable)
        {
            int i;
            decimal d;
            reliable = true;
            GaDeal gd = new GaDeal();

            if (item.Count == 10)
            {
                gd.PagePath = item[0];
                if (int.TryParse(item[1], out i))
                {
                    gd.Users = i;
                }
                else
                {
                    reliable = false;
                }
                if (int.TryParse(item[2], out i))
                {
                    gd.Entrances = i;
                }
                else
                {
                    reliable = false;
                }
                if (decimal.TryParse(item[3], out d))
                {
                    gd.EntranceRate = d;
                }
                else
                {
                    reliable = false;
                }
                if (int.TryParse(item[4], out i))
                {
                    gd.PageViews = i;
                }
                else
                {
                    reliable = false;
                }
                if (int.TryParse(item[5], out i))
                {
                    gd.UniquePageViews = i;
                }
                else
                {
                    reliable = false;
                }
                if (decimal.TryParse(item[6], out d))
                {
                    gd.AvgTimeOnPage = d;
                }
                else
                {
                    reliable = false;
                }
                if (int.TryParse(item[7], out i))
                {
                    gd.Exits = i;
                }
                else
                {
                    reliable = false;
                }
                if (decimal.TryParse(item[8], out d))
                {
                    gd.ExitRate = d;
                }
                else
                {
                    reliable = false;
                }
                if (int.TryParse(item[9], out i))
                {
                    gd.Sessions = i;
                }
                else
                {
                    reliable = false;
                }
            }
            return gd;
        }
    }

    public class GaQueryMatrics
    {
        public string Sessions { get; set; }
        public string Users { get; set; }

        public string GetQueryProperty()
        {
            var props = new List<string>();
            foreach (var item in typeof(GaQueryMatrics).GetProperties())
            {
                props.Add(item.Name);
            }
            return string.Join(",", props.ToArray());
        }
    }
}

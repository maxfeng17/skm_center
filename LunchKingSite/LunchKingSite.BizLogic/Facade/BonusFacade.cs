﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Transactions;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using System.Net.Mail;

namespace LunchKingSite.BizLogic.Facade
{
    public class BonusFacade
    {
        private static readonly IBonusProvider Bp;
        private static readonly IOrderProvider Op;
        private static readonly IMemberProvider Mp;
        private static readonly IPCPProvider Pcp;
        private static readonly IPponProvider Pnp;
        private static readonly ISellerProvider Sp;
        private static readonly INotificationProvider Np;
        private static readonly ISysConfProvider Conf;
        private static readonly ILog Log;

        /// <summary>
        /// 會員卡預設的背景顏色id
        /// </summary>
        public const int DefaultBackgroundColorId = 1;
        /// <summary>
        /// 會員卡預設的ICON編號
        /// </summary>
        public const int DefaultCardIconImageId = 1;
        /// <summary>
        /// 會員卡預設的背景壓圖圖片編號
        /// </summary>
        public const int DefaultCardBackgroundImageId = 1;

        static BonusFacade()
        {
            Bp = ProviderFactory.Instance().GetProvider<IBonusProvider>();
            Op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            Mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            Pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            Pnp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            Sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            Np = ProviderFactory.Instance().GetProvider<INotificationProvider>();
            Conf = ProviderFactory.Instance().GetConfig();
            Log = LogManager.GetLogger(typeof(BonusFacade));
        }

        #region PCP 身分識別碼

        /// <summary>
        /// 產生一組 Identity code 並回傳
        /// </summary>
        /// <param name="userMembershipCardId"></param>
        /// <param name="groupId"></param>
        /// <param name="expireDateTime"></param>
        /// <param name="userId"></param>
        /// <param name="expiredDays"></param>
        /// <param name="discountCodeId"></param>
        /// <returns>ViewIdentityCode</returns>
        public static ViewIdentityCode GenIdentityCode(int userMembershipCardId, int groupId, DateTime expireDateTime, int userId, int expiredDays, int? discountCodeId = null)
        {
            var result = Bp.IdentityCodeSet(userMembershipCardId, groupId, DateTime.Now.AddDays(expiredDays).SetTime(23, 59, 59), userId, discountCodeId);
            return result;
        }

        public static ViewIdentityCode GetViewIdentityCode(string code, int sellerUserId, DateTime transDateTime)
        {
            //查詢要核銷的卡
            var identityCode = Bp.ViewIdentityCodeGet(code, sellerUserId, transDateTime);
            
            //removed by Eason，現在熟客卡是by sellerGuid，以下帳號權限判斷拿掉
            ////查詢此賣家帳號所有相關的帳號（賣家、分店）
            //var vbsRelation = Sp.VbsRelationshipGet(sellerUserId);
            ////核銷不綁權限，只要有相關就可以核銷
            //if (vbsRelation.Any(x => x == identityCode.SellerUserId))
            //{
            //    return identityCode;
            //}
            //else
            //{
            //    var r = new ViewIdentityCode();
            //    r.IsLoaded = false;
            //    return r;
            //}
            //return Bp.ViewIdentityCodeGet(code, sellerUserId);
            return identityCode;
        }

        #endregion PCP 身分識別碼

        #region ViewDiscountDetail

        public static ViewDiscountDetailCollection GetViewDiscountDetail(Guid storeGuid, int userId, DiscountCampaignType type)
        {
            var campaignIds = Op.DiscountStoreGetList(storeGuid).Select(n=>n.CampaignId).ToList();
            return Op.ViewDiscountDetailGetList(userId, type, campaignIds);
        }

        #endregion

        #region ViewMemberSuperPromotionTransaction

        public static double GetUserSuperBonus(string userName)
        {
            var superBonus = Mp.ViewMemberPromotionTransactionGetListLastN(userName, ViewMemberPromotionTransaction.Columns.CreateTime, 1, MemberPromotionType.SuperBouns);
            if (superBonus.Any())
            {
                return superBonus[0].RunSum ?? 0;
            }
            return 0;
        }

        #endregion

        #region membership_card

        public static void MembershipCardSet(MembershipCard card)
        {
            Pcp.MembershipCardSet(card);
        }

        public static MembershipCard MembershipCardGet(int cardGroupId, DateTime closeTime, int level)
        {
            return Pcp.MembershipCardGet(cardGroupId, closeTime, level);
        }

        public static MembershipCard MembershipCardGet(int membershipCardId)
        {
            return Pcp.MembershipCardGet(membershipCardId);
        }

        public static MembershipCardCollection MembershipCardGetByGroupAndVersion(int cardGroupId, int versionId)
        {
            return Pcp.MembershipCardGetByGroupAndVersion(cardGroupId, versionId);
        }
        /// <summary>
        /// 異動同一群組的卡片的共用資訊
        /// </summary>
        /// <param name="cardGroupId"></param>
        /// <param name="versionId"></param>
        /// <param name="type"></param>
        /// <param name="combineUse"></param>
        /// <param name="instruction"></param>
        /// <returns></returns>
        public static bool MembershipCardUpdateSynchronizationData(int cardGroupId, int versionId, AvailableDateType type, bool combineUse)
        {
            int count = Pcp.MembershipCardUpdateSynchronizationData(cardGroupId, versionId, type, combineUse);
            return count > 0;
        }

        public static int MembershipCardLevelUpCheck(MembershipCard mc, int userMembershipCardId, int userId,Guid? pcpOrderGuid = null)
        {
            return MembershipCardLevelCheck(mc, userMembershipCardId, PcpLevelChangeType.Up, userId, pcpOrderGuid);
        }

        public static int MembershipCardLevelDownCheck(MembershipCard mc, int userMembershipCardId, int userId,Guid? pcpOrderGuid = null)
        {
            return MembershipCardLevelCheck(mc, userMembershipCardId, PcpLevelChangeType.Down, userId, pcpOrderGuid);
        }

        /// <summary>
        /// 檢核會員卡是否可升/降等，如可升/降則直接升/降等
        /// </summary>
        /// <param name="mc"></param>
        /// <param name="userMembershipCardId"></param>
        /// <param name="userId"></param>
        /// <param name="pcpOrderGuid"></param>
        /// <returns>升/降等或未升/降等的 membership_card.level</returns>
        public static int MembershipCardLevelCheck(MembershipCard mc, int userMembershipCardId, PcpLevelChangeType pcpPosLevelType, int userId, Guid? pcpOrderGuid = null)
        {

            var checkMc = Pcp.MembershipCardGetForLevelUp(userMembershipCardId, mc.VersionId, pcpPosLevelType);

            if (pcpPosLevelType==PcpLevelChangeType.Up)
            {
                
                if (checkMc.Level <= mc.Level)
                {
                    return mc.Level;
                }

                Pcp.UserMembershipCardLevelUp(userMembershipCardId, checkMc.Id);
                Mp.MembershipCardLogSet(new MembershipCardLog
                {
                    MembershipCardIdBefore = mc.Id,
                    MembershipCardIdAfter = checkMc.Id,
                    UserMembershipCardId = userMembershipCardId,
                    Memo = MembershipCardLevelUpLogMemo(checkMc.Level),
                    Type = (byte)MembershipCardLogType.System,
                    CreateId = userId,
                    CreateTime = DateTime.Now,
                    PcpOrderGuid = pcpOrderGuid
                });
                return checkMc.Level;
            }
            else
            {
                if (checkMc.Level >= mc.Level || mc.Level == (int)MembershipCardLevel.Level1)
                {
                    return mc.Level;
                }

                Pcp.UserMembershipCardLevelUp(userMembershipCardId, mc.Id);
                Mp.MembershipCardLogSet(new MembershipCardLog
                {
                    MembershipCardIdBefore = mc.Id,
                    MembershipCardIdAfter = checkMc.Id,
                    UserMembershipCardId = userMembershipCardId,
                    Memo = MembershipCardLevelDownLogMemo(checkMc.Level),
                    Type = (byte)MembershipCardLogType.System,
                    CreateId = userId,
                    CreateTime = DateTime.Now,
                    PcpOrderGuid = pcpOrderGuid
                });
                return checkMc.Level;
            }
            
        }

        public static string MembershipCardLevelUpLogMemo(int newlevel)
        {
            switch (newlevel)
            {
                case 1:
                    return "成為普卡會員";
                case 2:
                    return "升等為金卡會員";
                case 3:
                    return "升等為白金卡會員";
                case 4:
                    return "升等為鈦金卡會員";
            }
            return string.Empty;
        }

        public static string MembershipCardLevelDownLogMemo(int newlevel)
        {
            switch (newlevel)
            {
                case 1:
                    return "降為普卡會員";
                case 2:
                    return "降等為金卡會員";
                case 3:
                    return "降等為白金卡會員";
                case 4:
                    return "成等為鈦金卡會員";
            }
            return string.Empty;
        }

        #endregion

        #region membership_card_group_version

        public static bool MembershipCardGroupVersionUpdateStyle(int versionId, int backgroundColorId, int backgroundImageId, int iconImageId)
        {
            var version = Pcp.MembershipCardGroupVersionGet(versionId);
            if (!version.IsLoaded)
            {
                return false;
            }

            version.BackgroundColorId = backgroundColorId;
            version.BackgroundImageId = backgroundImageId;
            version.IconImageId = iconImageId;

            return Pcp.MembershipCardGroupVersionSet(version);
        }

        #endregion membership_card_group_version

        #region 消費者熟客卡

        public static UserMembershipCard UserMembershipCardGet(int userMembershipCardId)
        {
            return Pcp.UserMembershipCardGet(userMembershipCardId);
        }

        public static ViewUserMembershipCard UserMembershipCardVerify(int userMembershipCardId)
        {
            return Pcp.ViewUserMembershipCardGet(userMembershipCardId);
        }

        /// <summary>
        /// 取得startTime(含)到endTime(含)的UserGetCardDailyReport資料
        /// 由於系統日當天作業時間的關係，可能沒有資料
        /// </summary>
        /// <param name="cardGroupId"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static UserGetCardDailyReportCollection GetUserGetCardDailyReport(int cardGroupId, DateTime startTime, DateTime endTime)
        {

            return Pcp.UserGetCardDailyReportGetList(UserGetCardDailyReport.Columns.ReportDate
                , UserGetCardDailyReport.Columns.CardGroupId + " = " + cardGroupId
                , UserGetCardDailyReport.Columns.ReportDate + " >= " + startTime
                , UserGetCardDailyReport.Columns.ReportDate + " <= " + endTime);
        }
        #endregion 消費者熟客卡

        #region membership_card_group_store

        public static bool MembershipCardGroupStoreGuidIsExists(int groupId, Guid storeGuid)
        {
            return Pcp.MembershipCardGroupStoreGuidIsExists(groupId, storeGuid);
        }

        public static MembershipCardGroupStoreCollection MembershipCardGroupStoreGet(int groupId)
        {
            return Pcp.MembershipCardGroupStoreGet(groupId);
        }

        public static MembershipCardGroupStoreCollection MembershipCardGroupStoreGet(List<Guid> storeGuids)
        {
            return Pcp.MembershipCardGroupStoreGet(storeGuids);
        }

        #endregion membership_card_group_store

        #region view_membership_card_group_version

        public static ViewMembershipCardGroupVersion ViewMembershipCardGroupVersionGetByQuarter(int cardGroupId, int year, Quarter q)
        {
            ViewMembershipCardGroupVersionCollection rtnList = Pcp.ViewMembershipCardGroupVersionGetByQuarter(cardGroupId, year, q);
            return rtnList.FirstOrDefault();
        }
        /// <summary>
        /// 判斷有無以發行的熟客卡
        /// </summary>
        /// <param name="sellerUserId"></param>
        /// <returns></returns>
        public static bool HaveIssuedMembershipCard(int sellerUserId)
        {
            int count = Pcp.ViewMembershipCardGetCount(ViewMembershipCard.Columns.SellerUserId + " = " + sellerUserId
                , ViewMembershipCard.Columns.Status + " = " + (int)MembershipCardStatus.Open);
            return count > 0;
        }
        #endregion

        #region PCP商家核銷

        /// <summary>
        /// 
        /// </summary>
        /// <param name="umc"></param>
        /// <param name="result"></param>
        /// <param name="transDateTime"></param>
        /// <returns></returns>
        public static bool CheckUserMembershipCardStatus(UserMembershipCard umc, out ApiResult result, DateTime transDateTime)
        {
            result = new ApiResult();

            if (umc.IsLoaded)
            {
                if (!umc.Enabled)
                {
                    result.Code = ApiResultCode.MembershipCardUnable;
                    result.Message = "會員卡未啟用";
                    return false;
                }

                if (umc.StartTime > transDateTime || umc.EndTime < transDateTime)
                {
                    result.Code = ApiResultCode.MembershipCardWithoutTimeOfEffect;
                    result.Message = "會員卡不在有效期間";
                    return false;
                }
            }
            else
            {
                result.Code = ApiResultCode.MembershipCardNotFound;
                result.Message = "請檢查會員卡是否已過期或失效。";
                return false;
            }

            return true;
        }

        /// <summary>
        /// 檢核會員卡可使用狀態
        /// </summary>
        /// <param name="mc"></param>
        /// <param name="result"></param>
        /// <param name="transDateTime"></param>
        /// <returns></returns>
        public static bool CheckMembershipCardStatus(MembershipCard mc, out ApiResult result, DateTime transDateTime)
        {
            result = new ApiResult();
            
            if (mc.IsLoaded)
            {
                if (!mc.Enabled)
                {
                    result.Code = ApiResultCode.MembershipCardUnable;
                    result.Message = "會員卡未啟用";
                    return false;
                }

                //熟客卡是否上架，以membership_card_group status
                //if (mc.Status != (byte)MembershipCardStatus.Open)
                //{
                //    result.Code = ApiResultCode.MembershipCardUnopened;
                //    result.Message = "會員卡未上架";
                //    return false;
                //}

                if (mc.OpenTime > transDateTime || mc.CloseTime < transDateTime)
                {
                    result.Code = ApiResultCode.MembershipCardWithoutOpenTime;
                    result.Message = "會員卡不在上架期間";
                    return false;
                }

                switch ((AvailableDateType)mc.AvailableDateType)
                {
                    case AvailableDateType.ExceptWeekend:
                        if (transDateTime.DayOfWeek == DayOfWeek.Saturday || transDateTime.DayOfWeek == DayOfWeek.Sunday)
                        {
                            result.Code = ApiResultCode.MembershipCardNotUsingSaturdayOrSunday;
                            result.Message = "會員卡六日不可使用";
                            return false;
                        }
                        break;
                    case AvailableDateType.ExceptSpecialHoliday:
                    {
                        var holiday = GetVacation();
                        if (holiday.IsLoaded)
                        {
                            result.Code = ApiResultCode.MembershipCardCanNotUsingExceptSpecialHoliday;
                            result.Message = string.Format("會員卡特殊假日不可使用({0})", holiday.Name);
                            return false;
                        }
                        break;
                    }
                    case AvailableDateType.ExceptWeekendAndSpecialHoliday:
                    {
                        if (transDateTime.DayOfWeek == DayOfWeek.Saturday || transDateTime.DayOfWeek == DayOfWeek.Sunday)
                        {
                            result.Code = ApiResultCode.MembershipCardNotUsingSaturdayOrSunday;
                            result.Message = "會員卡六日不可使用";
                            return false;
                        }

                        var holiday = GetVacation();
                        if (holiday.IsLoaded)
                        {
                            result.Code = ApiResultCode.MembershipCardCanNotUsingExceptSpecialHoliday;
                            result.Message = string.Format("會員卡六日與特殊假日不可使用({0})", holiday.Name);
                            return false;
                        }
                        break;
                    }
                }
            }
            else
            {
                result.Code = ApiResultCode.MembershipCardNotFound;
                result.Message = "請檢查會員卡是否已過期或失效。";
                return false;
            }

            return true;
        }

        /// <summary>
        /// 檢核 view_identity_code 是否可核銷
        /// </summary>
        /// <param name="code"></param>
        /// <param name="result"></param>
        /// <param name="transDateTime"></param>
        /// <returns></returns>
        public static bool CheckViewIdentityCode(ViewIdentityCode code, out ApiResult result, DateTime transDateTime)
        {
            result = new ApiResult();

            if (!code.IsLoaded)
            {
                result.Code = ApiResultCode.DataNotFound;
                result.Message = string.Format("無法識別您會員卡的資料，識別碼({0})", code);
                return false;
            }

            if (code.ExpiredTime != null && transDateTime > code.ExpiredTime)
            {
                result.Code = ApiResultCode.PcpCodeIsExpired;
                result.Message = "交易碼已過期";
                return false;
            }

            switch ((IdentityCodeStatus)code.Status)
            {
                case IdentityCodeStatus.Used:
                    result.Code = ApiResultCode.PcpCodeIsUsed;
                    result.Message = "交易碼已使用過";
                    return false;
                case IdentityCodeStatus.Cancel:
                    result.Code = ApiResultCode.PcpCodeIsCancel;
                    result.Message = "交易碼已作廢";
                    return false;
            }

            return true;
        }

        public static ViewIdentityCode GetViewIdentityCode(int identityCodeId)
        {
            return Bp.ViewIdentityCodeGet(identityCodeId);
        }

        public static PcpCheckoutResult PcpTransCheckOut(long identityCodeId, int cardId, double? amount, double discountAmount
            , int? discountCodeId, int discountCodeAmount, double superBonus, int userId, int sellerUserId, int memberCardId, Guid storeGuid, out Guid pcpOrderGuid)
        {
            if (superBonus > 0 && superBonus > ((amount ?? 0) - discountAmount - discountCodeAmount))
            {
                pcpOrderGuid = Guid.Empty;
                return PcpCheckoutResult.SuperBonusOverAmount;
            }

            //建立訂單
            pcpOrderGuid = Guid.NewGuid();

            var order = new PcpOrder
            {
                Guid = pcpOrderGuid,
                UserId = userId,
                CardId = cardId,
                Amount = (decimal?)amount,
                DiscountAmount = (decimal)discountAmount,
                DiscountCodeId = discountCodeId,
                BonusPoint = (decimal)superBonus,
                OrderTime = DateTime.Now,
                Status = (int)PcpOrderStatus.Normal,
                IdentityCodeId = identityCodeId,
                StoreGuid = storeGuid,
                CreateId = sellerUserId,
                OrderType = (int)PcpOrderType.MembershipCard
            };

            var tranOpts = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted
            };
            using (var trans = new TransactionScope(TransactionScopeOption.RequiresNew, tranOpts))
            {
                try
                {
                    var identityCode = Pcp.IdentityCodeGet(identityCodeId);
                    if (identityCode.Status != (byte) IdentityCodeStatus.Create)
                    {
                        Transaction.Current.Rollback();
                        return identityCode.Status == (byte) IdentityCodeStatus.Used
                            ? PcpCheckoutResult.PcpCodeIsUsed
                            : PcpCheckoutResult.PcpCodeIsCancel;
                    }

                    Pcp.PcpOrderSet(order);

                    #region 抵用券 discont_code (熟客券 or 公關券)

                    if (discountCodeId != null && discountCodeId != 0)
                    {
                        var code = Op.DiscountCodeGetById((int)discountCodeId);
                        var campaign = Op.DiscountCampaignGet(code.CampaignId ?? 0);

                        if (!code.IsLoaded || !campaign.IsLoaded || code.UseTime != null || campaign.CancelTime != null 
                            || campaign.ApplyTime == null || campaign.StartTime > DateTime.Now || campaign.EndTime < DateTime.Now
                            || ((DiscountCodeType)campaign.Type != DiscountCodeType.RegularsTicket 
                            && (DiscountCodeType)campaign.Type != DiscountCodeType.FavorTicket))
                        {
                            Transaction.Current.Rollback();
                            return PcpCheckoutResult.UpdateDiscountCodeFail;
                        }

                        code.UseTime = DateTime.Now;
                        code.UseAmount = campaign.Amount;
                        code.OrderGuid = pcpOrderGuid;
                        code.OrderCost = (decimal?)amount;
                        code.OrderClassification = (int)OrderClassification.PcpOrder;
                        code.UseId = userId;

                        Op.DiscountCodeSet(code);

                        var pointType = (DiscountCodeType)campaign.Type == DiscountCodeType.RegularsTicket
                            ? PcpPointType.RegularsPoint
                            : PcpPointType.FavorPoint;

                        var tempDiscountCodeAmount = discountCodeAmount;

                        #region 同步依據點數扣除商家熟客點與公關點

                        var vppd = Pcp.ViewPcpPointDepositGetOnTimeData(sellerUserId);

                        var widthdrawOverall = new PcpPointWithdrawalOverall
                        {
                            Type = (byte)PcpPointWithdrawalOverallType.PcpTransaction,
                            OrderGuid = pcpOrderGuid,
                            AssignmentId = null,
                            Point = (int)discountAmount,
                            PointType = (byte)pointType,
                            CreateId = sellerUserId,
                            CreateTime = DateTime.Now,
                            UserId = sellerUserId
                        };

                        Pcp.PcpPointWithdrawalOverallSet(widthdrawOverall);

                        while (tempDiscountCodeAmount > 0)
                        {
                            int withdrawalPoint;
                            int? depositId = null;
                            if (vppd.Any())
                            {
                                var deposit = vppd.OrderByAsc(ViewPcpPointDeposit.Columns.ExpireTime).First();
                                var remainderMoney = deposit.Point - deposit.WithdrawalPoint ?? 0;
                                depositId = deposit.Id;
                                if (tempDiscountCodeAmount > remainderMoney)
                                {
                                    withdrawalPoint = remainderMoney;
                                    tempDiscountCodeAmount = tempDiscountCodeAmount - remainderMoney;
                                }
                                else
                                {
                                    withdrawalPoint = (int)tempDiscountCodeAmount;
                                    tempDiscountCodeAmount = 0;
                                }
                                vppd.Remove(deposit);
                            }
                            else
                            {
                                withdrawalPoint = (int)tempDiscountCodeAmount;
                                tempDiscountCodeAmount = 0;
                            }

                            var withdrawal = new PcpPointWithdrawal
                            {
                                DepositId = depositId,
                                Type = (byte)pointType,
                                Point = withdrawalPoint,
                                OrderGuid = pcpOrderGuid,
                                CreateId = sellerUserId,
                                CreateTime = DateTime.Now,
                                UserId = sellerUserId,
                                OverallId = widthdrawOverall.Id
                            };

                            Pcp.PcpPointWithdrawalSet(withdrawal);
                        }

                        #endregion 同步依據點數扣除商家熟客點與公關點

                        #region 扣除商家預扣點數 pcp_lock_point

                        var locks = Pcp.PcpLockPointGetOnTime(sellerUserId, pointType);
                        if (locks.Any())
                        {
                            tempDiscountCodeAmount = discountCodeAmount;
                            while (tempDiscountCodeAmount > 0)
                            {
                                var lockPoint = locks.OrderByAsc(PcpLockPoint.Columns.CreateTime).First();
                                if (lockPoint.VerificationPoint + tempDiscountCodeAmount > lockPoint.LockPoint)
                                {
                                    tempDiscountCodeAmount = tempDiscountCodeAmount - (lockPoint.LockPoint - lockPoint.VerificationPoint);
                                    lockPoint.VerificationPoint = lockPoint.LockPoint;
                                }
                                else
                                {
                                    lockPoint.VerificationPoint = lockPoint.VerificationPoint + (int)tempDiscountCodeAmount;
                                    tempDiscountCodeAmount = 0;
                                }
                                Pcp.PcpLockPointSet(lockPoint);
                            }
                        }

                        #endregion 扣除商家預扣點數 pcp_lock_point
                    }

                    #endregion 抵用券 discont_code (熟客券 or 公關券)

                    #region 消費者紅利金折抵

                    if (superBonus > 0)
                    {
                        var userName = Mp.MemberUserNameGet(userId);
                        var userSuperBonus = Mp.ViewMemberPromotionTransactionGetListLastN(userName, ViewMemberPromotionTransaction.Columns.CreateTime, 1, MemberPromotionType.SuperBouns);

                        if (userSuperBonus.Any() && (userSuperBonus[0].RunSum ?? 0) > superBonus)
                        {
                            var sellerUserName = Mp.MemberUserNameGet(sellerUserId);

                            //扣除消費者超級紅利
                            var pw = new MemberPromotionWithdrawal
                            {
                                DepositId = userSuperBonus[0].Id,
                                PromotionValue = superBonus,
                                OrderGuid = pcpOrderGuid,
                                Type = (int)BonusTransactionType.OrderAmtRedeeming,
                                Action = "pcp-折抵超級紅利金",
                                CreateTime = DateTime.Now,
                                CreateId = userName,
                                OrderClassification = (int)OrderClassification.PcpOrder
                            };
                            Mp.MemberPromotionWithdrawalSet(pw);

                            //增加商家累積的超級紅利
                            var pd = new MemberPromotionDeposit
                            {
                                StartTime = DateTime.Now,
                                ExpireTime = Helper.GetNextYearLastDay(DateTime.Now),
                                PromotionValue = superBonus,
                                CreateTime = DateTime.Now,
                                OrderGuid = pcpOrderGuid,
                                Action = string.Format("消費{0}元，使用超級紅利金抵用{1}元", amount ?? 0, superBonus),
                                CreateId = sellerUserName,
                                OrderClassification = (int)OrderClassification.PcpOrder,
                                UserId = sellerUserId,
                                VendorPaymentChangeId = null,
                                Type = (int)MemberPromotionType.SuperBouns
                            };
                            Mp.MemberPromotionDepositSet(pd);
                        }
                        else
                        {
                            Transaction.Current.Rollback();
                            return PcpCheckoutResult.NotEnoughBonus;
                        }
                    }

                    #endregion 消費者紅利金折抵

                    #region 熟客卡(會員卡)更新

                    Pcp.UserMembershipCardUpdateOrderCount(cardId, amount ?? 0);

                    #endregion 熟客卡(會員卡)更新

                    //region 更新 identity_code status
                    Pcp.IdentityCodeSetStatus(identityCodeId, IdentityCodeStatus.Used);

                    trans.Complete();
                }
                catch (Exception)
                {
                    Transaction.Current.Rollback();
                    return PcpCheckoutResult.PcpOrderTransactionFail;
                }
            }

            return PcpCheckoutResult.Success;
        }

        #endregion PCP商家核銷

        #region vacation

        public static Vacation GetVacation()
        {
            return Pnp.GetTodayVacation();
        }

        #endregion vacation

        #region store

        public static SellerCollection StoreGetByUserId(int userId)
        {
            return Pcp.AllowStoreGetList(userId);
        }

        #endregion

        #region 商家系統餘額與紀錄

        public static List<IEnumerable<ViewPcpPointRecord>> GetStorePcpPointRecordsGroupByType(int userId)
        {
            List<IEnumerable<ViewPcpPointRecord>> collections = new List<IEnumerable<ViewPcpPointRecord>>();
            IEnumerable<ViewPcpPointRecord> records = Pcp.ViewPcpPointRecordGetList(ViewPcpPointRecord.Columns.UserId, userId).ToList();

            foreach (PcpPointType type in (PcpPointType[]) Enum.GetValues(typeof (PcpPointType)))
            {
                collections.Add(records.Where(r => r.PointType == (byte)type));
            }
                        
            return collections;
        }

        public static int GetStoreLockedPointByPointType(int userId, PcpPointType theType)
        {
            PcpLockPointCollection theLockPointCollection = Pcp.PcpLockPointGetList(PcpLockPoint.Columns.UserId, userId);
            int theInitialLockedPoint =
                theLockPointCollection.Where(p => p.PointType == (byte)theType)
                    .Sum(p => p.LockPoint);
            int theRedeemedPoint =
                theLockPointCollection.Where(p => p.PointType == (byte)theType)
                    .Sum(p => p.VerificationPoint);
            int theRemainedLockedPoint = theInitialLockedPoint - theRedeemedPoint;
            //yap the remained locked point shouldn't be negative, give it zero in case wrong data processing
            return theRemainedLockedPoint < 0 ? 0 : theRemainedLockedPoint; 
        }

        public static List<int> GetStoreLockedPointList(int userId)
        {
            List<int> theList = new List<int>(); 
            PcpLockPointCollection theLockPointCollection = Pcp.PcpLockPointGetList(PcpLockPoint.Columns.UserId, userId);
            int theInitialLockedPoint, theRedeemedPoint, theRemainedLockedPoint;

            foreach (PcpPointType type in (PcpPointType[])Enum.GetValues(typeof(PcpPointType)))
            {
                theInitialLockedPoint =
                theLockPointCollection.Where(p => p.PointType == (byte)type)
                    .Sum(p => p.LockPoint);
                theRedeemedPoint =
                    theLockPointCollection.Where(p => p.PointType == (byte)type)
                        .Sum(p => p.VerificationPoint);
                theRemainedLockedPoint = theInitialLockedPoint - theRedeemedPoint;
                //yap the remained locked point shouldn't be negative, give it zero in case wrong data processing
                theList.Add(theRemainedLockedPoint < 0 ? 0 : theRemainedLockedPoint);
            }

            return theList;
        }

        public static ViewPcpDiscountCampaignLockPointCollection GetStorePcpLockPointListOrderByStartTime(int userId)
        {
            return Pcp.ViewPcpDiscountCampaignLockPointGetList(ViewPcpDiscountCampaignLockPoint.Columns.UserId, userId)
                .OrderByDesc(ViewPcpDiscountCampaignLockPoint.Columns.StartTime);
        }

        #endregion 商家系統餘額與紀錄

        #region ViewMembershipCard

        public static ViewMembershipCard ViewMembershipCatdGet(int membershipCardId)
        {
            return Pcp.ViewMembershipCardGet(membershipCardId);
        }

        #endregion ViewMembershipCard

        #region 商家異動會員卡

        public static bool CheckMembershipCardGroupIdAndVersionId(Guid sellerGuid, int sellerUserId, int cardGroupId, int cardVersionId,
            DateTime openTime, DateTime closeTime, out int groupId, out int versionId)
        {
            groupId = cardGroupId <= 0 ? 0 : cardGroupId;
            versionId = cardVersionId <= 0 ? 0 : cardVersionId;

            var mcg = Pcp.MembershipCardGroupGetSet(sellerGuid, true);
            
            if (groupId == 0)
            {
                groupId = mcg.Id;
            }
            else if (groupId != mcg.Id)
            {
                return false;
            }

            if (versionId == 0)
            {
                var cardVersion = new MembershipCardGroupVersion
                {
                    CardGroupId = groupId,
                    Status = (int)MembershipCardStatus.Draft,
                    OpenTime = openTime,
                    CloseTime = closeTime,
                    ImagePath = string.Empty,
                    BackgroundColorId = DefaultBackgroundColorId,
                    BackgroundColor = string.Empty,
                    BackgroundImageId = DefaultCardBackgroundImageId,
                    BackgroundImage = string.Empty,
                    IconImageId = DefaultCardIconImageId,
                    IconImage = string.Empty
                };
                //查詢有無舊的MembershipCardGroupVersion,目前卡片風格相關欄位一律套用前一版設定
                MembershipCardGroupVersion version = Pcp.MembershipCardGroupVersionGetCardGroupLastData(cardGroupId);
                if (version.IsLoaded)
                {
                    cardVersion.ImagePath = version.ImagePath;
                    cardVersion.BackgroundColorId = version.BackgroundColorId;
                    cardVersion.BackgroundColor = version.BackgroundColor;
                    cardVersion.BackgroundImageId = version.BackgroundImageId;
                    cardVersion.BackgroundImage = version.BackgroundImage;
                    cardVersion.IconImageId = version.IconImageId;
                    cardVersion.IconImage = version.IconImage;
                }

                Pcp.MembershipCardGroupVersionSet(cardVersion);
                versionId = cardVersion.Id;
            }
            else
            {
                MembershipCardGroupVersion version = Pcp.MembershipCardGroupVersionGet(versionId);
                //查無此版本
                if (!version.IsLoaded)
                {
                    return false;
                }
                //群組編號與傳入的不同
                if (version.CardGroupId != groupId)
                {
                    return false;
                }
                //群組編號與傳入的openTime closeTime不同
                if (version.CloseTime.CompareTo(closeTime) != 0)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool MembershipCardGroupStoreReset(int groupId, List<Guid> guids)
        {
            if (guids.Count == 0)
            {
                return true;
            }

            Pcp.MembershipCardGroupStoreDel(groupId);

            var storeCol = new MembershipCardGroupStoreCollection();

            storeCol.AddRange(guids.Select(x => new MembershipCardGroupStore
            {
                CardGroupId = groupId,
                StoreGuid = x
            }));

            Pcp.MembershipCardGroupStoreSet(storeCol);
            return true;
        }

        public static MembershipCardGroup GetMembershipCardGroupBySellerUserId(Guid sellerGuid)
        {
            return Pcp.MembershipCardGroupGetSet(sellerGuid, false);
        }
        #endregion

        #region Discount Campaign/Code/Template/Store 熟客券公關券

        public static int DiscountCampaignCloneTemplate(PcpAssignment assignment)
        {
            var template = Pcp.DiscountTemplateGet(assignment.DiscountTemplateId ?? 0);
            if (!template.IsLoaded)
            {
                return 0;
            }

            var seller = Mp.MemberGet(template.SellerUserId);
            var tickerType = (PcpPointType) assignment.PcpPointType == PcpPointType.FavorPoint
                ? PcpQueueTickerType.F
                : PcpQueueTickerType.R;

            var campaign = new DiscountCampaign
            {
                Name = assignment.Subject,
                Amount = template.Amount,
                Qty = 0,
                ApplyTime = DateTime.Now,
                StartTime = template.StartTime,
                EndTime = template.EndTime,
                CreateTime = DateTime.Now,
                CreateId = seller.UserName,
                ApplyId = seller.UserName,
                MinimumAmount = (int?)template.MinimumAmount,
                CardGroupId = assignment.MembershipCardGroupId,
                Type = (int)DiscountCampaignTypeParse((PcpPointType)assignment.PcpPointType),
                TemplateId = template.Id,
                SellerUserId = seller.UniqueId,
                AvailableDateType = template.AvailableDateType,
                CardCombineUse = template.CardCombineUse,
                CampaignNo = PcpQueueTickerDispenser(tickerType)
            };

            campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.Partner;
            campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.MinimumAmount;

            Op.DiscountCampaignSet(campaign);
            Op.DiscountStoreCloneByTemplate(template.Id, campaign.Id);

            return campaign.Id;
        }

        public static bool DiscountCodeCreate(int discountCampaignId, List<int> userId, AssignmentMemberType type)
        {
            var campaign = Op.DiscountCampaignGet(discountCampaignId);
            var seller = Mp.MemberGet(campaign.SellerUserId ?? 0);
            if (!seller.IsLoaded)
            {
                return false;
            }

            foreach (var t in userId)
            {
                var code = new DiscountCode
                {
                    CampaignId = campaign.Id,
                    Amount = campaign.Amount,
                    Code = PromotionFacade.GetDiscountCode(campaign.Amount ?? 0),
                };

                if (type == AssignmentMemberType.SellerMember)
                {
                    var sellerMember = Sp.ViewVbsSellerMemberGet(null, t, seller.UniqueId);
                    if (sellerMember.IsLoaded && sellerMember.UserId != null)
                    {
                        code.Owner = sellerMember.UserId;
                    }
                }
                else
                {
                    var member = Mp.MemberGet(t);
                    if (member.IsLoaded)
                    {
                        code.Owner = member.UniqueId;
                    }
                }

                Op.DiscountCodeSet(code);
            }

            campaign.Qty = userId.Count;
            Op.DiscountCampaignSet(campaign);

            return true;
        }

        public static DiscountCampaignType DiscountCampaignTypeParse(PcpPointType type)
        {
            switch (type)
            {
                case PcpPointType.RegularsPoint:
                    return DiscountCampaignType.RegularsTicket;
                case PcpPointType.FavorPoint:
                    return DiscountCampaignType.FavorTicket;
                default:
                    return DiscountCampaignType.None;
            }
        }

        public static DiscountTemplate DiscountTemplateGet(int templateId)
        {
            return Pcp.DiscountTemplateGet(templateId);
        }

        public static ApiResultCode GiveDiscountCodeToMember(int executeUserId, int sellerUserId, ViewUserMembershipCard card, DiscountTemplate template, PcpPointType type)
        {
            var subject = template.MinimumAmount == null || template.MinimumAmount == 0
                ? string.Format("出示熟客券折抵{0}元", template.Amount)
                : string.Format("滿{0}元，出示熟客券折抵{1}元", template.MinimumAmount, template.Amount);

            var assignment = new PcpAssignment
            {
                UserId = sellerUserId,
                AssignmentType = (int) AssignmentType.PushMsg17Life,
                SendType = (int) AssignmentSendType.Push,
                MessageType = (int) AssignmentMessageType.DiscountCode,
                Subject = subject,
                Message = subject,
                MembershipCardGroupId = card.CardGroupId,
                ExecutionType = (int) AssignmentExecutionType.Instant,
                ExecutionTime = DateTime.Now,
                CreateTime = DateTime.Now,
                CreateId = executeUserId,
                Status = (int) AssignmentStatus.NewAssign,
                SendCount = 1,
                MemberType = (byte) AssignmentMemberType.MembershipCardMember,
                DiscountTemplateId = template.Id,
                PcpPointType = (byte) type
            };
            Np.PcpAssignmentSet(assignment);

            var filter = new PcpAssignmentFilter
            {
                AssignmentId = assignment.Id,
                FilterType = (byte)AssignmentFilterType.UserMembershipCardId,
                OperatorX = (byte)Operators.IsEqualTo,
                ParameterFirst = card.Id.ToString(CultureInfo.InvariantCulture)
            };
            Np.PcpAssignmentFilterSet(filter);

            var utility = new PcpAssignmentUtility(assignment.Id);

            if (!utility.InsertPcpAssignmentMember())
            {
                return ApiResultCode.PushPcpInsertUserListFail;
            }

            //是否啟用推播
            if (!Conf.AppNotificationJob)
            {
                return ApiResultCode.PushPcpDisable;
            }

            var returnValue = ApiResultCode.Success;

            try
            {
                //推播動作
                NotificationFacade.PushPcpAssignment();
            }
            catch (Exception ex)
            {
                Log.Error("AppPCPNotificationJob Error:" + ex.Message + "<br/>" + ex.StackTrace);
                returnValue = ApiResultCode.PushPcpFail;
            }

            return returnValue;
        }

        #endregion

        #region seller_member

        public static SellerMember SellerMemberGet(int? sellerMemberId)
        {
            return sellerMemberId == null ? new SellerMember() : Pcp.SellerMemberGet((int) sellerMemberId);
        }

        #endregion

        #region PCP點數管理(熟客點、公關點儲值退款及超級紅利金請款)

        public static bool PcpTransactionOrderSet(PcpPointTransactionOrder pcpPointTransactionOrder, string sellerAcountId, string invoiceTitle, string invoiceCompanyId, string invoiceName, string invoiceAddress)
        {
            var vbsmembership = Mp.VbsMembershipGetByAccountId(sellerAcountId);
            if (vbsmembership.IsLoaded && vbsmembership.UserId.HasValue)
            {
                pcpPointTransactionOrder.SellerUserId = vbsmembership.UserId.Value;
                Guid orderGuid = Pcp.PcpPointTransactionOrderSet(pcpPointTransactionOrder);
                if (orderGuid == default(Guid))
                {
                    return false;
                }

                //熟客點 Deposit
                if (pcpPointTransactionOrder.RegularsPoint > 0)
                {
                    PcpPointDeposit regularsPcpPoint = new PcpPointDeposit();
                    regularsPcpPoint.Point = pcpPointTransactionOrder.RegularsPoint;
                    regularsPcpPoint.Type = (int)PcpPointType.RegularsPoint;
                    regularsPcpPoint.Message = string.Format("儲值 {0} 元，增加 {1} 熟客點， 贈送 {2} 公關點", pcpPointTransactionOrder.TransactionAmount, pcpPointTransactionOrder.RegularsPoint, pcpPointTransactionOrder.FavorPoint);
                    regularsPcpPoint.StartTime = DateTime.Now;
                    regularsPcpPoint.UserId = vbsmembership.UserId.Value;
                    //隔年年底
                    regularsPcpPoint.ExpireTime = regularsPcpPoint.OriginalExpireTime = new DateTime(DateTime.Now.AddYears(2).Year, 1, 1);
                    regularsPcpPoint.CreateId = pcpPointTransactionOrder.CreateId;
                    regularsPcpPoint.CreateTime = DateTime.Now;
                    regularsPcpPoint.OrderGuid = orderGuid;

                    Pcp.PcpPointDepositSet(regularsPcpPoint);
                }

                //公關點 Deposit
                if (pcpPointTransactionOrder.FavorPoint > 0)
                {
                    PcpPointDeposit favorPcpPoint = new PcpPointDeposit();
                    favorPcpPoint.Point = pcpPointTransactionOrder.FavorPoint;
                    favorPcpPoint.Type = (int)PcpPointType.FavorPoint;
                    favorPcpPoint.Message = string.Format("儲值 {0} 元，增加 {1} 熟客點， 贈送 {2} 公關點", pcpPointTransactionOrder.TransactionAmount, pcpPointTransactionOrder.RegularsPoint, pcpPointTransactionOrder.FavorPoint);
                    favorPcpPoint.StartTime = DateTime.Now;
                    favorPcpPoint.UserId = vbsmembership.UserId.Value;
                    //隔年年底
                    favorPcpPoint.ExpireTime = favorPcpPoint.OriginalExpireTime = new DateTime(DateTime.Now.AddYears(2).Year, 1, 1);
                    favorPcpPoint.CreateId = pcpPointTransactionOrder.CreateId;
                    favorPcpPoint.CreateTime = DateTime.Now;
                    favorPcpPoint.OrderGuid = orderGuid;

                    Pcp.PcpPointDepositSet(favorPcpPoint);
                }

                #region 產生發票流程

                //新增transaction
                string transId = PaymentFacade.GetTransactionId();

                //PCP熟客點儲值目前為人工做確認作業
                //現在製作的transaction都是用ATM匯款成功來製作
                //請相信我們的財會吧
                //參考PaymentFacade.CtAtmLogSave  這個是AP2AP接收後的動作

                PaymentFacade.NewTransaction(transId, orderGuid, LunchKingSite.Core.PaymentType.ATM, pcpPointTransactionOrder.TransactionAmount
                    , null, PayTransType.Charging, DateTime.Now, pcpPointTransactionOrder.CreateId.ToString(), "PCP熟客點儲值", (int)PayTransPhase.Successful
                    , PayTransResponseType.OK, OrderClassification.RegularsOrder);

                //熟客點預設都是三聯
                //ORDERID先寫PCP訂單編號
                //ORDERITEM先寫PCP熟客點訂單
                //isPponItem 先當憑證設false
                //status 初始 假設ATM都成功
                if (pcpPointTransactionOrder.EinvoiceMode == (int)InvoiceMode2.Triplicate)
                {
                    //三聯式
                    EinvoiceFacade.SetEinvoiceMain(orderGuid, pcpPointTransactionOrder.Id.ToString(), DateTime.Now, invoiceCompanyId, invoiceTitle, invoiceName, invoiceAddress, InvoiceMode2.Triplicate, transId, pcpPointTransactionOrder.Description
                    , null, pcpPointTransactionOrder.TransactionAmount, false, vbsmembership.UserId.Value, MemberFacade.GetUserName(pcpPointTransactionOrder.CreateId), true, OrderClassification.RegularsOrder
                    , CarrierType.None, EinvoiceType.Initial);
                }else
                {
                    //二聯式
                    EinvoiceFacade.SetEinvoiceMain(orderGuid, pcpPointTransactionOrder.Id.ToString(), DateTime.Now, string.Empty, string.Empty, string.Empty, string.Empty, InvoiceMode2.Duplicate, transId, pcpPointTransactionOrder.Description
                    , null, pcpPointTransactionOrder.TransactionAmount, false, vbsmembership.UserId.Value, MemberFacade.GetUserName(pcpPointTransactionOrder.CreateId), true, OrderClassification.RegularsOrder
                    , CarrierType.None, EinvoiceType.Initial);
                }

                #endregion 產生發票流程

                //發送儲值點數信件

                var orderMailInfo = new PcpPointTransactionOrderMailInformation
                {
                    SellerName = vbsmembership.Name,
                    ShowRegularsContent = pcpPointTransactionOrder.RegularsPoint>0,
                    RegularsPoint = pcpPointTransactionOrder.RegularsPoint,
                    FavorPoint = pcpPointTransactionOrder.FavorPoint
                };
                if (!string.IsNullOrEmpty(vbsmembership.Email))
                {
                    SendPcpTransactionOrderMail(vbsmembership.Email, orderMailInfo);
                }


                return true;
            }


            return false;
        }
        public static PcpPointTransactionOrder PcpTransactionOrderOverageGet(string sellerAcountId, int pcpPointTransactionOrderId)
        {
            PcpPointTransactionOrder overageTransactionOrder = Pcp.PcpPointTransactionOrderGet(pcpPointTransactionOrderId);
            var vbsmembership = Mp.VbsMembershipGetByAccountId(sellerAcountId);
            if (vbsmembership.IsLoaded && vbsmembership.UserId.HasValue && overageTransactionOrder.IsLoaded && vbsmembership.UserId == overageTransactionOrder.SellerUserId)
            {
                //取得view_Pcp_point_deposit資訊(熟客點、公關點)

                var viewPcpPointDepositCols = Pcp.ViewPcpPointDepositGetOnTimeData(vbsmembership.UserId.Value);
                if (viewPcpPointDepositCols != null)
                {
                    var viewPcpPointDepositList = viewPcpPointDepositCols.Where(x => x.OrderGuid == overageTransactionOrder.Guid).ToList();

                    foreach (var viewPcpPointDeposit in viewPcpPointDepositList)
                    {
                        //檢查訂單熟客點剩餘點數
                        if (viewPcpPointDeposit.Type == (int)PcpPointType.RegularsPoint)
                        {
                            if (viewPcpPointDeposit.WithdrawalPoint.HasValue)
                            {
                                overageTransactionOrder.RegularsPoint -= viewPcpPointDeposit.WithdrawalPoint.Value;
                            }

                        }//檢查訂單公關點剩餘點數
                        else if (viewPcpPointDeposit.Type == (int)PcpPointType.FavorPoint)
                        {
                            if (viewPcpPointDeposit.WithdrawalPoint.HasValue)
                            {
                                overageTransactionOrder.FavorPoint -= viewPcpPointDeposit.WithdrawalPoint.Value;
                            }

                        }
                    }
                    overageTransactionOrder.TransactionAmount = Math.Round((decimal)overageTransactionOrder.RegularsPoint / 3, 0);

                }
            }


            return overageTransactionOrder;
        }
        public static bool GeneratePcpTransactionRefund(string sellerAcountId, int pcpPointTransactionOrderId, string refundDescription, int createId)
        {
            List<ViewPcpPointDeposit> viewPcpPointDepositList = new List<ViewPcpPointDeposit>();
            PcpPointTransactionOrder overageOrder = Pcp.PcpPointTransactionOrderGet(pcpPointTransactionOrderId);
            var vbsmembership = Mp.VbsMembershipGetByAccountId(sellerAcountId);
            
            
            if (vbsmembership.IsLoaded && vbsmembership.UserId.HasValue && overageOrder.IsLoaded && vbsmembership.UserId == overageOrder.SellerUserId)
            {
                #region  取得view_Pcp_point_deposit資訊(熟客點、公關點)

                //取得view_Pcp_point_deposit資訊(熟客點、公關點)
                var viewPcpPointDepositCols = Pcp.ViewPcpPointDepositGetOnTimeData(vbsmembership.UserId.Value);
                if (viewPcpPointDepositCols != null)
                {
                    viewPcpPointDepositList = viewPcpPointDepositCols.Where(x => x.OrderGuid == overageOrder.Guid).ToList();

                    foreach (var viewPcpPointDeposit in viewPcpPointDepositList)
                    {
                        //檢查訂單熟客點剩餘點數
                        if (viewPcpPointDeposit.Type == (int)PcpPointType.RegularsPoint)
                        {
                            if (viewPcpPointDeposit.WithdrawalPoint.HasValue)
                            {
                                overageOrder.RegularsPoint -= viewPcpPointDeposit.WithdrawalPoint.Value;
                            }

                        }//檢查訂單公關點剩餘點數
                        else if (viewPcpPointDeposit.Type == (int)PcpPointType.FavorPoint)
                        {
                            if (viewPcpPointDeposit.WithdrawalPoint.HasValue)
                            {
                                overageOrder.FavorPoint -= viewPcpPointDeposit.WithdrawalPoint.Value;
                            }

                        }
                    }
                    overageOrder.TransactionAmount = Math.Round((decimal)overageOrder.RegularsPoint / 3, 0);

                }

                #endregion  取得view_Pcp_point_deposit資訊(熟客點、公關點)
            }

            if (overageOrder != null && viewPcpPointDepositList.Count > 0 && overageOrder.TransactionAmount + overageOrder.RegularsPoint + overageOrder.FavorPoint > 0 && vbsmembership.UserId.HasValue)
            {
                #region GeneratePcpPointTransactionRefund

                PcpPointTransactionRefund refund = new PcpPointTransactionRefund
                                                   {
                                                       Guid = Guid.NewGuid(),
                                                       PcpPointTransactionOrderId = overageOrder.Id,
                                                       OrderStatus = (byte) PcpTransactionOrderStatus.Success,
                                                       TransactionAmount = overageOrder.TransactionAmount,
                                                       RegularsPoint = overageOrder.RegularsPoint,
                                                       //FavorPoint = overageOrder.FavorPoint,
                                                       FavorPoint = 0,
                                                       Description =refundDescription + string.Format("申請退款 退回 {0} 元，扣除 {1} 熟客點",
                                                                    overageOrder.TransactionAmount,
                                                                    overageOrder.RegularsPoint),
                                                       CreateTime = DateTime.Now,
                                                       CreateId = createId
                                                   };
                Pcp.PcpPointTransactionRefundSet(refund);


                foreach (var viewPcpPointDeposit in viewPcpPointDepositList)
                {
                    if (viewPcpPointDeposit.Type == (int) PcpPointType.RegularsPoint && overageOrder.RegularsPoint > 0)
                    {
                        #region  產生熟客點負向總覽表及熟客點負向

                        //產生熟客點負向總覽表
                        var widthdrawOverall = new PcpPointWithdrawalOverall
                                               {
                                                   Type = (byte) PcpPointWithdrawalOverallType.PcpTransaction,
                                                   OrderGuid = refund.Guid,
                                                   AssignmentId = null,
                                                   Point = overageOrder.RegularsPoint,
                                                   PointType = (byte) PcpPointType.RegularsPoint,
                                                   CreateId = createId,
                                                   CreateTime = DateTime.Now,
                                                   UserId = vbsmembership.UserId.Value
                                               };

                        Pcp.PcpPointWithdrawalOverallSet(widthdrawOverall);

                        //產生 熟客點 withdrawal
                        var pcpRegularsPointWithdrawal = new PcpPointWithdrawal
                                                         {
                                                             DepositId = viewPcpPointDeposit.Id,
                                                             Type = (byte) PcpPointType.RegularsPoint,
                                                             Point = overageOrder.RegularsPoint,
                                                             Message =
                                                                 string.Format("申請退款 退回 {0} 元，扣除 {1} 熟客點",
                                                                     overageOrder.TransactionAmount,
                                                                     overageOrder.RegularsPoint),
                                                             OrderGuid = widthdrawOverall.OrderGuid,
                                                             OverallId = widthdrawOverall.Id,
                                                             CreateId = createId,
                                                             CreateTime = DateTime.Now
                                                         };


                        Pcp.PcpPointWithdrawalSet(pcpRegularsPointWithdrawal);

                        #endregion #region  產生熟客點負向總覽表及熟客點負向
                    }
                    else if (viewPcpPointDeposit.Type == (int) PcpPointType.FavorPoint && overageOrder.FavorPoint > 0)
                    {
                        #region 產生公關點負向總覽表及公關點負向

                        //產生公關點負向總覽表
                        var widthdrawOverall = new PcpPointWithdrawalOverall
                                               {
                                                   Type = (byte) PcpPointWithdrawalOverallType.PcpTransaction,
                                                   OrderGuid = refund.Guid,
                                                   AssignmentId = null,
                                                   Point = overageOrder.FavorPoint,
                                                   PointType = (byte) PcpPointType.FavorPoint,
                                                   CreateId = createId,
                                                   CreateTime = DateTime.Now,
                                                   UserId = vbsmembership.UserId.Value
                                               };

                        Pcp.PcpPointWithdrawalOverallSet(widthdrawOverall);

                        //產生 公關點 withdrawal
                        var pcpFavorPointWithdrawal = new PcpPointWithdrawal
                                                      {
                                                          DepositId = viewPcpPointDeposit.Id,
                                                          Type = (byte) PcpPointType.FavorPoint,
                                                          Point = overageOrder.FavorPoint,
                                                          Message =
                                                              string.Format("申請退款 退回 {0} 元，扣除 {1} 熟客點",
                                                                  overageOrder.TransactionAmount,
                                                                  overageOrder.RegularsPoint),
                                                          OrderGuid = widthdrawOverall.OrderGuid,
                                                          OverallId = widthdrawOverall.Id,
                                                          CreateId = createId,
                                                          CreateTime = DateTime.Now
                                                      };


                        Pcp.PcpPointWithdrawalSet(pcpFavorPointWithdrawal);

                        #endregion 產生公關點負向總覽表及公關點負向
                    }
                }

                //todo: 註銷發票流程

                //發送退款點數信件
                var refundMailInfo = new PcpPointTransactionRefundMailInformation
                {
                    SellerName = vbsmembership.Name,
                    RefundAmount = (int)overageOrder.TransactionAmount
                };
                if (!string.IsNullOrEmpty(vbsmembership.Email))
                {
                    SendPcpTransactionRefundMail(vbsmembership.Email, refundMailInfo);
                }

                return true;

                #endregion GeneratePcpPointTransactionRefund
            }
            return false;
        }
        public static bool GeneratePcpTransactionExchangeOrder(string sellerAcountId, PcpPointTransactionExchangeOrder pcpPointTransactionExchangeOrder, int createId)
        {
            var vbsmembership = Mp.VbsMembershipGetByAccountId(sellerAcountId);
            if (vbsmembership.IsLoaded && vbsmembership.UserId.HasValue)
            {
                pcpPointTransactionExchangeOrder.SellerUserId = vbsmembership.UserId.Value;
                Pcp.PcpPointTransactionExchangeOrderSet(pcpPointTransactionExchangeOrder);

                //金流動作-待確認

                //超級紅利金扣項
               var withdrawalcols = MemberFacade.GetDeductibleMemberPromotionDeposit(vbsmembership.UserId.Value,
                    pcpPointTransactionExchangeOrder.SuperBonus, pcpPointTransactionExchangeOrder.Description,
                    BonusTransactionType.OrderAmtRedeeming, pcpPointTransactionExchangeOrder.Guid, MemberFacade.GetUserName(createId), false,
                    false, OrderClassification.RegularsOrder, null, MemberPromotionType.SuperBouns);


               if (withdrawalcols != null &&  withdrawalcols.Count > 0)
               {
                   Mp.MemberPromotionWithdrawalCollectionSet(withdrawalcols);
               }

               //發送超級紅利金請款通知信件
               var exchangeMailInfo = new PcpPointTransactionExchangeOrderMailInformation()
               {
                   SellerName = vbsmembership.Name,
                   ExchangeAmount = (int)pcpPointTransactionExchangeOrder.TransactionAmount
               };
                if (!string.IsNullOrEmpty(vbsmembership.Email))
                {
                    SendPcpTransactionExchangeOrderMail(vbsmembership.Email, exchangeMailInfo);
                }
                return true;
            }
            return false;
        }

        public static double TotalSuperBonusAmountGetByAccountId(string sellerAcountId)
        {
            var vbsmembership = Mp.VbsMembershipGetByAccountId(sellerAcountId);
            if (vbsmembership.IsLoaded && vbsmembership.UserId.HasValue)
            {

                var memberPromotion = Mp.ViewMemberPromotionTransactionGetListLastN(vbsmembership.UserId.Value, ViewMemberPromotionTransaction.Columns.CreateTime, 1, MemberPromotionType.SuperBouns);

                if (memberPromotion != null && memberPromotion.Count==1)
                {
                    return memberPromotion.First().RunSum??  0;
                }
            }
            return 0;
        }

        public static KeyValuePair<double,int> ExchangeabledSuperBonusAmountGetByAccountId(string sellerAcountId)
        {
            var exchangeSuperBounsAmount = Conf.ExchangeSuperBounsAmount;
            var exchangeSuperBounsRatio = Conf.ExchangeSuperBounsRatio;
            var vbsmembership = Mp.VbsMembershipGetByAccountId(sellerAcountId);

            double memberPromotionSuperBonusAmount =default(double);
            double memberPromotionExchangeableSuperBonus = default(double);
            int memberPromotionExchangeableAmount = default(int);

            if (vbsmembership.IsLoaded && vbsmembership.UserId.HasValue)
            {
                var memberPromotion = Mp.ViewMemberPromotionTransactionGetListLastN(vbsmembership.UserId.Value, ViewMemberPromotionTransaction.Columns.CreateTime, 1, MemberPromotionType.SuperBouns);

                if (memberPromotion != null && memberPromotion.Count == 1)
                {
                   memberPromotionSuperBonusAmount =  memberPromotion.First().RunSum ?? 0;

                   memberPromotionExchangeableSuperBonus = (Math.Floor(memberPromotionSuperBonusAmount / exchangeSuperBounsAmount)) * exchangeSuperBounsAmount;
                   memberPromotionExchangeableAmount = Convert.ToInt16(Math.Floor(memberPromotionExchangeableSuperBonus * (double)exchangeSuperBounsRatio));

                   return new KeyValuePair<double, int>(memberPromotionExchangeableSuperBonus, memberPromotionExchangeableAmount);
                } 

            }
            return new KeyValuePair<double, int>(0,0);
        }

        #endregion PCP點數管理 PCP點數管理(熟客點、公關點儲值退款及超級紅利金請款)

        #region PCP點數管理通知信

        public static void SendPcpTransactionOrderMail(string toMail, PcpPointTransactionOrderMailInformation mailInfo)
        {
            MailMessage msg = new MailMessage();
            var template = TemplateFactory.Instance().GetTemplate<PcpPointTransactionOrderMail>();
            template.SiteUrl = Conf.SiteUrl;
            template.SiteServiceUrl = Conf.SiteServiceUrl;
            template.SellerName = mailInfo.SellerName;
            template.ShowRegularsContent = (mailInfo.RegularsPoint > 0);
            template.RegularsPoint =mailInfo.RegularsPoint;
            template.FavorPoint = mailInfo.FavorPoint;
            

            msg.From = new MailAddress(Conf.ServiceEmail, Conf.ServiceName);
            msg.To.Add(toMail);
            msg.Subject = ((mailInfo.RegularsPoint > 0) ? "儲值完成" : "公關點發送") + "通知(" + mailInfo.SellerName + ")";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }

        public static void SendPcpTransactionRefundMail(string toMail, PcpPointTransactionRefundMailInformation mailInfo)
        {
            MailMessage msg = new MailMessage();
            var template = TemplateFactory.Instance().GetTemplate<PcpPointTransactionRefundMail>();
            template.SiteUrl = Conf.SiteUrl;
            template.SiteServiceUrl = Conf.SiteServiceUrl;
            template.SellerName = mailInfo.SellerName;
            template.RefundAmount = mailInfo.RefundAmount;

            msg.From = new MailAddress(Conf.ServiceEmail, Conf.ServiceName);
            msg.To.Add(toMail);
            msg.Subject = "退款完成通知(" + mailInfo.SellerName + ")";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }

        public static void SendPcpTransactionExchangeOrderMail(string toMail, PcpPointTransactionExchangeOrderMailInformation mailInfo)
        {
            MailMessage msg = new MailMessage();
            var template = TemplateFactory.Instance().GetTemplate<PcpPointTransactionExchangeMail>();
            template.SiteUrl = Conf.SiteUrl;
            template.SiteServiceUrl = Conf.SiteServiceUrl;
            template.SellerName = mailInfo.SellerName;
            template.ExchangeAmount = mailInfo.ExchangeAmount;

            msg.From = new MailAddress(Conf.ServiceEmail, Conf.ServiceName);
            msg.To.Add(toMail);
            msg.Subject = "請款完成通知(" + mailInfo.SellerName + ")";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }

        #endregion PCP點數管理通知信

        #region pcp_queue_ticker

        /// <summary>
        /// PCP發號機
        /// </summary>
        public static string PcpQueueTickerDispenser(PcpQueueTickerType type)
        {
            var today = DateTime.Today;
            return string.Format("{0}{1}{2}", type.ToString(), today.ToString("yyMMdd"),
                Pcp.PcpQueueTicketGetNumber(type.ToString(), today).ToString(CultureInfo.InvariantCulture).PadLeft(4, '0'));
        }

        #endregion

        #region 建立PCP測試資料

        public static int CreateDiscountCampaign4PCPTest(int userId)
        {
            var card = Pcp.ViewMembershipCardGroupVersionGetByQuarter(userId, 2014, Quarter.Quarter4);
            var template = Pcp.ViewPcpDiscountTemplateGetList(userId, 1, null, null);
            Np.PcpAssignmentSet(new PcpAssignment
            {
                UserId = userId,
                Status = 0,
                AssignmentType = 1,
                SendType = 1,
                MessageType = 1,
                //test data, if no data let throw exception
                MembershipCardGroupId = card.First().CardGroupId,
                ExecutionType = 1,
                ExecutionTime = DateTime.Now,
                StartTime = DateTime.Now,
                CompleteTime = DateTime.Now,
                CreateTime = DateTime.Now,
                CreateId = userId,
                Message = "test",
                SendCount = 100,
                MemberType = 3,
                Subject = "test",
                DiscountTemplateId = template.First().Id
            });

            var assign = Np.PcpAssignmentGetList(userId, DateTime.Now.AddMinutes(-1), DateTime.Now.AddMinutes(1));
            return assign.First().Id;
        }

        #endregion

        #region PCP POS Trans Log

        public static bool PcpPosTransLogSet(PcpPosTransLog pcpPosTransLog, int transType, string storeCode, string storeName, Guid sellerGuid, Guid storeGuid, int indentityCode,Guid orderGuid,JsonSerializer inputdata,string result,DateTime createTime)
        {

            return false;
        }

        #endregion
    }
}

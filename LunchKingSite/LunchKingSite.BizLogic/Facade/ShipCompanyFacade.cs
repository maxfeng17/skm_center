﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.SellerModels;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Constant;
using LunchKingSite.DataOrm;
using System;
using System.Linq;
using System.Resources;
using System.Text.RegularExpressions;

namespace LunchKingSite.BizLogic.Facade
{
    public class ShipCompanyFacade
    {

        private static IPponProvider pp;
        private static ISellerProvider sp;
        private static ILocationProvider lp;
        private static IOrderProvider op;

        static ShipCompanyFacade()
        {
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        private ShipCompanyFacade()
        { }

        public static PagerList<ShipCompany> GetViewShipCompanyList(string searchItem,string searchValue,
            bool showLKSeller,int page)
        {
            int pageSize = 15;
            int count;
            PagerList<ShipCompany> result = new PagerList<ShipCompany>(
               op.ShipCompanyGetList(searchItem, searchValue, showLKSeller, page, pageSize, out count),
               new PagerOption(pageSize, (page - 1) * pageSize),
               page - 1,
               count);
            return result;
        }
    }
}

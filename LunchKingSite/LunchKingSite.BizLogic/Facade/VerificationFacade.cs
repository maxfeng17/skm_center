﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Verification;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using log4net;

namespace LunchKingSite.BizLogic.Facade
{
    public class VerificationFacade
    {
        private static IVerificationProvider vp;
        private static IMemberProvider mp;
        private static ISysConfProvider cp;
        private static IPponProvider pp;

        static VerificationFacade()
        {
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            cp = ProviderFactory.Instance().GetConfig();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        #region 宣告

        #endregion
        
        #region Web版

        #endregion

        #region Piinlife

        #endregion

        #region Method
        #region 建立帳號
        /// <summary>
        /// 重設全部的帳密資料
        /// </summary>
        /// <param name="intType">
        /// 0:全部都要新增/重設 <br />
        /// 1:只新增新增加的seller/store<br />
        /// </param>
        /// <returns>
        /// -1:錯誤;  0:什麼都沒做;  1:完成
        /// </returns>
        public int SetAllInfo(int intType)
        {
            var va = new VerifyAccount();
            int result;
            switch (intType)
            {
                case 0: 
                    //不判斷店家與分店的密碼是否有重覆
                    result = va.SetAllInfo(false, false);
                    break;
                case 1:
                    //要判斷店家與分店的密碼是否有重覆
                    result = va.SetAllInfo(true, true);
                    break;
                default:
                    result = 0;
                    break;
            }
            return result;
        }
        #endregion
        #endregion

        /// <summary>
        /// 公布欄訊息要公布的基本條件
        /// </summary>
        /// <param name="currentPage">現在第幾頁</param>
        /// <param name="pageSize">一頁有幾筆資料</param>
        /// <param name="IsShowDeleted">是否顯示刪除的資料</param>
        /// <param name="IsShowExpire">是否顯示未達公布時間的資料</param>
        /// <param name="filter">其它條件</param>
        public static VbsBulletinBoardCollection BulletinBoardGetListByAnnounce(int currentPage, int pageSize, bool IsShowDeleted, bool IsShowExpire, string filter)
        {
            return vp.VbsBulletinBoardGetListPaging(currentPage, pageSize,
                VbsBulletinBoard.Columns.IsSticky + " desc, " + VbsBulletinBoard.Columns.PublishTime + " desc",
                IsShowDeleted ? string.Empty : VbsBulletinBoard.Columns.PublishStatus + "=" + (int)VbsBulletinBoardPublishStatus.Enabled,
                IsShowExpire ? string.Empty : VbsBulletinBoard.Columns.PublishTime + "<" + DateTime.Now.AddDays(1).ToShortDateString(), filter);
        }

        public static VendorBillingAccountResourceModel VendorBillingStoreDealModelGet(string accountId)
        {
            VendorBillingAccountResourceModel model = new VendorBillingAccountResourceModel();
            model.StoreGuids = vp.StoreGuidsGetListByAccountId(accountId);
            model.Products = vp.DealGuidsGetListByAccountId(accountId);
            return model;
        }

        public static PagerList<VbsVerificationCouponInfo> GetPagerVbsVerificationCouponInfos(
            List<VbsVerificationCouponInfo> infos,
            VbsVerificationShowType verifyState, DateTime? dateFrom, DateTime? dateTo,
            int pageSize, int pageIndex, string sortCol, bool sortDesc, string accountId)
        {
            var query = (from t in infos select t);


            if (verifyState == VbsVerificationShowType.Verified)
            {
                query = query.Where(t => t.VerifyState == VbsCouponVerifyState.Verified).ToList();
                if (dateFrom != null)
                {
                    query = query.Where(t => t.VerifiedDateTime >= dateFrom.Value);
                }
                if (dateTo != null)
                {
                    query = query.Where(t => t.VerifiedDateTime <= Helper.GetFinalTime(dateTo.Value));
                }
            }
            else if (verifyState == VbsVerificationShowType.Unused)
            {
                query = query.Where(t => t.VerifyState == VbsCouponVerifyState.UnUsed).ToList();
            }
            else if (verifyState == VbsVerificationShowType.Return)
            {
                query = query.Where(t => t.VerifyState == VbsCouponVerifyState.Return).ToList();
                if (dateFrom != null)
                {
                    query = query.Where(t => t.ReturnDateTime >= dateFrom.Value);
                }
                if (dateTo != null)
                {
                    query = query.Where(t => t.ReturnDateTime <= Helper.GetFinalTime(dateTo.Value));
                }
            }
            else if (verifyState == VbsVerificationShowType.Returning)
            {
                query = query.Where(t => t.VerifyState == VbsCouponVerifyState.Returning).ToList();
                if (dateFrom != null)
                {
                    query = query.Where(t => t.ReturnDateTime >= dateFrom.Value);
                }
                if (dateTo != null)
                {
                    query = query.Where(t => t.ReturnDateTime <= Helper.GetFinalTime(dateTo.Value));
                }
            }
            else if (verifyState == VbsVerificationShowType.SysExpiredVerified)
            {
                query = query.Where(t => t.VerifyState == VbsCouponVerifyState.ExpiredVerified).ToList();
                if (dateFrom != null)
                {
                    query = query.Where(t => t.ReturnDateTime >= dateFrom.Value);
                }
                if (dateTo != null)
                {
                    query = query.Where(t => t.ReturnDateTime <= Helper.GetFinalTime(dateTo.Value));
                }
            }

            if (sortDesc == false)
            {
                switch (sortCol)
                {
                    case VbsVerificationCouponInfo.Columns.CouponId:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenBy(t => t.CouponId) ;
                        break;
                    case VbsVerificationCouponInfo.Columns.ProductName:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenBy(t => t.ProductName);
                        break;
                    case VbsVerificationCouponInfo.Columns.StoreName:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenBy(t => t.StoreName);
                        break;
                    case VbsVerificationCouponInfo.Columns.VerifyState:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenBy(t => t.VerifyState);
                        break;
                    case VbsVerificationCouponInfo.Columns.VerifiedDateTime:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenBy(t => t.VerifiedDateTime);
                        break;
                    case VbsVerificationCouponInfo.Columns.ReturnDateTime:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenBy(t => t.ReturnDateTime);
                        break;
                    case VbsVerificationCouponInfo.Columns.UserName:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenBy(t => t.UserName);
                        break;
                }
            }
            else 
            {
                switch (sortCol)
                {
                    case VbsVerificationCouponInfo.Columns.CouponId:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenByDescending(t => t.CouponId);
                        break;
                    case VbsVerificationCouponInfo.Columns.ProductName:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenByDescending(t => t.ProductName);
                        break;
                    case VbsVerificationCouponInfo.Columns.StoreName:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenByDescending(t => t.StoreName);
                        break;
                    case VbsVerificationCouponInfo.Columns.VerifyState:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenByDescending(t => t.VerifyState);
                        break;
                    case VbsVerificationCouponInfo.Columns.VerifiedDateTime:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenByDescending(t => t.VerifiedDateTime);
                        break;
                    case VbsVerificationCouponInfo.Columns.ReturnDateTime:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenByDescending(t => t.ReturnDateTime);
                        break;
                    case VbsVerificationCouponInfo.Columns.UserName:
                        query = query.OrderByDescending(t => !string.IsNullOrEmpty(t.UserName) && t.UserName.Equals(accountId)).ThenByDescending(t => t.UserName);
                        break;
                }
            }

            var paging = new PagerList<VbsVerificationCouponInfo>(
                query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                new PagerOption(pageSize, pageIndex * pageSize, sortCol, sortDesc),
                pageIndex,
                query.ToList().Count);

            return paging;
        }

        public static MemoryStream GetVbsVerificationCouponInfosExcel(List<VbsVerificationCouponInfo> couponInfos)
        {
            DataTable dt = new DataTable();
            DataRow drHeader = dt.NewRow();
            DataColumn dc0 = new DataColumn("憑證編號");
            dt.Columns.Add(dc0);
            drHeader[0] = dc0.ColumnName;
            DataColumn dc1 = new DataColumn("商品名稱");
            dt.Columns.Add(dc1);
            drHeader[1] = dc1.ColumnName;
            DataColumn dc2 = new DataColumn("分店");
            dt.Columns.Add(dc2);
            drHeader[2] = dc2.ColumnName;
            DataColumn dc3 = new DataColumn("狀態");
            dt.Columns.Add(dc3);
            drHeader[3] = dc3.ColumnName;
            DataColumn dc4 = new DataColumn("退貨時間");
            dt.Columns.Add(dc4);
            drHeader[4] = dc4.ColumnName;
            DataColumn dc5 = new DataColumn("核銷時間");
            dt.Columns.Add(dc5);
            drHeader[5] = dc5.ColumnName;
            DataColumn dc6 = new DataColumn("核銷帳號");
            dt.Columns.Add(dc6);
            drHeader[6] = dc6.ColumnName;
            dt.Rows.Add(drHeader);

            //var verifyStateMessage = new Dictionary<VbsCouponVerifyState, string>();
            //verifyStateMessage.Add(VbsCouponVerifyState.Null, "");
            //verifyStateMessage.Add(VbsCouponVerifyState.Verified, "已核銷");
            //verifyStateMessage.Add(VbsCouponVerifyState.UnUsed, "未核銷");
            //verifyStateMessage.Add(VbsCouponVerifyState.Return, "完成退貨");
            //verifyStateMessage.Add(VbsCouponVerifyState.Returning, "退貨中");
            foreach (var m in couponInfos)
            {
                string verifyStateMessage = Helper.GetEnumDescription(m.VerifyState);
                DataRow dr = dt.NewRow();
                dr[0] = m.CouponId;
                dr[1] = m.ProductName;
                dr[2] = m.StoreName;
                dr[3] = (m.VerifyState == VbsCouponVerifyState.UnUsed) ? ((m.IsBooked != null && m.IsBooked.Value) ? "未核銷(已預約)" : (m.IsReservationLock != null && m.IsReservationLock.Value) ? "未核銷(已鎖定)" : verifyStateMessage) : verifyStateMessage;
                dr[4] = m.ReturnDateTime;
                dr[5] = m.VerifiedDateTime;
                dr[6] = m.UserName;
                dt.Rows.Add(dr);
            }

            Workbook workbook = new HSSFWorkbook();

            // 新增試算表。
            Sheet sheet = workbook.CreateSheet("核銷查詢報表");
            
            int rowIndex = 0;
            foreach (DataRow row in dt.Rows)
            {
                Row dataRow = sheet.CreateRow(rowIndex);
                foreach (DataColumn column in dt.Columns)
                {
                    Cell c = dataRow.CreateCell(column.Ordinal);
                    c.SetCellValue(row[column].ToString());
                }
                rowIndex++;
            }
            //adjust column width
            for (int i = 0; i < sheet.GetRow(sheet.FirstRowNum).LastCellNum; i++)
            {
                sheet.AutoSizeColumn(i);
                int width = sheet.GetColumnWidth(i);
                sheet.SetColumnWidth(i, width + 200);
            }

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        public static MemoryStream GetVbsVerificationCouponInfosExcelByStore(List<VbsVerificationCoupon> couponInfos, string mailTo)
        {
            DataTable dt = new DataTable();
            DataRow drHeader = dt.NewRow();
            DataColumn dc0 = new DataColumn("商品名稱");
            dt.Columns.Add(dc0);
            drHeader[0] = dc0.ColumnName;
            DataColumn dc1 = new DataColumn("核銷張數");
            dt.Columns.Add(dc1);
            drHeader[1] = dc1.ColumnName;
            DataColumn dc2 = new DataColumn("憑證編號");
            dt.Columns.Add(dc2);
            drHeader[2] = dc2.ColumnName;
            DataColumn dc3 = new DataColumn("核銷時間");
            dt.Columns.Add(dc3);
            drHeader[3] = dc3.ColumnName;
            DataColumn dc4 = new DataColumn("核銷帳號");
            dt.Columns.Add(dc4);
            drHeader[4] = dc4.ColumnName;
            DataColumn dc5 = new DataColumn("分店");
            dt.Columns.Add(dc5);
            drHeader[5] = dc5.ColumnName;
           
            dt.Rows.Add(drHeader);

            foreach (var m in couponInfos)
            {
                DataRow dr = dt.NewRow();
                dr[0] = m.DealName;
                dr[1] = m.VerifyCount;
                dr[2] = m.CouponId;
                dr[3] = m.VerifiedDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
                dr[4] = m.VerifyAccount;
                dr[5] = m.VerifiedStore;
                dt.Rows.Add(dr);
            }

            Workbook workbook = new HSSFWorkbook();

            // 新增試算表。
            Sheet sheet = workbook.CreateSheet("核銷查詢報表");

            int rowIndex = 0;
            foreach (DataRow row in dt.Rows)
            {
                Row dataRow = sheet.CreateRow(rowIndex);
                foreach (DataColumn column in dt.Columns)
                {
                    Cell c = dataRow.CreateCell(column.Ordinal);
                    c.SetCellValue(row[column].ToString());
                }
                rowIndex++;
            }
            //adjust column width
            for (int i = 0; i < sheet.GetRow(sheet.FirstRowNum).LastCellNum; i++)
            {
                sheet.AutoSizeColumn(i);
                int width = sheet.GetColumnWidth(i);
                sheet.SetColumnWidth(i, width + 200);
            }

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);

            if (!string.IsNullOrEmpty(mailTo))
            {
                try
                {
                    var now = DateTime.Now;
                    var workStream = new MemoryStream();
                    using (var zip = new Ionic.Zip.ZipFile(Encoding.Default))
                    {
                        var bytes = new byte[outputStream.Length];
                        outputStream.Read(bytes, 0, bytes.Length);
                        zip.AddEntry(HttpUtility.UrlEncode(string.Format("{0}-VerificationCouponList.xls", now.ToString("yyyyMMddHHmm"))), bytes);
                        zip.Save(workStream);
                    }
                    workStream.Position = 0;

                    var msg = new MailMessage();
                    var template = TemplateFactory.Instance().GetTemplate<Core.Component.Template.VerificationCouponListMail>();
                    string[] arrMailTo = mailTo.Split(",");

                    if (arrMailTo.Length != 3)
                    {
                        throw new Exception("Email輸入格式有誤!");
                    }
                    if (string.IsNullOrEmpty(arrMailTo[2]))
                    {
                        return null;
                    }

                    template.exportTime = now.ToString("yyyy/MM/dd HH:mm:ss");
                    template.sellerName = arrMailTo[1];
                    AddMsgTo(msg, arrMailTo[2]);
                    msg.Subject = string.Format("17Life核銷紀錄《{0}》", arrMailTo[1]);
                    msg.SubjectEncoding = Encoding.UTF8;

                    //發送前先寫一筆request記錄 若有商家反應收不到信件問題 觀察歷程使用 實際發送之後 會依收件mail另寫一筆紀錄
                    var mailLogger = log4net.LogManager.GetLogger("MailLog");
                    string toAddrs = string.Join(",", msg.To.Select(t => t.Address).ToArray());
                    mailLogger.InfoFormat("Request:{0} 使用核銷紀錄匯至Email功能，收件人mail-{1}，信件主旨-{2}", arrMailTo[0], toAddrs, msg.Subject);

                    msg.From = new MailAddress(cp.ServiceEmail, cp.ServiceName);
                    msg.IsBodyHtml = true;
                    msg.Body = Convert.ToString(template);
                    msg.Attachments.Add(new Attachment(workStream, HttpUtility.UrlEncode(string.Format("{0}-VerificationCouponList.zip", now.ToString("yyyyMMddHHmm"))), "application/zip"));
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    msg.Attachments.Dispose();

                    return null;
                }
                catch (Exception ex)
                {
                    throw new Exception("進行匯出核銷紀錄至信箱作業出現異常狀況:" + ex.Message);
                }
            }

            return outputStream;
        }

        public static MemoryStream GetVbsVerificationSellerListExcel(string uniqueId, ViewPponCouponCollection vpsCols)
        {
            int i = 0;
            List<string> Headers = new List<string>(new string[]{"序號", "憑證編號", "訂購人姓名", "好康憑證確認碼", "使用日期", "簽名"});
            DataTable dt = new DataTable();
            
            #region Headers
            //首行, 標示檔號和份數
            DataRow dr0 = dt.NewRow();
            foreach (string item in Headers) 
            {
                DataColumn dc = new DataColumn(item);
                dt.Columns.Add(dc);
                i++;
            }
            dr0[0] = "本次活動清冊名單共 " + vpsCols.Count + " 份";
            dr0[5] = "檔號：" + uniqueId;
            dt.Rows.Add(dr0);

            //標題行, 顯示各欄位標題
            i = 0;
            DataRow drHeader = dt.NewRow();
            foreach (string item in Headers) {
                drHeader[i] = item;
                i++;
            }
            dt.Rows.Add(drHeader);
            #endregion

            #region Body
            i = 1;
            foreach (ViewPponCoupon c in vpsCols)
            {
                DataRow dr = dt.NewRow();
                //序號
                dr[0] = i.ToString().PadLeft(4, '0');
                dr[1] = c.SequenceNumber;
                dr[2] = c.MemberName;
                dt.Rows.Add(dr);
                i++;
            }
            #endregion

            Workbook workbook = new HSSFWorkbook();

            #region Style
            // 建立儲存格樣式。
            CellStyle style = workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;
            style.BorderBottom = CellBorderType.THICK;
            style.BorderLeft = CellBorderType.THICK;
            style.BorderRight = CellBorderType.THICK;
            style.BorderTop = CellBorderType.THICK;
            style.WrapText = true;

            Font cellFont = workbook.CreateFont();
            cellFont.Boldweight = (short)FontBoldWeight.BOLD;
            style.SetFont(cellFont);
            #endregion

            // 新增試算表
            Sheet sheet = workbook.CreateSheet("商家清冊報表");

            int rowIndex = 0;
            foreach (DataRow row in dt.Rows)
            {
                Row dataRow = sheet.CreateRow(rowIndex);
                dataRow.Height = 20 * 30;
                foreach (DataColumn column in dt.Columns)
                {
                    Cell c = dataRow.CreateCell(column.Ordinal);
                    
                    c.SetCellValue(row[column].ToString());
                    c.CellStyle = style;
                }
                rowIndex++;
            }
            //adjust column width
            for (i = 0; i < sheet.GetRow(sheet.FirstRowNum).LastCellNum; i++)
            {
                sheet.AutoSizeColumn(i);
                int width = sheet.GetColumnWidth(i);
                sheet.SetColumnWidth(i, width + 2000);
            }

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        /// <summary>
        /// 可核銷憑證
        /// </summary>
        /// <param name="member"></param>
        /// <param name="partialCouponSequence"></param>
        /// <param name="couponCode"></param>
        /// <param name="trustId">cash_trust_log.trust_id 取憑證</param>
        /// <param name="allowedDealUnits"></param>
        /// <param name="isPezEvent"></param>
        /// <returns></returns>
        public static IEnumerable<VerificationCouponInfo> GetAccessibleCoupon(VbsMembership member, string partialCouponSequence, 
            string couponCode, Guid trustId, List<IVbsVendorAce> allowedDealUnits = null,bool isPezEvent = false)
        {
            List<VerifiedCouponData> pponCouponInfo;
            List<VerifiedCouponData> piinCouponInfo;

            if (allowedDealUnits == null)
            {
                allowedDealUnits = VBSFacade.GetAllowedDealUnits(member);
            }

            bool is17LifeEmployee = member.VbsMembershipAccountType == VbsMembershipAccountType.VerificationAccount;

            #region initialize CouponInfo

            if (is17LifeEmployee)
            {
                pponCouponInfo = vp.GetVerifiedCouponDataWithPpon(partialCouponSequence, couponCode, trustId, isPezEvent).ToList();
                piinCouponInfo = vp.GetVerifiedCouponDataWithPiinLife(partialCouponSequence, couponCode, trustId, isPezEvent).ToList();
            }
            else
            {
                var pponAllowedDeals = allowedDealUnits.Where(x => x.IsPponDeal != null && x.IsPponDeal.Value).ToList();
                pponCouponInfo = TryGetPponCouponInfo(partialCouponSequence, couponCode, trustId, pponAllowedDeals, isPezEvent);
                
                var piinAllowedDeals = allowedDealUnits.Where(x => x.IsPiinDeal != null && x.IsPiinDeal.Value).ToList();
                piinCouponInfo = TryGetPiinCouponInfo(partialCouponSequence, couponCode, trustId, piinAllowedDeals, isPezEvent);
            }

            #endregion

            DateTime now = DateTime.Now;
            VbsCouponVerifyState verifyState = VbsCouponVerifyState.Null;
            List<VerificationCouponInfo> couponInfos = new List<VerificationCouponInfo>();

            #region Ppon
            foreach (VerifiedCouponData couponInfo in pponCouponInfo)
            {
                switch (couponInfo.Status)
                {
                    case ((int)TrustStatus.Initial):
                    case ((int)TrustStatus.Trusted):
                        verifyState = VbsCouponVerifyState.UnUsed;
                        break;
                    case ((int)TrustStatus.Verified):
                        verifyState = VbsCouponVerifyState.Verified;
                        break;
                    case ((int)TrustStatus.Refunded):
                    case ((int)TrustStatus.Returned):
                        verifyState = VbsCouponVerifyState.Return;
                        break;
                    case ((int)TrustStatus.ATM):
                        break;
                }
                //強制退貨 視為已核銷
                if ((couponInfo.SpecialStatus & (int)TrustSpecialStatus.ReturnForced) > 0)
                {
                    verifyState = VbsCouponVerifyState.Verified;
                }

                //Atm退款中 視為已退貨
                if ((couponInfo.SpecialStatus & (int)TrustSpecialStatus.AtmRefunding) > 0)
                {
                    verifyState = VbsCouponVerifyState.Return;
                }

                VerificationCouponInfo info = new VerificationCouponInfo
                {
                    DealName = couponInfo.DealName,
                    ItemName = couponInfo.ItemName.Replace("&nbsp", " "),  //避免App顯示  &nbsp 字元
                    DealId = couponInfo.DealId.ToString(),
                    DealType = BusinessModel.Ppon,
                    CouponSequence = couponInfo.CouponSequence,
                    CouponCode = couponInfo.CouponCode,
                    ExchangePeriodStartTime = couponInfo.UseStartTime,
                    ExchangePeriodEndTime = couponInfo.UseEndTime,
                    VerifyState = verifyState,
                    ModifyTime = couponInfo.ModifyTime,
                    BuyerName = couponInfo.MemberName,
                    TrustId = couponInfo.TrustId
                };

                couponInfos.Add(info);
            }
            #endregion
            #region 品生活

            foreach (VerifiedCouponData couponInfo in piinCouponInfo)
            {
                switch (couponInfo.Status)
                {
                    case ((int)TrustStatus.Initial):
                    case ((int)TrustStatus.Trusted):
                        verifyState = VbsCouponVerifyState.UnUsed;
                        break;
                    case ((int)TrustStatus.Verified):
                        verifyState = VbsCouponVerifyState.Verified;
                        break;
                    case ((int)TrustStatus.Refunded):
                    case ((int)TrustStatus.Returned):
                        verifyState = VbsCouponVerifyState.Return;
                        break;
                    case ((int)TrustStatus.ATM):
                        break;
                }
                //強制退貨數視為已核銷
                if ((couponInfo.SpecialStatus & (int)TrustSpecialStatus.ReturnForced) > 0)
                {
                    verifyState = VbsCouponVerifyState.Verified;
                }

                VerificationCouponInfo info = new VerificationCouponInfo
                {
                    DealName = couponInfo.DealName,
                    ItemName = couponInfo.ItemName.Replace("&nbsp", " "), //避免App顯示  &nbsp 字元
                    DealId = couponInfo.DealId.ToString(),
                    DealType = BusinessModel.PiinLife,
                    CouponSequence = couponInfo.CouponSequence,
                    CouponCode = couponInfo.CouponCode,
                    ExchangePeriodStartTime = couponInfo.UseStartTime,
                    ExchangePeriodEndTime = couponInfo.UseEndTime,
                    VerifyState = verifyState,
                    ModifyTime = couponInfo.ModifyTime,
                    BuyerName = couponInfo.MemberName,
                    TrustId = couponInfo.TrustId
                };

                couponInfos.Add(info);
            }
            #endregion

            return couponInfos;
        }

        private static List<VerifiedCouponData> TryGetPponCouponInfo(string couponSequence, string couponCode, Guid trustId,
                List<IVbsVendorAce> allowedDealUnits,bool isPezTemp=false)
        {
            List<VerifiedCouponData> result = new List<VerifiedCouponData>();

            List<VerifiedCouponData> rawCoupons = vp.GetVerifiedCouponDataWithPpon(couponSequence, couponCode, trustId, isPezTemp);
            foreach (var item in rawCoupons)
            {
                foreach (var ace in allowedDealUnits)
                {
                    if (item.DealGuid != ace.MerchandiseGuid)
                    {
                        continue;
                    }
                    //檢查賣家權限 是否有開啟該檔次之核銷
                    if (ace.IsSellerPermissionAllow)
                    {
                        if (!allowedDealUnits.Any(x => x.MerchandiseGuid == item.DealGuid && x.SellerGuid == x.StoreGuid))
                        {
                            break;
                        }
                    }
                    if (item.NoRestrictedStore)
                    {
                        result.Add(item);
                        break;
                    }
                    if (item.NoRestrictedStore == false && item.StoreGuid != null && item.StoreGuid.Value == ace.StoreGuid)
                    {
                        result.Add(item);
                        break;
                    }                    
                }
            }
            return result;
        }

        private static List<VerifiedCouponData> TryGetPiinCouponInfo(string couponSequence, string couponCode, Guid trustId, 
                List<IVbsVendorAce> allowedDealUnits,bool isPezEvent=false)
        {
            var result = new List<VerifiedCouponData>();
            var tempCoupons = vp.GetVerifiedCouponDataWithPiinLife(couponSequence, couponCode, trustId, isPezEvent);

            foreach (var ace in allowedDealUnits)
            {
                if (ace.IsPponDeal.GetValueOrDefault(false))
                {
                    continue;
                }
                if (ace.IsHomeDelivery.GetValueOrDefault(false))
                {
                    result.AddRange(tempCoupons.Where(x => x.DealGuid == ace.MerchandiseGuid));
                }
                else if (ace.IsInStore.GetValueOrDefault(false))
                { 
                    result.AddRange(tempCoupons.Where(x => x.DealGuid == ace.MerchandiseGuid && x.StoreGuid == ace.StoreGuid));
                }
            }
            return result;
        }
        
        /// <summary>
        /// 回傳可以［核銷］的憑證
        /// </summary>
        public static IEnumerable<VbsVerifyCouponInfo> GetVbsAccessibleCoupon(IEnumerable<VbsVerifyCouponInfo> infos)
        {
            List<VbsVerifyCouponInfo> couponInfos = new List<VbsVerifyCouponInfo>();

            foreach (VbsVerifyCouponInfo info in infos)
            {
                VbsVerifyCouponInfo couponInfo = info;

                //憑證為已使用、退貨或逾期狀態
                if (info.VerifyState == VbsCouponVerifyState.Return)
                {
                    couponInfo.Message = "此憑證已退貨\n退貨時間：" + info.ModifyTime;
                }
                else if (info.VerifyState == VbsCouponVerifyState.Verified)
                {
                    couponInfo.Message = "此憑證已使用過了，\n核銷時間:" + info.ModifyTime;
                }
                else if (info.ExchangePeriodStartTime > DateTime.Now.Date
                    || info.ExchangePeriodEndTime.AddDays(cp.VerificationBuffer) < DateTime.Now.Date)
                {
                    couponInfo.Message = "此憑證不在使用期限內。\n未開放使用，或已超過使用期限";
                }
                //憑證為未使用並於有效核銷期限內 即回傳檔次及憑證購買資料 供使用者確認是否進行核銷
                else if (info.VerifyState == VbsCouponVerifyState.UnUsed
                    && info.ExchangePeriodStartTime <= DateTime.Now.Date
                    && DateTime.Now.Date <= couponInfo.ExchangePeriodEndTime.AddDays(cp.VerificationBuffer))
                {
                    //回傳trust_id、檔次名稱、購買人及憑證號碼資料
                    couponInfo.IsCouponAvailable = true;

                    //判斷是否已過使用期限 卻仍在可核銷時限區間內
                    if (info.ExchangePeriodEndTime < DateTime.Now.Date     //零點零分的問題: 使用截止日當天仍然算在使用期限內
                        && DateTime.Now.Date <= info.ExchangePeriodEndTime.AddDays(cp.VerificationBuffer))
                    {
                        couponInfo.IsDuInTime = true;
                    }
                }

                couponInfos.Add(couponInfo);
            }

            return couponInfos;
        }

        /// <summary>
        /// 回傳可以［反核銷］的憑證
        /// </summary>
        public static IEnumerable<VbsVerifyCouponInfo> GetVbsAccessibleUnverifyCoupon(IEnumerable<VbsVerifyCouponInfo> infos) 
        {
            List<VbsVerifyCouponInfo> couponInfos = new List<VbsVerifyCouponInfo>();

            foreach (VbsVerifyCouponInfo info in infos)
            {
                VbsVerifyCouponInfo couponInfo = info;
                IEnumerable<CashTrustStatusLog> ctslCols = mp.CashTrustStatusLogGetList(new Guid(info.TrustId)).Where(
                    x => x.Status == (int)TrustStatus.Initial || x.Status == (int)TrustStatus.Trusted);

                switch (info.VerifyState)
                {
                    case VbsCouponVerifyState.Verified:
                        //是否在反核銷允許期限內
                        if (DateTime.Now.Date >= ((DateTime)info.ModifyTime).Date
                        && DateTime.Now.Date <= ((DateTime)info.ModifyTime).Date.AddDays(cp.UnverificationBuffer))
                        {
                            if (ctslCols.Count() >= 2)
                            {
                                //CashTrustStatusLog 出現兩次以上 Initial 或 Trusted, 代表曾經反核銷. 此情況不允許反核銷.
                                couponInfo.Message = "此憑證曾經取消核銷，無法再次取消，若有需求請洽客服。";
                            }
                            else
                            {
                                couponInfo.IsCouponAvailable = true;
                            }
                        }
                        else
                        {
                            couponInfo.Message = "已超過取消核銷期限，若有需求請洽客服。";
                        }
                        break;
                    case VbsCouponVerifyState.UnUsed:
                        couponInfo.Message = "此憑證尚未核銷，無須取消核銷。";
                        break;
                    case VbsCouponVerifyState.Return:
                        couponInfo.Message = "查無此憑證，無法取消核銷。";
                        break;
                    default:
                        //不明的例外狀況，拒絕反核銷並且請使用者聯繫客服
                        couponInfo.Message = "發生不明錯誤，請直接聯繫客服人員。";
                        break;
                }

                couponInfos.Add(couponInfo);
            }

            return couponInfos;
        }

        
        /// <summary>
        /// 將購買人姓名部分字元以*置換
        /// </summary>
        /// <param name="buyerName"></param>
        /// <returns></returns>
        public static string SetPayerNameHidden(string buyerName)
        {
            string payerName = string.Empty;
            buyerName = buyerName.Trim().Replace(" ", "");
            if (!string.IsNullOrEmpty(buyerName))
            {
                //UTF32 一個字(中/英) 4byte, 某些難字需要4byte
                byte[] nameByte = System.Text.Encoding.UTF32.GetBytes(buyerName);
                //名字字數
                int nameLen = (int)nameByte.Length / 4;          
                payerName = System.Text.Encoding.UTF32.GetString(nameByte, 0, 4); //get first word
                //只保留頭尾兩個字元 其餘以*置換
                if (nameLen > 2)
                {
                    for (var i = 1; i < nameLen - 1; i++)
                    {
                        payerName = payerName + "*";
                    }
                    //取最後一個字
                    payerName = payerName + System.Text.Encoding.UTF32.GetString(nameByte, (nameLen - 1)*4, 4);
                }
                else if (buyerName.Length == 2)
                {
                    payerName = payerName + "*";
                }
            }

            return payerName;
        }

        public static VbsVerifyResult GetVbsVerifyResult(VerificationStatus status, string trustId)
        {
            VbsVerifyResult verifyResult = new VbsVerifyResult 
            {
                IsVerifySuccess = false,
                Message = string.Empty
            };
            CashTrustStatusLog cashTrustLog;
            DateTime modifyTime = DateTime.MinValue;

            switch (status)
            {
                case VerificationStatus.CouponOk:
                    verifyResult.IsVerifySuccess = true;
                    verifyResult.Message = "已核銷成功，請繼續下一筆核銷。";
                    break;

                case VerificationStatus.CouponUsed:
                    cashTrustLog = mp.CashTrustStatusLogGetList(new Guid(trustId))
                                    .Where(x => x.Status.Equals((int)TrustStatus.Verified)).LastOrDefault();
                    modifyTime = (cashTrustLog != null) ? cashTrustLog.ModifyTime : modifyTime;                    
                    verifyResult.IsVerifySuccess = false;
                    verifyResult.Message = "此憑證已使用過了，\n核銷時間：" + modifyTime;
                    break;

                case VerificationStatus.CouponReted:
                    cashTrustLog = mp.CashTrustStatusLogGetList(new Guid(trustId))
                                    .Where(x => x.Status.Equals((int)TrustStatus.Refunded) || x.Status.Equals((int)TrustStatus.Returned)).LastOrDefault();
                    modifyTime = (cashTrustLog != null) ? cashTrustLog.ModifyTime : modifyTime;     
                    verifyResult.IsVerifySuccess = false;
                    verifyResult.Message = "此憑證已退貨\n退貨時間：" + modifyTime;
                    break;

                case VerificationStatus.CouponError:
                case VerificationStatus.CashTrustLogErr:
                    verifyResult.IsVerifySuccess = false;
                    verifyResult.Message = "其他憑證尚未使用完畢，贈送憑證不可用";
                    break;
                case VerificationStatus.CouponLocked:
                    verifyResult.IsVerifySuccess = false;
                    verifyResult.Message = "您欲核銷的憑證，目前處於【送禮中】的狀態，所以無法完成核銷，請您協助消費者再次確認憑證狀態後，再進行核銷。";
                    break;
                default:
                    verifyResult.IsVerifySuccess = false;
                    verifyResult.Message = "系統繁忙中，請30秒後再試一次。";
                    break;
            }

            return verifyResult;
        }

        public static CashTrustLog GetCashTrustLog(Guid trustId) {
            return mp.CashTrustLogGet(trustId);
        }
        /// <summary>
        /// 商家系統-憑證核銷查詢
        /// 部份舊檔次資料從 deal_archive_verification_summary取得
        /// 其它新檔次資料從 cash_trust_log 計算後取得
        /// </summary>
        /// <param name="bids"></param>
        /// <returns></returns>
        public static List<VendorDealSalesCount> GetPponVerificationSummary(List<Guid> bids)
        {
            List<VendorDealSalesCount> result = new List<VendorDealSalesCount>();
            var archiveItems = vp.GetPponVerificationSummaryFromArchive(bids);
            var unarchiveBids = bids.Except(archiveItems.Select(t => t.MerchandiseGuid)).ToList();
            result.AddRange(archiveItems);

            if (unarchiveBids.Count > 0)
            {
                var unarchiveItems = vp.GetPponVerificationSummary(unarchiveBids);
                result.AddRange(unarchiveItems);
            }
            

            result = result.Where(t => t.AnyData()).ToList();
            return result;
        }

        #region Private

        private static void AddMsgTo(MailMessage msg, string to)
        {
            if (RegExRules.IsEmailContainsSeperator(to))
            {
                msg.To.Add(RegExRules.CheckMultiEmail(to));
            }
            else
            {
                if (RegExRules.CheckEmail(to))
                {
                    msg.To.Add(to);
                }
            }
        }

        #endregion Private
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Transactions;
using System.Web;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using log4net;

namespace LunchKingSite.BizLogic.Facade
{
    public class OAuthFacade
    {
        private static ILog logger = LogManager.GetLogger(typeof(OAuthFacade));
        protected static IOAuthProvider oap;
        protected static IChannelProvider ccp;
        protected static IMemberProvider mp;
        protected static ISysConfProvider config;

        static OAuthFacade()
        {
            ccp = ProviderFactory.Instance().GetProvider<IChannelProvider>();
            oap = ProviderFactory.Instance().GetProvider<IOAuthProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            config = ProviderFactory.Instance().GetConfig();

        }

        public static bool ClientHasUser(int clientId, string userName)
        {
            int userId = mp.MemberGet(userName).UniqueId;
            return oap.ClientUserGet(clientId, userId).IsLoaded;
        }

        public static void ClientSetUser(string appKey, int userId)
        {
            if (userId == 0)
            {
                throw new Exception("userId can't be 0");
            }
            if (mp.MemberGet(userId).IsLoaded == false)
            {
                throw new Exception(string.Format("userId {0} not found.", userId));
            }
            var client = oap.ClientGet(appKey);
            if (client.IsLoaded == false)
            {
                throw new Exception(string.Format("client {0} not found", appKey));
            }
            if (client.IsLoaded == false)
            {
                throw new Exception(string.Format("client {0} not found", appKey));
            }
            if (oap.ClientUserGet(client.Id, userId).IsLoaded == false)
            {
                oap.ClientUserSet(client.Id, userId);
            }
            else
            {
                logger.WarnFormat("oauth clientuser(clientId={0},userId={1}) 試圖新增一筆己存在的資料", client.Id, userId);
            }
        }

        public static void ClientRemoveUser(int clientId, int userId)
        {
            OauthClientUser clientUser = oap.ClientUserGet(clientId, userId);
            if (clientUser.IsLoaded)
            {
                oap.ClientUserDelete(clientUser.Id);
                oap.TokenSetInvalid(clientUser.ClientId, clientUser.UserId);
            }
        }

        public static List<OAuthClientModel> GetClientList()
        {
            List<OAuthClientModel> result = new List<OAuthClientModel>();
            var clients = oap.ClientGetList();
            foreach (var client in clients)
            {
                result.Add(new OAuthClientModel(client, oap.ClientPermissionGetList(client.Id)));
            }
            return result;
        }

        public static OAuthClientModel GetClient(string appId)
        {
            var client = oap.ClientGet(appId);

            if (client.IsLoaded == false)
            {
                return null;
            }

            return new OAuthClientModel(client, oap.ClientPermissionGetList(client.Id));
        }

        public static OAuthClientModel GetClientByName(string name)
        {
            var client = oap.ClientGetByName(name);

            if (client.IsLoaded == false)
            {
                return null;
            }

            return new OAuthClientModel(client, oap.ClientPermissionGetList(client.Id));
        }
        /// <summary>
        /// 從channel_client_property取資料
        /// </summary>
        /// <param name="appId"></param>
        /// <returns></returns>
        public static ChannelClientProperty GetChannelClientProperty(string appId)
        {
            var property = ccp.ChannelClientPropertyGet(appId);

            if (property.IsLoaded == false)
            {
                ChannelClientProperty channelClientProperty = new ChannelClientProperty();
                channelClientProperty.AppId = appId;
                channelClientProperty.MinGrossMargin = 0;
                ccp.ChannelClientPropertySet(channelClientProperty);
            }
            return property;
        }

        public static void AddClient(string name, string owner, string returnUrl)
        {
            OauthClient client = new OauthClient();
            client.Name = name;
            client.Owner = owner;
            client.AppId = GenerateAppId();
            client.AppSecret = GenerateAppSecret();
            client.CreatedTime = DateTime.Now;
            client.ReturnUrl = returnUrl;
            client.Enabled = true;
            oap.ClientSet(client);

            ChannelClientProperty channelClientProperty = new ChannelClientProperty();
            channelClientProperty.AppId = client.AppId;
            ccp.ChannelClientPropertySet(channelClientProperty);
        }

        public static void UpdateClient(OAuthClientModel model, ChannelClientProperty channelClientProperty)
        {
            oap.ClientSet(model.Client);
            oap.ClientPermissionDeleteByClientId(model.Id);
            oap.ClientPermissionSetList(model.Id, model.Permissions);
            ccp.ChannelClientPropertySet(channelClientProperty);
        }

        public static void RenewClientSecret(string appId)
        {
            OauthClient client = oap.ClientGet(appId);
            if (client.IsLoaded)
            {
                client.AppSecret = GenerateAppSecret();
                oap.ClientSet(client);
            }
        }

        // 一個擁有 long-lived 權限的 client只能有一個long-lived token
        public static string RefreshClientTokenAndCancelOlders(string appId, bool reset)
        {
            OauthTokenCollection tokens = oap.OAuthTokenGetList(appId);

            if (reset)
            {
                foreach (OauthToken token in tokens)
                {
                    token.IsValid = false;
                }
                oap.TokenSetList(tokens);
                return CreateAppToken(appId);
            }
            if (tokens.Count == 0)
            {
                return CreateAppToken(appId);
            }
            var access_token = tokens.Where(t => t.IsValid == true).FirstOrDefault();
            return access_token == null ? string.Empty : access_token.AccessToken;
        }

        public static string CreateAppToken(string appId)
        {
            OauthToken token = new OauthToken();
            token.Target = (int)OAuthTargetType.Client;
            token.AppId = appId;
            token.AccessToken = GenerateToken(appId);
            token.RefreshToken = null; // client token 不需要 refresh token
            token.CreatedTime = DateTime.Now;
            token.IsValid = true;

            var client = GetClient(appId);
            if (client == null || client.Permissions == null || client.Permissions.Count == 0)
            {
                throw new Exception(string.Format("oauth client {0} 資料不正確", appId));
            }
            if (client.HasPermission(TokenScope.LongLived))
            {
                token.ExpiredTime = DateTime.Now.AddYears(10);// 十年之後
            }
            else
            {
                token.ExpiredTime = DateTime.Now.AddDays(1);
            }

            var channel = GetChannelClientProperty(appId);
            if (channel.IsLoaded && channel.SellerUserId != null)
            {
                token.UserId = channel.SellerUserId;
            }

            oap.TokenSet(token);
            //fill token permissions from client permission
            var tokenPerms = new OauthTokenPermissionCollection();
            tokenPerms.AddRange(client.Permissions.Select(t => new OauthTokenPermission
            {
                Scope = t.Scope,
                TokenId = token.Id
            }));
            oap.TokenPermissionSetList(tokenPerms);

            return token.AccessToken;
        }

        public static TokenResponse CreateMemberToken(string appId, int userId)
        {
            if (!MemberFacade.GetMember(userId).IsLoaded)
            {
                throw new Exception(string.Format("UserId 不正確 {0}", appId));
            }

            OauthToken token = new OauthToken();
            token.Target = (int)OAuthTargetType.Client;
            token.AppId = appId;
            token.AccessToken = GenerateToken(appId);
            token.RefreshToken = GenerateToken(appId); // client token 不需要 refresh token
            token.CreatedTime = DateTime.Now;
            token.IsValid = true;
            token.UserId = userId;

            var client = GetClient(appId);
            if (client == null || client.Permissions == null || client.Permissions.Count == 0)
            {
                throw new Exception(string.Format("oauth client {0} 資料不正確", appId));
            }

            TokenExpiredInfo expiredInfo;
            //Member 用 Token 不支援長效型Token
            if (client.HasPermission(TokenScope.OneWeekLived))
            {
                expiredInfo = new TokenExpiredInfo(TokenScope.OneWeekLived);
            }
            else if (client.HasPermission(TokenScope.OnedayLived))
            {
                expiredInfo = new TokenExpiredInfo(TokenScope.OnedayLived);
            }
            else
            {
                expiredInfo = new TokenExpiredInfo();
            }
            token.ExpiredTime = expiredInfo.ExpiredTime;

            oap.TokenSet(token);
            //fill token permissions from client permission
            var tokenPerms = new OauthTokenPermissionCollection();
            tokenPerms.AddRange(client.Permissions.Select(t => new OauthTokenPermission
            {
                Scope = t.Scope,
                TokenId = token.Id
            }));
            oap.TokenPermissionSetList(tokenPerms);

            return new TokenResponse
            {
                AccessToken = token.AccessToken,
                ExpiresIn = expiredInfo.ExpiredSec,
                RefreshToken = token.RefreshToken
            };
        }

        private static string GenerateAppId()
        {
            Random rand = new Random();
            long ticks = (long)(DateTime.Now - new DateTime(1900, 1, 1)).TotalSeconds * rand.Next(10000, 100000) + rand.Next(10000);
            return ticks.ToString("000000000000000");
        }

        private static string GenerateAppSecret()
        {
            StringBuilder sBuilder = new StringBuilder();
            var data = MD5.Create().ComputeHash(Guid.NewGuid().ToByteArray());
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        private static string GenerateToken(string appId)
        {
            return Guid.NewGuid().ToString("N") + "_" + Security.MD5Hash(appId + DateTime.Now);
        }

        public static OAuthTokenModel GetTokenByAccessToken(string accessToken)
        {
            OauthToken token = oap.TokenGetByAccessToken(accessToken);
            if (token.IsLoaded == false)
            {
                return null;
            }
            return new OAuthTokenModel(token, oap.TokenPermissionGetList(token.Id));
        }

        public static OAuthTokenModel GetTokenByCode(string code)
        {
            OauthToken token = oap.TokenGetByCode(code);
            if (token.IsLoaded == false)
            {
                return null;
            }
            return new OAuthTokenModel(token, oap.TokenPermissionGetList(token.Id));
        }

        #region token
        public static string CreateAuthorizeCode(int? uniqueId, string appId, OAuthTargetType targetType = OAuthTargetType.User)
        {
            OauthToken token = new OauthToken();
            token.Target = (int)targetType;
            token.UserId = uniqueId;
            token.AppId = appId;
            token.Code = GenerateToken(appId);
            token.CreatedTime = DateTime.Now;
            token.IsValid = true;
            token.ExpiredTime = DateTime.Now.AddMinutes(10); //10分鍾 rfc 6749建議

            if (token.Target == (int)OAuthTargetType.User && token.UserId == null)
            {
                throw new Exception("token user info invalid.");
            }
            oap.TokenSet(token);
            return token.Code;
        }

        public static TokenResponse ExchangeToken(string code)
        {
            OAuthTokenModel token = OAuthFacade.GetTokenByCode(code);
            if (token == null)
            {
                throw new Exception("token not found.");
            }
            if (token.IsValid == false)
            {
                throw new Exception("token is invalid.");
            }
            if (token.IsExpired)
            {
                throw new Exception("token is expired.");
            }
            if (token.IsCodeExchanged)
            {
                //code 只能被兌換一次，如果有重複兌換的動作，除此次兌換失敗，先前用此code兌換的token也被取消
                token.IsValid = false;
                UpdateToken(token);
                string err = string.Format("code {0} 嘗試「重複」兌換，此code先前兌換的token被取消", code);
                logger.WarnFormat(err);
                throw new Exception(err);
            }
            TokenExpiredInfo expiredInfo;
            OAuthClientModel client = GetClient(token.AppId);
            using (var trans = TransactionScopeBuilder.CreateReadCommitted())
            {
                token.AccessToken = GenerateToken(token.AppId);
                token.RefreshToken = GenerateToken(token.AppId);
                token.ModifiedTime = DateTime.Now;

                if (client.HasPermission(TokenScope.LongLived))
                {
                    expiredInfo = new TokenExpiredInfo(TokenScope.LongLived);
                }
                else if (client.HasPermission(TokenScope.OnedayLived))
                {
                    expiredInfo = new TokenExpiredInfo(TokenScope.OnedayLived);
                }
                else if (client.HasPermission(TokenScope.OneWeekLived))
                {
                    expiredInfo = new TokenExpiredInfo(TokenScope.OneWeekLived);
                }
                else
                {
                    expiredInfo = new TokenExpiredInfo();
                }
                token.ExpiredTime = expiredInfo.ExpiredTime;

                token.IsCodeExchanged = true;

                //fill token permissions from client permission                
                if (client == null || client.Permissions == null)
                {
                    throw new Exception(string.Format("oauth client {0} 資料不正確", token.AppId));
                }
                token.Permissions = new OauthTokenPermissionCollection();
                foreach (var perm in client.Permissions)
                {
                    token.Permissions.Add(new OauthTokenPermission
                    {
                        TokenId = token.Id,
                        Scope = perm.Scope
                    });
                }

                UpdateToken(token);
                trans.Complete();
            }

            TokenResponse result;

            if (client.HasPermission(TokenScope.RefreshToken))
            {
                result = new TokenResponse
                {
                    AccessToken = token.AccessToken,
                    ExpiresIn = expiredInfo.ExpiredSec, //2 hours
                    RefreshToken = token.RefreshToken
                };
            }
            else
            {
                result = new TokenResponse
                {
                    AccessToken = token.AccessToken,
                    ExpiresIn = expiredInfo.ExpiredSec //2 hours
                };
            }

            return result;
        }

        public static void UpdateToken(OAuthTokenModel model, bool updatePermissions = true)
        {
            oap.TokenSet(model.Token);
            if (updatePermissions)
            {
                oap.TokenPermissionDeleteByTokenId(model.Id);
                oap.TokenPermissionSetList(model.Permissions);
            }
        }
        #endregion

        public static dynamic RefreshToken(string refreshToken)
        {
            OAuthTokenModel token = GetTokenByRefreshToken(refreshToken);
            token.IsValid = false;
            token.IsTokenRefreshed = true;
            token.ModifiedTime = DateTime.Now;
            UpdateToken(token, updatePermissions: false);
            string code = CreateAuthorizeCode(token.UserId, token.AppId);
            return ExchangeToken(code);
        }
        /// <summary>
        /// 將Token做無效
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        public static bool ExpireToken(string accesstoken)
        {
            try
            {
                OAuthTokenModel token = GetTokenByAccessToken(accesstoken);
                if (token != null)
                {
                    token.IsValid = false;
                    token.ModifiedTime = DateTime.Now;
                    UpdateToken(token, updatePermissions: false);
                }

                token = GetTokenByRefreshToken(accesstoken);
                if (token != null)
                {
                    token.IsValid = false;
                    token.ModifiedTime = DateTime.Now;
                    UpdateToken(token, updatePermissions: false);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 判斷token是否有效
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static bool IsValid(string token)
        {
            OAuthTokenModel oathToken = GetTokenByAccessToken(token);
            if (oathToken != null)
            {
                return oathToken.IsExpired;
            }
            oathToken = GetTokenByRefreshToken(token);
            if (oathToken != null)
            {
                return oathToken.IsValid;
            }
            return false;
        }

        /// <summary>
        /// 取得OAuthTokenModel物件
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static OAuthTokenModel GetTokenByToken(string token)
        {
            OAuthTokenModel authToken = GetTokenByAccessToken(token);
            if (authToken == null || authToken.IsExpired || authToken.IsValid == false)
            {
                authToken = OAuthFacade.GetTokenByRefreshToken(token);
                if (authToken == null || authToken.IsExpired || authToken.IsValid == false)
                {
                    return null;
                }
            }

            return authToken;
        }

        public static OAuthTokenModel GetTokenByRefreshToken(string refreshToken)
        {
            OauthToken token = oap.TokenGetByRefreshToken(refreshToken);
            if (token.IsLoaded == false)
            {
                return null;
            }
            return new OAuthTokenModel(token, oap.TokenPermissionGetList(token.Id));
        }

        /// <summary>
        /// 清除Expired的OauthToken所關連的裝置
        /// </summary>
        public static void MemberCollectNoticeClear()
        {
            //RefreshToken延長失效的天數
            int Days = config.RefreshTokenExtendExpiredDays;
            OauthTokenCollection tokens = oap.OauthTokenGetExpiredList(DateTime.Now.AddDays(-Days));

            foreach (var token in tokens)
            {
                //移除關連的裝置
                int userId = token.UserId.GetValueOrDefault();
                MemberCollectNotice mcn = mp.MemberCollectNoticeGetByToken(userId, token.Id);
                if (mcn != null && mcn.IsLoaded)
                {
                    int device_id = mcn.DeviceId;
                    mp.MemberCollectNoticeRemove(userId, device_id);
                }

                //token做無效
                ExpireToken(token.AccessToken);
            }
        }
        /// <summary>
        /// 取出Access Token
        /// </summary>
        /// <param name="auth">HttpRequestMessage</param>
        /// <returns>Access Token</returns>
        public static string ExtractToken(AuthenticationHeaderValue auth)
        {
            //Header
            if (auth != null && (auth.Scheme == "OAuth" || auth.Scheme == "Bearer"))
            {
                return auth.Parameter;
            }
            else//Form
            {
                if (HttpContext.Current.Request["access_token"] != null)
                {
                    return HttpContext.Current.Request["access_token"];
                }
                else if (HttpContext.Current.Request["accessToken"] != null)
                {
                    return HttpContext.Current.Request["accessToken"];
                }
                else if (HttpContext.Current.Request.RequestContext.RouteData.Values["accessToken"] != null) //RouteData
                {
                    return HttpContext.Current.Request.RequestContext.RouteData.Values["accessToken"].ToString();
                }
            }

            //IEnumerable<string> tokenValues = null;
            //if (request.Headers.TryGetValues(Constants.TokenHeaderKey, out tokenValues))
            //    return tokenValues.First();

            return null;
        }
    }
}

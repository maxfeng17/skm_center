﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.Ppon;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Facade
{
    /// <summary>
    /// 給24到貨, M版首頁
    /// </summary>
    public class PponFacade2
    {
        /// <summary>
        /// 轉換 MultipleMainDealPreview 為 DealViewLite 格式，目前DealViewLite使用在24到貨、M版首頁
        /// </summary>
        /// <param name="mmdeal"></param>
        /// <param name="collectBids"></param>
        /// <returns></returns>
        public static DealViewLite ConvertToChannelDealView(MultipleMainDealPreview mmdeal, HashSet<Guid> collectBids)
        {
            DealViewLite result = new DealViewLite();
            var vpd = mmdeal.PponDeal;
            result.id = vpd.BusinessHourGuid;
            if (!string.IsNullOrEmpty(vpd.AppTitle))
            {
                result.title = vpd.AppTitle;
            }
            else
            {
                result.title = vpd.ItemName;
            }
            result.img = vpd.DefaultDealImage;
            result.imgAlt = vpd.PicAlt;
            if (string.IsNullOrEmpty(vpd.AppDealPic) == false)
            {
                result.sqImg = ImageFacade.GetMediaPathsFromRawData(vpd.AppDealPic, MediaType.PponDealPhoto)
                    .FirstOrDefault();
            }

            result.subTitle = vpd.EventTitle;
            result.url = string.Concat("/deal/", vpd.BusinessHourGuid);
            result.soldOut = vpd.IsSoldOut;
            result.promoImg = vpd.PromoImageHtml;
            result.salesVolume = string.IsNullOrEmpty(vpd.Slug) ?
                vpd.GetAdjustedOrderedQuantity().Quantity.ToString() : vpd.GetAdjustedSlug().Quantity.ToString();
            result.origPrice = vpd.ItemOrigPrice.ToString("F0");
            result.price = vpd.ItemPrice.ToString("F0");
            result.discountPrice = vpd.DiscountPrice.HasValue ? vpd.DiscountPrice.ToString("F0") : string.Empty;
            result.priceTail = (vpd.ComboDelas != null && vpd.ComboDelas.Count > 1) ? "起" : "元";

            result.icons = PponFacade.GetDealTagsAsMobileHtml(vpd, 2);
            result.discount = ViewPponDealManager.GetDealDiscountString(
                Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                PponFacade.CheckZeroPriceToShowPrice(vpd, 0), vpd.ItemOrigPrice, false);

            decimal ratingScore;
            int ratingNumber;
            bool ratingShow;
            PponFacade.GetNewDealStarRating(
                vpd.BusinessHourGuid, "pc", out ratingScore, out ratingNumber, out ratingShow);
            result.rating = new DealStarRating
            {
                show = ratingShow,
                number = ratingNumber,
                score = ratingScore
            };
            result.inCollect = collectBids.Contains(vpd.BusinessHourGuid);
            result.categories = vpd.CategoryIds;
            return result;
        }
        /// <summary>
        /// 轉換給APP用的model
        /// </summary>
        /// <param name="mmdeal"></param>
        /// <param name="collectBids"></param>
        /// <returns></returns>
        public static DealViewLite ConvertToChannelDealView(PponDealSynopsisBasic basic , HashSet<Guid> collectBids)
        {
            return ConvertToChannelDealView(basic.Bid, collectBids);
        }

        public static DealViewLite ConvertToChannelDealView(Guid dealGuid, HashSet<Guid> collectBids)
        {
            DealViewLite result = new DealViewLite();
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealGuid, true);
            if (vpd.IsLoaded == false)
            {
                return result;
            }
            result.id = vpd.BusinessHourGuid;
            if (!string.IsNullOrEmpty(vpd.AppTitle))
            {
                result.title = vpd.AppTitle;
            }
            else
            {
                result.title = vpd.ItemName;
            }
            result.img = vpd.DefaultDealImage;
            result.imgAlt = vpd.PicAlt;
            if (string.IsNullOrEmpty(vpd.AppDealPic) == false)
            {
                result.sqImg = ImageFacade.GetMediaPathsFromRawData(vpd.AppDealPic, MediaType.PponDealPhoto)
                    .FirstOrDefault();
            }

            result.subTitle = vpd.EventTitle;
            result.url = string.Concat("/deal/", vpd.BusinessHourGuid);
            result.soldOut = vpd.IsSoldOut;
            result.promoImg = vpd.PromoImageHtml;
            result.salesVolume = string.IsNullOrEmpty(vpd.Slug) ?
                vpd.GetAdjustedOrderedQuantity().Quantity.ToString() : vpd.GetAdjustedSlug().Quantity.ToString();
            result.origPrice = vpd.ItemOrigPrice.ToString("F0");
            result.price = vpd.ItemPrice.ToString("F0");
            result.discountPrice = vpd.DiscountPrice.HasValue ? vpd.DiscountPrice.ToString("F0") : string.Empty;
            result.priceTail = (vpd.ComboDelas != null && vpd.ComboDelas.Count > 1) ? "起" : "元";

            result.icons = PponFacade.GetDealTagsAsMobileHtml(vpd, 2);
            result.discount = ViewPponDealManager.GetDealDiscountString(
                Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                PponFacade.CheckZeroPriceToShowPrice(vpd, 0), vpd.ItemOrigPrice, false);

            decimal ratingScore;
            int ratingNumber;
            bool ratingShow;
            PponFacade.GetNewDealStarRating(
                vpd.BusinessHourGuid, "pc", out ratingScore, out ratingNumber, out ratingShow);
            result.rating = new DealStarRating
            {
                show = ratingShow,
                number = ratingNumber,
                score = ratingScore
            };
            result.inCollect = collectBids.Contains(vpd.BusinessHourGuid);
            result.categories = vpd.CategoryIds;
            if (vpd.CategoryIds == null)
            {
                throw new Exception();
            }
            return result;
        }

        /// <summary>
        /// 取得美食、玩美、旅遊熱銷檔次，不足17檔補到17檔
        /// </summary>
        /// <returns></returns>
        public static List<PponDealSynopsisBasic> GetChannelFoodBeautyTravelHotDeals()
        {
            List<PponDealSynopsisBasic> dealBasicList = new List<PponDealSynopsisBasic>();
            List<PponDealSynopsis> dealList = PponDealApiManager.GetFoodBeautyTravelHotDealSynopses();
            if (dealList.Count < 17)
            {
                HashSet<Guid> dealBids = new HashSet<Guid>(dealList.Select(t => t.Bid).ToArray());
                List<PponDealSynopsis> unsortedDeals = PponDealApiManager.GetFoodBeautyTravelCouponDealSynopses(30);
                foreach (var unsortedDeal in unsortedDeals)
                {
                    if (dealBids.Contains(unsortedDeal.Bid))
                    {
                        continue;
                    }
                    if (unsortedDeal.SoldOut)
                    {
                        continue;
                    }
                    dealBids.Add(unsortedDeal.Bid);
                    dealList.Add(unsortedDeal);
                }
            }
            foreach (PponDealSynopsis synopsis in dealList)
            {
                var pponDealSynopsisBasic = PponDealApiManager.PponDealSynopsisToBasic(synopsis);
                dealBasicList.Add(pponDealSynopsisBasic);
                if (dealBasicList.Count >= 17)
                {
                    break;
                }
            }
            return dealBasicList;
        }

        public static bool CheckOnlineAndAvailable(Guid bid)
        {
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            if (vpd.IsLoaded == false)
            {
                return false;
            }
            if (vpd.IsSoldOut)
            {
                return false;
            }
            if (Helper.IsFlagSet((int)vpd.GroupOrderStatus, GroupOrderStatus.Completed))
            {
                return false;
            }
            if (DateTime.Now < vpd.BusinessHourOrderTimeS || DateTime.Now > vpd.BusinessHourOrderTimeE)
            {
                return false;
            }
            if (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
            {
                return false;
            }
            return true;
        }
    }
}

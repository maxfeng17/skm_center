﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Web;
using System.Web.Security;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Facade
{
    public class HumanFacade
    {
        private static IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        #region Employee
        public static string GetNewEmpId()
        {
            var lastEmpId = hp.EmployeeGetLastEmpId();
            var newEmpId = "00001";
            if (!string.IsNullOrEmpty(lastEmpId))
            {
                newEmpId = (int.Parse(lastEmpId) + 1).ToString("00000");
            }
            return newEmpId;
        }

        public static List<ViewEmployee> ViewEmployeeGetListByDept(EmployeeDept dept, string deptId)
        {
            return hp.ViewEmployeeCollectionGetByDepartment(dept)
                .Where(x => x.DeptId.Equals(deptId)).ToList();
        }

        public static ViewEmployee ViewEmployeeGetByUserId(int userId)
        {
            return hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, userId);
        }

        public static ViewEmployee ViewEmployeeGet(string column, object value)
        {
            return hp.ViewEmployeeGet(column, value);
        }

        public static EmployeeCollection EmployeeGetListByDept(EmployeeDept dept)
        {
            return hp.EmployeeCollectionGetByDepartment(dept);
        }

        public static ViewEmployeeCollection ViewEmployeeGetAll()
        {
            return hp.ViewEmployeeCollectionGetAll();
        }

        /// <summary>
        /// 取得業務+行銷部門
        /// </summary>
        /// <returns></returns>
        public static List<Department> GetSalesDepartment()
        {
            return hp.DepartmentGetListByEnabled(true).ToList().Union(hp.DepartmentGetListByEnabled(true, EmployeeDept.M000)).ToList();
        }

        /// <summary>
        /// 取得ViewEmployee某一欄位包含"片段字詞"的列表 (前端autocomplete用)
        /// </summary>
        /// <param name="column">欄位名</param>
        /// <param name="partValue">片段字詞</param>
        /// <returns></returns>
        public static List<string> ViewEmployeeGetColumnListByLike(string column, string partValue)
        {
            List<string> list = new List<string>();
            if (!string.IsNullOrWhiteSpace(partValue) && !string.IsNullOrWhiteSpace(column))
            {
                list = hp.ViewEmployeeGetColumnListByLike(column, partValue.Trim());
            }
            return list;
        }
        #endregion Employee

        public static List<string> GetUsesEmailInRole(string roleName)
        {
            List<string> userEmails = new List<string>();
            foreach (string userName in Roles.GetUsersInRole(roleName))
            {
                Member mem = mp.MemberGet(userName);
                if (mem.IsLoaded)
                {
                    userEmails.Add(mem.UserEmail);
                }
            }
            return userEmails;
        }

        #region Department

        public static Department GetDepartmentByDeptId(string deptId)
        {
            Department dept = hp.DepartmentGet(deptId);
            return dept;
        }

        #endregion

        #region 業務績效KPI相關
        public static SalesVolumeMonth GetSalesVolumeMonth(int userId,string yearMon = "")
        {
            yearMon = string.IsNullOrEmpty(yearMon) ? DateTime.Now.ToString("yyyyMM") : yearMon;
            return hp.GetSalesVolumeMonthList(userId).Where(x => x.YearMon == yearMon).LastOrDefault();
        }

        public static bool GetKPISetPrivilege(string email)
        {
            return hp.GetPrivilege(email, (int)SystemFunctionType.KPISet).IsLoaded;
        }
        #endregion
    }
}

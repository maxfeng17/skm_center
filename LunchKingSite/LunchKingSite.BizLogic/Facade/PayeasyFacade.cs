﻿using System;
using System.Security.Cryptography;
using LunchKingSite.Core.Component;
using System.Text;
using System.Net;
using System.IO;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using log4net;

namespace LunchKingSite.BizLogic.Facade
{
    public class PayeasyFacade
    {
        static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        static ILog logger = LogManager.GetLogger("PayeasyFacade");
        public static Security AES1;
        public static Security AES2;
        static PayeasyFacade()
        {
            AES1 = new Security(SymmetricCryptoServiceProvider.AES,
                Convert.FromBase64String(config.PcashXchAESKey1), new byte[16], CipherMode.ECB)
            {
                StringStyle = EncryptedStringStyle.Hex
            };
            AES2 = new Security(SymmetricCryptoServiceProvider.AES,
                Convert.FromBase64String(config.PcashXchAESKey2), new byte[16], CipherMode.ECB)
            {
                StringStyle = EncryptedStringStyle.Hex
            };
        }
        public static int QueryPayeasyMemberPcash(int memNum)
        {

            return 0;
        }

        public static string EncryptAES1(string content)
        {
            return EncryptStr(content, AES1);
        }

        public static string EncryptAES2(string content)
        {
            return EncryptStr(content, AES2);
        }

        public static string DecryptASE1(string content)
        {
            return DecryptStr(content, AES1);
        }

        public static string DecryptASE2(string content)
        {
            return DecryptStr(content, AES2);
        }

        public static string EncryptStr(string plainStr, Security AES)
        {
            string encryptedStr = AES.Encrypt(plainStr);
            return encryptedStr;
        }

        public static string DecryptStr(string encryptedStr, Security AES)
        {
            string plainStr = AES.Decrypt(encryptedStr);
            return plainStr;
        }

        /// <summary>
        /// 取得MerchantContent
        /// </summary>
        public static string GetMerchantContent()
        {
            //扣點與取消扣點區分大小寫
            var json = new
            {
                merchantDealCode = Guid.NewGuid(),
                maxValue = "100000"
            };

            return PayeasyFacade.EncryptAES2(Newtonsoft.Json.JsonConvert.SerializeObject(json));
        }

        public static bool RefundToPasyeasyAPI(Guid guid, string pezAuthCode, decimal cancelPoint, out int actualCancelPoint)
        {
            string dePezAuthCode = EncryptAES2(pezAuthCode.Replace("pez:", ""));
            string deCancelPoint = EncryptAES2(Convert.ToInt32(cancelPoint).ToString());
            var data = new
            {
                merchantId = "M0025",
                merchantDealCode = guid.ToString(),
                cancelPoint = deCancelPoint,
                pezAuthCode = dePezAuthCode
            };
            string result = ApiRequest(Newtonsoft.Json.JsonConvert.SerializeObject(data), "cancelDeductPoint");
            CancelDeductPoint cancel = Newtonsoft.Json.JsonConvert.DeserializeObject<CancelDeductPoint>(result);
            actualCancelPoint = Convert.ToInt32(DecryptASE2(cancel.DealResult.CancelPoint));

            if (cancel.DealResult.ReturnCode != "0000")
            {
                logger.Error("RefundToPasyeasyAPI Error:" + cancel.DealResult.ReturnCode);
            }
            return (cancel.DealResult.ReturnCode == "0000");
        }

        public static string ApiRequest(string jsonData, string method)
        {
            byte[] dataBytes = Encoding.UTF8.GetBytes(jsonData);
            string apiUrl = "https://eip.payeasy.com.tw/WelfareWebService/corpPartner/exchange/" + method;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(apiUrl);
            request.ContentType = "application/json";
            request.ContentLength = dataBytes.Length;
            request.Method = WebRequestMethods.Http.Post;

            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(dataBytes, 0, dataBytes.Length);
                requestStream.Flush();
            }

            string result = "";
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        result = reader.ReadToEnd();
                    }
                }
            }
            return result;
        }
    }
}

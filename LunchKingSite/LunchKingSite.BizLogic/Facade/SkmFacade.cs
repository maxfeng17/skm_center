﻿using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using System.Text;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core.Models;
using System.Web;
using NewtonsoftJson = Newtonsoft.Json;
using Microsoft.SqlServer.Types;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Transactions;
using LunchKingSite.BizLogic.Models.Skm;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.Entities;
using log4net;
using LunchKingSite.BizLogic.Models.Wallet;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Models.TurnCloud;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using LunchKingSite.Core.ModelPartial;

namespace LunchKingSite.BizLogic.Facade
{
    public class SkmFacade
    {
        #region Property

        private static IChannelEventProvider cep;
        private static ISkmProvider skmMp;
        private static ISkmEfProvider skmEp;
        private static IMemberProvider mp;
        private static ISysConfProvider cp;
        private static IPponProvider pp;
        private static ISellerProvider sp;
        private static IHumanProvider hp;
        private static ISysConfProvider config;
        private static IOrderProvider op;
        private static IItemProvider it;
        private static ISystemProvider _sysp;
        private static IExhibitionProvider ep;
        public const string BeaconPowerNoticeSystemData = "SkmBeaconPowerNotice";
        private static int _skmActionEventInfoId;
        private static int _skmDefaultCategoryId;
        private static ILog logger = LogManager.GetLogger("Skm");
        private static string[] numberCode = new[]
        {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H",
            "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
        };
        private static Dictionary<string, string> _tmCode = new Dictionary<string, string>();
        private static Dictionary<string, string> _parkingTmCode = new Dictionary<string, string>();

        //刷卡分期快取資料
        private const int installmentCacheMinutes = 60;
        private const string installmentCacheKey = "InstallmentInfo";
        //結帳完成與退貨完成皆要通知
        public static string SKmPayParkingNoticeApiDesc = "Umall/2.14.6銷售(停車繳費確認)";
        public static string SkmPayPlatFormId = "7";//發票平台代號(精選優惠)
        public static string SkmPayParkingPlatFormId = "P";//發票平台代號(停車支付)
        public static string SkmPayParkingSellCasher = "60900171";//停車支付固定發票收銀員資料
        public static string SkmPayParking6XBrandCounterCode = "6090017";//停車支付固定6X櫃號
        public static string SkmPayParking7XBrandCounterCode = "7090028";//停車支付固定7X櫃號
        public static string SkmPayParkingProductCode = "9900007090028"; //停車支付固定商品條碼
        public static string SkmPayParkingGoodsName = "業外收入.停車場-新光三越-停車場收入";//停車支付固定商品名稱
        public static decimal SkmPayParkingTaxRate = 5;
        public static readonly Dictionary<SkmPayOrderStatus, string> ParkingOrderStatusDesc = new Dictionary<SkmPayOrderStatus, string>()
        {
            {SkmPayOrderStatus.None, "Init"},
            {SkmPayOrderStatus.Create, "建立預約付款單"},
            {SkmPayOrderStatus.GetPreTransNo, "取得預備交易編號"},
            {SkmPayOrderStatus.MakeOrderFinsh, "產出Order編號"},
            {SkmPayOrderStatus.AuthPaymentSuccess, "授權交易成功 尚未開立發票"},
            {SkmPayOrderStatus.WaitingOTPFinished, "等待OTP完成"},
            {SkmPayOrderStatus.SentInvoice, "傳送發票交易成功;購買成功(未取貨)"},
            {SkmPayOrderStatus.WriteOff, "核銷成功;購買成功(已取貨)"},

            {SkmPayOrderStatus.GetPreTransFail, "取得預備交易編號失敗"},
            {SkmPayOrderStatus.AuthPaymentFail, "授權交易失敗"},
            {SkmPayOrderStatus.SentInvoiceFail, "開立發票失敗 未退款 (需以Job做退款)"},
            {SkmPayOrderStatus.RefundPaymentFail, "Wallet 退貨交易失敗"},
            {SkmPayOrderStatus.CancelRefundPaymentFail, "Wallet 取消退貨交易失敗"},
            {SkmPayOrderStatus.MakeOrderFail, "建立訂單失敗"},
            {SkmPayOrderStatus.CreateInvoiceFail, "開立發票失敗且退款成功"},
            {SkmPayOrderStatus.CreateInvoiceFailRefundFail, "開立發票失敗且退款失敗 (需以Job做退款)"},
            {SkmPayOrderStatus.CreatedInvoiceWriteOffFail, "核銷成功 通知發票商(藤雲)失敗(In DB) (需以Job再通知)"},
            {SkmPayOrderStatus.WriteOffFail, "核銷成功 通知發票商(藤雲)失敗(API) (需以Job再通知)"},
            {SkmPayOrderStatus.ReturnInvoiceFail, "退貨 通知發票商(藤雲)失敗(API)"},
            {SkmPayOrderStatus.SkmPayOTPTimeout, "OTP驗證逾時(超過config設定的訪問次數上限)"},

            {SkmPayOrderStatus.CancelAuthPayment, "取消授權"},
            {SkmPayOrderStatus.ReturnSuccess, "退貨成功"},
            {SkmPayOrderStatus.CancelReturnSuccess, "取消退款成功"},

        };

        /// <summary>
        /// 可輸出SKM PAY訂單之訂單狀態
        /// </summary>
        public static List<SkmPayOrderStatus> SkmPayOrderPassStatus = new List<SkmPayOrderStatus>
        {
            SkmPayOrderStatus.SentInvoice,
            SkmPayOrderStatus.WriteOff,
            SkmPayOrderStatus.CancelAuthPayment,
            SkmPayOrderStatus.ReturnSuccess,
            SkmPayOrderStatus.CancelReturnSuccess,
            SkmPayOrderStatus.RefundPaymentFail,
            SkmPayOrderStatus.CancelRefundPaymentFail,
            SkmPayOrderStatus.CreatedInvoiceWriteOffFail,
            SkmPayOrderStatus.WriteOffFail,
            SkmPayOrderStatus.ReturnInvoiceFail
        };

        /// <summary>
        /// 可退貨SKM PAY訂單之訂單狀態
        /// </summary>
        public static List<SkmPayOrderStatus> SkmPayOrderReturnableStatus = new List<SkmPayOrderStatus>
        {
            SkmPayOrderStatus.SentInvoice,
            SkmPayOrderStatus.WriteOff,
            SkmPayOrderStatus.CancelReturnSuccess,
            SkmPayOrderStatus.SentInvoiceFail,
            SkmPayOrderStatus.RefundPaymentFail,
            SkmPayOrderStatus.CreateInvoiceFailRefundFail,
            SkmPayOrderStatus.CreatedInvoiceWriteOffFail,
            SkmPayOrderStatus.WriteOffFail,
            SkmPayOrderStatus.ReturnInvoiceFail
        };

        /// <summary>
        /// SKM PAY訂單失敗需要重跑流程之訂單狀態
        /// </summary>
        public static List<SkmPayOrderStatus> SkmPayOrderFaildNeedRetryStatus = new List<SkmPayOrderStatus>
        {
            SkmPayOrderStatus.CreateInvoiceFailRefundFail,
            SkmPayOrderStatus.SentInvoiceFail,
            SkmPayOrderStatus.CreatedInvoiceWriteOffFail,
            SkmPayOrderStatus.WriteOffFail,
            SkmPayOrderStatus.RefundPaymentFail,
            //SkmPayOrderStatus.RefundInvoiceFail
        };

        /// <summary>
        /// 是否可退貨
        /// </summary>
        /// <param name="orderStatus"></param>
        /// <returns></returns>
        public static bool IsReturnable(int orderStatus)
        {
            return SkmPayOrderReturnableStatus.Any(x => (int)x == orderStatus);
        }

        /// <summary>
        /// 目前是否可對Appos做退貨交易
        /// </summary>
        /// <returns></returns>
        public static bool IsApposRefundTime()
        {
            try
            {
                var now = DateTime.Now;
                var shortDate = now.ToString("yyyyMMdd");
                var tempStart = shortDate + config.ApposPauseRefundStartTime;
                var tempEnd = shortDate + config.ApposPauseRefundEndTime;
                var pauseRefundStartTime = DateTime.ParseExact(tempStart, "yyyyMMddHH:mm", CultureInfo.InvariantCulture);
                var pauseRefundEndTime = DateTime.ParseExact(tempEnd, "yyyyMMddHH:mm", CultureInfo.InvariantCulture);

                return pauseRefundStartTime >= now || now >= pauseRefundEndTime;
            }
            catch
            {
                return true;
            }
        }

        #endregion

        static SkmFacade()
        {
            cep = ProviderFactory.Instance().GetProvider<IChannelEventProvider>();
            skmMp = ProviderFactory.Instance().GetProvider<ISkmProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            cp = ProviderFactory.Instance().GetConfig();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            config = ProviderFactory.Instance().GetConfig();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            it = ProviderFactory.Instance().GetProvider<IItemProvider>();
            _sysp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            ep = ProviderFactory.Instance().GetProvider<IExhibitionProvider>();
            skmEp = ProviderFactory.Instance().GetProvider<ISkmEfProvider>();
        }

        #region 分類

        /// <summary>
        /// 分館代碼對照表
        /// </summary> 

        public static Dictionary<string, string> SkmTmCode
        {
            get
            {
                if (_tmCode.Any())
                {
                    return _tmCode;
                }
                else
                {
                    var sd = _sysp.SystemDataGet("SkmEcTmCode");
                    _tmCode = new JsonSerializer().Deserialize<Dictionary<string, string>>(sd.Data);
                    return _tmCode;
                }
            }
            set { _tmCode = value; }
        }

        public static Dictionary<string, string> SkmParkingTmCode
        {
            get
            {
                if (_parkingTmCode.Any())
                {
                    return _parkingTmCode;
                }
                else
                {
                    var sd = _sysp.SystemDataGet("SkmParkingEcTmCode");
                    _parkingTmCode = new JsonSerializer().Deserialize<Dictionary<string, string>>(sd.Data);
                    return _parkingTmCode;
                }
            }
            set { _parkingTmCode = value; }
        }

        /// <summary>
        /// 新光 Beacon Event Id
        /// </summary>
        public static int SkmActionEventInfoId
        {
            get
            {
                if (_skmActionEventInfoId == 0)
                {
                    var skmAei = mp.ActionEventInfoListGet().FirstOrDefault(x => x.ActionName.Equals("SkmBeaconEvent"));
                    if (skmAei != null)
                    {
                        _skmActionEventInfoId = skmAei.Id;
                    }
                }
                return _skmActionEventInfoId;
            }
            set { _skmActionEventInfoId = value; }
        }

        /// <summary>
        /// 取得新光預設第一筆分類的 Category Id
        /// </summary>
        public static int SkmDefaultCategoryId
        {
            get
            {
                if (_skmDefaultCategoryId == 0)
                {
                    _skmDefaultCategoryId = config.SkmDefaultDealCategordId;
                }
                return _skmDefaultCategoryId;
            }
        }

        /// <summary>
        /// 取得屬於SKM[_config.SkmCategordId(新光三越頻道 TYPE=4)]的分類
        /// </summary>
        /// <returns></returns>
        public static List<Category> GetBelongSkmCategoryList()
        {
            //所有分類都建構於_config.SkmCategordId(新光三越頻道 TYPE=4)底下
            var categorydpCol = sp.CategoryDependencyByRegionGet(config.SkmCategordId);

            List<string> filters = new List<string>();
            filters.Add(string.Format("{0} = {1}", Category.Columns.Status, (int)CategoryStatus.Enabled));
            filters.Add(string.Format("{0} = {1}", Category.Columns.Type, (int)CategoryType.DealCategory));

            //手動將舊有分類隱藏_is_show_back_end=false/已區分從此功能開始為true
            filters.Add(string.Format("{0} = {1}", Category.Columns.IsShowBackEnd, true));
            filters.Add(string.Format("{0} = {1}", Category.Columns.IsFinal, false));
            if (categorydpCol.Any())
            {
                filters.Add(string.Format("{0} in ({1})", Category.Columns.Id, string.Join(",", categorydpCol.Select(p => p.CategoryId).ToList())));
            }
            var categoryCol = sp.CategoryGetList(0, 0, Category.Columns.Rank.ToString(), filters.ToArray()).ToList();

            return categoryCol;
        }

        #endregion

        /// <summary>
        /// 商品優惠類型轉換
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static int GetQrCodeDiscountTypeVal(Guid bid)
        {
            var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);

            int discountType = 0;

            if (deal.DiscountType == (int)LunchKingSite.Core.DiscountType.DiscountPrice)
            {
                discountType = 2;
            }
            else if (deal.DiscountType == (int)LunchKingSite.Core.DiscountType.DiscountCash)
            {
                discountType = 0;
            }
            else if (deal.DiscountType == (int)LunchKingSite.Core.DiscountType.DiscountPercent)
            {
                discountType = 1;
            }
            else if (deal.DiscountType == (int)LunchKingSite.Core.DiscountType.DiscountFree)
            {
                discountType = 3;
            }
            else if (deal.DiscountType == (int)LunchKingSite.Core.DiscountType.DiscountOther)
            {
                discountType = 4;
            }

            return discountType;
        }

        #region Skm Member 會員

        /// <summary>
        /// 置換 skm token 追蹤
        /// </summary>
        /// <param name="model"></param>
        public static int ExchangeSkmToken(SkmExchangeTokenModel model)
        {
            if (model.OriSkmToken == model.NewSkmToken)
            {
                return 0;
            }

            var memberLevel = string.IsNullOrEmpty(model.MemberCardNo) || model.MemberCardNo.Length < 4
                ? string.Empty : model.MemberCardNo.Substring(0, 4);
            var tracking = new SkmTokenTracking
            {
                ShaMemberId = model.ShaMemberId,
                MemberCardNo = model.MemberCardNo,
                MemberLevel = memberLevel,
                OriSkmToken = model.OriSkmToken,
                NewSkmToken = model.NewSkmToken,
                DeviceType = model.DeviceType
            };

            return skmEp.SaveSkmTokenTracking(tracking);
        }

        #region 驗證 skm token 是否有效

        /// <summary>
        /// 驗證 xml
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="cardCheckCode"></param>
        /// <param name="registrationId"></param>
        /// <param name="t0100"></param>
        /// <param name="cardNumber"></param>
        /// <param name="deviceOs"></param>
        /// <param name="processCode"></param>
        /// <param name="loginPassword"></param>
        /// <returns></returns>
        private static string GetSkmQueryBalanceXml(string deviceId, string cardCheckCode, string registrationId
            , string cardNumber, string deviceOs, string loginPassword)
        {
            //T0100 skm 環友欄位[訊息類別] 0100 : Request
            //T0300 skm 環友欄位[ProcessingCode交易處理代碼] 207100 : 餘額查詢
            return
                System.Net.WebUtility.UrlEncode(
                    string.Format(@"<Trans><T1300>{0}</T1300><T1200>{1}</T1200><T4100>{2}</T4100><T0221>{3}</T0221>
                                    <T0279>{4}</T0279><T0100>0100</T0100><T0200>{5}</T0200><T5509>{6}</T5509>
                                    <T0300>207100</T0300><T0232>{7}</T0232></Trans>"
                        , DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"), deviceId, cardCheckCode,
                        registrationId, cardNumber, deviceOs, loginPassword));
        }

        public static bool CheckSkmToken(string deviceId, string cardCheckCode, string registrationId
            , string cardNumber, string deviceOs, string loginPassword, string skmToken)
        {
            if (config.MakeSkmMemberValidateFail)
            {
                skmMp.SkmLogSet(new SkmLog() { SkmToken = skmToken, UserId = 0, LogType = (int)SkmLogType.CheckSkmTokenApi, InputValue = "Os=" + deviceOs, OutputValue = "MakeSkmMemberValidateFail" });
                return false;
            }

            const string skmUmallCheckSuccessCode = "00";
            const string skmUmallCheckTagName = "T3900";

            var wsResult =
                System.Net.WebUtility.UrlDecode(
                    (string)CommonFacade.InvokeWebService(cp.SkmWebserviceUrl, 30, cp.SkmWsNamespace,
                        cp.SkmWsClassName, cp.SkmWsMethod
                        , new object[]
                        {
                            GetSkmQueryBalanceXml(deviceId, cardCheckCode,
                                registrationId, cardNumber, deviceOs, loginPassword)
                        }));

            using (StringReader checkResult = new StringReader(wsResult))
            {
                using (XmlReader reader = XmlReader.Create(checkResult))
                {
                    if (reader.ReadToDescendant(skmUmallCheckTagName))
                    {
                        reader.Read();
                        skmMp.SkmLogSet(new SkmLog() { SkmToken = skmToken, UserId = 0, LogType = (int)SkmLogType.CheckSkmTokenApi, InputValue = "Os=" + deviceOs, OutputValue = skmUmallCheckTagName + "=" + reader.Value });
                        return reader.Value == skmUmallCheckSuccessCode;
                    }
                }
            }

            return false;
        }

        #endregion

        /// <summary>
        /// get SKM member
        /// </summary>
        /// <param name="token">新光會員Token</param>
        /// <param name="isShared">同意共同行銷會員</param>
        public static SkmMember MemberGetBySkmToken(string token, bool isShared)
        {
            SkmMember member = skmMp.MemberGetBySkmToken(token, isShared);
            if (member.IsLoaded)
            {
                return member;
            }

            member.SkmToken = token;
            member.CreateTime = DateTime.Now;
            member.IsEnable = true;
            member.IsShared = isShared;

            SkmMemberRegister memberRegist = new SkmMemberRegister(member);
            MemberRegisterReplyType reply = memberRegist.Process(false);
            if (reply == MemberRegisterReplyType.MemberExisted)
            {
                skmMp.InsertSkmMember(member);
                member = skmMp.MemberGetBySkmToken(token);
                if (member.UserId == 0)
                {
                    var e7LifeMember = MemberFacade.GetMember(memberRegist.GetMemberRegisterName());
                    if (e7LifeMember != null && e7LifeMember.IsLoaded)
                    {
                        FixSkmMemberLink(member, e7LifeMember);
                    }
                }
            }

            return member;
        }

        public static void FixSkmMemberLink(SkmMember skmMember, Member e7Member)
        {
            skmMember.UserId = e7Member.UniqueId;
            var ml = mp.MemberLinkGet(e7Member.UniqueId, SingleSignOnSource.SkmMember);
            if (ml.IsLoaded)
            {
                ml.ExternalUserId = skmMember.Id.ToString();
                mp.MemberLinkSet(ml);
            }
            else
            {
                MemberFacade.AddMemberLink(
                    e7Member.UniqueId,
                    SingleSignOnSource.SkmMember,
                    skmMember.Id.ToString(), e7Member.UserName);
            }
            skmMp.MemberSet(skmMember);
            skmMp.SkmLogSet(new SkmLog() { SkmToken = skmMember.SkmToken, UserId = skmMember.UserId, LogType = (int)SkmLogType.IsSharedChange, InputValue = (skmMember.IsShared) ? "IsShared=True" : "IsShared=False", OutputValue = "MemberSet is Success" });
        }

        #endregion Skm Member

        #region SkmE7LifeUtility

        public static string GetSkmTrustCode(SkmTrustCode code)
        {
            return SkmE7LifeUtility.SkmSecurity.Encrypt(new JsonSerializer().Serialize(code));
        }

        public static string GetSkmTrustCode(string trustId)
        {
            return SkmE7LifeUtility.SkmSecurity.Encrypt(new JsonSerializer().Serialize(trustId));
        }

        #endregion

        /// <summary>
        /// 記錄核銷Log
        /// </summary>
        /// <param name="SkmVerifyLog"></param>
        /// <returns></returns>
        public static bool VerifyLogSet(SkmVerifyLog SkmVerifyLog)
        {
            return skmMp.VerifyLogSet(SkmVerifyLog);
        }

        #region 檔次資料

        /// <summary>
        /// 查詢檔次資料
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="imgSize"></param>
        /// <returns></returns>
        public static SkmApiDeal GetDealByBid(Guid bid, ApiImageSize imgSize)
        {
            if (bid == Guid.Empty)
            {
                return null;
            }

            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            if (deal.BusinessHourGuid == Guid.Empty)
            {
                logger.Info("deal.BusinessHourGuid == Guid.Empty");
                return null;
            }

            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
            var rtnObj = PponDealApiManager.SkmDealForApiGet(deal, imgSize);
            if (rtnObj == null)
            {
                logger.Info("rtnObj == null");
                return null;
            }

            var externalDeal = SkmCacheFacade.GetOnlineViewExternalDeal(deal.BusinessHourGuid);
            if (externalDeal.IsLoaded == false)
            {
                logger.Info("externalDeal.IsLoaded == false");
                return null;
            }

            var combodeals = skmEp.GetExternalDealCombo(externalDeal.Guid).Where(item => !item.IsDel);

            #region 處理可核銷店名

            //取ExternalDealComboe與
            var thisExternalDealComboShops = combodeals
                .Where(c => c.Bid != null && deal.ComboDelas.Select(vpd => vpd.BusinessHourGuid).Contains((Guid)c.Bid))
                .Select(c => c.ShopCode).ToList();
            var vedStoreDisplayNames = skmEp.GetViewExternalDealStoreDisplayNameByDealGuid(externalDeal.Guid);
            var thisStoreDisplayNames = vedStoreDisplayNames.Where(d => thisExternalDealComboShops.Contains(d.ShopCode))
                .OrderBy(x => x.ShopCode).ThenBy(x => x.DisplayName).Select(x => x.DisplayName).ToList();

            int storeCnt = 0;
            foreach (var item in rtnObj.Availabilities)
            {
                if (thisStoreDisplayNames.Any())
                {
                    if (storeCnt == 0)
                    {
                        int nameCnt = 0;
                        var storeDisplayName = "";
                        foreach (var displayName in thisStoreDisplayNames)
                        {
                            nameCnt++;
                            storeDisplayName += displayName;
                            if (nameCnt != thisStoreDisplayNames.Count)
                            {
                                storeDisplayName += "\n";
                            }
                        }
                        item.StoreName = storeDisplayName;
                    }
                    else
                    {
                        item.StoreName = string.Empty;
                    }
                }
                else
                {
                    item.StoreName = SkmFacade.PrefixStoreName(item.StoreName);
                }
                storeCnt++;
            }
            #endregion

            var skmDeal = new SkmApiDeal(rtnObj);

            #region 核銷店名+導航資訊

            var skmAvailabilities = SkmFacade.GetSkmPponStoreList(deal.BusinessHourGuid);
            foreach (var item in skmAvailabilities)
            {
                var shoppeDispalyNames = vedStoreDisplayNames
                    .Where(x => x.ShopCode == item.ShopCode)
                    .Select(x => x.DisplayName).ToList();

                if (shoppeDispalyNames.Any())
                {
                    var finalDisplayName = string.Join("\n", shoppeDispalyNames);
                    item.BrandCounterName = finalDisplayName;
                    item.StoreDisplayName = finalDisplayName;
                }
                else
                {
                    item.BrandCounterName = SkmFacade.PrefixStoreName(item.BrandCounterName);
                }
            }

            skmDeal.SkmAvailabilities = skmAvailabilities;

            #endregion

            #region SKM PAY

            skmDeal.IsSkmPay = externalDeal.IsSkmPay;
            skmDeal.BurningPoint = externalDeal.SkmPayBurningPoint;

            #endregion

            #region ComboDeals

            bool mainIsOrdered = true;
            skmDeal.ComboDeals.AddRange(deal.ComboDelas.Select(x => new SkmApiComboDeal(x)));
            foreach (var cd in skmDeal.ComboDeals)
            {
                var temp = combodeals.FirstOrDefault(x => x.Bid == cd.Bid);
                cd.ShopName = temp != null ? temp.ShopName : string.Empty;
                cd.IsOrdered = SkmFacade.GetIsOrdered(cd.Bid, HttpContext.Current.User.Identity.Name);
                if (!cd.IsOrdered)
                {
                    mainIsOrdered = false; //當有一個每日限領一次的子檔未領取，則母檔的 isOrdered 則為 false
                }
            }

            #endregion

            #region 燒點

            var be = skmMp.SkmBurningEventGet(externalDeal.Guid, externalDeal.DealVersion >= (int)ExternalDealVersion.SkmPay ? externalDeal.ShopCode : string.Empty);
            if (be.IsLoaded)
            {
                skmDeal.IsBurningDeal = true;
                skmDeal.BurningPoint = be.BurningPoint;
                skmDeal.GiftCash = be.GiftPoint;
            }

            #endregion

            #region 計算使用者可購買最大數量

            skmDeal.IsOrdered = externalDeal.DealVersion >= (int)ExternalDealVersion.SkmPay ? mainIsOrdered
                : GetIsOrdered(deal, HttpContext.Current.User.Identity.Name);

            #endregion 計算使用者可購買最大數量

            //external_relaction_store.is_del可以判斷是否從後台按下架
            skmDeal.IsOffSale = externalDeal.IsDel;
            //優惠類型
            skmDeal.ActiveType = externalDeal.ActiveType;
            skmDeal.Buy = externalDeal.Buy ?? 0;
            skmDeal.Free = externalDeal.Free ?? 0;
            skmDeal.Remark = contentLite.Remark.Split(new[] { "||" }, StringSplitOptions.None)[2];
            var dealIconData = GetDealIconModelList();
            var tempIcon = dealIconData.Where(x => x.Id == externalDeal.DealIconId).ToList();
            skmDeal.IconInfo = tempIcon.Any() ? new SkmDealIcon(tempIcon.First()) : new SkmDealIcon();
            skmDeal.DeliveryStartTime = externalDeal.BusinessHourDeliverTimeS == null ? string.Empty
                : ApiSystemManager.DateTimeToDateTimeString((DateTime)externalDeal.BusinessHourDeliverTimeS);
            skmDeal.DeliveryEndTime = externalDeal.BusinessHourDeliverTimeE == null ? string.Empty
                : ApiSystemManager.DateTimeToDateTimeString((DateTime)externalDeal.BusinessHourDeliverTimeE);
            skmDeal.SoldOut = skmDeal.SoldOut;
            skmDeal.IsInstallment = SkmFacade.CheckEnableInstall(externalDeal);
            skmDeal.InstallmentInfoUrl = config.SSLSiteUrl + "/SkmDeal/SkmInstallmentInfo?price=" + skmDeal.Discount;

            return skmDeal;
        }

        #endregion

        #region 後台-編輯檔次相關

        /// <summary>
        /// 更新檔次分類
        /// </summary>
        /// <param name="deal"></param> 
        public static void UpdateSkmDealCategory(ExternalDeal deal)
        {
            ViewExternalDealCollection vdc = pp.ViewExternalDealGetListByDealrGuid(deal.Guid);
            //if has bid then update category
            if (vdc.Any(x => x.Bid != null)) // && deal.category is not null?
            {
                List<Guid> bids = vdc.Where(x => x.Bid != null).GroupBy(x => x.Bid.Value).Select(x => x.Key).ToList();
                ViewCategoryDealBaseCollection allDealCategoey = pp.ViewCategoryDealBaseGetListByBids(bids);
                CategoryDealCollection addDealcategory = new CategoryDealCollection();
                foreach (Guid bid in bids)
                {
                    //clean type 6
                    pp.DeleteCategoryDealsByCategoryType(bid, CategoryType.DealCategory);
                    //add
                    if (!string.IsNullOrEmpty(deal.CategoryList))
                    {
                        foreach (int cid in deal.CategoryList.Trim('[', ']').Split(",").Select(int.Parse).ToArray())
                        {
                            CategoryDeal cd = new CategoryDeal();
                            cd.Bid = bid;
                            cd.Cid = cid;
                            addDealcategory.Add(cd);
                        }
                    }
                }
                pp.CategoryDealSetList(addDealcategory);
            }
        }

        /// <summary>
        /// 更新檔次底層架構資料 (business_hour, item, deal_property, coupon_event_content, ppon_store)
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="mainBid"></param>
        /// <param name="deal"></param>
        /// <param name="qty"></param>
        /// <param name="shopCode"></param>
        public static void UpdateBusinessHourData(Guid bid, Guid mainBid, ExternalDeal deal, int qty, string shopCode)
        {
            //business_hour
            var bh = pp.BusinessHourGet(bid);
            bh.BusinessHourOrderTimeS = deal.BusinessHourOrderTimeS;
            bh.BusinessHourOrderTimeE = deal.BusinessHourOrderTimeE;
            bh.BusinessHourDeliverTimeE = deal.BusinessHourDeliverTimeE;
            bh.BusinessHourDeliverTimeS = deal.BusinessHourDeliverTimeS;
            bh.SettlementTime = deal.SettlementTime;
            bh.OrderTotalLimit = bid == mainBid ? deal.OrderTotalLimit : qty;
            pp.BusinessHourSet(bh);
            //item
            var item = it.ItemGetByBid(bid);
            item.ItemName = deal.Title;
            item.ItemOrigPrice = (decimal)deal.ItemOrigPrice;
            if (deal.IsPrizeDeal || deal.RepeatPurchaseType == (int)SkmDealRepeatPurchaseType.None)
            {
                item.ItemDefaultDailyAmount = item.MaxItemCount = 9999999;
            }
            else
            {
                item.ItemDefaultDailyAmount = item.MaxItemCount = deal.RepeatPurchaseCount;
            }

            it.ItemSet(item);
            //deal_property
            var dp = pp.DealPropertyGet(bid);
            dp.ExchangePrice = deal.Discount;
            dp.DiscountType = deal.DiscountType;
            dp.Discount = deal.Discount;
            if (deal.IsPrizeDeal  //新邏輯要再綁上選取每人每日限購選項, 數量填在ItemDefaultDailyAmount
                || (config.EnableSkmDealRepeatPurchase
                    && deal.DealVersion == (int)ExternalDealVersion.SkmPay
                    && deal.RepeatPurchaseType == (int)SkmDealRepeatPurchaseType.Daily))
            {
                dp.IsDailyRestriction = true;
            }
            else //舊邏輯 若為IsPrizeDeal則限每人每日一次/ 或是skmPay沒有勾選重複購買時, 也限制IsDailyRestriction=true
            {
                dp.IsDailyRestriction =
                    (deal.DealVersion == (int)ExternalDealVersion.SkmPay ? deal.IsRepeatPurchase : false);
            }

            pp.DealPropertySet(dp);
            //coupon_event_content
            var cec = pp.CouponEventContentGetByBid(bid);

            string restrictions = HttpUtility.HtmlDecode(deal.Introduction); //更多權益說明 ==注意事項 == Introduction
            bool onlySpaceOrBreak = string.IsNullOrWhiteSpace(restrictions.Replace("<br />", "").Replace("<br >", "").Replace("<br>", ""));
            if (onlySpaceOrBreak)
            {
                cec.Introduction = cec.Restrictions = "";
            }
            else
            {
                cec.Introduction = cec.Restrictions = restrictions;
            }

            cec.Description = HttpUtility.HtmlDecode(deal.Description);
            cec.AppDescription = ViewPponDealManager.RemoveSpecialHtmlForApp(cec.Description);
            cec.Remark = 0 + "||" + string.Empty + "||" + HttpUtility.HtmlDecode(deal.Remark);
            cec.ImagePath = deal.ImagePath;
            cec.AppDealPic = deal.AppDealPic;
            cec.Name = deal.Title;
            cec.Title = deal.Title;
            cec.AppTitle = deal.Title;
            cec.CouponUsage = deal.Title;
            cec.ModifyTime = DateTime.Now;

            //ppon_store
            pp.PponStoreDeleteByBid(bid);
            var insStores = new PponStoreCollection();
            var stores = new List<Guid>();
            var ved = pp.ViewExternalDealGetListByBid(mainBid);

            if (ved.Any())
            {
                var temp = ved.ToList();
                if (bid != mainBid) //子檔
                {
                    temp = temp.Where(x => x.ShopCode == shopCode).ToList();
                }
                stores = temp.Select(x => x.StoreGuid).ToList();
            }

            var username = config.SkmDealSales;
            var store_sort = 1;
            foreach (var store in stores)
            {
                var pponStore = new PponStore();
                pponStore.BusinessHourGuid = bid;
                pponStore.StoreGuid = store;
                pponStore.ResourceGuid = Guid.NewGuid();
                pponStore.CreateId = username;
                pponStore.CreateTime = DateTime.Now;
                pponStore.SortOrder = store_sort;
                pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Location));
                pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Verify));
                pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Accouting));
                pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.ViewBalanceSheet));
                pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.VerifyShop));
                store_sort += 1;
                insStores.Add(pponStore);
            }
            pp.PponStoreSetList(insStores);
            //shoppe
            var availables = new List<AvailableInformation>();
            foreach (var st in insStores)
            {
                var store = sp.SellerGet(st.StoreGuid);
                var viewstorecategoryCol = sp.ViewStoreCategoryCollectionGetByStore(st.StoreGuid);
                var mrtInfo = string.Empty;
                if (viewstorecategoryCol.Any())
                {
                    mrtInfo = string.Join(",", viewstorecategoryCol.Select(x => x.Name).ToArray());
                }

                SqlGeography geo = LocationFacade.GetGeographyByCoordinate(store.Coordinate);
                var tmp = new AvailableInformation()
                {
                    N = GetSkmShoppeDisplayName(st.StoreGuid),
                    P = store.StoreTel,
                    A = string.Empty,
                    Longitude = geo != null ? geo.Long.ToString() : string.Empty,
                    Latitude = geo != null ? geo.Lat.ToString() : string.Empty,
                    OT = store.OpenTime,
                    UT = st.UseTime,
                    U = store.WebUrl,
                    R = store.StoreRemark,
                    CD = store.CloseDate,
                    MR = string.Empty,
                    CA = store.Car,
                    BU = store.Bus,
                    OV = store.OtherVehicles,
                    FB = store.FacebookUrl,
                    PL = string.Empty,
                    BL = store.BlogUrl,
                    OL = store.OtherUrl
                };
                availables.Add(tmp);
            }

            if (availables.Count >= 0)
            {
                cec.Availability = new JsonSerializer().Serialize(availables);
            }
            pp.CouponEventContentSet(cec);
        }

        /// <summary>
        /// 設定SKM檔次庫存量
        /// </summary>
        /// <param name="skmGiftInventory"><see cref="SkmGiftInventoryModel"/></param>
        /// <returns>是否設定成功</returns>
        public static bool SetSkmExternalDealInventory(SkmGiftInventoryModel skmGiftInventory, out string message)
        {
            message = string.Empty;
            SkmSetExternalDealInventoryLog skmSetExternalDealInventoryLog = new SkmSetExternalDealInventoryLog()
            {
                ExternalDealGuid = skmGiftInventory.ExternalDealGuid,
                ExchangeItemId = skmGiftInventory.ExchangeItemId,
                SkmEventId = skmGiftInventory.SkmEventId,
                ShopCode = skmGiftInventory.ShopCode,
                CreateUser = "0",
                CreateDate = DateTime.Now,
            };
            skmEp.InsertSkmSetExternalDealInventoryLog(skmSetExternalDealInventoryLog);
            ExternalDealCombo externalDealCombo = skmEp.GetExternalDealComboByGuidShopCode(skmGiftInventory.ExternalDealGuid, skmGiftInventory.ShopCode);
            BusinessHour businessHour = null;
            if (externalDealCombo != null &&
                externalDealCombo.ExternalDealGuid == skmGiftInventory.ExternalDealGuid &&
                externalDealCombo.Bid.HasValue)
            {
                businessHour = pp.BusinessHourGet(externalDealCombo.Bid.Value);
                //記錄更新前庫存
                skmSetExternalDealInventoryLog.OrigionBusinessHourData = new JsonSerializer().Serialize(businessHour);
                skmEp.UpdateSkmSetExternalDealInventoryLog(skmSetExternalDealInventoryLog);

                if (businessHour != null && businessHour.Guid == externalDealCombo.Bid.Value)
                {
                    //查詢燒點活動建檔
                    ViewSkmDealQuantity viewSkmBurningEventQuantity = skmEp.DealQuantityByBid(externalDealCombo.Bid.Value);
                    if (viewSkmBurningEventQuantity != null &&
                        viewSkmBurningEventQuantity.OrderTotalLimit + skmGiftInventory.AddQty >= viewSkmBurningEventQuantity.OrderedQuantity)
                    {
                        //更新 businessHour 庫存數
                        businessHour.OrderTotalLimit = viewSkmBurningEventQuantity.OrderTotalLimit + skmGiftInventory.AddQty;
                        pp.BusinessHourSet(businessHour);

                        //更新子檔庫存數
                        skmEp.SetExternalDealComboQty(skmGiftInventory.ExternalDealGuid, skmGiftInventory.ShopCode, (int)(viewSkmBurningEventQuantity.OrderTotalLimit + skmGiftInventory.AddQty));

                        //記錄更新後庫存
                        skmSetExternalDealInventoryLog.UpdateBusinessHourData = new JsonSerializer().Serialize(businessHour);
                        skmEp.UpdateSkmSetExternalDealInventoryLog(skmSetExternalDealInventoryLog);

                        return true;
                    }
                    else { message = "超過可調整庫存量下限"; }
                }
                else { message = "查無檔次資料!!"; }
            }
            else { message = "查無檔次資料!"; }
            return false;
        }

        /// <summary>
        /// 更新新光外掛檔次資料 (external_deal, external_deal_combo, external_deal_realation_stroe)
        /// </summary>
        /// <param name="deal"></param>
        public static void UpdateDeal(ExternalDeal deal)
        {
            ViewExternalDealCollection vdc = pp.ViewExternalDealGetListByDealrGuid(deal.Guid);
            //if has bid then update category
            if (vdc.Any(x => x.Bid != null)) // && deal.category is not null?
            {
                List<Guid> bids = vdc.Where(x => x.Bid != null).GroupBy(x => x.Bid.Value).Select(x => x.Key).ToList();
                foreach (Guid bid in bids)
                {
                    var parentSellerGuid = pp.ExternalDealRelationStoreGetListByDeal(deal.Guid).Where(x => x.Bid != null).Select(x => x.ParentSellerGuid).FirstOrDefault();
                    //exterl_deal_realation_stroe
                    var edrs = pp.ExternalDealRelationStoreGetListByDeal(deal.Guid);
                    foreach (var entity in edrs)
                    {
                        if (entity.Bid == null && entity.ParentSellerGuid == parentSellerGuid)
                        {
                            entity.Bid = bid;
                        }
                    }
                    pp.ExternalDealRelationStoreCollectionSet(edrs);
                    UpdateBusinessHourData(bid, bid, deal, 0, string.Empty);
                    var combo = skmEp.GetExternalDealComboByMainBid(bid);
                    foreach (var c in combo)
                    {
                        UpdateBusinessHourData((Guid)c.Bid, bid, deal, c.Qty, c.ShopCode);
                    }
                }
            }
        }

        /// <summary>
        /// 將 SKM 單檔轉成 PponDeal
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static PponDeal ConvertPponDeal(ViewExternalDeal deal)
        {
            return ConvertPponDeal(deal, new ExternalDealCombo());
        }

        /// <summary>
        /// 將 SKM 子母檔轉成 PponDeal
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="combo"></param>
        /// <returns></returns>
        public static PponDeal ConvertPponDeal(ViewExternalDeal deal, ExternalDealCombo combo)
        {
            var isSkmPay = deal.DealVersion == (int)ExternalDealVersion.SkmPay;
            PponDeal ret;
            ret = new PponDeal(true);
            //天貓檔次新增標示
            ret.DealContent.Name = deal.Title;
            ret.Deal.BusinessHourOnline = true;
            ret.ItemDetail.ItemPrice = 0;
            ret.ItemDetail.ItemOrigPrice = deal.ItemOrigPrice.Value;
            ret.ItemDetail.ItemName = deal.Title;
            ret.Deal.BusinessHourOrderTimeS = deal.BusinessHourOrderTimeS;
            ret.Deal.BusinessHourOrderTimeE = deal.BusinessHourOrderTimeE;
            ret.Deal.OrderTotalLimit = isSkmPay && combo != null ? combo.Qty : deal.OrderTotalLimit;
            string restrictions = HttpUtility.HtmlDecode(deal.Introduction); //更多權益說明 ==注意事項 == Introduction
            bool onlySpaceOrBreak = string.IsNullOrWhiteSpace(restrictions.Replace("<br />", "").Replace("<br >", "").Replace("<br>", ""));
            if (onlySpaceOrBreak)
            {
                ret.DealContent.Introduction = ret.DealContent.Restrictions = "";
            }
            else
            {
                ret.DealContent.Introduction = ret.DealContent.Restrictions = restrictions;
            }

            ret.DealContent.CouponUsage = deal.Title;
            ret.DealContent.Description = HttpUtility.HtmlDecode(deal.Description);
            ret.DealContent.AppDescription = ViewPponDealManager.RemoveSpecialHtmlForApp(ret.DealContent.Description);
            ret.DealContent.AppTitle = deal.Title;
            ret.DealContent.Title = deal.Title;
            ret.DealContent.Remark = 0 + "||" + string.Empty + "||" + HttpUtility.HtmlDecode(deal.Remark);
            ret.Deal.BusinessHourDeliverTimeS = deal.BusinessHourDeliverTimeS;
            ret.Deal.BusinessHourDeliverTimeE = deal.BusinessHourDeliverTimeE;
            ret.Deal.SettlementTime = deal.SettlementTime;
            if (deal.IsPrizeDeal || deal.RepeatPurchaseType == (int)SkmDealRepeatPurchaseType.None)
            {
                ret.ItemDetail.ItemDefaultDailyAmount = ret.ItemDetail.MaxItemCount = 9999999;
            }
            else
            {
                ret.ItemDetail.ItemDefaultDailyAmount = ret.ItemDetail.MaxItemCount = deal.RepeatPurchaseCount;
            }

            return ret;
        }

        /// <summary>
        /// 取得檔次櫃位列表
        /// </summary>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public static List<SkmPponStore> GetSkmPponStoreList(Guid businessHourGuid)
        {
            List<SkmPponStore> skmStores = new List<SkmPponStore>();
            ViewPponStoreSkmCollection pponStoresSkm = sp.ViewPponStoreSkmGetByBid(businessHourGuid);
            var externalCombo = skmEp.GetExternalDealComboByMainBid(businessHourGuid);
            foreach (var store in pponStoresSkm)
            {
                var displayName = string.Empty;
                if (store.ShopName == store.BrandCounterName)
                {
                    displayName = store.BrandCounterName;
                }
                else
                {
                    var shoppe = skmMp.SkmShoppeGet(store.StoreGuid);
                    if (shoppe.IsLoaded)
                    {
                        displayName = string.Format("{0}{1} {2}", shoppe.ShopName, shoppe.Floor, shoppe.IsExchange ? "贈品處" : PrefixStoreName(shoppe.BrandCounterName));
                    }
                }

                var skmStore = new SkmPponStore()
                {
                    //externalCombo若為null，表示帶入的bid為子檔
                    Bid = externalCombo.Where(x => x.ShopCode == store.ShopCode)
                        .Select(x => x.Bid).FirstOrDefault() ?? store.BusinessHourGuid,
                    SellerGuid = store.SellerGuid,
                    SellerName = store.SellerName,
                    ShopCode = store.ShopCode,
                    ShopName = store.ShopName,
                    BrandCounterCode = store.BrandCounterCode,
                    BrandCounterName = store.BrandCounterName,
                    Floor = store.Floor,
                    StoreDisplayName = displayName,
                    StoreGuid = store.SkmStoreGuid ?? Guid.Empty,
                    BrandGuid = store.BrandGuid ?? Guid.Empty
                };
                skmStores.Add(skmStore);
            }

            return skmStores;
        }

        /// <summary>
        /// 檔次櫃位資料
        /// </summary>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public static List<UsingShoppeLite> GetSkmUsingShoppeLiteList(Guid businessHourGuid)
        {
            var ed = SkmCacheFacade.GetOnlineViewExternalDeal(businessHourGuid);
            List<UsingShoppeLite> skmUsingShoppeList = new List<UsingShoppeLite>();
            var pponStoresSkm = sp.ViewPponStoreSkmGetByBid(businessHourGuid);

            var shopList = pponStoresSkm.Select(x => x.ShopCode).Distinct();

            foreach (var shop in shopList)
            {
                if (shop != ed.ShopCode)
                {
                    continue;
                }

                var brandCounterList = pponStoresSkm.Where(x => x.ShopCode == shop).ToList();
                var isAllShoppe = brandCounterList.Any(x => x.IsShoppe == false);

                if (isAllShoppe) //全館
                {
                    skmUsingShoppeList.Add(new UsingShoppeLite()
                    {
                        StoreCode = shop,
                        IsAllShoppe = 1
                    });
                    continue;
                }

                var shoppeList = new List<string>();
                foreach (var brandCounter in brandCounterList)
                {
                    shoppeList.Add(brandCounter.BrandCounterCode);
                }

                var usingShoppe = new UsingShoppeLite()
                {
                    StoreCode = shop,
                    IsAllShoppe = 0,
                    Shoppe = shoppeList.ToArray(),
                };
                skmUsingShoppeList.Add(usingShoppe);
            }

            return skmUsingShoppeList;
        }

        #region Old Shoppe Data Delete 2016/1/31

        public static List<UsingShoppe> GetSkmUsingShoppeList(Guid businessHourGuid)
        {
            List<UsingShoppe> skmUsingShoppeList = new List<UsingShoppe>();
            var pponStoresSkm = sp.ViewPponStoreSkmGetByBid(businessHourGuid);

            var shopList = pponStoresSkm.Select(x => x.ShopCode).Distinct();

            foreach (var shop in shopList)
            {
                var brandCounterList = pponStoresSkm.Where(x => x.ShopCode == shop).ToList();
                var isAllShoppe = brandCounterList.Any(x => x.IsShoppe == false);

                if (isAllShoppe) //全館
                {
                    skmUsingShoppeList.Add(new UsingShoppe()
                    {
                        StoreCode = shop,
                        IsAllShoppe = true
                    });
                    continue;
                }

                var shoppeList = new List<string>();
                foreach (var brandCounter in brandCounterList)
                {
                    shoppeList.Add(brandCounter.BrandCounterCode);
                }

                var usingShoppe = new UsingShoppe()
                {
                    StoreCode = shop,
                    IsAllShoppe = false,
                    Shoppe = shoppeList.ToArray(),
                };
                skmUsingShoppeList.Add(usingShoppe);
            }

            return skmUsingShoppeList;
        }

        #endregion

        #endregion

        #region 櫃位相關

        /// <summary>
        /// 取得櫃位顯示名稱
        /// </summary>
        /// <param name="storeGuid"></param>
        /// <returns></returns>
        public static string GetSkmShoppeDisplayName(Guid storeGuid)
        {
            var shoppe = skmMp.SkmShoppeGet(storeGuid);
            if (!shoppe.IsLoaded)
            {
                return null;
            }

            var displayName = shoppe.ShopName == shoppe.BrandCounterName ? shoppe.BrandCounterName : string.Format("{0}{1} {2}", shoppe.ShopName, shoppe.Floor, shoppe.IsExchange ? "贈品處" : PrefixStoreName(shoppe.BrandCounterName));

            return displayName;
        }

        /// <summary>
        /// 取得櫃位顯示名稱
        /// </summary>
        /// <param name="shoppe"></param>
        /// <returns></returns>
        public static string GetSkmShoppeDisplayName(SkmShoppe shoppe)
        {
            var displayName = shoppe.ShopName == shoppe.BrandCounterName ? shoppe.BrandCounterName : string.Format("{0}{1} {2}", shoppe.ShopName, shoppe.Floor, shoppe.IsExchange ? "贈品處" : PrefixStoreName(shoppe.BrandCounterName));

            return displayName;
        }

        /// <summary>
        /// 濾除櫃位多餘資訊
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static string PrefixStoreName(string result)
        {
            result = CutStoreName(result);
            result = ReplaceStoreName(result);
            result = RemoveRepeatStoreName(result);
            return result;
        }

        #endregion

        #region Private Method

        /// <summary>
        /// datetime convert (Int64)yyyyMMddHHmmss
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private static long DateTimeConvertLong(DateTime dt)
        {
            return long.Parse(dt.ToString("yyyyMMddHHmmss"));
        }

        private static string DateTimeTicks(DateTime times, out DateTime LastTimes)
        {
            LastTimes = DateTime.Now;
            return (DateTime.Now - times).TotalSeconds.ToString() + "秒<br/>";
        }

        public static void DealApplyNotify(ExternalDeal deal)
        {
            if (!string.IsNullOrEmpty(config.SkmDealBuildEmail))
            {
                MailMessage msg = new MailMessage();
                foreach (string email in config.SkmDealBuildEmail.Split(';'))
                {
                    msg.To.Add(email);
                }
                msg.Subject = string.Format("新光三越檔次提報通知【{0}】", deal.Title);
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = string.Format(@"您好，新光三越已於【{0}】申請【{1}】製檔， 
                                煩請前往<a href='{2}' target='skm_window'>新光三越管理後台</a>了解處理，謝謝。"
                            , System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                            , deal.Title
                            , string.Format("{0}/SKMDeal/SKMDealInfo?guid={1}", config.SiteUrl, deal.Guid));
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        public static string OnSaveBusinessHour(ViewExternalDealCollection deals, string username, bool isBurningEvent, out Dictionary<Guid, Guid> bids)
        {
            bids = new Dictionary<Guid, Guid>();
            string resultMsg = string.Empty;
            username = config.SkmDealSales;
            ViewExternalDealCollection newdeals = new ViewExternalDealCollection();
            var isHqDeal = deals.First().IsHqDeal;
            HashSet<Guid> tempNewDeal = new HashSet<Guid>();

            foreach (ViewExternalDeal deal in deals.OrderByDescending(x => x.IsShoppe))
            {
                //判斷是否全館
                if (deal.ParentSellerGuid == config.SkmRootSellerGuid)
                {
                    if (tempNewDeal.Contains(deal.SellerGuid) == false && deal.Bid == null)
                    {
                        newdeals.Add(deal);
                        tempNewDeal.Add(deal.ParentSellerGuid);
                    }
                }
                else
                {
                    if (tempNewDeal.Contains(deal.ParentSellerGuid) == false && deal.Bid == null)
                    {
                        newdeals.Add(deal);
                        tempNewDeal.Add(deal.ParentSellerGuid);
                    }
                }

                if (newdeals.Any() && isHqDeal)
                {
                    break;
                }
            }

            foreach (ViewExternalDeal deal in newdeals)
            {
                PponDeal data = ConvertPponDeal(deal);
                PponDeal entity;
                //bool orderStartDateChange = true;//好康開檔日是否有異動。
                bool success;
                bool IsNew = false;
                var insStores = new PponStoreCollection();
                var delStores = new PponStoreCollection();
                DateTime LastTimes = DateTime.Now;
                StringBuilder processBuilder = new StringBuilder();
                processBuilder.AppendLine("OnSaveDeal(Begin) = " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.ffff") + "<br/>");

                // add mode

                #region add mode
                processBuilder.AppendLine("add mode = " + DateTimeTicks(LastTimes, out LastTimes));
                IsNew = true;

                var sellerGuid = isHqDeal ? config.SkmRootSellerGuid : deal.ParentSellerGuid == config.SkmRootSellerGuid ? deal.SellerGuid : deal.ParentSellerGuid;
                entity = data;
                // now let's connect the dots
                entity.Store = sp.SellerGet(isHqDeal ? config.SkmRootSellerGuid : deal.ParentSellerGuid);
                entity.Deal.BusinessHourTypeId = (int)BusinessHourType.Ppon;
                entity.Deal.SellerGuid = sellerGuid;
                entity.Deal.Guid = Helper.GetNewGuid(entity);
                entity.DealContent.BusinessHourGuid = entity.Deal.Guid;

                entity.ItemDetail.BusinessHourGuid = entity.Deal.Guid;
                entity.ItemDetail.MenuGuid = Guid.Empty;
                entity.ItemDetail.Guid = Helper.GetNewGuid(entity);
                entity.Deal.CreateTime = entity.ItemDetail.CreateTime = DateTime.Now;
                entity.Deal.CreateId = entity.ItemDetail.CreateId = username;
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(true, entity.Deal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice);
                PromotionFacade.DiscountLimitSet(entity.Deal.Guid, username, DiscountLimitType.Enabled);

                entity.DealContent.AppDealPic = deal.AppDealPic;
                entity.DealContent.ImagePath = deal.ImagePath;

                #region PponStore
                processBuilder.AppendLine("PponStore = " + DateTimeTicks(LastTimes, out LastTimes));

                List<Guid> stores = new List<Guid>();

                if (isHqDeal)
                {
                    stores = deals.ToList().GroupBy(x => x.StoreGuid).Select(x => x.Key).ToList();
                }
                else
                {
                    //是否為非全館
                    var isShoppe = skmMp.SkmShoppeGet(deal.StoreGuid).IsShoppe;

                    stores = isShoppe ? deals.Where(x => x.ParentSellerGuid == deal.ParentSellerGuid && x.Bid == null).Select(x => x.StoreGuid).ToList()
                        : deals.Where(x => x.ParentSellerGuid == config.SkmRootSellerGuid && x.Bid == null && x.SellerGuid == deal.SellerGuid).Select(x => x.StoreGuid).ToList();
                    //增加判斷同一分店是否有其他館櫃
                    stores.AddRange(deals.Where(x => x.SellerGuid == deal.ParentSellerGuid && x.ParentSellerGuid == config.SkmRootSellerGuid).Select(x => x.StoreGuid).ToList());
                }

                var storeSort = 1;
                foreach (var store in stores)
                {
                    var pponStore = new PponStore();
                    pponStore.BusinessHourGuid = entity.Deal.Guid;
                    pponStore.StoreGuid = store;
                    pponStore.ResourceGuid = Guid.NewGuid();
                    pponStore.CreateId = username;
                    pponStore.CreateTime = DateTime.Now;
                    pponStore.SortOrder = storeSort;
                    pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Location));
                    pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Verify));
                    pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Accouting));
                    pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.ViewBalanceSheet));
                    pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.VerifyShop));
                    storeSort += 1;
                    insStores.Add(pponStore);
                }

                #endregion PponStore

                #endregion add mode

                if (PponFacade.CheckPponIntroductionValid(data) == false)
                {
                    resultMsg = "「權益說明」「更多權益」有無法解析的Html語法，請重新填寫或洽IT處理。";
                    return resultMsg;
                }

                processBuilder.AppendLine("pp.ViewPponDealGetByBusinessHourGuid = " + DateTimeTicks(LastTimes, out LastTimes));

                #region 好康哪裡找
                processBuilder.AppendLine("好康哪裡找 = " + DateTimeTicks(LastTimes, out LastTimes));
                //如果有輸入分店資料，以分店資料產生好康哪裡找的紀錄
                var availables = new List<AvailableInformation>();
                foreach (var st in insStores.OrderBy(x => x.SortOrder))
                {
                    var store = sp.SellerGet(st.StoreGuid);
                    var viewstorecategoryCol = sp.ViewStoreCategoryCollectionGetByStore(st.StoreGuid);
                    var mrtInfo = string.Empty;
                    if (viewstorecategoryCol.Any())
                    {
                        mrtInfo = string.Join(",", viewstorecategoryCol.Select(x => x.Name).ToArray());
                    }

                    SqlGeography geo = LocationFacade.GetGeographyByCoordinate(store.Coordinate);
                    var tmp = new AvailableInformation()
                    {
                        N = GetSkmShoppeDisplayName(st.StoreGuid),//store.SellerName,
                        P = store.StoreTel,
                        A = string.Empty,
                        Longitude = geo != null ? geo.Long.ToString() : string.Empty,
                        Latitude = geo != null ? geo.Lat.ToString() : string.Empty,
                        OT = store.OpenTime,
                        UT = st.UseTime,
                        U = store.WebUrl,
                        R = store.StoreRemark,
                        CD = store.CloseDate,
                        MR = string.Empty,
                        CA = store.Car,
                        BU = store.Bus,
                        OV = store.OtherVehicles,
                        FB = store.FacebookUrl,
                        PL = string.Empty,
                        BL = store.BlogUrl,
                        OL = store.OtherUrl
                    };
                    availables.Add(tmp);
                }

                if (availables.Count >= 0)
                {
                    entity.DealContent.Availability = new JsonSerializer().Serialize(availables);
                }

                #endregion 好康哪裡找

                #region DealProperty
                processBuilder.AppendLine("DealProperty = " + DateTimeTicks(LastTimes, out LastTimes));
                DealProperty dealProperty = pp.DealPropertyGet(entity.Deal.Guid);

                dealProperty.BusinessHourGuid = entity.Deal.Guid;
                dealProperty.CreateId = username;
                dealProperty.CreateTime = DateTime.Now;

                var empInfo = hp.EmployeeGet(Employee.Columns.UserId, mp.MemberGet(username).UniqueId);
                dealProperty.DealEmpName = empInfo.EmpName;

                //用業務名稱抓取對應Employee EmpNo 寫進dealProperty這個資料表 若對應不到 則無需填寫
                if (empInfo.IsLoaded && empInfo != null && (!IsNew | (!empInfo.IsInvisible && empInfo.DepartureDate == null)))
                {
                    dealProperty.EmpNo = empInfo.EmpNo;
                    dealProperty.DevelopeSalesId = empInfo.UserId;
                }
                else
                {
                    resultMsg = "請填入有效業務";
                    return resultMsg;
                }

                dealProperty.DeliveryType = (int)DeliveryType.ToShop;
                dealProperty.ShoppingCart = false;
                dealProperty.MultipleBranch = false;
                dealProperty.ComboPackCount = 1;
                dealProperty.PresentQuantity = 0;
                dealProperty.SaleMultipleBase = 0;
                dealProperty.GroupCouponAppStyle = 0;
                dealProperty.IsQuantityMultiplier = false;
                dealProperty.IsAveragePrice = false;
                dealProperty.ActivityUrl = string.Empty;
                dealProperty.IsDailyRestriction = deal.IsPrizeDeal ? true : deal.DealVersion == (int)ExternalDealVersion.SkmPay ? deal.IsRepeatPurchase : false;

                //設定BarcodeType
                dealProperty.CouponCodeType = 0;
                dealProperty.PinType = 0;

                dealProperty.ExchangePrice = deal.Discount;
                //for skm
                dealProperty.DiscountType = deal.DiscountType;
                dealProperty.Discount = deal.Discount;
                dealProperty.IsExperience = deal.ActiveType == (int)ActiveType.Experience;

                dealProperty.BookingSystemType = 0;
                dealProperty.AdvanceReservationDays = 0;
                dealProperty.CouponUsers = 0;
                dealProperty.IsReserveLock = false;

                dealProperty.TmallRmbPrice = 0;
                dealProperty.TmallRmbExchangeRate = 0;

                dealProperty.IsMergeCount = false;
                dealProperty.IsZeroActivityShowCoupon = false;
                dealProperty.IsLongContract = false;
                //設定台新分期
                dealProperty.Installment3months = false;
                dealProperty.Installment6months = false;
                dealProperty.Installment12months = false;
                dealProperty.DenyInstallment = false;

                dealProperty.ShipType = entity.Property.ShipType;
                dealProperty.ShippingdateType = entity.Property.ShippingdateType;
                dealProperty.Shippingdate = entity.Property.Shippingdate;
                dealProperty.ProductUseDateStartSet = entity.Property.ProductUseDateStartSet;
                dealProperty.ProductUseDateEndSet = entity.Property.ProductUseDateEndSet;
                dealProperty.LabelIconList = ((int)DealLabelSystemCode.AppLimitedEdition).ToString();
                #endregion DealProperty
                var dAccounting = new DealAccounting();
                dAccounting.BusinessHourGuid = entity.Deal.Guid;
                dAccounting.SalesCommission = 0.2;
                dAccounting.SalesBonus = 0;
                dAccounting.Status = (int)AccountsPayableFormula.Default;
                dAccounting.SalesId = empInfo.EmpName;
                dAccounting.Commission = 0;
                dAccounting.RemittanceType = 0;
                dAccounting.VendorBillingModel = 0;
                dAccounting.VendorReceiptType = 0;
                dAccounting.IsInputTaxRequired = false;

                #region Save Category Deal data
                processBuilder.AppendLine("Save Category Deal data = " + DateTimeTicks(LastTimes, out LastTimes));
                CategoryDealCollection cds = new CategoryDealCollection();

                #region 新版的頻道、區域、分類之處理
                //todo

                //行政區分店自動檢查category
                List<ViewCategoryDependency> vcdList = sp.ViewCategoryDependencyGetByParentType((int)CategoryType.PponChannelArea).ToList();
                foreach (PponStore pponStore in insStores)
                {
                    Store store = sp.StoreGet(pponStore.StoreGuid);

                    if (store.IsLoaded && store.CityId.HasValue && store.TownshipId.HasValue)
                    {
                        var township = CityManager.TownShipGetById(store.TownshipId.Value);
                        var parentcity = CityManager.CityGetById(store.CityId.Value);
                        if (township != null && parentcity != null)
                        {
                            ViewCategoryDependency vcd = vcdList.Where(x => x.ParentName == parentcity.CityName && x.CategoryName == township.CityName).DefaultIfEmpty(null).First();
                            if (vcd != null && !cds.Any(x => x.Bid == entity.Deal.Guid && x.Cid == vcd.ParentId))
                            {
                                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.ParentId });
                            }
                            if (vcd != null && !cds.Any(x => x.Bid == entity.Deal.Guid && x.Cid == vcd.CategoryId))
                            {
                                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.CategoryId });
                            }
                        }

                    }
                }

                ///first create store_category data to store_guid bind category_code
                ///and uses stores.store_guid to find categoryid 
                ///and set the final node and find parent node to set in type 5 4
                ViewStoreCategoryCollection vscc = sp.ViewStoreCategoryCollectionGetByStoreList(stores);
                //type5
                if (vscc.Count > 0)
                {
                    List<int> nodeList = new List<int>();
                    foreach (ViewStoreCategory vsc in vscc)
                    {
                        if (!cds.Any(x => x.Cid == vsc.CategoryId.Value))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vsc.CategoryId.Value });
                        }
                        if (!cds.Any(x => x.Cid == vsc.ParentId))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vsc.ParentId });
                            if (!nodeList.Contains(vsc.ParentId))
                            {
                                nodeList.Add(vsc.ParentId);
                            }
                        }
                    }
                    ViewCategoryDependencyCollection vcdc = sp.ViewCategoryDependencyGetByCategoryIdList(nodeList);
                    foreach (ViewCategoryDependency vcd in vcdc)
                    {
                        if (!cds.Any(x => x.Cid == vcd.ParentId))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.ParentId });
                        }
                    }
                }

                //type6
                List<int> selectedDealCategories = deal.CategoryList != null ? deal.CategoryList.Trim('[', ']').Split(",").Select(int.Parse).ToList() : new List<int>();
                selectedDealCategories.Add(config.SkmCategordId);
                foreach (var dealCategoryId in selectedDealCategories)
                {
                    cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = dealCategoryId });
                }


                ////檔次的特殊類別(ICON區塊)
                //List<int> selectedSpecialCategories = View.SelectedSpecialCategories;
                //foreach (var specialCategoryId in selectedSpecialCategories)
                //{
                //    //特殊區塊有24與72小時的須設定，所以也要查詢對應的cityId
                //    PponCity city = PponCityGroup.GetPponCityByCategoryId(specialCategoryId);
                //    if (city != null)
                //    {
                //        selectedCity.Add(city.CityId);
                //    }
                //    if (specialCategoryId != FamilyLockCategoryId && specialCategoryId != FamilyBeaconUnLockCategoryId)
                //    {
                //        cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = specialCategoryId });
                //    }
                //}


                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = CategoryManager.Default.AppLimitedEdition.CategoryId });

                #endregion 新版的頻道、區域、分類之處理

                processBuilder.AppendLine("//Save Category Deal data = " + DateTimeTicks(LastTimes, out LastTimes));
                #endregion Save Category Deal data

                //將選取的城市紀錄於dealProperty
                dealProperty.CityList = new JsonSerializer().Serialize(new int[] { config.SkmCityId });
                dealProperty.IsTravelDeal = false;

                //使用者選取的城市， 新版分類上線後，改由 新分類設定的頻道對應產生城市編號
                int[] cities = new int[] { config.SkmCityId };

                //取得原本設定的Dts
                DealTimeSlotCollection origDtsCol = new DealTimeSlotCollection();
                entity.TimeSlotCollection.CopyTo(origDtsCol);

                processBuilder.AppendLine("處理DealTimeSlot = " + DateTimeTicks(LastTimes, out LastTimes));
                //處理DealTimeSlot
                DealTimeSlotInsert(entity, cities, false, processBuilder);
                processBuilder.AppendLine("//處理DealTimeSlot = " + DateTimeTicks(LastTimes, out LastTimes));

                // preset reward to 500 pts for ppon & pponitem only

                entity.Store.SellerStatus = SellerFacade.SetRewardType(entity.Store.SellerStatus, RewardType.FiveHundredPoints);
                entity.Deal.BusinessHourDeliveryCharge = 0;

                //// first we check to see if number of records match
                bool syncLoc = entity.TimeSlotCollection == null || entity.TimeSlotCollection.Count != origDtsCol.Count;
                if (!syncLoc)
                {
                    origDtsCol = origDtsCol.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                    entity.TimeSlotCollection.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                    for (int i = 0; !syncLoc && i < entity.TimeSlotCollection.Count; i++)
                    {
                        syncLoc |= (entity.TimeSlotCollection[i].CityId != origDtsCol[i].CityId ||
                                    entity.TimeSlotCollection[i].EffectiveStart != origDtsCol[i].EffectiveStart ||
                                    entity.TimeSlotCollection[i].EffectiveEnd != origDtsCol[i].EffectiveEnd ||
                                    entity.TimeSlotCollection[i].Sequence != origDtsCol[i].Sequence ||
                                    entity.TimeSlotCollection[i].Status != origDtsCol[i].Status ||
                                    entity.TimeSlotCollection[i].IsInTurn != origDtsCol[i].IsInTurn ||
                                    entity.TimeSlotCollection[i].IsLockSeq != origDtsCol[i].IsLockSeq);
                    }
                }
                //processBuilder.AppendLine("syncLoc = " + DateTimeTicks(LastTimes, out LastTimes));
                success = pp.PponDealSet(entity, syncLoc, origDtsCol);
                //success = pp.PponDealSet(entity, syncLoc);
                //processBuilder.AppendLine("PponDealSet = " + DateTimeTicks(LastTimes, out LastTimes));

                processBuilder.AppendLine("GroupOrderGetList = " + DateTimeTicks(LastTimes, out LastTimes));
                GroupOrderCollection gOrderList = op.GroupOrderGetList(GroupOrder.Columns.BusinessHourGuid + "=" + entity.Deal.Guid);
                GroupOrder gOrder;

                Guid grpOrderGuid = OrderFacade.MakeGroupOrder(config.ServiceEmail, config.ServiceName,
                    entity.Deal.Guid, DateTime.Now, entity.Deal.BusinessHourOrderTimeE);
                gOrder = op.GroupOrderGet(grpOrderGuid);

                ////全家檔次
                //gOrder.Status = (int)Helper.SetFlag(View.FamiDeal, gOrder.Status ?? 0, GroupOrderStatus.FamiDeal);
                //gOrder.Status = (int)Helper.SetFlag(View.IsKindDeal, gOrder.Status ?? 0, GroupOrderStatus.KindDeal);
                //gOrder.Status = (int)Helper.SetFlag(View.QuantityToShow.Equals(((int)GroupOrderStatus.LowQuantityToShow).ToString()),
                //    gOrder.Status ?? 0, GroupOrderStatus.LowQuantityToShow);
                //gOrder.Status = (int)Helper.SetFlag(View.QuantityToShow.Equals(((int)GroupOrderStatus.HighQuantityNotToShow).ToString()),
                //    gOrder.Status ?? 0, GroupOrderStatus.HighQuantityNotToShow);
                //gOrder.Status = (int)Helper.SetFlag(true, gOrder.Status ?? 0, GroupOrderStatus.NoRefund);
                gOrder.Status = (int)Helper.SetFlag(true, gOrder.Status ?? 0, GroupOrderStatus.PEZeventCouponDownload);
                //新光檔次
                gOrder.Status = (int)Helper.SetFlag(true, gOrder.Status ?? 0, GroupOrderStatus.SKMDeal);
                op.GroupOrderSet(gOrder);
                processBuilder.AppendLine("//GroupOrderGetList = " + DateTimeTicks(LastTimes, out LastTimes));

                #region 進貨價處理
                processBuilder.AppendLine("進貨價處理 = " + DateTimeTicks(LastTimes, out LastTimes));
                var costCol1 = new DealCostCollection();
                var newFreight = new DealCost();
                newFreight.BusinessHourGuid = entity.Deal.Guid;
                //newFreight.Id = viewFreight.Id;
                newFreight.Cost = 0;
                newFreight.Quantity = 99999;
                newFreight.CumulativeQuantity = 99999;
                newFreight.LowerCumulativeQuantity = 0;
                if (pp.DealCostGet(newFreight.Id) == null || pp.DealCostGet(newFreight.Id).BusinessHourGuid != entity.Deal.Guid)
                {
                    costCol1.Add(newFreight);
                }


                #endregion 進貨價處理             
                //processBuilder.AppendLine("//Mongo 好康異動時須進行的處理  = " + DateTimeTicks(LastTimes, out LastTimes));

                if (success)
                {
                    //儲存PponStore
                    pp.PponStoreDeleteList(delStores);
                    pp.PponStoreSetList(insStores);
                    processBuilder.AppendLine("儲存PponStore = " + DateTimeTicks(LastTimes, out LastTimes));

                    pp.DealAccountingSet(dAccounting);
                    pp.DealPropertySet(dealProperty);
                    processBuilder.AppendLine("儲存業務資料 = " + DateTimeTicks(LastTimes, out LastTimes));

                    //寫入CategoryDeal
                    PponFacade.SaveCategoryDeals(cds, entity.Deal.Guid);
                    processBuilder.AppendLine("寫入CategoryDeal = " + DateTimeTicks(LastTimes, out LastTimes));

                    //儲存進貨價
                    pp.DealCostSetList(costCol1);
                    processBuilder.AppendLine("儲存進貨價 = " + DateTimeTicks(LastTimes, out LastTimes));

                    ViewPponDeal ppon = pp.ViewPponDealGetByBusinessHourGuid(dealProperty.BusinessHourGuid);
                    string s = ViewPponDealToJson(ppon);
                    pp.ChangeLogInsert("ViewPponDeal", dealProperty.BusinessHourGuid.ToString(), s);

                    PponStoreCollection pponStores = pp.PponStoreGetListByBusinessHourGuid(dealProperty.BusinessHourGuid);
                    foreach (var pponStore in pponStores)
                    {
                        pp.ChangeLogInsert("PponStore", dealProperty.BusinessHourGuid.ToString(), PponStoreToJson(pponStore));
                    }

                    pp.ChangeLogInsert("DealCategories", dealProperty.BusinessHourGuid.ToString(), (new JsonSerializer()).Serialize(selectedDealCategories));

                    processBuilder.AppendLine("pp.ChangeLogInsert = " + DateTimeTicks(LastTimes, out LastTimes));

                    //判斷是否全館
                    bids.Add(dealProperty.BusinessHourGuid,
                        deal.ParentSellerGuid == config.SkmRootSellerGuid ? deal.SellerGuid : deal.ParentSellerGuid);

                    resultMsg = "建檔成功";
                }
            }
            return resultMsg;
        }

        public static string OnSaveBusinessHourForSkmPay(ViewExternalDealCollection deals, List<ExternalDealCombo> comboDeals, string username, out Dictionary<Guid, Guid> bids)
        {
            bids = new Dictionary<Guid, Guid>();
            string resultMsg = string.Empty;
            username = config.SkmDealSales;
            ViewExternalDealCollection newdeals = new ViewExternalDealCollection();
            var isHqDeal = deals.First().IsHqDeal;
            HashSet<Guid> tempNewDeal = new HashSet<Guid>();

            foreach (ViewExternalDeal deal in deals.OrderByDescending(x => x.IsShoppe))
            {
                //判斷是否全館
                if (deal.ParentSellerGuid == config.SkmRootSellerGuid)
                {
                    if (tempNewDeal.Contains(deal.SellerGuid) == false && deal.Bid == null)
                    {
                        newdeals.Add(deal);
                        tempNewDeal.Add(deal.SellerGuid);
                    }
                }
                else
                {
                    if (tempNewDeal.Contains(deal.ParentSellerGuid) == false && deal.Bid == null)
                    {
                        newdeals.Add(deal);
                        tempNewDeal.Add(deal.ParentSellerGuid);
                    }
                }

                if (newdeals.Any() && isHqDeal)
                {
                    break;
                }
            }

            foreach (ViewExternalDeal deal in newdeals)
            {
                var skmShop = skmMp.SkmShoppeGetFirstByShopeCdoe(deal.ShopCode);
                var comboCol = comboDeals.Where(x => x.SellerGuid == skmShop.SellerGuid).ToList();
                KeyValuePair<Guid, Guid> mainBid;
                if (!CreateDeal(deals, deal, null, isHqDeal, username, out resultMsg, out mainBid))
                {
                    return resultMsg;
                }
                bids.Add(mainBid.Key, mainBid.Value);

                foreach (var combo in comboCol)
                {
                    KeyValuePair<Guid, Guid> bid;
                    if (!CreateDeal(deals, deal, combo, isHqDeal, username, out resultMsg, out bid))
                    {
                        return resultMsg;
                    }
                    combo.MainBid = mainBid.Key;
                    combo.Bid = bid.Key;
                }

                skmEp.SetExternalDealCombo(comboCol, true);

                #region 建立子母檔關係

                //連結子母檔 
                PponDeal mainDeal = pp.PponDealGet(mainBid.Key);
                foreach (var item in comboCol.Where(x => x.MainBid == mainBid.Key))
                {
                    PponFacade.AddComboDeal(mainDeal, new ComboItem
                    {
                        Bid = item.Bid.Value,
                        Title = item.ShopName
                    }, username);
                }

                #endregion 建立子母檔關係
            }
            return resultMsg;
        }

        /// <summary>
        /// 驗證檔次是否被隱藏
        /// </summary>
        /// <param name="dealGuid"></param>
        /// <returns></returns>
        public static bool CheckExternalDealIsHidden(Guid? dealGuid)
        {
            bool isHidden = false;

            if (config.IsCheckSkmDealTimeSlotStatusWhenMakeOrder)
            {
                if (dealGuid == null)
                {
                    return true;
                }
                SkmDealTimeSlot sdts = skmMp.SkmDealTimeSlotGetByBidAndDate((Guid)dealGuid, DateTime.Now.Date);
                if (sdts == null || sdts.Status == 1)
                {
                    isHidden = true;
                }
            }

            return isHidden;
        }

        private static bool CreateDeal(ViewExternalDealCollection deals, ViewExternalDeal deal, ExternalDealCombo combo, bool isHqDeal, string username, out string resultMsg, out KeyValuePair<Guid, Guid> bid)
        {
            resultMsg = string.Empty;
            bid = new KeyValuePair<Guid, Guid>();
            PponDeal data = ConvertPponDeal(deal, combo);
            PponDeal entity;
            bool isSkmPayVer = deal.DealVersion == (int)ExternalDealVersion.SkmPay;
            bool success;
            bool IsNew = false;
            var insStores = new PponStoreCollection();
            var delStores = new PponStoreCollection();
            DateTime LastTimes = DateTime.Now;
            StringBuilder processBuilder = new StringBuilder();
            processBuilder.AppendLine("OnSaveDeal(Begin) = " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.ffff") + "<br/>");

            #region add mode
            processBuilder.AppendLine("add mode = " + DateTimeTicks(LastTimes, out LastTimes));
            IsNew = true;

            var sellerGuid = isHqDeal ? config.SkmRootSellerGuid : combo == null ? deal.ParentSellerGuid == config.SkmRootSellerGuid ? deal.SellerGuid : deal.ParentSellerGuid : combo.ShopGuid;
            entity = data;
            // now let's connect the dots
            entity.Store = sp.SellerGet(isHqDeal ? config.SkmRootSellerGuid : deal.ParentSellerGuid);
            entity.Deal.BusinessHourTypeId = (int)BusinessHourType.Ppon;
            entity.Deal.SellerGuid = sellerGuid;
            entity.Deal.Guid = Helper.GetNewGuid(entity);
            entity.DealContent.BusinessHourGuid = entity.Deal.Guid;

            entity.ItemDetail.BusinessHourGuid = entity.Deal.Guid;
            entity.ItemDetail.MenuGuid = Guid.Empty;
            entity.ItemDetail.Guid = Helper.GetNewGuid(entity);
            entity.Deal.CreateTime = entity.ItemDetail.CreateTime = DateTime.Now;
            entity.Deal.CreateId = entity.ItemDetail.CreateId = username;
            entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(true, entity.Deal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice);
            PromotionFacade.DiscountLimitSet(entity.Deal.Guid, username, DiscountLimitType.Enabled);

            entity.DealContent.AppDealPic = deal.AppDealPic;
            entity.DealContent.ImagePath = deal.ImagePath;

            #region PponStore
            processBuilder.AppendLine("PponStore = " + DateTimeTicks(LastTimes, out LastTimes));

            List<Guid> stores = new List<Guid>();

            //是否為非全館
            if (isHqDeal)
            {
                stores = deals.ToList().GroupBy(x => x.StoreGuid).Select(x => x.Key).ToList();
            }
            else
            {
                if (combo == null)
                {
                    if (deal.ParentSellerGuid == config.SkmRootSellerGuid)
                    {
                        stores.AddRange(deals
                            .Where(x => x.ParentSellerGuid == config.SkmRootSellerGuid && x.Bid == null &&
                                        x.SellerGuid == deal.SellerGuid).Select(x => x.StoreGuid).ToList());
                        //增加判斷同一分店是否有其他館櫃
                        stores.AddRange(deals.Where(x => x.SellerGuid == deal.ParentSellerGuid && x.ParentSellerGuid == config.SkmRootSellerGuid).Select(x => x.StoreGuid).ToList());
                    }
                    else
                    {
                        stores.AddRange(deals.Where(x => x.ParentSellerGuid == deal.ParentSellerGuid && x.Bid == null).Select(x => x.StoreGuid).ToList());
                        stores.AddRange(deals
                            .Where(x => x.ParentSellerGuid == config.SkmRootSellerGuid && x.Bid == null &&
                                        x.SellerGuid == deal.SellerGuid).Select(x => x.StoreGuid).ToList());
                        //增加判斷同一分店是否有其他館櫃
                        stores.AddRange(deals.Where(x => x.SellerGuid == deal.ParentSellerGuid && x.ParentSellerGuid == config.SkmRootSellerGuid).Select(x => x.StoreGuid).ToList());
                    }
                }
                else
                {
                    var isShoppe = !deals.Any(x =>
                        x.ParentSellerGuid == config.SkmRootSellerGuid && x.StoreGuid == combo.ShopGuid);

                    stores = isShoppe ? deals.Where(x => x.ShopCode == combo.ShopCode && x.Bid == null).Select(x => x.StoreGuid).ToList()
                        : deals.Where(x => x.ParentSellerGuid == config.SkmRootSellerGuid && x.Bid == null && x.StoreGuid == combo.ShopGuid).Select(x => x.StoreGuid).ToList();
                }
            }

            var storeSort = 1;
            foreach (var store in stores)
            {
                var pponStore = new PponStore();
                pponStore.BusinessHourGuid = entity.Deal.Guid;
                pponStore.StoreGuid = store;
                pponStore.ResourceGuid = Guid.NewGuid();
                pponStore.CreateId = username;
                pponStore.CreateTime = DateTime.Now;
                pponStore.SortOrder = storeSort;
                pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Location));
                pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Verify));
                pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Accouting));
                pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.ViewBalanceSheet));
                pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.VerifyShop));
                storeSort += 1;
                insStores.Add(pponStore);
            }

            #endregion PponStore

            #endregion add mode

            if (PponFacade.CheckPponIntroductionValid(data) == false)
            {
                resultMsg = "「權益說明」「更多權益」有無法解析的Html語法，請重新填寫或洽IT處理。";
                return false;
            }

            processBuilder.AppendLine("pp.ViewPponDealGetByBusinessHourGuid = " + DateTimeTicks(LastTimes, out LastTimes));

            #region 好康哪裡找
            processBuilder.AppendLine("好康哪裡找 = " + DateTimeTicks(LastTimes, out LastTimes));
            //如果有輸入分店資料，以分店資料產生好康哪裡找的紀錄
            var availables = new List<AvailableInformation>();
            foreach (var st in insStores.OrderBy(x => x.SortOrder))
            {
                var store = sp.SellerGet(st.StoreGuid);
                var viewstorecategoryCol = sp.ViewStoreCategoryCollectionGetByStore(st.StoreGuid);
                var mrtInfo = string.Empty;
                if (viewstorecategoryCol.Any())
                {
                    mrtInfo = string.Join(",", viewstorecategoryCol.Select(x => x.Name).ToArray());
                }

                SqlGeography geo = LocationFacade.GetGeographyByCoordinate(store.Coordinate);
                var tmp = new AvailableInformation()
                {
                    N = GetSkmShoppeDisplayName(st.StoreGuid),//store.SellerName,
                    P = store.StoreTel,
                    A = string.Empty,
                    Longitude = geo != null ? geo.Long.ToString() : string.Empty,
                    Latitude = geo != null ? geo.Lat.ToString() : string.Empty,
                    OT = store.OpenTime,
                    UT = st.UseTime,
                    U = store.WebUrl,
                    R = store.StoreRemark,
                    CD = store.CloseDate,
                    MR = string.Empty,
                    CA = store.Car,
                    BU = store.Bus,
                    OV = store.OtherVehicles,
                    FB = store.FacebookUrl,
                    PL = string.Empty,
                    BL = store.BlogUrl,
                    OL = store.OtherUrl
                };
                availables.Add(tmp);
            }

            if (availables.Count >= 0)
            {
                entity.DealContent.Availability = new JsonSerializer().Serialize(availables);
            }

            #endregion 好康哪裡找

            #region DealProperty
            processBuilder.AppendLine("DealProperty = " + DateTimeTicks(LastTimes, out LastTimes));
            DealProperty dealProperty = pp.DealPropertyGet(entity.Deal.Guid);

            dealProperty.BusinessHourGuid = entity.Deal.Guid;
            dealProperty.CreateId = username;
            dealProperty.CreateTime = DateTime.Now;

            var empInfo = hp.EmployeeGet(Employee.Columns.UserId, mp.MemberGet(username).UniqueId);
            dealProperty.DealEmpName = empInfo.EmpName;

            //用業務名稱抓取對應Employee EmpNo 寫進dealProperty這個資料表 若對應不到 則無需填寫
            if (empInfo.IsLoaded && empInfo != null && (!IsNew | (!empInfo.IsInvisible && empInfo.DepartureDate == null)))
            {
                dealProperty.EmpNo = empInfo.EmpNo;
                dealProperty.DevelopeSalesId = empInfo.UserId;
            }
            else
            {
                resultMsg = "請填入有效業務";
                return false;
            }

            dealProperty.DeliveryType = (int)DeliveryType.ToShop;
            dealProperty.ShoppingCart = false;
            dealProperty.MultipleBranch = false;
            dealProperty.ComboPackCount = 1;
            dealProperty.PresentQuantity = 0;
            dealProperty.SaleMultipleBase = 0;
            dealProperty.GroupCouponAppStyle = 0;
            dealProperty.IsQuantityMultiplier = false;
            dealProperty.IsAveragePrice = false;
            dealProperty.ActivityUrl = string.Empty;
            if (deal.IsPrizeDeal ||
               (deal.DealVersion == (int)ExternalDealVersion.SkmPay && deal.RepeatPurchaseType == (int)SkmDealRepeatPurchaseType.Daily))
            {
                dealProperty.IsDailyRestriction = true;
            }
            else
            {
                dealProperty.IsDailyRestriction = false;
            }
            //dealProperty.IsDailyRestriction = deal.IsPrizeDeal ? true : deal.DealVersion == (int)ExternalDealVersion.SkmPay ? deal.RepeatPurchaseType == (int)SkmDealRepeatPurchaseType.Daily : false;

            //設定BarcodeType
            dealProperty.CouponCodeType = 0;
            dealProperty.PinType = 0;

            dealProperty.ExchangePrice = deal.Discount;
            //for skm
            dealProperty.DiscountType = deal.DiscountType;
            dealProperty.Discount = deal.Discount;
            dealProperty.IsExperience = deal.ActiveType == (int)ActiveType.Experience;

            dealProperty.BookingSystemType = 0;
            dealProperty.AdvanceReservationDays = 0;
            dealProperty.CouponUsers = 0;
            dealProperty.IsReserveLock = false;

            dealProperty.TmallRmbPrice = 0;
            dealProperty.TmallRmbExchangeRate = 0;

            dealProperty.IsMergeCount = false;
            dealProperty.IsZeroActivityShowCoupon = false;
            dealProperty.IsLongContract = false;
            //設定台新分期
            dealProperty.Installment3months = false;
            dealProperty.Installment6months = false;
            dealProperty.Installment12months = false;
            dealProperty.DenyInstallment = false;

            dealProperty.ShipType = entity.Property.ShipType;
            dealProperty.ShippingdateType = entity.Property.ShippingdateType;
            dealProperty.Shippingdate = entity.Property.Shippingdate;
            dealProperty.ProductUseDateStartSet = entity.Property.ProductUseDateStartSet;
            dealProperty.ProductUseDateEndSet = entity.Property.ProductUseDateEndSet;
            dealProperty.LabelIconList = ((int)DealLabelSystemCode.AppLimitedEdition).ToString();
            #endregion DealProperty
            var dAccounting = new DealAccounting();
            dAccounting.BusinessHourGuid = entity.Deal.Guid;
            dAccounting.SalesCommission = 0.2;
            dAccounting.SalesBonus = 0;
            dAccounting.Status = (int)AccountsPayableFormula.Default;
            dAccounting.SalesId = empInfo.EmpName;
            dAccounting.Commission = 0;
            dAccounting.RemittanceType = 0;
            dAccounting.VendorBillingModel = 0;
            dAccounting.VendorReceiptType = 0;
            dAccounting.IsInputTaxRequired = false;

            #region Save Category Deal data
            processBuilder.AppendLine("Save Category Deal data = " + DateTimeTicks(LastTimes, out LastTimes));
            CategoryDealCollection cds = new CategoryDealCollection();

            #region 新版的頻道、區域、分類之處理
            //todo

            //行政區分店自動檢查category
            List<ViewCategoryDependency> vcdList = sp.ViewCategoryDependencyGetByParentType((int)CategoryType.PponChannelArea).ToList();
            foreach (PponStore pponStore in insStores)
            {
                Store store = sp.StoreGet(pponStore.StoreGuid);

                if (store.IsLoaded && store.CityId.HasValue && store.TownshipId.HasValue)
                {
                    var township = CityManager.TownShipGetById(store.TownshipId.Value);
                    var parentcity = CityManager.CityGetById(store.CityId.Value);
                    if (township != null && parentcity != null)
                    {
                        ViewCategoryDependency vcd = vcdList.Where(x => x.ParentName == parentcity.CityName && x.CategoryName == township.CityName).DefaultIfEmpty(null).First();
                        if (vcd != null && !cds.Any(x => x.Bid == entity.Deal.Guid && x.Cid == vcd.ParentId))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.ParentId });
                        }
                        if (vcd != null && !cds.Any(x => x.Bid == entity.Deal.Guid && x.Cid == vcd.CategoryId))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.CategoryId });
                        }
                    }

                }
            }

            ///first create store_category data to store_guid bind category_code
            ///and uses stores.store_guid to find categoryid 
            ///and set the final node and find parent node to set in type 5 4
            ViewStoreCategoryCollection vscc = sp.ViewStoreCategoryCollectionGetByStoreList(stores);
            //type5
            if (vscc.Count > 0)
            {
                List<int> nodeList = new List<int>();
                foreach (ViewStoreCategory vsc in vscc)
                {
                    if (!cds.Any(x => x.Cid == vsc.CategoryId.Value))
                    {
                        cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vsc.CategoryId.Value });
                    }
                    if (!cds.Any(x => x.Cid == vsc.ParentId))
                    {
                        cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vsc.ParentId });
                        if (!nodeList.Contains(vsc.ParentId))
                        {
                            nodeList.Add(vsc.ParentId);
                        }
                    }
                }
                ViewCategoryDependencyCollection vcdc = sp.ViewCategoryDependencyGetByCategoryIdList(nodeList);
                foreach (ViewCategoryDependency vcd in vcdc)
                {
                    if (!cds.Any(x => x.Cid == vcd.ParentId))
                    {
                        cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.ParentId });
                    }
                }
            }

            //type6
            List<int> selectedDealCategories = deal.CategoryList != null ? deal.CategoryList.Trim('[', ']').Split(",").Select(int.Parse).ToList() : new List<int>();
            selectedDealCategories.Add(config.SkmCategordId);
            foreach (var dealCategoryId in selectedDealCategories)
            {
                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = dealCategoryId });
            }
            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = CategoryManager.Default.AppLimitedEdition.CategoryId });

            #endregion 新版的頻道、區域、分類之處理

            processBuilder.AppendLine("//Save Category Deal data = " + DateTimeTicks(LastTimes, out LastTimes));
            #endregion Save Category Deal data

            //將選取的城市紀錄於dealProperty
            dealProperty.CityList = new JsonSerializer().Serialize(new int[] { config.SkmCityId });
            dealProperty.IsTravelDeal = false;

            //使用者選取的城市， 新版分類上線後，改由 新分類設定的頻道對應產生城市編號
            int[] cities = new int[] { config.SkmCityId };

            //取得原本設定的Dts
            DealTimeSlotCollection origDtsCol = new DealTimeSlotCollection();
            entity.TimeSlotCollection.CopyTo(origDtsCol);

            processBuilder.AppendLine("處理DealTimeSlot = " + DateTimeTicks(LastTimes, out LastTimes));
            //處理DealTimeSlot
            DealTimeSlotInsert(entity, cities, false, processBuilder);
            processBuilder.AppendLine("//處理DealTimeSlot = " + DateTimeTicks(LastTimes, out LastTimes));

            // preset reward to 500 pts for ppon & pponitem only

            entity.Store.SellerStatus = SellerFacade.SetRewardType(entity.Store.SellerStatus, RewardType.FiveHundredPoints);
            entity.Deal.BusinessHourDeliveryCharge = 0;

            //// first we check to see if number of records match
            bool syncLoc = entity.TimeSlotCollection == null || entity.TimeSlotCollection.Count != origDtsCol.Count;
            if (!syncLoc)
            {
                origDtsCol = origDtsCol.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                entity.TimeSlotCollection.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                for (int i = 0; !syncLoc && i < entity.TimeSlotCollection.Count; i++)
                {
                    syncLoc |= (entity.TimeSlotCollection[i].CityId != origDtsCol[i].CityId ||
                                entity.TimeSlotCollection[i].EffectiveStart != origDtsCol[i].EffectiveStart ||
                                entity.TimeSlotCollection[i].EffectiveEnd != origDtsCol[i].EffectiveEnd ||
                                entity.TimeSlotCollection[i].Sequence != origDtsCol[i].Sequence ||
                                entity.TimeSlotCollection[i].Status != origDtsCol[i].Status ||
                                entity.TimeSlotCollection[i].IsInTurn != origDtsCol[i].IsInTurn ||
                                entity.TimeSlotCollection[i].IsLockSeq != origDtsCol[i].IsLockSeq);
                }
            }

            success = pp.PponDealSet(entity, syncLoc, origDtsCol);
            processBuilder.AppendLine("GroupOrderGetList = " + DateTimeTicks(LastTimes, out LastTimes));
            GroupOrderCollection gOrderList = op.GroupOrderGetList(GroupOrder.Columns.BusinessHourGuid + "=" + entity.Deal.Guid);
            GroupOrder gOrder;

            Guid grpOrderGuid = OrderFacade.MakeGroupOrder(config.ServiceEmail, config.ServiceName,
                entity.Deal.Guid, DateTime.Now, entity.Deal.BusinessHourOrderTimeE);
            gOrder = op.GroupOrderGet(grpOrderGuid);
            gOrder.Status = (int)Helper.SetFlag(true, gOrder.Status ?? 0, GroupOrderStatus.PEZeventCouponDownload);
            gOrder.Status = (int)Helper.SetFlag(true, gOrder.Status ?? 0, GroupOrderStatus.SKMDeal);
            op.GroupOrderSet(gOrder);
            processBuilder.AppendLine("//GroupOrderGetList = " + DateTimeTicks(LastTimes, out LastTimes));

            #region 進貨價處理
            processBuilder.AppendLine("進貨價處理 = " + DateTimeTicks(LastTimes, out LastTimes));
            var costCol1 = new DealCostCollection();
            var newFreight = new DealCost();
            newFreight.BusinessHourGuid = entity.Deal.Guid;
            //newFreight.Id = viewFreight.Id;
            newFreight.Cost = 0;
            newFreight.Quantity = 99999;
            newFreight.CumulativeQuantity = 99999;
            newFreight.LowerCumulativeQuantity = 0;
            if (pp.DealCostGet(newFreight.Id) == null || pp.DealCostGet(newFreight.Id).BusinessHourGuid != entity.Deal.Guid)
            {
                costCol1.Add(newFreight);
            }

            #endregion 進貨價處理

            if (success)
            {
                //儲存PponStore
                pp.PponStoreDeleteList(delStores);
                pp.PponStoreSetList(insStores);
                processBuilder.AppendLine("儲存PponStore = " + DateTimeTicks(LastTimes, out LastTimes));

                pp.DealAccountingSet(dAccounting);
                pp.DealPropertySet(dealProperty);
                processBuilder.AppendLine("儲存業務資料 = " + DateTimeTicks(LastTimes, out LastTimes));

                //寫入CategoryDeal
                PponFacade.SaveCategoryDeals(cds, entity.Deal.Guid);
                processBuilder.AppendLine("寫入CategoryDeal = " + DateTimeTicks(LastTimes, out LastTimes));

                //儲存進貨價
                pp.DealCostSetList(costCol1);
                processBuilder.AppendLine("儲存進貨價 = " + DateTimeTicks(LastTimes, out LastTimes));

                ViewPponDeal ppon = pp.ViewPponDealGetByBusinessHourGuid(dealProperty.BusinessHourGuid);
                string s = ViewPponDealToJson(ppon);
                pp.ChangeLogInsert("ViewPponDeal", dealProperty.BusinessHourGuid.ToString(), s);

                PponStoreCollection pponStores = pp.PponStoreGetListByBusinessHourGuid(dealProperty.BusinessHourGuid);
                foreach (var pponStore in pponStores)
                {
                    pp.ChangeLogInsert("PponStore", dealProperty.BusinessHourGuid.ToString(), PponStoreToJson(pponStore));
                }

                pp.ChangeLogInsert("DealCategories", dealProperty.BusinessHourGuid.ToString(), (new JsonSerializer()).Serialize(selectedDealCategories));

                processBuilder.AppendLine("pp.ChangeLogInsert = " + DateTimeTicks(LastTimes, out LastTimes));

                //判斷是否全館
                bid = new KeyValuePair<Guid, Guid>(dealProperty.BusinessHourGuid,
                    deal.ParentSellerGuid == config.SkmRootSellerGuid ? deal.SellerGuid : deal.ParentSellerGuid);
                resultMsg = "建檔成功";
            }
            return true;
        }

        private static string PponStoreToJson(PponStore pponStore)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("StoreGuid");
                writer.WriteValue(pponStore.StoreGuid);
                writer.WritePropertyName("TotalQuantity");
                writer.WriteValue(pponStore.TotalQuantity);
                writer.WritePropertyName("SortOrder");
                writer.WriteValue(pponStore.SortOrder);
                writer.WritePropertyName("ChangedExpireDate");
                writer.WriteValue(pponStore.ChangedExpireDate);

                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        private static string ViewPponDealToJson(ViewPponDeal ppon)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("Availability");
                writer.WriteValue(ppon.Availability);
                writer.WritePropertyName("BusinessHourAtmMaximum");
                writer.WriteValue(ppon.BusinessHourAtmMaximum);
                writer.WritePropertyName("BusinessHourDeliverTimeE");
                writer.WriteValue(ppon.BusinessHourDeliverTimeE == null ? "n/a" : ppon.BusinessHourDeliverTimeE.Value.ToString("o"));
                writer.WritePropertyName("BusinessHourDeliverTimeS");
                writer.WriteValue(ppon.BusinessHourDeliverTimeS == null ? "n/a" : ppon.BusinessHourDeliverTimeS.Value.ToString("o"));
                writer.WritePropertyName("BusinessHourDeliveryCharge");
                writer.WriteValue(ppon.BusinessHourDeliveryCharge);
                writer.WritePropertyName("BusinessHourGuid");
                writer.WriteValue(ppon.BusinessHourGuid.ToString());
                writer.WritePropertyName("BusinessHourOrderMinimum");
                writer.WriteValue(ppon.BusinessHourOrderMinimum);
                writer.WritePropertyName("BusinessHourOrderTimeE");
                writer.WriteValue(ppon.BusinessHourOrderTimeE.ToString("o"));
                writer.WritePropertyName("BusinessHourOrderTimeS");
                writer.WriteValue(ppon.BusinessHourOrderTimeS.ToString("o"));
                writer.WritePropertyName("BusinessHourStatus");
                writer.WriteValue(ppon.BusinessHourStatus);
                writer.WritePropertyName("ChangedExpireDate");
                writer.WriteValue(ppon.ChangedExpireDate.HasValue ? ppon.ChangedExpireDate.Value.ToString("yyyy/MM/dd") : "");
                writer.WritePropertyName("ComboPackCount");
                writer.WriteValue(ppon.ComboPackCount);
                writer.WritePropertyName("CouponUsage");
                writer.WriteValue(ppon.CouponUsage);
                writer.WritePropertyName("CreateTime");
                writer.WriteValue(ppon.CreateTime);
                writer.WritePropertyName("DeliveryType");
                writer.WriteValue(ppon.DeliveryType);
                writer.WritePropertyName("Department");
                writer.WriteValue(ppon.Department);
                writer.WritePropertyName("Description");
                string description = ViewPponDealManager.DefaultManager.GetDealDescription(ppon);
                writer.WriteValue(description);
                writer.WritePropertyName("EventName");
                writer.WriteValue(ppon.EventName);
                writer.WritePropertyName("EventTitle");
                writer.WriteValue(ppon.EventTitle);
                writer.WritePropertyName("GroupOrderGuid");
                writer.WriteValue(ppon.GroupOrderGuid.ToString());
                writer.WritePropertyName("GroupOrderStatus");
                writer.WriteValue(ppon.GroupOrderStatus);
                writer.WritePropertyName("Introduction");
                writer.WriteValue(ppon.Introduction);
                writer.WritePropertyName("ItemDefaultDailyAmount");
                writer.WriteValue(ppon.ItemDefaultDailyAmount);
                writer.WritePropertyName("ItemGuid");
                writer.WriteValue(ppon.ItemGuid.ToString());
                writer.WritePropertyName("ItemName");
                writer.WriteValue(ppon.ItemName);
                writer.WritePropertyName("ItemOrigPrice");
                writer.WriteValue(ppon.ItemOrigPrice);
                writer.WritePropertyName("ItemPrice");
                writer.WriteValue(ppon.ItemPrice);
                writer.WritePropertyName("SlottingFeeQuantity");
                writer.WriteValue(0);
                writer.WritePropertyName("PurchasePrice");
                writer.WriteValue(0);
                writer.WritePropertyName("MaxItemCount");
                writer.WriteValue(ppon.MaxItemCount);
                writer.WritePropertyName("OrderGuid");
                writer.WriteValue(ppon.OrderGuid.ToString());
                writer.WritePropertyName("OrderTotalLimit");
                writer.WriteValue(ppon.OrderTotalLimit);
                writer.WritePropertyName("OrderedQuantity");
                writer.WriteValue(ppon.OrderedQuantity);
                writer.WritePropertyName("OrderedTotal");
                writer.WriteValue(ppon.OrderedTotal);
                writer.WritePropertyName("Reasons");
                writer.WriteValue(ppon.Reasons);
                writer.WritePropertyName("ReferenceText");
                writer.WriteValue(ppon.ReferenceText);
                writer.WritePropertyName("Remark");
                writer.WriteValue(ppon.Remark);
                writer.WritePropertyName("SellerAddress");
                writer.WriteValue(ppon.SellerAddress);
                writer.WritePropertyName("SellerCityId");
                writer.WriteValue(ppon.SellerCityId);
                writer.WritePropertyName("SellerGuid");
                writer.WriteValue(ppon.SellerGuid.ToString());
                writer.WritePropertyName("SellerId");
                writer.WriteValue(ppon.SellerId);
                writer.WritePropertyName("SellerName");
                writer.WriteValue(ppon.SellerName);
                writer.WritePropertyName("SellerTel");
                writer.WriteValue(ppon.SellerTel);
                writer.WritePropertyName("ShoppingCart");
                writer.WriteValue(ppon.ShoppingCart);
                writer.WritePropertyName("Slug");
                writer.WriteValue(ppon.Slug);
                writer.WritePropertyName("SubjectName");
                writer.WriteValue(ppon.SubjectName);
                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        /// <summary>
        /// DealTimeSlot重構的方法
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="selectedCity"></param>
        /// <param name="orderStartDateChange"></param>
        private static void DealTimeSlotInsert(PponDeal entity, int[] cities, bool orderStartDateChange, StringBuilder processBuilder)
        {
            //**********寫入DealTimeSlot的邏輯 Begin**********
            #region 產生DealTimeSlot排程資料, it has a bug on removing items

            DateTime dateStartDay = entity.Deal.BusinessHourOrderTimeS;
            DateTime dateEndDay = entity.Deal.BusinessHourOrderTimeE;
            DealTimeSlotCollection dealTimeSlotColForInert = new DealTimeSlotCollection();

            //檢查目前是否已有排程資料，若沒有依據輸入的啟始與截止日期自動產生
            if (entity.TimeSlotCollection == null || entity.TimeSlotCollection.Count == 0)
            {
                #region 依據輸入的啟始與截止日期自動產生
                //依據On檔的時間與地區產生DealTimeSlot的資料(檔期排程資料)
                //依序新增勾選區域的排程記錄
                foreach (int cityId in cities)
                {
                    AddDealTimeSlotLoop(entity, dealTimeSlotColForInert, dateStartDay, dateEndDay, cityId);
                }

                if (dealTimeSlotColForInert.Count > 0)
                {
                    entity.TimeSlotCollection = dealTimeSlotColForInert;
                }
                #endregion 依據輸入的啟始與截止日期自動產生
            }
            else //若已有排程資料則需判斷是否需要修改
            {
                #region 若已有排程資料則需判斷是否需要修改

                //todo:改寫這塊邏輯，應該是不需要檢查，只要取得舊有的seq。反正最後都是全部砍掉重練。

                #region 逐筆檢查舊資料（邏輯需要再檢查）
                //CheckTimeSlotLoop(entity, dealTimeSlotColForInert, cities, dateStartDay, dateEndDay);
                #endregion 逐筆檢查舊資料

                //entity.TimeSlotCollection.AddRange(dealTimeSlotColForInert);

                //填入新增的資料

                #region 將不足的資料補上
                //將不足的資料補上
                //一開始排程時間設為DEAL起始日
                //依序新增勾選區域的排程記錄
                foreach (int cityId in cities)
                {
                    //只要排程時間小於DEAL結束日就需增加一筆排程記錄
                    AddDealTimeSlotLoop(entity, dealTimeSlotColForInert, dateStartDay, dateEndDay, cityId, true);
                }
                #endregion 將不足的資料補上

                entity.TimeSlotCollection = dealTimeSlotColForInert;


                #endregion
            }

            if ((entity.TimeSlotCollection != null) && (orderStartDateChange))
            {
                // 預設前24小時輪播  add by Max 2011/4/21
                foreach (DealTimeSlot dts in entity.TimeSlotCollection.Where(x => DateTime.Compare(entity.Deal.BusinessHourOrderTimeS.AddDays(1), x.EffectiveStart) > 0))
                {
                    dts.IsInTurn = true;
                }
            }
            //天貓檔次設定不顯示
            if ((entity.Deal.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
            {
                foreach (var item in entity.TimeSlotCollection)
                {
                    item.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                }
                // _pponProv.DealTimeSlotUpdateStatus(entity.Deal.Guid, DealTimeSlotStatus.NotShowInPponDefault);
            }


            //=====================================================

            #endregion 產生DealTimeSlot排程資料, it has a bug on removing items
            //**********寫入DealTimeSlot的邏輯 End**********
        }

        private static Tuple<DateTime, DealTimeSlot> AdjustFirstEffectiveStartDay(DateTime effectiveStartDay, Guid dealGuid, int cityId)
        {
            DateTime TempEndDay = DateTime.MinValue;
            DealTimeSlot insDeal = null;

            if (effectiveStartDay.ToString("HH:mm") != "12:00")
            {
                if (effectiveStartDay.Hour < 12) //未過中午12點
                {
                    TempEndDay = effectiveStartDay.Noon();

                }
                else //已過中午12點
                {
                    TempEndDay = effectiveStartDay.Noon().AddDays(1);
                }
            }
            //int newSeq = PponFacade.GetDealTimeSlotNonLockSeq(cityId, effectiveStartDay, vpdts);
            insDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                         999999, //★
                                          effectiveStartDay, TempEndDay,
                                          DealTimeSlotStatus.Default);

            return Tuple.Create(TempEndDay, insDeal);
        }

        /// <summary>
        /// 迴圈新增DealTimeSlot
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dealTimeSlotColForInert"></param>
        /// <param name="dateStartDay"></param>
        /// <param name="dateEndDate"></param>
        /// <param name="cityId"></param>
        /// <param name="calNewSeq"></param>
        private static void AddDealTimeSlotLoop(PponDeal entity, DealTimeSlotCollection dealTimeSlotColForInert, DateTime dateStartDay, DateTime dateEndDate, int cityId, bool calNewSeq = false)
        {
            Guid dealGuid = entity.Deal.Guid;
            //檢查第一筆
            bool isFirst = true;
            DateTime LastTimes = DateTime.Now;

            //EffectiveStart是Key值
            for (var date = dateStartDay; date < dateEndDate; date = date.AddDays(1))
            {
                //檢查資料是否已經存在
                bool anyExist = entity.TimeSlotCollection == null ? false : entity.TimeSlotCollection.Any(t => t.EffectiveStart == date && t.CityId == cityId);
                //取得該筆舊資料
                DealTimeSlot dts = !anyExist ? null : entity.TimeSlotCollection.Where(t => t.EffectiveStart == date && t.CityId == cityId).FirstOrDefault();

                DealTimeSlot insDeal = null;

                #region 第一筆

                if (isFirst) //針對第一筆的時間做調整
                {
                    //一開始排程時間設為DEAL起始日
                    Tuple<DateTime, DealTimeSlot> tuple = AdjustFirstEffectiveStartDay(date, dealGuid, cityId);
                    if (tuple.Item1 != DateTime.MinValue) //調整DEAL起始時間
                    {
                        if (!anyExist)
                        {
                            dealTimeSlotColForInert.Add(tuple.Item2);
                        }
                        else
                        {
                            //檔次已存在，維持舊的排序
                            dealTimeSlotColForInert.Add(GetNewDealTimeSlot(dts));
                        }

                        //用調整後的時間做新的起始點
                        date = tuple.Item1;

                        //檢查調整後的時間資料是否已經存在
                        anyExist = entity.TimeSlotCollection == null ? false : entity.TimeSlotCollection.Any(t => t.EffectiveStart == date && t.CityId == cityId);//★這兩行是多餘的?
                        dts = !anyExist ? null : entity.TimeSlotCollection.Where(t => t.EffectiveStart == date && t.CityId == cityId).FirstOrDefault();//★這兩行是多餘的?
                    }

                    //一定要把isFirst關掉
                    isFirst = false;
                }

                #endregion

                #region 最後一筆

                if (date.AddDays(1) > dateEndDate)
                {
                    int lastSeq = 999999;

                    if (!anyExist)
                    {
                    }
                    else
                    {
                        //檔次已存在，維持舊的排序
                        lastSeq = dts.Sequence;
                    }
                    DealTimeSlot lastDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                                      lastSeq,
                                                      date, dateEndDate,
                                                      DealTimeSlotStatus.Default);

                    dealTimeSlotColForInert.Add(lastDeal);
                    continue;
                }

                #endregion

                #region  其他筆

                if (!anyExist)
                {
                    //檔次不存在
                    int newSeq = 999999;

                    //新增 此城市這個時間一筆排程記錄
                    insDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                                        newSeq,
                                                        date, date.AddDays(1),
                                                        DealTimeSlotStatus.Default);
                }
                else
                {
                    //檔次已存在，維持舊的排序
                    insDeal = GetNewDealTimeSlot(dts);
                }

                #endregion

                if (!dealTimeSlotColForInert.Any(t => t.EffectiveStart == date && t.CityId == cityId))
                {
                    dealTimeSlotColForInert.Add(insDeal);
                }


                //      在2013/12/10現改成當新的slot增加時，順序放在最後面時，就不用+1
                //_pponProv.DealTimeSlotUpdateSeqAddOne(cityId, effectiveStartDay);
                //////////////////////////////////////////////
            }
        }

        /// <summary>
        /// 傳回新建的DealTimeSlot物件，DataTable欄位的值與傳入的DealTimeSlot相同
        /// </summary>
        /// <param name="fromData"></param>
        /// <returns></returns>
        private static DealTimeSlot GetNewDealTimeSlot(DealTimeSlot fromData)
        {
            //新增 此城市這個時間一筆排程記錄
            DealTimeSlot newDeal = new DealTimeSlot();
            newDeal.BusinessHourGuid = fromData.BusinessHourGuid;
            newDeal.CityId = fromData.CityId;
            newDeal.Sequence = fromData.Sequence;
            newDeal.EffectiveStart = fromData.EffectiveStart;

            //fix 手動結檔再開檔，fromData.EffectiveEnd.Noon()同日問題
            if (newDeal.EffectiveStart.Date.Equals(fromData.EffectiveEnd.Date) && newDeal.EffectiveStart >= fromData.EffectiveEnd.Noon())
            {
                newDeal.EffectiveEnd = fromData.EffectiveEnd.AddDays(1).Noon();
            }
            else
            {
                newDeal.EffectiveEnd = fromData.EffectiveEnd.Noon();
            }
            //日期銜接為12點，所以直接設定12點

            newDeal.Status = fromData.Status;
            newDeal.IsInTurn = fromData.IsInTurn;
            newDeal.IsLockSeq = fromData.IsLockSeq; //★本次新增

            return newDeal;
        }

        private static DealTimeSlot GetNewDealTimeSlot(Guid bid, int cityId, int seq, DateTime startDate, DateTime endDate, DealTimeSlotStatus status, bool isInTurn = false)
        {
            //新增 此城市這個時間一筆排程記錄
            DealTimeSlot newDeal = new DealTimeSlot();
            newDeal.BusinessHourGuid = bid;
            newDeal.CityId = cityId;
            newDeal.Sequence = seq;
            newDeal.EffectiveStart = startDate;
            newDeal.EffectiveEnd = endDate;
            newDeal.Status = (int)status;
            newDeal.IsInTurn = isInTurn;

            return newDeal;
        }
        #endregion

        #region SkmAppStyle

        public static SkmAppStyleCollection SkmAppStyleGetBySellerGuid(Guid SellerGuid)
        {
            return skmMp.SkmAppStyleGetBySeller(SellerGuid);
        }
        public static string SkmAppStyleImgPath(string fileName, bool plusGuid = true)
        {
            string returnValue = fileName == "" ? "" : config.MediaBaseUrl + "event/" + fileName;
            if (plusGuid) returnValue += "?g=" + Guid.NewGuid();
            return returnValue;
        }

        #endregion

        #region APP 首頁相關

        public static List<PrizeDrawEventModel> SkmPrizeEventGet()
        {
            return cep.GetPrizeDrawEventList(SkmPrizeDrawEventOrderField.App);
        }

        public static SkmLatestActivityCollection SkmLatestActivityGetBySellerGuid(Guid SellerGuid)
        {
            return skmMp.SkmLatestActivityGetBySid(SellerGuid);
        }
        public static void SkmLatestActivitySet(SkmLatestActivity act)
        {
            skmMp.SkmLatestActivitySet(act);
        }

        public static SkmLatestSellerDealCollection SkmLatestSellerDealGetBySellerGuid(Guid SellerGuid)
        {
            return skmMp.SkmLatestSellerDealGetBySid(SellerGuid);
        }

        public static void SkmLatestSellerDealSet(SkmLatestSellerDeal sdeal)
        {
            skmMp.SkmLatestSellerDealSet(sdeal);
        }


        #endregion

        #region 自訂版位設定設定 skm_customized_board

        /// <summary>
        /// 取得自訂版位資訊清單
        /// </summary>
        /// <param name="storeGuid">店編號</param>
        /// <returns>自訂版位資訊清單</returns>
        public static List<SkmCustomizedBoard> GetCustomizedBoardList(Guid sellerGuid)
        {
            return skmEp.GetSkmCustomizedBoardList(sellerGuid);
        }

        /// <summary>
        /// 依編號取得自訂版位資訊
        /// </summary>
        /// <param name="id">編號</param>
        /// <returns>自訂版位資訊</returns>
        public static SkmCustomizedBoard GetCustomizedBoardById(int id)
        {
            return skmEp.GetSkmCustomizedBoardById(id);
        }

        /// <summary>
        /// 修改自訂版位資訊
        /// </summary>
        /// <param name="skmCustomizedBoard">自訂版位資訊</param>
        /// <returns>是否修改成功</returns>
        public static bool SetCustomizedBoard(SkmCustomizedBoard skmCustomizedBoard)
        {
            return skmEp.UpdateSkmCustomizedBoard(skmCustomizedBoard);
        }
        #endregion

        #region Beacon

        #region SkmAppBeacon

        public static List<string> getFloor(string f)
        {
            List<string> fl = new List<string>();
            fl.Add(string.Format("{0}", f));

            if (f.Substring(0, 1).ToLower() == "b")
            {
                fl.Add(string.Format("{0}", f.Replace("F", "")));
            }
            else if (f.Substring(0, 1) == "0")
            {
                fl.Add(string.Format("{0}", f.Substring(1, f.Length - 1)));
            }
            return fl;
        }


        public static void SkmBeaconMessageTimeSlotInsert(SkmBeaconMessage entity, int beaconType, bool isTop)
        {
            DateTime dateStartDay = entity.EffectiveStart;
            DateTime dateEndDay = entity.EffectiveEnd;
            Guid gid = entity.Guid ?? Guid.Empty;
            string shopcode = "";
            string[] bids = new JsonSerializer().Deserialize<string[]>(entity.Beacon);

            int dealStatus = (int)SKMDealStatus.Draft;

            SkmBeaconMessageTimeSlotCollection beaconMessageColForInert = new SkmBeaconMessageTimeSlotCollection();

            if (beaconType == (int)SkmBeaconType.Deal)
            {
                gid = entity.BusinessHourGuid;

                string[] filter = new string[4];
                filter[0] = "business_hour_guid=" + gid;
                filter[1] = "shop_code=" + entity.ShopCode;
                //filter[2] = "floor=" + entity.Floor;
                filter[2] = string.Format("floor in ({0})", string.Join(",", getFloor(entity.Floor)));
                filter[3] = "beacon_type=" + (int)SkmBeaconType.Deal;

                beaconMessageColForInert = skmMp.SkmBeaconMessageTimeSlotGetList("sequence", filter);

                var externalDeals = SkmCacheFacade.GetOnlineViewExternalDeal(gid);
                if (externalDeals.IsLoaded)
                {
                    shopcode = entity.ShopCode;
                    dealStatus = externalDeals.Status;
                    if (externalDeals.Status < (int)SKMDealStatus.WaitingConfirm)
                    {
                        //尚未頁確，不產生檔案
                        return;
                    }
                    if (externalDeals.BeaconType == (int)BeaconType.None)
                    {
                        //不須產生Beacon Message
                        return;
                    }
                }
            }
            else
            {
                beaconMessageColForInert = skmMp.SkmBeaconMessageTimeSlotGetByBid(gid, beaconType);
            }

            //檢查目前是否已有排程資料，若沒有依據輸入的啟始與截止日期自動產生
            if (beaconMessageColForInert.Count == 0)
            {
                #region 依據輸入的啟始與截止日期自動產生
                //依據On檔的時間與地區產生SkmBeaconMessage的資料(檔期排程資料)
                //依序新增勾選區域的排程記錄
                SkmBeaconMessage beaconMsg = skmMp.SkmBeaconMessageGetList("", "business_hour_guid=" + gid).FirstOrDefault();
                if (beaconMsg == null)
                {
                    return;
                }

                if (isTop)
                {
                    /*
                     * 置頂
                     * */

                    if (bids == null)
                    {
                        return;
                    }

                    //刪除其他非置頂的資料
                    string[] filter = new string[2];
                    filter[0] = "business_hour_guid=" + gid;
                    filter[1] = "beacon_type<>" + (int)SkmBeaconType.Top;
                    SkmBeaconMessageTimeSlotCollection delData = skmMp.SkmBeaconMessageTimeSlotGetList("", filter);
                    foreach (SkmBeaconMessageTimeSlot sdts in delData)
                    {
                        skmMp.SkmBeaconMessageTimeSlotDelete(sdts.Id);
                    }
                    //刪除其他Beacon的資料
                    string[] filter2 = new string[3];
                    filter[0] = "business_hour_guid=" + gid;
                    filter[1] = "beacon_type=" + (int)SkmBeaconType.Top;

                    SkmBeaconMessageTimeSlotCollection delData2 = skmMp.SkmBeaconMessageTimeSlotGetList("", filter);
                    foreach (SkmBeaconMessageTimeSlot sdts in delData2)
                    {
                        if (!bids.Contains(sdts.BeaconId))
                        {
                            skmMp.SkmBeaconMessageTimeSlotDelete(sdts.Id);
                        }
                    }



                    //塞入置頂資料
                    string[] beacons = new JsonSerializer().Deserialize<string[]>(beaconMsg.Beacon);
                    foreach (string beacon in beacons)
                    {
                        for (var date = dateStartDay; DateTime.Parse(date.ToString("yyy/MM/dd")) <= DateTime.Parse(dateEndDay.ToString("yyy/MM/dd")); date = date.AddDays(1))
                        {
                            SkmBeaconMessageTimeSlot slot = new SkmBeaconMessageTimeSlot();
                            slot.BusinessHourGuid = gid;
                            slot.Sequence = 99999;
                            if (date.ToString("yyyy/MM/dd") == dateStartDay.ToString("yyyy/MM/dd"))
                            {
                                slot.EffectiveStart = DateTime.Parse(dateStartDay.ToString("yyyy/MM/dd HH:mm:ss"));
                            }
                            else
                            {
                                slot.EffectiveStart = DateTime.Parse(date.ToString("yyyy/MM/dd"));
                            }
                            if (date.ToString("yyyy/MM/dd") == dateEndDay.ToString("yyyy/MM/dd"))
                            {
                                slot.EffectiveEnd = DateTime.Parse(dateEndDay.ToString("yyyy/MM/dd HH:mm:ss"));
                            }
                            else
                            {
                                slot.EffectiveEnd = DateTime.Parse(date.ToString("yyyy/MM/dd 23:59"));
                            }

                            if (beaconType == (int)SkmBeaconType.Deal)
                            {
                                if (dealStatus == (int)SKMDealStatus.Confirmed)
                                {
                                    slot.Status = (int)DealTimeSlotStatus.Default;
                                }
                                else
                                {
                                    slot.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                                }
                            }
                            else
                            {
                                slot.Status = (int)DealTimeSlotStatus.Default;
                            }
                            slot.BeaconType = (int)SkmBeaconType.Top;
                            slot.BeaconId = beacon;
                            slot.ShopCode = shopcode;
                            skmMp.SkmBeaconMessageTimeSlotSet(slot);
                        }
                    }
                }
                else
                {
                    /*
                     * 非置頂
                     * */
                    //刪除置頂的資料
                    string[] filter = new string[2];
                    filter[0] = "business_hour_guid=" + gid;
                    filter[1] = "beacon_type=" + (int)SkmBeaconType.Top;
                    SkmBeaconMessageTimeSlotCollection delData = skmMp.SkmBeaconMessageTimeSlotGetList("", filter);
                    foreach (SkmBeaconMessageTimeSlot sdts in delData)
                    {
                        skmMp.SkmBeaconMessageTimeSlotDelete(sdts.Id);
                    }

                    //塞入非置頂資料
                    for (var date = dateStartDay; DateTime.Parse(date.ToString("yyy/MM/dd")) <= DateTime.Parse(dateEndDay.ToString("yyy/MM/dd")); date = date.AddDays(1))
                    {
                        SkmBeaconMessageTimeSlot slot = new SkmBeaconMessageTimeSlot();
                        slot.BusinessHourGuid = gid;
                        slot.Sequence = 99999;
                        if (date.ToString("yyyy/MM/dd") == dateStartDay.ToString("yyyy/MM/dd"))
                        {
                            slot.EffectiveStart = DateTime.Parse(dateStartDay.ToString("yyyy/MM/dd HH:mm:ss"));
                        }
                        else
                        {
                            slot.EffectiveStart = DateTime.Parse(date.ToString("yyyy/MM/dd"));
                        }
                        if (date.ToString("yyyy/MM/dd") == dateEndDay.ToString("yyyy/MM/dd"))
                        {
                            slot.EffectiveEnd = DateTime.Parse(dateEndDay.ToString("yyyy/MM/dd HH:mm:ss"));
                        }
                        else
                        {
                            slot.EffectiveEnd = DateTime.Parse(date.ToString("yyyy/MM/dd 23:59"));
                        }
                        if (beaconType == (int)SkmBeaconType.Deal)
                        {
                            if (dealStatus == (int)SKMDealStatus.Confirmed)
                            {
                                slot.Status = (int)DealTimeSlotStatus.Default;
                            }
                            else
                            {
                                slot.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                            }
                            slot.Floor = entity.Floor;
                        }
                        else
                        {
                            slot.Status = (int)DealTimeSlotStatus.Default;
                        }
                        slot.BeaconType = beaconType;
                        slot.ShopCode = shopcode;
                        skmMp.SkmBeaconMessageTimeSlotSet(slot);
                    }
                }
                #endregion
            }
            else  //若已有排程資料則需判斷是否需要修改
            {
                SkmBeaconMessageTimeSlotCollection beaconMessageColForUpdate = new SkmBeaconMessageTimeSlotCollection();
                if (beaconType == (int)SkmBeaconType.Deal)
                {
                    string[] filter = new string[4];
                    filter[0] = "business_hour_guid=" + gid;
                    filter[1] = "shop_code=" + entity.ShopCode;
                    //filter[2] = "floor=" + entity.Floor;
                    filter[2] = string.Format("floor in ({0})", string.Join(",", getFloor(entity.Floor)));
                    filter[3] = "beacon_type=" + (int)SkmBeaconType.Deal;

                    beaconMessageColForUpdate = skmMp.SkmBeaconMessageTimeSlotGetList("sequence", filter);
                }
                else
                {
                    beaconMessageColForUpdate = skmMp.SkmBeaconMessageTimeSlotGetByBid(gid, beaconType);
                }

                //刪除日期以外的資料
                List<SkmBeaconMessageTimeSlot> deleteData = beaconMessageColForUpdate.Where(x => x.EffectiveStart < dateStartDay || x.EffectiveEnd > dateEndDay).ToList();
                foreach (SkmBeaconMessageTimeSlot sdts in deleteData)
                {
                    skmMp.SkmBeaconMessageTimeSlotDelete(sdts.Id);
                }

                if (beaconType == (int)SkmBeaconType.Deal)
                {
                    string[] filter = new string[4];
                    filter[0] = "business_hour_guid=" + gid;
                    filter[1] = "shop_code=" + entity.ShopCode;
                    filter[2] = string.Format("floor in ({0})", string.Join(",", getFloor(entity.Floor)));
                    filter[3] = "beacon_type=" + (int)SkmBeaconType.Deal;

                    beaconMessageColForUpdate = skmMp.SkmBeaconMessageTimeSlotGetList("sequence", filter);
                }
                else
                {
                    beaconMessageColForUpdate = skmMp.SkmBeaconMessageTimeSlotGetByBid(gid, beaconType);
                }

                SkmBeaconMessage beaconMsg = skmMp.SkmBeaconMessageGetList("", "Guid=" + gid).FirstOrDefault();
                if (beaconMsg == null)
                {
                    return;
                }

                if (isTop)
                {
                    /*
                     * 置頂
                     * */

                    if (bids == null)
                    {
                        return;
                    }

                    //刪除其他非置頂的資料
                    string[] filter = new string[2];
                    filter[0] = "business_hour_guid=" + gid;
                    filter[1] = "beacon_type<>" + (int)SkmBeaconType.Top;
                    SkmBeaconMessageTimeSlotCollection delData = skmMp.SkmBeaconMessageTimeSlotGetList("", filter);
                    foreach (SkmBeaconMessageTimeSlot sdts in delData)
                    {
                        skmMp.SkmBeaconMessageTimeSlotDelete(sdts.Id);
                    }
                    //刪除其他Beacon的資料
                    string[] filter2 = new string[3];
                    filter[0] = "business_hour_guid=" + gid;
                    filter[1] = "beacon_type=" + (int)SkmBeaconType.Top;

                    SkmBeaconMessageTimeSlotCollection delData2 = skmMp.SkmBeaconMessageTimeSlotGetList("", filter);
                    foreach (SkmBeaconMessageTimeSlot sdts in delData2)
                    {
                        if (!bids.Contains(sdts.BeaconId))
                        {
                            skmMp.SkmBeaconMessageTimeSlotDelete(sdts.Id);
                        }
                    }


                    beaconMessageColForUpdate = skmMp.SkmBeaconMessageTimeSlotGetByBid(gid, beaconType);

                    //塞入置頂資料
                    string[] beacons = new JsonSerializer().Deserialize<string[]>(beaconMsg.Beacon);
                    foreach (string beacon in beacons)
                    {
                        for (DateTime date = dateStartDay; DateTime.Parse(date.ToString("yyy/MM/dd")) <= DateTime.Parse(dateEndDay.ToString("yyy/MM/dd")); date = date.AddDays(1))
                        {
                            SkmBeaconMessageTimeSlot osdts = beaconMessageColForUpdate.Where(x => DateTime.Parse((x.EffectiveStart ?? DateTime.Now).ToString("yyyy/MM/dd")) == DateTime.Parse(date.ToString("yyyy/MM/dd")) && x.BeaconId == beacon).FirstOrDefault();
                            if (osdts == null)
                            {
                                SkmBeaconMessageTimeSlot sdts = new SkmBeaconMessageTimeSlot();
                                sdts.BusinessHourGuid = gid;
                                sdts.Sequence = 99999;
                                if (date.ToString("yyyy/MM/dd") == dateStartDay.ToString("yyyy/MM/dd"))
                                {
                                    sdts.EffectiveStart = DateTime.Parse(dateStartDay.ToString("yyyy/MM/dd HH:mm:ss"));
                                }
                                else
                                {
                                    sdts.EffectiveStart = DateTime.Parse(date.ToString("yyyy/MM/dd"));
                                }
                                if (date.ToString("yyyy/MM/dd") == dateEndDay.ToString("yyyy/MM/dd"))
                                {
                                    sdts.EffectiveEnd = DateTime.Parse(dateEndDay.ToString("yyyy/MM/dd HH:mm:ss"));
                                }
                                else
                                {
                                    sdts.EffectiveEnd = DateTime.Parse(date.ToString("yyyy/MM/dd 23:59"));
                                }
                                if (beaconType == (int)SkmBeaconType.Deal)
                                {
                                    if (dealStatus == (int)SKMDealStatus.Confirmed)
                                    {
                                        sdts.Status = (int)DealTimeSlotStatus.Default;
                                    }
                                    else
                                    {
                                        sdts.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                                    }
                                    sdts.Floor = entity.Floor;
                                }
                                else
                                {
                                    sdts.Status = (int)DealTimeSlotStatus.Default;
                                }
                                sdts.BeaconType = (int)SkmBeaconType.Top;
                                sdts.BeaconId = beacon;
                                sdts.ShopCode = shopcode;
                                skmMp.SkmBeaconMessageTimeSlotSet(sdts);
                            }
                        }
                    }
                }
                else
                {
                    /*
                     * 非置頂
                     * */
                    //刪除置頂的資料
                    string[] filter = new string[2];
                    filter[0] = "business_hour_guid=" + gid;
                    filter[1] = "beacon_type=" + (int)SkmBeaconType.Top;
                    SkmBeaconMessageTimeSlotCollection delData = skmMp.SkmBeaconMessageTimeSlotGetList("", filter);
                    foreach (SkmBeaconMessageTimeSlot sdts in delData)
                    {
                        skmMp.SkmBeaconMessageTimeSlotDelete(sdts.Id);
                    }

                    if (beaconType == (int)SkmBeaconType.Deal)
                    {
                        string[] filter1 = new string[4];
                        filter1[0] = "business_hour_guid=" + gid;
                        filter1[1] = "shop_code=" + entity.ShopCode;
                        //filter1[2] = "floor=" + entity.Floor;
                        filter1[2] = string.Format("floor in ({0})", string.Join(",", getFloor(entity.Floor)));
                        filter1[3] = "beacon_type=" + (int)SkmBeaconType.Deal;

                        beaconMessageColForUpdate = skmMp.SkmBeaconMessageTimeSlotGetList("sequence", filter1);
                    }
                    else
                    {
                        beaconMessageColForUpdate = skmMp.SkmBeaconMessageTimeSlotGetByBid(gid, beaconType);
                    }

                    //塞入非置頂資料
                    for (DateTime date = dateStartDay; DateTime.Parse(date.ToString("yyy/MM/dd")) <= DateTime.Parse(dateEndDay.ToString("yyy/MM/dd")); date = date.AddDays(1))
                    {
                        SkmBeaconMessageTimeSlot osdts = beaconMessageColForUpdate.Where(x => DateTime.Parse((x.EffectiveStart ?? DateTime.Now).ToString("yyyy/MM/dd")) == DateTime.Parse(date.ToString("yyyy/MM/dd"))).FirstOrDefault();
                        if (osdts == null)
                        {
                            SkmBeaconMessageTimeSlot sdts = new SkmBeaconMessageTimeSlot();
                            sdts.BusinessHourGuid = gid;
                            sdts.Sequence = 99999;
                            if (date.ToString("yyyy/MM/dd") == dateStartDay.ToString("yyyy/MM/dd"))
                            {
                                sdts.EffectiveStart = DateTime.Parse(dateStartDay.ToString("yyyy/MM/dd HH:mm:ss"));
                            }
                            else
                            {
                                sdts.EffectiveStart = DateTime.Parse(date.ToString("yyyy/MM/dd"));
                            }
                            if (date.ToString("yyyy/MM/dd") == dateEndDay.ToString("yyyy/MM/dd"))
                            {
                                sdts.EffectiveEnd = DateTime.Parse(dateEndDay.ToString("yyyy/MM/dd HH:mm:ss"));
                            }
                            else
                            {
                                sdts.EffectiveEnd = DateTime.Parse(date.ToString("yyyy/MM/dd 23:59"));
                            }
                            if (beaconType == (int)SkmBeaconType.Deal)
                            {
                                if (dealStatus == (int)SKMDealStatus.Confirmed)
                                {
                                    sdts.Status = (int)DealTimeSlotStatus.Default;
                                }
                                else
                                {
                                    sdts.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                                }
                                sdts.Floor = entity.Floor;
                            }
                            else
                            {
                                sdts.Status = (int)DealTimeSlotStatus.Default;
                            }
                            sdts.BeaconType = beaconType;
                            sdts.ShopCode = shopcode;
                            skmMp.SkmBeaconMessageTimeSlotSet(sdts);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 發票核銷
        /// </summary>
        /// <param name="skmPayOrder">Skm Pay Order</param>
        /// <param name="skmPayOrderStatus">訂單狀態<see cref="SkmPayOrderStatus"/></param>
        /// <returns>是否核銷成功</returns>
        public static bool InvoiceWriteOff(SkmPayOrder skmPayOrder, out SkmPayOrderStatus skmPayOrderStatus)
        {
            skmPayOrderStatus = SkmPayOrderStatus.CreatedInvoiceWriteOffFail;

            #region 取得invoice
            //因為sell_no需要取得開立發票的那筆sell_no，所以需要找到開立發票的那筆資料，再找最新的一筆(這只是預防措施，正常來說只會有一筆開立發票)
            //因為sell_no是後面才加的，根本沒有時間調整DB結構，正常來說如果那是交易序號，那應該拿來當編號用，並且標註成功失敗
            SkmPayInvoice skmPayInvoice = skmEp.GetSkmPayInvoice(skmPayOrder.Id, SkmPayPlatFormId)
                                               .Where(item => item.SellTranstype == (int)TurnCloudSellTransType.Sell && item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale)
                                               .OrderByDescending(item => item.Id).FirstOrDefault();
            if (skmPayInvoice == null || skmPayInvoice.Id <= 0)
            {
                return false;
            }

            //不針對skmPayInvoice判斷交易是否是未被核銷(skmPayInvoice.SellTranstype == (int)TurnCloudSellTransType.Sell & skmPayInvoice.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale)
            //理由是相信原本核銷機制會判斷交易是否已被核銷

            SkmPayInvoiceSellDetail skmPayInvoiceSellDetail = skmEp.GetSkmPayInvoiceSellDetailsByInvoiceId(skmPayInvoice.Id).FirstOrDefault();
            if (skmPayInvoiceSellDetail == null || skmPayInvoiceSellDetail.Id <= 0)
            {
                return false;
            }

            List<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails = skmEp.GetSkmPayInvoiceTenderDetailsByInvoiceId(skmPayInvoice.Id);
            if (skmPayInvoiceTenderDetails == null || skmPayInvoiceTenderDetails.Count() <= 0)
            {
                return false;
            }
            #endregion

            #region 建立核銷資訊
            DateTime nowDate = DateTime.Now;
            //這邊有傳址呼叫問題，需要做object.Clone(),先暫時用先後順序解決
            SkmPayInvoice writeOffSkmPayInvoice = skmPayInvoice;
            writeOffSkmPayInvoice.Id = 0;
            writeOffSkmPayInvoice.SellOrgDay = skmPayInvoice.Tday;
            writeOffSkmPayInvoice.SellOrgNo = skmPayInvoice.SellNo;
            writeOffSkmPayInvoice.SellOrgPosid = skmPayInvoice.SellPosid;
            writeOffSkmPayInvoice.SellInvoice = skmPayInvoice.SellInvoice;
            writeOffSkmPayInvoice.SellDay = nowDate;
            writeOffSkmPayInvoice.Tday = nowDate;
            writeOffSkmPayInvoice.SellTranstype = (int)TurnCloudSellTransType.Sell;
            writeOffSkmPayInvoice.PreSalesStatus = (int)TurnCloudPreDalesStatus.WriteOff;
            writeOffSkmPayInvoice.SellNo = SkmFacade.GetSkmPayInvoiceWriteOffSellNo(skmPayInvoice.SellShopid, skmPayInvoice.SellDay);
            SkmPayInvoiceSellDetail writeOffSkmPayInvoiceSellDetail = skmPayInvoiceSellDetail;
            writeOffSkmPayInvoiceSellDetail.Id = 0;
            writeOffSkmPayInvoiceSellDetail.SellOrderNoS = "000001";
            List<SkmPayInvoiceTenderDetail> writeOffSkmPayInvoiceTenderDetails = skmPayInvoiceTenderDetails;
            writeOffSkmPayInvoiceTenderDetails.ForEach(item => item.Id = 0);
            bool isInvoiceSuccess = false;
            string responseMessage = string.Empty;
            try
            {
                isInvoiceSuccess = SkmFacade.SetSkmPayInvoice(writeOffSkmPayInvoice, writeOffSkmPayInvoiceSellDetail, writeOffSkmPayInvoiceTenderDetails);
                responseMessage = isInvoiceSuccess.ToString();
            }
            catch (Exception ex)
            {
                responseMessage = ex.ToString();
                logger.Error("SkmFacade.SetSkmPayInvoice", ex);
            }

            if (!isInvoiceSuccess)
            {
                skmPayOrderStatus = SkmPayOrderStatus.CreatedInvoiceWriteOffFail;
                skmPayOrder.Status = (int)SkmPayOrderStatus.CreatedInvoiceWriteOffFail;
                skmEp.SetSkmPayOrder(skmPayOrder);
                SkmFacade.SetSkmPayOrderTransLog(skmPayOrder.Id, skmPayOrder.Status, "SkmFacade.SetSkmPayInvoice",
                    isInvoiceSuccess,
                    string.Format("{0},{1},{2}",
                        new JsonSerializer().Serialize(skmPayInvoice),
                        new JsonSerializer().Serialize(skmPayInvoiceSellDetail),
                        new JsonSerializer().Serialize(skmPayInvoiceTenderDetails)),
                    responseMessage
                    , MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name));
                return false;
            }
            #endregion

            #region 呼叫發票API
            isInvoiceSuccess = false;
            responseMessage = string.Empty;
            WriteOffInvoiceRequest writeOffInvoiceRequest = TurnCloudServerUtility.GenerateWriteOffInvoiceRequest(writeOffSkmPayInvoice, writeOffSkmPayInvoiceSellDetail, writeOffSkmPayInvoiceTenderDetails);
            WriteOffInvoiceResponse writeOffInvoiceResponse = null;
            try
            {
                writeOffInvoiceResponse = TurnCloudServerUtility.WriteOffInvoice(writeOffInvoiceRequest);
                isInvoiceSuccess = writeOffInvoiceResponse.ResponseBase.ReturnCode == "1";
                responseMessage = writeOffInvoiceResponse.ResponseBase.ReturnMessage;
                skmPayInvoice.ReturnCode = writeOffInvoiceResponse.ResponseBase.ReturnCode;
                skmPayInvoice.ReturnMessage = writeOffInvoiceResponse.ResponseBase.ReturnMessage;
            }
            catch (Exception ex)
            {
                responseMessage = ex.ToString();
                skmPayInvoice.ReturnCode = "-1";
                logger.Error("TurnCloudServerUtility.WriteOffInvoice", ex);
            }
            skmPayOrderStatus = isInvoiceSuccess ? SkmPayOrderStatus.WriteOff : SkmPayOrderStatus.WriteOffFail;
            skmPayOrder.Status = (int)skmPayOrderStatus;
            skmEp.SetSkmPayOrder(skmPayOrder);
            writeOffSkmPayInvoice.Memo = responseMessage;
            skmEp.SetSkmPayInvoice(writeOffSkmPayInvoice);
            SkmFacade.SetSkmPayOrderTransLog(skmPayOrder.Id, skmPayOrder.Status, "TurnCloudServerUtility.WriteOffInvoice",
                isInvoiceSuccess,
                new JsonSerializer().Serialize(writeOffInvoiceRequest),
                new JsonSerializer().Serialize(writeOffInvoiceResponse),
                skmPayOrder.UserId, memo: responseMessage);
            return isInvoiceSuccess;
            #endregion

        }

        /// <summary>
        /// 獨立退發票
        /// </summary>
        /// <param name="skmPayOrder"></param>
        /// <param name="skmPayOrderStatus"></param>
        /// <param name="isGenerateRefundTradeNo"></param>
        /// <returns></returns>
        public static bool InvoiceRefund(SkmPayOrder skmPayOrder, out SkmPayOrderStatus skmPayOrderStatus, bool isGenerateRefundTradeNo = false)
        {
            skmPayOrderStatus = (SkmPayOrderStatus)skmPayOrder.Status;

            int snId = 0;
            SkmSerialNumberLog invoicetSnLog;

            //目前只許已產生退貨序號的訂單做單獨退發票，若未來有無退貨單號的獨退發票需求，再由此擴充取號
            if (string.IsNullOrEmpty(skmPayOrder.SkmRefundTradeNo) && !isGenerateRefundTradeNo)
            {
                return false;
            }

            if (isGenerateRefundTradeNo)
            {
                //取新的退貨單號
                SkmSerialNumber snObj = SkmFacade.GetSkmTradeNo(skmPayOrder.ShopCode, SkmFacade.SkmPayPlatFormId);
                snId = snObj.Id;
                skmPayOrder.SkmRefundTradeNo = snObj.GetTradeNo();
                invoicetSnLog = SkmFacade.AddSerialNumberLog(skmPayOrder.Id, snObj, SkmSerialNumberLogTradeType.ReturnInvoice);
            }

            if (skmPayOrder.SkmRefundTradeNo.Length > 24)
            {
                logger.ErrorFormat("{0} 發票退貨單長度大於 24", skmPayOrder.SkmRefundTradeNo);
                return false;
            }

            #region 組退貨發票

            var skmPayInvoices = skmEp.GetSkmPayInvoice(skmPayOrder.Id, SkmPayPlatFormId);

            SkmPayInvoice skmPayInvoice = null;
            if (skmPayInvoices.Any(item => item.SellTranstype == (int)TurnCloudSellTransType.Refund && item.ReturnCode == "1"))
            {
                //已退過發票
                return false;
            }
            else
            {
                skmPayInvoice = skmPayInvoices.Where(item => item.SellTranstype == (int)TurnCloudSellTransType.Sell &&
                                                             item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                             item.ReturnCode == "1")
                                              .OrderByDescending(item => item.Id).FirstOrDefault();
            };            

            var skmPayInvoiceSellDetail = skmEp.GetSkmPayInvoiceSellDetailsByInvoiceId(skmPayInvoice.Id).FirstOrDefault();
            if (skmPayInvoiceSellDetail == null || skmPayInvoiceSellDetail.Id <= 0) return false;
            
            var skmPayInvoiceTenderDetails = skmEp.GetSkmPayInvoiceTenderDetailsByInvoiceId(skmPayInvoice.Id);
            if (skmPayInvoiceTenderDetails == null || skmPayInvoiceTenderDetails.Count() <= 0) return false;
            
            DateTime nowDate = DateTime.Now;            
            
            //發票主檔
            SkmPayInvoice refundSkmPayInvoice = skmPayInvoice;
            refundSkmPayInvoice.Id = 0;
            refundSkmPayInvoice.SellOrgDay = skmPayInvoice.Tday;
            refundSkmPayInvoice.SellOrgNo = skmPayInvoice.SellNo;
            refundSkmPayInvoice.SellOrgPosid = skmPayInvoice.SellPosid;
            refundSkmPayInvoice.SellDay = nowDate;
            refundSkmPayInvoice.Tday = nowDate;
            refundSkmPayInvoice.SellTranstype = (int)TurnCloudSellTransType.Refund;
            refundSkmPayInvoice.SellNo = skmPayOrder.SkmRefundTradeNo.Substring(16, 6);            
            refundSkmPayInvoice.PreSalesStatus = skmPayOrder.Status == (int)SkmPayOrderStatus.WriteOff 
                    ? (int)TurnCloudPreDalesStatus.WriteOff : (int)TurnCloudPreDalesStatus.Sale;
            refundSkmPayInvoice.ReturnCode = null;
            refundSkmPayInvoice.ReturnMessage = null;

            //發票Detail
            SkmPayInvoiceSellDetail refundSkmPayInvoiceSellDetail = skmPayInvoiceSellDetail;
            refundSkmPayInvoiceSellDetail.Id = 0;
            refundSkmPayInvoiceSellDetail.SellId = skmPayInvoiceSellDetail.SellId;
            refundSkmPayInvoiceSellDetail.SellOrderNoS = "000001";

            //發票Tender
            List<SkmPayInvoiceTenderDetail> refundSkmPayInvoiceTenderDetails = skmPayInvoiceTenderDetails;
            refundSkmPayInvoiceTenderDetails.ForEach(item =>
            {
                item.Id = 0;
                if (item.SellTender != TurnCloudSellTenderType.MemberPointsDeduct)
                {
                    item.PayOrderNo = skmPayOrder.SkmRefundTradeNo;
                }

                switch (item.SellTender)
                {
                    case TurnCloudSellTenderType.MemberPointsDeduct:
                        item.SellTender = TurnCloudSellTenderType.MemberPointsDeductRefund;
                        break;
                    case TurnCloudSellTenderType.NationalCreditCardCenter:
                        item.SellTender = TurnCloudSellTenderType.NationalCreditCardCenterRefund;
                        break;
                    case TurnCloudSellTenderType.SkmPay:
                        item.SellTender = TurnCloudSellTenderType.SkmPayRefund;
                        break;
                    case TurnCloudSellTenderType.SkmPayNCCC:
                        item.SellTender = TurnCloudSellTenderType.SkmPayNCCCRefund;
                        break;
                    case TurnCloudSellTenderType.TaishinCreditCard:
                        item.SellTender = TurnCloudSellTenderType.TaishinCreditCardRefund;
                        break;
                }
            });

            #endregion

            #region 退貨發票存DB

            bool setInvoiceIsSuccess = false;
            
            try
            {                
                setInvoiceIsSuccess = SetSkmPayInvoice(refundSkmPayInvoice, refundSkmPayInvoiceSellDetail, refundSkmPayInvoiceTenderDetails);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("InvoiceRefund 儲存退貨發票失敗 {0}", ex.ToString());
            }

            if (!setInvoiceIsSuccess)
            {
                skmPayOrder.Status = (int)SkmPayOrderStatus.ReturnInvoiceFail;
                skmPayOrder.Memo = "儲存退貨發票失敗";
                skmEp.SetSkmPayOrder(skmPayOrder);
                
                SkmFacade.SetSkmPayOrderTransLog(skmPayOrder.Id, skmPayOrder.Status, "TurnCloudServerUtility.RefundInvoice", false,
                null, "儲存退貨發票失敗", skmPayOrder.UserId);

                if (isGenerateRefundTradeNo && snId != 0)
                {
                    skmEp.UpdateSkmSerialNumber(snId, false, false);
                }

                return false;
            }

            #endregion

            #region 呼叫騰雲退發票

            var refundInvoiceRequest = TurnCloudServerUtility.GenerateRefundInvoiceRequest(refundSkmPayInvoice, skmPayInvoiceSellDetail, skmPayInvoiceTenderDetails);
                        
            RefundInvoiceResponse refundInvoiceResponse = null;            
            bool isInvoiceRefundSuccess = false;

            try
            {
                refundInvoiceResponse = TurnCloudServerUtility.RefundInvoice(refundInvoiceRequest);
                isInvoiceRefundSuccess = refundInvoiceResponse.ResponseBase.ReturnCode == "1";
                
                skmPayInvoice.ReturnCode = refundInvoiceResponse.ResponseBase.ReturnCode;
                skmPayInvoice.ReturnMessage = refundInvoiceResponse.ResponseBase.ReturnMessage;
                skmEp.SetSkmPayInvoice(skmPayInvoice);                
            }
            catch (Exception ex)
            {                
                logger.ErrorFormat("InvoiceRefund 呼叫騰雲退發票失敗 {0}", ex.ToString());
                return false;
            }

            #endregion
            
            skmPayOrder.Status = isInvoiceRefundSuccess ? (int)SkmPayOrderStatus.RefundInvoiceOnlySuccess : (int)SkmPayOrderStatus.RefundInvoiceOnlyFail;
            skmPayOrder.Memo = isInvoiceRefundSuccess ? "單獨退發票成功" : "單獨退發票失敗";
            skmEp.SetSkmPayOrder(skmPayOrder);

            SkmFacade.SetSkmPayOrderTransLog(skmPayOrder.Id, skmPayOrder.Status, "TurnCloudServerUtility.RefundInvoice", isInvoiceRefundSuccess,
                new JsonSerializer().Serialize(refundInvoiceRequest), new JsonSerializer().Serialize(refundInvoiceResponse), skmPayOrder.UserId);            

            if (isGenerateRefundTradeNo && snId != 0)
            {
                skmEp.UpdateSkmSerialNumber(snId, false, isInvoiceRefundSuccess);
            }

            skmPayOrderStatus = (SkmPayOrderStatus)skmPayOrder.Status;

            return isInvoiceRefundSuccess;            
        }
        
        #endregion

        #region SKMBeacon電量管理通知信

        public static void SkmBeaconLowPowerNotify()
        {
            var shopCodeCol = skmMp.SkmShoppeGetAllShopCodeList();
            foreach (var shopCode in shopCodeCol)
            {
                var systemData = _sysp.SystemDataGet(string.Format("{0}{1}", BeaconPowerNoticeSystemData, shopCode));
                if (systemData.IsLoaded)
                {
                    JsonSerializer json = new JsonSerializer();
                    var setting = json.Deserialize<Dictionary<string, string>>(systemData.Data);
                    BeaconPowerLevel pl = (BeaconPowerLevel)int.Parse(setting["beaconPowerLevel"]);

                    var beaconCol = mp.ActionEventDeviceInfoGetByElePower(SkmActionEventInfoId, pl, shopCode).Where(x => x.LastUpdateTime != null).ToList();
                    if (beaconCol.Any())
                    {
                        int day = int.Parse(setting["eletricPowerNotUpdateWaitDay"]);
                        var notificationCol = beaconCol.Where(x => ((DateTime)x.LastUpdateTime).AddDays(day) <= DateTime.Now).ToList();
                        if (notificationCol.Any())
                        {
                            var skmShoppe = skmMp.ShoppeGetSellerByShopeCode(shopCode);
                            var seller = sp.SellerGet(skmShoppe.SellerGuid ?? Guid.Empty);
                            List<string> notificationEmails = setting["notificationEmails"].Split(';').ToList();
                            foreach (var beacon in notificationCol)
                            {
                                SendBeaconLowPowerMail(notificationEmails, seller.SellerName,
                                    skmShoppe.BrandCounterName.Replace("全館", "").Replace(seller.SellerName, ""), beacon.DeviceName);
                            }
                        }
                    }

                }
            }
        }

        private static bool EmailValid(string emailaddress)
        {
            string emailRegular = @"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$";
            return Regex.IsMatch(emailaddress, emailRegular);
        }

        private static void SendBeaconLowPowerMail(List<string> notificationEmails, string sellerName, string shopName, string beaconName)
        {
            MailMessage msg = new MailMessage();
            foreach (string email in notificationEmails)
            {
                if (EmailValid(email))
                {
                    msg.To.Add(email);
                }
            }
            if (msg.To.Any())
            {
                msg.Subject = string.Format("SKM Beacon 電量異常通知【{0}】", beaconName);
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = string.Format(@"您好，新光三越{0} {1}【{2}】已多日未更新電量或低電量，
                                煩請前往<a href='{3}' target='skm_window'>電量管理</a>了解處理，謝謝。"
                            , sellerName
                            , shopName.Replace("全館", "")
                            , beaconName
                            , string.Format("{0}/SKMDeal/SkmAppBeaconPowerList", config.SiteUrl));
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        #endregion  SKMBeacon電量管理通知信

        #endregion

        #region 後台-權限相關

        /// <summary>
        /// 檢查是否是總公司權限才能擁有編輯權限的類別
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static bool CheckSkmHqCategoriesPower(int userId, string categoryId)
        {
            int tempCategory;
            if (!int.TryParse(categoryId, out tempCategory))
            {
                return false;
            }

            var cateCol = config.SkmHQCategories.Split(',');
            var hqCategoryCol = cateCol.Any() ? cateCol.Select(int.Parse).ToList() : new List<int>();
            if (!hqCategoryCol.Any())
            {
                return true;
            }

            var sellerRoleGuids = ChannelFacade.GetOdmSellerRoleGuidsByUserId(userId, config.SkmRootSellerGuid);
            var isSkmHQOwner = sellerRoleGuids.Any(sellGuid => sellGuid == config.SkmRootSellerGuid);

            //若類別屬於SkmHQCategories且有總公司權限才 return true, 若類別不屬於SkmHQCategories皆true
            return hqCategoryCol.All(x => x != tempCategory) || isSkmHQOwner;
        }

        /// <summary>
        /// 同時查詢兩種權限 (判斷categoryid & userid 或單純userid)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="categoryId"></param>
        /// <param name="isSkmHQOwner"></param>
        /// <returns></returns>
        public static bool CheckSkmHqCategoriesPower(int userId, string categoryId, ref bool isSkmHQOwner)
        {
            int tempCategory;
            if (!int.TryParse(categoryId, out tempCategory))
            {
                return false;
            }

            var cateCol = config.SkmHQCategories.Split(',');
            var hqCategoryCol = cateCol.Any() ? cateCol.Select(int.Parse).ToList() : new List<int>();
            if (!hqCategoryCol.Any())
            {
                return true;
            }

            var sellerRoleGuids = ChannelFacade.GetOdmSellerRoleGuidsByUserId(userId, config.SkmRootSellerGuid);
            isSkmHQOwner = sellerRoleGuids.Any(sellGuid => sellGuid == config.SkmRootSellerGuid);

            //若類別屬於SkmHQCategories且有總公司權限才 return true, 若類別不屬於SkmHQCategories皆true
            return hqCategoryCol.All(x => x != tempCategory) || isSkmHQOwner;
        }

        /// <summary>
        /// 檢查是否是總公司權限
        /// </summary>
        /// <param name="userId"></param>        
        /// <returns></returns>
        public static bool CheckSkmHqPower(int userId)
        {
            var isSkmRootOwner = false;
            var sellGuidCol = config.SkmRootPowerSellerGuid.Split(',');
            var specialHqSellGuidCol = sellGuidCol.Any() ? sellGuidCol.Select(Guid.Parse).ToList() : new List<Guid>();
            if (!specialHqSellGuidCol.Any())
            {
                return true;
            }

            var sellerRoleGuids = ChannelFacade.GetOdmSellerRoleGuidsByUserId(userId, config.SkmRootSellerGuid);
            foreach (var sellGuid in sellerRoleGuids)
            {
                isSkmRootOwner = specialHqSellGuidCol.Any(x => x == sellGuid);
                if (isSkmRootOwner)
                {
                    break;
                }
            }

            return isSkmRootOwner;
        }

        /// <summary>
        /// 後台使用者預設店別權限
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="defaultGuid"></param>
        /// <returns></returns>
        public static Guid GetDefaultSellerGuid(int userId, Guid defaultGuid)
        {
            var sellerRoleGuids = ChannelFacade.GetOdmSellerRoleGuidsByUserId(userId, config.SkmRootSellerGuid).ToList();
            if (sellerRoleGuids.Any(x => x == defaultGuid))
            {
                return defaultGuid;
            }

            var onedownSeller = ChannelFacade.GetOwnOneDownSeller(sellerRoleGuids, config.SkmRootSellerGuid);
            return onedownSeller.Any() ? onedownSeller.First() : Guid.Empty;
        }

        public static List<string> ShopCodeGetByUserId(int userId)
        {
            var sellerRoleGuids = ChannelFacade.GetOdmSellerRoleGuidsByUserId(userId, cp.SkmRootSellerGuid).ToList();
            var shopCol = skmMp.SkmShoppeGetAllShop();
            return shopCol.Where(item => sellerRoleGuids.Contains(item.StoreGuid ?? Guid.Empty)).Select(x => x.ShopCode).ToList();
        }

        public static bool CheckSkmAccount(string userName)
        {
            return userName.ToLower().Contains("skm");
        }

        #endregion

        #region 燒點 

        public static int GetDealBurningPointByExternalGuid(Guid eid, string shopCode)
        {
            var be = skmMp.SkmBurningEventGet(eid, shopCode, true);
            return be.IsLoaded ? be.BurningPoint : 0;
        }

        public static SkmBurningEvent GetBurningEventByBid(Guid bid)
        {
            ViewExternalDeal externalDeal = SkmCacheFacade.GetOnlineViewExternalDeal(bid);
            return skmMp.SkmBurningEventGet(externalDeal.Guid, externalDeal.DealVersion >= (int)ExternalDealVersion.SkmPay ? externalDeal.ShopCode : string.Empty);
        }

        public static SkmBurningOrderLog GetSkmBurningOrderLog(int eventId, int userId, Guid bid, Guid orderGuid, int couponId)
        {
            var log = new SkmBurningOrderLog
            {
                EventId = eventId,
                UserId = userId,
                CreateTime = DateTime.Now,
                SeqId = 0,
                Status = (byte)SkmBurningOrderStatus.Init,
                OrderGuid = orderGuid,
                CouponId = couponId,
                Bid = bid
            };

            skmMp.SkmBurningOrderLogSet(log);

            return log;
        }

        /// <summary>
        /// 燒點
        /// </summary>
        /// <param name="be"></param>
        /// <param name="log"></param>
        /// <param name="skmAppCardNo"></param>
        /// <param name="deviceType"></param>
        /// <param name="deviceCode"></param>
        /// <param name="sellerGuid"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool OrderSkmBurningCoupon(SkmBurningEvent be, SkmBurningOrderLog log, string skmAppCardNo, ApiUserDeviceType deviceType, string deviceCode, Guid sellerGuid, out string message)
        {
            message = string.Empty;

            if (!config.EnableCallBurningWebService)
            {
                log.Status = (byte)SkmBurningOrderStatus.Create;
                skmMp.SkmBurningOrderLogSet(log);
                message = "Success for test.";
                return true;
            }

            var ed = pp.ExternalDealGet(be.ExternalGuid);
            if (ed.IsPrizeDeal && config.SkmEnablePrizeEventTestMode)
            {
                log.Status = (byte)SkmBurningOrderStatus.Create;
                skmMp.SkmBurningOrderLogSet(log);
                message = "Success for test.";
                return true;
            }

            var costCenter = GetCostCenterByBurningEventId(be.Id);

            //組燒點 sendData
            var sendData = GetBurningPointXml(SkmWebServiceTransCode.GetBurningPointCoupon
                , GetBurningPointCouponXml(be.BurningEventSid, log.CouponId, be.BurningPoint, be.ExchangeItemId, skmAppCardNo, be.SkmEventId
                , (ActiveType)ed.ActiveType, ed.Title, log.CreateTime, be.GiftPoint, be.GiftWalletId, costCenter, deviceCode)
                , deviceCode, (ActiveType)ed.ActiveType, sellerGuid);
            log.SendData = HttpUtility.UrlDecode(sendData);

            try
            {
                const string skmUmallCheckSuccessCode = "00";
                const string skmUmallCheckTagName = "T3900";

                var wsResult =
                    System.Net.WebUtility.UrlDecode(
                        (string)CommonFacade.InvokeWebService(cp.SkmWebserviceUrl, 30, cp.SkmWsNamespace,
                            cp.SkmWsClassName, cp.SkmWsMethod
                            , new object[]
                            {
                                sendData
                            }));

                using (StringReader checkResult = new StringReader(wsResult))
                {
                    skmMp.SkmSystemLogSet(SkmSystemLogType.BurningOrder, log.Id, HttpUtility.UrlDecode(wsResult));
                    using (XmlReader reader = XmlReader.Create(checkResult))
                    {
                        if (reader.ReadToDescendant(skmUmallCheckTagName))
                        {
                            reader.Read();
                            log.RequestLog = reader.Value;
                            log.ReceivedData = HttpUtility.UrlDecode(wsResult);
                            log.ReceivedTime = DateTime.Now;
                            var isSuccess = reader.Value == skmUmallCheckSuccessCode;
                            log.Status = isSuccess ? (byte)SkmBurningOrderStatus.Create : (byte)SkmBurningOrderStatus.Fail;
                            skmMp.SkmBurningOrderLogSet(log);
                            message = isSuccess ? "Success." : log.RequestLog;
                            return isSuccess;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //APP只能讀取umall回傳來的錯誤代碼，不可因為exception的問題而傳出app不認識的代碼而閃退
                //預設使用96
                message = "96";
                var errorMsg = ex.Message + "," + ex.ToString();
                log.RequestLog = errorMsg;
                skmMp.SkmSystemLogSet(SkmSystemLogType.BurningOrder, log.Id, errorMsg);
                skmMp.SkmBurningOrderLogSet(log);
            }

            return false;
        }

        public static bool SendVerifyReply(SkmVerifyReply reply, bool isCancel = false)
        {
            reply.SkmTmCode = string.Format("{0}_{1}", GetSkmTransIdTmCode(reply.SkmTransId), DateTime.Now.ToString("yyyyMMdd"));
            reply.SkmReplySn = _sysp.SerialNumberGetNext(reply.SkmTmCode).ToString().PadLeft(6, '0');

            skmMp.SkmVerifyReplySet(reply);
            skmMp.SkmVerifyReplyHistorySet(new SkmVerifyReplyHistory
            {
                TrustId = reply.TrustId,
                SkmTmCode = reply.SkmTmCode,
                SkmReplySn = reply.SkmReplySn,
                IsCancel = isCancel
            });

            var sendData = GetBurningPointXml(SkmWebServiceTransCode.VerifyBurningCoupon,
                GetBurningVerifyCouponXml(reply, isCancel), reply.ShopCode, DiscountTypeToActiveType((DiscountType)reply.DiscountType), Guid.Empty);

            reply.ReplyCount++;

            if (isCancel)
            {
                reply.CancelTime = DateTime.Now;
                reply.IsCancel = true;
                reply.CancelSendData = HttpUtility.UrlDecode(sendData);
            }
            else
            {
                reply.SendData = HttpUtility.UrlDecode(sendData);
            }

            try
            {
                const string skmUmallCheckSuccessCode = "00";
                const string skmUmallCheckTagName = "T3900";

                var wsResult =
                    System.Net.WebUtility.UrlDecode(
                        (string)CommonFacade.InvokeWebService(cp.SkmWebserviceUrl, 30, cp.SkmWsNamespace,
                            cp.SkmWsClassName, cp.SkmWsMethod
                            , new object[]
                            {
                                sendData
                            }));

                using (StringReader checkResult = new StringReader(wsResult))
                {
                    skmMp.SkmSystemLogSet(SkmSystemLogType.BurningReply, reply.Id, HttpUtility.UrlDecode(wsResult));
                    using (XmlReader reader = XmlReader.Create(checkResult))
                    {
                        if (reader.ReadToDescendant(skmUmallCheckTagName))
                        {
                            reader.Read();
                            var isSuccess = reader.Value == skmUmallCheckSuccessCode;
                            reply.ReplyMsg = reader.Value;
                            reply.ReplyStatus = isSuccess ? (byte)SkmVerifyReplyStatus.ReplySuccess : (byte)SkmVerifyReplyStatus.ReplyFail;
                            reply.ModifyTime = DateTime.Now;
                            skmMp.SkmVerifyReplySet(reply);
                            return isSuccess;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                reply.ModifyTime = DateTime.Now;
                reply.ReplyMsg = ex.StackTrace;
                reply.ReplyStatus = (byte)SkmVerifyReplyStatus.ReplyFail;
                skmMp.SkmSystemLogSet(SkmSystemLogType.BurningReply, reply.Id, ex.StackTrace);
                skmMp.SkmVerifyReplySet(reply);
            }

            return false;
        }

        /// <summary>
        /// 組中獎清冊API XML
        /// </summary>
        /// <param name="couponId"></param>
        /// <param name="skmEventId"></param>
        /// <param name="burningEventSid"></param>
        /// <param name="burningPoint"></param>
        /// <param name="exchangeItemId"></param>
        /// <param name="appCardNo"></param>
        /// <param name="shopCode"></param>
        /// <param name="costCode"></param>
        /// <param name="orderCode"></param>
        /// <returns></returns>
        public static bool SendPrizeCoupon(SkmBurningEvent burningEvent, SkmBurningCostCenter costCenter, int couponId, string appCardNo, string shopCode, int userId, int recordId, string deviceCode, Guid sellerGuid)
        {
            var sendData = GetBurningPointXml(SkmWebServiceTransCode.SendPrizeCoupon
                , GetSendPrizeCouponXml(couponId, burningEvent.SkmEventId, burningEvent.BurningEventSid, burningEvent.BurningPoint, burningEvent.ExchangeItemId, appCardNo
                                        , shopCode, costCenter.CostCenterCode, costCenter.OrderCode)
                , shopCode
                , DiscountTypeToActiveType(DiscountType.DiscountOther)//隨意填
                , sellerGuid
                , deviceCode);

            logger.Info("SendPrizeCoupon.sendData=" + HttpUtility.UrlDecode(sendData));

            PrizeDrawRecordLog log = new PrizeDrawRecordLog();
            log.SendData = HttpUtility.UrlDecode(sendData);
            log.SendTime = DateTime.Now;
            log.ApiName = Helper.GetEnumDescription(SkmWebServiceTransCode.SendPrizeCoupon);
            log.ShopCode = shopCode;
            log.UserId = userId;
            log.RequestLog = "";
            log.RecordId = recordId;

            if (config.EnableCallBurningWebService && config.SkmEnableSendPrizeData)
            {
                return SendPrizeCouponUmallApi(log, sendData);
            }
            else
            {
                cep.InsertOrUpdatePrizeDrawRecordLog(log);
                return true;
            }
        }

        /// <summary>
        /// 重新發送抽獎活動選擇的館別給Umall
        /// </summary>
        public static void ResendPrizeCoupon()
        {
            logger.Info("ReSendPrizeCoupon Need Resend Event ID:" + config.ResendPrizeCouponEventId);
            List<PrizeDrawRecordLog> prizeDrawRecordLogs = cep.GetNeedResendPrizeDrawRecordLogsByEventId(config.ResendPrizeCouponEventId);
            logger.Info("ReSendPrizeCoupon Need Resend Data Count:" + prizeDrawRecordLogs.Count);
            int successCount = 0;
            foreach (PrizeDrawRecordLog prizeDrawRecordLog in prizeDrawRecordLogs)
            {
                try
                {
                    prizeDrawRecordLog.SendTime = DateTime.Now;
                    if (SendPrizeCouponUmallApi(prizeDrawRecordLog, prizeDrawRecordLog.SendData))
                        successCount++;
                }
                catch (Exception ex)
                {
                    logger.Error("ReSendPrizeCoupon " + ex.ToString());
                }
            }
            logger.Info("ReSendPrizeCoupon Resend Success Count:" + successCount);
        }

        /// <summary>
        /// 發送抽獎活動選擇的館別給Umall
        /// </summary>
        /// <param name="log">發送紀錄</param>
        /// <param name="sendData">發送內容</param>
        /// <returns>是否發送成功</returns>
        private static bool SendPrizeCouponUmallApi(PrizeDrawRecordLog log, string sendData)
        {
            bool returnValuse = false;
            try
            {
                const string skmUmallCheckSuccessCode = "00";
                const string skmUmallCheckTagName = "T3900";

                var wsResult =
                    System.Net.WebUtility.UrlDecode(
                        (string)CommonFacade.InvokeWebService(cp.SkmWebserviceUrl, 30, cp.SkmWsNamespace,
                            cp.SkmWsClassName, cp.SkmWsMethod
                            , new object[]
                            {
                                    sendData
                            }));

                using (StringReader checkResult = new StringReader(wsResult))
                {
                    skmMp.SkmSystemLogSet(SkmSystemLogType.SendPrizeCoupon, log.Id, HttpUtility.UrlDecode(wsResult));
                    using (XmlReader reader = XmlReader.Create(checkResult))
                    {
                        if (reader.ReadToDescendant(skmUmallCheckTagName))
                        {
                            reader.Read();
                            returnValuse = reader.Value == skmUmallCheckSuccessCode;

                            log.RequestLog = reader.Value;
                            log.ReceivedData = HttpUtility.UrlDecode(wsResult);
                            log.ReceivedTime = DateTime.Now;

                            logger.Info("SendPrizeCoupon.RequestLog=" + reader.Value);
                            cep.InsertOrUpdatePrizeDrawRecordLog(log);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.ReceivedTime = DateTime.Now;
                log.ReceivedData = ex.StackTrace;
                log.RequestLog = "Exception" + ex.ToString();
                cep.InsertOrUpdatePrizeDrawRecordLog(log);
            }
            return returnValuse;
        }

        public static ActiveType DiscountTypeToActiveType(DiscountType discountType)
        {
            ActiveType at = ActiveType.Product;

            switch (discountType)
            {
                case DiscountType.DiscountPrice:
                    at = ActiveType.Product;
                    break;
                case DiscountType.DiscountBuyGetFree:
                    at = ActiveType.BuyGetFree;
                    break;
                case DiscountType.DiscountCash:
                case DiscountType.DiscountFree:
                case DiscountType.DiscountOther:
                case DiscountType.DiscountPercent:
                    at = ActiveType.Shoppe;
                    break;
            }

            return at;
        }

        /// <summary>
        /// skm pay 子母檔燒點
        /// </summary>
        /// <param name="model"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool CreateSkmBurningEventCombo(SkmBurningEventModel model, out string message,bool isAlibaba = false)
        {
            message = string.Empty;
            var result = false;
            logger.Info("model.CostCenter=" + new JsonSerializer().Serialize(model.CostCenter));

            foreach (var shop in model.CostCenter)
            {
                var newModel = model.Clone();//it's custom clone
                newModel.CostCenter = new List<SkmBurningEventCostCenter> { shop };

                var burningEvent = SaveBurningEvent(newModel);
                if (!burningEvent.IsLoaded)
                {
                    message = "DB save data fail.";
                    return false;
                }

                burningEvent.BurningEventSid = newModel.BurningEventSid = string.Format("C{0}", skmMp.SkmBurningEventGetYearCountByDate(DateTime.Now).ToString().PadLeft(5, '0'));
                newModel.EventId = burningEvent.SkmEventId;
                logger.InfoFormat("Bu.SaveEvent->guid:{0}, event sid:{1}, event id:{2}", newModel.ExternalGuid, burningEvent.BurningEventSid, newModel.EventId);

                //用分店的兌換數量才對
                newModel.ExchangeQty = newModel.CostCenter.FirstOrDefault().ExChangeQty;

                //拋帳店館代號 (send xml有做開關)
                if (model.IsCostCenterHq)
                {
                    newModel.CostStoreCode = "001";
                }
                else
                {
                    newModel.CostStoreCode = newModel.CostCenter.FirstOrDefault().StoreCode.PadLeft(3, '0');
                }

               
                var sendData = GetBurningPointXml(SkmWebServiceTransCode.AddBurningPointEvent,
                    GetBurningPointDetailXml(newModel), string.Empty, (ActiveType)newModel.ActiveType, Guid.Empty);
                burningEvent.SendData = HttpUtility.UrlDecode(sendData);
                logger.InfoFormat("Bu.SaveEvent->guid:{0}, sendXML:{1}", newModel.ExternalGuid, burningEvent.SendData);

                if (!config.EnableCallBurningWebService)
                {
                    burningEvent.Status = (byte)SkmBurningEventStatus.Create;
                    skmMp.SkmBurningEventSet(burningEvent);
                    message = "Success for test.";
                    result = true;
                    continue;
                }

                if (newModel.PrizeDeal && config.SkmEnablePrizeEventTestMode)
                {
                    burningEvent.Status = (byte)SkmBurningEventStatus.Create;
                    skmMp.SkmBurningEventSet(burningEvent);
                    message = "Success for test..";
                    result = true;
                    continue;
                }

                try
                {
                    if (!isAlibaba)//阿里巴巴上架才送UMALL
                    {
                        const string skmUmallCheckSuccessCode = "00";
                        const string skmUmallCheckTagName = "T3900";
                        logger.InfoFormat("Bu.SaveEvent->guid:{0}, call SKM WebService({1}).", newModel.ExternalGuid, cp.SkmWebserviceUrl);

                        var wsResult =
                            System.Net.WebUtility.UrlDecode(
                                (string)CommonFacade.InvokeWebService(cp.SkmWebserviceUrl, 30, cp.SkmWsNamespace,
                                    cp.SkmWsClassName, cp.SkmWsMethod
                                    , new object[]
                                    {
                                sendData
                                    }));

                        using (StringReader checkResult = new StringReader(wsResult))
                        {
                            skmMp.SkmSystemLogSet(SkmSystemLogType.BurningEvent, burningEvent.Id, HttpUtility.UrlDecode(wsResult));
                            using (XmlReader reader = XmlReader.Create(checkResult))
                            {
                                if (reader.ReadToDescendant(skmUmallCheckTagName))
                                {
                                    reader.Read();
                                    burningEvent.RequestLog = reader.Value;
                                    burningEvent.ReceivedData = HttpUtility.UrlDecode(wsResult);
                                    burningEvent.ReceivedTime = DateTime.Now;
                                    var isSuccess = reader.Value == skmUmallCheckSuccessCode;
                                    burningEvent.Status = isSuccess ? (byte)SkmBurningEventStatus.Create : (byte)SkmBurningEventStatus.Fail;
                                    skmMp.SkmBurningEventSet(burningEvent);
                                    message = isSuccess ? "Success." : "建立燒點活動錯誤代碼" + burningEvent.RequestLog;
                                    result = isSuccess;
                                }
                                else
                                {
                                    return false;
                                }
                            }

                        }

                        if (!result)
                        {
                            return false;
                        }
                    }
                    else {
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    message = ex.Message;
                    burningEvent.RequestLog = ex.StackTrace;
                    skmMp.SkmSystemLogSet(SkmSystemLogType.BurningEvent, burningEvent.Id, ex.StackTrace);
                    skmMp.SkmBurningEventSet(burningEvent);
                    logger.ErrorFormat("Bu.SaveEvent->guid:{0}, message:{1} -> {2}", newModel.ExternalGuid, ex.Message, ex.StackTrace);
                }
            }

            return result;
        }

        /// <summary>
        /// 單檔燒點
        /// </summary>
        /// <param name="model"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool CreateSkmBurningEvent(SkmBurningEventModel model, out string message,bool isAlibaba=false)
        {
            message = string.Empty;
            var burningEvent = SaveBurningEvent(model);
            if (!burningEvent.IsLoaded)
            {
                message = "DB save data fail.";
                return false;
            }

            burningEvent.BurningEventSid = model.BurningEventSid = string.Format("C{0}", skmMp.SkmBurningEventGetYearCountByDate(DateTime.Now).ToString().PadLeft(5, '0'));
            model.EventId = burningEvent.SkmEventId;
            logger.InfoFormat("Bu.SaveEvent->guid:{0}, event sid:{1}, event id:{2}", model.ExternalGuid, burningEvent.BurningEventSid, model.EventId);

            //若cost center 為總公司時, 拋帳店館代號為總公司(store_code=1) (理當只能選總公司一筆)
            if (model.CostCenter.FirstOrDefault(p => p.StoreCode.Equals("1")) != null)
            {
                model.CostStoreCode = "001";//歸帳館別
            }

            var sendData = GetBurningPointXml(SkmWebServiceTransCode.AddBurningPointEvent,
                GetBurningPointDetailXml(model), string.Empty, (ActiveType)model.ActiveType, Guid.Empty);
            burningEvent.SendData = HttpUtility.UrlDecode(sendData);
            logger.InfoFormat("Bu.SaveEvent->guid:{0}, sendXML:{1}", model.ExternalGuid, burningEvent.SendData);

            if (!config.EnableCallBurningWebService)
            {
                burningEvent.Status = (byte)SkmBurningEventStatus.Create;
                skmMp.SkmBurningEventSet(burningEvent);
                message = "Success for test.";
                return true;
            }

            if (model.PrizeDeal && config.SkmEnablePrizeEventTestMode)
            {
                burningEvent.Status = (byte)SkmBurningEventStatus.Create;
                skmMp.SkmBurningEventSet(burningEvent);
                message = "Success for test..";
                return true;
            }

            try
            {
                if (!isAlibaba)
                {
                    const string skmUmallCheckSuccessCode = "00";
                    const string skmUmallCheckTagName = "T3900";
                    logger.InfoFormat("Bu.SaveEvent->guid:{0}, call SKM WebService({1}).", model.ExternalGuid, cp.SkmWebserviceUrl);

                    var wsResult =
                        System.Net.WebUtility.UrlDecode(
                            (string)CommonFacade.InvokeWebService(cp.SkmWebserviceUrl, 30, cp.SkmWsNamespace,
                                cp.SkmWsClassName, cp.SkmWsMethod
                                , new object[]
                                {
                                sendData
                                }));

                    using (StringReader checkResult = new StringReader(wsResult))
                    {
                        skmMp.SkmSystemLogSet(SkmSystemLogType.BurningEvent, burningEvent.Id, HttpUtility.UrlDecode(wsResult));
                        using (XmlReader reader = XmlReader.Create(checkResult))
                        {
                            if (reader.ReadToDescendant(skmUmallCheckTagName))
                            {
                                reader.Read();
                                burningEvent.RequestLog = reader.Value;
                                burningEvent.ReceivedData = HttpUtility.UrlDecode(wsResult);
                                burningEvent.ReceivedTime = DateTime.Now;
                                var isSuccess = reader.Value == skmUmallCheckSuccessCode;
                                burningEvent.Status = isSuccess ? (byte)SkmBurningEventStatus.Create : (byte)SkmBurningEventStatus.Fail;
                                skmMp.SkmBurningEventSet(burningEvent);
                                message = isSuccess ? "Success." : "建立燒點活動錯誤代碼" + burningEvent.RequestLog;
                                return isSuccess;
                            }
                        }
                    }
                }
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                burningEvent.RequestLog = ex.StackTrace;
                skmMp.SkmSystemLogSet(SkmSystemLogType.BurningEvent, burningEvent.Id, ex.StackTrace);
                skmMp.SkmBurningEventSet(burningEvent);
                logger.ErrorFormat("Bu.SaveEvent->guid:{0}, message:{1} -> {2}", model.ExternalGuid, ex.Message, ex.StackTrace);
            }

            return false;
        }

        private static string GetSkmBurningEventId()
        {
            int eventCount = skmMp.SkmBurningEventGetMonthCountByDate(DateTime.Now) + 1;
            return string.Format("6{0}{1}", DateTime.Now.ToString("yyyyMM"), eventCount.ToString().PadLeft(5, '0'));
        }

        private static SkmBurningEvent SaveBurningEvent(SkmBurningEventModel model)
        {
            SkmBurningEvent be = new SkmBurningEvent();
            try
            {
                using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                {
                    be = skmMp.SkmBurningEventGet(model.ExternalGuid, model.DealVersion >= (int)ExternalDealVersion.SkmPay ? model.CostCenter.First().StoreCode : string.Empty);

                    if (be.IsLoaded)
                    {
                        be.ExchangeItemId = model.ExchangeItemId;
                        be.CloseProjectStatus = model.CloseProjectStatus;
                        be.BurningPoint = model.BurningPoint;
                        be.IsCostCenterHq = model.IsCostCenterHq;
                        be.Status = (byte)SkmBurningEventStatus.UpdateInit;
                    }
                    else
                    {
                        be = new SkmBurningEvent
                        {
                            ExternalGuid = model.ExternalGuid,
                            SkmEventId = GetSkmBurningEventId(),
                            ExchangeItemId = model.ExchangeItemId,
                            CreateTime = DateTime.Now,
                            Status = (byte)SkmBurningEventStatus.Init,
                            RequestLog = string.Empty,
                            SendData = string.Empty,
                            ReceivedData = string.Empty,
                            BurningPoint = model.BurningPoint,
                            CloseProjectStatus = model.CloseProjectStatus,
                            GiftWalletId = model.GiftWalletId,
                            GiftPoint = model.GiftPoint,
                            StoreCode = model.CostCenter.First().StoreCode,
                            IsCostCenterHq = model.IsCostCenterHq,
                        };
                    }

                    if (skmMp.SkmBurningEventSet(be) && model.CostCenter.Any())
                    {
                        skmMp.SkmBurningCostCenterDel(be.Id);
                        SkmBurningCostCenterCollection ccCol = new SkmBurningCostCenterCollection();
                        List<SkmBurningEventCostCenter> removeCol = new List<SkmBurningEventCostCenter>();
                        foreach (var c in model.CostCenter)
                        {
                            var codes = skmMp.SkmCostCenterInfoGetByStoreCode(c.StoreCode);
                            if (codes.Any(x => x.CostCenterCode == c.CostCenterCode))
                            {
                                var cc = new SkmBurningCostCenter
                                {
                                    BurningEventId = be.Id,
                                    StoreCode = int.Parse(c.StoreCode).ToString(),
                                    CostCenterCode = c.CostCenterCode,
                                    OrderCode = c.OrderCode
                                };
                                ccCol.Add(cc);
                            }
                            else
                            {
                                removeCol.Add(c);
                            }
                        }

                        if (removeCol.Any())
                        {
                            foreach (var r in removeCol)
                            {
                                string removeLog =
                                    string.Format("Remove CostCenter => storeCode:{0}, centerCode:{1}, orderCode:{2}",
                                        r.StoreCode, r.CostCenterCode, r.OrderCode);
                                skmMp.SkmSystemLogSet(SkmSystemLogType.BurningCostCenter, be.Id, removeLog);
                                logger.Info(removeLog);
                                model.CostCenter.Remove(r);
                            }
                        }

                        skmMp.SkmBurningCostCenterSet(ccCol);
                    }
                    ts.Complete();
                    logger.InfoFormat("Bu.SaveEvent->guid:{0}, DB Save Complate. be.id:{1}", model.ExternalGuid, be.Id);
                }
            }
            catch (Exception ex)
            {
                var st = ex.StackTrace;
                if (st.Length > 1000)
                {
                    st = st.Substring(0, 1000);
                }
                logger.ErrorFormat("Bu.SaveEvent->guid:{0}, msg:{1},-> {2}"
                    , model.ExternalGuid, ex.Message, ex.StackTrace);

                SkmLog(string.Empty, 0, SkmLogType.AddBurningEvent, string.Format("SaveBurningEvent Fail, ExternalGuid:{0}, Message:{1}", model.ExternalGuid, ex.Message), st);
                return new SkmBurningEvent();
            }

            return be;
        }

        private static string GetBurningPointXml(SkmWebServiceTransCode code, string functionXml, string supplementCode, ActiveType actionTp, Guid sellerGuid, string deviceCode = "")
        {
            var result = string.Empty;
            var isGiftPoint = actionTp == ActiveType.CashCoupon || actionTp == ActiveType.GiftCertificate;
            switch (code)
            {
                case SkmWebServiceTransCode.AddBurningPointEvent:
                    result = System.Net.WebUtility.UrlEncode(
                        string.Format(@"<Trans><T0100>0300</T0100>
                                                <T0300>{0}</T0300>
                                                <T1200>{1}</T1200>
                                                <T1300>{2}</T1300>
                                                <T5509>7</T5509>
                                                <{4}>{3}</{4}></Trans>"
                            , Helper.GetEnumDescription(code)
                            , DateTime.Now.ToString("HHmmss"), DateTime.Now.ToString("yyyyMMdd"), functionXml, isGiftPoint ? "T5579" : "T5549"));
                    break;
                case SkmWebServiceTransCode.GetBurningPointCoupon:
                    var T5509 = "7";
                    //supplementCode = deviceCode
                    var shopCodeTag = string.Empty;
                    if (sellerGuid != Guid.Empty && config.SkmEnablePrizeDealCallBurningWs)
                    {
                        shopCodeTag = GetT4200Tag(sellerGuid);
                    }
                    else
                    {
                        logger.InfoFormat("T4200.Error_3->sellerGuid Empty.");
                    }

                    result = System.Net.WebUtility.UrlEncode(
                        string.Format(@"<Trans><T0100>0300</T0100>
                                                <T0300>{0}</T0300>
                                                <T1200>{1}</T1200>
                                                <T1300>{2}</T1300>
                                                <T4100>{3}</T4100>{6}
                                                <T5509>{4}</T5509>
                                                <{7}>{5}</{7}></Trans>"
                            , Helper.GetEnumDescription(code)
                            , DateTime.Now.ToString("HHmmss"), DateTime.Now.ToString("yyyyMMdd"), supplementCode, T5509, functionXml, shopCodeTag, isGiftPoint ? "T5579" : "T5549"));
                    break;
                case SkmWebServiceTransCode.VerifyBurningCoupon:
                    var t5509 = "B";
                    //supplementCode = shopCode
                    result = System.Net.WebUtility.UrlEncode(
                        string.Format(@"<Trans><T0100>0300</T0100>
                                            <T0300>{0}</T0300>
                                            <T1200>{1}</T1200>
                                            <T1300>{2}</T1300>
                                            <T4200>{3}</T4200>
                                            <T5509>{4}</T5509>
                                            <T5549>{5}</T5549></Trans>"
                            , Helper.GetEnumDescription(code)
                            , DateTime.Now.ToString("HHmmss")
                            , DateTime.Now.ToString("yyyyMMdd")
                            , supplementCode
                            , t5509, functionXml));
                    break;
                case SkmWebServiceTransCode.SendPrizeCoupon://傳送中獎清冊
                    if (sellerGuid != Guid.Empty)
                    {
                        supplementCode = GetT4200Tag(sellerGuid, false);
                    }
                    else
                    {
                        logger.InfoFormat("T4200.Error_3->sellerGuid Empty.");
                    }

                    result = System.Net.WebUtility.UrlEncode(
                        string.Format(@"<Trans><T0100>0300</T0100>
                                            <T0300>{0}</T0300>
                                            <T1200>{1}</T1200>
                                            <T1300>{2}</T1300>
                                            <T4100>{3}</T4100>
                                            {4}
                                            <T5509>{5}</T5509>
                                            <T5549>{6}</T5549></Trans>"
                            , Helper.GetEnumDescription(code)
                            , DateTime.Now.ToString("HHmmss")
                            , DateTime.Now.ToString("yyyyMMdd")
                            , deviceCode
                            , supplementCode
                            , "7"
                            , functionXml));
                    break;
                default:
                    break;
            }

            return result;
        }

        /// <summary>
        /// 若遇到總店的seller guid，則直接使用總店的shop code
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="isParseInt">是否需要再做int轉型(已知傳送中獎清冊不用)</param>
        /// <returns></returns>
        private static string GetT4200Tag(Guid sellerGuid, bool isParseInt = true)
        {
            var shopCodeTag = string.Empty;

            //若遇到總店的seller guid，則直接使用總店的shop code
            string shoppe = string.Empty;
            if (sellerGuid == config.SkmRootSellerGuid)
            {
                shoppe = "001";
            }
            else
            {
                shoppe = skmMp.SkmShoppeGetShopCodeBySellerGuid(sellerGuid).OrderBy(x => x).FirstOrDefault();
            }

            if (!string.IsNullOrEmpty(shoppe))
            {
                int sc;
                if (int.TryParse(shoppe, out sc))
                {
                    shopCodeTag = string.Format("<T4200>{0}</T4200>", shoppe);
                }
                else
                {
                    logger.InfoFormat("T4200.Error_1->({0}), {1}", shoppe, sellerGuid);
                }
            }
            else
            {
                logger.InfoFormat("T4200.Error_2->({0}), {1}", shoppe ?? string.Empty, sellerGuid);
            }

            return shopCodeTag;
        }

        private static string GetCostCenterXml(List<SkmBurningEventCostCenter> data, bool isGiftPoint = false)
        {
            var result = string.Empty;
            foreach (var d in data)
            {
                if (d.OrderCode.Length > 12)
                {
                    d.OrderCode = d.OrderCode.Substring(0, 12);
                }

                result += string.Format("<{3}>{0}{1}{2}</{3}>", d.StoreCode.PadLeft(4, '0'), d.CostCenterCode.PadLeft(10, '0')
                    , d.OrderCode.PadLeft(12, '0'), isGiftPoint ? "T557922" : "T554928");
            }
            return result;
        }

        private static string GetBurningPointDetailXml(SkmBurningEventModel model)
        {
            var xml = string.Empty;
            ActiveType dealType = (ActiveType)model.ActiveType;

            DateTime minDate = model.DealStartTime > model.DeliveryTimeS ? model.DeliveryTimeS : model.DealStartTime;
            DateTime maxDate = model.DealEndTime > model.DeliveryTimeE ? model.DealEndTime : model.DeliveryTimeE;

            if (config.IsEnableCostByHq) //允許傳總店拋帳
            {
                if (dealType == ActiveType.GiftCertificate ||
                    dealType == ActiveType.CashCoupon)
                {
                    xml = string.Format(@"<T557901>{0}</T557901>
                                    <T557902>{1}</T557902>
                                    <T557903>{8}</T557903>
                                    <T557904>{2}</T557904>
                                    <T557905>{2}</T557905>
                                    <T557910>{3}</T557910>
                                    <T557911>{4}</T557911>
                                    <T557919>EW70</T557919>
                                    <T557920>{5}</T557920>
                                    <T557921>{6}</T557921>{7}
                                    <T557923>{9}</T557923>
                                    <T557924>{10}</T557924>
                                    <T557925>{11}</T557925>
                                    ",
                        model.EventId,
                        model.ExchangeItemId,
                        model.EventName,
                        dealType == ActiveType.GiftCertificate ? "E" : "C",
                        model.GiftPoint * 100,
                        model.BurningPoint * 100,
                        model.GiftWalletId,
                        GetCostCenterXml(model.CostCenter, true),
                        model.ExchangeQty * 100,
                        minDate.ToString("yyyyMMdd"),
                        maxDate.ToString("yyyyMMdd"),
                        model.CloseProjectStatus ? "1" : "0");
                }
                else
                {
                    xml = string.Format(@"<T554902>{0}</T554902>
                                    <T554911>EW70</T554911>
                                    <T554912></T554912>
                                    <T554913>{1}</T554913>
                                    <T554916>{2}</T554916>
                                    <T554917>{3}</T554917>
                                    <T554918>{4}</T554918>
                                    <T554919>{5}</T554919>
                                    <T554920>{6}</T554920>
                                    <T554921>{7}</T554921>
                                    <T554922>{8}</T554922>
                                    <T554923>{9}</T554923>
                                    <T554924>{10}</T554924>
                                    <T554927>{11}</T554927>
                                    {12}
                                    <T554939>{13}</T554939>",
                        maxDate.ToString("yyyyMMdd"), //折價券有效期
                        model.EventName, //折價券名稱
                        model.EventId, //活動檔期代號
                        model.BurningEventSid, //折價券 ID
                        minDate.ToString("yyyyMMdd"), //折價券有效起始期
                        model.EventName, //活動檔期名稱
                        minDate.ToString("yyyyMMdd"), //活動有效起始日期
                        maxDate.ToString("yyyyMMdd"), //活動有效結束日期
                        model.BurningPoint * 100, //兌換折價券須扣點數 5
                        model.ExchangeItemId, //折價券兌換贈品 ID
                        model.ExchangeQty * 100, //折價券兌換贈品數量
                        model.CloseProjectStatus ? "1" : "0", //活動結案狀態
                        GetCostCenterXml(model.CostCenter), //可使用折價券之分店代號、成本中心、內部訂單編號
                        model.CostStoreCode //拋帳店館代號
                    );
                }
            }
            else
            {
                if (dealType == ActiveType.GiftCertificate ||
                    dealType == ActiveType.CashCoupon)
                {
                    xml = string.Format(@"<T557901>{0}</T557901>
                                    <T557902>{1}</T557902>
                                    <T557903>{8}</T557903>
                                    <T557904>{2}</T557904>
                                    <T557905>{2}</T557905>
                                    <T557910>{3}</T557910>
                                    <T557911>{4}</T557911>
                                    <T557919>EW70</T557919>
                                    <T557920>{5}</T557920>
                                    <T557921>{6}</T557921>{7}
                                    <T557923>{9}</T557923>
                                    <T557924>{10}</T557924>
                                    <T557925>{11}</T557925>
                                    ",
                        model.EventId,
                        model.ExchangeItemId,
                        model.EventName,
                        dealType == ActiveType.GiftCertificate ? "E" : "C",
                        model.GiftPoint * 100,
                        model.BurningPoint * 100,
                        model.GiftWalletId,
                        GetCostCenterXml(model.CostCenter, true),
                        model.ExchangeQty * 100,
                        minDate.ToString("yyyyMMdd"),
                        maxDate.ToString("yyyyMMdd"),
                        model.CloseProjectStatus ? "1" : "0");
                }
                else
                {
                    xml = string.Format(@"<T554902>{0}</T554902>
                                    <T554911>EW70</T554911>
                                    <T554912></T554912>
                                    <T554913>{1}</T554913>
                                    <T554916>{2}</T554916>
                                    <T554917>{3}</T554917>
                                    <T554918>{4}</T554918>
                                    <T554919>{5}</T554919>
                                    <T554920>{6}</T554920>
                                    <T554921>{7}</T554921>
                                    <T554922>{8}</T554922>
                                    <T554923>{9}</T554923>
                                    <T554924>{10}</T554924>
                                    <T554927>{11}</T554927>{12}",
                        maxDate.ToString("yyyyMMdd"), //折價券有效期
                        model.EventName, //折價券名稱
                        model.EventId, //活動檔期代號
                        model.BurningEventSid, //折價券 ID
                        minDate.ToString("yyyyMMdd"), //折價券有效起始期
                        model.EventName, //活動檔期名稱
                        minDate.ToString("yyyyMMdd"), //活動有效起始日期
                        maxDate.ToString("yyyyMMdd"), //活動有效結束日期
                        model.BurningPoint * 100, //兌換折價券須扣點數 5
                        model.ExchangeItemId, //折價券兌換贈品 ID
                        model.ExchangeQty * 100, //折價券兌換贈品數量
                        model.CloseProjectStatus ? "1" : "0", //活動結案狀態
                        GetCostCenterXml(model.CostCenter)); //可使用折價券之分店代號、成本中心、內部訂單編號
                }
            }

            return xml;
        }

        /// <summary>
        /// 依商品型態建立5579或5549類型的燒點資料
        /// </summary>
        /// <param name="eventSid">折價券ID</param>
        /// <param name="couponId">折價券券號</param>
        /// <param name="burningPoint"></param>
        /// <param name="exchangeItemId"></param>
        /// <param name="appCardNo"></param>
        /// <param name="eventId">活動ID</param>
        /// <param name="actType">SKM檔次型態</param>
        /// <param name="eventName">活動名稱</param>
        /// <param name="orderCreateTime">燒點log建立時間</param>
        /// <param name="giftPoint">SkmBurningEvent.GiftPoint</param>
        /// <param name="giftWalletId">SkmBurningEvent.GiftWalletId</param>
        /// <param name="costCenter">成本中心</param>
        /// <param name="deviceCode">裝置代號</param>
        /// <returns></returns>
        private static string GetBurningPointCouponXml(string eventSid, int couponId, int burningPoint, string exchangeItemId
            , string appCardNo, string eventId, ActiveType actType, string eventName, DateTime orderCreateTime, int giftPoint
            , string giftWalletId, List<SkmBurningEventCostCenter> costCenter, string deviceCode)
        {
            var xml = string.Empty;

            if (actType == ActiveType.CashCoupon || actType == ActiveType.GiftCertificate)
            {
                xml = string.Format(@"<T557901>{0}</T557901>
                                <T557902>{1}</T557902>
                                <T557904>{2}</T557904>
                                <T557905>{3}</T557905>
                                <T557906>{4}</T557906>
                                <T557909>{5}</T557909>
                                <T557910>{6}</T557910>
                                <T557911>{7}</T557911>
                                <T557917>{8}</T557917>
                                <T557919>{9}</T557919>
                                <T557920>{10}</T557920>
                                <T557921>{11}</T557921>
                                {12}
                                <T557926>{13}</T557926>
                                <T557927>{14}</T557927>
                    ", eventId, exchangeItemId, eventName, eventName, "1", orderCreateTime.ToString("yyyyMMddHHmmss")
                , actType == ActiveType.GiftCertificate ? "E" : "C", giftPoint * 100, appCardNo, "EW70"
                    , burningPoint * 100, giftWalletId, GetCostCenterXml(costCenter, true), deviceCode, DateTime.Now.ToString("HHmmss"));
            }
            else
            {
                xml = string.Format(@"<T554901>{0}</T554901>
                            <T554903>{1}</T554903>
                            <T554916>{2}</T554916>
                            <T554917>{3}</T554917>
                            <T554922>{4}</T554922>
                            <T554923>{5}</T554923>
                            <T554929>{6}</T554929>
                            <T554930>{7}</T554930>", couponId, "1", eventId, eventSid, burningPoint, exchangeItemId, appCardNo, DateTime.Now.ToString("yyyyMMdd"));
            }

            return xml;
        }

        /// <summary>
        /// 回傳SKM發票交易代碼 POS 機碼 ex:400123-140105 0728 前六碼 (-)前
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private static string GetSkmTransIdTmCode(string code)
        {
            var transIdArray = code.Split('-');
            if (transIdArray.Length > 0)
            {
                return transIdArray[0];
            }
            return string.Empty;
        }

        /// <summary>
        /// 燒點核銷後回傳SKM
        /// 554904 核銷張數 1張=100, 10張=1000
        /// </summary>
        /// <param name="reply"></param>
        /// <param name="isCancelVerify">是否取回反核銷XML</param>
        /// <returns></returns>
        private static string GetBurningVerifyCouponXml(SkmVerifyReply reply, bool isCancelVerify = false)
        {
            var shopCode = reply.ShopCode.Trim().Length == 3 ? string.Format("0{0}", reply.ShopCode) : reply.ShopCode;

            var tmNo = GetSkmTransIdTmCode(reply.SkmTransId);
            var tmSn = reply.SkmReplySn;

            var xml = string.Format(@"<T554901>{0}</T554901>
                            <T554904>100</T554904>
                            <T554916>{1}</T554916>
                            <T554917>{2}</T554917>
                            <T554928>{3}</T554928>
                            <T554931>{4}</T554931>
                            <T554932>{5}</T554932>
                            <T554933>{6}</T554933>
                            <T554934>{7}</T554934>
                            <T554935>{8}</T554935>"
                        , reply.CouponId, reply.SkmEventId, reply.BurningEventSid, shopCode
                        , isCancelVerify ? DateTime.Now.ToString("yyyyMMdd") : reply.VerifyDate.ToString("yyyyMMdd")
                        , isCancelVerify ? "9" : reply.VerifySuccess ? "0" : "1", reply.DiscountType, tmNo, tmSn);

            return xml;
        }

        /// <summary>
        /// 傳送中獎名單
        /// </summary>
        /// <param name="couponId"></param>
        /// <param name="skmEventId"></param>
        /// <param name="burningEventSid"></param>
        /// <param name="burningPoint"></param>
        /// <param name="exchangeItemId"></param>
        /// <param name="appCardNo"></param>
        /// <returns></returns>
        private static string GetSendPrizeCouponXml(int couponId, string skmEventId, string burningEventSid,
            int burningPoint, string exchangeItemId, string appCardNo, string shopCode, string costCode, string orderCode)
        {
            var xml = string.Format(@"<T554901>{0}</T554901>
                            <T554903>1</T554903>
                            <T554916>{1}</T554916>
                            <T554917>{2}</T554917>
                            <T554922>{3}</T554922>
                            <T554923>{4}</T554923>
                            <T554928>{5}</T554928>
                            <T554929>{6}</T554929>
                            <T554930>{7}</T554930>"
                        , couponId, skmEventId, burningEventSid, burningPoint, exchangeItemId
                        , shopCode.PadLeft(4, '0') + costCode.PadLeft(10, '0') + orderCode.PadLeft(12, '0')
                        , appCardNo, DateTime.Now.ToString("yyyyMMdd"));

            return xml;
        }

        public static void SkmVerifyReplyRetry(DateTime start, DateTime end)
        {
            if (!config.EnableCallBurningWebService || !config.EnableSkmBurning)
            {
                return;
            }

            var retryCol = skmMp.SkmVerifyReplyGetRetry(start, end);
            foreach (var r in retryCol)
            {
                SendVerifyReply(r);
            }
        }

        public static SkmVerifyReply GetSkmVerifyReply(Guid trustId, SkmVerifyLog log, bool createNew = false)
        {
            if (config.EnableCallBurningWebService && config.EnableSkmBurning)
            {
                var reply = skmMp.SkmVerifyReplyGet(trustId);
                if (reply.IsLoaded)
                {
                    return reply;
                }

                if (createNew)
                {
                    reply.TrustId = trustId;
                    reply.SkmTransId = log.SkmTransId;
                    reply.ShopCode = log.ShopeCode;
                    reply.BrandCounterCode = log.BrandCounterCode;
                    reply.CreateTime = reply.ModifyTime = log.CreateTime;
                    reply.ReplyStatus = (byte)SkmVerifyReplyStatus.Init;
                    reply.ReplyCount = 0;
                    reply.ReplyMsg = string.Empty;
                    reply.VerifySuccess = false;
                    reply.CouponId = 0;
                    reply.BurningEventSid = string.Empty;
                    reply.SkmEventId = string.Empty;
                    reply.DiscountType = 0;
                    reply.IsCancel = false;
                    reply.CancelSendData = string.Empty;
                    reply.SkmReplySn = string.Empty;
                    reply.SkmTmCode = string.Empty;
                    return reply;
                }
            }

            return new SkmVerifyReply();
        }

        public static SkmVerifyReply SetSkmVerifyReply(SkmVerifyReply reply)
        {
            var ct = mp.CashTrustLogGet(reply.TrustId);
            var be = GetBurningEventByBid(ct.BusinessHourGuid ?? Guid.Empty);

            if (!be.IsLoaded || be.BurningPoint <= 0)
            {
                return new SkmVerifyReply();
            }

            var mem = skmMp.MemberGetByUserId(ct.UserId);
            reply.CouponId = ct.CouponId ?? 0;
            reply.BurningEventSid = be.BurningEventSid;
            reply.DiscountType = GetQrCodeDiscountTypeVal(ct.BusinessHourGuid ?? Guid.Empty);
            reply.VerifyDate = ct.UsageVerifiedTime ?? DateTime.Now;
            reply.SkmToken = mem.SkmToken;
            reply.ModifyTime = DateTime.Now;
            reply.SkmEventId = be.SkmEventId;
            return skmMp.SkmVerifyReplySet(reply);
        }

        public static List<SkmBurningEventCostCenter> GetCostCenterByBurningEventId(int eventId)
        {
            var result = new List<SkmBurningEventCostCenter>();
            var cc = skmMp.SkmBurningCostCenterGet(eventId);
            foreach (var c in cc)
            {
                result.Add(new SkmBurningEventCostCenter
                {
                    StoreCode = c.StoreCode,
                    CostCenterCode = c.CostCenterCode,
                    OrderCode = c.OrderCode
                });
            }

            return result;
        }

        #endregion 燒點

        #region PrizeDraw 抽獎活動相關

        public static ZeroPayResult MakePrizeDrawOrder(Guid bid, string userName, ApiUserDeviceType deviceType, int drawRecordId = 0, bool passPreventingOversell = false, int couponId = 0)
        {
            PaymentDTO paymentDto = new PaymentDTO
            {
                BusinessHourGuid = bid,
                DeliveryInfo = new PponDeliveryInfo
                {
                    DeliveryType = DeliveryType.ToShop,
                    InvoiceType = "2",
                    IsEinvoice = true,
                    MemberDeliveryID = -1,
                    PayType = Core.PaymentType.Creditcard,
                    Quantity = 1,
                    SellerType = DepartmentTypes.Ppon
                },
                ItemOptions = "|#|1",
                Quantity = 1,
            };

            var psList = pp.PponStoreGetListByBusinessHourGuid(paymentDto.BusinessHourGuid).ToList();
            var ps = psList.OrderBy(x => x.SortOrder).First();
            paymentDto.DeliveryInfo.StoreGuid = ps.StoreGuid;
            paymentDto.SelectedStoreGuid = ps.StoreGuid;

            ZeroPayResult payResult = new ZeroPayResult();
            try
            {
                payResult = PaymentFacade.ZeroPayMakeOrder(paymentDto, userName,
               deviceType, true, drawRecordId, passPreventingOversell, true);
            }
            catch (Exception e)
            {
                logger.Error(string.Format("MakePrizeDrawOrder.ZeroPayMakeOrder. Para={0} . Exceptio{1}", new
                {
                    paymentDto = paymentDto,
                    userName = userName,
                    deviceType = deviceType,
                    drawRecordId = drawRecordId,
                    passPreventingOversell = passPreventingOversell
                }, e));
                SystemFacade.SetApiLog("MakePrizeDrawOrder.ZeroPayMakeOrder", userName, new
                {
                    paymentDto = paymentDto,
                    userName = userName,
                    deviceType = deviceType,
                    drawRecordId = drawRecordId,
                    passPreventingOversell = passPreventingOversell
                }, e);
            }

            return payResult;
        }

        /// <summary>
        /// 取得分店下的所有館別下拉
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GetSkmSellerDpList()
        {
            var sellers = sp.SellerGetSkmParentList();
            return sellers.ToDictionary(p => p.Guid.ToString(), p => p.SellerName);
        }

        public static Dictionary<string, string> GetSkmStoreDpList()
        {
            var allStores = sp.SkmShoppeCollectionGetStore();
            return allStores.ToDictionary(p => p.SellerGuid.ToString() + "_" + p.ShopCode, p => p.ShopName);
        }

        /// <summary>
        /// SKM 抽獎
        /// </summary>
        /// <param name="items">還有數量的獎品+銘謝惠顧</param>
        /// <returns>抽中的項目</returns>
        public static PrizeDrawBoxItem GetPrizeDrawHitBid(List<PrizeDrawItem> items)
        {
            #region 2018年一月的紅包邏輯
            /*
            //宣告抽獎箱
            List<PrizeDrawBoxItem> prizeBox = new List<PrizeDrawBoxItem>();
            foreach (var pi in items.Where(item => !item.CanDrawOut))
            {
                //依各獎項機率放入抽獎球 (機率到小數點第三位)
                int hitMax = (int)(pi.WinningRate * 1000);
                for (int i = 0; i < hitMax; i++)
                {
                    prizeBox.Add(new PrizeDrawBoxItem
                    {
                        Bid = pi.ExternalDealId ?? Guid.Empty,
                        ItemId = pi.Id,
                        EventId = pi.PrizeDrawEventId,
                        RandomVal = new Random(Guid.NewGuid().GetHashCode()).Next(), //球的隨機排序值
                        IsThank = pi.IsThanks
                    });
                }
            }

            //抽獎箱不滿10萬顆球時補銘謝惠顧
            if (prizeBox.Count < 100000)
            {
                var thankYouPatronage = items.FirstOrDefault(x => x.IsThanks);
                if (thankYouPatronage != null)
                {
                    var temp = 100000 - prizeBox.Count; //補銘謝惠顧數量
                    for (int i = 0; i < temp; i++)
                    {
                        prizeBox.Add(new PrizeDrawBoxItem
                        {
                            Bid = thankYouPatronage.ExternalDealId ?? Guid.Empty,
                            ItemId = thankYouPatronage.Id,
                            EventId = thankYouPatronage.PrizeDrawEventId,
                            RandomVal = new Random(Guid.NewGuid().GetHashCode()).Next(), //球的隨機排序值
                            IsThank = thankYouPatronage.IsThanks
                        });
                    }
                }
            }
            
            //抽獎箱中的球先依隨機排序值由小到大排序
            prizeBox = prizeBox.OrderBy(x => x.RandomVal).ToList();
            //從 0~99999 (10萬) 隨機選一個數字
            int randomIndex = new Random().Next(0, 99999);
            //從抽獎箱中回傳第 randomIndex 個球
            PrizeDrawBoxItem returnValue = prizeBox[randomIndex];
            returnValue.RandomIndex = randomIndex;
            return returnValue;
            */
            #endregion

            #region 2018年12月的紅包邏輯

            //宣告抽獎箱
            List<PrizeDrawBoxItem> prizeBox = new List<PrizeDrawBoxItem>();
            Dictionary<PrizeDrawItem, int> rateList = new Dictionary<PrizeDrawItem, int>();
            int sumRate = 0;
            //設定獎品的機率
            foreach (var item in items.Where(item => item.CanDrawOut && !item.IsThanks).GroupBy(item => item.ExternalDealId))
            {
                //依各獎項機率放入抽獎球 (機率到小數點第三位)
                int hitMax = (int)(item.First().WinningRate * 1000);
                rateList.Add(item.First(), sumRate + hitMax);
                sumRate += hitMax;
            }

            //原中獎率
            decimal origionWinningRate = items.Sum(item => item.WinningRate * 1000);
            //現在中獎率
            decimal nowWinningRate = items.Where(item => item.CanDrawOut).Sum(item => item.WinningRate * 1000);
            //調整後總數量(現在中獎率/原中獎率 * 原總機率(100) * 機率到小數點第三位(1000))
            int maxCount = 1;
            if (nowWinningRate > 0)
            {
                maxCount = (int)Math.Round(nowWinningRate / origionWinningRate * 100 * 1000);
            }
            //設定銘謝惠顧的機率
            rateList.Add(items.First(item => item.CanDrawOut && item.IsThanks), maxCount);
            //抽獎
            int randomIndex = new Random(Guid.NewGuid().GetHashCode()).Next(0, maxCount - 1);
            logger.InfoFormat("GetPrizeDrawHitBid maxCount{0} randomIndex{1}", maxCount, randomIndex);
            PrizeDrawItem prizeDrawOutItem = rateList.Where(item => item.Value >= randomIndex).OrderBy(item => item.Value).First().Key;
            return new PrizeDrawBoxItem
            {
                Bid = prizeDrawOutItem.ExternalDealId,
                ItemId = prizeDrawOutItem.Id,
                EventId = prizeDrawOutItem.PrizeDrawEventId,
                RandomVal = randomIndex,
                RandomIndex = randomIndex,
                IsThank = prizeDrawOutItem.IsThanks
            };
            #endregion

        }

        public static bool PrizeDraw(int eventId, PrizeDrawBoxItem prizeDrawBoxItem, string userName, int userId, ApiUserDeviceType deviceType, Guid drawId, out PrizeDrawRecord record)
        {
            int index = 0;
            prizeDrawBoxItem.RandomIndex = index;
            logger.InfoFormat("GetPrizeDrawHitBid->{0}", prizeDrawBoxItem.ItemId);
            Coupon coupon = new Coupon();
            record = new PrizeDrawRecord
            {
                Bid = prizeDrawBoxItem.Bid,
                UserId = userId,
                DrawId = drawId,
                Status = (byte)PrizeDrawRecordStatus.Init,
                EventId = eventId,
                ItemId = prizeDrawBoxItem.ItemId,
                Qty = 1,
                CreateTime = DateTime.Now,
                RandomOrderVal = prizeDrawBoxItem.RandomVal,
                RandomIndexVal = prizeDrawBoxItem.RandomIndex,
            };

            if (prizeDrawBoxItem.IsThank)
            {
                logger.InfoFormat("passPreventingOversell->{0}", prizeDrawBoxItem.ItemId);
            }

            if (!cep.InsertPrizeDrawRecord(record))
            {
                logger.InfoFormat("InsertPrizeDrawRecord->fail");
                return false;
            }

            logger.InfoFormat("InsertPrizeDrawRecord->Success.");

            return true;
        }

        /// <summary>
        /// 驗證參與資格 參加次數上限
        /// </summary>
        /// <param name="prizeEvent"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static ApiResultCode VerifyPrizeJoin(PrizeDrawEvent prizeEvent, int userId)
        {
            //每日
            if (prizeEvent.DailyLimit != 0)
            {
                var records = cep.GetPrizeDrawRecordByUserId(prizeEvent.Id, userId, PrizeDrawRecordStatus.PrizeDrawCompleted, true);
                if (records.Count() >= prizeEvent.DailyLimit)
                {
                    return ApiResultCode.DrawEventOverDailyLimit;
                }

            }

            if (prizeEvent.TotalLimit != 0) //總參加次數
            {
                var records = cep.GetPrizeDrawRecordByUserId(prizeEvent.Id, userId, PrizeDrawRecordStatus.PrizeDrawCompleted, false);
                if (records.Count() >= prizeEvent.TotalLimit)
                {
                    return ApiResultCode.DrawEventOverTotailLimit;
                }
            }
            return ApiResultCode.Success;
        }


        public static int GetPrizeRemainQty(int dailyLimit, int totalLimit, List<ViewPrizeDrawRecord> drawRecords)
        {
            var remailQty = 0;
            if (dailyLimit != 0)
            {
                var st = DateTime.Now.Date;
                var et = DateTime.Now.AddDays(1).Date;
                //每日總抽獎數
                remailQty = dailyLimit - drawRecords.Count(r => r.CreateTime >= st && r.CreateTime < et);
            }
            else
            {
                remailQty = totalLimit - drawRecords.Count();
            }
            return remailQty > 0 ? remailQty : 0;
        }
        /// <summary>
        /// 驗證獎項
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="prizeItems"></param>
        /// <returns></returns>
        public static ApiResultCode VerifyPrizeDraw(int eventId, List<PrizeDrawItem> prizeItems)
        {
            if (!prizeItems.Any())
            {
                return ApiResultCode.DrawEventError;
            }

            //至少一則銘謝惠顧
            if (prizeItems.Count(x => x.IsThanks) == 0)
            {
                return ApiResultCode.DrawEventError;
            }

            List<int> recordItemIds = cep.GetPrizeDrawRecordItemIds(eventId, PrizeDrawRecordStatus.PrizeDrawCompleted);
            //將抽完的取出
            //if (prizeRecords == null)
            //{
            //    prizeRecords = cep.GetPrizeDrawRecordByEventId(eventId, PrizeDrawRecordStatus.PrizeDrawCompleted);
            //}
            #region 2018年一月的紅包邏輯

            var drawOut = new List<PrizeDrawItem>();
            foreach (var item in prizeItems)
            {
                if (recordItemIds.Count(x => x == item.Id) >= item.Qty)
                {
                    logger.InfoFormat("DrawOutItem->{0}", item.Id);
                    drawOut.Add(item);
                }
            }
            List<PrizeDrawItem> newPrizeItems = prizeItems.Except(drawOut).ToList();
            //扣除已抽完的獎項只剩一個 &而且是銘謝惠顧　則代表獎項抽完了
            if (newPrizeItems.Count == 1 && newPrizeItems.First().IsThanks)
            {
                logger.InfoFormat("DrawOutItem->DrawEventDrawOut");
                return ApiResultCode.DrawEventDrawOut;
            }

            #endregion

            #region 2018年12月的紅包邏輯

            foreach (var item in prizeItems)
            {
                int maxQty = cep.GetCanDrawOutItemCountsByItemId(item.Id);
                int nowPrizeRecordCount = recordItemIds.Count(x => x == item.Id);
                //銘謝惠顧永遠都可抽獎不會過期也不會限量 + 判斷可不可以抽獎
                if (item.IsThanks || nowPrizeRecordCount < maxQty)
                {
                    logger.InfoFormat("DrawOutItem->{0}", item.ExternalDealId);
                    item.CanDrawOut = true;
                }
            }
            #endregion

            //獎項與抽完的集合取差集
            logger.InfoFormat("DrawOutItem->Json:{0}", new JsonSerializer().Serialize(prizeItems.Where(item => !item.CanDrawOut)));
            logger.InfoFormat("newPrizeItems->Json:{0}", new JsonSerializer().Serialize(prizeItems.Where(item => item.CanDrawOut)));

            //如果找不到銘謝惠顧以外的獎項　則代表獎項抽完了
            //if (!prizeItems.Any(item => item.CanDrawOut && !item.IsThanks))
            //{
            //    logger.InfoFormat("DrawOutItem->DrawEventDrawOut");
            //    return ApiResultCode.DrawEventDrawOut;
            //}

            return ApiResultCode.Success;
        }

        /// <summary>
        /// 設定抽獎活動階段資料清單
        /// </summary>
        /// <param name="exernalDelaGuid">檔期Guid</param>
        public static bool SetPrizeDrawItemPeriod(Guid exernalDelaGuid)
        {
            PrizeDrawItem prizeDrawItem = cep.GetPrizeDrawItem(exernalDelaGuid);
            if (prizeDrawItem != null && prizeDrawItem.Id > 0)
            {
                cep.DeletePrizeDrawItemPeriods(prizeDrawItem.Id);
                PrizeDrawEvent prizeDrawEvent = cep.GetPrizeDrawEventByEventId(prizeDrawItem.PrizeDrawEventId);
                List<PrizeDrawItemPeriod> prizeDrawItemPeriods = GeneragetPrizeDrawItemPeriods(prizeDrawEvent, new List<PrizeDrawItem>() { prizeDrawItem });
                return cep.InsertPrizeDrawItemPeriods(prizeDrawItemPeriods);
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 設定抽獎活動階段資料清單
        /// </summary>
        /// <param name="prizeDrawEvent">抽獎活動資料</param>
        /// <param name="prizeDrawItems">獎項資料</param>
        public static bool SetPrizeDrawItemPeriod(PrizeDrawEvent prizeDrawEvent, List<PrizeDrawItem> prizeDrawItems)
        {
            cep.DeletePrizeDrawItemPeriods(prizeDrawItems.Select(item => item.Id).ToList());
            List<PrizeDrawItemPeriod> prizeDrawItemPeriods = GeneragetPrizeDrawItemPeriods(prizeDrawEvent, prizeDrawItems);
            return cep.InsertPrizeDrawItemPeriods(prizeDrawItemPeriods);
        }

        /// <summary>
        /// 依抽獎活動資訊取得獎項清單
        /// </summary>
        /// <param name="prizeDrawEventId">抽獎活動編號</param>
        /// <returns>獎項清單</returns>
        public static PrizeDrawEventModel GetGetPrizeDrawEvent(int prizeDrawEventId)
        {
            PrizeDrawEventModel returnValue = cep.GetPrizeDrawEvent(prizeDrawEventId);//event
            returnValue.StartDate = returnValue.SD.ToString("yyyy/MM/dd");
            returnValue.EndDate = returnValue.ED.ToString("yyyy/MM/dd");
            returnValue.StartTime = returnValue.SD.ToString("HH:mm");
            returnValue.EndTime = returnValue.ED.ToString("HH:mm");
            returnValue.PreviewUrl = config.SSLSiteUrl + "/skmevent/PrizeDrawEventPreview?token=" + returnValue.Token;
            returnValue.EventUrl = config.SSLSiteUrl + "/skmevent/PrizeDrawEvent?token=" + returnValue.Token;
            returnValue.DeepLink = string.Format(SkmDeepLinkFormat.PrizeDrawEvent, returnValue.Token);
            List<PrizeDrawEditItemModel> prizeDrawEditItems = cep.GetPrizeDrawItemList(prizeDrawEventId);
            List<IGrouping<int, PrizeDrawRecord>> prizeDrawRecords = cep.GetPrizeDrawRecordByEventId(prizeDrawEventId, PrizeDrawRecordStatus.PrizeDrawCompleted).GroupBy(item => item.ItemId).ToList();
            List<PrizeDrawItemPeriod> prizeDrawItemPeriods = cep.GetPrizeDrawItemPeriodByEventId(prizeDrawEventId);
            prizeDrawEditItems.ForEach((item =>
            {
                item.DrawOutCount = cep.GetPrizeDrawRecordCount(prizeDrawEventId, item.Id, PrizeDrawRecordStatus.PrizeDrawCompleted);
                List<PrizeDrawItemPeriod> tempPrizeDrawItemPeriods = prizeDrawItemPeriods.Where(period => period.PrizeDrawItemId == item.Id && DateTime.Now >= period.StartDate).ToList();
                if (tempPrizeDrawItemPeriods.Count <= 0) return;
                PrizeDrawItemPeriod nowPrizeDrawItemPeriod = tempPrizeDrawItemPeriods.OrderBy(period => period.StartDate).Last();
                IGrouping<int, PrizeDrawRecord> prizeDrawRecord = prizeDrawRecords.Where(records => records.Key == item.Id).FirstOrDefault();
                int usedPrizeCount = 0;
                if (prizeDrawRecord != null && prizeDrawRecord.Any())
                {
                    usedPrizeCount = prizeDrawRecord.Count();
                }
                int canDrawOutCount = tempPrizeDrawItemPeriods.Sum(period => period.Quantity);
                //int safetyStock = (int)Math.Ceiling((decimal)nowPrizeDrawItemPeriod.Quantity / 100 * config.SKMPrizeItemSafetyStock);

                //剩餘可抽的數量小於安全庫存量
                //if (canDrawOutCount - usedPrizeCount <= safetyStock)
                //{
                string memoFormat = "({0}~{1})本階段剩餘數量:{2}";
                item.Memo = string.Format(memoFormat,
                    nowPrizeDrawItemPeriod.StartDate.ToString("yyyy/MM/dd HH:mm"),
                    nowPrizeDrawItemPeriod.StartDate.AddHours(returnValue.PeriodHour).ToString("yyyy/MM/dd HH:mm"),
                    canDrawOutCount - usedPrizeCount);
                //}
            }));
            //獎項
            returnValue.PrizeDrawItemList = prizeDrawEditItems;
            returnValue.EventPrize = returnValue.PrizeDrawItemList.Where(p => !p.IsThanksFlag && p.IsUse).Sum(p => p.Rate);
            return returnValue;
        }

        /// <summary>
        /// 產生抽獎活動階段資料清單
        /// </summary>
        /// <param name="prizeDrawEvent">抽獎活動資料</param>
        /// <param name="prizeDrawItems">獎項資料</param>
        /// <returns>活動階段資料清單</returns>
        private static List<PrizeDrawItemPeriod> GeneragetPrizeDrawItemPeriods(PrizeDrawEvent prizeDrawEvent, List<PrizeDrawItem> prizeDrawItems)
        {
            List<PrizeDrawItemPeriod> returnValue = new List<PrizeDrawItemPeriod>();
            DateTime startDate = prizeDrawEvent.StartDate;
            List<PrizeDrawRecord> prizeDrawRecords = cep.GetPrizeDrawRecordByEventId(prizeDrawEvent.Id, PrizeDrawRecordStatus.PrizeDrawCompleted);
            int periodCount = 1;
            if (prizeDrawEvent.PeriodHour != 0)
                periodCount = (int)Math.Floor((decimal)((prizeDrawEvent.EndDate - prizeDrawEvent.StartDate).TotalHours) / prizeDrawEvent.PeriodHour) + 1;
            for (int i = 0; i < periodCount; i++)
            {
                prizeDrawItems.ForEach((item) =>
                {
                    decimal usedPrizeCount = prizeDrawRecords.Where(record => record.ItemId == item.Id).Count();
                    decimal quantityPrePeriod = (item.Qty - usedPrizeCount) / periodCount;
                    decimal quantity = (int)(Math.Ceiling(quantityPrePeriod * (i + 1)) - Math.Ceiling(quantityPrePeriod * (i)));
                    //把已經用過的放到第一階段
                    if (i == 0) quantity = quantity + usedPrizeCount;
                    returnValue.Add(
                        new PrizeDrawItemPeriod()
                        {
                            PrizeDrawItemId = item.Id,
                            StartDate = startDate,
                            EndDate = prizeDrawEvent.EndDate,
                            Quantity = (int)quantity,
                            IsActive = true,
                            CreateDate = DateTime.Now,
                            ModifyDate = DateTime.Now
                        }
                    );
                }
                );
                startDate = startDate.AddHours(prizeDrawEvent.PeriodHour);
            }
            return returnValue;
        }
        /// <summary>
        /// 依抽獎活動編號取得每期間隔小時
        /// </summary>
        /// <param name="prizeDrawEventId">抽獎活動編號</param>
        /// <returns>每期間隔小時</returns>
        public static int GetGetPrizeDrawItemPeriodHour(int prizeDrawEventId)
        {
            int returnVaule = 0;
            List<PrizeDrawItemPeriod> prizeDrawItemPeriods = cep.GetPrizeDrawItemPeriodByEventId(prizeDrawEventId);
            if (prizeDrawItemPeriods.Count > 0)
            {
                var groupPrizeDrawItemPeriods = prizeDrawItemPeriods.GroupBy(item => item.PrizeDrawItemId);
                if (groupPrizeDrawItemPeriods.First().Count() > 1)
                {
                    List<PrizeDrawItemPeriod> tempPrizeDrawItemPeriods = groupPrizeDrawItemPeriods.First().ToList();
                    returnVaule = (int)(tempPrizeDrawItemPeriods[1].StartDate - tempPrizeDrawItemPeriods[0].StartDate).TotalHours;
                }
            }
            return returnVaule;
        }

        /// <summary>
        /// 設定獎品是否使用
        /// </summary>
        /// <param name="id">獎品編號</param>
        /// <returns>是否設定成功</returns>
        public static PrizeDrawItem SetPrizeItemUseOrNot(int id)
        {
            PrizeDrawItem prizeDrawItem = cep.GetPrizeDrawItem(id);
            if (prizeDrawItem.Id > 0)
            {
                prizeDrawItem.IsUse = !prizeDrawItem.IsUse;
                cep.UpdatePrizeDrawItemList(new List<PrizeDrawItem>() { prizeDrawItem });
            }
            return prizeDrawItem;
        }
        #endregion PrizeDraw

        #region skmpay EC 結帳

        /// <summary>
        /// 取得SkmPay支付選項
        /// </summary>
        /// <returns></returns>
        public static GetSkmPaymentOptionResponse GetSkmPaymentOption(GetSkmPaymentOptionRequest model)
        {

            var enableInstall = false;

            if (config.EnableCheckDealInstallProperty)
            {
                Guid bid;
                if (Guid.TryParse(model.Bid, out bid))
                {
                    var externalDeal = SkmCacheFacade.GetOnlineViewExternalDeal(bid);
                    //todo:若有機會可以修改，這裡檢查externalDeal的enableInstall屬性即可，不需call CheckEnableInstall
                    enableInstall = SkmFacade.CheckEnableInstall(externalDeal);
                }
            }
            else
            {
                enableInstall = true;
            }

            var isCoBranded = !string.IsNullOrEmpty(model.CoBranded);
            var installInfo = GetInstallmentInfo();
            var installOption = installInfo.Where(x => x.BankNo == model.BankNo && x.IsCoBranded == isCoBranded && x.Amount <= model.ItemPrice &&
                            x.BankStartDate <= DateTime.Now && x.BankEndDate >= DateTime.Now && x.InstallPeriod > 0)
                            .OrderBy(x => x.InstallPeriod).ThenBy(x => x.PayFee)
                            .Select(x => new SkmPayInstallOption()
                            {
                                Period = x.InstallPeriod ?? 0,
                                Description = string.Format("{0}期{1} 每期{2}元{3}",
                                x.InstallPeriod,
                                x.InterestRate == 0 ? "0利率" : "",
                                model.ItemPrice / (int)x.InstallPeriod,
                                x.PayFee > 0 ? " 手續費" + x.PayFee + "元" : "")
                            })
                            .ToList();

            var result = new GetSkmPaymentOptionResponse();

            if (installOption.Count > 0 && enableInstall)
            {
                result.PaymentOption = new List<int> { (int)SkmPaymentOption.LumpSum, (int)SkmPaymentOption.Redeem, (int)SkmPaymentOption.Install };
                result.InstallOption = installOption;
            }
            else
            {
                result.PaymentOption = new List<int> { (int)SkmPaymentOption.LumpSum, (int)SkmPaymentOption.Redeem };
                result.InstallOption = new List<SkmPayInstallOption>();
            }

            return result;
        }

        /// <summary>
        /// 取SKM PAY訂單編號 (17Life自用)
        /// </summary>
        /// <param name="shopCode"></param>
        /// <returns></returns>
        public static string GetSkmPayOrderNo(string shopCode)
        {
            string prefix = string.Format("{0}{1}{2}", shopCode, numberCode[DateTime.Now.Year - 2000], numberCode[DateTime.Now.Month]);
            var sn = _sysp.SerialNumberGetNext(prefix, 10000).ToString().PadLeft(7, '0');
            return string.Format("{0}{1}", prefix, sn);
        }

        /// <summary>
        ///  取SKM PAY 停車訂單編號 (17Life自用)
        /// </summary>
        /// <param name="shopCode"></param>
        /// <returns></returns>
        public static string GetSkmPayParkingOrderNo(string shopCode)
        {
            string prefix = string.Format("{0}{1}{2}{3}", shopCode, "P", numberCode[DateTime.Now.Year - 2000], numberCode[DateTime.Now.Month]);
            var sn = _sysp.SerialNumberGetNext(prefix, 10000).ToString().PadLeft(7, '0');
            return string.Format("{0}{1}", prefix, sn);
        }

        /// <summary>
        /// 取SKM PAY交易序號(傳給APPOS用)
        /// </summary>
        /// <param name="storeCode">分館代碼 ex:121,122,124</param>
        /// <returns></returns>
        public static SkmSerialNumber GetSkmTradeNo(string storeCode, string PlatFormId)
        {
            //年月日6碼+0+店代號3碼+機號6碼+序號6碼+通訊流水2碼
            //通訊流水2碼 為店號機碼有始以來交易第一筆00一路編至99，超過重回00

            var dateTimeCode = string.Format("{0}{1}", Convert.ToString(DateTime.Now.Year - 2000),
                DateTime.Now.ToString("MMdd"));
            string tmCode = string.Empty;
            switch (PlatFormId)
            {
                case "P":
                    tmCode = SkmParkingTmCode[storeCode]; //待SKM定
                    break;
                case "7":
                    tmCode = SkmTmCode[storeCode]; //待SKM定
                    break;
            }
            var storefixcode = string.Format("{0}{1}", storeCode, tmCode); //店號加機碼
            var prefixcode = string.Format("{0}0{1}", dateTimeCode, storefixcode); //日期加店號機碼

            return skmEp.GetSkmSerialNumber(prefixcode);
        }

        /// <summary>
        /// for make order wallet success but skm invoice fail.
        /// 退發票不成功的交易
        /// </summary>
        /// <param name="oldSkmTradeNo"></param>
        /// <returns></returns>
        public static string GetReturnTradeNoForNonInvoice(string oldSkmTradeNo)
        {
            var temp = string.Format("R{0}", oldSkmTradeNo);
            var lastNo = _sysp.SerialNumberGetNext(temp, 1);
            return string.Format("{0}{1}", temp, lastNo.ToString().PadLeft(3, '0'));
        }

        public static SkmSerialNumberLog AddSerialNumberLog(int skmPayOrderId, SkmSerialNumber number, SkmSerialNumberLogTradeType tradeType)
        {
            var log = new SkmSerialNumberLog
            {
                SerialNumberId = number.Id,
                TradeType = (int)tradeType,
                SkmPayOrderId = skmPayOrderId,
                TradeNo = number.GetTradeNo(),
                RequestTime = DateTime.Now,
                ResultSuccess = false,
                ResultMsg = string.Empty
            };
            skmEp.SetSkmSerialNumberLog(log);
            return log;
        }

        public static SkmSerialNumberLog CompleteSerialNumberLog(SkmSerialNumberLog log, bool resultSuccess, string resultMsg)
        {
            log.ResponseTime = DateTime.Now;
            log.ResultSuccess = resultSuccess;
            log.ResultMsg = resultMsg;
            skmEp.SetSkmSerialNumberLog(log);
            return log;
        }

        /// <summary>
        /// 儲存發票資訊
        /// </summary>
        /// <param name="skmPayInvoice">發票資訊</param>
        /// <param name="skmPayInvoiceSellDetail">發票銷售明細</param>
        /// <param name="skmPayInvoiceTenderDetails">發票付款明細</param>
        /// <returns>是否建立成功</returns>
        public static bool SetSkmPayInvoice(SkmPayInvoice skmPayInvoice, SkmPayInvoiceSellDetail skmPayInvoiceSellDetail, IEnumerable<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails)
        {
            if (!skmEp.SetSkmPayInvoice(skmPayInvoice))
            {
                return false;
            }
            skmPayInvoiceSellDetail.SkmPayInvoiceId = skmPayInvoice.Id;
            if (!skmEp.SetSkmPayInvoiceSellDetail(skmPayInvoiceSellDetail))
            {
                return false;
            }
            skmPayInvoiceTenderDetails.ForEach(item => item.SkmPayInvoiceId = skmPayInvoice.Id);
            if (!skmEp.SetSkmPayInvoiceTenderDetail(skmPayInvoiceTenderDetails))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 完成OTP訂單
        /// </summary>
        /// <param name="paymentToken">交易TOKEN</param>
        /// <param name="isFromApi">是否來自Api</param>
        /// <returns></returns>
        public static ApiResult CompleteOTPOrder(string paymentToken, bool isFromApi)
        {
            #region 初始化
            ApiResult returnValue = new ApiResult
            {
                Code = ApiResultCode.InputError,
                Message = "輸入參數錯誤",
                Data = new SkmPayMakeOrderModel() { ApiAccseeLimite = config.SkmGetPaymentResultAccseeLimite }
            };

            SkmPayInvoice skmPayInvoice = new SkmPayInvoice();
            SkmPayInvoiceSellDetail skmPayInvoiceSellDetail = new SkmPayInvoiceSellDetail();
            List<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails = new List<SkmPayInvoiceTenderDetail>();
            SkmSerialNumber skmSerialNumber = new SkmSerialNumber();
            SkmPayOrder skmPayOrder = new SkmPayOrder();
            ApiFreeDealPayReply apiFreeDealPayReply = new ApiFreeDealPayReply();
            SkmPayOtp skmPayOtp = new SkmPayOtp();
            var userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            var postRedeemPt = 0;
            #endregion

            #region 取得訂單資訊並判斷是否需要執行&是否已在執行

            skmPayOrder = skmEp.GetSkmPayOrderByPaymentToken(paymentToken);
            if (skmPayOrder == null || skmPayOrder.Id <= 0)
            {
                return returnValue;
            }

            switch (skmPayOrder.Status)
            {
                //交易已確認完成
                case (int)SkmPayOrderStatus.SentInvoice:
                    skmPayOtp = skmEp.GetSkmPayOtpBySkmPayOrderId(skmPayOrder.Id);
                    apiFreeDealPayReply = new JsonSerializer().Deserialize<ApiFreeDealPayReply>(skmPayOtp.FreeDealPayReply);
                    skmPayInvoice = skmEp.GetSkmPayInvoice(skmPayOrder.Id, SkmPayPlatFormId).FirstOrDefault(item =>
                                                  item.SellTranstype == (int)TurnCloudSellTransType.Sell &&
                                                  item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                          /*因交易成功，取成功的資料*/item.ReturnCode == "1");
                    returnValue.Code = ApiResultCode.Success;
                    returnValue.Message = "購買成功";
                    break;
                case (int)SkmPayOrderStatus.WaitingOTPFinished:
                    //這邊是最主要阻擋同時執行的問題，當isprocessing=true時，會阻擋
                    using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                    {
                        skmPayOtp = skmEp.GetSkmPayOtpBySkmPayOrderId(skmPayOrder.Id);
                        //查無資料
                        if (skmPayOtp == null || skmPayOtp.Id <= 0)
                        {
                            return returnValue;
                        }
                        //非訂單已確認完成 且 非正在確認中
                        if (skmPayOtp.IsFinished != true && skmPayOtp.IsProcessing != true)
                        {
                            //鎖定此筆資料
                            skmPayOtp.IsProcessing = true;
                            //鎖定失敗
                            if (!skmEp.SetSkmPayOtp(skmPayOtp))
                            {
                                return returnValue;
                            }
                        }
                        else
                        {
                            //別人已經在確認的話，就打回去重取
                            returnValue.Code = ApiResultCode.SkmPayWaitingOTPResult;
                            returnValue.Message = "";
                            return returnValue;
                        }
                        ts.Complete();
                    }
                    apiFreeDealPayReply = new JsonSerializer().Deserialize<ApiFreeDealPayReply>(skmPayOtp.FreeDealPayReply);
                    skmPayInvoice = skmEp.GetSkmPayInvoice(skmPayOrder.Id, SkmPayPlatFormId).FirstOrDefault(item =>
                                                  item.SellTranstype == (int)TurnCloudSellTransType.Sell &&
                                                  item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                  /*尚未跑過的資料*/string.IsNullOrEmpty(item.ReturnCode));
                    break;
                default:
                    return returnValue;
            }

            #endregion

            #region 取得訂單/發票相關資料
            //若APP訪問次數超過上限，則當作交易逾時回傳交易失敗，並交由退款排程去執行
            if (skmPayOtp.AppAccessCount > config.SkmGetPaymentResultAccseeLimite)
            {
                skmEp.SetSkmPayOrder(skmPayOrder);
                returnValue.Code = ApiResultCode.SkmPayOTPTimeout;
                returnValue.Message = "交易逾時，交易失敗。";
                return returnValue;
            }

            if (skmPayInvoice == null || skmPayInvoice.Id <= 0)
            {
                return returnValue;
            }

            skmPayInvoiceSellDetail = skmEp.GetSkmPayInvoiceSellDetailsByInvoiceId(skmPayInvoice.Id).FirstOrDefault();
            if (skmPayInvoiceSellDetail == null || skmPayInvoiceSellDetail.Id <= 0)
            {
                return returnValue;
            }
            skmPayInvoice.VipAppPass = skmPayOtp.VipAppPass;

            skmPayInvoiceTenderDetails = skmEp.GetSkmPayInvoiceTenderDetailsByInvoiceId(skmPayInvoice.Id);

            string tempSkmTradeNo = skmPayOrder.SkmTradeNo.Substring(0, skmPayOrder.SkmTradeNo.Length - 2);
            skmSerialNumber = skmEp.GetSkmSerialNumberBySkmTradNo(tempSkmTradeNo);
            if (skmSerialNumber == null || skmSerialNumber.Id <= 0)
            {
                return returnValue;
            }
            #endregion

            #region 執行取得訂單狀態&完成訂單
            if (skmPayOrder.Status == (int)SkmPayOrderStatus.WaitingOTPFinished)
            {
                //向wallet查詢付款結果
                var walletServerResponse = SkmWalletServerUtility.GetPaymentResult(new WalletAuthPaymentRequest(skmPayOrder.SkmTradeNo));
                if (walletServerResponse.Code != WalletReturnCode.WaitingOTP)
                {
                    SkmFacade.SetSkmPayOrderTransLog(skmPayInvoice.SkmPayOrderId, skmPayOrder.Status, "SkmWalletServerUtility.GetPaymentResult",
                        walletServerResponse != null, skmPayOrder.SkmTradeNo, new JsonSerializer().Serialize(walletServerResponse), userId);
                }
                switch (walletServerResponse.Code)
                {
                    case WalletReturnCode.Success:
                        //授權失敗
                        if (!walletServerResponse.Data.IsSuccess)
                        {
                            skmPayOtp.IsFinished = true;
                            skmPayOtp.IsProcessing = false;
                            returnValue.Code = ApiResultCode.SkmPayOTPError;
                            returnValue.Message = Helper.GetDescription(walletServerResponse.Code);
                            break;
                        }

                        postRedeemPt = walletServerResponse.Data.PostRedeemPt;
                        #region 發票補丁

                        //OTP交易在MakeOrder時沒有傳信用卡交易相關訊息，所以如果這邊沒有資料的話，從這邊補上去
                        if (!skmPayInvoiceTenderDetails.Any(item => item.SellTender == TurnCloudSellTenderType.SkmPay
                            || item.SellTender == TurnCloudSellTenderType.SkmPayNCCC))
                        {
                            SkmPayInvoiceTenderDetail skmPayInvoiceTenderDetail = new SkmPayInvoiceTenderDetail()
                            {
                                SkmPayInvoiceId = skmPayInvoice.Id,
                                PayOrderNo = skmPayOrder.SkmTradeNo,
                                NdType = skmPayInvoiceSellDetail.NdType,
                                SellTender = walletServerResponse.Data.GatewayBankNo == config.NationalCreditCardCenterBankNo ?
                                    TurnCloudSellTenderType.SkmPayNCCC : TurnCloudSellTenderType.SkmPay,
                                TenderPrice = walletServerResponse.Data.TradeAmount, //付款金額
                                SecNo = walletServerResponse.Data.CardNumber,
                                TxAmount = walletServerResponse.Data.TradeAmount, //信用卡付款金額
                                CardNo = walletServerResponse.Data.CardNumber,
                                UseBonus = walletServerResponse.Data.RedeemAmt > 0, //使用紅利CHH
                                CardNo2 = walletServerResponse.Data.CardHash,

                                BonusUsePoint = walletServerResponse.Data.RedeemPt, //使用紅利點數
                                BonusPoint = walletServerResponse.Data.PostRedeemPt, //紅利剩餘點數
                                BonusDisAmt = walletServerResponse.Data.RedeemAmt, //紅利折抵金額

                                Periods = walletServerResponse.Data.InstallPeriod, //分期期數
                                HandAmt = walletServerResponse.Data.InstallDownPay, //分期的頭期款金額
                                EachAmt = walletServerResponse.Data.InstallPay //分期每期金額
                            };
                            skmPayInvoiceTenderDetails.Add(skmPayInvoiceTenderDetail);
                            skmEp.SetSkmPayInvoiceTenderDetail(new List<SkmPayInvoiceTenderDetail>() { skmPayInvoiceTenderDetail });

                            //重新計算總花費，並會在SkmFacade.CreateInvoice()中更新到DB
                            skmPayInvoice.TotalSaleAmt = skmPayInvoiceTenderDetails.Sum(item => item.TenderPrice);
                        }

                        //因為OTP交易在MakeOrder時沒有傳付款相關訊息，所以在這邊補上去
                        skmPayInvoiceSellDetail.SellAmt = walletServerResponse.Data.TradeAmount;
                        skmEp.SetSkmPayInvoiceSellDetail(skmPayInvoiceSellDetail);

                        #endregion

                        //呼叫騰雲開立發票
                        CreateInvoiceRequest createInvoiceRequest = null;
                        CreateInvoiceResponse createInvoiceResponse = null;
                        var skmPayOrderStatus = SkmFacade.CreateInvoice(skmPayInvoice, skmPayInvoiceSellDetail, skmPayInvoiceTenderDetails, skmSerialNumber,
                            out createInvoiceRequest, out createInvoiceResponse);

                        //回寫 skm pay order
                        skmPayOrder.Status = (int)skmPayOrderStatus;
                        skmPayOrder.PaymentAmount = walletServerResponse.Data.PayAmount;
                        skmPayOrder.RedeemAmount = walletServerResponse.Data.RedeemAmt;
                        skmPayOrder.RedeemPoint = walletServerResponse.Data.RedeemPt;
                        skmPayOrder.CardHash = walletServerResponse.Data.CardHash;
                        skmPayOrder.InstallPeriod = walletServerResponse.Data.InstallPeriod;
                        skmPayOrder.InstallDownPay = walletServerResponse.Data.InstallDownPay;
                        skmPayOrder.InstallEachPay = walletServerResponse.Data.InstallPay;
                        skmPayOrder.InstallFee = walletServerResponse.Data.InstallFee;
                        skmEp.SetSkmPayOrder(skmPayOrder);

                        SkmFacade.SetSkmPayOrderTransLog(skmPayInvoice.SkmPayOrderId, (int)skmPayOrderStatus, "TurnCloudServerUtility.CreateInvoice",
                            skmPayOrderStatus == SkmPayOrderStatus.SentInvoice,
                            new JsonSerializer().Serialize(createInvoiceRequest),
                            new JsonSerializer().Serialize(createInvoiceResponse),
                            skmPayOrder.UserId,
                            Helper.GetDescription(skmPayOrderStatus));

                        if (skmPayOrderStatus != SkmPayOrderStatus.SentInvoice)
                        {
                            //發票開立失敗退款
                            string responseMessage = string.Empty;
                            SkmFacade.ReturnWalletWhenBuyFail(skmPayOrder, "CompleteOTPOrder", out responseMessage);
                            returnValue.Code = ApiResultCode.CreateInvoiceFail;
                            returnValue.Message = "購買失敗:" + responseMessage;
                            skmPayOtp.ErrorMessage = returnValue.Code.ToString();
                            //最後會回壓訂單失敗 SetOrderFailed
                        }
                        else
                        {
                            //當所有交易皆成功
                            skmEp.SetSkmInAppMessage(new SkmInAppMessage
                            {
                                UserId = skmPayOrder.UserId,
                                EventType = (int)EventType.SkmPayMsg,
                                IsRead = false,
                                Subject = "【訂單通知】",
                                MsgContent = string.Format("您的訂單編號{0}已成功訂購，請記得於期限內至指定櫃位取貨!\n\r立即確認訂單>>", skmPayOrder.OrderNo),
                                MsgType = (byte)SkmInAppMessageType.MakeOrder,
                                IsDel = false
                            });

                            returnValue.Code = ApiResultCode.Success;
                            returnValue.Message = "購買成功";
                            skmPayOtp.Result = returnValue.Code.ToString();

                            //OTP授權成功、開立發票成功後，在這裡累計銷量 DealSalesInfo
                            PayEvents.OnOrdered(skmPayOrder.Bid, skmPayInvoiceSellDetail.SellQty, 0, skmPayOrder.OrderGuid);
                        }
                        skmPayOtp.IsFinished = true;
                        skmPayOtp.IsProcessing = false;
                        break;
                    case WalletReturnCode.WaitingOTP:
                        skmPayOtp.IsFinished = false;
                        skmPayOtp.IsProcessing = false;
                        returnValue.Code = ApiResultCode.SkmPayWaitingOTPResult;
                        returnValue.Message = "等待OTP驗證完成";
                        break;
                    default:
                        skmPayOtp.IsFinished = true;
                        skmPayOtp.IsProcessing = false;
                        returnValue.Code = ApiResultCode.SkmPayOTPError;
                        returnValue.Message = Helper.GetDescription(walletServerResponse.Code);
                        break;
                }

                //建立訂單失敗，將訂單更新回未完成單
                if (returnValue.Code != ApiResultCode.Success && returnValue.Code != ApiResultCode.SkmPayWaitingOTPResult)
                {
                    SetOrderFailed(skmPayOrder.OrderGuid, skmPayOrder.Bid);
                }
                //更新SkmPayOtp解除鎖定
                if (isFromApi)
                {
                    skmPayOtp.LastAppAccessTime = DateTime.Now;
                    skmPayOtp.AppAccessCount++;
                }
                else
                {
                    skmPayOtp.JobAccessCount++;
                }
                skmEp.SetSkmPayOtp(skmPayOtp);
            }
            #endregion

            #region 組合回傳的資料
            returnValue.Data = new SkmPayMakeOrderModel()
            {
                CouponId = apiFreeDealPayReply.CouponId,
                CouponCode = apiFreeDealPayReply.CouponCode,
                ThreeStageCode = apiFreeDealPayReply.ThreeStageCode,
                PromoUrl = apiFreeDealPayReply.PromoUrl,
                PromoImage = apiFreeDealPayReply.PromoImage,
                ShowCoupon = apiFreeDealPayReply.ShowCoupon,
                IsFami = apiFreeDealPayReply.IsFami,
                IsSkm = apiFreeDealPayReply.IsSkm,
                SkmQrCode = apiFreeDealPayReply.SkmQrCode,
                SkmExpireTime = apiFreeDealPayReply.SkmExpireTime,
                ProductCode = apiFreeDealPayReply.ProductCode,
                SkmAvailabilities = apiFreeDealPayReply.SkmAvailabilities,
                SpecialItemNo = apiFreeDealPayReply.SpecialItemNo,
                IsCouponDownload = apiFreeDealPayReply.IsCouponDownload,
                CouponType = apiFreeDealPayReply.CouponType,
                IsATM = apiFreeDealPayReply.IsATM,
                AtmData = apiFreeDealPayReply.AtmData,
                OrderGuid = apiFreeDealPayReply.OrderGuid,
                SequenceNumber = apiFreeDealPayReply.SequenceNumber,
                UnUsed17PayMsg = apiFreeDealPayReply.UnUsed17PayMsg,
                Used17PayMsg = apiFreeDealPayReply.Used17PayMsg,

                TransDate = skmPayInvoice.SellDay.ToString("yyyy/MM/dd HH:mm"),
                TradeAmount = skmPayInvoice.TotalAmt,

                DiscountAmount = skmPayOrder.RedeemAmount, //分期強更後可以拿掉
                CreditCardAmount = skmPayOrder.PaymentAmount,
                UsedPoint = skmPayOrder.BurningSkmPoint,
                NeedOTP = true,
                OTPUrl = skmPayOtp.OtpUrl,
                ApiAccseeLimite = config.SkmGetPaymentResultAccseeLimite,

                //分期/紅利
                RedeemAmt = skmPayOrder.RedeemAmount,
                RedeemPoint = skmPayOrder.RedeemPoint,
                PostRedeemPoint = postRedeemPt,
                InstallPeriod = skmPayOrder.InstallPeriod,
                InstallDownPay = skmPayOrder.InstallDownPay,
                InstallEachPay = skmPayOrder.InstallEachPay,
                InstallFee = skmPayOrder.InstallFee
            };
            #endregion

            SkmFacade.SetSkmPayOrderTransLog(skmPayInvoice.SkmPayOrderId, (int)skmPayOrder.Status,
                        "SkmFacade.CompleteOTPOrder",
                        (returnValue.Code == ApiResultCode.Success || returnValue.Code == ApiResultCode.SkmPayWaitingOTPResult),
                        paymentToken, new JsonSerializer().Serialize(returnValue), userId, "CompleteOTPOrder response data");
            return returnValue;
        }

        /// <summary>
        /// 阻擋同時執行的問題，當isprocessing=true時，會阻擋
        /// </summary>
        /// <returns></returns>
        public static bool SkmPayOtpLockProcessing(int orderId, out ApiResultCode code)
        {
            using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
            {
                SkmPayOtp skmPayOtp = skmEp.GetSkmPayOtpBySkmPayOrderId(orderId);
                //查無資料
                if (skmPayOtp == null || skmPayOtp.Id <= 0)
                {
                    code = ApiResultCode.InputError;
                    return false;
                }
                //非訂單已確認完成 且 非正在確認中
                if (skmPayOtp.IsFinished != true && skmPayOtp.IsProcessing != true)
                {
                    //鎖定此筆資料
                    skmPayOtp.IsProcessing = true;
                    //鎖定失敗
                    if (!skmEp.SetSkmPayOtp(skmPayOtp))
                    {
                        code = ApiResultCode.InputError;
                        return false;
                    }
                }
                else
                {
                    code = ApiResultCode.SkmPayWaitingOTPResult;
                    return false;
                }
                ts.Complete();
            }

            code = ApiResultCode.Ready;
            return true;
        }

        /// <summary>
        /// 完成停車 OTP訂單
        /// </summary>
        /// <param name="paymentToken">交易TOKEN</param>
        /// <param name="isFromApi">是否來自Api</param>
        /// <returns></returns>
        public static ApiResult CompleteParkingOTPOrder(string paymentToken, bool isFromApi)
        {
            try
            {

                #region 初始化
                ApiResult returnValue = new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "輸入參數錯誤",
                    Data = new SkmPayParkingMakeOrderModel() { ApiAccseeLimite = config.SkmGetPaymentResultAccseeLimite }
                };

                SkmPayInvoice skmPayInvoice = new SkmPayInvoice();
                SkmPayInvoiceSellDetail skmPayInvoiceSellDetail = new SkmPayInvoiceSellDetail();
                List<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails = new List<SkmPayInvoiceTenderDetail>();
                SkmSerialNumber skmSerialNumber = new SkmSerialNumber();
                SkmPayParkingOrder parkingOrder = new SkmPayParkingOrder();
                SkmPayOtp skmPayOtp = new SkmPayOtp();
                int userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);

                #endregion

                #region 取得訂單資訊並判斷是否需要執行&是否已在執行

                parkingOrder = skmEp.GetSkmParkingOrderByPaymentToken(paymentToken);
                if (parkingOrder == null || parkingOrder.Id <= 0)
                {
                    return returnValue;
                }

                SkmFacade.SetSkmPayParkingOrderTransLog(parkingOrder.Id, (int)parkingOrder.Status, "Api/Skm/CompleteParkingOrder",
                    true, paymentToken, "", userId, "");

                skmPayOtp = skmEp.GetSkmPayOtpBySkmPayOrderId(parkingOrder.Id);
                if (parkingOrder.Status == (int)SkmPayOrderStatus.SentInvoice)
                {
                    skmPayInvoice = skmEp.GetSkmPayInvoice(parkingOrder.Id, SkmPayParkingPlatFormId).FirstOrDefault(item =>
                                    item.SellTranstype == (int)TurnCloudSellTransType.Sell &&
                                    item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                    item.ReturnCode == "1");    /*因交易成功，取成功的資料*/
                    returnValue.Code = ApiResultCode.Success;
                    returnValue.Message = "購買成功";
                    //returnValue.Data 最後還要組合回傳的資料
                }
                else if (parkingOrder.Status == (int)SkmPayOrderStatus.WaitingOTPFinished)
                {
                    //這邊是最主要阻擋同時執行的問題，當isprocessing=true時，會阻擋
                    ApiResultCode errorCode;
                    if (SkmPayOtpLockProcessing(parkingOrder.Id, out errorCode) == false)
                    {
                        returnValue.Code = errorCode;
                        return returnValue;
                    }
                    skmPayInvoice = skmEp.GetSkmPayInvoice(parkingOrder.Id, SkmPayParkingPlatFormId).FirstOrDefault(item =>
                                    item.SellTranstype == (int)TurnCloudSellTransType.Sell &&
                                    item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                    string.IsNullOrEmpty(item.ReturnCode));/*尚未跑過的資料*/


                    //若APP訪問次數超過上限，則當作交易逾時回傳交易失敗，並交由退款排程去執行
                    if (skmPayOtp.AppAccessCount > config.SkmGetPaymentResultAccseeLimite)
                    {
                        skmEp.SetSkmPayParkingOrder(parkingOrder);
                        returnValue.Code = ApiResultCode.SkmPayOTPTimeout;
                        returnValue.Message = "交易逾時，交易失敗。";
                        return returnValue;
                    }

                    #region 檢查發票
                    if (skmPayInvoice == null || skmPayInvoice.Id <= 0)
                    {
                        return returnValue;
                    }
                    skmPayInvoiceSellDetail = skmEp.GetSkmPayInvoiceSellDetailsByInvoiceId(skmPayInvoice.Id).FirstOrDefault();
                    if (skmPayInvoiceSellDetail == null || skmPayInvoiceSellDetail.Id <= 0)
                    {
                        return returnValue;
                    }
                    string tempSkmTradeNo = parkingOrder.SkmTradeNo.Substring(0, parkingOrder.SkmTradeNo.Length - 2);
                    skmSerialNumber = skmEp.GetSkmSerialNumberBySkmTradNo(tempSkmTradeNo);
                    if (skmSerialNumber == null || skmSerialNumber.Id <= 0)
                    {
                        return returnValue;
                    }

                    skmPayInvoice.VipAppPass = skmPayOtp.VipAppPass;
                    skmPayInvoiceTenderDetails = skmEp.GetSkmPayInvoiceTenderDetailsByInvoiceId(skmPayInvoice.Id);

                    #endregion

                    #region 向wallet查詢付款結果 & 完成訂單(開發票)

                    //向wallet查詢付款結果 GetPaymentResult
                    var walletServerResponse = SkmWalletServerUtility.GetPaymentResult(new WalletAuthPaymentRequest(parkingOrder.SkmTradeNo));

                    SkmFacade.SetSkmPayParkingOrderTransLog(skmPayInvoice.SkmPayOrderId, (int)parkingOrder.Status,
                        "WalletServerUtility.GetPaymentResult",
                        walletServerResponse != null && (walletServerResponse.Data != null ? walletServerResponse.Data.IsSuccess : false),
                        parkingOrder.SkmTradeNo,
                        new JsonSerializer().Serialize(walletServerResponse),
                        userId,
                        "呼叫wallet查詢付款結果:" + (walletServerResponse.Data != null ? walletServerResponse.Data.IsSuccess : false));

                    switch (walletServerResponse.Code)
                    {
                        case WalletReturnCode.Success:
                            if (!walletServerResponse.Data.IsSuccess)
                            {
                                //退款
                                skmPayOtp.IsFinished = true;
                                skmPayOtp.IsProcessing = false;
                                returnValue.Code = ApiResultCode.SkmPayOTPError;
                                returnValue.Message = Helper.GetDescription(walletServerResponse.Code);
                                break;
                            }

                            //OTP交易在MakeOrder時沒有傳信用卡交易相關訊息，所以如果這邊沒有資料的話，從這邊補上去
                            if (!skmPayInvoiceTenderDetails.Any(item => item.SellTender == TurnCloudSellTenderType.SkmPay))
                            {
                                SkmPayInvoiceTenderDetail skmPayInvoiceTenderDetail = new SkmPayInvoiceTenderDetail()
                                {
                                    SkmPayInvoiceId = skmPayInvoice.Id,
                                    PayOrderNo = parkingOrder.SkmTradeNo,
                                    NdType = skmPayInvoiceSellDetail.NdType,
                                    SellTender = TurnCloudSellTenderType.SkmPay,
                                    TenderPrice = walletServerResponse.Data.TradeAmount,
                                    SecNo = walletServerResponse.Data.CardNumber,
                                    TxAmount = walletServerResponse.Data.TradeAmount,
                                    CardNo = walletServerResponse.Data.CardNumber,
                                    UseBonus = walletServerResponse.Data.RedeemAmt > 0,
                                    CardNo2 = walletServerResponse.Data.CardHash,
                                    BonusUsePoint = walletServerResponse.Data.RedeemPt,
                                    BonusPoint = walletServerResponse.Data.Point,
                                    BonusDisAmt = walletServerResponse.Data.RedeemAmt,
                                };
                                skmPayInvoiceTenderDetails.Add(skmPayInvoiceTenderDetail);
                                skmEp.SetSkmPayInvoiceTenderDetail(new List<SkmPayInvoiceTenderDetail>() { skmPayInvoiceTenderDetail });
                                //重新計算總花費，並會在SkmFacade.CreateInvoice()中更新到DB
                                skmPayInvoice.TotalSaleAmt = skmPayInvoiceTenderDetails.Sum(item => item.TenderPrice);
                            }

                            //因為OTP交易在MakeOrder時沒有傳付款相關訊息，所以在這邊補上去
                            skmPayInvoiceSellDetail.SellAmt = walletServerResponse.Data.TradeAmount;
                            skmEp.SetSkmPayInvoiceSellDetail(skmPayInvoiceSellDetail);
                            parkingOrder.WalletPaymentAmount = walletServerResponse.Data.PayAmount;
                            parkingOrder.WalletCardHash = walletServerResponse.Data.CardHash;

                            //補完發票資料後>>開發票
                            CreateInvoiceRequest createInvoiceRequest = null;
                            CreateInvoiceResponse createInvoiceResponse = null;
                            SkmPayOrderStatus skmPayOrderStatus = SkmFacade.CreateInvoice(skmPayInvoice, skmPayInvoiceSellDetail, skmPayInvoiceTenderDetails, skmSerialNumber, out createInvoiceRequest, out createInvoiceResponse);

                            //回存skm pay order狀態&memo
                            parkingOrder.Status = (int)skmPayOrderStatus;
                            parkingOrder.Memo = SkmFacade.ParkingOrderStatusDesc[skmPayOrderStatus];
                            skmEp.SetSkmPayParkingOrder(parkingOrder);

                            //add skm_pay_order_trans_log
                            SkmFacade.SetSkmPayParkingOrderTransLog(skmPayInvoice.SkmPayOrderId, (int)skmPayOrderStatus,
                                "TurnCloudServerUtility.CreateInvoice",
                                skmPayOrderStatus == SkmPayOrderStatus.SentInvoice,
                                new JsonSerializer().Serialize(createInvoiceRequest),
                                new JsonSerializer().Serialize(createInvoiceResponse),
                                parkingOrder.UserId,
                                ParkingOrderStatusDesc[skmPayOrderStatus]);

                            if (skmPayOrderStatus != SkmPayOrderStatus.SentInvoice)
                            {
                                //退款
                                string responseMessage = string.Empty;
                                SkmFacade.ReturnWalletWhenBuyFailWithParking(parkingOrder, WalletRefundFromType.FromCompleteParkingOTPOrderApi, out responseMessage);

                                returnValue.Code = ApiResultCode.CreateInvoiceFail;
                                returnValue.Message = "購買失敗";
                                skmPayOtp.ErrorMessage = Helper.GetDescription(returnValue.Code);
                            }
                            else
                            {
                                //當所有交易皆成功 不用在APP收件夾顯示
                                returnValue.Code = ApiResultCode.Success;
                                returnValue.Message = "購買成功";
                                skmPayOtp.Result = Helper.GetDescription(returnValue.Code);
                            }
                            skmPayOtp.IsFinished = true;
                            skmPayOtp.IsProcessing = false;
                            break;
                        case WalletReturnCode.WaitingOTP:
                            skmPayOtp.IsFinished = false;
                            skmPayOtp.IsProcessing = false;
                            returnValue.Code = ApiResultCode.SkmPayWaitingOTPResult;
                            returnValue.Message = Helper.GetDescription(returnValue.Code);
                            break;
                        default:
                            skmPayOtp.IsFinished = true;
                            skmPayOtp.IsProcessing = false;
                            returnValue.Code = ApiResultCode.SkmPayOTPError;
                            returnValue.Message = Helper.GetDescription(walletServerResponse.Code);
                            break;
                    }

                    //更新SkmPayOtp解除鎖定
                    if (isFromApi)
                    {
                        skmPayOtp.LastAppAccessTime = DateTime.Now;
                        skmPayOtp.AppAccessCount++;
                    }
                    else
                    {
                        skmPayOtp.JobAccessCount++;
                    }
                    skmEp.SetSkmPayOtp(skmPayOtp);
                    #endregion
                }
                else//只允許前面兩種狀況存在, 一種不是OTP且已經開發票成功, 一種是OTP完成後待開發票
                {
                    return returnValue;
                }

                #endregion

                #region 組合回傳的資料

                returnValue.Data = new SkmPayParkingMakeOrderModel()
                {
                    TransDate = skmPayInvoice.SellDay.ToString("yyyy/MM/dd HH:mm"),
                    TradeAmount = skmPayInvoice.TotalAmt,
                    CreditCardAmount = skmPayInvoiceTenderDetails.Where(item => item.SellTender == TurnCloudSellTenderType.SkmPay).Sum(item => item.TenderPrice),
                    DiscountAmount = skmPayInvoiceTenderDetails.Where(item => item.SellTender == TurnCloudSellTenderType.SkmPay).Sum(item => item.BonusDisAmt.HasValue ? item.BonusDisAmt.Value : 0),
                    NeedOTP = true,
                    OTPUrl = skmPayOtp.OtpUrl,
                    ApiAccseeLimite = config.SkmGetPaymentResultAccseeLimite
                };

                #endregion

                if (returnValue.Code == ApiResultCode.Success)
                {
                    try
                    {
                        //成功開發票則呼叫2.14.6呼叫UMALL停車繳費確認
                        ParkingPayConfirm(parkingOrder, skmPayInvoice, SkmPayParkingNoticeUmallApi.CompleteParkingOTPOrder);

                    }
                    catch (Exception ex)
                    {
                        logger.ErrorFormat("SkmFacade.ParkingPayConfirm[Pay].Exception{0}", ex.ToString());
                    }
                }
                SkmFacade.UpdateResponseTransLog(skmPayInvoice.SkmPayOrderId, (int)parkingOrder.Status,
                            "Api/Skm/CompleteParkingOrder",
                            (returnValue.Code == ApiResultCode.Success || returnValue.Code == ApiResultCode.SkmPayWaitingOTPResult),
                            new JsonSerializer().Serialize(returnValue),
                            userId,
                            Helper.GetDescription(returnValue.Code));

                return returnValue;
            }
            catch (Exception ex)
            {
                logger.Info(string.Format("CompleteParkingOTPOrder.Para={0},Exception={1}", paymentToken, ex.ToString()));
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = ex.ToString(),
                    Data = new SkmPayParkingMakeOrderModel() { ApiAccseeLimite = config.SkmGetPaymentResultAccseeLimite }
                };
            }
        }

        /// <summary>
        /// 2.14.6 銷售 (停車繳費確認)
        /// </summary>
        public static bool ParkingPayConfirm(SkmPayParkingOrder order, SkmPayInvoice invoice
            , SkmPayParkingNoticeUmallApi fromApi
            , bool isFromJob = false)
        {
            //T5571 商品資料Tag
            //T557103銷售額=T557104單價
            var productXml = string.Format(
                              @"<T557101>{0}</T557101>
                                <T557102>{1}</T557102>
                                <T557103>{2}</T557103>
                                <T557104>{3}</T557104>",
                              SkmPayParking7XBrandCounterCode,
                              "100",
                              order.OrderAmount.ToString() + "00",
                              order.OrderAmount.ToString() + "00");

            //T5508 付款資料Tag
            var payXml = string.Format(
                            @"<T550801>13045A</T550801>
                              <T550802>{0}</T550802>",
                            order.OrderAmount.ToString() + "00");

            var rrn = order.ShopCode + DateTime.Now.ToString("yyMMdd") + invoice.SellNo;
            order.ParkingRrn = rrn;
            skmEp.SetSkmPayParkingOrder(order);

            //已不用傳退貨  銷售統一用這代碼
            var processingCode = fromApi == SkmPayParkingNoticeUmallApi.RefundParkingPayment ? "727080" : "708070";

            #region sendData
            var sendData = string.Format(@"<Trans>
                                    <T0100>0200</T0100>
                                    <T0200>{0}</T0200>
                                    <T0207>{1}</T0207>
                                    <T0209>{2}</T0209>
                                    <T0210>{3}</T0210>
                                    <T0264>{4}</T0264>
                                    <T0265>{5}</T0265>
                                    <T02B0>{6}</T02B0>
                                    <T0300>{7}</T0300>
                                    <T0400>{8}</T0400>
                                    <T0460>{9}</T0460>
                                    <T1100>{10}</T1100>
                                    <T1200>{11}</T1200>
                                    <T1300>{12}</T1300>
                                    <T3700>{13}</T3700>
                                    <T4100>{14}</T4100>
                                    <T4200>{15}</T4200>
                                    <T4222>{16}</T4222>
                                    <T4223>{17}</T4223>
                                    <T5503>{18}</T5503>
                                    <T5507>{19}</T5507>
                                    <T5508>{20}</T5508>
                                    <T5509>{21}</T5509>
                                    <T5514>{22}</T5514>
                                    <T5571>{23}</T5571>
                                    <T5597>Y</T5597>
                                    <T5598>N</T5598>
                                    <T5599>N</T5599>"
                    , order.IsDiscountPoint ? order.MemberCardNo : string.Empty //若有點數折抵才需傳T0200 Tag
                    , order.ParkingTicketNo
                    , invoice.EuiVehicleNo
                    , order.ParkingToken
                    , invoice.EuiDonateNo
                    , invoice.EuiVehicleNoHidden
                    , order.ParkingLicensePictureUuid
                    , processingCode
                    , order.OrderAmount.ToString() + "00"
                    , Convert.ToInt32(order.DiscountMinutes).ToString() + "00"
                    , order.ParkingTansNo//交易序號
                    , DateTime.Now.ToString("HHmmss")   //LocalTime
                    , DateTime.Now.ToString("yyyyMMdd") //LocalDate
                    , rrn
                    , order.PosId
                    , order.ShopCode
                    , SkmPayParking6XBrandCounterCode
                    , order.ParkingAreaNo
                    , SkmPayParking7XBrandCounterCode
                    , invoice.SellInvoice
                    , payXml
                    , SkmPayParkingPlatFormId
                    , DateTime.Now.ToString("yyyyMMdd") //營業日
                    , productXml);

            //退貨須加上的Tag
            if (fromApi == SkmPayParkingNoticeUmallApi.RefundParkingPayment)
            {
                sendData += string.Format(
                            @"<T5581>{0}</T5581>
                              <T5582>{1}</T5582>
                              <T5583>{2}</T5583>"
                            , invoice.SellOrgPosid                 //原始交易機號
                            , invoice.SellOrgNo                    //原始交易序號
                            , invoice.SellOrgDay.Value.ToString("yyyyMMdd") //原始交易日期
                            );
            }

            sendData += "</Trans>";

            //order.ParkingTansNo=invoice.sell_no==自訂6碼交易序號==T1100
            #endregion

            string errorMsg = string.Empty;
            string wsResult = string.Empty;
            const string skmUmallCheckSuccessCode = "00";
            const string skmUmallCheckTagName = "T3900";
            bool isSuccess = false;
            try
            {
                wsResult = System.Net.WebUtility.UrlDecode(
                            (string)CommonFacade.InvokeWebService(cp.SkmWebserviceUrl, 30, cp.SkmWsNamespace,
                            cp.SkmWsClassName, cp.SkmWsMethod
                            , new object[]
                            {
                                System.Net.WebUtility.UrlEncode(sendData)
                            }));

                using (StringReader checkResult = new StringReader(wsResult))
                {
                    using (XmlReader reader = XmlReader.Create(checkResult))
                    {
                        if (reader.ReadToDescendant(skmUmallCheckTagName))
                        {
                            reader.Read();
                            isSuccess = reader.Value == skmUmallCheckSuccessCode;
                            if (isSuccess == false)
                            {
                                errorMsg = reader.Value;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = "ex:" + ex.ToString();
            }

            if (isFromJob == false)//from api
            {
                SkmFacade.SetSkmPayParkingOrderTransLog(order.Id, (int)order.Status,
                    //Umall/2.14.6銷售(停車繳費確認)CompleteParkingOTPOrder
                    //Umall/2.14.6銷售(停車繳費確認)RefundParkingPayment
                    SKmPayParkingNoticeApiDesc + fromApi.ToString(),
                    isSuccess,
                    sendData,
                    new JsonSerializer().Serialize(wsResult),
                    MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name),
                    "ReturnCode : " + (isSuccess ? skmUmallCheckSuccessCode : errorMsg));
            }
            else//from job
            {
                SkmFacade.SetSkmPayParkingOrderTransLog(order.Id, (int)order.Status,
                    //Umall/2.14.6銷售(停車繳費確認)CompleteParkingOTPOrder
                    //Umall/2.14.6銷售(停車繳費確認)RefundParkingPayment
                    SKmPayParkingNoticeApiDesc + SkmPayParkingNoticeUmallApi.SkmPayParkingPayConfirmReTryJob.ToString(),
                    isSuccess,
                    sendData,
                    new JsonSerializer().Serialize(wsResult),
                    MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name),
                    "ReturnCode : " + (isSuccess ? skmUmallCheckSuccessCode : errorMsg));
            }

            return isSuccess;

        }

        /// <summary>
        /// 若CompleteParkingOTPOrder API 呼叫失敗, 則此JOB會Retry
        /// 呼叫umall 2.14.6停車確認
        /// </summary>
        /// <param name="logger"></param>
        public static void SkmPayParkingPayConfirmReTryJob(ILog logger)
        {
            //查詢失敗的Log
            var logList = skmEp.GetSkmPayParkingOrderTransLogList(SKmPayParkingNoticeApiDesc, false);
            var orders = skmEp.GetSkmParkingOrderByIds(logList.Select(p => p.OrderId).ToList());

            foreach (var log in logList)
            {
                var parkingOrder = orders.FirstOrDefault(p => p.Id == log.OrderId);
                var apiType = SkmPayParkingNoticeUmallApi.CompleteParkingOTPOrder;
                //結帳通知2.14.6要重送
                var invoice = new SkmPayInvoice();
                if (log.Action.Equals(SKmPayParkingNoticeApiDesc + apiType.ToString()))
                {
                    invoice = skmEp.GetSkmPayInvoice(parkingOrder.Id, SkmPayParkingPlatFormId).FirstOrDefault(item =>
                    item.SellTranstype == (int)TurnCloudSellTransType.Sell &&
                    item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                    item.ReturnCode == "1");    /*因交易成功，取成功的資料*/
                }
                else if (log.Action.Equals(SKmPayParkingNoticeApiDesc + SkmPayParkingNoticeUmallApi.RefundParkingPayment.ToString()))
                {
                    apiType = SkmPayParkingNoticeUmallApi.RefundParkingPayment;
                    invoice = skmEp.GetSkmPayInvoice(parkingOrder.Id, SkmPayParkingPlatFormId).FirstOrDefault(item =>
                        item.SellTranstype != (int)TurnCloudSellTransType.Sell &&
                        item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                        item.ReturnCode == "1");    /*因交易成功，取成功的資料*/
                }

                if (parkingOrder == null || invoice == null || parkingOrder.Id == 0 || invoice.Id == 0)
                {
                    logger.Info(string.Format("SkmPayParkingPayConfirmReTryJob.LogId={0},ApiType={1},Error{2}:"
                        , log.Id, apiType.ToString(), (parkingOrder == null ? "parkingOrder == null" : "invoice == null")));
                    continue;
                }

                //呼叫umall 2.14.6
                if (ParkingPayConfirm(parkingOrder, invoice, apiType))
                {
                    log.IsSuccess = true;
                    log.Memo += ";Job已補送成功";
                    skmEp.SetSkmPayParkingOrderTransLog(log);//回寫log狀態改成isSuccess=True
                }

            }
        }

        /// <summary>
        /// 取得Skm Pay 發票明細的SellNo
        /// </summary>
        /// <param name="shopid">館別代碼</param>
        /// <param name="sellDay">銷售日</param>
        /// <returns></returns>
        public static string GetSkmPayInvoiceWriteOffSellNo(string shopid, DateTime sellDay)
        {
            string serialNameFormat = "{0}_{1}_{2}";
            string returnValue = string.Empty;
            int serialNumber = _sysp.SerialNumberGetNext(string.Format(serialNameFormat, shopid, sellDay.ToString("yyMMdd"), "WriteOff"));
            returnValue = "P" + serialNumber.ToString().PadLeft(5, '0');
            if (returnValue.Length > 6)
            {
                throw new Exception("SellNo Out Of Max Length 6!");
            }
            return returnValue;
        }

        /// <summary>
        /// 驗證載具條碼格式是否正確
        /// </summary>
        /// <param name="ecInvoiceType">發票載具類型</param>
        /// <param name="invoiceVehicleBarcode">載具條碼</param>
        /// <param name="donateNo">載具條碼</param>
        /// <param name="errorMessage">驗證錯誤訊息</param>
        /// <returns>是否驗證成功</returns>
        public static bool CheckInvoiceVehicleType(SkmPayInvoiceCarrierType ecInvoiceType, string invoiceVehicleBarcode, string uniformNumber, string donateNo, out string errorMessage)
        {
            errorMessage = string.Empty;
            Regex regex;
            switch (ecInvoiceType)
            {
                case SkmPayInvoiceCarrierType.SkmMember:
                    if (string.IsNullOrEmpty(invoiceVehicleBarcode))
                    {
                        errorMessage = "InvoiceVehicleBarcode需填寫會員卡號";
                    }
                    break;
                case SkmPayInvoiceCarrierType.PersonalCertificate:
                    regex = new Regex("[A-Z]{2}[0-9]{14}");
                    if (!regex.IsMatch(invoiceVehicleBarcode))
                    {
                        errorMessage = "錯誤的自然人憑證條碼格式";
                    }
                    break;
                case SkmPayInvoiceCarrierType.Mobile:
                    regex = new Regex("/[A-Z0-9\\.\\-\\+]{7}");
                    if (!regex.IsMatch(invoiceVehicleBarcode))
                    {
                        errorMessage = "錯誤的手機條碼格式";
                    }
                    break;
                case SkmPayInvoiceCarrierType.Donation:
                    regex = new Regex("[0-9]{3,}");
                    if (!regex.IsMatch(donateNo))
                    {
                        errorMessage = "錯誤的愛心碼格式";
                    }
                    break;
                case SkmPayInvoiceCarrierType.ElectronicInvoiceCertificate:
                    errorMessage = RegExRules.CompanyNoCheck(uniformNumber);
                    break;
            }
            return string.IsNullOrEmpty(errorMessage);
        }

        /// <summary>
        /// SkmPay重新退款(排程用)
        /// </summary>
        /// <param name="logger"><see cref="ILog"/></param>
        public static void SkmPayFailedRetryJob(ILog logger)
        {
            bool canRefundAppos = IsApposRefundTime();

            #region 精選優惠退款
            List<SkmPayOrder> needRetryRefundSkmPayOrders = skmEp.GetSkmPayOrdersByOrderStatus(SkmPayOrderFaildNeedRetryStatus);
            logger.Info("SkmPayFailedRetryJob Start:" + (needRetryRefundSkmPayOrders == null ? "0" : needRetryRefundSkmPayOrders.Count().ToString()));
            string errorMessage = string.Empty;
            foreach (SkmPayOrder skmPayOrder in needRetryRefundSkmPayOrders)
            {
                logger.Info("SkmPayFailedRetryJob.ReturnWalletWhenBuyFail=>OrderID:" + skmPayOrder.Id);
                bool isSuccess = false;
                SkmPayOrderStatus skmPayOrderStatus;
                switch (skmPayOrder.Status)
                {
                    //獨退發票
                    case (int)SkmPayOrderStatus.RefundInvoiceFail:
                        //isSuccess = SkmFacade.InvoiceRefund(skmPayOrder, out skmPayOrderStatus);
                        //logger.Info("SkmPayFailedRetryJob.RefundInvoiceFail=>OrderID:" + skmPayOrder.Id + " " + isSuccess);
                        break;
                    //購買失敗，wallet退款
                    case (int)SkmPayOrderStatus.CreateInvoiceFailRefundFail:
                    case (int)SkmPayOrderStatus.SentInvoiceFail:
                        if (canRefundAppos)
                        {
                            isSuccess = ReturnWalletWhenBuyFail(skmPayOrder, "SkmPayFailedRetryJob", out errorMessage);
                            logger.Info("SkmPayFailedRetryJob.ReturnWalletWhenBuyFail=>OrderID:" + skmPayOrder.Id + " " + isSuccess);
                        }
                        break;
                    //發票重新核銷
                    case (int)SkmPayOrderStatus.CreatedInvoiceWriteOffFail:
                    case (int)SkmPayOrderStatus.WriteOffFail:                        
                        isSuccess = SkmFacade.InvoiceWriteOff(skmPayOrder, out skmPayOrderStatus);
                        logger.Info("SkmPayFailedRetryJob.ReturnWalletWhenBuyFail=>OrderID:" + skmPayOrder.Id + " " + isSuccess);
                        break;
                    //退貨失敗，重新退貨
                    case (int)SkmPayOrderStatus.RefundPaymentFail:
                        if (canRefundAppos)
                        {
                            isSuccess = RefundSkmPayOrderByOrderNo(skmPayOrder.OrderNo, "SkmPayFailedRetryJob").Code == WalletReturnCode.Success;
                            logger.Info("SkmPayFailedRetryJob.ReturnWalletWhenBuyFail=>OrderID:" + skmPayOrder.Id + " " + isSuccess);
                        }
                        break;
                    case (int)SkmPayOrderStatus.WaitingOTPFinished:
                        break;                    
                }
            }
            #endregion

            #region 停車退款
            List<SkmPayParkingOrder> parkingOrderList = skmEp.GetSkmPayParkingOrdersByOrderStatus(SkmPayOrderFaildNeedRetryStatus);
            logger.Info("[Parking] SkmPayFailedRetryJob Start:" + (parkingOrderList == null ? "0" : parkingOrderList.Count().ToString()));
            string errMsg = string.Empty;
            foreach (SkmPayParkingOrder parkingOrder in parkingOrderList)
            {
                logger.Info("[Parking] SkmPayFailedRetryJob.ReturnWalletWhenBuyFail=>OrderID:" + parkingOrder.Id);
                bool isSuccess = false;
                switch (parkingOrder.Status)
                {
                    //購買失敗，wallet退款
                    case (int)SkmPayOrderStatus.CreateInvoiceFailRefundFail:
                        if (canRefundAppos)
                        {
                            isSuccess = ReturnWalletWhenBuyFailWithParking(parkingOrder, WalletRefundFromType.FromSkmPayFailedRetryJob, out errMsg);
                            logger.Info(string.Format("[Parking] SkmPayFailedRetryJob.ReturnWalletWhenBuyFail=> OrderID:{0},OrderOriStatus:{1},Success:{2}", parkingOrder.Id, (int)SkmPayOrderStatus.CreateInvoiceFailRefundFail, isSuccess));
                        }
                        break;
                    case (int)SkmPayOrderStatus.SentInvoiceFail:
                        if (canRefundAppos)
                        {
                            isSuccess = ReturnWalletWhenBuyFailWithParking(parkingOrder, WalletRefundFromType.FromSkmPayFailedRetryJob, out errMsg);
                            logger.Info(string.Format("[Parking] SkmPayFailedRetryJob.ReturnWalletWhenBuyFail=> OrderID:{0},OrderOriStatus:{1},Success:{2}", parkingOrder.Id, (int)SkmPayOrderStatus.SentInvoiceFail, isSuccess));
                        }
                        break;
                    //停車沒有核銷概念
                    case (int)SkmPayOrderStatus.CreatedInvoiceWriteOffFail:
                    //停車沒有核銷概念
                    case (int)SkmPayOrderStatus.WriteOffFail:
                    //退貨失敗，重新退貨
                    case (int)SkmPayOrderStatus.RefundPaymentFail: //1.wallet退款 2.騰雲退發票 
                        if (canRefundAppos)
                        {
                            isSuccess = RefundSkmPayParkingOrder(new RefundParkingPaymentRequest()
                            {
                                OrderNo = parkingOrder.OrderNo,
                                ParkingTicketNo = parkingOrder.ParkingTicketNo
                            }).Code == ApiResultCode.Success;
                            logger.Info(string.Format("[Parking] SkmPayFailedRetryJob.RefundSkmPayParkingOrder=> OrderID:{0},OrderOriStatus:{1},Success:{2}", parkingOrder.Id, (int)SkmPayOrderStatus.RefundPaymentFail, isSuccess));
                        }
                        break;
                    case (int)SkmPayOrderStatus.WaitingOTPFinished:
                        break;
                }
            }
            #endregion
        }

        /// <summary>
        /// 精選優惠OTP逾時退款(排程用)
        /// </summary>
        public static void SkmPayOTPTimeoutRefundJob(ILog logger)
        {
            bool canRefundAppos = IsApposRefundTime();
            List<SkmPayOrder> oTPTimeoutSkmPayOrders = skmEp.GetSkmPayOrdersByOtpTimeout(config.SkmPayOTPTimeoutSeconds);
            logger.Info("Need Process Order Count:" + (oTPTimeoutSkmPayOrders == null ? "0" : oTPTimeoutSkmPayOrders.Count().ToString()));

            //將訂單更新成退貨處理中
            foreach (var o in oTPTimeoutSkmPayOrders)
            {
                o.Status = (int)SkmPayOrderStatus.WaitingOTPRefund;                
            }
            skmEp.SetSkmPayOrders(oTPTimeoutSkmPayOrders);
                        
            foreach (SkmPayOrder skmPayOrder in oTPTimeoutSkmPayOrders)
            {
                try
                {
                    SkmPayOtp skmPayOtp = skmEp.GetSkmPayOtpBySkmPayOrderId(skmPayOrder.Id);
                    //若找不到Otp的資料，則寫一筆進去用來退貨
                    if (skmPayOtp == null || skmPayOtp.Id <= 0)
                    {
                        skmPayOtp = new SkmPayOtp()
                        {
                            JobAccessCount = 0,
                            AppAccessCount = 0,
                            LastAppAccessTime = skmPayOrder.CreateDate,
                            OtpUrl = "",
                            FreeDealPayReply = "",
                            VipAppPass = "",
                            SkmPayOrderId = skmPayOrder.Id,
                            CreateTime = DateTime.Now,
                            IsFinished = true,
                            IsProcessing = false,
                        };
                        skmEp.SetSkmPayOtp(skmPayOtp);
                    }
                    //若APP執行次數 小於 設定的訪問上限，或是Otp最後詢問時間 未超過 設定的過期時間，則放置不處理
                    if (skmPayOtp.AppAccessCount <= config.SkmGetPaymentResultAccseeLimite &&
                       skmPayOrder.CreateDate.AddSeconds(config.SkmPayOTPTimeoutSeconds) > DateTime.Now)
                        continue;

                    var walletServerResponse = SkmWalletServerUtility.GetPaymentResult(new WalletAuthPaymentRequest(skmPayOrder.SkmTradeNo));
                    bool isSuccess = false;
                    switch (walletServerResponse.Code)
                    {
                        case WalletReturnCode.WaitingOTP:
                            continue;
                        case WalletReturnCode.Success:
                            SkmFacade.SetSkmPayOrderTransLog(skmPayOrder.Id, skmPayOrder.Status, "SkmWalletServerUtility.GetPaymentResult",
                                true,
                                skmPayOrder.SkmTradeNo,
                                new JsonSerializer().Serialize(walletServerResponse),
                                skmPayOrder.UserId,
                                memo: walletServerResponse.Message);
                            string errorMessage = string.Empty;
                            skmPayOrder.Memo = walletServerResponse.Message;
                            if (walletServerResponse.Data.IsSuccess)
                            {
                                //wallet退款
                                isSuccess = ReturnWalletWhenBuyFail(skmPayOrder, "SkmPayOTPTimeoutRefundJob", out errorMessage);
                            }
                            else
                            {
                                skmPayOrder.Status = (int)SkmPayOrderStatus.AuthPaymentFail;
                                skmEp.SetSkmPayOrder(skmPayOrder);
                            }
                            SetOrderFailed(skmPayOrder.OrderGuid, skmPayOrder.Bid);
                            break;
                        default:
                            //查詢付款結果失敗
                            SkmFacade.SetSkmPayOrderTransLog(skmPayOrder.Id, skmPayOrder.Status, "SkmWalletServerUtility.GetPaymentResult",
                                false,
                                skmPayOrder.SkmTradeNo,
                                new JsonSerializer().Serialize(walletServerResponse),
                                skmPayOrder.UserId, memo: "from job " + walletServerResponse.Message);
                            skmPayOrder.Memo = new JsonSerializer().Serialize(walletServerResponse.Message);
                            skmPayOrder.Status = (int)SkmPayOrderStatus.WaitingOTPFinished; //查詢付款結果失敗應將狀壓成 41，讓下次Job Retry
                            skmEp.SetSkmPayOrder(skmPayOrder);
                            SetOrderFailed(skmPayOrder.OrderGuid, skmPayOrder.Bid);
                            break;
                    }
                    skmPayOtp.JobAccessCount++;
                    skmEp.SetSkmPayOtp(skmPayOtp);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }
        }

        /// <summary>
        /// 停車結帳OTP逾時退款(排程用)
        /// </summary>
        public static void SkmPayParkingOTPTimeoutRefundJob(ILog logger)
        {
            bool canRefundAppos = IsApposRefundTime();
            List<SkmPayParkingOrder> oTPTimeoutSkmPayOrders = skmEp.GetSkmPayParkingOrdersByOrderStatus(new List<SkmPayOrderStatus>() { SkmPayOrderStatus.WaitingOTPFinished });
            logger.Info("Need Process Order Count:" + (oTPTimeoutSkmPayOrders == null ? "0" : oTPTimeoutSkmPayOrders.Count().ToString()));
            foreach (SkmPayParkingOrder spkOrder in oTPTimeoutSkmPayOrders)
            {
                try
                {
                    SkmPayOtp skmPayOtp = skmEp.GetSkmPayOtpBySkmPayOrderId(spkOrder.Id);
                    //若找不到Otp的資料，則寫一筆進去用來退貨
                    if (skmPayOtp == null || skmPayOtp.Id <= 0)
                    {
                        skmPayOtp = new SkmPayOtp()
                        {
                            JobAccessCount = 0,
                            AppAccessCount = 0,
                            LastAppAccessTime = spkOrder.CreateDate,
                            OtpUrl = "",
                            FreeDealPayReply = "",
                            VipAppPass = "",
                            SkmPayOrderId = spkOrder.Id,
                            CreateTime = DateTime.Now,
                            IsFinished = true,
                            IsProcessing = false,
                        };
                        skmEp.SetSkmPayOtp(skmPayOtp);
                    }
                    //若APP執行次數 小於 設定的訪問上限，或是Otp最後詢問時間 未超過 設定的過期時間，則放置不處理
                    if (skmPayOtp.AppAccessCount <= config.SkmGetPaymentResultAccseeLimite &&
                       skmPayOtp.LastAppAccessTime.AddSeconds(config.SkmPayOTPTimeoutSeconds) > DateTime.Now)
                        continue;

                    var walletServerResponse = SkmWalletServerUtility.GetPaymentResult(new WalletAuthPaymentRequest(spkOrder.SkmTradeNo));

                    SkmFacade.SetSkmPayParkingOrderTransLog(spkOrder.Id, spkOrder.Status,
                        "WalletServerUtility.GetPaymentResult",
                        walletServerResponse != null && walletServerResponse.Data.IsSuccess,
                        spkOrder.SkmTradeNo,
                        new JsonSerializer().Serialize(walletServerResponse),
                        spkOrder.UserId,
                        "呼叫wallet查詢付款結果:" + Helper.GetDescription(walletServerResponse.Code));

                    bool isSuccess = false;
                    switch (walletServerResponse.Code)
                    {
                        case WalletReturnCode.WaitingOTP:
                            continue;
                        case WalletReturnCode.Success:
                            string errorMessage = string.Empty;
                            spkOrder.Memo = walletServerResponse.Message;
                            if (walletServerResponse.Data.IsSuccess)
                            {
                                //退款
                                isSuccess = ReturnWalletWhenBuyFailWithParking(spkOrder, WalletRefundFromType.FromSkmPayOTPTimeoutRefundJob, out errorMessage);
                            }
                            else
                            {
                                spkOrder.Status = (int)SkmPayOrderStatus.AuthPaymentFail;
                                skmEp.SetSkmPayParkingOrder(spkOrder);
                            }
                            //SetOrderFailed(spkOrder.OrderGuid, spkOrder.Bid);
                            break;
                        default:
                            spkOrder.Memo = new JsonSerializer().Serialize(walletServerResponse.Message);
                            spkOrder.Status = (int)SkmPayOrderStatus.AuthPaymentFail;

                            skmEp.SetSkmPayParkingOrder(spkOrder);
                            //SetOrderFailed(spkOrder.OrderGuid, spkOrder.Bid);
                            break;
                    }
                    skmPayOtp.JobAccessCount++;
                    skmEp.SetSkmPayOtp(skmPayOtp);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }
        }

        /// <summary>
        /// 購買開立發票
        /// </summary>
        /// <param name="skmPayInvoice">發票資訊</param>
        /// <param name="skmPayInvoiceSellDetail">發票銷售明細</param>
        /// <param name="skmPayInvoiceTenderDetails">發票付款明細</param>
        /// <param name="skmSerialNumber">SKM PAY交易序號</param>
        /// <param name="skmPayOrder"></param>
        /// <returns>是否開立成功</returns>
        public static SkmPayOrderStatus CreateInvoice(
            SkmPayInvoice skmPayInvoice,
            SkmPayInvoiceSellDetail skmPayInvoiceSellDetail,
            List<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails,
            SkmSerialNumber skmSerialNumber,
            out CreateInvoiceRequest createInvoiceRequest,
            out CreateInvoiceResponse createInvoiceResponse)
        {
            createInvoiceRequest = null;
            createInvoiceResponse = null;
            SkmPayOrderStatus orderStatus = SkmPayOrderStatus.None;
            //取發票號碼LOG
            var invoiceSnLog = SkmFacade.AddSerialNumberLog(skmPayInvoice.SkmPayOrderId, skmSerialNumber, SkmSerialNumberLogTradeType.MakeOrderInvoice);

            #region 呼叫發票API

            string responseMessage = string.Empty;
            bool isCreateSuccess = false;
            try
            {
                createInvoiceRequest = TurnCloudServerUtility.GenerateCreateInvoiceRequest(skmPayInvoice, skmPayInvoiceSellDetail, skmPayInvoiceTenderDetails);
                createInvoiceResponse = TurnCloudServerUtility.CreateInvoice(createInvoiceRequest);
                isCreateSuccess = createInvoiceResponse.ResponseBase.ReturnCode == "1";
                responseMessage = createInvoiceResponse.ResponseBase.ReturnMessage;
                skmPayInvoice.ReturnCode = createInvoiceResponse.ResponseBase.ReturnCode;
                skmPayInvoice.ReturnMessage = createInvoiceResponse.ResponseBase.ReturnMessage;
            }
            catch (Exception ex)
            {
                responseMessage = ex.ToString();
                logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
            }

            orderStatus = isCreateSuccess ? SkmPayOrderStatus.SentInvoice : SkmPayOrderStatus.SentInvoiceFail;

            #endregion

            //回存發票資訊
            if (isCreateSuccess)
            {
                skmPayInvoice.SellInvoice = createInvoiceResponse.results.InvoiceNo;
            }
            skmPayInvoice.Memo = responseMessage;
            skmEp.SetSkmPayInvoice(skmPayInvoice);

            //CompleteSerialNumberLog
            SkmFacade.CompleteSerialNumberLog(invoiceSnLog, isCreateSuccess, responseMessage);

            string logTradeNo = invoiceSnLog.TradeNo;
            string logTradeMsg = responseMessage;

            if (isCreateSuccess)
            {
                skmEp.UpdateSkmSerialNumber(skmSerialNumber.Id, false, true);
            }
            else
            {
                skmEp.UpdateSkmSerialNumber(skmSerialNumber.Id, true, false);
                SkmFacade.SendEmailAlter(config.SkmPayTradeNoErrorAlertEmail, "[skm pay EC]發票API交易異常通知",
                    string.Format("skm pay 交易中發票API發生異常, 交易序號:{0}, 原因:{1}", logTradeNo, logTradeMsg));
            }

            return orderStatus;
        }

        /// <summary>
        /// 建立訂單時並建立OTp資料
        /// </summary>
        /// <param name="id">Skm Pay訂單編號</param>
        /// <param name="apiFreeDealPayReply">預計回傳資料</param>
        /// <param name="oTPUrl">OTP 連結</param>
        /// <param name="vipAppPass">會員專屬密碼</param>
        /// <returns>是否建立成功</returns>
        public static bool CreateSkmPayOtpWhenMakeOrder(int skmPayOrderId, ApiFreeDealPayReply apiFreeDealPayReply, string oTPUrl, string vipAppPass, string platFormId)
        {
            SkmPayOtp skmPayOtp = new SkmPayOtp()
            {
                SkmPayOrderId = skmPayOrderId,
                AppAccessCount = 0,
                JobAccessCount = 0,
                FreeDealPayReply = new JsonSerializer().Serialize(apiFreeDealPayReply),
                IsProcessing = false,
                IsFinished = false,
                OtpUrl = oTPUrl,
                CreateTime = DateTime.Now,
                LastAppAccessTime = DateTime.Now,
                VipAppPass = vipAppPass,
                PlatFormId = platFormId
            };
            return skmEp.SetSkmPayOtp(skmPayOtp);
        }

        /// <summary>
        /// 購買失敗時的退款動作 (by精選優惠)
        /// </summary>
        /// <param name="skmPayOrder">訂單資訊</param>
        /// <param name="errorMessage">錯誤訊息</param>
        /// <param name="from">從誰退款的</param>
        /// <returns>是否退款成功</returns>
        public static bool ReturnWalletWhenBuyFail(SkmPayOrder skmPayOrder, string from, out string errorMessage)
        {
            bool isRefundSuccess = false;
            errorMessage = string.Empty;
            skmPayOrder.SkmRefundTradeNo = SkmFacade.GetReturnTradeNoForNonInvoice(skmPayOrder.SkmTradeNo);
            skmEp.SetSkmPayOrder(skmPayOrder);
            WalletServerResponse<WalletPaymentResponse> refundResult = null;
            try
            {
                var refundRequest = new WalletAuthPaymentRequest
                {
                    PaymentToken = skmPayOrder.PaymentToken,
                    EcOrderId = skmPayOrder.SkmRefundTradeNo
                };

                refundResult = SkmWalletServerUtility.Refund(refundRequest);
                if (refundResult.Code == WalletReturnCode.Success && refundResult.Data != null && refundResult.Data.IsSuccess)
                {
                    isRefundSuccess = true;
                }
                errorMessage = refundResult.Message;
            }
            catch (Exception ex)
            {
                errorMessage = ex.ToString();
                logger.Error("SkmWalletServerUtility.Refund", ex);
            }

            skmPayOrder.Status = isRefundSuccess ? (int)SkmPayOrderStatus.CreateInvoiceFail : (int)SkmPayOrderStatus.CreateInvoiceFailRefundFail;
            skmEp.SetSkmPayOrder(skmPayOrder);

            SkmFacade.SetSkmPayOrderTransLog(skmPayOrder.Id, skmPayOrder.Status, "SkmWalletServerUtility.Refund",
                isRefundSuccess,
                skmPayOrder.PaymentToken,
                new JsonSerializer().Serialize(refundResult),
                skmPayOrder.UserId,
                memo: from + " " +errorMessage);
            return isRefundSuccess;
        }

        /// <summary>
        /// 購買失敗時的退款動作
        /// </summary>
        /// <param name="skmPayOrder">訂單資訊</param>
        /// <param name="errorMessage">錯誤訊息</param>
        /// <returns>是否退款成功</returns>
        public static bool ReturnWalletWhenBuyFailWithParking(SkmPayParkingOrder spkOrder, WalletRefundFromType fromType, out string errorMessage)
        {
            bool isRefundSuccess = false;
            errorMessage = string.Empty;
            spkOrder.SkmRefundTradeNo = SkmFacade.GetReturnTradeNoForNonInvoice(spkOrder.SkmTradeNo);
            skmEp.SetSkmPayParkingOrder(spkOrder);

            WalletServerResponse<WalletPaymentResponse> refundResult = null;
            WalletAuthPaymentRequest refundRequest = new WalletAuthPaymentRequest
            {
                PaymentToken = spkOrder.WalletPaymentToken,
                EcOrderId = spkOrder.SkmRefundTradeNo
            };
            try
            {
                refundResult = SkmWalletServerUtility.Refund(refundRequest);
                if (refundResult.Code == WalletReturnCode.Success && refundResult.Data != null && refundResult.Data.IsSuccess)
                {
                    isRefundSuccess = true;
                }
                errorMessage = refundResult.Message;
            }
            catch (Exception ex)
            {
                errorMessage = ex.ToString();
                logger.Error(fromType.ToString() + ".SkmWalletServerUtility.Refund", ex);
            }

            spkOrder.RefundDate = DateTime.Now;
            spkOrder.Status = isRefundSuccess ? (int)SkmPayOrderStatus.CreateInvoiceFail : (int)SkmPayOrderStatus.CreateInvoiceFailRefundFail;
            skmEp.SetSkmPayParkingOrder(spkOrder);

            SkmFacade.SetSkmPayParkingOrderTransLog(spkOrder.Id, spkOrder.Status, "WalletServerUtility.Refund",
                isRefundSuccess,
                new JsonSerializer().Serialize(refundRequest),
                new JsonSerializer().Serialize(refundResult),
                spkOrder.UserId,
                "向Wallet退款 " + fromType.ToString() + ":" + errorMessage);

            return isRefundSuccess;
        }

        /// <summary>
        /// 依傳入日期上傳當日銷售總額
        /// </summary>
        /// <param name="searchDate">查詢日期</param
        public static void SkmPayClearPosByDate(DateTime searchDate)
        {
            searchDate = new DateTime(searchDate.Year, searchDate.Month, searchDate.Day, 00, 00, 00);

            #region 精選優惠 SkmPayPlatFormId="7"
            //判斷是否有post過
            if (skmEp.IsPostSkmPayClearPos(searchDate.Date, SkmPayPlatFormId))
            {
                //沒有retry過且第一次上傳失敗
                List<SkmPayClearPosTransLog> ClearLogs = skmEp.GetFailedSkmPayClearPosBySellDay(searchDate.Date, searchDate.Date.AddDays(1), SkmPayPlatFormId);
                foreach (SkmPayClearPosTransLog log in ClearLogs)
                {
                    ClearPosRequest clearPosRequest = new JsonSerializer().Deserialize<ClearPosRequest>(log.RequestObject);
                    SkmPayClearPosTransLog newSkmPayClearPosTransLog = new SkmPayClearPosTransLog()
                    {
                        PlatFormId = SkmPayPlatFormId,
                        SellDay = DateTime.Parse(clearPosRequest.SellDate),
                        RequestObject = log.RequestObject,
                        IsSuccess = false,
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        IsRetry = true
                    };

                    //將僅失敗一次結果重傳
                    if (PostSkmPayClearPos(clearPosRequest, newSkmPayClearPosTransLog))
                    {
                        log.ModifyDate = DateTime.Now;
                        log.IsSuccess = true;
                        log.Memo += "<|>Retry Suceess:" + log.ModifyDate.ToString("yyyy/MM/dd HH:mm:ss");
                        skmEp.SetSkmPayClearPosTransLog(log);
                    }
                }

            }
            else //從clearLog判斷今日沒上傳過, 則上傳
            {
                //取得今日成功開的發票
                List<ViewSkmPayClearPos> clearPosDatas = skmEp.GetSentSuccessSkmPayInvoiceBySellDate(searchDate.Date, searchDate.Date.AddDays(1));
                List<ClearPosRequest> clearPosRequests = TurnCloudServerUtility.GenerateClearPosRequests(clearPosDatas);
                foreach (ClearPosRequest clearPosRequest in clearPosRequests)
                {
                    SkmPayClearPosTransLog skmPayClearPosTransLog = new SkmPayClearPosTransLog()
                    {
                        PlatFormId = SkmPayPlatFormId,
                        SellDay = DateTime.Parse(clearPosRequest.SellDate),
                        RequestObject = new JsonSerializer().Serialize(clearPosRequest),
                        IsSuccess = false,
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        IsRetry = false
                    };
                    PostSkmPayClearPos(clearPosRequest, skmPayClearPosTransLog);
                }
            }
            #endregion

            #region 停車支付 SkmPayParkingPlatFormId="P"

            //判斷是否有post過
            if (skmEp.IsPostSkmPayClearPos(searchDate.Date, SkmPayParkingPlatFormId))
            {
                //沒有retry過且第一次上傳失敗
                List<SkmPayClearPosTransLog> ClearLogs = skmEp.GetFailedSkmPayClearPosBySellDay(searchDate.Date, searchDate.Date.AddDays(1), SkmPayParkingPlatFormId);
                foreach (SkmPayClearPosTransLog log in ClearLogs)
                {
                    ClearPosRequest clearPosRequest = new JsonSerializer().Deserialize<ClearPosRequest>(log.RequestObject);
                    SkmPayClearPosTransLog newSkmPayClearPosTransLog = new SkmPayClearPosTransLog()
                    {
                        PlatFormId = SkmPayParkingPlatFormId,
                        SellDay = DateTime.Parse(clearPosRequest.SellDate),
                        RequestObject = log.RequestObject,
                        IsSuccess = false,
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        IsRetry = true
                    };

                    //將僅失敗一次結果重傳
                    if (PostSkmPayClearPos(clearPosRequest, newSkmPayClearPosTransLog))
                    {
                        log.ModifyDate = DateTime.Now;
                        log.IsSuccess = true;
                        log.Memo += "<|>Retry Suceess:" + log.ModifyDate.ToString("yyyy/MM/dd HH:mm:ss");
                        skmEp.SetSkmPayClearPosTransLog(log);
                    }
                }

            }
            else //從clearLog判斷今日沒上傳過, 則上傳
            {
                //取得今日成功開的發票
                List<ViewSkmPayClearPos> clearPosDatas = skmEp.GetSentSuccessSkmPayParkingInvoiceBySellDate(searchDate.Date, searchDate.Date.AddDays(1));
                List<ClearPosRequest> clearPosRequests = TurnCloudServerUtility.GenerateParkingClearPosRequests(clearPosDatas);
                foreach (ClearPosRequest clearPosRequest in clearPosRequests)
                {
                    SkmPayClearPosTransLog parkingClearPosLog = new SkmPayClearPosTransLog()
                    {
                        PlatFormId = SkmPayParkingPlatFormId,
                        SellDay = DateTime.Parse(clearPosRequest.SellDate),
                        RequestObject = new JsonSerializer().Serialize(clearPosRequest),
                        IsSuccess = false,
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        IsRetry = false
                    };
                    PostSkmPayClearPos(clearPosRequest, parkingClearPosLog);
                }
            }

            #endregion
        }

        /// <summary>
        /// 上傳銷售總額
        /// </summary>
        /// <param name="clearPosRequest">每日上傳銷售總額請求資訊</param>
        /// <param name="skmPayClearPosTransLog">上傳銷售總額電文紀錄資訊</param>
        /// <returns>是否上傳成功</returns>
        private static bool PostSkmPayClearPos(ClearPosRequest clearPosRequest, SkmPayClearPosTransLog skmPayClearPosTransLog)
        {
            bool returnValue = false;
            ClearPosResponse clearPosResponse = null;
            string responseMessage = string.Empty;
            skmEp.SetSkmPayClearPosTransLog(skmPayClearPosTransLog);
            try
            {
                clearPosResponse = TurnCloudServerUtility.ClearPos(clearPosRequest);
                returnValue = clearPosResponse.ResponseBase.ReturnCode == "1";
                responseMessage = clearPosResponse.ResponseBase.ReturnMessage;
            }
            catch (Exception ex)
            {
                responseMessage = ex.ToString();
                logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
            }
            skmPayClearPosTransLog.ResponseObject = new JsonSerializer().Serialize(clearPosResponse);
            skmPayClearPosTransLog.Memo = responseMessage;
            skmPayClearPosTransLog.ModifyDate = DateTime.Now;
            skmPayClearPosTransLog.IsSuccess = returnValue;
            skmEp.SetSkmPayClearPosTransLog(skmPayClearPosTransLog);

            return returnValue;
        }

        /// <summary>
        /// 依照執行狀態儲存Skm Pay OTP 內容
        /// </summary>
        /// <param name="skmPayOtp">Skm Pay OTP 資訊</param>
        /// <param name="walletReturnCode">執行狀態</param>
        /// <param name="isFromApi">是否由Api(APP)執行</param>
        /// <returns>是否儲存成功</returns>
        public static bool SetSkmPayOtpByAccessResult(SkmPayOtp skmPayOtp, WalletReturnCode walletReturnCode, bool isFromApi)
        {
            bool returnVaslue = false;

            if (isFromApi)
            {
                skmPayOtp.LastAppAccessTime = DateTime.Now;
                skmPayOtp.AppAccessCount++;
            }
            else
            {
                skmPayOtp.JobAccessCount++;
            }

            switch (walletReturnCode)
            {
                case WalletReturnCode.WaitingOTP:
                    skmPayOtp.IsFinished = false;
                    break;
                case WalletReturnCode.Success:
                    skmPayOtp.IsFinished = true;
                    skmPayOtp.Result = walletReturnCode.ToString();
                    break;
                default:
                    skmPayOtp.IsFinished = true;
                    skmPayOtp.ErrorMessage = walletReturnCode.ToString();
                    break;
            }
            returnVaslue = skmEp.SetSkmPayOtp(skmPayOtp);
            return returnVaslue;
        }

        #endregion

        #region Deep Link
        public static List<AdvertisingBoardModel> GetOnlineAdvertisingBoards()
        {
            List<AdvertisingBoardModel> returnValue = new List<AdvertisingBoardModel>();
            DateTime nowDate = DateTime.Now;
            var ad = skmEp.GetSkmAdBoard();
            if (ad != null)
            {
                string openUrl = ad.OpenUrl;
                if (ad.NeedLogin)
                {
                    Uri uri = new Uri(openUrl);
                    if (!string.IsNullOrEmpty(uri.Query)) openUrl += "&iflogin=true";
                    else openUrl += "?iflogin=true";
                }
                returnValue.Add(new AdvertisingBoardModel()
                {
                    Url = openUrl,
                    Image = ad.ImageUrl,
                    Title = ad.Name,
                });
            }
            return returnValue;
        }
        #endregion

        #region Private Method

        /// <summary>
        /// 名稱裡有 ".", "-" 則剪
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static string CutStoreName(string name)
        {
            var cutList = new List<string> { ".", "-" };

            foreach (var cut in cutList)
            {
                if (name.IndexOf(cut, StringComparison.Ordinal) > -1)
                {
                    name = name.Substring(0, name.IndexOf(cut, StringComparison.Ordinal));
                }
            }

            return name;
        }

        /// <summary>
        /// 取代新光特別櫃位名稱
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static string ReplaceStoreName(string name)
        {
            name = name.Replace("裸賣", "超市");
            return name;
        }

        /// <summary>
        /// 移除新光全館時名稱重覆
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static string RemoveRepeatStoreName(string name)
        {
            var cutList = name.Split(" ");

            if (cutList.Length > 1 && cutList[0] == cutList[1])
            {
                name = cutList.FirstOrDefault();
            }

            return name;
        }

        #endregion Private Method

        #region 策展

        public static List<ExternalDealIconModel> GetDealIconModelList()
        {
            var result = new List<ExternalDealIconModel>();
            var iconData = cep.GetExternalDealIcons();
            var iconColorData = new JsonSerializer().Deserialize<List<ExternalDealIconStyle>>(_sysp.SystemDataGet("ExternalDealIconStyle").Data);

            foreach (var d in iconData)
            {
                result.Add(new ExternalDealIconModel
                {
                    Id = d.Id,
                    IconName = d.IconName,
                    ForeColor = iconColorData.First(x => x.StyleId == d.StyleId).ForeColor,
                    BackgroundColor = iconColorData.First(x => x.StyleId == d.StyleId).BackgroundColor
                });
            }

            return result;
        }

        public static bool SetExhibitionDealTimeSlot(Guid sellerGuid, int exhibitionEventId, int categoryId,
            Guid externalGuid, DateTime exhibitionStartTime, DateTime exhibitionEndTime)
        {
            var exhEvent = ep.GetExhibitionEvent(exhibitionEventId);
            if (exhEvent.EventType != (byte)SkmExhibitionEventType.DealExhibition)
            {
                return false;
            }

            var ved = pp.ViewExternalDealGetListByDealGuid(externalGuid, sellerGuid);
            if (ved == null || ved.Bid == null || !ved.IsLoaded)
            {
                return false;
            }

            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)ved.Bid);
            if (!vpd.IsLoaded)
            {
                return false;
            }

            var dealTimeSlotStart = vpd.BusinessHourOrderTimeS < exhibitionStartTime ? exhibitionStartTime : vpd.BusinessHourOrderTimeS;
            var dealTimeSlotEnd = vpd.BusinessHourOrderTimeE > exhibitionEndTime ? exhibitionEndTime : vpd.BusinessHourOrderTimeE;

            if (dealTimeSlotStart > dealTimeSlotEnd)
            {
                return false;
            }

            return ep.SaveExhibitionDealTimeSlot(sellerGuid, exhibitionEventId, categoryId, externalGuid, dealTimeSlotStart,
                dealTimeSlotEnd);
        }

        /// <summary>
        /// 取得APP顯示館位名稱資料清單
        /// </summary>
        /// <returns>館位名稱資料清單</returns>
        public static List<SkmStoreDisplayNameInfoModel> GetSkmStoreDisplayNameList()
        {
            List<SkmStoreDisplayNameInfoModel> returnValue = skmEp.GetViewSkmStoreDisplayNames().
                Select((item) =>
                {
                    return new SkmStoreDisplayNameInfoModel()
                    {
                        Guid = item.Guid,
                        ShopCode = item.ShopCode,
                        ExchangeType = (SkmDealExchangeType)Enum.Parse(typeof(SkmDealExchangeType), item.ExchangeType.ToString()),
                        DisplayName = item.DisplayName,
                        ShopName = item.ShopName,
                        SellerGuid = item.SellerGuid,
                    };
                }).OrderBy(item => item.ShopCode).ThenBy(item => item.DisplayName).ToList();
            return returnValue;
        }

        /// <summary>
        /// 依照檔次編號查詢APP顯示館名清單
        /// </summary>
        /// <param name="dealGuid">檔次編號</param>
        /// <returns>APP顯示館名清單</returns>
        public static List<SkmStoreDisplayNameInfoModel> GetViewExternalDealStoreDisplayNameBydealGuid(Guid dealGuid)
        {
            List<SkmStoreDisplayNameInfoModel> returnValue = skmEp.GetViewExternalDealStoreDisplayNameByDealGuid(dealGuid).
                Select((item) =>
                {
                    return new SkmStoreDisplayNameInfoModel()
                    {
                        Guid = item.SkmStoreDisplayNameGuid,
                        ShopCode = item.ShopCode,
                        ExchangeType = (SkmDealExchangeType)Enum.Parse(typeof(SkmDealExchangeType), item.ExchangeType.ToString()),
                        DisplayName = item.DisplayName,
                        ShopName = item.ShopName,
                        SellerGuid = item.SellerGuid,
                    };
                }).ToList();
            return returnValue;
        }

        #endregion

        #region 上傳圖檔檢測

        public static ImgUploadData CheckImgType(int checkImgW, int checkImgH, string preFixName, Guid nameGuid)
        {
            var imgUploadData = new ImgUploadData
            {
                SuccessLoadFile = true,
                ImgFiles = new List<ImgData>()
            };

            if (HttpContext.Current.Request.Files.Count > 0)
            {
                foreach (string file in HttpContext.Current.Request.Files)
                {
                    HttpPostedFile hpf = HttpContext.Current.Request.Files[file];
                    if (hpf != null && hpf.FileName != "")
                    {
                        var idata = new ImgData();

                        //idata = CheckImgType(hpf, checkImgW, checkImgH, preFixName, nameGuid.ToString());

                        #region 先搬家到ImgData CheckImgType()

                        idata.OriFileName = file;

                        string extension = hpf.FileName.Substring(hpf.FileName.LastIndexOf(".", StringComparison.Ordinal),
                            hpf.FileName.Length - hpf.FileName.LastIndexOf(".", StringComparison.Ordinal));
                        //檢查圖片格式
                        if (!CheckImageFormat(extension.ToLower()))
                        {
                            idata.IsExtensionPass = false;
                        }
                        else
                        {
                            System.Drawing.Image img = System.Drawing.Image.FromStream(hpf.InputStream);
                            int imgW = img.Width;
                            int imgH = img.Height;
                            //檢查圖片大小
                            if (imgW != checkImgW || imgH != checkImgH)
                            {
                                idata.IsSizePass = false;
                            }
                        }

                        if (idata.IsExtensionPass && idata.IsSizePass)
                        {
                            idata.SaveFileName = string.Format("{0}_{1}{2}",
                                nameGuid.ToString() == "" ? "{0}" : nameGuid.ToString(), preFixName, extension);
                        }
                        #endregion

                        imgUploadData.ImgFiles.Add(idata);
                    }
                }
            }

            if (!imgUploadData.ImgFiles.Any())
            {
                imgUploadData.SuccessLoadFile = false;
            }

            return imgUploadData;
        }

        public static ImgData CheckImgType(HttpPostedFileBase hpf, int checkImgW, int checkImgH, string nameGuid, string preFixName)
        {
            var idata = new ImgData();
            if (hpf != null && hpf.FileName != "")
            {
                idata.OriFileName = hpf.FileName;
                //原本方法-先放著試試看新方法沒有問題就會拿掉
                //string extension = hpf.FileName.Substring(hpf.FileName.LastIndexOf(".", StringComparison.Ordinal),
                //    hpf.FileName.Length - hpf.FileName.LastIndexOf(".", StringComparison.Ordinal));
                //新方法
                string extension = Path.GetExtension(hpf.FileName);
                //檢查圖片格式
                if (!CheckImageFormat(extension.ToLower()))
                {
                    idata.IsExtensionPass = false;
                }
                else
                {
                    System.Drawing.Image img = System.Drawing.Image.FromStream(hpf.InputStream);
                    int imgW = img.Width;
                    int imgH = img.Height;
                    //檢查圖片大小
                    if (imgW != checkImgW || imgH != checkImgH)
                    {
                        idata.IsSizePass = false;
                    }
                }

                if (idata.IsExtensionPass && idata.IsSizePass)
                {
                    idata.SaveFileName = string.Format("{0}_{1}{2}",
                        string.IsNullOrEmpty(nameGuid) ? "{0}" : nameGuid, preFixName, extension);
                }
            }

            return idata;
        }

        public static bool CheckImageFormat(string str)
        {
            bool flag = true;
            if (!string.IsNullOrEmpty(str))
            {
                if (str.ToLower() != ".jpg" && str.ToLower() != ".jpeg" && str.ToLower() != ".png" && str.ToLower() != ".gif")
                {
                    flag = false;
                }
            }

            return flag;
        }

        #endregion 上傳圖檔檢測

        #region 後台-skmpay相關

        public static int GetSkmPayExcludingTaxPrice(int discount, decimal taxRate)
        {
            decimal price = discount / (1 + taxRate);
            return Convert.ToInt32(Math.Round(price, MidpointRounding.AwayFromZero));
        }

        /// <summary>
        /// 查詢 skm pay 訂單
        /// </summary>
        /// <param name="orderNo"></param>
        /// <param name="shopCode"></param>
        /// <param name="tradeNo"></param>
        /// <returns></returns>
        public static BackendSkmPayOrder GetBackendSkmPayOrder(string orderNo, string shopCode, string tradeNo)
        {
            if (string.IsNullOrEmpty(orderNo) && string.IsNullOrEmpty(tradeNo) || string.IsNullOrEmpty(shopCode))
            {
                return null;
            }

            ViewSkmPayOrder viewSkmOrder = !string.IsNullOrEmpty(orderNo)
                ? skmEp.GetViewSkmPayOrderByOrderNo(orderNo, shopCode)
                : skmEp.GetViewSkmPayOrderByTradeNo(tradeNo, shopCode);

            if (viewSkmOrder == null)
            {
                return null;
            }

            var isApposRefundTime = IsApposRefundTime();
            //訂單、發票、商品
            var backendSkmPayOrder = new BackendSkmPayOrder
            {
                OrderId = viewSkmOrder.OrderId,
                OrderNo = viewSkmOrder.OrderNo,
                VipNo = viewSkmOrder.VipNo,
                TradeNo = viewSkmOrder.SkmTradeNo ?? string.Empty,
                RefundTradeNo = viewSkmOrder.SkmRefundTradeNo ?? string.Empty,
                OrderStatus = (SkmPayOrderStatus)viewSkmOrder.OrderStatus,
                OrderStatusDesc = Helper.GetEnumDescription((SkmPayOrderStatus)viewSkmOrder.OrderStatus),
                OrderTime = viewSkmOrder.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                IsReturnable = IsReturnable(viewSkmOrder.OrderStatus),
                IsApposRefundTime = isApposRefundTime,
                RefundStatusDesc = viewSkmOrder.RefundDate == null ? "未核准" : "退貨完成",
                ReturnDate = viewSkmOrder.RefundDate == null
                    ? string.Empty
                    : ((DateTime)viewSkmOrder.RefundDate).ToString("yyyy/MM/dd HH:mm:ss"),
                OrderAmount = viewSkmOrder.OrderAmount.ToString("C0"),
                PaymentDetail = new SkmPaymentDetail
                {
                    PaymentMethodDesc = viewSkmOrder.InstallPeriod > 0 ? "skm pay 分期付款" : "skm pay 一次付清",
                    PaymentMethod = SkmPaymentMethod.SkmPay,
                    PayAmount = viewSkmOrder.InstallPeriod > 0
                        ? string.Format("skm pay：{0:C0}；分期期數：{1}期；首期金額：{2:C0}；每期金額：{3:C0}；手續費：{4:C0}；",
                          viewSkmOrder.PaymentAmount, viewSkmOrder.InstallPeriod, viewSkmOrder.InstallDownPay, viewSkmOrder.InstallEachPay, viewSkmOrder.InstallFee)
                        : string.Format("skm pay: {0:C0}", viewSkmOrder.PaymentAmount),
                    RedeemAmount = viewSkmOrder.RedeemAmount.ToString("C0"),
                    RedeemPoint = viewSkmOrder.RedeemPoint.ToString("N0"),
                    BurningSkmPoint = viewSkmOrder.BurningSkmPoint.ToString("N0")
                },
                DealName = viewSkmOrder.DealName,
                ShopCode = viewSkmOrder.ShopCode,
                ShopName = viewSkmOrder.ShopName,
                ItemNo = viewSkmOrder.ItemNo,
                SpecialItemNo = viewSkmOrder.SpecialItemNo,
                ProductCode = viewSkmOrder.ProductCode,
                Invoice = new SkmInvoice
                {
                    InvoiceDate = viewSkmOrder.InvoiceDate == null ? string.Empty : ((DateTime)viewSkmOrder.InvoiceDate).ToString("yyyy/MM/dd"),
                    InvoiceNo = viewSkmOrder.InvoiceNo,
                    InvoiceAmount = viewSkmOrder.InvoiceAmount == null ? string.Empty : ((double)viewSkmOrder.InvoiceAmount).ToString("C0"),
                    IsDonate = viewSkmOrder.InvoiceIsdonate ?? false,
                    DonateCode = viewSkmOrder.InvoiceDonateNo,
                    CarrierType = viewSkmOrder.InvoiceCarrierType != (int)SkmPayInvoiceCarrierType.None
                                  ? Helper.GetEnumDescription((SkmPayInvoiceCarrierType)viewSkmOrder.InvoiceCarrierType) : string.Empty,
                    CarrierNo = viewSkmOrder.InvoiceCarrierNo,
                    CompId = string.IsNullOrEmpty(viewSkmOrder.CompId) ? string.Empty : viewSkmOrder.CompId
                }
            };

            //核銷、Coupon
            var cashTrustLogCol = mp.CashTrustLogGetListByOrderGuid(viewSkmOrder.OrderGuid);
            if (cashTrustLogCol.Any())
            {
                var firstCashTrustLog = cashTrustLogCol.FirstOrDefault();
                if (firstCashTrustLog != null)
                {
                    backendSkmPayOrder.CouponId = firstCashTrustLog.CouponId;
                    DateTime? verifyTime = firstCashTrustLog.UsageVerifiedTime;
                    if (verifyTime != null)
                    {
                        backendSkmPayOrder.VerifyTime = ((DateTime)verifyTime).ToString("yyyy/MM/dd HH:mm:ss");
                        backendSkmPayOrder.VerifyStatusDesc = "已取貨";
                        backendSkmPayOrder.FreeReturnDate = ((DateTime)verifyTime).AddDays(7).ToString("yyyy/MM/dd");
                    }
                    else
                    {
                        backendSkmPayOrder.VerifyTime = string.Empty;
                        backendSkmPayOrder.VerifyStatusDesc = "未取貨";
                        backendSkmPayOrder.FreeReturnDate = string.Empty;
                    }
                }
            }

            return backendSkmPayOrder;
        }

        /// <summary>
        /// 退 skm pay 訂單
        /// </summary>
        /// <param name="orderNo"></param>
        public static WalletServerResponse<WalletPaymentResponse> RefundSkmPayOrderByOrderNo(string orderNo, string from)
        {
            var skmPayOrder = skmEp.GetSkmPayOrderByOrderNo(orderNo);

            #region Check

            if (skmPayOrder == null)
            {
                return new WalletServerResponse<WalletPaymentResponse>
                {
                    Code = WalletReturnCode.InvokeError,
                    Message = "查無訂單!"
                };
            }

            if (!IsApposRefundTime())
            {
                return new WalletServerResponse<WalletPaymentResponse>
                {
                    Code = WalletReturnCode.InvokeError,
                    Message = "Appos 清算作業處理中，暫停退貨交易!!"
                };
            }

            if (!IsReturnable(skmPayOrder.Status) || string.IsNullOrEmpty(skmPayOrder.SkmTradeNo))
            {
                return new WalletServerResponse<WalletPaymentResponse>
                {
                    Code = WalletReturnCode.InvokeError,
                    Message = "此訂單不可退貨!!"
                };
            }

            #endregion

            //WriteOffFail 核銷還是成功，只是發票核銷失敗
            bool isWroteOff = skmPayOrder.Status == (int)SkmPayOrderStatus.WriteOff ||
                              skmPayOrder.Status == (int)SkmPayOrderStatus.WriteOffFail;
            //為了要讓後台可以退未成單的訂單
            if (skmPayOrder.Status == (int)SkmPayOrderStatus.CreateInvoiceFailRefundFail)
            {
                string errrorMessage = string.Empty;
                //因購買失敗產生的退貨，在新光那邊不需要連號，所以這邊就重取就好
                if (SkmFacade.ReturnWalletWhenBuyFail(skmPayOrder, "RefundSkmPayOrderByOrderNo", out errrorMessage))
                {
                    return new WalletServerResponse<WalletPaymentResponse>()
                    {
                        Code = WalletReturnCode.Success,
                        Message = ""
                    };
                }
                else
                {
                    return new WalletServerResponse<WalletPaymentResponse>()
                    {
                        Code = WalletReturnCode.GetPaymentResultfail,
                        Message = "退款失敗"
                    };
                }
            }

            #region 退發票

            bool isInvoiceRefundSuccess = false;

            #region 取得invoice by orderid
            bool hasInvoiceData = true;
            List<SkmPayInvoice> skmPayInvoices = skmEp.GetSkmPayInvoice(skmPayOrder.Id, SkmPayPlatFormId);
            SkmPayInvoice skmPayInvoice = null;
            //判斷發票是否已退貨成功過,已退貨成功則不取skmPayInvoice跳過發票退貨流程
            if (!skmPayInvoices.Any(item => item.SellTranstype == (int)TurnCloudSellTransType.Refund &&
                                           item.ReturnCode == "1"))
            {
                //因為sell_no需要取得開立發票的那筆sell_no，所以需要找到開立發票的那筆資料，再找最新的一筆(這只是預防措施，正常來說只會有一筆開立發票)
                //因為sell_no是後面才加的，根本沒有時間調整DB結構，正常來說如果那是交易序號，那應該拿來當編號用，並且標註成功失敗
                //再加上判斷開立發票時是否成功
                skmPayInvoice = skmPayInvoices.Where(item => item.SellTranstype == (int)TurnCloudSellTransType.Sell &&
                                                             item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale &&
                                                             item.ReturnCode == "1")
                                              .OrderByDescending(item => item.Id).FirstOrDefault();
            };
            if (skmPayInvoice == null || skmPayInvoice.Id <= 0)
            {
                hasInvoiceData = false;
            }

            SkmPayInvoiceSellDetail skmPayInvoiceSellDetail = new SkmPayInvoiceSellDetail();
            if (hasInvoiceData)
            {
                skmPayInvoiceSellDetail = skmEp.GetSkmPayInvoiceSellDetailsByInvoiceId(skmPayInvoice.Id).FirstOrDefault();
                if (skmPayInvoiceSellDetail == null || skmPayInvoiceSellDetail.Id <= 0)
                {
                    hasInvoiceData = false;
                }
            }
            List<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails = new List<SkmPayInvoiceTenderDetail>();

            if (hasInvoiceData)
            {
                skmPayInvoiceTenderDetails = skmEp.GetSkmPayInvoiceTenderDetailsByInvoiceId(skmPayInvoice.Id);
                if (skmPayInvoiceTenderDetails == null || skmPayInvoiceTenderDetails.Count() <= 0)
                {
                    hasInvoiceData = false;
                }
            }
            #endregion

            //對應發票商需要在交易跟退貨時使用不同的pay_order_no
            //所以這邊重新取一個新的TradeNo(就是發票商的pay_order_no)給退貨用
            SkmSerialNumber snObj = null;
            //上次已經呼叫發票成功，就取用舊的
            if (skmPayOrder.Status == (int)SkmPayOrderStatus.RefundPaymentFail)
            {
                //去掉尾兩碼的流水號
                string tempSkmRefundTradeNo = skmPayOrder.SkmRefundTradeNo.Substring(0, skmPayOrder.SkmRefundTradeNo.Length - 2);
                snObj = skmEp.GetSkmSerialNumberBySkmTradNo(tempSkmRefundTradeNo);
                //若沒有取到Trad No則回傳錯誤，且因沒有取到，不用更新SkmSerialNumber狀態
                if (snObj == null || snObj.Id <= 0)
                {
                    return new WalletServerResponse<WalletPaymentResponse>
                    {
                        Code = WalletReturnCode.InvokeError,
                        Message = "查無訂單!!"
                    };
                }
                snObj.ModifyTime = DateTime.Now;
                snObj.SerialTime += 1;
                snObj.Processing = true;
                snObj.IsTradeComplete = false;
                skmEp.UpdateSkmSerialNumber(snObj);
                skmPayOrder.SkmRefundTradeNo = snObj.GetTradeNo();
            }
            else
            {
                snObj = SkmFacade.GetSkmTradeNo(skmPayOrder.ShopCode, SkmFacade.SkmPayPlatFormId);
                skmPayOrder.SkmRefundTradeNo = snObj.GetTradeNo();
            }
            //沒有發票資訊或是已經退過發票的不執行這段
            if (hasInvoiceData && skmPayOrder.Status != (int)SkmPayOrderStatus.RefundPaymentFail)
            {
                #region 建立退貨資訊
                DateTime nowDate = DateTime.Now;
                //這邊有傳址呼叫問題，需要做object.Clone(),先暫時用先後順序解決
                SkmPayInvoice refundSkmPayInvoice = skmPayInvoice;
                refundSkmPayInvoice.Id = 0;
                refundSkmPayInvoice.SellOrgDay = skmPayInvoice.Tday;
                refundSkmPayInvoice.SellOrgNo = skmPayInvoice.SellNo;
                refundSkmPayInvoice.SellOrgPosid = skmPayInvoice.SellPosid;
                refundSkmPayInvoice.SellDay = nowDate;
                refundSkmPayInvoice.Tday = nowDate;
                refundSkmPayInvoice.SellTranstype = (int)TurnCloudSellTransType.Refund;
                refundSkmPayInvoice.SellNo = skmPayOrder.SkmRefundTradeNo.Substring(16, 6);
                //如果訂單狀態是已核銷，此值需要填核銷
                refundSkmPayInvoice.PreSalesStatus = skmPayOrder.Status == (int)SkmPayOrderStatus.WriteOff ? (int)TurnCloudPreDalesStatus.WriteOff : (int)TurnCloudPreDalesStatus.Sale;
                SkmPayInvoiceSellDetail refundSkmPayInvoiceSellDetail = skmPayInvoiceSellDetail;
                refundSkmPayInvoiceSellDetail.Id = 0;
                refundSkmPayInvoiceSellDetail.SellId = skmPayInvoiceSellDetail.SellId;
                refundSkmPayInvoiceSellDetail.SellOrderNoS = "000001";
                List<SkmPayInvoiceTenderDetail> refundSkmPayInvoiceTenderDetails = skmPayInvoiceTenderDetails;
                refundSkmPayInvoiceTenderDetails.ForEach(item =>
                {
                    item.Id = 0;
                    if (item.SellTender != TurnCloudSellTenderType.MemberPointsDeduct)
                    {
                        item.PayOrderNo = skmPayOrder.SkmRefundTradeNo;
                    }

                    switch (item.SellTender)
                    {
                        case TurnCloudSellTenderType.MemberPointsDeduct:
                            item.SellTender = TurnCloudSellTenderType.MemberPointsDeductRefund;
                            break;
                        case TurnCloudSellTenderType.NationalCreditCardCenter:
                            item.SellTender = TurnCloudSellTenderType.NationalCreditCardCenterRefund;
                            break;
                        case TurnCloudSellTenderType.SkmPay:
                            item.SellTender = TurnCloudSellTenderType.SkmPayRefund;
                            break;
                        case TurnCloudSellTenderType.SkmPayNCCC:
                            item.SellTender = TurnCloudSellTenderType.SkmPayNCCCRefund;
                            break;
                        case TurnCloudSellTenderType.TaishinCreditCard:
                            item.SellTender = TurnCloudSellTenderType.TaishinCreditCardRefund;
                            break;
                    }
                });

                bool setInvoiceIsSuccess = false;
                string responseMessage = string.Empty;
                var invoicetSnLog = SkmFacade.AddSerialNumberLog(skmPayOrder.Id, snObj, SkmSerialNumberLogTradeType.ReturnInvoice);
                try
                {
                    setInvoiceIsSuccess = SetSkmPayInvoice(refundSkmPayInvoice, refundSkmPayInvoiceSellDetail, refundSkmPayInvoiceTenderDetails);
                    responseMessage = setInvoiceIsSuccess.ToString();
                }
                catch (Exception ex)
                {
                    responseMessage = ex.ToString();
                    logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                }
                invoicetSnLog = SkmFacade.CompleteSerialNumberLog(invoicetSnLog, setInvoiceIsSuccess, responseMessage);
                if (!setInvoiceIsSuccess)
                {
                    skmPayOrder.Status = (int)SkmPayOrderStatus.ReturnInvoiceFail;
                    skmEp.SetSkmPayOrder(skmPayOrder);
                    skmEp.UpdateSkmSerialNumber(snObj.Id, false, false);
                    return new WalletServerResponse<WalletPaymentResponse>
                    {
                        Code = WalletReturnCode.InvokeError,
                        Message = "發票退貨失敗!"
                    };
                }
                RefundInvoiceRequest refundInvoiceRequest = TurnCloudServerUtility.GenerateRefundInvoiceRequest(refundSkmPayInvoice, skmPayInvoiceSellDetail, skmPayInvoiceTenderDetails);
                #endregion

                #region Call API

                responseMessage = string.Empty;
                RefundInvoiceResponse refundInvoiceResponse = null;
                invoicetSnLog = SkmFacade.AddSerialNumberLog(skmPayOrder.Id, snObj, SkmSerialNumberLogTradeType.ReturnInvoice);
                try
                {
                    refundInvoiceResponse = TurnCloudServerUtility.RefundInvoice(refundInvoiceRequest);
                    isInvoiceRefundSuccess = refundInvoiceResponse.ResponseBase.ReturnCode == "1";
                    responseMessage = refundInvoiceResponse.ResponseBase.ReturnMessage;
                    skmPayInvoice.ReturnCode = refundInvoiceResponse.ResponseBase.ReturnCode;
                    skmPayInvoice.ReturnMessage = refundInvoiceResponse.ResponseBase.ReturnMessage;
                }
                catch (Exception ex)
                {
                    responseMessage = ex.ToString();
                    logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                }

                skmPayOrder.Status = isInvoiceRefundSuccess ? skmPayOrder.Status : (int)SkmPayOrderStatus.ReturnInvoiceFail;
                skmEp.SetSkmPayInvoice(skmPayInvoice);
                skmEp.SetSkmPayOrder(skmPayOrder);
                invoicetSnLog = SkmFacade.CompleteSerialNumberLog(invoicetSnLog, isInvoiceRefundSuccess, responseMessage);
                SkmFacade.SetSkmPayOrderTransLog(skmPayOrder.Id, skmPayOrder.Status, "TurnCloudServerUtility.RefundInvoice",
                    isInvoiceRefundSuccess,
                    new JsonSerializer().Serialize(refundInvoiceRequest),
                    new JsonSerializer().Serialize(refundInvoiceResponse),
                    MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name));

                if (!isInvoiceRefundSuccess)
                {
                    skmPayOrder.Status = (int)SkmPayOrderStatus.ReturnInvoiceFail;
                    skmEp.SetSkmPayOrder(skmPayOrder);
                    skmEp.UpdateSkmSerialNumber(snObj.Id, true, false);
                    return new WalletServerResponse<WalletPaymentResponse>
                    {
                        Code = WalletReturnCode.InvokeError,
                        Message = "發票退貨失敗!!"
                    };
                }
                #endregion
            }
            //發票退貨成功就鎖定該交易編號為已使用
            skmEp.UpdateSkmSerialNumber(snObj.Id, false, true);
            #endregion

            #region Wallet

            bool isPaymentRefoundSuccess = false;
            string memo;
            var walletSnLog = SkmFacade.AddSerialNumberLog(skmPayOrder.Id, snObj, SkmSerialNumberLogTradeType.ReturnWallet);
            var request = new WalletAuthPaymentRequest { PaymentToken = skmPayOrder.PaymentToken, EcOrderId = skmPayOrder.SkmRefundTradeNo };
            var refundResult = SkmWalletServerUtility.Refund(request);
            if (refundResult.Code == WalletReturnCode.Success && refundResult.Data != null && refundResult.Data.IsSuccess)
            {
                isPaymentRefoundSuccess = true;
                skmPayOrder.Status = (int)SkmPayOrderStatus.ReturnSuccess;
                skmPayOrder.RefundDate = DateTime.Now;
                memo = from + "_退款成功";
            }
            else
            {
                refundResult.Code = WalletReturnCode.InvokeError;
                skmPayOrder.Status = (int)SkmPayOrderStatus.RefundPaymentFail;
                memo = string.Format("{0} 呼叫Wallet退貨失敗({1})", from, refundResult.Message);
                logger.Error("Skm pay 退款失敗 orderNo: " + skmPayOrder.OrderNo);
            }
            walletSnLog = SkmFacade.CompleteSerialNumberLog(walletSnLog, isPaymentRefoundSuccess, memo);
            skmPayOrder.LastModifyDate = DateTime.Now;
            skmEp.SetSkmPayOrder(skmPayOrder);

            #endregion

            if (isPaymentRefoundSuccess)
            {
                //退貨通知
                if (skmPayOrder.Status == (int)SkmPayOrderStatus.ReturnSuccess)
                {
                    skmEp.SetSkmInAppMessage(new SkmInAppMessage
                    {
                        UserId = skmPayOrder.UserId,
                        EventType = (int)EventType.SkmPayMsg,
                        IsRead = false,
                        Subject = "【退貨通知】",
                        MsgContent = string.Format("您的訂單編號{0}已辦理退貨，可至我的線上購訂單確認!\n\r立即確認訂單>>", skmPayOrder.OrderNo),
                        MsgType = (byte)SkmInAppMessageType.ReturnOrder,
                        IsDel = false
                    });
                }
            }

            //其中一個成功就回壓本站的相關資訊
            if (isPaymentRefoundSuccess || isInvoiceRefundSuccess)
            {
                SetOrderRefund(skmPayOrder.OrderGuid, skmPayOrder.Bid);
            }

            var responseObj = new JsonSerializer().Serialize(refundResult);
            SetSkmPayOrderTransLog(skmPayOrder.Id, skmPayOrder.Status, config.SkmWalletSiteUrl + "/api/ECPay/Refund", isPaymentRefoundSuccess,
                skmPayOrder.PaymentToken, responseObj, skmPayOrder.UserId, memo);

            return refundResult;
        }

        /// <summary>
        /// 2020 庫存中心 退貨 for 阿里巴巴雙中台
        /// </summary>
        /// <param name="skmPayOrder"></param>
        private static void callAlibabaStockReund(SkmPayOrder skmPayOrder)
        {
            var mainDeal = pp.ExternalDealGet(skmPayOrder.ExternalDealGuid);
        }

        /// <summary>
        /// 取消退 skm pay 訂單 (目前流程無使用)
        /// </summary>
        /// <param name="orderNo"></param>
        public static WalletServerResponse<WalletPaymentResponse> CancelRefundSkmPayOrderByOrderNo(string orderNo)
        {
            var skmPayOrder = skmEp.GetSkmPayOrderByOrderNo(orderNo);
            if (skmPayOrder == null)
            {
                return new WalletServerResponse<WalletPaymentResponse>
                {
                    Code = WalletReturnCode.InvokeError,
                    Message = "查無訂單!"
                };
            }

            if (skmPayOrder.Status != (int)SkmPayOrderStatus.ReturnSuccess && string.IsNullOrEmpty(skmPayOrder.PaymentToken)
                || string.IsNullOrEmpty(skmPayOrder.SkmTradeNo))
            {
                return new WalletServerResponse<WalletPaymentResponse>
                {
                    Code = WalletReturnCode.InvokeError,
                    Message = "此訂單不符取消退貨條件!"
                };
            }

            #region 取消退發票

            #endregion

            #region Call Wallet

            bool isSuccess = false;
            int action = (int)SkmPayOrderStatus.None;
            var request = new WalletAuthPaymentRequest { PaymentToken = skmPayOrder.PaymentToken, EcOrderId = skmPayOrder.SkmTradeNo };
            var refundResult = SkmWalletServerUtility.CancelRefund(request);
            if (refundResult.Code == WalletReturnCode.Success && refundResult.Data != null)
            {
                if (refundResult.Data.IsSuccess)
                {
                    isSuccess = true;
                    action = (int)SkmPayOrderStatus.CancelReturnSuccess;
                    skmPayOrder.Status = (int)SkmPayOrderStatus.AuthPaymentSuccess; //取消退貨成功，訂單狀態恢復成上一動"授權成功"
                    skmPayOrder.RefundDate = null;

                }
                else
                {
                    refundResult.Code = WalletReturnCode.InvokeError;
                    action = skmPayOrder.Status;
                    skmPayOrder.Status = (int)SkmPayOrderStatus.CancelRefundPaymentFail;
                }
            }
            skmPayOrder.LastModifyDate = DateTime.Now;
            skmEp.SetSkmPayOrder(skmPayOrder);

            var responseObj = new JsonSerializer().Serialize(refundResult);
            SetSkmPayOrderTransLog(skmPayOrder.Id, action, config.SkmWalletSiteUrl + "/api/ECPay/CancelRefund", isSuccess, skmPayOrder.PaymentToken,
                responseObj, skmPayOrder.UserId);

            #endregion

            return refundResult;
        }

        /// <summary>
        /// 記錄精選優惠skm pay交易歷程
        /// </summary>
        public static void SetSkmPayOrderTransLog(int orderId, int action, string api, bool isSuccess, string resquestObj, string responseObj,
            int userId, string memo = null)
        {
            SkmPayOrderTransLog transLog = new SkmPayOrderTransLog
            {
                OrderId = orderId,
                OrderStatus = action,
                ApiName = api,
                IsSuccess = isSuccess,
                RequestObject = resquestObj,
                ResponseObject = responseObj,
                UserId = userId,
                CreateTime = DateTime.Now,
                Memo = string.IsNullOrEmpty(memo) ? string.Empty : memo.Substring(0, Math.Min(1000, memo.Length))
            };
            skmEp.SetSkmPayOrderTransLog(transLog);
        }

        /// <summary>
        /// 記錄停車skm pay parking交易歷程
        /// </summary>
        public static void SetSkmPayParkingOrderTransLog(int orderId, int orderStatus, string action, bool isSuccess, string resquestObj, string responseObj,
            int userId, string memo = null)
        {
            SkmPayParkingOrderTransLog transLog = new SkmPayParkingOrderTransLog
            {
                OrderId = orderId,
                OrderStatus = orderStatus,
                Action = action,
                IsSuccess = isSuccess,
                RequestObject = resquestObj,
                ResponseObject = responseObj,
                UserId = userId,
                CreateTime = DateTime.Now,
                Memo = string.IsNullOrEmpty(memo) ? string.Empty : memo.Substring(0, Math.Min(1000, memo.Length))
            };
            skmEp.SetSkmPayParkingOrderTransLog(transLog);
        }

        public static void UpdateResponseTransLog(int orderId, int orderStatus, string action, bool isSuccess, string responseObj,
            int userId, string memo)
        {
            var log = skmEp.GetSkmPayParkingOrderTransLog(orderId, userId, action);
            if (log != null)
            {
                log.OrderStatus = orderStatus;
                log.IsSuccess = isSuccess;
                log.ResponseObject = responseObj;
                log.Memo = memo;
                skmEp.SetSkmPayParkingOrderTransLog(log);
            }
        }


        /// <summary>
        /// 至 Umall 查詢贈品資料
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public static BackendSkmOrderGift GetUmallGift(string orderNo)
        {
            BackendSkmOrderGift backendSkmOrderGift = new BackendSkmOrderGift { OrderNo = orderNo };

            var queryGift = skmEp.GetViewSkmPayOrderQueryUmallGift(orderNo);
            if (queryGift != null)
            {
                var sendXml = GetUmallGiftXml(queryGift);
                var sendData = new object[] { System.Net.WebUtility.UrlEncode(sendXml) };
                backendSkmOrderGift.RequestXml = sendXml;

                #region 呼叫 Umall WS

                var wsResult = System.Net.WebUtility.UrlDecode(
                    (string)CommonFacade.InvokeWebService(cp.SkmWebserviceUrl, 30, cp.SkmWsNamespace,
                        cp.SkmWsClassName, cp.SkmWsMethod, sendData));
                backendSkmOrderGift.ResponseXml = wsResult;

                //var wsResult = "<TransXML><Trans><T0100>0210</T0100><T0300>249000</T0300><T3900>00</T3900><T5579><T557901></T557901><T557902>EW70</T557902><T557903>3000</T557903><T557904>會員點數</T557904><T557905></T557905><T557906>1</T557906><T557908></T557908><T557909>20180920103346</T557909><T557910></T557910><T557911>000</T557911><T557912></T557912><T557913></T557913><T557917></T557917><T557918></T557918></T5579></Trans></TransXML>";
                //var wsResult = "<TransXML><Trans><T0100>0210</T0100><T0300>249000</T0300><T3900>00</T3900><T5579><T557901></T557901><T557902>S201800172</T557902><T557903>50000</T557903><T557904>台北南西店電子贈品券_全館</T557904><T557905></T557905><T557906>1</T557906><T557908></T557908><T557909>20180919145213</T557909><T557910></T557910><T557911>000</T557911><T557912></T557912><T557913></T557913><T557917></T557917><T557918></T557918></T5579><T5579><T557901></T557901><T557902>EW70</T557902><T557903>2000</T557903><T557904>會員點數</T557904><T557905></T557905><T557906>1</T557906><T557908></T557908><T557909>20180919145213</T557909><T557910></T557910><T557911>000</T557911><T557912></T557912><T557913></T557913><T557917></T557917><T557918></T557918></T5579><T5579><T557901></T557901><T557902>S201800153</T557902><T557903>50000</T557903><T557904>台北南西店電子贈品券_全館</T557904><T557905></T557905><T557906>1</T557906><T557908></T557908><T557909>20180919145213</T557909><T557910></T557910><T557911>000</T557911><T557912></T557912><T557913></T557913><T557917></T557917><T557918></T557918></T5579><T5579><T557901></T557901><T557902>B201800033</T557902><T557903>100000</T557903><T557904>(測試)會員集點活動</T557904><T557905></T557905><T557906>1</T557906><T557908></T557908><T557909>20180919145213</T557909><T557910></T557910><T557911>000</T557911><T557912></T557912><T557913></T557913><T557917></T557917><T557918></T557918></T5579><T5579><T557901>22018090004</T557901><T557902>201605001156</T557902><T557903>100</T557903><T557904>韓風陶瓷方便碗</T557904><T557905>(南西)2018周年慶BT21卡友禮</T557905><T557906>1</T557906><T557908>00014600</T557908><T557909>20180919144250</T557909><T557910>N</T557910><T557911>000</T557911><T557912>20180919</T557912><T557913></T557913><T557917></T557917><T557918></T557918></T5579><T5579><T557901>22018090005</T557901><T557902>201605001733</T557902><T557903>100</T557903><T557904>法國兔餐具三件組</T557904><T557905>(測試)會員集點活動</T557905><T557906>1</T557906><T557908>00014600</T557908><T557909>20180919171136</T557909><T557910>N</T557910><T557911>000</T557911><T557912>20180919</T557912><T557913></T557913><T557917></T557917><T557918></T557918></T5579><T5579><T557901>22018090005</T557901><T557902>201708000292</T557902><T557903>100</T557903><T557904>水賴束口袋</T557904><T557905>(測試)會員集點活動</T557905><T557906>1</T557906><T557908>00014600</T557908><T557909>20180919171136</T557909><T557910>N</T557910><T557911>000</T557911><T557912>20180919</T557912><T557913></T557913><T557917></T557917><T557918></T557918></T5579><T5579><T557901>22018090005</T557901><T557902>201708001334</T557902><T557903>100</T557903><T557904>美福飯店住宿券</T557904><T557905>(測試)會員集點活動</T557905><T557906>1</T557906><T557908>00014600</T557908><T557909>20180919171136</T557909><T557910>N</T557910><T557911>000</T557911><T557912>20180919</T557912><T557913></T557913><T557917></T557917><T557918></T557918></T5579><T5579><T557901>22018090005</T557901><T557902>201806001227</T557902><T557903>100</T557903><T557904>黑熊筆記本</T557904><T557905>(測試)會員集點活動</T557905><T557906>1</T557906><T557908>00014600</T557908><T557909>20180919171136</T557909><T557910>N</T557910><T557911>000</T557911><T557912>20180919</T557912><T557913></T557913><T557917></T557917><T557918></T557918></T5579></Trans></TransXML>";

                List<T5579> giftList = new List<T5579>();
                try
                {
                    using (StringReader checkResult = new StringReader(wsResult))
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(checkResult);
                        var jsonSer = new JsonSerializer();
                        var json = jsonSer.Serialize(xmlDoc);
                        JObject jObject = JObject.Parse(json);
                        var jToken = jObject.SelectToken("$..T5579");
                        if (jToken != null)
                        {
                            giftList = jsonSer.ConvertToList<T5579>(jToken);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("skm pay 後台查詢贈品發生錯誤! \n\r " + ex.Message);
                    return backendSkmOrderGift;
                }

                #endregion

                if (giftList.Any())
                {
                    backendSkmOrderGift.IsLoad = true;
                    foreach (var t5579 in giftList)
                    {
                        //if (string.IsNullOrEmpty(t5579.T557901)) continue; //只要show檔期代號有值的資料

                        #region parse
                        int exchangeQuantity;
                        int.TryParse(t5579.T557903, out exchangeQuantity);
                        string exchangeStatus;
                        switch (t5579.T557906)
                        {
                            case "1":
                                exchangeStatus = "兌換";
                                break;
                            case "R":
                                exchangeStatus = "兌換退貨";
                                break;

                            default:
                                exchangeStatus = t5579.T557906;
                                break;
                        }
                        string exchangeTime = string.Empty;
                        try
                        {
                            DateTime dd = DateTime.ParseExact(t5579.T557909, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                            exchangeTime = dd.ToString("yyyy/MM/dd HH:mm:ss");
                        }
                        catch
                        {
                            // ignored
                        }
                        string giftType;
                        switch (t5579.T557910)
                        {
                            case "N":
                                giftType = "一般贈品";
                                break;
                            case "C":
                                giftType = "禮券/抵用券";
                                break;
                            case "E":
                                giftType = "電子券";
                                break;
                            default:
                                giftType = t5579.T557910;
                                break;
                        }
                        int amount;
                        int.TryParse(t5579.T557911, out amount);

                        #endregion

                        var gift = new SkmOrderGift
                        {
                            DealCode = t5579.T557901,
                            GiftCode = t5579.T557902,
                            GiftExchangeQuantity = (exchangeQuantity / 100).ToString("#0.00"),
                            GiftName = t5579.T557904,
                            DealName = t5579.T557905,
                            GiftExchangeStatus = exchangeStatus,
                            GiftCertificateNo = t5579.T557907,
                            RedeemerName = t5579.T557908,
                            ExchangeTime = exchangeTime,
                            GiftType = giftType,
                            GiftCertificateAmount = (amount / 100).ToString("#0.00"),
                            GiftExchangeNo = t5579.T557912,
                            ExchangeStoreName = t5579.T557913,
                            MemberNo = t5579.T557917,
                            CardTypeNo = t5579.T557918
                        };
                        backendSkmOrderGift.GiftList.Add(gift);
                    }
                }

            }
            return backendSkmOrderGift;
        }

        /// <summary>
        /// 依銀行代碼取得銀行相關資訊
        /// </summary>
        /// <param name="bankNo">銀行代碼</param>
        /// <returns>銀行相關資訊 </returns>
        public static BankInfo GetBankInfoByBankNo(string bankNo)
        {
            return op.BankInfoGetMainByBankNo(bankNo);
        }

        /// <summary>
        /// 產生查贈品的Xml
        /// </summary>
        /// <param name="queryGift"></param>
        /// <returns></returns>
        private static string GetUmallGiftXml(ViewSkmPayOrderQueryUmallGift queryGift)
        {
            var sendModel = new SkmGiftTransXmlSendData();
            var invoiceDay = queryGift.SellDay == null ? string.Empty : ((DateTime)queryGift.SellDay).ToString("yyyyMMdd");
            sendModel.Trans = new SkmGiftSendData
            {
                T0100 = "0100",
                T0300 = "249000",
                T1200 = DateTime.Now.ToString("HHmmss"),
                T1300 = DateTime.Now.ToString("yyyyMMdd"),
                T4100 = GetIp8(),
                T4200 = queryGift.ShopCode,
                T5503 = queryGift.BrandCounterCode,
                T5504 = queryGift.SellPosid,
                T5505 = queryGift.SellNo,
                T5507 = queryGift.SellInvoice,
                T5509 = "7",
                T5583 = invoiceDay
            };

            var sendJson = new JsonSerializer().Serialize(sendModel);
            var xmlDoc = NewtonsoftJson.JsonConvert.DeserializeXmlNode(sendJson);
            return xmlDoc.InnerXml;
        }

        /// <summary>
        /// 取ip右8碼
        /// </summary>
        /// <returns></returns>
        private static string GetIp8()
        {
            var tempIp = Helper.GetClientIP().Replace(".", "");
            var len = tempIp.Length > 8 ? 8 : tempIp.Length;
            var ip8 = tempIp.Substring(tempIp.Length - len, len); //取ip右8碼
            return ip8;
        }

        #endregion

        #region 停車API

        public static List<SkmPayParkingOrderModel> GetParkingPaymentInfo(GetParkingPaymentInfoRequest model)
        {
            var orders = skmEp.GetSkmPayParkingOrdersByOrderStatus(new List<SkmPayOrderStatus>()
            {
                SkmPayOrderStatus.SentInvoice,//開完發票成功代表付款成功=1
                SkmPayOrderStatus.ReturnSuccess,//退款成功=2
                SkmPayOrderStatus.RefundPaymentFail,//退款失敗(Wallet 退貨交易失敗)=3
            }, model.MemberCardNo, model.ShopCode, model.ParkingTicketNo, model.ParkingToken, model.StartTime, model.EndTime);

            //搜尋條件

            var incoiveDatas = skmEp.GetSkmPayInvoiceList(orders.Select(p => p.Id).ToList(), SkmPayParkingPlatFormId);
            var result = new List<SkmPayParkingOrderModel>();
            foreach (var p in orders)
            {
                var invoice = incoiveDatas.FirstOrDefault(i => i.PlatFormId == SkmPayParkingPlatFormId
                                                               && i.SkmPayOrderId == p.Id
                                                               && i.ReturnCode == "1"); /*因交易成功，取成功的資料*/
                result.Add(new SkmPayParkingOrderModel()
                {
                    MemberCardNo = p.MemberCardNo,
                    ShopCode = p.ShopCode,
                    OrderNo = p.OrderNo,
                    ParkingTicketNo = p.ParkingTicketNo,
                    ParkingToken = p.ParkingToken,
                    ParkingAreaNo = p.ParkingAreaNo,
                    SellInvoice = invoice != null ? invoice.SellInvoice : string.Empty,
                    CreateDate = p.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    OrderAmount = p.OrderAmount,
                    MerchantTradeNo = p.SkmTradeNo,
                    Status = p.Status == (int)SkmPayOrderStatus.SentInvoice
                        ? (int)SkmPayParkingOrderApiStatus.PaySuccess
                        : (p.Status == (int)SkmPayOrderStatus.ReturnSuccess
                            ? (int)SkmPayParkingOrderApiStatus.ReturnSuccess
                            : (int)SkmPayParkingOrderApiStatus.ReturnFail)
                    //Status 
                    //付款成功 : 1   (僅只有狀態為付款成功才可成功執行退貨)
                    //退款成功 : 2  
                    //退款失敗 : 3   (退款失敗，不可再退款)
                });
            }

            return result;
        }

        /// <summary>
        /// 退 skm pay 訂單
        /// </summary>
        /// <param name="orderNo"></param>
        public static ApiResult RefundSkmPayParkingOrder(RefundParkingPaymentRequest model)
        {
            var parkingOrder = skmEp.GetSkmParkingOrder(model.OrderNo, model.ParkingTicketNo);

            #region Check

            if (parkingOrder == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                    Message = "查無訂單!"
                };
            }

            if (!IsApposRefundTime())
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                    Message = "Appos 清算作業處理中，暫停退貨交易!!"
                };
            }

            if (!IsReturnable(parkingOrder.Status) || string.IsNullOrEmpty(parkingOrder.SkmTradeNo))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "此訂單不可退貨!!"
                };
            }

            #endregion

            var userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);

            //add skm_parking_order_trans_log
            SkmFacade.SetSkmPayParkingOrderTransLog(parkingOrder.Id, parkingOrder.Status, "Api/Skm/RefundSkmPayParkingOrder",
                true, new JsonSerializer().Serialize(model), "", userId, "");

            //為了要讓後台可以退未成單的訂單
            if (parkingOrder.Status == (int)SkmPayOrderStatus.CreateInvoiceFailRefundFail)
            {
                string errrorMessage = string.Empty;
                //因購買失敗產生的退貨，在新光那邊不需要連號，所以這邊就重取就好
                //方法內已有紀錄trans_log
                if (SkmFacade.ReturnWalletWhenBuyFailWithParking(parkingOrder, WalletRefundFromType.FromSkmVerifyApiRefundParkingPayment,
                    out errrorMessage))
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.Success,
                        Message = "退貨成功"
                    };
                }
                else
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = "退款失敗"
                    };
                }
            }

            #region 退發票

            #region 取得發票 invoice by orderid

            bool hasInvoiceData = true;
            List<SkmPayInvoice> skmPayInvoices = skmEp.GetSkmPayInvoice(parkingOrder.Id, SkmPayParkingPlatFormId);
            SkmPayInvoice skmPayInvoice = null;
            //判斷發票是否已退貨成功過,已退貨成功則不取skmPayInvoice跳過發票退貨流程
            if (!skmPayInvoices.Any(item => item.SellTranstype == (int)TurnCloudSellTransType.Refund && item.ReturnCode == "1"))
            {
                //因為sell_no需要取得開立發票的那筆sell_no，所以需要找到開立發票的那筆資料，再找最新的一筆(這只是預防措施，正常來說只會有一筆開立發票)
                //因為sell_no是後面才加的，根本沒有時間調整DB結構，正常來說如果那是交易序號，那應該拿來當編號用，並且標註成功失敗
                //再加上判斷開立發票時是否成功
                skmPayInvoice = skmPayInvoices.OrderByDescending(item => item.Id)
                        .FirstOrDefault(item => item.SellTranstype == (int)TurnCloudSellTransType.Sell &&
                                                 item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                                                 item.ReturnCode == "1");
            };

            if (skmPayInvoice == null || skmPayInvoice.Id <= 0)
            {
                hasInvoiceData = false;
            }

            SkmPayInvoiceSellDetail skmPayInvoiceSellDetail = new SkmPayInvoiceSellDetail();
            if (hasInvoiceData)
            {
                skmPayInvoiceSellDetail = skmEp.GetSkmPayInvoiceSellDetailsByInvoiceId(skmPayInvoice.Id).FirstOrDefault();
                if (skmPayInvoiceSellDetail == null || skmPayInvoiceSellDetail.Id <= 0)
                {
                    hasInvoiceData = false;
                }
            }

            List<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails = new List<SkmPayInvoiceTenderDetail>();
            if (hasInvoiceData)
            {
                skmPayInvoiceTenderDetails = skmEp.GetSkmPayInvoiceTenderDetailsByInvoiceId(skmPayInvoice.Id);
                if (skmPayInvoiceTenderDetails == null || skmPayInvoiceTenderDetails.Count() <= 0)
                {
                    hasInvoiceData = false;
                }
            }
            #endregion

            bool isInvoiceRefundSuccess = false;
            //對應發票商需要在交易跟退貨時使用不同的pay_order_no
            //所以這邊重新取一個新的TradeNo(就是發票商的pay_order_no)給退貨用
            SkmSerialNumber snObj = null;
            //上次已經呼叫發票成功，就取用舊的
            if (parkingOrder.Status == (int)SkmPayOrderStatus.RefundPaymentFail)
            {
                //去掉尾兩碼的流水號
                string tempSkmRefundTradeNo = parkingOrder.SkmRefundTradeNo.Substring(0, parkingOrder.SkmRefundTradeNo.Length - 2);
                snObj = skmEp.GetSkmSerialNumberBySkmTradNo(tempSkmRefundTradeNo);
                //若沒有取到Trad No則回傳錯誤，且因沒有取到，不用更新SkmSerialNumber狀態
                if (snObj == null || snObj.Id <= 0)
                {
                    //add skm_parking_order_trans_log
                    SkmFacade.SetSkmPayParkingOrderTransLog(parkingOrder.Id, parkingOrder.Status, "skmEp.GetSkmSerialNumberBySkmTradNo",
                        false, tempSkmRefundTradeNo, new JsonSerializer().Serialize(snObj), userId, "更新SkmRefundTradeNo序號錯誤");

                    return new ApiResult
                    {
                        Code = ApiResultCode.DataNotFound,
                        Message = "查無訂單!!"
                    };
                }
                snObj.ModifyTime = DateTime.Now;
                snObj.SerialTime += 1;
                snObj.Processing = true;
                snObj.IsTradeComplete = false;
                skmEp.UpdateSkmSerialNumber(snObj);
                parkingOrder.SkmRefundTradeNo = snObj.GetTradeNo();//流水號SerialTime再加1
            }
            else
            {
                snObj = SkmFacade.GetSkmTradeNo(parkingOrder.ShopCode, SkmFacade.SkmPayParkingPlatFormId);
                parkingOrder.SkmRefundTradeNo = snObj.GetTradeNo();
            }

            //沒有發票資訊或是已經退過發票的不執行這段
            if (hasInvoiceData && parkingOrder.Status != (int)SkmPayOrderStatus.RefundPaymentFail)
            {
                #region 建立退貨資訊
                DateTime nowDate = DateTime.Now;
                //這邊有傳址呼叫問題，需要做object.Clone(),先暫時用先後順序解決

                int sellTranstype = (int)TurnCloudSellTransType.Refund;
                //另外，停管「負項」交易
                //當天退貨請送sell_transtype = 03，作廢；
                if (skmPayInvoice.Tday.Date == nowDate.Date)
                {
                    sellTranstype = (int)TurnCloudSellTransType.Invalid;
                }

                SkmPayInvoice refundSkmPayInvoice = skmPayInvoice;
                refundSkmPayInvoice.Id = 0;
                refundSkmPayInvoice.SellOrgDay = skmPayInvoice.Tday;
                refundSkmPayInvoice.SellOrgNo = skmPayInvoice.SellNo;
                refundSkmPayInvoice.SellOrgPosid = skmPayInvoice.SellPosid;
                refundSkmPayInvoice.SellDay = nowDate;
                refundSkmPayInvoice.Tday = nowDate;
                refundSkmPayInvoice.SellTranstype = sellTranstype;
                refundSkmPayInvoice.SellNo = parkingOrder.SkmRefundTradeNo.Substring(16, 6);

                //停車沒有核銷概念  PreSalesStatus不用填
                refundSkmPayInvoice.PreSalesStatus = (int)TurnCloudPreDalesStatus.Default;
                SkmPayInvoiceSellDetail refundSkmPayInvoiceSellDetail = skmPayInvoiceSellDetail;
                refundSkmPayInvoiceSellDetail.Id = 0;
                refundSkmPayInvoiceSellDetail.SellId = skmPayInvoiceSellDetail.SellId;
                refundSkmPayInvoiceSellDetail.SellOrderNoS = "000001";
                List<SkmPayInvoiceTenderDetail> refundSkmPayInvoiceTenderDetails = skmPayInvoiceTenderDetails;
                refundSkmPayInvoiceTenderDetails.ForEach(item =>
                {
                    item.Id = 0;
                    if (item.SellTender != TurnCloudSellTenderType.MemberPointsDeduct)
                    {
                        item.PayOrderNo = parkingOrder.SkmRefundTradeNo;
                    }

                    switch (item.SellTender)
                    {
                        case TurnCloudSellTenderType.MemberPointsDeduct:
                            item.SellTender = TurnCloudSellTenderType.MemberPointsDeductRefund;
                            break;
                        case TurnCloudSellTenderType.NationalCreditCardCenter:
                            item.SellTender = TurnCloudSellTenderType.NationalCreditCardCenterRefund;
                            break;
                        case TurnCloudSellTenderType.SkmPay:
                            item.SellTender = TurnCloudSellTenderType.SkmPayRefund;
                            break;
                        case TurnCloudSellTenderType.SkmPayNCCC:
                            item.SellTender = TurnCloudSellTenderType.SkmPayNCCCRefund;
                            break;
                        case TurnCloudSellTenderType.TaishinCreditCard:
                            item.SellTender = TurnCloudSellTenderType.TaishinCreditCardRefund;
                            break;
                    }
                });

                bool setInvoiceIsSuccess = false;
                string responseMessage = string.Empty;
                var invoicetSnLog = SkmFacade.AddSerialNumberLog(parkingOrder.Id, snObj, SkmSerialNumberLogTradeType.ReturnInvoice);
                try
                {
                    setInvoiceIsSuccess = SetSkmPayInvoice(refundSkmPayInvoice, refundSkmPayInvoiceSellDetail, refundSkmPayInvoiceTenderDetails);
                    responseMessage = setInvoiceIsSuccess.ToString();
                }
                catch (Exception ex)
                {
                    responseMessage = ex.ToString();
                    logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                }
                invoicetSnLog = SkmFacade.CompleteSerialNumberLog(invoicetSnLog, setInvoiceIsSuccess, responseMessage);


                if (!setInvoiceIsSuccess)
                {
                    parkingOrder.Status = (int)SkmPayOrderStatus.ReturnInvoiceFail;
                    skmEp.SetSkmPayParkingOrder(parkingOrder);
                    skmEp.UpdateSkmSerialNumber(snObj.Id, false, false);
                    //add skm_parking_order_trans_log
                    SkmFacade.SetSkmPayParkingOrderTransLog(parkingOrder.Id, parkingOrder.Status, "SkmFacade.SetSkmPayInvoice",
                        setInvoiceIsSuccess, new JsonSerializer().Serialize(new
                        {
                            SkmPayInvoice = refundSkmPayInvoice,
                            SkmPayInvoiceSellDetail = refundSkmPayInvoiceSellDetail,
                            SkmPayInvoiceTenderDetails = refundSkmPayInvoiceTenderDetails
                        }), responseMessage, userId, "發票存DB失敗");

                    return new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = "發票存DB失敗!"
                    };
                }

                #endregion

                #region 退發票 Call TurnCloud API

                RefundInvoiceRequest refundInvoiceRequest = TurnCloudServerUtility.GenerateRefundInvoiceRequest(refundSkmPayInvoice, skmPayInvoiceSellDetail, skmPayInvoiceTenderDetails);
                responseMessage = string.Empty;
                RefundInvoiceResponse refundInvoiceResponse = null;
                invoicetSnLog = SkmFacade.AddSerialNumberLog(parkingOrder.Id, snObj, SkmSerialNumberLogTradeType.ReturnInvoice);
                try
                {
                    refundInvoiceResponse = TurnCloudServerUtility.RefundInvoice(refundInvoiceRequest);
                    isInvoiceRefundSuccess = refundInvoiceResponse.ResponseBase.ReturnCode == "1";
                    responseMessage = refundInvoiceResponse.ResponseBase.ReturnMessage;
                    skmPayInvoice.ReturnCode = refundInvoiceResponse.ResponseBase.ReturnCode;
                    skmPayInvoice.ReturnMessage = refundInvoiceResponse.ResponseBase.ReturnMessage;
                }
                catch (Exception ex)
                {
                    responseMessage = ex.ToString();
                    logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                }

                parkingOrder.Status = isInvoiceRefundSuccess ? parkingOrder.Status : (int)SkmPayOrderStatus.ReturnInvoiceFail;
                skmEp.SetSkmPayInvoice(skmPayInvoice);
                skmEp.SetSkmPayParkingOrder(parkingOrder);
                invoicetSnLog = SkmFacade.CompleteSerialNumberLog(invoicetSnLog, isInvoiceRefundSuccess, responseMessage);

                SkmFacade.SetSkmPayParkingOrderTransLog(
                    parkingOrder.Id,
                    parkingOrder.Status,
                    "TurnCloudServerUtility.RefundInvoice",
                    isInvoiceRefundSuccess,
                    new JsonSerializer().Serialize(refundInvoiceRequest),
                    new JsonSerializer().Serialize(refundInvoiceResponse),
                    MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name),
                    "騰雲發票退貨API呼叫" + (isInvoiceRefundSuccess ? "成功!" : "失敗!"));

                if (!isInvoiceRefundSuccess)
                {
                    parkingOrder.Status = (int)SkmPayOrderStatus.ReturnInvoiceFail;
                    skmEp.SetSkmPayParkingOrder(parkingOrder);
                    skmEp.UpdateSkmSerialNumber(snObj.Id, true, false);
                    return new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = "騰雲發票退貨API呼叫失敗!"
                    };
                }
                #endregion
            }

            //發票退貨成功就鎖定該交易編號為已使用
            skmEp.UpdateSkmSerialNumber(snObj.Id, false, true);
            #endregion

            #region Wallet退款

            bool isPaymentRefoundSuccess = false;
            string memo;
            var walletSnLog = SkmFacade.AddSerialNumberLog(parkingOrder.Id, snObj, SkmSerialNumberLogTradeType.ReturnWallet);
            var request = new WalletAuthPaymentRequest
            {
                PaymentToken = parkingOrder.WalletPaymentToken,
                EcOrderId = parkingOrder.SkmRefundTradeNo
            };
            var refundResult = SkmWalletServerUtility.Refund(request);
            if (refundResult.Code == WalletReturnCode.Success && refundResult.Data != null && refundResult.Data.IsSuccess)
            {
                isPaymentRefoundSuccess = true;
                parkingOrder.Status = (int)SkmPayOrderStatus.ReturnSuccess;
                parkingOrder.Memo = ParkingOrderStatusDesc[SkmPayOrderStatus.ReturnSuccess];
                parkingOrder.RefundDate = DateTime.Now;
                memo = "呼叫Wallet退款成功";
            }
            else
            {
                refundResult.Code = WalletReturnCode.InvokeError;
                parkingOrder.Status = (int)SkmPayOrderStatus.RefundPaymentFail;
                memo = string.Format("呼叫Wallet退貨失敗({0})", refundResult.Message);
                logger.Error("Skm pay 退款失敗 orderNo: " + parkingOrder.OrderNo);
            }
            walletSnLog = SkmFacade.CompleteSerialNumberLog(walletSnLog, isPaymentRefoundSuccess, memo);
            parkingOrder.LastModifyDate = DateTime.Now;
            skmEp.SetSkmPayParkingOrder(parkingOrder);

            //log
            var requestResult = new JsonSerializer().Serialize(request);
            var responseResult = new JsonSerializer().Serialize(refundResult);
            SkmFacade.SetSkmPayParkingOrderTransLog(
                parkingOrder.Id,
                parkingOrder.Status,
                "WalletServerUtility/Refund",
                isPaymentRefoundSuccess,
                requestResult,
                responseResult,
                parkingOrder.UserId,
                memo);

            #endregion


            if (isPaymentRefoundSuccess)
            {
                //退貨通知
                if (parkingOrder.Status == (int)SkmPayOrderStatus.ReturnSuccess)
                {
                    skmEp.SetSkmInAppMessage(new SkmInAppMessage
                    {
                        UserId = parkingOrder.UserId,
                        EventType = (int)EventType.Message,//純文字,因為停車費交易並不會在訂單列表顯示,deeplink會壞掉
                        IsRead = false,
                        Subject = "【停車費退款通知】",
                        MsgContent = string.Format("您於{0}支付的停車費 已完成退款，如有任何問題請洽當店停車場管理處或服務台確認，謝謝您。", parkingOrder.CreateDate.ToString("yyyy/MM/dd")),
                        MsgType = (byte)SkmInAppMessageType.ReturnOrder,
                        IsDel = false
                    });
                }

                //try
                //{
                //    //尋找退貨的發票
                //    var refundSkmPayInvoice = skmEp.GetSkmPayInvoice(skmPayOrder.Id).OrderByDescending(item => item.Id)
                //        .FirstOrDefault(item => item.SellTranstype != (int)TurnCloudSellTransType.Sell &&
                //                                item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default &&
                //                                item.ReturnCode == "1");

                //    //成功開發票則呼叫2.14.6呼叫UMALL停車繳費確認
                //    ParkingPayConfirm(parkingOrder, refundSkmPayInvoice, SkmPayParkingNoticeUmallApi.RefundParkingPayment);
                //}
                //catch (Exception ex)
                //{
                //    logger.ErrorFormat("SkmFacade.ParkingPayConfirm[Refund].Exception{0}", ex.ToString());
                //}
            }

            var apiResult = new ApiResult
            {
                Code = isPaymentRefoundSuccess ? ApiResultCode.Success : ApiResultCode.Error,
                Message = isPaymentRefoundSuccess ? "退貨成功" : "退款失敗"
            };

            SkmFacade.UpdateResponseTransLog(skmPayInvoice.SkmPayOrderId, (int)parkingOrder.Status,
                "Api/Skm/RefundSkmPayParkingOrder",
                apiResult.Code == ApiResultCode.Success,
                new JsonSerializer().Serialize(apiResult),
                userId,
                "Success");

            return apiResult;
        }


        #endregion

        #region 訂單

        /// <summary>
        /// SKM Ec 訂單列表
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static List<SkmEcOrder> GetEcOrderList(string userName)
        {
            var ecOrderList = new List<SkmEcOrder>();

            #region 頁籤分類

            var ecOrder1 = new SkmEcOrder
            {
                FilterType = SkmOrderFilterType.EcNotPickedUp,
                FilterText = Helper.GetEnumDescription(SkmOrderFilterType.EcNotPickedUp),
                OrderList = new List<SkmCouponModel>()
            };

            var ecOrder2 = new SkmEcOrder
            {
                FilterType = SkmOrderFilterType.EcPickedUp,
                FilterText = Helper.GetEnumDescription(SkmOrderFilterType.EcPickedUp),
                OrderList = new List<SkmCouponModel>()
            };

            var ecOrder3 = new SkmEcOrder
            {
                FilterType = SkmOrderFilterType.EcCancel,
                FilterText = Helper.GetEnumDescription(SkmOrderFilterType.EcCancel),
                OrderList = new List<SkmCouponModel>()
            };

            //未使用
            var ecCouponStatusGroup1 = new List<int> {
                (int)ApiCouponUsedType.CanUsed,
                (int)ApiCouponUsedType.WaitToPass,
                (int)ApiCouponUsedType.CouponExpired
            };

            //已使用
            var ecCouponStatusGroup2 = new List<int> {
                (int)ApiCouponUsedType.Used
            };

            //取消
            var ecCouponStatusGroup3 = new List<int> {
                (int)ApiCouponUsedType.CouponRefund,
                (int)ApiCouponUsedType.FailDeal,
                (int)ApiCouponUsedType.ATMRefund,
                (int)ApiCouponUsedType.WaitForRefund
            };

            #endregion

            var skmOrders = GetSkmOrder(userName, true);
            foreach (var order in skmOrders.OrderList)
            {
                if (ecCouponStatusGroup1.Contains(order.CouponUsedType))
                {
                    ecOrder1.OrderList.Add(order);
                }
                else if (ecCouponStatusGroup2.Contains(order.CouponUsedType))
                {
                    ecOrder2.OrderList.Add(order);
                }
                else if (ecCouponStatusGroup3.Contains(order.CouponUsedType))
                {
                    ecOrder3.OrderList.Add(order);
                }

            }

            ecOrderList.Add(ecOrder1);
            ecOrderList.Add(ecOrder2);
            ecOrderList.Add(ecOrder3);

            return ecOrderList;
        }

        /// <summary>
        /// SKM 零元檔訂單列表
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="isSkmPay"></param>
        /// <returns></returns>
        public static ApiSkmOrder GetSkmOrder(string userName, bool isSkmPay = false)
        {
            var result = new ApiSkmOrder { OrderList = new List<SkmCouponModel>() };

            int userId = MemberFacade.GetUniqueId(userName);

            #region 排除關閉的館內沒有店的Seller

            var sellerShop = SkmCacheFacade.GetSkmSellers();
            var tempSeller = sellerShop == null ? new List<Guid>() : sellerShop.Select(x => x.SellerGuid ?? Guid.Empty).ToList();
            List<Guid> passSeller = new List<Guid>();
            foreach (var ps in tempSeller)
            {
                var aliveSeller = SkmCacheFacade.GetSkmShoppesBySeller(ps, false);
                if (!aliveSeller.Any())
                {
                    passSeller.Add(ps);
                }
            }

            #endregion

            #region 撈訂單

            List<SkmCouponModel> skmCouponList = new List<SkmCouponModel>();
            List<SkmPponOrderModel> orderGuidList = new List<SkmPponOrderModel>();
            if (isSkmPay)
            {
                var temp = skmEp.GetViewSkmPayPponOrder(userId, config.SkmEcExpiredDataMonth);
                if (temp.Any())
                {
                    orderGuidList.AddRange(temp.Select(x => new SkmPponOrderModel(x)));
                    orderGuidList = orderGuidList.Where(x => Helper.IsFlagSet(x.OrderStatus, OrderStatus.Complete)
                                                             && SkmPayOrderPassStatus.Contains((SkmPayOrderStatus)x.SkmPayOrderStatus))
                                                 .OrderByDescending(x => x.CreateTime).ToList();
                }
            }
            else
            {
                var temp = skmEp.GetViewSkmPponOrder(userId, config.SkmExpiredDataMonth);
                if (temp.Any())
                {
                    orderGuidList.AddRange(temp.Select(x => new SkmPponOrderModel(x)));
                    orderGuidList = orderGuidList.Where(x => Helper.IsFlagSet(x.OrderStatus, OrderStatus.Complete))
                        .OrderByDescending(x => x.CreateTime).ToList();
                }
            }

            #endregion

            #region 排除抽獎活動

            var excludeOrder = new List<Guid>();
            var activityOrder = skmEp.GetSkmActivityLogByUserId(userId);
            if (activityOrder.Any())
            {
                excludeOrder.AddRange(activityOrder.Select(x => x.OrderGuid));
            }

            #endregion

            #region 將本站訂單處理成 SkmCouponModel

            foreach (var og in orderGuidList)
            {
                if (passSeller.Contains(og.SellerGuid))
                {
                    continue;
                }

                //排除中獎贈品
                var ed = SkmCacheFacade.GetOnlineViewExternalDeal(og.BusinessHourGuid, false);
                if (ed.Guid == Guid.Empty || ed.IsPrizeDeal)
                {
                    continue;
                }

                if ((!isSkmPay && ed.IsSkmPay) || (isSkmPay && !ed.IsSkmPay))
                {
                    continue;
                }

                var skmCoupon = new SkmCouponModel();
                skmCoupon.DealVersion = ed.DealVersion;
                skmCoupon.ActiveType = ed.ActiveType;

                if (og.RefundDate != null)
                {
                    skmCoupon.RefundDate = ApiSystemManager.DateTimeToDateTimeString((DateTime)og.RefundDate);
                }

                ApiUserOrder apiUserOrder = PponDealApiManager.ApiUserOrderGetByOrderGuid(og.Guid, userId, true, true);
                if (apiUserOrder != null && apiUserOrder.CouponList.Any())
                {
                    if (excludeOrder.Any() && excludeOrder.Contains(apiUserOrder.OrderGuid))
                    {
                        continue;
                    }

                    skmCoupon.OrderGuid = apiUserOrder.OrderGuid;
                    skmCoupon.OrderId = apiUserOrder.OrderId;
                    skmCoupon.MainBid = apiUserOrder.MainBid;
                    skmCoupon.BusinessHourGuid = apiUserOrder.BusinessHourGuid;
                    skmCoupon.UseStartDate = apiUserOrder.UseStartDate;
                    skmCoupon.UseEndDate = apiUserOrder.UseEndDate;
                    skmCoupon.BuyDate = apiUserOrder.BuyDate;
                    skmCoupon.ShowCoupon = apiUserOrder.ShowCoupon;
                    skmCoupon.IsFreeDeal = apiUserOrder.IsFreeDeal;
                    skmCoupon.OrderStatus = (int)apiUserOrder.OrderStatus >= (int)ApiOrderStatus.Unreturnable  //後來擴充orderstatus，但skm暫用不到
                                            ? ApiOrderStatus.ToHouse : apiUserOrder.OrderStatus;
                    skmCoupon.SkmPponDeal = apiUserOrder.SkmPponDeal;
                    skmCoupon.SkmPponDeal.SkmAvailabilities.RemoveAll(x => x.ShopCode != ed.ShopCode);

                    //處理APP顯示櫃位
                    var vedStoreDisplayNames = skmEp.GetViewExternalDealStoreDisplayNameByDealGuid(ed.Guid).OrderBy(x => x.DisplayName);
                    foreach (var item in skmCoupon.SkmPponDeal.SkmAvailabilities)
                    {
                        var thisStoreDisplayNames = vedStoreDisplayNames
                            .Where(displayName => displayName.ShopCode == item.ShopCode).Select(x => x.DisplayName).ToList();
                        if (thisStoreDisplayNames.Any())
                        {
                            int i = 0;
                            var storeDisplayName = "";
                            foreach (var displayName in thisStoreDisplayNames)
                            {
                                storeDisplayName += displayName;
                                if (i != thisStoreDisplayNames.Count)
                                {
                                    storeDisplayName += "\n";
                                }
                            }
                            item.BrandCounterName = storeDisplayName;
                            item.StoreDisplayName = storeDisplayName;
                        }
                        else
                        {
                            item.BrandCounterName = SkmFacade.PrefixStoreName(item.BrandCounterName);
                        }
                    }

                    //展開憑證成一筆訂單資料
                    foreach (ApiCouponData coupon in apiUserOrder.CouponList)
                    {
                        skmCoupon.CouponId = coupon.CouponId;

                        if (isSkmPay)
                        {
                            skmCoupon.CouponUsedType = (coupon.CouponUsedType == ApiCouponUsedType.FailDeal ||
                                                        coupon.CouponUsedType == ApiCouponUsedType.ATMRefund ||
                                                        (coupon.CouponUsedType != ApiCouponUsedType.Used
                                                        && ApiSystemManager.DateTimeFromDateTimeString(apiUserOrder.UseEndDate) < DateTime.Now)) //未使用，但已過使用期限
                                                        ? (int)ApiCouponUsedType.CouponExpired
                                                        : (int)coupon.CouponUsedType;
                            if (skmCoupon.CouponUsedType == (int)ApiCouponUsedType.CouponExpired &&
                                Helper.IsFlagSet(og.OrderStatus, OrderStatus.Cancel))
                            {
                                skmCoupon.CouponUsedType = (int)ApiCouponUsedType.CouponRefund;
                            }
                        }
                        else
                        {
                            skmCoupon.CouponUsedType = (coupon.CouponUsedType == ApiCouponUsedType.FailDeal ||
                                                        coupon.CouponUsedType == ApiCouponUsedType.CouponRefund ||
                                                        coupon.CouponUsedType == ApiCouponUsedType.ATMRefund ||
                                                        coupon.CouponUsedType == ApiCouponUsedType.WaitForRefund ||
                                                        (coupon.CouponUsedType != ApiCouponUsedType.Used
                                                         && ApiSystemManager.DateTimeFromDateTimeString(apiUserOrder.UseEndDate) < DateTime.Now)) //未使用，但已過使用期限
                                ? (int)ApiCouponUsedType.CouponExpired
                                : (int)coupon.CouponUsedType;
                        }
                        skmCoupon.CouponCode = coupon.CouponCode;
                        skmCoupon.CouponSequenceNumber = coupon.SequenceNumber;
                        skmCoupon.CouponUsedTypeDesc = coupon.CouponUsedTypeDesc;
                        skmCoupon.UsageVerifiedTime = coupon.UsageVerifiedTime;
                        skmCoupon.ProductCode = (coupon.ProductCode == null) ? string.Empty : (coupon.ProductCode.ToLower() == "undefined" ? string.Empty : coupon.ProductCode);
                        skmCoupon.SpecialItemNo = (coupon.SpecialItemNo == null) ? string.Empty : (coupon.SpecialItemNo.ToLower() == "undefined" ? string.Empty : coupon.SpecialItemNo);
                        skmCoupon.Buy = ed.Buy ?? 0;
                        skmCoupon.Free = ed.Free ?? 0;
                        skmCouponList.Add(skmCoupon);
                    }

                    var be = SkmFacade.GetBurningEventByBid(og.BusinessHourGuid);
                    if (be.IsLoaded)
                    {
                        skmCoupon.IsBurningEvent = true;
                        skmCoupon.BurningPoint = be.BurningPoint;
                    }

                    if (isSkmPay)
                    {
                        var invoiceData = skmEp.GetSkmPayInvoice(og.SkmPayOrderId, SkmPayPlatFormId);
                        //有憑證的只會是精選優惠 , 不會有停車
                        if (invoiceData.Any())
                        {
                            var inv = invoiceData.FirstOrDefault(x =>
                                x.SellTranstype == (int)TurnCloudSellTransType.Sell);
                            if (inv != null)
                            {
                                skmCoupon.InvoiceNo = inv.SellInvoice;
                                skmCoupon.IsCompanyInvoice = !string.IsNullOrEmpty(inv.CompId);
                            }
                        }

                        skmCoupon.TradeNo = og.SkmTradeNo;
                        skmCoupon.EcOrderNo = og.EcOrderNo;
                        skmCoupon.BurningPoint = ed.SkmPayBurningPoint;
                        skmCoupon.SkmPayStatus = og.SkmPayOrderStatus;
                        skmCoupon.SkmPayStatusDesc =
                            Helper.GetEnumDescription((SkmPayOrderStatus)og.SkmPayOrderStatus);
                    }
                }
            }

            #endregion

            #region 排除太久遠的訂單與燒點不成功訂單

            List<SkmCouponModel> removeCouponList = new List<SkmCouponModel>();
            foreach (var sc in skmCouponList)
            {
                if (string.IsNullOrEmpty(sc.UseEndDate))
                {
                    continue;
                }

                DateTime checkDt = ApiSystemManager.DateTimeFromDateTimeString(sc.UseEndDate);
                if (!isSkmPay && checkDt.AddMonths(config.SkmExpiredDataMonth) < DateTime.Now)
                {
                    removeCouponList.Add(sc);
                }
                else if (sc.IsBurningEvent)
                {
                    //如若燒點憑證沒有成功的呼叫環友WS Log, 則排除於訂單中
                    if (!skmMp.SkmBurningOrderLogGet(userId, sc.OrderGuid, sc.CouponId).IsLoaded)
                    {
                        removeCouponList.Add(sc);
                    }
                }
            }

            if (removeCouponList.Any())
            {
                foreach (var r in removeCouponList)
                {
                    skmCouponList.Remove(r);
                }
            }

            #endregion

            result.OrderList = skmCouponList;
            return result;
        }

        #endregion

        public static bool GetIsOrdered(IViewPponDeal deal, string userName)
        {

            //此會員本檔次已購買數。
            int alreadyboughtcount = pp.ViewPponOrderDetailGetCountByUser(userName, deal.BusinessHourGuid,
                (bool)deal.IsDailyRestriction);

            if (deal.ItemDefaultDailyAmount != null)
            {
                if (alreadyboughtcount >= deal.ItemDefaultDailyAmount.Value)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool GetIsOrdered(Guid bid, string userName)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            return GetIsOrdered(deal, userName);
        }

        public static void SendEmailAlter(string emailStr, string subject, string mailBody)
        {
            if (string.IsNullOrEmpty(emailStr))
            {
                return;
            }

            var addrList = emailStr.Split(';');
            MailMessage msg = new MailMessage();
            foreach (var e in addrList)
            {
                msg.To.Add(e);
            }
            msg.Subject = subject;
            msg.From = new MailAddress(cp.SystemEmail);
            msg.IsBodyHtml = true;
            msg.Body = mailBody;

            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }

        #region 訂單交易失敗，回壓本站相關資料

        /// <summary>
        /// 訂單交易失敗，訂單壓回未成單
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="orderGuid"></param>
        /// <param name="failCode"></param>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ApiResult SetOrderFail(Guid bid, Guid orderGuid, ApiResultCode failCode, string message, object data = null)
        {
            SetOrderFailed(orderGuid, bid);

            return new ApiResult
            {
                Code = failCode,
                Message = message,
                Data = data
            };
        }

        /// <summary>
        /// 交易異常(付款失敗/開立發票失敗/otp驗證失敗)，訂單壓回未成單
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="bid"></param>
        private static void SetOrderFailed(Guid orderGuid, Guid bid)
        {
            Order order = op.OrderGet(orderGuid);
            if (order.OrderStatus == (int)OrderStatus.Complete)
            {
                order.OrderStatus = (int)Helper.SetFlag(false, order.OrderStatus, OrderStatus.Complete);
                op.OrderSet(order);
                mp.CashTrustLogUpdateRefundBySkm(orderGuid, TrustStatus.Refunded);
            }
            RefreshDealSalesInfo(bid);
        }

        /// <summary>
        /// 正常退貨
        /// </summary>
        /// <param name="orderGuid"></param>
        private static void SetOrderRefund(Guid orderGuid, Guid bid)
        {
            Order order = op.OrderGet(orderGuid);
            if (order.OrderStatus == (int)OrderStatus.Complete)
            {
                order.OrderStatus = (int)Helper.SetFlag(true, order.OrderStatus, OrderStatus.Cancel);
                op.OrderSet(order);
                mp.CashTrustLogUpdateRefundBySkm(orderGuid, TrustStatus.Refunded);
            }
            RefreshDealSalesInfo(bid);
        }

        /// <summary>
        /// 更新銷量
        /// </summary>
        /// <param name="bid"></param>
        public static void RefreshDealSalesInfo(Guid bid)
        {
            logger.InfoFormat("Refresh deal_sales_info bid->{0}, Mode->{1}", bid, config.SkmRefreshDealSalesInfoMode);
            switch (config.SkmRefreshDealSalesInfoMode)
            {
                case (int)SkmRefreshDealSalesInfoMode.SkmTempTable:
                    pp.DealSalesInfoRefreshForSkm(bid);
                    break;
                case (int)SkmRefreshDealSalesInfoMode.SkmStoredProcedure:
                    pp.ExecSpRefreshDealSalesInfoForSkm(bid);
                    break;
                case (int)SkmRefreshDealSalesInfoMode.PayEventsOnRefresh:
                    PayEvents.OnRefresh(bid);
                    break;
            }
            logger.InfoFormat("Refresh deal_sales_info bid->{0} done.", bid);
        }

        #endregion

        #region 訂單推播提醒

        /// <summary>
        /// 取得SkmPushRequestList
        /// </summary>
        /// <returns></returns>
        public static List<SkmPushRequest> GetPushData()
        {

            var preOrderData = skmEp.GetSkmCashTrustLogNotVerifiedByDay(
                config.SkmCashTrustLogStartDays, SkmCashTrustLogNotVerifiedType.PreOrder);
            var expiringOrderData = skmEp.GetSkmCashTrustLogNotVerifiedByDay(
                config.SkmCashTrustLogEndDays, SkmCashTrustLogNotVerifiedType.ExpiringOrder);

            Dictionary<Guid, SkmPushRequest> preOrderDic = new Dictionary<Guid, SkmPushRequest>();
            Dictionary<Guid, SkmPushRequest> expringOrderDic = new Dictionary<Guid, SkmPushRequest>();
            //預購憑證資料處理
            if (preOrderData.Any())
            {
                foreach (var data in preOrderData)
                {
                    if (!preOrderDic.ContainsKey(data.Bid))
                    {
                        var request = new SkmPushRequest(data, SkmCashTrustLogNotVerifiedType.PreOrder);
                        preOrderDic.Add(data.Bid, request);
                    }
                    else
                    {
                        preOrderDic[data.Bid].AddToken(data.SkmToken);
                    }
                }
            }
            //即將到期憑證處理
            if (expiringOrderData.Any())
            {
                foreach (var data in expiringOrderData)
                {
                    if (!expringOrderDic.ContainsKey(data.Bid))
                    {
                        var request = new SkmPushRequest(data, SkmCashTrustLogNotVerifiedType.ExpiringOrder);
                        expringOrderDic.Add(data.Bid, request);
                    }
                    else
                    {
                        expringOrderDic[data.Bid].AddToken(data.SkmToken);
                    }
                }
            }

            var result = preOrderDic.Values.ToList();
            result.AddRange(expringOrderDic.Values.ToList());

            return result;

        }

        /// <summary>
        /// Pre推播訊息InAppMessage
        /// </summary>
        /// <returns></returns>
        public static List<SkmInAppMessage> PrePushDataInAppMessage()
        {
            var preOrderData = skmEp.GetSkmCashTrustLogNotVerifiedByDay(
                config.SkmCashTrustLogStartDays, SkmCashTrustLogNotVerifiedType.PreOrder);
            var expiringOrderData = skmEp.GetSkmCashTrustLogNotVerifiedByDay(
                config.SkmCashTrustLogEndDays, SkmCashTrustLogNotVerifiedType.ExpiringOrder);


            var messages = new List<SkmInAppMessage>();
            var now = DateTime.Now;

            //預購憑證資料處理
            if (preOrderData.Any())
            {
                foreach (var data in preOrderData)
                {
                    var request = new SkmPushRequest(data, SkmCashTrustLogNotVerifiedType.PreOrder);

                    var message = new SkmInAppMessage()
                    {
                        UserId = data.UserId,
                        EventType = data.IsSkmPay
                            ? (byte)EventType.OrderListNotVerified
                            : (byte)EventType.CouponListNotVerified,
                        IsRead = false,
                        Subject = request.LocalName,
                        MsgContent = request.PushContent,
                        MsgType = (byte)SkmInAppMessageType.DeepLink,
                        IsDel = false,
                        ModifyTime = now,
                        SendTime = now
                    };

                    messages.Add(message);
                }
            }
            //即將到期憑證處理
            if (expiringOrderData.Any())
            {
                foreach (var data in expiringOrderData)
                {
                    var request = new SkmPushRequest(data, SkmCashTrustLogNotVerifiedType.ExpiringOrder);

                    var message = new SkmInAppMessage()
                    {
                        UserId = data.UserId,
                        EventType = data.IsSkmPay
                            ? (byte)EventType.OrderListNotVerified
                            : (byte)EventType.CouponListNotVerified,
                        IsRead = false,
                        Subject = request.LocalName,
                        MsgContent = request.PushContent,
                        MsgType = (byte)SkmInAppMessageType.DeepLink,
                        IsDel = false,
                        ModifyTime = now,
                        SendTime = now
                    };

                    messages.Add(message);
                }
            }

            return messages;
        }

        /// <summary>
        /// 塞訂單訊息至訊息中心
        /// </summary>
        /// <param name="messages"></param>
        public static void SaveSkmInAppMessageCollection(List<SkmInAppMessage> messages)
        {
            if (messages.Any())
            {
                skmEp.BulkInsertSkmInAppMessage(messages);
            }
        }

        /// <summary>
        /// 呼叫資訊部API推播
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static SkmPushResponse PostSkmPushData(SkmPushRequest data)
        {

            string uri, route, xSignature;

            uri = config.SkmPushSiteUrl;
            route = config.SkmPushRoute;
            xSignature = config.SkmPushXSignature;

            var jsonText = NewtonsoftJson.JsonConvert.SerializeObject(data);
            var client = new HttpClient();

            client.BaseAddress = new Uri(uri);


            //Headers
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            client.DefaultRequestHeaders.TryAddWithoutValidation("X-Signature", xSignature);

            //用於宣告客戶端要求服務端回應的文件型態
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpContent content = new StringContent(jsonText, Encoding.UTF8, "application/json");

            var response = client.PostAsync(route, content).Result;

            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;

                skmMp.SkmLogSet(new SkmLog()
                {
                    SkmToken = "PostSkmPushData",
                    UserId = 0,
                    LogType = (int)SkmLogType.PostSkmPushData,
                    InputValue = jsonText,
                    OutputValue = result
                });

                return NewtonsoftJson.JsonConvert.DeserializeObject<SkmPushResponse>(result);
            }
            else
            {
                var result = new SkmPushResponse()

                {
                    ActiveId = data.ActiveId,
                    ProgramId = "",
                    StatusNo = ((int)response.StatusCode).ToString(),
                    MessageTx = response.StatusCode.ToString()
                };

                skmMp.SkmLogSet(new SkmLog()
                {
                    SkmToken = "PostSkmPushData",
                    UserId = 0,
                    LogType = (int)SkmLogType.PostSkmPushData,
                    InputValue = jsonText,
                    OutputValue = NewtonsoftJson.JsonConvert.SerializeObject(result)
                });

                return result;
            }

        }

        /// <summary>
        /// 檢查Post得到的Response是否成功
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public static bool IsSkmPushSuccessed(SkmPushResponse response)
        {
            var isSuccessed = false;

            if (response != null)
            {
                isSuccessed = response.StatusNo == "1" ? true : false;
            }

            return isSuccessed;
        }

        #endregion

        #region SkmLog

        public static void SkmLog(string token, int userId, SkmLogType type, string inputStr, string outputStr)
        {
            skmMp.SkmLogSet(new SkmLog()
            {
                SkmToken = token,
                UserId = userId,
                LogType = (int)type,
                InputValue = inputStr,
                OutputValue = outputStr
            });
        }

        #endregion

        #region 信用卡分期活動

        /// <summary>
        /// 檢查是否可分期
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static bool CheckEnableInstall(ViewExternalDeal deal)
        {
            if (!deal.EnableInstall)
            {
                return false;
            }

            var installmentInfos = SkmFacade.GetInstallmentInfo();

            var now = DateTime.Now;
            if (!installmentInfos.Any(x => now >= x.BankStartDate && now <= x.BankEndDate && deal.Discount >= (x.Amount ?? 0)))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 取得當日分期活動資料
        /// </summary>
        /// <returns></returns>
        public static List<ViewSkmInstallEventBank> GetInstallmentInfo()
        {
            var now = DateTime.Now;
            CacheDataStatus dataStatus;

            var installInfos = MemoryCache2.Get<List<ViewSkmInstallEventBank>>(installmentCacheKey, out dataStatus);
            //若無快取資料 or 無當日分期資訊，則重撈資料。
            if (dataStatus != CacheDataStatus.OK || !installInfos.Any(y => now >= y.EventStartDate && now <= y.EventEndDate))
            {
                installInfos = SkmFacade.SetInstallmentCache();
            }

            return installInfos;
        }

        /// <summary>
        /// 取得當日分期活動資料並設定快取後返回資料
        /// </summary>
        /// <returns></returns>
        public static List<ViewSkmInstallEventBank> SetInstallmentCache()
        {
            var installInfos = skmEp.GetViewSkmInstallEventBankByDateTime(DateTime.Now).ToList();

            MemoryCache2.Set(installmentCacheKey, installInfos, installmentCacheMinutes);

            return installInfos;
        }

        /// <summary>
        /// 清除所有分期活動快取資料
        /// </summary>
        public static void ClearInstallmentCache()
        {
            SystemFacade.ClearAllServersCache<List<ViewSkmInstallEventBank>>(installmentCacheKey);
        }

        /// <summary>
        /// 清除所有分期活動快取資料並設定快取後返回資料
        /// </summary>
        public static List<ViewSkmInstallEventBank> RefreshInstallmentCache()
        {
            SystemFacade.ClearAllServersCache<List<ViewSkmInstallEventBank>>(installmentCacheKey);
            return SkmFacade.SetInstallmentCache();
        }

        /// <summary>
        /// 取得檔次分期資訊
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static SkmInstallmentInfoViewModel GetInstallmentInfoByPrice(int price)
        {
            var model = new SkmInstallmentInfoViewModel();

            var installmentInfos = SkmFacade.GetInstallmentInfo();
            if (installmentInfos == null || !installmentInfos.Any())
            {
                logger.Info(installmentInfos == null ? "installmentInfo == null" : "installmentInfo.Any == false");
                return model;
            }

            var firstInfo = installmentInfos.First();
            model.StartDate = firstInfo.EventStartDate.ToString("yyyy/MM/dd");
            model.EndDate = firstInfo.EventEndDate.ToString("yyyy/MM/dd");
            model.PayAmount = price;

            var bankInfosDic = VourcherFacade.BankInfoGetMainList().GetList()//先GroupBy防止Key重複
                .GroupBy(x => x.BankNo, x => x.BankName).ToDictionary(x => x.Key, x => x.FirstOrDefault());

            var now = DateTime.Now;
            foreach (var info in installmentInfos)
            {
                //排除不到門檻的分期
                if (info.Amount == null || info.Amount > price)
                {
                    continue;
                }
                //排除不在活動時間內的分期
                if (!(now >= info.BankStartDate && now <= info.BankEndDate))
                {
                    continue;
                }

                var bankDetail = new BankDetail()
                {
                    BankName = bankInfosDic[info.BankNo],
                    PayFee = info.PayFee ?? 0,
                    IsCoBranded = info.IsCoBranded ?? false,
                    Modifytime = info.BankModifyTime ?? info.BankCreateTime
                };

                var installPeriod = info.InstallPeriod ?? 1;
                var amount = info.Amount ?? 0;
                var key = installPeriod.ToString() + amount.ToString();//key = 分期數+門檻 (字串)

                if (model.InstallmentInfos.ContainsKey(key))
                {

                    model.InstallmentInfos[key].BankDetails.Add(bankDetail);
                }
                else
                {
                    model.InstallmentInfos.Add(key, new InstallmentInfo()
                    {
                        InstallPeriod = installPeriod,
                        InstallPay = model.PayAmount / installPeriod,
                        Amount = amount,
                        BankDetails = new List<BankDetail>()
                        {
                            bankDetail
                        }
                    });
                }
            }

            model.InstallmentInfos = model.InstallmentInfos.OrderBy(x => x.Value.InstallPeriod).ThenBy(x => x.Value.Amount).ToDictionary(x => x.Key, x => x.Value);
            return model;
        }

        #endregion

        #region for 阿里雙中台
        public static StockCenterDeal GetStockCenterDealGuidByAliItemId(long itemId) 
        {
            //long skuId = 0;
            var api = new SKMOpenAPIMain();
            string itemInfoString = api.GetItemInfo(itemId);
            var aliItemInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<AlibabaItemResult>(itemInfoString);
            StockCenterDeal deal = new StockCenterDeal();
            if (aliItemInfo != null && aliItemInfo.success && aliItemInfo.result != null && aliItemInfo.result.itemInfo != null)
            {
                if (aliItemInfo.result.itemInfo.skuIdList != null)
                {
                    deal.skuId = aliItemInfo.result.itemInfo.skuIdList[0];
                }

            }

            if (deal.skuId == 0)
            {
                return deal;
            }

            deal.deal = pp.ExternalDealGet(deal.skuId.ToString());
            return deal;
        }
        #endregion

        #region 千人千面 MemberPh資料同步Initial
        public static void SkmMemberPhSyncJob()
        {
            SkmFtpClient ftc = new SkmFtpClient();
            ftc.Config = new SkmFtpClient.FtpConfig();

            ftc.Config.UserId = config.SkmFtpUserId;
            ftc.Config.Password = config.SkmFPassword;
            ftc.Config.LocalFilePath = config.LocalFilePath;
            ftc.Config.Host =System.Net.Dns.GetHostEntry(config.SkmFtpHost).AddressList[0].ToString();
            ftc.Config.RemoteWorkPath = config.RemoteWorkPath;
            ftc.Config.RemoteFinishPath = config.RemoteFinishPath;
            ftc.Config.RemoteFailPath = config.RemoteFailPath;

            //FTP抓取檔案
            ftc.GetFile();


            foreach(var fname in Directory.GetFiles(ftc.Config.LocalFilePath))
            {
                try
                {
                    using (FileStream file = new FileStream(fname, FileMode.Open, FileAccess.Read))
                    {
                        #region 資料結構initial
                        string line = string.Empty;
                        string[] arrayLine = null;
                        string[] arrayPhCol = null;
                        string[] arrayPh = null;
                        string umallToken = string.Empty;
                        string createTime = string.Empty;
                        DateTime dateTime = default(DateTime);
                        string ph1 = string.Empty;
                        string ph2 = string.Empty;
                        string ph3 = string.Empty;
                        SkmMemberPhCollection smpAddCol = new SkmMemberPhCollection();
                        SkmMemberPhCollection smpUpdateCol = new SkmMemberPhCollection();
                        int smpAddCount = 0;
                        int smpUpdateCount = 0;
                        List<SkmPhModel> phModelList = new List<SkmPhModel>();
                        decimal weight = 0;
                        #endregion

                        using (StreamReader sr = new StreamReader(file, Encoding.UTF8))
                        {
                            while ((line = sr.ReadLine()) != null)
                            {

                                #region initial data
                                umallToken = string.Empty;
                                ph1 = string.Empty;
                                ph2 = string.Empty;
                                ph3 = string.Empty;
                                createTime = string.Empty;
                                dateTime = default(DateTime);
                                phModelList=new List<SkmPhModel>();
                                #endregion

                                arrayLine = line.Split(',');

                                //檢查是否為三層結構
                                if (arrayLine.Count() == 3)
                                {
                                    umallToken = arrayLine[0];
                                    createTime = arrayLine[2];

                                    //檢查時間欄位是否長度為8
                                    if (createTime.Length == 8)
                                    {
                                        DateTime.TryParse(createTime.Substring(0, 4) + "/" + createTime.Substring(4, 2) + "/" + createTime.Substring(6, 2), out dateTime);
                                    }

                                    //拆解Ph資料
                                    if (!string.IsNullOrEmpty(arrayLine[1]))
                                    {
                                        arrayPhCol = arrayLine[1].Split(';');

                                        foreach (var item in arrayPhCol)
                                        {
                                            weight = 0;
                                            arrayPh = item.Split(':');
                                            if (arrayPh.Count() == 2)
                                            {
                                                decimal.TryParse(arrayPh[1], out weight);
                                                phModelList.Add(new SkmPhModel()
                                                {
                                                    ph = arrayPh[0],
                                                    weights = weight
                                                });

                                            }
                                        }
                                    }

                                    //整理資料順序
                                    int i = 1;
                                    foreach (var item in phModelList.OrderByDescending(p => p.weights))
                                    {
                                        switch (i)
                                        {
                                            case 1:
                                                ph1 = item.ph;
                                                break;
                                            case 2:
                                                if (item.ph != ph1) {
                                                    ph2 = item.ph;
                                                } else {
                                                    ph2 = "";
                                                }
                                                break;
                                            case 3:
                                                if (item.ph != ph1 && item.ph != ph2) {
                                                    ph3 = item.ph;
                                                } else {
                                                    ph3 = "";
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                        i++;
                                    }


                                    var skmToken = skmMp.SkmMemberPhGetBySkmToken(umallToken);

                                    if (skmToken.IsLoaded)
                                    {
                                        //使用Update
                                        skmToken.Ph1 = ph1;
                                        skmToken.Ph2 = ph2;
                                        skmToken.Ph3 = ph3;
                                        skmToken.CreateTime = dateTime;
                                        smpUpdateCol.Add(skmToken);

                                        smpUpdateCount++;
                                    }
                                    else
                                    {
                                        //使用新增
                                        smpAddCol.Add(new SkmMemberPh()
                                        {
                                            SkmToken = umallToken,
                                            Ph1 = ph1,
                                            Ph2 = ph2,
                                            Ph3 = ph3,
                                            CreateTime = dateTime
                                        });

                                        smpAddCount++;

                                    }

                                }

                            }

                            if (smpUpdateCount > 0)
                            {
                                skmMp.SkmMemberPhCollectionSet(smpUpdateCol);
                            }

                            if (smpAddCount > 0)
                            {
                                skmMp.SkmMemberPhCollectionBulkInsert(smpAddCol);
                            }

                        }
                    }

                    //先將檔案上傳至FTP成功頁
                    ftc.UploadFinishFile(Path.GetFileName(fname));

                    

                }
                catch (Exception ex)
                {
                    //錯誤交易

                    //先將檔案上傳至FTP失敗頁
                    ftc.UploadFailFile(Path.GetFileName(fname));
                }
                finally
                {
                    //移除FTP上面工作頁的檔案
                    ftc.DeleteFile(Path.GetFileName(fname));
                    //移除本機檔案
                    File.Delete(Path.Combine(ftc.Config.LocalFilePath, Path.GetFileName(fname)));
                }              
            }
        }

        public class SkmPhModel
        {
            public string ph { get; set; }
            public decimal weights { get; set; }
        }

        #endregion

        public static ViewExternalDealCollection GetViewExternalCacheBySeller(Guid sellerGuid, List<Guid> timeSlotBids)
        {                        
            string key = string.Format("{0}.{1}", "ViewExternalDeal", sellerGuid);
            ViewExternalDealCollection veds = MemoryCache2.Get<ViewExternalDealCollection>(key);
            if (veds == null)
            {
                veds = pp.ViewExternalDealGetListByBid(timeSlotBids);
                MemoryCache2.Set<ViewExternalDealCollection>(key, veds, 15);
            }
            return veds;            
        }

        public static List<SkmBurningEvent> GetSkmBurningEventCacheBySeller(Guid sellerGuid, ViewExternalDealCollection veds)
        {
            string key = string.Format("{0}.{1}", "SkmBurningEvent", sellerGuid);
            List<SkmBurningEvent> sbes = MemoryCache2.Get<List<SkmBurningEvent>>(key);
            if (sbes == null)
            {
                sbes = skmMp.SkmBurningEventListGet(veds.Select(p => p.Guid).ToList(), false);
                MemoryCache2.Set<List<SkmBurningEvent>>(key, sbes, 15);
            }
            return sbes;
        }
    }
}


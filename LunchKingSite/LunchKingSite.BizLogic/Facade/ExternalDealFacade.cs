﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Facade
{
    /// <summary>
    /// 載入很快的，Cache機制用簡單的 MemoryCache2 即可
    /// </summary>
    public class ExternalDealFacade
    {
        const string _CACHE_NAME = "ViewExternalDeal";
        private static object thisLock = new object();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        /// <summary>
        /// 清掉ViewExternalDeal的Cache
        /// </summary>
        /// <param name="bids"></param>
        public static void ClearViewExternalDealCaches(IEnumerable<Guid> bids)
        {
            MemoryCache2.Remove<Dictionary<Guid, List<ViewExternalDeal>>>(_CACHE_NAME);
        }

        private static void ReloadViewExternalDealCaches()
        {
            var storage = MemoryCache2.Get<Dictionary<Guid, List<ViewExternalDeal>>>(_CACHE_NAME);
            if (storage == null)
            {
                storage = new Dictionary<Guid, List<ViewExternalDeal>>();
                MemoryCache2.Set(_CACHE_NAME, storage, 10);
                var externDeals = pp.ViewExternalDealGetListByNotExpired();
                foreach (var externDeal in externDeals)
                {
                    if (externDeal.Bid == null)
                    {
                        continue;
                    }
                    Guid dealBid = externDeal.Bid.Value;
                    if (storage.ContainsKey(dealBid) == false)
                    {
                        storage.Add(dealBid, new List<ViewExternalDeal>());
                    }
                    storage[dealBid].Add(externDeal);
                }
            }
        }

        /// <summary>
        /// 取得 ExternalDeal
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static List<ViewExternalDeal> GetViewExternalDeals(Guid bid)
        {
            if (bid == Guid.Empty)
            {
                return new List<ViewExternalDeal>();
            }
            Dictionary<Guid, List<ViewExternalDeal>> storage = 
                MemoryCache2.Get<Dictionary<Guid, List<ViewExternalDeal>>>(_CACHE_NAME);
            if (storage == null)
            {
                lock (thisLock)
                {
                    storage = MemoryCache2.Get<Dictionary<Guid, List<ViewExternalDeal>>>(_CACHE_NAME);
                    if (storage == null)
                    {
                        ReloadViewExternalDealCaches();
                    }
                }
            }            
            List<ViewExternalDeal> result;
            storage = MemoryCache2.Get<Dictionary<Guid, List<ViewExternalDeal>>>(_CACHE_NAME);
            if (storage != null && storage.TryGetValue(bid, out result))
            {
                return result;
            }
            return new List<ViewExternalDeal>();
        }
    }
}

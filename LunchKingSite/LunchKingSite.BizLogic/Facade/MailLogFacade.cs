﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.SsBLL.DbContexts;

namespace LunchKingSite.BizLogic.Facade
{
    public class MailLogFacade
    {
        public static List<MailLog> GetMailLogList(QueryMailLogFilter filter)
        {
            using (var ctx = new MailLogDbContext())
            {
                IQueryable<MailLog> query = ctx.MailLogEntities.AsQueryable();
                if (filter.StartDate != null)
                {
                    query = query.Where(t => t.CreateTime > filter.StartDate);
                }
                if (filter.EndDate != null)
                {
                    DateTime endTime = filter.EndDate.Value.AddDays(1);
                    query = query.Where(t => t.CreateTime < endTime);
                }
                if (string.IsNullOrEmpty(filter.Email) == false)
                {
                    query = query.Where(t => t.Receiver == filter.Email);
                }
                if (string.IsNullOrEmpty(filter.Subject) == false)
                {
                    query = query.Where(t => t.Subject.Contains(filter.Subject));
                }
                if (filter.Template != null)
                {
                    query = query.Where(t => t.Category == filter.Template.Value);
                }
                if (filter.Top != null)
                {
                    query = query.Take(filter.Top.Value);
                }
                query = query.OrderByDescending(t => t.Id);
                return query.ToList();
            }
        }
        /// <summary>
        /// 從信件Log的Id取得信件內文
        /// </summary>
        /// <param name="mailLogId"></param>
        /// <returns></returns>
        public static string GetContent(int mailLogId)
        {
            if (mailLogId == 0)
            {
                return string.Empty;
            }
            using (var ctx = new MailLogDbContext())
            {
                using (ctx.NoLock())
                {
                    var query = from m in ctx.MailLogEntities
                        from mc in ctx.MailContentLogEntities
                        where mc.Id == m.MailContentLogId
                              && m.Id == mailLogId
                        select mc;
                    var mailContent = query.FirstOrDefault();
                    return mailContent == null ? string.Empty : mailContent.Content;
                }                
            }
        }

        public static bool Resend(int id)
        {
            using (var ctx = new MailLogDbContext())
            {
                var mailLog = ctx.MailLogEntities.FirstOrDefault(t => t.Id == id);
                if (mailLog != null)
                {
                    var mailContentLog = ctx.MailContentLogEntities.FirstOrDefault(t => t.Id == mailLog.MailContentLogId);
                    if (mailContentLog != null)
                    {
                        MailMessage mm = new MailMessage()
                        {
                            From = new MailAddress(mailLog.Sender),
                            Subject = mailLog.Subject,
                            Sender = new MailAddress(mailLog.Sender),
                            IsBodyHtml = true,
                            BodyEncoding = Encoding.UTF8,
                            HeadersEncoding = Encoding.UTF8
                        };
                        mm.To.Add(new MailAddress(mailLog.Receiver));
                        mm.Body = mailContentLog.Content;
                        return PostMan.Instance().Resend(mm, (MailTemplateType)mailLog.Category, id);
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 搜尋maillog篩選條件
        /// </summary>
        public class QueryMailLogFilter
        {
            /// <summary>
            /// 信箱
            /// </summary>
            public string Email { get; set; }
            /// <summary>
            /// 主旨
            /// </summary>
            public string Subject { get; set; }
            /// <summary>
            /// 發送日期(起)
            /// </summary>
            public DateTime? StartDate { get; set; }
            /// <summary>
            /// 發送日期(迄)
            /// </summary>
            public DateTime? EndDate { get; set; }
            /// <summary>
            /// 分類
            /// </summary>
            public int? Template { get; set; }
            /// <summary>
            /// 是否最多前N筆
            /// </summary>
            public int? Top { get; set; }
        }
    }
}

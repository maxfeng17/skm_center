﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.Vourcher;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Facade
{
    public class VourcherFacade
    {
        private static IEventProvider ep;
        private static IOrderProvider op;
        private static ISellerProvider sp;
        private static IPponProvider pp;

        static VourcherFacade()
        {
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        #region TempSeller

        /// <summary>
        /// 依seller_guid抓取Seller資料
        /// </summary>
        /// <param name="guid">seller_guid</param>
        /// <returns></returns>
        public static Seller SellerGetByGuid(Guid seller_guid)
        {
            return sp.SellerGet(seller_guid);
        }

        /// <summary>
        /// 依seller_id抓取Seller資料
        /// </summary>
        /// <param name="id">seller_id</param>
        /// <returns></returns>
        public static Seller SellerGetById(string seller_id)
        {
            return sp.SellerGet(Seller.Columns.SellerId, seller_id);
        }

        /// <summary>
        /// 儲存賣家
        /// </summary>
        /// <param name="seller">賣家</param>
        public static void SellerSave(Seller seller)
        {
            sp.SellerSet(seller);
        }

        /// <summary>
        /// 儲存分店資料
        /// </summary>
        /// <param name="store">分店</param>
        public static void StoreSave(Store store)
        {
            sp.StoreSet(store);
        }

        /// <summary>
        /// 依分店Guid抓取分店資料
        /// </summary>
        /// <param name="store_guid">分店Guid</param>
        /// <returns></returns>
        public static Store StoreGetByGuid(Guid store_guid)
        {
            return sp.StoreGet(store_guid);
        }

        /// <summary>
        /// 依賣家Guid抓取分店資料
        /// </summary>
        /// <param name="seller_guid">賣家Guid</param>
        /// <returns></returns>
        public static StoreCollection StoreCollectionGetBySellerGuid(Guid seller_guid)
        {
            StoreCollection sts = new StoreCollection();
            sts.AddRange(sp.StoreGetListBySellerGuid(seller_guid).Where(x => x.Status != (int)StoreStatus.Cancel));
            return sts;
        }

        /// <summary>
        /// 搜尋賣家(分頁)
        /// </summary>
        /// <param name="pageStart">分頁起始</param>
        /// <param name="pageLength">每頁大小</param>
        /// <param name="search_keys">搜尋條件(欄位和值)</param>
        /// <param name="orderBy">排序欄位</param>
        /// <returns></returns>
        public static SellerCollection SearchSellerInfo(int pageStart, int pageLength, KeyValuePair<string, string> search_keys, string user_name, string orderBy)
        {
            return sp.SellerGetTempList(pageStart, pageLength, search_keys, user_name, orderBy);
        }

        /// <summary>
        /// 搜尋是否有重複的賣家資料
        /// </summary>
        /// <param name="seller_name">賣家名稱</param>
        /// <param name="signcompany_id">統編</param>
        /// <param name="company_boss_name">負責人</param>
        /// <returns></returns>
        public static SellerCollection SearchRepeatSellers(string seller_name, string signcompany_id, string company_boss_name)
        {
            return sp.SellerGetRepeatList(seller_name, signcompany_id, company_boss_name);
        }

        /// <summary>
        /// 搜尋賣家計算總筆數
        /// </summary>
        /// <param name="search_keys">搜尋條件(欄位和值)</param>
        /// <returns></returns>
        public static int SellerGetCount(KeyValuePair<string, string> search_keys, string user_name)
        {
            return sp.SellerGetTempCount(search_keys, user_name);
        }

        /// <summary>
        /// 搜尋退件的賣家紀錄
        /// </summary>
        /// <param name="username">申請人</param>
        /// <returns></returns>
        public static SellerCollection SellerCollectionReturnCase(string username, SellerTempStatus status)
        {
            return sp.SellerCollectionGetReturnCase(username, status);
        }

        /// <summary>
        /// 搜尋退件的分店紀錄
        /// </summary>
        /// <param name="username">申請人</param>
        /// <returns></returns>
        public static StoreCollection StoreCollectionReturnCase(string username, SellerTempStatus status)
        {
            return sp.StoreCollectionGetReturnCase(username, status);
        }

        public static ViewSellerStoreCollection ViewSellerStoreCollectionReturnCase(string username, SellerTempStatus status)
        {
            return sp.ViewSellerStoreCollectionGetReturnCase(username, status);
        }

        /// <summary>
        /// 新增異動紀錄
        /// </summary>
        /// <param name="change_object_name">Table</param>
        /// <param name="guid">Pkey</param>
        /// <param name="change_log">異動紀錄字串(json)</param>
        public static void ChangeLogAdd(string change_object_name, Guid guid, string change_log, bool enabled)
        {
            pp.ChangeLogInsert(change_object_name, guid.ToString(), change_log, enabled);
        }

        /// <summary>
        /// 抓取最後一筆異動資料
        /// </summary>
        /// <param name="change_object_name">Table</param>
        /// <param name="guid">Pkey</param>
        /// <param name="enabled">啟用否</param>
        /// <returns></returns>
        public static ChangeLog ChangeLogGetLatest(string change_object_name, Guid guid, bool enabled)
        {
            return pp.ChangeLogGetLatest(change_object_name, guid.ToString(), enabled);
        }

        /// <summary>
        /// 抓取異動紀錄列表
        /// </summary>
        /// <param name="change_object_name">Table名稱</param>
        /// <param name="key_val">Pkey</param>
        /// <returns></returns>
        public static ChangeLogCollection ChangeLogCollectionGetAll(string change_object_name, Guid guid)
        {
            return pp.ChangeLogCollectionGetAll(change_object_name, guid.ToString());
        }

        /// <summary>
        /// 更新所有異動紀錄(enable)
        /// </summary>
        /// <param name="change_object_name">Table</param>
        /// <param name="guid">Pkey</param>
        /// <param name="enabled">啟用否</param>
        public static void ChangeLogUpdateEnabled(string change_object_name, Guid guid, bool enabled)
        {
            pp.ChangeLogUpdateEnabled(change_object_name, guid.ToString(), enabled);
        }

        /// <summary>
        /// 抓取銀行分店碼(webmethod)
        /// </summary>
        /// <param name="id">銀行代碼</param>
        /// <returns></returns>
        public static BankInfoCollection BankInfoGetBranchList(string id)
        {
            return op.BankInfoGetBranchList(id);
        }

        /// <summary>
        /// 抓取銀行代碼(webmethod)
        /// </summary>
        /// <returns></returns>
        public static BankInfoCollection BankInfoGetMainList()
        {
            return op.BankInfoGetMainList();
        }
        
        /// <summary>
        /// 確認銀行代碼和名稱是否正確(webmethod)
        /// </summary>
        /// <returns></returns>
        public static bool  IsValidBankInfo(string bankNo,string bankName)
        {
            if (op.BankInfoGetMainList().Where(x => x.BankNo == bankNo && x.BankName == bankName).Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 確認銀行分行代碼和名稱是否正確(webmethod)
        /// </summary>
        /// <returns></returns>
        public static bool IsValidBankBranchInfo(string bankNo, string branchNo, string branchName)
        {
            if (op.BankInfoGetBranchList(bankNo).Where(x => x.BranchNo == branchNo && x.BranchName == branchName).Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 取得所有銀行的編號與名稱
        /// </summary>
        /// <returns></returns>
        public static List<ApiBankNo> ApiBankNoGetList()
        {
            BankInfoCollection bankInfoCollection = op.BankInfoGetMainList();
            List<ApiBankNo> rtnList = new List<ApiBankNo>();
            foreach (var bankInfo in bankInfoCollection)
            {
                ApiBankNo bankNo = new ApiBankNo();
                bankNo.BankNo = bankInfo.BankNo;
                bankNo.BankName = bankInfo.BankName;
                rtnList.Add(bankNo);
            }
            return rtnList;
        }
        /// <summary>
        /// 取得銀行的編號與名稱
        /// </summary>
        /// <returns></returns>
        public static ApiBankNo ApiBankNoGet(string code)
        {
            BankInfo bankInfo = op.BankInfoGet(code);
            List<ApiBankNo> rtnList = new List<ApiBankNo>();
            ApiBankNo bankNo = new ApiBankNo();
            bankNo.BankNo = bankInfo.BankNo;
            bankNo.BankName = bankInfo.BankName;
            return bankNo;
        }
        /// <summary>
        /// 依據銀行編號，查詢此銀行的分行編號與名稱
        /// </summary>
        /// <param name="bankNo"></param>
        /// <returns></returns>
        public static List<ApiBranchNo> ApiBranchNoGetList(string bankNo)
        {
            BankInfoCollection bankInfoCollection = op.BankInfoGetBranchList(bankNo);
            List<ApiBranchNo> rtnList = new List<ApiBranchNo>();
            foreach (var bankInfo in bankInfoCollection)
            {
                ApiBranchNo branchNo = new ApiBranchNo();
                branchNo.BranchNo = bankInfo.BranchNo;
                branchNo.BranchName = bankInfo.BranchName;
                rtnList.Add(branchNo);
            }
            return rtnList;
        }
        /// <summary>
        /// 依據分行編號，查詢此分行編號與名稱
        /// </summary>
        /// <param name="bankNo"></param>
        /// <returns></returns>
        public static ApiBranchNo ApiBranchNoGet(string bankCode, string code)
        {
            BankInfo bankInfo = op.BankInfoGetByBranch(bankCode, code);
            ApiBranchNo branchNo = new ApiBranchNo();
            branchNo.BranchNo = bankInfo.BranchNo;
            branchNo.BranchName = bankInfo.BranchName;
            return branchNo;
        }

        /// <summary>
        /// 抓取城市(webmethod)
        /// </summary>
        /// <returns></returns>
        public static CityCollection CityGetList()
        {
            return CityManager.Citys;
        }

        /// <summary>
        /// 抓取城市的分區(webmethod)
        /// </summary>
        /// <param name="city_id"></param>
        /// <returns></returns>
        public static List<City> TownShipGetListByCityId(int city_id)
        {
            return CityManager.TownShipGetListByCityId(city_id);
        }

        /// <summary>
        /// 依城市id抓取城市
        /// </summary>
        /// <param name="city_id"></param>
        /// <returns></returns>
        public static City CityGetById(int city_id)
        {
            return CityManager.CityGetById(city_id);
        }

        #endregion TempSeller

        #region VourcherEvent

        /// <summary>
        /// 依優惠券Id撈取優惠券內容
        /// </summary>
        /// <param name="event_id">優惠券id</param>
        /// <returns></returns>
        public static VourcherEvent VourcherEventGetById(int event_id)
        {
            return ep.VourcherEventGetById(event_id);
        }

        public static VourcherEventCollection VourcherEventGetBySellerGuid(Guid seller_guid)
        {
            return ep.VourcherEventGetBySellerGuid(seller_guid);
        }

        /// <summary>
        /// 依優惠券Id回傳對應分店
        /// </summary>
        /// <param name="event_id">優惠券id</param>
        /// <returns></returns>
        public static VourcherStoreCollection VourcherStoreCollectionGetByEventId(int event_id)
        {
            return ep.VourcherStoreCollectionGetByEventId(event_id);
        }

        /// <summary>
        /// 依優惠券Id刪除優惠券對應分店資料
        /// </summary>
        /// <param name="event_id">優惠券Id</param>
        public static void VourcherStoreDeleteByEventId(int event_id)
        {
            ep.VourcherStoreDeleteByEventId(event_id);
        }

        public static void VourcherStoreDeleteByStoreGuid(Guid store_guid)
        {
            ep.VourcherStoreDeleteByStoreGuid(store_guid);
        }

        public static List<int> VourcherEventIdListGetByStoreGuid(Guid store_guid)
        {
            return ep.VourcherEventGetByStoreGuid(store_guid).Select(x => x.VourcherEventId).ToList();
        }

        /// <summary>
        /// 儲存優惠券內容
        /// </summary>
        /// <param name="vourcher_event">優惠券資料</param>
        public static void VourcherEventSet(VourcherEvent vourcher_event)
        {
            ep.VourcherEventSet(vourcher_event);
        }

        /// <summary>
        /// 儲存優惠券對應分店
        /// </summary>
        /// <param name="vourcherstores">對應分店資料</param>
        public static void VourcherStoresSet(VourcherStoreCollection vourcherstores)
        {
            ep.VourcherStoresSet(vourcherstores);
        }

        public static ViewVourcherSellerCollection TodayVourcherEventCollectionGet(DateTime todayDateTime)
        {
            return ep.TodayVourcherEventSellerCollectionGet(todayDateTime);
        }


        /// <summary>
        /// 搜尋優惠券(分頁)
        /// </summary>
        /// <param name="pageStart">分頁起始</param>
        /// <param name="pageLength">每頁大小</param>
        /// <param name="search_keys">搜尋條件(欄位和值)</param>
        /// <param name="orderBy">排序欄位</param>
        /// <returns></returns>
        public static ViewVourcherSellerCollection VourcherEventCollectionGetBySearch(int pageStart, int pageLength, KeyValuePair<string, string> search_keys, string orderBy)
        {
            return ep.VourcherEventSellerCollectionGetBySearch(pageStart, pageLength, search_keys, orderBy);
        }

        /// <summary>
        /// 依賣家guid搜尋所有優惠券
        /// </summary>
        /// <param name="seller_guid">賣家guid</param>
        /// <returns></returns>
        public static ViewVourcherSellerCollection ViewVourcherSellerCollectionGetBySellerGuid(Guid seller_guid)
        {
            return ep.ViewVourcherSellerCollectionGetBySellerGuid(seller_guid);
        }

        public static ViewVourcherSeller ViewVourcherSellerGetEnabledById(int eventId)
        {
            ViewVourcherSeller vs = ep.ViewVourcherSellerGetById(eventId);
            if (vs.IsLoaded && vs.StartDate <= DateTime.Now && vs.Enable && vs.Status.Equals((int)VourcherEventStatus.EventChecked))
            {
                return vs;
            }
            else
            {
                return null;
            }
        }

        public static ViewVourcherSeller ViewVourcherSellerGetById(int id)
        {
            return ep.ViewVourcherSellerGetById(id);
        }

        /// <summary>
        /// 搜尋優惠券(分頁)
        /// </summary>
        /// <param name="pageStart">分頁起始</param>
        /// <param name="pageLength">每頁大小</param>
        /// <param name="search_keys">搜尋條件(欄位和值)</param>
        /// <param name="orderBy">排序欄位</param>
        /// <param name="status">狀態</param>
        /// <returns></returns>
        public static ViewVourcherSellerCollection VourcherEventCollectionGetBySearch(int pageStart, int pageLength, KeyValuePair<string, string> search_keys, string orderBy, int status)
        {
            return ep.VourcherEventSellerCollectionGetBySearch(pageStart, pageLength, search_keys, orderBy, status);
        }

        /// <summary>
        /// 搜尋優惠券計算總筆數
        /// </summary>
        /// <param name="search_keys">搜尋條件(欄位和值)</param>
        /// <returns></returns>
        public static int VourcherEventCollectionGetCount(KeyValuePair<string, string> search_keys)
        {
            if (string.IsNullOrEmpty(search_keys.Value))
            {
                return 0;
            }
            else
            {
                return ep.VourcherEventSellerCollectionGetCount(search_keys);
            }
        }

        public static int VourcherEventCollectionGetCount(string username, int status)
        {
            return ep.VourcherEventSellerCollectionGetCount(username, status);
        }

        /// <summary>
        /// 搜尋優惠券計算總筆數
        /// </summary>
        /// <param name="search_keys">搜尋條件(欄位和值)</param>
        /// <param name="status">狀態</param>
        /// <returns></returns>
        public static int VourcherEventCollectionGetCount(KeyValuePair<string, string> search_keys, int status)
        {
            return ep.VourcherEventSellerCollectionGetCount(search_keys, status);
        }

        public static ViewVourcherSellerCollection VourcherEventSellerCollectionGetByStatus(string username, int status)
        {
            return ep.VourcherEventSellerCollectionGetByStatus(username, status);
        }

        public static ViewVourcherSellerCollection VourcherEventSellerCollectionGetByStatus(int pageStart, int pageLength, string orderBy, string username, int status)
        {
            return ep.VourcherEventSellerCollectionGetByStatus(pageStart, pageLength, orderBy, username, status);
        }

        public static void VourcherEventUpdateStatus(int event_id, string message)
        {
            ep.VourcherEventReturnApply(event_id, message);
        }

        public static ViewVourcherStoreCollection ViewVourcherStoreCollectionGetByEventId(int event_id)
        {
            return ep.ViewVourcherStoreCollectionGetByEventId(event_id);
        }

        /// <summary>
        /// 回傳優惠券商家類別的dictionary,key是列舉的int,value是中文說明
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetSellerSampleCategoryDictionary()
        {
            Dictionary<int, string> sellersmplecategory = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(SellerSampleCategory)))
            {
                sellersmplecategory[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerSampleCategory)item);
            }
            return sellersmplecategory;
        }

        /// <summary>
        /// 回傳優惠券商家類別的dictionary, key是列舉的int,value是中文說明
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetVourcherSortTypeDictionary()
        {
            Dictionary<int, string> rtn = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(VourcherSortType)))
            {
                rtn[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VourcherSortType)item);
            }
            return rtn;
        }

        public static Dictionary<int, string> GetSellerConsumptionAvgDictionary()
        {
            Dictionary<int, string> sellerconsumptionavg = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(SellerConsumptionAvg)))
            {
                sellerconsumptionavg[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerConsumptionAvg)item);
            }
            return sellerconsumptionavg;
        }

        public static Dictionary<int, string> GetVourcherEventModeDictionary()
        {
            Dictionary<int, string> vourchereventmode = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(VourcherEventMode)))
            {
                vourchereventmode[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VourcherEventMode)item);
            }
            return vourchereventmode;
        }

        public static Dictionary<int, string> GetVourcherEventTypeDictionary()
        {
            Dictionary<int, string> vourchereventtype = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(VourcherEventType)))
            {
                vourchereventtype[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VourcherEventType)item);
            }
            return vourchereventtype;
        }

        public static Dictionary<int, string> GetVourcherContractTypeDictionary()
        {
            Dictionary<int, string> vourcherContractType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ContractSendStatus)))
            {
                vourcherContractType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ContractSendStatus)item);
            }
            return vourcherContractType;
        }

        public static ViewVourcherSellerCollection VourcherEventSellerCollectionGetSellerId(int pageStart, int pageLength, string seller_id, string orderBy)
        {
            return ep.VourcherEventSellerCollectionGetSellerId(pageStart, pageLength, seller_id, orderBy);
        }

        public static ViewVourcherStoreCollection ViewVourcherStoreCollectionGetByEventId(int pageStart, int pageLength, int event_id, string orderBy)
        {
            return ep.ViewVourcherStoreCollectionGetByEventId(pageStart, pageLength, event_id, orderBy);
        }

        public static ViewVourcherStore ViewVourcherStoreGetById(int id)
        {
            return ep.ViewVourcherStoreGetById(id);
        }

        /// <summary>
        /// 取得優惠券城市區域
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> VourcherCityGroupGetList()
        {
            return pp.CityGroupGetListByType(CityGroupType.Vourcher).GroupBy(x => new KeyValuePair<int, string>(x.RegionId, x.RegionName)).ToDictionary(y => y.Key.Key, y => y.Key.Value);
        }

        /// <summary>
        /// 更新優惠券瀏覽數-異動為輸入的數值
        /// </summary>
        /// <param name="event_id"></param>
        /// <param name="pageCount"></param>
        public static void UpdateVourcherEventPageCount(int event_id, int pageCount)
        {
            ep.VourcherEventUpdatePageCount(event_id, pageCount);
        }

        /// <summary>
        /// 更新優惠券瀏覽數-修改為 PageCount = PageCount + addCount
        /// </summary>
        /// <param name="event_id"></param>
        /// <param name="addCount"></param>
        public static void UpdateVourcherEventPageCountAddCount(int event_id, int addCount)
        {
            ep.VourcherEventAddPageCount(event_id, addCount);
        }

        /// <summary>
        /// 根據城市區域取得賣家優惠券清單
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="regionId"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public static ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeCollectionGetByRegionId(int pageStart, int pageLength, int regionId, string orderBy)
        {
            return ep.ViewVourcherStoreFacadeCollectionGet(pageStart, pageLength, regionId, 0, null, orderBy);
        }

        /// <summary>
        /// 搜尋城市區域id取得賣家優惠券計算總筆數
        /// </summary>
        /// <param name="regionId">城市區域id</param>
        /// <returns></returns>
        public static int ViewVourcherStoreFacadeCollectionGetCountByRegionId(int regionId)
        {
            return ep.ViewVourcherStoreFacadeCollectionGetCount(regionId, 0, null);
        }

        /// <summary>
        /// 根據經緯度取得賣家優惠券清單
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="latitude">緯度座標</param>
        /// <param name="longitude">經度座標</param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public static ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeCollectionGetByCoordinate(int pageStart, int pageLength, int distance, string latitude, string longitude, string orderBy)
        {
            SqlGeography geo = LocationFacade.GetGeographicPoint(latitude, longitude);
            return ep.ViewVourcherStoreFacadeCollectionGet(pageStart, pageLength, 0, distance, geo, orderBy);
        }

        /// <summary>
        /// 搜尋經緯度取得賣家優惠券計算總筆數
        /// </summary>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="latitude">緯度座標</param>
        /// <param name="longitude">經度座標</param>
        /// <returns></returns>
        public static int VourcherEventCollectionGetCount(int distance, string latitude, string longitude)
        {
            SqlGeography geo = LocationFacade.GetGeographicPoint(latitude, longitude);
            return ep.ViewVourcherStoreFacadeCollectionGetCount(0, distance, geo);
        }

        /// <summary>
        /// 收藏優惠券
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="eventId"></param>
        /// <param name="status"></param>
        public static void SetVourcherCollect(int uniqueId, int eventId, VourcherCollectStatus status)
        {
            VourcherCollect collect = ep.VourcherCollectGet(uniqueId, eventId);
            collect.UniqueId = uniqueId;
            collect.EventId = eventId;
            collect.Type = (int)VourcherCollectType.Favorite;
            collect.Status = (int)status;
            ep.VourcherCollectSet(collect);

            if (status.Equals(VourcherCollectStatus.Initial))
            {
                // 收藏時，瀏覽數+1
                UpdateVourcherEventPageCountAddCount(eventId, 1);
            }
        }

        /// <summary>
        /// 查取使用者的優惠券收藏清單
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="uniqueId">使用者id</param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public static ViewVourcherSellerCollectCollection ViewVourcherSellerCollectGetList(int pageStart, int pageLength, int uniqueId, string orderBy)
        {
            return ep.ViewVourcherSellerCollectCollectionGetByUniqueId(pageStart, pageLength, uniqueId, orderBy);
        }

        /// <summary>
        /// 依據 會員編號、可用或不可用的條件 查詢會員收藏的優惠券資料
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="uniqueId"></param>
        /// <param name="orderBy"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static ViewVourcherSellerCollectCollection ViewVourcherSellerCollectGetList(int pageStart, int pageLength, int uniqueId, string orderBy, VourcherGroupMainType type)
        {
            List<string> filters = new List<string>();
            filters.Add(ViewVourcherSellerCollect.Columns.UniqueId + "=" + uniqueId);
            filters.Add(ViewVourcherSellerCollect.Columns.CollectStatus + "=" + (int)VourcherCollectStatus.Initial);
            filters.Add(ViewVourcherSellerCollect.Columns.CollectType + "=" + (int)VourcherCollectType.Favorite);
            if (type == VourcherGroupMainType.Available)
            {
                filters.Add(ViewVourcherSellerCollect.Columns.EndDate + ">=" + DateTime.Now);
            }
            else if (type == VourcherGroupMainType.Overdue)
            {
                filters.Add(ViewVourcherSellerCollect.Columns.EndDate + "<" + DateTime.Now);
            }
            return ep.ViewVourcherSellerCollectGetList(pageStart, pageLength, orderBy, filters.ToArray());
        }

        /// <summary>
        /// 使用優惠券
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="eventId"></param>
        /// <param name="status"></param>
        public static void SetVourcherOrder(int uniqueId, int eventId, VourcherOrderStatus status)
        {
            VourcherOrder order = new VourcherOrder();
            order.UniqueId = uniqueId;
            order.EventId = eventId;
            order.Status = (int)status;
            ep.VourcherOrdertSet(order);

            // 使用時，瀏覽數+1
            UpdateVourcherEventPageCountAddCount(eventId, 1);
        }

        /// <summary>
        /// 查取使用者的優惠券使用清單
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="uniqueId">使用者id</param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public static ViewVourcherSellerOrderCollection ViewVourcherSellerOrderGetList(int pageStart, int pageLength, int uniqueId, string orderBy)
        {
            return ep.ViewVourcherSellerOrderCollectionGetByUniqueId(pageStart, pageLength, uniqueId, orderBy);
        }

        /// <summary>
        /// 搜尋優惠券行銷列表
        /// </summary>
        /// <returns></returns>
        public static ViewVourcherPromoCollection ViewVourcherPromoCollectionGetAll(int promo_type)
        {
            return ep.ViewVourcherPromoCollectionGetAll(promo_type);
        }

        /// <summary>
        /// 刪除優惠券行銷
        /// </summary>
        /// <param name="id">id</param>
        public static void VourcherPromoDelete(int id)
        {
            ep.VourcherPromoDelete(id);
        }

        /// <summary>
        /// 新增或更新優惠券行銷
        /// </summary>
        /// <param name="promo">優惠券行銷</param>
        public static void VourcherPromoSet(VourcherPromo promo)
        {
            ep.VourcherPromoSet(promo);
        }

        /// <summary>
        /// 依id取得優惠券行銷
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        public static VourcherPromo VourcherPromoGet(int id)
        {
            return ep.VourcherPromoGet(id);
        }

        public static VourcherPromoCollection VourcherPromoGetList(int type)
        {
            return ep.VourcherPromoGetList(type);
        }

        public static ViewYahooVoucherPromoCollection ViewYahooVoucherPromoGetList(int type, bool exclude_expired)
        {
            return ep.ViewYahooVoucherPromoGetAvailableList(type, exclude_expired);
        }

        public static VourcherCollectCollection GetVourcherCollect(int uniqueId)
        {
            return ep.VourcherCollectGetByType(uniqueId, VourcherCollectType.Favorite);
        }

        public static VourcherCollect GetVourcherCollect(int uniqueId, int eventId)
        {
            return ep.VourcherCollectGet(uniqueId, eventId);
        }

        public static int VourcherCollectSetDisabledForOverdueEvent(int uniqueId, VourcherCollectType type)
        {
            return ep.VourcherCollectSetDisabledForOverdueEvent(uniqueId, type);
        }

        public static void CategoryVourchersSet(CategoryVourcherCollection cvs, int vourcherId)
        {
            pp.CategoryVourcherSave(cvs, vourcherId);
        }

        public static CategoryVourcherCollection CategoryVourchersGetList(int vourcherId)
        {
            return pp.CategoryVourcherGetList(vourcherId);
        }

        public static Dictionary<int, string> GetVoucherCategoryList()
        {
            return sp.ViewCategoryDependencyGetByParentType((int)CategoryType.Voucher).Where(x => x.CategoryType == (int)CategoryType.VoucherCategory).ToDictionary(x => x.CategoryId, x => x.CategoryName);
        }
        #endregion VourcherEvent

        #region VourcherEventStoreDocument

        public static int ArrangeVourcherSellerDocument()
        {
            ViewVourcherStoreFacadeCollection storeFacadeCollection = ep.ViewVourcherStoreFacadeGetList(0, 0, ViewVourcherStoreFacade.Columns.SellerGuid, null);
            List<VourcherSellerDocument> arrangeData = new List<VourcherSellerDocument>();
            var sellerGroupData = storeFacadeCollection.GroupBy(x => new { x.SellerId, x.SellerName }).ToList();
            Parallel.ForEach(sellerGroupData, groupData =>
            {
                VourcherSellerDocument document = new VourcherSellerDocument();
                ViewVourcherStoreFacade storeFacade = groupData.FirstOrDefault();
                if (storeFacade == null)
                {
                    return;
                }
                document.SellerGuid = storeFacade.SellerGuid;
                document.SellerId = storeFacade.SellerId;
                document.SellerName = storeFacade.SellerName;
                string eventPicUrl = storeFacade.EventPicUrl;
                document.SellerLogoimgPath = GetSellerLogoImageUrl(storeFacade.SellerLogoimgPath, eventPicUrl);
                document.SellerCategory = storeFacade.SellerCategory == null ? SellerSampleCategory.Others : (SellerSampleCategory)storeFacade.SellerCategory.Value;
                document.VourcherCount = groupData.GroupBy(y => y.VourcherEventId).Count();
                foreach (var storeGroup in groupData.GroupBy(x => x.StoreGuid))
                {
                    ViewVourcherStoreFacade firstData = storeGroup.FirstOrDefault();
                    if (firstData == null)
                    {
                        continue;
                    }
                    //分店部分
                    VourcherStoreSynopsesDocument storeDocument = new VourcherStoreSynopsesDocument();
                    storeDocument.StoreName = firstData.StoreName;
                    SqlGeography storeGeo =
                        SqlGeography.STGeomFromText(new SqlChars(new SqlString(firstData.Coordinate)), 4326);
                    storeDocument.StoreLatitude = (double)storeGeo.Lat;
                    storeDocument.StoreLongitude = (double)storeGeo.Long;
                    storeDocument.EventCount = storeGroup.Count();
                    storeDocument.EventIdList = new List<int>();
                    foreach (ViewVourcherStoreFacade eventData in storeGroup)
                    {
                        storeDocument.EventIdList.Add(eventData.Id);
                        VourcherEventSynopsesDocument eventDocument = new VourcherEventSynopsesDocument();
                        eventDocument.EventId = eventData.Id;
                        eventDocument.VourcherContent = eventData.Contents;
                        eventDocument.StartDate = eventData.StartDate == null
                                                      ? DateTime.MinValue
                                                      : eventData.StartDate.Value;
                        document.EventList.Add(eventDocument);
                    }
                    document.StoreList.Add(storeDocument);
                }
                //vourcherservuce SaveVourcherSellerDocument
                arrangeData.Add(document);
            }
            );

            return 1;
        }

        #region private

        /// <summary>
        /// 傳入 seller.SellerLogoimgPath 紀錄的字串與 vourcherEvent.PicUrl 紀錄的字串，依據規則回傳顯示於優惠券商家列表上的圖片URL路徑
        /// </summary>
        /// <param name="sellerLogoPathString"></param>
        /// <param name="eventPicUrlString"></param>
        /// <returns></returns>
        public static string GetSellerLogoImageUrl(string sellerLogoPathString, string eventPicUrlString)
        {
            string[] sellerImagePaths = ImageFacade.GetMediaPathsFromRawData(sellerLogoPathString, MediaType.SellerPhotoLarge);
            string[] eventImagePaths = ImageFacade.GetMediaPathsFromRawData(eventPicUrlString, MediaType.PponDealPhoto);
            return GetSellerLogoImageUrlByArray(sellerImagePaths, eventImagePaths);
        }

        /// <summary>
        /// 傳入seller.seller_logoimg_path轉化成的圖片URL路徑陣列與 vourcher_event.pic_url轉化成的圖片URL路徑陣列
        /// 回傳seller於列表中應顯示的代表圖片URL
        /// </summary>
        /// <param name="sellerImagePaths"></param>
        /// <param name="eventImagePaths"></param>
        /// <returns></returns>
        public static string GetSellerLogoImageUrlByArray(string[] sellerImagePaths, string[] eventImagePaths)
        {
            string rtnImagePath = string.Empty;
            if (sellerImagePaths.Length > 0)
            {
                //有商家圖片，依據順位顯示
                foreach (string path in sellerImagePaths)
                {
                    if (!string.IsNullOrWhiteSpace(path))
                    {
                        rtnImagePath = path;
                        break;
                    }
                }
            }
            else
            {
                //沒有商家圖片，以優惠券圖片為主
                foreach (string path in eventImagePaths)
                {
                    if (!string.IsNullOrWhiteSpace(path))
                    {
                        rtnImagePath = path;
                        break;
                    }
                }
            }
            return rtnImagePath;
        }

        #endregion private

        #endregion VourcherEventStoreDocument
    }
}
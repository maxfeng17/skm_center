﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models.QueueModels;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using Newtonsoft.Json;


namespace LunchKingSite.BizLogic.Facade
{
    public class SystemFacade
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(SystemFacade));
        private static IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
        private static ISerializer serializer = ProviderFactory.Instance().GetSerializer();
        public const string _CLEAR_CACHE = "ClearCache";
        public const string _API_SYSTEM_MANAGER = "ApiSystemManager";
        public const string _API_PROMO_EVENT_MANAGER = "ApiPromoEventManager";
        public const string _MEMBER_ROLE_MANAGEMENT = "MemberRoleManagement";
        public const string _CURATION_CLEAR_CACHE = "Curation";
        public const string _CHANNEL_SELLER_TREE_CLEAR = "ChannelSellerTreeClear";
        public const string _RESET_FOR_DEAL_TIME_SLOTS_CHANGE = "ResetForDealTimeSlotsChange";
        public const string _CLEAR_CACHE_BY_OBJ = "ClearCacheByObject";
        public const string _RELOAD_CATEGORY = "ReloadCategory";
        public const string _SET_RUNTIME_CONFIG = "SetRuntimeConfig";
        public const string _INCR_IMG_VER = "IncrImgVer";

        /// <summary>
        /// 儲存LOG紀錄
        /// </summary>
        /// <param name="methodName">函式名稱</param>
        /// <param name="apiUserId">呼叫函式的apiUserId</param>
        /// <param name="parameter">物件，紀錄傳入參數</param>
        /// <param name="returnValue">物件，紀錄回傳參數</param>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool SetApiLog(string methodName, string apiUserId, object parameter, object returnValue, string ip = "")
        {
            if (config.SetApiLog == false)
            {
                return true;
            }
            if (ip == string.Empty)
            {
                ip = Helper.GetClientIP();
            }
            var apiLog = new ApiLogModel
            {
                MethodName = methodName,
                RequestTime = DateTime.Now,
                ApiUserId = apiUserId,
                Parameter = parameter,
                ReturnValue = returnValue,
                Ip = ip
            };
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.User != null &&
                System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                apiLog.UserId = MemberFacade.GetUniqueId(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                if (logger.IsDebugEnabled)
                {
                    logger.Debug(JsonConvert.SerializeObject(apiLog, Formatting.Indented));
                }
                mqp.Send(ApiLogModel._QUEUE_NAME, apiLog);
                return true;
            }
            catch (Exception)
            {
                return true;
            }
        }

        /// <summary>
        /// 依群組名稱，清除Cache
        /// </summary>
        /// <param name="groupName"></param>
        public static void ClearCacheByGroup(string groupName)
        {
            MemoryCache2.RemoveByGroup(groupName);
        }

        public static void ClearCacheByCacheKey<T>(string cacheKey)
        {
            MemoryCache2.Remove<T>(cacheKey);
        }

        public static void ClearCacheByCacheKey(Type type, string cacheKey)
        {
            MemoryCache2.Remove(type, cacheKey);
        }

        /// <summary>
        /// 依物件名稱及cacheKey清除快取
        /// 
        /// 將改名為 ClearAllServersCache
        /// 
        /// </summary>
        /// <param name="objectName"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public static string ClearCacheByObjectNameAndCacheKey(string objectName, string cacheKey)
        {
            var serverIPs = serializer.Deserialize<SortedDictionary<string, string>>(config.ServerIPs);
            return SyncCommand(serverIPs, _CLEAR_CACHE, objectName, cacheKey);
        }

        public static string ClearAllServersCache<T>(string cacheKey) where T : class, new()
        {
            var serverIPs = serializer.Deserialize<SortedDictionary<string, string>>(config.ServerIPs);
            return SyncCommand(serverIPs, _CLEAR_CACHE_BY_OBJ, typeof(T).FullName, cacheKey);
        }

        public static string IncrementImageVer()
        {
            var serverIPs = serializer.Deserialize<SortedDictionary<string, string>>(config.ServerIPs);
            return SyncCommand(serverIPs, _INCR_IMG_VER, null, null);
        }
        /// <summary>
        /// 呼叫每一臺web server 設定小web.config值
        /// </summary>
        /// <param name="configKey"></param>
        /// <param name="configValue"></param>
        /// <returns></returns>
        public static bool SetAllServersRuntimeConfig(string configKey, string configValue, out string message)
        {
            var serverIPs = serializer.Deserialize<SortedDictionary<string, string>>(config.ServerIPs);
            return SyncCommand(serverIPs, _SET_RUNTIME_CONFIG, configKey, configValue, out message);
        }

        public static string SyncCommand(SortedDictionary<string, string> serverIPs, string command, string argumentName, string argumentValue)
        {
            string message;
            SyncCommand(serverIPs, command, argumentName, argumentValue, out message);
            return message;
        }

        public static bool SyncCommand(SortedDictionary<string, string> serverIPs, string command,
            string argumentName, string argumentValue, out string message)
        {
            StringBuilder sb = new StringBuilder();
            bool result = true;
            foreach (string key in serverIPs.Keys)
            {
                try
                {
                    string value = serverIPs[key];
                    HttpClient client = new HttpClient
                    {
                        BaseAddress = new Uri(string.Format("http://{0}/", value)),
                        Timeout = TimeSpan.FromSeconds(30)
                    };
                    //關掉防護憑證的驗證，只有在存取已知安全的網站，才得以做關閉。
                    ServicePointManager.ServerCertificateValidationCallback =
                        delegate { return true; };
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // HTTP POST
                    var model = new { Command = command, ArgumentName = argumentName, ArgumentValue = argumentValue };
                    HttpResponseMessage response = client.PostAsJsonAsync("api/system/command", model).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        sb.AppendFormat("{0} 更新完成.\r\n", key);
                    }
                    else
                    {
                        string errorMessage = response.Content.ReadAsStringAsync().Result;
                        logger.Info(errorMessage);
                        sb.AppendFormat("{0} 發生未預期的狀況.\r\n", key);
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    logger.WarnFormat("SyncCommand error. command={0}, arg={1}, target={2}, ex={3}",
                        command, argumentName, key, ex);
                }
            }
            message = sb.ToString();
            return result;
        }

        public static bool RefreshExternalSearchEngine(string userName, out string message)
        {
            message = string.Empty;
            try
            {
                HttpClient client = new HttpClient
                {
                    BaseAddress = new Uri(config.SearchApiUrl)
                };
                //關掉防護憑證的驗證，只有在存取已知安全的網站，才得以做關閉。
                ServicePointManager.ServerCertificateValidationCallback =
                    delegate { return true; };
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // HTTP POST            
                HttpResponseMessage response = client.GetAsync("api/refresh?userName=" + userName).Result;
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    string errorMessage = response.Content.ReadAsStringAsync().Result;
                    logger.Warn(errorMessage);
                    message = string.Format("發生未預期的狀況. error={0}", errorMessage);
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Warn("RefreshExternalSearchEngine error, ex=" + ex);
                message = string.Format("發生未預期的狀況. error={0}", ex);
                return false;
            }
        }
    }
}

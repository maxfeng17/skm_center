﻿using System.Collections.Generic;
using Amazon.EC2.Model;
using Amazon.IdentityManagement.Model;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.SellerModels;
using LunchKingSite.BizLogic.Models.SellerModels;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Constant;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Linq;
using System.Resources;
using log4net;
using SellerTree = LunchKingSite.DataOrm.SellerTree;
using LunchKingSite.BizLogic.Model.SellerSales;
using System.Transactions;
using LunchKingSite.BizLogic.Model.API;

namespace LunchKingSite.BizLogic.Facade
{
    public class SellerFacade
    {
        private static IPponProvider pp;
        private static ISellerProvider sp;
        private static ILocationProvider lp;
        private static IHumanProvider hp;
        private static IMemberProvider mp;
        private static ISystemProvider sys;
        private static ILog logger;
        public static ISysConfProvider config;
        public const string _SELLERID_SERIAL_NUMBER = "seller_id";
        static SellerFacade()
        {
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            sys = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            logger = LogManager.GetLogger(typeof(SellerFacade));
            config = ProviderFactory.Instance().GetConfig();
        }

        private SellerFacade()
        {
        }

        /// <summary>
        /// Gets the department path.
        /// </summary>
        /// <param name="dept">The dept.</param>
        /// <returns>path regard to the department</returns>
        /// <remarks>if the department is unrecognized, it'll defalut to meal and log it</remarks>
        public static string GetDepartmentPath(DepartmentTypes dept)
        {
            string url = string.Empty;
            switch (dept)
            {
                case DepartmentTypes.Meal:
                default:
                    url = "meal";
                    break;

                case DepartmentTypes.Cosmetics:
                    url = "cosmetics";
                    break;

                case DepartmentTypes.Delicacies:
                    url = "pprod";
                    break;

                case DepartmentTypes.Ppon:
                    url = "ppon";
                    break;
            }
            return url;
        }

        public static string GetDepartmentPath(int dept)
        {
            return GetDepartmentPath((DepartmentTypes)dept);
        }

        public static IEnumerable<Store> StoreGetListBySidOrBid(Guid sid, Guid bid, int skip, int n)
        {
            StoreCollection sc = bid == Guid.Empty ? sp.StoreGetListBySellerGuid(sid) : sp.StoreGetListByBid(bid);
            return n < 0 ? sc.Where(x => x.Status == (int)StoreStatus.Available).Skip(skip) : sc.Where(x => x.Status == (int)StoreStatus.Available).Take(n);
        }

        public static ViewPponDealCollection GetRemindVerificationDealList()
        {
            ViewPponDealCollection dealList = pp.ViewPponDealGetVerificationNoticeList();
            return dealList;
        }

        public static SellerTransportConfidence GetTransportConfidence(int sellerStatus)
        {
            return (SellerTransportConfidence)((sellerStatus & (int)SellerStatusFlag.TransportConfidenceMask) >> (int)SellerStatusBitShift.TransportConfidence);
        }

        public static int SetTransportConfidence(int origStatus, SellerTransportConfidence confidence)
        {
            return (origStatus & ~(int)SellerStatusFlag.TransportConfidenceMask) |
                   ((int)confidence << (int)SellerStatusBitShift.TransportConfidence);
        }

        public static RewardType GetRewardType(int sellerStatus)
        {
            return (RewardType)((sellerStatus & (int)SellerStatusFlag.RewardMask) >> (int)SellerStatusBitShift.Reward);
        }

        public static int SetRewardType(int origStatus, RewardType reward)
        {
            return (origStatus & ~(int)SellerStatusFlag.RewardMask) |
                   ((int)reward << (int)SellerStatusBitShift.Reward);
        }

        public static SellerOnlineType GetOnlineType(int sellerStatus)
        {
            return (SellerOnlineType)((sellerStatus & (int)SellerStatusFlag.OnlineMask) >> (int)SellerStatusBitShift.OnlineType);
        }

        public static int SetOnlineType(int origStatus, SellerOnlineType type)
        {
            return (origStatus & ~(int)SellerStatusFlag.OnlineMask) |
                   ((int)type << (int)SellerStatusBitShift.OnlineType);
        }

        public static string GetCityAndTownShipNameByCityId(int cityId)
        {
            string rtn = string.Empty;
            City city = lp.CityGet(cityId);
            if (city.IsLoaded)
            {
                if (city.ParentId != null)
                {
                    City parentCity = lp.CityGet(city.ParentId.Value);
                    rtn = parentCity.CityName;
                }
                rtn = rtn + city.CityName;
            }
            return rtn;
        }

        public static bool DeleteContractFile(Guid guid)
        {
            SellerContractFile scf = sp.SellerContractFileGetByGuid(guid);
            if (scf != null)
            {
                scf.Status = -1;
                sp.SellerContractFileSet(scf);
                return true;
            }
            return false;
        }
        public static void SellerLogSet(Guid sid, string changeLog, string userName)
        {
            SellerChangeLog log = new SellerChangeLog();
            log.SellerGuid = sid;
            log.ChangeLog = changeLog;
            log.CreateId = userName;
            log.CreateTime = DateTime.Now;
            sp.SellerChangeLogSet(log);
        }
        public static void SellerManageLogSet(Guid sid, short developStatus, short sellerGrade, string changeLog, string userName)
        {
            SellerManageLog log = new SellerManageLog();
            log.SellerGuid = sid;
            log.DevelopStatus = developStatus;
            log.SellerGrade = sellerGrade;
            log.ChangeLog = changeLog;
            log.CreateId = userName;
            log.CreateTime = DateTime.Now;
            sp.SellerManageLogSet(log);
        }

        /// <summary>
        /// 查詢未離職之業務員的名稱
        /// </summary>
        public static List<string> GetSalesmanNameArray(string userName)
        {
            var empCollection = hp.EmployeeCollectionGetByDepartment(EmployeeDept.S000).Where(x => !x.IsInvisible);
            var memCollection = mp.MemberGetList(empCollection.Select(x => x.UserId).ToList());
            if (!string.IsNullOrEmpty(userName))
            {
                return memCollection.Where(x => x.UserName.Contains(userName)).Select(x => x.UserName).ToList();
            }
            return memCollection.Select(x => x.UserName).Take(20).ToList();
        }

        /// <summary>
        /// 取得宅配員工資料
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static List<Employee> GetToHouseSalesmanName(string userName)
        {
            var empCollection = hp.EmployeeCollectionGetByDepartment(EmployeeDept.S000).Where(x => !x.IsInvisible);
            return empCollection.Where(x => x.EmpName.ToLower().Contains(userName.ToLower()) && x.DeptId == EmployeeChildDept.S010.ToString()).ToList();

        }

        /// <summary>
        /// 查詢未離職之員工名稱
        /// </summary>
        public static List<string> GetEmployeeNameArray(string empName)
        {
            var empCollection = hp.ViewEmployeeCollectionGetAll().Where(x => !x.IsInvisible);
            var memCollection = mp.MemberGetList(empCollection.Select(x => x.UserId).ToList());
            if (!string.IsNullOrEmpty(empName))
            {
                return memCollection.Where(x => x.UserName.Contains(empName)).Select(x => x.UserName).ToList();
            }
            return memCollection.Select(x => x.UserName).Take(20).ToList();
        }

        public static int GetEmployeeIdArray(string empName)
        {
            var empCollection = hp.ViewEmployeeCollectionGetAll().Where(x => !x.IsInvisible && x.EmpName.Equals(empName));
            var memCollection = mp.MemberGetList(empCollection.Select(x => x.UserId).ToList());
            if (memCollection.Count > 0)
            {
                return memCollection.Select(x => x.UniqueId).FirstOrDefault();
            }
            return 0;
        }

        /// <summary>
        /// 查詢未離職之員工名稱by部門
        /// </summary>
        public static Dictionary<int, string> GetEmployeeNameArrayByDept(string empName, List<Department> dept)
        {
            var empCollection = hp.ViewEmployeeCollectionGetAll().Where(x => !x.IsInvisible && x.DeptId.EqualsAny(dept.Select(y => y.DeptId).ToArray()))
                .Select(x => new { UserId = x.UserId, EmpName = x.EmpName })
                .Distinct();
            if (!string.IsNullOrEmpty(empName))
            {
                Dictionary<int, string> data = new Dictionary<int, string>();
                var ret = empCollection.Where(x => x.EmpName.ToLower().Contains(empName.ToLower())).Take(20);
                foreach (var e in ret)
                {
                    if (!data.ContainsKey(e.UserId))
                    {
                        data.Add(e.UserId, e.EmpName);
                    }
                }
                return data;
            }
            return empCollection.Take(20).ToDictionary(x => x.UserId, x => x.EmpName);
        }

        /// <summary>
        /// 查詢賣家資料
        /// </summary>
        public static Dictionary<string, string> GetSellersByName(string sellerName, string UserName)
        {
            Dictionary<string, string> rtnValue = new Dictionary<string, string>();

            Member mem = mp.MemberGetByTrueEmail(UserName);
            int salesId = mem.UniqueId;
            Employee emp = hp.EmployeeCollectionGetByFilter(Employee.Columns.UserId, salesId).FirstOrDefault();

            string salesName = "";
            if (emp != null)
            {
                salesName = emp.EmpName;
            }
            int MaxLength = 20;
            List<string> filter = new List<string>();

            if (!string.IsNullOrWhiteSpace(sellerName))
            {
                filter.Add(Seller.Columns.SellerName + " like %" + sellerName.ToLower().Replace("in", "%").Replace("between", "%").Replace("like", "%").Replace("null", "%") + "%");
            }
            var sellers = sp.SellerGetList(1, 20, "", filter.ToArray());
            var data = sellers.Take(20);
            //先找賣家名稱類似的資料
            foreach (var seller in data)
            {
                if (rtnValue.Count > MaxLength)
                {
                    continue;
                }
                if (!rtnValue.ContainsKey(seller.SellerId))
                {
                    if ((string.IsNullOrEmpty(seller.SellerSales) || seller.SellerSales == salesName))
                    {
                        rtnValue.Add(seller.SellerId, seller.SellerName);
                    }

                }
            }

            //再找賣家下層     

            foreach (var d in data)
            {
                //找出root
                List<Guid> sids = new List<Guid>();
                Guid sid = GetRootSellerGuid(d.Guid);
                sids.Add(sid);
                sids.Add(d.Guid);
                SellerTreeCollection trees = sp.SellerTreeGetListByParentSellerGuid(sids);

                Seller s = sp.SellerGet(sid);
                if (!rtnValue.ContainsKey(s.SellerId))
                {
                    if ((string.IsNullOrEmpty(s.SellerSales) || s.SellerSales == salesName))
                    {
                        rtnValue.Add(s.SellerId, s.SellerName);
                    }
                }

                foreach (SellerTree tree in trees)
                {
                    if (rtnValue.Count > MaxLength)
                    {
                        continue;
                    }
                    Seller seller = sp.SellerGet(tree.SellerGuid);
                    if (!rtnValue.ContainsKey(seller.SellerId))
                    {
                        if ((string.IsNullOrEmpty(seller.SellerSales) || seller.SellerSales == salesName))
                        {
                            rtnValue.Add(seller.SellerId, seller.SellerName);
                        }
                    }
                }
            }
            return rtnValue.Take(20).ToDictionary(x => x.Key, x => x.Value);
        }

        /// <summary>
        /// 查詢未離職之創意部的名稱
        /// </summary>
        public static List<string> GetProductionNameArray(string userName)
        {
            var empPCollection = hp.EmployeeCollectionGetByDepartment(EmployeeDept.P000).Where(x => !x.IsInvisible);
            var empQCollection = hp.EmployeeCollectionGetByDepartment(EmployeeDept.Q000).Where(x => !x.IsInvisible);
            var memCollection = mp.MemberGetList(empPCollection.Select(x => x.UserId).ToList());
            memCollection.AddRange(mp.MemberGetList(empQCollection.Select(x => x.UserId).ToList()));

            if (!string.IsNullOrEmpty(userName))
            {
                return memCollection.Where(x => x.UserName.Contains(userName)).Select(x => x.UserName).ToList();
            }
            return memCollection.Select(x => x.UserName).Take(20).ToList();
        }

        /// <summary>
        /// 是否為該部門的人員
        /// </summary>
        public static bool IsDepartment(EmployeeDept dept, string userName)
        {

            var empCollection = hp.ViewEmployeeCollectionGetByDepartment(dept).Where(x => x.Email == userName);

            if (empCollection.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 取得Seller某一欄位包含"片段字詞"的列表 (前端autocomplete用)
        /// </summary>
        /// <param name="column">欄位名</param>
        /// <param name="partValue">片段字詞</param>
        /// <returns></returns>
        public static List<string> SellerGetColumnListByLike(string column, string partValue)
        {
            List<string> list = new List<string>();
            if (!string.IsNullOrWhiteSpace(partValue) && !string.IsNullOrWhiteSpace(column))
            {
                list = sp.SellerGetColumnListByLike(column, partValue.Trim());
            }
            return list;
        }

        public static SellerCollection GetSellerByCityId(int cityId)
        {
            return sp.SellerGetList(Seller.Columns.CityId, cityId);
        }

        public static string GetNewSellerID()
        {
            int n = sys.SerialNumberGetNext(_SELLERID_SERIAL_NUMBER);
            //A0-10-431
            //{prefix}{num1}-{num2}-{num3}
            int num1, num2, num3;
            int num0 = n / 1000000;
            char prefix = (char)('A' + num0);
            n = n % 1000000;
            num1 = n / 100000;
            n = n % 100000;
            num2 = n / 1000;
            num3 = n % 1000;
            if (prefix == 'Z' && num1 == 9)
            {
                logger.Warn("GetNewSellerId 編號即將用盡，請儘速處理");
            }
            if (prefix > 'Z')
            {
                logger.Warn("GetNewSellerId 編號已用盡.");
                return null;
            }
            return string.Format("{0}{1}-{2:00}-{3:000}", prefix, num1, num2, num3);
        }

        public static PagerList<ViewSellerDeal> GetViewSellerDealList(string searchItem, string searchValue,
            bool showLkSeller, int page)
        {
            if (string.IsNullOrEmpty(searchValue))
            {
                return PagerList<ViewSellerDeal>.Empty;
            }
            int pageSize = 15;
            int count;
            PagerList<ViewSellerDeal> result = new PagerList<ViewSellerDeal>(
                sp.ViewSellerDealGetList(searchItem, searchValue, showLkSeller, page, pageSize, out count).Skip((page - 1) * pageSize).Take(pageSize).ToList(),
                new PagerOption(pageSize, (page - 1) * pageSize),
                page - 1,
                count);
            return result;
        }

        public static void AuditStoreSet(SellerStoreModel model, Store store)
        {
            string aduitStr = string.Empty;

            if (store.StoreName != model.BranchName)
            {
                aduitStr += "StoreName : " + store.StoreName + " ->" + model.BranchName + "<br />";
            }

            if (store.SellerGuid != model.SellerGuid)
            {
                aduitStr += "SellerGuid : " + store.SellerGuid + " ->" + model.SellerGuid + "<br />";
            }

            if (store.Phone != model.StoreTel)
            {
                aduitStr += "Phone : " + store.Phone + " ->" + model.StoreTel + "<br />";
            }

            if (store.TownshipId != model.TownshipId)
            {
                aduitStr += "TownshipId : " + store.TownshipId + " ->" + model.TownshipId + "<br />";
            }

            if (store.AddressString != model.StoreAddress)
            {
                aduitStr += "AddressString : " + store.AddressString + " ->" + model.StoreAddress + "<br />";
            }

            if (store.OpenTime != model.BusinessHour)
            {
                aduitStr += "OpenTime : " + store.OpenTime + " ->" + model.BusinessHour + "<br />";
            }

            if (store.CloseDate != model.CloseDateInformation)
            {
                aduitStr += "CloseDate : " + store.CloseDate + " ->" + model.CloseDateInformation + "<br />";
            }

            if (store.IsOpenBooking != model.IsOpenBooking)
            {
                aduitStr += "IsOpenBooking : " + store.IsOpenBooking + " ->" + model.IsOpenBooking + "<br />";
            }

            if (store.IsOpenReservationSetting != model.IsOpenReservationSetting)
            {
                aduitStr += "IsOpenReservationSetting : " + store.IsOpenReservationSetting + " ->" + model.IsOpenReservationSetting + "<br />";
            }

            if (store.Status != (int)model.Status)
            {
                aduitStr += "Status : " + store.Status + " ->" + model.Status + "<br />";
            }

            if (store.IsCloseDown != model.IsCloseDown)
            {
                aduitStr += "IsCloseDown : " + store.IsCloseDown + " ->" + model.IsCloseDown + "<br />";
            }

            if (store.Remarks != model.Remark)
            {
                aduitStr += "Remarks : " + store.Remarks + " ->" + model.Remark + "<br />";
            }

            if (store.Mrt != model.TrafficMrt)
            {
                aduitStr += "Mrt : " + store.Mrt + " ->" + model.TrafficMrt + "<br />";
            }

            if (store.Car != model.TrafficCar)
            {
                aduitStr += "Car : " + store.Car + " ->" + model.TrafficCar + "<br />";
            }

            if (store.Bus != model.TrafficBus)
            {
                aduitStr += "Bus : " + store.Bus + " ->" + model.TrafficBus + "<br />";
            }

            if (store.OtherVehicles != model.TrafficOther)
            {
                aduitStr += "OtherVehicles : " + store.OtherVehicles + " ->" + model.TrafficOther + "<br />";
            }

            if (store.WebUrl != model.WebUrl)
            {
                aduitStr += "WebUrl : " + store.WebUrl + " ->" + model.WebUrl + "<br />";
            }

            if (store.FacebookUrl != model.FaceBookUrl)
            {
                aduitStr += "FacebookUrl : " + store.FacebookUrl + " ->" + model.FaceBookUrl + "<br />";
            }

            if (store.PlurkUrl != model.PlurkUrl)
            {
                aduitStr += "PlurkUrl : " + store.PlurkUrl + " ->" + model.PlurkUrl + "<br />";
            }

            if (store.BlogUrl != model.BlogUrl)
            {
                aduitStr += "BlogUrl : " + store.BlogUrl + " ->" + model.BlogUrl + "<br />";
            }

            if (store.OtherUrl != model.OtherUrl)
            {
                aduitStr += "OtherUrl : " + store.OtherUrl + " ->" + model.OtherUrl + "<br />";
            }

            if (store.CompanyAccount != model.CompanyAccount)
            {
                aduitStr += "CompanyAccount : " + store.CompanyAccount + " ->" + model.CompanyAccount + "<br />";
            }

            if (store.CompanyAccountName != model.CompanyAccountName)
            {
                aduitStr += "CompanyAccountName : " + store.CompanyAccountName + " ->" + model.CompanyAccountName + "<br />";
            }

            if (store.CompanyBankCode != model.CompanyBankCode)
            {
                aduitStr += "CompanyBankCode : " + store.CompanyBankCode + " ->" + model.CompanyBankCode + "<br />";
            }

            if (store.CompanyBossName != model.CompanyBossName)
            {
                aduitStr += "CompanyBossName : " + store.CompanyBossName + " ->" + model.CompanyBossName + "<br />";
            }

            if (store.CompanyBranchCode != model.CompanyBranchCode)
            {
                aduitStr += "CompanyBranchCode : " + store.CompanyBranchCode + " ->" + model.CompanyBranchCode + "<br />";
            }

            if (store.CompanyEmail != model.CompanyEmail)
            {
                aduitStr += "CompanyEmail : " + store.CompanyEmail + " ->" + model.CompanyEmail + "<br />";
            }

            if (store.CompanyID != model.CompanyID)
            {
                aduitStr += "CompanyID : " + store.CompanyID + " ->" + model.CompanyID + "<br />";
            }

            if (store.CompanyName != model.CompanyName)
            {
                aduitStr += "CompanyName : " + store.CompanyName + " ->" + model.CompanyName + "<br />";
            }

            if (store.CompanyNotice != model.CompanyNotice)
            {
                aduitStr += "CompanyNotice : " + store.CompanyNotice + " ->" + model.CompanyNotice + "<br />";
            }

            if (aduitStr.Length > 0)
            {
                CommonFacade.AddAudit(model.StoreGuid, AuditType.Store, aduitStr, model.UserName, true);
            }

        }

        #region view_vbs_seller_member

        /// <summary>
        /// 取得商家會員資料(含商家自行建立)
        /// </summary>
        /// <param name="sellerUserId"></param>
        /// <param name="sellerMemberType"></param>
        /// <param name="criteria">查詢條件;string;針對姓名或會員卡編號進行查詢，若不查詢回傳空字串</param>
        /// <param name="start">start index, first is 1</param>
        /// <param name="size">起始數往後捉幾筆</param>
        /// <param name="level">篩選的會員等級</param>
        /// <param name="newMember">是否只查詢新會員</param>
        /// <returns></returns>
        public static ViewVbsSellerMemberCollection GetViewVbsSellerMember(int sellerUserId, SellerMemberType sellerMemberType, string criteria, int start, int size, MembershipCardLevel? level, bool newMember)
        {
            List<string> filters = new List<string>();
            if (level.HasValue)
            {
                filters.Add(string.Format("{0} = {1}", ViewVbsSellerMember.Columns.Level, (int)level.Value));
            }
            //只查詢新會員，限定一個月內為新會員
            if (newMember)
            {
                filters.Add(string.Format("{0} > {1}", ViewVbsSellerMember.Columns.RequestTime, DateTime.Now.AddMonths(-1)));
            }
            return sp.ViewVbsSellerMemberGet(sellerUserId, sellerMemberType, criteria, start, size, filters.ToArray());
        }

        public static ViewVbsSellerMember GetViewVbsSellerMember(int sellerUserId, int? userId, int? sellerMemberId)
        {
            var result = new ViewVbsSellerMember();

            if (userId != null || sellerMemberId != null)
            {
                result = sp.ViewVbsSellerMemberGet(userId, sellerMemberId, sellerUserId);
            }

            return result;
        }

        public static int GetViewVbsSellerMemberCount(int sellerUserId, SellerMemberType sellerMemberType)
        {
            return sp.ViewVbsSellerMemberCount(sellerUserId, sellerMemberType);
        }

        public static int GetViewVbsSellerMemberCount(int sellerUserId, SellerMemberType sellerMemberType, string criteria, MembershipCardLevel? level, bool newMember)
        {
            List<string> filters = new List<string>();
            if (level.HasValue)
            {
                filters.Add(string.Format("{0} = {1}", ViewVbsSellerMember.Columns.Level, (int)level.Value));
            }
            //只查詢新會員，限定一個月內為新會員
            if (newMember)
            {
                filters.Add(string.Format("{0} > {1}", ViewVbsSellerMember.Columns.RequestTime, DateTime.Now.AddMonths(-1)));
            }

            return sp.ViewVbsSellerMemberCount(sellerUserId, sellerMemberType, criteria, filters.ToArray());
        }

        #endregion view_vbs_seller_member

        public static KeyValuePair<Type, string> GetProposalFlagsTypeByStatus(ProposalStatus status)
        {
            Type flagType = null;
            string col = string.Empty;
            switch (status)
            {
                case ProposalStatus.SellerCreate:
                    flagType = typeof(SellerProposalFlag);
                    col = Proposal.SellerProposalFlagColumn.ColumnName;
                    break;
                case ProposalStatus.Initial:
                    break;
                case ProposalStatus.Apply:
                    flagType = typeof(ProposalApplyFlag);
                    col = Proposal.ApplyFlagColumn.ColumnName;
                    break;
                case ProposalStatus.Approve:
                    flagType = typeof(ProposalApproveFlag);
                    col = Proposal.ApproveFlagColumn.ColumnName;
                    break;
                case ProposalStatus.Created:
                    flagType = typeof(ProposalBusinessCreateFlag);
                    col = Proposal.BusinessCreateFlagColumn.ColumnName;
                    break;
                case ProposalStatus.BusinessCheck:
                    flagType = typeof(ProposalBusinessFlag);
                    col = Proposal.BusinessFlagColumn.ColumnName;
                    break;
                case ProposalStatus.Editor:
                    flagType = typeof(ProposalEditorFlag);
                    col = Proposal.EditFlagColumn.ColumnName;
                    break;
                case ProposalStatus.Listing:
                    flagType = typeof(ProposalListingFlag);
                    col = Proposal.ListingFlagColumn.ColumnName;
                    break;
                default:
                    break;
            }
            return new KeyValuePair<Type, string>(flagType, col);
        }

        public static int GetFlagForProposalFlagCheck(Proposal pro)
        {
            int newStatus = 0;
            foreach (var status in Enum.GetValues(typeof(ProposalStatus)))
            {
                if ((ProposalStatus)status == ProposalStatus.Initial)
                {
                    continue;
                }
                KeyValuePair<Type, string> flags = GetProposalFlagsTypeByStatus((ProposalStatus)status);

                bool check = true;
                foreach (var flag in Enum.GetValues(flags.Key))
                {
                    if ((int)flag == 0)
                    {
                        continue;
                    }
                    if (!Helper.IsFlagSet(Convert.ToInt32(pro.GetColumnValue(flags.Value)), (Enum)flag))
                    {
                        check = false;
                        break;
                    }
                }
                if (check)
                {
                    newStatus = (int)status;
                }
                else
                {
                    return (int)status - 1;
                }
            }
            return pro.Status < newStatus ? newStatus : pro.Status;
        }

        public static Dictionary<ProposalStatus, Dictionary<Enum, bool>> GetProposalFlags(ViewProposalSeller pro)
        {
            Dictionary<ProposalStatus, Dictionary<Enum, bool>> data = new Dictionary<ProposalStatus, Dictionary<Enum, bool>>();
            foreach (var item in Enum.GetValues(typeof(ProposalStatus)))
            {
                ProposalStatus status = (ProposalStatus)item;


                //無商家只顯示到申請核准
                bool show = true;
                if (pro.SellerGuid == Guid.Empty && (status == ProposalStatus.BusinessCheck || status == ProposalStatus.Created || status == ProposalStatus.Editor || status == ProposalStatus.Listing))
                {
                    show = false;
                }

                if (show)
                {
                    if (status == ProposalStatus.Initial || status == ProposalStatus.SellerCreate)
                    {
                        continue;
                    }
                    KeyValuePair<Type, string> flagType = SellerFacade.GetProposalFlagsTypeByStatus(status);
                    data.Add(status, GetFlags(pro, flagType.Key, flagType.Value));
                }
            }
            return data;
        }

        public static Dictionary<Enum, bool> GetFlags(ViewProposalSeller pro, Type type, string col)
        {
            Dictionary<Enum, bool> flags = new Dictionary<Enum, bool>();
            if (pro.GetColumnValue(col) != null)
            {
                int flag = Convert.ToInt32(pro.GetColumnValue(col));
                return GetFlags(flag, type, col);
            }
            return flags;
        }

        public static Dictionary<Enum, bool> GetFlags(int flag, Type type, string col)
        {
            Dictionary<Enum, bool> flags = new Dictionary<Enum, bool>();
            foreach (var f in Enum.GetValues(type))
            {
                if ((int)f != 0)
                {
                    flags.Add((Enum)f, Helper.IsFlagSet(flag, (Enum)f));
                }
            }
            return flags;
        }

        public static string GetWeeklyNames(string frenquency, string weeklys, string beginTime, string endTime, bool allDay)
        {
            List<string> _WeeklyName = new List<string>();
            int nextValue = -1;
            int lastValue = -1;
            bool continues = false;
            string[] _weekly = weeklys.Split(",");

            try
            {
                for (int widx = 0; widx < 7; widx++)
                {
                    if (_weekly.Contains(widx.ToString()))
                    {
                        if (!continues)
                        {
                            lastValue = widx;
                        }
                        continues = true;
                    }
                    else
                    {
                        continues = false;
                    }
                    if (!continues)
                    {
                        if (nextValue != -1 && lastValue != -1)
                        {
                            _WeeklyName.Add(getWeeklyFullName(lastValue, nextValue));
                            nextValue = -1;
                            lastValue = -1;
                        }
                    }
                    if (lastValue != -1)
                    {
                        nextValue = widx;
                    }
                }
                if (continues)
                {
                    _WeeklyName.Add(getWeeklyFullName(lastValue, nextValue));
                }
            }
            catch
            {
                return "";
            }

            return frenquency + string.Join("，", _WeeklyName) + " " + (allDay ? "全天" : beginTime + "~" + endTime);
        }

        private static string getWeeklyFullName(int beginWeek, int endWeek)
        {
            var WeekTimes = "";
            if (beginWeek != endWeek)
            {
                WeekTimes = getWeeklyCnName(beginWeek) + "~" + getWeeklyCnName(endWeek);
            }
            else
            {
                WeekTimes = getWeeklyCnName(beginWeek);
            }
            return WeekTimes;
        }

        private static string getWeeklyCnName(int wIdx)
        {
            string CNName = "";
            switch (wIdx)
            {
                case 0:
                    CNName = "週一";
                    break;
                case 1:
                    CNName = "週二";
                    break;
                case 2:
                    CNName = "週三";
                    break;
                case 3:
                    CNName = "週四";
                    break;
                case 4:
                    CNName = "週五";
                    break;
                case 5:
                    CNName = "週六";
                    break;
                case 6:
                    CNName = "週日";
                    break;
            }
            return CNName;
        }

        public static bool StoreOpenTimeUpdate(List<Guid> storeGuid, string opentime)
        {
            foreach (var g in storeGuid)
            {
                var s = sp.SellerGet(g);
                s.OpenTime = opentime;
                sp.SellerSet(s);
            }
            return true;
        }
        public static bool StoreCloseDateUpdate(List<Guid> storeGuid, string closedate)
        {
            foreach (var g in storeGuid)
            {
                var s = sp.SellerGet(g);
                s.CloseDate = closedate;
                sp.SellerSet(s);
            }
            return true;
        }


        /// <summary>
        /// 判斷此賣家是否有商家資訊頁面
        /// 如果有，前臺檔次明細頁，店家資訊會顯示「詳細資料」連結
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static bool GetStorePageCheck(Guid sellerGuid)
        {
            return sp.SellerSellerInfoIsExistsGet(sellerGuid);
        }

        /// <summary>
        /// 依seller_id抓取Seller資料
        /// </summary>
        /// <param name="sellerId">sellerId</param>
        /// <returns></returns>
        public static Seller SellerGetById(string sellerId)
        {
            return sp.SellerGet(Seller.Columns.SellerId, sellerId);
        }

        /// <summary>
        /// 依sellerGuid抓取Seller資料
        /// </summary>
        /// <param name="sellerGuid">sellerId</param>
        /// <returns></returns>
        public static Seller SellerGet(Guid sellerGuid)
        {
            return sp.SellerGet(sellerGuid);
        }

        public static bool SaveProposalItemPriceContent(string sellerGuid, List<string> content, string userName)
        {
            Guid sid = Guid.Empty;
            Guid.TryParse(sellerGuid, out sid);
            Seller seller = sp.SellerGet(sid);
            if (seller.IsLoaded)
            {
                seller.ItemPriceCondition = new JsonSerializer().Serialize(content);
                sp.SellerSet(seller);
                List<SystemCode> dealType = SystemCodeManager.GetDealType().Where(x => x.ParentCodeId == null).ToList();
                List<string> listName = new List<string>();
                foreach (string c in content)
                {
                    int cc = 0;
                    int.TryParse(c, out cc);
                    var dtype = dealType.Where(x => x.CodeId == cc).FirstOrDefault();
                    if (dtype != null)
                    {
                        listName.Add(dtype.CodeName);
                    }
                }
                SellerLogSet(sid, string.Join(",", listName), userName);
            }
            return true;
        }
        public static List<string> GetProposalItemPriceContent(Guid sid)
        {
            Seller seller = sp.SellerGet(sid);
            List<string> content = new List<string>();
            if (seller.IsLoaded)
            {
                if (!string.IsNullOrEmpty(seller.ItemPriceCondition))
                {
                    content = new JsonSerializer().Deserialize<List<string>>(seller.ItemPriceCondition);
                }
            }
            return content;
        }

        public static bool SellerTreeSet(Guid sellerGuid, IEnumerable<Guid> parentSellerGuids, string userId, string reason)
        {
            var result = true;
            var now = DateTime.Now;
            var originSeller = sp.SellerTreeGetListBySellerGuid(sellerGuid);

            if (originSeller.Any())
            {
                if (parentSellerGuids != null)
                {
                    foreach (var parentSellerGuid in parentSellerGuids)
                    {
                        //**目前機制未允許rootGuid有多筆狀況 之後若有此需求 這段必須增加root_guid可能已存在或有多筆的判斷**
                        var rootGuid = GetRootSellerGuid(parentSellerGuid);
                        if (originSeller.Any(x => x.ParentSellerGuid == parentSellerGuid))
                        {
                            var sellerTree = originSeller.First(x => x.ParentSellerGuid == parentSellerGuid);
                            //檢查root id 有異動才須更新
                            if (rootGuid != sellerTree.RootSellerGuid)
                            {
                                //異動parent id 需一併更新root id
                                sellerTree.RootSellerGuid = rootGuid;
                                sellerTree.ModifyId = userId;
                                sellerTree.ModifyTime = now;

                                result = sp.SellerTreeSet(sellerTree);
                            }
                        }
                        else
                        {
                            //insert關聯
                            var sellerTree = new SellerTree
                            {
                                SellerGuid = sellerGuid,
                                ParentSellerGuid = parentSellerGuid,
                                RootSellerGuid = rootGuid,
                                CreateId = userId,
                                CreateTime = now
                            };
                            result = sp.SellerTreeSet(sellerTree);
                        }
                    }
                    foreach (var s in originSeller.Where(x => !parentSellerGuids.Contains(x.ParentSellerGuid)))
                    {
                        result = DisableSellerTree(s, userId, now, reason);
                    }
                }
                else
                {
                    foreach (var s in originSeller)
                    {
                        result = DisableSellerTree(s, userId, now, reason);
                    }
                }
            }
            else
            {
                if (parentSellerGuids != null)
                {
                    foreach (var parentSellerGuid in parentSellerGuids)
                    {
                        var sellerTree = new SellerTree
                        {
                            SellerGuid = sellerGuid,
                            ParentSellerGuid = parentSellerGuid,
                            RootSellerGuid = GetRootSellerGuid(parentSellerGuid),
                            CreateId = userId,
                            CreateTime = now
                        };
                        result = sp.SellerTreeSet(sellerTree);
                    }
                }
            }

            //拔除parent_id關聯 須將所屬下層賣家也一併拔除root關聯
            //將所屬下層賣家的root更新為自己(獨立為一棵樹)
            //若異動上層賣家 而需異動root id 則所屬下層賣家 也需一併異動root id
            ChangeSellerTreeRootGuid(sellerGuid, GetRootSellerGuid(sellerGuid), userId);

            return result;
        }

        private static bool DisableSellerTree(SellerTree s, string userId, DateTime modifyTime, string reason)
        {
            //移除parent id 則將此關聯清除 
            //並移除上層賣家之檔次中所有ppon_store中和該賣家有關之權限
            //並寫一筆異動紀錄至seller_tree_log
            var log = new SellerTreeLog
            {
                SellerGuid = s.SellerGuid,
                ParentSellerGuid = s.ParentSellerGuid,
                RootSellerGuid = s.RootSellerGuid,
                Sort = s.Sort,
                SellerTreeCreateId = s.CreateId,
                SellerTreeCreateTime = s.CreateTime,
                SellerTreeModifyId = s.ModifyId,
                SellerTreeModifyTime = s.ModifyTime,
                CreateId = userId,
                CreateTime = modifyTime,
                Reason = string.Format("Remove parent seller relation, Reason: {0}", reason)
            };
            sp.SellerTreeLogSet(log);

            //var dealStores = pp.ViewPponDealStoreGetList(s.ParentSellerGuid, s.SellerGuid);

            //foreach (var dealStore in dealStores)
            //{
            //    if (!dealStore.StoreGuid.HasValue)
            //    {
            //        continue;
            //    }
            //    RemovePponStore(dealStore.ProductGuid, dealStore.StoreGuid.Value);
            //}

            sp.SellerTreeDelete(s);

            return false;
        }

        private static void RemovePponStore(Guid bid, Guid sellerGuid)
        {
            var pponStore = pp.PponStoreGet(bid, sellerGuid);
            pponStore.VbsRight = (int)Helper.SetFlag(false, pponStore.VbsRight, VbsRightFlag.Location);
            pponStore.VbsRight = (int)Helper.SetFlag(false, pponStore.VbsRight, VbsRightFlag.Verify);
            pponStore.VbsRight = (int)Helper.SetFlag(false, pponStore.VbsRight, VbsRightFlag.Accouting);
            pponStore.VbsRight = (int)Helper.SetFlag(false, pponStore.VbsRight, VbsRightFlag.ViewBalanceSheet);

            pp.PponStoreSet(pponStore);
        }

        private static void ChangeSellerTreeRootGuid(Guid parentGuid, Guid rootGuid, string userId)
        {
            var sellerTrees = sp.SellerTreeGetListByParentSellerGuid(parentGuid);

            foreach (var s in sellerTrees)
            {
                if (s.RootSellerGuid == rootGuid)
                {
                    continue;
                }

                var now = DateTime.Now;
                var log = new SellerTreeLog
                {
                    SellerGuid = s.SellerGuid,
                    ParentSellerGuid = s.ParentSellerGuid,
                    RootSellerGuid = s.RootSellerGuid,
                    Sort = s.Sort,
                    SellerTreeCreateId = s.CreateId,
                    SellerTreeCreateTime = s.CreateTime,
                    SellerTreeModifyId = s.ModifyId,
                    SellerTreeModifyTime = s.ModifyTime,
                    CreateId = userId,
                    CreateTime = now,
                    Reason = string.Format("Change Root Id From {0} To {1}", s.RootSellerGuid, rootGuid)
                };
                sp.SellerTreeLogSet(log);

                s.RootSellerGuid = rootGuid;
                s.ModifyId = userId;
                s.ModifyTime = now;

                if (sp.SellerTreeSet(s))
                {
                    ChangeSellerTreeRootGuid(s.SellerGuid, rootGuid, userId);
                }
            }
        }

        public static bool CheckIsRootSellerGuid(Guid rootGuid)
        {
            var sellers = sp.SellerTreeGetListBySellerGuid(rootGuid);

            return !sellers.Any();
        }

        public static Guid GetRootSellerGuid(Guid sellerGuid)
        {
            var seller = sp.SellerTreeGetListBySellerGuid(sellerGuid)
                            .FirstOrDefault();
            if (seller == null)
            {
                return sellerGuid;
            }

            var parentSeller = sp.SellerTreeGetListBySellerGuid(seller.ParentSellerGuid)
                                    .FirstOrDefault();

            return parentSeller != null
                    ? GetRootSellerGuid(parentSeller.ParentSellerGuid)
                    : seller.ParentSellerGuid;

        }


        /// <summary>
        /// 依seller_guid，勾選之核銷賣家及對帳賣家資訊 回傳是否符合核銷/出帳權限設定規則
        /// </summary>
        /// <param name="sellerGuid">sellerGuid</param>
        /// <param name="vbsRightInfo">vbsRightInfo</param>
        /// <param name="checkErrorMsg">checkErrorMsg</param>
        /// <returns>true/false out檢核結果訊息</returns>
        public static bool ValiadateSellerTreeSetUp(Guid sellerGuid, Dictionary<VbsRightFlag, IEnumerable<Guid>> vbsRightInfo, out string checkErrorMsg)
        {
            if (!vbsRightInfo.ContainsKey(VbsRightFlag.Verify) ||
                !vbsRightInfo.ContainsKey(VbsRightFlag.Accouting) ||
                !vbsRightInfo.ContainsKey(VbsRightFlag.ViewBalanceSheet) ||
                !vbsRightInfo.ContainsKey(VbsRightFlag.BalanceSheetHideFromDealSeller) ||
                !vbsRightInfo.ContainsKey(VbsRightFlag.VerifyShop) ||
                !vbsRightInfo.ContainsKey(VbsRightFlag.HideFromVerifyShop))
            {
                checkErrorMsg = "檔次設定權限檢核發生異常，請聯繫It_Service協助排除問題";
                return false;
            }
            var rootGuid = GetRootSellerGuid(sellerGuid);
            var verifyInfo = vbsRightInfo[VbsRightFlag.Verify].Select(verifyGuid => new KeyValuePair<Guid, int>(verifyGuid, GetSellerTreeLevel(verifyGuid, rootGuid) ?? 0)).ToList();
            var accountingInfo = vbsRightInfo[VbsRightFlag.Accouting].Select(accountingGuid => new KeyValuePair<Guid, int>(accountingGuid, GetSellerTreeLevel(accountingGuid, rootGuid) ?? 0)).ToList();

            #region 必填欄位檢查

            if (!verifyInfo.Any())
            {
                checkErrorMsg = "請至少勾選一個[憑證核實]賣家";
                return false;
            }
            if (!accountingInfo.Any())
            {
                checkErrorMsg = "請至少勾選一個[匯款對象]賣家";
                return false;
            }

            #endregion 必填欄位檢查

            #region 憑證核實 及 匯款對象 設定檢查

            if (verifyInfo.Max(x => x.Value) < accountingInfo.Max(x => x.Value))
            {
                checkErrorMsg = "[憑證核實]的權限層級設置，不能高於[匯款對象]";
                return false;
            }
            if (accountingInfo.Sum(x => x.Value) > 0 &&
                verifyInfo.Sum(x => x.Value) != accountingInfo.Sum(x => x.Value) &&
                accountingInfo.Sum(x => x.Value) % accountingInfo.Count > 0)
            {
                checkErrorMsg = "[匯款對象]的權限設置不能同時存在於二個層級";
                return false;
            }

            var relatedSellerGuids = GetChildrenSellerGuid(sellerGuid).ToList();
            relatedSellerGuids.Add(sellerGuid);
            if (vbsRightInfo[VbsRightFlag.Verify].Any(x => !relatedSellerGuids.Contains(x)) ||
                vbsRightInfo[VbsRightFlag.Accouting].Any(x => !relatedSellerGuids.Contains(x)))
            {
                checkErrorMsg = "[匯款對象]的權限設置只能勾選和檔次建立之賣家及其下層賣家";
                return false;
            }

            #endregion 憑證核實 及 匯款對象 設定檢查

            #region 檢視對帳單 及 對賣家隱藏對帳單 設定檢查

            if (vbsRightInfo[VbsRightFlag.BalanceSheetHideFromDealSeller].Any(x => x == sellerGuid) &&
                vbsRightInfo[VbsRightFlag.ViewBalanceSheet].Any(x => x == sellerGuid))
            {
                checkErrorMsg = "檔次賣家為最大檔次權限管理者，無需設定[鎖定對帳單]功能";
                return false;
            }
            if (vbsRightInfo[VbsRightFlag.BalanceSheetHideFromDealSeller].Any() &&
                vbsRightInfo[VbsRightFlag.ViewBalanceSheet].All(x => x != sellerGuid))
            {
                checkErrorMsg = "檔次賣家未開啟[檢視對帳單]權限，無需設定[鎖定對帳單]功能";
                return false;
            }

            #endregion 檢視對帳單 及 對賣家隱藏對帳單 設定檢查

            #region 銷售分店 及 隱藏 設定檢查

            if (vbsRightInfo[VbsRightFlag.HideFromVerifyShop].Any() &&
                !vbsRightInfo[VbsRightFlag.HideFromVerifyShop].All(x => vbsRightInfo[VbsRightFlag.VerifyShop].Contains(x)))
            {
                checkErrorMsg = "[銷售分店]權限未開啟，無需設定[隱藏]功能";
                return false;
            }

            #endregion 銷售分店 及 隱藏 設定檢查

            checkErrorMsg = string.Empty;
            return true;
        }

        /// <summary>
        /// 依seller_guid回傳Seller Tree階層數字
        /// root為第1層
        /// children則往上累加
        /// </summary>
        /// <param name="sellerGuid">sellerId</param>
        /// <param name="rootGuid">rootGuid</param>
        /// <returns>Seller Tree階層數字</returns>
        public static int? GetSellerTreeLevel(Guid sellerGuid, Guid rootGuid)
        {
            var sellerTreeModel = new SellerTreeModel();
            sellerTreeModel.Initial(sellerGuid);
            var sellerTree = sellerTreeModel.SellerTree.FirstOrDefault(x => x.RootNode.Id == rootGuid);

            if (sellerTree != null)
            {
                if (sellerTree.RootNode.Id == sellerGuid)
                {
                    return sellerTree.RootNode.Level;
                }

                var children = sellerTree.Children;
                if (children.Any(x => x.Id == sellerGuid))
                {
                    return children.First(x => x.Id == sellerGuid).Level;
                }
            }

            return null;
        }

        public static IEnumerable<Seller> GetSellerTreeSellers(Guid sellerGuid)
        {
            var sellerGuids = GetSellerTreeSellerGuids(sellerGuid).ToList();

            var sellers = sp.SellerGetList(sellerGuids)
                                    .Where(x => x.StoreStatus == (int)StoreStatus.Available);

            return sellers;
        }

        public static IEnumerable<Guid> GetSellerTreeSellerGuids(Guid sellerGuid)
        {
            var seller = sp.SellerTreeGetListBySellerGuid(sellerGuid)
                            .FirstOrDefault();

            var rootGuid = seller != null ? seller.RootSellerGuid : sellerGuid;
            var sellerGuids = new List<Guid> { rootGuid };
            sellerGuids.AddRange(sp.SellerTreeGetSellerGuidListByRootSellerGuid(rootGuid));
            return sellerGuids;
        }

        public static IEnumerable<SellerNode> GetSellerTreeNode(Guid sellerGuid, bool enabledSelf)
        {
            var model = new SellerTreeModel();
            model.Initial(sellerGuid);

            var treesNodes = new List<SellerNode>();
            foreach (var tree in model.SellerTree)
            {
                var triggerSellerGuid = tree.RootNode.Id != sellerGuid
                                        ? sellerGuid
                                        : (Guid?)null;
                var child = new SellerNode
                {
                    Text = string.Format("- {0}{1}", tree.RootNode.Text, tree.RootNode.StoreStatus == (int)StoreStatus.Cancel ? "(隱藏)" : string.Empty),
                    Id = tree.RootNode.Id,
                    State = triggerSellerGuid.HasValue
                            ? TreeState.Closed
                            : TreeState.Open
                };
                treesNodes.Add(child);
                PopulateSellerTreeChildrenNode(tree.Children, tree.RootNode.Id, ref treesNodes, ref triggerSellerGuid, enabledSelf);
            }

            return treesNodes;
        }

        private static void PopulateSellerTreeChildrenNode(IEnumerable<SellerNode> nodes, Guid parentGuid, ref List<SellerNode> treeNodes, ref Guid? triggerSellerGuid, bool enabledSelf)
        {
            if (nodes.Any(x => x.ParentId == parentGuid))
            {
                var preText = string.Empty;
                var treeLevel = nodes.First(x => x.ParentId == parentGuid).Level;
                for (var e = 1; e < treeLevel; e++)
                {
                    preText = preText + "　";
                }

                foreach (var node in nodes.Where(x => x.ParentId == parentGuid))
                {
                    triggerSellerGuid = enabledSelf
                                        ? triggerSellerGuid
                                        : triggerSellerGuid.HasValue && node.Id != triggerSellerGuid.Value
                                            ? triggerSellerGuid.Value
                                            : (Guid?)null;
                    var child = new SellerNode
                    {
                        Text = string.Format("{0}- {1}{2}", preText, node.Text, node.StoreStatus == (int)StoreStatus.Cancel ? "(隱藏)" : string.Empty),
                        Id = node.Id,
                        ParentId = node.ParentId,
                        State = triggerSellerGuid.HasValue && node.Id != triggerSellerGuid.Value
                                ? TreeState.Closed
                                : TreeState.Open
                    };
                    treeNodes.Add(child);
                    PopulateSellerTreeChildrenNode(nodes, node.Id, ref treeNodes, ref triggerSellerGuid, enabledSelf);
                }
            }
        }

        public static IEnumerable<Guid> GetChildrenSellerGuid(Guid sellerGuid)
        {
            var childrenGuid = new List<Guid>();
            var sellerGuids = sp.SellerTreeGetSellerGuidListByParentSellerGuid(sellerGuid);
            if (sellerGuids.Any())
            {
                foreach (var sGuid in sellerGuids)
                {
                    childrenGuid.Add(sGuid);
                    childrenGuid.AddRange(GetChildrenSellerGuid(sGuid));
                }
            }

            return childrenGuid;
        }

        public static IEnumerable<Seller> GetChildrenSellers(Guid sellerGuid, bool includeSelf = false)
        {
            var sellerGuids = GetChildrenSellerGuid(sellerGuid);
            List<Seller> sellers = new List<Seller>();
            if (includeSelf)
            {
                var headSeller = sp.SellerGet(sellerGuid);
                if (headSeller.IsLoaded)
                {
                    sellers.Add(headSeller);
                }
            }

            var children = sp.SellerGetList(sellerGuids)
                             .Where(x => x.StoreStatus == (int)StoreStatus.Available);
            sellers.AddRange(children);

            return sellers;
        }

        public static IEnumerable<Seller> GetChildrenSellersByBId(Guid bid)
        {
            var sellerGuid = pp.BusinessHourGet(bid).SellerGuid;
            var sellerGuids = GetChildrenSellerGuid(sellerGuid);
            var sellers = sp.SellerGetList(sellerGuids)
                .Where(x => x.StoreStatus == (int)StoreStatus.Available);

            return sellers;
        }

        public static IEnumerable<Seller> GetPponStoreSellersByBId(Guid bid, VbsRightFlag? vbsRightFlag = null)
        {
            var pponStores = pp.PponStoreGetListByBusinessHourGuid(bid);
            var sellerGuids = vbsRightFlag.HasValue
                            ? pponStores.Where(x => Helper.IsFlagSet(x.VbsRight, vbsRightFlag))
                                        .Select(x => x.StoreGuid).ToList()
                            : pponStores.Select(x => x.StoreGuid).ToList();

            var sellers = sellerGuids.Any()
                            ? sp.SellerGetList(sellerGuids)
                                    .Where(x => x.StoreStatus == (int)StoreStatus.Available)
                            : new List<Seller>();

            return sellers;
        }

        /// <summary>
        /// 依ResoureAcld抓取SellerGuid資料
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="resourceType">resourceType</param>
        /// <returns></returns>
        public static ResourceAcl GetResourceAcl(string accountId, int resourceType)
        {
            return mp.ResourceAclGetByAccountIdAndResourceType(accountId, resourceType);
        }

        /// <summary>
        /// 依ResoureAcld抓取SellerGuid資料
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns></returns>
        public static ResourceAclCollection GetResourceAclList(string accountId)
        {
            return mp.ResourceAclGetListByAccountId(accountId);
        }

        /// <summary>
        /// 依SellerTree抓取StoreGuid資料
        /// </summary>
        /// <param name="sellerGuids">sellerGuid</param>
        /// <param name="storeCode">storeCode</param>
        /// <returns></returns>
        public static Guid GetSellerGuidByStoreCode(List<Guid> sellerGuids, string storeCode)
        {
            var sellerGuid = sp.StoreGetListBySellerGuidAndStoreCode(sellerGuids, storeCode);
            return sellerGuid.Any() ? sellerGuid.Select(x => x.Guid).First() : Guid.Empty;
        }

        /// <summary>
        /// 依SellerTree抓取SellerGuid資料
        /// </summary>
        /// <param name="rootSellerGuid">sellerGuid</param>
        /// <returns></returns>
        public static SellerTreeCollection GetSellerTreeCol(Guid rootSellerGuid)
        {
            return sp.SellerTreeGetListByRootSellerGuid(rootSellerGuid);
        }

        /// <summary>
        /// 依SellerUserId抓取UserId資料
        /// </summary>
        /// <param name="sellerUserId">sellerGuid</param>
        /// <param name="storeCode">storeCode</param>
        /// <returns></returns>
        public static int? GetUserIdBySellerUserId(int sellerUserId, string storeCode)
        {
            var vbs = VBSFacade.GetVbsMembershipByUserId(sellerUserId);
            if (!vbs.IsLoaded)
            {
                return null;
            }
            var resource = GetResourceAcl(vbs.AccountId, (int)ResourceType.Seller);
            if (!resource.IsLoaded)
            {
                return null;
            }
            var rootSellerGuid = resource.ResourceGuid;

            //單店
            var sellGuids = new List<Guid> { rootSellerGuid };
            var storeGuid = GetSellerGuidByStoreCode(sellGuids, storeCode);
            if (storeGuid == Guid.Empty)
            {
                //有分店
                var sellerTreeCol = GetSellerTreeCol(rootSellerGuid).ToList();
                if (!sellerTreeCol.Any())
                {
                    return null;
                }
                storeGuid = GetSellerGuidByStoreCode(sellerTreeCol.Select(x => x.SellerGuid).ToList(), storeCode);
                if (storeGuid == Guid.Empty)
                {
                    return null;
                }
            }

            var resourceacl = mp.ResourceAclGetListByResourceGuid(storeGuid);
            if (!resourceacl.IsLoaded)
            {
                return null;
            }
            var accountId = resourceacl.AccountId;

            var vbsMemberShip = mp.VbsMembershipGetByAccountId(accountId);
            if (!vbsMemberShip.IsLoaded || vbsMemberShip.UserId == null)
            {
                return null;
            }
            return (int)vbsMemberShip.UserId;
        }

        /// <summary>
        /// 依SellerUserId抓取UserId與storeGuid資料
        /// </summary>
        /// <param name="sellerUserId"></param>
        /// <param name="storeCode"></param>
        /// <param name="storeGuid"></param>
        /// <returns></returns>
        public static int? GetUserIdBySellerUserId(int sellerUserId, string storeCode, ref Guid storeGuid)
        {
            var vbs = VBSFacade.GetVbsMembershipByUserId(sellerUserId);
            if (!vbs.IsLoaded)
            {
                return null;
            }
            var resource = GetResourceAcl(vbs.AccountId, (int)ResourceType.Seller);
            if (!resource.IsLoaded)
            {
                return null;
            }
            var rootSellerGuid = resource.ResourceGuid;

            //單店
            var sellGuids = new List<Guid> { rootSellerGuid };
            storeGuid = GetSellerGuidByStoreCode(sellGuids, storeCode);
            if (storeGuid == Guid.Empty)
            {
                //有分店
                var sellerTreeCol = GetSellerTreeCol(rootSellerGuid).ToList();
                if (!sellerTreeCol.Any())
                {
                    return null;
                }
                storeGuid = GetSellerGuidByStoreCode(sellerTreeCol.Select(x => x.SellerGuid).ToList(), storeCode);
                if (storeGuid == Guid.Empty)
                {
                    return null;
                }
            }

            var resourceacl = mp.ResourceAclGetListByResourceGuid(storeGuid);
            if (!resourceacl.IsLoaded)
            {
                return null;
            }
            var accountId = resourceacl.AccountId;

            var vbsMemberShip = mp.VbsMembershipGetByAccountId(accountId);
            if (!vbsMemberShip.IsLoaded || vbsMemberShip.UserId == null)
            {
                return null;
            }
            return (int)vbsMemberShip.UserId;
        }

        /// <summary>
        /// 以storeGuid 反查 VbsMembership
        /// </summary>
        /// <param name="storeGuid"></param>
        /// <returns></returns>
        public static VbsMembership GetVbsMemberUserIdByStoreGuid(Guid storeGuid)
        {
            var resourceacl = mp.ResourceAclGetListByResourceGuid(storeGuid);
            if (!resourceacl.IsLoaded)
            {
                return null;
            }
            var accountId = resourceacl.AccountId;

            var vbsMemberShip = mp.VbsMembershipGetByAccountId(accountId);
            if (!vbsMemberShip.IsLoaded || vbsMemberShip.UserId == null)
            {
                return null;
            }
            var userId = (int)vbsMemberShip.UserId;

            return mp.VbsMembershipGetByUserId(userId);
        }


        public static void ResourceAclSetList(ResourceAclCollection acls, string reason, string userName)
        {
            var createTime = DateTime.Now;
            var createId = userName;

            TransactionOptions options = new TransactionOptions();
            options.IsolationLevel = IsolationLevel.ReadCommitted;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
            {
                try
                {
                    foreach (var acl in acls)
                    {
                        var aclId = mp.ResourceAclSet(acl);
                        if (acl.Id == 0)
                        {
                            acl.Id = aclId;
                        }
                        ResourceAclLogSet(acl, createId, createTime, reason);
                    }
                }
                catch
                {
                    throw;
                }

                scope.Complete();
            }
        }

        public static void ResourceAclDelete(ResourceAclCollection acls, string reason, string userName)
        {
            var createTime = DateTime.Now;
            var createId = userName;
            foreach (var acl in acls)
            {
                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = IsolationLevel.ReadCommitted;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    try
                    {
                        ResourceAclLogSet(acl, createId, createTime, reason);
                        mp.ResourceAclDelete(acl);
                    }
                    catch
                    {
                        throw;
                    }

                    scope.Complete();
                }
            }
        }

        public static void ResourceAclLogSet(ResourceAcl acl, string createId, DateTime createTime, string reason)
        {
            var log = new ResourceAclLog
            {
                ResourceAclId = acl.Id,
                AccountId = acl.AccountId,
                AccountType = acl.AccountType,
                ResourceGuid = acl.ResourceGuid,
                ResourceType = acl.ResourceType,
                PermissionType = acl.PermissionType,
                PermissionSetting = acl.PermissionSetting,
                Reason = reason,
                CreateId = createId,
                CreateTime = createTime
            };

            mp.ResourceAclLogSet(log);
        }

        public static DealAccountingPayType GetPayToCompany<T>(Guid sellerGuid, IEnumerable<T> stores)
        {
            if (typeof(T) == typeof(PponStore))
            {
                PponStoreCollection ppon = (PponStoreCollection)stores;

                var accountStores = ppon.Where(s => Helper.IsFlagSet(s.VbsRight, VbsRightFlag.Accouting));
                return accountStores.Any(s => s.StoreGuid == sellerGuid) && accountStores.Count() == 1
                    ? DealAccountingPayType.PayToCompany
                    : DealAccountingPayType.PayToStore;
            }

            else
            {
                ProposalStoreCollection proposal = (ProposalStoreCollection)stores;

                var accountStores = proposal.Where(s => Helper.IsFlagSet(s.VbsRight, VbsRightFlag.Accouting));
                return accountStores.Any(s => s.StoreGuid == sellerGuid) && accountStores.Count() == 1
                    ? DealAccountingPayType.PayToCompany
                    : DealAccountingPayType.PayToStore;
            }
        }

        public static Seller IsCompany(string CompanyName, string SellerGuid)
        {
            Seller seller = sp.SellerGetBySellerGuid(CompanyName, SellerGuid);
            return seller;
        }

        public static List<AjaxResponseResult> GetSellerNameArray(string sellerName)
        {
            List<AjaxResponseResult> res = new List<AjaxResponseResult>();
            if (!string.IsNullOrEmpty(sellerName))
            {
                var sellerList = sp.SellerGetListByLikelyName(sellerName)
                                                .Where(x => x.Guid != Guid.Empty)
                                                .OrderByDescending(x => x.ModifyTime).ThenByDescending(x => x.CreateTime)
                                                .Select(x => new
                                                {
                                                    Label = string.Format("{0} {1}", x.SellerId, x.SellerName.Replace("\"", "'").Trim()),
                                                    Value = x.Guid
                                                });

                foreach (var s in sellerList)
                {
                    res.Add(new AjaxResponseResult
                    {
                        Value = s.Value.ToString(),
                        Label = s.Label
                    });
                }
            }

            return res;
        }


        public static string GetSellerManageLogDiff(Guid SellerGuid)
        {
            string result = "";
            var SellerManageLog = sp.SellerManageLogGetList(SellerGuid);
            if (SellerManageLog.Count() > 0)
            {
                TimeSpan ts2 = DateTime.Now.Date - SellerManageLog.Max(x => x.CreateTime).Date;
                int Diff = ts2.Days;
                result = Diff.ToString() + "天未聯繫";
            }
            else
            {
                result = "未聯繫";
            }
            return result;
        }

        public static string GetSellerPorperty(Guid SellerGuid)
        {
            BusinessHourCollection bhs = sp.BusinessHourGetListBySeller(SellerGuid);
            string result = "";
            var SellerPorperty1 = bhs.Where(x => x.BusinessHourOrderTimeS <= DateTime.Now && x.BusinessHourOrderTimeE >= DateTime.Now);
            if (SellerPorperty1.Count() > 0)
            {
                result = "上檔中";
            }

            if (string.IsNullOrEmpty(result))
            {
                var SellerPorperty2 = bhs.Where(x => x.BusinessHourOrderTimeE.ToString("yyyy/MM/dd") != DateTime.MaxValue.ToString("yyyy/MM/dd") &&
                                                                                            !(x.BusinessHourOrderTimeS <= DateTime.Now && x.BusinessHourOrderTimeE >= DateTime.Now));

                if (SellerPorperty2.Count() > 0)
                {
                    var SellerPorperty2Max = SellerPorperty2.Max(x => x.BusinessHourOrderTimeE);
                    if (SellerPorperty2Max.Date >= DateTime.Now.AddYears(-1).Date)
                    {
                        result = "已合作";
                    }
                }
            }

            if (string.IsNullOrEmpty(result))
            {
                var SellerPorperty3 = bhs.Where(x => x.BusinessHourOrderTimeE.ToString("yyyy/MM/dd") != DateTime.MaxValue.ToString("yyyy/MM/dd"));
                if (SellerPorperty3.Count() > 0)
                {
                    var SellerPorperty3Max = SellerPorperty3.Max(x => x.BusinessHourOrderTimeE);
                    if (SellerPorperty3Max.Date < DateTime.Now.AddYears(-1).Date)
                    {
                        result = "一年以上未合作";
                    }
                }
            }

            if (string.IsNullOrEmpty(result))
            {
                var SellerPorperty4 = bhs;
                if (SellerPorperty4.Count() == 0)
                {
                    result = "從未合作";
                }
            }


            return result;
        }

        public static bool ShoppingCartFreightsCreateBySellerGuid(Guid sid)
        {
            Seller seller = sp.SellerGet(sid);
            if (seller != null && seller.IsLoaded)
            {
                ShoppingCartFreightCollection scfs = sp.ShoppingCartFreightsGetBySellerGuid(new List<Guid>() { sid });
                if (scfs.Count == 0)
                {
                    ShoppingCartFreight scf = new ShoppingCartFreight();
                    scf.UniqueId = seller.SellerId.Replace("-", "") + "001";
                    scf.SellerGuid = sid;
                    scf.FreightsStatus = (int)ShoppingCartFreightStatus.Approve;
                    scf.FreightsName = "免運";
                    scf.NoFreightLimit = 0;
                    scf.Freights = 0;
                    scf.CreateTime = DateTime.Now;
                    scf.CreateUser = "sys";

                    sp.ShoppingCartFreightsSet(scf);
                }
            }
            return true;
        }

        public static ShoppingCartFreight ShoppingCartFreightsGet(int id)
        {
            return sp.ShoppingCartFreightsGet(id);
        }
        public static List<ShoppingCartManageList> ShoppingCartFreightsSetBySellerGuid(Guid sid)
        {
            List<ShoppingCartFreight> freights = sp.ShoppingCartFreightsGetBySellerGuid(
                new List<Guid>() {
                    sid
                }
                ).ToList();
            List<ShoppingCartManageList> items = new List<ShoppingCartManageList>();
            foreach (ShoppingCartFreight sf in freights)
            {
                ShoppingCartManageList item = new ShoppingCartManageList
                {
                    Id = sf.Id,
                    UniqueId = sf.UniqueId,
                    SellerName = SellerNameGet(sf.SellerGuid),
                    FreightsStatusName = GetFreightStatus(sf.FreightsStatus),
                    FreightsName = sf.FreightsName,
                    NoFreightLimit = sf.NoFreightLimit,
                    Freights = sf.Freights,
                    CreateTime = sf.CreateTime
                };
                items.Add(item);
            }
            return items;
        }

        public static string SellerNameGet(Guid g)
        {
            string result = "";
            Seller seller = sp.SellerGet(g);
            if (seller.IsLoaded)
            {
                result = string.Format("{0}-{1}", seller.SellerId, seller.SellerName);
            }
            return result;
        }
        public static string GetFreightStatus(int s)
        {
            return Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ShoppingCartFreightStatus)s);
        }
        public static bool ShoppingCartFreightsSet(ShoppingCartFreight scfi)
        {
            sp.ShoppingCartFreightsSet(scfi);
            return true;
        }
        public static ShoppingCartFreight FreightsGetByBid(Guid bid)
        {
            ShoppingCartFreight freight = new ShoppingCartFreight();
            ShoppingCartFreightsItem item = sp.ShoppingCartFreightsItemGetByBid(bid);
            if (item != null && item.IsLoaded)
            {
                freight = sp.ShoppingCartFreightsGet(item.FreightsId);
            }
            return freight;
        }
        public static void ShoppingCartFreightsLogSet(string UserId, int id, string contentLog, ShoppingCartFreightLogType type = ShoppingCartFreightLogType.User)
        {
            ShoppingCartFreightsLog log = new ShoppingCartFreightsLog();
            log.FreightsId = id;
            log.ContentLog = contentLog;
            log.LogType = (int)type;
            log.CreateTime = DateTime.Now;
            log.CreateUser = UserId;
            sp.ShoppingCartFreightsLogSet(log);
        }
        /// <summary>
        /// 購物車啟用信件通知
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="subject"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static bool MailShoppingCartToSeller(Guid sellerGuid, string freightName)
        {
            bool flag = false;
            string UserEmail = "";
            string subject = "【17Life】[商家購物車啟用通知]";
            string content = "";

            Seller seller = sp.SellerGet(sellerGuid);
            if (seller != null && seller.IsLoaded)
            {
                List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                if (contacts != null)
                {
                    foreach (var c in contacts)
                    {
                        if (c.Type == ((int)ProposalContact.Normal).ToString())
                        {
                            if (!string.IsNullOrEmpty(c.ContactPersonEmail))
                            {
                                UserEmail = c.ContactPersonEmail;
                            }
                        }
                    }
                }

                content = string.Format(
                    @"商家:{0} 您好<br />,您的購物車{1}已審核通過,正式啟用,<br />
                    請盡速至商家系統_購物車管理,進行購物車的商品設定!",
                    seller.SellerName,
                    freightName
                    );
            }

            if (!string.IsNullOrEmpty(UserEmail) && RegExRules.CheckEmail(UserEmail))
            {
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new System.Net.Mail.MailAddress(config.SystemEmail);
                msg.To.Add(UserEmail);//賣家聯絡人

                msg.Subject = subject;
                msg.Body = System.Web.HttpUtility.HtmlDecode(content);
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                flag = true;
            }
            return flag;
        }



        /// <summary>
        /// 取得商家全部業務資料Model
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static List<MultipleSalesModel> SellerSaleGetBySellerGuid(Guid sellerGuid)
        {
            List<MultipleSalesModel> MultipleSales = new List<MultipleSalesModel>();
            List<SellerSale> salesList = sp.SellerSaleGetBySellerGuid(sellerGuid).ToList();
            int maxSalesGroup = salesList.Count() == 0 ? 0 : sp.SellerSaleGetBySellerGuid(sellerGuid).Max(x => x.SalesGroup);
            for (int i = 1; i <= maxSalesGroup; i++)
            {
                MultipleSalesModel item = new MultipleSalesModel();
                item.SalesGroup = i;
                ViewEmployee emp;
                var empSales = salesList.Where(x => x.SalesGroup == i && x.SalesType == (int)SellerSalesType.Develope).FirstOrDefault();
                if (empSales == null)
                {
                    item.DevelopeSalesId = 0;
                    item.DevelopeSalesEmpName = "";
                    item.DevelopeSalesEmail = "";
                    item.DevelopeSalesDeptId = "";
                    item.DevelopeSalesDeptName = "";
                    item.DevelopeSalesTeamNo = 0;
                }
                else
                {
                    emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, empSales.SellerSalesId);
                    item.DevelopeSalesId = emp.UserId;
                    item.DevelopeSalesEmpName = emp.EmpName;
                    item.DevelopeSalesEmail = emp.Email;
                    item.DevelopeSalesDeptId = emp.DeptId;
                    item.DevelopeSalesDeptName = emp.DeptName;
                    item.DevelopeSalesTeamNo = emp.TeamNo ?? 0;
                }


                empSales = salesList.Where(x => x.SalesGroup == i && x.SalesType == (int)SellerSalesType.Operation).FirstOrDefault();
                if (empSales == null)
                {
                    item.OperationSalesId = 0;
                    item.OperationSalesEmpName = "";
                    item.OperationSalesEmail = "";
                    item.OperationSalesDeptId = "";
                    item.OperationSalesDeptName = "";
                    item.OperationSalesTeamNo = 0;
                }
                else
                {
                    emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, empSales.SellerSalesId);
                    item.OperationSalesId = emp.UserId;
                    item.OperationSalesEmpName = emp.EmpName;
                    item.OperationSalesEmail = emp.Email;
                    item.OperationSalesDeptId = emp.DeptId;
                    item.OperationSalesDeptName = emp.DeptName;
                    item.OperationSalesTeamNo = emp.TeamNo ?? 0;

                }


                MultipleSales.Add(item);
            }
            return MultipleSales;
        }



        /// <summary>
        /// 取得商家全部業務名字
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static string GetSellerSales(Guid sellerGuid)
        {
            var salesList = sp.SellerSaleGetBySellerGuid(sellerGuid).GroupBy(x => x.SellerSalesId).ToList();
            string salesName = string.Empty;
            foreach (var s in salesList)
            {
                ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, s.Key);
                if (emp.IsLoaded)
                {
                    salesName += emp.EmpName + emp.DeptName.Replace("業務部(", "(").Replace("系統業務部", "(系統業務部)").Replace("通路發展部", "(通路發展部)");//避免系統業務部被卡掉
                    salesName += "<br>";
                }

            }

            return salesName;
        }

        /// <summary>
        /// 檢核業務輸入
        /// </summary>
        /// <param name="type">輸入類型</param>
        /// <param name="developeSales"></param>
        /// <param name="operationSales"></param>
        /// <returns></returns>
        public static string CheckSales(int type, string developeSales, string operationSales)
        {
            List<Department> dept = HumanFacade.GetSalesDepartment();

            if (type == 1)
            {
                //輸入email
                ViewEmployee developeSalesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, developeSales);
                ViewEmployee operationSalesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, operationSales);

                if (!developeSalesEmp.IsLoaded)
                {
                    return "Email帳號錯誤，請重新檢視。";
                }
                else if (!developeSalesEmp.DeptId.EqualsAny(dept.Select(x => x.DeptId).ToArray()))
                {
                    return "無法轉件於非業務，請重新檢視。";
                }
                else if (developeSalesEmp.DeptId == EmployeeChildDept.S001.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S002.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S003.ToString() ||
                    developeSalesEmp.DeptId == EmployeeChildDept.S004.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S005.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S006.ToString() ||
                    developeSalesEmp.DeptId == EmployeeChildDept.S007.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S008.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S010.ToString() ||
                    developeSalesEmp.DeptId == EmployeeChildDept.S012.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S013.ToString())
                {
                    if (operationSales == "")
                    {
                        return "負責業務1、2 欄位，沒有完整填入資料，無法儲存，請重新輸入。";
                    }
                    else if (!operationSalesEmp.IsLoaded)
                    {
                        return "Email帳號錯誤，請重新檢視。";
                    }
                    else if (!operationSalesEmp.DeptId.EqualsAny(dept.Select(x => x.DeptId).ToArray()))
                    {
                        return "無法轉件於非業務，請重新檢視。";
                    }
                }
                else if (operationSales != "" && !operationSalesEmp.IsLoaded)
                {
                    return "Email帳號錯誤，請重新檢視。";
                }

                return "";
            }
            else if (type == 2)
            {
                //輸入姓名(檔次設定頁使用)
                ViewEmployee developeSalesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.EmpName, developeSales);
                ViewEmployee operationSalesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.EmpName, operationSales);


                if (developeSalesEmp.DeptId == EmployeeChildDept.S001.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S002.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S003.ToString() ||
                    developeSalesEmp.DeptId == EmployeeChildDept.S004.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S005.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S006.ToString() ||
                    developeSalesEmp.DeptId == EmployeeChildDept.S007.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S008.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S010.ToString() ||
                    developeSalesEmp.DeptId == EmployeeChildDept.S012.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S013.ToString())
                {
                    if (operationSales == "")
                    {
                        return "負責業務1、2 欄位，沒有完整填入資料，無法儲存，請重新輸入。";
                    }
                    else if (!operationSalesEmp.IsLoaded)
                    {
                        return "Email帳號錯誤，請重新檢視。";
                    }
                }
                else if (operationSales != "" && !operationSalesEmp.IsLoaded)
                {
                    return "Email帳號錯誤，請重新檢視。";
                }

                return "";
            }
            else if (type == 3)
            {
                //輸入email(且可皆為空->刪除業務組別)
                ViewEmployee developeSalesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, developeSales);
                ViewEmployee operationSalesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, operationSales);

                if (string.IsNullOrEmpty(developeSales) && string.IsNullOrEmpty(operationSales))
                {
                    return "刪除";
                }
                else if (!developeSalesEmp.IsLoaded)
                {
                    return "Email帳號錯誤，請重新檢視。";
                }
                else if (!developeSalesEmp.DeptId.EqualsAny(dept.Select(x => x.DeptId).ToArray()))
                {
                    return "無法轉件於非業務，請重新檢視。";
                }
                else if (developeSalesEmp.DeptId == EmployeeChildDept.S001.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S002.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S003.ToString() ||
                    developeSalesEmp.DeptId == EmployeeChildDept.S004.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S005.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S006.ToString() ||
                    developeSalesEmp.DeptId == EmployeeChildDept.S007.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S008.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S010.ToString() ||
                    developeSalesEmp.DeptId == EmployeeChildDept.S012.ToString() || developeSalesEmp.DeptId == EmployeeChildDept.S013.ToString())
                {
                    if (operationSales == "")
                    {
                        return "負責業務1、2 欄位，沒有完整填入資料，無法儲存，請重新輸入。";
                    }
                    else if (!operationSalesEmp.IsLoaded)
                    {
                        return "Email帳號錯誤，請重新檢視。";
                    }
                }
                else if (operationSales != "" && !operationSalesEmp.IsLoaded)
                {
                    return "Email帳號錯誤，請重新檢視。";
                }

                return "";
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 若有刪除業務組別需重整編號
        /// </summary>
        /// <param name="resetSalesGroupSeller"></param>
        public static void ResetSalesGroup(List<Guid> resetSalesGroupSeller)
        {

            foreach (var s in resetSalesGroupSeller)
            {
                int maxNum = sp.SellerSaleGetBySellerGuid(s).Count() != 0 ? sp.SellerSaleGetBySellerGuid(s).Select(x => x.SalesGroup).Max() : 0;

                if (maxNum != 0)
                {
                    int group = 1;
                    for (int i = 1; i <= maxNum; i++)
                    {
                        SellerSale opSales = sp.SellerSaleGetBySellerGuid(s, i, 2);
                        if (opSales.IsLoaded)
                        {
                            opSales.SalesGroup = group;
                            sp.SellerSaleSet(opSales);
                        }

                        SellerSale deSales = sp.SellerSaleGetBySellerGuid(s, i, 1);
                        if (deSales.IsLoaded)
                        {
                            deSales.SalesGroup = group;
                            sp.SellerSaleSet(deSales);
                            group++;//放在開發在+因為經營可能為空
                        }
                    }
                }
            }
        }


        /// <summary>
        /// 該商家是否同意新版合約
        /// </summary>
        /// <param name="sid">商家sid</param>
        /// <param name="deliveryType">好康或宅配商家</param>
        /// <returns></returns>
        public static bool IsAgreeNewContractSeller(Guid sid, int deliveryType)
        {
            //8/1雙周結上線
            Seller s = sp.SellerGet(sid);
            if (deliveryType == (int)DeliveryType.ToShop)
            {
                if (s.CreateTime >= DateTime.Parse("2017/08/01"))
                {
                    return true;
                }
                else
                {
                    return !string.IsNullOrEmpty(s.ContractVersionPpon);
                }
            }
            else if (deliveryType == (int)DeliveryType.ToHouse)
            {
                var scf = sp.SellerContractFileGetByType(sid, (int)SellerContractType.Delivery).Where(x=>x.CreateTime > DateTime.Parse("2017/08/01"));
                if (s.CreateTime >= DateTime.Parse("2017/08/01") || scf.Count() > 0)
                {
                    return true;
                }
                else
                {
                    return !string.IsNullOrEmpty(s.ContractVersionHouse);
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// For 業績獎金 標記新舊檔
        /// </summary>
        /// <param name="isFirstExec"></param>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static bool ProcessDealSellerRelationship(bool isFirstExec, Guid? sellerGuid = null)
        {
            var startTime = DateTime.Now;
            
            var sellerDeals = sellerGuid == null 
                ? isFirstExec
                    ? sp.GetAllViewDealSellerRelationshipPreCollectionBeforeBaseDate(DateTime.Now) //昨日以前上檔檔次
                    : sp.GetAllViewDealSellerRelationshipPreCollectionByOneDays(DateTime.Now.AddDays(-1)) //前一日上檔檔次
                : sp.GetAllViewDealSellerRelationshipPreCollectionBySeller((Guid)sellerGuid);
            
            var sellerDealsDic = sellerDeals.GroupBy(x => x.SellerGuid).ToDictionary(
                s => s.Key,
                s => sellerDeals.Where(x => x.SellerGuid == s.Key).ToList());

            DealSellerRelationshipCollection col = new DealSellerRelationshipCollection();
            foreach (var sellerDealsKv in sellerDealsDic)
            {
                List<ViewDealSellerRelationshipPre> vpdsrList = sellerDealsKv.Value;

                DateTime newDealTime = DateTime.MinValue;
                foreach (var vpdsr in vpdsrList)
                {
                    string memo = vpdsr.Memo;
                    bool isNew = false;

                    if (vpdsr.IsNewDeal == true)
                    {
                        isNew = true;
                        memo = vpdsr.LastEndTime == null ? "首次合作" : "一年以上";

                        newDealTime = vpdsr.BusinessHourOrderTimeS; //暫存新檔上架時間
                    }
                    else
                    {
                        var days = new TimeSpan(vpdsr.BusinessHourOrderTimeS.Ticks - newDealTime.Ticks).Days;
                        if (days <= 15)
                        {
                            isNew = true;
                            memo = "15天內上檔";
                        }
                    }

                    DealSellerRelationship dsr = new DealSellerRelationship
                    {
                        SellerGuid = vpdsr.SellerGuid,
                        Bid = vpdsr.Bid,
                        BusinessHourOrderTimeS = vpdsr.BusinessHourOrderTimeS,
                        BusinessHourOrderTimeE = vpdsr.BusinessHourOrderTimeE,
                        LastEndTime = vpdsr.LastEndTime,
                        IsNewDeal = isNew,
                        Memo = memo
                    };
                    col.Add(dsr);
                }
            }

            bool isSuccess = false;
            if (col.Any())
            {
                isSuccess = isFirstExec
                    ? sp.TruncateAndBulkInsertBulkInsertDealSellerRelationshipCol(col)
                    : sp.BulkInsertBulkInsertDealSellerRelationshipCol(col);
            }

            var log = new {
                IsSuccess = isSuccess,
                IsFirstExec = isFirstExec,
                SellerCnt = sellerDealsDic.Count,
                ProcessRowCnt = col.Count,
                ElapsedSecs = (DateTime.Now - startTime).TotalSeconds
            };
            VourcherFacade.ChangeLogAdd("ProcessDealSellerRelationship", sellerGuid ?? Guid.Empty, new JsonSerializer().Serialize(log), false);
            
            return isSuccess;
        }
    }
}
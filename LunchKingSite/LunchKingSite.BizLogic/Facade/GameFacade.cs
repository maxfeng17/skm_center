﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models.Game;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.GameEntities;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Facade
{
    public class GameFacade
    {
        static GameFacade()
        {
            PayEvents.OnOrderCreatedEvents += PayEvents_OnOrderCreatedEvents;
        }
        /// <summary>
        /// 空的，是正常的，單純放在Global，用來確保會觸發 static GameFacade() 裏, 註冊事件的相關程式
        /// </summary>
        public static void Register()
        {
        }

        private static IGameProvider gp = ProviderFactory.Instance().GetDefaultProvider<IGameProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetDefaultProvider<IPponProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(GameFacade));
        /// <summary>
        /// 立即更新 GameRound 的狀態
        /// </summary>
        /// <param name="round"></param>
        public static void ResetGameGroundStatus(GameRound round, GameGame game, List<GameBuddy> buddies, string modifyId)
        {
            DateTime now = DateTime.Now;
            if (round.Status != GameRoundStatus.Running)
            {
                return;
            }
            if (game.RunOut || now > round.ExpiredTime)
            {
                //過期
                round.Status = GameRoundStatus.Fail;
                gp.SaveGameRound(round, modifyId);
            }
            else if (buddies.Count >= game.RulePeople)
            {
                bool runOut;                
                bool result = gp.ExecuteGameRoundCompletePlan(round, modifyId, out runOut);
                if (result)
                {
                    //完成通知
                    NotifyGameRoundComplete(round, true);
                }
                if (runOut)
                {
                    List<GameGame> games = gp.GetGamesByActivityId(game.ActivityId);
                    if (games.All(t => t.RunOut))
                    {
                        GameActivity activity = gp.GetGameActivity(game.ActivityId);
                        activity.RunOut = true;
                        gp.SaveGameActivity(activity, modifyId);
                    }
                }
            }
        }

        public static DateTime GetGameRoundExpiredTime(GameGame game, GameActivity activity, GameCampaign campaign)
        {
            DateTime absExpiredTime = campaign.EndTime;
            absExpiredTime = activity.EndTime < absExpiredTime ? activity.EndTime : absExpiredTime;
            DateTime expiredTime = DateTime.Now.AddHours(game.RuleHours);
            if (expiredTime > absExpiredTime)
            {
                expiredTime = absExpiredTime;
            }
            return expiredTime;
        }

        public static List<GameActivity> ImportGameActivitiesFromEligibleDeals(int topNum, GameCampaign campaign)
        {
            List<GameActivity> result = new List<GameActivity>();

            List<Guid> dealIds = gp.GetEligibleDealsDealIds(topNum);
            foreach (Guid dealId in dealIds)
            {
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealId, true);

                result.Add(new GameActivity
                {
                    CampaignId = campaign.Id,
                    ReferenceId = vpd.BusinessHourGuid,
                    Title = vpd.ItemName,
                    SubTitle = vpd.EventTitle,
                    Image = ImageFacade.GetDealAppPic(vpd, false),
                    Image2 = ImageFacade.GetDealPic(vpd),
                    StartTime = campaign.StartTime,
                    EndTime = campaign.EndTime,
                    Closed = false,
                    OriginalPrice = (int)vpd.ItemOrigPrice
                });
            }
            return result;
        }

        public static bool HasGameDealBuyPermission(int userId, Guid dealId, Guid orderGuid)
        {
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealId, true);
            if (theDeal.IsGame == false)
            {
                return false;
            }
            if (gp.GetUnexpiredGameDealPermission(userId, theDeal.BusinessHourGuid) != null)
            {
                return true;
            }
            if (gp.GetGameDearOrder(orderGuid) != null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 這個版本只會取得 Used == false 的 permission
        /// 也就是預期 一個permission(從遊戲完成取得)，只能完成一筆訂單
        /// 但目前不使用這版本 而是使用 HasGameDealBuyPermission
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="dealId"></param>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        [Obsolete("目前不使用這個版本")]
        public static bool HasGameDealBuyPermission_OneOrderPerPermission(int userId, Guid dealId, Guid orderGuid)
        {
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealId, true);
            if (theDeal.IsGame == false)
            {
                return false;
            }
            if (gp.GetAvailableGameDealPermission(userId, theDeal.BusinessHourGuid) != null)
            {
                return true;
            }
            if (gp.GetGameDearOrder(orderGuid) != null)
            {
                return true;
            }
            return false;
        }

        public static bool HasGameDealViewPermission(int userId, Guid dealId)
        {
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealId, true);
            if (theDeal.IsGame == false)
            {
                return false;
            }
            return gp.HasGameDealPermissionByMainOrSubDeal(userId, theDeal.BusinessHourGuid);
        }

        public static void UpdateGameCampaign(UpdateCampaignModel data, string modifyId)
        {
            var campaign = gp.GetGameCampaign(data.id);
            campaign.Title = data.Title;
            campaign.Image = data.Image;
            campaign.BonusTotal = data.BonusTotal;
            campaign.EnableBuddyReward = data.EnableBuddyReward;
            campaign.StartTime = data.StartTime.Value;
            campaign.EndTime = data.EndTime.Value.Date.AddDays(1).AddSeconds(-1);
            SaveGameCampaign(campaign, modifyId);
        }

        public static void UpdateGameActivity(UpdateActivityModel data, string modifyId)
        {
            int runOutCount = 0;
            foreach (var g in data.Games)
            {
                GameGame game = gp.GetGame(g.GameId);
                game.RulePeople = g.RulePeople;
                game.RuleHours = g.RuleHours;
                game.Maximum = g.Maximum;


                int count = gp.GetGameRoundByGameId(game.Id, GameRoundStatus.Completed).Count;
                if (count >= game.Maximum)
                {
                    game.RunOut = true;
                    runOutCount++;
                }
                else
                {
                    game.RunOut = false;
                }

                gp.SaveGameGame(game, modifyId);
            }


            var activity = gp.GetGameActivity(data.ActivityId);
            activity.Closed = data.Closed;

            if (data.Games.Count == runOutCount)
            {
                activity.RunOut = true;
            }
            else
            {
                activity.RunOut = false;
            }
            gp.SaveGameActivity(activity, modifyId);
        }
        public static void UpdateGameActivitySequence(List<UpdateActivityModel> data, string modifyId)
        {
            foreach (UpdateActivityModel a in data)
            {
                var activity = gp.GetGameActivity(a.ActivityId);
                activity.Seqence = a.Sequence;
                gp.SaveGameActivity(activity, modifyId);
            }
        }

        public static string GetLineFavourUrl(string favourUrl)
        {
            string text = "我在17Life有很想要的東西，需要你來幫我搶便宜！點連結->點授權->點幫忙砍價，就能完成。小小動作，大大幫助，愛你喔。(⺣◡⺣)♡*";
            //如果要轉換成短網址
            //favourUrl = WebUtility2.RequestShortUrl(favourUrl);
            if (CommonFacade.IsMobileVersion())
            {
                return string.Format("line://msg/text/{0}{1}{2}",
                   Uri.EscapeDataString(text), Uri.EscapeDataString(Environment.NewLine), Uri.EscapeDataString(favourUrl));
            }
            else
            {
                return string.Format("https://social-plugins.line.me/lineit/share?url={0}&text={1}&from=line_scheme",
                   Uri.EscapeDataString(favourUrl), Uri.EscapeDataString(text));
            }
        }

        public static string GetFacebookFavourUrl(string favourUrl)
        {
            //如果要轉換成短網址
            //favourUrl = WebUtility2.RequestShortUrl(favourUrl);
            return "https://www.facebook.com/sharer/sharer.php?u=" + Uri.EscapeDataString(favourUrl);
        }

        /// <summary>
        /// 遊戲完成，給參與者紅利金
        /// </summary>
        /// <returns>true: 完成獎勵動作</returns>
        public static RewardMemberGameBonusStatus RewardMemberGameBouns(SingleSignOnSource source, string externalId,
            int bounsValue, string receiverName, Guid roundGuid, out string message)
        {
            message = string.Empty;
            DateTime now = DateTime.Today;
            try
            {
                Member receiver = MemberFacade.GetMember(receiverName);
                if (receiver.IsLoaded == false || string.IsNullOrWhiteSpace(receiverName))
                {
                    message = "找不到符合的會員";
                    return RewardMemberGameBonusStatus.NoMatching;
                }
                GameActivity activity = gp.GetGameActivityByRoundGuid(roundGuid);
                if (activity == null)
                {
                    message = "找不到符合的資料";
                    return RewardMemberGameBonusStatus.NoMatching;
                }
                var campaign = gp.GetGameCampaign(activity.CampaignId);
                if (campaign == null || campaign.EnableBuddyReward == false)
                {
                    message = "紅利金已經發送完畢，感謝您參與活動。";
                    return RewardMemberGameBonusStatus.OverBonusLimit;
                }
                GameBuddy buddy = gp.GetGameBuddy(roundGuid, source, externalId);
                if (buddy == null)
                {
                    message = "找不到符合的資料";
                    return RewardMemberGameBonusStatus.NoMatching;
                }
                GameBuddyReward gameBuddyReward = gp.GetGameBuddyReward(buddy.Id);
                if (gameBuddyReward != null)
                {
                    message = "不能重複領取紅利金";
                    return RewardMemberGameBonusStatus.NoMatching;
                }
                GameBuddyReward reward = new GameBuddyReward
                {
                    BuddyId = buddy.Id,
                    ActivityId = activity.Id,
                    BonusValue = bounsValue,
                    DepositId = 0,
                    UserId = receiver.UniqueId,
                    TheDate = now.Date
                };
                bool doReward = gp.InsertGameBuddyReward(reward);

                if (doReward == false)
                {
                    message = "一個帳號同一天只能收到一次獎勵";
                    return RewardMemberGameBonusStatus.NoMatchingGrantCondition;
                }

                int depositId = MemberFacade.DepositMemberPromotion(
                    receiver.UniqueId, bounsValue, now, now.AddHours(48), campaign.Title, receiver.UserName, campaign.Guid);
                reward.DepositId = depositId;
                buddy.RewardId = reward.Id;
                gp.UpdateGameBuddyReward(reward);
                gp.SaveGameBuddy(buddy);
                return RewardMemberGameBonusStatus.Success;
            }
            catch (Exception ex)
            {
                logger.Warn("RewardMemberGameBouns fail", ex);
                message = "很抱歉，系統發生錯誤";
                return RewardMemberGameBonusStatus.NoMatching;
            }
        }

        /// <summary>
        /// 遊戲完成，給參與者紅利金
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>true: 完成獎勵動作</returns>
        public static bool IsGotBuddyRewardToday(int userId)
        {
            return gp.GetGameBuddyReward(userId, DateTime.Today) == null;
        }
        /// <summary>
        /// 己超過遊戲時間而沒有達成的，設定期狀態為失敗
        /// </summary>
        public static void SetExpiredGameRoundStatus()
        {
            foreach (GameRound gr in gp.GetEpiredRunningGameRounds())
            {
                gr.Status = GameRoundStatus.Fail;
                gp.SaveGameRound(gr, "job");
                GameFacade.NotifyGameRoundComplete(gr, false);
            }
            //gp.SetExpiredGameRoundStatus();
        }

        public static void ImportGameActivityAndGames(GameCampaign campaign, List<GameRule> rules, Guid dealId, string createId)
        {
            var activity = ImportGameActivity(campaign, dealId, createId);
            ImportGameGames(activity, rules, createId, 1);
        }

        /// <summary>
        /// 匯入Activity
        /// </summary>
        /// <param name="campaign"></param>
        /// <param name="dealIds"></param>
        /// <param name="createId"></param>
        /// <returns></returns>
        private static GameActivity ImportGameActivity(GameCampaign campaign, Guid dealId, string createId)
        {
            ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(dealId);
            GameActivity result = new GameActivity
            {
                CampaignId = campaign.Id,
                ReferenceId = vpd.BusinessHourGuid,
                Title = vpd.ItemName,
                SubTitle = vpd.EventTitle,
                Image = ImageFacade.GetDealAppPic(vpd, false),
                Image2 = ImageFacade.GetDealPic(vpd),
                StartTime = campaign.StartTime,
                EndTime = campaign.EndTime,
                Closed = true,
                OriginalPrice = Convert.ToInt32(vpd.ItemOrigPrice)
            };
            gp.SaveGameActivity(result, createId);

            return result;
        }

        /// <summary>
        /// 匯入Game
        /// </summary>
        /// <param name="activities"></param>
        /// <param name="rules"></param>
        /// <param name="createId"></param>
        /// <returns></returns>
        private static List<GameGame> ImportGameGames(
            GameActivity activity, List<GameRule> rules, string createId, int qty)
        {

            List<GameGame> games = new List<GameGame>();

            var bh = pp.BusinessHourGet(activity.ReferenceId);
            if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
            {
                var cdCol = pp.GetViewComboDealByBid(activity.ReferenceId, false);
                for (int i = 0; i < cdCol.Count; i++)
                {
                    var subDeal = cdCol[i];
                    var rule = rules[i];

                    GameGame game = new GameGame
                    {
                        ActivityId = activity.Id,
                        Title = subDeal.Title,
                        Price = (int)subDeal.ItemPrice,
                        OriginalPrice = (int)subDeal.ItemOrigPrice,
                        ReferenceId = subDeal.BusinessHourGuid,
                        //Maximum = (int)subDeal.OrderTotalLimit,
                        Maximum = qty,
                        RuleTitle = rule.Title,
                        RuleLevel = rule.Level,
                        RuleType = rule.Type,
                        RuleHours = rule.Hours,
                        RulePeople = rule.People
                    };
                    gp.SaveGameGame(game, createId);
                    games.Add(game);
                }
            }
            else
            {
                var rule = rules[0];
                ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(activity.ReferenceId);
                GameGame game = new GameGame
                {
                    ActivityId = activity.Id,
                    Title = deal.ItemName,
                    Price = (int)deal.ItemPrice,
                    OriginalPrice = (int)deal.ItemOrigPrice,
                    ReferenceId = deal.BusinessHourGuid,
                    //Maximum = (int)subDeal.OrderTotalLimit,
                    Maximum = 1,
                    RuleTitle = rule.Title,
                    RuleLevel = rule.Level,
                    RuleType = rule.Type,
                    RuleHours = rule.Hours,
                    RulePeople = rule.People
                };
                gp.SaveGameGame(game, createId);
                games.Add(game);
            }

            return games;
        }

        /// <summary>
        /// 遊戲完成
        /// </summary>
        public static void NotifyGameRoundComplete(GameRound round, bool notifySuccess)
        {
            int userId = round.OwnerId;
            Member mem = MemberFacade.GetMember(userId);
            if (mem.IsLoaded == false)
            {
                return;
            }
            GameActivity ga = gp.GetGameActivity(round.ActivityId);
            string notifyMessage;
            string mailSubject;
            string mailBody;
            string url = Helper.CombineUrl(config.SiteUrl, "game", "showGame?roundGuid=" + round.Guid);
            if (notifySuccess)
            {
                notifyMessage = string.Format("恭喜您，在朋友們的協助下幫忙“{0}“商品成功砍價，佔了便宜。別辜負朋友的好意，請於48小時內完成結帳。",
                    ga.Title);
                mailSubject = string.Format("17Life-[{0}]開團成功",
                    ga.Title);
                mailBody = string.Format("恭喜您，在朋友們的協助下幫忙“{0}“商品成功砍價，佔了便宜。別辜負朋友的好意，請於48小時內完成結帳。",
                    ga.Title);
            }
            else
            {
                notifyMessage = "差一點點..看來朋友們沒成功幫您佔到便宜，開團失敗。成團秘訣：先偷找好友，再開團請好友應援";
                mailSubject = string.Format("17Life-[{0}]開團失敗", ga.Title);
                mailBody = "差一點點..看來朋友們沒成功幫您佔到便宜，開團失敗。成團秘訣：先偷找好友，再開團請好友應援。";
            }
            mailBody += string.Format("<br /><a href='{0}'>連結</a>", url);
            
            NotificationFacade.PushCustomUrlMessage(userId, notifyMessage, url);
                    
            if (RegExRules.CheckEmail(mem.UserEmail))
            {
                MailMessage mail = new MailMessage
                {
                    From = new MailAddress(config.ServiceEmail, config.ServiceName)
                };
                mail.To.Add(mem.UserEmail);
                mail.IsBodyHtml = true;
                mail.Subject = mailSubject;
                mail.Body = mailBody;
                PostMan.Instance().Send(mail, SendPriorityType.Async);
            }
        }

        public static bool AddGameBuddy(Guid roundGuid, ExternalUser user, SingleSignOnSource source, string modifyId, out string message)
        {
            GameRound round = gp.GetGameRound(roundGuid);
            GameGame game = gp.GetGame(round.GameId);
            message = string.Empty;
            try
            {
                if (user == null)
                {
                    message = "請重新登入";
                    return false;
                }

                if (round.Status == GameRoundStatus.Fail || round.ExpiredTime < DateTime.Now)
                {
                    message = "活動已結束";
                    return false;
                }

                var buddies = gp.GetGameBuddyList(round.Id);
                if (buddies.Count >= game.RulePeople)
                {
                    message = "幫忙人數已滿";
                    return false;
                }

                if (buddies.Any(x => x.ExternalId == user.Id && x.Source == (int)source))
                {
                    message = "一個帳號只能砍價一次";
                    return false;
                }

                GameBuddy buddy = new GameBuddy
                {
                    RoundId = round.Id,
                    Source = (int)source,
                    Pic = user.Pic,
                    Name = user.FirstName + " " + user.LastName,
                    ExternalId = user.Id,
                    CreateTime = DateTime.Now
                };
                gp.SaveGameBuddy(buddy);

                buddies.Add(buddy);
                GameFacade.ResetGameGroundStatus(round, game, buddies, modifyId);

                return true;
            }
            catch (Exception ex)
            {
                logger.Warn("AddGameBuddy fail", ex);
                message = "很抱歉，系統發生錯誤";
                return false;
            }
        }

        public static string GetCurrentCampaignUrl(UrlHelper url)
        {            
            if (HttpContext.Current.Request.RawUrl.Contains("/home", StringComparison.OrdinalIgnoreCase)) {
                return "/event1111";
            } else {
                GameCampaign campaign = GetCurrentGameCampaign();
                if (campaign == null)
                {
                    return config.SiteUrl;
                }
                return url.Action("Home", new { campaignName = campaign.Name });
            }
        }

        public static GameCampaign GetCurrentGameCampaign()
        {            
            Wrapper<GameCampaign> wrapper = MemoryCache.Default.Get("CurrentGameCampaign") as Wrapper<GameCampaign>;
            if (wrapper == null)
            {
                wrapper = new Wrapper<GameCampaign>();
                wrapper.Item = gp.GetCurrentGameCampaign();                
                MemoryCache.Default.Set("CurrentGameCampaign", wrapper, new DateTimeOffset(DateTime.Now.AddMinutes(30)));
            }
            return wrapper.Item;
        }

        public static void ResetCurrentGameCampaign()
        {
            MemoryCache.Default.Remove("CurrentGameCampaign");
        }

        public static void SaveGameCampaign(GameCampaign campaign, string modifyId)
        {
            gp.SaveGameCampaign(campaign, modifyId);
            ResetCurrentGameCampaign();
        }


        private static void PayEvents_OnOrderCreatedEvents(Guid bid, int userId, Guid orderGuid, Core.PaymentType payType)
        {
            if (userId == 0 || orderGuid == Guid.Empty || bid == Guid.Empty)
            {
                return;
            }
            //GameDealPermission perm = gp.GetAvailableGameDealPermission(userId, bid);
            GameDealPermission perm = gp.GetUnexpiredGameDealPermission(userId, bid);
            if (perm == null)
            {
                return;
            }
            try
            {
                GameRound gr = gp.GetGameRound(perm.RoundId);
                gp.SaveGameDealOrder(
                    new GameDealOrder
                    {
                        ActivityId = gr.ActivityId,
                        GameId = gr.GameId,
                        OrderGuid = orderGuid,
                        UserId = userId,
                        PermissionId = perm.Id
                    });
                perm.Used = true;
                gp.SaveGameDealPermission(perm);
            }
            catch (Exception ex)
            {
                logger.WarnFormat("PayEvents_OnOrderCreatedEvents error. data={0}, ex={1}",
                    JsonConvert.SerializeObject(new { bid, userId, orderGuid, payType }), ex);
            }
        }

        protected class Wrapper<T>  where T  : class
        {
            public T Item { get; set; }
        }


        public static class PponAdapter
        {
            public static bool UserCanPlay(int userId, Guid dealId)
            {
                IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealId, true);
                int qty = 0;
                string message = string.Empty;
                PponBuyFacade.CheckAlreadyBoughtRestriction(userId, theDeal, out qty, out message);
                return qty > 0;
            }
        }
    }
}

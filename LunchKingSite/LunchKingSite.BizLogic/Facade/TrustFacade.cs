﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using SubSonic;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Enumeration;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.Util;

namespace LunchKingSite.BizLogic.Facade
{
    public class TrustFacade
    {
        protected static IMemberProvider mp;
        protected static IPponProvider pp;
        protected static IOrderProvider op;
        private static IPCPProvider pcp;
        private static ISysConfProvider config;
        protected static ILog logger = LogManager.GetLogger("TrustFacade");

        static TrustFacade()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        #region Trust Fix Tool
        public static DataTable TrustOverCoupon()
        {
            return mp.GetTrustOverCoupon();
        }

        public static DataTable CouponOverTrust()
        {
            return mp.GetCouponOverTrust();
        }

        public static DataTable TrustCouponEmpty()
        {
            return mp.GetTrustCouponEmpty();
        }

        public static string FillTrustEmptyCoupon(int checkDays = 3)
        {
            DataTable dt = mp.GetTrustCouponEmptyOrderDetailByDays(checkDays);
            string result = "";
            foreach (DataRow dr in dt.Rows)
            {
                string odid = dr["order_detail_guid"].ToString();
                CashTrustLogCollection ctlc = mp.CashTrustLogGetListByOrderDetailGuid(new Guid(odid));
                ViewPponCouponCollection vpcc = pp.ViewPponCouponGetListRegularOnly("order_detail_id", odid);
                if (ctlc.Count == vpcc.Count)
                {
                    int i;
                    for (i = 0; i < ctlc.Count; i++)
                    {
                        SPs.SpUpdateTrustCouponInfo(vpcc[i].SequenceNumber, vpcc[i].CouponId, ctlc[i].TrustId).Execute();

                        #region 補發票中的coupon_id
                        IEnumerable<EinvoiceMain> invoices = op.EinvoiceMainGetListByOrderGuid(ctlc[i].OrderGuid).Where(x => x.CouponId==null).OrderBy(x => x.Id);
                        if (invoices.Count() > 0 && vpcc.Where(x => !Helper.IsFlagSet(x.BusinessHourStatus, BusinessHourStatus.GroupCoupon)).Count() > 0)
                        {
                            EinvoiceMain invoice = invoices.First();
                            invoice.CouponId = vpcc[i].CouponId;
                            op.EinvoiceSetMain(invoice);
                        }
                        #endregion
                    }

                    result += odid + " 共 " + i.ToString() + " 筆資料已補coupon<br />";
                }
                else
                {
                    result +=odid+" 筆數不符<br />";
                }
            }

            return result;
        }
        #endregion

        #region 熟客點至信託

        /// <summary>
        /// 每個月準備資料信託
        /// 上個月一號至月底
        /// </summary>

        public static void RegularsPointToTrust(DateTime? now=null)
        {
            //string purpose = "熟客點至信託";
            //點數除三後，四捨五入轉台幣
            float comboRegulars = 3.0f;
            var queryDate = now ?? DateTime.Now;
            var firstLast = Helper.GetFirstAndLastDayOfMonth(queryDate.AddMonths(-1));
            DateTime firstDay = firstLast.Item1, lastDay = firstLast.Item2.SetTime(23, 59, 59);

            TrustRecordCollection trustColl = new TrustRecordCollection();

            #region 將一年以上的信託移出信託

            //_timeingMethod.Start();

            //var removeTrustPoint = pcp.PcpPointWithdrawalOverallGetByDatetime(DateTime.Now, DateTime.Now.AddSeconds(-1)
            //    , (int)PcpPointType.RegularsPoint);

            //_timeingMethod.Stop(purpose, "DB running", resultInfos);

            //if (trustPoint.Any())
            //{
            //    foreach (var item in trustPoint)
            //    {
            //        using (var trans = new TransactionScope())
            //        {
            //            trans.Complete();
            //        }
            //    }
            //}

            #endregion

            #region 將負向移出信託

            var removeTrustPoint = pcp.PcpPointWithdrawalOverallGetByDatetime(firstDay, lastDay, (int)PcpPointType.RegularsPoint);

            if (removeTrustPoint.Any())
            {
                foreach (var item in removeTrustPoint)
                {
                    var trustRecord = new TrustRecord();
                    trustRecord.MarkNew();
                    trustRecord.UserId = item.UserId;
                    trustRecord.Amount = 0 - (int)Math.Round(item.Point / comboRegulars, 0, MidpointRounding.AwayFromZero);
                    trustRecord.BankStatus = (int)TrustBankStatus.Initial;
                    trustRecord.CreateTime = DateTime.Now;
                    trustRecord.Message = string.Format("移出信託。（細節請查PCP扣項總表）");
                    trustRecord.TrustProvider = (int)TrustProvider.Initial;
                    trustRecord.SourceId = item.Id;
                    trustRecord.OrderClassification = (int)OrderClassification.RegularsOrder;

                    trustColl.Add(trustRecord);
                }
            }

            #endregion

            #region 將正向存入信託


            var trustPoint = pcp.PcpPointDepositGetByDatetime(firstDay, lastDay, (int)PcpPointType.RegularsPoint);
            
            if (trustPoint.Any())
            {
                foreach (var item in trustPoint)
                {
                    var trustRecord = new TrustRecord();
                    trustRecord.MarkNew();
                    trustRecord.UserId = item.UserId;
                    trustRecord.Amount = (int)Math.Round(item.Point / comboRegulars, 0, MidpointRounding.AwayFromZero);
                    trustRecord.BankStatus = (int)TrustBankStatus.Initial;
                    trustRecord.CreateTime = DateTime.Now;
                    trustRecord.Message = string.Format("熟客點儲值。");
                    trustRecord.TrustProvider = (int)TrustProvider.Initial;
                    trustRecord.SourceId = item.Id;
                    trustRecord.OrderClassification = (int)OrderClassification.RegularsOrder;

                    trustColl.Add(trustRecord);
                }
            }

            #endregion

            #region 先紀錄信託記錄

            try
            {
                //save
                mp.TrustRecordSet(trustColl);

                //將collection格式化寄出封存
                foreach (var item in trustColl)
                {
                    try
                    {
                        MailMessage msg = new MailMessage();
                        msg.To.Add(config.AdminEmail);
                        msg.Subject = string.Format("{0}：[系統]{1}", Environment.MachineName, "PCPCheckExpiration Running");
                        msg.From = new MailAddress(config.AdminEmail);
                        msg.IsBodyHtml = true;
                        msg.Body = "";
                        //PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    }
                    catch (Exception ex)
                    {
                        logger.Error("PCPCheck running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Warn(string.Format("{0}{1}{2}", ex.Source, ex.Message, ex.StackTrace));
            }

            #endregion

            #region 補金額差異項（熟客點跟台幣是3：1，所以可能差一些錢）

            //檢查當期有扣款的
            //取得點數餘額小於2的（因為除三後四捨五入應該變0）
            trustColl.Clear();
            var checkPcpPoint = pcp.PcpPointUsageLog(removeTrustPoint.Select(x => x.UserId).ToList<int>(), PcpPointType.RegularsPoint);

            //record fix
            var recordFix = mp.TrustRecordGetUserTotalAmount(removeTrustPoint.Select(x => x.UserId).ToList<int>(), DateTime.Now.Date);

            //檢查那個人的信託要不要補差異金額
            var trustFix = from point in checkPcpPoint
                           join trust in recordFix on point.UserId equals trust.UserId
                           let point2NT = (int)Math.Round(point.Total ?? 0 / comboRegulars, 0, MidpointRounding.AwayFromZero)
                           let diff = point2NT - trust.Amount
                           //每一筆異動差異+-1，所以要與負向異動數量相乘
                           let combo = removeTrustPoint.Count(x => x.UserId == point.UserId)
                           where diff != 0 && (-1 * combo <= diff || diff <= 1 * combo)
                           select new { UserId = point.UserId, Amount = 0 - diff };

            foreach (var item in trustFix)
            {
                var trustRecord = new TrustRecord();
                trustRecord.MarkNew();
                trustRecord.UserId = item.UserId;
                trustRecord.Amount = item.Amount;
                trustRecord.BankStatus = (int)TrustBankStatus.Initial;
                trustRecord.CreateTime = DateTime.Now;
                trustRecord.Message = string.Format("金額差異項");
                trustRecord.TrustProvider = (int)TrustProvider.Initial;
                trustRecord.OrderClassification = (int)OrderClassification.RegularsOrder;

                trustColl.Add(trustRecord);
            }

            if (trustColl.Any()) { mp.TrustRecordSet(trustColl); }
            
            #endregion
        }

        /// <summary>
        /// 信託統整進信託個人帳戶
        /// </summary>
        /// <param name="resultInfos"></param>
        public static void TrustToTrustBank(DateTime? now = null)
        {
            Guid guid = Guid.NewGuid();
            int totalTrust = 0, totalVerify = 0;

            //流程：
            //1.BY帳戶統計
            //2.計算此帳戶本期增減
            //3.查詢帳戶內所剩餘額
            //4.將本期增減與帳戶所剩餘額加總
            //5.將帳戶內有錢的金額信託，如果變成負的就不用信託
            //6.雨前一次信託金額做比較，金額大於上次信託則我們要信託，反之我們核銷
            //7.統計本期需信託額度

            //統計未建立的信託資料
            var unTrusted = mp.TrustRecordGetUnTrusted();
            List<int> updateUser = unTrusted.Select(x => x.UserId).Distinct().ToList<int>();
            //搜尋個人戶頭（最近一次的紀錄）
            var lastUserBank = mp.TrustUserBankGetLastUser(updateUser);
            //準備更新本季的銀行帳戶
            var currentUserBank = new TrustUserBankCollection();
            //1,2
            var currentMonthAmount = unTrusted.GroupBy(x => x.UserId).Select(x => new { user = x.Key, amount = x.Sum(c => c.Amount) });

            foreach (var item in currentMonthAmount)
            {
                var cub = new TrustUserBank();
                cub.MarkNew();
                cub.CreateTime = DateTime.Now;
                cub.TrustedReportGuid = guid;
                cub.UserId = item.user;

                //3
                var bank = lastUserBank.Where(x => x.UserId == item.user).DefaultIfEmpty(new TrustUserBank()).First();
                //4
                cub.Amount = item.amount + bank.Amount;
                //5
                cub.TrustedAmount = cub.Amount < 0 ? 0 : cub.Amount;
                //6
                int exChanged = cub.TrustedAmount - bank.TrustedAmount;
                if (exChanged > 0)
                {
                    totalTrust += exChanged;
                }
                else
                {
                    totalVerify += Math.Abs(exChanged);
                }
                currentUserBank.Add(cub);
                mp.TrustRecordSetTrust(item.user, (int)TrustProvider.TaiShin, guid);
            }

            mp.TrustUserBankSet(currentUserBank);
            
            var lastReport = mp.TrustVerificationReportGetLatest(TrustProvider.TaiShin, TrustVerificationReportType.PcpRegularsPoint);
            var lastAmount = lastReport.IsLoaded ? lastReport.TotalTrustAmount : 0;

            var queryDate = now ?? DateTime.Now;
            var firstLast = Helper.GetFirstAndLastDayOfMonth(queryDate.AddMonths(-1));
            DateTime firstDay = firstLast.Item1, lastDay = firstLast.Item2.SetTime(23, 59, 59);

            mp.TrustVerificationReportSet(new TrustVerificationReport
            {
                TrustProvider = (int)TrustProvider.TaiShin,
                ReportDate = DateTime.Now,
                ReportType = (int)TrustVerificationReportType.PcpRegularsPoint,
                UserId = "test",
                ReportIntervalStart = firstDay,
                ReportIntervalEnd = lastDay,
                AmountIncrease = totalTrust,
                AmountDecrease = totalVerify,
                TotalTrustAmount = lastAmount + totalTrust - totalVerify,
                ReportGuid = guid,
                TotalCount = currentMonthAmount.Count(x => x.amount != 0)
            });
        }

        /// <summary>
        /// 信託報表
        /// </summary>
        /// <param name="resultInfos"></param>
        public static void TrustToReport()
        {
            string purpose = "信託報表";
            Attachment attachOverview, attachUsers;

            ContentType ct = new ContentType("application/vnd.ms-excel");
            attachOverview = new Attachment(GetOverviewMemoryExcel(), ct);
            attachOverview.ContentDisposition.FileName = DateTime.Now.ToString() + "1.xls";

            attachUsers = new Attachment(GetUsersMemoryExcel(), ct);
            attachUsers.ContentDisposition.FileName = DateTime.Now.ToString() + "2.xls";

            try
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = string.Format("{0}：[系統]{1}", Environment.MachineName, purpose);
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = "test";
                msg.Attachments.Add(attachOverview);
                msg.Attachments.Add(attachUsers);
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                logger.Error("TrustToReport running alert Error:" + ex.Message + "<br/>" + ex.StackTrace);
            }
        }

        private static MemoryStream GetOverviewMemoryExcel()
        {
            Workbook workbook = new HSSFWorkbook();

            Sheet sheet = workbook.CreateSheet();

            #region 表頭 及 title
            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("信託日期");
            cols.CreateCell(1).SetCellValue("信託區間");
            cols.CreateCell(2).SetCellValue("本期增加");
            cols.CreateCell(3).SetCellValue("本期減少");
            cols.CreateCell(4).SetCellValue("總額");
            #endregion 表頭 及 title

            var data = mp.TrustVerificationReportGetLatest(TrustProvider.TaiShin, TrustVerificationReportType.PcpRegularsPoint);

            if (data.IsLoaded)
            {
                #region 報表內容
                Row row = sheet.CreateRow(1);
                row.CreateCell(0).SetCellValue(data.ReportDate.ToString("yyyy/MM/dd"));
                row.CreateCell(1).SetCellValue(string.Format("{0}~{1}", data.ReportIntervalStart, data.ReportIntervalEnd));
                row.CreateCell(2).SetCellValue(data.AmountIncrease);
                row.CreateCell(3).SetCellValue(data.AmountDecrease);
                row.CreateCell(4).SetCellValue(data.TotalTrustAmount);
                #endregion 報表內容
            }

            MemoryStream outputStream = new MemoryStream();
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        private static MemoryStream GetUsersMemoryExcel()
        {
            Workbook workbook = new HSSFWorkbook();

            Sheet sheet = workbook.CreateSheet();

            var data = mp.TrustRecordGetAll();

            if (data.Any())
            {
                foreach (var item in data.Select((value, index) => new { Index = index, Value = value }))
                {
                    Row row = sheet.CreateRow(item.Index);
                    row.CreateCell(0).SetCellValue(item.Value.UserId);
                    row.CreateCell(1).SetCellValue(item.Value.Amount);
                }
            }
            MemoryStream outputStream = new MemoryStream();
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        #endregion

        #region 信託報表

        public static TrustVerificationReport MonthTrustReport(TrustVerificationReport report)
        {
            Dictionary<string, byte[]> trustFile = new Dictionary<string, byte[]>();
            string filePath = config.TrustReportFtpPath;

            var scashOverYear = File.ReadLines(filePath + "憑證(滿一年).csv");
            var scashLessYear = File.ReadLines(filePath + "憑證(未滿一年).csv");
            var userAmount = File.ReadLines(filePath + "購物金(未滿一年).csv");

            report.ScashOverYearCount = scashOverYear.Count();
            report.ScashOverYearAmount = scashOverYear.Sum(x => int.Parse(x.Split(",")[1]));
            report.ScashLessYearCount = scashLessYear.Count();
            report.ScashLessYearAmount = scashLessYear.Sum(x => int.Parse(x.Split(",")[1]));
            report.UserCount = userAmount.Count();
            report.UserAmount = userAmount.Sum(x => int.Parse(x.Split(",")[1]));

            trustFile.Add("憑證(未滿一年).csv", File.ReadAllBytes(filePath + "憑證(未滿一年).csv"));
            trustFile.Add("憑證(滿一年).csv", File.ReadAllBytes(filePath + "憑證(滿一年).csv"));
            trustFile.Add("憑證.csv", File.ReadAllBytes(filePath + "憑證.csv"));
            trustFile.Add("購物金(未滿一年).csv", File.ReadAllBytes(filePath + "購物金(未滿一年).csv"));
            trustFile.Add("購物金(滿一年).csv", File.ReadAllBytes(filePath + "購物金(滿一年).csv"));
            trustFile.Add("信託報表.xls", XslTrustReport(report));
            //注意發信的夾檔限制大小
            report.ZipFile = Helper.ZipFile(trustFile);
            return report;
        }

        public static byte[] XslTrustReport(TrustVerificationReport report)
        {
            Workbook workbook = new HSSFWorkbook();
            MemoryStream ms = new MemoryStream();
            Sheet sheet = workbook.CreateSheet(report.ReportIntervalStart.Month + "月信託");

            #region 格式
            sheet.SetColumnWidth(0, 16 * 256);
            sheet.SetColumnWidth(1, 33 * 256);
            sheet.SetColumnWidth(2, 10 * 256);
            sheet.SetColumnWidth(3, 42 * 256);

            CellStyle cellHeaderStyle = workbook.CreateCellStyle();
            cellHeaderStyle.Alignment = HorizontalAlignment.CENTER;
            HSSFFont cellHeaderFont = (HSSFFont)workbook.CreateFont();
            cellHeaderFont.Boldweight = (short)FontBoldWeight.BOLD;
            cellHeaderFont.FontHeightInPoints = 22;
            cellHeaderStyle.SetFont(cellHeaderFont);

            HSSFCellStyle cellColumnStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            cellColumnStyle.FillForegroundColor = HSSFColor.LIGHT_YELLOW.index;
            cellColumnStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
            cellColumnStyle.BorderBottom = CellBorderType.THIN;
            cellColumnStyle.BorderLeft = CellBorderType.THIN;
            cellColumnStyle.BorderRight = CellBorderType.THIN;
            cellColumnStyle.BorderTop = CellBorderType.THIN;

            CellStyle cellValueStyle = workbook.CreateCellStyle();
            cellValueStyle.BorderBottom = CellBorderType.THIN;
            cellValueStyle.BorderLeft = CellBorderType.THIN;
            cellValueStyle.BorderRight = CellBorderType.THIN;
            cellValueStyle.BorderTop = CellBorderType.THIN;

            HSSFCellStyle cellCurrencyStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            HSSFDataFormat CurrencyFormat = (HSSFDataFormat)workbook.CreateDataFormat();
            cellCurrencyStyle.DataFormat = CurrencyFormat.GetFormat("$#,##0_);($#,##0)");
            cellCurrencyStyle.BorderBottom = CellBorderType.THIN;
            cellCurrencyStyle.BorderLeft = CellBorderType.THIN;
            cellCurrencyStyle.BorderRight = CellBorderType.THIN;
            cellCurrencyStyle.BorderTop = CellBorderType.THIN;

            #endregion

            sheet.CreateRow(0).CreateCell(0).SetCellValue("康太數位整合股份有限公司");
            sheet.GetRow(0).GetCell(0).CellStyle = cellHeaderStyle;
            sheet.GetRow(0).HeightInPoints = 27.75F;
            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));
            sheet.CreateRow(1).CreateCell(0).SetCellValue("本期信託核銷統計");
            sheet.GetRow(1).GetCell(0).CellStyle = cellHeaderStyle;
            sheet.GetRow(1).HeightInPoints = 27.75F;
            sheet.AddMergedRegion(new CellRangeAddress(1, 1, 0, 3));
            sheet.AddMergedRegion(new CellRangeAddress(2, 2, 0, 3));
            sheet.CreateRow(3).CreateCell(0).SetCellValue("委託人");
            sheet.GetRow(3).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(3).CreateCell(1).SetCellValue("康太數位整合股份有限公司");
            sheet.GetRow(3).GetCell(1).CellStyle = cellValueStyle;
            sheet.GetRow(3).CreateCell(2).SetCellValue("製表日期");
            sheet.GetRow(3).GetCell(2).CellStyle = cellColumnStyle;
            sheet.GetRow(3).CreateCell(3).SetCellValue(DateTime.Now.ToString("yyyy/MM/dd"));
            sheet.GetRow(3).GetCell(3).CellStyle = cellValueStyle;
            sheet.CreateRow(4).CreateCell(0).SetCellValue("受託人");
            sheet.GetRow(4).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(4).CreateCell(1).SetCellValue("台新銀行");
            sheet.GetRow(4).GetCell(1).CellStyle = cellValueStyle;
            sheet.GetRow(4).CreateCell(2).SetCellValue("本月區間");
            sheet.GetRow(4).GetCell(2).CellStyle = cellColumnStyle;
            sheet.GetRow(4).CreateCell(3).SetCellValue(report.ReportIntervalStart.ToString("yyyy/MM/dd HH:mm:ss") + " ~ " + report.ReportIntervalEnd.ToString("yyyy/MM/dd HH:mm:ss"));
            sheet.GetRow(4).GetCell(3).CellStyle = cellValueStyle;
            sheet.CreateRow(5).CreateCell(0).SetCellValue("期初未兌換餘額");
            sheet.GetRow(5).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(5).CreateCell(1).SetCellValue(report.BeginningNonexchangeAmount);
            sheet.GetRow(5).GetCell(1).CellStyle = cellCurrencyStyle;
            sheet.GetRow(5).CreateCell(2).CellStyle = cellCurrencyStyle;
            sheet.GetRow(5).CreateCell(3).CellStyle = cellCurrencyStyle;
            sheet.AddMergedRegion(new CellRangeAddress(5, 5, 1, 3));
            sheet.CreateRow(6).CreateCell(0).SetCellValue("本期增加總額");
            sheet.GetRow(6).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(6).CreateCell(1).SetCellValue(report.AmountIncrease);
            sheet.GetRow(6).GetCell(1).CellStyle = cellCurrencyStyle;
            sheet.GetRow(6).CreateCell(2).CellStyle = cellCurrencyStyle;
            sheet.GetRow(6).CreateCell(3).CellStyle = cellCurrencyStyle;
            sheet.AddMergedRegion(new CellRangeAddress(6, 6, 1, 3));
            sheet.CreateRow(7).CreateCell(0).SetCellValue("本期減少總額");
            sheet.GetRow(7).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(7).CreateCell(1).SetCellValue(report.AmountDecrease);
            sheet.GetRow(7).GetCell(1).CellStyle = cellCurrencyStyle;
            sheet.GetRow(7).CreateCell(2).CellStyle = cellCurrencyStyle;
            sheet.GetRow(7).CreateCell(3).CellStyle = cellCurrencyStyle;
            sheet.AddMergedRegion(new CellRangeAddress(7, 7, 1, 3));
            sheet.CreateRow(8).CreateCell(0).SetCellValue("期末未兌換餘額");
            sheet.GetRow(8).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(8).CreateCell(1).SetCellValue(report.ClosingNonexchangeAmount);
            sheet.GetRow(8).GetCell(1).CellStyle = cellCurrencyStyle;
            sheet.GetRow(8).CreateCell(2).CellStyle = cellCurrencyStyle;
            sheet.GetRow(8).CreateCell(3).CellStyle = cellCurrencyStyle;
            sheet.AddMergedRegion(new CellRangeAddress(8, 8, 1, 3));
            sheet.CreateRow(9).CreateCell(0).SetCellValue("基準日");
            sheet.GetRow(9).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(9).CreateCell(1).SetCellValue(report.ReportIntervalEnd.ToString("yyyy/MM/dd"));
            sheet.GetRow(9).GetCell(1).CellStyle = cellValueStyle;
            sheet.GetRow(9).GetCell(1).CellStyle.Alignment = HorizontalAlignment.RIGHT;
            sheet.GetRow(9).CreateCell(2).CellStyle = cellValueStyle;
            sheet.GetRow(9).CreateCell(3).CellStyle = cellValueStyle;
            sheet.AddMergedRegion(new CellRangeAddress(9, 9, 1, 3));
            sheet.CreateRow(10).CreateCell(0).SetCellValue("本期變動淨額");
            sheet.GetRow(10).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(10).CreateCell(1).SetCellValue(report.AmountIncrease - report.AmountDecrease);
            sheet.GetRow(10).GetCell(1).CellStyle = cellCurrencyStyle;
            sheet.GetRow(10).CreateCell(2).CellStyle = cellCurrencyStyle;
            sheet.GetRow(10).CreateCell(3).CellStyle = cellCurrencyStyle;
            sheet.AddMergedRegion(new CellRangeAddress(10, 10, 1, 3));
            sheet.AddMergedRegion(new CellRangeAddress(11, 11, 1, 3));
            sheet.CreateRow(12).CreateCell(0).SetCellValue("憑證");
            sheet.AddMergedRegion(new CellRangeAddress(12, 12, 1, 3));
            sheet.CreateRow(13).CreateCell(0).SetCellValue("本期增加總額");
            sheet.GetRow(13).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(13).CreateCell(1).SetCellValue(report.ScashIncrease.Value);
            sheet.GetRow(13).GetCell(1).CellStyle = cellCurrencyStyle;
            sheet.GetRow(13).CreateCell(2).CellStyle = cellCurrencyStyle;
            sheet.GetRow(13).CreateCell(3).CellStyle = cellCurrencyStyle;
            sheet.AddMergedRegion(new CellRangeAddress(13, 13, 1, 3));
            sheet.CreateRow(14).CreateCell(0).SetCellValue("本期減少總額");
            sheet.GetRow(14).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(14).CreateCell(1).SetCellValue(report.ScashDecrease.Value);
            sheet.GetRow(14).GetCell(1).CellStyle = cellCurrencyStyle;
            sheet.GetRow(14).CreateCell(2).CellStyle = cellCurrencyStyle;
            sheet.GetRow(14).CreateCell(3).CellStyle = cellCurrencyStyle;
            sheet.AddMergedRegion(new CellRangeAddress(14, 14, 1, 3));
            sheet.CreateRow(15).CreateCell(0).SetCellValue("本期變動淨額");
            sheet.GetRow(15).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(15).CreateCell(1).SetCellValue(report.ScashIncrease.Value - report.ScashDecrease.Value);
            sheet.GetRow(15).GetCell(1).CellStyle = cellCurrencyStyle;
            sheet.GetRow(15).CreateCell(2).CellStyle = cellCurrencyStyle;
            sheet.GetRow(15).CreateCell(3).CellStyle = cellCurrencyStyle;
            sheet.AddMergedRegion(new CellRangeAddress(15, 15, 1, 3));
            sheet.AddMergedRegion(new CellRangeAddress(16, 16, 1, 3));
            sheet.CreateRow(17).CreateCell(0).SetCellValue("購物金");
            sheet.AddMergedRegion(new CellRangeAddress(17, 17, 1, 3));
            sheet.CreateRow(18).CreateCell(0).SetCellValue("本期增加總額");
            sheet.GetRow(18).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(18).CreateCell(1).SetCellValue(report.UserIncrease.Value);
            sheet.GetRow(18).GetCell(1).CellStyle = cellCurrencyStyle;
            sheet.GetRow(18).CreateCell(2).CellStyle = cellCurrencyStyle;
            sheet.GetRow(18).CreateCell(3).CellStyle = cellCurrencyStyle;
            sheet.AddMergedRegion(new CellRangeAddress(18, 18, 1, 3));
            sheet.CreateRow(19).CreateCell(0).SetCellValue("本期減少總額");
            sheet.GetRow(19).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(19).CreateCell(1).SetCellValue(report.UserDecrease.Value);
            sheet.GetRow(19).GetCell(1).CellStyle = cellCurrencyStyle;
            sheet.GetRow(19).CreateCell(2).CellStyle = cellCurrencyStyle;
            sheet.GetRow(19).CreateCell(3).CellStyle = cellCurrencyStyle;
            sheet.AddMergedRegion(new CellRangeAddress(19, 19, 1, 3));
            sheet.CreateRow(20).CreateCell(0).SetCellValue("本期變動淨額");
            sheet.GetRow(20).GetCell(0).CellStyle = cellColumnStyle;
            sheet.GetRow(20).CreateCell(1).SetCellValue(report.UserIncrease.Value - report.UserDecrease.Value);
            sheet.GetRow(20).GetCell(1).CellStyle = cellCurrencyStyle;
            sheet.GetRow(20).CreateCell(2).CellStyle = cellCurrencyStyle;
            sheet.GetRow(20).CreateCell(3).CellStyle = cellCurrencyStyle;
            sheet.AddMergedRegion(new CellRangeAddress(20, 20, 1, 3));

            workbook.Write(ms);

            return ms.ToArray();
        }

        #endregion
    }
}

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Evaluate;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace LunchKingSite.BizLogic.Facade
{
    public class EvaluateFacade
    {

        private readonly IMemberProvider mp;
        private readonly IPponProvider pp;
        private readonly IVerificationProvider vp;

        public EvaluateFacade()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
        }

        private string ProductPageName { get { return "PPage"; } }

        public EvaluateData GetEvaluateProductData(string accountID, bool Is17LifeEmployee, ConditionObj condition)
        {
            //DT是評價的所有內容
            DataTable dt = new DataTable();
            DataTable pdDetails = new DataTable();
            List<Guid> bids = new List<Guid>();
            EvaluateData result = new EvaluateData() { DetailData = new List<EvaluateDetailData>() };
            List<VendorDealSalesCount> deliverConuting = new List<VendorDealSalesCount>();
            

            //將有評價的取出
            //權限檢查
            if (Is17LifeEmployee)
            {
                dt = mp.EvaluateStatisticsByBidGet(null);
            }
            else
            {
                var allowedDealUnits = (new VbsVendorAclMgmtModel(accountID)).GetAllowedDeals(VbsRightFlag.VerifyShop, DeliveryType.ToShop).ToList();
                var b = (from x in allowedDealUnits where x.MerchandiseGuid != null select new Guid(x.MerchandiseGuid.ToString()))
                    .Distinct().ToList();
                dt = mp.EvaluateStatisticsByBidGet(b, null);
            }

            bids = dt.AsEnumerable().Select(x => x.Field<Guid>("business_hour_guid")).ToList<Guid>();
            if (!bids.Any()) { result.totalCount = 0; return result; }

            //取得檔次資訊
            //ToDictionary加速後續查詢
            pdDetails = pp.ProductDetailGet(bids, condition.Range);
            var pdDetail = pdDetails.AsEnumerable().GroupBy(x => x.Field<Guid>("GUID")).ToDictionary(x => x.Key);

            //取得銷售資料
            deliverConuting = vp.GetPponVerificationSummary(bids);            
            
            int idx = 0;
            int page = condition.Page;

            foreach (var item in dt.AsEnumerable().GroupBy(x => new { bid = x.Field<Guid>("business_hour_guid"), mid = x.Field<Guid>("mid") }))
            {
                //從所有評價查詢有權限的資訊
                if (!pdDetail.ContainsKey(item.Key.bid)) { continue; }                
                var product = pdDetail[item.Key.bid].FirstOrDefault();
                if (product == null) { continue; }

                EvaluateDetailData detail = new EvaluateDetailData() { QuesCount = new List<QuestionCount>() };                

                detail.Bid = item.Key.bid;
                GenerateComonData(product, ref detail, item);
                detail.PageNo = string.Format("{0}_{1}_{2}", ProductPageName, page, idx++);

                detail.DealCount = deliverConuting.Where(xx => xx.MerchandiseGuid == item.Key.bid || xx.MerchandiseGuid == item.Key.mid)
                        .Select(
                        xx => xx.VerifiedCount + xx.UnverifiedCount + xx.ReturnedCount - xx.BeforeDealEndReturnedCount
                        ).DefaultIfEmpty(0).First();

                detail.VerifiedCount = deliverConuting.Where(xx => xx.MerchandiseGuid == item.Key.bid || xx.MerchandiseGuid == item.Key.mid)
                        .Select(xx => xx.VerifiedCount).DefaultIfEmpty(0).First();

                detail.WithSubStore = pdDetail[item.Key.bid].Count() > 1;
                result.DetailData.Add(detail);
            }

            result.totalCount = result.DetailData.Count();

            return result;
        }
        public EvaluateData GetEvaluateDetailData(string accountID, bool Is17LifeEmployee, ConditionObj condition, Guid bid, Guid sid)
        {
            //DT是評價的所有內容
            DataTable dt = new DataTable();
            DataTable pdDetails = new DataTable();
            //List<Guid> bids = new List<Guid>();
            List<VendorDealSalesCount> deliverConuting = new List<VendorDealSalesCount>();

            //List<VbsVendorAce> allowedDealUnits = (new VbsVendorAclMgmtModel(accountID)).GetAllowedToShopDealUnits(null).ToList();

            //將有評價的取出
            dt = mp.EvaluateStatisticsByBidGet(new List<Guid> { (Guid)bid }, null);
            //bids = new List<Guid> { (Guid)bid };
            //取得檔次資訊
            //ToDictionary加速後續查詢
            pdDetails = pp.ProductDetailGet(new List<Guid> { (Guid)bid }, condition.Range);
            var pdDetail = pdDetails.AsEnumerable().GroupBy(x => x.Field<Guid>("sid")).ToDictionary(x => x.Key);

            //取得銷售資料
            //過濾分店權限Is17LifeEmployee

            deliverConuting = vp.GetPponVerificationSummaryGroupByStore(new List<Guid> { (Guid)bid });

            //if (!Is17LifeEmployee || sid!=null)
            if (!Is17LifeEmployee)
            {
                List<Guid> allowedStore = (new VbsVendorAclMgmtModel(accountID)).GetAllowedDealStores((Guid)bid).ToList();
                deliverConuting = deliverConuting.Where(jj => jj.StoreGuid.HasValue && allowedStore.Contains(jj.StoreGuid.Value)).ToList();
                dt = dt.AsEnumerable().Where(jj => allowedStore.Contains(jj.Field<Guid>("store_guid"))).CopyToDataTable();
            }

            if (sid != null)
            {
                deliverConuting = deliverConuting.Where(jj => jj.StoreGuid == sid).ToList();
                pdDetail = pdDetail.Where(x => x.Key == sid).Select(x => x).ToDictionary(x => x.Key, x => x.Value);
            }

            EvaluateData result = new EvaluateData() { DetailData = new List<EvaluateDetailData>() };
            int idx = 0;
            int page = condition.Page;

            foreach (var item in dt.AsEnumerable().GroupBy(x => new { bid = x.Field<Guid>("business_hour_guid"), sid = x.Field<Guid>("store_guid") }))
            {
                //從所有評價查詢有權限的資訊
                if (!pdDetail.ContainsKey(item.Key.sid)) { continue; }
                var product = pdDetail[item.Key.sid].FirstOrDefault();
                if (product == null) { continue; }

                EvaluateDetailData detail = new EvaluateDetailData() { QuesCount = new List<QuestionCount>() };

                detail.Bid = item.Key.bid;
                GenerateComonData(product, ref detail, item);
                detail.WithSubStore = pdDetail[item.Key.sid].Count() > 1;
                detail.PageNo = string.Format("{0}_{1}_{2}", ProductPageName, page, idx++);

                detail.DealCount = deliverConuting.Where(xx => xx.MerchandiseGuid == item.Key.bid)
                        .Select(
                        xx => xx.VerifiedCount + xx.UnverifiedCount + xx.ReturnedCount - xx.BeforeDealEndReturnedCount
                        ).DefaultIfEmpty(0).First();

                detail.VerifiedCount = deliverConuting.Where(xx => xx.MerchandiseGuid == item.Key.bid)
                        .Select(xx => xx.VerifiedCount).DefaultIfEmpty(0).First();

                result.DetailData.Add(detail);
            }

            result.totalCount = result.DetailData.Count();

            return result;
        }
        public EvaluateStoreData GetEvaluateStoreListForCustomer(string accountID, bool Is17LifeEmployee, ConditionObj condition, Guid? seller_id = null)
        {
            if (Is17LifeEmployee && seller_id != null)
            {
                var aclc = mp.ResourceAclGetListByResourceGuidList(new List<Guid> { seller_id.Value });
                accountID = aclc.Select(x => x.AccountId).FirstOrDefault();
            }

            EvaluateStoreData result = new EvaluateStoreData() { DetailData = new List<EvaluateStoreDetail>() };
            DataTable dt = new DataTable();
            DataTable pdDetails = new DataTable();
            var stores = (new VbsVendorAclMgmtModel(accountID)).GetAllowedDealStores();
            //dt = mp.EvaluateStoreListForCustomer(accountID, condition.Range);
            dt = mp.EvaluateStoreListForCustomer3(stores, condition.Range);
            var ctl = vp.CashTrustLogGetListByStore(stores);
            var StoreList = dt.AsEnumerable().GroupBy(x => new { sid = x.Field<Guid?>("store_guid"), store_name = x.Field<string>("store_name"), seller_guid = x.Field<Guid?>("seller_guid") }).ToDictionary(x => x.Key);

            foreach (var item in StoreList)
            {
                EvaluateStoreDetail DetailData = new EvaluateStoreDetail() { Rating = new List<EvaluateDetailRating>() };
                //已評價
                int rated = 0;
                //3有評
                int rated_environment = 0;
                //4有評
                int rated_service = 0;

                DetailData.StoreName = item.Key.store_name;
                DetailData.StoreId = item.Key.sid.Value;
                DetailData.SellerGuid = item.Key.seller_guid.Value;
                EvaluateDetailRating DetailRating = new EvaluateDetailRating();

                foreach (var row in dt.AsEnumerable().Where(x => (x.Field<Guid?>("store_guid") == item.Key.sid))
                    .GroupBy(g => new { store_guid = g.Field<Guid?>("store_guid"), item_id = g.Field<int>("item_id") })
                    .Select(t => new { t }).ToList())
                {
                    int item_id = row.t.Select(t => t.Field<int>("item_id")).FirstOrDefault();
                    switch (item_id)
                    {
                        case 1:
                            rated = row.t.Where(x => x.Field<int>("item_id") == item_id).Count();
                            DetailRating.Q1 = row.t.Where(x => x.Field<int>("item_id") == item_id).Sum(t => t.Field<int>("rating"));
                            break;
                        case 2:
                            DetailRating.Q2 = row.t.Where(x => x.Field<int>("item_id") == item_id).Sum(t => t.Field<int>("rating"));
                            break;
                        case 6:
                            rated_environment = row.t.Where(x => x.Field<int>("item_id") == item_id).Count();
                            DetailRating.Q3 = row.t.Where(x => x.Field<int>("item_id") == item_id).Sum(t => t.Field<int>("rating"));
                            break;
                        case 4:
                            rated_service = row.t.Where(x => x.Field<int>("item_id") == item_id).Count();
                            DetailRating.Q4 = row.t.Where(x => x.Field<int>("item_id") == item_id).Sum(t => t.Field<int>("rating"));
                            break;
                    }
                }

                DetailData.Rating.Add(DetailRating);
                DetailData.AvgTotalSatisfactory = (rated != 0) ? Math.Round(((double)DetailRating.Q1 / (double)rated), 1) : 0;
                DetailData.AvgComeAgain = (rated != 0) ? Math.Round(((double)DetailRating.Q2 / (double)rated), 1) : 0;
                DetailData.AvgEnvironment = (rated_environment != 0) ? Math.Round(((double)DetailRating.Q3 / (double)rated_environment), 1) : 0;
                DetailData.AvgService = (rated_service != 0) ? Math.Round(((double)DetailRating.Q4 / (double)rated_service), 1) : 0;
                DetailData.EvaluateCount = rated;
                DetailData.EnvironmentCount = rated_environment;
                DetailData.ServiceCount = rated_service;
                DetailData.VerifiedCount = ctl.Count(x => x.VerifiedStoreGuid == item.Key.sid.Value);
                
                result.DetailData.Add(DetailData);
            }
            result.TotalCount = result.DetailData.Count;

            switch (condition.Order)
            {
                case (int)EvaluateStoreOrderDesc.TotalSatisfactoryHighToLow:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.AvgTotalSatisfactory).ToList();
                    break;
                case (int)EvaluateStoreOrderDesc.TotalSatisfactoryLowToHight:
                    result.DetailData = result.DetailData.OrderBy(x => x.AvgTotalSatisfactory).ToList();
                    break;
                case (int)EvaluateStoreOrderDesc.ComeAgainHighToLow:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.AvgComeAgain).ToList();
                    break;
                case (int)EvaluateStoreOrderDesc.ComeAgainLowToHight:
                    result.DetailData = result.DetailData.OrderBy(x => x.AvgComeAgain).ToList();
                    break;
                case (int)EvaluateStoreOrderDesc.EnvironmentHighToLow:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.AvgEnvironment).ToList();
                    break;
                case (int)EvaluateStoreOrderDesc.EnvironmentLowToHight:
                    result.DetailData = result.DetailData.OrderBy(x => x.AvgEnvironment).ToList();
                    break;
                case (int)EvaluateStoreOrderDesc.ServiceHighToLow:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.AvgService).ToList();
                    break;
                case (int)EvaluateStoreOrderDesc.ServiceLowToHight:
                    result.DetailData = result.DetailData.OrderBy(x => x.AvgService).ToList();
                    break;
            }

            return result;
        }

        public EvaluateStoreData GetEvaluateStoreList(string accountID, bool Is17LifeEmployee, EvaluateCondition ConditionIndex, string ConditionValue)
        {
            DataTable dt = new DataTable();
            EvaluateStoreData result = new EvaluateStoreData() { DetailData = new List<EvaluateStoreDetail>() };
            dt = ((int)ConditionIndex > 2) ? mp.EvaluateStoreListBySeller() : mp.EvaluateStoreList();
            
            EnumerableRowCollection<DataRow> c = dt.AsEnumerable();
            if (!string.IsNullOrWhiteSpace(ConditionValue))
            {
                switch (ConditionIndex)
                {
                    case EvaluateCondition.UniqueId:
                        int uid = (int.TryParse(ConditionValue, out uid) ? Convert.ToInt32(ConditionValue) : 0);
                        c = c.Where(x => x.Field<int>("unique_id") == uid);
                        break;
                    case EvaluateCondition.Name:
                        c = c.Where(x => x.Field<string>("item_name").Contains(ConditionValue));
                        break;
                    case EvaluateCondition.SellerName:
                        c = c.Where(x => x.Field<string>("seller_name").Contains(ConditionValue));
                        break;
                    case EvaluateCondition.SellerId:
                        c = c.Where(x => x.Field<string>("seller_id") == ConditionValue);
                        break;
                }
            }

            foreach (var item in c)
            {
                EvaluateStoreDetail store = new EvaluateStoreDetail();
                store.Bid = item.Field<Guid?>("business_hour_guid");
                store.StoreName = item.Field<string>("store_name");
                store.UniqueID = item.Field<int?>("unique_id");
                store.ItemName = item.Field<string>("item_name");
                store.SellerName = item.Field<string>("seller_name");
                store.SellerId = item.Field<string>("seller_id");
                store.SellerGuid = item.Field<Guid?>("seller_guid");
                store.StoreId = item.Field<Guid?>("guid");
                store.DeliverTimeS = item.Field<DateTime?>("business_hour_deliver_time_s");
                store.DeliverTimeE = item.Field<DateTime?>("business_hour_deliver_time_e");
                store.StoreCount = item.Field<Guid?>("seller_guid").HasValue ? SellerFacade.GetChildrenSellerGuid(item.Field<Guid?>("seller_guid").Value).Count() : 0;
                result.DetailData.Add(store);
            }

            result.ConditionIndex = ConditionIndex;
            result.TotalCount = result.DetailData.Count();
            return result;
        }

        public EvaluateData GetEvaluateStoreData(string accountID, bool Is17LifeEmployee, ConditionObj condition, Guid bid, Guid? sid)
        {
            //DT是評價的所有內容
            DataTable dt = new DataTable();
            DataTable pdDetails = new DataTable();
            //List<Guid> bids = new List<Guid>();
            List<VendorDealSalesCount> deliverConuting = new List<VendorDealSalesCount>();

            var allowedDealUnits = (new VbsVendorAclMgmtModel(accountID)).GetAllowedDeals(VbsRightFlag.VerifyShop, DeliveryType.ToShop).ToList();

            //將有評價的取出
            dt = mp.EvaluateStatisticsByBidGet(new List<Guid> { (Guid)bid }, null);
            //bids = new List<Guid> { (Guid)bid };
            //取得檔次資訊
            //ToDictionary加速後續查詢
            pdDetails = pp.ProductDetailGet(new List<Guid> { (Guid)bid }, condition.Range);
            var pdDetail = pdDetails.AsEnumerable().GroupBy(x => x.Field<Guid>("sid")).ToDictionary(x => x.Key);

            //取得銷售資料
            //過濾分店權限Is17LifeEmployee

            deliverConuting = vp.GetPponVerificationSummaryGroupByStore(new List<Guid> { (Guid)bid });

            //if (!Is17LifeEmployee || sid!=null)
            if (!Is17LifeEmployee)
            {
                List<Guid> allowedStore = (new VbsVendorAclMgmtModel(accountID)).GetAllowedDealStores((Guid)bid).ToList();
                deliverConuting = deliverConuting.Where(jj => allowedStore.Contains(jj.StoreGuid.Value)).ToList();
                dt = dt.AsEnumerable().Where(jj => allowedStore.Contains(jj.Field<Guid>("store_guid"))).CopyToDataTable();
            }

            if (sid != null)
            {
                deliverConuting = deliverConuting.Where(jj => jj.StoreGuid == sid).ToList();
                pdDetail = pdDetail.Where(x => x.Key == sid).Select(x => x).ToDictionary(x => x.Key, x => x.Value);
            }

            EvaluateData result = new EvaluateData() { DetailData = new List<EvaluateDetailData>() };
            int idx = 0;
            int page = condition.Page;

            foreach (var item in dt.AsEnumerable().GroupBy(x => new { bid = x.Field<Guid>("business_hour_guid"), sid = x.Field<Guid>("store_guid") }))
            {
                //從所有評價查詢有權限的資訊
                if (!pdDetail.ContainsKey(item.Key.sid)) { continue; }
                var product = pdDetail[item.Key.sid].FirstOrDefault();
                if (product == null) { continue; }


                EvaluateDetailData detail = new EvaluateDetailData() { QuesCount = new List<QuestionCount>() };

                detail.Bid = item.Key.bid;
                GenerateComonData(product, ref detail, item);
                detail.WithSubStore = pdDetail[item.Key.sid].Count() > 1;
                detail.PageNo = string.Format("{0}_{1}_{2}", ProductPageName, page, idx++);

                detail.DealCount = deliverConuting.Where(xx => xx.StoreGuid == item.Key.sid)
                        .Select(
                        xx => xx.VerifiedCount + xx.UnverifiedCount + xx.ReturnedCount - xx.BeforeDealEndReturnedCount
                        ).DefaultIfEmpty(0).First();

                detail.VerifiedCount = deliverConuting.Where(xx => xx.StoreGuid == item.Key.sid)
                        .Select(xx => xx.VerifiedCount).DefaultIfEmpty(0).First();

                result.DetailData.Add(detail);
            }

            result.totalCount = result.DetailData.Count();

            return result;
        }

        /// <summary>
        /// 查詢某分店某檔次評價
        /// </summary>
        /// <param name="accountID"></param>
        /// <param name="Is17LifeEmployee"></param>
        /// <param name="condition"></param>
        /// <param name="sid"></param>
        /// <returns></returns>
        public EvaluateData GetEvaluateAllBidsData(string accountID, bool Is17LifeEmployee, ConditionObj condition, Guid sid)
        {
            //DT是評價的所有內容
            DataTable dt = new DataTable();
            DataTable pdDetails = new DataTable();
            List<VendorDealSalesCount> deliverConuting = new List<VendorDealSalesCount>();

            if (!Is17LifeEmployee)
            {
                var allowedDealUnits = (new VbsVendorAclMgmtModel(accountID)).GetAllowedDeals(VbsRightFlag.VerifyShop, DeliveryType.ToShop).ToList();
                var ids = allowedDealUnits.Where(x => x.StoreGuid == sid && x.MerchandiseGuid != null).Select(x => (Guid)x.MerchandiseGuid).ToList<Guid>();
                if (!ids.Any()) { return null; }
                dt = mp.EvaluateStatisticsByBidGet(ids, sid);
            }
            else
            {
                dt = mp.EvaluateStatisticsByBidGet(sid);
            }            

            EvaluateData result = new EvaluateData() { DetailData = new List<EvaluateDetailData>() };

            if (dt.AsEnumerable().Any())
            {
                //將有評價的取出
                var allowedDeals = dt.AsEnumerable().Select(x => x.Field<Guid>("business_hour_guid")).ToList();
                //取得檔次資訊
                //ToDictionary加速後續查詢
                pdDetails = pp.ProductDetailGet(allowedDeals, condition.Range);
                var pdDetail = pdDetails.AsEnumerable().Where(x => x.Field<Guid>("sid") == sid).GroupBy(x => x.Field<Guid>("guid")).ToDictionary(x => x.Key);

                //取得銷售資料

                deliverConuting = vp.GetPponVerificationSummaryGroupByStore(allowedDeals);
                deliverConuting = deliverConuting.Where(x => x.StoreGuid == sid).ToList();

                int idx = 0;
                int page = condition.Page;

                foreach (var item in dt.AsEnumerable().GroupBy(x => new { bid = x.Field<Guid>("business_hour_guid") }))
                {
                    //從所有評價查詢有權限的資訊
                    if (!pdDetail.ContainsKey(item.Key.bid)) { continue; }
                    var product = pdDetail[item.Key.bid].FirstOrDefault();
                    if (product == null) { continue; }


                    EvaluateDetailData detail = new EvaluateDetailData() { QuesCount = new List<QuestionCount>() };

                    detail.Bid = item.Key.bid;
                    GenerateComonData(product, ref detail, item);
                    detail.WithSubStore = false;
                    detail.PageNo = string.Format("{0}_{1}_{2}", ProductPageName, page, idx++);

                    detail.DealCount = deliverConuting.Where(x => x.MerchandiseGuid == item.Key.bid).Select(
                            xx => xx.VerifiedCount + xx.UnverifiedCount + xx.ReturnedCount - xx.BeforeDealEndReturnedCount
                            ).DefaultIfEmpty(0).First();

                    detail.VerifiedCount = deliverConuting.Where(x => x.MerchandiseGuid == item.Key.bid).Select(xx => xx.VerifiedCount).DefaultIfEmpty(0).First();

                    result.DetailData.Add(detail);
                }

                result.totalCount = result.DetailData.Count();
            }

            return result;
        }

        private void GenerateComonData(DataRow row, ref EvaluateDetailData detail, IGrouping<object, DataRow> item)
        {
            detail.Sid = row.Field<Guid>("sid");
            detail.ItemName = row.Field<string>("item_name");
            detail.UniqueID = row.Field<int>("unique_id");
            detail.EventName = row.Field<string>("event_name");
            detail.DeliverTimeE = row.Field<DateTime>("business_hour_deliver_time_e");
            detail.DeliverTimeS = row.Field<DateTime>("business_hour_deliver_time_s");
            detail.ItemName = row.Field<string>("item_name");
            detail.StoreName = row.Field<string>("store_name");

            Tuple<int, int, List<QuestionCount>> statisticsResult = StatisticsQuestions(item);

            detail.EvaluateCount = statisticsResult.Item1;
            detail.CommentCount = statisticsResult.Item2;
            detail.QuesCount = statisticsResult.Item3;
            detail.AvgTotalSatisfactory = detail.QuesCount.Where(x => x.Qid == 1).Select(x => x.AvgStar).DefaultIfEmpty(0).FirstOrDefault();
            detail.AvgComeAgain = detail.QuesCount.Where(x => x.Qid == 2).Select(x => x.AvgStar).DefaultIfEmpty(0).FirstOrDefault();

        }
                
        private Tuple<int, int, List<QuestionCount>> StatisticsQuestions(IGrouping<object, DataRow> item)
        {
            int commentMemberCount = 0;
            int commentCount = 0;
            List<QuestionCount> listResult = new List<QuestionCount>();
            HashSet<Guid> trustCounter = new HashSet<Guid>();
            Dictionary<int, int> statistics = new Dictionary<int, int>();
            statistics.Add(1, 0);
            statistics.Add(2, 0);
            statistics.Add(6, 0);
            statistics.Add(4, 0);
            Dictionary<int, int> star = new Dictionary<int, int>();
            var rateAndCount = new Dictionary<int, Dictionary<int, int>>();
            foreach (var i in item)
            {
                QuestionDetail qDetail = new QuestionDetail();

                qDetail.TrustID = i.Field<Guid>("trust_id");
                qDetail.Qid = i.Field<int>("item_id");

                if (i.Field<int>("evaluate_type") == (int)EvaluateType.Rate) { qDetail.QIntValue = i.Field<int>("rating"); }
                else { qDetail.QIntValue = 0; }

                //已評分的人數
                trustCounter.Add(qDetail.TrustID);
                //統計問題的數量 ex: Q1 共 15人作答
                if (!statistics.ContainsKey(qDetail.Qid)) { statistics[qDetail.Qid] = 0; }
                statistics[qDetail.Qid] += 1;

                //統計問題的星等 ex: Q1 所有的星等加起來為35顆星（區間0～15*5）
                //然後等等在合併，就是平均星等
                if (!star.ContainsKey(qDetail.Qid)) { star[qDetail.Qid] = 0; }
                star[qDetail.Qid] += qDetail.QIntValue;

                //統計每個問題星等的數量 ex: star1:5票 star2:4票 star3:3票 star4:2票 star5:1票
                //QID為主KEY
                //然後問題為副KEY 星等為值
                if (!rateAndCount.ContainsKey(qDetail.Qid) || rateAndCount[qDetail.Qid] == null)
                {
                    rateAndCount[qDetail.Qid] = new Dictionary<int, int>();
                }

                if (!rateAndCount[qDetail.Qid].ContainsKey(qDetail.QIntValue))
                {
                    rateAndCount[qDetail.Qid][qDetail.QIntValue] = 0;
                }

                rateAndCount[qDetail.Qid][qDetail.QIntValue] += 1;
            }

            commentMemberCount = trustCounter.Count();

            int comments;
            commentCount = statistics.TryGetValue(5, out comments) ? comments : 0;

            listResult = (
                from sta in statistics
                join sar in star on sta.Key equals sar.Key into merge
                from c in merge.DefaultIfEmpty()
                select new QuestionCount
                {
                    Qid = sta.Key,
                    Counting = sta.Value,
                    AvgStar = sta.Value == 0 ? 0 : Math.Round((double)c.Value / (double)sta.Value, 1),
                    RateAndCount = rateAndCount.ContainsKey(sta.Key) ? rateAndCount[sta.Key].ToDictionary(aa => aa.Key.ToString(), aa => aa.Value.ToString()) : new Dictionary<string, string>
                    {
                        {sta.Key.ToString(),"0"}
                    }
                }
                ).ToList<QuestionCount>();

            return new Tuple<int, int, List<QuestionCount>>(commentMemberCount, commentCount, listResult);
        }

        public List<EvaluateDetailRating> GetEvaluateDetailData(Guid bid, Guid sid)
        {
            List<EvaluateDetailRating> result;
            DataTable dt = mp.EvaluateDetailRatingGetTable(bid, sid);
            result = (from x in dt.AsEnumerable()
                      select new EvaluateDetailRating
                      {
                          ItemName = x.Field<string>("item_name"),
                          StoreName = x.Field<string>("store_name"),
                          CreatTime = x.Field<DateTime>("create_time"),
                          CouponSequenceNumber = x.Field<string>("coupon_sequence_number"),
                          VerifiedTime = x.Field<DateTime?>("usage_verified_time"),
                          Q1 = x.Field<int>("1"),
                          Q2 = x.Field<int>("2"),
                          Q3 = x.Field<int>("3"),
                          Q4 = x.Field<int>("4"),
                          Comment = x.Field<string>("Comment")
                      }).ToList();
            return result;
        }

        /// <summary>
        /// 可填評價的憑證數量
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public int GetEvaluateCount(Guid orderGuid)
        {
            var cashTrustLogList = mp.CashTrustLogGetListByOrderGuid(orderGuid).Where(x => x.UsageVerifiedTime != null).ToList();
            int conut = 0;
            foreach (var cashTrustLog in cashTrustLogList)
            {
                //訂單列表上，核銷後超過7日不計入可填評價的憑證數量，數量>0才顯示「★填評價」；
                //訂單明細上，邏輯是已核銷就顯示「★填評價」(Visible=true)，超過7日就不能點(Enabled=false)
                if (!(cashTrustLog.UsageVerifiedTime < DateTime.Now.AddDays(-7)))
                {
                    if (OrderFacade.IsExpiredVerifiedDeal(cashTrustLog))
                    {
                        //系統強制核銷的檔次，不需要填寫評價
                        continue;
                    }
                    var evaluateCnt = mp.EvaluateGetMainID(cashTrustLog.TrustId); //此TrustId是否已評價
                    if (evaluateCnt == 0) //代表未填寫過
                    {
                        conut++;
                    }
                    else
                    {
                        int eValidItems = mp.EvaluateDetailGetByMainID(evaluateCnt, (int)EvaluateDataType.Final).Count();
                        if (eValidItems == 0) //代表僅填寫過暫存資料
                        {
                            conut++;
                        }
                    }
                }
            }
            return conut;
        }

    }
}

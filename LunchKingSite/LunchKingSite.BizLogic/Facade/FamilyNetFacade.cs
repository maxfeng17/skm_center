﻿using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Text;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core.Models;
using System.Web;
using NewtonsoftJson = Newtonsoft.Json;
using Microsoft.SqlServer.Types;
using System.Net.Mail;
using System.Text.RegularExpressions;
using FamilyNetEvent = LunchKingSite.Core.Models.Entities.FamilyNetEvent;

namespace LunchKingSite.BizLogic.Facade
{
    public class FamilyNetFacade
    {
        private static IMemberProvider mp;
        private static ISysConfProvider cp;
        private static IPponProvider pp;
        private static ISellerProvider sp;
        private static IHumanProvider hp;
        private static ISysConfProvider config;
        private static IOrderProvider op;
        private static IItemProvider it;
        private static ISystemProvider _sysp;        

        static FamilyNetFacade()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            cp = ProviderFactory.Instance().GetConfig();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            config = ProviderFactory.Instance().GetConfig();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            it = ProviderFactory.Instance().GetProvider<IItemProvider>();
            _sysp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        }
        

        public static PponDeal ConvertPponDeal(FamilyNetEvent deal)
        {
            PponDeal ret;
            ret = new PponDeal(true);
            //天貓檔次新增標示
            
            ret.DealContent.Name = deal.ItemName;
            ret.Deal.BusinessHourOnline = true;
            ret.ItemDetail.ItemPrice = deal.ItemPrice;
            ret.ItemDetail.ItemOrigPrice = deal.ItemOrigPrice;            
            ret.ItemDetail.ItemName = deal.ItemName; ;
            
            ret.Deal.BusinessHourOrderTimeS = deal.BusinessHourOrderTimeS;
            ret.Deal.BusinessHourOrderTimeE = deal.BusinessHourOrderTimeE;
            ret.Deal.OrderTotalLimit = deal.MaxItemCount;
            
            ret.DealContent.Introduction = ret.DealContent.Restrictions = "";
            ret.DealContent.CouponUsage = deal.ItemName;            
            ret.DealContent.AppTitle = deal.ItemName;
            ret.DealContent.Title = deal.ItemName;
            ret.DealContent.Remark = 0 + "||" + string.Empty + "||" + HttpUtility.HtmlDecode(deal.Remark);
            ret.Deal.BusinessHourDeliverTimeS = deal.BusinessHourDeliverTimeS;
            ret.Deal.BusinessHourDeliverTimeE = deal.BusinessHourDeliverTimeE;            
            //ret.ItemDetail.ItemDefaultDailyAmount = ret.ItemDetail.MaxItemCount = deal.MaxItemCount;
                       
            return ret;
        }

        public static string OnSaveBusinessHour(List<FamilyNetEvent> deals, out Dictionary<int, Guid> bids)
        {
            bids = new Dictionary<int, Guid>();
            string resultMsg = string.Empty;
            string username = config.FamilyNetDealSales;

            foreach (FamilyNetEvent deal in deals)
            {
                PponDeal data = ConvertPponDeal(deal);
                
                //bool orderStartDateChange = true;//好康開檔日是否有異動。
                bool success;
                bool isNew = false;
                var insStores = new PponStoreCollection();
                var delStores = new PponStoreCollection();
                DateTime lastTimes = DateTime.Now;
                StringBuilder processBuilder = new StringBuilder();
                processBuilder.AppendLine("OnSaveDeal(Begin) = " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.ffff") + "<br/>");

                // add mode

                #region add mode
                processBuilder.AppendLine("add mode = " + DateTimeTicks(lastTimes, out lastTimes));
                isNew = true;

                var sellerGuid = config.FamilyNetSellerGuid;
                PponDeal entity;
                entity = data;
                // now let's connect the dots
                entity.Store = sp.SellerGet(sellerGuid);
                entity.Deal.BusinessHourTypeId = (int)BusinessHourType.Ppon;
                entity.Deal.SellerGuid = sellerGuid;
                entity.Deal.Guid = Helper.GetNewGuid(entity);
                entity.DealContent.BusinessHourGuid = entity.Deal.Guid;

                entity.ItemDetail.BusinessHourGuid = entity.Deal.Guid;
                entity.ItemDetail.MenuGuid = Guid.Empty;
                entity.ItemDetail.Guid = Helper.GetNewGuid(entity);
                entity.Deal.CreateTime = entity.ItemDetail.CreateTime = DateTime.Now;
                entity.Deal.CreateId = entity.ItemDetail.CreateId = username;
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(true, entity.Deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(true, entity.Deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown);
                PromotionFacade.DiscountLimitSet(entity.Deal.Guid, username, DiscountLimitType.Enabled);

                #region PponStore
                processBuilder.AppendLine("PponStore = " + DateTimeTicks(lastTimes, out lastTimes));
                List<Guid> stores = new List<Guid>();
                stores.Add(sellerGuid);

                var storeSort = 1;
                foreach (var store in stores)
                {
                    var pponStore = new PponStore();
                    pponStore.BusinessHourGuid = entity.Deal.Guid;
                    pponStore.StoreGuid = store;
                    pponStore.ResourceGuid = Guid.NewGuid();
                    pponStore.CreateId = username;
                    pponStore.CreateTime = DateTime.Now;
                    pponStore.SortOrder = storeSort;
                    pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Location));
                    pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Verify));
                    pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.Accouting));
                    pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.ViewBalanceSheet));
                    pponStore.VbsRight = Convert.ToInt32(Helper.SetFlag(true, pponStore.VbsRight, VbsRightFlag.VerifyShop));
                    storeSort += 1;
                    insStores.Add(pponStore);
                }

                #endregion PponStore

                #endregion add mode

                //if (View.FamiDeal && View.ExchangePrice == 0)
                //{
                //    View.ShowMessage("此檔次為全家檔次，請填寫全家兌換價。");
                //    return;
                //}

                if (PponFacade.CheckPponIntroductionValid(data) == false)
                {
                    resultMsg = "「權益說明」「更多權益」有無法解析的Html語法，請重新填寫或洽IT處理。";
                    return resultMsg;
                }

                processBuilder.AppendLine("pp.ViewPponDealGetByBusinessHourGuid = " + DateTimeTicks(lastTimes, out lastTimes));

                #region 好康哪裡找
                processBuilder.AppendLine("好康哪裡找 = " + DateTimeTicks(lastTimes, out lastTimes));
                //如果有輸入分店資料，以分店資料產生好康哪裡找的紀錄
                var availables = new List<AvailableInformation>();
                foreach (var st in insStores.OrderBy(x => x.SortOrder))
                {
                    var store = sp.SellerGet(st.StoreGuid);
                    var viewstorecategoryCol = sp.ViewStoreCategoryCollectionGetByStore(st.StoreGuid);
                    var mrtInfo = string.Empty;
                    if (viewstorecategoryCol.Any())
                    {
                        mrtInfo = string.Join(",", viewstorecategoryCol.Select(x => x.Name).ToArray());
                    }

                    SqlGeography geo = LocationFacade.GetGeographyByCoordinate(store.Coordinate);
                    var tmp = new AvailableInformation()
                    {
                        N = store.SellerName,
                        P = store.StoreTel,
                        A = string.Empty,
                        Longitude = geo != null ? geo.Long.ToString() : string.Empty,
                        Latitude = geo != null ? geo.Lat.ToString() : string.Empty,
                        OT = store.OpenTime,
                        UT = st.UseTime,
                        U = store.WebUrl,
                        R = store.StoreRemark,
                        CD = store.CloseDate,
                        MR = string.Empty,
                        CA = store.Car,
                        BU = store.Bus,
                        OV = store.OtherVehicles,
                        FB = store.FacebookUrl,
                        PL = string.Empty,
                        BL = store.BlogUrl,
                        OL = store.OtherUrl
                    };
                    availables.Add(tmp);
                }

                if (availables.Count >= 0)
                {
                    entity.DealContent.Availability = new JsonSerializer().Serialize(availables);
                }

                #endregion 好康哪裡找

                #region DealProperty
                processBuilder.AppendLine("DealProperty = " + DateTimeTicks(lastTimes, out lastTimes));
                DealProperty dealProperty = pp.DealPropertyGet(entity.Deal.Guid);

                dealProperty.BusinessHourGuid = entity.Deal.Guid;
                dealProperty.CreateId = username;
                dealProperty.CreateTime = DateTime.Now;
                dealProperty.DenyInstallment = true;
                var empInfo = hp.EmployeeGet(Employee.Columns.UserId, mp.MemberGet(username).UniqueId);
                dealProperty.DealEmpName = empInfo.EmpName;

                //用業務名稱抓取對應Employee EmpNo 寫進dealProperty這個資料表 若對應不到 則無需填寫
                if (empInfo.IsLoaded && empInfo != null && (!isNew | (!empInfo.IsInvisible && empInfo.DepartureDate == null)))
                {
                    dealProperty.EmpNo = empInfo.EmpNo;
                    dealProperty.DevelopeSalesId = empInfo.UserId;
                }
                //else
                //{
                //    resultMsg = "請填入有效業務";
                //    return resultMsg;
                //}

                dealProperty.DeliveryType = (int)DeliveryType.ToShop;
                dealProperty.ShoppingCart = false;
                dealProperty.MultipleBranch = false;
                dealProperty.ComboPackCount = 1;
                dealProperty.PresentQuantity = deal.Free;
                dealProperty.SaleMultipleBase = deal.Buy;
                dealProperty.GroupCouponAppStyle = config.FamilyCategordId;
                dealProperty.GroupCouponDealType = (int)GroupCouponDealType.CostAssign;
                dealProperty.IsQuantityMultiplier = true;
                dealProperty.QuantityMultiplier = 1;
                dealProperty.IsAveragePrice = false;
                dealProperty.IsDailyRestriction = false;
                dealProperty.ActivityUrl = string.Empty;
                dealProperty.DealAccBusinessGroupId = (int)AccBusinessGroupNo.Fami;

                #region 儲存LabelIconList
                // + app限定tag
                #endregion 儲存LabelIconList

                //設定BarcodeType
                dealProperty.CouponCodeType = (int)CouponCodeType.FamiItemCode;
                //dealProperty.PinType = 0;

                dealProperty.ExchangePrice = 0;

                //設定台新分期
                dealProperty.Installment3months = false;
                dealProperty.Installment6months = false;
                dealProperty.Installment12months = false;

                dealProperty.ShipType = entity.Property.ShipType;
                dealProperty.ShippingdateType = entity.Property.ShippingdateType;
                dealProperty.Shippingdate = entity.Property.Shippingdate;
                dealProperty.ProductUseDateStartSet = entity.Property.ProductUseDateStartSet;
                dealProperty.ProductUseDateEndSet = entity.Property.ProductUseDateEndSet;
                dealProperty.IsDepositCoffee = true;

                #endregion DealProperty

                var dAccounting = new DealAccounting();
                dAccounting.BusinessHourGuid = entity.Deal.Guid;
                dAccounting.SalesCommission = 0.2;
                dAccounting.SalesBonus = 0;
                dAccounting.Status = (int)AccountsPayableFormula.Default;
                dAccounting.SalesId = empInfo.EmpName;
                dAccounting.Commission = 0;
                dAccounting.RemittanceType = (int)RemittanceType.Monthly;
                dAccounting.VendorBillingModel = 0;
                dAccounting.VendorReceiptType = (int)VendorReceiptType.Invoice;
                dAccounting.IsInputTaxRequired = true;
                dAccounting.DeptId = empInfo.DeptId;

                #region Save Category Deal data
                processBuilder.AppendLine("Save Category Deal data = " + DateTimeTicks(lastTimes, out lastTimes));
                CategoryDealCollection cds = new CategoryDealCollection();

                #region 新版的頻道、區域、分類之處理
                //todo

                //行政區分店自動檢查category
                List<ViewCategoryDependency> vcdList = sp.ViewCategoryDependencyGetByParentType((int)CategoryType.PponChannelArea).ToList();
                foreach (PponStore pponStore in insStores)
                {
                    Store store = sp.StoreGet(pponStore.StoreGuid);

                    if (store.IsLoaded && store.CityId.HasValue && store.TownshipId.HasValue)
                    {
                        var township = CityManager.TownShipGetById(store.TownshipId.Value);
                        var parentcity = CityManager.CityGetById(store.CityId.Value);
                        if (township != null && parentcity != null)
                        {
                            ViewCategoryDependency vcd = vcdList.Where(x => x.ParentName == parentcity.CityName && x.CategoryName == township.CityName).DefaultIfEmpty(null).First();
                            if (vcd != null && !cds.Any(x => x.Bid == entity.Deal.Guid && x.Cid == vcd.ParentId))
                            {
                                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.ParentId });
                            }
                            if (vcd != null && !cds.Any(x => x.Bid == entity.Deal.Guid && x.Cid == vcd.CategoryId))
                            {
                                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.CategoryId });
                            }
                        }

                    }
                }

                ///first create store_category data to store_guid bind category_code
                ///and uses stores.store_guid to find categoryid 
                ///and set the final node and find parent node to set in type 5 4
                ViewStoreCategoryCollection vscc = sp.ViewStoreCategoryCollectionGetByStoreList(stores);
                //type5
                if (vscc.Count > 0)
                {
                    List<int> nodeList = new List<int>();
                    foreach (ViewStoreCategory vsc in vscc)
                    {
                        if (!cds.Any(x => x.Cid == vsc.CategoryId.Value))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vsc.CategoryId.Value });
                        }
                        if (!cds.Any(x => x.Cid == vsc.ParentId))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vsc.ParentId });
                            if (!nodeList.Contains(vsc.ParentId))
                            {
                                nodeList.Add(vsc.ParentId);
                            }
                        }
                    }
                    ViewCategoryDependencyCollection vcdc = sp.ViewCategoryDependencyGetByCategoryIdList(nodeList);
                    foreach (ViewCategoryDependency vcd in vcdc)
                    {
                        if (!cds.Any(x => x.Cid == vcd.ParentId))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.ParentId });
                        }
                    }
                }

                //type6
                List<int> selectedDealCategories = new List<int>();
                selectedDealCategories.Add(config.FamilyCategordId);
                foreach (var dealCategoryId in selectedDealCategories)
                {
                    cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = dealCategoryId });
                }


                ////檔次的特殊類別(ICON區塊)
                //List<int> selectedSpecialCategories = View.SelectedSpecialCategories;
                //foreach (var specialCategoryId in selectedSpecialCategories)
                //{
                //    //特殊區塊有24與72小時的須設定，所以也要查詢對應的cityId
                //    PponCity city = PponCityGroup.GetPponCityByCategoryId(specialCategoryId);
                //    if (city != null)
                //    {
                //        selectedCity.Add(city.CityId);
                //    }
                //    if (specialCategoryId != FamilyLockCategoryId && specialCategoryId != FamilyBeaconUnLockCategoryId)
                //    {
                //        cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = specialCategoryId });
                //    }
                //}

                #endregion 新版的頻道、區域、分類之處理

                processBuilder.AppendLine("//Save Category Deal data = " + DateTimeTicks(lastTimes, out lastTimes));
                #endregion Save Category Deal data

                //將選取的城市紀錄於dealProperty
                dealProperty.CityList = new JsonSerializer().Serialize(new int[] { config.FamiportCityId });
                dealProperty.IsTravelDeal = false;

                //使用者選取的城市， 新版分類上線後，改由 新分類設定的頻道對應產生城市編號
                int[] cities = new int[] { config.FamiportCityId };

                //取得原本設定的Dts
                DealTimeSlotCollection origDtsCol = new DealTimeSlotCollection();
                entity.TimeSlotCollection.CopyTo(origDtsCol);

                processBuilder.AppendLine("處理DealTimeSlot = " + DateTimeTicks(lastTimes, out lastTimes));
                //處理DealTimeSlot
                DealTimeSlotInsert(entity, cities, false, processBuilder);
                processBuilder.AppendLine("//處理DealTimeSlot = " + DateTimeTicks(lastTimes, out lastTimes));

                // preset reward to 500 pts for ppon & pponitem only

                entity.Store.SellerStatus = SellerFacade.SetRewardType(entity.Store.SellerStatus, RewardType.FiveHundredPoints);
                entity.Deal.BusinessHourDeliveryCharge = 0;

                //// first we check to see if number of records match
                bool syncLoc = entity.TimeSlotCollection == null || entity.TimeSlotCollection.Count != origDtsCol.Count;
                if (!syncLoc)
                {
                    origDtsCol = origDtsCol.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                    entity.TimeSlotCollection.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                    for (int i = 0; !syncLoc && i < entity.TimeSlotCollection.Count; i++)
                    {
                        syncLoc |= (entity.TimeSlotCollection[i].CityId != origDtsCol[i].CityId ||
                                    entity.TimeSlotCollection[i].EffectiveStart != origDtsCol[i].EffectiveStart ||
                                    entity.TimeSlotCollection[i].EffectiveEnd != origDtsCol[i].EffectiveEnd ||
                                    entity.TimeSlotCollection[i].Sequence != origDtsCol[i].Sequence ||
                                    entity.TimeSlotCollection[i].Status != origDtsCol[i].Status ||
                                    entity.TimeSlotCollection[i].IsInTurn != origDtsCol[i].IsInTurn ||
                                    entity.TimeSlotCollection[i].IsLockSeq != origDtsCol[i].IsLockSeq);
                    }
                }
                //processBuilder.AppendLine("syncLoc = " + DateTimeTicks(LastTimes, out LastTimes));
                success = pp.PponDealSet(entity, syncLoc, origDtsCol);
                //success = pp.PponDealSet(entity, syncLoc);
                //processBuilder.AppendLine("PponDealSet = " + DateTimeTicks(LastTimes, out LastTimes));

                processBuilder.AppendLine("GroupOrderGetList = " + DateTimeTicks(lastTimes, out lastTimes));
                GroupOrderCollection gOrderList = op.GroupOrderGetList(GroupOrder.Columns.BusinessHourGuid + "=" + entity.Deal.Guid);
                GroupOrder gOrder;

                Guid grpOrderGuid = OrderFacade.MakeGroupOrder(config.ServiceEmail, config.ServiceName,
                    entity.Deal.Guid, DateTime.Now, entity.Deal.BusinessHourOrderTimeE);
                gOrder = op.GroupOrderGet(grpOrderGuid);

                ////全家檔次
                //gOrder.Status = (int)Helper.SetFlag(View.FamiDeal, gOrder.Status ?? 0, GroupOrderStatus.FamiDeal);
                //gOrder.Status = (int)Helper.SetFlag(View.IsKindDeal, gOrder.Status ?? 0, GroupOrderStatus.KindDeal);
                //gOrder.Status = (int)Helper.SetFlag(View.QuantityToShow.Equals(((int)GroupOrderStatus.LowQuantityToShow).ToString()),
                //    gOrder.Status ?? 0, GroupOrderStatus.LowQuantityToShow);
                //gOrder.Status = (int)Helper.SetFlag(View.QuantityToShow.Equals(((int)GroupOrderStatus.HighQuantityNotToShow).ToString()),
                //    gOrder.Status ?? 0, GroupOrderStatus.HighQuantityNotToShow);
                gOrder.Status = (int)Helper.SetFlag(true, gOrder.Status ?? 0, GroupOrderStatus.DisableSMS);
                gOrder.Status = (int)Helper.SetFlag(true, gOrder.Status ?? 0, GroupOrderStatus.PEZevent);
                gOrder.Status = (int)Helper.SetFlag(true, gOrder.Status ?? 0, GroupOrderStatus.FamiDeal);
                gOrder.Status = (int)Helper.SetFlag(true, gOrder.Status ?? 0, GroupOrderStatus.PEZeventCouponDownload);

                op.GroupOrderSet(gOrder);
                processBuilder.AppendLine("//GroupOrderGetList = " + DateTimeTicks(lastTimes, out lastTimes));

                #region 進貨價處理
                processBuilder.AppendLine("進貨價處理 = " + DateTimeTicks(lastTimes, out lastTimes));
                var costCol1 = new DealCostCollection();
                var newFreight = new DealCost();
                newFreight.BusinessHourGuid = entity.Deal.Guid;
                //newFreight.Id = viewFreight.Id;
                newFreight.Cost = deal.PurchasePrice;
                newFreight.Quantity = 99999;
                newFreight.CumulativeQuantity = 99999;
                newFreight.LowerCumulativeQuantity = 0;
                if (pp.DealCostGet(newFreight.Id) == null || pp.DealCostGet(newFreight.Id).BusinessHourGuid != entity.Deal.Guid)
                {
                    costCol1.Add(newFreight);
                }


                #endregion 進貨價處理             
                //processBuilder.AppendLine("//Mongo 好康異動時須進行的處理  = " + DateTimeTicks(LastTimes, out LastTimes));

                if (success)
                {
                    //儲存PponStore
                    pp.PponStoreDeleteList(delStores);
                    pp.PponStoreSetList(insStores);
                    processBuilder.AppendLine("儲存PponStore = " + DateTimeTicks(lastTimes, out lastTimes));

                    pp.DealAccountingSet(dAccounting);
                    pp.DealPropertySet(dealProperty);
                    processBuilder.AppendLine("儲存業務資料 = " + DateTimeTicks(lastTimes, out lastTimes));

                    //寫入CategoryDeal
                    PponFacade.SaveCategoryDeals(cds, entity.Deal.Guid);

                    processBuilder.AppendLine("寫入CategoryDeal = " + DateTimeTicks(lastTimes, out lastTimes));

                    //儲存進貨價
                    pp.DealCostSetList(costCol1);
                    processBuilder.AppendLine("儲存進貨價 = " + DateTimeTicks(lastTimes, out lastTimes));

                    ViewPponDeal ppon = pp.ViewPponDealGetByBusinessHourGuid(dealProperty.BusinessHourGuid);
                    string s = ViewPponDealToJson(ppon);
                    pp.ChangeLogInsert("ViewPponDeal", dealProperty.BusinessHourGuid.ToString(), s);

                    PponStoreCollection pponStores = pp.PponStoreGetListByBusinessHourGuid(dealProperty.BusinessHourGuid);
                    foreach (var pponStore in pponStores)
                    {
                        pp.ChangeLogInsert("PponStore", dealProperty.BusinessHourGuid.ToString(), PponStoreToJson(pponStore));
                    }

                    pp.ChangeLogInsert("DealCategories", dealProperty.BusinessHourGuid.ToString(), (new JsonSerializer()).Serialize(selectedDealCategories));

                    processBuilder.AppendLine("pp.ChangeLogInsert = " + DateTimeTicks(lastTimes, out lastTimes));

                    bids.Add(deal.Id, dealProperty.BusinessHourGuid);

                    resultMsg = "建檔成功";
                }
            }
            return resultMsg;
        }

        public static void DealApplyNotify(FamilyNetEvent deal)
        {
            //if (!string.IsNullOrEmpty(config.SkmDealBuildEmail))
            //{
            //    MailMessage msg = new MailMessage();
            //    foreach (string email in config.SkmDealBuildEmail.Split(';'))
            //    {
            //        msg.To.Add(email);
            //    }
            //    msg.Subject = string.Format("全家專區檔次申請通知【{0}】", deal.ItemName);
            //    msg.From = new MailAddress(config.AdminEmail);
            //    msg.IsBodyHtml = true;
            //    msg.Body = string.Format(@"您好，全家已於【{0}】申請【{1}】製檔， 
            //                    煩請前往<a href='{2}' target='skm_window'>17Life紅利pin碼系統</a>了解處理，謝謝。"
            //                , System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            //                , deal.ItemName
            //                , string.Format("{0}/FamiportPincode/FamilyNetEventList?bid={1}", config.SiteUrl, deal.MainBusinessHourGuid));
            //    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            //}
        }
        public static void UpdateFamilyDealsTime(List<FamilyNetEvent> deals,FamilyNetEvent deal)
        {
            foreach (var item in deals)
            {                
                UpdateBusinessHour(item.BusinessHourGuid,null,null, deal.BusinessHourOrderTimeE, deal.BusinessHourDeliverTimeE);

                DealTimeSlotCollection dts = pp.DealTimeSlotGetAllCol(item.BusinessHourGuid);

                var cityId = dts.Any() ? dts.Select(x => x.CityId).First() : config.FamiportCityId;
                var sequence = dts.Any() ? dts.Select(x => x.Sequence).First() : 999999;
                var daySpan = new TimeSpan(deal.BusinessHourOrderTimeE.Ticks - DateTime.Today.Ticks).Days;

                for (var i = 0; i < daySpan; i++)
                {
                    var j = i + 1;
                    if (!pp.DealTimeSlotGet(item.BusinessHourGuid, cityId, DateTime.Today.AddDays(i).AddHours(12)).IsLoaded)
                    {
                        DealTimeSlot dt = new DealTimeSlot
                        {
                            BusinessHourGuid = item.BusinessHourGuid,
                            CityId = cityId,
                            Sequence = sequence,
                            EffectiveStart = DateTime.Today.AddDays(i).Date.AddHours(12),
                            EffectiveEnd = i == daySpan - 1 ? deal.BusinessHourOrderTimeE : DateTime.Today.AddDays(j).AddHours(12),
                            Status = (int)DealTimeSlotStatus.NotShowInPponDefault
                        };
                        pp.DealTimeSlotSet(dt);
                    }
                }
            }
        }
        public static void AddFamilyDealsTime(List<FamilyNetEvent> deals, FamilyNetEvent deal)
        {
            foreach (var item in deals)
            {                
                UpdateBusinessHour(item.BusinessHourGuid, deal.BusinessHourOrderTimeS, deal.BusinessHourDeliverTimeS, deal.BusinessHourOrderTimeE, deal.BusinessHourDeliverTimeE);

                var cityId =  config.FamiportCityId;
                var sequence = 999999;
                var daySpan = new TimeSpan(deal.BusinessHourOrderTimeE.Ticks - deal.BusinessHourOrderTimeS.Ticks).Days;

                for (var i = 0; i <= daySpan; i++)
                {
                    var j = i + 1;
                    if (!pp.DealTimeSlotGet(item.BusinessHourGuid, cityId, deal.BusinessHourOrderTimeS.Date.AddDays(i).AddHours(12)).IsLoaded)
                    {
                        DealTimeSlot dt = new DealTimeSlot
                        {
                            BusinessHourGuid = item.BusinessHourGuid,
                            CityId = cityId,
                            Sequence = sequence,
                            EffectiveStart = i==0 ? deal.BusinessHourOrderTimeS : deal.BusinessHourOrderTimeS.Hour < DateTime.Today.Date.AddHours(12).Hour ? deal.BusinessHourOrderTimeS.AddDays(i-1).Date.AddHours(12) : deal.BusinessHourOrderTimeS.AddDays(i).Date.AddHours(12),
                            EffectiveEnd = i == daySpan ? deal.BusinessHourOrderTimeE : deal.BusinessHourOrderTimeS.Hour < DateTime.Today.Date.AddHours(12).Hour ? deal.BusinessHourOrderTimeS.AddDays(i).Date.AddHours(12) : deal.BusinessHourOrderTimeS.AddDays(j).Date.AddHours(12),
                            Status = (int)DealTimeSlotStatus.NotShowInPponDefault
                        };
                        pp.DealTimeSlotSet(dt);
                    }
                }
            }
        }
        public static void RemoveFamilyDealsTime(List<FamilyNetEvent> deals, DateTime updateDay,bool isAll=false)
        {
            foreach (var item in deals)
            {
                if (item.BusinessHourGuid != null)
                {
                    item.ModifyTime = DateTime.Now;
                    if (isAll)
                    {
                        pp.DealTimeSlotDeleteList(item.BusinessHourGuid);
                    }
                    else
                    {
                        pp.DealTimeSlotDeleteByToday(item.BusinessHourGuid);
                    }                   
                    UpdateBusinessHour(item.BusinessHourGuid,null,null, updateDay, updateDay);
                }
            }
        }
        
        public static void UpdateBusinessHour(Guid bid, DateTime? orderTimeS = null, DateTime? deliverTimeS = null, DateTime? orderTimeE = null,DateTime? deliverTimeE = null)
        {
            BusinessHour bh = pp.BusinessHourGet(bid);
            if(orderTimeS.HasValue)
            {
                bh.BusinessHourOrderTimeS = (DateTime)orderTimeS;
            }
            if (deliverTimeS.HasValue)
            {
                bh.BusinessHourDeliverTimeS = (DateTime)deliverTimeS;
            }
            if (orderTimeE.HasValue)
            {
                bh.BusinessHourOrderTimeE = (DateTime)orderTimeE;
            }
            if (deliverTimeE.HasValue)
            {
                bh.BusinessHourDeliverTimeE = (DateTime)deliverTimeE;
            }
            bh.ModifyTime = DateTime.Now;
            pp.BusinessHourSet(bh);
        }

        #region Private Method

        private static string DateTimeTicks(DateTime times, out DateTime LastTimes)
        {
            LastTimes = DateTime.Now;
            return (DateTime.Now - times).TotalSeconds.ToString() + "秒<br/>";
        }        
        private static string PponStoreToJson(PponStore pponStore)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("StoreGuid");
                writer.WriteValue(pponStore.StoreGuid);
                writer.WritePropertyName("TotalQuantity");
                writer.WriteValue(pponStore.TotalQuantity);
                writer.WritePropertyName("SortOrder");
                writer.WriteValue(pponStore.SortOrder);
                writer.WritePropertyName("ChangedExpireDate");
                writer.WriteValue(pponStore.ChangedExpireDate);

                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        private static string ViewPponDealToJson(ViewPponDeal ppon)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("Availability");
                writer.WriteValue(ppon.Availability);
                writer.WritePropertyName("BusinessHourAtmMaximum");
                writer.WriteValue(ppon.BusinessHourAtmMaximum);
                writer.WritePropertyName("BusinessHourDeliverTimeE");
                writer.WriteValue(ppon.BusinessHourDeliverTimeE == null ? "n/a" : ppon.BusinessHourDeliverTimeE.Value.ToString("o"));
                writer.WritePropertyName("BusinessHourDeliverTimeS");
                writer.WriteValue(ppon.BusinessHourDeliverTimeS == null ? "n/a" : ppon.BusinessHourDeliverTimeS.Value.ToString("o"));
                writer.WritePropertyName("BusinessHourDeliveryCharge");
                writer.WriteValue(ppon.BusinessHourDeliveryCharge);
                writer.WritePropertyName("BusinessHourGuid");
                writer.WriteValue(ppon.BusinessHourGuid.ToString());
                writer.WritePropertyName("BusinessHourOrderMinimum");
                writer.WriteValue(ppon.BusinessHourOrderMinimum);
                writer.WritePropertyName("BusinessHourOrderTimeE");
                writer.WriteValue(ppon.BusinessHourOrderTimeE.ToString("o"));
                writer.WritePropertyName("BusinessHourOrderTimeS");
                writer.WriteValue(ppon.BusinessHourOrderTimeS.ToString("o"));
                writer.WritePropertyName("BusinessHourStatus");
                writer.WriteValue(ppon.BusinessHourStatus);
                writer.WritePropertyName("ChangedExpireDate");
                writer.WriteValue(ppon.ChangedExpireDate.HasValue ? ppon.ChangedExpireDate.Value.ToString("yyyy/MM/dd") : "");
                writer.WritePropertyName("ComboPackCount");
                writer.WriteValue(ppon.ComboPackCount);
                writer.WritePropertyName("CouponUsage");
                writer.WriteValue(ppon.CouponUsage);
                writer.WritePropertyName("CreateTime");
                writer.WriteValue(ppon.CreateTime);
                writer.WritePropertyName("DeliveryType");
                writer.WriteValue(ppon.DeliveryType);
                writer.WritePropertyName("Department");
                writer.WriteValue(ppon.Department);
                writer.WritePropertyName("Description");
                string description = ViewPponDealManager.DefaultManager.GetDealDescription(ppon);
                writer.WriteValue(description);
                writer.WritePropertyName("EventName");
                writer.WriteValue(ppon.EventName);
                writer.WritePropertyName("EventTitle");
                writer.WriteValue(ppon.EventTitle);
                writer.WritePropertyName("GroupOrderGuid");
                writer.WriteValue(ppon.GroupOrderGuid.ToString());
                writer.WritePropertyName("GroupOrderStatus");
                writer.WriteValue(ppon.GroupOrderStatus);
                writer.WritePropertyName("Introduction");
                writer.WriteValue(ppon.Introduction);
                writer.WritePropertyName("ItemDefaultDailyAmount");
                writer.WriteValue(ppon.ItemDefaultDailyAmount);
                writer.WritePropertyName("ItemGuid");
                writer.WriteValue(ppon.ItemGuid.ToString());
                writer.WritePropertyName("ItemName");
                writer.WriteValue(ppon.ItemName);
                writer.WritePropertyName("ItemOrigPrice");
                writer.WriteValue(ppon.ItemOrigPrice);
                writer.WritePropertyName("ItemPrice");
                writer.WriteValue(ppon.ItemPrice);
                writer.WritePropertyName("SlottingFeeQuantity");
                writer.WriteValue(0);
                writer.WritePropertyName("PurchasePrice");
                writer.WriteValue(0);
                writer.WritePropertyName("MaxItemCount");
                writer.WriteValue(ppon.MaxItemCount);
                writer.WritePropertyName("OrderGuid");
                writer.WriteValue(ppon.OrderGuid.ToString());
                writer.WritePropertyName("OrderTotalLimit");
                writer.WriteValue(ppon.OrderTotalLimit);
                writer.WritePropertyName("OrderedQuantity");
                writer.WriteValue(ppon.OrderedQuantity);
                writer.WritePropertyName("OrderedTotal");
                writer.WriteValue(ppon.OrderedTotal);
                writer.WritePropertyName("Reasons");
                writer.WriteValue(ppon.Reasons);
                writer.WritePropertyName("ReferenceText");
                writer.WriteValue(ppon.ReferenceText);
                writer.WritePropertyName("Remark");
                writer.WriteValue(ppon.Remark);
                writer.WritePropertyName("SellerAddress");
                writer.WriteValue(ppon.SellerAddress);
                writer.WritePropertyName("SellerCityId");
                writer.WriteValue(ppon.SellerCityId);
                writer.WritePropertyName("SellerGuid");
                writer.WriteValue(ppon.SellerGuid.ToString());
                writer.WritePropertyName("SellerId");
                writer.WriteValue(ppon.SellerId);
                writer.WritePropertyName("SellerName");
                writer.WriteValue(ppon.SellerName);
                writer.WritePropertyName("SellerTel");
                writer.WriteValue(ppon.SellerTel);
                writer.WritePropertyName("ShoppingCart");
                writer.WriteValue(ppon.ShoppingCart);
                writer.WritePropertyName("Slug");
                writer.WriteValue(ppon.Slug);
                writer.WritePropertyName("SubjectName");
                writer.WriteValue(ppon.SubjectName);
                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        /// <summary>
        /// DealTimeSlot重構的方法
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="selectedCity"></param>
        /// <param name="orderStartDateChange"></param>
        private static void DealTimeSlotInsert(PponDeal entity, int[] cities, bool orderStartDateChange, StringBuilder processBuilder)
        {
            //**********寫入DealTimeSlot的邏輯 Begin**********
            #region 產生DealTimeSlot排程資料, it has a bug on removing items

            DateTime dateStartDay = entity.Deal.BusinessHourOrderTimeS;
            DateTime dateEndDay = entity.Deal.BusinessHourOrderTimeE;
            DealTimeSlotCollection dealTimeSlotColForInert = new DealTimeSlotCollection();

            //檢查目前是否已有排程資料，若沒有依據輸入的啟始與截止日期自動產生
            if (entity.TimeSlotCollection == null || entity.TimeSlotCollection.Count == 0)
            {
                #region 依據輸入的啟始與截止日期自動產生
                //依據On檔的時間與地區產生DealTimeSlot的資料(檔期排程資料)
                //依序新增勾選區域的排程記錄
                foreach (int cityId in cities)
                {
                    AddDealTimeSlotLoop(entity, dealTimeSlotColForInert, dateStartDay, dateEndDay, cityId);
                }

                if (dealTimeSlotColForInert.Count > 0)
                {
                    entity.TimeSlotCollection = dealTimeSlotColForInert;
                }
                #endregion 依據輸入的啟始與截止日期自動產生
            }
            else //若已有排程資料則需判斷是否需要修改
            {
                #region 若已有排程資料則需判斷是否需要修改

                //todo:改寫這塊邏輯，應該是不需要檢查，只要取得舊有的seq。反正最後都是全部砍掉重練。

                #region 逐筆檢查舊資料（邏輯需要再檢查）
                //CheckTimeSlotLoop(entity, dealTimeSlotColForInert, cities, dateStartDay, dateEndDay);
                #endregion 逐筆檢查舊資料

                //entity.TimeSlotCollection.AddRange(dealTimeSlotColForInert);

                //填入新增的資料

                #region 將不足的資料補上
                //將不足的資料補上
                //一開始排程時間設為DEAL起始日
                //依序新增勾選區域的排程記錄
                foreach (int cityId in cities)
                {
                    //只要排程時間小於DEAL結束日就需增加一筆排程記錄
                    AddDealTimeSlotLoop(entity, dealTimeSlotColForInert, dateStartDay, dateEndDay, cityId, true);
                }
                #endregion 將不足的資料補上

                entity.TimeSlotCollection = dealTimeSlotColForInert;


                #endregion
            }

            if ((entity.TimeSlotCollection != null) && (orderStartDateChange))
            {
                // 預設前24小時輪播  add by Max 2011/4/21
                foreach (DealTimeSlot dts in entity.TimeSlotCollection.Where(x => DateTime.Compare(entity.Deal.BusinessHourOrderTimeS.AddDays(1), x.EffectiveStart) > 0))
                {
                    dts.IsInTurn = true;
                }
            }
            //代金卷設定不顯示
            if ((entity.Deal.BusinessHourStatus & (int)BusinessHourStatus.GroupCoupon) > 0)
            {
                foreach (var item in entity.TimeSlotCollection)
                {
                    item.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                }
                // _pponProv.DealTimeSlotUpdateStatus(entity.Deal.Guid, DealTimeSlotStatus.NotShowInPponDefault);
            }


            //=====================================================

            #endregion 產生DealTimeSlot排程資料, it has a bug on removing items
            //**********寫入DealTimeSlot的邏輯 End**********
        }

        private static Tuple<DateTime, DealTimeSlot> AdjustFirstEffectiveStartDay(DateTime effectiveStartDay, Guid dealGuid, int cityId)
        {
            DateTime TempEndDay = DateTime.MinValue;
            DealTimeSlot insDeal = null;

            if (effectiveStartDay.ToString("HH:mm") != "12:00")
            {
                if (effectiveStartDay.Hour < 12) //未過中午12點
                {
                    TempEndDay = effectiveStartDay.Noon();

                }
                else //已過中午12點
                {
                    TempEndDay = effectiveStartDay.Noon().AddDays(1);
                }
            }
            //int newSeq = PponFacade.GetDealTimeSlotNonLockSeq(cityId, effectiveStartDay, vpdts);
            insDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                         999999, //★
                                          effectiveStartDay, TempEndDay,
                                          DealTimeSlotStatus.Default);

            return Tuple.Create(TempEndDay, insDeal);
        }

        /// <summary>
        /// 迴圈新增DealTimeSlot
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dealTimeSlotColForInert"></param>
        /// <param name="dateStartDay"></param>
        /// <param name="dateEndDate"></param>
        /// <param name="cityId"></param>
        /// <param name="calNewSeq"></param>
        private static void AddDealTimeSlotLoop(PponDeal entity, DealTimeSlotCollection dealTimeSlotColForInert, DateTime dateStartDay, DateTime dateEndDate, int cityId, bool calNewSeq = false)
        {
            Guid dealGuid = entity.Deal.Guid;
            //檢查第一筆
            bool isFirst = true;
            DateTime LastTimes = DateTime.Now;

            //EffectiveStart是Key值
            for (var date = dateStartDay; date < dateEndDate; date = date.AddDays(1))
            {
                //檢查資料是否已經存在
                bool anyExist = entity.TimeSlotCollection == null ? false : entity.TimeSlotCollection.Any(t => t.EffectiveStart == date && t.CityId == cityId);
                //取得該筆舊資料
                DealTimeSlot dts = !anyExist ? null : entity.TimeSlotCollection.Where(t => t.EffectiveStart == date && t.CityId == cityId).FirstOrDefault();

                DealTimeSlot insDeal = null;

                #region 第一筆

                if (isFirst) //針對第一筆的時間做調整
                {
                    //一開始排程時間設為DEAL起始日
                    Tuple<DateTime, DealTimeSlot> tuple = AdjustFirstEffectiveStartDay(date, dealGuid, cityId);
                    if (tuple.Item1 != DateTime.MinValue) //調整DEAL起始時間
                    {
                        if (!anyExist)
                        {
                            dealTimeSlotColForInert.Add(tuple.Item2);
                        }
                        else
                        {
                            //檔次已存在，維持舊的排序
                            dealTimeSlotColForInert.Add(GetNewDealTimeSlot(dts));
                        }

                        //用調整後的時間做新的起始點
                        date = tuple.Item1;

                        //檢查調整後的時間資料是否已經存在
                        anyExist = entity.TimeSlotCollection == null ? false : entity.TimeSlotCollection.Any(t => t.EffectiveStart == date && t.CityId == cityId);//★這兩行是多餘的?
                        dts = !anyExist ? null : entity.TimeSlotCollection.Where(t => t.EffectiveStart == date && t.CityId == cityId).FirstOrDefault();//★這兩行是多餘的?
                    }

                    //一定要把isFirst關掉
                    isFirst = false;
                }

                #endregion

                #region 最後一筆

                if (date.AddDays(1) > dateEndDate)
                {
                    int lastSeq = 999999;

                    if (!anyExist)
                    {
                    }
                    else
                    {
                        //檔次已存在，維持舊的排序
                        lastSeq = dts.Sequence;
                    }
                    DealTimeSlot lastDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                                      lastSeq,
                                                      date, dateEndDate,
                                                      DealTimeSlotStatus.Default);

                    dealTimeSlotColForInert.Add(lastDeal);
                    continue;
                }

                #endregion

                #region  其他筆

                if (!anyExist)
                {
                    //檔次不存在
                    int newSeq = 999999;

                    //新增 此城市這個時間一筆排程記錄
                    insDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                                        newSeq,
                                                        date, date.AddDays(1),
                                                        DealTimeSlotStatus.Default);
                }
                else
                {
                    //檔次已存在，維持舊的排序
                    insDeal = GetNewDealTimeSlot(dts);
                }

                #endregion

                if (!dealTimeSlotColForInert.Any(t => t.EffectiveStart == date && t.CityId == cityId))
                {
                    dealTimeSlotColForInert.Add(insDeal);
                }


                //      在2013/12/10現改成當新的slot增加時，順序放在最後面時，就不用+1
                //_pponProv.DealTimeSlotUpdateSeqAddOne(cityId, effectiveStartDay);
                //////////////////////////////////////////////
            }
        }
        /// <summary>
        /// 傳回新建的DealTimeSlot物件，DataTable欄位的值與傳入的DealTimeSlot相同
        /// </summary>
        /// <param name="fromData"></param>
        /// <returns></returns>
        private static DealTimeSlot GetNewDealTimeSlot(DealTimeSlot fromData)
        {
            //新增 此城市這個時間一筆排程記錄
            DealTimeSlot newDeal = new DealTimeSlot();
            newDeal.BusinessHourGuid = fromData.BusinessHourGuid;
            newDeal.CityId = fromData.CityId;
            newDeal.Sequence = fromData.Sequence;
            newDeal.EffectiveStart = fromData.EffectiveStart;

            //fix 手動結檔再開檔，fromData.EffectiveEnd.Noon()同日問題
            if (newDeal.EffectiveStart.Date.Equals(fromData.EffectiveEnd.Date) && newDeal.EffectiveStart >= fromData.EffectiveEnd.Noon())
            {
                newDeal.EffectiveEnd = fromData.EffectiveEnd.AddDays(1).Noon();
            }
            else
            {
                newDeal.EffectiveEnd = fromData.EffectiveEnd.Noon();
            }
            //日期銜接為12點，所以直接設定12點

            newDeal.Status = fromData.Status;
            newDeal.IsInTurn = fromData.IsInTurn;
            newDeal.IsLockSeq = fromData.IsLockSeq; //★本次新增

            return newDeal;
        }

        private static DealTimeSlot GetNewDealTimeSlot(Guid bid, int cityId, int seq, DateTime startDate, DateTime endDate, DealTimeSlotStatus status, bool isInTurn = false)
        {
            //新增 此城市這個時間一筆排程記錄
            DealTimeSlot newDeal = new DealTimeSlot();
            newDeal.BusinessHourGuid = bid;
            newDeal.CityId = cityId;
            newDeal.Sequence = seq;
            newDeal.EffectiveStart = startDate;
            newDeal.EffectiveEnd = endDate;
            newDeal.Status = (int)status;
            newDeal.IsInTurn = isInTurn;

            return newDeal;
        }
        #endregion
    }    
    
}

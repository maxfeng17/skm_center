﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;


namespace LunchKingSite.BizLogic.Facade
{
    public class ProposalFacade
    {
        private static ISysConfProvider config;
        private static ISellerProvider sp;
        private static IHumanProvider hp;
        private static IMemberProvider mp;
        private static IPponProvider pp;
        private static ISystemProvider sys;
        private static IItemProvider ip;
        private static IOrderProvider op;
        private static IWmsProvider wp;
        private static ILog logger = LogManager.GetLogger("ProposalFacade");

        static ProposalFacade()
        {
            config = ProviderFactory.Instance().GetConfig();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            sys = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        }

        public static bool IsVbsProposalNewVersion()
        {
            return config.IsVbsProposalNewVersion;
        }
        public static Proposal ProposalGet(int ProposalId)
        {
            return sp.ProposalGet(ProposalId);
        }
        public static Proposal ProposalGet(Guid bid)
        {
            return sp.ProposalGet(bid);
        }

        public static void ProposalSet(Proposal pro)
        {
            sp.ProposalSet(pro);
        }

        /// <summary>
        /// 提案單調整順序
        /// </summary>
        /// <param name="ProposalId"></param>
        /// <param name="UserName"></param>
        /// <param name="sortData"></param>
        /// <returns></returns>
        public static bool ProposalMultiDealSort(int ProposalId, string UserName, string sortData)
        {

            var data = new JsonSerializer().Deserialize<List<ProposalMultiDealSort>>(sortData);
            ProposalMultiDealCollection multiDeals = sp.ProposalMultiDealGetByPid(ProposalId);
            if (multiDeals.Count() > 0)
            {
                foreach (var d in data)
                {
                    var deal = multiDeals.Where(x => x.Id == d.ID).FirstOrDefault();
                    if (deal != null)
                    {
                        deal.Sort = int.Parse(d.Sort);
                    }
                }
                sp.ProposalMultiDealSetList(multiDeals);
            }
            return true;
        }



        /// <summary>
        /// 提案單優惠內容儲存&異動紀錄(單筆)
        /// </summary>
        public static ProposalMultiDealCollection ProposalMultiDealSave(ProposalMultiDealModel model, int ProposalId, string UserName, bool isDelete = false, bool isCopy = false, string itemName = "", string itemPrice = "0")
        {
            ProposalMultiDealCollection multiDeals = ProposalMultiDealGetByPid(ProposalId);
            ProposalMultiDealCollection oriMultiDeals = multiDeals.Clone();

            ProposalMultiDeal pmd = ProposalMultiDealGetById(model.Id);

            //新增&異動&複製
            if (!isDelete)
            {
                if (pmd.IsLoaded)
                {
                    //異動
                    pmd.ModifyId = UserName;
                    pmd.ModifyTime = DateTime.Now;
                }
                else
                {
                    //新增&複製
                    pmd.Pid = ProposalId;
                    pmd.CreateId = UserName;
                    pmd.CreateTime = DateTime.Now;
                }


                pmd.ItemName = model.ItemName;
                pmd.ItemNameTravel = model.ItemNameTravel;
                pmd.OrigPrice = model.OrigPrice;
                pmd.ItemPrice = model.ItemPrice;
                pmd.Cost = model.Cost;
                pmd.GroupCouponBuy = model.CPCBuy;
                pmd.GroupCouponPresent = model.CPCPresent;
                pmd.SlottingFeeQuantity = model.SlottingFeeQuantity;
                pmd.OrderMaxPersonal = Convert.ToInt32(model.OrderMaxPersonal);
                pmd.OrderTotalLimit = Convert.ToInt32(model.OrderTotalLimit);
                pmd.AtmMaximum = model.AtmMaximum;
                pmd.Freights = model.Freights;
                pmd.NoFreightLimit = Convert.ToInt32(model.NoFreightLimit);
                pmd.Options = model.Options;
                pmd.Sort = model.Sort;
                pmd.BoxQuantityLimit = model.BoxQuantityLimit;
                sp.ProposalMultiDealsSet(pmd);


            }
            //刪除
            if (isDelete && !isCopy)
            {
                sp.ProposalMultiDealDelete(pmd);
            }


            //重編順序
            multiDeals = ProposalMultiDealGetByPid(ProposalId);
            var sortList = multiDeals.OrderBy(x => x.Sort).ToList();

            int sort = 0;
            foreach (var sl in sortList)
            {
                sort++;
                multiDeals.Where(x => x.Id == sl.Id).FirstOrDefault().Sort = sort;
            }


            sp.ProposalMultiDealSetList(multiDeals);

            #region 檢查變更/新增的檔次
            if (multiDeals != oriMultiDeals)
            {
                if (!isDelete && !isCopy)
                {
                    foreach (ProposalMultiDeal deal in multiDeals)
                    {
                        if (oriMultiDeals != null)
                        {
                            ProposalMultiDeal oriDeal = oriMultiDeals.Where(x => x.Id == deal.Id).FirstOrDefault();
                            if (oriDeal != null)
                            {
                                //異動
                                CompareMultiDealModel(deal, oriDeal, ProposalId, UserName, ProposalSourceType.Original);
                            }
                            else
                            {
                                //新增
                                ProposalLog(ProposalId, "新增檔次 編號：" + deal.Id + "，優惠內容：" + deal.ItemName + "，售價：" + deal.ItemPrice, UserName);
                            }
                        }
                        else
                        {
                            //第一筆新增
                            ProposalLog(ProposalId, "第一筆新增檔次 編號：" + deal.Id + "，優惠內容：" + deal.ItemName + "，售價：" + deal.ItemPrice, UserName);
                        }
                    }
                }

                //複製
                if (isCopy)
                {
                    ProposalLog(ProposalId, "檔次複製 原優惠內容：" + itemName + "，原售價：" + itemPrice, UserName);
                }

                //刪除
                if (isDelete && !isCopy)
                {
                    ProposalLog(ProposalId, "檔次刪除 優惠內容：" + itemName + "，售價: " + itemPrice, UserName);
                }


            }
            #endregion


            return multiDeals;
        }


        /// <summary>
        /// 優惠內容新舊資料比較
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ori"></param>
        /// <param name="ProposalId"></param>
        /// <param name="UserName"></param>
        /// <param name="type"></param>
        public static void CompareMultiDealModel(ProposalMultiDeal pmd, ProposalMultiDeal ori, int ProposalId, string UserName,
            ProposalSourceType sourceType, ProposalLogType type = ProposalLogType.Initial)
        {
            List<string> changeList = new List<string>();
            foreach (var item in PponFacade.ColumnValueComparison(ori, pmd))
            {
                string itemValue = item.Value;
                if (item.Key == "多重選項" || item.Key == "賣家備註")
                {
                    if (sourceType == ProposalSourceType.House)
                    {
                        try
                        {
                            List<ProposalMultiDealsSpec> dealSpec = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(item.Value);
                            itemValue = string.Join(",", dealSpec.SelectMany(x => x.Items).Select(t => t.name));
                        }
                        catch
                        {
                            itemValue = item.Value;
                        }
                    }
                }

                changeList.Add(string.Format("優惠內容：{0}，售價：${1} 檔次 變更 {2} 為 {3}", ori.ItemName, ori.ItemPrice.ToString("N0"), item.Key, itemValue));
            }
            if (changeList.Count > 0)
            {
                if (config.IsVbsProposalNewVersion)
                {
                    ProposalLog(ProposalId, string.Join("|", changeList), UserName);
                }
                else
                {
                    ProposalLog(ProposalId, string.Join("|", changeList), UserName);
                    if (type == ProposalLogType.Seller)
                    {
                        ProposalLog(ProposalId, string.Join("|", changeList), UserName, ProposalLogType.Seller);
                    }
                }

            }
        }

        public static void CompareProductModel(BizLogic.Models.ProductInfoModel pmd, Guid ProductGuid, string userName)
        {
            ProductLog(ProductGuid, new JsonSerializer().Serialize(pmd), userName);
        }

        public static ProposalMultiDealCollection GetProposalMultiDeal(int pid, string strMultiDeals, ProposalMultiDealCollection multiDeals, string userName)
        {

            List<ProposalMultiDealModel> data = new JsonSerializer().Deserialize<List<ProposalMultiDealModel>>(strMultiDeals);

            //新增&異動&複製
            foreach (var m in data)
            {
                bool add = false;
                var pmd = multiDeals.Where(x => x.Id == m.Id && m.Id != 0).FirstOrDefault();


                if (pmd != null)
                {
                    //異動
                    pmd.ModifyId = userName;
                    pmd.ModifyTime = DateTime.Now;
                }
                else
                {
                    //新增&複製
                    add = true;
                    pmd = new ProposalMultiDeal();
                    pmd.Pid = pid;
                    pmd.CreateId = userName;
                    pmd.CreateTime = DateTime.Now;

                }

                pmd.BrandName = ProposalFacade.RemoveSpecialCharacter(m.BrandName.Trim());
                pmd.ItemName = ProposalFacade.RemoveSpecialCharacter(m.ItemName);
                pmd.ItemNameTravel = ProposalFacade.RemoveSpecialCharacter(m.ItemNameTravel);
                pmd.QuantityMultiplier = m.QuantityMultiplier;
                pmd.Unit = m.Unit;
                pmd.Content = string.Empty;
                pmd.OrigPrice = m.OrigPrice;
                pmd.ItemPrice = m.ItemPrice;
                pmd.Cost = m.Cost;
                pmd.GroupCouponBuy = m.CPCBuy;
                pmd.GroupCouponPresent = m.CPCPresent;
                pmd.SlottingFeeQuantity = m.SlottingFeeQuantity;

                int orderMaxPersonal = default(int);
                int.TryParse(m.OrderMaxPersonal, out orderMaxPersonal);
                pmd.OrderMaxPersonal = orderMaxPersonal;

                pmd.OrderTotalLimit = Convert.ToInt32(m.OrderTotalLimit);
                pmd.AtmMaximum = m.AtmMaximum;
                pmd.Freights = m.Freights;
                pmd.NoFreightLimit = Convert.ToInt32(m.NoFreightLimit);
                pmd.Options = m.Options;
                pmd.SellerOptions = m.SellerOptions;
                pmd.Sort = m.Sort;
                pmd.Gifts = m.Gifts;
                pmd.ComboPackCount = m.ComboPackCount;
                pmd.BoxQuantityLimit = m.BoxQuantityLimit;

                if (add)
                {
                    multiDeals.Add(pmd);
                }



                //刪除方案
                List<ProposalMultiDeal> delete = new List<ProposalMultiDeal>();
                ProposalMultiDealCollection tmp = multiDeals.Clone();
                foreach (var t in tmp)
                {
                    if (!t.Id.EqualsAny(data.Select(y => y.Id).ToArray()))
                    {
                        var p = multiDeals.Where(x => x.Id == t.Id).FirstOrDefault();
                        if (p != null)
                        {
                            if (p.Bid != null)
                            {
                                //已建檔，資料不刪除，只註記
                                p.Status = (int)ProposalMultiDealStatus.Delete;
                            }
                            else
                            {
                                //尚未建檔，直接刪除
                                multiDeals.Remove(t);
                                sp.ProposalMultiDealDelete(t);
                            }
                        }
                    }
                }
            }


            //重編順序//前端已處理


            return multiDeals;
        }

        /// <summary>
        /// 提案單優惠內容儲存&異動紀錄(全部)
        /// </summary>
        /// <param name="PrpopsalId"></param>
        /// <param name="newDeal"></param>
        /// <param name="oriDeal"></param>
        /// <param name="UserName"></param>
        public static void CompareMultiDeals(int PrpopsalId, ProposalMultiDealCollection newDeal, ProposalMultiDealCollection oriDeal, string userName, ProposalSourceType sourceType, ProposalLogType type)
        {
            try
            {
                //新增/修改
                foreach (ProposalMultiDeal m in newDeal)
                {
                    var multi = oriDeal.Where(x => x.Id == m.Id).FirstOrDefault();
                    if (multi == null)
                    {
                        ProposalFacade.ProposalLog(PrpopsalId, "新增檔次 編號:" + m.Id + "，售價: " + m.ItemPrice, userName, type);
                    }
                    else
                    {
                        ProposalFacade.CompareMultiDealModel(m, multi, PrpopsalId, userName, sourceType, type);
                    }
                }
                //刪除
                foreach (ProposalMultiDeal m in oriDeal)
                {
                    var multi = newDeal.Where(x => x.Id == m.Id).FirstOrDefault();
                    if (multi == null)
                    {
                        ProposalFacade.ProposalLog(PrpopsalId, "刪除檔次 編號:" + m.Id + "，售價: " + m.ItemPrice, userName, type);
                    }
                }
            }
            catch (Exception ex)
            {
                ProposalFacade.ProposalPerformanceLogSet("CompareMultiDeals", ex.Message, userName);
            }

        }

        public static void ProposalLog(int ProposalId, string changeLog, string UserName, ProposalLogType type = ProposalLogType.Initial)
        {
            ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, UserName);
            ProposalLog log = new ProposalLog();
            log.ProposalId = ProposalId;
            log.ChangeLog = changeLog;
            log.Dept = emp.DeptName;
            log.Type = (int)type;
            log.CreateId = UserName;
            log.CreateTime = DateTime.Now;
            sp.ProposalLogSet(log);
        }

        public static void ProductLog(Guid prouctGuid, string changeLog, string userName)
        {
            ProductLog log = new ProductLog();
            log.ProductGuid = prouctGuid;
            log.ChangeLog = changeLog;
            log.CreateId = userName;
            log.CreateTime = DateTime.Now;
            pp.ProductLogSet(log);
        }

        public static int GetProposalLogCount(int ProposalId, string changeLog, ProposalLogType type = ProposalLogType.Initial)
        {
            ProposalLogCollection logList = sp.ProposalLogGetList(ProposalId, ProposalLogType.Initial);
            int count = 0;

            foreach (var log in logList.Where(x => x.ChangeLog.EqualsAny(changeLog)))
            {
                count++;
            }
            return count;
        }

        public static CouponEventContent RestrictionsGet(Guid bid)
        {
            CouponEventContent cec = pp.CouponEventContentGetByBid(bid);
            return cec;
        }

        public static ProposalCouponEventContent RestrictionsGet(int pid)
        {
            ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pid);
            return pcec;
        }

        public static DateTime GetUseTimeEnd(DateTime useStart, int startUseValue, DateTime startUseDate, ProposalStartUseUnitType unit)
        {
            switch (unit)
            {
                case ProposalStartUseUnitType.Day:
                    return useStart.AddDays(startUseValue);
                case ProposalStartUseUnitType.Month:
                    return useStart.AddMonths(startUseValue);
                case ProposalStartUseUnitType.BeforeDay:
                    return startUseDate;
                default:
                    return DateTime.MinValue;
            }
        }

        /// <summary>
        /// 取得未指派的檔次
        /// </summary>
        /// <returns></returns>
        public static ProposalAssignLogCollection ProposalAssignFalg(int dealType, out List<Proposal> pros)
        {
            List<Proposal> opro = ProposalGetByOrderTimeS();
            if (dealType != 0)
            {
                opro = opro.Where(x => x.DealType == dealType).ToList();
            }
            pros = opro;
            List<int> pids = opro.Select(x => x.Id).ToList();
            ProposalAssignLogCollection palc = sp.ProposalAssignLogGetList(pids);
            return palc;
        }

        /// <summary>
        /// 取得指派紀錄
        /// </summary>
        /// <returns></returns>
        public static ProposalAssignLogCollection ProposalAssignLog(int Proposalid)
        {
            ProposalAssignLogCollection palc = sp.ProposalAssignLogGetList(Proposalid);
            return palc;
        }

        public static List<Proposal> ProposalGetByOrderTimeS()
        {
            return sp.ProposalGetByOrderTimeS().ToList();
        }

        public static bool ProposalPhotoSet(Proposal pro, string UserName)
        {
            bool flag = false;
            if (pro != null)
            {
                sp.ProposalSet(pro);
                flag = true;

                Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);
                Dictionary<ProposalPhotographer, string> SpecialAppointFlagText = new JsonSerializer().Deserialize<Dictionary<ProposalPhotographer, string>>(pro.SpecialAppointFlagText);
                string photoString = "";
                string photoAppointString = "";
                if (specialText != null && specialText.Count > 0)
                {
                    if (specialText != null)
                    {
                        photoString = specialText[(ProposalSpecialFlag)ProposalSpecialFlag.Photographers];
                    }
                    if (SpecialAppointFlagText != null)
                    {
                        photoAppointString = "，" + SpecialAppointFlagText[(ProposalPhotographer)ProposalPhotographer.PhotographerAppointCheck];
                    }
                }

                ProposalLog(pro.Id, "變更攝影資訊: " + photoString.Replace("|", "，") + photoAppointString.Replace("|", "，"), UserName);
            }
            return flag;
        }



        public static Dictionary<int, string> ProposalSubDealTypeGet(ProposalDealType type, bool falg)
        {
            Dictionary<int, string> rtn = new Dictionary<int, string>();
            if (falg)
            {
                rtn.Add(-1, "請選擇");
            }
            switch (type)
            {
                case ProposalDealType.LocalDeal:
                    rtn.Add((int)ProposalDealSubType.DealSet, Helper.GetLocalizedEnum(ProposalDealSubType.DealSet));
                    rtn.Add((int)ProposalDealSubType.Offsetting, Helper.GetLocalizedEnum(ProposalDealSubType.Offsetting));
                    rtn.Add((int)ProposalDealSubType.Propoduct, Helper.GetLocalizedEnum(ProposalDealSubType.Propoduct));
                    rtn.Add((int)ProposalDealSubType.EatToDie, Helper.GetLocalizedEnum(ProposalDealSubType.EatToDie));
                    rtn.Add((int)ProposalDealSubType.Tickets, Helper.GetLocalizedEnum(ProposalDealSubType.Tickets));
                    rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                    break;
                case ProposalDealType.PBeauty:
                    rtn.Add((int)ProposalDealSubType.SPA, Helper.GetLocalizedEnum(ProposalDealSubType.SPA));
                    rtn.Add((int)ProposalDealSubType.HairSalon, Helper.GetLocalizedEnum(ProposalDealSubType.HairSalon));
                    rtn.Add((int)ProposalDealSubType.NailSalon, Helper.GetLocalizedEnum(ProposalDealSubType.NailSalon));
                    rtn.Add((int)ProposalDealSubType.Massage, Helper.GetLocalizedEnum(ProposalDealSubType.Massage));
                    rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                    break;
                case ProposalDealType.Piinlife:
                    rtn.Add((int)ProposalDealSubType.DealSet, Helper.GetLocalizedEnum(ProposalDealSubType.DealSet));
                    rtn.Add((int)ProposalDealSubType.Offsetting, Helper.GetLocalizedEnum(ProposalDealSubType.Offsetting));
                    rtn.Add((int)ProposalDealSubType.Propoduct, Helper.GetLocalizedEnum(ProposalDealSubType.Propoduct));
                    rtn.Add((int)ProposalDealSubType.EatToDie, Helper.GetLocalizedEnum(ProposalDealSubType.EatToDie));
                    rtn.Add((int)ProposalDealSubType.Course, Helper.GetLocalizedEnum(ProposalDealSubType.Course));
                    rtn.Add((int)ProposalDealSubType.SPA, Helper.GetLocalizedEnum(ProposalDealSubType.SPA));
                    rtn.Add((int)ProposalDealSubType.HairSalon, Helper.GetLocalizedEnum(ProposalDealSubType.HairSalon));
                    rtn.Add((int)ProposalDealSubType.NailSalon, Helper.GetLocalizedEnum(ProposalDealSubType.NailSalon));
                    rtn.Add((int)ProposalDealSubType.Massage, Helper.GetLocalizedEnum(ProposalDealSubType.Massage));
                    rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                    break;
                case ProposalDealType.Travel:
                    rtn.Add((int)ProposalDealSubType.Hotel, Helper.GetLocalizedEnum(ProposalDealSubType.Hotel));
                    rtn.Add((int)ProposalDealSubType.BedAndBreakfast, Helper.GetLocalizedEnum(ProposalDealSubType.BedAndBreakfast));
                    rtn.Add((int)ProposalDealSubType.HotelRest, Helper.GetLocalizedEnum(ProposalDealSubType.HotelRest));
                    rtn.Add((int)ProposalDealSubType.Motel, Helper.GetLocalizedEnum(ProposalDealSubType.Motel));
                    rtn.Add((int)ProposalDealSubType.Travel, Helper.GetLocalizedEnum(ProposalDealSubType.Travel));
                    rtn.Add((int)ProposalDealSubType.Tickets, Helper.GetLocalizedEnum(ProposalDealSubType.Tickets));
                    rtn.Add((int)ProposalDealSubType.HotSpring, Helper.GetLocalizedEnum(ProposalDealSubType.HotSpring));
                    rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                    break;
                case ProposalDealType.Product:
                    rtn.Add((int)ProposalDealSubType.Food, Helper.GetLocalizedEnum(ProposalDealSubType.Food));
                    rtn.Add((int)ProposalDealSubType.Living, Helper.GetLocalizedEnum(ProposalDealSubType.Living));
                    rtn.Add((int)ProposalDealSubType.Furniture, Helper.GetLocalizedEnum(ProposalDealSubType.Furniture));
                    rtn.Add((int)ProposalDealSubType.Popular, Helper.GetLocalizedEnum(ProposalDealSubType.Popular));
                    rtn.Add((int)ProposalDealSubType.ConsumerElectronic, Helper.GetLocalizedEnum(ProposalDealSubType.ConsumerElectronic));
                    rtn.Add((int)ProposalDealSubType.HomeAppliances, Helper.GetLocalizedEnum(ProposalDealSubType.HomeAppliances));
                    rtn.Add((int)ProposalDealSubType.Beauty, Helper.GetLocalizedEnum(ProposalDealSubType.Beauty));
                    rtn.Add((int)ProposalDealSubType.Child, Helper.GetLocalizedEnum(ProposalDealSubType.Child));
                    break;
                case ProposalDealType.None:
                default:
                    break;
            }
            return rtn;
        }

        public static bool MailToPhotographer(string UserName, string PhotographerName, int ProposalId)
        {
            //攝影師通知信
            bool flag = false;

            string subject = string.Format("17Life 約拍通知 單號 {0}", ProposalId);
            string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, ProposalId);


            Proposal pro = ProposalGet(ProposalId);
            if (!Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointCheck))
            {
                pro.PhotographerAppointFlag = Convert.ToInt32(Helper.SetFlag(true, pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointCheck));
                sp.ProposalSet(pro);


                ProposalLog(ProposalId, "變更 攝影狀態 為 " + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppointCheck), UserName);
            }


            if (!string.IsNullOrEmpty(PhotographerName))
            {
                Member PhotographerMem = mp.MemberGet(PhotographerName);
                ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.OperationSalesId ?? 0);



                if (PhotographerMem != null && !string.IsNullOrEmpty(PhotographerMem.UserEmail))
                {
                    List<string> mailToUser = new List<string>();
                    List<string> mailCCUser = new List<string>();

                    //攝影師
                    mailToUser.Add(PhotographerMem.UserEmail);


                    //經營業務
                    if (opSalesEmp.IsLoaded)
                    {
                        mailCCUser.Add(opSalesEmp.Email);
                    }
                    // 開發業務
                    if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString())
                    {
                        mailCCUser.Add(deSalesEmp.Email);
                    }

                    //開發業務
                    string sales = ProposalFacade.PponAssistantEmailGet(opSalesEmp.DeptId);
                    if (!mailCCUser.Contains(sales))
                        mailCCUser.Add(sales);

                    //經營業務
                    sales = ProposalFacade.PponAssistantEmailGet(deSalesEmp.DeptId);
                    if (!mailCCUser.Contains(sales))
                        mailCCUser.Add(sales);

                    SendEmail(mailToUser, subject, GetPhotographerBodyHtml(pro, url), mailCCUser);

                    flag = true;
                }
            }
            return flag;
        }

        public static bool MailToPhotographerForCancel(string UserName, string PhotographerAppointer, int ProposalId)
        {
            //約拍取消通知信
            bool flag = false;

            string subject = string.Format("17Life 約拍取消通知 單號 {0}", ProposalId);
            string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, ProposalId);
            var empCollection = hp.ViewEmployeeCollectionGetAll().Where(x => !x.IsInvisible);

            Proposal pro = ProposalGet(ProposalId);

            if (!string.IsNullOrEmpty(UserName))
            {

                Member mem = mp.MemberGet(UserName);
                ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.OperationSalesId ?? 0);


                if (mem != null && !string.IsNullOrEmpty(mem.UserEmail))
                {
                    List<string> mailToUser = new List<string>();
                    List<string> mailCCUser = new List<string>();

                    //攝影師
                    if (PhotographerAppointer != "請選擇")
                    {
                        mailToUser.Add(PhotographerAppointer);
                    }

                    //登入者
                    mailCCUser.Add(UserName);

                    //經營業務
                    if (opSalesEmp.IsLoaded)
                    {
                        mailCCUser.Add(opSalesEmp.Email);
                    }
                    // 開發業務
                    if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString())
                    {
                        mailCCUser.Add(deSalesEmp.Email);
                    }

                    //創意組管
                    mailCCUser.Add(config.SupportingLeaderEmail);


                    //開發業務
                    string sales = ProposalFacade.PponAssistantEmailGet(deSalesEmp.DeptId);
                    if (!mailCCUser.Contains(sales))
                        mailCCUser.Add(sales);

                    //經營業務
                    sales = ProposalFacade.PponAssistantEmailGet(opSalesEmp.DeptId);
                    if (!mailCCUser.Contains(sales))
                        mailCCUser.Add(sales);



                    //業務主管
                    var emps = hp.ViewEmployeeCollectionGetAll().Where(x => x.DeptManager != null && x.DeptManager != "" && x.CrossDeptTeam != null);
                    if (emps != null)
                    {
                        if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString())
                        {
                            IEnumerable<ViewEmployee> deManager = emps.Where(x => x.CrossDeptTeam.Contains(deSalesEmp.DeptId));
                            foreach (ViewEmployee m in deManager)
                            {
                                if (RegExRules.CheckEmail(m.Email))
                                {
                                    mailToUser.Add(m.Email);//業務的主管email        
                                }
                            }
                        }

                        if (opSalesEmp.IsLoaded)
                        {
                            IEnumerable<ViewEmployee> opManager = emps.Where(x => x.CrossDeptTeam.Contains(opSalesEmp.DeptId));
                            foreach (ViewEmployee m in opManager)
                            {
                                if (RegExRules.CheckEmail(m.Email))
                                {
                                    mailToUser.Add(m.Email);//業務的主管email        
                                }
                            }
                        }

                    }

                    //商審
                    EmployeeCollection Qemp = HumanFacade.EmployeeGetListByDept(EmployeeDept.Q000);
                    if (Qemp != null)
                    {
                        foreach (Employee em in Qemp)
                        {
                            var _member = hp.ViewEmployeeGet("emp_id", em.EmpId);
                            if (RegExRules.CheckEmail(_member.Email))
                            {
                                mailCCUser.Add(_member.Email);//商審人員email
                            }
                        }
                    }


                    SendEmail(mailToUser, subject, GetPhotographerBodyHtml(pro, url), mailCCUser);
                    flag = true;
                }
            }
            return flag;
        }


        public static bool MailToVbsDealOn(int ProposalId)
        {
            try
            {
                Proposal pro = ProposalGet(ProposalId);
                if (pro.IsLoaded)
                {
                    if (pro.BusinessHourGuid != null)
                    {
                        BusinessHour bh = pp.BusinessHourGet(pro.BusinessHourGuid.Value);
                        if (bh.IsLoaded)
                        {
                            Guid bid = bh.Guid;
                            Item item = pp.ItemGetByBid(bid);
                            CouponEventContent cec = pp.CouponEventContentGetByBid(bid);
                            ComboDealCollection cdc = pp.GetComboDealByBid(bid, false);

                            string subject = string.Format("17Life【合作上檔通知】 {0} 上檔 - {1} ", bh.BusinessHourOrderTimeS.ToString("yyyy/MM/dd"), !string.IsNullOrEmpty(cec.AppTitle) ? cec.AppTitle : item.ItemName);
                            string header = string.Empty;
                            header += @"<style>
                                        table {
                                            border-collapse: collapse;
                                        }
                                        th, td {
                                            border: 1px solid black;
                                            padding: 10px;
                                            text-align: left;
                                        }
                                        </style>";


                            header += "您好：<br />";
                            header += "此次合作方案將於" + bh.BusinessHourOrderTimeS.ToString("yyyy/MM/dd HH:mm") + "上檔曝光，";
                            header += "17Life與貴公司簽定的合約，擬定出標題、價格、權益說明如下表：<br />";

                            string body = string.Empty;
                            body += "<table border='1'>";
                            body += string.Format("<tr><td>品牌</td><td><a href='{0}' target=_blank>{1}</a></td></tr>", config.SiteUrl + "/" + bid, !string.IsNullOrEmpty(cec.AppTitle) ? cec.AppTitle : item.ItemName);
                            body += string.Format("<tr><td>權益說明</td><td>{0}</td></tr>", cec.Restrictions);
                            body += string.Format("<tr><td>規格功能/主要成分</td><td>{0}</td></tr>", cec.ProductSpec);
                            foreach (ComboDeal cd in cdc)
                            {
                                if (cd.Title == "主檔")
                                {
                                    continue;
                                }
                                GroupOrder comboGo = op.GroupOrderGetByBid(cd.BusinessHourGuid);
                                if (Helper.IsFlagSet(comboGo.Status.GetValueOrDefault(0), GroupOrderStatus.Completed) || comboGo.Slug != null)
                                {
                                    continue;
                                }
                                Item comboItem = pp.ItemGetByBid(cd.BusinessHourGuid);
                                body += string.Format("<tr><td>方案(子)</td><td>{0}</td></tr>", comboItem.ItemName);
                            }
                            body += "</table>";

                            string footer = string.Format("提醒您！檔次上檔販售後請留意每日訂單相關資訊，並協助『準時出貨』。<br />商家系統網址：<a href='{0}' target=_blank>{0}/vbs</a>", config.SiteUrl);


                            List<string> mailUsers = new List<string>();
                            #region 賣家一般聯絡人
                            Seller seller = sp.SellerGet(pro.SellerGuid);
                            List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                            if (contacts != null)
                            {
                                //一般聯絡人
                                foreach (Seller.MultiContracts c in contacts)
                                {
                                    if (c.Type == ((int)ProposalContact.Normal).ToString() || c.Type == ((int)ProposalContact.ReturnPerson).ToString())
                                    {
                                        mailUsers.Add(c.ContactPersonEmail);
                                    }
                                }
                            }
                            #endregion 賣家一般聯絡人

                            #region 額外通知
                            string[] otherMails = config.VbsShortageMail.Split(",");
                            foreach (string otherMail in otherMails)
                            {
                                mailUsers.Add(otherMail);
                            }
                            #endregion 額外通知

                            SendEmail(mailUsers, subject, header + body + footer);
                        }
                    }

                }
            }
            catch
            {

            }
            return true;
        }

        public static bool CheckIsEmployee(string userName)
        {
            bool flag = false;
            if (!string.IsNullOrEmpty(userName))
            {
                Member mem = mp.MemberGet(userName);
                if (mem != null && !string.IsNullOrEmpty(mem.UserEmail))
                {
                    var emp = hp.EmployeeGet(Employee.Columns.UserId, mem.UniqueId);
                    if (emp != null)
                    {
                        flag = true;
                    }
                }
            }
            return flag;
        }



        public static bool ProposalEditorPassFlagGet(ProposalEditorFlag editorPassFlag, int value)
        {
            bool flag = false;
            if (Helper.IsFlagSet(editorPassFlag, (ProposalEditorFlag)value))
            {
                flag = true;
            }
            return flag;
        }


        /// <summary>
        /// 建立提案單
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="UserName"></param>
        /// <param name="ProposalDeliveryType"></param>
        /// <param name="IsTravelProposal"></param>
        /// <param name="IsPBeautyProposal"></param>
        /// <param name="IsSellerProposal"></param>
        /// <param name="createdType"></param>
        /// <returns></returns>
        public static int CreateProposal(string selectDevelopeSales, string selectOperationSalesGuid, Guid sellerGuid, string UserName, int ProposalDeliveryType, bool IsTravelProposal,
                      bool IsPBeautyProposal, bool IsSellerProposal, bool IsPiinLifeProposal,
                      ProposalCreatedType createdType)
        {

            bool isFirstDeal = false;
            Proposal pro = new Proposal();

            //先塞類型，下方會用到
            pro.DeliveryType = ProposalDeliveryType;
            if (IsTravelProposal)
            {
                //是否為旅遊
                pro.DealType = (int)ProposalDealType.Travel;
            }
            if (IsPBeautyProposal)
            {
                //是否為玩美
                pro.DealType = (int)ProposalDealType.PBeauty;
            }

            if (IsPiinLifeProposal)
            {
                //是否為品生活
                pro.DealType = (int)ProposalDealType.Piinlife;
            }

            if (IsSellerProposal)
            {
                //是否為商家提案單
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));
                pro.Status = (int)ProposalStatus.SellerCreate;

                if (ProposalDeliveryType == (int)DeliveryType.ToHouse)
                {
                    pro.DealType = (int)ProposalDealType.Product;
                }
            }
            else
            {
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Send));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
            }


            #region 有商家才要，提案諮詢不用
            if (sellerGuid != Guid.Empty)
            {
                Seller s = sp.SellerGet(sellerGuid);
                ViewPponDealCollection vpdc = pp.ViewPponDealGetBySellerGuid(s.Guid);
                if (vpdc.Count == 0)
                {
                    isFirstDeal = true;
                }


                if (s.VendorReceiptType != null)
                {
                    pro.VendorReceiptType = s.VendorReceiptType;
                    pro.Othermessage = s.Othermessage;
                }

                #region 若賣家規模 = SA or A or B or C or 大 ，則該欄位在新開提案單時會自動勾選，預代 2 天，可以變更；其他規模，則預設不勾
                if (s.SellerLevel == (int)SellerLevel.SA || s.SellerLevel == (int)SellerLevel.A || s.SellerLevel == (int)SellerLevel.B || s.SellerLevel == (int)SellerLevel.C || s.SellerLevel == (int)SellerLevel.Big)
                {
                    if (pro.DealType == (int)ProposalDealType.Product)
                    {
                        //宅配不預設
                    }
                    else
                    {
                        pro.IsEarlierPageCheck = true;
                        if (pro.DealType == (int)ProposalDealType.Travel)
                        {
                            //旅遊預設兩天
                            pro.EarlierPageCheckDay = 2;
                        }
                        else
                        {
                            //其他預設一天
                            pro.EarlierPageCheckDay = 1;
                        }
                    }
                }
                #endregion
            }
            #endregion


            pro.SellerGuid = sellerGuid;
            pro.DevelopeSalesId = MemberFacade.GetUniqueId(selectDevelopeSales);
            if (!string.IsNullOrEmpty(selectOperationSalesGuid))
            {
                pro.OperationSalesId = MemberFacade.GetUniqueId(selectOperationSalesGuid);
            }

            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(isFirstDeal, pro.SpecialFlag, ProposalSpecialFlag.FirstDeal));
            pro.PayType = (int)DealAccountingPayType.PayToCompany; // 預設為匯至賣家
            pro.Mohist = pro.Mohist ?? false; // 預設墨攻為關閉;
            //代銷通路
            pro.AgentChannels = string.Join(",", ProposalFacade.GetAgentChannels().Select(x => x.Key).ToList());

            pro.ProposalCreatedType = (short)createdType;
            pro.CreateId = UserName;
            pro.CreateTime = DateTime.Now;

            if (pro.DeliveryType == (int)DeliveryType.ToShop)
            {
                pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SpecialFlag, ProposalSpecialFlag.NotAllowedDiscount));
            }

            sp.ProposalSet(pro);


            #region 有商家才要，提案諮詢不用
            if (sellerGuid != Guid.Empty)
            {
                #region  宅配該賣家預設打勾(提案單建立後才能新增)

                if (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Travel)
                {
                    ProposalStoreCollection psc = new ProposalStoreCollection();
                    psc.Add(new ProposalStore()
                    {
                        ProposalId = pro.Id,
                        StoreGuid = pro.SellerGuid,
                        TotalQuantity = null,
                        VbsRight = (int)VbsRightFlag.Verify + (int)VbsRightFlag.Accouting + (int)VbsRightFlag.ViewBalanceSheet,
                        ResourceGuid = Guid.NewGuid(),
                        CreateId = UserName,
                        CreateTime = DateTime.Now
                    });

                    pp.ProposalStoreSetList(psc);
                }
                #endregion
            }
            #endregion

            #region Log
            if (IsSellerProposal)
            {
                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Apply);
                ProposalFacade.ProposalLog(pro.Id, "[" + title + "] 建立提案單", UserName, ProposalLogType.Seller);
            }
            else
            {
                if (sellerGuid != Guid.Empty)
                {
                    ProposalFacade.ProposalLog(pro.Id, "[系統] 建立提案單", UserName, ProposalLogType.Initial);
                }
                else
                {
                    ProposalFacade.ProposalLog(pro.Id, "[系統] 建立提案單(提案諮詢)", UserName, ProposalLogType.Initial);
                }
            }
            #endregion

            return pro.Id;
        }

        /// <summary>
        /// 建立新版提案單
        /// </summary>
        /// <param name="selectDevelopeSales"></param>
        /// <param name="selectOperationSalesGuid"></param>
        /// <param name="sellerGuid"></param>
        /// <param name="UserName"></param>
        /// <param name="IsSellerProposal"></param>
        /// <param name="createdType"></param>
        /// <returns></returns>
        public static int CreateProposalByHouse(string selectDevelopeSales, string selectOperationSalesGuid, Guid sellerGuid, string UserName, bool IsSellerProposal, bool isWms, ProposalCreatedType createdType)
        {

            bool isFirstDeal = false;
            Proposal pro = new Proposal();
            Seller s = new Seller();
            //先塞類型，下方會用到
            pro.DeliveryType = (int)DeliveryType.ToHouse;
            pro.DealType = (int)ProposalDealType.Product;

            if (IsSellerProposal)
            {
                //是否為商家提案單
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SellerEditor));
                pro.Status = (int)ProposalStatus.SellerCreate;
            }
            else
            {
                pro.ApplyFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ApplyFlag, ProposalApplyFlag.Apply));//直接提案申請
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Send));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
            }

            pro.ProposalSourceType = (int)ProposalSourceType.House;

            #region 有商家才要，提案諮詢不用
            if (sellerGuid != Guid.Empty)
            {
                s = sp.SellerGet(sellerGuid);
                ViewPponDealCollection vpdc = pp.ViewPponDealGetBySellerGuid(s.Guid);
                if (vpdc.Count == 0)
                {
                    isFirstDeal = true;
                }


                if (s.VendorReceiptType != null)
                {
                    pro.VendorReceiptType = s.VendorReceiptType;
                    pro.Othermessage = s.Othermessage;
                }
            }
            #endregion

            pro.BusinessHourGuid = Guid.NewGuid();  //先預產Bid，建檔時用這個Bid去建後臺檔次，主要是提案單編輯器及上傳圖片需先用到
            pro.SellerGuid = sellerGuid;
            pro.DevelopeSalesId = MemberFacade.GetUniqueId(selectDevelopeSales);
            if (!string.IsNullOrEmpty(selectOperationSalesGuid))
            {
                pro.OperationSalesId = MemberFacade.GetUniqueId(selectOperationSalesGuid);
            }

            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(isFirstDeal, pro.SpecialFlag, ProposalSpecialFlag.FirstDeal));
            pro.PayType = (int)DealAccountingPayType.PayToCompany; // 預設為匯至賣家
            if (config.IsRemittanceFortnightly)
            {
                pro.RemittanceType = (int)RemittanceType.Fortnightly;//預帶24HR配合的雙周結
            }
            else
            {
                if (s.RemittanceType != null)
                {
                    pro.RemittanceType = (int)s.RemittanceType;//預帶商家的出帳方式
                }
            }
            pro.ProposalCreatedType = (short)createdType;
            pro.CreateId = UserName;
            pro.CreateTime = DateTime.Now;
            pro.Mohist = false;
            //代銷通路
            pro.AgentChannels = string.Join(",", ProposalFacade.GetAgentChannels().Select(x => x.Key).ToList());


            if (pro.DeliveryType == (int)DeliveryType.ToShop)
            {
                pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SpecialFlag, ProposalSpecialFlag.NotAllowedDiscount));
            }
            pro.IsWms = isWms;
            sp.ProposalSet(pro);

            #region 自動塞分類
            if (isWms)
            {
                var wmsWmsDeal = sp.CategoryGetList((int)CategoryType.DealCategory).Where(x => x.Name == "24H到貨");
                var wmsWmsChannel = sp.CategoryGetList((int)CategoryType.PponChannel).Where(x => x.Name == "24H到貨");
                ProposalCategoryDealCollection rtn = new ProposalCategoryDealCollection();

                if (wmsWmsDeal.Any() && wmsWmsChannel.Any())
                {
                    rtn.Add(new ProposalCategoryDeal()
                    {
                        Pid = pro.Id,
                        Cid = 88
                    });

                    rtn.Add(new ProposalCategoryDeal()
                    {
                        Pid = pro.Id,
                        Cid = wmsWmsDeal.First().Id
                    });
                    rtn.Add(new ProposalCategoryDeal()
                    {
                        Pid = pro.Id,
                        Cid = wmsWmsChannel.First().Id
                    });
                }
                ProposalFacade.ProposalCategoryDealSave(pro, rtn, UserName);
            }



            #endregion

            #region 有商家才要，提案諮詢不用
            if (sellerGuid != Guid.Empty)
            {
                #region  宅配該賣家預設打勾(提案單建立後才能新增)

                if (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Travel)
                {
                    ProposalStoreCollection psc = new ProposalStoreCollection();
                    psc.Add(new ProposalStore()
                    {
                        ProposalId = pro.Id,
                        StoreGuid = pro.SellerGuid,
                        TotalQuantity = null,
                        VbsRight = (int)VbsRightFlag.Verify + (int)VbsRightFlag.Accouting + (int)VbsRightFlag.ViewBalanceSheet,
                        ResourceGuid = Guid.NewGuid(),
                        CreateId = UserName,
                        CreateTime = DateTime.Now
                    });

                    pp.ProposalStoreSetList(psc);
                }
                #endregion
            }
            #endregion

            #region Log
            if (IsSellerProposal)
            {
                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Apply);
                ProposalFacade.ProposalLog(pro.Id, "[" + title + "] 建立提案單", UserName, ProposalLogType.Initial);
            }
            else
            {
                if (sellerGuid != Guid.Empty)
                {
                    ProposalFacade.ProposalLog(pro.Id, "[系統] 建立提案單", UserName, ProposalLogType.Initial);
                }
                else
                {
                    ProposalFacade.ProposalLog(pro.Id, "[系統] 建立提案單(提案諮詢)", UserName, ProposalLogType.Initial);
                }
            }
            #endregion

            return pro.Id;
        }

        public static dynamic GetDealSubType(string id)
        {
            Dictionary<int, string> rtn = new Dictionary<int, string>();
            ProposalDealType type = ProposalDealType.TryParse(id, out type) ? type : ProposalDealType.None;
            if (type != ProposalDealType.None)
            {
                switch (type)
                {
                    case ProposalDealType.LocalDeal:
                        rtn.Add((int)ProposalDealSubType.DealSet, Helper.GetLocalizedEnum(ProposalDealSubType.DealSet));
                        rtn.Add((int)ProposalDealSubType.Offsetting, Helper.GetLocalizedEnum(ProposalDealSubType.Offsetting));
                        rtn.Add((int)ProposalDealSubType.Propoduct, Helper.GetLocalizedEnum(ProposalDealSubType.Propoduct));
                        rtn.Add((int)ProposalDealSubType.EatToDie, Helper.GetLocalizedEnum(ProposalDealSubType.EatToDie));
                        rtn.Add((int)ProposalDealSubType.Tickets, Helper.GetLocalizedEnum(ProposalDealSubType.Tickets));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.PBeauty:
                        rtn.Add((int)ProposalDealSubType.SPA, Helper.GetLocalizedEnum(ProposalDealSubType.SPA));
                        rtn.Add((int)ProposalDealSubType.HairSalon, Helper.GetLocalizedEnum(ProposalDealSubType.HairSalon));
                        rtn.Add((int)ProposalDealSubType.NailSalon, Helper.GetLocalizedEnum(ProposalDealSubType.NailSalon));
                        rtn.Add((int)ProposalDealSubType.Massage, Helper.GetLocalizedEnum(ProposalDealSubType.Massage));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.Piinlife:
                        rtn.Add((int)ProposalDealSubType.DealSet, Helper.GetLocalizedEnum(ProposalDealSubType.DealSet));
                        rtn.Add((int)ProposalDealSubType.Offsetting, Helper.GetLocalizedEnum(ProposalDealSubType.Offsetting));
                        rtn.Add((int)ProposalDealSubType.Propoduct, Helper.GetLocalizedEnum(ProposalDealSubType.Propoduct));
                        rtn.Add((int)ProposalDealSubType.EatToDie, Helper.GetLocalizedEnum(ProposalDealSubType.EatToDie));
                        rtn.Add((int)ProposalDealSubType.Course, Helper.GetLocalizedEnum(ProposalDealSubType.Course));
                        rtn.Add((int)ProposalDealSubType.SPA, Helper.GetLocalizedEnum(ProposalDealSubType.SPA));
                        rtn.Add((int)ProposalDealSubType.HairSalon, Helper.GetLocalizedEnum(ProposalDealSubType.HairSalon));
                        rtn.Add((int)ProposalDealSubType.NailSalon, Helper.GetLocalizedEnum(ProposalDealSubType.NailSalon));
                        rtn.Add((int)ProposalDealSubType.Massage, Helper.GetLocalizedEnum(ProposalDealSubType.Massage));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.Travel:
                        rtn.Add((int)ProposalDealSubType.Hotel, Helper.GetLocalizedEnum(ProposalDealSubType.Hotel));
                        rtn.Add((int)ProposalDealSubType.BedAndBreakfast, Helper.GetLocalizedEnum(ProposalDealSubType.BedAndBreakfast));
                        rtn.Add((int)ProposalDealSubType.HotelRest, Helper.GetLocalizedEnum(ProposalDealSubType.HotelRest));
                        rtn.Add((int)ProposalDealSubType.Motel, Helper.GetLocalizedEnum(ProposalDealSubType.Motel));
                        rtn.Add((int)ProposalDealSubType.Travel, Helper.GetLocalizedEnum(ProposalDealSubType.Travel));
                        rtn.Add((int)ProposalDealSubType.Tickets, Helper.GetLocalizedEnum(ProposalDealSubType.Tickets));
                        rtn.Add((int)ProposalDealSubType.HotSpring, Helper.GetLocalizedEnum(ProposalDealSubType.HotSpring));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.Product:
                        rtn.Add((int)ProposalDealSubType.Food, Helper.GetLocalizedEnum(ProposalDealSubType.Food));
                        rtn.Add((int)ProposalDealSubType.Living, Helper.GetLocalizedEnum(ProposalDealSubType.Living));
                        rtn.Add((int)ProposalDealSubType.Furniture, Helper.GetLocalizedEnum(ProposalDealSubType.Furniture));
                        rtn.Add((int)ProposalDealSubType.Popular, Helper.GetLocalizedEnum(ProposalDealSubType.Popular));
                        rtn.Add((int)ProposalDealSubType.ConsumerElectronic, Helper.GetLocalizedEnum(ProposalDealSubType.ConsumerElectronic));
                        rtn.Add((int)ProposalDealSubType.HomeAppliances, Helper.GetLocalizedEnum(ProposalDealSubType.HomeAppliances));
                        rtn.Add((int)ProposalDealSubType.Beauty, Helper.GetLocalizedEnum(ProposalDealSubType.Beauty));
                        rtn.Add((int)ProposalDealSubType.Child, Helper.GetLocalizedEnum(ProposalDealSubType.Child));
                        break;
                    case ProposalDealType.None:
                    default:
                        break;
                }
            }
            return rtn.ToArray();
        }

        public static bool DeleteContractFile(Guid guid)
        {
            ProposalContractFile pcf = sp.ProposalContractFileGetByGuid(guid);
            if (pcf != null)
            {
                pcf.Status = -1;
                sp.ProposalContractFileSet(pcf);
                return true;
            }
            return false;
        }

        public static ProposalContractFileCollection ProposalContractFileGetByType(int proposal_id, int Type)
        {
            return sp.ProposalContractFileGetByType(proposal_id, Type);
        }

        public static SellerContractFileCollection SellerContractFileGetByType(Guid seller_guid, int Type)
        {
            return sp.SellerContractFileGetByType(seller_guid, Type);
        }

        public static ReferralType ProposalReferralTypeGet(string userName)
        {
            var emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, userName);
            if (emp.IsLoaded)
            {
                var deptId = emp.DeptId;
                Department department = hp.DepartmentGet(deptId);
                if (department.ParentDeptId == EmployeeDept.S000.ToString())
                {
                    return ReferralType.Sale;
                }
                else if (department.ParentDeptId == EmployeeDept.ETC0.ToString())
                {
                    return ReferralType.Outer;
                }
                else
                {
                    return ReferralType.CompanyOthers;
                }
            }
            return ReferralType.None;
        }

        public static Dictionary<int, string> ProposalReferralPercentGet(ReferralType type)
        {
            Dictionary<int, string> rtn = new Dictionary<int, string>();
            rtn.Add(-1, "請選擇");

            switch (type)
            {
                case ReferralType.Sale:
                    //業務同仁
                    rtn.Add(5, Helper.GetLocalizedEnum(ReferralType.Sale) + " 5%");
                    rtn.Add(20, Helper.GetLocalizedEnum(ReferralType.Sale) + " 20%");
                    break;
                case ReferralType.CompanyOthers:
                    rtn.Add(10, Helper.GetLocalizedEnum(ReferralType.CompanyOthers) + " 10%");
                    rtn.Add(15, Helper.GetLocalizedEnum(ReferralType.CompanyOthers) + " 15%");
                    rtn.Add(30, Helper.GetLocalizedEnum(ReferralType.CompanyOthers) + " 30%");
                    rtn.Add(35, Helper.GetLocalizedEnum(ReferralType.CompanyOthers) + " 35%");
                    rtn.Add(40, Helper.GetLocalizedEnum(ReferralType.CompanyOthers) + " 40%");
                    break;
                case ReferralType.Outer:
                    rtn.Add(20, Helper.GetLocalizedEnum(ReferralType.Outer) + " 20%");
                    rtn.Add(30, Helper.GetLocalizedEnum(ReferralType.Outer) + " 30%");
                    break;
            }
            return rtn;
        }
        /// <summary>
        /// 寄通知信
        /// </summary>
        /// <param name="mailToUser">email收件者</param>
        /// <param name="subject">信件標題</param>
        /// <param name="content">信件內容</param>
        /// <param name="mailCCUser">email副本</param>
        public static void SendEmail(List<string> mailToUser, string subject, string content, List<string> mailCCUser = null)
        {
            //排除重複人員
            mailToUser = mailToUser.Distinct().ToList();

            try
            {
                foreach (string email in mailToUser)
                {
                    string[] emails = email.Split(";");
                    foreach (string mail in emails)
                    {
                        if (RegExRules.CheckEmail(mail))
                        {
                            MailMessage msg = new MailMessage();
                            msg.From = new MailAddress(config.SystemEmail);
                            if (mailCCUser != null)
                            {
                                foreach (string ccemail in mailCCUser)
                                {
                                    msg.CC.Add(ccemail);
                                }
                            }
                            msg.To.Add(mail);
                            msg.Subject = subject;
                            msg.Body = HttpUtility.HtmlDecode(content);
                            msg.IsBodyHtml = true;
                            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("subject={0}, content={1}, ex={2}", subject, content, ex.Message));
            }
        }

        /// <summary>
        /// 組合提案單信件內容
        /// </summary>
        /// <param name="pid">單號</param>
        /// <param name="sellerName">店家名稱</param>
        /// <param name="salesName">負責業務</param>
        /// <param name="url">連結</param>
        /// <param name="returned">退件原因</param>
        /// <returns></returns>
        public static string GetProposalBodyHtml(int pid, string sellerName, string salesName, string url, string returned = "")
        {
            string html = I18N.Message.ProposalSendEmailHtmlContent;
            string returnHtml = string.Empty;
            if (!string.IsNullOrWhiteSpace(returned))
            {
                returnHtml = string.Format("<li>退件原因：{0}</li>", returned.Replace(Environment.NewLine, "<br />"));
            }
            return string.Format(html, config.SiteUrl, pid, sellerName, salesName, url, returnHtml);
        }

        /// <summary>
        /// 組合商家提案信件內容
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="SaleName"></param>
        /// <param name="url"></param>
        /// <param name="Memo"></param>
        /// <returns></returns>
        public static string SendSellerProposalMailContent(Proposal pro, string SaleName, string url, string Memo)
        {
            Seller seller = sp.SellerGet(pro.SellerGuid);

            return string.Format(@"單號：{0}<br />
                                    店家名稱：{1}<br />
                                    負責業務：{2}<br />
                                    提案者：{3}<br />
                                    連結：{4}<p />
                                    {5}",
                                    pro.Id,
                                    seller.SellerName,
                                    SaleName,
                                    pro.CreateId,
                                    url + pro.Id,
                                    Memo);
        }

        /// <summary>
        /// 組合攝影師信件內容
        /// </summary>
        /// <param name="pro">提案單</param>
        /// <param name="url">連結</param>
        /// <returns></returns>
        private static string GetPhotographerBodyHtml(Proposal pro, string url)
        {
            string html = "";

            ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);
            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);
            Dictionary<ProposalPhotographer, string> SpecialAppointFlagText = new JsonSerializer().Deserialize<Dictionary<ProposalPhotographer, string>>(pro.SpecialAppointFlagText);

            bool PhotohrapherAppoint = Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppoint);
            bool PhotohrapherAppointCheck = Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointCheck);
            bool PhotohrapherAppointCancel = Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointCancel);
            string photoStatus = "";

            if (PhotohrapherAppointCancel)
            {
                photoStatus = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppointCancel);
            }
            else if (PhotohrapherAppointCheck)
            {
                photoStatus = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppointCheck);
            }
            else if (PhotohrapherAppoint)
            {
                photoStatus = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppoint);
            }

            string photoString = specialText[(ProposalSpecialFlag)ProposalSpecialFlag.Photographers];
            string photoAppointString = SpecialAppointFlagText[(ProposalPhotographer)ProposalPhotographer.PhotographerAppointCheck];

            //html += "<a href='" + url + "' target='_balnk'>" + url + "</a><p />";
            html += "店家名稱：" + sp.SellerGet(pro.SellerGuid).SellerName + "<br/>";
            html += "約定拍攝狀態：" + photoStatus + "<br/>";
            if (pro.DeliveryType == (int)DeliveryType.ToShop)
            {
                html += "拍攝地點：" + photoString.Split('|')[0] + "<br/>";
                if (photoString.Split('|').Length > 7)
                {
                    html += "現場聯絡人姓名：" + photoString.Split('|')[6] + (photoString.Split('|')[7] == "M" ? "先生" : "小姐") + "<br/>";
                }
                if (photoString.Split('|').Length > 2)
                {
                    html += "現場聯絡人手機：" + photoString.Split('|')[2] + "<br/>";
                }
                if (photoString.Split('|').Length > 1)
                {
                    html += "現場聯絡人電話：" + photoString.Split('|')[1] + "<br/>";
                }
                if (photoString.Split('|').Length > 4)
                {
                    html += "店家希望拍照時間：" + photoString.Split('|')[4] + "<br/>";
                }
                if (photoString.Split('|').Length > 5)
                {
                    html += "參考網址 / 資料：" + photoString.Split('|')[5] + "<br/>";
                }
                if (photoString.Split('|').Length > 3)
                {
                    html += "建議重點拍攝項目：" + photoString.Split('|')[3] + "<br/>";
                }


            }
            else
            {
                if (photoString.Split('|').Length > 0)
                {
                    html += "拍攝需求：" + photoString.Split('|')[0] + "<br/>";
                }
                if (photoString.Split('|').Length > 1)
                {
                    html += "參考資料：" + photoString.Split('|')[1] + "<br/>";
                }
            }

            html += "<hr />";
            if (photoAppointString.Split('|').Length > 0)
            {
                html += "約定拍攝日期：" + photoAppointString.Split('|')[0] + "<br/>";
            }
            if (photoAppointString.Split('|').Length > 2)
            {
                html += "約定拍攝時間：" + photoAppointString.Split('|')[1] + ":" + photoAppointString.Split('|')[2] + "<br/>";
            }
            if (photoAppointString.Split('|').Length > 4)
            {
                html += "備註：" + photoAppointString.Split('|')[4] + "<br/>";
            }

            html += "<table border='1' style='border-collapse:collapse;width:300px;' borderColor='black'>";
            html += "<tr><td>專案內容</td><td>多重選項</td></tr>";
            foreach (ProposalMultiDeal deal in multiDeals)
            {
                html += "<tr><td>" + deal.ItemName + "</td><td>" + deal.Options + "</td></tr>";
            }
            html += "</table>";


            return html;
        }

        public static void ProposalFileChangeMail(Proposal pro)
        {
            if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
            {
                ProposalLogCollection plc = sp.ProposalLogGetList(pro.Id, ProposalLogType.Initial);
                string text = Helper.GetLocalizedEnum(ProposalApproveFlag.QCcheck);
                ProposalLog log = plc.Where(x => x.ChangeLog == text).OrderByDescending(t => t.CreateTime).FirstOrDefault();
                DateTime cDate = DateTime.MinValue;
                if (log != null)
                {
                    cDate = log.CreateTime;
                }


                List<string> MailUsers = new List<string>();

                //業務
                ViewEmployee opSalesEmp = hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.OperationSalesId ?? 0);


                //全國宅配特助群組
                MailUsers.Add(config.SalesSpecialAssistantShippingEmail);


                string SaleName = "";
                string SaleMail = "";
                if (opSalesEmp.IsLoaded && opSalesEmp != null)
                {
                    SaleName = opSalesEmp.EmpName;
                    SaleMail = opSalesEmp.Email;
                    MailUsers.Add(SaleMail);

                    List<ProposalSellerFile> files = sp.ProposalSellerFileListGet(pro.Id).Where(x => x.CreateTime >= cDate).ToList();
                    if (files.Count > 0)
                    {
                        StringBuilder Memo = new StringBuilder();
                        Memo.AppendLine("<table border='1'>");
                        Memo.AppendLine("<tr>");
                        Memo.AppendLine("<th>檔名</th><th>大小</th><th>建立時間</th>");
                        Memo.AppendLine("</tr>");
                        foreach (ProposalSellerFile file in files)
                        {
                            string size = "0";
                            if (file.FileSize < 1024)
                            {
                                size = file.FileSize.ToString();
                            }
                            else if ((file.FileSize / 1024) > 0)
                            {
                                size = Convert.ToInt32(Math.Round(Convert.ToDecimal(file.FileSize / 1024), 0)).ToString() + " KB";
                            }
                            else if ((file.FileSize / 1024 / 1024) > 0)
                            {
                                size = Convert.ToInt32(Math.Round(Convert.ToDecimal(file.FileSize / 1024 / 1024), 0)).ToString() + " MB";
                            }

                            Memo.AppendLine("<tr>");
                            Memo.AppendLine(string.Format("<td>{0}</td><td>{1}</td><td>{2}</td>", file.OriFileName, size, file.CreateTime.ToString("yyyy-MM-dd")));
                            Memo.AppendLine("</tr>");
                        }
                        Memo.AppendLine("</table>");
                        Memo.AppendLine("• 此信件為系統自動發出，如有任何問題，請直接洽詢商家");
                        ProposalFacade.SendEmail(MailUsers, "【QC確認後上傳檔案通知】 NO." + pro.Id + "  " + pro.BrandName,
                            ProposalFacade.SendSellerProposalMailContent(pro, SaleName, config.SiteUrl + "/sal/ProposalContent.aspx?pid=",
                            Memo.ToString()));
                    }

                }
            }
        }



        public static List<ProposalSellerFile> ProposalSellerFileDelete(int id, string UserName, ProposalLogType logType)
        {
            List<ProposalSellerFile> fileList = new List<ProposalSellerFile>();
            ProposalSellerFile files = sp.ProposalSellerFileGet(id);

            if (files.IsLoaded && files != null)
            {
                files.FileStatus = (int)ProposalFileStatus.Delete;
                sp.ProposalSellerFileSet(files);

                ProposalLog(files.ProposalId, "[刪除檔案]：" + files.OriFileName + "", UserName, logType);

                fileList = sp.ProposalSellerFileListGet(files.ProposalId).ToList();
            }

            return fileList;
        }

        public static bool SalesCalendarEventDelete(int id)
        {
            return pp.SalesCalendarEventDelete(id);
        }

        /// <summary>
        /// 查詢未離職之攝影師權限
        /// </summary>
        public static List<string> GetPhotographerNameArray(string empName)
        {
            var empCollection = hp.ViewEmployeeCollectionGetAll().Where(x => !x.IsInvisible);
            var PhotographerCollection = FunctionPrivilegeManager.GetPrivilegesByFunctionType("/sal/ProposalContent.aspx", SystemFunctionType.Photographer);
            empCollection = empCollection.Where(x => PhotographerCollection.Contains(x.Email));
            var memCollection = mp.MemberGetList(empCollection.Select(x => x.UserId).ToList());
            if (!string.IsNullOrEmpty(empName))
            {
                return memCollection.Where(x => x.UserName.Contains(empName)).Select(x => x.UserName).ToList();
            }
            return memCollection.Select(x => x.UserName).Take(20).ToList();
        }

        public static void ProposalPerformanceLogSet(string link, string content_log, string user_name)
        {
            ProposalPerformanceLog log = new ProposalPerformanceLog();
            log.Link = link;
            log.ContentLog = content_log;
            log.CreateTime = DateTime.Now;
            log.CreateUser = user_name;
            sp.ProposalPerformanceLogSet(log);
        }
        public static void ProposalFileOauthSet(string ticketId, string user_name)
        {
            ProposalFileOauth oauth = new ProposalFileOauth();
            oauth.TicketId = ticketId;
            oauth.CreateTime = DateTime.Now;
            oauth.CreateUser = user_name;
            sp.ProposalFileOauthSet(oauth);
        }

        public static void ZipDownloadFile(string zipFolder, string zipFile)
        {
            using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile(Encoding.UTF8))
            {
                zip.AddDirectory(zipFolder);
                zip.Save(zipFile);
            }
        }

        public static List<Cross> GetCrossDeptTeam(string CrossDeptTeam)
        {
            List<Cross> listcross = new List<Cross>();
            foreach (var c in CrossDeptTeam.Split('/'))
            {
                Cross cross = new Cross();

                if (c.Contains("["))
                {

                    cross.Dept = c.Substring(0, c.IndexOf("["));
                    cross.Team = c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1);
                }
                else
                {
                    cross.Dept = c;
                    cross.Team = "";
                }
                listcross.Add(cross);
            }
            return listcross;
        }

        public static bool GetCrossPrivilege(int type, ViewEmployee emp, string DeptId, int? TeamNo, string DeptName, List<MultipleSalesModel> model, ref string message)
        {
            string typemessage = "";
            if (type == 1)
            {
                typemessage = "商家資訊";
            }
            else if (type == 2)
            {
                typemessage = "提案單資訊";
            }
            else if (type == 3)
            {
                typemessage = "檔次資訊";
            }

            bool check = true;
            if (type == 1)
            {
                List<Cross> listCross = new List<Cross>();
                listCross = ProposalFacade.GetCrossDeptTeam(emp.CrossDeptTeam);

                if (!string.IsNullOrEmpty(emp.CrossDeptTeam))
                {
                    if ((listCross.Select(x => x.Dept).ToList().Intersect(model.Select(x => x.DevelopeSalesDeptId).ToList())).Count() > 0 ||
                        (listCross.Select(x => x.Dept).ToList().Intersect(model.Select(x => x.OperationSalesDeptId).ToList())).Count() > 0)
                    {

                        //先預設false
                        check = false;
                        foreach (var lc in listCross)
                        {
                            if (lc.Team != "")
                            {
                                //同單位同組
                                var split = lc.Team.Split(',');
                                foreach (var s in split)
                                {
                                    if (model.Exists(x => x.DevelopeSalesDeptId == lc.Dept && x.DevelopeSalesTeamNo.ToString() == s) || model.Exists(x => x.OperationSalesDeptId == lc.Dept && x.OperationSalesTeamNo.ToString() == s))
                                    {
                                        check = true;
                                        break;
                                    }
                                }

                            }
                            else
                            {
                                //同單位(全)
                                if (model.Exists(x => x.DevelopeSalesDeptId == lc.Dept) || model.Exists(x => x.OperationSalesDeptId == lc.Dept))
                                {
                                    check = true;
                                    break;
                                }
                            }
                        }

                        if (!check)
                        {
                            message = string.Format("您歸屬的業務部門為 {0} {1} 組 ，無法檢視該" + typemessage + "。", emp.DeptName, emp.TeamNo);
                        }
                    }
                    else
                    {
                        //不同單位
                        message = string.Format("您歸屬的業務部門為 {0} ，無法檢視該" + typemessage + "。", emp.DeptName);
                        check = false;
                    }
                }
                else
                {
                    //不同單位不同組
                    if ((model.Exists(x => x.DevelopeSalesDeptId == emp.DeptId && x.DevelopeSalesTeamNo == emp.TeamNo)) || (model.Exists(x => x.OperationSalesDeptId == emp.DeptId && x.OperationSalesTeamNo == emp.TeamNo)))
                    {
                        check = true;
                    }
                    else
                    {
                        message = string.Format("您歸屬的業務部門為 {0} {1} 組 ，無法檢視該" + typemessage + "。", emp.DeptName, emp.TeamNo);
                        check = false;
                    }
                }

            }
            else
            {
                if (!string.IsNullOrEmpty(emp.CrossDeptTeam))
                {
                    List<Cross> listCross = new List<Cross>();
                    listCross = ProposalFacade.GetCrossDeptTeam(emp.CrossDeptTeam);

                    if (listCross.Exists(x => x.Dept == DeptId))
                    {
                        var lc = listCross.Find(x => x.Dept == DeptId);
                        if (lc.Team != "")
                        {
                            var split = lc.Team.Split(',');
                            if (split.Where(x => x == TeamNo.ToString()).Count() > 0)
                            {
                                //同單位同組
                                check = true;
                            }
                            else
                            {
                                //同單位不同組
                                message = string.Format("您歸屬的業務部門為 {0} {1} 組 ，無法檢視 {2} {3} 組 的" + typemessage + "。", emp.DeptName, emp.TeamNo, DeptName, TeamNo);
                                check = false;
                            }
                        }
                        else
                        {
                            //同單位(全)
                            check = true;
                        }
                    }
                    else
                    {
                        //不同單位
                        message = string.Format("您歸屬的業務部門為 {0} ，無法檢視 {1} 的" + typemessage + "。", emp.DeptName, DeptName);
                        check = false;
                    }
                }
                else
                {
                    //不同單位不同組
                    if (emp.DeptId == DeptId && emp.TeamNo == TeamNo)
                    {
                        check = true;
                    }
                    else
                    {
                        message = string.Format("您歸屬的業務部門為 {0} {1} 組 ，無法檢視 {2} {3} 組 的" + typemessage + "。", emp.DeptName, emp.TeamNo, DeptName, TeamNo);
                        check = false;
                    }
                }
            }


            return check;
        }

        /// <summary>
        /// 顯示商家帳號
        /// </summary>
        /// <param name="VbsRight"></param>
        /// <param name="RootSellerGuid"></param>
        /// <param name="StoreGuid"></param>
        /// <param name="BusinessHourGuid"></param>
        /// <param name="strVbsAccountID"></param>
        /// <param name="strMoreVbsAccountID"></param>
        /// <param name="IsShowToolTip"></param>
        public static void ShowVbsAccount(int VbsRight, Guid RootSellerGuid, string StoreGuid, Guid BusinessHourGuid, out string strVbsAccountID, out string strMoreVbsAccountID, out bool IsShowToolTip)
        {
            strVbsAccountID = "";
            strMoreVbsAccountID = "";
            IsShowToolTip = false;

            //判斷是否顯示帳號、黑灰
            if (Helper.IsFlagSet(VbsRight, VbsRightFlag.VerifyShop) || Helper.IsFlagSet(VbsRight, VbsRightFlag.Verify) ||
                Helper.IsFlagSet(VbsRight, VbsRightFlag.Accouting) || Helper.IsFlagSet(VbsRight, VbsRightFlag.ViewBalanceSheet))
            {
                //有任一勾就顯示帳號
                Guid SellerGuid = Guid.Empty;
                Guid.TryParse(StoreGuid, out SellerGuid);
                List<Guid> SellerGuidList = new List<Guid>() { SellerGuid };


                //找尋該賣家的所有帳號
                List<string> rac = MemberFacade.ResourceAclGetListByResourceGuid(SellerGuidList);

                if (rac.Count() > 0)
                {
                    int VbsAccountCount = 0;
                    int ShowMaxAccountCount = 3;
                    if (Helper.IsFlagSet(VbsRight, VbsRightFlag.Verify))
                    {
                        foreach (var ra in rac)
                        {

                            bool SellerPri = false;

                            if (RootSellerGuid == SellerGuid)
                            {
                                //檢查賣家權限
                                if (MemberFacade.ResourceAclGetListByAccounntId(SellerGuidList, ra, 1))
                                {
                                    SellerPri = true;
                                }
                            }
                            else
                            {
                                //檢查分店權限
                                if (MemberFacade.ResourceAclGetListByAccounntId(SellerGuidList, ra, 2))
                                {
                                    SellerPri = true;
                                }
                            }

                            //該帳號與該檔次權限關係
                            if (MemberFacade.FnVbsFlattenedAclLiteGetList(ra, BusinessHourGuid, SellerGuid) && SellerPri)
                            {
                                if (VbsAccountCount < ShowMaxAccountCount)
                                {
                                    strVbsAccountID += "<span style='color: black; '>" + ra + "</span>" + " / ";
                                }

                                if (rac.Count() > ShowMaxAccountCount)
                                {
                                    strMoreVbsAccountID += "<span style='color: black; '>" + ra + "</span>" + " / ";
                                }
                            }
                            else
                            {
                                if (VbsAccountCount < ShowMaxAccountCount)
                                {
                                    strVbsAccountID += "<span style='color: gray; '>" + ra + "</span>" + " / ";
                                }

                                if (rac.Count() > ShowMaxAccountCount)
                                {
                                    strMoreVbsAccountID += "<span style='color: gray; '>" + ra + "</span>" + " / ";
                                }
                            }

                            VbsAccountCount++;
                        }

                        strVbsAccountID = strVbsAccountID.TrimEnd(" / ");
                        strMoreVbsAccountID = strMoreVbsAccountID.TrimEnd(" / ");
                    }
                    else
                    {
                        if (rac.Count() > ShowMaxAccountCount)
                        {
                            strMoreVbsAccountID += "<span style='color: gray; '>" + string.Join(" / ", rac) + "</span>";
                            rac.RemoveAt(ShowMaxAccountCount);
                            strVbsAccountID += "<span style='color: gray; '>" + string.Join(" / ", rac) + "</span>";
                        }
                        else
                        {
                            strVbsAccountID += "<span style='color: gray; '>" + string.Join(" / ", rac) + "</span>";
                        }
                    }

                    //多於三個帳號顯示小提示
                    if (rac.Count() > ShowMaxAccountCount)
                    {
                        strMoreVbsAccountID = "<span class='tooltiptext'>" + strMoreVbsAccountID + "</span>";
                        IsShowToolTip = true;
                    }

                }

            }
        }

        private static string CachePrefix = "Proposal";
        public static Proposal GetProposals(Guid bid)
        {
            if (bid == Guid.Empty)
            {
                return null;
            }
            var storage = System.Runtime.Caching.MemoryCache.Default.Get(CachePrefix) as System.Collections.Concurrent.ConcurrentDictionary<Guid, Proposal>;
            if (storage == null)
            {
                storage = new System.Collections.Concurrent.ConcurrentDictionary<Guid, Proposal>();
                System.Runtime.Caching.MemoryCache.Default.Add(CachePrefix, storage, System.Runtime.Caching.ObjectCache.InfiniteAbsoluteExpiration);
            }
            var result = storage.GetOrAdd(bid, delegate
            {
                var pro = new Proposal();
                List<Guid> bids = new List<Guid>();
                bids.Add(bid);
                pro = sp.ProposalGetByBids(bids).FirstOrDefault();
                return pro;
            });
            return result;
        }

        public static void ClearProposalCaches(IEnumerable<Guid> bids)
        {
            if (bids == null)
            {
                return;
            }
            var storage = System.Runtime.Caching.MemoryCache.Default.Get(CachePrefix) as System.Collections.Concurrent.ConcurrentDictionary<Guid, Proposal>;
            if (storage == null)
            {
                storage = new System.Collections.Concurrent.ConcurrentDictionary<Guid, Proposal>();
                System.Runtime.Caching.MemoryCache.Default.Add(CachePrefix, storage, System.Runtime.Caching.ObjectCache.InfiniteAbsoluteExpiration);
            }
            foreach (Guid bid in bids)
            {
                Proposal tmp;
                storage.TryRemove(bid, out tmp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="IgnoreCRLF">是否忽略換行</param>
        /// <returns></returns>
        public static string RemoveSpecialCharacter(string context, bool ignoreCRLF = false)
        {
            //u0008-BS
            //u0003-ETX
            //u000B-VT
            //u2028-LS
            //9    -Tab
            //return Regex.Replace(context, @"[\u000B\u0003\u0008\u2028]", string.Empty);

            string output = "";

            foreach (char c in context)
            {
                int asciiInt = Convert.ToInt32(c);
                if (ignoreCRLF && (asciiInt == 10 || asciiInt == 13))
                {
                    //換行不去除
                    output += c;
                }
                else if (((asciiInt >= 0 && asciiInt <= 31) || asciiInt == 8232) == false)
                {
                    //非控制字元
                    output += c;
                }

            }

            return output;

        }


        public static bool ProposalDraw(int pid, string ProposalDrawReason, string UserName)
        {
            Proposal pro = sp.ProposalGet(pid);
            if (pro != null && pro.IsLoaded)
            {
                if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                {
                    return false;
                }
                pro.ApplyFlag = (int)ProposalApplyFlag.Initial;
                pro.ApproveFlag = (int)ProposalApproveFlag.Initial;
                sp.ProposalSet(pro);

                ProposalLog(pro.Id, string.Format("[抽單]{0}", ProposalDrawReason), UserName);
            }
            return true;
        }

        /// <summary>
        /// 創意部查詢指派人員
        /// </summary>
        /// <param name="AssignPro"></param>
        /// <param name="AssignFlag"></param>
        /// <param name="AssignEmp"></param>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public static bool AssignProduction(string AssignPro, string AssignFlag, string AssignEmp, string UserName)
        {
            List<string> AssignProList = AssignPro.Split(',').ToList();
            List<string> AssignFlagList = AssignFlag.Split(',').ToList();

            if (!string.IsNullOrEmpty(AssignFlag) && !string.IsNullOrEmpty(AssignEmp))
            {
                ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, AssignEmp);
                string log = "";
                for (int i = 0; i < AssignProList.Count(); i++)
                {
                    for (int j = 0; j < AssignFlagList.Count(); j++)
                    {
                        int ProposalID = Convert.ToInt32(AssignProList[i]);
                        Proposal pro = sp.ProposalGet(ProposalID);

                        string flag = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalEditorFlag)Convert.ToInt32(AssignFlagList[j]));
                        log = string.Format("指派 {0} 為 {1}", flag, emp.EmpName);
                        ProposalLog(ProposalID, log, UserName, ProposalLogType.Assign);

                        string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, ProposalID);
                        string subject = string.Format("【指派】 單號 NO.{0} <{1}>", ProposalID, flag);
                        List<string> mailToUser = new List<string>() { emp.Email };

                        ProposalAssignLog palc = sp.ProposalAssignLogGetList(ProposalID).Where(x => x.AssignFlag == Convert.ToInt32(AssignFlagList[j])).FirstOrDefault();
                        if (palc != null)
                        {
                            //Update
                            palc.AssignEmail = emp.EmpName;
                            palc.CreateId = UserName;
                            palc.CreateTime = DateTime.Now;
                            sp.ProposalAssignLogSet(palc);
                        }
                        else
                        {
                            //Create
                            ProposalAssignLog paf = new ProposalAssignLog()
                            {
                                Pid = ProposalID,
                                AssignFlag = Convert.ToInt32(AssignFlagList[j]),
                                AssignEmail = emp.EmpName,
                                CreateId = UserName,
                                CreateTime = DateTime.Now
                            };
                            sp.ProposalAssignLogSet(paf);
                        }

                        Seller s = sp.SellerGet(pro.SellerGuid);
                        ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                        SendEmail(mailToUser, subject, GetProposalBodyHtml(pro.Id, s.SellerName, deSalesEmp.EmpName, url));

                        //指派後要取消檢核
                        pro.EditFlag = Convert.ToInt32(Helper.SetFlag(false, pro.EditFlag, (ProposalEditorFlag)Convert.ToInt32(AssignFlagList[j])));
                        sp.ProposalSet(pro);

                    }
                }


            }

            return true;
        }


        /// <summary>
        /// 匯款對象商家檢核財務資料
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="proposalId"></param>
        public static void CheckProposalFinanceCheck(int type, string userName, int proposalId, Guid bid)
        {
            List<Guid> guidList = new List<Guid>();
            Proposal pro = null;
            if (type == 1)
            {
                //提案單
                ProposalStoreCollection ps = pp.ProposalStoreGetListByProposalId(proposalId);
                guidList = ps.Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid).ToList();
                pro = sp.ProposalGet(proposalId);
            }
            else if (type == 2)
            {
                //建檔平台
                PponStoreCollection ps = pp.PponStoreGetListByBusinessHourGuid(bid);
                guidList = ps.Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid).ToList();
                pro = sp.ProposalGet(bid);
            }


            bool checkFinance = true;
            foreach (var guid in guidList)
            {
                Seller s = sp.SellerGet(guid);
                if (string.IsNullOrEmpty(s.CompanyAccount))
                {
                    checkFinance = false;
                    break;
                }
            }



            if (Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.FinanceCheck) != checkFinance)
            {
                pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(checkFinance, pro.ListingFlag, ProposalListingFlag.FinanceCheck));
                pro.Status = SellerFacade.GetFlagForProposalFlagCheck(pro);
                pro.ModifyId = userName;
                pro.ModifyTime = DateTime.Now;
                sp.ProposalSet(pro);

                ProposalLog(pro.Id, (checkFinance ? string.Empty : "取消-") + Helper.GetLocalizedEnum(ProposalListingFlag.FinanceCheck), userName);
            }


        }

        public static string PponAssistantEmailGet(string deptId)
        {
            string pponAssistantEmail = string.Empty;

            //業助群組
            string dataName = "ProposalEmail";
            SystemData data = sys.SystemDataGet(dataName);
            List<ProposalEmail> ProposalEmail = new List<ProposalEmail>();
            if (data.IsLoaded)
            {
                ProposalEmail = new JsonSerializer().Deserialize<List<ProposalEmail>>(data.Data);
            }


            //if (deptId == EmployeeChildDept.S001.ToString() || (deptId == EmployeeChildDept.S007.ToString()) || (deptId == EmployeeChildDept.S013.ToString()))
            //{
            //    pponAssistantEmail = ProposalEmail.Find(x => x.name == "台北業助群組").email;
            //}
            //else if (deptId == EmployeeChildDept.S002.ToString())
            //{
            //    pponAssistantEmail = ProposalEmail.Find(x => x.name == "桃園業助群組").email;
            //}
            //else if (deptId == EmployeeChildDept.S003.ToString())
            //{
            //    pponAssistantEmail = ProposalEmail.Find(x => x.name == "新竹業助群組").email;
            //}
            //else if (deptId == EmployeeChildDept.S004.ToString())
            //{
            //    pponAssistantEmail = ProposalEmail.Find(x => x.name == "台中業助群組").email;
            //}
            //else if (deptId == EmployeeChildDept.S005.ToString())
            //{
            //    pponAssistantEmail = ProposalEmail.Find(x => x.name == "高雄業助群組").email;
            //}
            //else if (deptId == EmployeeChildDept.S006.ToString())
            //{
            //    pponAssistantEmail = ProposalEmail.Find(x => x.name == "台南業助群組").email;
            //}
            //else if (deptId == EmployeeChildDept.S008.ToString())
            //{
            //    pponAssistantEmail = ProposalEmail.Find(x => x.name == "休閒產業業助群組").email;
            //}
            //else if (deptId == EmployeeChildDept.S012.ToString())
            //{
            //    pponAssistantEmail = ProposalEmail.Find(x => x.name == "精品生活業助群組").email;
            //}


            if (deptId != EmployeeChildDept.S010.ToString() && hp.DepartmentGet(deptId).ParentDeptId != EmployeeDept.M000.ToString())
            {
                //在地各區業助群組
                if (deptId == EmployeeChildDept.S008.ToString())
                {
                    //旅遊
                    pponAssistantEmail = ProposalEmail.Find(x => x.name == "休閒產業業助群組").email;
                }
                else
                {
                    //其他在地
                    pponAssistantEmail = config.OpdEmail;
                }

            }
            else
            {
                //全國宅配特助群組
                //宅配或行銷
                pponAssistantEmail = config.SalesSpecialAssistantShippingEmail;
            }


            return pponAssistantEmail;
        }


        /// <summary>
        /// 取得提案單業務
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        public static ProposalSalesModel ProposalSaleGetByPid(int pid)
        {
            ProposalSalesModel proposalSalesModel = new ProposalSalesModel();
            Proposal p = sp.ProposalGet(pid);
            ViewEmployee developeEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, p.DevelopeSalesId);
            ViewEmployee operationEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, p.OperationSalesId);


            proposalSalesModel.DevelopeSalesEmail = developeEmp.Email;
            proposalSalesModel.DevelopeSalesEmpName = developeEmp.EmpName;
            proposalSalesModel.DevelopeSalesDeptName = developeEmp.DeptName;
            proposalSalesModel.OperationSalesEmail = operationEmp.Email ?? "";
            proposalSalesModel.OperationSalesEmpName = operationEmp.EmpName ?? "";
            proposalSalesModel.OperationSalesDeptName = operationEmp.DeptName ?? "";


            return proposalSalesModel;
        }

        /// <summary>
        /// 取得檔次業務
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        public static ProposalSalesModel DealGetSalesByBid(Guid bid)
        {
            ProposalSalesModel proposalSalesModel = new ProposalSalesModel();
            DealProperty dp = pp.DealPropertyGet(bid);
            ViewEmployee developeEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, dp.DevelopeSalesId);
            ViewEmployee operationEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, dp.OperationSalesId);


            proposalSalesModel.DevelopeSalesEmail = developeEmp.Email;
            proposalSalesModel.DevelopeSalesEmpName = developeEmp.EmpName;
            proposalSalesModel.DevelopeSalesDeptName = developeEmp.DeptName;
            proposalSalesModel.OperationSalesEmail = operationEmp.Email ?? "";
            proposalSalesModel.OperationSalesEmpName = operationEmp.EmpName ?? "";
            proposalSalesModel.OperationSalesDeptName = operationEmp.DeptName ?? "";


            return proposalSalesModel;
        }

        public static Dictionary<Guid, string> GetVbsSeller(string UserId, bool chkStoreStatus = false)
        {
            Dictionary<Guid, string> dicSeller = new Dictionary<Guid, string>();
            VbsVendorAclMgmtModel aclModel = new VbsVendorAclMgmtModel(UserId);
            var data = aclModel.GetAllowedDealStores().ToList();
            List<Guid> sid = new List<Guid>();
            sid = data.Distinct().ToList();
            if (sid.Count == 0)
            {
                sid = mp.ResourceAclGetListByAccountId(UserId)
                                       .Where(x => x.ResourceType == (int)ResourceType.Seller)
                                       .Select(x => x.ResourceGuid).Distinct().ToList();
            }
            if (chkStoreStatus)
            {
                var rtn = sp.SellerGetList(sid).Where(x => x.StoreStatus == (int)StoreStatus.Available).ToDictionary(x => x.Guid, x => x.SellerName);
                return rtn;
            }
            else
            {
                var rtn = sp.SellerGetList(sid).ToDictionary(x => x.Guid, x => x.SellerName);
                return rtn;
            }
        }

        public static List<Seller> GetVbsSellerByCol(string UserId, bool chkStoreStatus = false)
        {
            Dictionary<Guid, string> sellers = GetVbsSeller(UserId, chkStoreStatus);
            List<Seller> newSellers = new List<Seller>();
            foreach (var da in sellers)
            {
                Seller s = sp.SellerGet(da.Key);
                if (s != null && s.IsLoaded)
                {
                    newSellers.Add(s);
                }
            }
            return newSellers;
        }
        public static ProposalMultiDealCollection ProposalMultiDealGetByPid(int pid)
        {
            ProposalMultiDealCollection multiDeals = sp.ProposalMultiDealGetByPid(pid);

            return multiDeals;
        }

        public static ProposalMultiDeal ProposalMultiDealGetById(int id)
        {
            ProposalMultiDeal deal = sp.ProposalMultiDealGetById(id);

            return deal;
        }


        /// <summary>
        /// 取得提案單合約相關table資料
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="model"></param>
        /// <param name="stores"></param>
        /// <param name="psc"></param>
        public static void GetPponDealData(Proposal pro, out SalesProposalModel model, out SellerCollection stores, out ProposalStoreCollection psc)
        {
            model = new SalesProposalModel();

            #region Get Model Data

            Seller s = sp.SellerGet(pro.SellerGuid);
            ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pro.Id);

            model.SellerProperty = s;
            model.ContentProperty = pcec;

            #endregion

            psc = pp.ProposalStoreGetListByProposalId(pro.Id);
            stores = sp.SellerGetListBySellerGuidList(psc.Select(x => x.StoreGuid).ToList());
        }

        /// <summary>
        /// 取得提案單合約資料
        /// </summary>
        /// <param name="model"></param>
        /// <param name="pro"></param>
        /// <param name="isListCheck"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetPponDealDictionary(SalesProposalModel model, Proposal pro, bool isListCheck)
        {
            string newLine = "\n";
            Dictionary<string, string> dataList = new Dictionary<string, string>();
            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);


            #region 抬頭
            dataList.Add("提案單單號", pro.Id.ToString());
            dataList.Add("BrandName", pro.BrandName);//品牌名稱
            dataList.Add("CompanyName", model.SellerProperty.CompanyName);//公司名稱 
            #endregion

            #region 上架確認單帶入總店資料
            if (pro.DeliveryType == (int)DeliveryType.ToShop || (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType == (int)ProposalDealType.Travel))
            {
                if (isListCheck)
                {
                    dataList.Add("ContactPerson", model.SellerProperty.SellerContactPerson);
                    dataList.Add("BossMobile", model.SellerProperty.SellerMobile);
                    dataList.Add("BossTel", model.SellerProperty.SellerTel);
                    dataList.Add("BossEmail", model.SellerProperty.SellerEmail);
                    dataList.Add("BossFax", model.SellerProperty.SellerFax);
                }
            }
            #endregion

            #region 優惠條件內容
            ProposalMultiDealCollection multiDeal = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);
            string dealDesc = string.Empty;
            string[] paragraph = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            for (int i = 0; i < multiDeal.Count; i++)
            {
                dealDesc += string.Format("{0}.1　", paragraph[i]);



                if (config.IsVbsProposalNewVersion)
                {
                    if (pro.ProposalSourceType == (int)ProposalSourceType.House)
                    {
                        //草稿要抓新的status!!!
                        bool status = true;
                        //上架確認單、草稿要印賣價，其他不印賣價
                        if (isListCheck || !status)
                        {
                            dealDesc += string.Format("原價${0}；售價${1}；", multiDeal[i].OrigPrice.ToString("F0"), multiDeal[i].ItemPrice.ToString("F0"));
                        }
                        else
                        {
                            dealDesc += string.Format("原價${0}；", multiDeal[i].OrigPrice.ToString("F0"));
                        }
                    }
                    else
                    {
                        //上架確認單、草稿要印賣價，其他不印賣價
                        if (isListCheck || !Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                        {
                            dealDesc += string.Format("原價${0}；售價${1}；", multiDeal[i].OrigPrice.ToString("F0"), multiDeal[i].ItemPrice.ToString("F0"));
                        }
                        else
                        {
                            dealDesc += string.Format("原價${0}；", multiDeal[i].OrigPrice.ToString("F0"));
                        }

                        dealDesc += string.Format("優惠活動：{0}", multiDeal[i].ItemName + multiDeal[i].ItemNameTravel);
                    }


                }
                else
                {
                    //上架確認單、草稿要印賣價，其他不印賣價
                    if (isListCheck || !Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                    {
                        dealDesc += string.Format("原價${0}；售價${1}；", multiDeal[i].OrigPrice.ToString("F0"), multiDeal[i].ItemPrice.ToString("F0"));
                    }
                    else
                    {
                        dealDesc += string.Format("原價${0}；", multiDeal[i].OrigPrice.ToString("F0"));
                    }

                    dealDesc += string.Format("優惠活動：{0}", multiDeal[i].ItemName + multiDeal[i].ItemNameTravel);

                }

                dealDesc += string.Format(newLine + "{0}.2　最高購買數量{1}單位；", paragraph[i], multiDeal[i].OrderTotalLimit.ToString("F0"), multiDeal[i]);
                bool applyServiceChannel = false;
                ViewVbsInstorePickupCollection vips = sp.ViewVbsInstorePickupCollectionGet(pro.SellerGuid);
                var vip = vips.Where(x => x.IsEnabled == true).FirstOrDefault();
                if (vip != null)
                {
                    applyServiceChannel = true;
                }
                int num = 4;

                if (applyServiceChannel)
                {
                    dealDesc += string.Format(newLine + "{0}.3　超商取貨限制：{1} 組；", paragraph[i], multiDeal[i].BoxQuantityLimit);
                    dealDesc += string.Format(newLine + "{0}.4　上架費份數：{1}；進貨價格：{2}(含稅)", paragraph[i], multiDeal[i].SlottingFeeQuantity, multiDeal[i].Cost.ToString("F0"));
                    num = 5;
                }
                else
                {
                    dealDesc += string.Format(newLine + "{0}.3　上架費份數：{1}；進貨價格：{2}(含稅)", paragraph[i], multiDeal[i].SlottingFeeQuantity, multiDeal[i].Cost.ToString("F0"));
                }

                if (pro.DeliveryType == (int)DeliveryType.ToHouse && multiDeal[i].Freights != 0)
                {
                    dealDesc += string.Format(newLine + "{0}.{1}　運費：{2}元整", paragraph[i], num, multiDeal[i].Freights.ToString("F0"));
                    if (multiDeal[i].NoFreightLimit != 0)
                    {
                        dealDesc += string.Format("；{0}份免運", multiDeal[i].NoFreightLimit.ToString("F0"));
                    }
                    num++;
                }
                if (pro.ProposalSourceType == (int)ProposalSourceType.Original)
                {
                    if (!string.IsNullOrWhiteSpace(multiDeal[i].Options))////之後沒有
                    {
                        dealDesc += string.Format(newLine + "{0}.{1}　款式選項：{2}", paragraph[i], num, multiDeal[i].Options);
                    }
                }
                else
                {
                    List<string> specDesc = new List<string>();
                    List<ProposalMultiDealsSpec> dbSpecs = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(multiDeal[i].Options);
                    foreach (ProposalMultiDealsSpec spec in dbSpecs)
                    {
                        foreach (ProposalMultiDealsItem item in spec.Items)
                        {
                            specDesc.Add(item.name);
                        }
                    }
                    dealDesc += string.Format(newLine + "{0}.{1}　款式選項：{2}", paragraph[i], num, string.Join(",", specDesc));
                }

                dealDesc += newLine + newLine;
            }
            dataList.Add("DealDesc", dealDesc);
            #endregion

            #region 活動有效使用時間
            if (pro.DeliveryType == (int)DeliveryType.ToShop || pro.DealType == (int)ProposalDealType.Travel)
            {


                if (pro.StartUseUnit == (int)ProposalStartUseUnitType.BeforeDay)
                {
                    dataList.Add("StartUseDesc", string.Format("{0}{1}皆可使用", pro.StartUseText2 + "~" + pro.StartUseText, Helper.GetLocalizedEnum((ProposalStartUseUnitType)pro.StartUseUnit)));
                }
                else
                {
                    dataList.Add("StartUseDesc", string.Format("活動開始兌換後{0}{1}皆可使用", pro.StartUseText, Helper.GetLocalizedEnum((ProposalStartUseUnitType)pro.StartUseUnit)));
                }
            }
            #endregion

            #region 權益說明
            string provisionList = string.Empty;
            int index = 1;

            if (model.ContentProperty.Restrictions != null)
            {
                model.ContentProperty.Restrictions = model.ContentProperty.Restrictions.Replace("<br />", newLine);
                foreach (var provi in Regex.Replace(model.ContentProperty.Restrictions, @"<[^>]*>", string.Empty).Split(newLine))
                {
                    provisionList += provi + newLine;
                    index++;
                }
                dataList.Add("ProvisionList", provisionList.TrimEnd(newLine.ToCharArray()));
            }
            else
            {
                dataList.Add("ProvisionList", string.Empty);
            }


            #endregion

            #region 商品相關資訊
            if (pro.DeliveryType == (int)DeliveryType.ToHouse)
            {
                #region 可提供憑證

                if (pro.DealType != (int)ProposalDealType.Travel)
                {
                    string invoice = string.Empty;
                    if (pro.VendorReceiptType != null)
                    {
                        switch ((VendorReceiptType)pro.VendorReceiptType)
                        {
                            case VendorReceiptType.Other:
                                invoice = pro.Othermessage;
                                break;
                            case VendorReceiptType.Receipt:
                            case VendorReceiptType.Invoice:
                            case VendorReceiptType.NoTaxInvoice:
                                invoice = Helper.GetLocalizedEnum((VendorReceiptType)pro.VendorReceiptType);
                                break;
                            default:
                                break;
                        }
                    }

                    dataList.Add("Invoice", invoice);
                }
                #endregion


                if (config.IsVbsProposalNewVersion)
                {
                    if (pro.ProposalSourceType == (int)ProposalSourceType.House)
                    {
                        dataList.Add("IsParallelProduct", pro.DealSource == (int)ProposalDealSource.ParallelImportation ? "是" : "否");

                        ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pro.Id);
                        string productSpec = Regex.Replace(pcec.ProductSpec, "<[^>]*>", "");
                        dataList.Add("ProductSpec", !string.IsNullOrEmpty(pcec.ProductSpec) ? productSpec.Replace(System.Environment.NewLine, newLine) : "無");//讀哪裡要再確認

                    }
                    else
                    {
                        dataList.Add("IsParallelProduct", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ParallelImportation) ? "是" : "否");
                        dataList.Add("IsExpiringItems", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ExpiringProduct) ? "是" : "否");
                        dataList.Add("ExpiringItemsDate", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ExpiringProduct) && specialText.ContainsKey(ProposalSpecialFlag.ExpiringProduct) ? specialText[ProposalSpecialFlag.ExpiringProduct] : string.Empty);
                        dataList.Add("ProductSpec", !string.IsNullOrEmpty(pro.ProductSpec) ? pro.ProductSpec.Replace(System.Environment.NewLine, newLine) : "無");
                        dataList.Add("ProductIngredients", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Beauty) && specialText.ContainsKey(ProposalSpecialFlag.Beauty) ? specialText[ProposalSpecialFlag.Beauty] : string.Empty);
                        dataList.Add("InspectRule", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.InspectRule) && specialText.ContainsKey(ProposalSpecialFlag.InspectRule) ? specialText[ProposalSpecialFlag.InspectRule] : string.Empty);
                    }
                }
                else
                {
                    dataList.Add("IsParallelProduct", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ParallelImportation) ? "是" : "否");
                    dataList.Add("IsExpiringItems", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ExpiringProduct) ? "是" : "否");
                    dataList.Add("ExpiringItemsDate", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ExpiringProduct) && specialText.ContainsKey(ProposalSpecialFlag.ExpiringProduct) ? specialText[ProposalSpecialFlag.ExpiringProduct] : string.Empty);
                    dataList.Add("ProductSpec", !string.IsNullOrEmpty(pro.ProductSpec) ? pro.ProductSpec.Replace(System.Environment.NewLine, newLine) : "無");
                    dataList.Add("ProductIngredients", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Beauty) && specialText.ContainsKey(ProposalSpecialFlag.Beauty) ? specialText[ProposalSpecialFlag.Beauty] : string.Empty);
                    dataList.Add("InspectRule", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.InspectRule) && specialText.ContainsKey(ProposalSpecialFlag.InspectRule) ? specialText[ProposalSpecialFlag.InspectRule] : string.Empty);
                }

                dataList.Add("DeliveryMethod", !pro.DeliveryMethod.ToString().Equals("") ? Helper.GetLocalizedEnum((ProposalDeliveryMethod)pro.DeliveryMethod) : "");


                #region 出貨與退貨
                string businessOrderUseTimeSet = string.Empty;
                List<string> bList = new List<string>();

                if (config.IsVbsProposalNewVersion)
                {
                    if (pro.ProposalSourceType == (int)ProposalSourceType.House)
                    {
                        if ((int)pro.ShipType == (int)DealShipType.Ship72Hrs)
                        {
                            bList.Add("24小時出貨不適用區域：離島及部分偏遠地區。");
                            bList.Add("商品最晚於購買成功後24小時出貨完畢(不含假日)，出貨後配送約2~3個工作日，恕無法指定送達時間。");
                            bList.Add("此專案供應商每日應登入本平台商家系統回報出貨狀況並回填出貨單號。");
                            bList.Add("若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受合約第六條第3項第甲款賠償金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。");

                        }
                        else if ((int)pro.ShipType == (int)DealShipType.Normal)
                        {
                            bList.Add(string.Format("商品最晚於購買成功{0}個工作日內出貨完畢，出貨後配送約2~3個工作日，恕無法指定送達時間(限台灣本島配送)", pro.ShipText3));
                            bList.Add("供應商應於出貨後1天內，登入本平台商家系統回報出貨狀況並回填出貨單號。");
                            bList.Add("若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受合約第六條第3項第甲款賠償金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。");
                        }
                        else if ((int)pro.ShipType == (int)DealShipType.Other)
                        {
                            bList.Add("配送時間：商品將於yyyy/mm/dd起開始陸續出貨，最晚將於yyyy/mm/dd前出貨完畢；出貨後配送約2~3個工作日，恕無法指定配達時間");
                        }
                    }
                    else
                    {
                        if (pro.DealType == (int)ProposalDealType.Piinlife)
                        {
                            bList.Add(I18N.Message.SalesBusinessOrderPiinlifeDeliveryDesc);
                        }
                        else if ((int)pro.ShipType == (int)DealShipType.Ship72Hrs)
                        {
                            bList.Add("24小時出貨不適用區域：離島及部分偏遠地區。");
                            bList.Add("商品最晚於購買成功後24小時出貨完畢(不含假日)，出貨後配送約2~3個工作日，恕無法指定送達時間。");
                            bList.Add("此專案供應商每日應登入本平台商家系統回報出貨狀況並回填出貨單號。");
                            bList.Add("若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受合約第六條第3項第甲款賠償金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(pro.ShipText1))
                            {
                                bList.Add(string.Format("最早出貨日：上檔後+{0}個工作日；最晚出貨日：統一結檔後+{1}個工作日，出貨後配送約2~3個工作日，恕無法指定送達時間(限台灣本島配送)", pro.ShipText1, pro.ShipText2));
                            }
                            else
                            {
                                bList.Add(string.Format("商品最晚於購買成功{0}個工作日內出貨完畢，出貨後配送約2~3個工作日，恕無法指定送達時間(限台灣本島配送)", pro.ShipText3));
                            }

                            bList.Add("供應商應於出貨後1天內，登入本平台商家系統回報出貨狀況並回填出貨單號。");
                            bList.Add("若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受合約第六條第3項第甲款賠償金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。");
                        }
                    }
                }
                else
                {
                    if (pro.DealType == (int)ProposalDealType.Piinlife)
                    {
                        bList.Add(I18N.Message.SalesBusinessOrderPiinlifeDeliveryDesc);
                    }
                    else if ((int)pro.ShipType == (int)DealShipType.Ship72Hrs)
                    {
                        bList.Add("24小時出貨不適用區域：離島及部分偏遠地區。");
                        bList.Add("商品最晚於購買成功後24小時出貨完畢(不含假日)，出貨後配送約2~3個工作日，恕無法指定送達時間。");
                        bList.Add("此專案供應商每日應登入本平台商家系統回報出貨狀況並回填出貨單號。");
                        bList.Add("若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受合約第六條第3項第甲款賠償金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(pro.ShipText1))
                        {
                            bList.Add(string.Format("最早出貨日：上檔後+{0}個工作日；最晚出貨日：統一結檔後+{1}個工作日，出貨後配送約2~3個工作日，恕無法指定送達時間(限台灣本島配送)", pro.ShipText1, pro.ShipText2));
                        }
                        else
                        {
                            bList.Add(string.Format("商品最晚於購買成功{0}個工作日內出貨完畢，出貨後配送約2~3個工作日，恕無法指定送達時間(限台灣本島配送)", pro.ShipText3));
                        }

                        bList.Add("供應商應於出貨後1天內，登入本平台商家系統回報出貨狀況並回填出貨單號。");
                        bList.Add("若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受合約第六條第3項第甲款賠償金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。");
                    }
                }

                bList.Add("週休假日或例假日成立之訂單，均視為次一工作天之訂單。");


                if (pro.TrialPeriod == null)
                {
                    bList.Add("鑑賞期：消費者收到貨品後七天內(含六日)。");
                }
                else if (!(bool)pro.TrialPeriod)
                {
                    bList.Add("鑑賞期：消費者收到貨品後七天內(含六日)。");
                }
                else if ((bool)pro.TrialPeriod)
                {
                    bList.Add("鑑賞期：此商品依現行法令規範，排除適用於七日鑑賞期。");
                }

                bList.Add("商品退貨：於鑑賞期內完成退貨申請，鑑賞期後除商品瑕疵外，不再接受消費者退貨。");

                for (int i = 0; i < bList.Count; i++)
                {
                    businessOrderUseTimeSet += "(" + (i + 1) + ").　" + bList[i] + newLine;
                }
                dataList.Add("BusinessOrderUseTimeSet", businessOrderUseTimeSet);
                #endregion
                dataList.Add("ProductGuaranteeClauseDesc", I18N.Message.SalesBusinessOrderProductGuaranteeClauseDesc);////現在也沒再用，旅遊宅配的商品保證 看之後要不要直接讀


            }
            #endregion

            #region 其他附註說明
            dataList.Add("OthersInfo", "無");//其他
            dataList.Add("OtherMemo", pro.ContractMemo);//附註說明 
            #endregion


            return dataList;
        }



        /// <summary>
        /// 提案單同步到後台
        /// </summary>
        /// <param name="pro">提案單</param>
        public static void SyncProposalHouse(Proposal pro, string userName)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder proBuilder = new StringBuilder();
            proBuilder.AppendLine("開始同步後台檔次");
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[開始同步後台檔次]" + pro.Id);
            try
            {
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.YearContract))
                {
                    pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SpecialFlag, ProposalSpecialFlag.YearContract));          //長期上架，會影響出貨日期
                    sp.ProposalSet(pro);
                }
                if (pro.BusinessHourGuid.HasValue)
                {
                    Guid mainBid = pro.BusinessHourGuid ?? Guid.Empty;
                    if (mainBid == Guid.Empty)
                    {
                        return;
                    }
                    //已建檔才需要同步
                    /*
                     * 1. 單檔變母子檔
                     * 2. 母子檔變單檔
                     * 3. 子檔數量改變
                     * */
                    ComboDealCollection cdc = pp.GetComboDealByBid(mainBid, false);
                    BusinessHour mainBh = pp.BusinessHourGet(mainBid);
                    GroupOrder mainGOrder = op.GroupOrderGetByBid(mainBid);

                    if (Helper.IsFlagSet(mainGOrder.Status.GetValueOrDefault(0), GroupOrderStatus.Completed) || mainGOrder.Slug != null)
                    {
                        //已結檔，不須同步
                        return;
                    }

                    if (!mainGOrder.IsLoaded)
                    {
                        OrderFacade.MakeGroupOrder(config.ServiceEmail, config.ServiceName, mainBid, DateTime.Now, DateTime.MaxValue.Date);
                    }
                    ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);
                    if (cdc.Count == 0)
                    {
                        if (multiDeals.Count() > 1)
                        {
                            #region 單檔變母子檔
                            if (mainBh.BusinessHourOrderTimeS <= DateTime.Now && mainBh.BusinessHourOrderTimeE >= DateTime.Now)
                            {
                                //已開檔，單檔不能再變母子檔了，因為Bid會變
                                return;
                            }
                            ComboDeal mainCdc = pp.GetComboDeal(mainBid);
                            if (mainCdc.IsLoaded)
                            {
                                //如果已經有了，把他刪除，因為下面會再新增一次
                                pp.DeleteComboDeal(mainCdc.Id, mainCdc.BusinessHourGuid);

                            }
                            var sortbyMultiDeals = multiDeals.OrderBy(x => x.Sort);
                            foreach (ProposalMultiDeal sub in sortbyMultiDeals)
                            {
                                string msg;
                                Guid newGuid;
                                PponFacade.CopyDeal(pro.BusinessHourGuid.Value, true, false, out msg, out newGuid, userName);
                                if (newGuid != Guid.Empty)
                                {
                                    PponFacade.PponDealUpdateByHouseroposal(newGuid, userName, pro, sub);
                                }
                                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[建檔時須補回Bid]");
                                //建檔時須補回bid
                                ProposalMultiDeal pmd = ProposalFacade.ProposalMultiDealGetById(sub.Id);
                                pmd.Bid = newGuid;
                                sp.ProposalMultiDealsSet(pmd);
                            }
                            #endregion 單檔變母子檔
                        }
                        else if (multiDeals.Count() == 1)
                        {
                            //單檔全部變為母子檔
                            string errorMessages;
                            Guid returnNewGuid;
                            PponFacade.CopyDeal(mainBid, false, false, out errorMessages, out returnNewGuid, userName);
                            Item itemSub = pp.ItemGetByBid(mainBid);

                            #region ComboDeal
                            ComboDeal cdMain = new ComboDeal()
                            {
                                MainBusinessHourGuid = returnNewGuid,
                                BusinessHourGuid = returnNewGuid,
                                Title = "主檔",
                                Sequence = -1,
                                Creator = userName,
                                CreateTime = DateTime.Now
                            };
                            pp.AddComboDeal(cdMain);

                            ComboDeal cdSub = new ComboDeal()
                            {
                                MainBusinessHourGuid = returnNewGuid,
                                BusinessHourGuid = mainBid,
                                Title = itemSub.ItemName,
                                Sequence = 1,
                                Creator = userName,
                                CreateTime = DateTime.Now
                            };
                            pp.AddComboDeal(cdSub);
                            #endregion ComboDeal

                            #region BusinessHour
                            BusinessHour bhMain = pp.BusinessHourGet(returnNewGuid);
                            bhMain.BusinessHourStatus = Convert.ToInt32(Helper.SetFlag(true, bhMain.BusinessHourStatus, BusinessHourStatus.ComboDealMain));
                            pp.BusinessHourSet(bhMain);

                            BusinessHour bhSub = pp.BusinessHourGet(mainBid);
                            bhSub.BusinessHourStatus = Convert.ToInt32(Helper.SetFlag(false, bhSub.BusinessHourStatus, BusinessHourStatus.ComboDealMain));
                            bhSub.BusinessHourStatus = Convert.ToInt32(Helper.SetFlag(true, bhSub.BusinessHourStatus, BusinessHourStatus.ComboDealSub));
                            pp.BusinessHourSet(bhSub);
                            #endregion BusinessHour

                            #region DealCost                            
                            pp.DealCostDeleteListByBid(bhMain.Guid);
                            DealCostCollection dcc = new DealCostCollection();
                            dcc.Add(new DealCost() { BusinessHourGuid = bhMain.Guid, Cost = 1, Quantity = 99999, CumulativeQuantity = 99999, LowerCumulativeQuantity = 0 });
                            pp.DealCostSetList(dcc);
                            #endregion DealCost

                            #region ProposalMultiOptionSpec
                            //把規格改成新的母檔bid，不然庫存會扣不到
                            ProposalMultiOptionSpecCollection specList = sp.ProposalMultiOptionSpecGetByBid(mainBid);
                            foreach (ProposalMultiOptionSpec spec in specList)
                            {
                                spec.BusinessHourGuid = bhMain.Guid;
                                sp.ProposalMultiOptionSpecSet(spec);
                            }
                            #endregion ProposalMultiOptionSpec

                            #region Proposal
                            pro.BusinessHourGuid = bhMain.Guid;
                            mainBid = pro.BusinessHourGuid.Value;
                            sp.ProposalSet(pro);
                            #endregion Proposal
                        }
                    }
                    else if (cdc.Count > 0)
                    {
                        DateTime dateStart = DateTime.Now;
                        for (int ti = 1; ti <= 5; ti++)
                        {
                            DateTime tmpDate = dateStart.AddMinutes(0 - ti);
                            if ((tmpDate.Minute % 5) == 0)
                            {
                                dateStart = DateTime.Parse(tmpDate.ToString("yyyy/MM/dd HH:mm:ss"));
                                break;
                            }
                        }

                        //if (multiDeals.Count() == 1)
                        //{
                        //if(cdc.Count() > 0)
                        //{
                        #region 母子檔變單檔
                        //以後沒單檔了，故此段程式刪除

                        //if (mainBh.BusinessHourOrderTimeS <= DateTime.Now && mainBh.BusinessHourOrderTimeE >= DateTime.Now)
                        //{
                        //    //已開檔，母子檔不能再變單檔了，因為有可能已產生訂單
                        //    return;
                        //}
                        //ViewComboDealCollection vcdc = pp.GetViewComboDealAllByBid(mainBid);

                        //foreach (ViewComboDeal cd in vcdc)
                        //{
                        //    BusinessHour bh = pp.BusinessHourGet(cd.BusinessHourGuid);

                        //    if (cd.MainBusinessHourGuid != cd.BusinessHourGuid)
                        //    {
                        //        if (bh.BusinessHourOrderTimeS >= DateTime.Now)
                        //        {
                        //            bh.BusinessHourOrderTimeS = DateTime.MaxValue.Date;
                        //            bh.BusinessHourOrderTimeE = DateTime.MaxValue.Date;
                        //        }
                        //        else
                        //        {
                        //            if (bh.BusinessHourOrderTimeS < dateStart)
                        //            {
                        //                bh.BusinessHourOrderTimeE = dateStart;
                        //            }
                        //            else
                        //            {
                        //                bh.BusinessHourOrderTimeE = bh.BusinessHourOrderTimeS;
                        //            }
                        //        }
                        //        bh.BusinessHourStatus = bh.BusinessHourStatus & ~(int)BusinessHourStatus.ComboDealSub;
                        //    }
                        //    else
                        //    {
                        //        bh.BusinessHourStatus = bh.BusinessHourStatus & ~(int)BusinessHourStatus.ComboDealMain;
                        //    }
                        //    pp.BusinessHourSet(bh);

                        //    //#region deal_cost
                        //    //decimal cost = model.Cost;
                        //    //if (cost > 0)
                        //    //{
                        //    //    bool multiflag = false;
                        //    //    if (multiDeals.Count > 1)
                        //    //    {
                        //    //        //多檔次
                        //    //        multiflag = true;
                        //    //    }
                        //    //    DealCostCollection dcc = new DealCostCollection();
                        //    //    int maxQty = Convert.ToInt32(orderTotoalLimit);
                        //    //    if (slottingFeeQuantity > 0)
                        //    //    {
                        //    //        //母檔進貨價應預設為1
                        //    //        //dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = 0, Quantity = slottingFeeQuantity, CumulativeQuantity = slottingFeeQuantity, LowerCumulativeQuantity = 0 });
                        //    //        //dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = cost, Quantity = maxQty - slottingFeeQuantity, CumulativeQuantity = maxQty, LowerCumulativeQuantity = slottingFeeQuantity });
                        //    //        dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = (multiflag ? 1 : cost), Quantity = slottingFeeQuantity, CumulativeQuantity = slottingFeeQuantity, LowerCumulativeQuantity = 0 });
                        //    //        dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = (multiflag ? 1 : cost), Quantity = maxQty - slottingFeeQuantity, CumulativeQuantity = maxQty, LowerCumulativeQuantity = slottingFeeQuantity });
                        //    //    }
                        //    //    else
                        //    //    {
                        //    //        //母檔進貨價應預設為1
                        //    //        dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = (multiflag ? 1 : cost), Quantity = maxQty, CumulativeQuantity = maxQty, LowerCumulativeQuantity = 0 });
                        //    //    }
                        //    //    pp.DealCostSetList(dcc);
                        //    //}
                        //    //#endregion deal_cost

                        //    pp.DeleteComboDeal(cd.Id, cd.BusinessHourGuid);
                        //}

                        #endregion 母子檔變單檔
                        //}

                        //}
                        //else
                        //{
                        #region 子檔數量變化
                        List<Guid> comboBids = cdc.Select(x => x.BusinessHourGuid).ToList();
                        var sortbyMultiDeals = multiDeals.OrderBy(x => x.Sort);
                        foreach (ProposalMultiDeal sub in sortbyMultiDeals)
                        {
                            if (sub.Bid.HasValue)
                            {
                                if (comboBids.Contains(sub.Bid.Value))
                                {
                                    continue;
                                }
                            }
                            string msg;
                            Guid newGuid;
                            PponFacade.CopyDeal(pro.BusinessHourGuid.Value, true, false, out msg, out newGuid, userName);
                            if (newGuid != Guid.Empty)
                            {
                                PponFacade.PponDealUpdateByHouseroposal(newGuid, userName, pro, sub);
                            }
                            //建檔時須補回bid
                            ProposalMultiDeal pmd = ProposalFacade.ProposalMultiDealGetById(sub.Id);
                            pmd.Bid = newGuid;
                            sp.ProposalMultiDealsSet(pmd);
                            //已提案單的出帳方式為主
                            DealAccounting da = pp.DealAccountingGet(newGuid);
                            da.RemittanceType = pro.RemittanceType;
                            pp.DealAccountingSet(da);
                        }

                        foreach (ComboDeal cd in cdc)
                        {
                            if (sortbyMultiDeals.Where(x => x.Bid == cd.BusinessHourGuid).FirstOrDefault() != null)
                            {
                                continue;
                            }
                            else
                            {
                                GroupOrder gpo = op.GroupOrderGetByBid(cd.BusinessHourGuid);
                                if (Helper.IsFlagSet(gpo.Status.GetValueOrDefault(), GroupOrderStatus.Completed)
                                    || gpo.Slug != null)
                                {
                                    //已結檔就跳過
                                    continue;
                                }
                                PponDealClose _dealClose = new PponDealClose();
                                BusinessHour bh = pp.BusinessHourGet(cd.BusinessHourGuid);
                                if (bh.IsLoaded)
                                {
                                    DealProperty sdp = pp.DealPropertyGet(cd.BusinessHourGuid);
                                    bool dealOn = (bh.BusinessHourOrderTimeS <= DateTime.Now && bh.BusinessHourOrderTimeE >= DateTime.Now);

                                    if (bh.BusinessHourOrderTimeS >= DateTime.Now)
                                    {
                                        //尚未開賣
                                        bh.BusinessHourOrderTimeS = DateTime.MaxValue.Date;
                                        bh.BusinessHourOrderTimeE = DateTime.MaxValue.Date;
                                    }
                                    else
                                    {
                                        //已開賣
                                        if (bh.BusinessHourOrderTimeS < dateStart)
                                        {
                                            bh.BusinessHourOrderTimeE = dateStart;
                                        }
                                        else
                                        {
                                            bh.BusinessHourOrderTimeE = bh.BusinessHourOrderTimeS;
                                        }
                                    }

                                    if (!dealOn)
                                    {
                                        bh.BusinessHourStatus = bh.BusinessHourStatus & ~(int)BusinessHourStatus.ComboDealSub;
                                    }
                                    else
                                    {
                                        //最大購買數要更新為0，否則前台不會判斷到已結檔的檔次，母檔會不顯示已售完
                                        bh.OrderTotalLimit = 0;
                                    }
                                    pp.BusinessHourSet(bh);

                                    List<ViewPponDeal> vpdList = new List<ViewPponDeal>();
                                    vpdList.Add(pp.ViewPponDealGetByBusinessHourGuid(bh.Guid));
                                    List<DealCloseResult> results = _dealClose.CloseDeal(vpdList, "System");

                                    if (!dealOn)
                                    {
                                        //尚未上檔的，把子檔跟母檔的關係分離
                                        //已上檔的，直接結檔，不能分離了
                                        pp.DeleteComboDeal(cd.Id, cd.BusinessHourGuid);
                                    }
                                    else
                                    {
                                        sdp.IsExpiredDeal = true;
                                        pp.DealPropertySet(sdp);
                                    }
                                }
                            }
                        }
                        #endregion 子檔數量變化
                        //}

                    }
                    else
                    {

                    }


                    cdc = pp.GetComboDealByBid(mainBid, false);

                    VacationCollection recentHolidays = pp.GetVacationListByPeriod(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(14));

                    CategoryCollection ccs = sp.CategoryGetAll(Category.Columns.Code);
                    Category cc = ccs.Where(x => x.Name.Equals("超商取貨") && x.Type == (int)CategoryType.DealCategory).FirstOrDefault();

                    List<Guid> syncGuid = new List<Guid>();

                    //母子檔Bid

                    if (cdc.Count() > 0)
                    {
                        syncGuid.AddRange(cdc.Select(x => x.BusinessHourGuid));
                    }
                    syncGuid.Add(mainBid);

                    if (syncGuid.Count() == 1)
                    {
                        if (pro.BusinessHourGuid != null)
                        {
                            ProposalMultiDeal md = sp.ProposalMultiDealGetByPid(pro.Id).FirstOrDefault();
                            if (md != null && md.Bid != null)
                            {
                                if (md.Bid.Value != pro.BusinessHourGuid.Value)
                                {
                                    md.Bid = pro.BusinessHourGuid.Value;
                                    sp.ProposalMultiDealsSet(md);
                                }
                            }
                        }
                    }


                    //該檔次底下有無訂單
                    bool hasOrder = false;
                    CashTrustLogCollection ctlogs = mp.CashTrustLogGetListByBid(syncGuid);
                    if (ctlogs.Count() > 0)
                    {
                        hasOrder = true;
                    }

                    foreach (var bid in syncGuid)
                    {
                        bool isMainDeal = false;//是否為母檔
                        bool isComboDeal = false;//是否為多檔，非多檔為單檔

                        if (bid == mainBid)
                        {
                            isMainDeal = true;
                        }

                        if (syncGuid.Count() > 1)
                        {
                            isComboDeal = true;
                        }

                        BusinessHour bh = pp.BusinessHourGet(bid);
                        Item item = ip.ItemGetByBid(bid);
                        ComboDeal cd = pp.GetComboDeal(bid);
                        DealProperty dp = pp.DealPropertyGet(bid);
                        DealAccounting da = pp.DealAccountingGet(bid);
                        DealPayment dpm = pp.DealPaymentGet(bid);
                        GroupOrder gOrder = op.GroupOrderGetByBid(bid);
                        CouponEventContent cec = pp.CouponEventContentGetByBid(bid);
                        int proposalSpecialFlag = pro.SpecialFlag;
                        ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pro.Id);
                        ProposalMultiDeal model = multiDeals.OrderBy(x => x.ItemPrice).FirstOrDefault();
                        //優惠內容
                        ProposalMultiDeal pmd = sp.ProposalMultiDealGetByBid(bid);

                        if (Helper.IsFlagSet(gOrder.Status.GetValueOrDefault(0), GroupOrderStatus.Completed) || gOrder.Slug != null)
                        {
                            //已結檔，不須同步
                            continue;
                        }

                        //上檔頻道
                        CategoryDealCollection category = new CategoryDealCollection();
                        ProposalCategoryDealCollection pcdc = sp.ProposalCategoryDealsGetList(pro.Id);
                        foreach (ProposalCategoryDeal p in pcdc)
                        {
                            category.Add(new CategoryDeal()
                            {
                                Bid = bh.Guid,
                                Cid = p.Cid
                            });
                        }

                        #region business_hour
                        //ATM
                        bh.BusinessHourAtmMaximum = pro.NoAtm ? 0 : 19999;
                        #endregion business_hour

                        //只有更新母檔/單檔
                        if (isMainDeal || !isComboDeal)
                        {

                            CategoryDealCollection cdcs = pp.CategoryDealsGetList(bh.Guid);
                            Dictionary<int, List<int>> selectedChannelCategories = new Dictionary<int, List<int>>();
                            foreach (CategoryDeal cdi in cdcs)
                            {
                                if (!selectedChannelCategories.ContainsKey(cdi.Cid))
                                {
                                    selectedChannelCategories.Add(cdi.Cid, new List<int>());
                                }
                            }

                            List<int> selectedCity = new List<int>();
                            foreach (var selectedChannel in selectedChannelCategories)
                            {
                                PponCity city = PponCityGroup.GetPponCityByCategoryId(selectedChannel.Key);
                                if (city != null)
                                {
                                    selectedCity.Add(city.CityId);
                                }
                            }


                            #region Item
                            if (isMainDeal || !hasOrder)
                            {
                                item.ItemOrigPrice = model.OrigPrice;//售價 //會影響分期
                                item.ItemPrice = model.ItemPrice;//進貨價、上架費 //會影響分期
                                if (isMainDeal)
                                {
                                    //母檔的售價為均價最低的金額
                                    decimal min_oriPrice = 0;
                                    decimal min_itemPrice = 0;
                                    decimal avg = 999999;
                                    foreach (var m in multiDeals)
                                    {
                                        decimal d = Math.Ceiling(m.ItemPrice / decimal.Parse(m.QuantityMultiplier));
                                        if (d == avg)
                                        {
                                            if (min_itemPrice != 0)
                                            {
                                                //若均價一樣，取原價大的，這樣折數比較小
                                                decimal newOri = m.ItemPrice / m.OrigPrice;
                                                decimal oldOri = min_itemPrice / min_oriPrice;
                                                if (newOri < oldOri)
                                                {
                                                    avg = d;
                                                    min_oriPrice = Math.Ceiling(m.OrigPrice / decimal.Parse(m.QuantityMultiplier));
                                                    min_itemPrice = Math.Ceiling(m.ItemPrice / decimal.Parse(m.QuantityMultiplier));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (d < avg)
                                            {
                                                avg = d;
                                                min_oriPrice = Math.Ceiling(m.OrigPrice / decimal.Parse(m.QuantityMultiplier));
                                                min_itemPrice = Math.Ceiling(m.ItemPrice / decimal.Parse(m.QuantityMultiplier));
                                            }
                                        }
                                    }
                                    item.ItemOrigPrice = min_oriPrice;
                                    item.ItemPrice = min_itemPrice;
                                }

                            }
                            //訂單短標
                            //item.ItemName = (string.IsNullOrEmpty(model.BrandName) ? string.Empty : "【" + model.BrandName + "】") + model.ItemName;
                            item.ItemName = (string.IsNullOrEmpty(pro.BrandName) ? string.Empty : "【" + pro.BrandName + "】") + pro.DealName;
                            #endregion Item

                            #region CouponEventContent
                            //黑標
                            //if (cec.IsHideContent != null && cec.IsHideContent == false)
                            //{
                            //    //新版宅配提案單應該不會進來
                            //    int deliveryType = 0;
                            //    int itemOrigPrice = 0;
                            //    int itemPrice = 0;
                            //    int.TryParse(dp.DeliveryType.ToString(), out deliveryType);
                            //    int.TryParse(item.ItemOrigPrice.ToString(), out itemOrigPrice);
                            //    int.TryParse(item.ItemPrice.ToString(), out itemPrice);
                            //    bool labelTag = dp.LabelTagList.Contains(((int)DealLabelSystemCode.IncludedDeliveryCharge).ToString());
                            //    string ContentName = PponFacade.PponContentGet(bid, deliveryType, cec.Intro, cec.PriceDesc, item.ItemName, itemOrigPrice, itemPrice, dp.IsAveragePrice, labelTag);

                            //    string tbN = cec.Name.Replace("（", "").Replace("）", "").Replace("﹝", "").Replace("﹞", ""); //內文
                            //    string content = (ContentName + (tbN != "" ? "" + tbN : "。")).Replace("\\", "\\\\").Replace("'", "\\u0027"); //兩段加起來
                            //    cec.ContentName = content;
                            //    cec.ProductSpec = pcec.ProductSpec;
                            //    cec.AppDescription = GetAppDescription(pcec, bid);
                            //    cec.AppDealPic = ImageFacade.GetMediaPathsFromRawData(
                            //                        pcec.AppDealPic, MediaType.PponDealPhoto).FirstOrDefault();
                            //    cec.AppTitle = string.Empty;
                            //}
                            //else
                            //{
                            string eventName = ProposalFacade.GetProEventName(pro, multiDeals.ToList());
                            string deal_content = (string.IsNullOrEmpty(model.BrandName) ? string.Empty : "【" + model.BrandName + "】") + model.ItemName;
                            if (eventName.Length > 400)
                            {
                                eventName = eventName.Substring(0, 390);
                            }
                            cec.Name = eventName;//黑標
                            cec.Title = pcec.Title;//橘標
                            cec.Availability = "[]";
                            cec.CouponUsage = deal_content;
                            cec.Restrictions = pcec.Restrictions;
                            cec.Introduction = pcec.Restrictions;
                            cec.Description = pcec.Description;
                            cec.ProductSpec = pcec.ProductSpec.Replace(@"alt=""""", "");

                            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);
                            if (specialText != null && specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.InspectRule))
                            {
                                cec.ProductSpec += "\n" + specialText[(ProposalSpecialFlag)ProposalSpecialFlag.InspectRule];
                            }

                            cec.Remark = (0 + "||" + string.Empty + "||" + HttpUtility.HtmlDecode(pcec.Remark) + "||右邊##||##||##||##||##||##");
                            cec.ImagePath = pcec.ImagePath;
                            cec.OriImagePath = pcec.OriImagePath;
                            cec.AppDealPic = pcec.AppDealPic;
                            cec.AppDescription = ViewPponDealManager.RemoveSpecialHtmlForApp(pcec.Description);
                            cec.RemoveBgPic = pcec.RemoveBgPic;
                            //cec.AppDealPic = ImageFacade.GetMediaPathsFromRawData(
                            //                    pcec.AppDealPic, MediaType.PponDealPhoto).FirstOrDefault();
                            cec.AppTitle = pcec.AppTitle;
                            //}
                            cec.IsHideContent = true;
                            #endregion CouponEventContent

                            #region deal_propoerty
                            //續接數量檔次
                            dp.AncestorBusinessHourGuid = pro.AncestorBusinessHourGuid;

                            Guid ancBid;
                            Guid.TryParse(pro.AncestorBusinessHourGuid.ToString(), out ancBid);
                            //ViewPponDeal ancDeal = pp.ViewPponDealGetByBusinessHourGuid(ancBid);         //接續數量

                            int ancQuantity = 0;
                            if (!Guid.Equals(Guid.Empty, ancBid))
                            {
                                IViewPponDeal ancDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(pro.AncestorBusinessHourGuid.Value);
                                ancQuantity = PponFacade.GetPponDealContinuedQuantity(ancBid, Helper.IsFlagSet(ancDeal.BusinessHourStatus, BusinessHourStatus.ComboDealMain));
                            }



                            if (!Guid.Equals(Guid.Empty, ancBid) && ancQuantity != -1)
                            {
                                dp.AncestorBusinessHourGuid = ancBid;
                                dp.IsContinuedQuantity = true;
                                dp.ContinuedQuantity = ancQuantity;
                            }
                            else
                            {
                                dp.AncestorBusinessHourGuid = null;
                                dp.IsContinuedQuantity = false;
                                dp.ContinuedQuantity = 0;
                            }

                            dp.CityList = new JsonSerializer().Serialize(selectedCity);
                            dp.CategoryList = new JsonSerializer().Serialize(category.Select(t => t.Cid).Distinct().ToList());

                            int quantityMultiplier = 0;
                            int.TryParse(pmd.QuantityMultiplier, out quantityMultiplier);
                            if (quantityMultiplier == 0)
                            {
                                quantityMultiplier = 1;
                            }
                            dp.QuantityMultiplier = quantityMultiplier;
                            dp.DealAccBusinessGroupId = 7;//館別預設塞宅配
                            bool includedDeliveryCharge = true;
                            foreach (var m in multiDeals)
                            {
                                if (m.Freights > 0)
                                {
                                    includedDeliveryCharge = false;
                                    break;
                                }
                            }
                            //免運
                            if (includedDeliveryCharge)
                            {
                                List<int> tagList = new List<int>();
                                if (dp.LabelTagList != null)
                                {
                                    string[] labelTagList = dp.LabelTagList.Split(",");
                                    foreach (string tag in labelTagList)
                                    {
                                        int t = 0;
                                        int.TryParse(tag, out t);
                                        if (!tagList.Contains(t))
                                        {
                                            tagList.Add(t);
                                        }
                                    }
                                }
                                if (!tagList.Contains((int)DealLabelSystemCode.IncludedDeliveryCharge))
                                {
                                    tagList.Add((int)DealLabelSystemCode.IncludedDeliveryCharge);
                                }
                                dp.LabelTagList = string.Join(",", tagList);
                            }

                            #endregion deal_propoerty

                            //不可使用折價券
                            if (Helper.IsFlagSet(proposalSpecialFlag, ProposalSpecialFlag.NotAllowedDiscount))
                            {
                                PromotionFacade.DiscountLimitSet(bid, userName, DiscountLimitType.Enabled);
                            }
                            else
                            {
                                PromotionFacade.DiscountLimitSet(bid, userName, DiscountLimitType.Disabled);
                            }
                            #region DealProductDelivery
                            int boxQuantityLimit = 0;
                            int.TryParse(pmd.BoxQuantityLimit, out boxQuantityLimit);
                            PponFacade.SetDealProductDelivery(bh.SellerGuid, bh.Guid, boxQuantityLimit);
                            #endregion DealProductDelivery

                            #region deal_cost
                            if (!hasOrder)
                            {
                                if (isMainDeal)
                                {
                                    ProposalMultiDeal mpmd = sp.ProposalMultiDealGetByPid(pro.Id).OrderBy(x => x.ItemPrice).FirstOrDefault();
                                    if (mpmd != null)
                                    {
                                        DealCostCollection dcc = new DealCostCollection();
                                        pp.DealCostDeleteListByBid(bid);
                                        if (mpmd.Cost > 0)
                                        {
                                            int maxCount = Convert.ToInt32(mpmd.OrderTotalLimit);
                                            if (mpmd.SlottingFeeQuantity > 0)
                                            {
                                                dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = (isComboDeal ? 1 : mpmd.Cost), Quantity = mpmd.SlottingFeeQuantity, CumulativeQuantity = mpmd.SlottingFeeQuantity, LowerCumulativeQuantity = 0 });
                                                dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = (isComboDeal ? 1 : mpmd.Cost), Quantity = maxCount - mpmd.SlottingFeeQuantity, CumulativeQuantity = maxCount, LowerCumulativeQuantity = mpmd.SlottingFeeQuantity });
                                            }
                                            else
                                            {
                                                dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = (isComboDeal ? 1 : mpmd.Cost), Quantity = maxCount, CumulativeQuantity = maxCount, LowerCumulativeQuantity = 0 });
                                            }
                                        }
                                        pp.DealCostSetList(dcc);
                                    }
                                }
                            }
                            #endregion deal_cost

                            #region sms_content
                            SmsContent baseSms = pp.SMSContentGet(bh.Guid);
                            if (baseSms != null && baseSms.IsLoaded)
                            {
                                string[] contents = baseSms.Content.Split('|');
                                if (contents.Length >= 2)
                                {
                                    baseSms.Content = string.Format("{0}|{1} |{2}", contents[0], (string.IsNullOrEmpty(pro.BrandName) ? string.Empty : "【" + pro.BrandName + "】") + pro.DealName, contents[2]);
                                }
                                else
                                {
                                    baseSms.Content = "17Life:|" + (string.IsNullOrEmpty(pro.BrandName) ? string.Empty : "【" + pro.BrandName + "】") + pro.DealName + " |，{0}-{1}，期限{2}-{3}，App載憑證：http://x.co/4r1JG";
                                }

                                pp.SMSContentSet(baseSms);
                            }
                            #endregion sms_content

                            #region 超取
                            if (isComboDeal)
                            {
                                int boxQuantityLimitMain = 0;
                                foreach (ProposalMultiDeal p in multiDeals)
                                {
                                    int b = 0;
                                    int.TryParse(p.BoxQuantityLimit, out b);
                                    if (b > 0)
                                    {
                                        boxQuantityLimitMain = b;
                                    }
                                }
                                if (boxQuantityLimitMain > 0)
                                {
                                    if (cc != null)
                                    {
                                        if (category.Where(t => t.Cid == cc.Id).FirstOrDefault() == null)
                                        {
                                            category.Add(new CategoryDeal()
                                            {
                                                Bid = bh.Guid,
                                                Cid = cc.Id
                                            });
                                        }
                                    }
                                }
                                else if (boxQuantityLimit == 0)
                                {
                                    if (cc != null)
                                    {
                                        //var c = category.Where(t => t.Cid == cc.Id).FirstOrDefault();
                                        //if (c != null)
                                        //{
                                        //    category.Remove(c);
                                        //}
                                        CategoryDealCollection tmp = category.Clone();
                                        category.Clear();
                                        foreach (CategoryDeal c in tmp)
                                        {
                                            if (c.Cid != cc.Id)
                                            {
                                                category.Add(c);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion 超取

                            #region 策展
                            if (!string.IsNullOrEmpty(pro.MarketingResource) && pro.FlowCompleteTimes == 0)
                            {
                                if (pro.BusinessHourGuid != null)
                                {
                                    SaveBrandItem(pro.MarketingResource, pro.BusinessHourGuid.Value, userName);
                                }
                            }
                            #endregion 策展
                        }


                        //只更新子檔/單檔
                        if (!isMainDeal || !isComboDeal)
                        {
                            #region Item
                            if (!hasOrder)
                            {
                                //原價
                                item.ItemOrigPrice = pmd.OrigPrice;//會影響黑標
                                                                   //售價 //會影響分期
                                item.ItemPrice = pmd.ItemPrice;//會影響黑標
                                                               //進貨價、上架費 //會影響分期  
                            }
                            item.ItemName = GetProposalCouponUsage(pmd);
                            #endregion Item

                            #region combo_deals
                            if (cd != null && cd.IsLoaded)
                            {
                                cd.Title = GetProposalCouponUsage(pmd);
                                cd.Sequence = pmd.Sort;
                                pp.AddComboDeal(cd);
                            }
                            #endregion combo_deals

                            #region deal_cost
                            if (!hasOrder)
                            {
                                DealCostCollection dcc = new DealCostCollection();
                                pp.DealCostDeleteListByBid(bid);
                                if (pmd.Cost > 0)
                                {
                                    int maxCount = Convert.ToInt32(pmd.OrderTotalLimit);
                                    if (pmd.SlottingFeeQuantity > 0)
                                    {
                                        dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = 0, Quantity = pmd.SlottingFeeQuantity, CumulativeQuantity = pmd.SlottingFeeQuantity, LowerCumulativeQuantity = 0 });
                                        dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = pmd.Cost, Quantity = maxCount - pmd.SlottingFeeQuantity, CumulativeQuantity = maxCount, LowerCumulativeQuantity = pmd.SlottingFeeQuantity });
                                    }
                                    else
                                    {
                                        dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = pmd.Cost, Quantity = maxCount, CumulativeQuantity = maxCount, LowerCumulativeQuantity = 0 });
                                    }
                                }
                                pp.DealCostSetList(dcc);
                            }
                            #endregion deal_cost

                            #region CouponEventContent
                            string eventName = ProposalFacade.GetProEventNameByComboDeal(pro, pmd);
                            cec.Name = eventName;   //黑標
                            cec.CouponUsage = ProposalFacade.GetProposalCouponUsage(pmd);
                            #endregion CouponEventContent

                            #region business_hour
                            //最大購買數
                            bh.OrderTotalLimit = pmd.OrderTotalLimit;
                            #endregion business_hour

                            #region sms_content
                            SmsContent baseSms = pp.SMSContentGet(bh.Guid);
                            if (baseSms != null && baseSms.IsLoaded)
                            {
                                string[] contents = baseSms.Content.Split('|');
                                if (contents.Length >= 2)
                                {
                                    baseSms.Content = string.Format("{0}|{1} |{2}", contents[0], GetProposalCouponUsage(pmd), contents[2]);
                                }
                                else
                                {
                                    baseSms.Content = "17Life:|" + GetProposalCouponUsage(pmd) + " |，{0}-{1}，期限{2}-{3}，App載憑證：http://x.co/4r1JG";
                                }

                                pp.SMSContentSet(baseSms);
                            }
                            #endregion sms_content

                            // 運費、免運門檻//憑證應該不用
                            CouponFreightCollection cfc = new CouponFreightCollection();
                            pp.CouponFreightDeleteListByBid(bid);
                            if (pmd.Freights > 0)
                            {
                                if (pmd.NoFreightLimit > 0)
                                {
                                    cfc.Add(new CouponFreight() { BusinessHourGuid = bh.Guid, StartAmount = pmd.ItemPrice * pmd.NoFreightLimit, EndAmount = 9999999, FreightAmount = 0, CreateId = userName, CreateTime = DateTime.Now });
                                    cfc.Add(new CouponFreight() { BusinessHourGuid = bid, StartAmount = 0, EndAmount = pmd.ItemPrice * pmd.NoFreightLimit, FreightAmount = pmd.Freights, CreateId = userName, CreateTime = DateTime.Now });
                                }
                                else
                                {
                                    cfc.Add(new CouponFreight() { BusinessHourGuid = bid, StartAmount = 0, EndAmount = 9999999, FreightAmount = pmd.Freights, CreateId = userName, CreateTime = DateTime.Now });
                                }
                            }
                            pp.CouponFreightSetList(cfc);


                            //成套組數
                            dp.SaleMultipleBase = pmd.GroupCouponBuy + pmd.GroupCouponPresent;
                            dp.PresentQuantity = pmd.GroupCouponPresent;
                            int comboPackCount = 0;
                            int.TryParse(pmd.ComboPackCount, out comboPackCount);
                            dp.ComboPackCount = (comboPackCount == 0 ? 1 : comboPackCount);

                            int quantityMultiplier = 0;
                            int.TryParse(pmd.QuantityMultiplier, out quantityMultiplier);
                            if (quantityMultiplier == 0)
                            {
                                quantityMultiplier = 1;
                            }
                            dp.QuantityMultiplier = quantityMultiplier;
                            dp.DealAccBusinessGroupId = 7;//館別預設塞宅配
                                                          //免運
                            bool includedDeliveryCharge = (model.Freights == 0);
                            if (includedDeliveryCharge)
                            {
                                List<int> tagList = new List<int>();
                                if (dp.LabelTagList != null)
                                {
                                    string[] labelTagList = dp.LabelTagList.Split(",");
                                    foreach (string tag in labelTagList)
                                    {
                                        int t = 0;
                                        int.TryParse(tag, out t);
                                        if (!tagList.Contains(t))
                                        {
                                            tagList.Add(t);
                                        }
                                    }
                                }
                                if (!tagList.Contains((int)DealLabelSystemCode.IncludedDeliveryCharge))
                                {
                                    tagList.Add((int)DealLabelSystemCode.IncludedDeliveryCharge);
                                }
                                dp.LabelTagList = string.Join(",", tagList);
                            }
                            //多重選項
                            //if (!string.IsNullOrEmpty(pmd.Options))
                            //{

                            //    PponFacade.ProcessOptions(pmd.Options, bid, pp.DealPropertyGet(bid).UniqueId, userName);
                            //}

                            //每人限購
                            item.MaxItemCount = Convert.ToInt32(pmd.OrderMaxPersonal);
                            item.ItemDefaultDailyAmount = Convert.ToInt32(pmd.OrderMaxPersonal);

                            #region DealProductDelivery
                            int boxQuantityLimit = 0;
                            int.TryParse(pmd.BoxQuantityLimit, out boxQuantityLimit);
                            PponFacade.SetDealProductDelivery(bh.SellerGuid, bh.Guid, boxQuantityLimit);
                            #endregion DealProductDelivery

                            #region 判斷是否為多重選項
                            bool isMulti = IsProposalMultiSpec(pmd.Options);
                            if (isMulti)
                            {
                                dp.ShoppingCart = true;//有填寫多重選項的時候，要勾選購物車
                                ProcessMultiSpecToPponOption(pmd.Options, bid, userName);
                            }
                            else
                            {
                                //單規格
                                dp.ShoppingCart = false;
                                PponOptionCollection dbOptions = pp.PponOptionGetList(bid);
                                foreach (PponOption dbOption in dbOptions)
                                {
                                    dbOption.IsDisabled = true;
                                }
                                pp.PponOptionSetList(dbOptions);
                            }
                            dp.ShoppingCart = isMulti;
                            #endregion 判斷是否為多重選項
                        }




                        //有訂單就不可同步
                        if (!hasOrder)
                        {
                            //店家單據開立方式
                            int vendor_receipt_type = pro.VendorReceiptType == null ? (int)VendorReceiptType.Invoice : (int)pro.VendorReceiptType;
                            bool isInputTaxRequired = (vendor_receipt_type != (int)VendorReceiptType.NoTaxInvoice ? true : false);


                            da.VendorReceiptType = ((int)vendor_receipt_type == (int)VendorReceiptType.Invoice || (int)vendor_receipt_type == (int)
                                                   VendorReceiptType.NoTaxInvoice) ? (int)VendorReceiptType.Invoice : (int)vendor_receipt_type;
                            //店家單據開立方式其他說明
                            da.Message = pro.Othermessage;
                            //店家發票免稅
                            da.IsInputTaxRequired = isInputTaxRequired;




                            //成套販售//store!!!!!要再調整
                            List<int> iconIdList = new List<int>();
                            if (!string.IsNullOrEmpty(dp.LabelIconList))
                            {
                                iconIdList = dp.LabelIconList.Split(',').Select(int.Parse).ToList();
                            }

                            if (Helper.IsFlagSet(proposalSpecialFlag, ProposalSpecialFlag.GroupCoupon))
                            {
                                bh.BusinessHourStatus = (int)Helper.SetFlag(true, bh.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
                                dp.GroupCouponDealType = pro.GroupCouponDealType ?? 0;

                                if (!iconIdList.Contains((int)DealLabelSystemCode.GroupCoupon))
                                {
                                    iconIdList.Add((int)DealLabelSystemCode.GroupCoupon);
                                }

                                dp.LabelIconList = string.Join(",", iconIdList);
                            }
                            else
                            {
                                bh.BusinessHourStatus = (int)Helper.SetFlag(false, bh.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
                                dp.GroupCouponDealType = null;
                                dp.GroupCouponAppStyle = 0;

                                iconIdList.Remove((int)DealLabelSystemCode.GroupCoupon);
                                dp.LabelIconList = string.Join(",", iconIdList);
                            }



                            //通用券
                            if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.NoRestrictedStore))
                            {
                                bh.BusinessHourStatus = (int)Helper.SetFlag(true, bh.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);//通用券
                            }
                            else
                            {
                                bh.BusinessHourStatus = (int)Helper.SetFlag(false, bh.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);//通用券
                            }


                            //活動遊戲檔
                            if (pro.IsGame)
                                bh.BusinessHourStatus = (int)Helper.SetFlag(true, bh.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb);


                            //銀行合作
                            //dealtime會重塞
                            dpm.IsBankDeal = pro.IsBankDeal;


                            //墨攻
                            if ((bool)pro.Mohist.GetValueOrDefault(false))
                            {
                                bh.BusinessHourStatus = Helper.SetBusinessHourTrustProvider(bh.BusinessHourStatus, TrustProvider.Sunny);
                                da.VendorBillingModel = (int)VendorBillingModel.MohistSystem;
                                da.RemittanceType = (int)RemittanceType.Others;
                            }
                            else
                            {
                                bh.BusinessHourStatus = Helper.SetBusinessHourTrustProvider(bh.BusinessHourStatus, TrustProvider.TaiShin);
                                da.VendorBillingModel = (int)VendorBillingModel.BalanceSheetSystem;
                                //da.RemittanceType = (int)RemittanceType.AchMonthly;//???5這邊怪怪的
                                da.RemittanceType = pro.RemittanceType;
                            }

                        }
                        //以下皆可同步                  


                        #region group_order
                        gOrder.Status = (int)Helper.SetFlag(pro.NoTax, gOrder.Status ?? 0, GroupOrderStatus.NoTax);
                        #endregion group_order

                        //提案子類型
                        dp.NewDealType = pro.DealSubType;
                        //銷售分析分類
                        dp.DealType = pro.DealType2;
                        dp.DealTypeDetail = pro.DealTypeDetail;
                        //Cchannel
                        dp.Cchannel = pro.Cchannel;
                        dp.CchannelLink = pro.CchannelLink;
                        //活動遊戲
                        dp.IsGame = pro.IsGame;
                        //代銷通路
                        dp.AgentChannels = pro.AgentChannels;

                        //展演類型檔次
                        dp.IsPromotionDeal = pro.IsPromotionDeal;   //演出時間前十日內不能退貨
                        dp.IsExhibitionDeal = pro.IsExhibitionDeal; //過期不能退貨
                        dp.IsReserveLock = dp.IsPromotionDeal;
                        dp.IsHouseNewVer = true;//新提案單檔次

                        //宅配
                        dp.ShipType = pro.ShipType; //出貨類型
                        if (pro.ShipType == (int)DealShipType.Normal)
                        {
                            //一般出貨
                            dp.ProductUseDateStartSet = string.IsNullOrEmpty(pro.ShipText1) ? 0 : int.Parse(pro.ShipText1);//上檔後幾個工作日
                            dp.ProductUseDateEndSet = string.IsNullOrEmpty(pro.ShipText2) ? 0 : int.Parse(pro.ShipText2);   //統一結檔後幾個工作日
                            dp.Shippingdate = string.IsNullOrEmpty(pro.ShipText3) ? 0 : int.Parse(pro.ShipText3);           //訂單成立後幾個工作日
                            dp.ShippingdateType = pro.ShippingdateType;         //最早出貨日 or 訂單成立後
                        }
                        else if (pro.ShipType == (int)DealShipType.Ship72Hrs)
                        {
                            //快速出貨(如果是Null，SetUp頁面會Error，所以塞0)
                            dp.ProductUseDateStartSet = 0;//上檔後幾個工作日
                            dp.ProductUseDateEndSet = 0;   //統一結檔後幾個工作日
                            dp.Shippingdate = 0;           //訂單成立後幾個工作日
                            dp.ShippingdateType = 0;         //最早出貨日 or 訂單成立後
                        }
                        if (dp.IsWms)
                        {
                            //24H檔次於提案單同步後台時，品項統計方式預設設定為:多重選項個別統計
                            dp.IsMergeCount = false;
                        }

                        #region week_pay_account
                        var insStores = new PponStoreCollection();
                        insStores = pp.PponStoreGetListByBusinessHourGuid(bh.Guid);
                        var accounts = op.WeeklyPayAccountGetList(bh.Guid);
                        var stores = sp.SellerGetList(insStores.Where(t => Helper.IsFlagSet(t.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid));
                        foreach (var store in stores)
                        {
                            var account = accounts.FirstOrDefault(x => x.StoreGuid.HasValue && x.StoreGuid.Value == store.Guid)
                                        ?? new WeeklyPayAccount
                                        {
                                            BusinessHourGuid = bh.Guid,
                                            CreateDate = DateTime.Now,
                                            Creator = userName
                                        };
                            account.StoreGuid = store.Guid;
                            account.EventName = (item.ItemName.Length > 100)
                                                    ? item.ItemName.Substring(0, 100) + "..."
                                                    : item.ItemName;
                            account.CompanyName = store.CompanyName;
                            account.SellerName = store.CompanyBossName;
                            account.AccountId = store.CompanyID;
                            account.AccountName = store.CompanyAccountName;
                            account.AccountNo = store.CompanyAccount;
                            account.Email = store.CompanyEmail;
                            account.BankNo = store.CompanyBankCode;
                            account.BranchNo = store.CompanyBranchCode;
                            account.Message = store.CompanyNotice;
                            account.SignCompanyID = store.SignCompanyID;

                            accounts.Add(account);
                        }
                        op.WeeklyPayAccountSetList(accounts);

                        #endregion week_pay_account

                        if (pro.IsPromotionDeal || pro.IsExhibitionDeal)
                        {

                            if (gOrder.IsLoaded)
                            {
                                //演出時間前十日內不能退貨
                                gOrder.Status = (int)Helper.SetFlag(pro.IsPromotionDeal, gOrder.Status ?? 0, GroupOrderStatus.NoRefundBeforeDays);
                                //過期不能退貨
                                gOrder.Status = (int)Helper.SetFlag(pro.IsExhibitionDeal, gOrder.Status ?? 0, GroupOrderStatus.ExpireNoRefund);

                                op.GroupOrderSet(gOrder);
                            }
                        }


                        #region 超取
                        if (!string.IsNullOrEmpty(pmd.BoxQuantityLimit))
                        {
                            int boxQuantityLimit = 0;
                            int.TryParse(pmd.BoxQuantityLimit, out boxQuantityLimit);
                            if (boxQuantityLimit > 0)
                            {
                                if (cc != null)
                                {
                                    if (category.Where(t => t.Cid == cc.Id).FirstOrDefault() == null)
                                    {
                                        category.Add(new CategoryDeal()
                                        {
                                            Bid = bh.Guid,
                                            Cid = cc.Id
                                        });
                                    }
                                }
                            }
                            else if (boxQuantityLimit == 0)
                            {
                                if (cc != null)
                                {
                                    //var c = category.Where(t => t.Cid == cc.Id).FirstOrDefault();
                                    //if (c != null)
                                    //{
                                    //    category.Remove(c);
                                    //}

                                    CategoryDealCollection tmp = category.Clone();
                                    category.Clear();
                                    foreach (CategoryDeal c in tmp)
                                    {
                                        if (c.Cid != cc.Id)
                                        {
                                            category.Add(c);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion 超取


                        //搜尋關鍵字
                        bh.PicAlt = pro.PicAlt;

                        #region 上檔日
                        if (pro.OrderTimeS != null && pro.OrderTimeE != null)
                        {

                            if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                            {
                                if (bh.BusinessHourOrderTimeE <= DateTime.Now || gOrder.Slug != null)
                                {
                                    //已結檔不再同步時間，避免蓋到已結檔的子檔
                                }
                                else
                                {
                                    DateTime useStart = DateTime.MaxValue.Date;
                                    DateTime useEnd = DateTime.MaxValue.Date;

                                    bh.BusinessHourOrderTimeS = pro.OrderTimeS.Value;
                                    bh.BusinessHourOrderTimeE = pro.OrderTimeE.Value;

                                    GetBusinessHourDeliverTime(pro, recentHolidays, bh.BusinessHourOrderTimeS, bh.BusinessHourOrderTimeE,
                                        out useStart, out useEnd);

                                    bh.BusinessHourDeliverTimeE = useEnd;
                                    bh.BusinessHourDeliverTimeS = DateTime.Parse(useStart.ToString("yyyy/MM/dd 00:00"));
                                }
                            }
                        }
                        else
                        {
                            bh.BusinessHourOrderTimeS = DateTime.MaxValue;
                            bh.BusinessHourOrderTimeE = DateTime.MaxValue;
                        }
                        #endregion 上檔日

                        //業務
                        ViewEmployee deEmp = hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.DevelopeSalesId);
                        ViewEmployee opEmp = hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.OperationSalesId);
                        dp.DealEmpName = deEmp.EmpName;
                        dp.EmpNo = deEmp.EmpNo;
                        dp.DevelopeSalesId = deEmp.UserId;
                        da.SalesId = deEmp.EmpName;

                        if (opEmp.IsLoaded)
                        {
                            dp.OperationSalesId = opEmp.UserId;
                        }
                        if (opEmp.IsLoaded)
                        {
                            da.OperationSalesId = opEmp.EmpName;
                        }
                        da.DeptId = opEmp.DeptId;

                        //營業據點+核銷相關
                        pp.PponStoreDeleteByBid(bid);

                        PponStoreCollection pss = new PponStoreCollection();
                        ProposalStoreCollection psc = pp.ProposalStoreGetListByProposalId(pro.Id);
                        foreach (ProposalStore p in psc)
                        {
                            pss.Add(new PponStore()
                            {
                                BusinessHourGuid = bh.Guid,
                                StoreGuid = p.StoreGuid,
                                TotalQuantity = p.TotalQuantity,
                                SortOrder = p.SortOrder,
                                VbsRight = p.VbsRight,
                                ResourceGuid = Guid.NewGuid(),
                                CreateId = userName,
                                CreateTime = DateTime.Now
                            });
                        }
                        pp.PponStoreSetList(pss);

                        #region 快速出貨標籤
                        if (!string.IsNullOrEmpty(dp.LabelIconList))
                        {
                            if (category.Where(x => x.Cid == 221).FirstOrDefault() == null)
                            {
                                List<int> iconCodeIdList = dp.LabelIconList.Split(',').Select(int.Parse).ToList();
                                if (iconCodeIdList.Contains((int)DealLabelSystemCode.ArriveIn72Hrs))
                                {
                                    iconCodeIdList.Remove((int)DealLabelSystemCode.ArriveIn72Hrs);
                                    dp.LabelIconList = string.Join(",", iconCodeIdList);
                                }
                            }
                            else
                            {
                                List<int> iconCodeIdList = dp.LabelIconList.Split(',').Select(int.Parse).ToList();
                                if (!iconCodeIdList.Contains((int)DealLabelSystemCode.ArriveIn72Hrs))
                                {
                                    iconCodeIdList.Add((int)DealLabelSystemCode.ArriveIn72Hrs);
                                    dp.LabelIconList = string.Join(",", iconCodeIdList);
                                }
                            }
                        }
                        #endregion 快速出貨標籤

                        //權益說明

                        //cec.Restrictions = pcec.Restrictions;
                        //cec.Introduction = pcec.Introduction;


                        ip.ItemSet(item);
                        pp.DealPropertySet(dp);
                        pp.DealAccountingSet(da);
                        pp.BusinessHourSet(bh);
                        pp.DealPaymentSet(dpm);
                        PponFacade.SaveCategoryDeals(category, bid);
                        try
                        {
                            ProposalMultiDeal pmDeal = sp.ProposalMultiDealGetByBid(bid);
                            if (pmDeal.IsLoaded)
                            {
                                ProposalMultiDealsSpec spec = new LunchKingSite.Core.JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(pmDeal.Options).FirstOrDefault();
                                if (spec != null)
                                {
                                    IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                                    PponFacade.UpdateDealMaxItemCount(theDeal, spec.Items.FirstOrDefault().item_guid); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                }
                            }
                        }
                        catch { }


                        pp.CouponEventContentSet(cec);
                        op.GroupOrderSet(gOrder);

                        #region 分期設定
                        //多檔次取得毛利率最低的子檔毛利
                        var getRate = pp.DealMinGrossMarginGet(bid) * 100;
                        //檔次類型,不可分期是否打勾,毛利率計算結果
                        bool[] getInstallmentReturn = PponFacade.DecideInstallment(DeliveryType.ToHouse, false, item.ItemPrice, 0, getRate);
                        dp.Installment3months = getInstallmentReturn[0];
                        dp.Installment6months = getInstallmentReturn[1];
                        dp.Installment12months = getInstallmentReturn[2];
                        dp.DenyInstallment = false;
                        pp.DealPropertySet(dp);
                        #endregion

                    }


                    #region deal_time_slot
                    if (pro.BusinessHourGuid != null)
                    {
                        ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(pro.BusinessHourGuid.Value);
                        if (vpd != null && vpd.IsLoaded)
                        {
                            if (vpd.BusinessHourDeliverTimeS != null && vpd.BusinessHourDeliverTimeE != null)
                            {
                                if (vpd.BusinessHourDeliverTimeS.Value.Year == DateTime.MaxValue.Year)
                                {
                                    //如果起始日設定為9999/12/31，將DealTimeSlot刪除，避免顯示於前台
                                    pp.DealTimeSlotDeleteList(vpd.BusinessHourGuid);
                                }
                                else
                                {
                                    if (!Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealSub)) //不是子檔才insert DealTimeSlot
                                    {
                                        CategoryDealCollection cdcs = pp.CategoryDealsGetList(vpd.BusinessHourGuid);
                                        Dictionary<int, List<int>> selectedChannelCategories = new Dictionary<int, List<int>>();
                                        foreach (CategoryDeal cd in cdcs)
                                        {
                                            selectedChannelCategories.Add(cd.Cid, new List<int>());
                                        }

                                        List<int> selectedCity = new List<int>();
                                        foreach (var selectedChannel in selectedChannelCategories)
                                        {
                                            PponCity city = PponCityGroup.GetPponCityByCategoryId(selectedChannel.Key);
                                            if (city != null)
                                            {
                                                selectedCity.Add(city.CityId);
                                            }
                                        }
                                        //處理DealTimeSlot
                                        DealTimeSlotInsert(vpd, selectedCity.ToArray(), false);
                                    }
                                }
                            }
                        }
                    }
                    #endregion deal_time_slot
                }

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[結束同步後台檔次]");
                proBuilder.AppendLine("結束同步後台檔次");

                ProposalLog(pro.Id, "同步後台成功", userName);
                ProposalFacade.ProposalPerformanceLogSet("/Proposal/SyncProposalHouse", builder.ToString(), "Sys");
            }
            catch (Exception ex)
            {
                proBuilder.AppendLine("同步後台檔次失敗");
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[Error]" + ex.Message + ex.StackTrace + ex.Source);

                ProposalLog(pro.Id, "同步後台失敗", userName);
                ProposalFacade.ProposalPerformanceLogSet("/Proposal/SyncProposalHouse", builder.ToString(), "Sys");
                throw;
            }
            finally
            {
                ProposalLog(pro.Id, "同步後台結束", userName);
                ProposalFacade.ProposalPerformanceLogSet("/Proposal/SyncProposalHouse", builder.ToString(), "Sys");
            }


            //ProposalLog(pro.Id, builder.ToString(), ProposalLogType.Initial);
        }

        public static Brand GetBrand(int id)
        {
            return pp.GetBrand(id);
        }

        public static void SaveBrandItem(string marketingResource, Guid bid, string userNamee)
        {
            string[] brandIds = marketingResource.Split("/");

            foreach (string brandId in brandIds)
            {
                int id = 0;
                int.TryParse(brandId, out id);
                if (id != 0)
                {
                    List<string> bidList = new List<string>();
                    bidList.Add(bid.ToString());
                    string alertMsg = string.Empty;
                    Brand brand = pp.GetBrand(id);
                    if (brand.IsLoaded)
                    {
                        BrandItemCollection itemCollection = PponFacade.ValidateAndGetItemListAndAlertMsg(bidList, id, userNamee, out alertMsg);
                        if (itemCollection.Count > 0)
                        {
                            pp.SaveBrandItemList(itemCollection);
                        }
                        int itemCatagoryAllId = 0;
                        var itemCatagoryAll = pp.GetBrandItemCategoryByBrandId(id).Where(x => x.Name.Equals("全部")).First();
                        if (itemCatagoryAll != null) //所有匯入商品要綁定"全部"分類
                        {
                            int.TryParse(itemCatagoryAll.Id.ToString(), out itemCatagoryAllId);
                            if (itemCatagoryAllId > 0)
                            {
                                PponFacade.EditBrandItemCategory("Add", itemCatagoryAllId, itemCollection.Select(x => x.Id).ToList(), id);
                            }
                        }
                        #region 清除暫存
                        var cacheKey = "B" + id;
                        SystemFacade.ClearCacheByObjectNameAndCacheKey("ApiEventPromo", cacheKey);
                        #endregion 清除暫存
                    }
                }
            }
        }

        public static void GetBusinessHourDeliverTime(Proposal pro, VacationCollection recentHolidays,
            DateTime businessHourOrderTimeS, DateTime businessHourOrderTimeE,
            out DateTime businessHourDeliverTimeS, out DateTime businessHourDeliverTimeE)
        {
            DateTime orderStart = pro.OrderTimeS.Value; // 上檔日
            DateTime orderEnd = pro.OrderTimeE.Value;// 結檔日
            DateTime useStart = DateTime.MaxValue.Date; // 開始使用
            DateTime useEnd = DateTime.MaxValue.Date; // 結束使用

            if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.YearContract))
            {
                #region 長期上架

                // 結檔日 = 上檔日+一年
                orderEnd = orderStart.AddYears(1);

                // 開始使用 = 上檔日
                useStart = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderStart, orderStart);

                if (pro.ShipType == (int)DealShipType.Ship72Hrs)
                {
                    //快速出貨
                    //結束使用 = 結檔日+1
                    useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(1));
                }
                else
                {
                    if (pro.ShippingdateType == (int)Model.Sales.ShippingDateType.Normal)
                    {
                        #region 訂單成立後X個工作日出貨完畢
                        int useDateEndSet = int.TryParse(pro.ShipText3, out useDateEndSet) ? useDateEndSet : 0;
                        useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(useDateEndSet));
                        #endregion
                    }
                    else
                    {
                        #region 最早出貨日 ：上檔後X個工作日 ，最晚出貨日：統一結檔後x個工作日
                        int useDateEndSet = int.TryParse(pro.ShipText2, out useDateEndSet) ? useDateEndSet : 0;
                        useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(useDateEndSet));
                        #endregion
                    }
                }
                #endregion
            }
            else if (pro.ShipType == (int)DealShipType.Ship72Hrs || pro.ShipType == (int)DealShipType.Wms)
            {
                #region 快速出貨

                // 結檔日 = 上檔日+一年
                //orderEnd = orderStart.AddYears(1);

                // 開始使用 = 上檔日
                useStart = orderStart;

                // 結束使用 = 結檔日+1
                useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(1));

                #endregion
            }
            else
            {
                #region 一般出貨

                if (pro.DealType == (int)ProposalDealType.Travel)
                {
                    #region 宅配旅遊

                    // 結檔日=上檔日+45
                    orderEnd = orderStart.AddDays(45);

                    // 開始使用=上檔日
                    useStart = orderStart;

                    // 結束使用=開始使用+4
                    useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, useStart, useStart.AddDays(4));

                    #endregion
                }
                else
                {
                    #region 一般宅配

                    // 結檔日=上檔日+一年
                    //orderEnd = orderStart.AddYears(1);

                    // 開始使用=上檔日
                    useStart = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderStart, orderStart);


                    if (pro.ShipType == (int)DealShipType.Other)
                    {
                        //選其他直接指定配送日期
                        DateTime delivertTimeS;
                        DateTime delivertTimeE;
                        DateTime.TryParse(pro.DeliveryTimeS.ToString(), out delivertTimeS);
                        DateTime.TryParse(pro.DeliveryTimeE.ToString(), out delivertTimeE);
                        useStart = delivertTimeS;
                        useEnd = delivertTimeE;
                    }
                    // 結束使用=結檔日+指定天數
                    else if (pro.ShippingdateType == (int)Model.Sales.ShippingDateType.Normal)
                    {
                        #region 訂單成立後X個工作日出貨完畢
                        int useDateEndSet = int.TryParse(pro.ShipText3, out useDateEndSet) ? useDateEndSet : 0;
                        useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(useDateEndSet));
                        #endregion
                    }
                    else
                    {
                        #region 最早出貨日 ：上檔後X個工作日 ，最晚出貨日：統一結檔後x個工作日
                        int useDateEndSet = int.TryParse(pro.ShipText2, out useDateEndSet) ? useDateEndSet : 0;
                        useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(useDateEndSet));
                        #endregion
                    }
                    #endregion
                }

                #endregion
            }

            businessHourDeliverTimeS = useStart;
            businessHourDeliverTimeE = useEnd;
        }

        #region DealTimeSlot
        private static void DealTimeSlotInsert(ViewPponDeal entity, int[] cities, bool orderStartDateChange)
        {
            //**********寫入DealTimeSlot的邏輯 Begin**********
            #region 產生DealTimeSlot排程資料, it has a bug on removing items

            DateTime dateStartDay = entity.BusinessHourOrderTimeS;
            DateTime dateEndDay = entity.BusinessHourOrderTimeE;
            DealTimeSlotCollection dealTimeSlotColForInert = new DealTimeSlotCollection();
            DealTimeSlotCollection dealTimeSlotExist = pp.DealTimeSlotGetAllCol(entity.BusinessHourGuid);

            //檢查目前是否已有排程資料，若沒有依據輸入的啟始與截止日期自動產生
            if (dealTimeSlotExist == null || dealTimeSlotExist.Count == 0)
            {
                #region 依據輸入的啟始與截止日期自動產生
                //依據On檔的時間與地區產生DealTimeSlot的資料(檔期排程資料)
                //依序新增勾選區域的排程記錄
                foreach (int cityId in cities)
                {
                    AddDealTimeSlotLoop(entity, dealTimeSlotColForInert, dealTimeSlotExist, dateStartDay, dateEndDay, cityId);
                }

                //if (dealTimeSlotColForInert.Count > 0)
                //{
                //    entity.TimeSlotCollection = dealTimeSlotColForInert;
                //}
                #endregion 依據輸入的啟始與截止日期自動產生
            }
            else //若已有排程資料則需判斷是否需要修改
            {
                #region 若已有排程資料則需判斷是否需要修改

                //todo:改寫這塊邏輯，應該是不需要檢查，只要取得舊有的seq。反正最後都是全部砍掉重練。

                #region 逐筆檢查舊資料（邏輯需要再檢查）
                //CheckTimeSlotLoop(entity, dealTimeSlotColForInert, cities, dateStartDay, dateEndDay);
                #endregion 逐筆檢查舊資料

                //entity.TimeSlotCollection.AddRange(dealTimeSlotColForInert);

                //填入新增的資料

                #region 將不足的資料補上
                //將不足的資料補上
                //一開始排程時間設為DEAL起始日
                //依序新增勾選區域的排程記錄
                foreach (int cityId in cities)
                {
                    //只要排程時間小於DEAL結束日就需增加一筆排程記錄
                    AddDealTimeSlotLoop(entity, dealTimeSlotColForInert, dealTimeSlotExist, dateStartDay, dateEndDay, cityId, true);
                }
                #endregion 將不足的資料補上

                dealTimeSlotExist = dealTimeSlotColForInert;


                #endregion
            }

            if ((dealTimeSlotExist != null) && (orderStartDateChange))
            {
                // 預設前24小時輪播  add by Max 2011/4/21
                foreach (DealTimeSlot dts in dealTimeSlotExist.Where(x => DateTime.Compare(entity.BusinessHourOrderTimeS.AddDays(1), x.EffectiveStart) > 0))
                {
                    dts.IsInTurn = true;
                }
            }
            //天貓檔次設定不顯示
            if ((entity.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
            {
                foreach (var item in dealTimeSlotExist)
                {
                    item.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                }
                // _pponProv.DealTimeSlotUpdateStatus(entity.Deal.Guid, DealTimeSlotStatus.NotShowInPponDefault);
            }

            DealTimeSlotCollection dtss = pp.DealTimeSlotGetAllCol(entity.BusinessHourGuid);
            List<Guid> bids = dtss.Select(x => x.BusinessHourGuid).Distinct().ToList();
            foreach (Guid bid in bids)
            {
                pp.DealTimeSlotDeleteList(bid);
            }
            foreach (DealTimeSlot ds in dealTimeSlotColForInert)
            {
                pp.DealTimeSlotSet(ds);
            }


            //=====================================================

            #endregion 產生DealTimeSlot排程資料, it has a bug on removing items
            //**********寫入DealTimeSlot的邏輯 End**********
        }

        /// <summary>
        /// 迴圈新增DealTimeSlot
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dealTimeSlotColForInert">要寫入的dealTimeSlot</param>
        /// <param name="dealTimeSlotExist">目前的dealTimeSlot</param>
        /// <param name="dateStartDay"></param>
        /// <param name="dateEndDate"></param>
        /// <param name="cityId"></param>
        /// <param name="calNewSeq"></param>
        private static void AddDealTimeSlotLoop(ViewPponDeal entity, DealTimeSlotCollection dealTimeSlotColForInert, DealTimeSlotCollection dealTimeSlotExist,
            DateTime dateStartDay, DateTime dateEndDate, int cityId, bool calNewSeq = false)
        {
            Guid dealGuid = entity.BusinessHourGuid;
            //檢查第一筆
            bool isFirst = true;

            DealTimeSlotStatus timeSlotStatus = (entity.IsBankDeal.GetValueOrDefault() || entity.IsGame.GetValueOrDefault()) ? DealTimeSlotStatus.NotShowInPponDefault : DealTimeSlotStatus.Default;
            if (timeSlotStatus == DealTimeSlotStatus.Default && dealTimeSlotExist.Any())
            {
                //直接抓最後一筆的狀態
                var last = dealTimeSlotExist.Where(x => x.CityId == cityId).OrderBy(y => y.EffectiveEnd).LastOrDefault();
                if (last != null && last.Status == (int)DealTimeSlotStatus.NotShowInPponDefault)
                {
                    timeSlotStatus = DealTimeSlotStatus.NotShowInPponDefault;
                }
            }

            //EffectiveStart是Key值
            for (var date = dateStartDay; date < dateEndDate; date = date.AddDays(1))
            {
                //檢查資料是否已經存在
                bool anyExist = dealTimeSlotExist == null ? false : dealTimeSlotExist.Any(t => t.EffectiveStart == date && t.CityId == cityId);

                //取得該筆舊資料
                DealTimeSlot dts = !anyExist ? null : dealTimeSlotExist.Where(t => t.EffectiveStart == date && t.CityId == cityId).FirstOrDefault();

                DealTimeSlot insDeal = null;

                #region 第一筆

                if (isFirst) //針對第一筆的時間做調整
                {
                    //一開始排程時間設為DEAL起始日
                    Tuple<DateTime, DealTimeSlot> tuple = AdjustFirstEffectiveStartDay(date, dealGuid, cityId, timeSlotStatus);
                    if (tuple.Item1 != DateTime.MinValue) //調整DEAL起始時間
                    {
                        if (!anyExist)
                        {
                            dealTimeSlotColForInert.Add(tuple.Item2);
                        }
                        else
                        {
                            //檔次已存在，維持舊的排序
                            dealTimeSlotColForInert.Add(GetNewDealTimeSlot(dts));
                        }

                        //用調整後的時間做新的起始點
                        date = tuple.Item1;

                        //檢查調整後的時間資料是否已經存在
                        anyExist = dealTimeSlotExist == null ? false : dealTimeSlotExist.Any(t => t.EffectiveStart == date && t.CityId == cityId);//★這兩行是多餘的?
                        dts = !anyExist ? null : dealTimeSlotExist.Where(t => t.EffectiveStart == date && t.CityId == cityId).FirstOrDefault();//★這兩行是多餘的?
                    }

                    //一定要把isFirst關掉
                    isFirst = false;
                }

                #endregion

                #region 最後一筆

                if (date.AddDays(1) > dateEndDate)
                {
                    int lastSeq = 999999;

                    if (!anyExist)
                    {
                    }
                    else
                    {
                        //檔次已存在，維持舊的排序
                        lastSeq = dts.Sequence;
                    }
                    DealTimeSlot lastDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                                      lastSeq,
                                                      date, dateEndDate,
                                                      timeSlotStatus);

                    dealTimeSlotColForInert.Add(lastDeal);
                    continue;
                }

                #endregion

                #region  其他筆

                if (!anyExist)
                {
                    //檔次不存在
                    int newSeq = 999999;

                    //新增 此城市這個時間一筆排程記錄
                    insDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                                        newSeq,
                                                        date, date.AddDays(1),
                                                        timeSlotStatus);
                }
                else
                {
                    //檔次已存在，維持舊的排序
                    insDeal = GetNewDealTimeSlot(dts);
                }

                #endregion

                if (!dealTimeSlotColForInert.Any(t => t.EffectiveStart == date && t.CityId == cityId))
                {
                    dealTimeSlotColForInert.Add(insDeal);
                }


                //      在2013/12/10現改成當新的slot增加時，順序放在最後面時，就不用+1
                //_pponProv.DealTimeSlotUpdateSeqAddOne(cityId, effectiveStartDay);
                //////////////////////////////////////////////
            }
        }

        /// <summary>
        /// 處理第一筆，如果起始時間不是12點，先寫一筆將其調整到12點
        /// </summary>
        /// <param name="effectiveStartDay"></param>
        /// <param name="dealGuid"></param>
        /// <param name="cityId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        private static Tuple<DateTime, DealTimeSlot> AdjustFirstEffectiveStartDay(DateTime effectiveStartDay, Guid dealGuid, int cityId, DealTimeSlotStatus status = DealTimeSlotStatus.Default)
        {
            DateTime tempEndDay = DateTime.MinValue;
            DealTimeSlot insDeal = null;

            if (effectiveStartDay.ToString("HH:mm") != "12:00")
            {
                if (effectiveStartDay.Hour < 12) //未過中午12點
                {
                    tempEndDay = effectiveStartDay.Noon();

                }
                else //已過中午12點
                {
                    tempEndDay = effectiveStartDay.Noon().AddDays(1);
                }
            }
            //int newSeq = PponFacade.GetDealTimeSlotNonLockSeq(cityId, effectiveStartDay, vpdts);
            insDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                         999999, //★
                                          effectiveStartDay, tempEndDay,
                                          status);

            return Tuple.Create(tempEndDay, insDeal);
        }

        private static DealTimeSlot GetNewDealTimeSlot(Guid bid, int cityId, int seq, DateTime startDate, DateTime endDate, DealTimeSlotStatus status, bool isInTurn = false)
        {
            //新增 此城市這個時間一筆排程記錄
            DealTimeSlot newDeal = new DealTimeSlot();
            newDeal.BusinessHourGuid = bid;
            newDeal.CityId = cityId;
            newDeal.Sequence = seq;
            newDeal.EffectiveStart = startDate;
            newDeal.EffectiveEnd = endDate;
            newDeal.Status = (int)status;
            newDeal.IsInTurn = isInTurn;

            return newDeal;
        }

        private static DealTimeSlot GetNewDealTimeSlot(DealTimeSlot fromData)
        {
            //新增 此城市這個時間一筆排程記錄
            DealTimeSlot newDeal = new DealTimeSlot();
            newDeal.BusinessHourGuid = fromData.BusinessHourGuid;
            newDeal.CityId = fromData.CityId;
            newDeal.Sequence = fromData.Sequence;
            newDeal.EffectiveStart = fromData.EffectiveStart;

            //fix 手動結檔再開檔，fromData.EffectiveEnd.Noon()同日問題
            if (newDeal.EffectiveStart.Date.Equals(fromData.EffectiveEnd.Date) && newDeal.EffectiveStart >= fromData.EffectiveEnd.Noon())
            {
                newDeal.EffectiveEnd = fromData.EffectiveEnd.AddDays(1).Noon();
            }
            else
            {
                newDeal.EffectiveEnd = fromData.EffectiveEnd.Noon();
            }
            //日期銜接為12點，所以直接設定12點

            newDeal.Status = fromData.Status;
            newDeal.IsInTurn = fromData.IsInTurn;
            newDeal.IsLockSeq = fromData.IsLockSeq; //★本次新增

            return newDeal;
        }

        #endregion DealTimeSlot

        /// <summary>
        /// 判斷提案單是否需多重選項
        /// </summary>
        /// <param name = "options"></ param >
        /// < returns ></ returns >
        public static bool IsProposalMultiSpec(string options)
        {
            bool isMulti = false;
            if (!string.IsNullOrEmpty(options))
            {
                List<ProposalMultiDealsSpec> dbSpecs = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(options);
                List<Guid> infoGuids = dbSpecs.SelectMany(x => x.Items).Select(y => y.product_guid).Distinct().ToList();
                foreach (Guid pid in infoGuids)
                {
                    ProductInfo info = pp.ProductInfoGet(pid);
                    if (info.IsLoaded)
                    {
                        if (info.IsMulti)
                        {
                            isMulti = true;
                            break;
                        }
                    }
                }
                if (infoGuids.Count > 1)
                {
                    isMulti = true;
                }
            }
            return isMulti;
        }

        public static bool ProcessMultiSpecToPponOption(string options, Guid bid, string userName)
        {
            List<ProposalMultiDealsSpec> dbSpecs = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(options);
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);

            #region ppon_option
            int uniqueId = vpd.UniqueId.Value;
            PponOptionCollection dbOption = pp.PponOptionGetList(bid, true);
            PponOptionCollection dbOptionAll = pp.PponOptionGetList(bid, false);
            //disabled nonuse
            PponOptionCollection ops = new PponOptionCollection();
            foreach (var opt in dbOption)
            {
                //var spec = dbSpecs.SelectMany(x => x.Items).Where(y => y.product_guid == opt.InfoGuid).FirstOrDefault();
                //if (spec == null)
                //{
                //    opt.IsDisabled = true;
                //    opt.ModifyId = userName;
                //    opt.ModifyTime = DateTime.Now;
                //    pp.PponOptionSet(opt);
                //}
                opt.IsDisabled = true;
                opt.ModifyId = userName;
                opt.ModifyTime = DateTime.Now;
                //pp.PponOptionSet(opt);
                ops.Add(opt);
            }
            pp.PponOptionSetList(ops);
            //pp.PponOptionSet(bid, userName);
            int infoSeq = 1;
            foreach (var info in dbSpecs)
            {
                int specSeq = 1;
                ProductItemCollection pitemList = pp.ProductItemGetList(info.Items.Select(x => x.item_guid).ToList());
                ProductInfoCollection productList = pp.ProductInfoListGet(info.Items.Select(x => x.product_guid).ToList());
                //builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[productList]" + new JsonSerializer().Serialize(productList));

                foreach (ProposalMultiDealsItem spec in info.Items)
                {
                    ProductItem pitem = pitemList.Where(x => x.Guid == spec.item_guid).FirstOrDefault(); //pp.ProductItemGet(spec.item_guid);

                    if (pitem != null && pitem.ItemStatus == (int)ProductItemStatus.Normal)
                    {
                        ProductInfo product = productList.Where(x => x.Guid == pitem.InfoGuid).FirstOrDefault(); //pp.ProductInfoGet(pitem.InfoGuid);
                        if (product == null)
                        {
                            continue;
                        }

                        string specName = pitem.SpecName;
                        if ((product.ProductBrandName + product.ProductName + "_" + pitem.SpecName).Length < 45)
                        {
                            specName = product.ProductBrandName + product.ProductName;
                            if (!string.IsNullOrEmpty(pitem.SpecName))
                            {
                                specName += "_" + pitem.SpecName; ;
                            }
                        }
                        else if ((product.ProductName + pitem.SpecName).Length < 45)
                        {
                            specName = product.ProductName;
                            if (!string.IsNullOrEmpty(pitem.SpecName))
                            {
                                specName += "_" + pitem.SpecName; ;
                            }
                        }
                        if (string.IsNullOrEmpty(specName))
                        {
                            specName = product.ProductName;
                            if (!string.IsNullOrEmpty(pitem.SpecName))
                            {
                                specName += "_" + pitem.SpecName; ;
                            }
                            specName = specName.Substring(0, 45);
                        }

                        string catgName = string.Format("選購項目{0}", infoSeq.ToString());
                        var opt = dbOption.Where(x => x.ItemGuid == spec.item_guid
                                                        && x.CatgName == catgName
                                                        && x.OptionName == specName).FirstOrDefault();

                        if (opt == null)
                        {
                            //string orispecName = specName;
                            //for (int idx = 1; idx <= 5; idx++)
                            //{
                            //    //最多偵測五次
                            //    List<PponOption> dbOpts = dbOptionAll.Where(
                            //                        x => x.DealId == vpd.UniqueId.Value &&
                            //                        x.CatgName == catgName &&
                            //                        x.OptionName == specName)
                            //                        .ToList();

                            //    if (dbOpts.Count > 0)
                            //    {
                            //        specName = orispecName + "_" + idx;
                            //    }
                            //    else
                            //    {
                            //        break;
                            //    }
                            //}


                            PponOption opti = new PponOption()
                            {
                                Guid = Guid.NewGuid(),
                                DealId = vpd.UniqueId.Value,
                                BusinessHourGuid = bid,
                                CatgName = catgName,
                                CatgSeq = infoSeq,
                                OptionName = specName,
                                OptionSeq = specSeq,
                                Quantity = 0,
                                IsDisabled = false,
                                CreateId = userName,
                                CreateTime = DateTime.Now,
                                ItemNo = pitem.ProductCode,
                                InfoGuid = spec.product_guid,
                                ItemGuid = spec.item_guid
                            };
                            pp.PponOptionSet(opti);
                        }
                        else
                        {
                            if (opt.Guid == null || opt.Guid == Guid.Empty)
                            {
                                opt.Guid = Guid.NewGuid();
                            }
                            opt.CatgName = catgName;
                            opt.CatgSeq = infoSeq;
                            opt.OptionName = specName;//string.Format("{0}{1}_{2}", string.IsNullOrEmpty(product.ProductBrandName) ? string.Empty : "【" + product.ProductBrandName + "】", product.ProductName , spec.name);
                            opt.OptionSeq = specSeq;
                            opt.Quantity = 0;
                            opt.ItemNo = pitem.ProductCode;
                            opt.IsDisabled = false;
                            pp.PponOptionSet(opt);
                        }
                        specSeq++;
                    }
                }
                infoSeq++;
            }
            #endregion ppon_option
            return true;
        }

        public static string GetProEventName(Proposal pro, List<ProposalMultiDeal> multiDeals)
        {
            string result = string.Empty;
            try
            {
                if (multiDeals.Count() > 1)
                {
                    //多檔次
                    //平均最低只要 [最低均價] 元起(含運)即可享有原價最高 [最高原價] 元 [檔次名稱]：(A)[方案名稱](多款可選)，加贈：[贈品內容]/(B)[方案名稱]，(多款可選)，加贈：[贈品內容]

                    var avgItem = multiDeals.Select(x => new { AvgPrice = Math.Ceiling(Convert.ToDouble(x.ItemPrice) / Convert.ToDouble(x.QuantityMultiplier)) }).OrderBy(y => y.AvgPrice).FirstOrDefault();

                    string dealContent = string.Empty;
                    string[] Code = { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P",
                                           "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d",
                                           "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u",
                                           "v", "w", "x", "y", "z"};
                    int idx = 0;
                    foreach (ProposalMultiDeal deal in multiDeals)
                    {
                        //[方案名稱](多款可選)，加贈：[贈品內容]
                        string giftContent = string.Empty;
                        try
                        {
                            List<string> gifts = new JsonSerializer().Deserialize<List<string>>(deal.Gifts);
                            if (gifts.Count > 0)
                            {
                                giftContent = string.Format("，加贈：{0}", string.Join(",", gifts));
                            }
                        }
                        catch { }


                        string couponUsage = GetProposalCouponUsage(deal);

                        if ((dealContent + couponUsage).Length < 350)
                        {
                            dealContent += string.Format("({0}){1}{2}",
                            Code[idx],
                            GetProposalCouponUsage(deal),
                            giftContent
                            );
                        }
                        else
                        {
                            dealContent += "...";
                            continue;
                        }
                        idx++;
                    }

                    result = string.Format("平均最低只要 {0} 元起 {1} 即可享有{2}",
                        avgItem.AvgPrice,
                        multiDeals.OrderBy(x => x.ItemPrice).FirstOrDefault().Freights == 0 ? "(含運)" : string.Empty,
                        dealContent
                        );
                }
                else if (multiDeals.Count() == 1)
                {
                    //單檔
                    //只要 [售價] 元(含運) 即可享有原價 [原價] 元 [方案名稱]，加贈：[贈品內容]
                    ProposalMultiDeal md = multiDeals.FirstOrDefault();

                    string giftContent = string.Empty;
                    try
                    {
                        List<string> gifts = new JsonSerializer().Deserialize<List<string>>(md.Gifts);
                        if (gifts.Count > 0)
                        {
                            giftContent = string.Format("，加贈：{0}", string.Join(",", gifts));
                        }
                    }
                    catch { }

                    result = string.Format("只要 {0} 元 {1} 即可享有原價 {2} 元 {3}{4}",
                        md.ItemPrice.ToString("N0"),
                        md.Freights == 0 ? "(含運)" : string.Empty,
                        md.OrigPrice.ToString("N0"),
                        (!string.IsNullOrEmpty(md.BrandName) ? "【" + md.BrandName + "】" : string.Empty) + md.ItemName,
                        giftContent
                        );
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return result;
        }
        public static string GetProposalCouponUsage(ProposalMultiDeal deal)
        {
            return (string.IsNullOrEmpty(deal.BrandName) ? string.Empty : "【" + deal.BrandName + "】") + deal.ItemName + " " + deal.QuantityMultiplier + deal.Unit + (deal.Unit == "組" ? string.Empty : "/組");
        }
        public static string GetProposalCouponUsageWithGifts(ProposalMultiDeal deal)
        {
            string res_gift = string.Empty;
            if (!string.IsNullOrEmpty(deal.Gifts))
            {
                List<string> gifts = new JsonSerializer().Deserialize<List<string>>(deal.Gifts);
                if (gifts.Count > 0)
                {
                    res_gift = string.Join(",", gifts);
                    if (res_gift.Length > 0)
                    {
                        res_gift = string.Format("，加贈:{0}", res_gift.Substring(0, res_gift.Length - 1));
                    }
                }
            }
            return (string.IsNullOrEmpty(deal.BrandName) ? string.Empty : "【" + deal.BrandName + "】") + deal.ItemName + " " + deal.QuantityMultiplier + deal.Unit + (deal.Unit == "組" ? string.Empty : "/組") + res_gift;
        }
        public static string GetProEventNameByComboDeal(Proposal pro, ProposalMultiDeal deal)
        {
            string result = string.Empty;
            try
            {
                //多檔次子檔
                var avgPrice = Math.Ceiling(Convert.ToDouble(deal.ItemPrice) / Convert.ToDouble(deal.QuantityMultiplier));

                string dealContent = string.Empty;
                List<string> gifts = new JsonSerializer().Deserialize<List<string>>(deal.Gifts);
                string giftContent = string.Empty;
                if (gifts.Count > 0)
                {
                    giftContent = string.Format("，加贈：{0}", string.Join(",", gifts));
                }

                //只要289元(含運)即可享有原價700元升級款太空鋁合金超大後視鏡線控自拍桿1入/組，加贈：藍芽搖控器(不挑色)＊1
                result = string.Format("只要{0}元{1}即可享有原價{2}元{3}{4}",
                    deal.ItemPrice.ToString("N0"),
                    deal.Freights == 0 ? "(含運)" : string.Empty,
                    deal.OrigPrice.ToString("N0"),
                    GetProposalCouponUsage(deal),
                    giftContent
                    );

            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// 業務轉件
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="salesType"></param>
        /// <param name="sales"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static string ReferralSales(int pid, SellerSalesType salesType, string sales, string userName)
        {
            ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, sales);
            SellerSalesType type = salesType;
            if (emp.IsLoaded)
            {
                List<Department> dept = HumanFacade.GetSalesDepartment();
                if (emp.DeptId.EqualsAny(dept.Select(x => x.DeptId).ToArray()))
                {
                    if (type == SellerSalesType.Develope)
                    {
                        Proposal p = sp.ProposalGet(pid);
                        p.DevelopeSalesId = emp.UserId;
                        sp.ProposalSet(p);


                        ProposalLog(pid, "變更 負責業務1 為 " + emp.EmpName, userName);
                        return "";
                    }
                    else
                    {
                        Proposal p = sp.ProposalGet(pid);
                        p.OperationSalesId = emp.UserId;
                        sp.ProposalSet(p);


                        ProposalLog(pid, "變更 負責業務2 為 " + emp.EmpName, userName);
                        return "";
                    }
                }
                else
                {
                    return "無法轉件於非業務，請重新檢視。";
                }
            }
            else
            {
                return "Email帳號錯誤，請重新檢視。";
            }



        }

        /// <summary>
        /// 毛利率，是否須過主管關卡
        /// </summary>
        /// <param name="pid">單號</param>
        /// <returns></returns>
        public static bool IsProposalGrossMarginRestrictionComplete(Proposal pro)
        {
            bool complete = true;
            int pid = pro.Id;
            int dealType2 = pro.DealType2.GetValueOrDefault(0);
            int businessFlag = pro.BusinessFlag;
            DateTime? flowCompleteTime = pro.FlowCompleteTime;
            if (flowCompleteTime != null)
            {
                //流程已結束
                return true;
            }

            ProposalMultiDealCollection pmdc = sp.ProposalMultiDealGetByPid(pid);
            ProposalRestriction pr = sp.ProposalRestrictionGetByCodeId(dealType2);

            if (Helper.IsFlagSet(businessFlag, ProposalBusinessFlag.ProposalAudit))
            {
                //主管已審過
                complete = true;
            }
            else
            {
                if (pr.IsLoaded)
                {
                    ViewProposalSeller vps = sp.ViewProposalSellerGet(ViewProposalSeller.Columns.Id, pid);
                    foreach (var pmd in pmdc)
                    {
                        double grossMargin = GetGrossMargin(vps, pmd);
                        double costPercent = GetCostPercent(pmd);

                        if (pr.GrossMarginRange != null)
                        {
                            if (pr.GrossMarginRange == 1)
                            {
                                if (grossMargin <= pr.GrossMarginNumber)
                                {
                                    complete = false;//要審
                                    break;
                                }
                                else
                                {
                                    complete = true;//不用審
                                }
                            }
                            else if (pr.GrossMarginRange == 2)
                            {
                                if (grossMargin < pr.GrossMarginNumber)
                                {
                                    complete = false;
                                    break;
                                }
                                else
                                {
                                    complete = true;
                                }
                            }
                            else if (pr.GrossMarginRange == 3)
                            {
                                if (grossMargin == pr.GrossMarginNumber)
                                {
                                    complete = false;
                                    break;
                                }
                                else
                                {
                                    complete = true;
                                }
                            }
                            else if (pr.GrossMarginRange == 4)
                            {
                                if (grossMargin > pr.GrossMarginNumber)
                                {
                                    complete = false;
                                    break;
                                }
                                else
                                {
                                    complete = true;
                                }
                            }
                            else if (pr.GrossMarginRange == 5)
                            {
                                if (grossMargin >= pr.GrossMarginNumber)
                                {
                                    complete = false;
                                    break;
                                }
                                else
                                {
                                    complete = true;
                                }
                            }

                        }
                        if (pr.CostPercentRange != null)
                        {
                            if (pr.CostPercentRange == 1)
                            {
                                if (costPercent <= pr.CostPercentNumber)
                                {
                                    complete = false;
                                    break;
                                }
                                else
                                {
                                    complete = true;
                                }
                            }
                            else if (pr.CostPercentRange == 2)
                            {
                                if (costPercent < pr.CostPercentNumber)
                                {
                                    complete = false;
                                    break;
                                }
                                else
                                {
                                    complete = true;
                                }
                            }
                            else if (pr.CostPercentRange == 3)
                            {
                                if (costPercent == pr.CostPercentNumber)
                                {
                                    complete = false;
                                    break;
                                }
                                else
                                {
                                    complete = true;
                                }
                            }
                            else if (pr.CostPercentRange == 4)
                            {
                                if (costPercent > pr.CostPercentNumber)
                                {
                                    complete = false;
                                    break;
                                }
                                else
                                {
                                    complete = true;
                                }
                            }
                            else if (pr.CostPercentRange == 5)
                            {
                                if (costPercent >= pr.CostPercentNumber)
                                {
                                    complete = false;
                                    break;
                                }
                                else
                                {
                                    complete = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    //沒有毛利率限制
                    complete = true;
                }
            }



            return complete;
        }

        public static string ProposalFlowCompleted(Proposal pro, bool isUpdateOrderTimeS, string userName)
        {
            string resultMsg = string.Empty;
            #region 如果所有關卡都跑完，將資料同步後台
            if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck) &&
                ProposalFacade.IsProposalGrossMarginRestrictionComplete(pro) &&
                ProposalFacade.IsProposalProductionComplete(pro))
            {
                #region 檢查ppon_option是否重複
                if (pro.BusinessHourGuid.HasValue)
                {
                    try
                    {
                        ViewComboDealCollection vcdc = pp.GetViewComboDealByBid(pro.BusinessHourGuid.Value, false);

                        foreach (ViewComboDeal vcd in vcdc)
                        {
                            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vcd.BusinessHourGuid);
                            PponOptionCollection dbOption = pp.PponOptionGetList(vcd.BusinessHourGuid);
                            PponOptionCollection dbOptionAll = pp.PponOptionGetList(vcd.BusinessHourGuid, false);
                            ProposalMultiDeal pmd = sp.ProposalMultiDealGetByBid(vcd.BusinessHourGuid);

                            List<ProposalMultiDealsSpec> dbSpecs = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(pmd.Options);
                            int infoSeq = 1;
                            foreach (var info in dbSpecs)
                            {
                                ProductItemCollection pitemList = pp.ProductItemGetList(info.Items.Select(x => x.item_guid).ToList());
                                ProductInfoCollection productList = pp.ProductInfoListGet(info.Items.Select(x => x.product_guid).ToList());
                                //builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[productList]" + new JsonSerializer().Serialize(productList));
                                foreach (ProposalMultiDealsItem spec in info.Items)
                                {
                                    ProductItem pitem = pitemList.Where(x => x.Guid == spec.item_guid).FirstOrDefault(); //pp.ProductItemGet(spec.item_guid);
                                    if (pitem != null && pitem.ItemStatus == (int)ProductItemStatus.Normal)
                                    {
                                        ProductInfo product = productList.Where(x => x.Guid == pitem.InfoGuid).FirstOrDefault(); //pp.ProductInfoGet(pitem.InfoGuid);
                                        if (product == null)
                                        {
                                            continue;
                                        }

                                        string specName = pitem.SpecName;
                                        if ((product.ProductBrandName + product.ProductName + "_" + pitem.SpecName).Length < 45)
                                        {
                                            specName = product.ProductBrandName + product.ProductName;
                                            if (!string.IsNullOrEmpty(pitem.SpecName))
                                            {
                                                specName += "_" + pitem.SpecName; ;
                                            }
                                        }
                                        else if ((product.ProductName + pitem.SpecName).Length < 45)
                                        {
                                            specName = product.ProductName;
                                            if (!string.IsNullOrEmpty(pitem.SpecName))
                                            {
                                                specName += "_" + pitem.SpecName; ;
                                            }
                                        }
                                        if (string.IsNullOrEmpty(specName))
                                        {
                                            specName = product.ProductName;
                                            if (!string.IsNullOrEmpty(pitem.SpecName))
                                            {
                                                specName += "_" + pitem.SpecName; ;
                                            }
                                            specName = specName.Substring(0, 45);
                                        }

                                        string catgName = string.Format("選購項目{0}", infoSeq.ToString());
                                        var opt = dbOption.Where(x => x.ItemGuid == spec.item_guid
                                                                        && x.CatgName == catgName
                                                                        && x.OptionName == specName).FirstOrDefault();

                                        if (opt == null)
                                        {
                                            List<PponOption> dbOpts = dbOptionAll.Where(
                                                                x => x.DealId == vpd.UniqueId.Value &&
                                                                x.CatgName == catgName &&
                                                                x.OptionName == specName)
                                                                .ToList();

                                            if (dbOpts.Count > 0)
                                            {
                                                resultMsg = string.Format("{0}為舊商品名稱，無法同步後台，請重新修正。", specName);
                                            }
                                            if (!string.IsNullOrEmpty(resultMsg))
                                            {
                                                return resultMsg;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch { }
                }
                #endregion 檢查ppon_option是否重複

                if (isUpdateOrderTimeS)
                {
                    //壓上流程結束日期
                    if (pro.FlowCompleteTime == null)
                    {
                        pro.FlowCompleteTime = DateTime.Now;
                        sp.ProposalSet(pro);
                    }
                    //最後一關的關卡審核時，才要變更日期，儲存時不要變更
                    ProposalFacade.UpdateOrderTimeS(pro, userName);
                }

                //同步後臺檔次
                ProposalFacade.SyncProposalHouse(pro, userName);
                //清除毛利率現金券相關cache
                if (pro.BusinessHourGuid != Guid.Empty)
                {
                    DiscountManager.ForceRefreshDealDiscountedPrice((Guid)pro.BusinessHourGuid);
                }

                if (pro.FlowCompleteTimes == 0)
                {
                    pro.FlowCompleteTimes = pro.FlowCompleteTimes + 1;
                    sp.ProposalSet(pro);
                }

                if (isUpdateOrderTimeS)
                {
                    //寄信通知商家檔次上檔資訊
                    ProposalFacade.MailToVbsDealOn(pro.Id);
                }
            }
            #endregion 如果所有關卡都跑完，將資料同步後台

            return resultMsg;
        }

        private static void UpdateOrderTimeS(Proposal pro, string userName)
        {
            bool isDealOn = false;
            if (pro.BusinessHourGuid != null)
            {
                BusinessHour bh = sp.BusinessHourGet(pro.BusinessHourGuid.Value);
                if (bh.IsLoaded)
                {
                    if (bh.BusinessHourOrderTimeS < DateTime.Now && bh.BusinessHourOrderTimeE >= DateTime.Now)
                    {
                        isDealOn = true;
                    }
                }
            }
            if (isDealOn)
            {
                //已上檔，則不更改上檔日期
                return;
            }

            //調整檔期
            /*
            1. 如果 預壓檔期已經超過，則自動改壓最後審核通過的日期和時間，
            2. 如果 沒有預壓檔期，則自動壓上最後審核通過的日期和時間，
            3. 如果 沒有超過預壓檔期，則依照原預壓檔期不改，等待上架
            */
            bool isChangeOrderTimeS = false;
            DateTime dateStart = DateTime.Now;
            for (int ti = 1; ti <= 5; ti++)
            {
                DateTime tmpDate = dateStart.AddMinutes(ti);
                if ((tmpDate.Minute % 5) == 0)
                {
                    dateStart = DateTime.Parse(tmpDate.ToString("yyyy/MM/dd HH:mm:ss"));
                    break;
                }
            }
            DateTime dateEnd = DateTime.Now.AddYears(1);
            //皆預設壓23:59
            if (pro.OrderTimeS == null)
            {
                pro.OrderTimeS = dateStart;
                if (pro.ShipType == (int)DealShipType.Other)
                {
                    //自訂出貨日期
                    if (pro.DeliveryTimeE != null)
                    {
                        pro.OrderTimeE = pro.DeliveryTimeE.Value.AddDays(-1).AddMinutes(config.BusinessOrderTimeEMinute);
                    }
                    else
                    {
                        pro.OrderTimeE = dateEnd.Date.AddMinutes(config.BusinessOrderTimeEMinute);
                    }
                }
                else
                {
                    pro.OrderTimeE = dateEnd.Date.AddMinutes(config.BusinessOrderTimeEMinute);
                }
                isChangeOrderTimeS = true;
            }
            else
            {
                if (pro.OrderTimeS < DateTime.Now)
                {
                    pro.OrderTimeS = dateStart;
                    isChangeOrderTimeS = true;
                }
                if (pro.OrderTimeE < DateTime.Now)
                {
                    if (pro.ShipType == (int)DealShipType.Other)
                    {
                        //自訂出貨日期
                        if (pro.DeliveryTimeE != null)
                        {
                            pro.OrderTimeE = pro.DeliveryTimeE.Value.AddDays(-1).AddMinutes(config.BusinessOrderTimeEMinute);
                        }
                        else
                        {
                            pro.OrderTimeE = dateEnd.Date.AddMinutes(config.BusinessOrderTimeEMinute);
                        }
                    }
                    else
                    {
                        pro.OrderTimeE = dateEnd.Date.AddMinutes(config.BusinessOrderTimeEMinute);
                    }
                }
            }
            if (isChangeOrderTimeS)
            {
                sp.ProposalSet(pro);
                ProposalFacade.ProposalLog(pro.Id, string.Format("{0} {1} ~ {2}", "[審核完成] 檔期為空，系統壓上日期", pro.OrderTimeS, pro.OrderTimeE), userName);
                pro = sp.ProposalGet(pro.Id);
            }
        }

        /// <summary>
        /// 取得獲利毛利率(js也有，要改起一併調整)
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static double GetGrossMargin(ViewProposalSeller pro, ProposalMultiDeal item)
        {
            double value = 0;
            double itemPrice = (double)item.ItemPrice;
            double cost = (double)item.Cost;
            double slottingFeeQuantity = item.SlottingFeeQuantity;
            double orderTotalLimit = (double)item.OrderTotalLimit;



            if (!Helper.IsFlagSet(Convert.ToInt32(pro.BusinessCreateFlag), (int)ProposalBusinessCreateFlag.Created))
            {
                //還在提案單(尚未建檔)


                //售價都要/1.05
                itemPrice = itemPrice / 1.05;
                itemPrice = Math.Floor(itemPrice * 10000) / 10000;

                //統一發票(含稅)，才除以1.05
                if (pro.VendorReceiptType == (int)VendorReceiptType.Invoice || pro.VendorReceiptType == (int)VendorReceiptType.Other)
                {
                    cost = cost / 1.05;
                    cost = Math.Floor(cost * 10000) / 10000;
                }

                if (slottingFeeQuantity != 0)
                {
                    //上架費算法
                    value = (itemPrice == 0 ? 0 : ((itemPrice) * orderTotalLimit - (cost * (orderTotalLimit - slottingFeeQuantity))) / ((itemPrice) * orderTotalLimit) * 100);
                }
                else
                {
                    //一般算法 or 成套票券算法
                    value = (itemPrice == 0 ? 0 : (itemPrice - cost) / itemPrice * 100);
                }


                return Math.Floor(value * Math.Pow(10, 2)) / Math.Pow(10, 2);
            }
            else if (Helper.IsFlagSet(Convert.ToInt32(pro.BusinessCreateFlag), (int)ProposalBusinessCreateFlag.Created))
            {
                //已建檔

                //直接抓view值
                Guid sid = Guid.Empty;
                if (pro.BusinessHourGuid != null)
                {
                    Guid.TryParse(pro.BusinessHourGuid.ToString(), out sid);
                }

                return ((double)PponFacade.GetMinimumGrossMarginFromBids(sid, true) * 100);
            }

            return 0;
        }


        /// <summary>
        /// 取得洽談毛利率(js也有，要改起一併調整)
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static double GetCostPercent(ProposalMultiDeal item)
        {
            double value = 0;
            double itemPrice = (double)item.ItemPrice;
            double cost = (double)item.Cost;
            double slottingFeeQuantity = item.SlottingFeeQuantity;
            double orderTotalLimit = (double)item.OrderTotalLimit;

            if (itemPrice == 0)
            {
                return value;
            }

            value = ((itemPrice - cost) / itemPrice) * 100;
            value = Math.Floor(value * Math.Pow(10, 2)) / Math.Pow(10, 2);
            return value;

        }


        /// <summary>
        /// 申請製檔，是否需過創編關卡
        /// </summary>
        /// <param name="pro"></param>
        /// <returns></returns>
        public static bool IsProposalProductionComplete(Proposal pro)
        {
            bool complete = false;
            bool isProduction = pro.IsProduction;
            int listingFlag = pro.ListingFlag;
            DateTime? flowCompleteTime = pro.FlowCompleteTime;
            //if (flowCompleteTime != null)
            //{
            //    //流程已結束
            //    return true;
            //}

            if (isProduction)
            {
                if (Helper.IsFlagSet(listingFlag, ProposalListingFlag.PageCheck))
                {
                    complete = true;
                }
                else
                {
                    complete = false;
                }
            }
            else
            {
                complete = true;
            }
            return complete;
        }

        //移轉提案單賣家
        public static string ChangeSeller(ProposalSourceType sourceType, int pid, string sellerId, string userName)
        {
            //更換賣家資訊
            Seller newSeller = sp.SellerGet(Seller.Columns.SellerId, sellerId);
            if (newSeller != null && newSeller.IsLoaded)
            {
                Proposal pro = sp.ProposalGet(pid);
                if (pro.SellerGuid == newSeller.Guid)
                {
                    return "不允許移轉為賣家本身";
                }
                if (pro != null && pro.IsLoaded)
                {
                    if (sourceType == ProposalSourceType.Original)
                    {
                        if (pro.BusinessHourGuid == null)
                        {
                            //建檔後不能再搬賣家
                            string title = "【更換賣家】" + pro.SellerGuid + " 變更為 " + newSeller.Guid;
                            pro.SellerGuid = newSeller.Guid;
                            sp.ProposalSet(pro);

                            //變更proposal_store的賣家資料
                            ProposalStoreCollection psc = pp.ProposalStoreGetListByProposalId(pro.Id);
                            foreach (ProposalStore ps in psc)
                            {
                                if (psc.Where(x => x.StoreGuid == newSeller.Guid).FirstOrDefault() == null)
                                {
                                    ProposalStore newStore = new ProposalStore();
                                    newStore.ProposalId = ps.ProposalId;
                                    newStore.StoreGuid = newSeller.Guid;
                                    newStore.CreateId = ps.CreateId;
                                    newStore.CreateTime = DateTime.Now;
                                    newStore.TotalQuantity = ps.TotalQuantity;
                                    newStore.OrderedQuantity = ps.OrderedQuantity;
                                    newStore.SortOrder = ps.SortOrder;
                                    newStore.ChangedExpireDate = ps.ChangedExpireDate;
                                    newStore.ResourceGuid = ps.ResourceGuid;
                                    newStore.VbsRight = ps.VbsRight;
                                    pp.ProposalStoreSet(newStore);
                                }
                            }
                            pp.ProposalStoreDeleteList(psc);

                            ProposalLog(pro.Id, title, userName);
                            return "";
                        }
                        else
                        {
                            return "已建檔之提案單不允許移轉賣家";
                        }
                    }
                    else
                    {
                        if (!pp.BusinessHourGet((Guid)pro.BusinessHourGuid).IsLoaded)
                        {
                            Seller oldSeller = sp.SellerGet(Seller.Columns.Guid, pro.SellerGuid);
                            ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pro.Id);
                            if (pcec.IsLoaded)
                            {
                                string[] images = ImageFacade.GetMediaPathsFromRawData(pcec.ImagePath, MediaType.PponDealPhoto);
                                string appDealPic = ImageFacade.GetMediaPathsFromRawData(pcec.AppDealPic, MediaType.PponDealPhoto).FirstOrDefault();

                                string newBaseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/media/{0}/", sellerId));
                                string oldBaseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/media/{0}/", oldSeller.SellerId));

                                if (!System.IO.Directory.Exists(newBaseDirectoryPath))
                                {
                                    System.IO.Directory.CreateDirectory(newBaseDirectoryPath);
                                }

                                string fileName1 = string.Empty;
                                string fileName2 = string.Empty;
                                string fileName3 = string.Empty;
                                if (images.Count() > 1)
                                {
                                    fileName1 = Path.GetFileName(images[0].Split('?')[0]);
                                    fileName2 = Path.GetFileName(images[1]);
                                    fileName3 = Path.GetFileName(appDealPic);

                                    System.IO.File.Move(oldBaseDirectoryPath + fileName1, newBaseDirectoryPath + fileName1);
                                    System.IO.File.Move(oldBaseDirectoryPath + fileName2, newBaseDirectoryPath + fileName2);
                                    System.IO.File.Move(oldBaseDirectoryPath + fileName3, newBaseDirectoryPath + fileName3);

                                    //改圖檔路徑
                                    pcec.ImagePath = pcec.ImagePath.Replace(oldSeller.SellerId, sellerId);
                                    pcec.AppDealPic = pcec.AppDealPic.Replace(oldSeller.SellerId, sellerId);
                                    pp.ProposalCouponEventContentSet(pcec);
                                }
                            }
                            //建檔後不能再搬賣家
                            string title = "【更換賣家】" + pro.SellerGuid + " 變更為 " + newSeller.Guid;
                            pro.SellerGuid = newSeller.Guid;
                            sp.ProposalSet(pro);


                            //變更proposal_store的賣家資料
                            ProposalStoreCollection psc = pp.ProposalStoreGetListByProposalId(pro.Id);
                            foreach (ProposalStore ps in psc)
                            {
                                if (psc.Where(x => x.StoreGuid == newSeller.Guid).FirstOrDefault() == null)
                                {
                                    ProposalStore newStore = new ProposalStore();
                                    newStore.ProposalId = ps.ProposalId;
                                    newStore.StoreGuid = newSeller.Guid;
                                    newStore.CreateId = ps.CreateId;
                                    newStore.CreateTime = DateTime.Now;
                                    newStore.TotalQuantity = ps.TotalQuantity;
                                    newStore.OrderedQuantity = ps.OrderedQuantity;
                                    newStore.SortOrder = ps.SortOrder;
                                    newStore.ChangedExpireDate = ps.ChangedExpireDate;
                                    newStore.ResourceGuid = ps.ResourceGuid;
                                    newStore.VbsRight = ps.VbsRight;
                                    pp.ProposalStoreSet(newStore);
                                }
                            }
                            pp.ProposalStoreDeleteList(psc);


                            ProposalLog(pro.Id, title, userName);
                            return "";
                        }
                        else
                        {
                            return "已建檔之提案單不允許移轉賣家";
                        }
                    }
                }
                else
                {
                    return "提案單不存在";
                }
            }
            else
            {
                return "賣家資料錯誤";
            }
        }


        /// <summary>
        /// 提案單新舊資料比較
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="ori"></param>
        /// <param name="userName"></param>
        /// <param name="type"></param>
        public static void CompareProposal(Proposal pro, Proposal ori, string userName, ProposalLogType type = ProposalLogType.Initial)
        {
            List<string> changeList = new List<string>();
            foreach (var item in PponFacade.ColumnValueComparison(ori, pro))
            {
                changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
            }

            if (pro.ProposalSourceType == (int)ProposalSourceType.Original)
            {
                #region 特殊標記
                if (ori.SpecialFlagText != pro.SpecialFlagText)
                {
                    Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);
                    Dictionary<ProposalSpecialFlag, string> oriSpecialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(ori.SpecialFlagText);
                    foreach (KeyValuePair<ProposalSpecialFlag, string> item in specialText)
                    {
                        if (oriSpecialText != null)
                        {
                            if (!oriSpecialText.ContainsKey(item.Key) || (oriSpecialText.ContainsKey(item.Key) && oriSpecialText[item.Key] != item.Value))
                            {
                                string log = string.Empty;
                                if (item.Key == ProposalSpecialFlag.Photographers)
                                {
                                    #region 需攝影Log處理
                                    if (!oriSpecialText.ContainsKey(item.Key))
                                    {
                                        if (item.Value.IndexOf('|') > 0)
                                        {
                                            for (int i = 0; i < item.Value.Split('|').Length; i++)
                                            {
                                                changeList.Add(string.Format("變更 {0} 為 {1}", "攝影註記-" + GetPhotoTitle(i), item.Value.Split('|')[i]));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string[] photostring = item.Value.Split('|');
                                        for (int i = 0; i < photostring.Length; i++)
                                        {
                                            if (oriSpecialText[item.Key].Split('|').Length >= i + 1)
                                            {
                                                if (photostring[i] != oriSpecialText[item.Key].Split('|')[i])
                                                {
                                                    changeList.Add(string.Format("變更 {0} 為 {1}", "攝影註記-" + GetPhotoTitle(i), photostring[i]));
                                                    GetPhotoTitle(i);
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    changeList.Add(string.Format("變更 {0} 為 {1}", Helper.GetLocalizedEnum(item.Key), item.Value));
                                }
                            }
                        }
                        else
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", Helper.GetLocalizedEnum(item.Key), item.Value));
                        }
                    }
                }
                #endregion

                #region 檔次特色
                if (ori.DealCharacterFlagText != pro.DealCharacterFlagText)
                {
                    Dictionary<ProposalDealCharacter, string> dealcharacterText = new JsonSerializer().Deserialize<Dictionary<ProposalDealCharacter, string>>(pro.DealCharacterFlagText);
                    Dictionary<ProposalDealCharacter, string> oriDealCharacterText = new JsonSerializer().Deserialize<Dictionary<ProposalDealCharacter, string>>(ori.DealCharacterFlagText);
                    foreach (KeyValuePair<ProposalDealCharacter, string> item in dealcharacterText)
                    {
                        if (oriDealCharacterText != null)
                        {
                            if (!oriDealCharacterText.ContainsKey(item.Key) || (oriDealCharacterText.ContainsKey(item.Key) && oriDealCharacterText[item.Key] != item.Value))
                            {
                                string log = string.Empty;
                                changeList.Add(string.Format("變更 {0} 為 {1}", Helper.GetLocalizedEnum(item.Key), item.Value));
                            }
                        }
                        else
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", Helper.GetLocalizedEnum(item.Key), item.Value));
                        }
                    }
                }
                #endregion

                #region 巿場分析比價
                if (ori.SaleMarketAnalysis != pro.SaleMarketAnalysis)
                {
                    if (!string.IsNullOrEmpty(pro.SaleMarketAnalysis))
                    {
                        ProposalMarketAnalysis pro_ma = new JsonSerializer().Deserialize<ProposalMarketAnalysis>(pro.SaleMarketAnalysis);
                        ProposalMarketAnalysis ori_ma = new JsonSerializer().Deserialize<ProposalMarketAnalysis>(ori.SaleMarketAnalysis);

                        if (pro_ma.GomajiPrice != (ori_ma != null ? ori_ma.GomajiPrice : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-Gomaji價格", pro_ma.GomajiPrice));
                        }

                        if (pro_ma.GomajiLink != (ori_ma != null ? ori_ma.GomajiLink : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-Gomaji連結", pro_ma.GomajiLink));
                        }

                        if (pro_ma.KuobrothersPrice != (ori_ma != null ? ori_ma.KuobrothersPrice : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-創業家兄弟價格", pro_ma.KuobrothersPrice));
                        }

                        if (pro_ma.KuobrothersLink != (ori_ma != null ? ori_ma.KuobrothersLink : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-創業家兄弟連結", pro_ma.KuobrothersLink));
                        }

                        if (pro_ma.CrazymikePrice != (ori_ma != null ? ori_ma.CrazymikePrice : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-瘋狂賣客價格", pro_ma.CrazymikePrice));
                        }

                        if (pro_ma.CrazymikeLink != (ori_ma != null ? ori_ma.CrazymikeLink : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-瘋狂賣客連結", pro_ma.CrazymikeLink));
                        }

                        if (pro_ma.EzPricePrice1 != (ori_ma != null ? ori_ma.EzPricePrice1 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-EzPrice比價網價格1", pro_ma.EzPricePrice1));
                        }

                        if (pro_ma.EzPriceLink1 != (ori_ma != null ? ori_ma.EzPriceLink1 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-EzPrice比價網連結1", pro_ma.EzPriceLink1));
                        }

                        if (pro_ma.EzPricePrice2 != (ori_ma != null ? ori_ma.EzPricePrice2 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-EzPrice比價網價格2", pro_ma.EzPricePrice2));
                        }

                        if (pro_ma.EzPriceLink2 != (ori_ma != null ? ori_ma.EzPriceLink2 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-EzPrice比價網連結2", pro_ma.EzPriceLink2));
                        }

                        if (pro_ma.EzPricePrice3 != (ori_ma != null ? ori_ma.EzPricePrice3 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-EzPrice比價網價格3", pro_ma.EzPricePrice3));
                        }

                        if (pro_ma.EzPriceLink3 != (ori_ma != null ? ori_ma.EzPriceLink3 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-EzPrice比價網連結3", pro_ma.EzPriceLink3));
                        }

                        if (pro_ma.PinglePrice1 != (ori_ma != null ? ori_ma.PinglePrice1 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-品購價格1", pro_ma.PinglePrice1));
                        }

                        if (pro_ma.PingleLink1 != (ori_ma != null ? ori_ma.PingleLink1 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-品購連結1", pro_ma.PingleLink1));
                        }

                        if (pro_ma.PinglePrice2 != (ori_ma != null ? ori_ma.PinglePrice2 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-品購價格2", pro_ma.PinglePrice2));
                        }

                        if (pro_ma.PingleLink2 != (ori_ma != null ? ori_ma.PingleLink2 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-品購連結2", pro_ma.PingleLink2));
                        }

                        if (pro_ma.PinglePrice3 != (ori_ma != null ? ori_ma.PinglePrice3 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-品購價格3", pro_ma.PinglePrice3));
                        }

                        if (pro_ma.PingleLink3 != (ori_ma != null ? ori_ma.PingleLink3 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-品購連結3", pro_ma.PingleLink3));
                        }

                        if (pro_ma.SimilarPrice != (ori_ma != null ? ori_ma.SimilarPrice : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-類似商品價格", pro_ma.SimilarPrice));
                        }

                        if (pro_ma.SimilarLink != (ori_ma != null ? ori_ma.SimilarLink : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-類似商品連結", pro_ma.SimilarLink));
                        }

                        if (pro_ma.OtherShop1 != (ori_ma != null ? ori_ma.OtherShop1 : "-1"))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-其他商店1", pro_ma.OtherShop1.Replace("pchome", "PChome 商店街").Replace("yahoo", "Yahoo!奇摩超級商城").Replace("taobao", "淘寶").Replace("-1", "請選擇")));
                        }

                        if (pro_ma.OtherPrice1 != (ori_ma != null ? ori_ma.OtherPrice1 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-其他商店價格1", pro_ma.OtherPrice1));
                        }

                        if (pro_ma.OtherLink1 != (ori_ma != null ? ori_ma.OtherLink1 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-其他商店連結1", pro_ma.OtherLink1));
                        }

                        if (pro_ma.OtherShop2 != (ori_ma != null ? ori_ma.OtherShop2 : "-1"))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-其他商店2", pro_ma.OtherShop2.Replace("pchome", "PChome 商店街").Replace("yahoo", "Yahoo!奇摩超級商城").Replace("taobao", "淘寶").Replace("-1", "請選擇")));
                        }

                        if (pro_ma.OtherPrice2 != (ori_ma != null ? ori_ma.OtherPrice2 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-其他商店價格2", pro_ma.OtherPrice2));
                        }

                        if (pro_ma.OtherLink2 != (ori_ma != null ? ori_ma.OtherLink2 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-其他商店連結2", pro_ma.OtherLink2));
                        }

                        if (pro_ma.OtherShop3 != (ori_ma != null ? ori_ma.OtherShop3 : "-1"))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-其他商店3", pro_ma.OtherShop3.Replace("pchome", "PChome 商店街").Replace("yahoo", "Yahoo!奇摩超級商城").Replace("taobao", "淘寶").Replace("-1", "請選擇")));
                        }

                        if (pro_ma.OtherPrice3 != (ori_ma != null ? ori_ma.OtherPrice3 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-其他商店價格3", pro_ma.OtherPrice3));
                        }

                        if (pro_ma.OtherLink3 != (ori_ma != null ? ori_ma.OtherLink3 : ""))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-其他商店連結3", pro_ma.OtherLink3));
                        }
                    }
                    else if (ori.SaleMarketAnalysis != null && string.IsNullOrEmpty(pro.SaleMarketAnalysis))
                    {
                        changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價", "\"空\""));
                    }
                }
                #endregion

                #region 媒體連結

                if (ori.MediaReportFlagText != pro.MediaReportFlagText)
                {
                    List<ProposalMediaReportLink> mediareportText = new JsonSerializer().Deserialize<List<ProposalMediaReportLink>>(pro.MediaReportFlagText);
                    List<string> mrf2 = new List<string>();

                    foreach (var media in mediareportText)
                    {
                        if (Helper.IsFlagSet(media.Type, (ProposalMediaReport)media.Type))
                        {
                            mrf2.Add(Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalMediaReport)media.Type) + " " + media.Link);
                        }
                    }
                    if (mrf2.Count() > 0)
                    {
                        changeList.Add(string.Format("變更 {0} 為 {1}", "媒體連結", string.Join("、", mrf2)));
                    }
                }


                #endregion
            }
            else if (pro.ProposalSourceType == (int)ProposalSourceType.House)
            {
                #region 巿場分析比價
                if (ori.SaleMarketAnalysis != pro.SaleMarketAnalysis)
                {
                    if (!string.IsNullOrEmpty(pro.SaleMarketAnalysis))
                    {
                        ProposalSaleMarketAnalysis pro_ma = new JsonSerializer().Deserialize<ProposalSaleMarketAnalysis>(pro.SaleMarketAnalysis);
                        ProposalSaleMarketAnalysis ori_ma = new JsonSerializer().Deserialize<ProposalSaleMarketAnalysis>(ori.SaleMarketAnalysis);

                        if ((ori_ma == null && !string.IsNullOrEmpty(new JsonSerializer().Serialize(pro_ma.Gomaji))) || (new JsonSerializer().Serialize(pro_ma.Gomaji) != new JsonSerializer().Serialize(ori_ma.Gomaji)))
                        {
                            if (pro_ma.Gomaji.Count() > 0)
                            {
                                foreach (var item in pro_ma.Gomaji)
                                {
                                    changeList.Add(string.Format("變更 {0} 為 價格 {1}，連結 {2}", "巿場分析比價-Gomaji", item.Price, item.Link));
                                }
                            }
                            else
                            {
                                changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-Gomaji", "無資料"));
                            }
                        }

                        if ((ori_ma == null && !string.IsNullOrEmpty(new JsonSerializer().Serialize(pro_ma.Kuobrothers))) || (new JsonSerializer().Serialize(pro_ma.Kuobrothers) != new JsonSerializer().Serialize(ori_ma.Kuobrothers)))
                        {
                            if (pro_ma.Kuobrothers.Count() > 0)
                            {
                                foreach (var item in pro_ma.Kuobrothers)
                                {
                                    changeList.Add(string.Format("變更 {0} 為 價格 {1}，連結 {2}", "巿場分析比價-創業家兄弟", item.Price, item.Link));
                                }
                            }
                            else
                            {
                                changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-創業家兄弟", "無資料"));
                            }
                        }

                        if ((ori_ma == null && !string.IsNullOrEmpty(new JsonSerializer().Serialize(pro_ma.Crazymike))) || (new JsonSerializer().Serialize(pro_ma.Crazymike) != new JsonSerializer().Serialize(ori_ma.Crazymike)))
                        {
                            if (pro_ma.Crazymike.Count() > 0)
                            {
                                foreach (var item in pro_ma.Crazymike)
                                {
                                    changeList.Add(string.Format("變更 {0} 為 價格 {1}，連結 {2}", "巿場分析比價-瘋狂賣客", item.Price, item.Link));
                                }
                            }
                            else
                            {
                                changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-瘋狂賣客", "無資料"));
                            }
                        }

                        if ((ori_ma == null && !string.IsNullOrEmpty(new JsonSerializer().Serialize(pro_ma.EzPrice))) || (new JsonSerializer().Serialize(pro_ma.EzPrice) != new JsonSerializer().Serialize(ori_ma.EzPrice)))
                        {
                            if (pro_ma.EzPrice.Count() > 0)
                            {
                                foreach (var item in pro_ma.EzPrice)
                                {
                                    changeList.Add(string.Format("變更 {0} 為 價格 {1}，連結 {2}", "巿場分析比價-EzPrice比價網", item.Price, item.Link));
                                }
                            }
                            else
                            {
                                changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-EzPrice比價網", "無資料"));
                            }
                        }

                        if ((ori_ma == null && !string.IsNullOrEmpty(new JsonSerializer().Serialize(pro_ma.Other))) || (new JsonSerializer().Serialize(pro_ma.Other) != new JsonSerializer().Serialize(ori_ma.Other)))
                        {
                            if (pro_ma.Other.Count() > 0)
                            {
                                foreach (var item in pro_ma.Other)
                                {
                                    changeList.Add(string.Format("變更 {0} 為 價格 {1}，連結 {2}", "巿場分析比價-其他", item.Price, item.Link));
                                }
                            }
                            else
                            {
                                changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-其他", "無資料"));
                            }
                        }

                        if ((ori_ma == null && !string.IsNullOrEmpty(new JsonSerializer().Serialize(pro_ma.SpecialQC))) || (new JsonSerializer().Serialize(pro_ma.SpecialQC) != new JsonSerializer().Serialize(ori_ma.SpecialQC)))
                        {
                            if (pro_ma.SpecialQC.Count() > 0)
                            {
                                foreach (var item in pro_ma.SpecialQC)
                                {
                                    changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-特別QC條件", item.Link));
                                }
                            }
                            else
                            {
                                changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價-特別QC條件", "無資料"));
                            }
                        }


                    }
                    else if (ori.SaleMarketAnalysis != null && string.IsNullOrEmpty(pro.SaleMarketAnalysis))
                    {
                        changeList.Add(string.Format("變更 {0} 為 {1}", "巿場分析比價", "\"空\""));
                    }
                }
                #endregion
            }



            if (changeList.Count > 0)
            {
                ProposalLog(pro.Id, string.Join("|", changeList), userName, type);
            }
        }

        private static string GetPhotoTitle(int i)
        {
            string title = string.Empty;
            switch (i)
            {
                case 0:
                    return "拍攝地點";
                case 1:
                    return "現場聯絡人電話";
                case 2:
                    return "現場聯絡人手機";
                case 3:
                    return "建議重點拍攝項目";
                case 4:
                    return "店家希望拍照時間";
                case 5:
                    return "參考網址";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// 上檔頻道新舊資料比較
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="oricategories"></param>
        public static void CompareProposalCategoryDeal(ProposalCategoryDealCollection categories, ProposalCategoryDealCollection oricategories, string userName)
        {
            List<string> changeList = new List<string>();
            List<string> categoryname = new List<string>();
            List<string> oricategoryname = new List<string>();

            foreach (var c in categories)
            {
                categoryname.Add(sp.CategoryGet(c.Cid).Name);
            }

            foreach (var oc in oricategories)
            {
                oricategoryname.Add(sp.CategoryGet(oc.Cid).Name);
            }

            if (string.Join("、", categoryname) != string.Join("、", oricategoryname))
            {
                changeList.Add(string.Format("變更 {0} 為 {1}", "上檔頻道", string.Join("、", categoryname)));
            }

            if (changeList.Count > 0)
            {
                if (categories.Count() == 0)
                {
                    ProposalLog(oricategories[0].Pid, string.Format("變更 {0} 為 {1}", "上檔頻道", "\"移除上檔頻道\""), userName);
                }
                else
                {
                    ProposalLog(categories[0].Pid, string.Join("、", changeList), userName);
                }
            }
        }

        /// <summary>
        /// ProposalCouponEventContent新舊資料比較
        /// </summary>
        /// <param name="pcec"></param>
        /// <param name="oriPcec"></param>
        /// <param name="userName"></param>
        public static void CompareProposalCouponEventContent(ProposalCouponEventContent pcec, ProposalCouponEventContent oriPcec, string userName)
        {
            List<string> changeList = new List<string>();
            List<string> changeListHide = new List<string>();
            foreach (var item in PponFacade.ColumnValueComparison(oriPcec, pcec))
            {
                changeList.Add(string.Format("變更 {0}", item.Key));
                changeListHide.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
            }



            if (changeList.Count > 0)
            {
                ProposalLog((int)pcec.ProposalId, string.Join("|", changeList), userName);
                ProposalLog((int)pcec.ProposalId, string.Join("|", changeListHide), userName, ProposalLogType.HTML);
            }
        }

        /// <summary>
        /// 儲存上檔頻道
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="newCategories"></param>
        /// <param name="userName"></param>
        public static void ProposalCategoryDealSave(Proposal pro, ProposalCategoryDealCollection newCategories, string userName)
        {
            ProposalCategoryDealCollection categories = sp.ProposalCategoryDealsGetList(pro.Id);
            ProposalCategoryDealCollection oricategories = categories.Clone();


            categories = newCategories;


            //宅配
            if (pro.DeliveryType == (int)DeliveryType.ToHouse)
            {
                //快速出貨
                if (pro.ShipType == (int)DealShipType.Ship72Hrs)
                {
                    //宅配
                    bool flag88 = false;
                    foreach (ProposalCategoryDeal c in categories)
                    {
                        if (c.Cid == 88)
                        {
                            flag88 = true;
                        }
                    }
                    if (!flag88)
                    {
                        categories.Add(new ProposalCategoryDeal
                        {
                            Pid = pro.Id,
                            Cid = 88
                        });
                    }
                    //24H出貨(串聯前台搜尋快速出貨)
                    bool flag139 = false;
                    foreach (ProposalCategoryDeal c in categories)
                    {
                        if (c.Cid == 139)
                        {
                            flag139 = true;
                        }
                    }
                    if (!flag139)
                    {
                        categories.Add(new ProposalCategoryDeal
                        {
                            Pid = pro.Id,
                            Cid = 139
                        });
                    }
                    //快速到貨(串聯後台24 iocn)                            
                    bool flag221 = false;
                    foreach (ProposalCategoryDeal c in categories)
                    {
                        if (c.Cid == 221)
                        {
                            flag221 = true;
                        }
                    }
                    if (!flag221)
                    {
                        categories.Add(new ProposalCategoryDeal
                        {
                            Pid = pro.Id,
                            Cid = 221
                        });
                    }
                }
            }

            CategoryCollection ccs = sp.CategoryGetAll(Category.Columns.Code);
            Category cc = ccs.Where(x => x.Name.Equals("超商取貨") && x.Type == (int)CategoryType.DealCategory).FirstOrDefault();

            bool isIsp = false;
            ProposalMultiDealCollection pmds = sp.ProposalMultiDealGetByPid(pro.Id);
            foreach (ProposalMultiDeal pmd in pmds)
            {
                int boxQuantityLimit = 0;
                int.TryParse(pmd.BoxQuantityLimit, out boxQuantityLimit);
                if (boxQuantityLimit > 0)
                {
                    isIsp = true;
                    break;
                }
            }
            if (isIsp && pro.IsWms == false)
            {
                if (cc != null)
                {
                    if (categories.Where(t => t.Cid == cc.Id).FirstOrDefault() == null)
                    {
                        categories.Add(new ProposalCategoryDeal()
                        {
                            Pid = pro.Id,
                            Cid = cc.Id
                        });
                    }
                }
            }
            //else
            //{
            //    if (cc != null)
            //    {
            //        ProposalCategoryDeal c = categories.Where(t => t.Cid == cc.Id).FirstOrDefault();
            //        if (c != null)
            //        {
            //            categories.Remove(c);
            //        }
            //    }
            //}

            sp.DeleteProposalCategoryDealsByCategoryType(pro.Id, CategoryType.PponChannel);
            sp.DeleteProposalCategoryDealsByCategoryType(pro.Id, CategoryType.PponChannelArea);
            sp.DeleteProposalCategoryDealsByCategoryType(pro.Id, CategoryType.DealCategory);
            sp.DeleteProposalCategoryDealsByCategoryType(pro.Id, CategoryType.DealSpecialCategory);
            sp.ProposalCategoryDealSetList(categories);

            if (!isIsp)
            {
                if (cc != null)
                {
                    ProposalCategoryDeal c = categories.Where(t => t.Cid == cc.Id).FirstOrDefault();
                    if (c != null)
                    {
                        sp.DeleteProposalCategoryDealsByCategoryType(pro.Id, cc.Id);
                    }
                }
            }


            CompareProposalCategoryDeal(categories, oricategories, userName);
        }

        /// <summary>
        /// App商品描述: 優惠方案、詳細介紹、商品規格
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="bid"></param>
        /// <param name="section">1:優惠方案 2: 詳細介紹 4: 商品規格 7: 全部</param>
        /// <returns></returns>
        public static string GetAppDescription(IViewPponDeal deal, Guid bid, int section = 7)
        {
            string appDeascription = string.Empty;

            #region header
            appDeascription += string.Format(@"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
                        <head>
                            <title>17Life</title>
                            <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
                            <meta name='viewport' content='width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />

                            <link href='{0}/Themes/PCweb/css/MasterPage.css' rel='stylesheet' type='text/css' />
                            <link href='{0}/Themes/PCweb/css/ppon.css' rel='stylesheet' type='text/css' />
                            <link href='{0}/Themes/PCweb/plugin/font-awesome/font-awesome.css' rel='stylesheet' type='text/css' />
                            <link href='{0}/Themes/PCweb/css/RDL-L.css' rel='stylesheet' type='text/css' />
                            <link href='{0}/Themes/PCweb/css/RDL-M.css' rel='stylesheet' type='text/css' />
                            <link href='{0}/Themes/PCweb/css/RDL-S.css' rel='stylesheet' type='text/css' />
                            <link href='{0}/Themes/PCweb/css/ppon_item_min.css?0.6' rel='stylesheet' type='text/css' />
                            <script src='{0}/Tools/js/jquery-1.9.1.min.js'></script>
                        </ head>
                        <body style='line-height: normal;'> ", config.SiteUrl);
            #endregion header

            if (Helper.IsFlagSet(section, AppDescriptionSection.PlanSelection))
            {
                #region 優惠方案
                appDeascription += @"<div class='EntryTitle PublicwelfareTBk'>
                            <a name='DealDetail'></ a >
                                    優惠方案
                                </div> ";
                appDeascription += @"<div class='Multi-grade-Setting content-menu' style=''>
                                    <div id='div_combodeallist2'>";

                var vcdCol = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(bid);
                if (vcdCol == null || vcdCol.Count == 0)
                {
                    //單檔
                    IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
                    //bool isSoldOut = DateTime.Now >= theDeal.BusinessHourOrderTimeE || theDeal.OrderedQuantity >= theDeal.OrderTotalLimit.GetValueOrDefault();
                    string discount = theDeal.ItemOrigPrice > 0
                        ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                            theDeal.ItemPrice, theDeal.ItemOrigPrice, false)
                        : string.Empty;
                    string origPrice = theDeal.ItemOrigPrice.ToString("N0");
                    string freight = GetDealFreight(theDeal);
                    string averagePrice = GetAveragePriceLab(theDeal);
                    string price = ChecksubComboDealItemPrice(theDeal);

                    appDeascription += GetDealInfoHtml(theDeal, theDeal.ItemName, discount, origPrice, freight, averagePrice, price);
                }
                else
                {
                    //母子檔
                    foreach (ViewComboDeal vcd in vcdCol)
                    {
                        IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vcd.BusinessHourGuid, true);
                        if (theDeal.IsExpiredDeal.GetValueOrDefault(false))
                        {
                            continue;
                        }
                        //bool isSoldOut = DateTime.Now >= theDeal.BusinessHourOrderTimeE || theDeal.OrderedQuantity >= theDeal.OrderTotalLimit.GetValueOrDefault();
                        string discount = theDeal.ItemOrigPrice > 0
                            ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                                theDeal.ItemPrice, theDeal.ItemOrigPrice, false)
                            : string.Empty;
                        string origPrice = theDeal.ItemOrigPrice.ToString("N0");
                        string freight = GetDealFreight(theDeal);
                        string averagePrice = GetAveragePriceLab(vcd);
                        string price = ChecksubComboDealItemPrice(theDeal);

                        appDeascription += GetDealInfoHtml(theDeal, theDeal.ItemName, discount, origPrice, freight, averagePrice, price);
                    }
                }

                appDeascription += @"</div>
                                </div>";
                #endregion
            }

            if (Helper.IsFlagSet(section, AppDescriptionSection.DetailDescription))
            {
                #region 詳細介紹

                string description = string.Empty;
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                if (vpd.Cchannel.GetValueOrDefault(false) && !string.IsNullOrEmpty(vpd.CchannelLink))
                {
                    description += PponFacade.GetCchannelLink(vpd.CchannelLink);
                    description += "<div style='height:20px'></div>";
                }
                description += ViewPponDealManager.DefaultManager.GetDealDescription(vpd);
                description = PponFacade.ReplaceMobileYoutubeTag(description);
                if (!config.EnableNewAppPponDescriptionVersion && section == 7)
                {
                    appDeascription += @"<div class='EntryTitle PublicwelfareTBk'>
                                <a name='DealDetail'></ a >
                                     詳細介紹
                                 </div> ";
                }
                appDeascription += "<div class='EntryZone'><div><div id='detailDesc'>";
                appDeascription += description;
                appDeascription += "</div></div></div>";

                #endregion
            }


            if (Helper.IsFlagSet(section, AppDescriptionSection.ProductSpec))
            {
                #region 商品規格

                if (!string.IsNullOrEmpty(deal.ProductSpec))
                {
                    if (!config.EnableNewAppPponDescriptionVersion && section == 7)
                    {
                        appDeascription += @"<div class='EntryTitle PublicwelfareTBk'>
                                <a name='DealDetail'></ a >
                                     商品規格
                                 </div> ";
                    }
                    appDeascription += "<div class='EntryZone'><div id='DetailinnerM'><div id='detailDescM'>";
                    appDeascription += deal.ProductSpec;
                    appDeascription += "</div></div></div>";
                }

                #endregion
            }

            appDeascription += @"<script type=""text/javascript"">
                            var winWidth = $(window).width();
                            if (winWidth < 560)
                            {
                                var _iframe = $('iframe');
                                $.each(_iframe, function(idx, obj) {
                                    var _iw = $(obj).attr('width');
                                    var _ih = $(obj).attr('height');
                                    if (_iw > winWidth)
                                    {
                                        var w = ((_iw - (winWidth - 50)) / _iw);
                                        var h = _ih - (_ih * w);
                                        $(obj).attr('width', winWidth - 50);
                                        $(obj).attr('height', parseInt(h));
                                    }
                                })
                            }
                            </script>
                            </body>";

            return appDeascription;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetAppDescription()
        {
            return "";
        }

        private static string GetDealInfoHtml(
            IViewPponDeal theDeal,
            string itemName, string discount, string origPrice, string freight,
            string averagePrice, string price)
        {
            string result = string.Empty;
            result += string.Format(@"<div class='mgs-item-box' style='cursor: default'>
                                            <div class=''>
                                                <div class='mgs-content'>
                                                    <div class='mgs-item-title'>{0}{1}</div>

                                                    <div class='mgs-item-state'>
                                                        <div class='mgs-sale rdl-a2wdis'>{2}</div>
                                                        <div class='mgs-sale oriprice'>原價 $ {3}</div>",
                                                        ((theDeal.EnableIsp) ? "<span class='title-tag'>可超商取貨</span>" : string.Empty),
                                                itemName,
                                    discount,
                                    origPrice);
            if (!string.IsNullOrEmpty(freight))
            {
                result += "<div class='mgs-sale'>" + freight + "</div>";
            }

            result += string.Format(@"<div class='mgs-buyer mgs-brcolor-org'>均價&nbsp;{0}</div>
                                                    </div>
                                                </div>
                                                <div class='mgs-price-box'>
                                                    <div class='mgs-price '>
                                                        {1}
                                                    </div>
                                                </div>
                                                <div class='clearfix'>
                                                </div>
                                            </div>
                                        </div>",
                                     averagePrice,
                                     price);
            return result;
        }
        #region App優惠內容版型
        private static string GetAveragePriceTag(ViewComboDeal deal)
        {
            return IsShowAveragePriceInfo(deal) ? "均價" : string.Empty;
        }
        private static string GetAveragePriceTag(IViewPponDeal theDeal)
        {
            return IsShowAveragePriceInfo(theDeal) ? "均價" : string.Empty;
        }

        private static bool IsShowAveragePriceInfo(ViewComboDeal deal)
        {
            return (deal.IsAveragePrice && deal.QuantityMultiplier != null && deal.QuantityMultiplier >= 1);
        }
        private static bool IsShowAveragePriceInfo(IViewPponDeal deal)
        {
            return ((deal.IsAveragePrice ?? false) && deal.QuantityMultiplier != null && deal.QuantityMultiplier >= 1);
        }
        private static string GetAveragePriceLab(ViewComboDeal deal)
        {
            var averagePrice = CheckZeroPriceToShowAveragePrice(deal);
            var isGroupCouponB = deal.GroupCouponDealType == (int)GroupCouponDealType.CostAssign;
            return (IsShowAveragePriceInfo(deal) && averagePrice > 0) ? "$" + averagePrice.ToString("F0") + (isGroupCouponB ? "起" : string.Empty) : string.Empty;
        }
        private static string GetAveragePriceLab(IViewPponDeal theDeal)
        {
            var averagePrice = CheckZeroPriceToShowAveragePrice(theDeal);
            var isGroupCouponB = theDeal.GroupCouponDealType == (int)GroupCouponDealType.CostAssign;
            return (IsShowAveragePriceInfo(theDeal) && averagePrice > 0) ? "$" + averagePrice.ToString("F0") + (isGroupCouponB ? "起" : string.Empty) : string.Empty;
        }
        private static decimal CheckZeroPriceToShowAveragePrice(ViewComboDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                return deal.ItemPrice;
            }
            else
            {
                if (IsShowAveragePriceInfo(deal))
                {
                    return Math.Ceiling(deal.ItemPrice / deal.QuantityMultiplier.Value);
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
        }
        private static decimal CheckZeroPriceToShowAveragePrice(IViewPponDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                return deal.ItemPrice;
            }
            else
            {
                if (IsShowAveragePriceInfo(deal))
                {
                    return Math.Ceiling(deal.ItemPrice / deal.QuantityMultiplier.Value);
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
        }
        private static string GetDealFreight(ViewComboDeal vcd)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vcd.BusinessHourGuid, true);
            return GetDealFreight(deal);
        }
        private static string GetDealFreight(IViewPponDeal deal)
        {
            if (deal == null || deal.FreightAmount == 0)
            {
                return string.Empty;
            }
            return string.Format("| 運費 ${0}", (int)deal.FreightAmount);
        }
        /// <summary>
        /// 多檔次均價版型售價顯示
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        private static string ChecksubComboDealItemPrice(ViewComboDeal deal)
        {
            if (deal.BusinessHourOrderTimeE > DateTime.Now)
            {
                if (deal.OrderedQuantity < (deal.OrderTotalLimit ?? 0))
                {
                    return "$" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                }
                else
                {
                    return "售完 $" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                }
            }
            else
            {
                return "售完 $" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                ;
            }
        }
        private static string ChecksubComboDealItemPrice(IViewPponDeal deal)
        {
            if (deal.BusinessHourOrderTimeE > DateTime.Now)
            {
                if (deal.OrderedQuantity < (deal.OrderTotalLimit ?? 0))
                {
                    return "$" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                }
                else
                {
                    return "售完 $" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                }
            }
            else
            {
                return "售完 $" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                ;
            }
        }
        private static decimal CheckZeroPriceToShowPrice(IViewPponDeal deal)
        {
            //若是全家檔次須將CityId改成全家CityId
            if ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)
            {
                return PponFacade.CheckZeroPriceToShowPrice(deal, PponCityGroup.DefaultPponCityGroup.Family.CityId);
            }
            else
            {
                return PponFacade.CheckZeroPriceToShowPrice(deal);
            }
        }

        private static decimal CheckZeroPriceToShowPrice(ViewComboDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                if ((deal.BusinessHourStatus & (int)BusinessHourStatus.PriceZeorShowOriginalPrice) > 0)
                {
                    return deal.ItemOrigPrice;
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
            else
            {
                return deal.ItemPrice;
            }
        }
        #endregion App優惠內容版型
        /// <summary>
        /// 刪除提案單相關table
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="bid"></param>
        /// <param name="userName"></param>
        public static void DeleteProposal(int pid, Guid? bid, string userName)
        {

            sp.ProposalDelete(pid);
            sp.ProposalCategoryDealsDelete(pid);
            sp.ProposalLogDeleteList(pid);
            pp.ProposalStoreDelete(pid);
            pp.ProposalCouponEventContentDelete(pid);
            sp.ProposalMultiDealDeleteByPid(pid);
            if (bid != null)
            {
                Guid _bid;
                Guid.TryParse(bid.ToString(), out _bid);
                sp.ProposalMultiOptionSpecDelete(_bid);
            }

            sp.ProposalAssignLogDelete(pid);

            //[proposal_file_oauth]
            //[proposal_contract_files]
            //[proposal_seller_files]
            //[proposal_return_detail] ??
            ProposalLog(pid, "刪除提案單", userName);
        }

        public static string GetProposalRelatedItemName(ProposalRelatedDealModel model)
        {
            return string.Format("{0}({1})", model.ItemName, model.ShipTypeDesc);
        }

        public static List<ProposalRelatedDealModel> GetRelatedDeal(int pid)
        {
            Proposal ori_pro = sp.ProposalGet(pid);
            List<ProposalRelatedDealModel> list = new List<ProposalRelatedDealModel>();
            if (ori_pro.IsLoaded)
            {
                ProposalRelatedDealCollection prds = sp.ProposalRelatedDealGetByPid(ori_pro.Id);
                foreach (ProposalRelatedDeal prd in prds)
                {
                    Proposal rdpro = sp.ProposalGet(prd.RelatedId);
                    if (rdpro.IsLoaded)
                    {
                        if (rdpro.BusinessHourGuid != null)
                        {
                            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(rdpro.BusinessHourGuid.Value);
                            if (theDeal.IsLoaded)
                            {
                                //已建檔
                                if (Helper.IsFlagSet(theDeal.GroupOrderStatus.GetValueOrDefault(0), GroupOrderStatus.Completed) || theDeal.Slug != null)
                                {
                                    //已結檔
                                    list.Add(new ProposalRelatedDealModel
                                    {
                                        ProposalId = rdpro.Id,
                                        BusinessHourGuid = theDeal.BusinessHourGuid,
                                        ItemName = theDeal.ItemName,
                                        ShipTypeDesc = GetProposalShipTypeDesc(rdpro),
                                        DealStatusDesc = "已結檔",
                                        IsShow = false
                                    });
                                }
                                else if (theDeal.BusinessHourOrderTimeS.Year == DateTime.MaxValue.Year)
                                {
                                    list.Add(new ProposalRelatedDealModel
                                    {
                                        ProposalId = rdpro.Id,
                                        BusinessHourGuid = theDeal.BusinessHourGuid,
                                        ItemName = theDeal.ItemName,
                                        ShipTypeDesc = GetProposalShipTypeDesc(rdpro),
                                        DealStatusDesc = "尚未開賣",
                                        IsShow = false
                                    });
                                }
                                else if (theDeal.BusinessHourOrderTimeS >= DateTime.Now || theDeal.BusinessHourOrderTimeE <= DateTime.Now)
                                {
                                    list.Add(new ProposalRelatedDealModel
                                    {
                                        ProposalId = rdpro.Id,
                                        BusinessHourGuid = theDeal.BusinessHourGuid,
                                        ItemName = theDeal.ItemName,
                                        ShipTypeDesc = GetProposalShipTypeDesc(rdpro),
                                        DealStatusDesc = "已結檔",
                                        IsShow = false
                                    });
                                }
                                else
                                {
                                    //尚未結檔
                                    DealTimeSlotCollection dts = pp.DealTimeSlotGetAllCol(theDeal.BusinessHourGuid);
                                    bool isRunning = dts.Where(x => x.Status == (int)DealTimeSlotStatus.Default && x.EffectiveStart <= DateTime.Now && x.EffectiveEnd >= DateTime.Now).Count() > 0;
                                    if (isRunning)
                                    {
                                        list.Add(new ProposalRelatedDealModel
                                        {
                                            ProposalId = rdpro.Id,
                                            BusinessHourGuid = theDeal.BusinessHourGuid,
                                            ItemName = theDeal.ItemName,
                                            ShipTypeDesc = GetProposalShipTypeDesc(rdpro),
                                            DealStatusDesc = "上架銷售",
                                            IsShow = true
                                        });
                                    }
                                    else
                                    {
                                        list.Add(new ProposalRelatedDealModel
                                        {
                                            ProposalId = rdpro.Id,
                                            BusinessHourGuid = theDeal.BusinessHourGuid,
                                            ItemName = theDeal.ItemName,
                                            ShipTypeDesc = GetProposalShipTypeDesc(rdpro),
                                            DealStatusDesc = "暫停銷售",
                                            IsShow = false
                                        });
                                    }
                                }
                            }
                            else
                            {
                                //未建檔
                                list.Add(new ProposalRelatedDealModel
                                {
                                    ProposalId = rdpro.Id,
                                    BusinessHourGuid = Guid.Empty,
                                    ItemName = (!string.IsNullOrEmpty(rdpro.BrandName) ? string.Format("【{0}】", rdpro.BrandName) : string.Empty) + rdpro.DealName,
                                    ShipTypeDesc = GetProposalShipTypeDesc(rdpro),
                                    DealStatusDesc = "-",
                                    IsShow = false
                                });
                            }
                        }
                        else
                        {
                            list.Add(new ProposalRelatedDealModel
                            {
                                ProposalId = rdpro.Id,
                                BusinessHourGuid = Guid.Empty,
                                ItemName = (!string.IsNullOrEmpty(rdpro.BrandName) ? string.Format("【{0}】", rdpro.BrandName) : string.Empty) + rdpro.DealName,
                                ShipTypeDesc = GetProposalShipTypeDesc(rdpro),
                                DealStatusDesc = "-",
                                IsShow = false
                            });
                        }
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// 取得出貨類型
        /// </summary>
        /// <returns></returns>
        private static string GetProposalShipTypeDesc(Proposal pro)
        {
            string desc = string.Empty;
            //出貨類型
            if (pro.ShipType == (int)DealShipType.Ship72Hrs)
            {
                desc = "購買成功日後24小時內出貨";
            }
            else if (pro.ShipType == (int)DealShipType.Normal)
            {
                desc = string.Format("購買成功日後{0}天內出貨", pro.ShipText3);
            }
            else if (pro.ShipType == (int)DealShipType.Other)
            {
                if (pro.DeliveryTimeS != null && pro.DeliveryTimeE != null)
                {
                    desc = string.Format("{0}~{1}出貨", pro.DeliveryTimeS.Value.ToString("yyyy/MM/dd"), pro.DeliveryTimeE.Value.ToString("yyyy/MM/dd"));
                }
            }

            return desc;
        }

        public static void SendWmsPurchaseOrderMail(List<Guid> orderIdList, bool apply, bool? result, bool reject = false)
        {
            ViewWmsPurchaseOrderCollection vwpoc = wp.ViewWmsPurchaseOrderListGetByPurchaseOrderGuid(orderIdList);
            List<Guid> sellerGuid = new List<Guid>();
            List<string> mailToUser = new List<string>();
            List<string> mailCCUser = new List<string> { "elin@17life.com" };
            string subject = string.Empty;
            string content = string.Empty;
            string subContent = string.Empty;
            if (apply)
            {
                subject = "17Life【商品進倉單】送審通知";
            }
            else
            {
                if (!reject)
                {
                    subject = "17Life【商品進倉單】審核" + (result.Value ? "通過" : "未過") + "通知";

                    if (result.Value)
                    {
                        subContent = "您向17Life申請送審的商品進倉單已經審核通過，請您儘快協助至17Life商家系統中的<a href='" + config.SSLSiteUrl + "/vbs/ship/WmsPrint' target=_blank>列印進倉單麥頭與二聯單：Step 02. 列印進倉單麥頭與二聯單</a>，依照系統提供的操作流程，依序下載、列印、貼標、出貨等程序完成商品入倉";
                    }
                    else
                    {
                        subContent = "您向17Life申請送審的商品進倉單審核結果為未通過，無法依申請商品與數量進行入倉。您可至17Life商家系統中的<a href='" + config.SSLSiteUrl + "/vbs/ship/WmsPrint' target=_blank>列印進倉單麥頭與二聯單：Step 02. 列印進倉單麥頭與二聯單</a>查看相關資訊。若您對審核結果有任何問題，皆可與您的服務人員聯繫，或嘗試送出另一個新的進倉單申請。";
                    }
                }
                else
                {
                    subject = "17Life【商品進倉單】申請未過通知";
                    subContent = "您向17Life申請的進倉單申請結果為未通過。若您對申請結果有任何問題，皆可與您的服務人員聯繫，或嘗試送出另一個新進倉單申請";
                }
            }


            foreach (ViewWmsPurchaseOrder vwpo in vwpoc)
            {
                if (apply)
                {
                    //廠商->業務
                    ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(vwpo.SalesId);
                    content = "親愛的 17Lifer 您好: <br/><br/> 您的商家：" + vwpo.SellerName + "，於" + DateTime.Now.ToString("yyyy/MM/dd") + "，向您申請商品進倉單送審，請您儘快協助至17Life業務系統中的<a href='" + config.SSLSiteUrl + "/sal/proposal/WmsManage' target=_blank>進倉單管理</a>，進行審核，以利後續商家進行商品入倉準備。";
                    SendEmail(new List<string> { deSalesEmp.Email }, subject, content, mailCCUser);
                }
                else
                {
                    //業務->廠商
                    if (!sellerGuid.Contains(vwpo.SellerGuid))
                    {
                        #region 商家聯絡人
                        Seller seller = sp.SellerGet(vwpo.SellerGuid);

                        List<string> mailList = new List<string>();
                        string sellerEmail = string.Empty;
                        List<string> sellerMails = new List<string>();

                        List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                        if (contacts != null)
                        {
                            foreach (var c in contacts)
                            {
                                if (c.Type == ((int)ProposalContact.Normal).ToString())
                                {
                                    if (!string.IsNullOrEmpty(c.ContactPersonEmail))
                                    {
                                        if (!sellerMails.Contains(c.ContactPersonEmail))
                                        {
                                            sellerMails.Add(c.ContactPersonEmail);
                                        }
                                    }
                                }
                            }
                        }

                        sellerEmail = string.Join(",", sellerMails);

                        var mailTo = RegExRules.CheckMultiEmail(sellerEmail);
                        if (string.IsNullOrEmpty(mailTo))
                        {
                            string errorMsg = string.Format("商家:{0}<a href='{1}'>{2}</a> 聯絡人mail有誤，無法發送商品進倉單審核通知。聯絡人Email: \"{3}\"", seller.SellerId, config.SiteUrl + "/sal/SellerContent.aspx?sid=" + seller.Guid, seller.SellerName, sellerEmail);
                            try
                            {
                                EmailFacade.SendErrorEmail(seller.Guid, errorMsg);
                            }
                            catch { }

                            logger.Warn(errorMsg);
                        }
                        else
                        {
                            mailList.AddRange(mailTo.Split(","));
                        }
                        #endregion

                        content = "親愛的 " + vwpo.SellerName + " 您好：<br/><br/>" + subContent;
                        sellerGuid.Add(vwpo.SellerGuid);
                        SendEmail(mailList, subject, content, mailCCUser);
                    }
                }
            }
        }

        public static void SendWmsReturnOrderMail(List<Guid> orderIdList, bool apply, bool? result, bool reject = false)
        {
            ViewWmsReturnOrderCollection vwpoc = wp.ViewWmsReturnOrderListGetByReturnOrderGuid(orderIdList);
            List<Guid> sellerGuid = new List<Guid>();
            List<string> mailToUser = new List<string>();
            List<string> mailCCUser = new List<string> { "elin@17life.com" };
            string subject = string.Empty;
            string content = string.Empty;
            string subContent = string.Empty;
            if (apply)
            {
                subject = "17Life【還貨單】送審通知";
            }
            else
            {
                if (!reject)
                {
                    subject = "17Life【還貨單】審核" + (result.Value ? "通過" : "未過") + "通知";

                    if (result.Value)
                    {
                        subContent = "您向17Life申請送審的商品還貨單已經審核通過，已將您的申請還貨資料送至倉庫，進行還貨流程；近日內將有宅配人員連續配送事宜，請隨時注意。您亦可至商家系統注意還貨進度與動向。";
                    }
                    else
                    {
                        subContent = "您向17Life申請送審的商品還貨單審核結果為未通過。若您對審核結果有任何問題，皆可與您的服務人員聯繫，或嘗試送出另一個新的還貨單申請。";
                    }
                }
                else
                {
                    subject = "17Life【還貨單】申請未過通知";
                    subContent = "您向17Life申請的還貨單申請結果為未通過。若您對申請結果有任何問題，皆可與您的服務人員聯繫，或嘗試送出另一個新的還貨單申請。";
                }
            }


            foreach (ViewWmsReturnOrder vwpo in vwpoc)
            {
                if (apply)
                {
                    //廠商->業務
                    ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(vwpo.SalesId);
                    content = "親愛的 17Lifer 您好: <br/><br/> 您的商家：" + vwpo.SellerName + "，於" + DateTime.Now.ToString("yyyy") + "年" + DateTime.Now.ToString("MM") + "月" + DateTime.Now.ToString("dd") + "日，向您申請商品還貨單送審，請您儘快協助至17Life業務系統中的<a href='" + config.SSLSiteUrl + "/sal/proposal/WmsReturn' target=_blank>還貨單管理</a>，進行審核，以利後續倉庫進行還貨作業。";
                    SendEmail(new List<string> { deSalesEmp.Email }, subject, content, mailCCUser);
                }
                else
                {
                    //業務->廠商
                    if (!sellerGuid.Contains(vwpo.SellerGuid))
                    {
                        #region 商家聯絡人
                        Seller seller = sp.SellerGet(vwpo.SellerGuid);

                        List<string> mailList = new List<string>();
                        string sellerEmail = string.Empty;
                        List<string> sellerMails = new List<string>();

                        List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                        if (contacts != null)
                        {
                            foreach (var c in contacts)
                            {
                                if (c.Type == ((int)ProposalContact.Normal).ToString())
                                {
                                    if (!string.IsNullOrEmpty(c.ContactPersonEmail))
                                    {
                                        if (!sellerMails.Contains(c.ContactPersonEmail))
                                        {
                                            sellerMails.Add(c.ContactPersonEmail);
                                        }
                                    }
                                }
                            }
                        }

                        sellerEmail = string.Join(",", sellerMails);

                        var mailTo = RegExRules.CheckMultiEmail(sellerEmail);
                        if (string.IsNullOrEmpty(mailTo))
                        {
                            string errorMsg = string.Format("商家:{0}<a href='{1}'>{2}</a> 聯絡人mail有誤，無法發送商品進倉單審核通知。聯絡人Email: \"{3}\"", seller.SellerId, config.SiteUrl + "/sal/SellerContent.aspx?sid=" + seller.Guid, seller.SellerName, sellerEmail);
                            try
                            {
                                EmailFacade.SendErrorEmail(seller.Guid, errorMsg);
                            }
                            catch { }

                            logger.Warn(errorMsg);
                        }
                        else
                        {
                            mailList.AddRange(mailTo.Split(","));
                        }
                        #endregion

                        content = "親愛的 " + vwpo.SellerName + " 您好：<br/><br/>" + subContent;
                        sellerGuid.Add(vwpo.SellerGuid);
                        SendEmail(mailList, subject, content, mailCCUser);
                    }
                }
            }
        }
        public static Dictionary<int, string> GetAgentChannels()
        {
            Dictionary<int, string> flagAgentChannels = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(AgentChannel)))
            {
                if ((AgentChannel)item == AgentChannel.NONE ||
                    (AgentChannel)item == AgentChannel.ASUS ||
                    (AgentChannel)item == AgentChannel.BuyerToBoss ||
                    (AgentChannel)item == AgentChannel.LionItri ||
                    (AgentChannel)item == AgentChannel.TaiShinMall ||
                    (AgentChannel)item == AgentChannel.CChannel)
                {
                    continue;
                }
                flagAgentChannels[(int)item] = Helper.GetDescription((AgentChannel)item);
            }

            return flagAgentChannels;
        }

        public static List<int> GetAgentChannels(string ori)
        {
            List<int> result = new List<int>();
            if (!string.IsNullOrEmpty(ori))
            {
                string[] list = ori.Split(",");

                foreach (string li in list)
                {
                    int s = 0;
                    if (int.TryParse(li, out s))
                    {
                        result.Add(s);
                    }
                }
            }
            return result;
        }
    }



    public enum AppDescriptionSection
    {
        None = 0,
        /// <summary>
        /// 方案選擇
        /// </summary>
        PlanSelection = 1,
        /// <summary>
        /// 詳細介紹
        /// </summary>
        DetailDescription = 2,
        /// <summary>
        /// 產品規格
        /// </summary>
        ProductSpec = 4
    }


}

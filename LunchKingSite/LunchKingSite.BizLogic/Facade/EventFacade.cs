﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models;
using LunchKingSite.Core.Models.PponEntities;
using SMS = LunchKingSite.BizLogic.Component.SMS;
using LunchKingSite.BizLogic.Models.LimitedTimeSelection;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.API;

namespace LunchKingSite.BizLogic.Facade
{
    public class EventFacade
    {
        private static ISysConfProvider config;
        private static ISerializer serializer;
        private static IEventProvider ep;
        private static IEventEntityProvider eep;
        private static IOrderProvider op;
        private static ICmsProvider cmp;
        private static IPponEntityProvider pep;

        static EventFacade()
        {
            config = ProviderFactory.Instance().GetConfig();
            serializer = ProviderFactory.Instance().GetSerializer();
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            eep = ProviderFactory.Instance().GetProvider<IEventEntityProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            cmp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
        }

        #region AppDownloadSMSLog

        public static string AppDownloadSendSMS(string phoneNo, string ipAddress)
        {
            AppDownloadSmsLogCollection logs = ep.AppDownloadSmsLogGetList(phoneNo, System.DateTime.Now.AddDays(-Convert.ToDouble(config.AppDownloadSMSCheckDays)), System.DateTime.Now);

            if (logs.Count(n => n.IsSendSuccess == true) >= config.AppDownloadSMSCheckSendTimes)
            {
                return new LunchKingSite.Core.JsonSerializer().Serialize(new { Success = false, Message = "此手機號已接收過下載簡訊了唷。" });
            }

            log4net.ILog log = log4net.LogManager.GetLogger("SMS");

            string returnMessage = string.Empty;
            AppDownloadSmsLog smsLog = new AppDownloadSmsLog();
            smsLog.CreateTime = System.DateTime.Now;
            smsLog.PhoneNo = phoneNo;
            smsLog.Ip = ipAddress;
            smsLog.IsSendSuccess = false;

            try
            {
                if (config.IsSmsSend)
                {
                    SMS sms = new SMS();
                    string showMessage = string.Format("吃喝玩樂3折起，優惠帶著走！立即下載17Life一起生活省錢APP：http://x.co/4r1I7");
                    sms.SendMessage("APP下載簡訊", showMessage, "", phoneNo, "APP下載簡訊", Core.SmsType.System, Guid.Empty, "0");
                    smsLog.IsSendSuccess = true;
                    returnMessage = "發送成功，請稍待收取簡訊。";
                }
                else
                {
                    returnMessage = "目前無法發送簡訊，請稍後再試。";
                }
            }
            catch (Exception ex)
            {
                smsLog.IsSendSuccess = false;
                log.InfoFormat("APP下載簡訊發送錯誤 phone:{0}, ip:{1}, message:{2}, trace:{3}", phoneNo, ipAddress, ex.Message, ex.StackTrace);
                returnMessage = "簡訊發送失敗，請稍後再試。";
            }
            finally
            {
                ep.AppDownloadSmsLogSet(smsLog);
            }

            return new LunchKingSite.Core.JsonSerializer().Serialize(new { Success = smsLog.IsSendSuccess, Message = returnMessage });
        }

        #endregion

        #region EventPremiumsPromo

        public static void GreetingPremiumSuccessMail(EventPremiumPromo eventPremiumPromo, EventPremiumPromoItem eventPremiumItem)
        {
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                mail.To.Add(eventPremiumItem.AwardEmail);
                mail.Subject = "恭喜您~ " + "已免費索取" + eventPremiumPromo.PremiumName + "！";

                PremiumSuccessMail content = TemplateFactory.Instance().GetTemplate<PremiumSuccessMail>();
                content.SiteUrl = config.SiteUrl;
                content.SiteServiceUrl = config.SiteServiceUrl;
                content.PremiumName = eventPremiumPromo.PremiumName;

                mail.Body = content.ToString();
                mail.IsBodyHtml = true;
                PostMan.Instance().Send(mail, SendPriorityType.Immediate);
            }

        }


        #endregion EventPremiumsPromo
        /// <summary>
        /// 取得目前的第1筆符合的活動
        /// 有cache機制
        /// </summary>
        /// <returns></returns>
        public static EventActivity GetCurrentEventActivity()
        {
            EventActivity activity;
            if (HttpContext.Current.Cache[config.EdmPopUpCacheName] == null)
            {
                HttpContext.Current.Cache.Remove(config.EdmPopUpCacheName);
                var activities = ep.EventActivityEdmGetList(-1).Where(x => x.StartDate <= DateTime.Now && DateTime.Now <= x.EndDate);
                if (activities.Any())
                {
                    activity = activities.First();
                    HttpContext.Current.Cache.Insert(config.EdmPopUpCacheName, activity, null,
                        DateTime.Now.AddMinutes(5), TimeSpan.Zero);
                }
                else
                {
                    activity = new EventActivity()
                    {
                        Type = (int)EventActivityType.PopUp,
                        EdmBg1 = config.SiteUrl + "/Themes/default/images/17Life/Gactivities/EMS/EMS_1.jpg",
                        EdmBg2 = config.SiteUrl + "/Themes/default/images/17Life/Gactivities/EMS/EMS_2.jpg",
                        EdmDefaultCity = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId,
                        ExcludeCpaList = string.Empty
                    };
                    HttpContext.Current.Cache.Insert(config.EdmPopUpCacheName, activity, null,
                        DateTime.Now.AddMinutes(5), TimeSpan.Zero);
                }
            }
            else
            {
                activity = (EventActivity)HttpContext.Current.Cache[config.EdmPopUpCacheName];
            }
            return activity;
        }

        public static EventActivity GetEventActivity(int eventActivityId)
        {
            var eventActivity = ep.EventActivityGet(eventActivityId);
            return eventActivity.Id == 0 ? null : eventActivity;
        }
        /// <summary>
        /// 取得目前的app蓋版廣告，給Api用
        /// </summary>
        /// <returns></returns>
        public static ApiEventNews GetCurrentApiEventNews()
        {
            EventActivity activity = GetCurrentEventActivity();
            if (activity == null || activity.Id == 0)
            {
                return null;
            }
            if (String.IsNullOrEmpty(activity.AppImage))
            {
                return null;
            }
            ApiEventNews eventNews = new ApiEventNews
            {
                Id = activity.Id,
                ImageUrl = Helper.CombineUrl(config.SiteUrl, ImageFacade.EventActivityImageProcessor.EventActivityImageUrl, activity.AppImage),
                LinkUrl = activity.AppLinkUrl,
                AutoCoseSeconds = activity.AutoCloseSeconds.GetValueOrDefault(),
                QuietSeconds = (int)TimeSpan.FromDays(activity.Cookieexpire1.GetValueOrDefault(1)).TotalSeconds
            };

            return eventNews;
        }

        public static bool TryGetEventActivityToShow(bool isWeb, HttpCookie cookie, out int eventActivityId,
            out string eventActivityHtml, out int openTarget)
        {
            eventActivityId = 0;
            eventActivityHtml = string.Empty;
            openTarget = 0;
            EventActivity activity = GetCurrentEventActivity();
            if (activity == null || activity.Id == 0)
            {
                return false;
            }
            if (isWeb)
            {
                openTarget = activity.WebOpenTarget.GetValueOrDefault();
            }
            else
            {
                openTarget = activity.MobileOpenTarget.GetValueOrDefault();
            }
            eventActivityHtml = EventFacade.GetEventActivityHtml(activity, isWeb);
            if (string.IsNullOrEmpty(eventActivityHtml))
            {
                return false;
            }
            if (cookie == null)
            {
                eventActivityId = activity.Id;
                return true;
            }
            else
            {
                //cookie存在表示看過，就不再顯示
                int eventActivityIdAtClient;
                if (int.TryParse(cookie.Value, out eventActivityIdAtClient) && eventActivityIdAtClient != activity.Id)
                {
                    eventActivityId = activity.Id;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 取得蓋板跳窗的html內容
        /// </summary>
        /// <param name="activity"></param>
        /// <param name="isWeb">true is web,  false is m-web</param>
        /// <returns></returns>
        public static string GetEventActivityHtml(EventActivity activity, bool isWeb)
        {
            if (activity.Id == 0)
            {
                return string.Empty;
            }
            if (string.IsNullOrEmpty(activity.Description1) == false)
            {
                return HttpUtility.HtmlDecode(Helper.ReplaceToHttps(activity.Description1));
            }
            StringBuilder sb = new StringBuilder();
            string img;
            string link;
            bool openNew;

            if (isWeb)
            {
                img = activity.WebImage;
                link = activity.WebLinkUrl;
                openNew = activity.WebOpenTarget.GetValueOrDefault() == 1;
            }
            else
            {
                img = activity.MobileImage;
                link = activity.MobileLinkUrl;
                openNew = activity.MobileOpenTarget.GetValueOrDefault() == 1;
            }

            //if (string.IsNullOrEmpty(link) == false)
            //{
            //    sb.Append("<a href='");
            //    sb.Append(link);
            //    sb.Append("'");
            //    if (openNew)
            //    {
            //        sb.Append(" target='_blank'");
            //    }
            //    sb.Append(">");
            //}
            if (isWeb)
            {
                //if (string.IsNullOrEmpty(link) == false)
                //{
                //    sb.Append("<a href='javascript:void(0)' onclick='OpenActivityEvent(true)'>");
                //}
                if (string.IsNullOrEmpty(img) == false)
                {
                    sb.Append("<img alt='' src='");
                    sb.Append(Helper.CombineUrl(ImageFacade.EventActivityImageProcessor.EventActivityImageUrl, img));
                    sb.Append("'");
                    sb.Append(" style='width: 640px; height: 440px;'");
                    sb.Append(" />");
                }

                //if (string.IsNullOrEmpty(link) == false)
                //{
                //    sb.Append("</a>");
                //}
            }
            else
            {
                if (string.IsNullOrEmpty(img) == false)
                {
                    sb.Append("<img alt='' src='");
                    sb.Append(Helper.CombineUrl(ImageFacade.EventActivityImageProcessor.EventActivityImageUrl, img));
                    sb.Append("'");
                    sb.Append(" />");
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// 取得M版蓋版廣告
        /// </summary>
        /// <returns>MobileEventAdModel</returns>
        public static ApiEventNews GetCurrentMobileEventAd()
        {
            EventActivity activity = GetCurrentEventActivity();
            if (activity == null || activity.Id == 0)
            {
                return null;
            }
            if (String.IsNullOrEmpty(activity.MobileImage))
            {
                return null;
            }
            ApiEventNews eventNews = new ApiEventNews
            {
                Id = activity.Id,
                ImageUrl = Helper.CombineUrl(config.SiteUrl, ImageFacade.EventActivityImageProcessor.EventActivityImageUrl, activity.MobileImage),
                LinkUrl = activity.MobileLinkUrl,
                AutoCoseSeconds = activity.AutoCloseSeconds.GetValueOrDefault(),
                QuietSeconds = (int)TimeSpan.FromDays(activity.Cookieexpire1.GetValueOrDefault(1)).TotalSeconds
            };

            return eventNews;
        }

        #region 限時優惠

        public static LimitedTimeSelection GetEffectiveLimitedTimeSelections(DateTime theDay, string previewCode)
        {
            LimitedTimeSelection limitedTimeSelection = new LimitedTimeSelection()
            {
                LimitedTimeSelectionDealViews = new List<LimitedTimeSelectionDealView>(),
            };

            limitedTimeSelection.StartDate = theDay.AddHours(12);
            limitedTimeSelection.EndDate = theDay.AddDays(1).AddHours(12);

            LimitedTimeSelectionMain limitedTimeSelectionMain = pep.GetLimitedTimeSelectionMain(theDay);
            if (limitedTimeSelectionMain == null)
            {
                return limitedTimeSelection;
            }

            if (IsEffectiveRange(limitedTimeSelectionMain) || limitedTimeSelectionMain.PreviewCode == previewCode)
            {
                List<LimitedTimeSelectionDeal> limitedTimeSelectionDeals = pep.GetEffectiveLimitedTimeSelectionDeals(limitedTimeSelectionMain.Id);
                limitedTimeSelection.Id = limitedTimeSelectionMain.Id;
                limitedTimeSelection.TheDate = limitedTimeSelectionMain.TheDate;
                limitedTimeSelection.LimitedTimeSelectionDealViews = EffectiveLimitedTimeSelections(limitedTimeSelectionDeals);
                limitedTimeSelection.ModifyTime = limitedTimeSelectionMain.ModifyTime ?? limitedTimeSelectionMain.CreateTime;

                return limitedTimeSelection;
            }

            return limitedTimeSelection;
        }

        //limitedTimeSelectionMain.TheDate 值2018-07-03 00:00:00，指有效範圍為 2018-07-03 12:00:00 ~ 2018-07-04 12:00:00
        private static bool IsEffectiveRange(LimitedTimeSelectionMain limitedTimeSelectionMain)
        {
            return limitedTimeSelectionMain.TheDate.AddHours(12) <= DateTime.Now && limitedTimeSelectionMain.TheDate.AddDays(1).AddHours(12) > DateTime.Now;
        }

        private static List<LimitedTimeSelectionDealView> EffectiveLimitedTimeSelections(List<LimitedTimeSelectionDeal> limitedTimeSelectionDeals)
        {
            List<LimitedTimeSelectionDealView> viewLimitedTimeSelectionDeals = new List<LimitedTimeSelectionDealView>();
            foreach (LimitedTimeSelectionDeal limitedTimeSelection in limitedTimeSelectionDeals)
            {
                LimitedTimeSelectionDealView limitedTimeSelectionDealView = new LimitedTimeSelectionDealView();
                limitedTimeSelectionDealView.Id = limitedTimeSelection.Id;
                limitedTimeSelectionDealView.Bid = limitedTimeSelection.Bid;
                limitedTimeSelectionDealView.Enabled = limitedTimeSelection.Enabled;
                limitedTimeSelectionDealView.MainId = limitedTimeSelection.MainId;
                limitedTimeSelectionDealView.Sequence = limitedTimeSelection.Sequence;

                IViewPponDeal ppdeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(limitedTimeSelection.Bid);
                PponDealStage stage = ppdeal.GetDealStage();

                if (InBusinessHourOrderTime(ppdeal) &&
                    NoCompleted(ppdeal) &&
                    ShowDealDetailOnWeb(ppdeal) &&
                    NoInBusinessHourOrderTimeS(ppdeal)) //已下檔、已結檔、NotShowDealDetailOnWeb的不加入列表
                {
                    MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(0, limitedTimeSelection.Sequence, ppdeal,
                        new List<int>(), DealTimeSlotStatus.Default, 0);

                    limitedTimeSelectionDealView.MultipleMainDealPreviews = mainDealPreview;
                    viewLimitedTimeSelectionDeals.Add(limitedTimeSelectionDealView);
                }

            }

            return viewLimitedTimeSelectionDeals;
        }

        //不在預備中
        private static bool NoInBusinessHourOrderTimeS(IViewPponDeal ppdeal)
        {
            return (ppdeal.BusinessHourOrderTimeS <= DateTime.Now);
        }

        private static bool InBusinessHourOrderTime(IViewPponDeal ppdeal)
        {
            return !(ppdeal.BusinessHourOrderTimeE <= DateTime.Now);
        }

        private static bool NoCompleted(IViewPponDeal ppdeal)
        {
            return !Helper.IsFlagSet(ppdeal.GroupOrderStatus.GetValueOrDefault(), GroupOrderStatus.Completed);
        }
        //非暫停銷售
        private static bool ShowDealDetailOnWeb(IViewPponDeal ppdeal)
        {
            return !Helper.IsFlagSet(ppdeal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb);
        }

        /// <summary>
        /// 依限時優惠活動主鍵，取得活動的檔次列表，並排除標記刪除的檔次資料
        /// </summary>
        /// <param name="mainId"></param>
        /// <returns></returns>
        public static List<LimitedTimeSelectionDeal> GetLimitedTimeSelectionValidDeals(int mainId)
        {
            return pep.GetLimitedTimeSelectionValidDeals(mainId);
        }
        /// <summary>
        /// 依限時優惠活動主鍵，取得活動的全部檔次列表(包括標記刪除)
        /// </summary>
        /// <param name="mainId"></param>
        /// <returns></returns>
        public static List<LimitedTimeSelectionDeal> GetLimitedTimeSelectionAllDeals(int mainId)
        {
            return pep.GetLimitedTimeSelectionAllDeals(mainId);
        }
        /// <summary>
        /// 依日期取得限時優惠活動
        /// </summary>
        /// <param name="theDate"></param>
        /// <returns></returns>
        public static LimitedTimeSelectionMain GetLimitedTimeSelectionMain(DateTime theDate)
        {
            return pep.GetLimitedTimeSelectionMain(theDate);
        }
        /// <summary>
        /// 依主鍵取得限時優惠活動
        /// </summary>
        /// <param name="mainId"></param>
        /// <returns></returns>
        public static LimitedTimeSelectionMain GetLimitedTimeSelectionMain(int mainId)
        {
            return pep.GetLimitedTimeSelectionMain(mainId);
        }
        /// <summary>
        /// 更新限時優惠的主檔
        /// </summary>
        /// <param name="main"></param>
        /// <param name="createId"></param>
        /// <param name="createTime"></param>
        public static void SaveLimitedTimeSelectionMain(LimitedTimeSelectionMain main,
            string createId, DateTime createTime)
        {
            if (main.TheDate == default(DateTime))
            {
                throw new Exception("LimitedTimeSelectionMain未設定TheDate");
            }
            if (main.Id == 0)
            {
                main.PreviewCode = Guid.NewGuid().ToString().Substring(0, 4);
                main.CreateId = createId;
                main.CreateTime = createTime;
            }
            else
            {
                main.ModifyId = createId;
                main.ModifyTime = createTime;
            }

            pep.SaveLimitedTimeSelectionMain(main);
        }
        /// <summary>
        /// 新增或異動限時優惠活動檔次
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="createId"></param>
        /// <param name="createTime"></param>
        /// <returns></returns>
        public static int SaveLimitedTimeSelectionDeal(LimitedTimeSelectionDeal deal, string createId, DateTime createTime)
        {
            if (deal.Id == 0)
            {
                deal.CreateId = createId;
                deal.CreateTime = createTime;
            }
            else
            {
                deal.ModifyId = createId;
                deal.ModifyTime = createTime;
            }
            return pep.SaveLimitedTimeSelectionDeal(deal);
        }
        /// <summary>
        /// 限時優惠活動刪除檔次
        /// </summary>
        /// <param name="main"></param>
        /// <param name="uselessBids"></param>
        /// <param name="userId"></param>
        /// <param name="now"></param>
        /// <param name="logItems"></param>
        public static void SetLimitedTimeSelectionDealsDisabled(LimitedTimeSelectionMain main, IEnumerable<Guid> uselessBids,
            string userId, DateTime now, out List<DailySelectionEditLogViewItem> logItems)
        {
            if (main == null || main.Id == 0)
            {
                throw new Exception("未指定 LimitedTimeSelectionMain");
            }
            logItems = new List<DailySelectionEditLogViewItem>();
            foreach (Guid bid in uselessBids)
            {
                int affectedCount = pep.SetLimitedTimeSelectionDealDisabled(main.Id, bid, userId, now); ;
                if (affectedCount > 0)
                {
                    logItems.Add(new DailySelectionEditLogViewItem
                    {
                        Bid = bid
                    });
                }
            }
        }
        /// <summary>
        /// 記錄限時優惠異動的log
        /// </summary>
        /// <param name="mainId"></param>
        /// <param name="category"></param>
        /// <param name="selectionDeals"></param>
        /// <param name="userName"></param>
        public static void AddLimitedTimeSelectionChangeLog(int mainId, string category,
            List<LimitedTimeSelectionDeal> selectionDeals, string userName)
        {
            AddLimitedTimeSelectionChangeLog(mainId, category,
                selectionDeals.Select(t => new DailySelectionEditLogViewItem { Bid = t.Bid, Sequence = t.Sequence }).ToList(),
                userName);
        }
        /// <summary>
        /// 記錄限時優惠異動的log
        /// </summary>
        /// <param name="mainId"></param>
        /// <param name="action"></param>
        /// <param name="selectionDeals"></param>
        /// <param name="userName"></param>
        public static void AddLimitedTimeSelectionChangeLog(int mainId, string action,
            List<DailySelectionEditLogViewItem> logViewItems, string userName)
        {
            DateTime now = DateTime.Now;
            LimitedTimeSelectionLog log = new LimitedTimeSelectionLog
            {
                Action = action,
                Content = serializer.Serialize(logViewItems),
                MainId = mainId,
                CreateId = userName,
                CreateTime = now
            };
            pep.SaveLimitedTimeSelectionLog(log);
        }

        public static List<DailySelectionEditLogView> GetLimitedTimeSelectionChangeLogViews(int mainId)
        {
            List<DailySelectionEditLogView> result = new List<DailySelectionEditLogView>();
            var logs = pep.GetLimitedTimeSelectionLogs(mainId);
            foreach (var log in logs)
            {
                result.Add(new DailySelectionEditLogView
                {
                    Action = log.Action,
                    CreateId = log.CreateId,
                    CreateTime = log.CreateTime.ToString("yyyy-MM-dd HH:mm:ss"),
                    Items = serializer.Deserialize<List<DailySelectionEditLogViewItem>>(log.Content)
                });
            }
            return result;
        }

        public static SelectionDealStateType GetSelectionDealState(IViewPponDeal deal)
        {
            DateTime now = DateTime.Now;
            if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
            {
                return SelectionDealStateType.Suspend;
            }
            if (now < deal.BusinessHourOrderTimeS)
            {
                return SelectionDealStateType.Preparing;
            }
            if (now > deal.BusinessHourOrderTimeE)
            {
                return SelectionDealStateType.Offline;
            }
            return SelectionDealStateType.Online;
        }

        #endregion


        public static DiscountCodeCollection GetDiscountCodeByCondition(int uniqueid)
        {
            var dcc = ep.CheckDiscountCodeByCondition(uniqueid);
            return dcc;
        }

        /// <summary>
        /// 發送一次性EDM
        /// </summary>
        /// <param name="memberMail"></param>
        /// <param name="memberName"></param>
        public static void SendOneTimeEventdMail(string memberMail, string memberName)
        {
            MailMessage msg = new MailMessage();
            OneTimeEventMail template = TemplateFactory.Instance().GetTemplate<OneTimeEventMail>();
            template.MemberName = memberName;
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;

            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(memberMail);
            msg.Subject = "恭喜您！獲得免費索取大立百貨遊樂園入場券資格！";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }
        /// <summary>
        /// 記錄使用者點擊蓋版廣告的log
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="id"></param>
        /// <param name="now"></param>
        /// <param name="staySecond"></param>
        /// <param name="triggerType">0:web, 1:mobile, 2:app</param>
        /// <param name="deviceType"></param>
        public static void AddEventActivityHitLog(
            int userId, int id, DateTime now, bool openEvent, decimal staySeconds, int triggerType, OrderFromType deviceType)
        {
            EventActivityHitLog log = new EventActivityHitLog
            {
                UserId = userId,
                EventActivityId = id,
                OpenEvent = openEvent,
                StaySeconds = staySeconds,
                CreateTime = now,
                TriggerType = triggerType,
                DeviceType = (int)deviceType
            };
            eep.SaveEventActivityHitLog(log);
        }
    }
}

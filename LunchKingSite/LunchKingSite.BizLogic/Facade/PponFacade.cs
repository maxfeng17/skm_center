﻿using iTextSharp.text.html.simpleparser;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Models.LineShopping;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Xml;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.API;
using Image = System.Drawing.Image;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.Core.Interface;
using LunchKingSite.I18N;
using LunchKingSite.Core.Models.PponEntities;
using System.Runtime.Caching;

namespace LunchKingSite.BizLogic.Facade
{
    public class PponFacade
    {
        private static IPponProvider _pponProv;
        private static ISellerProvider _sellerProv;
        private static ILocationProvider _locationProv;
        private static IOrderProvider _orderProv;
        private static IOrderEntityProvider _op;
        private static ISysConfProvider config;
        private static ISystemProvider _systemProv;
        private static IHiDealProvider _hidealProv;
        private static IHumanProvider _humProv;
        private static IItemProvider _itemProv;
        private static ISkmProvider _skmProv;
        private static ISkmEfProvider _skmEf;
        private static IMemberProvider _memProv;
        private static IVerificationProvider _verificationProv;
        private static ILog logger = LogManager.GetLogger("PponFacade");
        private static IChannelEventProvider _cep;
        private static IPponEntityProvider _pep;
        private static IWmsProvider _wp;
        private static IChannelProvider _channelProv;
        private static IFamiportProvider fami;

        private const string CouponCodeBase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private const string rfc822DateimeFormat = "ddd, dd MMM yy HH:mm:ss zz";

        static PponFacade()
        {
            _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sellerProv = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _locationProv = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            _orderProv = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            config = ProviderFactory.Instance().GetConfig();
            _systemProv = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _hidealProv = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _humProv = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            _itemProv = ProviderFactory.Instance().GetProvider<IItemProvider>();
            _skmProv = ProviderFactory.Instance().GetProvider<ISkmProvider>();
            _skmEf = ProviderFactory.Instance().GetProvider<ISkmEfProvider>();
            _memProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _verificationProv = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            _cep = ProviderFactory.Instance().GetProvider<IChannelEventProvider>();
            _pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
            _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
            _channelProv = ProviderFactory.Instance().GetProvider<IChannelProvider>();
            fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderEntityProvider>();
        }

        #region DealProperty

        public static DealProperty DealPropertyGetByBid(Guid bid)
        {
            return _pponProv.DealPropertyGet(bid);
        }

        #endregion

        #region DealPayment

        public static DealPayment DealPaymentGetByBid(Guid bid)
        {
            return _pponProv.DealPaymentGet(bid);
        }

        #endregion

        #region DealTimeSlot

        public static DealTimeSlotCollection GetDealTimeSlotList(Guid bid)
        {
            return _pponProv.DealTimeSlotGetList(DealTimeSlot.Columns.Sequence, DealTimeSlot.Columns.BusinessHourGuid + "=" + bid);
        }

        /// <summary>
        /// 取得新增檔次的排序編號 (即某城市某日最小的非鎖定排序編號)
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="effectiveStart"></param>
        /// <returns></returns>
        public static int GetDealTimeSlotNonLockSeq(int cityId, DateTime effectiveStart, List<ViewPponDealTimeSlot> vpdts)
        {
            var workDate = new DateTime(effectiveStart.Year, effectiveStart.Month, effectiveStart.Day, 12, 0, 0);
            if (effectiveStart.Hour < 12)
            {
                workDate = workDate.AddDays(-1);
            }

            //var lockedDealList = GetViewPponDealTimeSlot(cityId, workDate).Where(x => !x.IsLockSeq).ToList();
            var lockedDealList = vpdts.Where(x => !x.IsLockSeq).ToList();
            return lockedDealList.Any() ? lockedDealList.Min(x => x.Sequence) : 1;
        }

        /// <summary>
        /// 取得某城市某日的鎖定排序編號清單
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="effectiveStart"></param>
        /// <returns></returns>
        public static List<int> GetDealTimeSlotLockSeqList(int cityId, DateTime effectiveStart, List<ViewPponDealTimeSlot> vpdts)
        {
            var workDate = new DateTime(effectiveStart.Year, effectiveStart.Month, effectiveStart.Day, 12, 0, 0);
            if (effectiveStart.Hour < 12)
            {
                workDate = workDate.AddDays(-1);
            }

            //var lockedDealList = GetViewPponDealTimeSlot(cityId, workDate).Where(x => x.IsLockSeq).ToList();
            var lockedDealList = vpdts.Where(x => x.IsLockSeq).ToList();
            return lockedDealList.Any() ? lockedDealList.Select(x => x.Sequence).ToList() : new List<int>(); ;
        }

        /// <summary>
        /// 將某城市某日的非鎖定檔次後退一個排序，並跳過已鎖定的編號
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="effectiveStart"></param>
        /// <returns></returns>
        public static bool SetNonLockDealTimeSlotSeqAddOne(int cityId, DateTime effectiveStart, List<ViewPponDealTimeSlot> vpdts)
        {
            var workDate = new DateTime(effectiveStart.Year, effectiveStart.Month, effectiveStart.Day, 12, 0, 0);
            if (effectiveStart.Hour < 12)
            {
                workDate = workDate.AddDays(-1);
            }

            //var vpdtsList = GetViewPponDealTimeSlot(cityId, workDate).Where(x => !x.IsLockSeq).OrderBy(x => x.Sequence).ToList(); //準備後挪的檔次
            var vpdtsList = vpdts.Where(x => !x.IsLockSeq).OrderBy(x => x.Sequence).ToList(); //準備後挪的檔次

            var lockedSeqList = GetDealTimeSlotLockSeqList(cityId, workDate, vpdts); //不可使用的編號清單
            lockedSeqList.Add(GetDealTimeSlotNonLockSeq(cityId, workDate, vpdts));   //加入保留編號

            var newSeq = 1;
            foreach (var vpdt in vpdtsList)
            {
                while (lockedSeqList.Contains(newSeq))
                {
                    newSeq++;
                }
                vpdt.Sequence = newSeq;
                newSeq++;
            }
            _pponProv.DealTimeSlotUpdateSeqWithTransaction(vpdtsList);

            return true;
        }

        public static bool IsSkmDeal(Guid businessHourId)
        {
            if (businessHourId == Guid.Empty)
            {
                return false;
            }
            ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(businessHourId);
            if (vpd.IsLoaded == false)
            {
                return false;
            }
            SellerTree sellerTree = _sellerProv.SellerTreeGetListBySellerGuid(vpd.SellerGuid).FirstOrDefault();
            if (sellerTree == null)
            {
                return false;
            }
            return sellerTree.RootSellerGuid == config.SkmRootSellerGuid;
        }

        #endregion DealTimeSlot

        #region DealTimeSlot 檔次自動排序

        /// <summary>
        /// 註記熱銷檔次
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="workDate"></param>
        public static void SetHotDealFlag(int cityId, DateTime workDate)
        {
            if (cityId > 0)
            {
                var hConfig = _pponProv.HotDealConfigGet(cityId);
                if (hConfig.IsLoaded && hConfig.Enable)
                {
                    _pponProv.ClearHotDealFlagByCityId(cityId, workDate);
                    _pponProv.ClearDealTimeSlotHotDealOrderedTotalByCityId(cityId);
                    _pponProv.GetHotDealToTempTable(hConfig.TopN, hConfig.NDays, cityId, workDate);
                    _pponProv.SetDealTimeSlotHotDealFlagByCity(cityId);
                }
            }
            else
            {
                var hotdealConfigCol = _pponProv.HotDealConfigGetAll();
                if (hotdealConfigCol.Any())
                {
                    _pponProv.ClearHotDealFlag(workDate);
                    _pponProv.ClearDealTimeSlotHotDealOrderedTotal();

                    foreach (var hotdealConfig in hotdealConfigCol)
                    {
                        if (hotdealConfig.Enable)
                        {
                            _pponProv.GetHotDealToTempTable(hotdealConfig.TopN, hotdealConfig.NDays, hotdealConfig.CityId, workDate);
                        }
                    }
                    _pponProv.SetDealTimeSlotHotDealFlag();
                }
            }
        }

        /// <summary>
        /// 鎖定排序
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="cityId"></param>
        /// <param name="effectiveStart"></param>
        /// <param name="isLockSeq"></param>
        /// <returns></returns>
        public static bool SetLockSeqToDealTimeSlotData(Guid guid, int cityId, DateTime effectiveStart, bool isLockSeq)
        {
            var slot = _pponProv.DealTimeSlotGet(guid, cityId, effectiveStart);
            if (slot.IsLoaded)
            {
                slot.IsLockSeq = isLockSeq;
                _pponProv.DealTimeSlotUpdate(slot);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 取得DealTimeSlot的workDate
        /// </summary>
        /// <returns></returns>
        public static DateTime GetDealTimeSlotWorkDate()
        {
            //中午前，取昨日~今日；中午後，取今日~明日
            var workDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 0, 0);
            if (DateTime.Now.Hour < 12)
            {
                workDate = workDate.AddDays(-1);
            }

            return workDate;
        }

        /// <summary>
        /// 取得今日DealTimeSlot
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        public static List<ViewPponDealTimeSlot> GetViewPponDealTimeSlot(int cityId, DateTime workDate)
        {
            var searchWhere = new List<string>()
            {
                ViewPponDealTimeSlot.Columns.EffectiveStart + " >= " + workDate.ToString("yyyy/MM/dd HH:mm:ss"),
                ViewPponDealTimeSlot.Columns.EffectiveStart + " < " + workDate.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss")
            };

            if (cityId > 0)
            {
                searchWhere.Add(string.Format("{0} = {1}", ViewPponDealTimeSlot.Columns.CityId, cityId));
            }
            else
            {
                searchWhere.Add(ViewPponDealTimeSlot.Columns.CityId + " in (" + CitiesGet() + ")");
            }

            return _pponProv.ViewPponDealTimeSlotGetList(string.Empty, searchWhere.ToArray())
                .Where(x => !((x.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)).ToList();
        }

        /// <summary>
        /// DealTimeSlot 檔次自動排序
        /// </summary>
        public static int SortDealTimeSlotByOperatingInfo(int cityId, string operation)
        {
            var startTime = DateTime.Now;
            var workDate = GetDealTimeSlotWorkDate();

            //1-註記熱銷檔次
            SetHotDealFlag(cityId, workDate);
            var step1Sec = (DateTime.Now - startTime).TotalSeconds;

            //2-取得今日 DealTimeSlot
            var todayDealTimeSlot = GetViewPponDealTimeSlot(cityId, workDate);
            if (todayDealTimeSlot.Count <= 0)
            {
                return 0;
            }

            var step2Sec = (DateTime.Now - startTime).TotalSeconds - step1Sec;

            //3-熱銷檔次排序
            if (config.EnableHotDealSetting)
            {
                int hotDealCnt = SetHotDealSeq(todayDealTimeSlot, workDate);
                if (hotDealCnt > 0)
                {
                    todayDealTimeSlot = GetViewPponDealTimeSlot(cityId, workDate); //更新完需取一次最新DealTimeSlot
                }
            }

            var step3Sec = (DateTime.Now - startTime).TotalSeconds - step1Sec - step2Sec;

            //4-計算檔質 DealTimeSlot 分數
            var qualityScoreList = MergeDealWeightScores(todayDealTimeSlot);

            var step4Sec = (DateTime.Now - startTime).TotalSeconds - step1Sec - step2Sec - step3Sec;

            //5-檔質排序
            int updateCnt = UpdateDealTimeSlotSeq(qualityScoreList, todayDealTimeSlot);

            var step5Sec = (DateTime.Now - startTime).TotalSeconds - step1Sec - step2Sec - step3Sec - step4Sec;
            var totalSec = (DateTime.Now - startTime).TotalSeconds;

            var sortCity = (cityId > 0) ? cityId.ToString() : "全站";
            var costLog = string.Format("{0}檔次({1})  {7} \r\n1-註記熱銷檔次: {2} \r\n2-取得今日檔次: {3}\r\n" +
                                        "3-熱銷檔次排序: {4} \r\n4-計算檔質: {5} \r\n5-檔質排序: {6} \r\n",
                                        sortCity, todayDealTimeSlot.Count, step1Sec, step2Sec, step3Sec, step4Sec, step5Sec, totalSec);
            VourcherFacade.ChangeLogAdd("SortDealTimeSlot", Guid.Empty, costLog, false);
            return updateCnt;
        }

        /// <summary>
        /// 合併DealTimeSlot Key與Score
        /// </summary>
        /// <param name="todayDealTimeSlot"></param>
        /// <returns></returns>
        private static List<DealWeightScore> MergeDealWeightScores(List<ViewPponDealTimeSlot> todayDealTimeSlot)
        {
            var startTime = DateTime.Now;
            var qualityScoreList = new List<DealWeightScore>();

            try
            {
                #region 4-1 準備單檔、多檔的檔質資料

                //單檔檔質
                var singleDealBids = todayDealTimeSlot.Where(x => !x.IsLockSeq && !x.IsHotDeal && !((x.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0))
                                              .Select(x => x.BusinessHourGuid).Distinct().ToList();
                var step4_1_1 = (DateTime.Now - startTime).TotalSeconds;
                var singleDealQualityCol = _pponProv.ViewPponDealOperatingInfoGetInBids(singleDealBids);
                var singleDealQualityDic = singleDealQualityCol.ToDictionary(x => x.BusinessHourGuid);
                var step4_1_2 = (DateTime.Now - startTime).TotalSeconds - step4_1_1;

                //多檔次檔質
                //子檔不用多做排除IsLockSeq, IsHotDeal的動作，只有母檔才會被Lock及被註記為HotDeal
                var combodealMainBids = todayDealTimeSlot.Where(x => (x.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                                                 .Select(x => x.BusinessHourGuid).Distinct();

                var step4_1_3 = (DateTime.Now - startTime).TotalSeconds - step4_1_1 - step4_1_2;

                var comboDealBidDic = combodealMainBids.ToDictionary(
                            bid => bid, //母檔bid
                            bid => ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(bid)
                                    .Select(x => x.BusinessHourGuid).ToList()); //子檔bids，目前子檔若提早結檔仍會被列入檔質計算

                var step4_1_4 = (DateTime.Now - startTime).TotalSeconds - step4_1_1 - step4_1_2 - step4_1_3;

                var comboDealBids = new List<Guid>();

                foreach (var data in comboDealBidDic.Values)
                {
                    comboDealBids.AddRange(data);
                }

                var step4_1_5 = (DateTime.Now - startTime).TotalSeconds - step4_1_1 - step4_1_2 - step4_1_3 - step4_1_4;
                var tempComboDealQuality = _pponProv.ViewPponDealOperatingInfoGetInBids(comboDealBids);
                var step4_1_6 = (DateTime.Now - startTime).TotalSeconds - step4_1_1 - step4_1_2 - step4_1_3 - step4_1_4 - step4_1_5;

                var comboDealQualityDic = tempComboDealQuality.ToDictionary(x => x.BusinessHourGuid);
                var step4_1_7 = (DateTime.Now - startTime).TotalSeconds - step4_1_1 - step4_1_2 - step4_1_3 - step4_1_4 - step4_1_5 - step4_1_6;
                var step4_1 = (DateTime.Now - startTime).TotalSeconds;

                #endregion

                #region 4-2 取得權重對照

                //取得權重對照
                var allWeights = _pponProv.DealTimeSlotSortElementGet();

                //取得分數對照
                var rank = _pponProv.DealTimeSlotSortElementRankGet().ToList();

                var step4_2 = (DateTime.Now - startTime).TotalSeconds - step4_1;

                #endregion

                #region 4-3 計算檔次權重分數

                double step4_3_1 = 0;
                double step4_3_2 = 0;
                double step4_3_3 = 0;
                double step4_3_4 = 0;
                int combolDealCnt = 0;
                int singelDealCnt = 0;

                foreach (var vpdts in todayDealTimeSlot)
                {
                    //非鎖定及熱銷檔才要計算分數
                    if (!vpdts.IsLockSeq && !vpdts.IsHotDeal) //合併在foreach迴圈裡判斷，才不會跑2次大量foreach (2016/8月 約6、7千筆)
                    {
                        var step_4_3_start = DateTime.Now;

                        double step4_3_1_sub = 0;
                        double step4_3_2_sub = 0;
                        double step4_3_3_sub = 0;
                        double step4_3_4_sub = 0;

                        var totalWeightScore = decimal.Zero;
                        var orderedTotal = decimal.Zero;
                        decimal subWeightScore;
                        decimal revenue;
                        if ((vpdts.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                        {
                            //若為多檔次，母檔分數以子檔的平均加權分數取代。
                            var comboDealQualityList = comboDealBidDic[vpdts.BusinessHourGuid].Select(bid => comboDealQualityDic[bid]).ToList();
                            var comboDealTotalWeightScore = decimal.Zero;
                            var comboDealTotalOrderedTotal = decimal.Zero;
                            step4_3_1_sub = (DateTime.Now - step_4_3_start).TotalSeconds;

                            if (comboDealQualityList.Count > 0)
                            {
                                foreach (var comboDealQuality in comboDealQualityList)
                                {
                                    CalculateDealWeightScore(comboDealQuality, vpdts.CityId, rank, allWeights, out subWeightScore, out revenue);
                                    comboDealTotalWeightScore += subWeightScore;
                                    comboDealTotalOrderedTotal += revenue;
                                }
                                totalWeightScore = comboDealTotalWeightScore / comboDealQualityList.Count;
                                orderedTotal = comboDealTotalOrderedTotal;
                            }
                            combolDealCnt += comboDealQualityList.Count;
                            step4_3_2_sub = (DateTime.Now - step_4_3_start).TotalSeconds - step4_3_1_sub;
                        }
                        else
                        {   //單檔
                            var singleDealQuality = singleDealQualityDic[vpdts.BusinessHourGuid];
                            step4_3_3_sub = (DateTime.Now - step_4_3_start).TotalSeconds - step4_3_1_sub - step4_3_2_sub;

                            CalculateDealWeightScore(singleDealQuality, vpdts.CityId, rank, allWeights, out subWeightScore, out revenue);
                            totalWeightScore = subWeightScore;
                            orderedTotal = revenue;

                            singelDealCnt++;
                            step4_3_4_sub = (DateTime.Now - step_4_3_start).TotalSeconds - step4_3_1_sub - step4_3_2_sub - step4_3_3_sub;
                        }
                        step4_3_1 += step4_3_1_sub;
                        step4_3_2 += step4_3_2_sub;
                        step4_3_3 += step4_3_3_sub;
                        step4_3_4 += step4_3_4_sub;

                        qualityScoreList.Add(new DealWeightScore()
                        {
                            Bid = vpdts.BusinessHourGuid,
                            CityId = vpdts.CityId,
                            EffStart = vpdts.EffectiveStart,
                            WeightScore = totalWeightScore,
                            OrderedTotal = orderedTotal
                        });
                    }
                }

                var step4_3 = (DateTime.Now - startTime).TotalSeconds - step4_1 - step4_2;

                var costLog = string.Format("4-1-準備檔質資料: {0} \r\n" +
                                            "4-2-取得權重分數對照: {1}\r\n" +
                                            "4-3-計算檔質分數: {2}, 計算檔質數目({11})\r\n\r\n" +
                                            "4-1-1: {3}\r\n" +
                                            "4-1-2-DB整批取單檔檔質({19})+產生單檔檔質Dic: {4}, Take({16})\r\n" +
                                            "4-1-3: {5}\r\n" +
                                            "4-1-4-產生多檔BidDic: {6}\r\n" +
                                            "4-1-5: {7}\r\n" +
                                            "4-1-6-DB整批取子檔檔質({10}): {8}, Take({16})\r\n" +
                                            "4-1-7-產生多檔檔質Dic: {9} \r\n\r\n" +
                                            "4-3-1-取出多檔檔質: {12}\r\n" +
                                            "4-3-2-計算多檔分數({17}): {13}\r\n" +
                                            "4-3-3-取出單檔檔質: {14}\r\n" +
                                            "4-3-4-計算單檔分數({18}): {15}\r\n",
                                            step4_1, step4_2, step4_3,
                                            step4_1_1, step4_1_2, step4_1_3, step4_1_4, step4_1_5, step4_1_6, step4_1_7,
                                            comboDealBids.Count, qualityScoreList.Count,
                                            step4_3_1, step4_3_2, step4_3_3, step4_3_4, config.TakeViewPponDealOperatingInfoCount,
                                            combolDealCnt, singelDealCnt, singleDealBids.Count);

                VourcherFacade.ChangeLogAdd("SortDealTimeSlot", Guid.Empty, costLog, false);

                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return qualityScoreList;
        }

        public static bool IsDealDiscountPriceBlacklist(Guid bid)
        {
            var dealDiscountPriceBlacklists = _pep.GetAllDealDiscountPriceBlacklist();
            return dealDiscountPriceBlacklists.Any(x => x.Bid == bid);
        }

        /// <summary>
        /// 計算DealTimeSlot檔質分數、權重
        /// </summary>
        /// <param name="dealQuality"></param>
        /// <param name="cityId"></param>
        /// <param name="rank"></param>
        /// <param name="allWeights"></param>
        /// <param name="subWeightScore"></param>
        /// <param name="revenue"></param>
        /// <returns></returns>
        private static void CalculateDealWeightScore(ViewPponDealOperatingInfo dealQuality, int cityId, List<DealTimeSlotSortElementRank> rank,
            IEnumerable<DealTimeSlotSortElement> allWeights, out decimal subWeightScore, out decimal revenue)
        {
            subWeightScore = decimal.Zero;
            revenue = decimal.Zero;

            //權重對照
            var cityWeights = allWeights.Where(x => x.CityId == cityId).ToList();
            var totalWeight = cityWeights.Sum(x => x.ElementWeight);
            if (totalWeight == 0)
            {
                return;
            }

            //計算分數、權重
            foreach (var elementId in Enum.GetValues(typeof(DealTimeSlotElement)))
            {
                #region set value

                dynamic value = null;
                switch ((int)elementId)
                {
                    case (int)DealTimeSlotElement.GrossMargin:
                        value = dealQuality.MultistepGrossMargin ?? decimal.Zero;
                        break;
                    case (int)DealTimeSlotElement.GrossProfit:
                        value = dealQuality.MultistepGrossProfit;
                        break;
                    case (int)DealTimeSlotElement.Quantity:
                        value = dealQuality.OrderedQuantity;
                        break;
                    case (int)DealTimeSlotElement.DiscountRate:
                        value = dealQuality.DiscountRate ?? decimal.Zero;
                        break;
                    case (int)DealTimeSlotElement.Revenue:
                        value = dealQuality.OrderedTotal;
                        revenue = value;
                        break;
                    case (int)DealTimeSlotElement.Days:
                        value = dealQuality.SellDays;
                        break;
                    case (int)DealTimeSlotElement.Price:
                        value = dealQuality.ItemPrice;
                        break;
                    case (int)DealTimeSlotElement.SellerLevel:
                        value = dealQuality.SellerLevel;
                        break;
                }

                if (value == null)
                {
                    continue;
                }

                #endregion

                var scoreList = (int)elementId == (int)DealTimeSlotElement.SellerLevel
                     ? rank.Where(x => x.ElementId == (int)elementId && x.RankEnd == value).Select(x => x.Score).ToList()
                     : rank.Where(x => x.ElementId == (int)elementId && x.RankStart < value).Where(x => x.RankEnd >= value).Select(x => x.Score).ToList();

                var score = decimal.Zero;
                if (scoreList.Count == 1)
                {
                    score = scoreList.SingleOrDefault();
                }
                else if (!scoreList.Any() && (int)elementId != (int)DealTimeSlotElement.SellerLevel)
                {
                    throw new Exception("級距資料設定錯誤!! elementId=" + (int)elementId + ", " + elementId + "=" + value);
                }

                var weight = cityWeights.Where(x => x.ElementId == (int)elementId).Select(x => x.ElementWeight).SingleOrDefault();
                var weightScore = score * (weight / totalWeight);

                subWeightScore += weightScore;
            }
        }

        /// <summary>
        /// 更新DealTimeSlot排序
        /// </summary>
        /// <param name="scoreList"></param>
        /// <param name="todayDealTimeSlot"></param>
        /// <returns></returns>
        private static int UpdateDealTimeSlotSeq(List<DealWeightScore> scoreList, List<ViewPponDealTimeSlot> todayDealTimeSlot)
        {
            //var startTime = DateTime.Now;
            var cityIdGroup = scoreList.Select(x => x.CityId).Distinct().ToList();
            var tempCol = new TempDealTimeSlotSeqCollection();

            foreach (var cityId in cityIdGroup)
            {
                var newSeq = 1;
                var unavailableSeqList = todayDealTimeSlot.Where(x => x.CityId == cityId && (x.IsLockSeq || x.IsHotDeal))
                                            .Select(x => x.Sequence).ToList();
                var sortedCityDeals = scoreList.Where(x => x.CityId == cityId)
                                    .OrderByDescending(x => x.WeightScore)
                                    .ThenByDescending(x => x.OrderedTotal)
                                    .ThenByDescending(x => x.Bid).ToList();
                foreach (var d in sortedCityDeals)
                {
                    while (unavailableSeqList.Contains(newSeq))
                    {
                        newSeq++;
                    }

                    tempCol.Add(new TempDealTimeSlotSeq
                    {
                        BusinessHourGuid = d.Bid,
                        CityId = cityId,
                        EffectiveStart = d.EffStart,
                        Sequence = newSeq,
                        ModifyTime = DateTime.Now
                    });

                    newSeq++;
                }
            }

            //var step5_1 = (DateTime.Now - startTime).TotalSeconds;

            //double step5_2 = 0;
            //double step5_3 = 0;

            if (tempCol.Any())
            {
                bool insertResult = _pponProv.TruncateAndBulkInsertTempDealTimeSlotSeqCol(tempCol); //Insert至temp
                //step5_2 = (DateTime.Now - startTime).TotalSeconds - step5_1;
                if (insertResult)
                {
                    return _pponProv.UpdateDealTimeSlotSeqFromTemp(); //由temp更新回DealTimeSlot
                }
                //step5_3 = (DateTime.Now - startTime).TotalSeconds - step5_1 - step5_2;
            }

            /*
            var costLog = string.Format("5-1-註記熱銷檔次: {0} \r\n" +
                                        "5-2-TruncateAndBulkInsert: {1} \r\n" +
                                        "5-3-UpdateFromTemp: {2}\r\n", step5_1, step5_2, step5_3);
            VourcherFacade.ChangeLogAdd("SortDealTimeSlot", Guid.Empty, costLog, false);
            */
            return 0;
        }

        /// <summary>
        /// 設定熱銷檔次排序 (目前僅提供在自動排序Facade中使用，若要單獨使用，要再補更新非熱銷、非鎖定的seq)
        /// </summary>
        /// <param name="todayDealTimeSlot"></param>
        /// <param name="workdate"></param>
        /// <returns></returns>
        private static int SetHotDealSeq(List<ViewPponDealTimeSlot> todayDealTimeSlot, DateTime workdate)
        {
            var hotDeals = _pponProv.HotDealOrderedTotalGetAll();
            if (!hotDeals.Any())
            {
                return 0;
            }

            var cityIdGroup = todayDealTimeSlot.ToList().Select(x => x.CityId).Distinct().ToList();

            var tempCol = new TempDealTimeSlotSeqCollection();

            foreach (var cityId in cityIdGroup)
            {
                var hotDealConfig = _pponProv.HotDealConfigGet(cityId);
                if (!hotDealConfig.IsLoaded || hotDealConfig.Enable == false)
                {
                    continue;
                }

                var lockedDeals = todayDealTimeSlot.Where(x => x.CityId == cityId && x.IsLockSeq).ToList();
                var lockedBidList = lockedDeals.Select(l => l.BusinessHourGuid).ToList();

                var cityHotDeals = hotDeals.Where(x => x.CityId == cityId && !lockedBidList.Contains(x.BusinessHourGuid))
                                    .OrderByDescending(x => x.AvgTotal).ToList(); //排除鎖定檔次
                if (!cityHotDeals.Any())
                {
                    continue;
                }

                var lockedSeqList = lockedDeals.Select(l => l.Sequence).ToList();

                var newSeq = 1;
                foreach (var d in cityHotDeals.OrderByDescending(x => x.AvgTotal))
                {
                    while (lockedSeqList.Contains(newSeq) || (newSeq % hotDealConfig.NMultiple != 0)) //熱銷檔放在n的倍數
                    {
                        newSeq++;
                    }

                    tempCol.Add(new TempDealTimeSlotSeq
                    {
                        BusinessHourGuid = d.BusinessHourGuid,
                        CityId = d.CityId,
                        EffectiveStart = d.EffectiveStart,
                        Sequence = newSeq,
                        ModifyTime = DateTime.Now
                    });

                    newSeq++;
                }
            }

            if (tempCol.Any())
            {
                bool insertResult = _pponProv.TruncateAndBulkInsertTempDealTimeSlotSeqCol(tempCol);
                if (insertResult)
                {
                    return _pponProv.UpdateDealTimeSlotSeqFromTemp();
                }
            }

            return 0;
        }

        /// <summary>
        /// 取得需要自動排序頻道
        /// </summary>
        /// <returns></returns>
        private static string CitiesGet()
        {
            var cities = new StringBuilder();
            cities.Append(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId + ",");
            cities.Append(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId + ",");
            cities.Append(PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId + ",");
            cities.Append(PponCityGroup.DefaultPponCityGroup.Hsinchu.CityId + ",");
            cities.Append(PponCityGroup.DefaultPponCityGroup.Taichung.CityId + ",");
            cities.Append(PponCityGroup.DefaultPponCityGroup.Tainan.CityId + ",");
            cities.Append(PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId + ",");
            cities.Append(PponCityGroup.DefaultPponCityGroup.Travel.CityId + ",");
            cities.Append(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId + ",");
            cities.Append(PponCityGroup.DefaultPponCityGroup.Piinlife.CityId);
            cities.Append(PponCityGroup.DefaultPponCityGroup.Family.CityId);

            return cities.ToString();
        }

        public static Dictionary<Guid, Old2NewBid> GetOld2NewBids()
        {
            Dictionary<Guid, Old2NewBid> result = MemoryCache2.Get<Dictionary<Guid, Old2NewBid>>("old2NewBids");
            if (result == null)
            {
                result = GetOld2NewBidsFromFile();
                MemoryCache2.Set("old2NewBids", result, 60);
            }
            return result ?? new Dictionary<Guid, Old2NewBid>();
        }

        public static Dictionary<Guid, Old2NewBid> GetOld2NewBidsFromFile()
        {
            string filePath = Path.Combine(config.WebAppDataPath, "old2newBids.json");
            if (File.Exists(filePath) == false)
            {
                return new Dictionary<Guid, Old2NewBid>();
            }
            try
            {
                string json = File.ReadAllText(filePath);
                List<Old2NewBid> items = ProviderFactory.Instance().GetSerializer().Deserialize<List<Old2NewBid>>(json);
                return items.ToDictionary(t => t.OldBid, t => t);
            }
            catch (Exception ex)
            {
                logger.Warn("無法解析" + filePath, ex);
                return new Dictionary<Guid, Old2NewBid>();
            }
        }

        /// <summary>
        /// 儲存熱銷設定
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="topN"></param>
        /// <param name="days"></param>
        /// <param name="multiple"></param>
        /// <param name="enable"></param>
        /// <param name="isSort"></param>
        /// <returns></returns>
        public static string SaveHotDealSetting(int cityId, int topN, int days, int multiple, bool enable, bool isSort)
        {
            bool isSuccess = false;
            var hdc = _pponProv.HotDealConfigGet(cityId);
            if (hdc.IsLoaded)
            {
                hdc.NDays = days;
                hdc.TopN = topN;
                hdc.NMultiple = multiple;
                hdc.Enable = enable;

                isSuccess = _pponProv.HotDealConfigSet(hdc);
            }

            if (isSuccess)
            {
                if (isSort)
                {
                    var cnt = SortDealTimeSlotByOperatingInfo(cityId, "儲存熱銷並排序");
                    return cnt > 0 ? "儲存並排序成功!" : string.Empty;
                }
                return "儲存成功!";
            }
            return string.Empty;
        }

        #endregion DealTimeSlot 檔次自動排序

        #region PponDeal

        /// <summary>
        /// 根據條件取得Default頁檔次列表 (改寫自DefaultPponPresenter.cs的SetMultipleDeals)
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static List<MultipleMainDealPreview> GetDealListByCondition(PponDealListCondition condition)
        {
            #region 變數宣告
            //回傳列表
            List<MultipleMainDealPreview> multideals = new List<MultipleMainDealPreview>();

            //區域分類
            int cityId = condition.CityId;
            int channelId = condition.ChannelId;
            int areaId = condition.AreaId;
            int subCategoryId = condition.SubCategoryId;

            //新版分類
            int dealCategoryId = condition.NewDealCategoryId;

            //特色檔次
            List<int> filterCategoryIdList = condition.FilterCategoryIdList;

            //過濾檔次用List<int>
            List<int> categoryList = new List<int>()
            {
                areaId, subCategoryId, dealCategoryId
            };
            categoryList = categoryList.Where(x => x > 0).Distinct().ToList();
            #endregion

            #region 過濾 區域分類、新版分類
            multideals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(channelId, areaId, categoryList);

            if (filterCategoryIdList.Count > 0)
            {
                multideals = multideals.Where(x => filterCategoryIdList.Any(y => x.DealCategoryIdList.Contains(y) ||
                                                                                 (y == CategoryManager.Default.LastDay.CategoryId && (new TimeSpan(x.PponDeal.BusinessHourOrderTimeE.Ticks - DateTime.Now.Ticks)).TotalDays < 1))).ToList();
            }

            if (config.IsEveryDayNewDeals)
            {
                //每日下殺分類篩選
                List<MultipleMainDealPreview> newdealList = multideals.ToList();
                int categoryId = config.EveryDayNewDealsCategoryId;
                if (categoryId != 0)
                {
                    if (condition.NewDealCategoryId == categoryId)
                    {
                        foreach (var deal in newdealList)
                        {
                            if (PponFacade.GetEveryDayNewDealFlag(deal.PponDeal.BusinessHourGuid) == "")
                            {
                                multideals.Remove(deal);
                            }
                        }
                    }
                }
            }
            #endregion

            return multideals;
        }

        /// <summary>
        /// 每日精選檔次(依後台設定)
        /// </summary>
        /// <param name="workAreaId"></param>
        /// <param name="isApp"></param>
        /// <returns></returns>
        public static List<MultipleMainDealPreview> GetTodayChoicePponDealSynopses(int workAreaId, bool isApp = true)
        {
            var systemData = _systemProv.SystemDataGet("ChoiceDealSetting");
            if (!systemData.IsLoaded)
            {
                return null;
            }

            ApiChoiceDealSettingModel choiceDealSetting = new JsonSerializer().Deserialize<ApiChoiceDealSettingModel>(systemData.Data);

            //整理所需頻道檔次
            List<int> channelIdList = choiceDealSetting.ChannelList.Select(x => x.ChannelId).Distinct().ToList();
            var channelCounterDic = new Dictionary<int, int>();

            var dealsDic = new Dictionary<int, List<MultipleMainDealPreview>>();
            foreach (var channelId in channelIdList)
            {
                List<int> categoryCriteria = new List<int>();
                categoryCriteria.Add(channelId);

                int areaId = 0;
                if (channelId == CategoryManager.Default.PponDeal.CategoryId)
                {
                    areaId = workAreaId;
                    categoryCriteria.Add(areaId);
                }

                var pponCity = PponCityGroup.GetPponCityByChannel(channelId, areaId);
                var deals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(pponCity, categoryCriteria);
                if (deals.Any())
                {
                    if (isApp)
                    {
                        deals = deals.Where(x => !IsHiddenDeal(x.PponDeal.BusinessHourGuid)).ToList(); //去除隱藏於app檔次
                    }
                    dealsDic.Add(channelId, deals); //有檔次才加入
                    channelCounterDic.Add(channelId, 0);//預設值
                }
            }

            //依後台設定塞精選
            var totalDealsCnt = dealsDic.Sum(dic => dic.Value.Count);
            var totalTakeCnt = choiceDealSetting.TotalCount;
            var allowChannelList = choiceDealSetting.ChannelList.Where(x => dealsDic.ContainsKey(x.ChannelId)).ToList(); //濾掉無檔次channel
            var result = new List<MultipleMainDealPreview>();
            while (result.Count < totalTakeCnt)
            {
                foreach (var allowChanne in allowChannelList)
                {
                    var channelId = allowChanne.ChannelId;
                    var takeCnt = allowChanne.TakeCount; //取幾筆

                    var categoryDeals = dealsDic[channelId];
                    int tempCounter = channelCounterDic[channelId];

                    for (int i = 0; i < takeCnt; i++)
                    {
                        if (tempCounter < categoryDeals.Count)
                        {
                            result.Add(categoryDeals[tempCounter]);
                            channelCounterDic[channelId] = ++tempCounter;

                            if (result.Count == totalDealsCnt)
                            {
                                return result;
                            }
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 是否為DealTimeSlot隱藏檔及公益檔 (判斷檔次是否隱藏於APP)
        /// </summary>
        /// <param name="bGuid"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public static bool IsHiddenDeal(Guid bGuid, int? cityId = null)
        {
            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bGuid, true);
            //排除公益檔次
            if (config.EnableKindDealInApp == false && (EntrustSellType)vpd.EntrustSell.GetValueOrDefault() == EntrustSellType.KindReceipt)
            {
                return true;
            }
            try
            {
                int dealCityId;
                if (cityId == null && string.IsNullOrEmpty(vpd.CityList) == false)
                {
                    var cityList = new JsonSerializer().Deserialize<List<int>>(vpd.CityList);
                    if (cityList.Count == 0)
                    {
                        return true;
                    }
                    dealCityId = cityList.First();
                }
                else
                {
                    dealCityId = cityId.Value;
                }

                IDealTimeSlot dealTimeSlot = ViewPponDealManager.DefaultManager.DealTimeSlotGetByBidCityAndTime(
                    bGuid, dealCityId, DateTime.Now);
                //判斷是否不顯示於檔次列表
                if (dealTimeSlot != null)
                {
                    if (dealTimeSlot.Status == (int)DealTimeSlotStatus.NotShowInPponDefault)
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 取得續接檔次數量(2013/12/11 改為續接母檔，其續接數量為續接母檔slug加上continued_quantity)
        /// </summary>
        /// <param name="ancBid">續接檔次Bid</param>
        /// <param name="ancIsMain">續接檔次是否為主檔</param>
        /// <returns></returns>
        public static int GetPponDealContinuedQuantity(Guid ancBid, bool ancIsMain)
        {
            int slug_quantity = 0;
            if (ancIsMain)
            {
                // 續接檔次為母檔，查找其子檔的slug(若為空值取OrderedQuantity)總和再加上續接母檔的續接數量
                ViewComboDealCollection comboDeals = _pponProv.GetViewComboDealAllByBid(ancBid);
                DealProperty anc_dp = _pponProv.DealPropertyGet(ancBid);
                if (anc_dp.IsLoaded)
                {
                    foreach (var combodeal in comboDeals.Where(x => x.MainBusinessHourGuid != x.BusinessHourGuid))
                    {
                        int slug;
                        if (string.IsNullOrEmpty(combodeal.Slug) || !int.TryParse(combodeal.Slug, out slug))
                        {
                            slug = combodeal.OrderedQuantity;
                        }
                        slug_quantity += slug * (combodeal.IsQuantityMultiplier ? combodeal.QuantityMultiplier ?? 1 : 1);
                    }
                    slug_quantity += anc_dp.ContinuedQuantity;
                }
            }
            else
            {
                // 續接檔次非母檔，查找續接檔的slug(若為空值取OrderedQuantity)再加上續接檔的續接數量
                var ancDeal = _pponProv.ViewPponDealGetByBusinessHourGuid(ancBid);
                if (ancDeal.IsLoaded)
                {
                    int slug;
                    slug_quantity = (int.TryParse(ancDeal.Slug, out slug) ? slug : ancDeal.OrderedQuantity ?? 0) * (ancDeal.IsQuantityMultiplier.GetValueOrDefault(false) ? ancDeal.QuantityMultiplier ?? 1 : 1);
                    slug_quantity += ancDeal.ContinuedQuantity ?? 0;
                }
            }
            return slug_quantity;
        }

        public static ViewPponDeal ViewPponDealGetByBid(Guid bid)
        {
            return _pponProv.ViewPponDealGetByBusinessHourGuid(bid);
        }

        public static bool GetDealIsColsed(Guid bid)
        {
            var ppon = _pponProv.ViewPponDealGetByBusinessHourGuid(bid);
            return GetDealIsColsed(ppon);
        }

        /// <summary>
        /// 更新檔次續接數量
        /// </summary>
        /// <param name="ancbid">續接檔次bid</param>
        public static void UpdateDealPropertyContinuedQuantity(Guid ancbid)
        {
            DealPropertyCollection dps = _pponProv.DealPropertyGetByAncestor(ancbid);
            //可多筆接續檔，故多筆皆須更新
            foreach (DealProperty dp in dps)
            {
                //判斷接續的檔次尚未結檔，才去更新數量
                ViewPponDeal dd = _pponProv.ViewPponDealGetByBusinessHourGuid(dp.BusinessHourGuid);
                if ((DateTime.Compare(dd.BusinessHourOrderTimeE, DateTime.Now) < 0) || (dd.Slug != null))
                {
                    //活動搶購時間(迄)<現在，或是slug數量有值，判斷為已結檔，跳過不更新
                    continue;
                }


                if (dp.IsLoaded && dp.IsContinuedQuantity && dp.AncestorBusinessHourGuid != null)
                {
                    BusinessHour ancBuzHour = _pponProv.BusinessHourGet(dp.AncestorBusinessHourGuid.Value);
                    dp.ContinuedQuantity = GetPponDealContinuedQuantity(ancBuzHour.Guid, Helper.IsFlagSet(ancBuzHour.BusinessHourStatus, BusinessHourStatus.ComboDealMain));
                    _pponProv.DealPropertySet(dp);
                }
            }
            //if (dps.Count > 0 && dps.FirstOrDefault() != null)
            //{
            //    DealProperty dp = dps.FirstOrDefault();
            //    if (dp.IsLoaded && dp.IsContinuedQuantity && dp.AncestorBusinessHourGuid != null)
            //    {
            //        BusinessHour ancBuzHour = _pponProv.BusinessHourGet(dp.AncestorBusinessHourGuid.Value);
            //        dp.ContinuedQuantity = GetPponDealContinuedQuantity(ancBuzHour.Guid, Helper.IsFlagSet(ancBuzHour.BusinessHourStatus, BusinessHourStatus.ComboDealMain));
            //        _pponProv.DealPropertySet(dp);
            //    }
            //}
        }

        /// <summary>
        /// 檢查接續檔次
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="ancBid"></param>
        /// <param name="ancSeqBid"></param>
        /// <param name="ancQuantity"></param>
        /// <returns></returns>
        public static string CheckAncestorBusinessHourGuid(Guid bid, Guid? ancBid, Guid? ancSeqBid, out int ancQuantity)
        {
            ancQuantity = 0;
            string errormessage = string.Empty;
            ViewPponDeal selfDeal = _pponProv.ViewPponDealGetByBusinessHourGuid(bid); //Self

            if (ancBid != null && ancBid != Guid.Empty)
            {
                ViewPponDeal ancDeal = _pponProv.ViewPponDealGetByBusinessHourGuid(ancBid.Value);

                #region 接續數量

                if (null == ancDeal && !ancDeal.IsLoaded)
                {
                    errormessage = "接續數量檔次的 bid : " + ancBid + " 好康不存在。";
                }

                if (DateTime.Compare(selfDeal.BusinessHourOrderTimeS, ancDeal.BusinessHourOrderTimeE) < 0)
                {
                    //errormessage += "續接數量檔次結檔日期晚於此檔開檔日期。";
                }

                if (!PponFacade.CheckPponDealContinuedSelf(bid, ancBid.Value))
                {
                    errormessage += "接續檔不可重複續接回自己。";
                }

                if (Helper.IsFlagSet(ancDeal.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                {
                    errormessage += "接續銷售數字(前台顯示) 不可接續子檔。";
                }

                ancQuantity = PponFacade.GetPponDealContinuedQuantity(ancBid.Value, Helper.IsFlagSet(ancDeal.BusinessHourStatus, BusinessHourStatus.ComboDealMain));

                #endregion
            }

            if (ancSeqBid != null && ancSeqBid != Guid.Empty)
            {
                ViewPponDeal ancSeqDeal = _pponProv.ViewPponDealGetByBusinessHourGuid(ancSeqBid.Value);

                #region 接續憑證

                if (null == ancSeqDeal && !ancSeqDeal.IsLoaded)
                {
                    errormessage = "接續憑證檔次的 bid : " + ancSeqBid + " 好康不存在。";
                }

                if (DateTime.Compare(selfDeal.BusinessHourOrderTimeS, ancSeqDeal.BusinessHourOrderTimeE) < 0)
                {
                    //errormessage += "續接憑證檔次結檔日期晚於此檔開檔日期。";
                }

                if (bid == ancSeqBid)
                {
                    errormessage += "接續憑證號碼不可重複續接回自己。";
                }

                if (Helper.IsFlagSet(ancSeqDeal.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                {
                    errormessage += "接續憑證號碼 不可接續母檔。";
                }
                if (Helper.IsFlagSet(selfDeal.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                {
                    errormessage += "多檔次母檔不可續接憑證號碼。";
                }

                #endregion
            }

            return errormessage;
        }

        /// <summary>
        /// 檢查續接檔次是否續接回自己
        /// </summary>
        /// <param name="curBid">當前檔次Bid</param>
        /// <param name="ancBid">續接檔次Bid</param>
        /// <returns></returns>
        public static bool CheckPponDealContinuedSelf(Guid curBid, Guid ancBid)
        {
            if (curBid == ancBid)
            {
                return false;
            }
            List<Guid> ancBidList = new List<Guid>();
            DealProperty ancDeal = _pponProv.DealPropertyGet(ancBid);
            while (ancDeal.IsLoaded)
            {
                if (!ancBidList.Contains(curBid))
                {
                    if (ancDeal.IsContinuedQuantity == true && ancDeal.AncestorBusinessHourGuid.HasValue)
                    {
                        ancBidList.Add(ancDeal.AncestorBusinessHourGuid.Value);
                        ancDeal = _pponProv.DealPropertyGet(ancDeal.AncestorBusinessHourGuid.Value);
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 檢查檔次的「更多權益」能否正確的在 PponCouponDocument 被 Parse
        /// </summary>
        /// <returns></returns>
        public static bool CheckPponIntroductionValid(PponDeal deal)
        {
            try
            {
                {
                    StringReader sr = new StringReader(deal.DealContent.Introduction);
                    HTMLWorker.ParseToList(sr, null);
                }
                {
                    StringReader sr = new StringReader(deal.DealContent.Restrictions);
                    HTMLWorker.ParseToList(sr, null);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 取得以目前時間判定，現在是歸屬於哪個上檔時間
        /// 舉例:每天07:00以後呼叫，回傳當天07:00；每天07:00以前呼叫，回傳前一天07:00
        /// 2017/04/18 時間改為凌晨整點
        /// </summary>
        /// <returns></returns>
        public static DateTime GetOrderStartTimeAtPresent()
        {
            DateTime now = DateTime.Now;
            DateTime baseDate = DateTime.ParseExact(now.ToString("yyyyMMdd000000"), "yyyyMMddHHmmss", null);
            return baseDate;
        }

        /// <summary>
        /// 取得檔次的所有分類 id
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static ViewAllDealCategoryCollection GetDealAllCategories(Guid bid)
        {
            return _pponProv.ViewAllDealCategoryGetListByBid(bid);
        }

        /// <summary>
        /// 傳入檔次資料中記錄輪播圖片的字串，依據規範回傳檔次主要大圖的路徑，若傳入資料有誤，回傳空字串
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="defaultImageWhenNotFound">當載不到圖時回傳預設圖片</param>
        /// <returns></returns>
        public static string GetPponDealFirstImagePath(string imagePath, bool defaultImageWhenNotFound = false)
        {
            string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePath, MediaType.PponDealPhoto);
            if (imgPaths == null || imgPaths.Length == 0)
            {
                if (defaultImageWhenNotFound)
                {
                    return ImageFacade.EmptyPponImage;
                }
                else
                {
                    return string.Empty;
                }
            }
            return imgPaths[0];
        }

        public static string GetDealStarRating(Guid bid, string mode,
            out decimal ratingScore, out int ratingNumber, out bool ratingShow)
        {
            ratingScore = 0;
            string srString = string.Empty;
            string srHeader = string.Empty;
            string srFooter = "</div>";
            string ratingString = string.Empty;
            ratingNumber = 0;
            ratingShow = false;

            PponDealEvaluateStar evaAvg = ViewPponDealManager.DefaultManager.GetPponDealEvaluateStarList(bid);
            ratingShow = evaAvg != null;

            if (ratingShow)
            {
                srHeader = "<div class=\"rating\">";
                ratingScore = evaAvg.Star;

                if (evaAvg.Cnt > 0)
                {
                    ratingNumber = evaAvg.Cnt;
                    if (mode == "m")
                    {
                        ratingString = "<span class=\"rating_count\">(" + evaAvg.Cnt.ToString() + "人)</span>";
                    }
                    else
                    {
                        ratingString = "<span class=\"rating_count\">(" + evaAvg.Cnt.ToString() + "人使用後評價)</span>";
                    }
                }
                else if (evaAvg.Cnt == 0)
                {
                    srHeader = "<div class=\"rating insufficient\">";
                    ratingScore = 0;
                    ratingString = "<span class=\"rating_count\">(評價資料不足)</span>";
                }
            }

            for (int i = 0; i < Math.Floor(ratingScore); i++)
            {
                srString = srString + "<i class=\"fa fa-star\" aria-hidden=\"true\"></i> ";
            }
            if ((double)ratingScore / 0.5 % 2 != 0)
            {
                srString = srString + "<i class=\"fa fa-star-half-o\" aria-hidden=\"true\"></i> ";
            }
            for (int i = 0; i < Math.Floor(5 - ratingScore); i++)
            {
                srString = srString + "<i class=\"fa fa-star-o\" aria-hidden=\"true\"></i> ";
            }

            //手機版評價不足不顯示星星
            if (ratingShow)
            {
                if (evaAvg.Cnt > 0)
                {
                    srString = srHeader + srString + ratingString + srFooter;
                }
                else if (evaAvg.Cnt == 0)
                {
                    if (mode == "m")
                    {
                        srString = srHeader + ratingString + srFooter;
                    }
                    else
                    {
                        srString = srHeader + srString + ratingString + srFooter;
                    }
                }
            }
            else
            {
                srString = "";
            }

            return srString;
        }

        public static string GetNewDealStarRating(Guid bid, string mode,
            out decimal ratingScore, out int ratingNumber, out bool ratingShow)
        {
            ratingScore = 0;
            string srString = string.Empty;
            string srHeader = string.Empty;
            string srFooter = "</div>";
            string ratingString = string.Empty;
            ratingNumber = 0;
            ratingShow = false;

            PponDealEvaluateStar evaAvg = ViewPponDealManager.DefaultManager.GetPponDealEvaluateStarList(bid);
            ratingShow = evaAvg != null;

            if (ratingShow)
            {
                srHeader = "<div class=\"item_info_rating\">";
                ratingScore = evaAvg.Star;

                if (evaAvg.Cnt > 0)
                {
                    ratingNumber = evaAvg.Cnt;

                    ratingString = "<i class=\"fa fa-star\" aria-hidden=\"true\"></i>"
                                   + "<span class=\"count\"> " + ratingScore.ToString("0.#") + "</span>"
                                   + "<span\"> (" + evaAvg.Cnt.ToString() + ")</span>";
                }
                else if (evaAvg.Cnt == 0)
                {
                    ratingScore = 0;
                    ratingString = "<span style=\"display: block\">(評價累積中)</span>";
                }
            }

            //手機版評價不足不顯示星星
            if (ratingShow)
            {
                if (evaAvg.Cnt > 0)
                {
                    srString = srHeader + srString + ratingString + srFooter;
                }
                else if (evaAvg.Cnt == 0)
                {
                    if (mode == "m")
                    {
                        srString = srHeader + ratingString + srFooter;
                    }
                    else
                    {
                        srString = srHeader + srString + ratingString + srFooter;
                    }
                }
            }
            else
            {
                srString = "";
            }

            return srString;
        }

        /// <summary>
        /// 目前是給M版商品頁用的取得檔次倒數副程式
        /// 如需倒數，回傳譬如86400000為倒數一天
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static string GetCountdownString(IViewPponDeal deal)
        {
            PponDealStage dealStage = deal.GetDealStage();

            switch (dealStage)
            {
                case PponDealStage.Created:
                case PponDealStage.Ready:
                    return "即將開賣";
                    break;

                case PponDealStage.ClosedAndFail:
                    return "已結束販售";

                case PponDealStage.ClosedAndOn:
                case PponDealStage.CouponGenerated:
                    return "已結束販售";


                default:
                    DateTime end = deal.BusinessHourOrderTimeE;
                    if (IsEveryDayNewDeal(deal.BusinessHourGuid))
                    {
                        EveryDayNewDeal flag = ViewPponDealManager.DefaultManager.GetEveryDayNewDealByBid(deal.BusinessHourGuid);
                        if (flag == EveryDayNewDeal.Limited24HR)
                        {
                            end = deal.BusinessHourOrderTimeS.AddDays(1);
                        }
                        else if (flag == EveryDayNewDeal.Limited48HR)
                        {
                            end = deal.BusinessHourOrderTimeS.AddDays(2);
                        }
                        else if (flag == EveryDayNewDeal.Limited72HR)
                        {
                            end = deal.BusinessHourOrderTimeS.AddDays(3);
                        }

                        if (end > deal.BusinessHourOrderTimeE)
                        {
                            end = deal.BusinessHourOrderTimeE;
                        }
                    }
                    int daysToClose = (int)(end - DateTime.Now).TotalDays;


                    if (daysToClose > 3)
                    {
                        return "限時優惠中!";
                    }

                    return ((int)(end - DateTime.Now).TotalMilliseconds).ToString();
            }
        }

        /// <summary>
        /// GSA 要求需在第1個屏幕顯示內，有上傳給GSA的價格
        /// 
        /// 給M版用的
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static string GetGsaPriceString(IViewPponDeal mainDeal)
        {
            string gclid = HttpContext.Current.Request["gclid"];
            if (mainDeal.DeliveryType == (int)DeliveryType.ToHouse && (config.DealPagePriceMode == 1 || config.DealPagePriceMode == 2 && gclid != null))
            {
                ViewComboDeal comboDeal = null;
                foreach (var t in mainDeal.ComboDelas)
                {
                    var subDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(t.BusinessHourGuid, true);
                    if (subDeal != null && subDeal.IsLoaded && subDeal.IsSoldOut == false)
                    {
                        comboDeal = t;
                        break;
                    }
                }
                if (comboDeal != null)
                {
                    string[] titleParts = comboDeal.Title.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    string prodEntityTitle;
                    if (titleParts.Length > 1)
                    {
                        prodEntityTitle = titleParts.Last();
                    }
                    else
                    {
                        prodEntityTitle = comboDeal.Title;
                    }
                    return string.Format("最低購買：{0} ${1}", prodEntityTitle, (int)comboDeal.ItemPrice);
                }
            }
            return string.Empty;
        }

        public static List<IViewPponDeal> GetRelatedDeals(IViewPponDeal deal)
        {
            int logicType;

            List<IViewPponDeal> relatedDeals = new List<IViewPponDeal>();
            int relatedDealsLimit = 12;
            if (deal.DeliveryType == (int)DeliveryType.ToHouse)
            {
                List<PponDealSynopsisBasic> recommendDeals = PponDealApiManager.GetPponDealSynopsisBasicRecommendDeal(
                    deal.BusinessHourGuid, relatedDealsLimit, out logicType);

                foreach (PponDealSynopsisBasic recommendDeal in recommendDeals)
                {
                    IViewPponDeal relatedDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(recommendDeal.Bid, true);
                    relatedDeals.Add(relatedDeal);
                }
            }
            else
            {
                //憑證先取同賣家檔次
                relatedDeals.AddRange(ViewPponDealManager.DefaultManager.GetOnlineDealsBySeller(deal.SellerGuid, deal.BusinessHourGuid));
                if (relatedDeals.Count < relatedDealsLimit)
                {
                    List<PponDealSynopsisBasic> recommendDeals = PponDealApiManager.GetPponDealSynopsisBasicRecommendDeal(
                        deal.BusinessHourGuid, relatedDealsLimit, out logicType);
                    foreach (PponDealSynopsisBasic recommendDeal in recommendDeals)
                    {
                        IViewPponDeal relatedDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(recommendDeal.Bid, true);
                        if (relatedDeals.Exists(t => t.BusinessHourGuid == recommendDeal.Bid))
                        {
                            continue;
                        }
                        relatedDeals.Add(relatedDeal);
                        if (relatedDeals.Count == relatedDealsLimit)
                        {
                            break;
                        }
                    }
                }
                else if (relatedDeals.Count > relatedDealsLimit)
                {
                    relatedDeals.RemoveRange(relatedDealsLimit, relatedDeals.Count - relatedDealsLimit);
                }
            }

            return relatedDeals;
        }

        public static List<IViewPponDeal> GetWatchOtherDeals(IViewPponDeal deal)
        {
            List<IViewPponDeal> watchDeals = new List<IViewPponDeal>();
            List<Guid> watchDealsBidList = new List<Guid>();
            //抓取12筆關聯Bid
            watchDealsBidList = _pponProv.GetGuidListByMainBid(deal.BusinessHourGuid);
            foreach (var item in watchDealsBidList)
            {
                IViewPponDeal relatedDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(item, true);
                watchDeals.Add(relatedDeal);
            }

            return watchDeals;
        }

        public const string _BLANK_IMAGE_SRC = "data:image/png;base64,R0lGODlhFAAUAIAAAP///wAAACH5BAEAAAAALAAAAAAUABQAAAIRhI+py+0Po5y02ouz3rz7rxUAOw==";
        /// <summary>
        /// 取得每日下殺圖片路徑
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static string GetEveryDayNewDealFlag(Guid bid)
        {
            if (config.IsEveryDayNewDeals)
            {
                EveryDayNewDeal flag = ViewPponDealManager.DefaultManager.GetEveryDayNewDealByBid(bid);
                if (flag != EveryDayNewDeal.None)
                {
                    return "https://www.17life.com/images/17P/active/17sticker/sticker_" + (
                        flag == EveryDayNewDeal.Limited24HR ? "24" :
                        flag == EveryDayNewDeal.Limited48HR ? "48" :
                        flag == EveryDayNewDeal.Limited72HR ? "72" :
                        flag == EveryDayNewDeal.Time ? "time" : "") + ".png";
                }
                else
                {
                    //非每日下殺或已過優惠時間
                    return _BLANK_IMAGE_SRC;
                }
            }
            else
            {
                return _BLANK_IMAGE_SRC;
            }

        }

        /// <summary>
        /// 是否為每日下殺檔次
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static bool IsEveryDayNewDeal(Guid bid)
        {
            if (config.IsEveryDayNewDeals)
            {
                int categoryId = config.EveryDayNewDealsCategoryId;
                var cdc = ViewPponDealManager.DefaultManager.GetCategoryIds(bid).Contains(categoryId);
                return cdc;
            }
            else
            {
                return false;
            }

        }

        public static string GetClosedDealExpireRedirectUrl(IViewPponDeal mainDeal, string oriExpireRedirectUrl)
        {
            string expireRedirectUrl = string.Empty;
            if (mainDeal.Slug == null)
            {
                return expireRedirectUrl;
            }
            BusinessHour bh = _pponProv.BusinessHourGet(mainDeal.BusinessHourGuid);
            oriExpireRedirectUrl = bh.ExpireRedirectUrl;
            expireRedirectUrl = oriExpireRedirectUrl;
            if (!string.IsNullOrEmpty(oriExpireRedirectUrl) && oriExpireRedirectUrl.IndexOf("/deal/") >= 0)
            {
                //判斷要導向的檔次是否已結檔
                int rdiIdx = oriExpireRedirectUrl.IndexOf("/deal/");
                string rdiBid = oriExpireRedirectUrl.Substring(rdiIdx + "/deal/".Length, Guid.Empty.ToString().Length);
                Guid newBid = Guid.Empty;
                if (Guid.TryParse(rdiBid, out newBid))
                {
                    IViewPponDeal linkDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(newBid);
                    if (linkDeal.Slug != null)
                    {
                        IViewPponDeal similarDeal = PponFacade.GetSimilarDeal(mainDeal);
                        if (similarDeal != null)
                        {
                            expireRedirectUrl = PponFacade.ExpireRedirectUrl(similarDeal, mainDeal.PicAlt);
                        }
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(oriExpireRedirectUrl))
                {
                    expireRedirectUrl = oriExpireRedirectUrl;
                }
            }
            return expireRedirectUrl;
        }
        public static IViewPponDeal GetSimilarDeal(IViewPponDeal deal)
        {
            List<IViewPponDeal> relatedDeals = PponFacade.GetRelatedDeals(deal);

            //將檔次和原檔次比對，最相近的排最前面
            IViewPponDeal similarDeal = null;
            Dictionary<IViewPponDeal, decimal> vpds = new Dictionary<IViewPponDeal, decimal>();
            foreach (var relatedDeal in relatedDeals)
            {
                LevenshteinDistance distance = new LevenshteinDistance();
                decimal d = distance.LevenshteinDistancePercent(deal.ItemName, relatedDeal.ItemName);
                if (d >= decimal.Parse("0.6"))
                {
                    similarDeal = relatedDeal;
                    break;
                }
            }
            return similarDeal;
        }
        public static string ExpireRedirectUrl(IViewPponDeal similarDeal, string picAlt)
        {
            string expireRedirectUrl = string.Empty;
            if (similarDeal != null)
            {
                expireRedirectUrl = string.Format("{0}/deal/{1}", config.SSLSiteUrl, similarDeal.BusinessHourGuid);
            }
            else
            {
                string keyWord = string.Empty;
                if (!string.IsNullOrEmpty(picAlt))
                {
                    keyWord = picAlt.Split("/")[0];
                }
                if (!string.IsNullOrEmpty(keyWord))
                {
                    expireRedirectUrl = string.Format("{0}/ppon/pponsearch.aspx?search={1}", config.SSLSiteUrl, HttpUtility.UrlEncode(keyWord));
                }
            }
            return expireRedirectUrl;
        }

        #endregion PponDeal

        #region DealInfoTranslate

        /// <summary>
        /// 過濾低毛利
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="defaultMinGrossMargin">預設低毛利門檻</param>
        /// <param name="couponGrossMargin">憑證低毛利門檻</param>
        /// <param name="deliveryGrossMargin">宅配低毛利門檻</param>
        /// <returns></returns>
        private static bool IsLowGrossMarginDeal(Guid bid, decimal defaultMinGrossMargin, decimal couponGrossMargin, decimal deliveryGrossMargin)
        {
            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            if (!vpd.IsLoaded) //快取沒檔次直接排除
            {
                return true;
            }

            var channelMinGrossMargin = vpd.DeliveryType == (int)DeliveryType.ToShop
                ? couponGrossMargin : vpd.DeliveryType == (int)DeliveryType.ToHouse
                ? deliveryGrossMargin : defaultMinGrossMargin;

            if (vpd.ComboDelas != null && vpd.ComboDelas.Any()) //多檔次母檔有任何子檔低毛利整檔排除
            {
                return ChannelFacade.IsOverLowGrossmarginDealsForShoppingGuide(vpd.ComboDelas.Select(x => x.BusinessHourGuid).ToList()
                    , channelMinGrossMargin);
            }

            BaseCostGrossMargin cost = ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(vpd.BusinessHourGuid);
            if (cost == null || cost.MinGrossMargin == 0)
            {
                return true;
            }

            if (Math.Round(cost.MinGrossMargin, 4) < channelMinGrossMargin)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cpaCode"></param>
        /// <param name="queryDate"></param>
        /// <param name="grossMargin">預設低毛利門檻 預設1.0m(10%)</param>
        /// <param name="couponGrossMargin">憑證低毛利門檻 預設1.0m(10%)</param>
        /// <param name="deliveryGrossMargin">宅配低毛利門檻 預設1.0m(10%)</param>
        /// <param name="cityType"></param>
        /// <returns></returns>
        public static List<DealInfoTranslate> DealInfoTranslateGetList(string cpaCode, DateTime queryDate, decimal grossMargin = 1.0m, decimal couponGrossMargin = 1.0m, decimal deliveryGrossMargin = 1.0m, PponDealSpecialCityType cityType = PponDealSpecialCityType.WithOutSpecialCity)
        {
            ConcurrentDictionary<Guid, List<DealCategory>> dealCategoryCol;
            var bidlist = GetShoppingGuideDealBid(ShoppingGuideDealType.Overall, queryDate, out dealCategoryCol, grossMargin,
                couponGrossMargin, deliveryGrossMargin, cityType, true, true);

            return bidlist.Select(bid => DealInfoTranslateGet(bid, cpaCode, dealCategoryCol.First(x => x.Key == bid).Value)).ToList();
        }

        public static List<Guid> GetShoppingGuideKeepBid(List<ViewAllDealCategory> vadc, ShoppingGuideDealType type)
        {
            IEnumerable<ViewAllDealCategory> tempData = new List<ViewAllDealCategory>();
            List<Guid> keepBids = new List<Guid>();
            List<Guid> removeBids = new List<Guid>();
            switch (type)
            {
                case ShoppingGuideDealType.Coupon:
                    tempData = vadc.ToList().Where(x => x.Cid == PponCityGroup.DefaultPponCityGroup.AllCountry.ChannelId);
                    if (tempData.Any())
                    {
                        removeBids = tempData.Select(x => x.Bid).ToList();
                    }
                    break;
                case ShoppingGuideDealType.Delivery:
                    tempData = vadc.ToList().Where(x => x.Cid == PponCityGroup.DefaultPponCityGroup.AllCountry.ChannelId);
                    if (tempData.Any())
                    {
                        keepBids = tempData.Select(x => x.Bid).ToList();
                    }
                    break;
                case ShoppingGuideDealType.Food:
                    tempData = vadc.ToList().Where(x => x.Cid == PponCityGroup.DefaultPponCityGroup.TaipeiCity.ChannelId);
                    if (tempData.Any())
                    {
                        keepBids = tempData.Select(x => x.Bid).ToList();
                    }
                    break;
                case ShoppingGuideDealType.Piinlife:
                    tempData = vadc.ToList().Where(x => x.Cid == PponCityGroup.DefaultPponCityGroup.Piinlife.ChannelId);
                    if (tempData.Any())
                    {
                        keepBids = tempData.Select(x => x.Bid).ToList();
                    }
                    break;
                case ShoppingGuideDealType.Travel:
                    tempData = vadc.ToList().Where(x => x.Cid == PponCityGroup.DefaultPponCityGroup.Travel.ChannelId);
                    if (tempData.Any())
                    {
                        keepBids = tempData.Select(x => x.Bid).ToList();
                    }
                    break;
                case ShoppingGuideDealType.BeautyAndLeisure:
                    tempData = vadc.ToList().Where(x => x.Cid == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.ChannelId);
                    if (tempData.Any())
                    {
                        keepBids = tempData.Select(x => x.Bid).ToList();
                    }
                    break;
            }

            if (keepBids.Any())
            {
                return keepBids;
            }

            vadc = vadc.Where(x => !removeBids.Contains(x.Bid)).ToList();
            if (vadc.Any())
            {
                return vadc.Select(x => x.Bid).ToList();
            }

            return new List<Guid>();
        }

        public static List<Guid> GetShoppingGuideDealBid(ShoppingGuideDealType type, DateTime queryDate
            , out ConcurrentDictionary<Guid, List<DealCategory>> dealCategoryCol
            , decimal grossMargin = 1.0m, decimal couponGrossMargin = 1.0m, decimal deliveryGrossMargin = 1.0m
            , PponDealSpecialCityType cityType = PponDealSpecialCityType.WithOutSpecialCity, bool removeSoldOut = false, bool newFilterLowGM = false)
        {
            var resultBids = ChannelFacade.GetDealBidFromDbCache(type, queryDate, out dealCategoryCol, newFilterLowGM, couponGrossMargin, deliveryGrossMargin);

            if (!resultBids.Any())
            {
                dealCategoryCol = new ConcurrentDictionary<Guid, List<DealCategory>>();
                resultBids = ChannelFacade.GetDealBidByShoppingGuideDealType(type, queryDate, out dealCategoryCol, cityType);
                if (newFilterLowGM)
                {
                    var filterBids = ChannelFacade.GetFilterLowGrossMarginDeal(type, queryDate, couponGrossMargin, deliveryGrossMargin);
                    resultBids = resultBids.Intersect(filterBids).ToList();
                }
            }

            //排除已售完
            if (removeSoldOut)
            {
                resultBids = resultBids.Where(x => !ChannelFacade.IsDealSoldOut(x)).ToList();
            }

            //排除低毛利
            if (!newFilterLowGM && config.FilterLowGrossMarginDealForShoppingGuideApi)
            {
                resultBids = resultBids.Where(x => !IsLowGrossMarginDeal(x, grossMargin, couponGrossMargin, deliveryGrossMargin)).ToList();
            }

            return resultBids;
        }

        public static DealInfoTranslate DealInfoTranslateGet(Guid bid, string cpaCode, List<DealCategory> adcList = null)
        {
            string cpaStr = string.Empty;
            if (!string.IsNullOrWhiteSpace(cpaCode))
            {
                cpaStr = "&rsrc=" + cpaCode;
            }

            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(vpd);

            var dealInfo = new DealInfoTranslate();
            foreach (var city in new JsonSerializer().Deserialize<List<string>>(vpd.CityList))
            {
                int cityId;
                if (!int.TryParse(city, out cityId)) { continue; }

                dealInfo.Area.Add(new DealAreaInfo() { Id = cityId, Name = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId).CityName });
            }
            dealInfo.DealType = vpd.DealType.ToString();
            if (vpd.DealType != null)
            {
                dealInfo.DealTypeDesc = SystemCodeManager.GetDealTypeName(vpd.DealType.Value);
            }

            if (adcList != null)
            {
                //for List使用
                dealInfo.Category = adcList;
            }
            else
            {
                //for 單筆使用
                dealInfo.Category = _pponProv.ViewAllDealCategoryGetListByBid(bid).Select(x => new DealCategory
                {
                    Id = x.Cid,
                    Name = x.Name
                }).ToList();
            }

            var isPiinLife = dealInfo.Category.Any(x => x.Id == CategoryManager.Default.Piinlife.CategoryId);

            string cEventName = EventNameGet(vpd);

            dealInfo.DealId = vpd.UniqueId.GetValueOrDefault();
            dealInfo.Title = cEventName;
            dealInfo.ShortTitle = !string.IsNullOrWhiteSpace(vpd.AppTitle) ? vpd.AppTitle : vpd.CouponUsage;
            dealInfo.Description = vpd.EventTitle;
            dealInfo.SquareImagePath = (string.IsNullOrEmpty(vpd.AppDealPic)) ? PponFacade.GetPponDealFirstImagePath(vpd.EventImagePath) : PponFacade.GetPponDealFirstImagePath(vpd.AppDealPic);

            string[] imagePaths = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto);
            dealInfo.PhotoUrls = imagePaths.Where(path => !string.IsNullOrWhiteSpace(path.Trim())).ToList();

            //將品生活寬版首圖移至最後
            if (isPiinLife && dealInfo.PhotoUrls.Count() > 1)
            {
                var wideImg = dealInfo.PhotoUrls.First();
                dealInfo.PhotoUrls.Remove(wideImg);
                dealInfo.PhotoUrls.Add(wideImg);
            }

            dealInfo.DealUrl = string.Format("{0}?bid={1}{2}"
                , (isPiinLife) ? config.PiinLifeDefaultPageUrl : config.PponDefaultPageUrl
                , vpd.BusinessHourGuid.ToString()
                , cpaStr);
            dealInfo.BuyNowUrl = string.Format("{0}/Ppon/buy.aspx?bid={1}{2}{3}"
                , config.SSLSiteUrl
                , vpd.BusinessHourGuid.ToString()
                , cpaStr
                , (isPiinLife) ? "&style=pl" : string.Empty);
            dealInfo.OriginPrice = vpd.ItemOrigPrice;

            dealInfo.SellPrice = vpd.ItemPrice;
            if (config.EnableGSASubstituteDiscountPriceForPrice && vpd.DiscountPrice != null)
            {
                dealInfo.SellPrice = vpd.DiscountPrice.Value;
            }

            dealInfo.Discount = ViewPponDealManager.GetDealDiscountString(vpd);
            dealInfo.Total = vpd.OrderTotalLimit != null ? vpd.OrderTotalLimit.Value : 999999;
            dealInfo.OrderedQuantity = vpd.GetAdjustedOrderedQuantity().Quantity;
            dealInfo.QuantityMin = vpd.BusinessHourOrderMinimum;
            dealInfo.DealStartTime = vpd.BusinessHourOrderTimeS.ToString("yyyy-MM-ddTHH:mm:ss");
            dealInfo.DealEndTime = vpd.BusinessHourOrderTimeE.ToString("yyyy-MM-ddTHH:mm:ss");
            dealInfo.Restriction = contentLite.Introduction;
            var availablitys = ProviderFactory.Instance().GetSerializer()
                .Deserialize<AvailableInformation[]>(contentLite.Availability).ToList();
            foreach (var availablity in availablitys)
            {
                var dealStore = new DealStore();
                dealStore.StoreName = availablity.N;
                dealStore.Address = availablity.A;
                dealStore.Phone = availablity.P;
                dealStore.BusinessHours = availablity.OT;
                dealStore.WebUrl = availablity.U;
                dealStore.TrafficInfo = string.Empty;
                var trafficInfo = new List<string>();
                if (!string.IsNullOrWhiteSpace(availablity.MR))
                {
                    trafficInfo.Add(availablity.MR);
                }

                if (!string.IsNullOrWhiteSpace(availablity.CA))
                {
                    trafficInfo.Add(availablity.CA);
                }

                if (!string.IsNullOrWhiteSpace(availablity.BU))
                {
                    trafficInfo.Add(availablity.BU);
                }

                if (!string.IsNullOrWhiteSpace(availablity.OV))
                {
                    trafficInfo.Add(availablity.OV);
                }
                dealStore.TrafficInfo = string.Join("/", trafficInfo);
                dealInfo.Stores.Add(dealStore);
            }
            return dealInfo;
        }

        /// <summary>
        /// 依Bid搜尋檔次
        /// </summary>
        /// <param name="businessHourGuid">bid</param>
        /// <returns></returns>
        public static IViewPponDeal ViewPponDealGetByGuid(Guid businessHourGuid)
        {
            return ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(businessHourGuid);
        }

        /// <summary>
        /// 依Bid搜尋DealAccounting
        /// </summary>
        /// <param name="businessHourGuid">bid</param>
        /// <returns></returns>
        public static DealAccounting DealAccountingGetByGuid(Guid businessHourGuid)
        {
            return _pponProv.DealAccountingGet(businessHourGuid);
        }

        // <summary>
        /// GroupOrder
        /// </summary>
        /// <param name="businessHourGuid">bid</param>
        /// <returns></returns>
        public static GroupOrder GroupOrderGetByGuid(Guid businessHourGuid)
        {
            return _orderProv.GroupOrderGetByBid(businessHourGuid);
        }

        /// <summary>
        /// 取得檔次的上檔頻道
        /// </summary>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public static List<int> DealPropertyCityListGetByGuid(Guid businessHourGuid)
        {
            var dealProperty = _pponProv.DealPropertyGet(businessHourGuid);
            return dealProperty.IsLoaded ? new JsonSerializer().Deserialize<List<string>>(dealProperty.CityList).Select(city => int.Parse(city)).ToList() : new List<int>();
        }

        #endregion DealInfoTranslate

        #region HTC

        public static HtcDealRtn HtcPponDealGetListByRunning(string cbaCode)
        {
            var rtn = new HtcDealRtn();
            rtn.deals = new HtcDeals();
            rtn.deals.modification_date = DateTime.Now.ToString(rfc822DateimeFormat, DateTimeFormatInfo.InvariantInfo) + "00";
            rtn.deals.deal = new List<HtcPponDeal>();

            var workSDate = DateTime.Today;
            var workEDate = workSDate.AddDays(1);
            var cbaStr = string.Empty;
            if (!string.IsNullOrWhiteSpace(cbaCode))
            {
                cbaStr = "&rsrc=" + cbaCode;
            }
            var vpdc = _pponProv.ViewPponDealGetListByDayWithNoLock(workSDate, workEDate, PponDealSpecialCityType.WithOutSpecialCity, true);
            string categoryStr = "";
            foreach (var deal in vpdc)
            {
                var dealProperty = _pponProv.DealPropertyGet(deal.BusinessHourGuid);
                var htcDeal = new HtcPponDeal();

                if (dealProperty.DealType != null)
                {
                    var category = PponApiCategoryManager.HamiCategoryGetByTypeCode(dealProperty.DealType.Value);
                    if (category != null)
                    {
                        categoryStr = category.HtcCategoryDesc;
                    }
                }

                htcDeal.id_deal = dealProperty.UniqueId.ToString();
                htcDeal.deal_provider = HTCDataModel.DefaultDealProvider;
                htcDeal.featured = "false";
                htcDeal.categories = new HtcCategory() { category = categoryStr };
                htcDeal.national = "false";
                htcDeal.country = "TW";
                htcDeal.price = deal.ItemPrice.ToString("0") + "NT";
                htcDeal.reduction = (deal.ItemOrigPrice - deal.ItemPrice).ToString("0") + "NT";
                htcDeal.savings = (deal.ItemPrice * 100 / deal.ItemOrigPrice).FractionFloor(2).ToString("N1") + "%";
                htcDeal.start_date = deal.BusinessHourOrderTimeS.ToString(rfc822DateimeFormat, DateTimeFormatInfo.InvariantInfo) + "00";
                htcDeal.end_date = deal.BusinessHourOrderTimeE.ToString(rfc822DateimeFormat, DateTimeFormatInfo.InvariantInfo) + "00";
                var conditions = new HtcCondition()
                {
                    min_number_purchaser = deal.BusinessHourOrderMinimum.ToString("0"),
                    max_number_purchaser = deal.OrderTotalLimit == null ? "999999" : deal.OrderTotalLimit.Value.ToString("0"),
                    redeemed_start_date = deal.BusinessHourDeliverTimeS.Value.ToString(rfc822DateimeFormat, DateTimeFormatInfo.InvariantInfo) + "00",
                    redeemed_end_date = deal.BusinessHourDeliverTimeE.Value.ToString(rfc822DateimeFormat, DateTimeFormatInfo.InvariantInfo) + "00",
                };
                htcDeal.conditions = conditions;
                string[] imagePaths = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto);
                var language = new HtcLanguage()
                {
                    @attributes = new HtcLanguage.HtcLanguageAttributes() { id = "zh" },
                    catch_phrase = deal.EventTitle,
                    short_description = deal.ItemName,
                    long_description = deal.EventName,
                    purchase_url = config.PponDefaultMobilePageUrl + "?bid=" + deal.BusinessHourGuid.ToString() + cbaStr,
                    preview_image = imagePaths.FirstOrDefault(),
                    discount_conditions = string.Empty,
                    action = "Buy"
                };
                htcDeal.language = language;
                var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
                var availablitys = ProviderFactory.Instance().GetSerializer()
                    .Deserialize<AvailableInformation[]>(contentLite.Availability).ToList();
                var merchant = new HtcMerchant()
                {
                    merchant_name = deal.SellerName,
                    web_address = string.Empty
                };
                merchant.location = new HtcLocation();
                if (availablitys.Count > 0)
                {
                    var availablity = availablitys.First();
                    merchant.location.postal_address = availablity.A;
                    merchant.location.phone_number = availablity.P;
                    var geography = LocationFacade.GetGeography(availablity.A);
                    if (geography != null)
                    {
                        merchant.location.lat = geography.Lat.ToString();
                        merchant.location.lon = geography.Long.ToString();
                    }
                    else
                    {
                        //預設值
                        merchant.location.lat = "23.973875";
                        merchant.location.lon = "120.979682";
                    }
                }
                htcDeal.merchant = merchant;

                rtn.deals.deal.Add(htcDeal);
            }
            return rtn;
        }

        #endregion HTC

        #region New Cpa

        //預設造訪幾次做一次cpa存取
        /// <summary>
        /// 檢查新版CPA，判斷目前運行和Client Session存在的資料，依各類別新增或略過
        /// </summary>
        /// <param name="cpa_code">QueryString Cpa代碼</param>
        /// <param name="page_name">目前頁面名稱</param>
        /// <param name="cpa_session">Client Session</param>
        /// <param name="client_cpa_data">傳址是否有cpa紀錄</param>
        /// <returns></returns>
        public static bool CpaCheck(string rsrc_code, string cpa_code, string content, string page_name, object cpa_session, out CpaClientMain<CpaClientEvent<CpaClientDeail>> client_cpa_data)
        {
            // 取得目前運行的cpa資料
            CpaMainCollection cmc = ViewPponDealManager.DefaultManager.CpaMainCollectionGetAll();
            if (cmc.Count > 0)
            {
                // 若有運行中的cpa資料，則需判斷session中是否有值、或者過期則刪除
                CpaClientMain<CpaClientEvent<CpaClientDeail>> cpacliectmain;
                //判斷session是否為null給定初始
                if (cpa_session is CpaClientMain<CpaClientEvent<CpaClientDeail>>)
                {
                    cpacliectmain = (CpaClientMain<CpaClientEvent<CpaClientDeail>>)cpa_session;
                }
                else
                {
                    cpacliectmain = new CpaClientMain<CpaClientEvent<CpaClientDeail>>();
                }

                //判斷是否為第一次接收Cpa querystring
                CpaMain main = cmc.FirstOrDefault(x => x.CpaCode.Equals(cpa_code, StringComparison.OrdinalIgnoreCase));
                if (!string.IsNullOrWhiteSpace(cpa_code) && main != null && cpacliectmain.All(x => x.MainId != main.Id))
                {
                    Random r = new Random();
                    //是否在抽樣比例中
                    if (r.Next(100) <= main.Ratio)
                    {
                        cpacliectmain.Add(new CpaClientEvent<CpaClientDeail>(main.Id) { new CpaClientDeail(page_name, rsrc_code, content) });
                    }
                }
                //非購買暫時不做歷程紀錄
                else
                {
                }

                if (cpacliectmain.Count > 0)
                {
                    client_cpa_data = cpacliectmain;
                    return true;
                }
                else
                {
                    client_cpa_data = null;
                    return false;
                }
            }
            else
            {
                client_cpa_data = null;
                return false;
            }
        }

        public static CpaClientMain<CpaClientEvent<CpaClientDeail>> CpaSetDetailToDB(string userName, string sessionId, string ip,
            CpaClientMain<CpaClientEvent<CpaClientDeail>> cpacliectmain, Guid? orderGuid = null)
        {
            if (string.IsNullOrEmpty(cpacliectmain.UserName))
            {
                cpacliectmain.UserName = userName;
            }

            string keepUserName = cpacliectmain.UserName;
            cpacliectmain.SessionId = sessionId;
            cpacliectmain.Ip = ip;
            foreach (var item in cpacliectmain)
            {
                foreach (var detail in item)
                {
                    string pageName = detail.PageName;
                    if (pageName.Length > 150)
                    {
                        pageName = pageName.Substring(150);
                    }
                    _pponProv.CpaDetailSet(new CpaDetail()
                    {
                        MainId = item.MainId,
                        UserName = keepUserName,
                        SessionId = sessionId,
                        Ip = ip,
                        OrderGuid = orderGuid,
                        PageName = pageName,
                        CreateTime = detail.CreateDate,
                        Rsrc = detail.Rsrc,
                        Content = detail.Content
                    });
                }
            }
            cpacliectmain.Clear();
            return cpacliectmain;
        }

        #endregion New Cpa

        #region FamilyDeal

        public static void PeztempUpdatePrintTime(string pezcode, DateTime time, int cvsid)
        {
            _pponProv.PeztempUpdatePrintTime(pezcode, time);
            _pponProv.PeztempUpdateCvsId(pezcode, cvsid);
        }

        public static int CvsIdGet(string cvsstoreid)
        {
            return _pponProv.CvsStoreGet(cvsstoreid);
        }

        public static void PeztempErrorLogInsert(string pezcode, string cvsstoreid, int status)
        {
            _pponProv.PezErrorLogInsert(pezcode, cvsstoreid, status);
        }

        public static bool PeztempValidator(string pezcode)
        {
            return _pponProv.PezTempValidator(pezcode);
        }

        #endregion FamilyDeal

        public static void AddComboDeal(PponDeal comboMain, ComboItem comboSub, string userName)
        {
            var comboDeals = _pponProv.GetViewComboDealAllByBid(comboMain.Deal.Guid).ToList();

            if (comboDeals.Count == 0)
            {
                ComboDeal mdeal = new ComboDeal();
                mdeal.Title = "主檔"; //嗯?我也不知道為啥他就叫主檔，但似乎前臺未使用
                mdeal.CreateTime = DateTime.Now;
                mdeal.Creator = userName;
                mdeal.BusinessHourGuid = comboMain.Deal.Guid;
                mdeal.MainBusinessHourGuid = comboMain.Deal.Guid;
                mdeal.Sequence = -1;
                _pponProv.AddComboDeal(mdeal);
            }

            ComboDeal deal = new ComboDeal();
            deal.Title = comboSub.Title;
            deal.CreateTime = DateTime.Now;
            deal.Creator = userName;
            deal.BusinessHourGuid = comboSub.Bid;
            deal.MainBusinessHourGuid = comboMain.Deal.Guid;
            deal.Sequence = comboDeals.Count;
            _pponProv.AddComboDeal(deal);
        }

        public static void ResetComboDeal(PponDeal comboMain, List<ComboItem> comboSubDeals, string userName)
        {
            //包括主檔與子檔，第1筆是主檔，特徵是 BusinessHourGuid 跟 MainBusinessHourGuid相同
            var comboDeals = _pponProv.GetViewComboDealAllByBid(comboMain.Deal.Guid).ToList();
            foreach (var item in comboDeals)
            {
                if (item.BusinessHourGuid == item.MainBusinessHourGuid)
                {
                    _pponProv.DeleteComboDeal(item.MainBusinessHourGuid.Value);
                }
                else
                {
                    _pponProv.DeleteComboDeal(item.Id, item.BusinessHourGuid);
                }
            }

            if (comboSubDeals.Count == 0)
            {
                return;
            }
            ComboDeal mdeal = new ComboDeal();
            mdeal.Title = "主檔"; //嗯?我也不知道為啥他就叫主檔，但似乎前臺未使用
            mdeal.CreateTime = DateTime.Now;
            mdeal.Creator = userName;
            mdeal.BusinessHourGuid = comboMain.Deal.Guid;
            mdeal.MainBusinessHourGuid = comboMain.Deal.Guid;
            mdeal.Sequence = -1;
            _pponProv.AddComboDeal(mdeal);
            int sequence = 0;
            foreach (var item in comboSubDeals)
            {
                ComboDeal deal = new ComboDeal();
                deal.Title = item.Title;
                deal.CreateTime = DateTime.Now;
                deal.Creator = userName;
                deal.BusinessHourGuid = item.Bid;
                deal.MainBusinessHourGuid = comboMain.Deal.Guid;
                deal.Sequence = sequence;
                _pponProv.AddComboDeal(deal);
                sequence++;
            }
        }

        /// <summary>
        /// 檢查多檔次主檔賣價使用子檔最低均價設定
        /// </summary>
        /// <param name="entity">PponDeal主體</param>
        public static void CheckMainComboDealAveragePrice(PponDeal entity)
        {
            if ((entity.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0 ||
                (entity.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
            {
                var _combodeals = _pponProv.GetViewComboDealAllByBid(entity.Deal.Guid);
                ViewComboDealCollection combodeals = new ViewComboDealCollection();
                foreach (var _combodeal in _combodeals)
                {
                    //計算均價時，排除已結檔的子檔
                    DealProperty dp = _pponProv.DealPropertyGet(_combodeal.BusinessHourGuid);
                    if (dp.IsExpiredDeal)
                    {
                        //提案單刪除的方案才要排掉(因為前台不顯示刪除的子檔)，後台手動結檔的不動(前台仍會顯示該子檔)
                        GroupOrder go = _orderProv.GroupOrderGetByBid(_combodeal.BusinessHourGuid);
                        if (go.Slug != null)
                        {
                            continue;
                        }
                    }

                    combodeals.Add(_combodeal);
                }

                PponDeal comboMain = null;
                Guid comboMainBid =
                    combodeals.Where(t => t.MainBusinessHourGuid == t.BusinessHourGuid)
                              .Select(t => t.BusinessHourGuid)
                              .FirstOrDefault();

                if (comboMainBid != Guid.Empty)
                {
                    comboMain = _pponProv.PponDealGet(comboMainBid);
                }

                var limitAverage = default(decimal);
                if (comboMain != null)
                {
                    foreach (ViewComboDeal combodeal in combodeals)
                    {
                        decimal averagePrice = default(decimal);
                        if (combodeal.MainBusinessHourGuid == combodeal.BusinessHourGuid)
                        {
                            continue;
                        }
                        if(combodeal.ItemPrice == 0)
                        {
                            continue;
                        }
                        if (combodeal.IsAveragePrice)
                        {
                            averagePrice = (combodeal.QuantityMultiplier.HasValue) ? Math.Ceiling(combodeal.ItemPrice / combodeal.QuantityMultiplier.Value) : 0;
                        }
                        else
                        {
                            averagePrice = combodeal.ItemPrice;
                        }

                        if(limitAverage == 0 || limitAverage > averagePrice)
                        {
                            if(averagePrice != 0)
                            {
                                limitAverage = averagePrice;
                            }
                        }
                    }

                    //var averagePriceList = from d in combodeals
                    //                       where d.MainBusinessHourGuid != d.BusinessHourGuid && d.IsAveragePrice && (d.QuantityMultiplier.HasValue && d.QuantityMultiplier.Value >= 1)
                    //                             && d.ItemPrice > 0
                    //                       select new { AveragePrice = (d.QuantityMultiplier.HasValue) ? Math.Ceiling(d.ItemPrice / d.QuantityMultiplier.Value) : 0 };


                    //if (averagePriceList.Any(x => x.AveragePrice > 0))
                    //{
                    //    limitAverage = averagePriceList.Where(x => x.AveragePrice > 0).OrderBy(x => x.AveragePrice).First().AveragePrice;
                    //}

                    if (limitAverage > default(decimal) && comboMain.ItemDetail.ItemPrice != limitAverage)
                    {
                        comboMain.ItemDetail.ItemPrice = limitAverage;
                        _pponProv.PponDealSet(comboMain, false);

                    }
                }
            }
        }

        public static decimal CheckZeroPriceToShowPrice(IViewPponDeal deal, int cityId)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                if ((deal.BusinessHourStatus & (int)BusinessHourStatus.PriceZeorShowOriginalPrice) > 0)
                {
                    return deal.ItemOrigPrice;
                }
                else if (cityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && deal.ExchangePrice.GetValueOrDefault() > 0)
                {
                    return deal.ExchangePrice.Value;
                }
                else if (deal.CategoryIds.Contains(PponCityGroup.DefaultPponCityGroup.Family.CategoryId) && deal.ExchangePrice.GetValueOrDefault() > 0)
                {
                    return deal.ExchangePrice.Value;
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
            else
            {
                return deal.ItemPrice;
            }
        }

        public static decimal CheckZeroPriceToShowPrice(IViewPponDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                if ((deal.BusinessHourStatus & (int)BusinessHourStatus.PriceZeorShowOriginalPrice) > 0)
                {
                    return deal.ItemOrigPrice;
                }
                else if (deal.ExchangePrice.HasValue && deal.ExchangePrice.Value > default(decimal))
                {
                    return deal.ExchangePrice.Value;
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
            else
            {
                return deal.ItemPrice;
            }
        }

        #region DealCoupon相關
        /// <summary>
        /// 依據檔次狀態，回傳前台顯示用的憑證狀態
        /// </summary>
        /// <param name="deal"></param>
        public static DealCouponType GetDealCouponType(ViewPponDeal deal)
        {
            if (deal.DeliveryType == null)
            {
                throw new Exception(string.Format("LunchKingSite.BizLogic.Facade.PponFacade.GetDealCouponType():檔次之ViewPponDeal.DeliveryType為null，檔次BID:{0}", deal.BusinessHourGuid));
            }
            if (deal.CouponCodeType == null)
            {
                throw new Exception(string.Format("LunchKingSite.BizLogic.Facade.PponFacade.GetDealCouponType():檔次之ViewPponDeal.CouponCodeType為null，檔次BID:{0}", deal.BusinessHourGuid));
            }

            bool isFamiSingleBarcode = false;
            FamilyNetEvent ed = fami.GetFamilyNetEvent(deal.BusinessHourGuid);
            if (ed != null && ed.Version == (int)FamilyBarcodeVersion.FamiSingleBarcode)
            {
                isFamiSingleBarcode = true; //一段
            }

            return GetDealCouponType((DeliveryType)deal.DeliveryType,
                deal.GroupOrderStatus ?? 0,
                (CouponCodeType)deal.CouponCodeType,
                0,
                false,
                isFamiSingleBarcode);
        }

        /// <summary>
        /// 傳入ViewCouponListMain物件，回傳前台顯示用的憑證狀態
        /// </summary>
        /// <param name="couponListMain"></param>
        /// <returns></returns>
        public static DealCouponType GetDealCouponType(ViewCouponListMain couponListMain)
        {
            if (couponListMain.DeliveryType == null)
            {
                throw new Exception(string.Format("LunchKingSite.BizLogic.Facade.PponFacade.GetDealCouponType():檔次之ViewCouponListMain.DeliveryType為null，檔次BID:{0}", couponListMain.BusinessHourGuid));
            }

            bool isFamiSingleBarcode = false;
            FamilyNetEvent ed = fami.GetFamilyNetEvent(couponListMain.BusinessHourGuid);
            if (ed != null && ed.Version == (int)FamilyBarcodeVersion.FamiSingleBarcode)
            {
                isFamiSingleBarcode = true; //一段
            }

            return GetDealCouponType(
                (DeliveryType)couponListMain.DeliveryType,
                couponListMain.Status ?? 0,
                (CouponCodeType)couponListMain.CouponCodeType,
                couponListMain.BusinessHourStatus,
                couponListMain.IsDepositCoffee,
                isFamiSingleBarcode);
        }

        /// <summary>
        /// 傳入檔次屬性回傳前台顯示用的憑證類型
        /// </summary>
        /// <param name="deliveryType"></param>
        /// <param name="groupOrderStatus"></param>
        /// <param name="couponCodeType"></param>
        /// <param name="businessHourStatus"></param>
        /// <param name="isDepositCoffee"></param>
        /// <returns></returns>
        public static DealCouponType GetDealCouponType(DeliveryType deliveryType, int groupOrderStatus, CouponCodeType couponCodeType,
            int businessHourStatus = 0, bool isDepositCoffee = false, bool isFamiSingleBarcode = false)
        {
            //檢查是否為宅配檔次
            if (deliveryType == DeliveryType.ToHouse)
            {
                return DealCouponType.NotCounpnDeal;
            }

            //檢查是否為全家檔次
            if ((groupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)
            {
                if (businessHourStatus > 0 && Helper.IsFlagSet(businessHourStatus, BusinessHourStatus.GroupCoupon) && Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.PEZevent))
                {
                    if (isFamiSingleBarcode)
                    {
                        return DealCouponType.FamiSingleBarcode; //一段碼
                    }
                    else
                    {
                        return DealCouponType.FamilyNetPincode;
                    }
                }
                //全家檔次，檢查憑證編碼類型
                if (couponCodeType == CouponCodeType.Pincode)
                {
                    //判斷是否以三段式條碼取代pincode的顯示
                    if (config.EnableFami3Barcode)
                    {
                        return DealCouponType.ThreeStageBarcode;
                    }
                    else
                    {
                        return DealCouponType.FamiPinCode;
                    }

                }
                else if (couponCodeType == CouponCodeType.BarcodeEAN13)
                {
                    return DealCouponType.FamiBarCode;
                }
            }

            if ((groupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0)
            {
                return DealCouponType.SkmQrCode;
            }

            //檢查是否為萊爾富成套
            if (Helper.IsFlagSet(groupOrderStatus, (int)GroupOrderStatus.PEZevent) &&
                Helper.IsFlagSet(businessHourStatus, (int)BusinessHourStatus.GroupCoupon) &&
                Helper.IsFlagSet(groupOrderStatus, (int)GroupOrderStatus.HiLifeDeal) &&
                isDepositCoffee && config.EnableHiLifeDealSetup)
            {
                return DealCouponType.HiLifePincode;
            }

            //檢查是否為丹堤成套
            if (Helper.IsFlagSet(groupOrderStatus, (int)GroupOrderStatus.PEZevent) &&
                Helper.IsFlagSet(businessHourStatus, (int)BusinessHourStatus.GroupCoupon) &&
                !Helper.IsFlagSet(groupOrderStatus, (int)GroupOrderStatus.FamiDeal) &&
                isDepositCoffee)
            {
                return DealCouponType.ManyToManyQrCode;
            }

            //檢查是否為廠商提供序號檔次
            if (Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.PEZevent))
            {
                //廠商提供序號，且不為全家檔次，直接回SellerProvided
                return DealCouponType.SellerProvided;
            }

            //沒有特殊設定的好康檔次，一律產出 17life的憑證
            return DealCouponType.ContactCoupon;
        }

        public static bool IsFamilyNetPincode(int businessHourStatus, int groupOrderStatus)
        {
            return Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.FamiDeal) &&
                    Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.PEZevent) &&
                        Helper.IsFlagSet(businessHourStatus, BusinessHourStatus.GroupCoupon);
        }

        #region 判斷商品是否過期
        public static bool IsPastFinalExpireDate(ViewCouponListSequence view)
        {
            ExpirationDateSelector selector = GetExpirationComponent(view.Guid);

            if (selector == null)
            {
                return false;
            }

            return selector.IsPastExpirationDate(DateTime.Now);
        }

        public static bool IsPastFinalExpireDate(Guid orderGuid)
        {
            ExpirationDateSelector selector = GetExpirationComponent(orderGuid);

            if (selector == null)
            {
                return false;
            }

            return selector.IsPastExpirationDate(DateTime.Now);
        }

        public static ExpirationDateSelector GetExpirationComponent(Guid orderGuid)
        {
            Dictionary<Guid, ExpirationDateSelector> expirationComponents = new Dictionary<Guid, ExpirationDateSelector>();
            if (expirationComponents.ContainsKey(orderGuid))
            {
                return expirationComponents[orderGuid];
            }

            ExpirationDateSelector selector = new ExpirationDateSelector();
            selector.ExpirationDates = new PponUsageExpiration(orderGuid);

            expirationComponents.Add(orderGuid, selector);
            return selector;
        }
        #endregion 判斷商品是否過期
        #endregion DealCoupon相關


        #region PChome


        /// <summary>
        /// 查詢P好康以及品生活檔次資料，並且轉成PChome需要的XMLDocument
        /// </summary>
        /// <param name="requestTime">查詢日期</param>
        /// <param name="cpaCode">CPA</param>
        /// <param name="isPreview">是否為預覽(非預覽模式會更新request_time)</param>
        /// <returns></returns>
        public static XmlDocument PChomePponDealGetByDate(DateTime requestTime, string cpaCode, bool isPreview)
        {
            var cpa = "rsrc=PCH_find&utm_source=PChome&utm_medium=AD&utm_campaign=PCH_find";
            var xml = new XmlDocument();
            //建立根節點
            var search = xml.CreateElement("search");
            xml.AppendChild(search);
            //設定XML標頭
            var xmldecl = xml.CreateXmlDeclaration("1.0", null, null);
            xmldecl.Encoding = "UTF-8";
            xml.InsertBefore(xmldecl, search);

            #region 查詢所有資料組成 item

            var itemList = _pponProv.ViewPponDealGetListByDayWithNoLock(DateTime.Today, DateTime.Today.AddHours(24), PponDealSpecialCityType.WithOutSpecialCity, true);

            foreach (var tmp in itemList)
            {
                var item = xml.CreateElement("item");
                var Allcategory = _pponProv.ViewCategoryDealBaseGetListByBid(tmp.BusinessHourGuid);
                var DealCategory = Allcategory.Where(x => x.Type == 6).ToList();
                var PponChannel = Allcategory.Where(x => x.Type == 4).ToList();

                PChomePponDeal ypd = new PChomePponDeal();
                DateTime createTime = DateTime.Now;

                if (tmp.CreateTime.HasValue)
                {
                    createTime = tmp.CreateTime.Value;
                }
                if (DealCategory.Count > 0)
                {
                    foreach (var Category in DealCategory)
                    {
                        ypd.Tag += Category.Name + " ";
                    }
                }
                if (PponChannel.Count > 0)
                {
                    ypd.Channel = PponChannel.First().Name;
                }
                ypd.Pk = tmp.BusinessHourGuid.ToString();
                ypd.Title = tmp.CouponUsage;
                ypd.Desc = tmp.EventTitle;
                ypd.UrlDisp = "www.17Life.com";
                ypd.UrlLink = config.SiteUrl + "/" + tmp.BusinessHourGuid + "?" + cpa;
                ypd.ImgLink = ImageFacade.GetMediaPathsFromRawData(tmp.EventImagePath, MediaType.PponDealPhoto)
                        .DefaultIfEmpty(config.SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First();
                ypd.Update = tmp.EventModifyTime.ToString("yyyy-MM-dd HH:mm:ss");
                ypd.Create = createTime.ToString("yyyy-MM-dd HH:mm:ss");
                ypd.VaA = tmp.ItemOrigPrice.ToString("F0");
                ypd.VaB = tmp.ItemPrice.ToString("F0");

                item = Helper.ObjectToXmlElement(ypd, item, xml);
                search.AppendChild(item);
            }

            #endregion

            return xml;
        }


        #endregion




        #region Yahoo

        /// <summary>
        /// 更新不顯示折後價於前台選項
        /// </summary>
        /// <param name="isDealDiscountPriceBlacklist"></param>
        /// <param name="dealDiscountPriceBlacklists"></param>
        public static void UpdateDealDiscountPriceBlacklist(bool isDealDiscountPriceBlacklist, DealDiscountPriceBlacklist dealDiscountPriceBlacklists)
        {
            if (isDealDiscountPriceBlacklist)
            {
                _pep.InsertDealDiscountPriceBlacklist(dealDiscountPriceBlacklists);
            }
            else
            {
                _pep.DeleteDealDiscountPriceBlacklist(dealDiscountPriceBlacklists.Bid);
            }
        }

        /// <summary>
        /// Yahoo大團購資料確認更新
        /// </summary>
        /// <param name="user_name"></param>
        /// <returns></returns>
        public static void YahooPropertyActionUpdateAll(string user_name)
        {
            _pponProv.YahooPropertyActionUpdateAll(user_name, DateTime.Now);
        }
        #endregion

        #region Web Service Coupon
        /// <summary>
        /// 依DNS Server Name回傳對應的網路芳鄰帳密和路徑
        /// </summary>
        /// <param name="dns_name"></param>
        /// <returns></returns>
        public static bool ImpersonateAccountGet(string dns_name, out ImpersonateAccount account)
        {
            account = new ImpersonateAccount();
            SystemCodeCollection system_codes = _systemProv.SystemCodeGetListByCodeGroup(SingleOperator.ImageUNC.ToString());
            if (system_codes.Any(x => x.ShortName.Equals(dns_name, StringComparison.CurrentCultureIgnoreCase)))
            {
                SystemCode unc_info = system_codes.First(x => x.ShortName.Equals(dns_name, StringComparison.CurrentCultureIgnoreCase));
                SystemCode account_info = _systemProv.SystemCodeGetByCodeGroupId(SingleOperator.ImpersonateAccount.ToString(), 1);
                account = new ImpersonateAccount(account_info.CodeName, account_info.ShortName, unc_info.CodeName);
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion Web Service Coupon

        #region DealIcon

        public static string GetDealIconHtmlContent(IViewPponDeal vpd, int dealIconsLimit, string wrapTag = "span")
        {
            StringBuilder sb = new StringBuilder();
            //目前Icon計數
            int dealIconCount = 0;

            List<int> iconCodeIdList = new List<int>();
            if (!string.IsNullOrEmpty(vpd.LabelIconList))
            {
                iconCodeIdList = vpd.LabelIconList.Split(',').Select(int.Parse).ToList();
                iconCodeIdList.Sort();
            }

            //優先出現1-24小時到貨、24小時出貨
            //改用旗幟顯示在檔次左上
            /*
            if (iconCodeIdList.Contains((int)DealLabelSystemCode.TwentyFourHoursArrival))
            {
                sb.AppendFormat("<{0} class='dts-label dts-lb-4'>24H</{0}>", wrapTag);
                iconCodeIdList.Remove((int)DealLabelSystemCode.TwentyFourHoursArrival);
                dealIconCount++;
                if (dealIconCount >= dealIconsLimit)
                {
                    return sb.ToString();
                }
            }
            */


            //優先出現2-折價券
            if (vpd.DiscountUseType != null && vpd.DiscountUseType.Value)
            {
                sb.AppendFormat("<{0} class='dts-label dts-lb-11'>折價</{0}>", wrapTag);
                dealIconCount++;
                if (dealIconCount >= dealIconsLimit)
                {
                    return sb.ToString();
                }
            }

            //優先出現3-熱銷 (排除公益檔)
            if (!Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
            {
                QuantityAdjustment adj = vpd.GetAdjustedOrderedQuantity();

                //熱銷 Icon
                if (adj.Quantity >= 1000)
                {
                    sb.AppendFormat("<{0} class='dts-label dts-lb-1'>熱銷</{0}>", wrapTag);
                    dealIconCount++;
                    if (dealIconCount >= dealIconsLimit)
                    {
                        return sb.ToString();
                    }
                }
            }

            //優先出現4-超商取貨
            if (vpd.EnableIsp)
            {
                sb.AppendFormat("<{0} class='dts-label dts-lb-12'>超取</{0}>", wrapTag);
                dealIconCount++;
                if (dealIconCount >= dealIconsLimit)
                {
                    return sb.ToString();
                }
            }

            foreach (var iconCodeId in iconCodeIdList)
            {
                switch ((DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), iconCodeId))
                {
                    case DealLabelSystemCode.CanBeUsedImmediately:
                        sb.AppendFormat("<{0} class='dts-label dts-lb-3'>即用</{0}>", wrapTag);
                        break;
                    case DealLabelSystemCode.FiveStarHotel:
                        break;
                    case DealLabelSystemCode.FourStarHotel:
                        break;
                    case DealLabelSystemCode.AppLimitedEdition:
                        sb.AppendFormat("<{0} class='dts-label dts-lb-6'>APP限定</{0}>", wrapTag);
                        break;
                    case DealLabelSystemCode.PiinlifeEvent:
                        sb.AppendFormat("<{0} class='dts-label dts-lb-5'>五星美饌</{0}>", wrapTag);
                        break;
                    case DealLabelSystemCode.Visa:
                        sb.AppendFormat("<{0} class='dts-label dts-lb-5'>VISA</{0}>", wrapTag);
                        break;
                    case DealLabelSystemCode.PiinlifeFatherEvent:
                        sb.AppendFormat("<{0} class='dts-label dts-lb-5'>父親節精選</{0}>", wrapTag);
                        break;
                    case DealLabelSystemCode.PiinlifeMoonEvent:
                        sb.AppendFormat("<{0} class='dts-label dts-lb-5'>賞悅中秋</{0}>", wrapTag);
                        break;
                    case DealLabelSystemCode.PiinlifeValentinesEvent:
                        sb.AppendFormat("<{0} class='dts-label dts-lb-5'>情人專屬</{0}>", wrapTag);
                        break;
                    case DealLabelSystemCode.PiinlifeMajiEvent:
                        sb.AppendFormat("<{0} class='dts-label dts-lb-5'>花博MAJI廣場</{0}>", wrapTag);
                        break;
                    case DealLabelSystemCode.PiinlifeRecommend:
                        sb.AppendFormat("<{0} class='dts-label dts-lb-5'>每月推薦</{0}>", wrapTag);
                        break;
                    case DealLabelSystemCode.GroupCoupon:
                        sb.AppendFormat("<{0} class='dts-label dts-lb-5'>套票</{0}>", wrapTag);
                        break;
                }
                dealIconCount++;
                if (dealIconCount >= dealIconsLimit)
                {
                    return sb.ToString();
                }
            }
            return sb.ToString();
            //return dealIconHtmlContent;
        }

        public static string GetDealIconHtmlContentList(IViewPponDeal vpd, int dealIconsLimit)
        {
            return GetDealIconHtmlContent(vpd, dealIconsLimit, "li");
        }

        public static string GetPiinlifeDealIconHtmlContent(IViewPponDeal vpd, int dealIconsLimit)
        {
            string dealIconHtmlContent = string.Empty;
            //目前Icon計數
            int dealIconCount = 0;

            if (!string.IsNullOrEmpty(vpd.LabelIconList))
            {
                List<int> iconCodeIdList = vpd.LabelIconList.Split(',').Select(int.Parse).ToList();
                foreach (var iconCodeId in iconCodeIdList)
                {
                    switch ((DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), iconCodeId))
                    {
                        case DealLabelSystemCode.CanBeUsedImmediately:
                            dealIconHtmlContent += "<p class='use_sp'></p>";
                            dealIconCount++;
                            break;
                        case DealLabelSystemCode.PiinlifeEvent:
                            dealIconHtmlContent += "<p class='event_sp'></p>";
                            dealIconCount++;
                            break;
                        case DealLabelSystemCode.PiinlifeMoonEvent:
                            dealIconHtmlContent += "<p class='moon_sp'><svg><image xlink:href='" + config.SiteUrl + "/Themes/HighDeal/images/index/moon_sp.svg' src='" + config.SiteUrl + "/Themes/HighDeal/images/index/moon_sp.png' width='80' height='22' /></svg></p>";
                            dealIconCount++;
                            break;
                        case DealLabelSystemCode.PiinlifeValentinesEvent:
                            dealIconHtmlContent += "<p class='valentines_sp'><svg><image xlink:href='" + config.SiteUrl + "/Themes/HighDeal/images/index/valentines_sp.svg' src='" + config.SiteUrl + "/Themes/HighDeal/images/index/valentines_sp.jpg' width='80' height='22' /></svg></p>";
                            dealIconCount++;
                            break;
                        case DealLabelSystemCode.PiinlifeFatherEvent:
                            dealIconHtmlContent += "<p class='father_sp'><svg><image xlink:href='" + config.SiteUrl + "/Themes/HighDeal/images/index/father_sp.svg' src='" + config.SiteUrl + "/Themes/HighDeal/images/index/father_sp.png' width='80' height='22' /></svg></p>";
                            dealIconCount++;
                            break;
                        case DealLabelSystemCode.PiinlifeMajiEvent:
                            dealIconHtmlContent += "<p class='maji_sp'><svg><image xlink:href='" + config.SiteUrl + "/Themes/HighDeal/images/index/maji_sp.svg' src='" + config.SiteUrl + "/Themes/HighDeal/images/index/maji_sp.png' width='80' height='22' /></svg></p>";
                            dealIconCount++;
                            break;
                        case DealLabelSystemCode.PiinlifeRecommend:
                            dealIconHtmlContent += "<p class='moon_sp'><svg><image xlink:href='" + config.SiteUrl + "/Themes/HighDeal/images/index/month_sp.svg' src='" + config.SiteUrl + "/Themes/HighDeal/images/index/month_sp.png' width='80' height='22' /></svg></p>";
                            dealIconCount++;
                            break;
                        case DealLabelSystemCode.Visa:
                            if (vpd.IsVisaNow)
                            {
                                dealIconHtmlContent += "<p class='VISA_sp'></p>";
                                dealIconCount++;
                            }
                            break;
                        case DealLabelSystemCode.GroupCoupon:
                            dealIconHtmlContent += "<p class='certificate_sp'></p>";
                            dealIconCount++;
                            break;
                        default:
                            break;
                    }
                    if (dealIconCount >= dealIconsLimit)
                    {
                        break;
                    }
                }
            }

            return dealIconHtmlContent;
        }

        /// <summary>
        /// 產生 M版列表頁 標籤的html
        /// </summary>
        /// <param name="vpd"></param>
        /// <param name="dealIconsLimit">最多回傳幾筆，預防標籤太多畫面跑版</param>
        /// <returns></returns>
        public static string RenderDealTagsAsMobileHtml(IViewPponDeal vpd, int dealIconsLimit)
        {
            List<DealTag> tags = GetDealTagsAsMobileHtml(vpd, dealIconsLimit);
            return DealTagHelper.RenderDealTagsAsMobileHtml(tags);
        }

        public static List<DealTag> GetDealTagsAsMobileHtml(IViewPponDeal vpd, int dealIconsLimit)
        {
            List<DealTag> dealTags = DealTagHelper.GetDealTags(vpd, dealIconsLimit);
            List<DealTag> tags = dealTags.Count > dealIconsLimit ? dealTags.Take(dealIconsLimit).ToList() : dealTags;
            return tags;
        }

        public static bool DealHasLastDayTag(IViewPponDeal vpd)
        {
            return DealTagHelper.HasLastDayTag(vpd);
        }

        #endregion

        #region SideDeal

        public static List<IViewPponDeal> GetSideDeals(List<Guid> bids, int workCityId)
        {
            var hotsaleDeals = ViewPponDealManager.DefaultManager.HotSaleMultipleMainDealPreviewGetListByCityId(workCityId, null);
            List<IViewPponDeal> sideDeals = (hotsaleDeals.Select(x => x.PponDeal))
                .Where(x => !Helper.IsFlagSet(x.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal)).ToList();
            //sideDeals = sideDeals.Where(x => !x.BusinessHourGuid.EqualsAny(bids.ToArray())).ToList();

            return sideDeals;
        }

        public static List<SideDeal> GetSideDeals(List<Guid> bids, int workCityId, int takeCount)
        {
            List<SideDeal> sideDeals = new List<SideDeal>();
            if (bids == null)
            {
                bids = new List<Guid>();
            }

            var hotsaledeals = GetSideDeals(bids, workCityId).Take(20);

            List<IViewPponDeal> vpds = hotsaledeals.OrderBy(x => new Guid()).Take(takeCount).ToList();
            foreach (ViewPponDeal vpd in vpds)
            {
                SideDeal sd = new SideDeal();
                sd.SiteUrl = config.SiteUrl;
                sd.CityID = workCityId;
                sd.BusinessHourGuid = vpd.BusinessHourGuid;
                sd.EventName = vpd.EventName.TrimToMaxLength(53, "...");
                sd.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).Skip(1).DefaultIfEmpty(config.SiteUrl + "/Themes/default/images/17Life/EDM/EDMDefault.jpg").First();
                sd.ItemName = vpd.ItemName.TrimToMaxLength(53, "...");
                if (!string.IsNullOrEmpty(vpd.DealPromoImage))
                {
                    sd.PromoPic = string.Format(@"<img src='{0}' alt='' />"
                    , ImageFacade.GetMediaPath(vpd.DealPromoImage, MediaType.DealPromoImage));
                }
                sd.IsMultipleStores = vpd.IsMultipleStores;
                sd.HoverMessage = vpd.HoverMessage;
                sd.SaleQuantity = OrderedQuantityHelper.Show(vpd, OrderedQuantityHelper.ShowType.MStyleDeal);
                sd.IsSoldOut = vpd.OrderedQuantity >= vpd.OrderTotalLimit;
                if (vpd.OrderedQuantity >= vpd.OrderTotalLimit)
                {
                    sd.OverBadge = "<div class=\"SoldOut_Bar_220\"></div>";
                }
                else if (OrderedQuantityHelper.OverThousand(vpd))
                {
                    sd.OverBadge = "<div class=\"Over1K_SS_Badge\"></div>";
                }

                if (vpd.ItemOrigPrice == 0)
                {
                    sd.SaleMessage = "...";
                }
                else
                {
                    if (vpd.ItemPrice > 0)
                    {
                        sd.SaleMessage = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice) + "折";
                    }
                    else
                    {
                        sd.SaleMessage = "...";
                    }
                }
                sd.ItemPrice = CheckZeroPriceToShowPrice(vpd, workCityId).ToString("F0");
                sideDeals.Add(sd);
            }
            return sideDeals;
        }

        public static string GetSideDealJson(List<Guid> bids, int workCityId, int takeCount)
        {
            var vpds = GetSideDeals(bids, workCityId).Take(takeCount).ToList();
            List<SideDeal> sideDeals = new List<SideDeal>();

            foreach (ViewPponDeal vpd in vpds)
            {
                SideDeal sd = new SideDeal();
                sd.CityID = workCityId;
                sd.BusinessHourGuid = vpd.BusinessHourGuid;
                sd.EventName = vpd.EventName.TrimToMaxLength(53, "...");
                sd.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).Skip(1).DefaultIfEmpty(config.SiteUrl + "/Themes/default/images/17Life/EDM/EDMDefault.jpg").First();

                if (vpd.OrderedQuantity >= vpd.OrderTotalLimit)
                {
                    sd.OverBadge = "<div class=\"SoldOut_Bar_220\"></div>";
                }
                else if (OrderedQuantityHelper.OverThousand(vpd))
                {
                    sd.OverBadge = "<div class=\"Over1K_SS_Badge\"></div>";
                }

                if (vpd.ItemOrigPrice == 0)
                {
                    sd.SaleMessage = "...";
                }
                else
                {
                    if (vpd.ItemPrice > 0)
                    {
                        sd.SaleMessage = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice) + "折";
                    }
                    else
                    {
                        sd.SaleMessage = "...";
                    }
                }
                sd.ItemPrice = string.Format("${0}", CheckZeroPriceToShowPrice(vpd, workCityId).ToString("F0"));
                sideDeals.Add(sd);
            }

            return new LunchKingSite.Core.JsonSerializer().Serialize(new { SideDeals = sideDeals });
        }

        #endregion

        #region MultipleMainDealPreview

        public static List<MultipleMainDealPreview> GetMultipleMainDealPreviewList(int _categoryId, int workCityId, int travelCategoryId, int femaleCategoryId, List<int> filterCategoryIdList)
        {
            int? selectedCategoryId = null;
            // 使用多主檔排列
            if (_categoryId > 0)
            {
                selectedCategoryId = _categoryId;
            }


            List<MultipleMainDealPreview> multideals;

            //取得目前城市
            PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(workCityId);
            if (city == null)
            {
                city = PponCityGroup.DefaultPponCityGroup.TaipeiCity;
            }
            //目前城市沒有對應的頻道，目前應該只有24或72小時會這樣，直接設定為台北
            if (!city.ChannelId.HasValue)
            {
                city = PponCityGroup.DefaultPponCityGroup.TaipeiCity;
            }
            int? areaCategoryId = null;
            //判斷是否為旅遊頻道
            if (workCityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                //int? travelCategoryId = null;
                if (travelCategoryId != CategoryManager.GetDefaultCategoryNode().CategoryId)
                {
                    //針對舊的TravelCategoryId區域，因紀錄於cookie所以須進行處理，避免查無資料
                    areaCategoryId = travelCategoryId;
                }
                //旅遊頻道的話，區域CategoryId 由 View.TravelCategoryId指定
                multideals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(city.ChannelId.Value, areaCategoryId,
                                                                 selectedCategoryId);
            }
            else if (workCityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                if (femaleCategoryId != CategoryManager.GetDefaultCategoryNode().CategoryId)
                {
                    areaCategoryId = femaleCategoryId;
                }
                multideals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(city.ChannelId.Value, areaCategoryId,
                                                                 selectedCategoryId);
            }
            else
            {
                areaCategoryId = city.ChannelAreaId;
                multideals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(city.ChannelId.Value, areaCategoryId,
                                                                 selectedCategoryId);
            }
            if (filterCategoryIdList.Count > 0)
            {
                multideals = multideals.Where(x => filterCategoryIdList.Any(y => x.DealCategoryIdList.Contains(y))).ToList();
            }
            return multideals;
        }

        public static void SaveCategoryDeals(CategoryDealCollection category, Guid bid)
        {
            var dp = _pponProv.DealPropertyGet(bid);
            if (dp.IsLoaded)
            {
                dp.CategoryList = new JsonSerializer().Serialize(category.Select(t => t.Cid).Distinct().ToList());
                _pponProv.DealPropertySet(dp);
            }
            _pponProv.CategoryDealSave(category, bid);
        }

        public static List<RelatedDeal> GetRelatedDeals(Guid mainBid)
        {
            int workCityId = 0, categoryId = 0;
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(mainBid);
            Guid _mainbid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(mainBid);
            string mainCity = string.Empty;
            if (vpd.IsLoaded)
            {
                mainCity = (string.IsNullOrEmpty(vpd.CityList)) ? string.Empty : vpd.CityList.Replace("[", "").Replace("]", "");
            }
            if (!string.IsNullOrEmpty(mainCity))
            {
                List<string> mainCityList = mainCity.Split(',').ToList();
                PponCity pc = PponCityGroup.DefaultPponCityGroup.GetPponCityForDealChannel(mainCityList, workCityId);
                workCityId = pc.CityId;
                categoryId = pc.CategoryId;
            }

            List<MultipleMainDealPreview> multiDeals = GetMultipleMainDealPreviewList(categoryId, workCityId, 0, 0, new List<int>());
            var mainDeal = multiDeals.FirstOrDefault(x => x.PponDeal.BusinessHourGuid == _mainbid);

            return GetRelatedDeals(mainDeal, multiDeals, workCityId)
                        .OrderByDescending(x => x.Priority)
                        .Take(5).ToList();

        }

        public static string GetRelatedDealsParseJson(Guid mainBid, int _categoryId, int workCityId, int travelCategoryId, int femaleCategoryId, List<int> filterCategoryIdList)
        {
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(mainBid);
            Guid _mainbid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(mainBid);
            string mainCity = string.Empty;
            if (vpd.IsLoaded)
            {
                mainCity = (string.IsNullOrEmpty(vpd.CityList)) ? string.Empty : vpd.CityList.Replace("[", "").Replace("]", "");
            }
            if (!string.IsNullOrEmpty(mainCity))
            {
                List<string> mainCityList = mainCity.Split(',').ToList();
                PponCity pc = PponCityGroup.DefaultPponCityGroup.GetPponCityForDealChannel(mainCityList, workCityId);
                workCityId = pc.CityId;
                _categoryId = pc.CategoryId;
            }

            List<MultipleMainDealPreview> multiDeals = GetMultipleMainDealPreviewList(_categoryId, workCityId, travelCategoryId, femaleCategoryId, filterCategoryIdList);
            MultipleMainDealPreview mainDeal = multiDeals.Where(x => x.PponDeal.BusinessHourGuid == _mainbid).FirstOrDefault();
            return new JsonSerializer().Serialize(new { RelatedDeals = GetRelatedDeals(mainDeal, multiDeals, workCityId).OrderByDescending(x => x.Priority).Take(config.RelatedDealCount) });

        }


        public static List<BuyRelatedDeal> GetRelatedDeals(Guid mainBid, int _categoryId, int workCityId)
        {
            PponCity pc = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(workCityId);
            List<int> PponChannelList = new List<int>();
            List<BuyRelatedDeal> BuyRelatedDeals = new List<BuyRelatedDeal>();
            var Allcategory = _pponProv.ViewCategoryDealBaseGetListByBid(mainBid);
            var DealCategory = Allcategory.Where(x => x.Type == 6).ToList();
            var PponChannel = Allcategory.Where(x => x.Type == 4).ToList();

            foreach (var deal in DealCategory)
            {
                List<int> DealCategoryList = new List<int>();
                DealCategoryList.Add(deal.Cid);
                GetRelatedDealItme(mainBid, pc, DealCategoryList, BuyRelatedDeals);
                if (BuyRelatedDeals.Count >= 6)
                {
                    return BuyRelatedDeals.ToList();
                }
            }

            if (BuyRelatedDeals.Count < 6)
            {
                if (PponChannel.Count() != 0)
                {
                    var viewCategoryDeal = PponChannel.FirstOrDefault();
                    if (viewCategoryDeal != null)
                    {
                        PponChannelList.Add(viewCategoryDeal.Cid);
                        GetRelatedDealItme(mainBid, pc, PponChannelList, BuyRelatedDeals);
                    }
                }
            }

            return BuyRelatedDeals.ToList();
        }

        public static void GetRelatedDealItme(Guid mainBid, PponCity City, List<int> Category, List<BuyRelatedDeal> BuyRelatedDeals)
        {
            var RelatedDeal = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(City, Category);
            Guid mainDealBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(mainBid);

            foreach (MultipleMainDealPreview deal in RelatedDeal)
            {
                BuyRelatedDeal buyRelatedDeal = new BuyRelatedDeal();
                if (deal.PponDeal.OrderedQuantity < deal.PponDeal.OrderTotalLimit)
                {
                    buyRelatedDeal.BusinessHourGuid = deal.PponDeal.BusinessHourGuid;
                    buyRelatedDeal.CouponUsage = deal.PponDeal.CouponUsage;
                    buyRelatedDeal.EventTitle = deal.PponDeal.EventTitle;
                    buyRelatedDeal.ItemPrice = deal.PponDeal.ItemPrice;
                    buyRelatedDeal.ItemOrigPrice = deal.PponDeal.ItemOrigPrice;
                    buyRelatedDeal.EventImagePath = deal.PponDeal.EventImagePath;
                    buyRelatedDeal.BusinessHourStatus = deal.PponDeal.BusinessHourStatus;

                    if (buyRelatedDeal.BusinessHourGuid != mainBid && mainDealBid != ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(buyRelatedDeal.BusinessHourGuid)
                        && BuyRelatedDeals.Count(x => x.BusinessHourGuid == buyRelatedDeal.BusinessHourGuid) == 0)
                    {
                        BuyRelatedDeals.Add(buyRelatedDeal);
                    }
                }

                if (BuyRelatedDeals.Count >= 6)
                {
                    break;
                }
            }
        }

        public static string GenBaseRelateDealsJson(List<IViewPponDeal> deals)
        {
            const string CPA_PROMO_DEALS = "cpa-17_payment";
            List<RelatedDeal> relatedDeals = new List<RelatedDeal>();
            foreach(IViewPponDeal deal in deals)
            {
                string tempSaleMessage = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), deal.ItemPrice, deal.ItemOrigPrice);

                RelatedDeal relatedDeal = new RelatedDeal();
                relatedDeal.CityID = deal.SellerCityId;
                relatedDeal.BusinessHourGuid = deal.BusinessHourGuid;
                relatedDeal.DealImageUrl = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(config.SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First();
                relatedDeal.DealPromoImage = (string.IsNullOrEmpty(deal.DealPromoImage)) ? string.Empty : string.Format(@"<img src='{0}' alt='' />"
                , ImageFacade.GetMediaPath(deal.DealPromoImage, MediaType.DealPromoImage));
                relatedDeal.IsSoldOut = (deal.OrderedQuantity >= deal.OrderTotalLimit) ? true : false;
                relatedDeal.DealTitle = (string.IsNullOrEmpty(deal.AppTitle)) ? deal.CouponUsage : deal.AppTitle;
                relatedDeal.SaleMessage = (tempSaleMessage != "") ? string.Format("{0}<span class='smalltext'>折</span>", tempSaleMessage) : "特選";
                relatedDeal.ItemPrice = string.Format("${0}{1}", CheckZeroPriceToShowPrice(deal, relatedDeal.CityID).ToString("F0"), ((deal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "<span class='smalltext'>起</span>" : string.Empty));
                relatedDeal.SellCount = OrderedQuantityHelper.Show(deal, OrderedQuantityHelper.ShowType.RelateDeal);
                relatedDeal.DealIcon = GetDealIconHtmlContent(deal, 2);
                relatedDeal.ItemOrigPrice = deal.ItemOrigPrice;
                relatedDeal.ItemPriceValue = deal.ItemPrice;
                relatedDeal.BusinessHourStatus = deal.BusinessHourStatus;
                relatedDeal.GroupOrderStatus = deal.GroupOrderStatus;
                //取得相關檔次與mainDeal間新舊分類與所有分類的交集數量
                //relatedDeal.Priority = (isMainDealNull) ? 0 : mainDealCategoryId.Intersect(deal.DealCategoryIdList).Count();
                relatedDeal.RecommendLink = VirtualPathUtility.ToAbsolute(string.Format("~/{0}/{1}", deal.BusinessHourGuid, CPA_PROMO_DEALS));
                relatedDeal.EventTitle = deal.EventTitle;
                relatedDeals.Add(relatedDeal);
            }
            return new JsonSerializer().Serialize(new { RelatedDeals = relatedDeals });
        }

        private static List<RelatedDeal> GetRelatedDeals(MultipleMainDealPreview mainDeal, List<MultipleMainDealPreview> multideals, int cityId)
        {
            bool isMainDealNull = (mainDeal == null) ? true : false;
            List<RelatedDeal> relatedDeals = new List<RelatedDeal>();
            List<int> mainDealCategoryId = new List<int>();

            // 結帳完成推薦檔次CPA(舊版使用)                   
            const string CPA_PROMO_DEALS = "cpa-17_payment";

            if (!isMainDealNull)
            {
                //取出mainDeal新類別 (type = 6)
                mainDealCategoryId = mainDeal.DealCategoryIdList.Intersect(CategoryManager.CategoryGetListByType(CategoryType.DealCategory).Select(x => x.Id).ToList()).ToList();
            }

            foreach (MultipleMainDealPreview deal in multideals)
            {
                string tempSaleMessage = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), deal.PponDeal.ItemPrice, deal.PponDeal.ItemOrigPrice);

                RelatedDeal relatedDeal = new RelatedDeal();
                relatedDeal.CityID = deal.CityID;
                relatedDeal.BusinessHourGuid = deal.PponDeal.BusinessHourGuid;
                relatedDeal.DealImageUrl = ImageFacade.GetMediaPathsFromRawData(deal.PponDeal.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(config.SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First();
                relatedDeal.DealPromoImage = (string.IsNullOrEmpty(deal.PponDeal.DealPromoImage)) ? string.Empty : string.Format(@"<img src='{0}' alt='' />"
                , ImageFacade.GetMediaPath(deal.PponDeal.DealPromoImage, MediaType.DealPromoImage));
                relatedDeal.IsSoldOut = (deal.PponDeal.OrderedQuantity >= deal.PponDeal.OrderTotalLimit) ? true : false;
                relatedDeal.DealTitle = (string.IsNullOrEmpty(deal.PponDeal.AppTitle)) ? deal.PponDeal.CouponUsage : deal.PponDeal.AppTitle;
                relatedDeal.SaleMessage = (tempSaleMessage != "") ? string.Format("{0}<span class='smalltext'>折</span>", tempSaleMessage) : "特選";
                relatedDeal.ItemPrice = string.Format("${0}{1}", CheckZeroPriceToShowPrice(deal.PponDeal, cityId).ToString("F0"), ((deal.PponDeal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "<span class='smalltext'>起</span>" : string.Empty));
                relatedDeal.SellCount = OrderedQuantityHelper.Show(deal.PponDeal, OrderedQuantityHelper.ShowType.RelateDeal);
                relatedDeal.DealIcon = GetDealIconHtmlContent(deal.PponDeal, 2);
                relatedDeal.ItemOrigPrice = deal.PponDeal.ItemOrigPrice;
                relatedDeal.ItemPriceValue = deal.PponDeal.ItemPrice;
                relatedDeal.BusinessHourStatus = deal.PponDeal.BusinessHourStatus;
                relatedDeal.GroupOrderStatus = deal.PponDeal.GroupOrderStatus;
                //取得相關檔次與mainDeal間新舊分類與所有分類的交集數量
                relatedDeal.Priority = (isMainDealNull) ? 0 : mainDealCategoryId.Intersect(deal.DealCategoryIdList).Count();
                relatedDeal.RecommendLink = VirtualPathUtility.ToAbsolute(string.Format("~/{0}/{1}", deal.PponDeal.BusinessHourGuid, CPA_PROMO_DEALS));
                relatedDeal.EventTitle = deal.PponDeal.EventTitle;
                relatedDeals.Add(relatedDeal);
            }

            if (!isMainDealNull)
            {
                relatedDeals.RemoveAll(x => x.BusinessHourGuid == mainDeal.PponDeal.BusinessHourGuid);
            }

            return relatedDeals;
        }

        /// <summary>
        /// 是否超過可購買上限(check order_detail)
        /// </summary>
        /// <param name="vpd"></param>
        /// <param name="qty"></param>
        /// <returns>true 超過可購買上限, 不能購買</returns>
        public static bool CheckExceedOrderedQuantityLimit(IViewPponDeal vpd, int qty)
        {
            if (ProposalFacade.IsVbsProposalNewVersion() && vpd.IsHouseDealNewVersion())
            {
                return CheckDeliveryToHouseQuantity(vpd, qty);
            }

            int orderedQuantity = _pponProv.OrderedQuantityGet(vpd.BusinessHourGuid) * (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? vpd.SaleMultipleBase ?? 1 : 1);
            qty = Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? vpd.SaleMultipleBase ?? 1 : qty;
            if (orderedQuantity + qty > (vpd.OrderTotalLimit ?? decimal.MaxValue))
            {
                return true;
            }
            return false;
        }

        public static bool CheckDeliveryToHouseQuantity(IViewPponDeal vpd, int qty)
        {
            List<Guid> bids = new List<Guid>();
            bids.Add(vpd.BusinessHourGuid);
            Proposal pro = _sellerProv.ProposalGetByBids(bids).FirstOrDefault();
            if (pro != null)
            {
                ProposalMultiDeal pmd = _sellerProv.ProposalMultiDealGetByPid(pro.Id).FirstOrDefault();
                if (pmd != null)
                {
                    ProposalMultiDealsSpec spec = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(pmd.Options).FirstOrDefault();
                    if (spec != null)
                    {
                        ProductItemCollection pdi = _pponProv.ProductItemGetList(spec.Items.Select(x => x.item_guid).ToList());
                        if (pdi.Count() > 0)
                        {
                            if (qty > pdi.Sum(x => x.Stock))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public static decimal GetNotMultiDealStock(IViewPponDeal vpd)
        {
            if (vpd.DeliveryType == (int)DeliveryType.ToHouse)
            {
                if (vpd.ShoppingCart == null || (bool)!vpd.ShoppingCart)
                {
                    ProposalMultiDeal pmd = _sellerProv.ProposalMultiDealGetByBid(vpd.BusinessHourGuid);
                    if (pmd.IsLoaded && !string.IsNullOrEmpty(pmd.Options))
                    {
                        ProposalMultiDealsSpec spec = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(pmd.Options).FirstOrDefault();
                        if (spec != null)
                        {
                            //宅配單一規格撈stock,其餘撈原本的OrderTotalLimit
                            ProductItemCollection pdi = _pponProv.ProductItemGetList(spec.Items.Select(x => x.item_guid).ToList());
                            return pdi.First().Stock;
                        }
                    }
                }
            }

            return vpd.OrderTotalLimit ?? 0;
        }

        /// <summary>
        /// 預防超賣的訂購數量檢查，零元檔在MakeOrder前檢查，非零元檔在付款前檢查
        /// </summary>
        /// <param name="vpd"></param>
        /// <param name="qty"></param>
        /// <param name="userId"></param>
        /// <param name="preventingOversellItem"></param>
        /// <returns>ture is pass, false is oversell.</returns>
        public static bool CheckPreventingOversell(IViewPponDeal vpd, int qty, int userId, out int preventId)
        {
            qty = Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon)
                ? vpd.SaleMultipleBase ?? 1
                : qty;
            bool result = false; //預設超賣
            preventId = 0;
            try
            {
                if (config.UsingPreventingOversellProcedure) 
                {
                    string msg;
                    var saleMultipleBase = Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon)
                                                  ? vpd.SaleMultipleBase ?? 1 : 1;
                    var sp = SPs.PreventingOversell(vpd.BusinessHourGuid, qty, userId, saleMultipleBase, config.PreventingOversellTimeoutSec, decimal.ToInt32(vpd.OrderTotalLimit ?? 0));
                    sp.Execute();
                    preventId = Convert.ToInt32(sp.OutputValues[0]);
                    result = Convert.ToInt32(sp.OutputValues[1]) == 1;
                    msg = sp.OutputValues[2].ToString();

                    if (!result) 
                    {
                        logger.InfoFormat("預防超賣阻擋:{0}", msg);
                    }
                } 
                else 
                {
                    using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                    {
                        Guid TTestingGuid = Guid.NewGuid();
                        //已成單訂購數量
                        int orderedQuantity = _pep.OrderedQuantityGet(vpd.BusinessHourGuid) *
                                              (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon)
                                                  ? vpd.SaleMultipleBase ?? 1
                                                  : 1);

                        //訂購中數量
                        int processingQty = _orderProv.GetPreventingOversellProcessingQty(vpd.BusinessHourGuid);

                        logger.Info("[" + TTestingGuid + "]TTesting AddPreventingOversell:bid=" + vpd.BusinessHourGuid);
                        var po = _orderProv.AddPreventingOversell(new PreventingOversell
                        {
                            Bid = vpd.BusinessHourGuid,
                            OrderQty = qty,
                            Processing = true,
                            UserId = userId
                        });
                        preventId = po.Id;

                        if ((vpd.OrderTotalLimit ?? decimal.MaxValue) >= (orderedQuantity + processingQty + qty))
                        {
                            result = true;
                            ts.Complete();
                            logger.Info("[" + TTestingGuid + "]TTesting ts.Complete():bid=" + vpd.BusinessHourGuid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SystemFacade.SetApiLog("PponFacade.CheckPreventingOversell", userId.ToString(), vpd, ex.ToString());
            }
            return result;
        }

        public static bool CheckPrizeOverdraw(PrizeDrawRecord record, PrizeDrawItem item, out ApiResultCode resultCode, out int preventingOverdrawId)
        {
            bool result = false; //預設超賣
            resultCode = ApiResultCode.OutofLimitOrderError;
            preventingOverdrawId = 0;
            try
            {
                var sp = SPs.PrizeOverdraw(item.Id, record.Qty, record.UserId, config.PreventingOversellTimeoutSec, item.Qty);
                sp.Execute();
                preventingOverdrawId = Convert.ToInt32(sp.OutputValues[0]);
                result = Convert.ToInt32(sp.OutputValues[1]) == 1;
                string msg = sp.OutputValues[2].ToString();

                if (!result)
                {
                    logger.InfoFormat("預防超賣阻擋:{0}", msg);
                }
                else
                {
                    resultCode = ApiResultCode.Ready;
                }
            }
            catch (Exception e)
            {
                logger.ErrorFormat("預防超賣 Exception:{0}, {1}", e.Message, e.StackTrace);
                resultCode = ApiResultCode.DrawEventError;
            }

            return result;

        }

        public static bool SetPrizeDrawRecord(bool isPrizeDeal, int drawRecordId, PrizeDrawRecordStatus status)
        {
            if (!isPrizeDeal)
            {
                return false;
            }

            var record = _cep.GetPrizeDrawRecordWithNoLock(drawRecordId);
            record.Status = (byte)status;
            record.ModifyTime = DateTime.Now;
            if (config.CheckPrizeOverdraw)
            {
                _cep.SetPreventingOversellExpired(record.ItemId, record.UserId, record.Qty);
            }
            return _cep.UpdatePrizeDrawRecord(record);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vpd"></param>
        /// <param name="selectedStore"></param>
        /// <param name="qty"></param>
        /// <returns>true 超過分店可購買上限, 不能購買</returns>
        public static bool CheckExceedStoreTotalQuantityLimit(IViewPponDeal vpd, Guid selectedStore, int qty)
        {
            var vps = _pponProv.PponStoreGet(vpd.BusinessHourGuid, selectedStore);
            int orderedQuantity = vps.OrderedQuantity ?? 0;
            qty = (vpd.SaleMultipleBase ?? 0) > 0 ? vpd.SaleMultipleBase.Value : qty;
            if (vps.TotalQuantity != null && orderedQuantity + qty > (vps.TotalQuantity ?? 0))
            {
                return true;
            }
            return false;
        }
        #endregion

        #region SystemCode

        public static SystemCodeCollection DealType2GetList(int parentId)
        {
            return _systemProv.SystemCodeGetListByParentId(parentId);
        }

        #endregion

        #region category

        public static Category SubDealCategoryGet(int categoryId)
        {
            return _sellerProv.CategoryGet(categoryId);
        }

        public static List<ViewCategoryDependency> ViewCategoryDependencyGetByParentId(int subCategoryId,
            List<ViewCategoryDependency> allViewCategoryDependencies = null)
        {
            if (allViewCategoryDependencies != null)
            {
                return allViewCategoryDependencies.Where(t => t.ParentId == subCategoryId).OrderBy(t => t.CategoryId).ToList();
            }
            return _sellerProv.ViewCategoryDependencyGetByParentId(subCategoryId).ToList();
        }

        public static List<int> ViewCategoryDependencyGetByParentType(CategoryType categorytype)
        {
            return _sellerProv.ViewCategoryDependencyGetByParentType((int)categorytype).Select(x => x.CategoryId).ToList();
        }

        public static List<int> ShowFrontEndCategoryIdGetAllByCategoryType(CategoryType categorytype)
        {
            CategoryCollection allcategorys = _sellerProv.CategoryGetList((int)categorytype);
            List<int> isShowFrontEndCategorys = (allcategorys.ToList()).Where(x => x.IsShowFrontEnd).Select(y => y.Id).ToList();
            return (isShowFrontEndCategorys == null) ? new List<int>() : isShowFrontEndCategorys;
        }

        public static List<string> ViewCategoryDealGetNameList(List<int> cityList, CategoryType categoryType, Guid bid)
        {
            List<string> rtn = new List<string>();
            foreach (int cityId in cityList)
            {
                rtn = rtn.Union(ViewCategoryDealGetNameList(cityId, categoryType, bid)).ToList();
            }
            return rtn;
        }

        public static List<string> ViewCategoryDealGetNameList(int cityId, CategoryType categoryType, Guid bid)
        {
            ViewCategoryDealCollection vcdc = _pponProv.ViewCategoryDealGetCidListByBid(cityId, categoryType, bid);
            return vcdc.Select(x => x.Name).ToList();
        }

        public static List<string> ViewCategoryDealByCid(int cityId, CategoryType categoryType, Guid bid)
        {
            ViewCategoryDealCollection vcdc = _pponProv.ViewCategoryDealGetCidListByBid(cityId, categoryType, bid);
            return vcdc.Select(x => x.Name).ToList();
        }

        private static ConcurrentDictionary<int, ViewCategoryDependencyCollection> _viewSubCategoryDependencyCollection = new ConcurrentDictionary<int, ViewCategoryDependencyCollection>();
        public static void ViewSubCategoryDependencyCollectionClear()
        {
            _viewSubCategoryDependencyCollection.Clear();
        }

        /// <summary>
        /// 比對MainDeals取得有檔次的DealCategoryCount及SubDealCategoryCount資料 
        /// </summary>
        /// <param name="categoryDealCounts"></param>
        /// <param name="multiplemaindeals"></param>
        /// <returns></returns>
        public static List<KeyValuePair<CategoryDealCount, List<CategoryDealCount>>> SubCategoryDealCountByMainCategoryDealCount(
            List<CategoryDealCount> categoryDealCounts, List<MultipleMainDealPreview> multiplemaindeals)
        {
            var subCategoryCounts = new List<KeyValuePair<CategoryDealCount, List<CategoryDealCount>>>();

            foreach (var categorydealcount in categoryDealCounts)
            {
                List<CategoryDealCount> subCategoryDealCountList = new List<CategoryDealCount>();
                var subcategorydepency = _viewSubCategoryDependencyCollection.GetOrAdd(categorydealcount.CategoryId, delegate
                {
                    return _sellerProv.ViewCategoryDependencyGetByParentId(categorydealcount.CategoryId);
                });

                foreach (var subcategory in subcategorydepency.Where(x => x.IsShowFrontEnd).OrderBy(y => y.Seq))
                {
                    int subCategoryId = subcategory.CategoryId;
                    var subcategroydealcount = new CategoryDealCount(subCategoryId, subcategory.CategoryName);
                    subcategroydealcount.DealCount = multiplemaindeals.Count(
                        x => x.DealCategoryIdList.Contains(subCategoryId) && x.DealCategoryIdList.Contains(categorydealcount.CategoryId));
                    if (subcategroydealcount.DealCount > 0)
                    {
                        subCategoryDealCountList.Add(subcategroydealcount);
                    }
                }

                if (subCategoryDealCountList.Count > 0)
                {
                    var totalcategroydealcount = new CategoryDealCount(categorydealcount.CategoryId, "全部");
                    totalcategroydealcount.DealCount = categorydealcount.DealCount;
                    subCategoryDealCountList.Insert(0, totalcategroydealcount);
                }
                subCategoryCounts.Add(new KeyValuePair<CategoryDealCount, List<CategoryDealCount>>(categorydealcount, subCategoryDealCountList));
            }

            return subCategoryCounts;
        }

        /// <summary>
        /// 回傳檔次所在頻道
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static List<CategoryNode> DealChannelCategoryGet(Guid bid)
        {
            var dealCoategoryId = ViewPponDealManager.DefaultManager.GetCategoryIds(bid);
            return CategoryManager.PponChannelCategoryTree.CategoryNodes.Where(x => dealCoategoryId.Contains(x.CategoryId)).ToList();
        }

        public static List<string> CategoryNameGetListByBid(Guid bid, params CategoryType[] type)
        {
            List<string> rtn = new List<string>();
            CategoryDealCollection cdc = _pponProv.CategoryDealsGetList(bid);
            foreach (CategoryDeal cd in cdc)
            {
                Category cate = CategoryManager.CategoryGetById(cd.Cid);
                if (cate != null && type.Contains((CategoryType)cate.Type))
                {
                    rtn.Add(cate.Name);
                }
            }
            return rtn;
        }

        #endregion category

        #region 檔次質量相關

        /// <summary>
        /// 取出最高毛利檔次Bid
        /// </summary>
        /// <param name="bids">Business_Hour_Guid</param>
        /// <returns></returns>
        public static Guid GetMaximumGrossProfitFromBids(List<Guid> bids)
        {
            var vpdoiCol = _pponProv.ViewPponDealOperatingInfoGetInBids(bids);

            return (vpdoiCol.Count > 0)
                ? vpdoiCol.OrderByDescending(x => x.GrossProfit)
                    .Select(x => x.BusinessHourGuid)
                    .First()
                    .GetValueOrDefault()
                : bids[0];
        }

        /// <summary>
        /// 取出毛利, 多檔次主檔則回傳最低子檔毛利
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static decimal GetMinimumGrossMarginFromBids(Guid bid, bool Type = false)
        {
            var data = _pponProv.ViewDealBaseGrossMarginGet(bid);
            if (!data.IsLoaded)
            {
                return 0;
            }

            decimal tmpData;
            //非多檔次或已是多檔次的代表檔
            if (data.MainBusinessHourGuid == null || data.BusinessHourGuid == data.MainBusinessHourGuid || Type)
            {
                tmpData = data.GrossMargin ?? 0;
                tmpData = Math.Floor(tmpData * 10000) / 10000;
                return tmpData;
            }
            //子檔次則依代表檔bid重抓
            data = _pponProv.ViewDealBaseGrossMarginGet((Guid)data.MainBusinessHourGuid);
            tmpData = data.GrossMargin ?? 0;
            tmpData = Math.Floor(tmpData * 10000) / 10000;
            return tmpData;
        }

        /// <summary>
        /// 取得檔次毛利
        /// 如為單檔，取該檔毛利
        /// 如果子檔，取該檔毛利
        /// 如果母檔且結檔，回傳最低子檔毛利
        /// 如果母檔未結檔，回傳未結檔子檔的最低毛利
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static decimal GetDealMinimumGrossMargin(Guid bid, bool tryGetFromMemoryCache = false)
        {
            string key = string.Format("DealMinimumGrossMargin:{0}", bid);
            decimal? margin = MemoryCache.Default.Get(key) as decimal?;
            if (tryGetFromMemoryCache == false || margin == null)
            {
                margin = _pponProv.DealMinGrossMarginGet(bid);
                MemoryCache.Default.Set(key, margin, new DateTimeOffset(DateTime.Now.AddMinutes(60)));
            }
            return margin == null ? 0m : margin.Value;
        }

        /// <summary>
        /// 傳入母檔或子檔bid查詢是否為折價券黑名單
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static bool IsBlackDeal(Guid bid)
        {
            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            Guid mainBid;
            if (vpd != null)
            {
                mainBid = vpd.MainBid ?? bid;
            }
            else
            {
                mainBid = bid;
            }

            return _orderProv.DiscountLimitEnabledGetByBid(mainBid);
        }

        /// <summary>
        /// 依據檔次毛利率判斷是否可使用優惠券
        /// </summary>
        /// <param name="vpd"></param>
        /// <returns></returns>
        public static bool DiscountCanUsedWithGrossMargin(IViewPponDeal vpd)
        {
            bool allowedUseDiscount = true;

            //在折價券黑名檔的檔次，一律不可使用折價券
            if (IsBlackDeal(vpd.BusinessHourGuid))
            {
                return false;
            }

            return allowedUseDiscount;
        }

        public static List<DealTimeSlotSortElement> DealTimeSlotSortElementGetByCity(int cityId)
        {
            return _pponProv.DealTimeSlotSortElementGet().Where(x => x.CityId == cityId).ToList();
        }

        public static bool DealTimeSlotSortElementUpdate(DealTimeSlotSortElement dtsse)
        {
            return _pponProv.DealTimeSlotSortElementUpdate(dtsse);
        }

        #endregion

        #region 搜尋關鍵字的log
        public static void AddPponSearchLog(string word, int dealCount, string sessionId, string userName)
        {
            if (_pponProv.PponSearchLogGet(sessionId, word).IsLoaded)
            {
                return;
            }
            if (word == "測試")
            {
                return;
            }
            int deviceType = (int)Helper.GetOrderFromType();
            _pponProv.PponSearchLogSet(new PponSearchLog
            {
                Word = word,
                ResultCount = dealCount,
                SessionId = sessionId,
                CreateId = userName,
                CreateTime = DateTime.Now,
                DeviceType = deviceType
            });
        }

        public static void UpdateKeywords()
        {
            int minCount = config.PponSearchLogLimitCount;
            _pponProv.KeywordInsertByPponSearchLog(minCount);
        }
        #endregion

        #region 多重選項

        public static string GetPponOptionSetupText(Guid bid)
        {
            PponOptionCollection options = _pponProv.PponOptionGetList(bid);
            if (options.Count == 0)
            {
                return string.Empty;
            }

            StringBuilder sbResult = new StringBuilder();

            IEnumerable<IGrouping<int, PponOption>> orderedCatgs = options.GroupBy(x => x.CatgSeq).OrderBy(g => g.Key);

            foreach (IGrouping<int, PponOption> catgGroup in orderedCatgs)
            {
                sbResult.Append(catgGroup.First().CatgName + ":\n");
                IList<PponOption> orderOptions = catgGroup.OrderBy(option => option.OptionSeq).ToList();
                foreach (PponOption option in orderOptions)
                {
                    if (option.Quantity.HasValue)
                    {
                        sbResult.Append(string.Format("[{0}]{1}{2}\n", option.Quantity, (string.IsNullOrEmpty(option.ItemNo) ? string.Empty : option.ItemNo + "!"), option.OptionName));
                    }
                    else
                    {
                        sbResult.Append(string.Format("{0}{1}\n", (string.IsNullOrEmpty(option.ItemNo) ? string.Empty : option.ItemNo + "!"), option.OptionName));
                    }
                }
                sbResult.Append("\n");
            }

            return sbResult.ToString();
        }

        #endregion

        #region Copy Deal

        public static void CopyDeal(Guid bid, bool isCopyAsSubDeal, bool isCopyComboDeals, out string errorMessages, out Guid returnNewGuid, string username)
        {
            var cloneDealGuids = new List<Guid>();
            returnNewGuid = Guid.Empty;
            errorMessages = string.Empty;
            var mainDealGuid = isCopyAsSubDeal ? bid : Guid.Empty;
            string errorMessage;
            if (isCopyComboDeals)
            {
                //打包複製主檔 只須作[複製檔次](無須寫資料進combo_deals) 其餘子檔則進行[複製成子檔]作業
                mainDealGuid = SetUpCloneDeal(bid, mainDealGuid, out errorMessage, username, isCopyAsSubDeal);
                if (mainDealGuid != Guid.Empty)
                {
                    cloneDealGuids = _pponProv.GetComboDealByBid(bid, false)
                                            .Select(x => x.BusinessHourGuid)
                                            .ToList();
                }
            }
            else
            {
                cloneDealGuids.Add(bid);
            }
            foreach (var cdeal in cloneDealGuids)
            {
                var newGuid = SetUpCloneDeal(cdeal, mainDealGuid, out errorMessage, username, isCopyAsSubDeal);
                if (newGuid != Guid.Empty)
                {
                    //打包複製須回傳複製後之新主檔bid 其餘回傳new bid
                    returnNewGuid = isCopyComboDeals ? mainDealGuid : newGuid;
                }
                else
                {
                    errorMessages += string.Join("檔次{0}：{1}\n", cdeal, errorMessage);
                }
            }
        }

        public static Guid SetUpCloneDeal(Guid cloneDealGuid, Guid mainDealGuid, out string errorMessage, string username, bool isCopyAsSubDeal)
        {
            errorMessage = string.Empty;
            //檢查目前BusinessHour存不存在
            var pponDeal = _pponProv.PponDealGet(cloneDealGuid);
            if (!pponDeal.Deal.IsLoaded)
            {
                errorMessage = "好康不存在，請先將目前編輯的好康存檔。";
                return Guid.Empty;
            }
            PponDeal mainDeal = null;
            if (mainDealGuid != Guid.Empty)
            {
                mainDeal = mainDealGuid == cloneDealGuid
                            ? pponDeal
                            : _pponProv.PponDealGet(mainDealGuid);
                if (!mainDeal.Deal.IsLoaded)
                {
                    errorMessage = "主檔不存在，複製失敗。";
                    return Guid.Empty;
                }
            }
            //複製相同的資料
            var entity = GetNewPponDealClone(pponDeal);
            //不複製
            entity.Deal.ChangedExpireDate = null; //調整有效期限截止日
            entity.Deal.ExpireRedirectUrl = null;
            entity.Deal.ExpireRedirectDisplay = null;
            entity.Deal.UseExpireRedirectUrlAsCanonical = false;

            //修改有差異的部分
            entity.Store = pponDeal.Store;
            entity.Deal.Guid = Helper.GetNewGuid(entity);
            entity.DealContent.BusinessHourGuid = entity.Deal.Guid;
            entity.ItemDetail.BusinessHourGuid = entity.Deal.Guid;
            entity.ItemDetail.MenuGuid = Guid.Empty;
            entity.ItemDetail.Guid = Helper.GetNewGuid(entity);
            entity.Deal.CreateTime = entity.ItemDetail.CreateTime = DateTime.Now;
            entity.Deal.CreateId = entity.ItemDetail.CreateId = username;

            if (isCopyAsSubDeal)
            {
                entity.DealContent.MenuD = "";
                entity.DealContent.MenuE = "";
                entity.DealContent.MenuF = "";
                entity.DealContent.MenuG = "";
            }

            //重設多檔次狀態
            entity.Deal.BusinessHourStatus = entity.Deal.BusinessHourStatus & ~(int)BusinessHourStatus.ComboDealMain;
            entity.Deal.BusinessHourStatus = entity.Deal.BusinessHourStatus & ~(int)BusinessHourStatus.ComboDealSub;
            //20140602 免運功能取消  使用複製檔次時  將勾選移除
            entity.Deal.BusinessHourStatus = entity.Deal.BusinessHourStatus & ~(int)BusinessHourStatus.NoShippingFeeMessage;

            if (mainDealGuid == Guid.Empty) //非執行複製為子檔 才進行deal_time_slot新增
            {
                foreach (var timeSlot in pponDeal.TimeSlotCollection) //只針對原檔次有deal_time_slot才進行複製 複製完後有進行檔次異動 會另外進行deal_time_slot新增
                {
                    var copyTimeSlot = GetNewDealTimeSlot(entity.Deal.Guid, timeSlot.CityId,
                                                                          _pponProv.DealTimeSlotGetMaxSeqInCityAndDay(timeSlot.CityId, timeSlot.EffectiveStart) + 1,
                                                                          //STEP1.複製檔次做為新檔，檔次需往前放。
                                                                          //PponFacade.GetDealTimeSlotNonLockSeq(timeSlot.CityId, timeSlot.EffectiveStart), 
                                                                          timeSlot.EffectiveStart, timeSlot.EffectiveEnd, (DealTimeSlotStatus)timeSlot.Status,
                                                                          timeSlot.IsInTurn);
                    //STEP2.編輯複製後的新檔，會被認為此檔已存在，而被放到最後一筆。
                    //PponFacade.SetNonLockDealTimeSlotSeqAddOne(timeSlot.CityId, timeSlot.EffectiveStart); 
                    entity.TimeSlotCollection.Add(copyTimeSlot);
                }
            }

            #region 複製圖檔
            //一般檔次圖片
            if (!string.IsNullOrEmpty(pponDeal.DealContent.ImagePath))
            {
                string[] baseImageFiles = Helper.GetRawPathsFromRawData(entity.DealContent.ImagePath);
                var newImageFiles = new List<string>();
                //避免程式數度太快導致圖檔檔名相同，加一個timeShift的參數，由時間轉檔名時增加差異。
                var timeShift = 0;
                foreach (var fileStr in baseImageFiles)
                {
                    if (string.IsNullOrEmpty(fileStr.Trim()))
                    {
                        //20120816_Jusin 原本如果第 N張圖被刪, 則程式直接跳過並由第 N+1張圖遞補, 這樣不行, 所以加了下面這行以加入空字串
                        newImageFiles.Add(string.Empty);
                        continue;
                    }
                    var baseFile = fileStr.Split(',');
                    var newFileName = (DateTime.Now.Ticks + timeShift).ToString() + "." + baseFile[1].Split('.')[1];
                    ImageFacade.CopyFile(UploadFileType.PponEvent, baseFile[0], baseFile[1], entity.Store.SellerId, newFileName);
                    newImageFiles.Add(ImageFacade.GenerateMediaPath(entity.Store.SellerId, newFileName));
                    timeShift++;
                }

                if (newImageFiles.Count > 0)
                {
                    entity.DealContent.ImagePath = Helper.GenerateRawDataFromRawPaths(newImageFiles.ToArray());
                }
            }
            else
            {
                entity.DealContent.ImagePath = null;
            }

            //特殊檔次圖片
            if (!string.IsNullOrEmpty(pponDeal.DealContent.SpecialImagePath))
            {
                string[] baseImageFiles = Helper.GetRawPathsFromRawData(entity.DealContent.SpecialImagePath);
                var newImageFiles = new List<string>();
                //避免程式數度太快導致圖檔檔名相同，加一個timeShift的參數，由時間轉檔名時增加差異。
                var timeShift = 0;
                foreach (var fileStr in baseImageFiles)
                {
                    if (string.IsNullOrEmpty(fileStr.Trim()))
                    {
                        //20120816_Jusin 原本如果第 N張圖被刪, 則程式直接跳過並由第 N+1張圖遞補, 這樣不行, 所以加了下面這行以加入空字串
                        newImageFiles.Add(string.Empty);
                        continue;
                    }
                    var baseFile = fileStr.Split(',');
                    var newFileName = "SP" + (DateTime.Now.Ticks + timeShift).ToString() + "." + baseFile[1].Split('.')[1];
                    ImageFacade.CopyFile(UploadFileType.PponEvent, baseFile[0], baseFile[1], entity.Store.SellerId, newFileName);
                    newImageFiles.Add(ImageFacade.GenerateMediaPath(entity.Store.SellerId, newFileName));
                    timeShift++;
                }

                if (newImageFiles.Count > 0)
                {
                    entity.DealContent.SpecialImagePath = Helper.GenerateRawDataFromRawPaths(newImageFiles.ToArray());
                }
            }
            else
            {
                entity.DealContent.SpecialImagePath = null;
            }
            #endregion 複製圖檔

            #region SMSContent

            var baseSms = _pponProv.SMSContentGet(cloneDealGuid);
            string[] contents = baseSms.Content.Split('|');
            var sc = new SmsContent
            {
                BusinessHourGuid = entity.Deal.Guid,
                Content = string.Format("{0}|{1}|{2}", contents[0], "", contents[2]),
                CreateId = username,
                CreateTime = DateTime.Now
            };

            #endregion SMSContent

            var oldGo = _orderProv.GroupOrderGetByBid(cloneDealGuid);

            #region Save DealAccounting

            var sellerInfo = _sellerProv.SellerGet(entity.Deal.SellerGuid);
            int? sellerPaidType = null;
            if (sellerInfo.IsLoaded)
            {
                sellerPaidType = sellerInfo.RemittanceType;
            }
            var dAccounting = _pponProv.DealAccountingGet(cloneDealGuid);
            var newDealAccounting = new DealAccounting();
            if (dAccounting.IsLoaded)
            {
                newDealAccounting.BusinessHourGuid = entity.Deal.Guid;
                newDealAccounting.SalesCommission = dAccounting.SalesCommission;
                newDealAccounting.SalesBonus = dAccounting.SalesBonus;
                newDealAccounting.Status = dAccounting.Status;
                newDealAccounting.SalesId = dAccounting.SalesId;
                newDealAccounting.OperationSalesId = dAccounting.OperationSalesId;
                newDealAccounting.AccCity = dAccounting.AccCity;
                newDealAccounting.DeptId = dAccounting.DeptId;
                newDealAccounting.Paytocompany = dAccounting.Paytocompany;

                if (Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.KindDeal))
                {
                    newDealAccounting.RemittanceType = (int)RemittanceType.Others;
                    newDealAccounting.VendorBillingModel = (int)VendorBillingModel.None;
                }
                else
                {
                    if (dAccounting.RemittanceType == (int)RemittanceType.AchMonthly ||
                        dAccounting.RemittanceType == (int)RemittanceType.AchWeekly ||
                        dAccounting.RemittanceType == (int)RemittanceType.ManualMonthly ||
                        dAccounting.RemittanceType == (int)RemittanceType.ManualPartially ||
                        dAccounting.RemittanceType == (int)RemittanceType.ManualWeekly)
                    {
                        //如果為舊版核銷對帳系統的出帳方式，則複製賣家設定頁的設定
                        newDealAccounting.RemittanceType = Convert.ToInt32(sellerPaidType);//前台按複製的時候有檢查，若sellerPaidType == null則會擋掉，不可複製
                    }
                    else
                    {
                        //如果為新版核銷對帳系統的出帳方式，直接複製
                        if (config.IsRemittanceFortnightly)
                        {
                            if (entity.Property.DeliveryType == (int)DeliveryType.ToShop || (entity.Property.DeliveryType == (int)DeliveryType.ToHouse && dAccounting.DeptId != EmployeeChildDept.S010.ToString()))
                            {
                                if (SellerFacade.IsAgreeNewContractSeller(entity.Deal.SellerGuid, (int)DeliveryType.ToShop))
                                {
                                    //同意憑證合約照商家設定
                                    newDealAccounting.RemittanceType = Convert.ToInt32(sellerPaidType);
                                }
                                else
                                {
                                    //不同意憑證合約照舊
                                    newDealAccounting.RemittanceType = dAccounting.RemittanceType;
                                }
                            }
                            else
                            {
                                if (SellerFacade.IsAgreeNewContractSeller(entity.Deal.SellerGuid, (int)DeliveryType.ToHouse))
                                {
                                    //同意憑證合約照出貨設定
                                    if (entity.Property.ShipType == (int)DealShipType.Ship72Hrs)
                                    {
                                        newDealAccounting.RemittanceType = (int)RemittanceType.Fortnightly;
                                    }
                                    else
                                    {
                                        newDealAccounting.RemittanceType = (int)RemittanceType.Monthly;
                                    }
                                }
                                else
                                {
                                    //不同意之宅配商家不予複製
                                }

                            }
                        }
                        else
                        {
                            newDealAccounting.RemittanceType = dAccounting.RemittanceType;
                        }

                    }

                    //如果原檔次是憑證檔且用舊核銷對帳, 則新檔次設定為新核銷對帳
                    newDealAccounting.VendorBillingModel = dAccounting.VendorBillingModel == (int)VendorBillingModel.None ? (int)VendorBillingModel.BalanceSheetSystem : dAccounting.VendorBillingModel;
                }
                newDealAccounting.VendorReceiptType = dAccounting.VendorReceiptType;
                newDealAccounting.IsInputTaxRequired = dAccounting.IsInputTaxRequired;
                newDealAccounting.Message = dAccounting.Message;
                newDealAccounting.Commission = dAccounting.Commission;
            }

            #endregion Save DealAccounting

            #region DealProperty

            var newDealProperty = new DealProperty();
            newDealProperty.CityList = entity.Property.CityList;
            newDealProperty.BusinessHourGuid = entity.Deal.Guid;
            newDealProperty.CreateId = username;
            newDealProperty.CreateTime = DateTime.Now;
            newDealProperty.DealType = entity.Property.DealType;
            newDealProperty.NewDealType = entity.Property.NewDealType;
            newDealProperty.EntrustSell = entity.Property.EntrustSell;
            newDealProperty.TravelPlace = entity.Property.TravelPlace;
            newDealProperty.SellerVerifyType = entity.Property.SellerVerifyType;
            newDealProperty.DealAccBusinessGroupId = entity.Property.DealAccBusinessGroupId;
            newDealProperty.DealEmpName = entity.Property.DealEmpName;
            newDealProperty.DevelopeSalesId = entity.Property.DevelopeSalesId;
            newDealProperty.OperationSalesId = entity.Property.OperationSalesId;
            newDealProperty.DeliveryType = entity.Property.DeliveryType;
            newDealProperty.ShoppingCart = entity.Property.ShoppingCart;
            newDealProperty.ComboPackCount = entity.Property.ComboPackCount;
            newDealProperty.MultipleBranch = entity.Property.MultipleBranch;
            //Copy 每人每日限購份數
            newDealProperty.IsDailyRestriction = entity.Property.IsDailyRestriction;
            //組合數
            newDealProperty.QuantityMultiplier = entity.Property.QuantityMultiplier;
            //成套憑證組合
            newDealProperty.SaleMultipleBase = entity.Property.SaleMultipleBase;
            newDealProperty.PresentQuantity = entity.Property.PresentQuantity;
            newDealProperty.GroupCouponDealType = entity.Property.GroupCouponDealType;
            newDealProperty.GroupCouponAppStyle = entity.Property.GroupCouponAppStyle;
            //是否顯示銷售倍數
            newDealProperty.IsQuantityMultiplier = entity.Property.IsQuantityMultiplier;
            //是否顯示均價
            newDealProperty.IsAveragePrice = entity.Property.IsAveragePrice;
            newDealProperty.ActivityUrl = entity.Property.ActivityUrl;
            newDealProperty.IsMergeCount = entity.Property.IsMergeCount;
            newDealProperty.LabelTagList = entity.Property.LabelTagList;
            newDealProperty.LabelIconList = entity.Property.LabelIconList;
            newDealProperty.CustomTag = entity.Property.CustomTag;
            //設定BarcodeType
            newDealProperty.CouponCodeType = entity.Property.CouponCodeType;
            newDealProperty.PinType = entity.Property.PinType;
            //全家兌換價
            newDealProperty.ExchangePrice = entity.Property.ExchangePrice;
            //訂位系統
            newDealProperty.BookingSystemType = entity.Property.BookingSystemType;
            newDealProperty.AdvanceReservationDays = entity.Property.AdvanceReservationDays;
            newDealProperty.CouponUsers = entity.Property.CouponUsers;
            newDealProperty.IsReserveLock = entity.Property.IsReserveLock;
            newDealProperty.TmallRmbExchangeRate = entity.Property.TmallRmbExchangeRate;
            newDealProperty.TmallRmbPrice = entity.Property.TmallRmbPrice;
            newDealProperty.IsZeroActivityShowCoupon = entity.Property.IsZeroActivityShowCoupon;
            newDealProperty.IsLongContract = entity.Property.IsLongContract;

            newDealProperty.ShipType = entity.Property.ShipType;
            newDealProperty.ShippingdateType = entity.Property.ShippingdateType;
            newDealProperty.Shippingdate = entity.Property.Shippingdate;
            newDealProperty.ProductUseDateStartSet = entity.Property.ProductUseDateStartSet;
            newDealProperty.ProductUseDateEndSet = entity.Property.ProductUseDateEndSet;

            //分期相關
            newDealProperty.Installment3months = entity.Property.Installment3months;
            newDealProperty.Installment6months = entity.Property.Installment6months;
            newDealProperty.Installment12months = entity.Property.Installment12months;
            newDealProperty.DenyInstallment = entity.Property.DenyInstallment;

            //類別屬性
            newDealProperty.CategoryList = entity.Property.CategoryList;

            //咖啡寄杯
            newDealProperty.IsDepositCoffee = entity.Property.IsDepositCoffee;
            //活動遊戲檔
            newDealProperty.IsGame = entity.Property.IsGame;
            //代銷贈品檔
            newDealProperty.IsChannelGift = entity.Property.IsChannelGift;
            //24到貨
            newDealProperty.IsWms = entity.Property.IsWms;
            newDealProperty.AgentChannels = entity.Property.AgentChannels;

            #region 展演檔次
            newDealProperty.IsPromotionDeal = entity.Property.IsPromotionDeal;
            newDealProperty.IsExhibitionDeal = entity.Property.IsExhibitionDeal;
            newDealProperty.IsReserveLock = entity.Property.IsReserveLock;
            #endregion 展演檔次

            #endregion DealProperty

            #region PponStore

            var selectedPponStore = _pponProv.PponStoreGetListByBusinessHourGuid(cloneDealGuid);
            var insStores = new PponStoreCollection();
            foreach (var selectedStore in selectedPponStore)
            {
                var pponStore = new PponStore();
                pponStore.BusinessHourGuid = entity.Deal.Guid;
                pponStore.StoreGuid = selectedStore.StoreGuid;
                pponStore.UseTime = selectedStore.UseTime;
                pponStore.ResourceGuid = Guid.NewGuid();
                pponStore.TotalQuantity = selectedStore.TotalQuantity;
                pponStore.SortOrder = selectedStore.SortOrder;
                pponStore.VbsRight = selectedStore.VbsRight;
                pponStore.CreateId = username;
                pponStore.CreateTime = DateTime.Now;
                insStores.Add(pponStore);
            }

            #endregion PponStore

            #region WeeklyPayAccount
            var accounts = _orderProv.WeeklyPayAccountGetList(cloneDealGuid);
            var newAccounts = new WeeklyPayAccountCollection();
            foreach (var account in accounts)
            {
                var newAccount = new WeeklyPayAccount();
                newAccount.BusinessHourGuid = entity.Deal.Guid;
                newAccount.EventName = account.EventName;
                newAccount.CompanyName = account.CompanyName;
                newAccount.SellerName = account.SellerName;
                newAccount.AccountName = account.AccountName;
                newAccount.AccountId = account.AccountId;
                newAccount.BankNo = account.BankNo;
                newAccount.BranchNo = account.BranchNo;
                newAccount.AccountNo = account.AccountNo;
                newAccount.Email = account.Email;
                newAccount.Message = account.Message;
                newAccount.SignCompanyID = account.SignCompanyID;
                newAccount.AccountantName = account.AccountantName;
                newAccount.AccountantTel = account.AccountantTel;
                newAccount.StoreGuid = account.StoreGuid;
                newAccount.Creator = username;
                newAccount.CreateDate = DateTime.Now;
                newAccounts.Add(newAccount);
            }
            _orderProv.WeeklyPayAccountSetList(newAccounts);
            #endregion

            #region 運費處理

            var oldFrieights = _pponProv.CouponFreightGetList(cloneDealGuid);
            var newFrieights = new CouponFreightCollection();
            foreach (var freight in oldFrieights)
            {
                var newFreight = new CouponFreight();
                newFreight.BusinessHourGuid = entity.Deal.Guid;
                newFreight.StartAmount = freight.StartAmount;
                newFreight.EndAmount = freight.EndAmount;
                newFreight.FreightAmount = freight.FreightAmount;
                newFreight.FreightType = freight.FreightType;
                newFreight.CreateId = username;
                newFreight.CreateTime = DateTime.Now;
                newFreight.PayTo = freight.PayTo;
                newFrieights.Add(newFreight);
            }

            #endregion 運費處理

            //首頁類別

            #region 首頁類別 copy PponDeal Category Deal data

            int categoryId = config.EveryDayNewDealsCategoryId;

            var oldCds = _pponProv.CategoryDealsGetList(cloneDealGuid);
            var cds = new CategoryDealCollection();

            List<ViewCategoryDependency> subCategorylist = _sellerProv.ViewCategoryDependencyGetAll(null)
                                                    .Where(x => x.CategoryType == (int)CategoryType.DealCategory).ToList();

            if (config.IsEveryDayNewDeals)
            {
                foreach (var cd in oldCds)
                {
                    //不顯示後台的頻道不複製
                    var c = subCategorylist.Where(x => x.CategoryId == cd.Cid).FirstOrDefault();
                    if (c != null && !c.IsShowBackEnd)
                    {
                        continue;
                    }

                    if (cd.Cid != categoryId)
                    {
                        cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = cd.Cid });
                    }
                }
            }
            else
            {
                foreach (var cd in oldCds)
                {
                    //不顯示後台的頻道不複製
                    var c = subCategorylist.Where(x => x.CategoryId == cd.Cid).FirstOrDefault();
                    if (c != null && !c.IsShowBackEnd)
                    {
                        continue;
                    }

                    cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = cd.Cid });
                }
            }


            #endregion 首頁類別 copy PponDeal Category Deal data


            #region DealPrperty part 2

            if (cds.FirstOrDefault(t => t.Cid == CategoryManager.Default.Travel.CategoryId) != null)
            {
                newDealProperty.IsTravelDeal = true;
            }
            else
            {
                newDealProperty.IsTravelDeal = false;
            }
            #endregion DealPrperty part 2

            #region copy deal 進貨價處理

            var multiCost = _pponProv.DealCostGetList(cloneDealGuid);
            var costCol1 = new DealCostCollection();

            foreach (var freight in multiCost.OrderBy(x => x.Id))
            {
                var newFreight = new DealCost();
                newFreight.BusinessHourGuid = entity.Deal.Guid;
                newFreight.Cost = freight.Cost;
                newFreight.Quantity = freight.Quantity;
                newFreight.CumulativeQuantity = freight.CumulativeQuantity;
                newFreight.LowerCumulativeQuantity = freight.LowerCumulativeQuantity;
                costCol1.Add(newFreight);
            }

            #endregion copy deal 進貨價處理

            #region dealpayment

            var newDealPayment = new DealPayment();
            newDealPayment.BusinessHourGuid = entity.Deal.Guid;
            newDealPayment.BankId = entity.Payment.BankId;
            newDealPayment.IsBankDeal = entity.Payment.IsBankDeal;
            newDealPayment.AllowGuestBuy = entity.Payment.AllowGuestBuy;
            #endregion

            //新增好康
            if (_pponProv.PponDealSet(entity, true))
            {
                //寫入SMS內容
                _pponProv.SMSContentSet(sc);
                //寫入DealProperty
                _pponProv.DealPropertySet(newDealProperty);
                //寫入DealAccounting
                _pponProv.DealAccountingSet(newDealAccounting);
                //寫入pponStore
                _pponProv.PponStoreSetList(insStores);
                //寫入couponFreight
                _pponProv.CouponFreightSetList(newFrieights);
                //寫入CategoryDeal Sam
                PponFacade.SaveCategoryDeals(cds, entity.Deal.Guid);
                //寫入DealCost Sam
                _pponProv.DealCostSetList(costCol1);
                //寫入DealPayment
                _pponProv.DealPaymentSet(newDealPayment);

                int uniqueId = _pponProv.DealPropertyGet(entity.Deal.Guid).UniqueId;

                #region 購物車處理
                if (config.ShoppingCartV2Enabled)
                {
                    var SCFItem = _sellerProv.ShoppingCartFreightsItemGetByBid(cloneDealGuid).Clone();
                    SCFItem.UniqueId = uniqueId;
                    SCFItem.BusinessHourGuid = entity.Deal.Guid;
                    SCFItem.CreateTime = DateTime.Now;
                    SCFItem.CreateUser = username;
                    _sellerProv.ShoppingCartFreightsItemSetBulkInsert(new List<ShoppingCartFreightsItem> { SCFItem });
                    SellerFacade.ShoppingCartFreightsLogSet(username, SCFItem.FreightsId, string.Format("【套用商品】：{0}", uniqueId), ShoppingCartFreightLogType.User);
                }
                #endregion

                #region Group Order Settings

                GroupOrder go;
                Guid goGuid = OrderFacade.MakeGroupOrder(config.ServiceEmail, config.ServiceName,
                    entity.Deal.Guid, entity.Deal.CreateTime, entity.Deal.BusinessHourOrderTimeE);
                go = _orderProv.GroupOrderGet(goGuid);
                //是否開立免稅發票給消費者
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.NoTax), go.Status ?? 0, GroupOrderStatus.NoTax);
                //不接受退貨
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.NoRefund), go.Status ?? 0, GroupOrderStatus.NoRefund);
                //結檔後七天不能退貨
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.DaysNoRefund), go.Status ?? 0, GroupOrderStatus.DaysNoRefund);
                //過期不能退貨
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.ExpireNoRefund), go.Status ?? 0, GroupOrderStatus.ExpireNoRefund);
                //演出時間前十日內不能退貨
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.NoRefundBeforeDays), go.Status ?? 0, GroupOrderStatus.NoRefundBeforeDays);
                //是否關閉簡訊
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.DisableSMS), go.Status ?? 0, GroupOrderStatus.DisableSMS);
                //公益檔次
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.KindDeal), go.Status ?? 0, GroupOrderStatus.KindDeal);
                //全家檔次
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.FamiDeal), go.Status ?? 0, GroupOrderStatus.FamiDeal);
                //for PEZ 特殊活動
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.PEZevent), go.Status ?? 0, GroupOrderStatus.PEZevent);
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.PEZeventCouponDownload), go.Status ?? 0, GroupOrderStatus.PEZeventCouponDownload);
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.PEZeventWithUserId), go.Status ?? 0, GroupOrderStatus.PEZeventWithUserId);
                //萊爾富檔次
                go.Status = (int)Helper.SetFlag(Helper.IsFlagSet(oldGo.Status ?? 0, GroupOrderStatus.HiLifeDeal), go.Status ?? 0, GroupOrderStatus.HiLifeDeal);

                //寫入GroupOrder
                _orderProv.GroupOrderSet(go);

                #endregion Group Order Settings

                //選項需於好康存在時才可新增
                var accessoryString = ItemFacade.ConvertAccessoryGroupToSetupText(_itemProv.AccessoryGroupGetListByItem(pponDeal.ItemDetail.Guid));
                var pponOptionStr = PponFacade.GetPponOptionSetupText(cloneDealGuid);
                //edit by jaguar 2013/05/27
                if (!string.IsNullOrEmpty(accessoryString))
                {
                    var grps = ItemFacade.ProcessAccessoryGroupText(entity.Deal.Guid, entity.Store.Guid, "17P", accessoryString);
                    if (grps != null && grps.Count > 0)
                    {
                        List<dynamic> dummy;
                        if (TryParseOptions(pponOptionStr, out dummy))
                        {
                            ProcessAccessory(entity.Deal.Guid, entity.Store.Guid, entity.ItemDetail.Guid, "17P", grps);
                            ProcessOptions(pponOptionStr, entity.Deal.Guid, uniqueId, username);
                        }
                    }
                }

                //是否複製成子檔
                if (mainDealGuid != Guid.Empty)
                {
                    PponFacade.AddComboDeal(mainDeal, new ComboItem
                    {
                        Bid = entity.Deal.Guid,
                        Title = entity.DealContent.CouponUsage
                    }, username);
                }

                return entity.Deal.Guid;
            }

            errorMessage = "複製失敗。";
            return Guid.Empty;
        }

        /// <summary>
        /// 產生一筆新的PponDeal資料
        /// </summary>
        /// <param name="pponDeal">要複製的PponDeal</param>
        /// <returns></returns>
        private static PponDeal GetNewPponDealClone(PponDeal pponDeal)
        {
            var entity = new PponDeal(true);
            entity.Deal = pponDeal.Deal.Clone();
            entity.DealContent = pponDeal.DealContent.Clone();
            entity.ItemDetail = pponDeal.ItemDetail.Clone();
            entity.Property = pponDeal.Property.Clone();
            entity.Payment = pponDeal.Payment.Clone();
            return entity;
        }

        private static DealTimeSlot GetNewDealTimeSlot(Guid bid, int cityId, int seq, DateTime startDate, DateTime endDate, DealTimeSlotStatus status, bool isInTurn = false)
        {
            //新增 此城市這個時間一筆排程記錄
            DealTimeSlot newDeal = new DealTimeSlot();
            newDeal.BusinessHourGuid = bid;
            newDeal.CityId = cityId;
            newDeal.Sequence = seq;
            newDeal.EffectiveStart = startDate;
            newDeal.EffectiveEnd = endDate;
            newDeal.Status = (int)status;
            newDeal.IsInTurn = isInTurn;

            return newDeal;
        }

        /// <summary>
        /// </summary>
        /// <returns>list of {OptCatg, OptItem, OptQuantity} if valid; null if invalid </returns>
        public static bool TryParseOptions(string uiOptionTxt, out List<dynamic> uiOptions)
        {
            List<dynamic> result = new List<dynamic>();

            List<string> lines = uiOptionTxt.Split(new string[] { "\n" }, StringSplitOptions.None).ToList();
            lines.Add(string.Empty);    // add empty line to distinguish the last group of items

            List<string> categories = new List<string>();
            string optionCatg = string.Empty;
            Dictionary<string, int?> optionItems = new Dictionary<string, int?>();  // key: option name; value: quantity

            foreach (string line in lines)
            {
                string text = line.Trim();

                if (string.IsNullOrEmpty(text))
                {
                    // if current group of items is valid, add to result
                    if (optionCatg != string.Empty && optionItems.Count > 0)
                    {
                        foreach (var item in optionItems)
                        {
                            dynamic x = new ExpandoObject();
                            string[] itemInfo = item.Key.Split('!');
                            x.OptCatg = optionCatg;
                            x.OptItem = itemInfo.Count() > 1 ? itemInfo[1].Trim() : itemInfo[0].Trim();
                            x.OptItemNo = itemInfo.Count() > 1 ? itemInfo[0].Trim() : null;
                            x.OptQuantity = item.Value;
                            result.Add(x);
                        }
                    }

                    //reset group variables
                    optionCatg = string.Empty;
                    optionItems = new Dictionary<string, int?>();
                    continue;
                }

                //sample : [100]貨號001!米羅時尚-米色 
                //多重選項增加貨號,已全半形驚嘆號區隔,故這邊先全部替換成半形驚嘆號
                text = text.Replace('！', '!');

                if (text.EndsWith(":")
                    || text.EndsWith("："))
                {
                    if (optionCatg != string.Empty)
                    {
                        uiOptions = null;
                        return false;
                    }

                    optionCatg = text.Substring(0, text.Length - 1);
                    if (categories.Contains(optionCatg))
                    {
                        uiOptions = null;
                        return false;
                    }

                    categories.Add(optionCatg);
                }
                else
                {
                    if (text.StartsWith("["))
                    {
                        int endIdx = text.IndexOf("]");
                        string quantityPart = text.Substring(1, endIdx - 1);
                        string name = text.Substring(endIdx + 1).Trim();

                        if (optionItems.ContainsKey(name))   //duplicate item
                        {
                            uiOptions = null;
                            return false;
                        }

                        int quantity;
                        if (!int.TryParse(quantityPart, out quantity))  //quantity parsing error
                        {
                            uiOptions = null;
                            return false;
                        }

                        optionItems.Add(name, quantity);
                    }
                    else
                    {
                        if (optionItems.ContainsKey(text))   //duplicate item
                        {
                            uiOptions = null;
                            return false;
                        }

                        optionItems.Add(text, null);
                    }
                }
            }

            uiOptions = result;
            return true;
        }

        public static void ProcessOptions(string uiOptionTxt, Guid bid, int uniqueId, string userName)
        {
            List<dynamic> uiOptions;
            if (!TryParseOptions(uiOptionTxt, out uiOptions))
            {
                return;
            }

            DateTime now = DateTime.Now;
            PponOptionCollection dbOptions = _pponProv.PponOptionGetList(bid, false);

            DisableOptions(dbOptions, uiOptions, userName, now);
            UpdateOptions(dbOptions, uiOptions, userName, now);
            InsertOptions(dbOptions, uiOptions, bid, uniqueId, userName, now);
            ResequenceOptions(dbOptions, uiOptions);

            _pponProv.PponOptionSetList(dbOptions);
        }

        public static bool ProcessAccessory(Guid bid, Guid sellerGuid, Guid itemGuid, string accessoryCategoryName, AccessoryGroupCollection newGroups)
        {
            // load existing one first
            AccessoryGroupCollection grps = _itemProv.AccessoryGroupGetListByItem(itemGuid);
            string oldString = !string.IsNullOrEmpty(ItemFacade.ConvertAccessoryGroupToSetupText(grps)) ? ItemFacade.ConvertAccessoryGroupToSetupText(grps) : "";
            string newString = !string.IsNullOrEmpty(ItemFacade.ConvertAccessoryGroupToSetupText(newGroups)) ? ItemFacade.ConvertAccessoryGroupToSetupText(newGroups) : "";
            // only update if not match
            bool isProcessSuccess = newString.CompareTo(oldString) == 0;
            if (!isProcessSuccess)
            {
                isProcessSuccess = ItemFacade.UpdateAccessoryGroupViaSetupText(bid, sellerGuid, itemGuid, accessoryCategoryName, newGroups);
            }
            return isProcessSuccess;
        }

        private static void DisableOptions(PponOptionCollection dbOption, List<dynamic> uiOptions, string userName, DateTime now)
        {
            foreach (PponOption dbOpt in dbOption)
            {
                if (!dbOpt.IsDisabled
                    && !uiOptions.Any(uiOpt =>
                                uiOpt.OptCatg == dbOpt.CatgName
                                && uiOpt.OptItem == dbOpt.OptionName))
                {
                    dbOpt.IsDisabled = true;
                    dbOpt.ModifyId = userName;
                    dbOpt.ModifyTime = now;
                }
            }
        }

        private static void UpdateOptions(PponOptionCollection dbOption, List<dynamic> uiOptions, string userName, DateTime now)
        {
            foreach (PponOption dbOpt in dbOption)
            {
                string optionName = Helper.CapText(dbOpt.OptionName);
                //檢查選項名稱須不區分大小寫 方能調整原選項之大小寫
                var uiOption = uiOptions.FirstOrDefault(uiOpt =>
                                                            uiOpt.OptCatg == dbOpt.CatgName
                                                            && string.Compare(Helper.CapText(uiOpt.OptItem), Helper.CapText(optionName), true) == 0);

                if (uiOption == null)
                {
                    continue;    // DisableOptions
                }

                if (dbOpt.IsDisabled
                    || dbOpt.OptionName != uiOption.OptItem
                    || dbOpt.ItemNo != uiOption.OptItemNo
                    || dbOpt.Quantity != uiOption.OptQuantity)
                {
                    dbOpt.IsDisabled = false;
                    dbOpt.OptionName = uiOption.OptItem;
                    dbOpt.ItemNo = uiOption.OptItemNo;
                    dbOpt.Quantity = uiOption.OptQuantity;
                    dbOpt.ModifyId = userName;
                    dbOpt.ModifyTime = now;
                }
            }
        }

        private static void InsertOptions(PponOptionCollection dbOption, List<dynamic> uiOptions, Guid bid, int uniqueId, string userName, DateTime now)
        {
            foreach (var option in uiOptions)
            {
                //檢查選項名稱是否存在須不區分大小寫 目前ppon_option不允許同個檔次有相同選項存在 避免新增失敗
                if (!dbOption.Any(dbOpt =>
                                  dbOpt.CatgName == option.OptCatg
                                  && string.Compare(Helper.CapText(dbOpt.OptionName), Helper.CapText(option.OptItem), true) == 0))
                {

                    dbOption.Add(new PponOption
                    {
                        DealId = uniqueId,
                        BusinessHourGuid = bid,
                        CatgName = option.OptCatg,
                        OptionName = option.OptItem,
                        ItemNo = option.OptItemNo,
                        Quantity = option.OptQuantity,
                        CreateId = userName,
                        CreateTime = now
                    });
                }
            }
        }

        private static void ResequenceOptions(PponOptionCollection dbOption, List<dynamic> uiOptions)
        {
            int catgSeq = 1;
            int itemSeq = 1;

            IEnumerable<IGrouping<dynamic, dynamic>> optGroup = uiOptions.GroupBy(opt => opt.OptCatg);
            foreach (var catg in optGroup)
            {
                foreach (var item in catg)
                {
                    PponOption option = dbOption.First(dbOpt => dbOpt.CatgName == item.OptCatg
                                                                && string.Compare(Helper.CapText(dbOpt.OptionName), Helper.CapText(item.OptItem), true) == 0);
                    option.CatgSeq = catgSeq;
                    option.OptionSeq = itemSeq;

                    itemSeq++;
                }

                catgSeq++;
                itemSeq = 1;
            }
        }

        #endregion

        #region 業務系統

        /// <summary>
        /// 依據提案單的內容建立檔次資料(母檔)(舊提案單)
        /// </summary>
        /// <param name="loginUser">使用者</param>
        /// <param name="proposal">提案單</param>
        /// <param name="model">提案單中的優惠內容的主檔(主檔default為售價最低的)</param>
        /// <returns></returns>
        public static Guid PponDealCreateByProposal(string loginUser, Proposal proposal, ProposalMultiDeal model)
        {
            #region 參數

            #region 提案單property

            Guid sid = proposal.SellerGuid;
            DeliveryType type = (DeliveryType)proposal.DeliveryType;
            int proposalSpecialFlag = proposal.SpecialFlag;
            int deSalesId = proposal.DevelopeSalesId;
            int opSalesId = proposal.OperationSalesId ?? 0;
            string brandName = proposal.BrandName;
            string PicAlt = proposal.PicAlt;
            DealAccountingPayType payType = (DealAccountingPayType)proposal.PayType;
            ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(proposal.Id);
            ProposalStoreCollection psc = _pponProv.ProposalStoreGetListByProposalId(proposal.Id);
            ProposalCategoryDealCollection pcdc = _sellerProv.ProposalCategoryDealsGetList(proposal.Id);
            int vendor_receipt_type = proposal.VendorReceiptType == null ? (int)VendorReceiptType.Invoice : (int)proposal.VendorReceiptType;
            bool isInputTaxRequired = (vendor_receipt_type != (int)VendorReceiptType.NoTaxInvoice ? true : false);
            string message = proposal.Othermessage;

            #endregion

            #region 提案單的優惠內容(檔次)property

            string deal_content = string.IsNullOrEmpty(model.ItemNameTravel) ? model.ItemName : model.ItemNameTravel;
            decimal orig_price = model.OrigPrice;
            decimal item_price = model.ItemPrice;
            decimal cost = model.Cost;
            int slottingFeeQuantity = model.SlottingFeeQuantity;
            decimal orderTotoalLimit = model.OrderTotalLimit;
            decimal freights = model.Freights;
            decimal noFreightLimit = model.NoFreightLimit;
            int atmMaximum = model.AtmMaximum;
            string dealOptions = model.Options;
            int maxItemCount = model.OrderMaxPersonal;

            #endregion

            #endregion

            Guid bid = Guid.Empty;
            DateTime now = DateTime.Now; ;
            ViewEmployee deEmp = _humProv.ViewEmployeeGet(ViewEmployee.Columns.UserId, deSalesId);
            ViewEmployee opEmp = _humProv.ViewEmployeeGet(ViewEmployee.Columns.UserId, opSalesId);
            if (deEmp.IsLoaded)
            {
                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    #region business_hour
                    BusinessHour bh = new BusinessHour();
                    bh.BusinessHourTypeId = (int)BusinessHourType.Ppon;
                    bh.SellerGuid = sid;
                    bh.BusinessHourOrderTimeS = DateTime.MaxValue.Date;
                    bh.BusinessHourOrderTimeE = DateTime.MaxValue.Date;
                    bh.BusinessHourDeliverTimeS = DateTime.MaxValue.Date;
                    bh.BusinessHourDeliverTimeE = DateTime.MaxValue.Date;
                    bh.BusinessHourDeliveryCharge = 0;
                    bh.BusinessHourOrderMinimum = 1;
                    bh.BusinessHourPreparationTime = 0;
                    bh.BusinessHourOnline = true;
                    if (type == DeliveryType.ToHouse)
                    {
                        bh.BusinessHourStatus |= (int)BusinessHourStatus.NotDeliveryIslands;
                    }
                    else
                    {
                        bh.BusinessHourStatus |= (int)BusinessHourStatus.WeeklyPay;
                    }
                    bh.BusinessHourStatus |= (bool)proposal.Mohist ? Helper.SetBusinessHourTrustProvider(bh.BusinessHourStatus, TrustProvider.Sunny) : Helper.SetBusinessHourTrustProvider(bh.BusinessHourStatus, TrustProvider.TaiShin);
                    #region 依據ProposalSpecialFlag設置bh.BusinessHourStatus (折價券相關)

                    if (Helper.IsFlagSet(proposal.SpecialFlag, ProposalSpecialFlag.GroupCoupon))
                    {
                        bh.BusinessHourStatus |= (int)BusinessHourStatus.GroupCoupon;//成套販售
                    }

                    if (Helper.IsFlagSet(proposal.SpecialFlag, ProposalSpecialFlag.NoRestrictedStore))
                    {
                        bh.BusinessHourStatus |= (int)BusinessHourStatus.NoRestrictedStore;//通用券
                    }

                    #endregion

                    if (proposal.IsGame)
                    {
                        bh.BusinessHourStatus |= (int)BusinessHourStatus.NotShowDealDetailOnWeb;//活動遊戲檔
                    }

                    if (proposal.IsChannelGift)
                    {
                        bh.BusinessHourStatus |= (int)BusinessHourStatus.NotShowDealDetailOnWeb;//代銷贈品檔
                    }

                    bh.OrderTotalLimit = orderTotoalLimit;
                    bh.Holiday = 0;
                    bh.CreateId = loginUser;
                    bh.CreateTime = now;
                    bh.BusinessHourAtmMaximum = atmMaximum;
                    bh.PicAlt = PicAlt;


                    _sellerProv.BusinessHourSet(bh);
                    #endregion

                    #region deal_property
                    DealProperty dp = new DealProperty();
                    dp.BusinessHourGuid = bh.Guid;
                    dp.DeliveryType = (int)type;
                    dp.SellerVerifyType = (int)SellerVerifyType.VerificationOnline;
                    dp.DealEmpName = deEmp.EmpName;
                    dp.CreateId = loginUser;
                    dp.CreateTime = now;
                    dp.CityList = "[]";
                    dp.ComboPackCount = 1;
                    //dp.QuantityMultiplier = 1;
                    dp.NewDealType = proposal.DealSubType;
                    dp.EmpNo = deEmp.EmpNo;
                    dp.DevelopeSalesId = deEmp.UserId;
                    if (opEmp.IsLoaded)
                    {
                        dp.OperationSalesId = opEmp.UserId;
                    }

                    dp.IsTravelDeal = false;
                    dp.IsMergeCount = true;//多重選項合併統計（預設）
                    dp.IsGame = proposal.IsGame;
                    dp.IsChannelGift = proposal.IsChannelGift;
                    dp.IsTaishinChosen = proposal.IsTaishinChosen;
                    if (!string.IsNullOrEmpty(dealOptions))
                    {
                        //有填寫多重選項的時候，後台購物車要打開
                        dp.ShoppingCart = true;
                    }

                    if (type == DeliveryType.ToHouse)
                    {
                        //宅配
                        dp.ShipType = proposal.ShipType; //出貨類型
                        if (proposal.ShipType == (int)DealShipType.Normal)
                        {
                            //一般出貨
                            dp.ProductUseDateStartSet = string.IsNullOrEmpty(proposal.ShipText1) ? 0 : int.Parse(proposal.ShipText1);//上檔後幾個工作日
                            dp.ProductUseDateEndSet = string.IsNullOrEmpty(proposal.ShipText2) ? 0 : int.Parse(proposal.ShipText2);   //統一結檔後幾個工作日
                            dp.Shippingdate = string.IsNullOrEmpty(proposal.ShipText3) ? 0 : int.Parse(proposal.ShipText3);           //訂單成立後幾個工作日
                            dp.ShippingdateType = proposal.ShippingdateType;         //最早出貨日 or 訂單成立後
                        }
                        else if (proposal.ShipType == (int)DealShipType.Ship72Hrs)
                        {
                            //快速出貨(如果是Null，SetUp頁面會Error，所以塞0)
                            dp.ProductUseDateStartSet = 0;//上檔後幾個工作日
                            dp.ProductUseDateEndSet = 0;   //統一結檔後幾個工作日
                            dp.Shippingdate = 0;           //訂單成立後幾個工作日
                            dp.ShippingdateType = 0;         //最早出貨日 or 訂單成立後
                        }

                        if (config.IsConsignment)
                        {
                            //商品寄倉
                            dp.Consignment = proposal.Consignment;
                        }
                    }
                    else
                    {
                        if (multiDeals.Count == 1)
                        {
                            //成套商品券
                            //單檔次，多檔次之後會在PponFacade.PponDealUpdate處理
                            ProposalMultiDeal sub = multiDeals.FirstOrDefault();
                            if (sub != null)
                            {
                                dp.SaleMultipleBase = sub.GroupCouponBuy + sub.GroupCouponPresent;
                                dp.PresentQuantity = sub.GroupCouponPresent;
                            }
                        }
                        //todo:成套票券選項先設在母檔，之後再視情況調整至子檔
                        dp.GroupCouponDealType = proposal.GroupCouponDealType ?? 0;
                    }

                    if (dp.DeliveryType == (int)DeliveryType.ToHouse && item_price == cost)
                    {
                        dp.DenyInstallment = true;
                        dp.Installment3months = dp.Installment6months = dp.Installment12months = false;
                    }

                    dp.AgentChannels = proposal.AgentChannels;

                    #region 接續份數
                    dp.AncestorBusinessHourGuid = proposal.AncestorBusinessHourGuid;
                    dp.AncestorSequenceBusinessHourGuid = proposal.AncestorSequenceBusinessHourGuid;
                    #endregion

                    #region 展演檔次
                    dp.IsPromotionDeal = proposal.IsPromotionDeal;
                    dp.IsExhibitionDeal = proposal.IsExhibitionDeal;
                    if (dp.IsPromotionDeal)
                    {
                        //商品資訊中「展演類型檔次(預設鎖定功能)」有勾起，則系統於上檔後台自動勾起鎖定欄位
                        dp.IsReserveLock = true;
                    }
                    #endregion 展演檔次

                    _pponProv.DealPropertySet(dp);
                    #endregion

                    #region coupon_event_content
                    ProposalCouponEventContent pcec = _pponProv.ProposalCouponEventContentGetByPid(proposal.Id);
                    if (pcec != null)
                    {
                        CouponEventContent cec = new CouponEventContent();
                        cec.BusinessHourGuid = bh.Guid;
                        cec.Name = deal_content;
                        cec.Availability = "[]";
                        cec.CouponUsage = deal_content;
                        cec.Restrictions = pcec.Restrictions;
                        cec.ModifyTime = now;

                        _pponProv.CouponEventContentSet(cec);
                    }
                    else
                    {
                        CouponEventContent cec = new CouponEventContent();
                        cec.BusinessHourGuid = bh.Guid;
                        cec.Name = deal_content;
                        cec.Availability = "[]";
                        cec.CouponUsage = deal_content;
                        cec.ModifyTime = now;

                        _pponProv.CouponEventContentSet(cec);
                    }

                    #endregion

                    #region item
                    Item i = new Item();
                    i.MenuGuid = Guid.Empty;
                    i.ItemName = brandName;
                    i.ItemOrigPrice = orig_price;
                    i.ItemPrice = item_price;
                    i.ItemDefaultDailyAmount = maxItemCount;// type == DeliveryType.ToHouse ? 50 : 20;
                    i.Status = 0;
                    i.CreateId = loginUser;
                    i.CreateTime = now;
                    i.BusinessHourGuid = bh.Guid;
                    i.MaxItemCount = maxItemCount;// type == DeliveryType.ToHouse ? 50 : 20;
                    i.PdfItemName = brandName;

                    _itemProv.ItemSet(i);
                    #endregion

                    #region deal_accounting
                    Seller s = _sellerProv.SellerGet(sid);
                    DealAccounting da = new DealAccounting();
                    da.BusinessHourGuid = bh.Guid;
                    da.SalesId = deEmp.EmpName;
                    if (opEmp.IsLoaded)
                    {
                        da.OperationSalesId = opEmp.EmpName;
                    }

                    da.SalesCommission = 0.2;
                    da.SalesBonus = 0;
                    da.Status = 0;
                    da.AccCity = -1;
                    da.DeptId = opEmp.DeptId;
                    da.Flag = (int)AccountingFlag.Verifying;
                    da.Paytocompany = (int)payType;
                    da.VendorBillingModel = (bool)proposal.Mohist ? (int)VendorBillingModel.MohistSystem : (int)VendorBillingModel.BalanceSheetSystem;
                    da.VendorReceiptType = ((int)vendor_receipt_type == (int)VendorReceiptType.Invoice || (int)vendor_receipt_type == (int)VendorReceiptType.NoTaxInvoice) ? (int)VendorReceiptType.Invoice :
                                           (int)vendor_receipt_type;
                    da.Message = message;


                    //該檔次是否同意新合約
                    bool IsAgreeNewContractDeal = false;
                    if (type == DeliveryType.ToShop || (type == DeliveryType.ToHouse && proposal.DealType == (int)ProposalDealType.Travel))
                    {
                        IsAgreeNewContractDeal = SellerFacade.IsAgreeNewContractSeller(s.Guid, (int)DeliveryType.ToShop);
                    }
                    else
                    {
                        IsAgreeNewContractDeal = SellerFacade.IsAgreeNewContractSeller(s.Guid, (int)DeliveryType.ToHouse);
                    }

                    if (config.IsConsignment)
                    {
                        if (config.IsRemittanceFortnightly && IsAgreeNewContractDeal)
                        {
                            if (proposal.Consignment)
                            {
                                //若為寄倉，預設為每月付款
                                da.RemittanceType = (int)RemittanceType.Monthly;
                            }
                            else if ((bool)proposal.Mohist)
                            {
                                //為墨攻時，預設為其他付款方式(RemittanceType.other)
                                da.RemittanceType = (int)RemittanceType.Others;
                            }
                            else
                            {
                                if (proposal.DeliveryType == (int)DeliveryType.ToShop || (proposal.DeliveryType == (int)DeliveryType.ToHouse && proposal.DealType == (int)ProposalDealType.Travel))
                                {
                                    //依照商家的
                                    da.RemittanceType = (int)s.RemittanceType;
                                }
                                else
                                {
                                    if (proposal.ShipType == (int)DealShipType.Ship72Hrs)
                                    {
                                        //24HR為雙周結
                                        da.RemittanceType = (int)RemittanceType.Fortnightly;
                                    }
                                    else
                                    {
                                        //非24HR為月結
                                        da.RemittanceType = (int)RemittanceType.Monthly;
                                    }
                                }

                            }

                        }
                        else
                        {
                            if (proposal.Consignment)
                            {
                                //若為寄倉，預設為每月付款
                                da.RemittanceType = (int)RemittanceType.Monthly;
                            }
                            else if ((bool)proposal.Mohist)
                            {
                                //為墨攻時，預設為其他付款方式(RemittanceType.other)
                                da.RemittanceType = (int)RemittanceType.Others;
                            }
                            else if (s.RemittanceType != null)
                            {
                                //依照商家的
                                da.RemittanceType = (int)s.RemittanceType;
                            }
                            else
                            {
                                da.RemittanceType = type == DeliveryType.ToHouse ? (int)RemittanceType.ManualPartially : (int)RemittanceType.AchWeekly;
                            }
                        }
                    }
                    else
                    {
                        if (s.RemittanceType != null)
                        {
                            da.RemittanceType = (int)s.RemittanceType;
                        }
                        else if ((bool)proposal.Mohist) //為墨攻時，無條件初始化為其他付款方式(RemittanceType.other)
                        {
                            da.RemittanceType = (int)RemittanceType.Others;
                        }
                        else
                        {
                            da.RemittanceType = type == DeliveryType.ToHouse ? (int)RemittanceType.ManualPartially : (int)RemittanceType.AchWeekly;
                        }
                    }


                    da.IsInputTaxRequired = isInputTaxRequired;

                    _pponProv.DealAccountingSet(da);
                    #endregion

                    #region deal_cost

                    if (cost > 0)
                    {
                        bool multiflag = false;
                        if (multiDeals.Count > 1)
                        {
                            //多檔次
                            multiflag = true;
                        }
                        DealCostCollection dcc = new DealCostCollection();
                        int maxQty = Convert.ToInt32(orderTotoalLimit);
                        if (slottingFeeQuantity > 0)
                        {
                            //母檔進貨價應預設為1
                            //dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = 0, Quantity = slottingFeeQuantity, CumulativeQuantity = slottingFeeQuantity, LowerCumulativeQuantity = 0 });
                            //dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = cost, Quantity = maxQty - slottingFeeQuantity, CumulativeQuantity = maxQty, LowerCumulativeQuantity = slottingFeeQuantity });
                            dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = (multiflag ? 1 : cost), Quantity = slottingFeeQuantity, CumulativeQuantity = slottingFeeQuantity, LowerCumulativeQuantity = 0 });
                            dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = (multiflag ? 1 : cost), Quantity = maxQty - slottingFeeQuantity, CumulativeQuantity = maxQty, LowerCumulativeQuantity = slottingFeeQuantity });
                        }
                        else
                        {
                            //母檔進貨價應預設為1
                            dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = (multiflag ? 1 : cost), Quantity = maxQty, CumulativeQuantity = maxQty, LowerCumulativeQuantity = 0 });
                        }
                        _pponProv.DealCostSetList(dcc);
                    }

                    #endregion

                    #region coupon_freight

                    if (freights > 0)
                    {
                        CouponFreightCollection cfc = new CouponFreightCollection();
                        if (noFreightLimit > 0)
                        {
                            cfc.Add(new CouponFreight() { BusinessHourGuid = bh.Guid, StartAmount = item_price * noFreightLimit, EndAmount = 9999999, FreightAmount = 0, CreateId = loginUser, CreateTime = DateTime.Now });
                            //cfc.Add(new CouponFreight() { BusinessHourGuid = bh.Guid, StartAmount = noFreightLimit, EndAmount = 9999999, FreightAmount = 0, CreateId = loginUser, CreateTime = DateTime.Now });
                            cfc.Add(new CouponFreight() { BusinessHourGuid = bh.Guid, StartAmount = 0, EndAmount = noFreightLimit, FreightAmount = freights, CreateId = loginUser, CreateTime = DateTime.Now });
                        }
                        else
                        {
                            cfc.Add(new CouponFreight() { BusinessHourGuid = bh.Guid, StartAmount = 0, EndAmount = 9999999, FreightAmount = freights, CreateId = loginUser, CreateTime = DateTime.Now });
                        }
                        _pponProv.CouponFreightSetList(cfc);
                    }

                    #endregion

                    #region sms_content
                    SmsContent sms = new SmsContent();
                    sms.BusinessHourGuid = bh.Guid;
                    string smsContent = brandName;
                    if (smsContent.Length > 100)
                    {
                        smsContent = smsContent.Substring(0, 100);
                    }
                    sms.Content = "17Life:|" + smsContent + "|，{0}-{1}，期限{2}-{3}，App載憑證：http://x.co/4r1JG";
                    sms.CreateId = loginUser;
                    sms.CreateTime = now;
                    _pponProv.SMSContentSet(sms);
                    #endregion

                    #region pponstore
                    PponStoreCollection pss = new PponStoreCollection();
                    foreach (ProposalStore p in psc)
                    {
                        pss.Add(new PponStore()
                        {
                            BusinessHourGuid = bh.Guid,
                            StoreGuid = p.StoreGuid,
                            TotalQuantity = p.TotalQuantity,
                            SortOrder = p.SortOrder,
                            VbsRight = p.VbsRight,
                            ResourceGuid = Guid.NewGuid(),
                            CreateId = loginUser,
                            CreateTime = DateTime.Now
                        });
                    }
                    _pponProv.PponStoreSetList(pss);
                    #endregion

                    #region DealPayment
                    DealPayment dealPayment = new DealPayment();
                    dealPayment.BusinessHourGuid = bh.Guid;
                    dealPayment.BankId = 0;
                    dealPayment.IsBankDeal = proposal.IsBankDeal;
                    _pponProv.DealPaymentSet(dealPayment);
                    #endregion

                    #region CategoryDeal
                    CategoryDealCollection cdc = new CategoryDealCollection();
                    foreach (ProposalCategoryDeal p in pcdc)
                    {
                        cdc.Add(new CategoryDeal()
                        {
                            Bid = bh.Guid,
                            Cid = p.Cid
                        });
                    }
                    if (proposal.DeliveryType == (int)DeliveryType.ToShop)
                    {
                        //即買即用
                        if (Helper.IsFlagSet(proposal.SpecialFlag, ProposalSpecialFlag.OnlineUse))
                        {
                            CategoryCollection category = _sellerProv.CategoryGetAll(Category.Columns.Code);
                            Category cc = category.Where(x => x.Name == "即買即用" && x.Type == (int)CategoryType.DealSpecialCategory).FirstOrDefault();

                            if (cc != null)
                            {
                                if (cdc.Where(t => t.Cid == cc.Id).FirstOrDefault() == null)
                                {
                                    cdc.Add(new CategoryDeal()
                                    {
                                        Bid = bh.Guid,
                                        Cid = cc.Id
                                    });
                                }
                            }
                        }
                    }
                    PponFacade.SaveCategoryDeals(cdc, bh.Guid);
                    #endregion

                    scope.Complete();

                    bid = bh.Guid;
                }

                OrderFacade.MakeGroupOrder(config.ServiceEmail, config.ServiceName, bid, now, DateTime.MaxValue.Date);

                #region 更新展演/展覽型票券產生到後台
                if (proposal.IsPromotionDeal || proposal.IsExhibitionDeal)
                {
                    GroupOrder gOrder = _orderProv.GroupOrderGetByBid(bid);
                    if (gOrder.IsLoaded)
                    {
                        //演出時間前十日內不能退貨
                        gOrder.Status = (int)Helper.SetFlag(proposal.IsPromotionDeal, gOrder.Status ?? 0, GroupOrderStatus.NoRefundBeforeDays);
                        //過期不能退貨
                        gOrder.Status = (int)Helper.SetFlag(proposal.IsExhibitionDeal, gOrder.Status ?? 0, GroupOrderStatus.ExpireNoRefund);

                        _orderProv.GroupOrderSet(gOrder);
                    }
                }
                #endregion 更新展演/展覽型票券產生到後台
            }

            return bid;
        }



        /// <summary>
        /// 依據新版宅配提案單的內容建立檔次資料(母檔)(新版宅配)
        /// </summary>
        /// <param name="loginUser"></param>
        /// <param name="proposal"></param>
        /// <param name="model"></param>
        /// <param name="models"></param>
        /// <returns></returns>
        public static Guid PponDealCreateByHouseProposal(string loginUser, Proposal proposal
            , ProposalMultiDeal model
            , List<ProposalMultiDeal> models)
        {
            #region 參數

            #region mark
            //declare @bid varchar(100)
            //set @bid = '2e2f6ec8-b7b1-47d5-ba96-9a588771dcb7'
            //select* from Business_Hour where guid = @bid
            //select* from Group_Order where business_hour_guid = @bid
            //select* from deal_property where business_hour_guid = @bid
            //select* from coupon_event_content where business_hour_guid = @bid
            //select* from item where business_hour_guid = @bid
            //select* from deal_accounting where business_hour_guid = @bid
            //select* from Deal_Cost where business_hour_guid = @bid
            //select* from Sms_Content where business_hour_guid = @bid
            //select* from Ppon_Store where business_hour_guid = @bid
            //select* from Deal_Payment where business_hour_guid = @bid
            //select* from Category_Deals where bid = @bid
            #endregion mark

            #region 提案單property

            Guid sid = proposal.SellerGuid;
            DeliveryType type = (DeliveryType)proposal.DeliveryType;
            int proposalSpecialFlag = proposal.SpecialFlag;
            int deSalesId = proposal.DevelopeSalesId;
            int opSalesId = proposal.OperationSalesId ?? 0;
            string brandName = proposal.BrandName;
            string PicAlt = proposal.PicAlt;
            DealAccountingPayType payType = (DealAccountingPayType)proposal.PayType;
            ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(proposal.Id);
            ProposalStoreCollection psc = _pponProv.ProposalStoreGetListByProposalId(proposal.Id);
            ProposalCategoryDealCollection pcdc = _sellerProv.ProposalCategoryDealsGetList(proposal.Id);
            int vendor_receipt_type = proposal.VendorReceiptType == null ? (int)VendorReceiptType.Invoice : (int)proposal.VendorReceiptType;
            bool isInputTaxRequired = (vendor_receipt_type != (int)VendorReceiptType.NoTaxInvoice ? true : false);
            string message = proposal.Othermessage;
            bool delivery_islands = proposal.DeliveryIslands;
            int remittance_type = proposal.RemittanceType;
            bool isWms = proposal.IsWms;

            #endregion

            #region 提案單的優惠內容(檔次)property
            string deal_content = (string.IsNullOrEmpty(proposal.BrandName) ? string.Empty : "【" + proposal.BrandName + "】") + proposal.DealName;//商品名稱(品牌名稱+檔次名稱)
            decimal orig_price = model.OrigPrice;
            decimal item_price = model.ItemPrice;
            decimal cost = model.Cost;
            int slottingFeeQuantity = model.SlottingFeeQuantity;
            decimal orderTotoalLimit = model.OrderTotalLimit;
            decimal freights = model.Freights;
            decimal noFreightLimit = model.NoFreightLimit;
            int atmMaximum = model.AtmMaximum;
            string dealOptions = model.Options;
            int maxItemCount = model.OrderMaxPersonal;

            #endregion

            #endregion

            Guid bid = Guid.Empty;
            DateTime now = DateTime.Now; ;
            ViewEmployee deEmp = _humProv.ViewEmployeeGet(ViewEmployee.Columns.UserId, deSalesId);
            ViewEmployee opEmp = _humProv.ViewEmployeeGet(ViewEmployee.Columns.UserId, opSalesId);
            if (deEmp.IsLoaded)
            {
                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    #region business_hour
                    BusinessHour bh = new BusinessHour();
                    if (proposal.BusinessHourGuid != null)
                    {
                        bh.Guid = proposal.BusinessHourGuid.Value;
                    }
                    bh.BusinessHourTypeId = (int)BusinessHourType.Ppon;
                    bh.SellerGuid = sid;
                    bh.BusinessHourOrderTimeS = DateTime.MaxValue.Date;
                    bh.BusinessHourOrderTimeE = DateTime.MaxValue.Date;
                    bh.BusinessHourDeliverTimeS = DateTime.MaxValue.Date;
                    bh.BusinessHourDeliverTimeE = DateTime.MaxValue.Date;
                    bh.BusinessHourDeliveryCharge = 0;
                    bh.BusinessHourOrderMinimum = 1;
                    bh.BusinessHourPreparationTime = 0;
                    bh.BusinessHourOnline = true;
                    if (type == DeliveryType.ToHouse)
                    {
                        //bh.BusinessHourStatus |= (int)BusinessHourStatus.NotDeliveryIslands;
                        if (!delivery_islands)
                        {
                            bh.BusinessHourStatus |= (int)BusinessHourStatus.NotDeliveryIslands;
                        }
                    }
                    else
                    {
                        bh.BusinessHourStatus |= (int)BusinessHourStatus.WeeklyPay;
                    }
                    bh.BusinessHourStatus |= (bool)proposal.Mohist.GetValueOrDefault(false) ? Helper.SetBusinessHourTrustProvider(bh.BusinessHourStatus, TrustProvider.Sunny) : Helper.SetBusinessHourTrustProvider(bh.BusinessHourStatus, TrustProvider.TaiShin);

                    #region 依據ProposalSpecialFlag設置bh.BusinessHourStatus (折價券相關)

                    if (Helper.IsFlagSet(proposal.SpecialFlag, ProposalSpecialFlag.GroupCoupon))
                    {
                        bh.BusinessHourStatus |= (int)BusinessHourStatus.GroupCoupon;//成套販售
                    }

                    if (Helper.IsFlagSet(proposal.SpecialFlag, ProposalSpecialFlag.NoRestrictedStore))
                    {
                        bh.BusinessHourStatus |= (int)BusinessHourStatus.NoRestrictedStore;//通用券
                    }

                    if (proposal.IsGame)
                    {
                        bh.BusinessHourStatus |= (int)BusinessHourStatus.NotShowDealDetailOnWeb;//活動遊戲檔
                    }

                    #endregion

                    bh.OrderTotalLimit = orderTotoalLimit;
                    bh.Holiday = 0;
                    bh.CreateId = loginUser;
                    bh.CreateTime = now;
                    bh.BusinessHourAtmMaximum = atmMaximum;
                    bh.PicAlt = PicAlt;


                    _sellerProv.BusinessHourSet(bh);
                    #endregion

                    #region CategoryDeal
                    CategoryDealCollection cdc = new CategoryDealCollection();
                    foreach (ProposalCategoryDeal p in pcdc)
                    {
                        cdc.Add(new CategoryDeal()
                        {
                            Bid = bh.Guid,
                            Cid = p.Cid
                        });
                    }

                    #region 超取
                    if (!string.IsNullOrEmpty(model.BoxQuantityLimit))
                    {
                        int boxQuantityLimit = 0;
                        int.TryParse(model.BoxQuantityLimit, out boxQuantityLimit);
                        CategoryCollection ccs = _sellerProv.CategoryGetAll(Category.Columns.Code);
                        Category cc = ccs.Where(x => x.Name.Equals("超商取貨") && x.Type == (int)CategoryType.DealCategory).FirstOrDefault();

                        if (boxQuantityLimit > 0)
                        {
                            if (cc != null)
                            {
                                if (cdc.Where(t => t.Cid == cc.Id).FirstOrDefault() == null)
                                {
                                    cdc.Add(new CategoryDeal()
                                    {
                                        Bid = bh.Guid,
                                        Cid = cc.Id
                                    });
                                }
                            }
                        }
                        else if (boxQuantityLimit == 0)
                        {
                            if (cc != null)
                            {
                                CategoryDealCollection tmp = cdc.Clone();
                                cdc.Clear();
                                foreach (CategoryDeal c in tmp)
                                {
                                    if (c.Cid != cc.Id)
                                    {
                                        cdc.Add(c);
                                    }
                                }

                                //var c = cdc.Where(t => t.Cid == cc.Id).FirstOrDefault();
                                //if (c != null)
                                //{
                                //    cdc.Remove(c);
                                //}
                            }
                        }
                    }
                    #endregion 超取
                    PponFacade.SaveCategoryDeals(cdc, bh.Guid);


                    #endregion

                    #region deal_property
                    DealProperty dp = new DealProperty();
                    dp.BusinessHourGuid = bh.Guid;
                    dp.DeliveryType = (int)type;
                    dp.SellerVerifyType = (int)SellerVerifyType.VerificationOnline;
                    dp.DealEmpName = deEmp.EmpName;
                    dp.CreateId = loginUser;
                    dp.CreateTime = now;
                    dp.CityList = "[]";
                    dp.ComboPackCount = 1;
                    int quantityMultiplier = default(int);
                    int.TryParse(model.QuantityMultiplier, out quantityMultiplier);
                    if (quantityMultiplier == 0)
                    {
                        quantityMultiplier = 1;
                    }
                    dp.QuantityMultiplier = quantityMultiplier;
                    dp.NewDealType = proposal.DealSubType;
                    dp.DealType = proposal.DealType2;
                    dp.DealTypeDetail = proposal.DealTypeDetail;
                    dp.EmpNo = deEmp.EmpNo;
                    dp.DevelopeSalesId = deEmp.UserId;
                    if (opEmp.IsLoaded)
                    {
                        dp.OperationSalesId = opEmp.UserId;
                    }

                    dp.IsTravelDeal = false;
                    dp.IsMergeCount = true;//多重選項合併統計（預設）
                    dp.IsHouseNewVer = true;//提案單新版本
                    dp.IsAveragePrice = true;   //均價
                    dp.IsQuantityMultiplier = true;//銷售倍數
                    dp.DealAccBusinessGroupId = 7;//館別預設塞宅配
                    bool includedDeliveryCharge = true;
                    foreach (var m in models)
                    {
                        if (m.Freights > 0)
                        {
                            includedDeliveryCharge = false;
                            break;
                        }
                    }
                    //免運
                    if (includedDeliveryCharge)
                    {
                        List<int> tagList = new List<int>();
                        if (dp.LabelTagList != null)
                        {
                            string[] labelTagList = dp.LabelTagList.Split(",");
                            foreach (string tag in labelTagList)
                            {
                                int t = 0;
                                int.TryParse(tag, out t);
                                if (!tagList.Contains(t))
                                {
                                    tagList.Add(t);
                                }
                            }
                        }
                        if (!tagList.Contains((int)DealLabelSystemCode.IncludedDeliveryCharge))
                        {
                            tagList.Add((int)DealLabelSystemCode.IncludedDeliveryCharge);
                        }
                        dp.LabelTagList = string.Join(",", tagList);
                    }

                    if (!string.IsNullOrEmpty(dealOptions))
                    {
                        #region 判斷是否為多重選項
                        bool isMulti = ProposalFacade.IsProposalMultiSpec(model.Options);
                        //建檔時先不要產生，因為還沒有unique_id
                        //if (isMulti)
                        //{
                        //    ProposalFacade.UpdateMultiSpecToPponOption(model.Options, bh.Guid, loginUser);
                        //}
                        //dp.ShoppingCart = isMulti;
                        #endregion 判斷是否為多重選項

                        dp.ShoppingCart = isMulti;
                    }

                    if (type == DeliveryType.ToHouse)
                    {
                        //宅配
                        dp.ShipType = proposal.ShipType; //出貨類型
                        if (proposal.ShipType == (int)DealShipType.Normal)
                        {
                            //一般出貨
                            dp.ProductUseDateStartSet = string.IsNullOrEmpty(proposal.ShipText1) ? 0 : int.Parse(proposal.ShipText1);//上檔後幾個工作日
                            dp.ProductUseDateEndSet = string.IsNullOrEmpty(proposal.ShipText2) ? 0 : int.Parse(proposal.ShipText2);   //統一結檔後幾個工作日
                            dp.Shippingdate = string.IsNullOrEmpty(proposal.ShipText3) ? 0 : int.Parse(proposal.ShipText3);           //訂單成立後幾個工作日
                            dp.ShippingdateType = proposal.ShippingdateType;         //最早出貨日 or 訂單成立後
                        }
                        else if (proposal.ShipType == (int)DealShipType.Ship72Hrs)
                        {
                            //快速出貨(如果是Null，SetUp頁面會Error，所以塞0)
                            dp.ProductUseDateStartSet = 0;//上檔後幾個工作日
                            dp.ProductUseDateEndSet = 0;   //統一結檔後幾個工作日
                            dp.Shippingdate = 0;           //訂單成立後幾個工作日
                            dp.ShippingdateType = 0;         //最早出貨日 or 訂單成立後
                        }

                        dp.Consignment = proposal.Consignment;
                    }
                    else
                    {
                        if (multiDeals.Count == 1)
                        {
                            //成套商品券
                            //單檔次，多檔次之後會在PponFacade.PponDealUpdate處理
                            ProposalMultiDeal sub = multiDeals.FirstOrDefault();
                            if (sub != null)
                            {
                                dp.SaleMultipleBase = sub.GroupCouponBuy + sub.GroupCouponPresent;
                                dp.PresentQuantity = sub.GroupCouponPresent;
                            }
                        }
                        //todo:成套票券選項先設在母檔，之後再視情況調整至子檔
                        dp.GroupCouponDealType = proposal.GroupCouponDealType ?? 0;
                    }

                    if (dp.DeliveryType == (int)DeliveryType.ToHouse && item_price == cost)
                    {
                        dp.DenyInstallment = true;
                        dp.Installment3months = dp.Installment6months = dp.Installment12months = false;
                    }
                    dp.IsWms = isWms;

                    #region 接續份數
                    dp.AncestorBusinessHourGuid = proposal.AncestorBusinessHourGuid;
                    dp.AncestorSequenceBusinessHourGuid = proposal.AncestorSequenceBusinessHourGuid;
                    #endregion

                    #region 展演檔次
                    dp.IsPromotionDeal = proposal.IsPromotionDeal;
                    dp.IsExhibitionDeal = proposal.IsExhibitionDeal;
                    if (dp.IsPromotionDeal)
                    {
                        //商品資訊中「展演類型檔次(預設鎖定功能)」有勾起，則系統於上檔後台自動勾起鎖定欄位
                        dp.IsReserveLock = true;
                    }
                    #endregion 展演檔次

                    #region ICON
                    int[] specialCategoryIds = CategoryManager.CategoryGetListByType(CategoryType.DealSpecialCategory).Select(x => x.Id).ToArray();
                    List<int> selectedSpecialCategoryIds = cdc.Where(x => x.Cid.EqualsAny(specialCategoryIds)).Select(x => x.Cid).ToList();
                    List<int> iconIdList = new List<int>();
                    foreach (int categoryId in selectedSpecialCategoryIds)
                    {
                        DealLabelSystemCode? code = CategoryManager.GetDealLabelSystemCodeByCategoryId(categoryId);
                        if (code.HasValue)
                        {
                            iconIdList.Add((int)code.Value);
                        }
                    }
                    if (isWms)
                        iconIdList.Add((int)DealLabelSystemCode.TwentyFourHoursArrival);

                    dp.LabelIconList = string.Join(",", iconIdList);



                    List<int> dealLabelTags = new List<int>();
                    if (!proposal.DeliveryIslands)
                    {
                        dealLabelTags.Add((int)DealLabelSystemCode.NAOutlyingIslands);
                    }
                    dp.LabelTagList = string.Join(",", dealLabelTags);
                    #endregion



                    _pponProv.DealPropertySet(dp);
                    #endregion

                    #region coupon_event_content
                    ProposalCouponEventContent pcec = _pponProv.ProposalCouponEventContentGetByPid(proposal.Id);
                    string eventName = ProposalFacade.GetProEventName(proposal, models);
                    if (pcec != null)
                    {
                        CouponEventContent cec = new CouponEventContent();
                        cec.BusinessHourGuid = bh.Guid;
                        if (eventName.Length > 400)
                        {
                            eventName = eventName.Substring(0, 390);
                        }
                        cec.Name = eventName;//黑標
                        cec.Title = pcec.Title;//橘標
                        cec.Availability = "[]";
                        cec.CouponUsage = deal_content;
                        cec.Restrictions = pcec.Restrictions;
                        cec.Introduction = pcec.Restrictions;
                        cec.Description = pcec.Description;
                        cec.ProductSpec = pcec.ProductSpec;
                        cec.ImagePath = pcec.ImagePath;
                        cec.AppDealPic = pcec.AppDealPic;
                        cec.AppDescription = ViewPponDealManager.RemoveSpecialHtmlForApp(pcec.Description + "<p />商品規格：<p />" + pcec.ProductSpec);
                        cec.Remark = (0 + "||" + string.Empty + "||" + HttpUtility.HtmlDecode(pcec.Remark) + "||右邊##||##||##||##||##||##");
                        cec.ModifyTime = now;

                        _pponProv.CouponEventContentSet(cec);
                    }
                    else
                    {
                        CouponEventContent cec = new CouponEventContent();
                        cec.BusinessHourGuid = bh.Guid;
                        cec.Name = eventName;
                        cec.Title = pcec.Title;
                        cec.Availability = "[]";
                        cec.CouponUsage = deal_content;
                        cec.ImagePath = pcec.ImagePath;
                        cec.AppDealPic = pcec.AppDealPic;
                        cec.ModifyTime = now;

                        _pponProv.CouponEventContentSet(cec);
                    }

                    #endregion

                    #region item
                    decimal min_oriPrice = 0;
                    decimal min_itemPrice = 0;
                    decimal avg = 99999;
                    if (models.Count > 1)
                    {
                        foreach (var m in models)
                        {
                            decimal d = Math.Ceiling(m.ItemPrice / decimal.Parse(m.QuantityMultiplier));
                            if (d < avg)
                            {
                                avg = d;
                                min_oriPrice = Math.Ceiling(m.OrigPrice / decimal.Parse(m.QuantityMultiplier));
                                min_itemPrice = Math.Ceiling(m.ItemPrice / decimal.Parse(m.QuantityMultiplier));
                            }
                        }
                    }
                    else
                    {
                        min_oriPrice = orig_price;
                        min_itemPrice = item_price;
                    }
                    Item i = new Item();
                    i.MenuGuid = Guid.Empty;
                    i.ItemName = deal_content;
                    i.ItemOrigPrice = min_oriPrice;
                    i.ItemPrice = min_itemPrice;
                    i.ItemDefaultDailyAmount = maxItemCount;// type == DeliveryType.ToHouse ? 50 : 20;
                    i.Status = 0;
                    i.CreateId = loginUser;
                    i.CreateTime = now;
                    i.BusinessHourGuid = bh.Guid;
                    i.MaxItemCount = maxItemCount;// type == DeliveryType.ToHouse ? 50 : 20;
                    i.PdfItemName = deal_content;

                    _itemProv.ItemSet(i);
                    #endregion

                    #region deal_accounting
                    Seller s = _sellerProv.SellerGet(sid);
                    DealAccounting da = new DealAccounting();
                    da.BusinessHourGuid = bh.Guid;
                    da.SalesId = deEmp.EmpName;
                    if (opEmp.IsLoaded)
                    {
                        da.OperationSalesId = opEmp.EmpName;
                    }

                    da.SalesCommission = 0.2;
                    da.SalesBonus = 0;
                    da.Status = 0;
                    da.AccCity = -1;
                    da.DeptId = opEmp.DeptId;
                    da.Flag = (int)AccountingFlag.Verifying;
                    da.Paytocompany = (int)payType;
                    da.VendorBillingModel = (bool)proposal.Mohist ? (int)VendorBillingModel.MohistSystem : (int)VendorBillingModel.BalanceSheetSystem;
                    da.VendorReceiptType = ((int)vendor_receipt_type == (int)VendorReceiptType.Invoice || (int)vendor_receipt_type == (int)VendorReceiptType.NoTaxInvoice) ? (int)VendorReceiptType.Invoice :
                                           (int)vendor_receipt_type;
                    da.Message = message;
                    da.RemittanceType = remittance_type;
                    da.IsInputTaxRequired = isInputTaxRequired;

                    _pponProv.DealAccountingSet(da);
                    #endregion

                    #region deal_cost

                    if (cost > 0)
                    {
                        bool multiflag = false;
                        if (multiDeals.Count > 1)
                        {
                            //多檔次
                            multiflag = true;
                        }
                        DealCostCollection dcc = new DealCostCollection();
                        int maxQty = Convert.ToInt32(orderTotoalLimit);
                        if (slottingFeeQuantity > 0)
                        {
                            //母檔進貨價應預設為1
                            //dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = 0, Quantity = slottingFeeQuantity, CumulativeQuantity = slottingFeeQuantity, LowerCumulativeQuantity = 0 });
                            //dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = cost, Quantity = maxQty - slottingFeeQuantity, CumulativeQuantity = maxQty, LowerCumulativeQuantity = slottingFeeQuantity });
                            dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = (multiflag ? 1 : cost), Quantity = slottingFeeQuantity, CumulativeQuantity = slottingFeeQuantity, LowerCumulativeQuantity = 0 });
                            dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = (multiflag ? 1 : cost), Quantity = maxQty - slottingFeeQuantity, CumulativeQuantity = maxQty, LowerCumulativeQuantity = slottingFeeQuantity });
                        }
                        else
                        {
                            //母檔進貨價應預設為1
                            dcc.Add(new DealCost() { BusinessHourGuid = bh.Guid, Cost = (multiflag ? 1 : cost), Quantity = maxQty, CumulativeQuantity = maxQty, LowerCumulativeQuantity = 0 });
                        }
                        _pponProv.DealCostSetList(dcc);
                    }

                    #endregion

                    #region coupon_freight

                    if (freights > 0)
                    {
                        CouponFreightCollection cfc = new CouponFreightCollection();
                        if (noFreightLimit > 0)
                        {
                            cfc.Add(new CouponFreight() { BusinessHourGuid = bh.Guid, StartAmount = item_price * noFreightLimit, EndAmount = 9999999, FreightAmount = 0, CreateId = loginUser, CreateTime = DateTime.Now });
                            //cfc.Add(new CouponFreight() { BusinessHourGuid = bh.Guid, StartAmount = noFreightLimit, EndAmount = 9999999, FreightAmount = 0, CreateId = loginUser, CreateTime = DateTime.Now });
                            cfc.Add(new CouponFreight() { BusinessHourGuid = bh.Guid, StartAmount = 0, EndAmount = item_price * noFreightLimit, FreightAmount = freights, CreateId = loginUser, CreateTime = DateTime.Now });
                        }
                        else
                        {
                            cfc.Add(new CouponFreight() { BusinessHourGuid = bh.Guid, StartAmount = 0, EndAmount = 9999999, FreightAmount = freights, CreateId = loginUser, CreateTime = DateTime.Now });
                        }
                        _pponProv.CouponFreightSetList(cfc);
                    }

                    #endregion

                    #region sms_content
                    SmsContent sms = new SmsContent();
                    sms.BusinessHourGuid = bh.Guid;
                    string smsContent = proposal.BrandName + proposal.DealName;//brandName;
                    if (smsContent.Length > 100)
                    {
                        smsContent = smsContent.Substring(0, 100);
                    }
                    sms.Content = "17Life:|" + smsContent + "|，{0}-{1}，期限{2}-{3}，App載憑證：http://x.co/4r1JG";
                    sms.CreateId = loginUser;
                    sms.CreateTime = now;
                    _pponProv.SMSContentSet(sms);
                    #endregion

                    #region pponstore
                    PponStoreCollection pss = new PponStoreCollection();
                    foreach (ProposalStore p in psc)
                    {
                        pss.Add(new PponStore()
                        {
                            BusinessHourGuid = bh.Guid,
                            StoreGuid = p.StoreGuid,
                            TotalQuantity = p.TotalQuantity,
                            SortOrder = p.SortOrder,
                            VbsRight = p.VbsRight,
                            ResourceGuid = Guid.NewGuid(),
                            CreateId = loginUser,
                            CreateTime = DateTime.Now
                        });
                    }
                    _pponProv.PponStoreSetList(pss);
                    #endregion

                    #region DealPayment
                    DealPayment dealPayment = new DealPayment();
                    dealPayment.BusinessHourGuid = bh.Guid;
                    dealPayment.BankId = 0;
                    dealPayment.IsBankDeal = proposal.IsBankDeal;
                    _pponProv.DealPaymentSet(dealPayment);
                    #endregion

                    #region week_pay_account
                    var insStores = new PponStoreCollection();
                    insStores = _pponProv.PponStoreGetListByBusinessHourGuid(bh.Guid);
                    var accounts = _orderProv.WeeklyPayAccountGetList(bh.Guid);
                    var stores = _sellerProv.SellerGetList(insStores.Where(t => Helper.IsFlagSet(t.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid));
                    foreach (var store in stores)
                    {
                        var account = accounts.FirstOrDefault(x => x.StoreGuid.HasValue && x.StoreGuid.Value == store.Guid)
                                    ?? new WeeklyPayAccount
                                    {
                                        BusinessHourGuid = bh.Guid,
                                        CreateDate = now,
                                        Creator = loginUser
                                    };
                        account.StoreGuid = store.Guid;
                        account.EventName = (i.ItemName.Length > 100)
                                                ? i.ItemName.Substring(0, 100) + "..."
                                                : i.ItemName;
                        account.CompanyName = store.CompanyName;
                        account.SellerName = store.CompanyBossName;
                        account.AccountId = store.CompanyID;
                        account.AccountName = store.CompanyAccountName;
                        account.AccountNo = store.CompanyAccount;
                        account.Email = store.CompanyEmail;
                        account.BankNo = store.CompanyBankCode;
                        account.BranchNo = store.CompanyBranchCode;
                        account.Message = store.CompanyNotice;
                        account.SignCompanyID = store.SignCompanyID;

                        accounts.Add(account);
                    }
                    _orderProv.WeeklyPayAccountSetList(accounts);

                    #endregion week_pay_account

                    #region deal_product_delivery
                    //單檔才寫入資料，母子檔另外產生子檔的資料
                    int subBoxQuantityLimit = 0;
                    int.TryParse(model.QuantityMultiplier, out subBoxQuantityLimit);

                    SetDealProductDelivery(proposal.SellerGuid, bh.Guid, subBoxQuantityLimit);
                    #endregion deal_product_delivery

                    scope.Complete();

                    bid = bh.Guid;
                }
                OrderFacade.MakeGroupOrder(config.ServiceEmail, config.ServiceName, bid, now, DateTime.MaxValue.Date);
            }

            return bid;
        }

        /// <summary>
        /// 依據提案單的內容更新檔次資料(子檔)
        /// </summary>
        /// <param name="loginUser">使用者</param>
        /// <param name="proposal">提案單</param>
        /// <param name="model">提案單中的優惠內容的主檔(主檔default為售價最低的)</param>
        public static void PponDealUpdateByProposal(Guid bid, string username, Proposal proposal, ProposalMultiDeal model)
        {
            #region 參數

            #region 提案單property

            string brandName = proposal.BrandName;
            int proposalSpecialFlag = proposal.SpecialFlag;

            #endregion

            #region 提案單的優惠內容(檔次)property

            string itemName = model.ItemName.Replace("\n", "");
            string itemDescription = model.ItemNameTravel;
            decimal origPrice = model.OrigPrice;
            decimal itemPrice = model.ItemPrice;
            decimal cost = model.Cost;
            int slottingFeeQuantity = model.SlottingFeeQuantity;
            decimal orderTotalLimit = model.OrderTotalLimit;
            int atmMaximum = model.AtmMaximum;
            decimal freight = model.Freights;
            decimal noFreightLimit = model.NoFreightLimit;
            int sale_multiple_base = model.GroupCouponBuy;
            int present_quantity = model.GroupCouponPresent;
            string options = model.Options;
            int orderMaxPersonal = model.OrderMaxPersonal;

            #endregion

            #endregion

            // 優惠內容
            CouponEventContent cec = _pponProv.CouponEventContentGetByBid(bid);
            cec.Name = itemDescription;
            cec.CouponUsage = itemName;
            _pponProv.CouponEventContentSet(cec);

            ComboDeal cd = _pponProv.GetComboDeal(bid);
            cd.Title = itemName;
            _pponProv.AddComboDeal(cd);

            // 優惠內容、原價、售價、每人限購
            Item i = _itemProv.ItemGetByBid(bid);
            i.ItemName = itemName;
            i.ItemOrigPrice = origPrice;
            i.ItemPrice = itemPrice;
            i.MaxItemCount = Convert.ToInt32(orderMaxPersonal);
            i.ItemDefaultDailyAmount = Convert.ToInt32(orderMaxPersonal);
            _itemProv.ItemSet(i);

            // 進貨價、上架費
            DealCostCollection dcc = new DealCostCollection();
            _pponProv.DealCostDeleteListByBid(bid);
            if (cost > 0)
            {
                int maxCount = Convert.ToInt32(orderTotalLimit);
                if (slottingFeeQuantity > 0)
                {
                    dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = 0, Quantity = slottingFeeQuantity, CumulativeQuantity = slottingFeeQuantity, LowerCumulativeQuantity = 0 });
                    dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = cost, Quantity = maxCount - slottingFeeQuantity, CumulativeQuantity = maxCount, LowerCumulativeQuantity = slottingFeeQuantity });
                }
                else
                {
                    dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = cost, Quantity = maxCount, CumulativeQuantity = maxCount, LowerCumulativeQuantity = 0 });
                }
            }
            _pponProv.DealCostSetList(dcc);

            // 最大購買數量、ATM
            BusinessHour bh = _sellerProv.BusinessHourGet(bid);
            bh.OrderTotalLimit = orderTotalLimit;
            bh.BusinessHourAtmMaximum = atmMaximum;

            _sellerProv.BusinessHourSet(bh);

            //成套販售
            DealProperty dp = _pponProv.DealPropertyGet(bid);
            dp.SaleMultipleBase = sale_multiple_base + present_quantity;
            dp.PresentQuantity = present_quantity;
            dp.GroupCouponDealType = proposal.GroupCouponDealType ?? 0;
            if (!string.IsNullOrEmpty(options))
            {
                dp.ShoppingCart = true;//有填寫多重選項的時候，要勾選購物車
            }

            if (config.IsConsignment)
            {
                dp.Consignment = proposal.Consignment;
            }
            _pponProv.DealPropertySet(dp);

            // 運費、免運門檻
            CouponFreightCollection cfc = new CouponFreightCollection();
            _pponProv.CouponFreightDeleteListByBid(bid);
            if (freight > 0)
            {
                if (noFreightLimit > 0)
                {
                    cfc.Add(new CouponFreight() { BusinessHourGuid = bh.Guid, StartAmount = itemPrice * noFreightLimit, EndAmount = 9999999, FreightAmount = 0, CreateId = username, CreateTime = DateTime.Now });
                    cfc.Add(new CouponFreight() { BusinessHourGuid = bid, StartAmount = 0, EndAmount = noFreightLimit, FreightAmount = freight, CreateId = username, CreateTime = DateTime.Now });
                }
                else
                {
                    cfc.Add(new CouponFreight() { BusinessHourGuid = bid, StartAmount = 0, EndAmount = 9999999, FreightAmount = freight, CreateId = username, CreateTime = DateTime.Now });
                }
            }
            _pponProv.CouponFreightSetList(cfc);

            #region 成套票券要存入前置文(母檔短標) sms_content
            if (Helper.IsFlagSet(proposal.SpecialFlag, ProposalSpecialFlag.GroupCoupon))
            {
                SmsContent sms = _pponProv.SMSContentGet(bid);
                string smsContent = brandName;
                if (smsContent.Length > 100)
                {
                    smsContent = smsContent.Substring(0, 100);
                }
                sms.Content = "17Life:|" + smsContent + "|，{0}-{1}，期限{2}-{3}，App載憑證：http://x.co/4r1JG";
                _pponProv.SMSContentSet(sms);
            }
            #endregion


            #region deal_product_delivery
            //單檔才寫入資料，母子檔另外產生子檔的資料
            int boxQuantityLimit = 0;
            int.TryParse(model.QuantityMultiplier, out boxQuantityLimit);

            SetDealProductDelivery(proposal.SellerGuid, bh.Guid, boxQuantityLimit);
            #endregion deal_product_delivery

            //多重選項
            if (!string.IsNullOrEmpty(options))
            {
                PponFacade.ProcessOptions(options, bid, _pponProv.DealPropertyGet(bid).UniqueId, username);
            }

            #region DealPayment

            DealPayment dealPayment = _pponProv.DealPaymentGet(bid);
            dealPayment.IsBankDeal = proposal.IsBankDeal;
            _pponProv.DealPaymentSet(dealPayment);
            #endregion
        }



        /// <summary>
        /// 依據新版宅配提案單的內容更新檔次資料(子檔)
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="username"></param>
        /// <param name="proposal"></param>
        /// <param name="model"></param>
        public static void PponDealUpdateByHouseroposal(Guid bid, string username, Proposal proposal, ProposalMultiDeal model)
        {
            #region 參數

            #region 提案單property

            string brandName = proposal.BrandName;
            int proposalSpecialFlag = proposal.SpecialFlag;

            #endregion

            #region 提案單的優惠內容(檔次)property

            string itemName = model.ItemName;
            decimal origPrice = model.OrigPrice;
            decimal itemPrice = model.ItemPrice;
            decimal cost = model.Cost;
            int slottingFeeQuantity = model.SlottingFeeQuantity;
            decimal orderTotalLimit = model.OrderTotalLimit;
            int atmMaximum = model.AtmMaximum;
            decimal freight = model.Freights;
            decimal noFreightLimit = model.NoFreightLimit;
            int sale_multiple_base = model.GroupCouponBuy;
            int present_quantity = model.GroupCouponPresent;
            string options = model.Options;
            int orderMaxPersonal = model.OrderMaxPersonal;

            #endregion

            #endregion

            // 優惠內容
            CouponEventContent cec = _pponProv.CouponEventContentGetByBid(bid);
            string eventName = ProposalFacade.GetProEventNameByComboDeal(proposal, model);
            cec.Name = eventName;   //黑標
            cec.CouponUsage = ProposalFacade.GetProposalCouponUsage(model);
            cec.AppTitle = null;//子檔不需要填app短標
            _pponProv.CouponEventContentSet(cec);

            ComboDeal cd = _pponProv.GetComboDeal(bid);
            cd.Title = ProposalFacade.GetProposalCouponUsage(model);
            _pponProv.AddComboDeal(cd);

            // 優惠內容、原價、售價、每人限購
            Item i = _itemProv.ItemGetByBid(bid);
            i.ItemName = ProposalFacade.GetProposalCouponUsage(model);
            i.ItemOrigPrice = origPrice;
            i.ItemPrice = itemPrice;
            i.MaxItemCount = Convert.ToInt32(orderMaxPersonal);
            i.ItemDefaultDailyAmount = Convert.ToInt32(orderMaxPersonal);
            _itemProv.ItemSet(i);


            // 進貨價、上架費
            DealCostCollection dcc = new DealCostCollection();
            _pponProv.DealCostDeleteListByBid(bid);
            if (cost > 0)
            {
                int maxCount = Convert.ToInt32(orderTotalLimit);
                if (slottingFeeQuantity > 0)
                {
                    dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = 0, Quantity = slottingFeeQuantity, CumulativeQuantity = slottingFeeQuantity, LowerCumulativeQuantity = 0 });
                    dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = cost, Quantity = maxCount - slottingFeeQuantity, CumulativeQuantity = maxCount, LowerCumulativeQuantity = slottingFeeQuantity });
                }
                else
                {
                    dcc.Add(new DealCost() { BusinessHourGuid = bid, Cost = cost, Quantity = maxCount, CumulativeQuantity = maxCount, LowerCumulativeQuantity = 0 });
                }
            }
            _pponProv.DealCostSetList(dcc);

            // 最大購買數量、ATM
            BusinessHour bh = _sellerProv.BusinessHourGet(bid);
            bh.OrderTotalLimit = orderTotalLimit;
            bh.BusinessHourAtmMaximum = atmMaximum;

            _sellerProv.BusinessHourSet(bh);

            //成套販售
            DealProperty dp = _pponProv.DealPropertyGet(bid);
            dp.SaleMultipleBase = sale_multiple_base + present_quantity;
            dp.PresentQuantity = present_quantity;
            dp.GroupCouponDealType = proposal.GroupCouponDealType ?? 0;
            dp.IsHouseNewVer = true;
            dp.IsAveragePrice = true;   //均價
            dp.IsQuantityMultiplier = true;//銷售倍數
            dp.DealAccBusinessGroupId = 7;//館別預設塞宅配
            int quantityMultiplier = default(int);
            int.TryParse(model.QuantityMultiplier, out quantityMultiplier);
            if (quantityMultiplier == 0)
            {
                quantityMultiplier = 1;
            }
            dp.QuantityMultiplier = quantityMultiplier;
            int comboPackCount = 0;
            int.TryParse(model.ComboPackCount, out comboPackCount);
            dp.ComboPackCount = comboPackCount;

            if (!string.IsNullOrEmpty(options))
            {
                dp.ShoppingCart = true;//有填寫多重選項的時候，要勾選購物車
            }

            if (config.IsConsignment)
            {
                dp.Consignment = proposal.Consignment;
            }
            bool includedDeliveryCharge = (model.Freights == 0);
            //免運
            if (includedDeliveryCharge)
            {
                List<int> tagList = new List<int>();
                if (dp.LabelTagList != null)
                {
                    string[] labelTagList = dp.LabelTagList.Split(",");
                    foreach (string tag in labelTagList)
                    {
                        int t = 0;
                        int.TryParse(tag, out t);
                        if (!tagList.Contains(t))
                        {
                            tagList.Add(t);
                        }
                    }
                }
                if (!tagList.Contains((int)DealLabelSystemCode.IncludedDeliveryCharge))
                {
                    tagList.Add((int)DealLabelSystemCode.IncludedDeliveryCharge);
                }
                dp.LabelTagList = string.Join(",", tagList.OrderBy(x => x));
            }
            _pponProv.DealPropertySet(dp);

            // 運費、免運門檻
            CouponFreightCollection cfc = new CouponFreightCollection();
            _pponProv.CouponFreightDeleteListByBid(bid);
            if (freight > 0)
            {
                if (noFreightLimit > 0)
                {
                    cfc.Add(new CouponFreight() { BusinessHourGuid = bh.Guid, StartAmount = itemPrice * noFreightLimit, EndAmount = 9999999, FreightAmount = 0, CreateId = username, CreateTime = DateTime.Now });
                    cfc.Add(new CouponFreight() { BusinessHourGuid = bid, StartAmount = 0, EndAmount = itemPrice * noFreightLimit, FreightAmount = freight, CreateId = username, CreateTime = DateTime.Now });
                }
                else
                {
                    cfc.Add(new CouponFreight() { BusinessHourGuid = bid, StartAmount = 0, EndAmount = 9999999, FreightAmount = freight, CreateId = username, CreateTime = DateTime.Now });
                }
            }
            _pponProv.CouponFreightSetList(cfc);

            #region 成套票券要存入前置文(母檔短標) sms_content
            if (Helper.IsFlagSet(proposal.SpecialFlag, ProposalSpecialFlag.GroupCoupon))
            {
                SmsContent sms = _pponProv.SMSContentGet(bid);
                string smsContent = brandName;
                if (smsContent.Length > 100)
                {
                    smsContent = smsContent.Substring(0, 100);
                }
                sms.Content = "17Life:|" + smsContent + "|，{0}-{1}，期限{2}-{3}，App載憑證：http://x.co/4r1JG";
                _pponProv.SMSContentSet(sms);
            }
            #endregion


            #region week_pay_account
            var insStores = new PponStoreCollection();
            insStores = _pponProv.PponStoreGetListByBusinessHourGuid(bh.Guid);
            var accounts = _orderProv.WeeklyPayAccountGetList(bh.Guid);
            var stores = _sellerProv.SellerGetList(insStores.Where(t => Helper.IsFlagSet(t.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid));
            foreach (var store in stores)
            {
                var account = accounts.FirstOrDefault(x => x.StoreGuid.HasValue && x.StoreGuid.Value == store.Guid)
                            ?? new WeeklyPayAccount
                            {
                                BusinessHourGuid = bh.Guid,
                                CreateDate = DateTime.Now,
                                Creator = username
                            };
                account.StoreGuid = store.Guid;
                account.EventName = (i.ItemName.Length > 100)
                                        ? i.ItemName.Substring(0, 100) + "..."
                                        : i.ItemName;
                account.CompanyName = store.CompanyName;
                account.SellerName = store.CompanyBossName;
                account.AccountId = store.CompanyID;
                account.AccountName = store.CompanyAccountName;
                account.AccountNo = store.CompanyAccount;
                account.Email = store.CompanyEmail;
                account.BankNo = store.CompanyBankCode;
                account.BranchNo = store.CompanyBranchCode;
                account.Message = store.CompanyNotice;
                account.SignCompanyID = store.SignCompanyID;

                accounts.Add(account);
            }
            _orderProv.WeeklyPayAccountSetList(accounts);

            #endregion week_pay_account

            //多重選項
            if (!string.IsNullOrEmpty(options))
            {
                PponFacade.ProcessOptions(options, bid, _pponProv.DealPropertyGet(bid).UniqueId, username);
            }

            #region DealPayment

            DealPayment dealPayment = _pponProv.DealPaymentGet(bid);
            dealPayment.IsBankDeal = proposal.IsBankDeal;
            _pponProv.DealPaymentSet(dealPayment);
            #endregion
        }
        /// <summary>
        /// 新舊資料比較
        /// </summary>
        /// <param name="item"></param>
        /// <param name="newItem"></param>
        /// <returns></returns>
        public static Dictionary<string, string> ColumnValueComparison(object item, object newItem)
        {
            Dictionary<string, string> dif = new Dictionary<string, string>();
            PropertyInfo[] pros = item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);
            for (int i = 0; i < pros.Length; i++)
            {
                if (pros[i].Name == "ModifyTime" || pros[i].Name == "ModifyId" || pros[i].Name == "Coordinate" || pros[i].Name == "IsLoaded" || pros[i].Name == "IsNew" ||
                    pros[i].Name == "IsDirty" || pros[i].Name == "StartUseUnit" || pros[i].Name == "StartUseText2" || pros[i].Name == "Id" || pros[i].Name == "ProposalId")
                {
                    continue;
                }
                object value = pros[i].GetValue(item, null);
                object newValue = pros[i].GetValue(newItem, null);


                if (pros[i].Name == "OrigPrice" || pros[i].Name == "ItemPrice" || pros[i].Name == "Cost" || pros[i].Name == "OrderTotalLimit" || pros[i].Name == "SlottingFeeQuantity" ||
                    pros[i].Name == "OrderMaxPersonal" || pros[i].Name == "AtmMaximum" || pros[i].Name == "CPCBuy" || pros[i].Name == "CPCPresent")
                {
                    if (Convert.ToDouble(value).ToString() != Convert.ToDouble(newValue).ToString())
                    {
                        KeyValuePair<string, string> data = GetColumnDescription(newItem, newItem.GetType().Name, pros[i].Name);
                        if (!string.IsNullOrWhiteSpace(data.Value) && !string.IsNullOrWhiteSpace(data.Key))
                        {
                            dif.Add(data.Key, data.Value);
                        }
                    }
                }
                else
                {
                    if (Convert.ToString(value).Trim().TrimEnd(".0000".ToCharArray()) != Convert.ToString(newValue).Trim().TrimEnd(".0000".ToCharArray()))
                    {
                        KeyValuePair<string, string> data = GetColumnDescription(newItem, newItem.GetType().Name, pros[i].Name);
                        if (!string.IsNullOrWhiteSpace(data.Value) && !string.IsNullOrWhiteSpace(data.Key))
                        {
                            dif.Add(data.Key, data.Value);
                        }
                        else if (string.IsNullOrWhiteSpace(data.Value) && !string.IsNullOrWhiteSpace(data.Key))
                        {
                            dif.Add(data.Key, "\"移除" + data.Key + "\"");
                        }
                    }
                }
            }
            return dif;
        }

        /// <summary>
        /// 新舊資料比較(商家提案)
        /// </summary>
        /// <param name="item"></param>
        /// <param name="newItem"></param>
        /// <param name="cols"></param>
        /// <param name="specialFlagList"></param>
        /// <returns></returns>
        public static Dictionary<string, string> ColumnValueComparison(object item, object newItem, List<string> cols, List<ProposalSpecialFlag> specialFlagList)
        {
            Dictionary<string, string> dif = new Dictionary<string, string>();
            PropertyInfo[] pros = item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);
            for (int i = 0; i < pros.Length; i++)
            {
                if (cols.Contains(pros[i].Name))
                {
                    object value = pros[i].GetValue(item, null);
                    object newValue = pros[i].GetValue(newItem, null);
                    if (Convert.ToString(value).Trim().TrimEnd(".0000".ToCharArray()) != Convert.ToString(newValue).Trim().TrimEnd(".0000".ToCharArray()))
                    {
                        KeyValuePair<string, string> data = GetColumnDescription(newItem, newItem.GetType().Name, pros[i].Name, specialFlagList);
                        if (!string.IsNullOrWhiteSpace(data.Value) && !string.IsNullOrWhiteSpace(data.Key))
                        {
                            dif.Add(data.Key, data.Value);
                        }
                        else if (string.IsNullOrWhiteSpace(data.Value) && !string.IsNullOrWhiteSpace(data.Key))
                        {
                            dif.Add(data.Key, "\"移除" + data.Key + "\"");
                        }
                    }
                }
            }
            return dif;
        }

        public static KeyValuePair<string, string> GetColumnDescription(object item, string typeName, string propertyName, List<ProposalSpecialFlag> specialFlagList = null)
        {
            string desc = string.Empty;
            string value = Convert.ToString(item.GetPropertyValue(propertyName));
            switch (typeName)
            {
                case "Seller":
                    if (item is Seller)
                    {
                        Seller seller = (Seller)item;
                        #region get desc
                        switch (propertyName)
                        {
                            case "SellerSales":
                                desc = "負責業務";
                                break;
                            case "SellerId":
                                desc = "賣家編號";
                                break;
                            case "SellerName":
                                desc = "品牌名稱";
                                break;
                            case "SellerLevel":
                                desc = "商家規模";
                                value = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerLevel)seller.SellerLevel);
                                break;
                            case "CityId":
                                desc = "城市";
                                value = CityManager.CityTownShopStringGet(seller.CityId);
                                break;
                            case "SellerAddress":
                                desc = "地址";
                                value = CityManager.CityTownShopStringGet(seller.CityId) + seller.SellerAddress;
                                break;
                            case "SellerBossName":
                                desc = "負責人";
                                break;
                            case "SellerContactPerson":
                                desc = "聯絡人";
                                break;
                            case "SellerTel":
                                desc = "連絡電話1";
                                break;
                            case "SellerFax":
                                desc = "傳真";
                                break;
                            case "SellerEmail":
                                desc = "電子信箱";
                                break;
                            case "SellerDescription":
                                desc = "其他";
                                break;
                            case "CompanyName":
                                desc = "簽約公司名稱";
                                break;
                            case "CompanyBossName":
                                desc = "簽約公司負責人";
                                break;
                            case "CompanyID":
                                desc = "簽約公司ID/統一編號";
                                break;
                            case "CompanyBankCode":
                                desc = "廠商受款帳戶-銀行代碼";
                                break;
                            case "CompanyBranchCode":
                                desc = "廠商受款帳戶-分行代碼";
                                break;
                            case "SignCompanyID":
                                desc = "受款人ID/統一編號";
                                break;
                            case "CompanyAccount":
                                desc = "廠商受款帳戶-帳號";
                                break;
                            case "CompanyAccountName":
                                desc = "廠商受款帳戶-戶名";
                                break;
                            case "AccountantName":
                                desc = "帳務聯絡人";
                                break;
                            case "AccountantTel":
                                desc = "帳務聯絡人電話";
                                break;
                            case "CompanyEmail":
                                desc = "帳務聯絡人Email";
                                break;
                            case "ReturnedPersonName":
                                desc = "退換貨聯絡人";
                                break;
                            case "ReturnedPersonTel":
                                desc = "退換貨聯絡人電話";
                                break;
                            case "ReturnedPersonEmail":
                                desc = "退換貨聯絡人Email";
                                break;
                            case "SalesId":
                                desc = "負責業務";
                                break;
                            case "TempStatus":
                                desc = "審核狀態";
                                value = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerTempStatus)seller.TempStatus);
                                break;
                            case "ApproveTime":
                                desc = "審核時間";
                                value = string.Empty;
                                break;
                            case "NewCreated":
                                value = string.Empty;
                                break;
                            case "SellerFrom":
                                desc = "店家來源";
                                value = Helper.GetDevelopStatus<ProposalSellerFrom>(seller.SellerFrom ?? -1);
                                break;
                            case "ReferralOther":
                                desc = "轉介人員(其他)";
                                break;
                            case "ReferralSale":
                                desc = "轉介人員(業務)";
                                break;
                            case "ReferralOtherCreateTime":
                                desc = "轉介人員(其他)建立時間";
                                break;
                            case "ReferralSaleCreateTime":
                                desc = "轉介人員(業務)建立時間";
                                break;
                            case "SellerPorperty":
                                desc = "店家屬性";
                                string changeSellerPorperty = "";
                                if (seller.SellerPorperty != null)
                                {
                                    string[] pros = seller.SellerPorperty.Split(",");
                                    if (pros.Length > 0)
                                    {
                                        foreach (string pro in pros)
                                        {
                                            int p = -1;
                                            int.TryParse(pro, out p);
                                            changeSellerPorperty += Helper.GetDevelopStatus<ProposalSellerPorperty>(p) + "、";
                                        }
                                    }
                                }
                                value = changeSellerPorperty.TrimEnd("、");
                                break;
                            case "Contacts":
                                desc = "多重聯絡人";
                                string changeContacts = "";
                                try
                                {
                                    List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                                    foreach (Seller.MultiContracts smc in contacts)
                                    {
                                        int smcType = 1;
                                        int.TryParse(smc.Type, out smcType);
                                        changeContacts += @"聯絡人類別:" + Helper.GetDevelopStatus<ProposalContact>(smcType)
                                                            + "姓名:" + smc.ContactPersonName
                                                            + "聯絡電話:" + smc.SellerTel
                                                            + "行動電話:" + smc.SellerMobile
                                                            + "傳真:" + smc.SellerFax
                                                            + "Email:" + smc.ContactPersonEmail
                                                            + "其他:" + smc.Others
                                                            + "<br />";
                                    }
                                }
                                catch
                                {
                                }
                                value = changeContacts;
                                break;
                            case "VendorReceiptType":
                                desc = "店家單據開立方式";
                                value = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VendorReceiptType)seller.VendorReceiptType);
                                break;
                            case "StoreTel":
                                desc = "營業據點-訂位電話";
                                break;
                            case "StoreAddress":
                                desc = "營業據點-地址";
                                break;
                            case "StoreCityId":
                                desc = "營業據點-地址-縣市";
                                break;
                            case "StoreTownshipId":
                                desc = "營業據點-地址-行政區";
                                break;
                            case "BranchName":
                                desc = "營業據點-店名";
                                break;
                            case "BranchBossName":
                                desc = "營業據點-負責人";
                                break;
                            case "StoreRemark":
                                desc = "營業據點-備註";
                                break;
                            case "OpenTime":
                                desc = "營業時間";
                                break;
                            case "CloseDate":
                                desc = "公休日";
                                break;
                            case "Mrt":
                                desc = "捷運";
                                break;
                            case "Car":
                                desc = "開車";
                                break;
                            case "Bus":
                                desc = "公車";
                                break;
                            case "OtherVehicles":
                                desc = "交通其他";
                                break;
                            case "WebUrl":
                                desc = "官網";
                                break;
                            case "FacebookUrl":
                                desc = "Facebook";
                                break;
                            case "BlogUrl":
                                desc = "部落格";
                                break;
                            case "OtherUrl":
                                desc = "官網其他";
                                break;
                            case "CreditcardAvailable":
                                desc = "刷卡服務";
                                value = seller.CreditcardAvailable ? "有" : "無"; ;
                                break;
                            case "IsOpenBooking":
                                desc = "預約管理";
                                value = seller.IsOpenBooking ? "有" : "無"; ;
                                break;
                            case "IsOpenReservationSetting":
                                desc = "預約服務連結";
                                value = seller.IsOpenReservationSetting ? "開啟" : "關閉";
                                break;
                            case "StoreStatus":
                                desc = "顯示狀態";
                                value = Convert.ToBoolean(seller.StoreStatus) ? "隱藏" : "顯示";
                                break;
                            case "IsCloseDown":
                                desc = "營業狀態";
                                value = seller.IsCloseDown ? "結束營業" : "正常營業";
                                break;
                            case "CloseDownDate":
                                desc = "結業營業日";
                                value = seller.CloseDownDate != null ? seller.CloseDownDate.Value.ToString("yyyy/MM/dd") : "\"空\"";
                                break;
                            case "RemittanceType":
                                desc = "出帳方式";
                                if (seller.RemittanceType != null)
                                {
                                    value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)seller.RemittanceType);
                                }

                                break;
                            case "SellerLevelDetail":
                                desc = "商家規模細項";
                                string changeDetail = "";
                                try
                                {
                                    Seller.LevelDetail detail = new JsonSerializer().Deserialize<Seller.LevelDetail>(seller.SellerLevelDetail);
                                    SellerLevel noCaterLv;
                                    var aLv = Enum.TryParse(detail.NoCaterLv, out noCaterLv) ? Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, noCaterLv) : "";
                                    changeDetail += string.Format(@"A區規模:{0},店家數:{1},平均客單價:{2}元/人,平均座位數:{3}單店/個,滿桌率:{4}%,翻桌次:{5}次/月,人潮流量:{6}人/月<br />", aLv, detail.Sellers, detail.AvgPrice, detail.AvgSeat, detail.TurnoverRate, detail.TurnoverFreq, detail.CrowdsFlow);
                                }
                                catch
                                {
                                }
                                value = changeDetail;
                                break;
                            default:
                                desc = propertyName;
                                break;
                        }
                        #endregion
                    }

                    break;
                case "SellerTree":
                    if (item is SellerTree)
                    {
                        SellerTree sellertree = (SellerTree)item;
                        #region get desc
                        switch (propertyName)
                        {
                            case "ParentSellerGuid":
                                desc = "上層公司";
                                value = sellertree.ParentSellerGuid != Guid.Empty ? _sellerProv.SellerGet(sellertree.ParentSellerGuid).SellerName : "\"已移除\"";
                                break;
                            case "RootSellerGuid":
                                desc = "最上層公司";
                                value = sellertree.RootSellerGuid != Guid.Empty ? _sellerProv.SellerGet(sellertree.RootSellerGuid).SellerName : "\"已移除\"";
                                break;
                        }
                        #endregion
                    }
                    break;
                case "Store":
                    if (item is Store)
                    {
                        Store store = (Store)item;
                        #region get desc
                        switch (propertyName)
                        {
                            case "StoreName":
                                desc = "分店名稱";
                                break;
                            case "TownshipId":
                                desc = "城市";
                                if (store.TownshipId.HasValue)
                                {
                                    value = CityManager.CityTownShopStringGet(store.TownshipId.Value);
                                }
                                break;
                            case "AddressString":
                                desc = "地址";
                                if (store.TownshipId.HasValue)
                                {
                                    value = CityManager.CityTownShopStringGet(store.TownshipId.Value) + store.AddressString;
                                }
                                break;
                            case "CompanyBossName":
                                desc = "負責人";
                                break;
                            case "Phone":
                                desc = "訂位電話";
                                break;
                            case "CreditcardAvailable":
                                desc = "刷卡服務";
                                value = store.CreditcardAvailable ? "有" : "無"; ;
                                break;
                            case "Mrt":
                                desc = "交通資訊-捷運";
                                break;
                            case "Car":
                                desc = "交通資訊-開車";
                                break;
                            case "Bus":
                                desc = "交通資訊-公車";
                                break;
                            case "OtherVehicles":
                                desc = "交通資訊-其他";
                                break;
                            case "WebUrl":
                                desc = "網站-官網";
                                break;
                            case "FacebookUrl":
                                desc = "網站-FaceBook";
                                break;
                            case "BlogUrl":
                                desc = "網站-部落格";
                                break;
                            case "OtherUrl":
                                desc = "網站-其他";
                                break;
                            case "OpenTime":
                                desc = "營業時間";
                                break;
                            case "CloseDate":
                                desc = "公休日";
                                break;
                            case "Remarks":
                                desc = "備註";
                                break;
                            case "CompanyName":
                                desc = "簽約公司名稱";
                                break;
                            case "CompanyID":
                                desc = "簽約公司ID/統一編號";
                                break;
                            case "CompanyBankCode":
                                desc = "廠商受款帳戶-銀行代碼";
                                break;
                            case "CompanyBranchCode":
                                desc = "廠商受款帳戶-分行代碼";
                                break;
                            case "SignCompanyID":
                                desc = "受款人ID/統一編號";
                                break;
                            case "CompanyAccount":
                                desc = "廠商受款帳戶-帳號";
                                break;
                            case "CompanyAccountName":
                                desc = "廠商受款帳戶-戶名";
                                break;
                            case "AccountantName":
                                desc = "帳務聯絡人";
                                break;
                            case "AccountantTel":
                                desc = "帳務聯絡人電話";
                                break;
                            case "CompanyEmail":
                                desc = "帳務聯絡人Email";
                                break;
                            case "TempStatus":
                                desc = "審核狀態";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (SellerTempStatus)store.TempStatus);
                                break;
                            case "ApproveTime":
                                desc = "審核時間";
                                value = string.Empty;
                                break;
                            case "NewCreated":
                                value = string.Empty;
                                break;
                            default:
                                desc = propertyName;
                                break;
                        }
                        #endregion
                    }
                    break;
                case "BusinessHour":
                    if (item is BusinessHour)
                    {
                        #region get desc
                        switch (propertyName)
                        {
                            case "PicAlt":
                                desc = "搜尋關鍵字";
                                break;
                            case "BusinessHourAtmMaximum":
                                desc = "ATM保留份數";
                                break;
                            case "OrderTotalLimit":
                                desc = "最大購買數量";
                                break;
                            default:
                                desc = propertyName;
                                value = string.Empty;
                                break;
                        }
                        #endregion
                    }
                    break;
                case "DealCost":
                    if (item is DealCost)
                    {
                        DealCost cost = (DealCost)item;
                        #region get desc
                        switch (propertyName)
                        {
                            case "Cost":
                                desc = "進貨價";
                                break;
                            case "Quantity":
                                desc = "上架費";
                                break;
                            default:
                                desc = propertyName;
                                value = string.Empty;
                                break;
                        }
                        #endregion
                    }
                    break;
                case "Item":
                    if (item is Item)
                    {
                        #region get desc
                        switch (propertyName)
                        {
                            case "ItemName":
                                desc = "優惠活動";
                                break;
                            case "ItemOrigPrice":
                                desc = "原價";
                                break;
                            case "ItemPrice":
                                desc = "售價";
                                break;
                            case "ItemDefaultDailyAmount":
                                desc = "每人限購";
                                break;
                            default:
                                desc = propertyName;
                                value = string.Empty;
                                break;
                        }
                        #endregion
                    }
                    break;
                case "DealProperty":
                    if (item is DealProperty)
                    {
                        DealProperty dp = (DealProperty)item;
                        #region get desc
                        switch (propertyName)
                        {
                            case "QuantityMultiplier":
                                desc = "組合數";
                                break;
                            case "DeliveryType":
                                desc = "類型";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (DeliveryType)dp.DeliveryType.Value);
                                break;
                            case "DealAccBusinessGroupId":
                                desc = "館別";
                                if (dp.DealAccBusinessGroupId.HasValue)
                                {
                                    value = AccBusinessGroupManager.GetAccBusinessGroupName(dp.DealAccBusinessGroupId.Value);
                                }
                                break;
                            case "DealType":
                                desc = "分類";
                                if (dp.DealType.HasValue)
                                {
                                    value = SystemCodeManager.GetDealTypeName(dp.DealType.Value);
                                }
                                break;
                            case "AncestorBusinessHourGuid":
                                desc = "續接數量檔次";
                                if (dp.AncestorBusinessHourGuid != Guid.Empty)
                                {
                                    value = dp.AncestorBusinessHourGuid.ToString();
                                }
                                else
                                {
                                    value = string.Empty;
                                }
                                break;
                            case "ContinuedQuantity":
                                desc = "續接數量";
                                break;
                            case "AncestorSequenceBusinessHourGuid":
                                desc = "續接憑證檔次";
                                if (dp.AncestorSequenceBusinessHourGuid != Guid.Empty)
                                {
                                    value = dp.AncestorSequenceBusinessHourGuid.ToString();
                                }
                                else
                                {
                                    value = string.Empty;
                                }
                                break;
                            case "VerifyType":
                                desc = "核銷方式";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (PponVerifyType)dp.VerifyType.Value);
                                break;
                            case "Consignment":
                                desc = "商品寄倉";
                                value = dp.Consignment ? "是" : "否";
                                break;
                            default:
                                desc = propertyName;
                                value = string.Empty;
                                break;
                        }
                        #endregion
                    }
                    break;
                case "DealAccounting":
                    if (item is DealAccounting)
                    {
                        DealAccounting da = (DealAccounting)item;
                        #region get desc
                        switch (propertyName)
                        {
                            case "DeptId":
                                desc = "業績歸屬";
                                Department dept = HumanFacade.GetDepartmentByDeptId(da.DeptId);
                                if (dept.IsLoaded)
                                {
                                    value = dept.DeptName;
                                }
                                break;
                            case "Paytocompany":
                                desc = "匯款方式";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (DealAccountingPayType)da.Paytocompany);
                                break;
                            case "RemittanceType":
                                desc = "出帳方式";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)da.RemittanceType);
                                break;
                            case "VendorReceiptType":
                                desc = "店家單據開立方式";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (VendorReceiptType)da.VendorReceiptType);
                                break;
                            case "IsInputTaxRequired":
                                desc = "店家發票免稅";
                                value = da.IsInputTaxRequired ? "否" : "是";
                                break;
                            case "Message":
                                desc = "店家單據開立方式-其他";
                                value = da.Message;
                                break;
                            default:
                                desc = propertyName;
                                value = string.Empty;
                                break;
                        }
                        #endregion
                    }
                    break;
                case "Proposal":
                    if (item is Proposal)
                    {
                        Proposal pro = (Proposal)item;
                        #region get desc
                        switch (propertyName)
                        {
                            case "BusinessHourGuid":
                                desc = "串接Bid";
                                break;
                            case "DeliveryType":
                                desc = "消費方式";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (DeliveryType)pro.DeliveryType);
                                break;
                            case "BrandName":
                                desc = "品牌名稱";
                                break;
                            case "DealType":
                                desc = "提案類型";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalDealType)pro.DealType);
                                break;
                            case "DealSubType":
                                desc = "提案類型(子)";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalDealSubType)pro.DealSubType);
                                break;
                            case "ExpireInfo":
                                desc = "製造日期";
                                break;
                            case "PayType":
                                desc = "匯款方式";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (DealAccountingPayType)pro.PayType);
                                break;
                            case "SpecialFlag":
                                desc = "特殊標記";
                                List<string> rtn = new List<string>();
                                foreach (var flag in Enum.GetValues(typeof(ProposalSpecialFlag)))
                                {
                                    if (specialFlagList != null)
                                    {
                                        if (!specialFlagList.Contains((ProposalSpecialFlag)flag))
                                        {
                                            continue;
                                        }
                                    }
                                    if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)flag))
                                    {
                                        rtn.Add(Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalSpecialFlag)flag));
                                    }
                                }
                                value = string.Join("、", rtn);
                                break;
                            case "SpecialFlagText":
                                value = string.Empty;
                                break;
                            case "DealCharacterFlag":
                                desc = "檔次特色";
                                List<string> dcf = new List<string>();
                                foreach (var flag in Enum.GetValues(typeof(ProposalDealCharacter)))
                                {
                                    if (Helper.IsFlagSet(pro.DealCharacterFlag, (ProposalDealCharacter)flag))
                                    {
                                        dcf.Add(Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalDealCharacter)flag));
                                    }
                                }
                                if (dcf.Count == 0)
                                {
                                    value = "無";
                                }
                                else
                                {
                                    value = string.Join("、", dcf);
                                }

                                break;
                            case "DealCharacterFlagText":
                                value = string.Empty;
                                break;
                            case "MediaReportFlag":
                                desc = "媒體報導";
                                List<string> mrf = new List<string>();
                                foreach (var flag in Enum.GetValues(typeof(ProposalMediaReport)))
                                {
                                    if (Helper.IsFlagSet(pro.MediaReportFlag, (ProposalMediaReport)flag))
                                    {
                                        mrf.Add(Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalMediaReport)flag));
                                    }
                                }
                                if (mrf.Count == 0)
                                {
                                    value = "無";
                                }
                                else
                                {
                                    value = string.Join("、", mrf);
                                }

                                break;
                            case "MediaReportFlagText":
                                value = string.Empty;
                                break;
                            case "ConsumerTime":
                                desc = "消費時間";
                                List<string> ct = new List<string>();
                                foreach (var flag in Enum.GetValues(typeof(ProposalConsumerTime)))
                                {
                                    if (Helper.IsFlagSet(pro.ConsumerTime, (ProposalConsumerTime)flag))
                                    {
                                        ct.Add(Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalConsumerTime)flag));
                                    }
                                }
                                value = string.Join("、", ct);
                                break;
                            case "OrderTimeS":
                                desc = "上檔日期";
                                value = pro.OrderTimeS.HasValue ? pro.OrderTimeS.Value.ToString("yyyy/MM/dd") : "未排檔";
                                break;
                            case "OrderTimeE":
                                desc = "結檔日期";
                                value = pro.OrderTimeE != null ? pro.OrderTimeE.Value.ToString("yyyy/MM/dd HH:mm:ss") : "未排檔";
                                break;
                            case "SellerProposalFlag":
                                desc = "";
                                break;
                            case "ScheduleExpect":
                                desc = "建議時程資訊";
                                break;
                            case "StartUseText":
                                desc = "活動使用有效時間";
                                if (pro.StartUseUnit == (int)ProposalStartUseUnitType.BeforeDay)
                                {
                                    value = pro.StartUseText2 + "~" + pro.StartUseText;
                                }
                                else
                                {
                                    value = pro.StartUseText + Helper.GetLocalizedEnum((ProposalStartUseUnitType)pro.StartUseUnit);
                                }

                                break;
                            case "ShipType":
                                desc = "出貨方式";
                                value = Helper.GetLocalizedEnum((DealShipType)pro.ShipType);
                                break;
                            case "ShipText1":
                                if (pro.ShipType == (int)DealShipType.Normal && !string.IsNullOrEmpty(pro.ShipText1))
                                {
                                    desc = "最早出貨日";
                                    value = string.Format("上檔後+{0}個工作日", pro.ShipText1);
                                }
                                else
                                {
                                    value = string.Empty;
                                }
                                break;
                            case "ShipText2":
                                if (pro.ShipType == (int)DealShipType.Normal && !string.IsNullOrEmpty(pro.ShipText2))
                                {
                                    desc = "最晚出貨日";
                                    value = string.Format("統一結檔後+{0}個工作日", pro.ShipText2);
                                }
                                else
                                {
                                    value = string.Empty;
                                }
                                break;
                            case "ShipText3":
                                if (pro.ShipType == (int)DealShipType.Normal && !string.IsNullOrEmpty(pro.ShipText3))
                                {
                                    desc = "出貨日";
                                    value = string.Format("訂單成立後{0}個工作日出貨完畢", pro.ShipText3);
                                }
                                else
                                {
                                    value = string.Empty;
                                }
                                break;
                            case "ContractMemo":
                                desc = "合約附註說明";
                                break;
                            case "ProductSpec":
                                desc = "商品規格/功能";
                                break;
                            case "MarketAnalysis":
                                desc = "市場分析";
                                break;
                            case "MarketingResource":
                                desc = "行銷策展";
                                break;
                            case "Options":
                                desc = "多重選項";
                                break;
                            case "Memo":
                                desc = "備註";
                                break;
                            case "MultiDeals":
                                desc = "優惠內容";
                                value = "";
                                break;
                            case "SaleMarketAnalysis":
                                value = string.Empty;
                                break;
                            case "PicAlt":
                                desc = "搜尋關鍵字";
                                break;
                            case "VendorReceiptType":
                                desc = "店家單據開立方式";
                                if (pro.VendorReceiptType != null)
                                {
                                    value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (VendorReceiptType)pro.VendorReceiptType);
                                }
                                break;
                            case "Othermessage":
                                desc = "店家單據開立方式-其他";
                                break;
                            case "DeliveryMethod":
                                desc = "配送方式";
                                if (pro.DeliveryMethod != null)
                                {
                                    value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalDeliveryMethod)pro.DeliveryMethod);
                                }
                                break;
                            case "BusinessPrice":
                                desc = "預估營業額";
                                break;
                            case "AncestorBusinessHourGuid":
                                desc = "續接數量檔次";
                                break;
                            case "AncestorSequenceBusinessHourGuid":
                                desc = "續接憑證檔次";
                                break;
                            case "IsEarlierPageCheck":
                                desc = "是否提前頁確";
                                value = pro.IsEarlierPageCheck ? "勾選" : "未勾選";
                                break;
                            case "EarlierPageCheckDay":
                                desc = "提前頁確天數";
                                value = pro.EarlierPageCheckDay.ToString();
                                break;
                            case "ShippingdateType":
                                desc = "一般出貨";
                                value = pro.ShippingdateType == 0 ? "訂單成立後天數" : "最早出貨天數";
                                break;
                            case "TrialPeriod":
                                desc = "鑑賞期";
                                value = pro.TrialPeriod != null ? (bool)pro.TrialPeriod ? "是" : "否" : "否";
                                break;
                            case "Mohist":
                                desc = "出帳方式-墨攻";
                                value = pro.Mohist != null ? (bool)pro.Mohist ? "是" : "否" : "否";
                                break;
                            case "FilesDone":
                                desc = "圖檔文件已上傳完整";
                                value = pro.FilesDone ? "是" : "否";
                                break;
                            case "Consignment":
                                desc = "商品寄倉";
                                value = pro.Consignment ? "是" : "否";
                                break;
                            case "SystemPic":
                                desc = "系統圖庫";
                                value = pro.SystemPic ? "是" : "否";
                                break;
                            case "DevelopeSalesId":
                                desc = "負責業務1";
                                value = _humProv.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.DevelopeSalesId).EmpName;
                                break;
                            case "OperationSalesId":
                                desc = "負責業務2";
                                value = _humProv.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.OperationSalesId).EmpName;
                                break;
                            case "IsBankDeal":
                                desc = "銀行合作";
                                value = pro.IsBankDeal ? "是" : "否";
                                break;
                            case "IsPromotionDeal":
                                desc = "展演類型檔次";
                                value = pro.IsPromotionDeal ? "是" : "否";
                                break;
                            case "IsExhibitionDeal":
                                desc = "展覽類型檔次";
                                value = pro.IsExhibitionDeal ? "是" : "否";
                                break;
                            case "GroupCouponDealType":
                                desc = "成套票券類型";
                                value = Helper.GetDescription((GroupCouponDealType)pro.GroupCouponDealType);
                                break;
                            case "DealName":
                                desc = "檔次名稱";
                                break;
                            case "NoDiscountShown":
                                desc = "折數改為特選";
                                value = pro.NoDiscountShown ? "是" : "否";
                                break;
                            case "DealSource":
                                desc = "商品來源";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalDealSource)pro.DealSource);
                                break;
                            case "DealType1":
                                desc = "檔次類型";
                                value = _systemProv.SystemCodeGetName("DealType", pro.DealType1 ?? 0);
                                break;
                            case "DealType2":
                                desc = "商品分線";
                                value = _systemProv.SystemCodeGetName("DealType", pro.DealType2 ?? 0);
                                break;
                            case "DealTypeDetail":
                                desc = "商品分線(子)";
                                value = _systemProv.SystemCodeGetName("DealType", pro.DealTypeDetail ?? 0);
                                break;
                            case "DeliveryIslands":
                                desc = "開放配送外島";
                                value = pro.DeliveryIslands ? "是" : "否";
                                break;
                            case "RemittanceType":
                                desc = "出帳方式";
                                value = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)pro.RemittanceType);
                                break;
                            case "NoTax":
                                desc = "開立免稅發票給消費者";
                                value = pro.NoTax ? "是" : "否";
                                break;
                            case "IsProduction":
                                desc = "是否申請製檔";
                                value = pro.IsProduction ? "是" : "否";
                                break;
                            case "ProductionDescription":
                                desc = "申請製檔說明";
                                value = pro.ProductionDescription;
                                break;
                            case "Cchannel":
                                desc = "獨立影片設定";
                                value = pro.Cchannel ? "是" : "否";
                                break;
                            case "CchannelLink":
                                desc = "獨立影片設定連結";
                                value = pro.CchannelLink;
                                break;
                            case "NoTaxNotification":
                                desc = "免稅商品通報";
                                value = pro.NoTaxNotification ? "是" : "否";
                                break;
                            case "IsGame":
                                desc = "活動遊戲檔";
                                value = pro.IsGame ? "是" : "否";
                                break;
                            case "IsChannelGift":
                                desc = "代銷贈品檔";
                                value = pro.IsChannelGift ? "是" : "否";
                                break;
                            case "AgentChannels":
                                desc = "代銷通路";
                                List<int> agentChannels = ProposalFacade.GetAgentChannels(pro.AgentChannels);
                                string agentChannelName = string.Empty;
                                foreach (int agentChannel in agentChannels)
                                {
                                    agentChannelName += string.Format("「{0}」", Helper.GetEnumDescription((AgentChannel)agentChannel));
                                }
                                value = agentChannelName;
                                break;
                            case "IsTaishinChosen":
                                desc = "台新特談商品";
                                value = pro.IsTaishinChosen ? "是" : "否";
                                break;
                            default:
                                desc = propertyName;
                                value = string.Empty;
                                break;
                        }
                        #endregion
                    }
                    break;
                case "ProposalMultiDeal":
                    if (item is ProposalMultiDeal)
                    {
                        ProposalMultiDeal pmd = (ProposalMultiDeal)item;
                        #region get desc
                        switch (propertyName)
                        {
                            case "BrandName":
                                desc = "品牌名稱";
                                break;
                            case "ItemName":
                                desc = "專案內容";
                                break;
                            case "OrigPrice":
                                desc = "原價";
                                break;
                            case "ItemPrice":
                                desc = "售價";
                                break;
                            case "Cost":
                                desc = "進貨價";
                                break;
                            case "SlottingFeeQuantity":
                                desc = "上架費";
                                break;
                            case "OrderTotalLimit":
                                desc = "最大購買數量";
                                break;
                            case "AtmMaximum":
                                desc = "ATM保留份數";
                                break;
                            case "OrderMaxPersonal":
                                desc = "每人限購";
                                break;
                            case "QuantityMultiplier":
                                desc = "組合數-數字";
                                break;
                            case "Unit":
                                desc = "組合數-單位";
                                break;
                            case "Freights":
                                desc = "運費";
                                break;
                            case "NoFreightLimit":
                                desc = "免運門檻";
                                break;
                            case "Options":
                                desc = "多重選項";
                                break;
                            case "SellerOptions":
                                desc = "賣家備註";
                                break;
                            case "GroupCouponBuy":
                                desc = "憑證組數(買)";
                                break;
                            case "GroupCouponPresent":
                                desc = "憑證組數(送)";
                                break;
                            case "Sort":
                                desc = "順序";
                                break;
                            case "Gifts":
                                desc = "贈品內容";
                                value = string.Join("、", new JsonSerializer().Deserialize<string[]>(pmd.Gifts));
                                break;
                            case "ComboPackCount":
                                desc = "結帳倍數";
                                break;
                            case "BoxQuantityLimit":
                                desc = "超商取貨限制";
                                break;
                            default:
                                desc = propertyName;
                                value = string.Empty;
                                break;
                        }
                        #endregion
                    }
                    break;
                case "ProposalCouponEventContent":
                    if (item is ProposalCouponEventContent)
                    {
                        ProposalCouponEventContent pcec = (ProposalCouponEventContent)item;
                        #region get desc
                        switch (propertyName)
                        {
                            case "Name":
                                desc = "專案內容";
                                break;
                            case "Description":
                                desc = "詳細說明";
                                break;
                            case "Remark":
                                desc = "一姬說好康";
                                break;
                            case "ProductSpec":
                                desc = "商品規格";
                                break;
                            case "ImagePath":
                                desc = "電腦版網頁主圖";
                                break;
                            case "AppDealPic":
                                desc = "APP / 行動版網頁主圖";
                                break;
                            case "RemoveBgPic":
                                desc = "Google 廣告去背圖";
                                break;
                            case "Restrictions":
                                desc = "好康權益說明";
                                break;
                            case "Title":
                                desc = "檔次行銷文案";
                                break;
                            case "AppTitle":
                                desc = "網站露出商品名稱";
                                break;
                            default:
                                desc = propertyName;
                                value = string.Empty;
                                break;
                        }
                        #endregion
                    }
                    break;
                default:
                    break;

            }
            return new KeyValuePair<string, string>(desc, value);
        }


        /// <summary>
        /// 新版宅配複製提案單
        /// </summary>
        /// <param name="ProposalId"></param>
        /// <param name="copyType"></param>
        /// <param name="loginUser"></param>
        /// <param name="IsSellerProposal"></param>
        /// <returns></returns>
        public static int CreateProposalByCloneSellerPro(int ProposalId, ProposalCopyType copyType, string loginUser, bool IsSellerProposal, bool isWms)
        {
            Proposal vpd = _sellerProv.ProposalGet(ProposalId);
            ProposalCategoryDealCollection pcdc_ori = _sellerProv.ProposalCategoryDealsGetList(ProposalId);
            ProposalCouponEventContent pcec_ori = _pponProv.ProposalCouponEventContentGetByPid(ProposalId);
            ViewPponDeal theDeal = null;

            if (vpd.BusinessHourGuid != null)
            {
                theDeal = _pponProv.ViewPponDealGetByBusinessHourGuid(vpd.BusinessHourGuid.Value);
            }


            #region create proposal

            Proposal pro = new Proposal();
            pro.SellerGuid = vpd.SellerGuid;
            pro.DevelopeSalesId = vpd.DevelopeSalesId;
            pro.OperationSalesId = vpd.OperationSalesId;
            pro.BrandName = vpd.BrandName;
            pro.DealName = vpd.DealName;
            pro.DeliveryType = (int)vpd.DeliveryType;
            pro.DealType = (int)vpd.DealType;
            pro.DealSubType = (int)vpd.DealSubType;
            pro.VendorReceiptType = vpd.VendorReceiptType;

            pro.Othermessage = vpd.Othermessage;
            pro.DeliveryMethod = vpd.DeliveryMethod;
            pro.DealType1 = vpd.DealType1; //檔次類型
            pro.PicAlt = vpd.PicAlt;    //關鍵字
            pro.AgentChannels = vpd.AgentChannels;  //代銷通路

            //出貨方式
            if (isWms)
            {
                //複製成24到貨
                pro.ShipType = (int)DealShipType.Wms;
                pro.ShipText3 = string.Empty;
                pro.DeliveryTimeS = null;
                pro.DeliveryTimeE = null;
            }
            else if (vpd.IsWms && !isWms)
            {
                //24到貨複製成一般
                pro.ShipType = (int)DealShipType.Ship72Hrs;
                pro.ShipText3 = string.Empty;
                pro.DeliveryTimeS = null;
                pro.DeliveryTimeE = null;
            }
            else
            {
                pro.ShipType = vpd.ShipType;
                pro.ShipText3 = vpd.ShipText3;
                pro.DeliveryTimeS = vpd.DeliveryTimeS;
                pro.DeliveryTimeE = vpd.DeliveryTimeE;
            }



            //匯款對象(宅配皆匯款至賣家)
            pro.PayType = (int)DealAccountingPayType.PayToCompany;

            if (config.IsRemittanceFortnightly)
            {

                if (SellerFacade.IsAgreeNewContractSeller(vpd.SellerGuid, (int)DeliveryType.ToHouse))
                {
                    //同意合約之宅配商家,依出貨方式塞出帳方式
                    if (isWms || vpd.IsWms)
                    {
                        //複製成24到貨或是24到或複製成一班
                        pro.RemittanceType = (int)RemittanceType.Fortnightly;
                    }
                    else
                    {
                        if (vpd.ShipType == (int)DealShipType.Ship72Hrs)
                        {
                            pro.RemittanceType = (int)RemittanceType.Fortnightly;
                        }
                        else
                        {
                            pro.RemittanceType = (int)RemittanceType.Monthly;
                        }
                    }

                }
                else
                {
                    //不同意之宅配商家不予複製
                }
            }
            else
            {
                //出帳方式(舊的提案單會沒有出帳方式，改抓檔次的)
                pro.RemittanceType = vpd.RemittanceType;

                if (vpd.BusinessHourGuid != null)
                {
                    DealAccounting da = _pponProv.DealAccountingGet(vpd.BusinessHourGuid.Value);
                    if (da.IsLoaded)
                    {
                        pro.RemittanceType = da.RemittanceType;
                    }
                }
            }
            pro.DealType2 = vpd.DealType2;  //商品分線
            pro.DealTypeDetail = vpd.DealTypeDetail;

            pro.ContractMemo = vpd.ContractMemo;
            pro.SpecialFlagText = vpd.SpecialFlagText;

            if (ProposalFacade.IsVbsProposalNewVersion())
            {
                if (IsSellerProposal)
                {
                    //是否為商家提案單
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SellerEditor));
                    pro.Status = (int)ProposalStatus.SellerCreate;
                }
                else
                {
                    pro.ApplyFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ApplyFlag, ProposalApplyFlag.Apply));//直接提案申請
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Send));
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
                }
            }
            else
            {
                pro.SellerProposalFlag = (int)SellerProposalFlag.Apply;
            }

            pro.CopyType = (int)copyType;
            pro.SpecialFlag = vpd.SpecialFlag;

            //宅配複製提案單時，不複製低毛利率可使用折價券
            if (pro.DeliveryType == (int)DeliveryType.ToHouse)
            {
                if (pro.DealType != (int)ProposalDealType.Travel && pro.DealType != (int)ProposalDealType.Piinlife)
                {
                    pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SpecialFlag, ProposalSpecialFlag.LowGrossMarginAllowedDiscount));
                    pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SpecialFlag, ProposalSpecialFlag.Photographers));
                    pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SpecialFlag, ProposalSpecialFlag.SelfOptional));
                    pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SpecialFlag, ProposalSpecialFlag.SellerPhoto));
                    pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SpecialFlag, ProposalSpecialFlag.FTPPhoto));
                }


                //每日一物不複製
                if (config.IsEveryDayNewDeals)
                {
                    pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SpecialFlag, ProposalSpecialFlag.EveryDayNewDeal));
                }
                else
                {
                    pro.SpecialFlag = pro.SpecialFlag;
                }
            }

            if (ProposalFacade.IsVbsProposalNewVersion())
            {
                if (copyType == ProposalCopyType.Seller)
                {
                    pro.ProposalCreatedType = (int)ProposalCreatedType.Seller;
                }
                else
                {
                    pro.ProposalCreatedType = (int)ProposalCreatedType.Sales;
                }
                pro.BusinessHourGuid = Guid.NewGuid();
            }
            else
            {
                pro.ProposalCreatedType = (int)ProposalCreatedType.Seller;
            }

            if (ProposalFacade.IsVbsProposalNewVersion())
            {
                if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    pro.ProposalSourceType = (int)ProposalSourceType.House;
                }
                else if (pro.DeliveryType == (int)DeliveryType.ToShop)
                {
                    pro.ProposalSourceType = (int)ProposalSourceType.Shop;
                }
                pro.DealSource = vpd.DealSource;
                pro.Mohist = vpd.Mohist ?? false;
            }


            if (theDeal != null && theDeal.IsLoaded && vpd.ProposalSourceType == (int)ProposalSourceType.Original)
            {
                pro.DealType2 = theDeal.DealType;
                pro.DealTypeDetail = theDeal.DealTypeDetail;
                pro.PicAlt = theDeal.PicAlt;
            }
            pro.IsWms = isWms;
            pro.CreateId = loginUser;
            pro.CreateTime = DateTime.Now;
            pro.ModifyId = loginUser;
            pro.ModifyTime = DateTime.Now;
            _sellerProv.ProposalSet(pro);

            #endregion

            #region Category

            int categoryId = config.EveryDayNewDealsCategoryId;
            var wmsWmsDeal = _sellerProv.CategoryGetList((int)CategoryType.DealCategory).Where(x => x.Name == "24H到貨");
            var wmsWmsChannel = _sellerProv.CategoryGetList((int)CategoryType.PponChannel).Where(x => x.Name == "24H到貨");
            ProposalCategoryDealCollection pcdc = new ProposalCategoryDealCollection();

            if (config.IsEveryDayNewDeals)
            {
                foreach (var pcd in pcdc_ori)
                {
                    if (pcd.Cid != categoryId)
                    {
                        if (!(!pro.IsWms && vpd.IsWms && (pcd.Cid == wmsWmsDeal.FirstOrDefault().Id || pcd.Cid == wmsWmsChannel.FirstOrDefault().Id)))
                        {
                            //24到貨複製成一般不用複製相關分類
                            pcdc.Add(new ProposalCategoryDeal
                            {
                                Pid = pro.Id,
                                Cid = pcd.Cid
                            });
                        }
                    }
                }
            }
            else
            {
                foreach (var pcd in pcdc_ori)
                {
                    if (!(!pro.IsWms && vpd.IsWms && (pcd.Cid == wmsWmsDeal.FirstOrDefault().Id || pcd.Cid == wmsWmsChannel.FirstOrDefault().Id)))
                    {
                        //24到貨複製成一般不用複製相關分類
                        pcdc.Add(new ProposalCategoryDeal
                        {
                            Pid = pro.Id,
                            Cid = pcd.Cid
                        });
                    }
                }
            }


            if (wmsWmsDeal.Any() && wmsWmsChannel.Any())
            {
                if (pro.IsWms && !vpd.IsWms)
                {
                    //一般複製成24到貨
                    pcdc.Add(new ProposalCategoryDeal()
                    {
                        Pid = pro.Id,
                        Cid = wmsWmsDeal.First().Id
                    });
                    pcdc.Add(new ProposalCategoryDeal()
                    {
                        Pid = pro.Id,
                        Cid = wmsWmsChannel.First().Id
                    });
                }
            }


            _sellerProv.ProposalCategoryDealSetList(pcdc);
            #endregion

            #region ProposalCouponEventContent
            if (theDeal != null && theDeal.IsLoaded && vpd.ProposalSourceType == (int)ProposalSourceType.Original)
            {
                CouponEventContent cec = _pponProv.CouponEventContentGetByBid(theDeal.BusinessHourGuid);
                ProposalCouponEventContent pcec = new ProposalCouponEventContent();
                pcec.ProposalId = pro.Id;
                pcec.Restrictions = cec.Restrictions;
                pcec.Description = cec.Description;
                pcec.ProductSpec = cec.ProductSpec;

                pcec.Title = cec.Title;
                pcec.AppTitle = cec.AppTitle;
                pcec.Remark = cec.Remark;
                pcec.ImagePath = cec.ImagePath;
                pcec.AppDealPic = cec.AppDealPic;
                _pponProv.ProposalCouponEventContentSet(pcec);
            }
            else
            {
                ProposalCouponEventContent pcec = new ProposalCouponEventContent();
                pcec.ProposalId = pro.Id;
                pcec.Restrictions = pcec_ori.Restrictions;
                pcec.Description = pcec_ori.Description;
                pcec.ProductSpec = pcec_ori.ProductSpec;
                if (vpd.ProposalSourceType == (int)ProposalSourceType.Original && pro.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    pcec.ProductSpec = vpd.ProductSpec;
                }
                pcec.Title = pcec_ori.Title;
                pcec.AppTitle = pcec_ori.AppTitle;
                pcec.Remark = pcec_ori.Remark;
                pcec.ImagePath = pcec_ori.ImagePath;
                pcec.AppDealPic = pcec_ori.AppDealPic;
                pcec.RemoveBgPic = pcec_ori.RemoveBgPic;
                _pponProv.ProposalCouponEventContentSet(pcec);
            }
            #endregion

            #region ProposalStore
            //宅配該賣家預設打勾
            if (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Travel)
            {
                ProposalStoreCollection psc = new ProposalStoreCollection();
                psc.Add(new ProposalStore()
                {
                    ProposalId = pro.Id,
                    StoreGuid = pro.SellerGuid,
                    TotalQuantity = null,
                    VbsRight = (int)VbsRightFlag.Verify + (int)VbsRightFlag.Accouting + (int)VbsRightFlag.ViewBalanceSheet,
                    ResourceGuid = Guid.NewGuid(),
                    CreateId = loginUser,
                    CreateTime = DateTime.Now
                });

                _pponProv.ProposalStoreSetList(psc);
            }
            #endregion

            #region proposal log

            ProposalFacade.ProposalLog(pro.Id, ProposalId + " " + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, copyType), loginUser, ProposalLogType.Seller);

            #endregion


            #region ProposalMultiDeal
            try
            {
                if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    if (copyType == ProposalCopyType.Seller)
                    {
                        //商家複製提案單
                        ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(ProposalId);
                        ProposalMultiDealCollection OriMultiDeals = multiDeals.Clone();
                        foreach (ProposalMultiDeal deal in OriMultiDeals)
                        {

                            deal.Pid = pro.Id;
                            if (pro.ProposalSourceType == (int)ProposalSourceType.Original)
                            {
                                deal.Options = "";
                            }
                            else if (pro.IsWms != vpd.IsWms)
                            {
                                //複製成不同倉儲清空規格
                                deal.SellerOptions = "";
                                deal.Options = "";
                            }
                            deal.Freights = 0;
                            deal.NoFreightLimit = 0;
                            deal.Bid = null;
                            deal.CreateId = loginUser;
                            deal.CreateTime = DateTime.Now;
                            deal.ModifyId = null;
                            deal.ModifyTime = null;
                        }
                        _sellerProv.ProposalMultiDealSetList(OriMultiDeals);
                    }
                    else
                    {
                        //業務複製提案單
                        ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(ProposalId);
                        ProposalMultiDealCollection OriMultiDeals = multiDeals.Clone();
                        foreach (ProposalMultiDeal deal in OriMultiDeals)
                        {
                            deal.Pid = pro.Id;
                            deal.Freights = 0;
                            deal.NoFreightLimit = 0;
                            deal.Bid = null;
                            if (pro.IsWms != vpd.IsWms)
                            {
                                //複製成不同倉儲清空規格
                                deal.SellerOptions = "";
                                deal.Options = "";
                            }
                            else
                            {
                                deal.SellerOptions = "";
                            }
                            deal.CreateId = loginUser;
                            deal.CreateTime = DateTime.Now;
                            deal.ModifyId = null;
                            deal.ModifyTime = null;
                        }
                        _sellerProv.ProposalMultiDealSetList(OriMultiDeals);
                    }
                }
            }
            catch { }
            #endregion

            return pro.Id;
        }

        /// <summary>
        /// 複製提案單(尚未有檔次)
        /// </summary>
        /// <param name="ProposalId">提案單單號</param>
        /// <param name="copyType">複製類型</param>
        /// <param name="loginUser">登入者</param>
        /// <returns></returns>
        public static int CreateProposalByCloneNewDeal(int ProposalId, ProposalCopyType copyType, string loginUser)
        {
            Proposal pro_ori = _sellerProv.ProposalGet(ProposalId);
            int new_pid = ReCreateProposal(pro_ori, copyType, loginUser);

            #region proposal log

            ProposalFacade.ProposalLog(new_pid, ProposalId + " " + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, copyType), loginUser);

            #endregion

            return new_pid;
        }

        /// <summary>
        /// 複製提案單(有檔次)
        /// </summary>
        /// <param name="bid">bid</param>
        /// <param name="copyType">複製類型</param>
        /// <param name="loginUser">登入者</param>
        /// <returns></returns>
        public static int CreateProposalByCloneDeal(Guid bid, ProposalCopyType copyType, string loginUser)
        {
            Proposal pro_ori = _sellerProv.ProposalGet(bid);
            int new_pid = ReCreateProposal(pro_ori, copyType, loginUser);

            #region proposal log

            ProposalFacade.ProposalLog(new_pid, bid + " " + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, copyType), loginUser);

            #endregion

            return new_pid;
        }

        public static int ReCreateProposal(Proposal pro_ori, ProposalCopyType copyType, string loginUser)
        {

            ProposalCategoryDealCollection pcdc_ori = _sellerProv.ProposalCategoryDealsGetList(pro_ori.Id);
            ProposalCouponEventContent pcec_ori = _pponProv.ProposalCouponEventContentGetByPid(pro_ori.Id);
            ProposalMultiDealCollection multiDeals_ori = _sellerProv.ProposalMultiDealGetByPid(pro_ori.Id);

            #region create proposal

            Proposal pro = new Proposal();
            pro.BrandName = pro_ori.BrandName;
            pro.DeliveryType = (int)pro_ori.DeliveryType;
            pro.DealType = (int)pro_ori.DealType;
            pro.DealSubType = (int)pro_ori.DealSubType;
            pro.BusinessPrice = pro_ori.BusinessPrice;
            pro.VendorReceiptType = pro_ori.VendorReceiptType;
            pro.Othermessage = pro_ori.Othermessage;
            pro.DeliveryMethod = pro_ori.DeliveryMethod;
            pro.ShipType = pro_ori.ShipType;
            pro.ShipText1 = pro_ori.ShipText1;
            pro.ShipText2 = pro_ori.ShipText2;
            pro.ShipText3 = pro_ori.ShipText3;
            pro.ShippingdateType = pro_ori.ShippingdateType;
            pro.MarketAnalysis = pro_ori.MarketAnalysis;
            pro.MarketingResource = pro_ori.MarketingResource;
            pro.ScheduleExpect = pro_ori.ScheduleExpect;
            pro.StartUseUnit = pro_ori.StartUseUnit;
            pro.StartUseText = pro_ori.StartUseText;
            pro.StartUseText2 = pro_ori.StartUseText2;
            pro.ConsumerTime = pro_ori.ConsumerTime;
            pro.ProductSpec = pro_ori.ProductSpec;
            pro.MediaReportFlag = pro_ori.MediaReportFlag;
            pro.MediaReportFlagText = pro_ori.MediaReportFlagText;
            pro.ContractMemo = pro_ori.ContractMemo;
            pro.Memo = pro_ori.Memo;
            pro.PicAlt = pro_ori.PicAlt;
            pro.GroupCouponDealType = pro_ori.GroupCouponDealType;

            pro.IsEarlierPageCheck = pro_ori.IsEarlierPageCheck;
            pro.EarlierPageCheckDay = pro_ori.EarlierPageCheckDay;

            try
            {
                if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    if (copyType == ProposalCopyType.Seller)
                    {
                        //商家複製提案單
                        ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);
                        foreach (ProposalMultiDeal deal in multiDeals)
                        {
                            deal.Options = "";
                        }
                        _sellerProv.ProposalMultiDealSetList(multiDeals);
                    }
                    else
                    {
                        //業務複製提案單
                        ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);
                        foreach (ProposalMultiDeal deal in multiDeals)
                        {
                            deal.SellerOptions = "";
                        }
                        _sellerProv.ProposalMultiDealSetList(multiDeals);
                    }
                }
            }
            catch { }



            pro.SellerGuid = pro_ori.SellerGuid;
            pro.BusinessHourGuid = null;
            pro.ReferrerBusinessHourGuid = pro_ori.BusinessHourGuid ?? null;
            pro.DevelopeSalesId = MemberFacade.GetUniqueId(loginUser);
            pro.OperationSalesId = pro_ori.OperationSalesId;
            pro.CopyType = (int)copyType;
            pro.PayType = pro_ori.PayType;
            //每日一物不複製
            if (config.IsEveryDayNewDeals)
            {
                pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro_ori.SpecialFlag, ProposalSpecialFlag.EveryDayNewDeal));
            }
            else
            {
                pro.SpecialFlag = pro_ori.SpecialFlag;
            }

            if (pro.DeliveryType == (int)DeliveryType.ToShop)
            {
                pro.SpecialFlag = pro.SpecialFlag | (int)ProposalSpecialFlag.NotAllowedDiscount;
            }

            //FAB不複製
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro_ori.SpecialFlag, ProposalSpecialFlag.DealStar));


            //不複製攝影狀態
            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro_ori.SpecialFlagText);
            specialText.Remove(ProposalSpecialFlag.Photographers);
            specialText.Remove(ProposalSpecialFlag.DealStar);

            pro.SpecialFlagText = new JsonSerializer().Serialize(specialText);

            pro.ProposalCreatedType = (int)ProposalCreatedType.Sales;
            pro.Mohist = pro.Mohist ?? false; // 預設墨攻為關閉;
            pro.AgentChannels = pro_ori.AgentChannels;//代銷通路


            #region flags & status

            pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
            pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting));  //等待商家覆核           
            pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SellerEditor));   //商家編輯中
            pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.Check));          //提案覆核

            switch (copyType)
            {
                case ProposalCopyType.Similar:

                    #region 經由再次上檔建立提案單，可略過的檢查項目 (因商業邏輯變更，除了[財務確認]不需要重新檢核以外，其餘都需要重新確認過)

                    // 財務確認
                    if (Helper.IsFlagSet(pro_ori.ListingFlag, (int)ProposalListingFlag.FinanceCheck))
                    {
                        pro.ListingFlag = (int)ProposalListingFlag.FinanceCheck;
                    }

                    #endregion

                    break;
                case ProposalCopyType.Same:

                    #region 經由快速上檔建立提案單，可略過的檢查項目 (只剩下 建檔確認 及 設定確認)

                    // 提案申請
                    pro.ApplyFlag = (int)ProposalApplyFlag.Apply;
                    // QC確認
                    pro.ApproveFlag = (int)ProposalApproveFlag.QCcheck;
                    // 設定確認
                    pro.BusinessCreateFlag = (int)ProposalBusinessCreateFlag.Created | (int)ProposalBusinessCreateFlag.SettingCheck;
                    // 提案稽核、攝影確認
                    pro.BusinessFlag = (int)ProposalBusinessFlag.ProposalAudit | (int)ProposalBusinessFlag.PhotographerCheck;
                    // 文案編輯、圖片設計
                    pro.EditFlag = (int)ProposalEditorFlag.CopyWriter | (int)ProposalEditorFlag.ImageDesign | (int)ProposalEditorFlag.ART | (int)ProposalEditorFlag.Listing;
                    // 頁面確認、紙本合約、財務確認
                    pro.ListingFlag = (int)ProposalListingFlag.PageCheck | (int)ProposalListingFlag.PaperContractCheck;

                    if (Helper.IsFlagSet(pro_ori.ListingFlag, (int)ProposalListingFlag.FinanceCheck))
                    {
                        pro.ListingFlag = pro.ListingFlag | (int)ProposalListingFlag.FinanceCheck;
                    }
                    #endregion

                    break;
                case ProposalCopyType.None:
                default:
                    break;
            }
            pro.Status = SellerFacade.GetFlagForProposalFlagCheck(pro);

            #endregion

            pro.IsPromotionDeal = pro_ori.IsPromotionDeal;//展演型檔次
            pro.IsExhibitionDeal = pro_ori.IsExhibitionDeal;//展覽型檔次

            pro.CreateId = loginUser;
            pro.CreateTime = DateTime.Now;
            pro.ModifyId = null;
            pro.ModifyTime = null;
            _sellerProv.ProposalSet(pro);

            #endregion

            #region Category

            int categoryId = config.EveryDayNewDealsCategoryId;
            ProposalCategoryDealCollection pcdc = new ProposalCategoryDealCollection();
            List<ViewCategoryDependency> subCategorylist = _sellerProv.ViewCategoryDependencyGetAll(null)
                                                    .Where(x => x.CategoryType == (int)CategoryType.DealCategory).ToList();

            if (config.IsEveryDayNewDeals)
            {
                foreach (var pcd in pcdc_ori)
                {
                    if (pcd.Cid != categoryId)
                    {
                        //不顯示後台的頻道不複製
                        var c = subCategorylist.Where(x => x.CategoryId == pcd.Cid).FirstOrDefault();
                        if (c != null && !c.IsShowBackEnd)
                        {
                            continue;
                        }

                        pcdc.Add(new ProposalCategoryDeal
                        {
                            Pid = pro.Id,
                            Cid = pcd.Cid
                        });
                    }

                }
            }
            else
            {
                foreach (var pcd in pcdc_ori)
                {
                    //不顯示後台的頻道不複製
                    var c = subCategorylist.Where(x => x.CategoryId == pcd.Cid).FirstOrDefault();
                    if (c != null && !c.IsShowBackEnd)
                    {
                        continue;
                    }
                    pcdc.Add(new ProposalCategoryDeal
                    {
                        Pid = pro.Id,
                        Cid = pcd.Cid
                    });

                }
            }


            _sellerProv.ProposalCategoryDealSetList(pcdc);
            #endregion

            #region ProposalCouponEventContent
            ProposalCouponEventContent pcec = new ProposalCouponEventContent();
            pcec.ProposalId = pro.Id;
            pcec.Restrictions = pcec_ori.Restrictions;
            _pponProv.ProposalCouponEventContentSet(pcec);
            #endregion

            #region ProposalStore
            //宅配該賣家預設打勾
            if (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Travel)
            {
                ProposalStoreCollection psc = new ProposalStoreCollection();
                psc.Add(new ProposalStore()
                {
                    ProposalId = pro.Id,
                    StoreGuid = pro.SellerGuid,
                    TotalQuantity = null,
                    VbsRight = (int)VbsRightFlag.Verify + (int)VbsRightFlag.Accouting + (int)VbsRightFlag.ViewBalanceSheet,
                    ResourceGuid = Guid.NewGuid(),
                    CreateId = loginUser,
                    CreateTime = DateTime.Now
                });

                _pponProv.ProposalStoreSetList(psc);
            }
            #endregion

            #region ProposalMultiDeal

            if (!(Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.GroupCoupon) && pro_ori.CreateTime < config.NewGroupCouponOn))
            {
                ProposalMultiDealCollection pmdc = new ProposalMultiDealCollection();
                foreach (var deal in multiDeals_ori)
                {
                    ProposalMultiDeal pmd = new ProposalMultiDeal();
                    pmd.Pid = pro.Id;
                    pmd.ItemName = deal.ItemName;
                    pmd.ItemNameTravel = deal.ItemNameTravel;
                    pmd.OrigPrice = deal.OrigPrice;
                    pmd.ItemPrice = deal.ItemPrice;
                    pmd.Cost = deal.Cost;
                    pmd.GroupCouponBuy = deal.GroupCouponBuy;
                    pmd.GroupCouponPresent = deal.GroupCouponPresent;
                    pmd.SlottingFeeQuantity = deal.SlottingFeeQuantity;
                    pmd.OrderMaxPersonal = Convert.ToInt32(deal.OrderMaxPersonal);
                    pmd.OrderTotalLimit = Convert.ToInt32(deal.OrderTotalLimit);
                    pmd.AtmMaximum = deal.AtmMaximum;
                    pmd.Freights = deal.Freights;
                    pmd.NoFreightLimit = Convert.ToInt32(deal.NoFreightLimit);
                    pmd.Options = deal.Options;
                    pmd.Sort = deal.Sort;
                    pmd.CreateId = loginUser;
                    pmd.CreateTime = DateTime.Now;
                    pmdc.Add(pmd);
                }

                _sellerProv.ProposalMultiDealSetList(pmdc);
            }

            #endregion


            return pro.Id;
        }

        public static DealCostCollection GetDealCostByGuid(Guid bid)
        {
            DealCostCollection dcs = _pponProv.DealCostGetList(bid);
            return dcs;
        }

        public static ViewPponDealCollection GetOnUseViewPponDealListBySellerGuid(Guid sid)
        {
            ViewPponDealCollection vpds = _pponProv.ViewPponDealGetListOnUseBySellerGuid(sid);
            return vpds;
        }

        public static PponOptionItems PponOptionItemsGet(Guid bid, Guid assignBid)
        {
            PponOptionItems pos = new PponOptionItems();
            bool multiDeals = false;
            if (bid != Guid.Empty)
            {
                List<ViewPponDeal> listData = new List<ViewPponDeal>();
                ViewComboDealCollection vcdc = _pponProv.GetViewComboDealAllByBid(bid);//子母檔
                Guid MainBusinessHourGuid = Guid.Empty;
                decimal MainOrderTotalLimit = 0;    //母檔的最大購買數
                decimal OrderTotalLimit = 0;        //子檔的最大購買數(選中的子檔)
                decimal SumOrderTotalLimit = 0;     //子檔的最大購買數加總
                string DeptId = EmployeeChildDept.S001.ToString();
                bool IsSlottingFeeQuantity = false;
                bool IsNoRestrictedStore = false;
                bool isConsignment = false; //是否為商品寄倉
                Dictionary<Guid, decimal> Verified = new Dictionary<Guid, decimal>();                                              //核銷數量
                Dictionary<Guid, Dictionary<Guid, decimal>> StoreVerified = new Dictionary<Guid, Dictionary<Guid, decimal>>();     //各分店核銷數量
                Dictionary<Guid, decimal> Sale = new Dictionary<Guid, decimal>();                                                  //銷售數量
                Dictionary<Guid, Dictionary<Guid, decimal>> StoreSale = new Dictionary<Guid, Dictionary<Guid, decimal>>();         //各分店銷售數量
                Dictionary<Guid, Dictionary<Guid, decimal>> StoreReturn = new Dictionary<Guid, Dictionary<Guid, decimal>>();       //各分店退貨數量
                Dictionary<Guid, decimal> Return = new Dictionary<Guid, decimal>();                                                //退貨數量
                int Delivery_type = 0;
                Guid newBid = assignBid;
                if (vcdc.Count > 0)
                {

                    #region 多檔次
                    multiDeals = true;
                    foreach (ViewComboDeal vcd in vcdc.OrderBy(x => x.Id))
                    {
                        if (vcd.Title == "主檔")
                        {
                            MainBusinessHourGuid = vcd.BusinessHourGuid;
                            continue;
                        }
                        ViewPponDealCollection vpdc = _pponProv.ViewPponDealGetListByPeriod(vcd.BusinessHourGuid, DateTime.Now.AddDays(-360), DateTime.Now.AddDays(60));
                        BusinessHour _bh = _pponProv.BusinessHourGet(vcd.BusinessHourGuid);

                        //是否為上架費檔次
                        if (!IsSlottingFeeQuantity)
                        {
                            DealCost dc = _pponProv.DealCostGetList(vcd.BusinessHourGuid).OrderBy(x => x.Quantity).LastOrDefault();
                            if (dc != null)
                            {
                                if (dc.LowerCumulativeQuantity > 0)
                                {
                                    IsSlottingFeeQuantity = true;
                                }
                            }
                        }

                        //是否為通用券檔次
                        if (!IsNoRestrictedStore)
                        {
                            if (Helper.IsFlagSet(vcd.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore))
                            {
                                IsNoRestrictedStore = true;
                            }
                        }


                        if (vpdc.Count > 0)
                        {
                            ViewPponDeal vpd = vpdc.FirstOrDefault();
                            Delivery_type = (int)vpd.DeliveryType;
                            if (vpd != null)
                            {
                                if (newBid == Guid.Empty)
                                {
                                    newBid = vcd.BusinessHourGuid;
                                }

                                SumOrderTotalLimit += _bh.OrderTotalLimit ?? 0;


                                //取得已售，已核銷數量
                                List<VendorDealSalesCount> storeds = _verificationProv.GetPponVerificationSummaryGroupByStore(new List<Guid> { vpd.BusinessHourGuid });
                                if (storeds != null)
                                {
                                    //每個分店的核銷/已售數量
                                    Dictionary<Guid, decimal> StoreVerifiedDetail = new Dictionary<Guid, decimal>();
                                    Dictionary<Guid, decimal> StoreSaleDetail = new Dictionary<Guid, decimal>();
                                    Dictionary<Guid, decimal> StoreReturnDetail = new Dictionary<Guid, decimal>();
                                    int NoRestrictedStoreSale = 0;
                                    int NoRestrictedStoreReturn = 0;
                                    if (vpd.DeliveryType == (int)DeliveryType.ToShop)
                                    {
                                        #region 憑證
                                        foreach (var vdsc in storeds)
                                        {
                                            if (vdsc.StoreGuid != null)
                                            {
                                                if (StoreVerifiedDetail.ContainsKey((Guid)vdsc.StoreGuid))
                                                {
                                                    StoreVerifiedDetail[(Guid)vdsc.StoreGuid] += vdsc.VerifiedCount;
                                                }
                                                else
                                                {
                                                    StoreVerifiedDetail.Add((Guid)vdsc.StoreGuid, vdsc.VerifiedCount);
                                                }

                                                if (!IsNoRestrictedStore)
                                                {
                                                    if (StoreSaleDetail.ContainsKey((Guid)vdsc.StoreGuid))
                                                    {
                                                        StoreSaleDetail[(Guid)vdsc.StoreGuid] += vdsc.UnverifiedCount + vdsc.VerifiedCount + vdsc.ReturnedCount;
                                                    }
                                                    else
                                                    {
                                                        StoreSaleDetail.Add((Guid)vdsc.StoreGuid, vdsc.UnverifiedCount + vdsc.VerifiedCount + vdsc.ReturnedCount);
                                                    }
                                                    if (StoreReturnDetail.ContainsKey((Guid)vdsc.StoreGuid))
                                                    {
                                                        StoreReturnDetail[(Guid)vdsc.StoreGuid] += vdsc.ReturnedCount;
                                                    }
                                                    else
                                                    {
                                                        StoreReturnDetail.Add((Guid)vdsc.StoreGuid, vdsc.ReturnedCount);
                                                    }
                                                }
                                            }

                                            if (IsNoRestrictedStore)
                                            {
                                                NoRestrictedStoreSale += vdsc.UnverifiedCount + vdsc.VerifiedCount + vdsc.ReturnedCount;
                                                NoRestrictedStoreReturn += vdsc.ReturnedCount;
                                            }
                                        }

                                        StoreVerified.Add(vpd.BusinessHourGuid, StoreVerifiedDetail);
                                        StoreSale.Add(vpd.BusinessHourGuid, StoreSaleDetail);
                                        StoreReturn.Add(vpd.BusinessHourGuid, StoreReturnDetail);

                                        //檔次所有已售、已核銷總和
                                        Verified.Add(vpd.BusinessHourGuid, StoreVerified[vpd.BusinessHourGuid].Sum(x => x.Value));
                                        if (!IsNoRestrictedStore)
                                        {
                                            Sale.Add(vpd.BusinessHourGuid, StoreSale[vpd.BusinessHourGuid].Sum(x => x.Value));
                                            Return.Add(vpd.BusinessHourGuid, StoreReturn[vpd.BusinessHourGuid].Sum(x => x.Value));
                                        }
                                        else
                                        {
                                            Sale.Add(vpd.BusinessHourGuid, NoRestrictedStoreSale);
                                            Return.Add(vpd.BusinessHourGuid, NoRestrictedStoreReturn);
                                        }
                                        #endregion
                                    }
                                    else if (vpd.DeliveryType == (int)DeliveryType.ToHouse)
                                    {
                                        #region 宅配
                                        foreach (var vdsc in storeds)
                                        {
                                            //宅配不區分分店，以檔次賣家來看即可
                                            if (StoreVerifiedDetail.ContainsKey(vpd.SellerGuid))
                                            {
                                                StoreVerifiedDetail[vpd.SellerGuid] += vdsc.VerifiedCount;
                                            }
                                            else
                                            {
                                                StoreVerifiedDetail.Add(vpd.SellerGuid, vdsc.VerifiedCount);
                                            }
                                            if (StoreSaleDetail.ContainsKey(vpd.SellerGuid))
                                            {
                                                StoreSaleDetail[vpd.SellerGuid] += vdsc.UnverifiedCount + vdsc.VerifiedCount + vdsc.ReturnedCount;
                                            }
                                            else
                                            {
                                                StoreSaleDetail.Add(vpd.SellerGuid, vdsc.UnverifiedCount + vdsc.VerifiedCount + vdsc.ReturnedCount);
                                            }
                                            if (StoreReturnDetail.ContainsKey(vpd.SellerGuid))
                                            {
                                                StoreReturnDetail[vpd.SellerGuid] += vdsc.ReturnedCount;
                                            }
                                            else
                                            {
                                                StoreReturnDetail.Add(vpd.SellerGuid, vdsc.ReturnedCount);
                                            }
                                        }

                                        StoreVerified.Add(vpd.BusinessHourGuid, StoreVerifiedDetail);
                                        StoreSale.Add(vpd.BusinessHourGuid, StoreSaleDetail);
                                        StoreReturn.Add(vpd.BusinessHourGuid, StoreReturnDetail);

                                        //檔次所有已售、已核銷總和
                                        Verified.Add(vpd.BusinessHourGuid, StoreVerified[vpd.BusinessHourGuid].Sum(x => x.Value));
                                        Sale.Add(vpd.BusinessHourGuid, StoreSale[vpd.BusinessHourGuid].Sum(x => x.Value));
                                        Return.Add(vpd.BusinessHourGuid, StoreReturn[vpd.BusinessHourGuid].Sum(x => x.Value));
                                        #endregion
                                    }
                                }
                                else
                                {
                                    StoreVerified.Add(vpd.BusinessHourGuid, null);
                                    Verified.Add(vpd.BusinessHourGuid, 0);
                                    StoreSale.Add(vpd.BusinessHourGuid, null);
                                    Sale.Add(vpd.BusinessHourGuid, 0);
                                    StoreReturn.Add(vpd.BusinessHourGuid, null);
                                    Return.Add(vpd.BusinessHourGuid, 0);
                                }

                                listData.Add(new ViewPponDeal()
                                {
                                    ItemName = vpd.ItemName,
                                    DealEmpName = vpd.DealEmpName,
                                    DevelopeSalesId = vpd.DevelopeSalesId,
                                    OperationSalesId = vpd.OperationSalesId,
                                    UniqueId = vpd.UniqueId ?? 0,
                                    BusinessHourGuid = vpd.BusinessHourGuid,
                                    OrderTotalLimit = (vpd.OrderTotalLimit ?? 0),
                                    BusinessHourOrderTimeS = vpd.BusinessHourOrderTimeS,
                                    BusinessHourOrderTimeE = vpd.BusinessHourOrderTimeE
                                });
                            }
                        }
                    }
                    #endregion
                }
                else
                {

                    #region 單檔次
                    ViewPponDealCollection vpdc = _pponProv.ViewPponDealGetListByPeriod(bid, DateTime.Now.AddDays(-360), DateTime.Now.AddDays(60));
                    if (vpdc.Count > 0)
                    {
                        ViewPponDeal vpd = vpdc.FirstOrDefault();
                        Delivery_type = (int)vpd.DeliveryType;
                        if (vpd != null)
                        {
                            newBid = vpd.BusinessHourGuid;

                            DealCost dc = _pponProv.DealCostGetList(newBid).OrderBy(x => x.Quantity).LastOrDefault();
                            if (dc != null)
                            {
                                if (dc.LowerCumulativeQuantity > 0)
                                {
                                    IsSlottingFeeQuantity = true;
                                }
                            }


                            if (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore))
                            {
                                IsNoRestrictedStore = true;
                            }


                            //取得已售，已核銷數量
                            List<VendorDealSalesCount> storeds = _verificationProv.GetPponVerificationSummaryGroupByStore(new List<Guid> { vpd.BusinessHourGuid });
                            if (storeds != null)
                            {
                                //分店的核銷數量
                                Dictionary<Guid, decimal> StoreVerifiedDetail = new Dictionary<Guid, decimal>();
                                Dictionary<Guid, decimal> StoreSaleDetail = new Dictionary<Guid, decimal>();
                                Dictionary<Guid, decimal> StoreReturnDetail = new Dictionary<Guid, decimal>();
                                int NoRestrictedStoreSale = 0;
                                int NoRestrictedStoreReturn = 0;
                                if (vpd.DeliveryType == (int)DeliveryType.ToShop)
                                {
                                    #region 憑證
                                    foreach (var vdsc in storeds)
                                    {
                                        if (vdsc.StoreGuid != null)
                                        {
                                            if (StoreVerifiedDetail.ContainsKey((Guid)vdsc.StoreGuid))
                                            {
                                                StoreVerifiedDetail[(Guid)vdsc.StoreGuid] += vdsc.VerifiedCount;
                                            }
                                            else
                                            {
                                                StoreVerifiedDetail.Add((Guid)vdsc.StoreGuid, vdsc.VerifiedCount);
                                            }

                                            if (!IsNoRestrictedStore)
                                            {
                                                if (StoreSaleDetail.ContainsKey((Guid)vdsc.StoreGuid))
                                                {
                                                    StoreSaleDetail[(Guid)vdsc.StoreGuid] += vdsc.UnverifiedCount + vdsc.VerifiedCount + vdsc.ReturnedCount;
                                                }
                                                else
                                                {
                                                    StoreSaleDetail.Add((Guid)vdsc.StoreGuid, vdsc.UnverifiedCount + vdsc.VerifiedCount + vdsc.ReturnedCount);
                                                }
                                                if (StoreReturnDetail.ContainsKey((Guid)vdsc.StoreGuid))
                                                {
                                                    StoreReturnDetail[(Guid)vdsc.StoreGuid] += vdsc.ReturnedCount;
                                                }
                                                else
                                                {
                                                    StoreReturnDetail.Add((Guid)vdsc.StoreGuid, vdsc.ReturnedCount);
                                                }
                                            }
                                        }

                                        if (IsNoRestrictedStore)
                                        {
                                            NoRestrictedStoreSale += vdsc.UnverifiedCount + vdsc.VerifiedCount + vdsc.ReturnedCount;
                                            NoRestrictedStoreReturn += vdsc.ReturnedCount;
                                        }

                                    }

                                    StoreVerified.Add(vpd.BusinessHourGuid, StoreVerifiedDetail);
                                    StoreSale.Add(vpd.BusinessHourGuid, StoreSaleDetail);
                                    StoreReturn.Add(vpd.BusinessHourGuid, StoreReturnDetail);

                                    //檔次所有已售、已核銷總和
                                    Verified.Add(vpd.BusinessHourGuid, StoreVerified[vpd.BusinessHourGuid].Sum(x => x.Value));
                                    if (!IsNoRestrictedStore)
                                    {
                                        Sale.Add(vpd.BusinessHourGuid, StoreSale[vpd.BusinessHourGuid].Sum(x => x.Value));
                                        Return.Add(vpd.BusinessHourGuid, StoreReturn[vpd.BusinessHourGuid].Sum(x => x.Value));
                                    }
                                    else
                                    {
                                        Sale.Add(vpd.BusinessHourGuid, NoRestrictedStoreSale);
                                        Return.Add(vpd.BusinessHourGuid, NoRestrictedStoreReturn);
                                    }
                                    #endregion
                                }
                                else if (vpd.DeliveryType == (int)DeliveryType.ToHouse)
                                {
                                    #region 宅配
                                    foreach (var vdsc in storeds)
                                    {
                                        //宅配不區分分店，以檔次賣家來看即可
                                        if (StoreVerifiedDetail.ContainsKey(vpd.SellerGuid))
                                        {
                                            StoreVerifiedDetail[vpd.SellerGuid] += vdsc.VerifiedCount;
                                        }
                                        else
                                        {
                                            StoreVerifiedDetail.Add(vpd.SellerGuid, vdsc.VerifiedCount);
                                        }
                                        if (StoreSaleDetail.ContainsKey(vpd.SellerGuid))
                                        {
                                            StoreSaleDetail[vpd.SellerGuid] += vdsc.UnverifiedCount + vdsc.VerifiedCount + vdsc.ReturnedCount;
                                        }
                                        else
                                        {
                                            StoreSaleDetail.Add(vpd.SellerGuid, vdsc.UnverifiedCount + vdsc.VerifiedCount + vdsc.ReturnedCount);
                                        }
                                        if (StoreReturnDetail.ContainsKey(vpd.SellerGuid))
                                        {
                                            StoreReturnDetail[vpd.SellerGuid] += vdsc.ReturnedCount;
                                        }
                                        else
                                        {
                                            StoreReturnDetail.Add(vpd.SellerGuid, vdsc.ReturnedCount);
                                        }
                                    }

                                    StoreVerified.Add(vpd.BusinessHourGuid, StoreVerifiedDetail);
                                    StoreSale.Add(vpd.BusinessHourGuid, StoreSaleDetail);
                                    StoreReturn.Add(vpd.BusinessHourGuid, StoreReturnDetail);

                                    //檔次所有已售、已核銷總和
                                    Verified.Add(vpd.BusinessHourGuid, StoreVerified[vpd.BusinessHourGuid].Sum(x => x.Value));
                                    Sale.Add(vpd.BusinessHourGuid, StoreSale[vpd.BusinessHourGuid].Sum(x => x.Value));
                                    Return.Add(vpd.BusinessHourGuid, StoreReturn[vpd.BusinessHourGuid].Sum(x => x.Value));
                                    #endregion
                                }
                            }
                            else
                            {
                                StoreVerified.Add(vpd.BusinessHourGuid, null);
                                Verified.Add(vpd.BusinessHourGuid, 0);
                                StoreSale.Add(vpd.BusinessHourGuid, null);
                                Sale.Add(vpd.BusinessHourGuid, 0);
                                StoreReturn.Add(vpd.BusinessHourGuid, null);
                                Return.Add(vpd.BusinessHourGuid, 0);
                            }

                            listData.Add(new ViewPponDeal()
                            {
                                ItemName = vpd.ItemName,
                                DealEmpName = vpd.DealEmpName,
                                DevelopeSalesId = vpd.DevelopeSalesId,
                                OperationSalesId = vpd.OperationSalesId,
                                UniqueId = vpd.UniqueId ?? 0,
                                BusinessHourGuid = vpd.BusinessHourGuid,
                                OrderTotalLimit = (vpd.OrderTotalLimit ?? 0),
                                BusinessHourOrderTimeS = vpd.BusinessHourOrderTimeS,
                                BusinessHourOrderTimeE = vpd.BusinessHourOrderTimeE
                            });
                        }
                    }
                    #endregion
                }
                BusinessHour bh = _pponProv.BusinessHourGet(newBid);
                PponOptionCollection poc = _pponProv.PponOptionGetList(newBid);
                SellerCollection stc = _sellerProv.SellerGetListBySellerGuidList(_sellerProv.SellerTreeGetListByParentSellerGuid(bh.SellerGuid).Select(x => x.SellerGuid).ToList());
                stc.Add(_sellerProv.SellerGet(bh.SellerGuid));

                PponStoreCollection psc = _pponProv.PponStoreGetListByBusinessHourGuid(newBid);
                PponOptionLogCollection polc = _pponProv.PponOptionLogGet(newBid);

                List<Guid> pscIds = psc.Select(x => x.StoreGuid).ToList();

                if (MainBusinessHourGuid != Guid.Empty)
                {
                    BusinessHour mbh = _pponProv.BusinessHourGet(MainBusinessHourGuid);
                    MainOrderTotalLimit = mbh.OrderTotalLimit ?? 0;
                }

                OrderTotalLimit = bh.OrderTotalLimit ?? 0;

                DeptId = _pponProv.DealAccountingGet(newBid).DeptId;
                isConsignment = (bool)_pponProv.DealPropertyGet(newBid).Consignment;

                pos.IsClosed = false;
                if (bh.BusinessHourOrderTimeE <= DateTime.Now || bh.BusinessHourOrderTimeS >= DateTime.Now)
                {
                    pos.IsClosed = true;
                }

                pos.BusinessHourGuid = newBid;
                pos.DeptId = DeptId;
                pos.multiDeals = multiDeals;
                pos.PponDeals = listData;
                pos.pponOption = poc;
                pos.pponOptionLog = polc;
                if (Delivery_type == (int)DeliveryType.ToShop)
                {
                    pos.store = stc.Where(x => pscIds.Contains(x.Guid)).ToList();
                }
                else if (Delivery_type == (int)DeliveryType.ToHouse)
                {
                    pos.store = stc.Where(x => x.Guid == bh.SellerGuid).ToList();
                }
                pos.pponStores = psc;
                pos.MainOrderTotalLimit = MainOrderTotalLimit;
                pos.OrderTotalLimit = OrderTotalLimit;
                pos.SumOrderTotalLimit = (SumOrderTotalLimit > 99999 ? 99999 : SumOrderTotalLimit);
                pos.IsSlottingFeeQuantity = IsSlottingFeeQuantity;
                pos.IsNoRestrictedStore = IsNoRestrictedStore;
                pos.StoreVerified = StoreVerified;
                pos.Verified = Verified;
                pos.StoreSale = StoreSale;
                pos.Sale = Sale;
                pos.Return = Return;
                pos.IsConsignment = config.IsConsignment ? isConsignment : false;
            }
            return pos;
        }

        public static bool PponOptionItemsSet(string _bid,
            List<storeList> stores,
            List<QuantityList> Quantitys,
            List<OrderTotalLimitList> OrderTotalLimits,
            string UserName)
        {
            bool flag = true;
            PponOptionLogCollection poc = new PponOptionLogCollection();
            Guid bid = Guid.Empty;
            Guid.TryParse(_bid, out bid);

            //分店更新上限
            foreach (var o in stores)
            {
                Guid sid = Guid.Empty;
                Guid.TryParse(o.Sid, out sid);
                Guid guid = Guid.Empty;
                Guid.TryParse(o.Bid, out guid);
                PponStore ps = _pponProv.PponStoreGet(guid, sid);
                PponStore orips = ps.Clone();
                if (ps != null)
                {
                    int _TotalQuantity = 0;
                    int.TryParse(o.TotalQuantity, out _TotalQuantity);
                    if (orips.TotalQuantity != _TotalQuantity)
                    {
                        ps.TotalQuantity = _TotalQuantity;
                        _pponProv.PponStoreSet(ps);
                        poc.Add(new PponOptionLog()
                        {
                            BusinessHourGuid = bid,
                            LogContent = "異動分店上限：" + orips.StoreGuid.ToString() + "|" + orips.TotalQuantity.ToString() + "變更為：" + _TotalQuantity.ToString(),
                            ModifyTime = DateTime.Now,
                            ModifyUser = UserName
                        });
                    }
                }
            }

            ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(bid);
            ViewItemAccessoryGroupCollection vagc = _itemProv.ViewItemAccessoryGroupGetList(vpd.ItemGuid);


            //多重選項更新可售數量
            foreach (var o in Quantitys)
            {
                int _Id = 0;
                int.TryParse(o.Id, out _Id);
                PponOption po = _pponProv.PponOptionGet(_Id);
                PponOption oripo = po.Clone();
                if (po != null)
                {
                    int _Quantity = 0;
                    int.TryParse(o.Quantity, out _Quantity);
                    if (_Quantity != 0)
                    {
                        if (oripo.Quantity != _Quantity)
                        {
                            po.Quantity = _Quantity;
                            _pponProv.PponOptionSet(po);

                            List<ViewItemAccessoryGroup> vag = vagc.Where(x => x.AccessoryName == po.OptionName).ToList();
                            if (vag.Count > 0)
                            {
                                AccessoryGroupMember agm = _itemProv.AccessoryGroupMemberGet(vag.FirstOrDefault().AccessoryGroupMemberGuid);
                                agm.Quantity = _Quantity;
                                _itemProv.AccessoryGroupMemberSet(agm);
                            }

                            poc.Add(new PponOptionLog()
                            {
                                BusinessHourGuid = bid,
                                LogContent = "異動多重選項可售數量：" + oripo.BusinessHourGuid.ToString() + "|" + oripo.Quantity.ToString() + "變更為：" + _Quantity.ToString(),
                                ModifyTime = DateTime.Now,
                                ModifyUser = UserName
                            });
                        }
                    }

                }
            }

            //更新最大購買量
            foreach (var o in OrderTotalLimits)
            {
                Guid guid = Guid.Empty;
                Guid.TryParse(o.Bid, out guid);
                if (guid != Guid.Empty)
                {
                    BusinessHour bh = _pponProv.BusinessHourGet(guid);
                    BusinessHour oribh = bh.Clone();
                    if (bh != null)
                    {
                        int _OrderTotalLimit = 0;
                        int.TryParse(o.OrderTotalLimit, out _OrderTotalLimit);
                        if (oribh.OrderTotalLimit != _OrderTotalLimit)
                        {
                            bh.OrderTotalLimit = _OrderTotalLimit;
                            _pponProv.BusinessHourSet(bh);
                            poc.Add(new PponOptionLog()
                            {
                                BusinessHourGuid = bid,
                                LogContent = "異動最大購買量：" + bid.ToString() + "|" + oribh.OrderTotalLimit.ToString() + "變更為：" + _OrderTotalLimit.ToString(),
                                ModifyTime = DateTime.Now,
                                ModifyUser = UserName
                            });
                        }
                    }
                }
            }

            if (poc.Count > 0)
            {
                _pponProv.PponOptionLogSetList(poc);
            }

            return flag;
        }

        public static string PponNewContentGet(Guid bid)
        {
            ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(bid);
            return PponNewContentGetByDeal(vpd);
        }
        public static string PponNewContentGetByDeal(IViewPponDeal vpd)
        {
            string newEventName = string.Empty;
            if (vpd != null)
            {
                //前台不顯示內文就不用置換了
                if (vpd.IsHideContent != null && vpd.IsHideContent == false)
                {
                    newEventName = vpd.EventName;
                    if (!string.IsNullOrEmpty(vpd.ContentName))
                    {
                        newEventName = vpd.EventName;
                    }
                }
                else
                {
                    newEventName = vpd.EventName + vpd.DealPromoTitle;
                }
            }
            return newEventName;
        }

        /// <summary>
        /// 系統組出黑標內容
        /// </summary>
        /// <param name="bid">檔次guid</param>
        /// <param name="RreceiveType">消費方式</param>
        /// <param name="Intro">引言</param>
        /// <param name="PriceDesc">價格描述</param>
        /// <param name="tbUse">短標</param>
        /// <param name="OrigPrice">原價</param>
        /// <param name="ItemPrice">賣價</param>
        /// <param name="IsAveragePrice">均價</param>
        /// <param name="deliveryDealTag">含運</param>
        /// <returns></returns>
        public static string PponContentGet(
            Guid bid,
            int RreceiveType,
            string Intro,
            string PriceDesc,
            string tbUse,
            int OrigPrice,
            int ItemPrice,
            bool IsAveragePrice,
            bool deliveryDealTag
            )
        {
            string res = "";

            bool bAveragePrice = checkAveragePrice(bid);
            string firstWord = "";
            string secondWord = "";
            if (!string.IsNullOrEmpty(tbUse))
            {
                string[] tbUses = tbUse.Split("-");
                if (tbUses.Length > 1)
                {
                    firstWord = "【" + tbUses[0] + "】";
                    for (int i = 0; i < tbUses.Length; i++)
                    {
                        if (i == 0)
                        {
                            continue;
                        }
                        secondWord += tbUses[i] + "-";
                    }
                    if (secondWord.Length > 1)
                    {
                        secondWord = secondWord.Substring(0, secondWord.Length - 1);
                    }
                }
                else
                {
                    secondWord = tbUses[0];
                }
            }
            PriceDesc = (PriceDesc != "" ? "(" + PriceDesc + ")" : "");

            var comboDealMain = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            if ((IsAveragePrice || bAveragePrice) && (RreceiveType == (int)DeliveryType.ToHouse))
            {
                /*
                 * 均價(宅配) 若檔次中只要有一子檔均價勾勾被勾起，則該檔次黑標以下述顯示為主
                 * */
                if ((comboDealMain.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                {
                    //多檔次-代表檔                    
                    //[A欄]平均最低只要[子檔中最低售價]元起([B欄])[含運勾勾]即可享有【[代表檔訂單短標中第一個"-"前文字]】[代表檔訂單短標中第一個"-"後文字]

                    //取得均價
                    var vcds = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(bid);
                    List<Guid> gids = vcds.Select(x => x.BusinessHourGuid).ToList();
                    List<decimal> avgPrice = new List<decimal>();
                    List<IViewPponDeal> vpdc = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(gids);
                    foreach (IViewPponDeal vpd in vpdc)
                    {
                        decimal QuantityMultiplier = 1;
                        if (vpd.QuantityMultiplier != null)
                        {
                            decimal.TryParse(vpd.QuantityMultiplier.ToString(), out QuantityMultiplier);
                        }
                        if (QuantityMultiplier != 0)
                        {
                            avgPrice.Add(Math.Round(vpd.ItemPrice / QuantityMultiplier, 0, MidpointRounding.AwayFromZero));
                        }
                    }

                    res = string.Format("{0}平均最低只要{1}元起{2}{3}即可享有{4}{5}",
                        Intro,
                        avgPrice.Min().ToString("N0"),
                        (deliveryDealTag ? "" : PriceDesc),
                        (deliveryDealTag ? "(含運)" : ""),
                        firstWord,
                        secondWord);
                }
                else
                {
                    //單檔/多檔次-子檔
                    //[A欄]只要[售價]元([B欄])[含運勾勾]即可享有【[訂單短標中第一個"-"前文字]】原價[原價]元[訂單短標中第一個"-"後文字]：
                    res = string.Format("{0}只要{1}元{2}{3}即可享有{4}{5}{6}",
                        Intro,
                        ItemPrice.ToString("N0"),
                        (deliveryDealTag ? "" : PriceDesc),
                        (deliveryDealTag ? "(含運)" : ""),
                        firstWord,
                        (OrigPrice == ItemPrice ? "" : "原價" + OrigPrice.ToString("N0") + "元"),//若原價等於售價，則不帶原價
                        secondWord);
                }
            }
            else
            {
                /*
                 * 一般次檔(無均價，含好康、宅配、旅遊....)
                 * */
                if ((comboDealMain.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                {

                    //多檔次-代表檔
                    //只要[子檔中最低售價]元([B欄])起[含運勾勾]即可享有【[代表檔訂單短標"-"前文字]】原價最高[子檔中最高原價]元[代表檔訂單短標"-"後文字]：                    
                    var subDeals = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(bid);
                    if (subDeals.Select(x => x.ItemPrice).Min().ToString("N0") == subDeals.Select(x => x.ItemPrice).Max().ToString("N0"))
                    {
                        //多檔次同售價、同原價，黑標產生器需調整(不用「起」和「最高」)
                        res = string.Format("{0}只要{1}元{2}{3}即可享有{4}原價{5}元{6}",
                               Intro,
                               subDeals.Select(x => x.ItemPrice).Min().ToString("N0"),
                               PriceDesc,
                               (deliveryDealTag ? "(含運)" : ""),
                               firstWord,
                               subDeals.Select(x => x.ItemOrigPrice).Max().ToString("N0"),
                               secondWord);
                    }
                    else
                    {
                        res = string.Format("{0}只要{1}元{2}起{3}即可享有{4}原價最高{5}元{6}",
                               Intro,
                               subDeals.Select(x => x.ItemPrice).Min().ToString("N0"),
                               PriceDesc,
                               (deliveryDealTag ? "(含運)" : ""),
                               firstWord,
                               subDeals.Select(x => x.ItemOrigPrice).Max().ToString("N0"),
                               secondWord);
                    }
                }
                else
                {
                    //單檔/多檔次-子檔
                    //只要[售價]元([B欄])[含運勾勾]即可享有【[訂單短標中第一個"-"前文字]】原價[原價]元[訂單短標中第一個"-"後文字]：
                    res = string.Format("{0}只要{1}元{2}{3}即可享有{4}{5}{6}",
                        Intro,
                        ItemPrice.ToString("N0"),
                        PriceDesc,
                        (deliveryDealTag ? "(含運)" : ""),
                        firstWord,
                        (OrigPrice == ItemPrice ? "" : "原價" + OrigPrice.ToString("N0") + "元"),//若原價等於售價，則不帶原價
                        secondWord);
                }
            }
            return replaceSpecCharacter(res);
        }

        public static void PponContentSet(Guid bid, string content_name)
        {
            CouponEventContent cec = _pponProv.CouponEventContentGetByBid(bid);
            cec.ContentName = content_name;
            cec.ModifyTime = DateTime.Now;
            _pponProv.CouponEventContentSet(cec);
        }

        /// <summary>
        /// 前台黑標
        /// </summary>
        /// <param name="oriEventName"></param>
        /// <param name="newEventName"></param>
        /// <param name="IsHideContent">前台不顯示內文</param>
        /// <returns></returns>
        public static string PponContentCombine(string oriEventName, string newEventName, bool IsHideContent)
        {
            return (IsHideContent ? oriEventName : newEventName + (oriEventName.Trim() != "" ? "" + replaceSpecCharacter(oriEventName) : "。"));
        }

        private static string replaceSpecCharacter(string s)
        {
            return s.Replace("（", "").Replace("）", "").Replace("﹝", "").Replace("﹞", "");
        }


        public static string EventNameGet(IViewPponDeal vpd)
        {
            //前台不顯示內文就不用置換了
            string cEventName = vpd.EventName;
            if (vpd.IsHideContent != null && vpd.IsHideContent == false)
            {
                bool fare = false;
                if (vpd.LabelTagList != null)
                {
                    if (vpd.LabelTagList.Contains(((int)DealLabelSystemCode.IncludedDeliveryCharge).ToString()))
                    {
                        fare = true;
                    }
                }

                string newEventName = PponFacade.PponContentGet(vpd.BusinessHourGuid, vpd.DeliveryType ?? 0, vpd.Intro,
                vpd.PriceDesc, vpd.CouponUsage,
                (int)vpd.ItemOrigPrice, (int)vpd.ItemPrice,
                vpd.IsAveragePrice ?? false,
                fare);

                cEventName = PponFacade.PponContentCombine(vpd.EventName, newEventName, vpd.IsHideContent ?? true);
            }
            return cEventName;
        }

        /// <summary>
        /// 檢查檔次中只要有一子檔均價勾勾被勾起
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        private static bool checkAveragePrice(Guid bid)
        {
            bool bAveragePrice = false;
            ViewComboDealCollection vcdcs = _pponProv.GetViewComboDealAllByBid(bid);
            foreach (ViewComboDeal v in vcdcs)
            {
                //var deal = _pponProv.DealPropertyGet(v.BusinessHourGuid);
                var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(v.BusinessHourGuid);
                if (deal.IsAveragePrice == true)
                {
                    bAveragePrice = true;
                    break;
                }
            }
            return bAveragePrice;
        }

        /// <summary>
        /// 開關單規格的最大購買數量(不用處理多重選項，因為多重選項是鎖在下拉)
        /// </summary>
        /// <param name="itemGuid"></param>
        /// <returns></returns>
        public static bool UpdateDealMaxItemCount(IViewPponDeal theDeal, Guid itemGuid)
        {
            if (theDeal != null && !theDeal.IsHouseDealNewVersion())
            {
                return false;
            }
            try
            {

                ViewComboDealCollection comboDeals = _pponProv.GetViewComboDealAllByBid(theDeal.BusinessHourGuid);
                if (comboDeals.Count > 0)
                {
                    //母子檔
                    foreach (ViewComboDeal combo in comboDeals)
                    {
                        UpdateDealOrderTotalLimit(combo);
                    }
                }
                else
                {
                    UpdateDealOrderTotalLimit(theDeal);
                }
            }
            catch (Exception ex)
            {
                logger.Error("更新庫存失敗，itemGuid=" + itemGuid + "||" + ex);
            }

            return true;
        }

        private static bool UpdateDealOrderTotalLimit(ViewComboDeal theCombol)
        {
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(theCombol.BusinessHourGuid);
            return UpdateDealOrderTotalLimit(theDeal);
        }
        private static bool UpdateDealOrderTotalLimit(IViewPponDeal theDeal)
        {
            if (theDeal.Slug != null)
            {
                //已結檔，不須處理
                return false;
            }
            if (Helper.IsFlagSet(theDeal.GroupOrderStatus.GetValueOrDefault(), GroupOrderStatus.Completed))
            {
                //已結檔，不須處理
                return false;
            }
            int quantityMultiplier = theDeal.QuantityMultiplier.GetValueOrDefault(1);

            Guid mainBid = theDeal.BusinessHourGuid;
            bool isCombol = false;
            ViewComboDealCollection comboDeals = _pponProv.GetViewComboDealAllByBid(theDeal.BusinessHourGuid);
            if (comboDeals.Count > 0)
            {
                ViewComboDeal comboDeal = comboDeals.Where(x => x.MainBusinessHourGuid == x.BusinessHourGuid).FirstOrDefault();
                if (comboDeal != null)
                {
                    mainBid = comboDeal.BusinessHourGuid;
                }
                isCombol = true;
            }
            ProposalMultiOptionSpecCollection optionSpecs = _sellerProv.ProposalMultiOptionSpecGetByBid(mainBid);

            StringBuilder logBuilder = new StringBuilder();
            if (theDeal.ShoppingCart.GetValueOrDefault(false) == false)
            {
                #region 非多重選項
                logBuilder.AppendLine("[非多重選項]");
                Guid item_Guid = Guid.Empty;
                int stock = 0;
                if (optionSpecs.Count > 0)
                {
                    logBuilder.AppendLine("optionSpecs.Count > 0");
                    if (isCombol && mainBid == theDeal.BusinessHourGuid)
                    {
                        logBuilder.AppendLine("[A1]");
                        stock = 99999;
                        logBuilder.AppendLine("[stock = 99999]");
                    }
                    else
                    {
                        logBuilder.AppendLine("[A2]");
                        ProposalMultiDeal pmd = _sellerProv.ProposalMultiDealGetAllByBid(theDeal.BusinessHourGuid);
                        logBuilder.AppendLine("[pmd]" + new JsonSerializer().Serialize(pmd));
                        if (pmd != null && pmd.IsLoaded)
                        {
                            logBuilder.AppendLine("[pmd.IsLoaded]");

                            List<ProposalMultiOptionSpec> sc = optionSpecs.Where(x => x.MultiOptionId == pmd.Id).ToList();
                            logBuilder.AppendLine("[sc]" + new JsonSerializer().Serialize(sc));
                            foreach (ProposalMultiOptionSpec s in sc)
                            {
                                item_Guid = s.ItemGuid;
                                ProductItem proItem = _pponProv.ProductItemGet(item_Guid);
                                logBuilder.AppendLine("[proItem]" + new JsonSerializer().Serialize(proItem));
                                if (proItem.IsLoaded)
                                {
                                    if (stock < proItem.Stock)
                                    {
                                        logBuilder.AppendLine("[stock]" + proItem.Stock);
                                        stock = proItem.Stock;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    logBuilder.AppendLine("[return false]");
                    return false;
                }
                BusinessHour bh = _sellerProv.BusinessHourGet(theDeal.BusinessHourGuid);
                decimal avgQuantity = 0;
                if (quantityMultiplier > 0)
                {
                    avgQuantity = Math.Floor(Convert.ToDecimal(stock.ToString()) / Convert.ToDecimal(quantityMultiplier.ToString()));
                }

                logBuilder.AppendLine("[stock]" + stock);
                logBuilder.AppendLine("[avgQuantity]" + avgQuantity);
                logBuilder.AppendLine("[quantityMultiplier]" + quantityMultiplier);

                if (stock == 0)
                {
                    //無庫存
                    logBuilder.AppendLine("[B1]");
                    bh.OrderTotalLimit = 0;
                }
                else if (stock <= quantityMultiplier)
                {
                    logBuilder.AppendLine("[B2]");
                    if (stock < quantityMultiplier)
                    {
                        //庫存<倍數
                        //例如庫存剩2，但是每次需要買4個，等於不能買，故更新為0
                        logBuilder.AppendLine("[B3]");
                        bh.OrderTotalLimit = 0;
                    }
                    //else if (stock > avgQuantity)
                    //{
                    //    //庫存<倍數
                    //    //例如庫存剩2，但是買一送一，所以只能買一個(買一個就出兩個，扣兩個庫存)
                    //    bh.OrderTotalLimit = avgQuantity;
                    //}
                    else
                    {
                        logBuilder.AppendLine("[B4]");
                        bh.OrderTotalLimit = 99999;
                    }
                }
                else
                {
                    logBuilder.AppendLine("[B5]");
                    bh.OrderTotalLimit = 99999;
                }
                _sellerProv.BusinessHourSet(bh);

                Item item = _itemProv.ItemGetByBid(bh.Guid);
                if (item.IsLoaded)
                {
                    logBuilder.AppendLine("[C1]");
                    List<Guid> bids = new List<Guid>();
                    bids.Add(mainBid);
                    Proposal pro = _sellerProv.ProposalGetByBids(bids).FirstOrDefault();
                    if (pro != null)
                    {
                        logBuilder.AppendLine("[pro]" + pro.Id);
                        ProposalMultiDeal pmdModel = _sellerProv.ProposalMultiDealGetByPid(pro.Id)
                                                                .Where(x => x.Bid == theDeal.BusinessHourGuid).FirstOrDefault();

                        logBuilder.AppendLine("[pmdModel]" + new JsonSerializer().Serialize(pmdModel));

                        if (pmdModel != null)
                        {
                            if (avgQuantity <= 1)
                            {
                                avgQuantity = 1;
                            }
                            item.ItemDefaultDailyAmount = pmdModel.OrderMaxPersonal;
                            if (avgQuantity < item.ItemDefaultDailyAmount)
                            {
                                item.ItemDefaultDailyAmount = Convert.ToInt32(avgQuantity);
                            }
                            _itemProv.ItemSet(item);
                        }
                    }
                }
                logBuilder.AppendLine("更新:" + theDeal.BusinessHourGuid.ToString() + "/數量:" + bh.OrderTotalLimit.GetValueOrDefault(0));
                ProposalFacade.ProposalPerformanceLogSet("UpdateDealOrderTotalLimit", logBuilder.ToString(), "sys");
                #endregion 非多重選項
            }
            else
            {
                #region 多重選項
                logBuilder.AppendLine("[多重選項]");
                int totalStock = 0;
                ProposalMultiDeal pmd = _sellerProv.ProposalMultiDealGetAllByBid(theDeal.BusinessHourGuid);
                logBuilder.AppendLine("[pmd]" + new JsonSerializer().Serialize(pmd));
                if (pmd != null && pmd.IsLoaded)
                {
                    List<ProposalMultiOptionSpec> sc = optionSpecs.Where(x => x.MultiOptionId == pmd.Id).ToList();
                    logBuilder.AppendLine("[sc]" + new JsonSerializer().Serialize(sc));
                    foreach (ProposalMultiOptionSpec optionSpec in sc)
                    {
                        ProductItem proItem = _pponProv.ProductItemGet(optionSpec.ItemGuid);
                        logBuilder.AppendLine("[proItem]" + new JsonSerializer().Serialize(proItem));
                        if (proItem.IsLoaded)
                        {
                            logBuilder.AppendLine("[totalStock]" + totalStock);
                            logBuilder.AppendLine("[proItem.Stock]" + proItem.Stock);
                            totalStock += proItem.Stock;
                            if (totalStock > 0)
                            {
                                break;
                            }
                        }
                    }
                    logBuilder.AppendLine("[totalStock2]" + totalStock);
                    if (totalStock == 0)
                    {
                        BusinessHour bh = _sellerProv.BusinessHourGet(theDeal.BusinessHourGuid);
                        bh.OrderTotalLimit = 0;
                        _sellerProv.BusinessHourSet(bh);
                        logBuilder.AppendLine("更新:" + theDeal.BusinessHourGuid.ToString() + "/數量:" + bh.OrderTotalLimit.GetValueOrDefault(0));
                        ProposalFacade.ProposalPerformanceLogSet("UpdateDealOrderTotalLimit", logBuilder.ToString(), "sys");
                    }
                    else
                    {
                        BusinessHour bh = _sellerProv.BusinessHourGet(theDeal.BusinessHourGuid);
                        bh.OrderTotalLimit = 99999;
                        _sellerProv.BusinessHourSet(bh);
                        logBuilder.AppendLine("更新:" + theDeal.BusinessHourGuid.ToString() + "/數量:" + bh.OrderTotalLimit.GetValueOrDefault(0));
                        ProposalFacade.ProposalPerformanceLogSet("UpdateDealOrderTotalLimit", logBuilder.ToString(), "sys");
                    }
                }
                #endregion 多重選項
            }
            return true;
        }

        public static bool UpdateDealAllMaxItemCount(Guid infoGuid)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[Begin]");
            ProductInfo product = _pponProv.ProductInfoGet(infoGuid);
            ProductItemCollection items = _pponProv.ProductItemListGetByProductGuid(product.Guid);
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[ProductItemListGetByProductGuid]");

            List<Guid> bids = new List<Guid>();
            foreach (ProductItem item in items)
            {
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[a1]");
                List<int> optionIds = _sellerProv.ProposalMultiOptionSpecGetByItem(item.Guid)
                .Select(x => x.MultiOptionId).Distinct().ToList();

                foreach (int optionId in optionIds)
                {
                    ProposalMultiDeal multiDeal = _sellerProv.ProposalMultiDealGetById(optionId);
                    if (multiDeal.Bid != null)
                    {
                        if (bids.Contains(multiDeal.Bid.Value))
                        {
                            continue;
                        }
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[Bid]" + multiDeal.Bid.Value);
                        ViewComboDealCollection comboDeals = _pponProv.GetViewComboDealAllByBid(multiDeal.Bid.Value);
                        if (comboDeals.Count > 0)
                        {
                            //母子檔
                            foreach (ViewComboDeal combo in comboDeals)
                            {
                                UpdateDealOrderTotalLimit(combo);
                                bids.Add(combo.BusinessHourGuid);
                            }
                        }
                        else
                        {
                            //單檔
                            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(multiDeal.Bid.Value);
                            UpdateDealOrderTotalLimit(theDeal);
                            bids.Add(theDeal.BusinessHourGuid);
                        }
                    }
                }
            }
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[End]");
            ProposalFacade.ProposalPerformanceLogSet("UpdateDealAllMaxItemCount", builder.ToString(), "sys");
            return true;
        }


        public static string CreatePreviewMenu(Guid BusinessHourId, byte[] bytes, bool CloseMunuContent)
        {
            Bitmap oBitmap = null;
            Image oImage = null;
            int chgWidth = 560;
            string filePath = "";
            BusinessHour bh = _pponProv.BusinessHourGet(BusinessHourId);
            string sellerId = "";
            if (bh != null)
            {
                Seller seller = _sellerProv.SellerGet(bh.SellerGuid);
                sellerId = seller.SellerId;
            }
            string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/media/{0}/", sellerId));

            if (!System.IO.Directory.Exists(baseDirectoryPath))
            {
                System.IO.Directory.CreateDirectory(baseDirectoryPath);
            }

            byte[] data = (byte[])bytes.Clone();
            try
            {
                using (MemoryStream oMemoryStream = new MemoryStream(data))
                {
                    //設定資料流位置
                    oMemoryStream.Position = 0;
                    oImage = System.Drawing.Image.FromStream(oMemoryStream);
                    //建立副本
                    oBitmap = new Bitmap(oImage);
                    oBitmap.Save(baseDirectoryPath + BusinessHourId + "_Menu_Editor_S.png", ImageFormat.Png);
                }

                //縮圖
                System.Drawing.Image image = System.Drawing.Image.FromFile(baseDirectoryPath + BusinessHourId + "_Menu_Editor_S.png");
                int oriWidth = image.Width;
                int oriHeight = image.Height;
                //計算圖片寬高
                if (oriWidth > chgWidth)
                {
                    using (var newImage = new Bitmap(chgWidth, oriHeight))
                    {
                        newImage.SetResolution(image.HorizontalResolution, newImage.VerticalResolution);
                        var graphic = Graphics.FromImage(newImage);
                        graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                        graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

                        graphic.DrawImage(image, 0, 0, chgWidth, oriHeight);
                        using (var encoderParameters = new EncoderParameters(1))
                        {
                            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
                            newImage.Save(baseDirectoryPath + BusinessHourId + "_Menu_Editor.png",
                                ImageCodecInfo.GetImageEncoders().Where(x => x.FilenameExtension.Contains("png".ToUpperInvariant())).FirstOrDefault(),
                                encoderParameters);
                        }

                        graphic.Dispose();
                    }
                }
                else
                {
                    //不執行縮圖
                    oBitmap.Save(baseDirectoryPath + BusinessHourId + "_Menu_Editor.png", ImageFormat.Png);
                }
                image.Dispose();


                if (CloseMunuContent)
                {
                    //關閉Menu
                    if (File.Exists(baseDirectoryPath + BusinessHourId + "_Menu_Upload.png"))
                    {
                        System.Drawing.Image image1 = System.Drawing.Image.FromFile(baseDirectoryPath + BusinessHourId + "_Menu_Upload.png");
                        List<System.Drawing.Image> images = new List<System.Drawing.Image>();
                        images.Add(image1);

                        System.Drawing.Bitmap finalImage = new System.Drawing.Bitmap(chgWidth, image1.Height);

                        using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(finalImage))
                        {
                            g.Clear(System.Drawing.Color.White);

                            int offset = 0;
                            foreach (System.Drawing.Image img in images)
                            {
                                g.DrawImage(img,
                                  new System.Drawing.Rectangle(0, offset, img.Width, img.Height));
                                offset += img.Height;
                            }

                            finalImage.Save(baseDirectoryPath + BusinessHourId + "_Menu_tmp.png");
                        }

                        image1.Dispose();
                        finalImage.Dispose();

                        filePath = string.Format("{0}/{1}/{2}_Menu_tmp.png", config.MediaBaseUrl, sellerId, BusinessHourId);

                        CouponEventContent cec = _pponProv.CouponEventContentGetByBid(BusinessHourId);
                        if (cec != null)
                        {
                            cec.MenuImagePath = filePath;
                            _pponProv.CouponEventContentSet(cec);
                        }
                    }
                }
                else
                {
                    //將兩張圖片合併
                    if (File.Exists(baseDirectoryPath + BusinessHourId + "_Menu_Upload.png") && File.Exists(baseDirectoryPath + BusinessHourId + "_Menu_Editor.png"))
                    {
                        System.Drawing.Image image1 = System.Drawing.Image.FromFile(baseDirectoryPath + BusinessHourId + "_Menu_Upload.png");
                        System.Drawing.Image image2 = System.Drawing.Image.FromFile(baseDirectoryPath + BusinessHourId + "_Menu_Editor.png");
                        List<System.Drawing.Image> images = new List<System.Drawing.Image>();
                        images.Add(image1);
                        images.Add(image2);

                        System.Drawing.Bitmap finalImage = new System.Drawing.Bitmap(chgWidth, image1.Height + image2.Height);

                        using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(finalImage))
                        {
                            g.Clear(System.Drawing.Color.White);

                            int offset = 0;
                            foreach (System.Drawing.Image img in images)
                            {
                                g.DrawImage(img,
                                  new System.Drawing.Rectangle(0, offset, img.Width, img.Height));
                                offset += img.Height;
                            }

                            finalImage.Save(baseDirectoryPath + BusinessHourId + "_Menu_tmp.png");
                        }

                        image1.Dispose();
                        image2.Dispose();
                        finalImage.Dispose();

                        filePath = string.Format("{0}/{1}/{2}_Menu_tmp.png", config.MediaBaseUrl, sellerId, BusinessHourId);

                        CouponEventContent cec = _pponProv.CouponEventContentGetByBid(BusinessHourId);
                        if (cec != null)
                        {
                            cec.MenuImagePath = filePath;
                            _pponProv.CouponEventContentSet(cec);
                        }
                    }
                }


                //ImageUtility.UploadFile(new PhysicalPostedFileAdapter(baseDirectoryPath + View.BusinessHourId + "_Menu_tmp.png"),
                //    UploadFileType.PponEvent, sellerId, View.BusinessHourId + "_Menu.png", true);

                /*
                 * 刪除暫存檔案
                 * */
                if (File.Exists(baseDirectoryPath + BusinessHourId + "_Menu_Editor_S.png"))
                {
                    File.Delete(baseDirectoryPath + BusinessHourId + "_Menu_Editor_S.png");
                }
                if (File.Exists(baseDirectoryPath + BusinessHourId + "_Menu_Editor.png"))
                {
                    File.Delete(baseDirectoryPath + BusinessHourId + "_Menu_Editor.png");
                }

            }
            catch
            {
            }
            return filePath;
        }
        #endregion

        #region 台新分期判斷
        /// <summary>
        /// 台新分期判斷
        /// 帶入[憑證型態][不可分期是否打勾][賣價][進貨價][上架費份數]
        /// 回傳陣列結果:{[三期],[六期],[十二期]}
        /// returnInstallment[0]=三期;
        /// returnInstallment[1]=六期;
        /// returnInstallment[2]=十二期;
        /// 判斷方式:
        /// 1.只要不可分期打勾，回傳false,false,false
        /// 2.不可分期無打勾，取得毛利率
        ///   毛利率公式:(售價-進貨價)/售價
        ///   預設三期都開，回傳true,false,false
        ///   憑證型態為[好康]，且毛利率>=7% 或 [上架費份數]>0，開放六期，回傳六期為true
        ///   else
        ///   憑證型態為[宅配]
        ///   毛利率>=5%，開放六期，回傳六期為true
        ///   毛利率>=10%，開放十二期，回傳十二期為true
        /// </summary>
        /// <param name="deliveryType"></param>
        /// <param name="donotInstallment"></param>
        /// <param name="itemPrice"></param>
        /// <param name="purchasePrice"></param>
        /// <param name="slottingFeeQuantity"></param>
        /// <param name="getRate">多檔次傳入子檔最低毛利率</param>
        /// <returns></returns>
        public static bool[] DecideInstallment(DeliveryType deliveryType, bool doNotInstallment, decimal itemPrice, int slottingFeeQuantity, decimal getRate)
        {
            bool[] returnInstallment = new bool[3];
            if (doNotInstallment || itemPrice == 0)
            {
                returnInstallment[0] = false;
                returnInstallment[1] = false;
                returnInstallment[2] = false;
                return returnInstallment;
            }
            else
            {
                returnInstallment[0] = true;
                returnInstallment[1] = false;
                returnInstallment[2] = false;
                if (deliveryType == DeliveryType.ToShop)
                {
                    if (getRate >= 7 || slottingFeeQuantity > 0)
                    {
                        returnInstallment[1] = true;
                    }
                }
                else
                {
                    if (getRate >= 5)
                    {
                        returnInstallment[1] = true;
                    }
                    if (getRate >= 10)
                    {
                        returnInstallment[2] = true;
                    }
                }
                return returnInstallment;
            }
        }

        /// <summary>
        /// 當刷卡分期，驗證宅配的訂單金額是否滿足分期條件
        /// </summary>
        /// <param name="vpd"></param>
        /// <param name="installment"></param>
        /// <param name="orderAmount"></param>
        /// <returns></returns>
        public static bool CheckPayInstallmentValid(IViewPponDeal vpd, OrderInstallment installment, int orderAmount)
        {
            bool installment3months = vpd.Installment3months.GetValueOrDefault();
            bool installment6months = vpd.Installment6months.GetValueOrDefault();
            bool installment12months = vpd.Installment12months.GetValueOrDefault();
            if (installment == OrderInstallment.No)
            {
                return true;
            }
            if (installment == OrderInstallment.In3Months && installment3months)
            {
                return true;
            }
            if (installment == OrderInstallment.In6Months && installment6months)
            {
                if ((int)vpd.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    return orderAmount > 2000;
                }
                return true;
            }
            if (installment == OrderInstallment.In12Months && (installment12months || (!vpd.DenyInstallment.GetValueOrDefault() && DateTime.Now >= config.EnableInstallment12TimeS && DateTime.Now <= config.EnableInstallment12TimeE)))
            {
                //2016/5/10 [活動期間+毛利率>3%]
                return PponFacade.GetMinimumGrossMarginFromBids(vpd.BusinessHourGuid, true) * 100 > config.EnableInstallment12GrossMargin;
            }
            return false;
        }

        public static string GetDealItemBtnText(Guid bid)
        {
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            if (vpd.IsLoaded)
            {
                return GetDealItemBtnText(vpd);
            }
            return string.Empty;
        }

        public static string GetDealItemPiinDealCss(Guid bid)
        {
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            if (vpd.IsLoaded)
            {
                if (vpd.CategoryIds.Contains(CategoryManager.Default.Piinlife.CategoryId))
                {
                    return "btn-primary-flat-HD";
                }
            }
            return "btn-primary-flat";
        }

        public static string GetDealItemBtnText(IViewPponDeal pponDeal)
        {
            if (config.InstallmentPayEnabled == false || pponDeal.Installment3months.GetValueOrDefault() == false)
            {
                return "馬上買";
            }
            if (config.ShoppingCartV2Enabled)
            {
                if (PponFacade.GetDealIsColsed(pponDeal))
                {
                    return "已售完";
                }
                return "搶購";
            }
            return "分期0利率";
        }

        #endregion

        #region 成套商品相關

        public static ApiGroupCouponInfo GetApiGroupCouponInfo(IViewPponDeal theDeal)
        {
            var freeQty = theDeal.PresentQuantity ?? 0;
            var bougthQty = theDeal.SaleMultipleBase ?? 0 - freeQty;
            return new ApiGroupCouponInfo
            {
                FreeQuantity = freeQty,
                BoughtQuantity = bougthQty,
                MaxQuantity = 1
            };
        }

        /// <summary>
        /// 新版
        /// </summary>
        /// <param name="theDeal"></param>
        /// <returns></returns>
        public static ApiGroupCouponInfo GetApiGroupCouponInfoV2(IViewPponDeal theDeal)
        {
            var freeQty = theDeal.PresentQuantity ?? 0;
            return new ApiGroupCouponInfo
            {
                FreeQuantity = freeQty,
                BoughtQuantity = 1,
                MaxQuantity = 1
            };
        }

        /// <summary>
        /// 取得PDF短標(FOR成套禮券)
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string GetDealPdfItemName(Guid guid)
        {
            ComboDeal combodeals = _pponProv.GetComboDeal(guid);
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(guid);
            if (combodeals != null && combodeals.IsLoaded && string.IsNullOrEmpty(deal.PdfItemName))
            {
                deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(combodeals.MainBusinessHourGuid.Value);
                return deal.ItemName;
            }
            return !string.IsNullOrEmpty(deal.PdfItemName) ? deal.PdfItemName : deal.ItemName;
        }
        #endregion 成套商品相關

        #region skm

        /// <summary>
        /// 取得目前正在舉辦的活動
        /// </summary>
        /// <param name="mtype"></param>
        /// <returns></returns>
        public static List<MerchantEventPromo> MerchantEventPromoGetByToday(MerchantEventType mtype)
        {
            List<MerchantEventPromo> mepc = _pponProv.MerchantEventPromoListGetToday(mtype).Where(x => x.Status == (int)SkmAppStyleStatus.Normal).ToList();
            return mepc;
        }

        /// <summary>
        /// 新增/修改 SkmDealTimeSlot
        /// </summary>
        /// <param name="entity"></param>
        public static void SkmDealTimeSlotSet(PponDeal entity)
        {
            #region 產生SkmDealTimeSlot排程資料

            DateTime dateStartDay = entity.Deal.BusinessHourOrderTimeS;
            DateTime dateEndDay = entity.Deal.BusinessHourOrderTimeE;
            Guid bid = entity.Deal.Guid;

            if (bid == Guid.Empty)
            {
                return;
            }

            SkmDealTimeSlotCollection oldDealTimeSlotCol = _skmProv.SkmDealTimeSlotGetByBid(bid);

            var externalDeals = _pponProv.ViewExternalDealGetByBid(bid);
            if (string.IsNullOrEmpty(externalDeals.CategoryList) || string.IsNullOrEmpty(externalDeals.TagList))
            {
                //未勾選任何類別
                return;
            }
            if (externalDeals.Status < (int)SKMDealStatus.WaitingConfirm)
            {
                //尚未頁確，不產生檔案
                return;
            }
            string[] category = new JsonSerializer().Deserialize<string[]>(externalDeals.CategoryList);
            string[] tag = new JsonSerializer().Deserialize<string[]>(externalDeals.TagList);

            //檢查目前是否已有排程資料，若沒有依據輸入的啟始與截止日期自動產生
            if (oldDealTimeSlotCol.Count == 0) //Add
            {
                #region 依據輸入的啟始與截止日期自動產生
                //依據On檔的時間與地區產生DealTimeSlot的資料(檔期排程資料)
                //依序新增勾選區域的排程記錄
                SkmDealTimeSlotCollection newCol = new SkmDealTimeSlotCollection();
                for (var date = dateStartDay; date.Date <= dateEndDay.Date; date = date.AddDays(1))
                {
                    foreach (string t in tag)
                    {
                        SkmDealTimeSlot sdts = new SkmDealTimeSlot();
                        sdts.BusinessHourGuid = bid;
                        sdts.CityId = config.SkmCityId;
                        sdts.Sequence = 1;
                        sdts.EffectiveStart = DateTime.Parse(date.ToString("yyyy/MM/dd"));
                        sdts.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                        //skm_deal_time_slot只用預設的精選優惠(SkmFacade.SkmDefaultCategoryId)紀錄排序  
                        sdts.CategoryType = SkmFacade.SkmDefaultCategoryId;
                        sdts.DealType = Convert.ToInt32(t);
                        newCol.Add(sdts);
                    }
                }

                if (newCol.Any())
                {
                    _skmProv.SkmDealTimeSlotSaveAll(newCol);
                }

                #endregion 依據輸入的啟始與截止日期自動產生
            }
            else //若已有排程資料則需判斷是否需要修改
            {
                //刪除日期不符的資料
                List<SkmDealTimeSlot> delData = oldDealTimeSlotCol.Where(x => x.EffectiveStart < dateStartDay.Date || x.EffectiveStart > dateEndDay.Date).ToList();
                var delIds = delData.Select(p => p.Id).ToList();
                if (delIds.Any())
                {
                    _skmProv.SkmDealTimeSlotDeleteAll(delIds);//日期與類別舊資料一次刪
                }

                oldDealTimeSlotCol = _skmProv.SkmDealTimeSlotGetByBid(bid);
                SkmDealTimeSlotCollection newCol = new SkmDealTimeSlotCollection();
                for (var date = dateStartDay; date.Date <= dateEndDay.Date; date = date.AddDays(1))
                {
                    foreach (string t in tag)
                    {
                        SkmDealTimeSlot osdts = oldDealTimeSlotCol.Where(x => x.CategoryType == SkmFacade.SkmDefaultCategoryId
                                                                              && x.DealType == Convert.ToInt32(t)
                                                                              && x.EffectiveStart == date.Date).FirstOrDefault();
                        if (osdts == null)
                        {
                            SkmDealTimeSlot sdts = new SkmDealTimeSlot();
                            sdts.BusinessHourGuid = bid;
                            sdts.CityId = config.SkmCityId;
                            sdts.Sequence = -1;
                            sdts.EffectiveStart = DateTime.Parse(date.ToString("yyyy/MM/dd"));
                            sdts.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                            //skm_deal_time_slot只用預設的精選優惠(SkmFacade.SkmDefaultCategoryId)紀錄排序  
                            sdts.CategoryType = SkmFacade.SkmDefaultCategoryId;
                            sdts.DealType = Convert.ToInt32(t);
                            newCol.Add(sdts);
                        }
                    }
                }

                if (newCol.Any())
                {
                    _skmProv.SkmDealTimeSlotSaveAll(newCol);
                }
            }

            #endregion 產生SkmDealTimeSlot排程資料
        }
        #endregion skm

        #region 業務行事曆
        public static bool SalesCalendarEventSave(SalesCalendarEvent calendar)
        {
            bool flag = true;
            _pponProv.SalesCalendarEventSet(calendar);
            return flag;
        }
        public static SalesCalendarEvent SalesCalendarEventGet(int Id)
        {
            return _pponProv.SalesCalendarEventGet(Id);
        }
        #endregion

        #region brand

        public static List<string> GetBidByBrandId(int brandId)
        {
            var brand = _pponProv.GetBrand(brandId);
            return GetBidByBrandId(brand);
        }

        public static List<string> GetBidByBrandId(Brand brand)
        {
            var vepctmp = _pponProv.GetViewBrandCategoryByBrandId(brand.Id).Where(x => x.ItemStatus).OrderBy(x => x.Seq).OrderBy(x => x.BrandItemCategoryId).ToList();
            Guid gid;
            List<string> bannerList = new List<string>();
            if (!string.IsNullOrEmpty(brand.Mk1ActName) && Guid.TryParse(brand.Mk1ActName, out gid))
            {
                bannerList.Add(gid.ToString());
                if (!string.IsNullOrEmpty(brand.Mk2ActName) && Guid.TryParse(brand.Mk2ActName, out gid))
                {
                    bannerList.Add(gid.ToString());
                }
                if (!string.IsNullOrEmpty(brand.Mk3ActName) && Guid.TryParse(brand.Mk3ActName, out gid))
                {
                    bannerList.Add(gid.ToString());
                }
                if (!string.IsNullOrEmpty(brand.Mk4ActName) && Guid.TryParse(brand.Mk4ActName, out gid))
                {
                    bannerList.Add(gid.ToString());
                }

                bannerList.Add(brand.HotItemBid1 == null ? null : brand.HotItemBid1.ToString());
                bannerList.Add(brand.HotItemBid2 == null ? null : brand.HotItemBid2.ToString());
                bannerList.Add(brand.HotItemBid3 == null ? null : brand.HotItemBid3.ToString());
                bannerList.Add(brand.HotItemBid4 == null ? null : brand.HotItemBid4.ToString());

            }

            List<string> dealList = new List<string>();

            foreach (ViewBrandCategory vbc in vepctmp)
            {
                IViewPponDeal ppdeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vbc.BusinessHourGuid);

                PponDealStage stage = ppdeal.GetDealStage();
                //已售完檔次自動排序最後
                if (!(stage == PponDealStage.CouponGenerated || stage == PponDealStage.ClosedAndFail || stage == PponDealStage.ClosedAndOn) &&
                    !(ppdeal.OrderedQuantity >= ((ppdeal.OrderTotalLimit.HasValue) ? ppdeal.OrderTotalLimit : 0)))
                {
                    if (!(ppdeal.BusinessHourOrderTimeE <= DateTime.Now) && !(ppdeal.GroupOrderStatus != null && Helper.IsFlagSet((long)ppdeal.GroupOrderStatus, GroupOrderStatus.Completed))) //已下檔、已結檔的不加入列表
                    {
                        if (!Helper.IsFlagSet(ppdeal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
                        {

                            //非已結檔，非已下檔的才顯示
                            MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(0, vbc.Seq, ppdeal, new List<int>(), DealTimeSlotStatus.Default, 0);
                            if (!bannerList.Contains(mainDealPreview.PponDeal.BusinessHourGuid.ToString()))
                            {
                                dealList.Add(mainDealPreview.PponDeal.BusinessHourGuid.ToString());
                            }
                        }
                    }
                }

                if (dealList.Count >= 10)
                {
                    break;
                }
            }

            return dealList;
        }



        public static string ChangeBrandItemCategory(int brandId, int brandItemId, string categoryList)
        {
            try
            {
                //先刪除商品的所有分類
                _pponProv.DeleteBrandItemCategoryDependency(brandItemId);
                //取得單一Brand的BrandItem分類cid(全部)
                var brandItemCategoryList = _pponProv.GetBrandItemCategoryByBrandId(brandId).Where(x => x.Name.Equals("全部")).FirstOrDefault();
                categoryList = brandItemCategoryList.Id.ToString() + "," + categoryList;

                var arrayCategory = categoryList.Split(",").Distinct();

                //新增選擇的分類
                foreach (string tempCategory in arrayCategory)
                {
                    int bicId = int.TryParse(tempCategory, out bicId) ? bicId : 0;
                    if (bicId > 0)
                    {
                        BrandItemCategoryDependency bidc = new BrandItemCategoryDependency();
                        bidc.BrandItemId = brandItemId;
                        bidc.BrandItemCategoryId = bicId;
                        _pponProv.SaveBrandItemCategoryDependency(bidc);
                    }
                }

                return "";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }
        #endregion brand

        #region 前端購物車購買頁面
        public static string GetDefaultPponDealDetail()
        {
            string html = "";
            using (StreamReader sr = new StreamReader(HttpContext.Current.Server.MapPath("~/Themes/cart/CartPponDetail.html")))
            {
                html = sr.ReadToEnd();
            }
            return html;
        }
        public static bool GetDealIsColsed(IViewPponDeal deal)
        {
            CartDealStatus cartDealStatus = GetCartDealStatus(deal);
            if (cartDealStatus == CartDealStatus.Normal)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static CartDealStatus GetCartDealStatus(IViewPponDeal deal)
        {
            CartDealStatus cartDealStatus = CartDealStatus.Normal;
            if (deal.Slug != null)
            {
                //已結檔
                cartDealStatus = CartDealStatus.Closed;
            }
            if (deal.BusinessHourOrderTimeE > DateTime.Now && deal.BusinessHourOrderTimeS < DateTime.Now)
            {
                if (deal.OrderedQuantity < (deal.OrderTotalLimit ?? 0))
                {

                }
                else
                {
                    //數量超過，不可販售
                    cartDealStatus = CartDealStatus.SoldOut;
                }
            }
            else
            {
                //購買日期已過
                cartDealStatus = CartDealStatus.Closed;
            }

            return cartDealStatus;
        }
        #endregion

        #region Facebook Pixel Code
        public static string GetPponDealBid(List<MultipleMainDealPreview> deals, int type)
        {
            int maxbid = 10;
            int i = 0;
            string[] bids;
            string jsBidString;
            string nsBidString;

            if (maxbid > deals.Count)
            {
                bids = new string[deals.Count];
            }
            else
            {
                bids = new string[maxbid];
            }

            foreach (MultipleMainDealPreview m in deals)
            {
                if (i > maxbid - 1)
                {
                    break;
                }
                bids[i] = m.PponDeal.BusinessHourGuid.ToString();
                i++;
            }
            jsBidString = "'" + string.Join("','", bids) + "'";
            nsBidString = string.Join(",", bids);

            if (type == (int)FacebookPixelCode.JsCode)
            {
                return jsBidString;
            }
            else if (type == (int)FacebookPixelCode.NsCode)
            {
                return nsBidString;
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetSearchDealBid(List<IViewPponDeal> deals, int type)
        {
            int maxbid = 10;
            int i = 0;
            string[] bids;
            string jsBidString;
            string nsBidString;

            if (maxbid > deals.Count)
            {
                bids = new string[deals.Count];
            }
            else
            {
                bids = new string[maxbid];
            }

            foreach (IViewPponDeal d in deals)
            {
                if (i > maxbid - 1)
                {
                    break;
                }
                bids[i] = d.BusinessHourGuid.ToString();
                i++;
            }
            jsBidString = "'" + string.Join("','", bids) + "'";
            nsBidString = string.Join(",", bids);

            if (type == (int)FacebookPixelCode.JsCode)
            {
                return jsBidString;
            }
            else if (type == (int)FacebookPixelCode.NsCode)
            {
                return nsBidString;
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetBrandDealBid(Brand brand, int type)
        {
            int maxbid = 10;
            int i = 0;
            string[] bids;
            string jsBidString;
            string nsBidString;

            List<ViewBrandCategory> vbc = _pponProv.GetViewBrandCategoryByBrandId(brand.Id).Where(x => x.ItemStatus).OrderBy(x => x.Seq).OrderBy(x => x.BrandItemCategoryId).ToList();
            List<ViewBrandCategory> vbcAll = new List<ViewBrandCategory>();

            foreach (ViewBrandCategory v in vbc)
            {
                if (v.CategoryName == "全部")
                {
                    vbcAll.Add(v);
                }
            }

            if (maxbid > vbcAll.Count)
            {
                bids = new string[vbcAll.Count];
            }
            else
            {
                bids = new string[maxbid];
            }

            foreach (ViewBrandCategory v in vbcAll)
            {
                if (i > maxbid - 1)
                {
                    break;
                }
                bids[i] = v.BusinessHourGuid.ToString();
                i++;
            }
            jsBidString = "'" + string.Join("','", bids) + "'";
            nsBidString = string.Join(",", bids);

            if (type == (int)FacebookPixelCode.JsCode)
            {
                return jsBidString;
            }
            else if (type == (int)FacebookPixelCode.NsCode)
            {
                return nsBidString;
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetContentName(int channelid, int categoryid)
        {
            if (categoryid == 0)
            {
                var channelCategory = ViewPponDealManager.DefaultManager.GetCategory(channelid);
                var result = channelCategory == null ? string.Empty : channelCategory.Name;
                return result;
            }
            else
            {
                var category = ViewPponDealManager.DefaultManager.GetCategory(categoryid);
                var result = category == null ? string.Empty : category.Name;
                return result;
            }
        }

        public static string GetContentCategory(int channelid, int categoryid)
        {
            int _category;
            if (categoryid == 0)
            {
                return "Top";
            }
            else
            {
                _category = _pponProv.GetCategoryDependencyParentIdByCategoryId(categoryid);
                if (_category == channelid)
                {
                    var channelCategory = ViewPponDealManager.DefaultManager.GetCategory(channelid);
                    var result = channelCategory == null ? string.Empty : channelCategory.Name;
                    return result;
                }
                else
                {
                    var channelCategory = ViewPponDealManager.DefaultManager.GetCategory(channelid);
                    var category = ViewPponDealManager.DefaultManager.GetCategory(_category);
                    string result = string.Empty;
                    if (channelCategory != null && category != null)
                    {
                        result = channelCategory.Name + ">" + category.Name;
                    }
                    return result;
                }
            }
        }
        #endregion

        public static void UpdateDealImageReference(Guid bid, string dealContentDescription)
        {
            List<string> imageLinks = ImgPoolExtensions.GetImageLink(
                dealContentDescription, "img,a,br", "src,href,src");

            if (bid != Guid.Empty)
            {
                _pponProv.ImgPoolDeleteByBid(bid);
            }
            foreach (var link in imageLinks)
            {
                Uri url;
                if (Uri.TryCreate(link, UriKind.Absolute, out url) == false)
                {
                    continue;
                }
                ImgPool imgPool = new ImgPool();
                imgPool.Bid = bid;
                imgPool.Host = url.Host;
                imgPool.Path = url.AbsolutePath;
                imgPool.Type = (int)DealImageType.ContentImage;
                _pponProv.ImgPoolSet(imgPool);
            }
        }
        public static IEnumerable<ProvisionDepartmentModel> GetAllProvisionDepartmentModel()
        {
            ProvisionDescriptionCollection pds = _pponProv.ProvisionDescriptionGetAll();
            List<ProvisionDepartmentModel> descriptions = new List<ProvisionDepartmentModel>();

            //Department
            foreach (ProvisionDescription pd in pds.Where(x => x.ParentId == null).OrderBy(x => x.Rank))
            {
                //Category
                List<ProvisionCategoryModel> categorys = new List<ProvisionCategoryModel>();
                List<ProvisionDescription> cats = pds.Where(x => x.ParentId == pd.Id).ToList();
                foreach (ProvisionDescription cat in cats.OrderBy(x => x.Rank))
                {
                    //Item
                    List<ProvisionItemModel> items = new List<ProvisionItemModel>();
                    List<ProvisionDescription> its = pds.Where(x => x.ParentId == cat.Id).ToList();
                    foreach (ProvisionDescription it in its.OrderBy(x => x.Rank))
                    {
                        //Content
                        List<ProvisionContentModel> contents = new List<ProvisionContentModel>();
                        List<ProvisionDescription> cos = pds.Where(x => x.ParentId == it.Id).ToList();
                        foreach (ProvisionDescription co in cos)
                        {
                            //ProvisionDescModel
                            List<ProvisionDescModel> descs = new List<ProvisionDescModel>();
                            List<ProvisionDescription> des = pds.Where(x => x.ParentId == co.Id).ToList();
                            foreach (ProvisionDescription de in des.OrderBy(x => x.Rank))
                            {
                                ProvisionDescModel descModel = new ProvisionDescModel();
                                descModel.Id = de.Id;
                                descModel.Status = (ProvisionDescStatus)de.Status;
                                descModel.Description = de.Name;
                                descs.Add(descModel);
                            }

                            ProvisionContentModel contentModel = new ProvisionContentModel();
                            contentModel.Id = co.Id;
                            contentModel.ProvisionDescs.AddRange(descs);
                            contentModel.Name = co.Name;
                            contentModel.Rank = co.Rank.GetValueOrDefault(0);
                            contents.Add(contentModel);
                        }

                        ProvisionItemModel itemModel = new ProvisionItemModel();
                        itemModel.Id = it.Id;
                        itemModel.Contents.AddRange(contents);
                        itemModel.Name = it.Name;
                        itemModel.Rank = it.Rank.GetValueOrDefault(0);
                        items.Add(itemModel);
                    }

                    ProvisionCategoryModel catModel = new ProvisionCategoryModel();
                    catModel.Id = cat.Id;
                    catModel.Items.AddRange(items);
                    catModel.Name = cat.Name;
                    catModel.Rank = cat.Rank.GetValueOrDefault(0);
                    categorys.Add(catModel);
                }

                ProvisionDepartmentModel department = new ProvisionDepartmentModel();
                department.Id = pd.Id;
                department.Categorys.AddRange(categorys);
                department.Name = pd.Name;
                descriptions.Add(department);
            }

            return descriptions;
        }

        public static AccessoryGroupCollection AccessoryGroupGetByHouseDeal(IViewPponDeal theDeal)
        {
            AccessoryGroupCollection viagc = new AccessoryGroupCollection();
            PponOptionCollection pos = _pponProv.PponOptionGetList(theDeal.BusinessHourGuid);
            var catgNames = pos.Select(x => new { CatgName = x.CatgName, CatgSeq = x.CatgSeq }).Distinct().ToList();
            ProductItemCollection pic = _pponProv.ProductItemGetList(pos.Select(x => x.ItemGuid.GetValueOrDefault()).ToList());
            foreach (var cName in catgNames)
            {
                AccessoryGroup ag = new AccessoryGroup()
                {
                    AccessoryGroupName = cName.CatgName
                };
                List<PponOption> oplist = pos.Where(x => x.CatgSeq == cName.CatgSeq).OrderBy(y => y.OptionSeq).ToList();
                ag.members = new List<ViewItemAccessoryGroup>();
                foreach (PponOption op in oplist)
                {
                    ProductItem piOption = pic.Where(x => x.Guid == op.ItemGuid.GetValueOrDefault()).FirstOrDefault();
                    int quantity = 0;
                    if (piOption != null)
                    {
                        quantity = piOption.Stock;
                    }
                    ViewItemAccessoryGroup viag = new ViewItemAccessoryGroup()
                    {
                        ItemName = theDeal.ItemName,
                        ItemPrice = theDeal.ItemPrice,
                        AccessoryGroupMemberGuid = op.Guid,
                        AccessoryGroupMemberSequence = op.OptionSeq,
                        AccessoryGroupName = op.CatgName,
                        AccessoryName = op.OptionName,
                        Quantity = quantity
                    };
                    ag.members.Add(viag);
                }

                viagc.Add(ag);
            }
            return viagc;
        }


        #region 庫存規格

        /// <summary>
        /// 取得新版提案單商品規格 for 商家系統
        /// </summary>
        public static List<ViewProductItem> GetProductOptionListForVbs(List<Guid> sellerGuids, int? productNo, string productBrandName, string productName,
            string gtin, string productCode, string items, int? stockStatus, bool? hideDisabled, Guid? bid, int? uniqueId, int? pid, int? warehouseType)
        {
            List<ViewProductItem> vpiList = new List<ViewProductItem>();

            if (bid != null)
            {
                vpiList = GetProductItemGuidByBid((Guid)bid, sellerGuids);
            }
            else if (uniqueId != null) //檔號
            {
                var dp = _pponProv.DealPropertyGet((int)uniqueId);
                if (dp.IsLoaded)
                {
                    vpiList = GetProductItemGuidByBid(dp.BusinessHourGuid, sellerGuids);
                }
            }
            else
            {
                vpiList = _pponProv.ViewProductItemListGetBySellerGuidList(sellerGuids).ToList();
            }

            if (pid != null && pid != 0)
            {
                ProposalMultiDealCollection pmdc = _sellerProv.ProposalMultiDealGetListByPid(new List<int>() { (int)pid });
                ProposalMultiOptionSpecCollection pmosc = _sellerProv.ProposalMultiOptionSpecGetByMids(pmdc.Select(x => x.Id).ToList());
                List<Guid> itemGuid = pmosc.Select(x => x.ItemGuid).ToList();
                vpiList = vpiList.Where(x => x.ItemGuid.EqualsAny(itemGuid.ToArray())).ToList();
            }

            //過濾
            return GetViewProductItemListByFilter(vpiList, null, productNo.GetValueOrDefault(),
                productBrandName, productName, gtin, productCode, items, stockStatus.GetValueOrDefault(), hideDisabled.GetValueOrDefault(),
                false, warehouseType);
        }

        /// <summary>
        /// 取得新版提案單商品規格 for 業務系統
        /// </summary> 
        public static List<ViewProductItem> GetProductOptionListForSal(Guid sellerGuid, int? productNo, string productBrandName, string productName,
            string gtin, string productCode, string items, int? stockStatus, bool? hideDisabled, Guid? bid, int? uniqueId, int? warehouseType)
        {
            List<ViewProductItem> vpiList = new List<ViewProductItem>();

            if (bid != null)
            {
                vpiList = GetProductItemGuidByBid((Guid)bid, new List<Guid> { sellerGuid });
            }
            else if (uniqueId != null) //檔號
            {
                var dp = _pponProv.DealPropertyGet((int)uniqueId);
                if (dp.IsLoaded)
                {
                    vpiList = GetProductItemGuidByBid(dp.BusinessHourGuid, new List<Guid> { sellerGuid });
                }
            }
            else
            {
                vpiList = _pponProv.ViewProductItemListGetBySellerGuid(sellerGuid).ToList();
            }

            //過濾
            return GetViewProductItemListByFilter(vpiList, null, productNo.GetValueOrDefault(),
                productBrandName, productName, gtin, productCode, items, stockStatus.GetValueOrDefault(),
                hideDisabled.GetValueOrDefault(), false, warehouseType);
        }



        /// <summary>
        /// 以檔次 Bid 取得規格 Guid
        /// </summary>
        /// <param name="inputBid"></param>
        /// <param name="sellerGuids"></param>
        /// <param name="pponOptionItemGuids">可從 pponProvider.PponOptionGetItemGuidDictByOnlineDeals 取得目前on檔的規格Guid</param>
        /// <returns></returns>
        public static List<ViewProductItem> GetProductItemGuidByBid(Guid inputBid, List<Guid> sellerGuids,
            Dictionary<Guid, List<Guid>> pponOptionItemGuids = null,
            Dictionary<Guid, string> proposalMultiDealOptions = null,
            Dictionary<Guid, ViewProductItem> viewProductItems = null)
        {
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(inputBid, true);

            if (!sellerGuids.Contains(theDeal.SellerGuid))
            {
                return new List<ViewProductItem>();
            }

            bool isHouseNewVersion = ProposalFacade.IsVbsProposalNewVersion() && theDeal.IsHouseDealNewVersion();
            if (!isHouseNewVersion)
            {
                //舊版提案單，查無新版規格
                return new List<ViewProductItem>();
            }

            List<Guid> bids = new List<Guid>();
            if (Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.ComboDealMain) && theDeal.MainBid != null)
            {
                //多檔母檔        
                List<Guid> combolBids = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid((Guid)theDeal.MainBid)
                    .Select(x => x.BusinessHourGuid).ToList();
                bids = combolBids;
            }
            else
            {
                //單檔 or 多檔子檔
                bids.Add(inputBid);
            }

            List<Guid> itemGuids = new List<Guid>();
            foreach (var bid in bids)
            {
                //多重選項 (有 PponOption)
                if (pponOptionItemGuids == null)
                {
                    itemGuids.AddRange(_pponProv.PponOptionGetItemGuidListByBid(bid));
                }
                else if (pponOptionItemGuids.ContainsKey(bid))
                {
                    itemGuids.AddRange(pponOptionItemGuids[bid]);
                }

                //單一選項
                //if (!itemGuids.Any())
                //{
                string multiDealOption = null;

                if (proposalMultiDealOptions == null)
                {
                    ProposalMultiDeal multiDeal = _sellerProv.ProposalMultiDealGetByBid(bid);
                    multiDealOption = multiDeal.Options;
                }
                else if (proposalMultiDealOptions.ContainsKey(bid))
                {
                    multiDealOption = proposalMultiDealOptions[bid];
                }
                if (string.IsNullOrEmpty(multiDealOption) == false)
                {
                    List<ProposalMultiDealsSpec> specs = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(
                        multiDealOption);
                    foreach (var spec in specs)
                    {
                        foreach (var item in spec.Items)
                        {
                            if (!itemGuids.Contains(item.item_guid))
                            {
                                itemGuids.Add(item.item_guid);
                            }
                        }
                    }
                }
                //}
            }

            if (viewProductItems == null)
            {
                return _pponProv.ViewProductItemListGetByItemGuids(itemGuids).ToList();
            }
            else
            {
                List<ViewProductItem> result = new List<ViewProductItem>();
                foreach (Guid itemGuid in itemGuids)
                {
                    if (viewProductItems.ContainsKey(itemGuid))
                    {
                        result.Add(viewProductItems[itemGuid]);
                    }
                }
                return result;
            }


        }

        /// <summary>
        /// 過濾商品規格
        /// </summary>
        public static List<ViewProductItem> GetViewProductItemListByFilter(List<ViewProductItem> vpiList, Guid? infoGuid, int productNo,
            string productBrandName, string productName, string gtin, string productCode, string items, int stockStatus, bool hideDisabled,
            bool isSimilarProductNo, int? warehouseType)
        {
            if (!vpiList.Any())
            {
                return new List<ViewProductItem>();
            }

            //ProductSpecCollection pits = _pponProv.ProductSpecListGetByItemGuid(vpiList.Select(x => x.ItemGuid).ToList());

            //foreach (ViewProductItem vpi in vpiList)
            //{
            //    var ppits = pits.Where(x => x.ItemGuid == vpi.ItemGuid);
            //    vpi.ItemName = string.Join("_", ppits.OrderBy(t => t.Sort).Select(x => x.SpecName).ToList());
            //}

            if (infoGuid != null && infoGuid != Guid.Empty)
            {
                vpiList = vpiList.Where(x => x.InfoGuid == infoGuid).ToList();
            }

            //商品編號
            if (productNo != 0)
            {
                vpiList = vpiList.Where(x => x.ProductNo == productNo).ToList();
            }
            //商品品牌名稱
            if (!string.IsNullOrEmpty(productBrandName))
            {
                vpiList = vpiList.Where(x => x.ProductBrandName.Contains(productBrandName)).ToList();
            }
            //商品名稱
            if (!string.IsNullOrEmpty(productName))
            {
                vpiList = vpiList.Where(x => x.ProductName.Contains(productName)).ToList();
            }
            //原廠編號(GTINs)
            if (!string.IsNullOrEmpty(gtin))
            {
                vpiList = vpiList.Where(x => x.Gtins == gtin).ToList();
            }
            //貨號資料
            if (isSimilarProductNo)
            {
                if (!string.IsNullOrEmpty(productCode))
                {
                    var infogid = vpiList.Where(x => x.ProductCode.Contains(productCode)).Select(y => y.InfoGuid).FirstOrDefault();
                    vpiList = vpiList.Where(x => x.InfoGuid.Equals(infogid)).ToList();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(productCode))
                {
                    vpiList = vpiList.Where(x => x.ProductCode.Contains(productCode)).ToList();
                }
            }

            //規格
            if (!string.IsNullOrEmpty(items))
            {
                string[] itemList = items.Split(",");
                foreach (string it in itemList)
                {
                    vpiList = vpiList.Where(x => x.ItemName.Contains(it)).ToList();
                }
            }
            //庫存量
            if (stockStatus != 0)
            {
                if (stockStatus == (int)ProductStockFilter.Normal)
                {
                    vpiList = vpiList.Where(x => x.Stock > 0).ToList();
                }
                else if (stockStatus == (int)ProductStockFilter.LessThanBase)
                {
                    vpiList = vpiList.Where(x => x.Stock <= x.SafetyStock && x.Stock > 0).ToList();
                }
                else if (stockStatus == (int)ProductStockFilter.Zero)
                {
                    vpiList = vpiList.Where(x => x.Stock <= 0).ToList();
                }
            }

            if (warehouseType != null && warehouseType != -1)
            {
                vpiList = vpiList.Where(t => t.WarehouseType == warehouseType.Value).ToList();
            }

            //隱藏停用商品
            if (hideDisabled)
            {
                vpiList = vpiList.Where(x => x.ItemStatus == (int)ProductItemStatus.Normal).ToList();
            }

            return vpiList;
        }

        /// <summary>
        /// 依照頁數取規格
        /// </summary>
        public static dynamic GetBaseProductOption(List<ViewProductItem> vpiList, int pageStart, int pageLength)
        {
            int totalCount = vpiList.Count();
            int totalPage = (totalCount / pageLength);
            if ((totalCount % pageLength) > 0)
            {
                totalPage += 1;
            }
            if (totalCount == 0)
            {
                totalPage = 1;
            }

            var rtnData = vpiList.OrderBy(z => z.ProductNo).OrderBy(y => y.ProductBrandName).OrderBy(x => x.ProductName)
                .Skip((pageStart - 1) * pageLength).Take(pageLength).ToList().Select(x => new
                {
                    InfoGuid = x.InfoGuid,
                    ItemGuid = x.ItemGuid,
                    Sid = x.SellerGuid,
                    ProductNo = x.ProductNo,
                    ProductBrandName = x.ProductBrandName,
                    ProductName = x.ProductName,
                    ItemName = x.ItemName,
                    Gtins = x.Gtins,
                    Mpn = x.Mpn,
                    ProductCode = x.ProductCode,
                    Stock = x.Stock,
                    SaftyStock = x.SafetyStock,
                    ItemStatus = x.ItemStatus,
                    Price = x.Price,
                    WarehouseType = x.WarehouseType,
                    PchomeProdId = x.PchomeProdId ?? string.Empty
                }).ToList();

            dynamic obj = new
            {
                TotalCount = totalCount,
                TotalPage = totalPage,
                CurrentPage = pageStart,
                Data = rtnData
            };
            return obj;
        }

        public static dynamic GetViewWmsReturnOrderModel(List<ViewWmsReturnOrder> vpiList, int pageStart, int pageLength)
        {
            int totalCount = vpiList.Count();
            int totalPage = (totalCount / pageLength);
            if ((totalCount % pageLength) > 0)
            {
                totalPage += 1;
            }
            if (totalCount == 0)
            {
                totalPage = 1;
            }

            var rtnData = vpiList.OrderBy(z => z.ProductNo).OrderBy(y => y.ProductBrandName).OrderBy(x => x.ProductName)
                .Skip((pageStart - 1) * pageLength).Take(pageLength).ToList().Select(x => new
                {
                    ReturnOrderGuid = x.ReturnOrderGuid,
                    InfoGuid = x.InfoGuid,
                    ItemGuid = x.ItemGuid,
                    Sid = x.SellerGuid,
                    ProductNo = x.ProductNo,
                    ProductBrandName = x.ProductBrandName,
                    ProductName = x.ProductName,
                    SpecName = x.SpecName,
                    PchomeReturnOrderId = x.PchomeReturnOrderId ?? string.Empty,
                    PchomeProdId = x.PchomeProdId ?? string.Empty,
                    ProductCode = x.ProductCode,
                    Qty = x.Qty,
                    Reason = x.Reason,
                    Remark = x.Remark,
                    Price = x.Price,
                    Status = Helper.GetDescription((WmsReturnOrderStatus)x.Status),
                    Source = Helper.GetDescription((WmsReturnSource)x.Source),
                    CreateTime = x.CreateTime.ToString("yyyy/MM/dd"),
                    InValidation = x.Invalidation
                }).ToList();

            dynamic obj = new
            {
                TotalCount = totalCount,
                TotalPage = totalPage,
                CurrentPage = pageStart,
                Data = rtnData
            };
            return obj;
        }

        #endregion

        public static List<ViewWmsPurchaseOrder> GetWmsProchaseOrder(List<Guid> sellerGuids, int? pid, Guid? bid, int? uniqueId, string productBrandName, string productName,
                                                                     int? productNo, string specs, string productCode, string pchomePurchaseOrderId, int? purchaseOrderStatus,
                                                                     string sellerName, DateTime? startTime, DateTime? endTime, int warehouseType, string userName, bool? dontDisplayInvalidation, string pchomeProdId)
        {
            List<ViewWmsPurchaseOrder> vpiList = new List<ViewWmsPurchaseOrder>();

            if (bid != null)
            {
                vpiList = GetPurchaseOrderGuidByPidBid((Guid)bid, sellerGuids);
            }
            else if (uniqueId != null) //檔號
            {
                var dp = _pponProv.DealPropertyGet((int)uniqueId);
                if (dp.IsLoaded)
                {
                    vpiList = GetPurchaseOrderGuidByPidBid(dp.BusinessHourGuid, sellerGuids);
                }
            }
            else
            {
                if (sellerGuids != null)
                    vpiList = _wp.ViewWmsPurchaseOrdermListGetBySellerGuidList(sellerGuids).ToList();
                else
                    vpiList = _wp.ViewWmsPurchaseOrdermList().ToList();
            }

            if (dontDisplayInvalidation.HasValue && dontDisplayInvalidation.Value)
            {
                vpiList = vpiList.Where(x => x.Invalidation == Convert.ToInt32(false)).ToList();
            }

            if (!string.IsNullOrEmpty(pchomeProdId))
            {
                vpiList = vpiList.Where(x => x.PchomeProdId == pchomeProdId).ToList();
            }

            if (pid != null && pid != 0)
            {
                ProposalMultiDealCollection pmdc = _sellerProv.ProposalMultiDealGetListByPid(new List<int>() { (int)pid });
                ProposalMultiOptionSpecCollection pmosc = _sellerProv.ProposalMultiOptionSpecGetByMids(pmdc.Select(x => x.Id).ToList());
                List<Guid> itemGuid = pmosc.Select(x => x.ItemGuid).ToList();
                vpiList = vpiList.Where(x => x.ItemGuid.EqualsAny(itemGuid.ToArray())).ToList();
            }
            if (!string.IsNullOrEmpty(userName))
            {
                ViewEmployee emp = _humProv.ViewEmployeeGet(ViewEmployee.Columns.Email, userName);
                string privilegeUri = "/sal/proposal/WmsManage";
                if (CommonFacade.IsInSystemFunctionPrivilege(userName, SystemFunctionType.ReadAll, privilegeUri))
                {
                    //全區
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(userName, SystemFunctionType.CrossDeptTeam, privilegeUri))
                {
                    //本區
                    if (string.IsNullOrEmpty(emp.CrossDeptTeam))
                    {
                        //如果CrossDeptTeam是空的，把自己區的業務抓出來，只能看到自己區的資料
                        ViewEmployeeCollection empDept = _humProv.ViewEmployeeCollectionGetByFilter(ViewEmployee.Columns.DeptId, emp.DeptId);
                        vpiList = vpiList.Where(x => x.SalesId.EqualsAny(empDept.Select(y => y.UserId).Distinct().ToArray())).ToList();
                    }
                    else
                    {
                        //如果CrossDeptTeam不是空的，把CrossDeptTeam所有的業務抓出來
                        ViewEmployeeCollection emps = _humProv.ViewEmployeeCollectionGetAll();
                        string[] cdts = emp.CrossDeptTeam.Split("/");
                        List<int> empNos = new List<int>();
                        foreach (string cdt in cdts)
                        {
                            if (!cdt.Contains("["))
                            {
                                List<ViewEmployee> empDept = emps.Where(x => x.DeptId.Equals(cdt)).ToList();
                                empNos.AddRange(empDept.Select(x => x.UserId).Distinct().ToArray());
                            }
                            else
                            {
                                string cs = cdt.Substring(cdt.IndexOf("[") + 1, cdt.IndexOf("]") - cdt.IndexOf("[") - 1);
                                string[] scs = cs.Split(",");
                                foreach (string c in scs)
                                {
                                    int c1 = default(int);
                                    int.TryParse(c, out c1);
                                    string d = cdt.Substring(0, cdt.IndexOf("["));
                                    List<ViewEmployee> empDept = emps.Where(x => x.DeptId.Equals(d) && x.TeamNo.Equals(c1)).ToList();
                                    empNos.AddRange(empDept.Select(x => x.UserId).Distinct().ToArray());
                                }
                            }
                        }
                        vpiList = vpiList.Where(x => x.SalesId.EqualsAny(empNos.Distinct().ToArray())).ToList();
                    }
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(userName, SystemFunctionType.Read, privilegeUri))
                {
                    //瀏覽，只能看到自己的
                    vpiList = vpiList.Where(x => x.SalesId.Equals(emp.UserId)).ToList();
                }
                else
                {
                    //沒權限，不要回傳任何資料
                    vpiList.Clear();
                    vpiList = vpiList.ToList();
                }
            }
            //過濾
            return GetViewWmsPurchaseOrderListByFilter(vpiList, productBrandName, productName, productNo.GetValueOrDefault(), specs, productCode, pchomePurchaseOrderId, purchaseOrderStatus, sellerName, startTime, endTime, warehouseType);
        }

        public static List<ViewWmsPurchaseOrder> GetPurchaseOrderGuidByPidBid(Guid inputBid, List<Guid> sellerGuids)
        {
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(inputBid, true);

            if (sellerGuids != null)
            {
                if (!sellerGuids.Contains(theDeal.SellerGuid))
                {
                    return new List<ViewWmsPurchaseOrder>();
                }
            }


            bool isHouseNewVersion = ProposalFacade.IsVbsProposalNewVersion() && theDeal.IsHouseDealNewVersion();
            if (!isHouseNewVersion)
            {
                //舊版提案單，查無新版規格
                return new List<ViewWmsPurchaseOrder>();
            }

            List<Guid> bids = new List<Guid>();
            if (Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.ComboDealMain) && theDeal.MainBid != null)
            {
                //多檔母檔        
                List<Guid> combolBids = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid((Guid)theDeal.MainBid)
                    .Select(x => x.BusinessHourGuid).ToList();
                bids = combolBids;
            }
            else
            {
                //單檔 or 多檔子檔
                bids.Add(inputBid);
            }

            List<Guid> itemGuids = new List<Guid>();
            foreach (var bid in bids)
            {
                //多重選項 (有 PponOption)
                itemGuids.AddRange(_pponProv.PponOptionGetItemGuidListByBid(bid));

                //單一選項
                string multiDealOption = null;

                ProposalMultiDeal multiDeal = _sellerProv.ProposalMultiDealGetByBid(bid);
                multiDealOption = multiDeal.Options;

                if (string.IsNullOrEmpty(multiDealOption) == false)
                {
                    List<ProposalMultiDealsSpec> specs = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(
                        multiDealOption);
                    foreach (var spec in specs)
                    {
                        foreach (var item in spec.Items)
                        {
                            if (!itemGuids.Contains(item.item_guid))
                            {
                                itemGuids.Add(item.item_guid);
                            }
                        }
                    }
                }
                //}
            }

            return _wp.ViewWmsPurchaseOrderListGetByItemGuids(itemGuids).ToList();


        }

        /// <summary>
        /// 過濾商品規格
        /// </summary>
        public static List<ViewWmsPurchaseOrder> GetViewWmsPurchaseOrderListByFilter(List<ViewWmsPurchaseOrder> vpiList, string productBrandName, string productName,
                                                                     int? productNo, string specs, string productCode, string pchomePurchaseOrderId, int? purchaseOrderStatus,
                                                                     string sellerName, DateTime? startTime, DateTime? endTime, int warehouseType)
        {
            if (!vpiList.Any())
            {
                return new List<ViewWmsPurchaseOrder>();
            }


            //商品品牌名稱
            if (!string.IsNullOrEmpty(productBrandName))
            {
                vpiList = vpiList.Where(x => x.ProductBrandName.Contains(productBrandName)).ToList();
            }
            //商品名稱
            if (!string.IsNullOrEmpty(productName))
            {
                vpiList = vpiList.Where(x => x.ProductName.Contains(productName)).ToList();
            }

            //商品編號
            if (productNo != 0)
            {
                vpiList = vpiList.Where(x => x.ProductNo == productNo).ToList();
            }

            //規格
            if (!string.IsNullOrEmpty(specs))
            {
                string[] specList = specs.Split(",");
                foreach (string sp in specList)
                {
                    vpiList = vpiList.Where(x => x.SpecName.Contains(sp)).ToList();
                }
            }

            //貨號資料
            if (!string.IsNullOrEmpty(productCode))
            {
                vpiList = vpiList.Where(x => x.ProductCode.Contains(productCode)).ToList();
            }

            //倉儲進倉單號
            if (!string.IsNullOrEmpty(pchomePurchaseOrderId))
            {
                vpiList = vpiList.Where(x => x.PchomePurchaseOrderId == pchomePurchaseOrderId).ToList();
            }

            //進倉單狀狀態
            if (purchaseOrderStatus != null && purchaseOrderStatus != 0)
            {
                vpiList = vpiList.Where(x => x.Status == purchaseOrderStatus).ToList();
            }

            //商家名稱
            if (!string.IsNullOrEmpty(sellerName))
            {
                vpiList = vpiList.Where(x => x.SellerName.Contains(sellerName)).ToList();
            }

            //進倉單日期
            if (startTime != null)
            {
                vpiList = vpiList.Where(x => x.CreateTime >= startTime).ToList();
            }

            if (endTime != null)
            {
                vpiList = vpiList.Where(x => x.CreateTime <= endTime.Value.AddDays(1)).ToList();
            }

            vpiList = vpiList.Where(t => t.WarehouseType == warehouseType).ToList();

            return vpiList;
        }

        /// <summary>
        /// 依照頁數取規格
        /// </summary>
        public static dynamic GetWmsPurchaseOrder(List<ViewWmsPurchaseOrder> vpiList, int pageStart, int pageLength)
        {
            int totalCount = vpiList.Count();
            int totalPage = (totalCount / pageLength);
            if ((totalCount % pageLength) > 0)
            {
                totalPage += 1;
            }
            if (totalCount == 0)
            {
                totalPage = 1;
            }

            var rtnData = vpiList.OrderBy(z => z.ProductNo).OrderBy(y => y.ProductBrandName).OrderBy(x => x.ProductName)
                .Skip((pageStart - 1) * pageLength).Take(pageLength).ToList().Select(x => new
                {
                    SellerGuid = x.SellerGuid,
                    SellerName = x.SellerName,
                    InfoGuid = x.InfoGuid,
                    ItemGuid = x.ItemGuid,
                    PchomeProdId = x.PchomeProdId ?? string.Empty,
                    PurchaseOrderGuid = x.PurchaseOrderGuid,
                    PchomePurchaseOrderId = x.PchomePurchaseOrderId ?? string.Empty,
                    ProductNo = x.ProductNo,
                    ProductBrandName = x.ProductBrandName,
                    ProductName = x.ProductName,
                    SpecName = x.SpecName,
                    ProductCode = x.ProductCode,
                    CreateTime = x.CreateTime.ToString("yyyy/MM/dd"),
                    Status = x.Status,
                    StatusText = Helper.GetDescription((PurchaseOrderStatus)x.Status),
                    Qty = x.Qty,
                    Price = x.Price,
                    ActualQty = x.ActualQty,
                    RefundQty = x.RefundQty,
                    Invalidation = (x.Invalidation == 1),
                }).ToList();

            dynamic obj = new
            {
                TotalCount = totalCount,
                TotalPage = totalPage,
                CurrentPage = pageStart,
                Data = rtnData
            };
            return obj;
        }

        public static bool ChangeWmsPurchaseOrderInvalidationStatus(string purchaseOrderGuidString, string userName)
        {
            var purchaseOrderGuid = Guid.Parse(purchaseOrderGuidString);
            var wmsPurchaseOrder = _wp.WmsPurchaseOrderGet(purchaseOrderGuid);

            if (wmsPurchaseOrder.IsLoaded && wmsPurchaseOrder != null)
            {
                if (CanModifyInvalidationStatus(wmsPurchaseOrder.Status))
                {
                    int newInvalidationStatus = (int)WmsInvalidation.Yes;
                    if (wmsPurchaseOrder.Invalidation == (int)WmsInvalidation.Yes)
                    {
                        newInvalidationStatus = (int)WmsInvalidation.No;
                    }
                    wmsPurchaseOrder.Invalidation = newInvalidationStatus;
                    wmsPurchaseOrder.ModifyId = userName;
                    wmsPurchaseOrder.ModifyTime = DateTime.Now;
                    _wp.WmsPurchaseOrderSet(wmsPurchaseOrder);

                    return true;
                }
            }

            return false;
        }

        public static bool ChangeWmsReturnOrderInvalidationStatus(
            string returnOrderGuidString,
            string userName)
        {
            var returnOrderGuid = Guid.Parse(returnOrderGuidString);


            WmsReturnOrder wro = _wp.WmsReturnOrderGet(returnOrderGuid);
            if (wro != null && wro.IsLoaded)
            {
                if (CanModifyWmsReturnInvalidationStatus(wro.Status))
                {
                    int newInvalidationStatus = (int)WmsInvalidation.Yes;
                    if (wro.Invalidation == (int)WmsInvalidation.Yes)
                    {
                        newInvalidationStatus = (int)WmsInvalidation.No;
                    }
                    wro.Invalidation = newInvalidationStatus;
                    wro.ModifyId = userName;
                    wro.ModifyTime = DateTime.Now;
                    _wp.WmsReturnOrderSet(wro);

                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 作廢修改規則
        /// </summary>
        /// <param name="wmsPurchaseOrder"></param>
        /// <returns></returns>
        private static bool CanModifyInvalidationStatus(int wmsPurchaseOrderStatus)
        {
            switch (wmsPurchaseOrderStatus)
            {
                case (int)PurchaseOrderStatus.Verify:
                case (int)PurchaseOrderStatus.Submit:
                case (int)PurchaseOrderStatus.WmsArrived:
                case (int)PurchaseOrderStatus.WmsInventory:
                case (int)PurchaseOrderStatus.WmsCancel:
                    return true;
                case (int)PurchaseOrderStatus.Initial:
                case (int)PurchaseOrderStatus.Apply:
                case (int)PurchaseOrderStatus.Fail:
                case (int)PurchaseOrderStatus.WmsFail:
                default:
                    return false;
            }
        }


        private static bool CanModifyWmsReturnInvalidationStatus(int wmsReturnOrderStatus)
        {
            bool flag = false;
            switch (wmsReturnOrderStatus)
            {
                case (int)WmsReturnOrderStatus.Initial:
                case (int)WmsReturnOrderStatus.Apply:
                case (int)WmsReturnOrderStatus.Fail:
                case (int)WmsReturnOrderStatus.WmsFail:
                    flag = false;
                    break;
                default:
                    flag = true;
                    break;
            }
            return flag;
        }


        #region 還貨單
        public static List<ViewWmsReturnOrder> GetWmsReturnOrder(string productBrandName, string productName,
                                                                     int? productNo, string specs, string productCode, string pchomeProId, int? returnOrderStatus, int? returnSourceStatus,
                                                                     string sellerName, DateTime? startTime, DateTime? endTime, int warehouseType, string userName, bool dontDisplayInvalidation)
        {
            List<ViewWmsReturnOrder> vpiList = new List<ViewWmsReturnOrder>();

            vpiList = _wp.ViewWmsReturnOrdermList().ToList();

            if (dontDisplayInvalidation)
            {
                vpiList = vpiList.Where(x => x.Invalidation == Convert.ToInt32(false)).ToList();
            }

            if (!string.IsNullOrEmpty(userName))
            {
                ViewEmployee emp = _humProv.ViewEmployeeGet(ViewEmployee.Columns.Email, userName);
                string privilegeUri = "/sal/proposal/WmsReturn";
                if (CommonFacade.IsInSystemFunctionPrivilege(userName, SystemFunctionType.ReadAll, privilegeUri))
                {
                    //全區
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(userName, SystemFunctionType.CrossDeptTeam, privilegeUri))
                {
                    //本區
                    if (string.IsNullOrEmpty(emp.CrossDeptTeam))
                    {
                        //如果CrossDeptTeam是空的，把自己區的業務抓出來，只能看到自己區的資料
                        ViewEmployeeCollection empDept = _humProv.ViewEmployeeCollectionGetByFilter(ViewEmployee.Columns.DeptId, emp.DeptId);
                        vpiList = vpiList.Where(x => x.SalesId.EqualsAny(empDept.Select(y => y.UserId).Distinct().ToArray())).ToList();
                    }
                    else
                    {
                        //如果CrossDeptTeam不是空的，把CrossDeptTeam所有的業務抓出來
                        ViewEmployeeCollection emps = _humProv.ViewEmployeeCollectionGetAll();
                        string[] cdts = emp.CrossDeptTeam.Split("/");
                        List<int> empNos = new List<int>();
                        foreach (string cdt in cdts)
                        {
                            if (!cdt.Contains("["))
                            {
                                List<ViewEmployee> empDept = emps.Where(x => x.DeptId.Equals(cdt)).ToList();
                                empNos.AddRange(empDept.Select(x => x.UserId).Distinct().ToArray());
                            }
                            else
                            {
                                string cs = cdt.Substring(cdt.IndexOf("[") + 1, cdt.IndexOf("]") - cdt.IndexOf("[") - 1);
                                string[] scs = cs.Split(",");
                                foreach (string c in scs)
                                {
                                    int c1 = default(int);
                                    int.TryParse(c, out c1);
                                    string d = cdt.Substring(0, cdt.IndexOf("["));
                                    List<ViewEmployee> empDept = emps.Where(x => x.DeptId.Equals(d) && x.TeamNo.Equals(c1)).ToList();
                                    empNos.AddRange(empDept.Select(x => x.UserId).Distinct().ToArray());
                                }
                            }
                        }
                        vpiList = vpiList.Where(x => x.SalesId.EqualsAny(empNos.Distinct().ToArray())).ToList();
                    }
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(userName, SystemFunctionType.Read, privilegeUri))
                {
                    //瀏覽，只能看到自己的
                    vpiList = vpiList.Where(x => x.SalesId.Equals(emp.UserId)).ToList();
                }
                else
                {
                    //沒權限，不要回傳任何資料
                    vpiList.Clear();
                    vpiList = vpiList.ToList();
                }
            }
            //過濾
            return GetViewWmsReturnOrderListByFilter(vpiList, productBrandName, productName, productNo.GetValueOrDefault(), specs, productCode, pchomeProId, returnOrderStatus, returnSourceStatus, sellerName, startTime, endTime, warehouseType);
        }

        /// <summary>
        /// 過濾商品規格
        /// </summary>
        public static List<ViewWmsReturnOrder> GetViewWmsReturnOrderListByFilter(List<ViewWmsReturnOrder> vpiList, string productBrandName, string productName,
                                                                     int? productNo, string specs, string productCode, string pchomeProId, int? returnOrderStatus, int? returnSourceStatus,
                                                                     string sellerName, DateTime? startTime, DateTime? endTime, int warehouseType)
        {
            if (!vpiList.Any())
            {
                return new List<ViewWmsReturnOrder>();
            }


            //商品品牌名稱
            if (!string.IsNullOrEmpty(productBrandName))
            {
                vpiList = vpiList.Where(x => x.ProductBrandName.Contains(productBrandName)).ToList();
            }
            //商品名稱
            if (!string.IsNullOrEmpty(productName))
            {
                vpiList = vpiList.Where(x => x.ProductName.Contains(productName)).ToList();
            }

            //商品編號
            if (productNo != 0)
            {
                vpiList = vpiList.Where(x => x.ProductNo == productNo).ToList();
            }

            //規格
            if (!string.IsNullOrEmpty(specs))
            {
                string[] specList = specs.Split(",");
                foreach (string sp in specList)
                {
                    vpiList = vpiList.Where(x => x.SpecName.Contains(sp)).ToList();
                }
            }

            //貨號資料
            if (!string.IsNullOrEmpty(productCode))
            {
                vpiList = vpiList.Where(x => x.ProductCode.Contains(productCode)).ToList();
            }

            //倉儲進倉單號
            if (!string.IsNullOrEmpty(pchomeProId))
            {
                vpiList = vpiList.Where(x => x.PchomeProdId == pchomeProId).ToList();
            }

            //進倉單狀狀態
            if (returnOrderStatus != null && returnOrderStatus != 0)
            {
                vpiList = vpiList.Where(x => x.Status == returnOrderStatus).ToList();
            }

            //資料來源
            if (returnSourceStatus != null)
            {
                vpiList = vpiList.Where(x => x.Source == returnSourceStatus).ToList();
            }

            //商家名稱
            if (!string.IsNullOrEmpty(sellerName))
            {
                vpiList = vpiList.Where(x => x.SellerName.Contains(sellerName)).ToList();
            }

            //進倉單日期
            if (startTime != null)
            {
                vpiList = vpiList.Where(x => x.CreateTime >= startTime).ToList();
            }

            if (endTime != null)
            {
                vpiList = vpiList.Where(x => x.CreateTime <= endTime.Value.AddDays(1)).ToList();
            }

            vpiList = vpiList.Where(t => t.WarehouseType == warehouseType).ToList();

            return vpiList;
        }

        /// <summary>
        /// 依照頁數取規格
        /// </summary>
        public static dynamic GetWmsReturnOrder(List<ViewWmsReturnOrder> vpiList, int pageStart, int pageLength)
        {
            int totalCount = vpiList.Count();
            int totalPage = (totalCount / pageLength);
            if ((totalCount % pageLength) > 0)
            {
                totalPage += 1;
            }
            if (totalCount == 0)
            {
                totalPage = 1;
            }

            var rtnData = vpiList.OrderBy(z => z.ProductNo).OrderBy(y => y.ProductBrandName).OrderBy(x => x.ProductName)
                .Skip((pageStart - 1) * pageLength).Take(pageLength).ToList().Select(x => new
                {
                    SellerGuid = x.SellerGuid,
                    SellerName = x.SellerName,
                    InfoGuid = x.InfoGuid,
                    ItemGuid = x.ItemGuid,
                    PchomeProdId = x.PchomeProdId ?? string.Empty,
                    ReturnOrderGuid = x.ReturnOrderGuid,
                    PchomeReturnOrderId = x.PchomeReturnOrderId ?? string.Empty,
                    ProductNo = x.ProductNo,
                    ProductBrandName = x.ProductBrandName,
                    ProductName = x.ProductName,
                    SpecName = x.SpecName,
                    ProductCode = x.ProductCode,
                    CreateTime = x.CreateTime.ToString("yyyy/MM/dd"),
                    Remark = x.Remark ?? string.Empty,
                    Reason = x.Reason ?? string.Empty,
                    Status = x.Status,
                    StatusText = Helper.GetDescription((WmsReturnOrderStatus)x.Status),
                    Qty = x.Qty,
                    Price = x.Price,
                    Source = x.Source,
                    SourceText = Helper.GetDescription((WmsReturnSource)x.Source),
                    Invalidation = (x.Invalidation == 1),
                }).ToList();

            dynamic obj = new
            {
                TotalCount = totalCount,
                TotalPage = totalPage,
                CurrentPage = pageStart,
                Data = rtnData
            };
            return obj;
        }


        #endregion
        #region 抓取商家帳號
        public static List<SellerAccountModel> GetSellerAccount(Guid bid)
        {
            List<Guid> pStoreGuid = new List<Guid>();
            List<SellerAccountModel> sellerModel = new List<SellerAccountModel>();
            //先撈store
            var pStore = _pponProv.PponStoreGetListByBusinessHourGuid(bid);
            if (pStore.Count() > 0)
            {
                foreach (var pstore in pStore)
                {
                    pStoreGuid.Add(pstore.StoreGuid);
                }
            }

            if (pStoreGuid.Count() > 0)
            {
                IEnumerable<Guid> resources = pStoreGuid.ToList();
                var acls = _memProv.ResourceAclGetListByResourceGuidList(resources)
                    .Where(x => ((PermissionType)x.PermissionType).HasFlag(PermissionType.Read)
                            && ((PermissionType)x.PermissionSetting).HasFlag(PermissionType.Read))
                    .GroupBy(x => new { x.AccountId, x.ResourceGuid });

                foreach (var pstoreguid in pStoreGuid)
                {
                    string accountId = string.Empty;
                    var accountCollection = acls.Where(p => p.Key.ResourceGuid == pstoreguid);
                    if (accountCollection.Count() > 0)
                    {
                        foreach (var account in accountCollection)
                        {
                            if (string.IsNullOrEmpty(accountId))
                            {
                                accountId = account.Key.AccountId;
                            }
                            else
                            {
                                accountId = accountId + "," + account.Key.AccountId;
                            }
                        }
                    }
                    sellerModel.Add(new SellerAccountModel()
                    {
                        sellerName = _sellerProv.SellerGet(pstoreguid).SellerName,
                        accountId = accountId
                    });

                }
            }

            return sellerModel;
        }
        #endregion

        public static bool SetDealProductDelivery(Guid sellerGuid, Guid businessHourGuid, int boxQuantityLimit)
        {
            ViewVbsInstorePickupCollection vips = _sellerProv.ViewVbsInstorePickupCollectionGet(sellerGuid);
            var vipList = vips.Where(x => x.IsEnabled == true).Select(t => new
            {
                SellerGuid = t.SellerGuid,
                ServiceChannel = t.ServiceChannel
            });

            ViewComboDealCollection vcds = _pponProv.GetViewComboDealAllByBid(businessHourGuid);
            if (vcds.Count > 0)
            {
                if (vcds.FirstOrDefault().MainBusinessHourGuid == businessHourGuid)
                {
                    List<Guid> mainBid = new List<Guid>();
                    mainBid.Add(businessHourGuid);
                    Proposal pro = _sellerProv.ProposalGetByBids(mainBid).FirstOrDefault();
                    List<int> pids = new List<int>();
                    pids.Add(pro.Id);
                    ProposalMultiDealCollection pmds = _sellerProv.ProposalMultiDealGetListByPid(pids);
                    foreach (ProposalMultiDeal pmd in pmds)
                    {
                        if (!string.IsNullOrEmpty(pmd.BoxQuantityLimit))
                        {
                            int box = 0;
                            int.TryParse(pmd.BoxQuantityLimit, out box);
                            if (box > 0)
                            {
                                boxQuantityLimit = box;
                            }
                        }
                    }
                }
            }

            DealProductDelivery dpDelivery = _pponProv.DealProductDeliveryGetByBid(businessHourGuid);
            if (!dpDelivery.IsLoaded)
            {
                dpDelivery = new DealProductDelivery();
            }
            dpDelivery.BusinessHourGuid = businessHourGuid;
            dpDelivery.EnableDelivery = true;

            if (boxQuantityLimit > 0)
            {
                dpDelivery.EnableIsp = vipList.Any();
            }
            else
            {
                //boxQuantityLimit 若為0，則是該檔次不允許超商取貨
                dpDelivery.EnableIsp = false;
            }

            dpDelivery.IspQuantityLimit = boxQuantityLimit;

            _pponProv.DealProductDeliverySet(dpDelivery);

            return true;
        }

        private static HashSet<string> AllowBidsForOnlineTest;

        public static List<ServiceChannel> GetIspServiceChannelByBid(Guid bid)
        {
            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            if (vpd.IsLoaded)
            {
                return GetIspServiceChannelBySellerGuid(vpd.SellerGuid);
            }

            return new List<ServiceChannel>();
        }

        public static List<ServiceChannel> GetIspServiceChannelBySellerGuid(Guid sellerGuid)
        {
            var data = _sellerProv.VbsInstorePickupGet(sellerGuid).ToList();
            data = data.Where(x => x.Status == (int)ISPStatus.Complete && x.IsEnabled == true).ToList();

            if (data.Any())
            {
                return data.Select(x => (ServiceChannel)x.ServiceChannel).ToList();
            }

            return new List<ServiceChannel>();
        }

        public static string ReplaceMobileYoutubeTag(string resource)
        {
            resource = resource.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
            string description = resource.Replace("http://www.youtube", "https://www.youtube");
            if (description.IndexOf("object") >= 0)
            {
                string pattern = @"<object (?<first>.*?)><param name=""movie"" value=""https://www.youtube.com/v/(?<url>.*?)""(?<tag1>.*?)>(?<last>.*?)</object>";
                System.Text.RegularExpressions.MatchCollection matches = System.Text.RegularExpressions.Regex.Matches(description, pattern);
                foreach (Match m in matches)
                {
                    string res = @"<object " + m.Groups["first"].Value + @"><param name=""movie"" value=""https://www.youtube.com/v/" + m.Groups["url"].Value + @"""" + m.Groups["tag1"].Value + ">" + m.Groups["last"].Value + "</object>";
                    string result = @"<iframe width=""560"" height=""315"" src=""https://www.youtube.com/embed/" + m.Groups["url"].Value + @""" frameborder=""0"" allowfullscreen></iframe>";
                    description = description.Replace(res, result);
                }
            }
            return description;
        }
        public static string GetCchannelLink(string url)
        {
            string youtubeLink = url.Replace("http://", "https://");
            string YoutubeLinkRegex = "(?:.+?)?(?:\\/v\\/|watch\\/|\\?v=|\\&v=|youtu\\.be\\/|\\/v=|^youtu\\.be\\/)([a-zA-Z0-9_-]{11})+";
            Regex regexExtractId = new Regex(YoutubeLinkRegex, RegexOptions.Compiled);
            var regRes = regexExtractId.Match(youtubeLink.ToString());
            if (regRes.Groups.Count >= 2)
            {
                youtubeLink = "https://www.youtube.com/embed/" + regRes.Groups[1].Value;
            }
            string result = @"<iframe width=""560"" height=""315"" src=""" + youtubeLink + @""" frameborder=""0"" allowfullscreen></iframe>";
            return result;
        }
        public static List<string> GetOuterUri(string source)
        {
            List<string> uriList = new List<string>();
            //Regex r = new Regex(@"(?<Protocol>\w+):\/\/(?<Domain>[\w@][\w.:@]+)\/?[\w\.?=%&=\-@/$,]*");
            //Regex r = new Regex("&lt;img.+?src=[\"'](.+?)[\"'].+?&gt;");
            Regex r = new Regex("&lt;img.+?src=[\"'](.+?)[\"'].+?&gt;");
            Match m = r.Match(source);

            string[] proposalImageAllowDomain = config.ProposalImageAllowDomain.Split(";");

            while (m.Success)
            {
                Regex rUrl = new Regex(@"(?<Protocol>\w+):\/\/(?<Domain>[\w@][\w.:@]+)\/?[\w\.?=%&=\-@/$,]*");
                Match mUrl = rUrl.Match(m.Groups[0].Value);
                if (mUrl.Success)
                {
                    uriList.Add(mUrl.Groups["Domain"].Value);
                }

                m = m.NextMatch();
            }

            return uriList.Where(x => !proposalImageAllowDomain.Contains(x)).Distinct().ToList();
        }
        /// <summary>
        /// 一姬說好康的欄位是複合型態，同時包含了品生活的頁籤檔次介紹。
        /// 這邊只取得一姬說好康的內文
        /// </summary>
        /// <param name="remark"></param>
        /// <returns></returns>
        public static string GetPponRemark(string remark)
        {
            if (string.IsNullOrEmpty(remark) || remark.Length < 3)
            {
                return remark;
            }
            if (remark.Substring(0, 3) == "0||")
            {
                return remark.Split(new[] { "||" }, StringSplitOptions.None)[2];
            }
            return remark;
        }
        public static BrandItemCollection ValidateAndGetItemListAndAlertMsg(
            List<string> dataList,
            int brandId,
            string userName,
            out string alertMsg)
        {
            BrandItemCollection itemCollection = new BrandItemCollection();
            alertMsg = string.Empty;


            var items = _pponProv.GetBrandItemList(brandId);
            int maxSeq = 0;
            if (items.Count > 0)
            {
                maxSeq = items.OrderByDescending(x => x.Seq).First().Seq;
            }
            else //第一次匯入要塞入"全部"分類
            {
                //先做一次清除此Brand所有分類的動作
                _pponProv.DeleteBrandItemCategoryByBrandId(brandId);

                //塞入全部分類
                BrandItemCategory bic = new BrandItemCategory();
                bic.BrandId = brandId;
                bic.Name = "全部";
                bic.CreateId = userName;
                bic.CreateTime = DateTime.Now;
                bic.Seq = 0;
                _pponProv.SaveBrandItemCategory(bic);
            }

            for (int i = 0; i < dataList.Count; i++)
            {
                Guid bid = new Guid();
                if (Guid.TryParse(dataList[i], out bid))
                {
                    ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(bid);
                    if (vpd != null && vpd.IsLoaded && vpd.UniqueId.HasValue)
                    {

                        if (!Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                        {

                            if (vpd.GroupOrderStatus != null && Helper.IsFlagSet((long)vpd.GroupOrderStatus, GroupOrderStatus.Completed))
                            {
                                alertMsg += string.Format("第{0}列-此bid已結檔，無法匯入：{1}<br />", i + 1, dataList[i]);
                                continue;
                            }

                            if (vpd.BusinessHourOrderTimeE <= DateTime.Now)
                            {
                                alertMsg += string.Format("第{0}列-此bid已下檔，無法匯入：{1}<br />", i + 1, dataList[i]);
                                continue;
                            }

                            #region 檢查ViewPponDeal的欄位是否有填
                            bool columnsComplete = true;
                            string editLink = string.Format("<a target='_blank' href='{0}/controlroom/ppon/setup.aspx?bid={1}'>{1}</a>", config.SiteUrl, dataList[i]);
                            if (string.IsNullOrWhiteSpace(vpd.EventTitle))
                            {
                                //columnsComplete = false; 暫時不擋僅出現警語，1111之後在考慮怎麼做阻擋
                                alertMsg += string.Format("第{0}列-行銷標題(橘標)未填：{1}<br />", i + 1, editLink);
                            }
                            if (string.IsNullOrWhiteSpace(vpd.ItemName))
                            {
                                //columnsComplete = false; 暫時不擋僅出現警語，1111之後在考慮怎麼做阻擋
                                alertMsg += string.Format("第{0}列-訂單短標(品牌名稱.商品)未填：{1}<br />", i + 1, editLink);
                            }
                            #endregion

                            if (columnsComplete)
                            {
                                BrandItem item = _pponProv.GetBrandItem(brandId, vpd.UniqueId.Value);
                                if (item == null)
                                {
                                    item = new BrandItem();
                                    item.MainId = brandId;
                                    item.Seq = maxSeq + i + 1;
                                    item.Status = true;
                                    item.ItemId = vpd.UniqueId.Value;
                                    item.Creator = userName;
                                    item.CreateTime = DateTime.Now;
                                }
                                itemCollection.Add(item);
                            }
                        }
                        else
                        {
                            alertMsg += string.Format("第{0}列-匯入的bid為子檔的：{1}<br />{3}(必須為母檔的：{2})", i + 1, dataList[i], vpd.MainBid, string.Join("&nbsp;", new string[18]));//18為實際測出來的寬度，為了對齊
                        }
                    }
                    else
                    {
                        alertMsg += string.Format("第{0}列-查無此bid：{1}<br />", i + 1, dataList[i]);
                    }
                }
                else
                {
                    alertMsg += string.Format("第{0}列-bid格式有誤：{1}<br />", i + 1, dataList[i]);
                }
            }
            if (!string.IsNullOrEmpty(alertMsg))
            {
                alertMsg = "匯入警告:<br />" + alertMsg;
            }
            else
            {
            }
            return itemCollection;
        }

        public static void EditBrandItemCategory(string mode, int bicId, List<int> selectItems, int brandId)
        {
            //取得單一Brand的BrandItem分類cid(全部)
            var brandItemCategoryAll = _pponProv.GetBrandItemCategoryByBrandId(brandId).Where(x => x.Name.Equals("全部")).First();


            foreach (int brandItemId in selectItems)
            {
                if (mode == "Replace")
                {
                    //取代分類，需先刪除勾選商品的所有分類
                    _pponProv.DeleteBrandItemCategoryDependency(brandItemId);
                }
                else if (mode == "Add")
                {
                    if (bicId > 0)
                    {
                        //新增分類，若有此分類資料則先刪除
                        _pponProv.DeleteBrandItemCategoryDependencyBybicId(bicId, brandItemId);
                    }
                }
            }

            if (mode == "Replace" || mode == "Add")
            {
                //新增勾選商品的選擇的分類
                foreach (int brandItemId in selectItems)
                {
                    if (bicId > 0)
                    {
                        BrandItemCategoryDependency bidc;

                        //取代分類需將分類"全部"補回去
                        if (brandItemCategoryAll != null && mode == "Replace")
                        {
                            bidc = new BrandItemCategoryDependency();
                            bidc.BrandItemId = brandItemId;
                            bidc.BrandItemCategoryId = brandItemCategoryAll.Id;
                            _pponProv.SaveBrandItemCategoryDependency(bidc);
                        }

                        bidc = new BrandItemCategoryDependency();
                        bidc.BrandItemId = brandItemId;
                        bidc.BrandItemCategoryId = bicId;
                        _pponProv.SaveBrandItemCategoryDependency(bidc);
                    }
                }
            }
        }
        /// <summary>
        /// 產生 Deal 商品頁用的 SEO / Linke html metas
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static string RenderMetasForDeal(IViewPponDeal deal)
        {
            string dealUrl = Helper.GetDealDeepLink(deal.BusinessHourGuid);
            StringBuilder sb = new StringBuilder();
            //for SEO
            sb.AppendFormat("\t<meta property=\"og:type\" content=\"website\" />\r\n");
            string ogTitle = HttpUtility.HtmlEncode(deal.EventName.TrimToMaxLength(60, "..."));
            sb.AppendFormat("\t<meta property=\"og:title\" content=\"{0}\" />\r\n", deal.CouponUsage);
            sb.AppendFormat("\t<meta property=\"og:url\" content=\"{0}\" />\r\n", dealUrl);
            sb.AppendFormat("\t<meta property=\"og:site_name\" content=\"17Life\" />\r\n");
            sb.AppendFormat("\t<meta property=\"og:description\" content=\"{0}\" />\r\n", Phrase.FacebookShareDescription);
            sb.AppendFormat("\t<meta property=\"og:image\" content=\"{0}\" />\r\n", deal.DefaultDealImage);
            //for Line
            sb.AppendFormat("\t<meta property=\"image_src\" content=\"{0}\" />\r\n", deal.DefaultDealImage);

            string open17LifeUrl = string.Format("open17life://www.17life.com/deal/{0}", deal.BusinessHourGuid);
            sb.AppendFormat("\t<meta property=\"al:ios:url\" content=\"{0}\" />\r\n", open17LifeUrl);
            sb.AppendFormat("\t<meta property=\"al:ios:app_store_id\" content=\"{0}\" />\r\n", config.AppLinksiOSId);
            sb.AppendFormat("\t<meta property=\"al:ios:app_name\" content=\"{0}\" />\r\n", config.AppLinksiOSName);
            sb.AppendFormat("\t<meta property=\"al:android:url\" content=\"{0}\" />\r\n", open17LifeUrl);
            sb.AppendFormat("\t<meta property=\"al:android:package\" content=\"{0}\" />\r\n", config.AppLinksAndroidPackage);
            sb.AppendFormat("\t<meta property=\"al:android:app_name\" content=\"{0}\" />\r\n", config.AppLinksAndroidAppName);

            return sb.ToString();
        }

        public static string RenderCanonicalRel(IViewPponDeal deal)
        {
            string dealUrl = Helper.GetDealDeepLink(deal.BusinessHourGuid);
            //結檔後，轉檔的canonical判斷
            string result = string.Format("\t<link rel=\"canonical\" href=\"{0}\" />\r\n",
                (deal.UseExpireRedirectUrlAsCanonical ?? false) ? deal.ExpireRedirectUrl : dealUrl);

            return result;
        }

        public static string GetDisplayPponName(IViewPponDeal vpd)
        {
            if (!string.IsNullOrEmpty(vpd.AppTitle))
            {
                return vpd.AppTitle;
            }
            else
            {
                return vpd.ItemName;
            }
        }

        #region LineShoppingGuide

        private const string UpdateConditionCacheKey = "LinePartialUpdateConditionInfo";
        private const string CategoryCacheKey = "LineProductCategoryForDeal";
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<LineProductData> GetLineProductList()
        {

            List<IViewPponDeal> pponDealList = ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true);

            if (!pponDealList.Any()) return new List<LineProductData>();

            //取得分類列表
            Dictionary<int, string> categoryDic = ChannelFacade.GetLineProductCategoryList().
                ToLookup(x => int.Parse(x.CategoryValue), x => x.CategoryTitle).ToDictionary(x => x.Key, x => x.First());

            //取得黑白名單
            LineCommissionCollection commissions = GetLineCommissionList();

            //for更新比對用的容器
            var updateConditions = new ConcurrentDictionary<Guid, LinePartialUpdateConditionInfo>();
            //for拋單用的資料容器
            var categoryCacheDic = new ConcurrentDictionary<Guid, LineProductCategoryForDeal>();

            var result = new List<LineProductData>();

            foreach (var deal in pponDealList)
            {
                var mainBid = deal.MainBid ?? deal.BusinessHourGuid;

                if (deal.IsHiddenInRecommendDeal) continue;
                //判斷檔次是否符合過檔條件
                bool isDiscontinued = ShouldExcludeIViewPponDeal(deal, commissions);
                //剔除不符合篩選邏輯的檔次(首次撈全檔時)
                if (isDiscontinued) continue;

                var latestUpdateCondition = new LinePartialUpdateConditionInfo()
                {
                    ModifyTime = deal.EventModifyTime,
                    //用於判斷DealTimeSlotStatus是否有變動
                    IsHiddenInRecommendDeal = deal.IsHiddenInRecommendDeal
                };

                updateConditions.TryAdd(mainBid, latestUpdateCondition);

                //取得整理過後的分類
                string categoryIds = GetLineCategoryIdsByDeal(deal);

                //跳過無分類的檔次
                if (categoryIds.Length == 0) continue;

                //建構式中整理資料
                LineProductData data = new LineProductData(deal, categoryIds, isDiscontinued);

                result.Add(data);
                categoryCacheDic.AddOrUpdate(mainBid, delegate { return new LineProductCategoryForDeal(deal, categoryIds, categoryDic); },
                    delegate { return new LineProductCategoryForDeal(deal, categoryIds, categoryDic); });
            }

            MemoryCache2.Set<ConcurrentDictionary<Guid, LinePartialUpdateConditionInfo>>(UpdateConditionCacheKey, updateConditions, 1440);
            MemoryCache2.Set<ConcurrentDictionary<Guid, LineProductCategoryForDeal>>(CategoryCacheKey, categoryCacheDic, 1440);

            return result;
        }
        /// <summary>
        /// 取得更新過的檔次資料
        /// </summary>
        /// <returns></returns>
        public static List<LineProductData> GetLineProductPartialList()
        {
            var updateConditions = MemoryCache2.Get<ConcurrentDictionary<Guid, LinePartialUpdateConditionInfo>>(UpdateConditionCacheKey);

            if (updateConditions != null && updateConditions.Any())
            {
                List<IViewPponDeal> pponDealList = ViewPponDealManager.DefaultManager.ViewPponDealGetListByGuidList(updateConditions.Keys.ToList());
                if (!pponDealList.Any()) return new List<LineProductData>();
                //取得分類列表
                Dictionary<int, string> categoryDic = ChannelFacade.GetLineProductCategoryList().
                    ToLookup(x => int.Parse(x.CategoryValue), x => x.CategoryTitle).ToDictionary(x => x.Key, x => x.First());
                List<int> CategoryIds = categoryDic.Keys.ToList();
                //取得黑白名單
                LineCommissionCollection commissions = GetLineCommissionList();
                //for拋單用的資料
                var categoryCacheDic = MemoryCache2.Get<ConcurrentDictionary<Guid, LineProductCategoryForDeal>>(CategoryCacheKey) ?? new ConcurrentDictionary<Guid, LineProductCategoryForDeal>();

                var result = new List<LineProductData>();

                foreach (var deal in pponDealList)
                {
                    var mainBid = deal.MainBid ?? deal.BusinessHourGuid;

                    //判斷檔次是否符合過檔條件
                    bool isDiscontinued = ShouldExcludeIViewPponDeal(deal, commissions);

                    var latestUpdateCondition = new LinePartialUpdateConditionInfo()
                    {
                        ModifyTime = deal.EventModifyTime,
                        //用於判斷DealTimeSlotStatus是否有變動
                        IsHiddenInRecommendDeal = deal.IsHiddenInRecommendDeal
                    };

                    //若加入失敗代表資料已存在，就去比對修改時間及DealTimeSlotStatus
                    if (!updateConditions.TryAdd(mainBid, latestUpdateCondition))
                    {
                        //判斷資料是否有更新
                        LinePartialUpdateConditionInfo partialInfo = updateConditions[mainBid];

                        if (partialInfo.ModifyTime == latestUpdateCondition.ModifyTime && partialInfo.IsHiddenInRecommendDeal == latestUpdateCondition.IsHiddenInRecommendDeal)
                        {
                            continue;
                        }
                        else
                        {
                            updateConditions[mainBid] = latestUpdateCondition;
                        }
                    }

                    string categoryIds = GetLineCategoryIdsByDeal(deal);

                    //跳過無分類的檔次
                    if (categoryIds.Length == 0) continue;

                    //建構式中整理資料
                    LineProductData data = new LineProductData(deal, categoryIds, isDiscontinued);

                    result.Add(data);
                    categoryCacheDic.AddOrUpdate(mainBid, delegate { return new LineProductCategoryForDeal(deal, categoryIds, categoryDic); },
                        delegate { return new LineProductCategoryForDeal(deal, categoryIds, categoryDic); });
                }

                MemoryCache2.Set<ConcurrentDictionary<Guid, LinePartialUpdateConditionInfo>>(UpdateConditionCacheKey, updateConditions, 1440);
                MemoryCache2.Set<ConcurrentDictionary<Guid, LineProductCategoryForDeal>>(CategoryCacheKey, categoryCacheDic, 1440);

                return result;
            }
            return GetLineProductList();
        }
        /// <summary>
        /// 返回檔的分類(符合LineDataFeed格式)
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static string GetLineCategoryIdsByDeal(IViewPponDeal deal)
        {

            string cacheKey = "LineCategoryIds";
            CacheDataStatus dataStatus;
            List<int> CategoryIds = MemoryCache2.Get<List<int>>(cacheKey, out dataStatus) ?? ChannelFacade.GetLineProductCategoryList().Select(x => int.Parse(x.CategoryValue)).ToList();
            if (dataStatus != CacheDataStatus.OK)
            {
                MemoryCache2.Set<List<int>>(cacheKey, CategoryIds, 10);
            }

            string result = "";
            var intersectedList = deal.CategoryIds.Intersect(CategoryIds).ToList();
            foreach (var category in intersectedList)
            {
                //檢查是否會超過50Characters
                if (result.Length + category.ToString().Length > 50) break;
                result += category + ",";
            }
            result = result.TrimEnd(',');

            return result;
        }
        /// <summary>
        /// 帶入母檔or子檔最後返回母檔分類
        /// </summary>
        /// <param name="bid">母檔or子檔</param>
        /// <returns></returns>
        public static LineProductCategoryForDeal GetLineProductCategoryByBid(Guid bid)
        {
            //嘗試從快取拿資料
            var resultDic =
                MemoryCache2.Get<ConcurrentDictionary<Guid, LineProductCategoryForDeal>>(CategoryCacheKey) ??
                new ConcurrentDictionary<Guid, LineProductCategoryForDeal>();

            var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            var mainBid = deal.MainBid ?? deal.BusinessHourGuid;

            if (!resultDic.ContainsKey(mainBid))
            {
                //若是子檔則取母檔bid再撈一次
                if (deal.MainBid != deal.BusinessHourGuid)
                {
                    deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(mainBid);
                }
                //取得分類列表
                Dictionary<int, string> lineProductCategoryDic = ChannelFacade.GetLineProductCategoryList().
                    ToLookup(x => int.Parse(x.CategoryValue), x => x.CategoryTitle).ToDictionary(x => x.Key, x => x.First());

                string categoryIds = GetLineCategoryIdsByDeal(deal);

                //建構式中整理資料
                var data = new LineProductCategoryForDeal(deal, categoryIds, lineProductCategoryDic);

                resultDic.TryAdd(mainBid, data);
                MemoryCache2.Set<ConcurrentDictionary<Guid, LineProductCategoryForDeal>>(CategoryCacheKey, resultDic, 1440);

                return resultDic[mainBid];
            }

            return resultDic[mainBid];
        }

        /// <summary>
        /// 是否為排除的檔次(包含處理黑白名單)
        /// </summary>
        /// <param name="deal">母檔bid</param>
        /// <returns></returns>
        public static bool ShouldExcludeIViewPponDeal(IViewPponDeal deal)
        {
            var lineCommission = GetLineCommissionList();
            return ShouldExcludeIViewPponDeal(deal, lineCommission);
        }

        /// <summary>
        /// 是否為排除的檔次(只能傳母檔bid)
        /// </summary>
        /// <param name="deal">母檔bid</param>
        /// <param name="col">黑白名單</param>
        /// <returns></returns>
        public static bool ShouldExcludeIViewPponDeal(IViewPponDeal deal, LineCommissionCollection col)
        {
            var mainBid = deal.MainBid ?? deal.BusinessHourGuid;

            //判斷是否為黑名單內的檔次
            //商家黑名單要確認母檔白名單不包含在內
            if (col.SellerBlackGuidList.Contains(deal.SellerGuid)
                && !col.MainDealWhiteGuidList.Contains(mainBid)) return true;

            if (col.MainDealBlackGuidList.Contains(mainBid)) return true;

            //0元
            if (!(deal.ItemPrice > 0)) return true;

            //沒有圖片
            if (string.IsNullOrEmpty(deal.AppDealPic) && string.IsNullOrEmpty(deal.EventImagePath)) return true;

            //排除新光檔
            if (Helper.IsFlagSet(deal.GroupOrderStatus.GetValueOrDefault(), GroupOrderStatus.SKMDeal)) return true;

            //無條件導回首頁
            if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb)) return true;

            //
            if (deal.CityIds.Count == 0) return true;

            //排除全家檔次
            if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal)) return true;

            //排除萊爾富檔次
            if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.HiLifeDeal)) return true;

            //排除APP限定檔次
            if (deal.CategoryIds.Any(x => x == CategoryManager.Default.AppLimitedEdition.CategoryId)) return true;

            //排除隱藏檔次
            List<IDealTimeSlot> dealTimeSlots = ViewPponDealManager.DefaultManager.DealTimeSlotGetByBidAndTime(
                    deal.BusinessHourGuid, DateTime.Now);
            if (dealTimeSlots.Any(t => t.Status == (int)DealTimeSlotStatus.NotShowInPponDefault)) return true;

            #region 白名單篩選邏輯

            //判斷是否為白名單內的檔次
            bool isWhiteDeal = false;
            isWhiteDeal = col.SellerWhiteGuidList.Contains(deal.SellerGuid)
                || col.MainDealWhiteGuidList.Contains(mainBid) ? true : false;

            if (!isWhiteDeal)
            {
                //排除公益檔
                if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal)) return true;

                //排除低毛利檔次
                var minGrossMargin = ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(deal.BusinessHourGuid).MinGrossMargin;

                if (Math.Round(minGrossMargin, 4) < config.LineMinGrossMargin) return true;

                //排除子檔內含低毛利檔次
                if (deal.ComboDelas != null && deal.ComboDelas.Any())
                {
                    if (ChannelFacade.IsOverLowGrossmarginDeals(deal.ComboDelas.Select(y => y.BusinessHourGuid).ToList(), config.LineMinGrossMargin)) return true;
                }
            }

            #endregion

            return false;
        }
        /// <summary>
        /// 取得Line黑白名單
        /// </summary>
        /// <returns></returns>
        public static LineCommissionCollection GetLineCommissionList()
        {
            var result = new LineCommissionCollection();

            List<ChannelCommissionList> list = _channelProv.ChannelCommissionListGet((int)ChannelSource.Line).ToList();

            if (list.Any())
            {
                foreach (var item in list)
                {
                    //判斷黑白名單
                    List<Guid> sellerList = item.Type == (int)CommissionListType.WhiteList ? result.SellerWhiteGuidList : result.SellerBlackGuidList;
                    List<Guid> mainDealList = item.Type == (int)CommissionListType.WhiteList ? result.MainDealWhiteGuidList : result.MainDealBlackGuidList;
                    //商家 seller!=null,mainbid=null,bid=null
                    if (item.SellerGuid != null && item.MainBid == null && item.Bid == null)
                    {
                        sellerList.Add((Guid)item.SellerGuid);
                    }
                    //母檔或者子檔 都把mainbid放進去
                    //母檔 seller!=null,mainbid!=null,bid=null or 子檔 seller!=null,mainbid!=null,bid!=null
                    else if (item.MainBid != null && item.Bid == null || item.MainBid != null && item.Bid != null)
                    {
                        mainDealList.Add((Guid)item.MainBid);
                    }
                    //單檔 seller!=null,mainbid=null,bid!=null
                    else if (item.MainBid == null && item.Bid != null)
                    {
                        mainDealList.Add((Guid)item.Bid);
                    }
                }
            }
            return result;
        }

        #endregion
    }
    #region class

    public static class DateTimeHelper
    {
        public static DateTime Noon(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 12, 0, 0);
        }
    }

    public class DealTag
    {
        public enum TagStyle
        {
            Tag1 = 1,
            Tag2 = 2,
            Tag3 = 3,
            Tag4 = 4,
            Tag5 = 5,
            Tag6 = 6,
            Tag7 = 7,
            Tag8 = 8,
            Tag9 = 9,
            Tag10 = 10,
            Tag11 = 11,
            Tag12 = 12
        }

        private TagStyle _style;
        private string _title;

        public DealTag(TagStyle style, string title)
        {
            this._style = style;
            this._title = title;
        }

        public string title
        {
            get
            {
                return _title;
            }
        }

        public TagStyle style
        {
            get
            {
                return _style;
            }
        }
    }

    internal class DealTagHelper
    {
        private static Dictionary<DealTag.TagStyle, string> tagMobileHtmlStyles;
        private static Dictionary<DealLabelSystemCode, DealTag> dealLabelDic;
        static DealTagHelper()
        {
            tagMobileHtmlStyles = new Dictionary<DealTag.TagStyle, string>();
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag1, "tag_1");
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag2, "tag_2");
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag3, "tag_3");
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag4, "tag_4");
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag5, "tag_5");
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag6, "tag_6");
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag7, "tag_7");
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag8, "tag_8");
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag9, "tag_9");
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag10, "tag_10");
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag11, "tag_11");
            tagMobileHtmlStyles.Add(DealTag.TagStyle.Tag12, "tag_12");

            dealLabelDic = new Dictionary<DealLabelSystemCode, DealTag>();
            dealLabelDic.Add(DealLabelSystemCode.TwentyFourHoursArrival, new DealTag(DealTag.TagStyle.Tag4, "24H"));
            dealLabelDic.Add(DealLabelSystemCode.CanBeUsedImmediately, new DealTag(DealTag.TagStyle.Tag3, "即用"));
            dealLabelDic.Add(DealLabelSystemCode.AppLimitedEdition, new DealTag(DealTag.TagStyle.Tag6, "APP限定"));
            dealLabelDic.Add(DealLabelSystemCode.PiinlifeEvent, new DealTag(DealTag.TagStyle.Tag5, "五星美饌"));
            dealLabelDic.Add(DealLabelSystemCode.Visa, new DealTag(DealTag.TagStyle.Tag5, "VISA"));
            dealLabelDic.Add(DealLabelSystemCode.PiinlifeFatherEvent, new DealTag(DealTag.TagStyle.Tag5, "父親節精選"));
            dealLabelDic.Add(DealLabelSystemCode.PiinlifeMoonEvent, new DealTag(DealTag.TagStyle.Tag5, "賞悅中秋"));
            dealLabelDic.Add(DealLabelSystemCode.PiinlifeValentinesEvent, new DealTag(DealTag.TagStyle.Tag5, "情人專屬"));
            dealLabelDic.Add(DealLabelSystemCode.PiinlifeMajiEvent, new DealTag(DealTag.TagStyle.Tag5, "花博MAJI廣場"));
            dealLabelDic.Add(DealLabelSystemCode.PiinlifeRecommend, new DealTag(DealTag.TagStyle.Tag5, "每月推薦"));
            dealLabelDic.Add(DealLabelSystemCode.SellOverOneThousand, new DealTag(DealTag.TagStyle.Tag1, "熱銷"));
            dealLabelDic.Add(DealLabelSystemCode.GroupCoupon, new DealTag(DealTag.TagStyle.Tag5, "套票"));
            dealLabelDic.Add(DealLabelSystemCode.DiscountCanUse, new DealTag(DealTag.TagStyle.Tag11, "折價"));
            dealLabelDic.Add(DealLabelSystemCode.IspShipping, new DealTag(DealTag.TagStyle.Tag12, "超取"));
        }

        public static List<DealTag> GetDealTags(IViewPponDeal vpd, int dealIconsLimit)
        {
            List<DealTag> result = new List<DealTag>();
            int dealIconCount = 0;

            List<int> iconCodeIdList = new List<int>();
            if (!string.IsNullOrEmpty(vpd.LabelIconList))
            {
                iconCodeIdList = vpd.LabelIconList.Split(',').Select(int.Parse).ToList();
                iconCodeIdList.Sort();
            }

            //1.折價券
            if (vpd.DiscountUseType != null && vpd.DiscountUseType.Value)
            {
                result.Add(dealLabelDic[DealLabelSystemCode.DiscountCanUse]);
                dealIconCount++;
            }

            //2.24H
            if (iconCodeIdList.Contains((int)DealLabelSystemCode.TwentyFourHoursArrival))
            {
                //result.Add(dealLabelDic[DealLabelSystemCode.TwentyFourHoursArrival]);
                //dealIconCount++;
                iconCodeIdList.Remove((int)DealLabelSystemCode.TwentyFourHoursArrival);
            }


            //3.超取
            if (vpd.EnableIsp)
            {
                result.Add(dealLabelDic[DealLabelSystemCode.IspShipping]);
                dealIconCount++;
            }

            //排除公益檔次的破千破萬 Icon
            if (!Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
            {
                QuantityAdjustment adj = vpd.GetAdjustedOrderedQuantity();

                //熱銷 Icon
                if (adj.Quantity >= 1000)
                {
                    result.Add(dealLabelDic[DealLabelSystemCode.SellOverOneThousand]);
                    dealIconCount++;
                }
            }


            foreach (var iconCodeId in iconCodeIdList)
            {
                DealTag dealTag;
                DealLabelSystemCode dealLabelKey = (DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), iconCodeId);
                if (dealLabelDic.TryGetValue(dealLabelKey, out dealTag))
                {
                    result.Add(dealTag);
                }

                dealIconCount++;
                if (dealIconCount >= dealIconsLimit)
                {
                    break;
                }
            }

            return result;
        }

        public static string RenderDealTagsAsMobileHtml(List<DealTag> dealTags)
        {
            StringBuilder sb = new StringBuilder();
            foreach (DealTag dealTag in dealTags)
            {
                sb.AppendFormat("<li class=\"{0}\">{1}</li>", tagMobileHtmlStyles[dealTag.style], dealTag.title);
            }
            return sb.ToString();
        }

        public static bool HasLastDayTag(IViewPponDeal vpd)
        {
            return false;
            //TimeSpan soldEndDays = new TimeSpan(vpd.BusinessHourOrderTimeE.Ticks - DateTime.Now.Ticks);
            //if (0 <= soldEndDays.TotalDays && soldEndDays.TotalDays < 1)
            //{
            //    return true;
            //}
            //return false;
        }
    }

    public class ImpersonateAccount
    {
        public string UserName { get; set; }

        public string PassWord { get; set; }

        public string UNCPath { get; set; }

        public ImpersonateAccount(string username, string password, string unc_path)
        {
            UserName = username;
            PassWord = password;
            UNCPath = unc_path;
        }

        public ImpersonateAccount()
        { }
    }

    public class RelatedDeal
    {
        public int CityID { set; get; }
        public Guid BusinessHourGuid { set; get; }
        public bool IsSoldOut { set; get; }
        public string DealImageUrl { set; get; }
        public string DealPromoImage { set; get; }
        public string DealTitle { set; get; }
        public string ItemPrice { set; get; }
        public string SaleMessage { set; get; }
        public string SellCount { set; get; }
        public string DealIcon { set; get; }
        public int Priority { set; get; }
        public decimal ItemOrigPrice { set; get; }
        public decimal ItemPriceValue { set; get; }
        public int? GroupOrderStatus { set; get; }
        public int BusinessHourStatus { set; get; }
        public string RecommendLink { set; get; }
        public string EventTitle { set; get; }

        public RelatedDeal()
        {
            ItemOrigPrice = 0;
            ItemPriceValue = 0;
        }
    }

    public class SellerAccountModel
    {
        public string sellerName { get; set; }
        public string accountId { get; set; }
    }

    public class BuyRelatedDeal
    {
        public Guid BusinessHourGuid { set; get; }
        public string CouponUsage { set; get; }
        public string EventTitle { set; get; }
        public decimal ItemPrice { set; get; }
        public decimal ItemOrigPrice { set; get; }
        public string EventImagePath { set; get; }
        public int BusinessHourStatus { set; get; }
        public string NavigateUrl { set; get; }
        public string discountText { set; get; }
        public string ImageUrl { set; get; }
        public string PriceText { set; get; }
    }

    public class SideDeal
    {
        public int CityID { set; get; }
        public Guid BusinessHourGuid { set; get; }
        public string EventName { set; get; }
        public string OverBadge { set; get; }
        public string SidePic { set; get; }
        public string ImageUrl { set; get; }
        public string SaleMessage { set; get; }
        public string ItemPrice { set; get; }
        public string ItemName { get; set; }
        public bool IsMultipleStores { get; set; }
        public string HoverMessage { get; set; }
        public string PromoPic { get; set; }
        public string SaleQuantity { get; set; }
        public SideDeal() { }
        public string SiteUrl { get; set; }
        public bool IsSoldOut { get; set; }
        public string LinkString
        {
            get
            {
                return string.Format("{0}/{1}/{2}/17_sidedeal", SiteUrl, CityID, BusinessHourGuid);
            }
        }
    }

    public class DealWeightScore
    {
        public Guid Bid { set; get; }
        public int CityId { set; get; }
        public DateTime EffStart { set; get; }
        public decimal WeightScore { set; get; }
        public decimal OrderedTotal { set; get; }
    }

    public class DealTimeSlotKeyAndSeq
    {
        public Guid Bid { set; get; }
        public int CityId { set; get; }
        public DateTime EffStart { set; get; }
        public int Seq { set; get; }
    }

    #endregion class

}
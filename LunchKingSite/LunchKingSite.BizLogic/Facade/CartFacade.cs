﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using PaymentType = LunchKingSite.Core.PaymentType;
using System.Transactions;
using System.IO;

namespace LunchKingSite.BizLogic.Facade
{
    public class CartFacade
    {
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        private static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger("ECPayService");

        public static ApiReturnObject MakeOrderByCart(MainCartDTO mainCartDTO, string userName, bool withBuilding,
            bool supportGroupCoupon = false, ApiUserDeviceType apiUserDeviceType = ApiUserDeviceType.WebService)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            List<CartPaymentDTO> defaultPaymentDTOList = mainCartDTO.SmallCart.FirstOrDefault().PaymentDTOList;
            CartPaymentDTO defaultPaymentDTO = defaultPaymentDTOList.FirstOrDefault();

            bool IsMasterPass = mainCartDTO.IsMasterPass;
            //Web不須登入也可購買
            int userId = MemberFacade.GetUniqueId(userName);

            // make shoppingcartgroup
            ShoppingCartGroup cartGroup = OrderFacade.MakeShoppingCartGroup(userId);

            foreach (SmallCart smallCart in mainCartDTO.SmallCart)
            {
                ShoppingCartGroupD cartGroupD = OrderFacade.MakeShoppingCartGroupD(cartGroup, userId);
                foreach (CartPaymentDTO paymentDTO in smallCart.PaymentDTOList)
                {
                    #region Check Order
                    if (mainCartDTO.IsMasterPass)
                    {
                        if (mainCartDTO.MasterPassTransData == null || !mainCartDTO.MasterPassTransData.IsValid())
                        {
                            rtnObject.Code = ApiReturnCode.PaymentError;
                            rtnObject.Message = "MasterPass 資料不正確";
                            return rtnObject;
                        }

                        MasterpassCardInfoLog mpclog = ChannelFacade.GetMasterpassCardInfoLog(mainCartDTO.MasterPassTransData.VerifierCode,
                            mainCartDTO.MasterPassTransData.AuthToken);

                        if (!mpclog.IsLoaded)
                        {
                            rtnObject.Code = ApiReturnCode.PaymentError;
                            rtnObject.Message = "無法取得MasterPass資料";
                            return rtnObject;
                        }

                        if (mpclog.IsUsed)
                        {
                            rtnObject.Code = ApiReturnCode.PaymentError;
                            rtnObject.Message = "MasterPass 交易已使用";
                            return rtnObject;
                        }

                        mainCartDTO.CreditcardNumber = mpclog.CardNumber;
                        mainCartDTO.CreditcardExpireMonth = mpclog.ExpiryMonth.PadLeft(2, '0');
                        mainCartDTO.CreditcardExpireYear = mpclog.ExpiryYear.Length == 4 ? mpclog.ExpiryYear.Substring(2, 2) : mpclog.ExpiryYear;
                        mainCartDTO.CreditcardSecurityCode = "000";
                    }

                    if (paymentDTO.PCash > 0)
                    {
                        if (MemberFacade.GetPayeasyCashPoint(userId) < paymentDTO.PCash)
                        {
                            rtnObject.Code = ApiReturnCode.PaymentError;
                            rtnObject.Message = "Payeasy購物金不足";
                            return rtnObject;
                        }
                    }

                    IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);
                    bool isGroupCoupon = config.IsGroupCouponOn && Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
                    if (config.EnableGroupCouponNewMakeOrderApi)
                    {
                        if (isGroupCoupon && !supportGroupCoupon)
                        {
                            rtnObject.Code = ApiReturnCode.PaymentError;
                            rtnObject.Message = "此檔次限最新版本APP才可以購買，請更新您的APP。";
                            return rtnObject;
                        }
                    }

                    //檢查品生活visa檔是否未用visa特定卡購買
                    if (theDeal.IsVisaNow)
                    {
                        HiDealVisaCardType cardType = CreditCardPremiumManager.GetPiinlifeVisaCardType(mainCartDTO.CreditcardNumber);
                        if (cardType != HiDealVisaCardType.Signature &&
                            cardType != HiDealVisaCardType.Infinite &&
                            cardType != HiDealVisaCardType.Platinum)
                        {
                            rtnObject.Code = ApiReturnCode.PaymentError;
                            rtnObject.Message = "Visa優先選購專區，只提供Visa御璽卡、Visa無限卡、Visa白金卡優先購買。";
                            return rtnObject;
                        }
                    }

                    // check branch 非0元檔次，檢查分店編號是否正確
                    // 通用券檔次無須檢查分店
                    bool noRestrictedStore = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
                    if ((theDeal.ItemPrice != 0) && (!CheckBranch(paymentDTO.BusinessHourGuid, paymentDTO.IsItem, paymentDTO.SelectedStoreGuid, paymentDTO.Quantity, noRestrictedStore,theDeal.SaleMultipleBase)))
                    {
                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = "請選擇還有數量的分店";
                        return rtnObject;
                    }

                    // check option
                    if (!checkOptions(paymentDTO.ItemOptions))
                    {
                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = "請選擇還有數量的選項";
                        return rtnObject;
                    }

                    // add for empty address
                    if (paymentDTO.IsItem)
                    {
                        if (string.IsNullOrWhiteSpace(mainCartDTO.DeliveryInfo.WriteAddress) || mainCartDTO.DeliveryInfo.WriteAddress.IndexOf("請選擇") >= 0)
                        {
                            rtnObject.Code = ApiReturnCode.PaymentError;
                            rtnObject.Message = "請選擇收件人地址";
                            return rtnObject;
                        }
                    }

                    //使用17Life購物金付全額仍需紀錄invoice_mode (invoice_mode不為0)
                    int sc_price = Convert.ToInt32(theDeal.ItemPrice) + paymentDTO.SCash;

                    if (sc_price == 0)
                    {
                        paymentDTO.DonationReceiptsType = DonationReceiptsType.None;//0
                    }

                    //關於公益檔的檢查
                    bool isKindDeal = theDeal.GroupOrderStatus != null && Helper.IsFlagSet(theDeal.GroupOrderStatus.Value, GroupOrderStatus.KindDeal);
                    if (isKindDeal)
                    {
                        if (paymentDTO.SCash > 0 ||
                            paymentDTO.BCash > 0 ||
                            string.IsNullOrEmpty(paymentDTO.ATMAccount) == false ||
                            string.IsNullOrEmpty(paymentDTO.DiscountCode) == false)
                        {
                            rtnObject.Code = ApiReturnCode.InputError;
                            rtnObject.Message = "公益檔只允許信用卡支付";
                            return rtnObject;
                        }
                    }
                    #endregion Check Order

                    #region 發票問題應急修正

                    //針對APP傳入參數的錯誤，先增加的修改，相容於正確判斷後傳來的參數。
                    if ((DonationReceiptsType)mainCartDTO.DeliveryInfo.ReceiptsType == DonationReceiptsType.None)
                    {
                        mainCartDTO.DeliveryInfo.ReceiptsType = DonationReceiptsType.DoNotContribute;
                        if (string.IsNullOrWhiteSpace(mainCartDTO.DeliveryInfo.CompanyTitle))
                        {
                            mainCartDTO.DeliveryInfo.InvoiceType = "2";
                            mainCartDTO.DeliveryInfo.IsEinvoice = true;
                        }
                        else
                        {
                            mainCartDTO.DeliveryInfo.InvoiceType = "3";
                            mainCartDTO.DeliveryInfo.IsEinvoice = false;
                        }
                    }

                    #endregion 發票問題應急修正

                    #region CheckOut

                    //paymentDTO.TicketId = OrderFacade.MakeRegularTicketId(paymentDTO.SessionId, paymentDTO.BusinessHourGuid) + "_" + Path.GetRandomFileName().Replace(".", "");

                    paymentDTO.APIProvider = (PaymentAPIProvider)Enum.Parse(typeof(PaymentAPIProvider), config.CreditCardAPIProvider);

                    int quantity = (int)(Math.Ceiling((double)(paymentDTO.Quantity) / (theDeal.ComboPackCount ?? 1)));

                    if (PponFacade.CheckExceedOrderedQuantityLimit(theDeal, quantity))
                    {
                        rtnObject.Code = ApiReturnCode.OutofLimitOrderError;
                        rtnObject.Message = I18N.Message.OutofLimitOrderError;
                        return rtnObject;
                    }

                    if (DateTime.Now > theDeal.BusinessHourOrderTimeE)
                    {
                        rtnObject.Code = ApiReturnCode.DealClosed;
                        rtnObject.Message = I18N.Message.DealClosed;
                        return rtnObject;
                    }

                    int minimum_amount;
                    decimal amount = theDeal.ItemPrice * quantity + mainCartDTO.DeliveryInfo.Freight;
                    var discountCodeStatus = PromotionFacade.GetDiscountCodeStatus(paymentDTO.DiscountCode, DiscountCampaignUsedFlags.Ppon, amount, userName, out minimum_amount, theDeal.BusinessHourGuid.ToString());

                    rtnObject.Message = "";

                    if (!string.IsNullOrWhiteSpace(paymentDTO.DiscountCode))
                    {
                        if (discountCodeStatus == DiscountCodeStatus.None)
                        {
                            rtnObject.Message = I18N.Phrase.DiscountCodeNone;
                        }
                        else if (discountCodeStatus == DiscountCodeStatus.IsUsed)
                        {
                            rtnObject.Message = I18N.Phrase.DiscountCodeIsUsed;
                        }
                        else if (discountCodeStatus == DiscountCodeStatus.UpToQuantity)
                        {
                            rtnObject.Message = I18N.Phrase.DiscountCodeUptoQuantity;
                        }
                        else if (discountCodeStatus == DiscountCodeStatus.Disabled)
                        {
                            rtnObject.Message = I18N.Phrase.DiscountCodeDisabled;
                        }
                        else if (discountCodeStatus == DiscountCodeStatus.OverTime)
                        {
                            rtnObject.Message = I18N.Phrase.DiscountCodeOverTime;
                        }
                        else if (discountCodeStatus == DiscountCodeStatus.Restricted)
                        {
                            rtnObject.Message = string.Format(I18N.Phrase.DiscountCodeRestricted, "品生活");
                        }
                        else if (discountCodeStatus == DiscountCodeStatus.UnderMinimumAmount)
                        {
                            rtnObject.Message = string.Format(I18N.Phrase.DiscountCodeUnderMinimumAmount, minimum_amount);
                        }
                        else if (discountCodeStatus == DiscountCodeStatus.DiscountCodeCategoryWrong)
                        {
                            rtnObject.Message = I18N.Phrase.DiscountCodeCategoryWrong;
                        }
                        else if (discountCodeStatus == DiscountCodeStatus.OwnerUseOnlyWrong)
                        {
                            rtnObject.Message = I18N.Phrase.DiscountCodeOwnerUseOnlyWrong;
                        }
                        else if (discountCodeStatus == DiscountCodeStatus.CategoryUseLimit)
                        {
                            rtnObject.Message = string.Format("僅限" + PromotionFacade.GetDiscountCategoryString(paymentDTO.DiscountCode) + "使用");
                        }
                        else if (discountCodeStatus != DiscountCodeStatus.CanUse)
                        {
                            rtnObject.Message = I18N.Phrase.DiscountCodeCanNotUse;
                        }

                        if (!string.IsNullOrWhiteSpace(rtnObject.Message))
                        {
                            rtnObject.Code = ApiReturnCode.DiscountCodeIsUsed;
                            return rtnObject;
                        }
                    }
                    string strResult = CheckData(theDeal, quantity, paymentDTO.BCash, paymentDTO.SCash, userName, 
                        (bool)theDeal.IsDailyRestriction, isGroupCoupon);
                    if (string.IsNullOrWhiteSpace(strResult))
                    {
                        string infoStr = new JsonSerializer().Serialize(mainCartDTO.DeliveryInfo);

                        List<ItemEntry> ies = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(
                            paymentDTO.ItemOptions, paymentDTO.TicketId, theDeal, paymentDTO.SelectedStoreGuid, infoStr);

                        try
                        {
                            OrderFacade.PutItemToCart(paymentDTO.TicketId, ies);

                            #region 把資料放到暫存Session中

                            TempSession ts = new TempSession();
                            ts.SessionId = paymentDTO.TicketId;
                            ts.Name = "pponContent";
                            ts.ValueX = infoStr;
                            pp.TempSessionSetForUpdate(ts);

                            #endregion 把資料放到暫存Session中
                        }
                        catch (Exception ex)
                        {
                            rtnObject.Code = ApiReturnCode.PaymentError;
                            rtnObject.Message = "交易失敗！（error:120）";
                            logger.Error(string.Format("Something wrong while put items to cart : ticketId={0}, bid={1}", paymentDTO.TicketId,
                                    paymentDTO.BusinessHourGuid), ex);
                            return rtnObject;
                        }
                    }
                    else
                    {
                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = strResult;
                        return rtnObject;
                    }
                    #endregion CheckOut




                    TempSessionCollection tsc = pp.TempSessionGetList(paymentDTO.TicketId);

                    if (tsc.Count <= 0)
                    {
                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = "交易失敗！（error:121）";
                        logger.Error("Temp Session Missing.");
                        return rtnObject;
                    }

                    //paymentDTO.TransactionId = PaymentFacade.GetTransactionId();

                    #region order amount

                    var discountCodeObject = new DiscountCode();
                    //discountCode實際扣抵金額
                    var discountAmount = 0;
                    if (!string.IsNullOrWhiteSpace(mainCartDTO.DeliveryInfo.DiscountCode))
                    {
                        discountCodeObject = op.DiscountCodeGetByCodeWithoutUseTime(mainCartDTO.DeliveryInfo.DiscountCode);

                        if (discountCodeObject.IsLoaded)
                        {
                            if (mainCartDTO.IsMasterPass && mainCartDTO.CreditcardNumber.Length > 6)
                            {
                                var viewDiscountDetail = op.ViewDiscountDetailByCode(mainCartDTO.DeliveryInfo.DiscountCode);
                                if (Helper.IsFlagSet(viewDiscountDetail.Flag, DiscountCampaignUsedFlags.VISA))
                                {
                                    if (!CreditCardPremiumManager.IsVisaCardNumber(mainCartDTO.CreditcardNumber.Substring(0, 6)))
                                    {
                                        rtnObject.Code = ApiReturnCode.DiscountCodeError;
                                        rtnObject.Message = "折價券限定VISA卡使用";
                                        logger.Error("Temp Session Missing. discount code must using VISA card by masterpass.");
                                        return rtnObject;
                                    }
                                }
                            }
                            discountAmount = discountCodeObject.Amount == null ? 0 : (int)discountCodeObject.Amount.Value;
                        }
                        else
                        {
                            rtnObject.Code = ApiReturnCode.DiscountCodeError;
                            rtnObject.Message = "折價券已達使用數量上限";
                            logger.Error("Temp Session Missing.");
                            return rtnObject;
                        }
                    }

                    int appInputCashAmount = paymentDTO.Amount; //先將 App 傳進來的 amount(實際支付金額) keep.

                    //訂單總金額
                    ShoppingCartCollection carts = new ShoppingCartCollection();
                    var orderTotalPay = 0;

                    if (paymentDTO.IsShoppingCart)
                    {
                        paymentDTO.Amount = 0;
                        ShoppingCartCollection scc = op.ShoppingCartGetList(ShoppingCart.Columns.TicketId, paymentDTO.TicketId);
                        foreach (ShoppingCart sc in scc)
                        {
                            carts.Add(sc);
                            int x = (int)Math.Round(sc.ItemUnitPrice * sc.ItemQuantity);
                            paymentDTO.Amount += x;
                            orderTotalPay += x;
                        }
                    }
                    else
                    {
                        //前端Deal以憑證宅配撈取，所以結帳時不以館別判斷
                        carts.Add(op.ShoppingCartGet(ShoppingCart.Columns.TicketId, paymentDTO.TicketId));
                        paymentDTO.Amount = orderTotalPay = (int)Math.Round(carts[0].ItemUnitPrice * carts[0].ItemQuantity);
                    }

                    paymentDTO.DeliveryCharge = SetDeliveryCharge(paymentDTO.Amount, theDeal);
                    paymentDTO.Amount += paymentDTO.DeliveryCharge - mainCartDTO.DeliveryInfo.BonusPoints - mainCartDTO.DeliveryInfo.PayEasyCash - (int)mainCartDTO.DeliveryInfo.SCash - discountAmount;
                    orderTotalPay += paymentDTO.DeliveryCharge;
                    //若扣除付款項目後，金額小於0，則視為0且調整discount實際扣抵金額
                    if (paymentDTO.Amount < 0)
                    {
                        discountAmount = discountAmount + paymentDTO.Amount;
                        paymentDTO.Amount = 0;
                    }

                    if (supportGroupCoupon && config.EnableGroupCouponNewMakeOrderApi && paymentDTO.Amount != appInputCashAmount)
                    {
                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = string.Format("支付金額與系統核算金額不符。");
                        return rtnObject;
                    }

                    #endregion order amount

                    #region New Version Order Start

                    DeliveryInfo di = new DeliveryInfo();
                    di.DeliveryCharge = paymentDTO.DeliveryCharge;
                    OrderFromType orderfromtype;
                    switch (apiUserDeviceType)
                    {
                        case ApiUserDeviceType.WebService:
                            orderfromtype = OrderFromType.ByWebService;
                            break;

                        case ApiUserDeviceType.Android:
                            orderfromtype = OrderFromType.ByAndroid;
                            break;

                        case ApiUserDeviceType.IOS:
                            orderfromtype = OrderFromType.ByIOS;
                            break;

                        case ApiUserDeviceType.WinPhone:
                            orderfromtype = OrderFromType.ByWinPhone;
                            break;

                        default:
                            orderfromtype = OrderFromType.ByWebService;
                            break;
                    }

                    string userMemo = mainCartDTO.DeliveryInfo.DeliveryDateString + mainCartDTO.DeliveryInfo.DeliveryTimeString + mainCartDTO.DeliveryInfo.Notes;

                    if (theDeal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, theDeal.DeliveryType.Value)) //好康到店到府消費方式判斷；PponItem=ToHouse
                    {
                        string deliveryAddress = mainCartDTO.DeliveryInfo.WriteAddress;
                        if (string.IsNullOrWhiteSpace(mainCartDTO.DeliveryInfo.WriteZipCode))
                        {
                            if (withBuilding && mainCartDTO.DeliveryInfo.WriteBuildingGuid != Guid.Empty)
                            {
                                ViewBuildingCity buildingCity = lp.ViewBuildingCityGet(mainCartDTO.DeliveryInfo.WriteBuildingGuid);
                                if (buildingCity.IsLoaded)
                                {
                                    deliveryAddress = buildingCity.ZipCode + " " + deliveryAddress;
                                }
                            }
                            else
                            {
                                //停用BUILDING，嘗試由 paymentDTO的addressInfo取得資料
                                LkAddress address = mainCartDTO.GetReceiverAddress();
                                if (address != null)
                                {
                                    City township = CityManager.TownShipGetById(address.TownshipId);
                                    if (township != null)
                                    {
                                        deliveryAddress = township.ZipCode + " " + address.CompleteAddress;
                                    }
                                }
                            }
                        }
                        else
                        {
                            deliveryAddress = mainCartDTO.DeliveryInfo.WriteZipCode + " " + deliveryAddress;
                        }

                        di.CustomerName = mainCartDTO.DeliveryInfo.WriteName;
                        di.CustomerMobile = mainCartDTO.DeliveryInfo.Phone;
                        di.DeliveryAddress = deliveryAddress;

                        //宅配商品檢查地址資料是否包含路段
                        if (!CheckAddressBuilding(deliveryAddress))
                        {
                            //不包含路段，提示使用者補資料
                            rtnObject.Code = ApiReturnCode.PaymentError;
                            rtnObject.Message = "收件人地址內容錯誤，請再檢查看看是否包含足夠的資訊供送貨人員進行配送。";
                            return rtnObject;
                        }
                    }
                    else
                    {
                        di.DeliveryAddress = PponBuyFacade.GetOrderDesc(mainCartDTO.DeliveryInfo);
                    }

                    // make order
                    paymentDTO.OrderGuid = OrderFacade.MakeOrder(userName, paymentDTO.TicketId, DateTime.Now, userMemo, di,
                        theDeal, orderfromtype);

                    Order o = op.OrderGet(paymentDTO.OrderGuid);

                    //小購物車的序號
                    o.CartDGuid = cartGroupD.Guid;

                    o.OrderMemo = ((int)mainCartDTO.DeliveryInfo.ReceiptsType.GetValueOrDefault()).ToString() + "|" + mainCartDTO.DeliveryInfo.InvoiceType + "|" + mainCartDTO.DeliveryInfo.UnifiedSerialNumber + "|" + mainCartDTO.DeliveryInfo.CompanyTitle;
                    op.OrderSet(o);

                    #endregion New Version Order Start

                    #region Paying by 17Life bonus cash

                    if (mainCartDTO.DeliveryInfo.BonusPoints > 0)
                    {
                        using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                        {
                            decimal bonusCash = mainCartDTO.DeliveryInfo.BonusPoints;
                            string action = "折抵:" + 
                                (theDeal.EventName.Length <= 30 ? theDeal.EventName : (theDeal.EventName.Substring(0, 30) + "..."));
                            //pt 2.3 bcash
                            MemberFacade.DeductibleMemberPromotionDeposit(o.UserId, Convert.ToDouble(bonusCash * 10), action,
                                BonusTransactionType.OrderAmtRedeeming, o.Guid, MemberFacade.GetUserName(o.UserId), false, false);
                            //pt 1.3 bcash
                            OrderFacade.MakeTransaction(paymentDTO.TicketId, PaymentType.BonusPoint, mainCartDTO.DeliveryInfo.BonusPoints,
                                (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDTO.PezId);
                            tx.Complete();
                        }
                        if (paymentDTO.Amount + mainCartDTO.DeliveryInfo.PayEasyCash + mainCartDTO.DeliveryInfo.SCash + discountAmount == 0) //means paid all by bonus cash
                        {
                            //MakePayment(mainCartDTO, withBuilding, userName, userId);
                            return rtnObject;
                        }
                    }

                    #endregion Paying by 17Life bonus cash

                    #region Check PCash

                    if (mainCartDTO.DeliveryInfo.PayEasyCash > 0)
                    {
                        //check if one pez member do have enough PCash or redirect to Pay.aspx
                        if (MemberFacade.GetPayeasyCashPoint(userId) >= mainCartDTO.DeliveryInfo.PayEasyCash)
                        {
                            //僅建立transation 不預先扣款
                            int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department), PayTransPhase.Created);
                            PaymentFacade.NewTransaction(paymentDTO.TicketId, o.Guid, PaymentType.PCash, mainCartDTO.DeliveryInfo.PayEasyCash, string.Empty, PayTransType.Authorization, DateTime.Now, userName, string.Empty, status, PayTransResponseType.OK);

                            if (paymentDTO.Amount + mainCartDTO.DeliveryInfo.BonusPoints + discountAmount == 0) //means paid all by PCash
                            {
                                //MakePayment(mainCartDTO, withBuilding, userName, userId);
                                return rtnObject;
                            }
                        }
                        else
                        {
                            rtnObject.Code = ApiReturnCode.PaymentError;
                            rtnObject.Message = "Payeasy購物金不足";
                            return rtnObject;
                        }
                    }

                    #endregion Check PCash

                    #region 17購物金折抵

                    PaymentTransaction ptScash = new PaymentTransaction();
                    if (mainCartDTO.DeliveryInfo.SCash > 0)
                    {
                        ptScash = OrderFacade.MakeTransaction(paymentDTO.TicketId, PaymentType.SCash, int.Parse(mainCartDTO.DeliveryInfo.SCash.ToString("F0")),
                            (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDTO.PezId);
                    }

                    #endregion 17購物金折抵

                    #region DiscountCode

                    if (!string.IsNullOrWhiteSpace(mainCartDTO.DeliveryInfo.DiscountCode))
                    {
                        if ((discountCodeObject.IsLoaded) && (discountCodeObject.Amount != null) && (discountAmount > 0))
                        {
                            #region  檢查是否使用visa 限定折價券 且必須要有刷卡金額
                            if (paymentDTO.Amount == 0 || paymentDTO.IsPaidByATM)
                            {
                                ViewDiscountDetail view_discountCode_detail = op.ViewDiscountDetailByCode(mainCartDTO.DeliveryInfo.DiscountCode);
                                if (Helper.IsFlagSet(view_discountCode_detail.Flag, DiscountCampaignUsedFlags.VISA))
                                {
                                    rtnObject.Code = ApiReturnCode.DiscountCodeError;
                                    rtnObject.Message = "很抱歉！該折價券僅限VISA信用卡使用，請您再試一次。";
                                    return rtnObject;
                                }
                            }
                            #endregion

                            OrderFacade.MakeTransaction(paymentDTO.TicketId, LunchKingSite.Core.PaymentType.DiscountCode, discountAmount, (DepartmentTypes)theDeal.Department,
                                userName, o.Guid, paymentDTO.PezId);
                            try
                            {
                                var updateCount = op.DiscountCodeUpdateStatusSet((int)discountCodeObject.CampaignId, discountCodeObject.Code, discountAmount, userId, o.Guid,
                                                               orderTotalPay, paymentDTO.Amount);
                                if (updateCount == 0)
                                {
                                    rtnObject.Code = ApiReturnCode.DiscountCodeError;
                                    rtnObject.Message = "折價券已達使用數量上限";
                                    return rtnObject;
                                }
                                else if (updateCount > 1)
                                {
                                    //找無折價券或是已使用完畢
                                    logger.Error(string.Format("折價券更新使用狀態ERROR：=>PayService.asmx.cs，更新{0}筆異常，UseTime:{1},UseAmount:{2},UseId:{3},OrderGuid:{4},OrderAmount{5},OrderCost:{6}", updateCount, discountCodeObject.Code, discountAmount, userId, o.Guid, orderTotalPay, paymentDTO.Amount));
                                }
                            }
                            catch (Exception ex)
                            {
                                logger.Error(string.Format("折價券更新使用狀態ERROR：PayService.asmx.cs=>UseTime:{0},UseAmount:{1},UseId:{2},OrderGuid:{3},OrderAmount{4},OrderCost:{5}", discountCodeObject.Code, discountAmount, userId, o.Guid, orderTotalPay, paymentDTO.Amount), ex);
                            }
                        }
                    }

                    #endregion DiscountCode

                    //有金額或好康設定為0元好康
                    if ((paymentDTO.Amount > 0) || (theDeal.ItemPrice == 0))
                    {
                        if ((config.BypassPaymentProcess) || (theDeal.ItemPrice == 0))
                        {
                            #region For test purpose

                            OrderFacade.MakeTransaction(paymentDTO.TicketId, PaymentType.ByPass, 
                                paymentDTO.Amount, (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDTO.PezId);

                            #endregion For test purpose
                        }
                        else
                        {
                            PaymentTransaction ptCredit = OrderFacade.MakeTransaction(paymentDTO.TicketId, paymentDTO.IsPaidByATM ? PaymentType.ATM : PaymentType.Creditcard,
                                paymentDTO.Amount, (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDTO.PezId);

                            if (paymentDTO.IsPaidByATM)
                            {
                                if (!string.IsNullOrWhiteSpace(paymentDTO.ATMAccount))
                                {
                                    CtAtm ca = op.CtAtmGetByVirtualAccount(paymentDTO.ATMAccount);
                                    ca.Status = (int)AtmStatus.Expired;
                                    op.CtAtmSet(ca);
                                }

                                paymentDTO.ATMAccount = PaymentFacade.GetAtmAccount(ptCredit.TransId, userId, paymentDTO.Amount, theDeal.BusinessHourGuid, o.OrderId, o.Guid);


                                if (Equals(14, paymentDTO.ATMAccount.Length))
                                {
                                    if (!string.IsNullOrWhiteSpace(ptCredit.TransId))
                                    {
                                        PaymentTransaction pt = op.PaymentTransactionGet(ptCredit.TransId, PaymentType.ATM, PayTransType.Authorization);
                                        int status = Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Requested);

                                        PaymentFacade.UpdateTransaction(ptCredit.TransId, pt.OrderGuid, PaymentType.ATM, Convert.ToDecimal(paymentDTO.Amount), "",
                                            PayTransType.Authorization, DateTime.Now, paymentDTO.ATMAccount, status, (int)PayTransResponseType.OK);
                                    }
                                }
                            }
                        }

                        //MakePayment(mainCartDTO, withBuilding, userName, userId);
                    }
                    else
                    {
                        //means paid by the combination of PCash and bonus cash
                        //MakePayment(mainCartDTO, withBuilding, userName, userId);
                    }
                }

            }

            //註記購物車成功
            cartGroup.CartStatus = (int)CartStatus.Complete;
            op.ShoppingCartGroupSet(cartGroup);
            //將本次的購買資訊註銷，以免下次進來又有購物車
            ShoppingCartItemOptionCollection items = op.ShoppingCartItemOptionGetByTicketid(mainCartDTO.TransactionId);
            foreach (ShoppingCartItemOption item in items)
            {
                item.IsRemove = true;
                op.ShoppingCartItemOptionSet(item);
            }


            rtnObject = MakePayment(mainCartDTO, true, userName);
            return rtnObject;
        }
        #region private method
        private static bool CheckBranch(Guid bid, bool isItem, Guid storeId, int itemQuantity, bool noRestrictedStore,int? saleMultipleBase)
        {
            //商品檔，回傳true
            if (isItem)
            {
                return true;
            }

            //通用券檔次 無須檢查分店 return true
            if (noRestrictedStore)
            {
                return true;
            }

            //非商品檔，分店編號錯誤回傳false
            if (Guid.Empty == storeId)
            {
                return false;
            }
            //有分店編號，檢查是否還有數量
            PponStore pponStore = pp.PponStoreGet(bid, storeId);
            //查無分店設定，回傳false
            if (!pponStore.IsLoaded)
            {
                return false;
            }
            //分店設定有設定數量控制，檢查是否超過上限，若是則不允許購買
            itemQuantity = (saleMultipleBase ?? 0) > 0 ? saleMultipleBase.Value : itemQuantity;
            if ((pponStore.TotalQuantity != null) && (pponStore.TotalQuantity.Value < pponStore.OrderedQuantity + itemQuantity))
            {
                return false;
            }

            return true;
        }
        private static bool checkOptions(string itemOptions)
        {
            if (itemOptions.IndexOf("請選擇") > -1 || itemOptions.IndexOf("已售完") > -1)
            {
                return false;
            }

            return true;
        }
        //private ApiReturnObject MakePayment(PaymentDTO paymentDTO, bool withBuilding, string userName)
        public static ApiReturnObject MakePayment(MainCartDTO mainCartDTO,
                                                    bool withBuilding, string userName)
        {
            InvoiceVersion version = InvoiceVersion.CarrierEra;
            #region 針對新版發票處理預設的 空手道推展協會 與 創世基金會 的預設愛心碼

            //Web不須登入也可購買
            int userId = MemberFacade.GetUniqueId(userName);

            if (version == InvoiceVersion.CarrierEra)
            {
                switch (mainCartDTO.ReceiptsType)
                {
                    case DonationReceiptsType.Karate:
                        mainCartDTO.LoveCode = "23981";
                        if (mainCartDTO.InvoiceType == "3")
                        {
                            mainCartDTO.InvoiceType = "2";
                            SystemFacade.SetApiLog("PayService.MakePayment", string.Empty, new { message = "發票資料錯誤InvoiceType 傳入3 應為2，已修正。" }
                                                   , null);
                        }
                        break;
                    case DonationReceiptsType.GSWF:
                        mainCartDTO.LoveCode = "919";
                        if (mainCartDTO.InvoiceType == "3")
                        {
                            mainCartDTO.InvoiceType = "2";
                            SystemFacade.SetApiLog("PayService.MakePayment", string.Empty, new { message = "發票資料錯誤InvoiceType 傳入3 應為2，已修正。" }
                                                   , null);
                        }
                        break;
                    default:
                        //其他類型採用傳入值
                        break;
                }
                //避免使用者傳入含有空白的不正確愛心碼，將空白取代為空字串
                mainCartDTO.LoveCode =
                    mainCartDTO.LoveCode == null ? "" :
                    mainCartDTO.LoveCode.Replace(I18N.Phrase.Space, string.Empty);
            }

            #endregion




            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            List<CartPaymentDTO> paymentDTOList = new List<CartPaymentDTO>();
            foreach (SmallCart cart in mainCartDTO.SmallCart)
            {
                paymentDTOList.AddRange(cart.PaymentDTOList);
            }
            CartPaymentDTO defaultPaymentDTO = paymentDTOList.FirstOrDefault();

            if (userId == 0)
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                return rtnObject;
            }
            else
            {
                // 避免使用者在交易過程中改變自己的 email，導致之後在用 email 反查 unique id 時查不到資料填 0
                if (int.Equals(0, userId))
                {
                    rtnObject.Code = ApiReturnCode.PaymentError;
                    rtnObject.Message = "交易失敗！（error:122）";
                    logger.Error("Can't find unique id. email = " + userName);
                    return rtnObject;
                }

                if (!string.IsNullOrWhiteSpace(mainCartDTO.TransactionId))
                {
                    if (mainCartDTO.Amount > 0)
                    {
                        #region pay by credit card

                        if (!mainCartDTO.IsPaidByATM)
                        {
                            Regex rex = new Regex(@"\D");
                            if (!int.Equals(16, mainCartDTO.CreditcardNumber.Length) ||
                                !int.Equals(2, mainCartDTO.CreditcardExpireYear.Length) ||
                                !int.Equals(2, mainCartDTO.CreditcardExpireMonth.Length) ||
                                rex.IsMatch(mainCartDTO.CreditcardNumber) ||
                                rex.IsMatch(mainCartDTO.CreditcardExpireYear) ||
                                rex.IsMatch(mainCartDTO.CreditcardExpireMonth))
                            {
                                rtnObject.Code = ApiReturnCode.CreditcardInfoError;
                                rtnObject.Message = I18N.Message.CreditcardInfoError;
                                return rtnObject;
                            }
                            else
                            {
                                CreditCardOrderSource orderSource = CreditCardOrderSource.Ppon;
                                if (mainCartDTO.IsMasterPass)
                                {
                                    orderSource = CreditCardOrderSource.PiinLife; // 2014-10-23 決議 MasterPass 的交易都走 piinlife 的特店
                                }
                                //信用卡扣款
                                CreditCardAuthResult result = CreditCardUtility.Authenticate(
                                    new CreditCardAuthObject()
                                    {
                                        TransactionId = mainCartDTO.TransactionId,
                                        Amount = mainCartDTO.Amount,
                                        CardNumber = mainCartDTO.CreditcardNumber,
                                        ExpireYear = mainCartDTO.CreditcardExpireYear,
                                        ExpireMonth = mainCartDTO.CreditcardExpireMonth,
                                        SecurityCode = mainCartDTO.CreditcardSecurityCode,
                                        Description = defaultPaymentDTO.TicketId
                                    }, defaultPaymentDTO.OrderGuid, userName, OrderClassification.LkSite,
                                    orderSource);

                                if (mainCartDTO.IsMasterPass)
                                {
                                    op.MasterpassCardInfoLogSetUsed(mainCartDTO.MasterPassTransData.VerifierCode);
                                }

                                foreach (CartPaymentDTO paymentDTO in paymentDTOList)
                                {
                                    CheckResult(mainCartDTO, paymentDTO, result, userId);
                                }
                            }
                        }

                        #endregion pay by credit card
                    }


                    foreach (CartPaymentDTO paymentDTO in paymentDTOList)
                    {
                        PaymentTransactionCollection ptc = op.PaymentTransactionGetListByTransIdAndTransType(paymentDTO.TicketId, PayTransType.Authorization);
                        IEnumerable<PaymentTransaction> ptCreditcardCol = from i in ptc where (i.PaymentType == (int)PaymentType.Creditcard) select i;
                        PaymentTransaction ptCreditcard = (ptCreditcardCol.Count() > 0) ? ptCreditcardCol.First() : null;
                        IEnumerable<PaymentTransaction> ptScashCol = from i in ptc where (i.PaymentType == (int)PaymentType.SCash) select i;
                        PaymentTransaction ptScash = (ptScashCol.Count() > 0) ? ptScashCol.First() : null;
                        IEnumerable<PaymentTransaction> ptPcashCol = from i in ptc where (i.PaymentType == (int)PaymentType.PCash) select i;
                        PaymentTransaction ptPcash = (ptPcashCol.Count() > 0) ? ptPcashCol.First() : null;
                        IEnumerable<PaymentTransaction> ptDiscountCol = from i in ptc where (i.PaymentType == (int)PaymentType.DiscountCode) select i;
                        PaymentTransaction ptDiscount = (ptDiscountCol.Count() > 0) ? ptDiscountCol.First() : null;
                        IEnumerable<PaymentTransaction> ptATMCol = from i in ptc where (i.PaymentType == (int)PaymentType.ATM) select i;
                        PaymentTransaction ptATM = (ptATMCol.Count() > 0) ? ptATMCol.First() : null;

                        #region paying by pcash

                        if (mainCartDTO.PCash > 0 &&
                            (ptCreditcard == null || (ptCreditcard != null &&
                            Enum.Equals(PayTransResponseType.OK, (PayTransResponseType)ptCreditcard.Result))))
                        {
                            //check if one pez member do have enough PCash
                            if (MemberFacade.GetPayeasyCashPoint(userId) >= mainCartDTO.PCash)
                            {
                                ptc.Add(ptPcash);
                            }
                            else
                            {
                                rtnObject.Code = ApiReturnCode.PaymentError;
                                rtnObject.Message = "Payeasy購物金不足";
                                return rtnObject;
                            }
                        }

                        #endregion paying by pcash

                        PayTransResponseType theResponseType = PayTransResponseType.OK;
                        PaymentType errorPaymentType = PaymentType.PCash;
                        List<PaymentType> paymentTypes = new List<PaymentType>();

                        foreach (var pt in ptc)
                        {
                            paymentTypes.Add((PaymentType)pt.PaymentType);

                            if (pt.Result != (int)PayTransResponseType.OK)
                            {
                                theResponseType = PayTransResponseType.GenericError;
                                errorPaymentType = (PaymentType)pt.PaymentType;
                            }
                        }

                        if (theResponseType == PayTransResponseType.OK)
                        {
                            List<Coupon> coupons = new List<Coupon>();
                            foreach (CartPaymentDTO _paymentDTO in paymentDTOList)
                            {
                                IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(_paymentDTO.BusinessHourGuid);

                                if (theDeal.DealIsOrderable())
                                {
                                    #region 檢查pcash transation

                                    string authCode = string.Empty;
                                    if (ptPcash != null)
                                    {
                                        //如扣款不成功
                                        if (!PponBuyFacade.GetPcashAuthCode(_paymentDTO.PezId, _paymentDTO.TicketId, ptPcash.Amount, (DepartmentTypes)theDeal.Department, out authCode))
                                        {
                                            //更新transation紀錄 並刪除購物車和session 顯示pcash扣款失敗頁面
                                            PaymentFacade.UpdateTransaction(ptPcash.TransId, Guid.Empty, PaymentType.PCash, ptPcash.Amount, ptPcash.AuthCode,
                                                PayTransType.Authorization, DateTime.Now, ptPcash.Message,
                                                Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department), PayTransPhase.Failed),
                                                (int)PayTransResponseType.GenericError);

                                            OrderFacade.DeleteItemInCart(_paymentDTO.TicketId);

                                            pp.TempSessionDelete(_paymentDTO.TicketId);
                                            _paymentDTO.ResultPageType = (errorPaymentType == PaymentType.PCash) ? PaymentResultPageType.PcashFailed : PaymentResultPageType.CreditcardFailed;
                                            rtnObject.Code = ApiReturnCode.PaymentError;
                                            rtnObject.Message = "交易失敗!（error:123）";
                                            return rtnObject;
                                        }
                                        else
                                        {
                                            PaymentFacade.UpdateTransaction(ptPcash.TransId, Guid.Empty, PaymentType.PCash, ptPcash.Amount, authCode,
                                                PayTransType.Authorization, DateTime.Now, ptPcash.Message,
                                                Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department),
                                                PayTransPhase.Successful), (int)PayTransResponseType.OK);
                                        }
                                    }

                                    #endregion 檢查pcash transation

                                    #region Transaction Scope

                                    bool orderOK = false;
                                    Order theOrder = op.OrderGet(ptc.First().OrderGuid.Value);
                                    OrderDetailCollection odCol = op.OrderDetailGetList(1, theDeal.MaxItemCount ?? 10, string.Empty, theOrder.Guid, OrderDetailTypes.Regular);
                                    int atmAmount = ptATM == null ? 0 : (int)ptATM.Amount;
                                    int discountAmt = ptDiscount == null ? 0 : (int)ptDiscount.Amount;
                                    int trustCreditCardAmount = ptCreditcard == null ? 0 : (int)ptCreditcard.Amount;

                                    #region 檢查付款金額與份數是否相符

                                    int quantityTotal = (int)odCol.Sum(x => x.ItemQuantity * x.ItemUnitPrice) + paymentDTO.DeliveryCharge;
                                    int paymentTotal = trustCreditCardAmount + (int)mainCartDTO.DeliveryInfo.SCash + mainCartDTO.DeliveryInfo.PayEasyCash +
                                        mainCartDTO.DeliveryInfo.BonusPoints + discountAmt + atmAmount;

                                    if (!int.Equals(quantityTotal, paymentTotal))
                                    {
                                        rtnObject.Code = ApiReturnCode.PaymentError;
                                        rtnObject.Message = "交易失敗！（error:124）";
                                        StringBuilder sb = new StringBuilder();
                                        sb.AppendFormat("付款金額與份數不符: orderGuid={0}\r\n", theOrder.Guid);
                                        sb.AppendFormat(",quantityTotal={0},odSum={1},DeliveryCharge={2}\r\n",
                                            quantityTotal, odCol.Sum(x => x.ItemQuantity * x.ItemUnitPrice), paymentDTO.DeliveryCharge);
                                        sb.AppendFormat(
                                            "paymentTotal={0}, trustCreditCardAmount={1}, SCash={2}, PayEasyCash={3}, BonusPoints={4}, discountAmt={5},atmAmount={6}\r\n",
                                            paymentTotal, trustCreditCardAmount,
                                            (int)mainCartDTO.DeliveryInfo.SCash, mainCartDTO.DeliveryInfo.PayEasyCash,
                                            mainCartDTO.DeliveryInfo.BonusPoints, discountAmt, atmAmount);
                                        logger.ErrorFormat(sb.ToString());
                                        paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;

                                        return rtnObject;
                                    }

                                    #endregion 檢查付款金額與份數是否相符

                                    int quantity = odCol.Sum(x => x.ItemQuantity);

                                    using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                                    {
                                        #region pcash扣款成功後 更新transation

                                        if (ptPcash != null)
                                        {
                                            PaymentFacade.UpdateTransaction(ptPcash.TransId, theOrder.Guid, PaymentType.PCash, ptPcash.Amount, authCode,
                                                PayTransType.Authorization, DateTime.Now, ptPcash.Message,
                                                Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department), PayTransPhase.Successful),
                                                (int)PayTransResponseType.OK);
                                        }

                                        #endregion pcash扣款成功後 更新transation

                                        pp.TempSessionDelete(paymentDTO.TicketId);

                                        #region Scash

                                        if (mainCartDTO.DeliveryInfo.SCash > 0 &&
                                            (ptCreditcard == null || (ptCreditcard != null &&
                                            Enum.Equals(PayTransResponseType.OK, (PayTransResponseType)ptCreditcard.Result))))
                                        {
                                            OrderFacade.ScashUsed(mainCartDTO.DeliveryInfo.SCash, userId, theOrder.Guid, "購物金折抵:" + theDeal.SellerName,
                                                userName, OrderClassification.CashPointOrder);
                                        }

                                        #endregion Scash

                                        #region 扣除選項

                                        TempSessionCollection tsc = pp.TempSessionGetList("Accessory" + paymentDTO.TicketId);
                                        foreach (TempSession ts in tsc)
                                        {
                                            int forOut = 0;
                                            AccessoryGroupMember agm = ip.AccessoryGroupMemberGet(new Guid(ts.Name));
                                            if (!agm.IsLoaded)
                                            {
                                                continue;
                                            }

                                            if (agm.Quantity != null && int.TryParse(ts.ValueX, out forOut))
                                            {
                                                agm.Quantity = agm.Quantity - int.Parse(ts.ValueX);

                                                int? optId = PponOption.FindId(agm.Guid, theDeal.BusinessHourGuid);
                                                if (optId.HasValue)
                                                {
                                                    PponOption opt = pp.PponOptionGet(optId.Value);
                                                    if (opt != null && opt.IsLoaded)
                                                    {
                                                        opt.Quantity = agm.Quantity;
                                                        pp.PponOptionSet(opt);
                                                    }
                                                }
                                            }

                                            ip.AccessoryGroupMemberSet(agm);
                                        }
                                        pp.TempSessionDelete("Accessory" + paymentDTO.TicketId);

                                        #endregion 扣除選項

                                        #region 分店銷售數量

                                        if (mainCartDTO.DeliveryInfo.StoreGuid != Guid.Empty)
                                        {
                                            int k = 0;
                                            if (paymentDTO.IsShoppingCart)
                                            {
                                                k = (int)(Math.Ceiling((double)mainCartDTO.DeliveryInfo.Quantity / (theDeal.ComboPackCount ?? 1)));
                                            }
                                            else
                                            {
                                                k = mainCartDTO.DeliveryInfo.Quantity;
                                            }
                                            k = (theDeal.SaleMultipleBase ?? 0) > 0 ? theDeal.SaleMultipleBase.Value : k;
                                            pp.PponStoreUpdateOrderQuantity(theDeal.BusinessHourGuid, mainCartDTO.DeliveryInfo.StoreGuid, k);
                                        }

                                        #endregion 分店銷售數量

                                        #region 信用卡轉購物金

                                        if (ptCreditcard != null)
                                        {
                                            Guid sOrderGuid = OrderFacade.MakeSCachOrder(ptCreditcard.Amount, ptCreditcard.Amount, "17Life購物金購買(系統)",
                                                CashPointStatus.Approve, userId, theDeal.ItemName, CashPointListType.Income, userName, theOrder.Guid);

                                            //Scash will not be invoiced initially after the NewInvoiceDate.
                                            bool invoiced = (DateTime.Now < config.NewInvoiceDate);
                                            int depositId = OrderFacade.NewScashDeposit(sOrderGuid, ptCreditcard.Amount, theOrder.UserId, theOrder.Guid,
                                                "購物金購買(系統):" + theDeal.SellerName, userName, invoiced, OrderClassification.CashPointOrder);

                                            OrderFacade.NewScashWithdrawal(depositId, ptCreditcard.Amount, theOrder.Guid, "購物金折抵(系統):" + theDeal.SellerName,
                                                userName, OrderClassification.CashPointOrder);

                                            ptCreditcard.OrderGuid = sOrderGuid;
                                            ptCreditcard.Message += "|" + theOrder.Guid.ToString();
                                            op.PaymentTransactionSet(ptCreditcard);

                                            if (ptScash == null)
                                            {
                                                OrderFacade.MakeTransaction(paymentDTO.TicketId, PaymentType.SCash, int.Parse(ptCreditcard.Amount.ToString("F0")),
                                                    (DepartmentTypes)theDeal.Department, userName, theOrder.Guid, paymentDTO.PezId);
                                            }
                                            else
                                            {
                                                PaymentFacade.UpdateTransaction(ptScash.TransId, ptScash.OrderGuid, PaymentType.SCash, ptScash.Amount + ptCreditcard.Amount,
                                                    ptScash.AuthCode, PayTransType.Authorization, DateTime.Now, ptScash.Message, ptScash.Status, ptScash.Result.Value);
                                            }
                                        }

                                        #endregion 信用卡轉購物金

                                        transScope.Complete();
                                        orderOK = true;
                                    }

                                    #endregion Transaction Scope

                                    if (!orderOK)
                                    {
                                        CreditCardUtility.AuthenticationReverse(paymentDTO.TicketId, ptCreditcard.AuthCode);
                                        if (ptPcash != null)
                                        {
                                            PCashWorker.DeCheckOut(paymentDTO.TicketId, authCode, ptPcash.Amount);
                                        }
                                        rtnObject.Code = ApiReturnCode.PaymentError;
                                        rtnObject.Message = "交易失敗！（error:125）";
                                        logger.Error("Order update failed.");
                                        paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;
                                        return rtnObject;
                                    }
                                    else
                                    {

                                        //加入照會(2016/7/21從CreditCardUtility移過來)
                                        CreditcardOrder orderInfo = new CreditcardOrder();
                                        CreditcardOrderCollection oldOrders = op.GetCreditcardOrderByOrderGuid(theOrder.Guid);
                                        if (oldOrders != null && oldOrders.Count > 0)
                                        {
                                            orderInfo = oldOrders[0];
                                            if (orderInfo.Result == 0 || orderInfo.Result == -1)   //0是授權正常，-1是初始值
                                            {
                                                if (orderInfo.CreditcardAmount >= config.CreditcardReferAmount)
                                                {
                                                    orderInfo.Result = (int)CreditcardResult.OverQuantity;

                                                    //if (View.OrderPayType == OrderClassification.LkSite)
                                                    {
                                                        if (PaymentFacade.IsCreditcardRefer(theOrder.Guid))
                                                        {
                                                            orderInfo.IsReference = true;
                                                            orderInfo.ReferenceId = "sys";
                                                            orderInfo.ReferenceTime = DateTime.Now;
                                                        }
                                                    }
                                                }
                                                else if (op.IsCreditcardOrderOverAmount(orderInfo.CreditcardInfo, orderInfo.CreditcardAmount, orderInfo.CreateTime, 24, 10000))
                                                {
                                                    orderInfo.Result = (int)CreditcardResult.OverQuantityWithin24Hr;

                                                    //if (View.OrderPayType == OrderClassification.LkSite)
                                                    {
                                                        if (PaymentFacade.IsCreditcardRefer(theOrder.Guid))
                                                        {
                                                            orderInfo.IsReference = true;
                                                            orderInfo.ReferenceId = "sys";
                                                            orderInfo.ReferenceTime = DateTime.Now;
                                                        }
                                                    }
                                                }
                                                else if (op.IsCreditcardOrderOverAmount(orderInfo.CreditcardInfo, orderInfo.CreditcardAmount, orderInfo.CreateTime, 1, config.CreditcardReferAmount))
                                                {
                                                    orderInfo.Result = (int)CreditcardResult.OverQuantityWithin1Hr;
                                                }

                                            }
                                            op.SetCreditcardOrder(orderInfo);
                                        }


                                        //if weeklypay deal
                                        int trust_checkout_type = 0;
                                        if (((theDeal.BusinessHourStatus) & (int)BusinessHourStatus.WeeklyPay) > 0)
                                        {
                                            trust_checkout_type = (int)TrustCheckOutType.WeeklyPay;
                                        }

                                        #region make cash trust log

                                        #region payments
                                        Dictionary<LunchKingSite.Core.PaymentType, int> payments = new Dictionary<LunchKingSite.Core.PaymentType, int>();
                                        if ((int)mainCartDTO.DeliveryInfo.SCash > 0)
                                        {
                                            payments.Add(LunchKingSite.Core.PaymentType.SCash, (int)mainCartDTO.DeliveryInfo.SCash);
                                        }
                                        if ((int)mainCartDTO.DeliveryInfo.PayEasyCash > 0)
                                        {
                                            payments.Add(LunchKingSite.Core.PaymentType.PCash, (int)mainCartDTO.DeliveryInfo.PayEasyCash);
                                        }
                                        if ((int)mainCartDTO.DeliveryInfo.BonusPoints > 0)
                                        {
                                            payments.Add(LunchKingSite.Core.PaymentType.BonusPoint, (int)mainCartDTO.DeliveryInfo.BonusPoints);
                                        }
                                        if ((int)trustCreditCardAmount > 0)
                                        {
                                            payments.Add(LunchKingSite.Core.PaymentType.Creditcard, (int)trustCreditCardAmount);
                                        }
                                        if ((int)discountAmt > 0)
                                        {
                                            payments.Add(LunchKingSite.Core.PaymentType.DiscountCode, (int)discountAmt);
                                        }
                                        if ((int)atmAmount > 0)
                                        {
                                            payments.Add(LunchKingSite.Core.PaymentType.ATM, (int)atmAmount);
                                        }
                                        #endregion payments

                                        //判斷成套數量
                                        int saleMultipleBase = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? theDeal.SaleMultipleBase ?? 0 : 0;

                                        CashTrustInfo cti = new CashTrustInfo
                                        {
                                            OrderGuid = theOrder.Guid,
                                            OrderDetails = odCol,
                                            CreditCardAmount = trustCreditCardAmount,
                                            SCashAmount = (int)mainCartDTO.DeliveryInfo.SCash,
                                            PCashAmount = mainCartDTO.DeliveryInfo.PayEasyCash,
                                            BCashAmount = mainCartDTO.DeliveryInfo.BonusPoints,
                                            Quantity = saleMultipleBase > 0 ? saleMultipleBase : quantity,
                                            DeliveryCharge = paymentDTO.DeliveryCharge,
                                            DeliveryType = theDeal.DeliveryType == null ? DeliveryType.ToShop : (DeliveryType)theDeal.DeliveryType.Value,
                                            DiscountAmount = discountAmt,
                                            AtmAmount = atmAmount,
                                            BusinessHourGuid = theDeal.BusinessHourGuid,
                                            CheckoutType = trust_checkout_type,
                                            TrustProvider = Helper.GetBusinessHourTrustProvider(theDeal.BusinessHourStatus),
                                            ItemName = theDeal.ItemName,
                                            ItemPrice = (int)theDeal.ItemPrice,
                                            User = mp.MemberGet(userName),
                                            Payments = payments,
                                            PresentQuantity = theDeal.PresentQuantity ?? 0,
                                            IsGroupCoupon = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon)
                                        };

                                        CashTrustLogCollection ctCol = OrderFacade.MakeCashTrust(cti);

                                        #endregion make cash trust log

                                        if (theDeal.EntrustSell == (int)EntrustSellType.No)
                                        {
                                            #region 新制憑證發票

                                            string creditCardTail = !string.IsNullOrEmpty(mainCartDTO.CreditcardNumber) ? mainCartDTO.CreditcardNumber.Substring(mainCartDTO.CreditcardNumber.Length - 4) : string.Empty;
                                            if (ctCol.Count() > 0 && mainCartDTO.DeliveryInfo.DeliveryType == DeliveryType.ToShop)
                                            {
                                                foreach (var ct in ctCol)
                                                {
                                                    int uninvoicedAmount = saleMultipleBase > 0 ? ((ctCol.Count() > 0) ? ctCol.Sum(x => x.UninvoicedAmount) : 0) : ct.UninvoicedAmount;
                                                    if (paymentDTO.IsPaidByATM)
                                                    {
                                                        EinvoiceFacade.SetEinvoiceMain(theOrder, mainCartDTO.DeliveryInfo, paymentDTO.TicketId, 1,
                                                            theDeal.ItemName, uninvoicedAmount,
                                                            Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                            userName, false, OrderClassification.LkSite, creditCardTail, (int)EinvoiceType.NotComplete, version, Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon));
                                                    }
                                                    else if (uninvoicedAmount > 0)
                                                    {
                                                        EinvoiceFacade.SetEinvoiceMain(theOrder, mainCartDTO.DeliveryInfo, paymentDTO.TicketId, 1,
                                                            theDeal.ItemName, uninvoicedAmount,
                                                            Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                            userName, false, OrderClassification.LkSite, creditCardTail, (int)EinvoiceType.Initial, version, Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon));
                                                    }
                                                    if (Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
                                                    {
                                                        break;
                                                    }
                                                }
                                            }

                                            #endregion 新制憑證發票

                                            #region 新制宅配發票

                                            int uninvoicedTotal = (ctCol.Count() > 0) ? ctCol.Sum(x => x.UninvoicedAmount) : 0;
                                            if (mainCartDTO.DeliveryInfo.DeliveryType == DeliveryType.ToHouse)
                                            {
                                                if (paymentDTO.IsPaidByATM)
                                                {
                                                    EinvoiceFacade.SetEinvoiceMain(theOrder, mainCartDTO.DeliveryInfo, paymentDTO.TicketId, quantity,
                                                        theDeal.ItemName, uninvoicedTotal,
                                                        Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                        userName, true, OrderClassification.LkSite, creditCardTail, (int)EinvoiceType.NotComplete, version);
                                                }
                                                else if (uninvoicedTotal > 0)
                                                {
                                                    EinvoiceFacade.SetEinvoiceMain(theOrder, mainCartDTO.DeliveryInfo, paymentDTO.TicketId, quantity,
                                                        theDeal.ItemName, uninvoicedTotal,
                                                        Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                        userName, true, OrderClassification.LkSite, creditCardTail, (int)EinvoiceType.Initial, version);
                                                }
                                            }

                                            #endregion 新制宅配發票
                                        }
                                        else
                                        {
                                            //代收轉付以收據代替發票。
                                            OrderFacade.EntrustSellReceiptSet(mainCartDTO.DeliveryInfo, theOrder, odCol, ctCol, theDeal.ItemName, userName);
                                        }

                                        // save delivery info
                                        saveDeliveryInfo(mainCartDTO, paymentDTO, theOrder, userName, theDeal.DeliveryType, withBuilding);

                                        // set deal close time
                                        OrderFacade.PponSetCloseTimeIfJustReachMinimum(theDeal, quantity);

                                        CouponFacade.GenCouponWithSmsWithGeneratePeztemp(theOrder, theDeal, paymentDTO.IsPaidByATM);

                                        // send deal-on mail  //付款成功
                                        if (paymentDTO.IsPaidByATM)
                                        {
                                            //加註訂單為ATM付款
                                            OrderFacade.SetOrderStatus(theOrder.Guid, userName, OrderStatus.ATMOrder, true, new SalesInfoArg
                                            {
                                                BusinessHourGuid = theDeal.BusinessHourGuid,
                                                Quantity = theDeal.OrderedQuantity.GetValueOrDefault(),
                                                Total = theDeal.OrderedTotal.GetValueOrDefault()
                                            });
                                            Member m = mp.MemberGet(userName);
                                            OrderFacade.SendATMOrderNotifyMail(m.DisplayName, m.UserEmail, theDeal.ItemName,
                                                paymentDTO.ATMAccount, atmAmount.ToString(), theDeal, theOrder, quantity);
                                        }
                                        else
                                        {
                                            OrderFacade.SetOrderStatus(theOrder.Guid, userName, OrderStatus.Complete, true,
                                                new SalesInfoArg
                                                {
                                                    BusinessHourGuid = theDeal.BusinessHourGuid,
                                                    Quantity = odCol.OrderedQuantity,
                                                    Total = odCol.OrderedTotal
                                                });

                                            //如果付款金而為0表示該檔次為0元好康，不發信
                                            if (paymentDTO.Amount != 0)
                                            {
                                                OrderFacade.SendPponMail(MailContentType.Authorized, theOrder.Guid);
                                            }
                                        }

                                        #region 推薦送折價券
                                        //有推薦人資料，發放推薦折價券
                                        if (!string.IsNullOrWhiteSpace(paymentDTO.ReferenceId))
                                        {
                                            PromotionFacade.ReferenceDiscountCheckAndPay(paymentDTO.ReferenceId, theOrder, theDeal, MemberFacade.GetUserName(theOrder.UserId));
                                        }
                                        #endregion 推薦送折價券

                                        #region 首購送折價券
                                        PromotionFacade.FirstBuyDiscountCheckAndPay(theOrder, theDeal);
                                        #endregion
                                    }

                                    paymentDTO.ResultPageType = (paymentTypes.Contains(PaymentType.Creditcard) || paymentTypes.Contains(PaymentType.ATM)) ?
                                        PaymentResultPageType.CreditcardSuccess : PaymentResultPageType.PcashSuccess;

                                    rtnObject.Code = ApiReturnCode.Success;

                                    //0元的DEAL處理回傳的變數
                                    if ((paymentTypes.Contains(PaymentType.Creditcard)) && (theDeal.ItemPrice == 0))
                                    {
                                        paymentDTO.ResultPageType = PaymentResultPageType.ZeroPriceSuccess;

                                        if (int.Equals(0, coupons.Count))
                                        {
                                            rtnObject.Code = ApiReturnCode.PaymentError;
                                            paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;
                                            rtnObject.Message = "很抱歉，這個序號無法被取得！";
                                            return rtnObject;
                                        }
                                        else
                                        {
                                            //寄送完成通知
                                            string memberEmail = MemberFacade.GetUserEmail(theOrder.UserId);
                                            OrderFacade.SendZeroDealSuccessMail(theOrder.MemberName, memberEmail, theDeal,
                                                pp.ViewPponCouponGet(coupons[0].Id), (GroupOrderStatus)theDeal.GroupOrderStatus, (CouponCodeType)theDeal.CouponCodeType.Value);
                                            //零元檔次的回覆
                                            ApiFreeDealPayReply payReply = new ApiFreeDealPayReply();

                                            payReply.CouponCode = coupons[0].Code;

                                            payReply.PromoImage = ImageFacade.GetMediaPathsFromRawData(theDeal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 3).DefaultIfEmpty(config.SiteUrl + "/Themes/default/images/17Life/G3/0act.jpg").First();
                                            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(theDeal);
                                            AvailableInformation aif = ProviderFactory.Instance().GetSerializer()
                                                .Deserialize<AvailableInformation[]>(contentLite.Availability).FirstOrDefault();
                                            if (aif != null && !string.IsNullOrEmpty(aif.U))
                                            {
                                                payReply.PromoUrl = aif.U.Trim();
                                            }
                                            else
                                            {
                                                payReply.PromoUrl = config.SiteUrl + "/ppon/default.aspx";
                                            }
                                            payReply.IsFami = (theDeal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0;
                                            payReply.IsCouponDownload = (theDeal.GroupOrderStatus & (int)GroupOrderStatus.PEZeventCouponDownload) > 0;
                                            rtnObject.Data = payReply;
                                        }

                                    }
                                    else if (paymentDTO.IsPaidByATM)
                                    {
                                        if (Equals(14, paymentDTO.ATMAccount.Length))
                                        {
                                            ApiFreeDealPayReply payReply = new ApiFreeDealPayReply();

                                            payReply.IsATM = paymentDTO.IsPaidByATM;
                                            payReply.AtmData.BankAccount = paymentDTO.ATMAccount;
                                            payReply.AtmData.Amount = atmAmount;
                                            string dateFormat = "{0:yyyyMMdd 235900 zz00}";

                                            payReply.AtmData.DeadlineDate = string.Format(dateFormat, DateTime.Today);

                                            rtnObject.Data = payReply;
                                        }
                                        else if (string.Equals(paymentDTO.ATMAccount, "-2"))
                                        {
                                            rtnObject.Code = ApiReturnCode.ATMDealsFull;
                                            rtnObject.Message = I18N.Message.ATMDealsFull;
                                        }
                                    }

                                    return rtnObject;
                                }
                                else
                                {
                                    if (ptc.Count > 0)
                                    {
                                        PaymentFacade.CancelPaymentTransactionByTransId(ptc[0].TransId, "sys", "Unorderable. ");
                                    }

                                    rtnObject.Code = ApiReturnCode.PaymentError;
                                    paymentDTO.ResultPageType = (errorPaymentType == PaymentType.PCash) ? PaymentResultPageType.PcashFailed : PaymentResultPageType.CreditcardFailed;
                                    rtnObject.Message = "交易失敗！（error:126）";
                                    return rtnObject;
                                }
                            }
                        }
                        else
                        {
                            foreach (CartPaymentDTO _paymentDTO in paymentDTOList)
                            {
                                OrderFacade.DeleteItemInCart(_paymentDTO.TicketId);
                                pp.TempSessionDelete(paymentDTO.TicketId);
                                _paymentDTO.TicketId = null;
                                _paymentDTO.ResultPageType = (errorPaymentType == PaymentType.PCash) ? PaymentResultPageType.PcashFailed : PaymentResultPageType.CreditcardFailed;
                            }
                            rtnObject.Code = ApiReturnCode.CreditcardInfoError;
                            rtnObject.Message = I18N.Message.CreditcardInfoError;
                            return rtnObject;
                        }

                    }
                }
                else
                {
                    rtnObject.Code = ApiReturnCode.PaymentError;
                    rtnObject.Message = "交易失敗！（error:127）";
                    logger.Error("TransactionID is missing.");
                    return rtnObject;
                }
            }
            return rtnObject;
        }

        /// <summary>
        /// 將商品加入購物車
        /// </summary>
        /// <param name="cartItemDTO"></param>
        /// <param name="UserName"></param>
        /// <param name="SessionId"></param>
        /// <returns></returns>
        public static MainCartDTO SmallCartListGet(CartItemDTO cartItemDTO, string UserName, string TransactionId)
        {
            MainCartDTO mainCartDTO = new MainCartDTO();
            int UserId = 0;

            if (string.IsNullOrEmpty(UserName) && string.IsNullOrEmpty(TransactionId))
            {
                return mainCartDTO;
            }

            if (cartItemDTO != null
                && cartItemDTO.BusinessHourGuid != null
                && cartItemDTO.BusinessHourGuid != Guid.Empty)
            {
                //Add
                Guid bid = cartItemDTO.BusinessHourGuid;


                ShoppingCartItemOption scio = FindItemOption(UserName, TransactionId, out UserId);

                if (scio != null)
                {
                    //資料庫有購物車資料
                    if (!string.IsNullOrEmpty(scio.ItemOptions))
                    {
                        mainCartDTO = new JsonSerializer().Deserialize<MainCartDTO>(scio.ItemOptions);
                    }
                }
                else
                {
                    //資料庫無購物車資料
                    scio = new ShoppingCartItemOption();
                    TransactionId = Security.MD5Hash(Guid.NewGuid().ToString() + DateTime.Now);
                }

                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                ShoppingCartFreight cartFreights = SellerFacade.FreightsGetByBid(bid);
                SmallCart smallCart = null;


                //多分店
                Guid tmpStoreGuid = cartItemDTO.StoreGuid;
                if (tmpStoreGuid == Guid.Empty)
                {
                    ViewPponStoreCollection stores = pp.ViewPponStoreGetListByBidWithNoLock(bid, VbsRightFlag.VerifyShop);
                    List<CartPponSotres> cartPponSotres = new List<CartPponSotres>();
                    if (stores.Count == 1)
                    {
                        tmpStoreGuid = stores.FirstOrDefault().StoreGuid;
                    }
                }

                CartPaymentDTO paymentDTO = new CartPaymentDTO()
                {
                    TicketId = PaymentFacade.GetTransactionId(),
                    OrderGuid = Guid.Empty,
                    SellerGuid = vpd.SellerGuid,
                    PponDeliveryType = vpd.DeliveryType ?? (int)DeliveryType.ToHouse,
                    SessionId = "",
                    PezId = "",
                    ItemOptions = GetCartItemOption(cartItemDTO.ItemOptions),
                    BusinessHourGuid = vpd.BusinessHourGuid,
                    SelectedStoreGuid = tmpStoreGuid,
                    DealName = vpd.CouponUsage,
                    DealPic = !string.IsNullOrEmpty(vpd.AppDealPic) ? PponFacade.GetPponDealFirstImagePath(vpd.AppDealPic) : vpd.DefaultDealImage,
                    Quantity = cartItemDTO.Quantity,
                    ItemPrice = Convert.ToInt32(vpd.ItemPrice),
                    Amount = Convert.ToInt32(cartItemDTO.Quantity * vpd.ItemPrice),
                    Freights = cartFreights.Freights,
                    FreightsId = cartFreights.Id,
                    EventName = PponFacade.EventNameGet(vpd)
                };

                if (cartFreights.Freights == 0)
                {
                    //免運購物車
                    smallCart = mainCartDTO.SmallCart.Where(x => x.Freights == 0).FirstOrDefault();
                    if (smallCart != null)
                    {
                        //已有免運購物車，把商品放入免運購物車中
                        smallCart.PaymentDTOList.Add(paymentDTO);
                        smallCart.FreightsDescription = cartFreights.FreightsName;
                    }
                    else
                    {
                        //沒有免運購物車，建立一台
                        smallCart = new SmallCart();
                        smallCart.SellerGuid = Guid.Empty;//0元購物車可能包含不同賣家，故不用放SellerGuid
                        smallCart.FreightsId = 0;
                        smallCart.PaymentDTOList.Add(paymentDTO);
                        smallCart.Freights = cartFreights.Freights;
                        smallCart.FreightsDescription = cartFreights.FreightsName;
                        mainCartDTO.SmallCart.Add(smallCart);
                    }
                }
                else
                {
                    //【非】免運購物車
                    smallCart = mainCartDTO.SmallCart.Where(x => x.FreightsId == cartFreights.Id).FirstOrDefault();
                    if (smallCart != null)
                    {
                        //已有X元購物車，把商品放入X元購物車中
                        smallCart.PaymentDTOList.Add(paymentDTO);
                        smallCart.FreightsDescription = cartFreights.FreightsName;
                    }
                    else
                    {
                        //沒有X元購物車，建立一台
                        smallCart = new SmallCart();
                        smallCart.SellerGuid = vpd.SellerGuid;
                        smallCart.FreightsId = cartFreights.Id;
                        smallCart.PaymentDTOList.Add(paymentDTO);
                        smallCart.Freights = cartFreights.Freights;
                        smallCart.FreightsDescription = cartFreights.FreightsName;
                        mainCartDTO.SmallCart.Add(smallCart);
                    }
                }
                mainCartDTO.TransactionId = TransactionId;

                mainCartDTO = RebuildCart(mainCartDTO);

                scio.TicketId = TransactionId;
                scio.ItemOptions = new JsonSerializer().Serialize(mainCartDTO);
                scio.CreateTime = DateTime.Now;
                scio.UserId = UserId;
                scio.IsRemove = false;
                op.ShoppingCartItemOptionSet(scio);
            }
            else
            {
                //Query
                ShoppingCartItemOption scio = FindItemOption(UserName, TransactionId, out UserId);

                if (scio != null)
                {
                    //資料庫有購物車資料
                    if (!string.IsNullOrEmpty(scio.ItemOptions))
                    {
                        mainCartDTO = new JsonSerializer().Deserialize<MainCartDTO>(scio.ItemOptions);
                    }
                }
                else
                {
                    //資料庫無購物車資料
                    scio = new ShoppingCartItemOption();
                    TransactionId = Security.MD5Hash(Guid.NewGuid().ToString() + DateTime.Now);
                }

                if (scio != null)
                {
                    mainCartDTO = RebuildCart(mainCartDTO);
                    mainCartDTO.TransactionId = TransactionId;
                }
            }

            mainCartDTO.TransactionId = TransactionId;
            return mainCartDTO;
        }

        private static ShoppingCartItemOption FindItemOption(string UserName, string TransactionId, out int UserId)
        {
            ShoppingCartItemOption scio = new ShoppingCartItemOption();
            UserId = 0;
            if (!string.IsNullOrEmpty(UserName))
            {
                Member mem = mp.MemberGetByUserEmail(UserName);
                UserId = mem.UniqueId;
            }
            if (!string.IsNullOrEmpty(TransactionId))
            {
                scio = op.ShoppingCartItemOptionGetByTicketid(TransactionId)
                    .OrderByDescending(x => x.CreateTime).FirstOrDefault();
                if (scio != null)
                {
                    if (scio.UserId == 0 && UserId != 0)
                    {
                        //先把其他筆的資料移除
                        ShoppingCartItemOptionCollection Datas = op.ShoppingCartItemOptionGetByUid(UserId);
                        foreach(ShoppingCartItemOption data in Datas)
                        {
                            data.IsRemove = true;
                            op.ShoppingCartItemOptionSet(data);
                        }
                        //更新最新的一筆資料
                        scio.UserId = UserId;
                        op.ShoppingCartItemOptionSet(scio);
                    }
                }
            }
            else if (!string.IsNullOrEmpty(UserName))
            {
                
                if (scio == null || scio.IsLoaded == false)
                {
                    //用userid找不到，改用ticketid查，若有找到，將userid更新回該筆資料
                    scio = op.ShoppingCartItemOptionGetByTicketid(TransactionId)
                    .OrderByDescending(x => x.CreateTime).FirstOrDefault();
                    if (scio != null)
                    {
                        if (scio.UserId == 0)
                        {
                            scio.UserId = UserId;
                            op.ShoppingCartItemOptionSet(scio);
                        }
                    }
                }
            }

            return scio;
        }

        private static MainCartDTO RebuildCart(MainCartDTO mainCartDTO)
        {
            List<CartPaymentDTO> PaymentDTOList = new List<CartPaymentDTO>();
            List<CartPaymentDTO> DataList = new List<CartPaymentDTO>();

            PaymentDTOList = mainCartDTO.SmallCart.SelectMany(x => x.PaymentDTOList).ToList();
            //Merge 重複的項目
            foreach (CartPaymentDTO tmpData in PaymentDTOList)
            {
                bool isExist = false;
                foreach (CartPaymentDTO tmp in DataList)
                {
                    if (tmp.BusinessHourGuid == tmpData.BusinessHourGuid
                            && tmp.SelectedStoreGuid == tmpData.SelectedStoreGuid
                            && CompareItemOptions(tmp.ItemOptions,tmpData.ItemOptions))
                    {
                        tmp.Quantity += tmpData.Quantity;
                        tmp.Amount += tmpData.Quantity * tmpData.ItemPrice;
                        isExist = true;
                        continue;
                    }
                }
                if (!isExist)
                {
                    DataList.Add(tmpData);
                }
            }
            DataList = DataList.OrderBy(x => x.Freights).ToList();

            int totalAmount = 0;

            MainCartDTO tmpMainCart = new MainCartDTO();
            foreach (CartPaymentDTO paymentDTO in DataList)
            {
                SmallCart tmpCart = new SmallCart();
                if (paymentDTO.Freights == 0)
                {
                    //免運
                    var existCart = tmpMainCart.SmallCart.Where(x => x.Freights == 0).FirstOrDefault();
                    if (existCart == null)
                    {
                        //無免運購物車
                        string FreightsDescription = "";
                        SmallCart main = mainCartDTO.SmallCart.Where(x => x.FreightsId == paymentDTO.FreightsId).FirstOrDefault();
                        if (main == null)
                        {
                            ShoppingCartFreight tmp = sp.ShoppingCartFreightsGet(paymentDTO.FreightsId);
                            FreightsDescription = tmp.FreightsName;
                        }
                        else
                        {
                            FreightsDescription = main.FreightsDescription;
                        }
                        tmpCart.SellerGuid = Guid.Empty;
                        tmpCart.FreightsId = 0;
                        tmpCart.PaymentDTOList.Add(paymentDTO);
                        tmpCart.Freights = paymentDTO.Freights;
                        tmpCart.FreightsDescription = FreightsDescription;
                        tmpMainCart.SmallCart.Add(tmpCart);
                    }
                    else
                    {
                        tmpCart = existCart;
                        tmpCart.PaymentDTOList.Add(paymentDTO);
                    }
                }
                else
                {
                    var existCart = tmpMainCart.SmallCart.Where(x => x.FreightsId == paymentDTO.FreightsId).FirstOrDefault();
                    ShoppingCartFreight tmp = sp.ShoppingCartFreightsGet(paymentDTO.FreightsId);
                    if (existCart == null)
                    {
                        //無此購物車資料
                        var main = mainCartDTO.SmallCart.Where(x => x.FreightsId == paymentDTO.FreightsId).FirstOrDefault();
                        tmpCart.SellerGuid = paymentDTO.SellerGuid;
                        tmpCart.FreightsId = paymentDTO.FreightsId;
                        tmpCart.PaymentDTOList.Add(paymentDTO);
                        tmpCart.Freights = paymentDTO.Freights;
                        tmpCart.FreightsDescription = main.FreightsDescription;
                        tmpCart.NoFreightLimit = tmp.NoFreightLimit;
                        tmpMainCart.SmallCart.Add(tmpCart);
                    }
                    else
                    {
                        tmpCart = existCart;
                        tmpCart.PaymentDTOList.Add(paymentDTO);
                    }
                }

                tmpCart.Amount = tmpCart.PaymentDTOList.Select(x => x.Amount).Sum();        //子購物車金額

                
                if (tmpCart.Amount < tmpCart.NoFreightLimit)
                {
                    tmpCart.ActualFreights = tmpCart.Freights;//實際運費
                }
                else
                {
                    tmpCart.ActualFreights = 0;
                }

                totalAmount += Convert.ToInt32(paymentDTO.Amount);                             //母購物車金額
            }

            tmpMainCart.BCash = mainCartDTO.BCash;
            tmpMainCart.SCash = mainCartDTO.SCash;
            tmpMainCart.Amount = totalAmount;
            tmpMainCart.Quantity = DataList.Count();


            return tmpMainCart;
        }

        private static bool CompareItemOptions(string s1, string s2)
        {
            bool flag = false;

            List<CartItemOption> list1 = GetCartItemOption(s1).SelectMany(x => x.ItemList).ToList();
            List<CartItemOption> list2 = GetCartItemOption(s2).SelectMany(x => x.ItemList).ToList();

            flag = !list1.Except(list2).Union(list2.Except(list1)).Any();

            return flag;
        }

        /// <summary>
        /// 取得小購物車內購買清單
        /// </summary>
        /// <param name="cartItemDTO"></param>
        /// <param name="UserName"></param>
        /// <param name="TransactionId"></param>
        /// <returns></returns>
        public static LiteMainCart LiteCartListGet(CartItemDTO cartItemDTO, string UserName, string TransactionId)
        {
            MainCartDTO mainCartDTO = SmallCartListGet(cartItemDTO, UserName, TransactionId);

            return CartSCL(mainCartDTO);
        }
        /// <summary>
        /// 取得小購物車內購買清單數量
        /// </summary>
        /// <param name="cartItemDTO"></param>
        /// <param name="UserName"></param>
        /// <param name="TransactionId"></param>
        /// <returns></returns>
        public static int LiteCartListGetCount(CartItemDTO cartItemDTO, string UserName, string TransactionId)
        {
            MainCartDTO mainCartDTO = SmallCartListGet(cartItemDTO, UserName, TransactionId);

            return mainCartDTO.SmallCart.SelectMany(x => x.PaymentDTOList).ToList().Count();
        }
        /// <summary>
        /// MainCartDTO轉成精簡版
        /// </summary>
        /// <param name="mainCartDTO"></param>
        /// <returns></returns>
        private static LiteMainCart CartSCL(MainCartDTO mainCartDTO)
        {
            LiteMainCart liteMainCart = new LiteMainCart();

            foreach (SmallCart smallCart in mainCartDTO.SmallCart)
            {
                liteMainCart.LiteCart.Add(new LiteCart()
                {
                    SellerGuid = smallCart.SellerGuid,
                    FreightsId = smallCart.FreightsId,
                    FreightsDescription = smallCart.FreightsDescription,
                    PaymentDTOList = PaymentDTOCSL(smallCart.PaymentDTOList),
                    Freights = smallCart.Freights,
                    Amount = smallCart.Amount,
                    IsChecked = smallCart.IsChecked
                });
            }

            liteMainCart.Quantity = mainCartDTO.Quantity;
            liteMainCart.Amount = mainCartDTO.Amount;
            liteMainCart.TransactionId = mainCartDTO.TransactionId;

            return liteMainCart;
        }
        /// <summary>
        /// CartPaymentDTO轉成精簡版
        /// </summary>
        /// <param name="PaymentDTOList"></param>
        /// <returns></returns>
        private static List<PaymentDTOLite> PaymentDTOCSL(List<CartPaymentDTO> PaymentDTOList)
        {
            List<PaymentDTOLite> PaymentDTOLiteList = new List<PaymentDTOLite>();
            foreach (CartPaymentDTO dto in PaymentDTOList)
            {
                PaymentDTOLiteList.Add(new PaymentDTOLite()
                {
                    TicketId = dto.TicketId,
                    SellerGuid = dto.SellerGuid,
                    SessionId = dto.SessionId,
                    PponDeliveryType = dto.PponDeliveryType,
                    BusinessHourGuid = dto.BusinessHourGuid,
                    DealName = dto.DealName,
                    EventName = dto.EventName,
                    DealPic = dto.DealPic,
                    DealStatus = dto.DealStatus,
                    Quantity = dto.Quantity,
                    Amount = dto.Amount,
                    Freights = dto.Freights,
                    IsDealHasDiscount = dto.IsDealHasDiscount,
                    SelectedStoreGuid = dto.SelectedStoreGuid,
                    ItemOptions = GetCartItemOption(dto.ItemOptions),
                    DonationReceiptsType = dto.DonationReceiptsType,
                    DiscountCode = dto.DiscountCode,
                    IsDealUseDiscount = dto.IsDealUseDiscount,
                    DiscountList = dto.DiscountList
                });
            }
            return PaymentDTOLiteList;
        }
        /// <summary>
        /// 湊單
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="fid"></param>
        /// <returns></returns>
        public static List<IViewPponDeal> OrderAddOn(string sid, string fid)
        {
            Guid SellerGuid = Guid.Empty;
            int freightsId = 0;

            Guid.TryParse(sid, out SellerGuid);
            int.TryParse(fid, out freightsId);
            List<Guid> bids = new List<Guid>();
            List<IViewPponDeal> vpds = new List<IViewPponDeal>();

            if (SellerGuid != Guid.Empty && freightsId != 0)
            {
                ShoppingCartFreight scf = sp.ShoppingCartFreightsGet(freightsId);
                if (scf != null && scf.IsLoaded)
                {
                    if (scf.SellerGuid == SellerGuid)
                    {
                        ShoppingCartFreightsItemCollection scfc = sp.ShoppingCartFreightsItemGetByFid(scf.Id);
                        bids.AddRange(scfc.Select(x => x.BusinessHourGuid));
                    }
                }
            }

            foreach (Guid bid in bids)
            {
                Guid tmpBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(bid);
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(tmpBid);
                if (vpds.Where(x => x.BusinessHourGuid == tmpBid).FirstOrDefault() == null)
                {
                    vpd.OrderedQuantity = vpd.GetAdjustedOrderedQuantity().Quantity;
                    vpds.Add(vpd);
                }
            }
            return vpds;
        }

        /// <summary>
        /// 更新折價券至購物車(單商品)
        /// </summary>
        /// <param name="discountCode">折價券代碼</param>
        /// <param name="ticketId">TicketId</param>
        /// <param name="bid">BusinessHourGuid</param>
        /// <param name="userId">UserId</param>
        /// <param name="transactionId">TransactionId</param>
        /// <param name="discountAmount">折價券面額</param>
        /// <returns></returns>
        public static bool SetDiscountToSmallCartItem(string discountCode, string ticketId, Guid? bid, int userId, string transactionId, out decimal discountAmount)
        {
            discountAmount = 0m;

            if (userId == 0 || string.IsNullOrEmpty(transactionId) || string.IsNullOrEmpty(ticketId) || (!string.IsNullOrEmpty(discountCode) && bid == null))
            {
                return false;
            }

            if (!string.IsNullOrEmpty(discountCode))
            {
                IEnumerable<ViewDiscountDetail> discountList = PromotionFacade.GetMemberDiscountList(userId, bid);
                if (!discountList.Any(x => x.Code == discountCode))
                {
                    return false;
                }

                discountAmount = discountList.FirstOrDefault(x => x.Code == discountCode).Amount ?? 0m;
            }

            ShoppingCartItemOption scio = new ShoppingCartItemOption();
             
            scio = op.ShoppingCartItemOptionGetByTicketid(transactionId)
                .OrderByDescending(x => x.CreateTime).FirstOrDefault();

            if (scio == null)
            {
                return false;
            }

            //總金額扣掉折價券
            MainCartDTO mainCartDTO = new JsonSerializer().Deserialize<MainCartDTO>(scio.ItemOptions);

            //塞使用中的折價券
            foreach (SmallCart tmpCart in mainCartDTO.SmallCart)
            {
                if (tmpCart.PaymentDTOList.Any(x => x.TicketId == ticketId))
                {
                    tmpCart.PaymentDTOList.Where(x => x.TicketId == ticketId).FirstOrDefault().DiscountCode = discountCode;
                    tmpCart.PaymentDTOList.Where(x => x.TicketId == ticketId).FirstOrDefault().DiscountAmount = discountAmount;
                    break;
                }
            }

            scio.ItemOptions = new JsonSerializer().Serialize(mainCartDTO);
            op.ShoppingCartItemOptionSet(scio);

            return true;
        }

        /// <summary>
        /// 取得折價券使用狀態
        /// </summary>
        /// <param name="discountCode">折價券代碼</param>
        /// <param name="ticketId">TicketId</param>
        /// <param name="transactionId">TransactionId</param>
        /// <param name="busineseHourGuid">BusineseHourGuid</param>
        /// <param name="userId">UserId</param>
        /// <returns></returns>
        public static CartDiscountUseState GetDiscountState(string discountCode, string ticketId, string transactionId, int userId, Guid busineseHourGuid)
        {
            CartDiscountUseState discountStatu = CartDiscountUseState.NoDiscount;
            bool isCanUse = true;

            if (string.IsNullOrEmpty(discountCode) || string.IsNullOrEmpty(transactionId))
            {
                return discountStatu;
            }
            
            IEnumerable<ViewDiscountDetail> discountList = PromotionFacade.GetMemberDiscountList(userId, busineseHourGuid);
            if (!discountList.Any(x => x.Code == discountCode))
            {
                return discountStatu;
            }

            ShoppingCartItemOption scio = new ShoppingCartItemOption();

            scio = op.ShoppingCartItemOptionGetByTicketid(transactionId)
                .OrderByDescending(x => x.CreateTime).FirstOrDefault();

            if (scio == null)
            {
                return discountStatu;
            }
                        
            MainCartDTO mainCartDTO = new JsonSerializer().Deserialize<MainCartDTO>(scio.ItemOptions);
                
            foreach (SmallCart tmpCart in mainCartDTO.SmallCart)
            {
                //先找到折價券代碼
                if (!tmpCart.PaymentDTOList.Any(x => x.DiscountCode == discountCode))
                {
                    continue;
                }

                isCanUse = false;

                //再比對是否是此商品使用的折價券
                if (tmpCart.PaymentDTOList.Any(x => (x.TicketId == ticketId && x.DiscountCode == discountCode)))
                {
                    return CartDiscountUseState.Using;
                }
                else
                {
                    return CartDiscountUseState.Used;
                }
            }

            if (isCanUse)
            {
                return CartDiscountUseState.CanUse;
            }

            return discountStatu;
        }

        /// <summary>
        /// 將多重選項Object轉成系統辨識的String
        /// </summary>
        /// <param name="OptionList"></param>
        /// <returns></returns>
        private static string GetCartItemOption(List<CartItemOptionList> OptionList)
        {
            string AccessoryEntry = "";
            int idx = 0;
            foreach (CartItemOptionList Options in OptionList)
            {
                if (idx > 0)
                {
                    AccessoryEntry += "||";
                }
                int oidx = 0;
                foreach (CartItemOption cio in Options.ItemList)
                {
                    if (oidx > 0)
                    {
                        AccessoryEntry += ",";
                    }
                    AccessoryEntry += cio.OptionName + "|^|" + cio.OptionGuid;
                    oidx++;
                }
                AccessoryEntry += "|#|" + Options.ItemQuantity;
                idx++;
            }

            return AccessoryEntry;
        }
        /// <summary>
        /// 將多重選項String轉成APP辨識的Object
        /// </summary>
        /// <param name="OptionList"></param>
        /// <returns></returns>
        private static List<CartItemOptionList> GetCartItemOption(string OptionList)
        {
            List<CartItemOptionList> list = new List<CartItemOptionList>();

            string[] AccessoryEntry = OptionList.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string entry in AccessoryEntry)
            {
                if (string.IsNullOrEmpty(entry))
                {
                    continue;
                }
                var tmp = entry.Split(new string[] { "|#|" }, StringSplitOptions.RemoveEmptyEntries);
                string itemQuantity = tmp[1];
                string[] ItemList = tmp[0].ToString().Split(',');
                List<CartItemOption> options = new List<CartItemOption>();
                foreach (string item in ItemList)
                {
                    string[] data = item.Split(new string[] { "|^|" }, StringSplitOptions.RemoveEmptyEntries);
                    CartItemOption option = new CartItemOption()
                    {
                        OptionName = data[0],
                        OptionGuid = data[1]
                    };
                    options.Add(option);
                }

                CartItemOptionList cartList = new CartItemOptionList()
                {
                    ItemList = options,
                    ItemQuantity = Convert.ToInt32(itemQuantity)
                };
                list.Add(cartList);
            }

            return list;
        }
        /// <summary>
        /// 取得檔次圖片
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static string GetAppPic(string bid)
        {
            Guid _bid = Guid.Empty;
            Guid.TryParse(bid, out _bid);
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(_bid);
            return !string.IsNullOrEmpty(vpd.AppDealPic) ? PponFacade.GetPponDealFirstImagePath(vpd.AppDealPic) : vpd.DefaultDealImage;
        }

        public static string ItemOptionGet(string itemOptions)
        {
            string result = string.Empty;
            string temp = string.Empty;
            string[] kk = itemOptions.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string kz in kk)
            {
                string[] yy = kz.Split(new string[] { "|#|" }, StringSplitOptions.None);
                int itemCount = int.Parse(yy[1]);

                string[] ww = yy[0].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string wz in ww)
                {
                    string[] zz = wz.Split(new string[] { "|^|" }, StringSplitOptions.RemoveEmptyEntries);
                    result += "(" + zz[0] + ")";
                }
                result += "X" + itemCount + ",";
            }
            if (result.Length > 1)
            {
                result = result.Substring(0, result.Length - 1);
            }

            return result;
        }
        /// <summary>
        /// 取的TicketId
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public static string GetTicketId(string UserName)
        {
            string TicketId = Security.MD5Hash(Guid.NewGuid().ToString() + DateTime.Now);
            int UserId = 0;
            ShoppingCartItemOption scio = null;

            if (!string.IsNullOrEmpty(UserName))
            {
                //使用者已登入
                Member mem = mp.MemberGetByUserEmail(UserName);
                if (mem.IsLoaded)
                {
                    UserId = mem.UniqueId;
                    var data = op.ShoppingCartItemOptionGetByUid(UserId)
                                            .OrderByDescending(x => x.CreateTime);
                    if (data.Count() > 1)
                    {
                        int idx = 0;
                        foreach (ShoppingCartItemOption d in data)
                        {
                            if (idx == 0)
                            {
                                continue;
                            }
                            d.IsRemove = true;
                            op.ShoppingCartItemOptionSet(d);
                        }
                    }
                    scio = op.ShoppingCartItemOptionGetByUid(UserId)
                                            .OrderByDescending(x => x.CreateTime).FirstOrDefault();
                }
            }
            else
            {
                //使用者未登入
                scio = op.ShoppingCartItemOptionGetByTicketid(TicketId)
                                            .OrderByDescending(x => x.CreateTime).FirstOrDefault();
            }

            if (scio == null)
            {
                ShoppingCartItemOption item = new ShoppingCartItemOption();
                item.TicketId = TicketId;
                item.UserId = UserId;
                item.CreateTime = DateTime.Now;
                item.IsRemove = false;
                op.ShoppingCartItemOptionSet(item);
            }
            else
            {
                TicketId = scio.TicketId;
            }
            return TicketId;
        }

        /// <summary>
        /// 移除購物車中商品
        /// </summary>
        /// <param name="tid"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        public static List<string> RemoveItemFromCart(string tid, string oid, string UserName, int UserId)
        {
            bool flag = false;
            List<string> result = new List<string>();
            List<ShoppingCartItemOption> scios = new List<ShoppingCartItemOption>();
            if (!string.IsNullOrEmpty(UserName) && UserId != 0)
            {
                scios = op.ShoppingCartItemOptionGetByUid(UserId).ToList();
            }
            else if (oid != null)
            {
                scios = op.ShoppingCartItemOptionGetByTicketid(oid).ToList();
            }

            int Amount = 0;
            int TotalAmount = 0;
            int TotalFreight = 0;
            int Result = 0;
            
            if (scios.Count > 0)
            {
                ShoppingCartItemOption sco = scios[0];
                MainCartDTO mainCartDTO = new JsonSerializer().Deserialize<MainCartDTO>(sco.ItemOptions);
                foreach (SmallCart smallCart in mainCartDTO.SmallCart)
                {
                    var ori = smallCart.PaymentDTOList.Where(x => x.TicketId == tid).FirstOrDefault();
                    if (ori != null)
                    {
                        smallCart.PaymentDTOList.Remove(ori);
                        if (smallCart.PaymentDTOList.Count == 0)
                        {
                            mainCartDTO.SmallCart.Remove(smallCart);//若小購物車沒有資料了就把小購物車刪掉
                        }

                        flag = true;


                        //重算小購物車總和和運費
                        foreach (var s in smallCart.PaymentDTOList)
                        {
                            Amount = Amount + s.Amount;
                        }
                        smallCart.Amount = Amount;

                        if (smallCart.Amount < smallCart.NoFreightLimit)
                        {
                            smallCart.ActualFreights = smallCart.Freights;
                        }
                        else
                        {
                            smallCart.ActualFreights = 0;
                        }

                        break;
                    }
                }
                sco.ItemOptions = new JsonSerializer().Serialize(mainCartDTO);
                op.ShoppingCartItemOptionSet(sco);

                //重算大購物車總和和運費
                foreach (var s in mainCartDTO.SmallCart)
                {
                    TotalAmount = TotalAmount + Convert.ToInt32(s.Amount);
                    TotalFreight = TotalFreight + s.ActualFreights;
                }
                mainCartDTO.Amount = TotalAmount;
                Result = TotalAmount + TotalFreight - mainCartDTO.BCash - mainCartDTO.SCash;
            }

            result.Add(flag.ToString());
            result.Add(TotalAmount.ToString());
            result.Add(TotalFreight.ToString());
            result.Add(Result.ToString());
            return result;
        }        

        private static int SetDeliveryCharge(decimal orderedAmount, IViewPponDeal deal)
        {
            CouponFreightCollection cfc = pp.CouponFreightGetList(deal.BusinessHourGuid, CouponFreightType.Income);

            if (null != cfc && cfc.Count > 0)
            {
                foreach (CouponFreight cf in cfc)
                {
                    if (orderedAmount >= cf.StartAmount && cf.EndAmount > orderedAmount)
                    {
                        return (int)(cf.FreightAmount);
                    }
                }
            }
            else
            {
                return (int)CheckDeliveryCharge(orderedAmount, deal);
            }

            return 0;
        }
        private static decimal CheckDeliveryCharge(decimal orderedAmount, IViewPponDeal deal)
        {
            return deal.BusinessHourDeliveryCharge;
        }
        private static string CheckData(IViewPponDeal viewPponDeal, int quantity, int bonusCash, decimal scash, string userName, bool isDailyRestriction, bool isGroupCoupon)
        {
            if (isGroupCoupon && config.EnableGroupCouponNewMakeOrderApi)
            {
                var info = PponFacade.GetApiGroupCouponInfo(viewPponDeal);
                if (quantity > (info.MaxQuantity * info.BoughtQuantity))
                {
                    return "每單訂購數量最多" + info.MaxQuantity * (info.BoughtQuantity + info.FreeQuantity) + "份，請重新輸入數量";
                }
            }
            else if (quantity > (viewPponDeal.MaxItemCount ?? int.MaxValue))
            {
                return "每單訂購數量最多" + viewPponDeal.MaxItemCount + "份，請重新輸入數量";
            }

            int already = pp.ViewPponOrderDetailGetCountByUser(userName, viewPponDeal.BusinessHourGuid, isDailyRestriction);
            if (viewPponDeal.ItemDefaultDailyAmount != null)
            {
                if (quantity + already > viewPponDeal.ItemDefaultDailyAmount.Value)
                {
                    return "每人訂購數量最多" + viewPponDeal.ItemDefaultDailyAmount + "份，您已訂購" + already + "份，請重新輸入數量";
                }
            }

            if (bonusCash > Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(userName) / 10))
            {
                return "您的可用紅利點數換算可抵用金額最多為" + Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(userName) / 10) + "元，請重新輸入金額";
            }

            if (bonusCash < 0)
            {
                return "紅利點數金額不可為負，請重新輸入金額";
            }

            if (scash > OrderFacade.GetSCashSum(userName))
            {
                return "您的可用17購物金可抵用金額最多為" + OrderFacade.GetSCashSum(userName).ToString() + "元，請重新輸入金額";
            }

            return string.Empty;
        }

        /// <summary>
        /// 修改會員資料與會員紀錄的收件者資料
        /// </summary>
        /// <param name="paymentDTO"></param>
        /// <param name="order"></param>
        /// <param name="userName"></param>
        /// <param name="deliveryType"></param>
        /// <param name="withBuilding">是否要考慮building的紀錄</param>
        private static void saveDeliveryInfo(MainCartDTO mainCartDTO, CartPaymentDTO paymentDTO, Order order, string userName, int? deliveryType, bool withBuilding)
        {
            if (!string.IsNullOrWhiteSpace(mainCartDTO.AddressInfo))
            {
                string[] yy = mainCartDTO.AddressInfo.Split('^');
                if (!string.IsNullOrWhiteSpace(yy[0]))
                {
                    string[] badd = yy[0].Split('|');

                    Member m = mp.MemberGet(userName);
                    if (null != m && m.IsLoaded)
                    {
                        if (!string.IsNullOrWhiteSpace(badd[0]))
                        {
                            m.LastName = badd[0];
                        }

                        if (!string.IsNullOrWhiteSpace(badd[1]))
                        {
                            m.FirstName = badd[1];
                        }

                        if (!string.IsNullOrWhiteSpace(badd[2]))
                        {
                            m.Mobile = badd[2];
                        }

                        // carrier
                        if (!string.IsNullOrWhiteSpace(mainCartDTO.DeliveryInfo.CarrierId) && (mainCartDTO.DeliveryInfo.CarrierType == CarrierType.Phone || mainCartDTO.DeliveryInfo.CarrierType == CarrierType.PersonalCertificate))
                        {
                            m.CarrierType = (int)mainCartDTO.DeliveryInfo.CarrierType;
                            m.CarrierId = mainCartDTO.DeliveryInfo.CarrierId;
                        }
                        else
                        {
                            m.CarrierType = (int)CarrierType.Member;
                            m.CarrierId = m.UniqueId.ToString();
                        }

                        Guid buid = Guid.Empty;
                        if (!string.IsNullOrWhiteSpace(badd[6]) && Guid.TryParse(badd[5], out buid))
                        {
                            //移除building
                            if (withBuilding)
                            {
                                m.BuildingGuid = buid;
                                m.CompanyAddress = lp.CityGet(int.Parse(badd[3])).CityName + lp.CityGet(int.Parse(badd[4])).CityName + lp.BuildingGet(buid).BuildingStreetName + badd[6];
                            }
                            else
                            {
                                //給予預設值，該township的第一個路段
                                BuildingCollection buildings = CityManager.BuildingListGetByTownshipId(int.Parse(badd[4]));
                                if (buildings.Count > 0)
                                {
                                    m.BuildingGuid = buildings.First().Guid;
                                    m.CompanyAddress = lp.CityGet(int.Parse(badd[3])).CityName + lp.CityGet(int.Parse(badd[4])).CityName + badd[6];
                                }
                            }
                        }

                        mp.MemberSet(m);

                        // fix order's buyer name
                        if (deliveryType.HasValue && int.Equals((int)DeliveryType.ToShop, deliveryType.Value))
                        {
                            order.MemberName = m.DisplayName;

                            op.OrderSet(order);
                        }
                    }
                }

                // receiver delivery
                if (!string.IsNullOrWhiteSpace(yy[1]))
                {
                    string[] radd = yy[1].Split('|');
                    if (bool.Parse(radd[6]))
                    {
                        if (!string.IsNullOrWhiteSpace(radd[5]))
                        {
                            MemberDelivery md = new MemberDelivery();
                            md.CreateTime = DateTime.Now;
                            md.UserId = MemberFacade.GetUniqueId(userName);
                            Guid buid = Guid.Empty;
                            if (!string.IsNullOrWhiteSpace(radd[5]) && Guid.TryParse(radd[4], out buid))
                            {
                                //移除building
                                if (withBuilding)
                                {
                                    md.BuildingGuid = Guid.Parse(radd[4]);
                                    md.Address = lp.CityGet(int.Parse(radd[2])).CityName +
                                                 lp.CityGet(int.Parse(radd[3])).CityName +
                                                 lp.BuildingGet(md.BuildingGuid).BuildingStreetName + radd[5];
                                }
                                else
                                {
                                    //移除路段。
                                    //給予預設值，該township的第一個路段
                                    BuildingCollection buildings = CityManager.BuildingListGetByTownshipId(int.Parse(radd[3]));
                                    if (buildings.Count > 0)
                                    {
                                        md.BuildingGuid = buildings.First().Guid;
                                        md.Address = lp.CityGet(int.Parse(radd[2])).CityName + lp.CityGet(int.Parse(radd[3])).CityName + radd[5];
                                    }
                                }
                            }
                            md.ContactName = radd[0];
                            md.AddressAlias = radd[0];
                            md.Mobile = radd[1];

                            mp.MemberDeliverySet(md);
                        }
                    }
                }
            }
        }

        private static void CheckResult(MainCartDTO mainCartDTO, CartPaymentDTO paymentDTO, CreditCardAuthResult result, int userId)
        {
            PaymentTransaction pt = op.PaymentTransactionGet(paymentDTO.TicketId, PaymentType.Creditcard, PayTransType.Authorization);
            int status = Helper.SetPaymentTransactionPhase(pt.Status, result.TransPhase);
            int rlt = 0;
            try
            {
                rlt = int.Parse(result.ReturnCode);
            }
            catch
            {
                rlt = -9527;
            }

            if (Helper.GetPaymentTransactionPhase(pt.Status) != PayTransPhase.Created)
            {
                logger.Error("trans_id: " + pt.TransId + " 被拒絕覆蓋已回覆的授權紀錄\n result: " + rlt + "\n message: " + result.TransMessage
                    + "\n api_provider: " + (int)result.APIProvider + "\n original_apiprovider: " + pt.ApiProvider);
            }
            else
            {
                PaymentFacade.UpdateTransaction(paymentDTO.TicketId, pt.OrderGuid, PaymentType.Creditcard, Convert.ToDecimal(paymentDTO.Amount), result.AuthenticationCode,
                    result.TransType, result.OrderDate, result.TransMessage + " " + result.CardNumber, status, rlt, result.APIProvider);

                if (mainCartDTO.IsMasterPass)
                {
                    MasterpassCardInfoLog mpclog = ChannelFacade.GetMasterpassCardInfoLog(mainCartDTO.MasterPassTransData.VerifierCode,
                    mainCartDTO.MasterPassTransData.AuthToken);

                    if (mpclog.IsLoaded && mpclog.IsUsed)
                    {
                        MasterPass mpass = new MasterPass();
                        try
                        {
                            mpass.MasterPassTransLog(mpclog.TransId, mpclog.Indicator, paymentDTO.Amount, result.AuthenticationCode
                                , rlt, pt.OrderGuid ?? Guid.Empty, userId, mpclog.OauthToken, mpclog.CardBrandId
                                , mpclog.CardBrandName, mpclog.CardHolder, mpclog.CardNumber);
                        }
                        catch (Exception error)
                        {
                            string errormessage = string.Format("masterpass log error:{0},masterpasstransid:{1},authenticationcode:{2},resultcode:{3}orderguid:{4}"
                                , error.Message, mpclog.TransId, result.AuthenticationCode, rlt, (pt.OrderGuid ?? Guid.Empty));
                            mpass.MasterPassTransErrorLog((pt.OrderGuid ?? Guid.Empty), userId, errormessage);
                            logger.Error(errormessage);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 檢查地址字串是否包含了路段
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private static bool CheckAddressBuilding(string address)
        {
            TaiwanAddress twAddress = new TaiwanAddress(address);
            string rem = address.Remove(address.IndexOf(twAddress.Town, System.StringComparison.Ordinal) + twAddress.Town.Length);
            string building = address.Replace(rem, "").Trim();
            if (building.Length > 0)
            {
                string pattern = @"(^\d)"; // 規則字串
                Regex regex = new Regex(pattern);
                Match match = regex.Match(building);
                if (match.Success)
                {
                    return false;
                }
            }
            return true;
        }

        public static Dictionary<string, string> GetDealPiinLifeTabList(IViewPponDeal vpd)
        {
            Dictionary<string, string> tabs = new Dictionary<string, string>();
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(vpd);
            if (contentLite.Remark != null)
            {
                // 各項頁籤說明
                foreach (string tab in contentLite.Remark.Split(new[] { "||" }, StringSplitOptions.None))
                {
                    string[] item = tab.Split(new[] { "##" }, StringSplitOptions.None);
                    if (item.Length == 2 && !string.IsNullOrWhiteSpace(item[0]))
                    {
                        if (!tabs.ContainsKey(item[0]))
                        {
                            tabs.Add(item[0], item[1]);
                        }
                    }
                }
            }
            else
            {
                tabs.Add("", "");
            }

            // 最後再加入權益說明
            tabs.Add(I18N.Phrase.PiinlifeIntroductionTitle, contentLite.Introduction);
            return tabs;
        }

        #endregion private method
    }
}

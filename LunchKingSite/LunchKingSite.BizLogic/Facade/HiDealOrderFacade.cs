﻿using System;
using System.Collections.Generic;
using System.Transactions;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Linq;
using SubSonic;
using LunchKingSite.BizLogic.Interface;
using System.ServiceModel;
using System.Web;

namespace LunchKingSite.BizLogic.Facade
{
    public class HiDealOrderFacade
    {
        protected static IHiDealProvider hp;
        protected static IAccountingProvider ap;
        protected static ISysConfProvider config;
        protected static ISystemProvider _systemProv;
        protected static IMemberProvider mp;

        static HiDealOrderFacade()
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            config = ProviderFactory.Instance().GetConfig();
            _systemProv = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        #region HiDeal消費者交易行為相關
        /// <summary>
        /// TicketId編碼
        /// </summary>
        /// <param name="sessionId">連線sessionID</param>
        /// <param name="produceId">商品編號</param>
        /// <param name="randomCode">亂數字串</param>
        /// <returns></returns>
        public static string MakeRegularTicketId(string sessionId, int produceId, string randomCode)
        {
            return string.Format("{0}_{1}_{2}", sessionId, produceId.ToString(), randomCode);
        }

        /// <summary>
        /// 依據傳入資料判斷運費金額。
        /// </summary>
        /// <param name="productId">商品編號</param>
        /// <param name="productTotalAmt">購買金額</param>
        /// <param name="freightType">運費類型:費用 或 收入</param>
        /// <param name="freights">運費設定的資料，預設為null，若為null會由資料庫裝查詢</param>
        /// <returns></returns>
        public static decimal CalculateHiDealProductFreight(int productId, decimal productTotalAmt, HiDealFreightType freightType, HiDealFreightCollection freights = null)
        {
            if (freights == null)
            {
                freights = hp.HiDealFreightGetListByProductId(productId, freightType);
            }

            var theFreights = freights.Where(x => x.StartAmount <= productTotalAmt && x.EndAmount > productTotalAmt).ToList();
            return theFreights.Count > 0 ? theFreights.First().FreightAmount : 0;
        }

        /// <summary>
        /// 依據商品編號，取得該商品的選項資料
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>查無資料回傳空的list</returns>
        public static List<HiDealProductOptionCategoryInfo> HiDealProductOptionCategoryInfoGetListByPid(int productId)
        {
            var categoryInfos = new List<HiDealProductOptionCategoryInfo>();
            var categoryDb = hp.HiDealProductOptionCategoryGetListByProductId(productId);
            foreach (var category in categoryDb)
            {
                var categoryInfo = new HiDealProductOptionCategoryInfo();
                categoryInfo.Category = category;
                categoryInfo.OptionItems = hp.HiDealProductOptionItemGetListdByCategoryId(category.Id);
                categoryInfos.Add(categoryInfo);
            }
            return categoryInfos;
        }
        #endregion HiDeal消費者交易行為相關

        #region 前台訂單顯示列表相關
        /// <summary>
        /// [建立]前台顯示訂單資料
        /// </summary>
        /// <param name="order">HiDealOrder</param>
        /// <param name="isOrderNorefund">是否不接受退貨</param>
        /// <returns></returns>
        public static bool HiDealOrderShowInsert(HiDealOrder order, bool isOrderNorefund, bool isDaysOrderNorefund)
        {
            var hod = hp.HiDealOrderDetailGetListByOrderGuid(order.Guid, HiDealProductType.Product).FirstOrDefault();
            if (hod == null)
            {
                return false;
            }
            HiDealDeal hdd = hp.HiDealDealGet(hod.HiDealId);
            var product = hp.HiDealProductGet(hod.ProductId);

            // get remainCount
            var remainCount = hp.HidealRemainCouponCountGet(order.Guid);

            var data = new HiDealOrderShow();
            data.UserId = order.UserId;

            data.OrderPk = order.Pk;
            data.OrderId = order.OrderId;
            data.OrderGuid = order.Guid;
            data.DealId = hdd.Id;
            data.DealName = hdd.Name;
            data.ProductName = hod.ProductName;
            data.ProductId = hod.ProductId;
            data.DeliveryType = hod.DeliveryType;
            data.PaymentType = !order.AtmAmount.Equals(0) ? (int)HiDealOrderShowPaymentType.ATM : (int)HiDealOrderShowPaymentType.CreditCard;
            data.OrderShowStatus = (int)HiDealOrderShowStatus.Create;
            data.RemainCount = remainCount;
            data.IsOrderNorefund = isOrderNorefund;
            data.IsOrderDaysNoRefund = isDaysOrderNorefund;
            data.OrderCreateTime = order.CreateTime;
            data.OrderEndTime = hdd.DealEndTime.Value;
            data.UseStartTime = product.UseStartTime ?? DateTime.MinValue;
            data.UseEndTime = product.UseEndTime ?? DateTime.MinValue;
            hp.HiDealOrderShowSet(data);

            return true;
        }
        /// <summary>
        /// [更新]前台顯示訂單[狀態]
        /// </summary>
        /// <param name="hidealOrderGuid">訂單GUID</param>
        /// <param name="status">訂單狀態</param>
        /// <param name="isOrderComplete">是否為成立單</param>
        /// <returns></returns>
        public static bool HiDealOrderShowUpdate(Guid hidealOrderGuid, HiDealOrderShowStatus status, bool isOrderComplete)
        {
            HiDealOrderShow data = hp.HiDealOrderShowGet(hidealOrderGuid);
            return HiDealOrderShowUpdate(data, status, isOrderComplete);
        }
        /// <summary>
        /// [更新]前台顯示訂單[狀態]
        /// </summary>
        /// <param name="hidealOrderPk">訂單PK</param>
        /// <param name="status">訂單狀態</param>
        /// <param name="isOrderComplete">是否為成立單</param>
        /// <returns></returns>
        public static bool HiDealOrderShowUpdate(int hidealOrderPk, HiDealOrderShowStatus status, bool isOrderComplete)
        {
            HiDealOrderShow data = hp.HiDealOrderShowGetByOrderPk(hidealOrderPk);
            return HiDealOrderShowUpdate(data, status, isOrderComplete);
        }

        /// <summary>
        /// [更新]前台顯示訂單[狀態]
        /// </summary>
        /// <param name="orderShow">orderShow物件</param>
        /// <param name="status">訂單狀態</param>
        /// <param name="isOrderComplete">是否為成立單</param>
        /// <returns></returns>
        private static bool HiDealOrderShowUpdate(HiDealOrderShow orderShow, HiDealOrderShowStatus status, bool isOrderComplete)
        {
            orderShow.OrderShowStatus = (int)status;
            orderShow.IsOrderComplete = isOrderComplete;
            hp.HiDealOrderShowSet(orderShow);
            return true;
        }


        /// <summary>
        /// [更新]前台顯示訂單[憑證使用數]
        /// </summary>
        /// <param name="order">HiDealOrder</param>
        /// <param name="status">訂單狀態</param>
        ///    0	建立訂單
        ///    1	退貨處理中
        ///    2	退貨失敗
        ///    3	訂單已取消
        /// <param name="isOrderNorefund">是否不接受退貨</param>
        /// <param name="isOrderComplete">是否為成立單</param>
        /// <returns></returns>
        public static bool HiDealOrderShowUpdateRemainCount(Guid hidealOrderGuid)
        {
            HiDealOrderShow data = hp.HiDealOrderShowGet(hidealOrderGuid);
            data.RemainCount = hp.HidealRemainCouponCountGet(hidealOrderGuid);
            hp.HiDealOrderShowSet(data);

            return true;
        }
        #endregion

        public static bool CheckOrderGuidOwner(string userName, Guid orderGuid)
        {
            HiDealOrder order = hp.HiDealOrderGet(orderGuid);
            if (order.IsLoaded)
            {
                Member m = mp.MemberGet(userName);
                if (m.IsLoaded && order.UserId == m.UniqueId)
                {
                    return true;
                }
            }
            return false;
        }

        public static ViewHiDealOrderMemberOrderShowCollection ViewHiDealOrderMemberOrderShowGetByOrderGuid(Guid orderGuid) 
        {
            return hp.ViewHiDealOrderMemberOrderShowGetByOrderGuid(orderGuid);
        }

        #region Private Method


        #endregion
    }
}


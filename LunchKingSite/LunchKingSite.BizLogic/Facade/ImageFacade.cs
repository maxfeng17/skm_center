﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using LunchKingSite.BizLogic.Component;
using HtmlAgilityPack;
using System.Threading.Tasks;
using log4net;
using System.Reflection;

namespace LunchKingSite.BizLogic.Facade
{
    public class ImageFacade
    {
        protected static ISysConfProvider config = null;

        private static ILog logger = LogManager.GetLogger("ImageFacade");
        protected static string[] folder = { "original", "small", "large" };

        static ImageFacade()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        private ImageFacade()
        {
        }

        public static string EmptyPponImage {
            get { return Helper.CombineUrl(config.SiteUrl, "Themes/default/images/17Life/G2/PPImage/PPDPic.jpg"); }
        }

        public static string NoAppPic
        {
            get { return Helper.CombineUrl(config.SiteUrl, "/Themes/PCweb/images/no-app-pic.jpg"); }
        }

        public static string GetMediaBaseUrl()
        {
            if (config.MediaCDNEnabled)
            {
                return config.CDNBaseUrl + "/" + ImageLoadBalanceFolder.Media.ToString().ToLower() + "/";
            }
            else
            {
                return config.MediaBaseUrl; 
            }
        }

        /// <summary>
        /// 包裝 GetMediaPath, 但預設回傳https的圖片網址
        /// </summary>
        /// <param name="rawPath"></param>
        /// <param name="type"></param>
        /// <param name="forceHttps"></param>
        /// <returns></returns>
        public static string GetMediaUrl(string rawPath, MediaType type, bool forceHttps = true)
        {
            if (string.IsNullOrEmpty(rawPath))
            {
                rawPath = string.Empty;
            }

            if (rawPath.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
            {
                return rawPath;
            }
            string result = GetMediaPath(rawPath, type);
            if (result.StartsWith("http://", StringComparison.OrdinalIgnoreCase))
            {
                result = "https://" + result.Substring(7);
            }
            return result;
        }

        public static string GetMediaPath(string rawPath, MediaType type)
        {
            if (!string.IsNullOrEmpty(rawPath))
            {
                // if path is an url, use it instead of parse it
                if (rawPath.StartsWith("http://", StringComparison.OrdinalIgnoreCase))
                {
                    return rawPath;
                }
                if (rawPath.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
                {
                    return rawPath;
                }

                string infix = string.Empty;
                switch (type)
                {
                    case MediaType.SellerPhotoOriginal:
                    case MediaType.ItemPhotoOriginal:
                        infix = folder[0].ToString() + "/";
                        break;

                    case MediaType.SellerPhotoSmall:
                    case MediaType.ItemPhotoSmall:
                        infix = folder[1].ToString() + "/";
                        break;

                    case MediaType.SellerPhotoLarge:
                    case MediaType.ItemPhotoLarge:
                        infix = folder[2].ToString() + "/";
                        break;

                    default:
                        break;
                }

                switch (type)
                {
                    case MediaType.ItemPhotoLarge:
                    case MediaType.ItemPhotoOriginal:
                    case MediaType.ItemPhotoSmall:
                        infix += "item/";
                        break;
                    case MediaType.PCPImage:
                        //PCP規格不一樣，所以就不走下面的return
                        return GetMediaBaseUrl() + "regulars/" + string.Join("/", rawPath.Split(','));
                    default:
                        break;
                }

                string[] fields = rawPath.Split(',');
                return GetMediaBaseUrl() + (fields.Length > 0 ? fields[0] + "/" + infix + (fields.Length > 1 ? fields[1] : string.Empty) : string.Empty);
            }
            else
            {
                return string.Empty;
            }
        }

        public static string CheckHtmlImagesBaseUrl(string htmlstring)
        {
            if (config.ImagesCDNEnabled)
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(htmlstring);

                foreach (HtmlNode node in doc.DocumentNode.Descendants())
                {
                    //修改img的路徑
                    if (node.Name.ToLower() == "img")
                    {
                        HtmlAttribute attr = node.Attributes["src"];
                        if (attr != null)
                        {
                            int position = attr.Value.IndexOf(ImageLoadBalanceFolder.ImagesU.ToString(), StringComparison.OrdinalIgnoreCase);
                            if (position > 0)
                            {
                                string imagePath = attr.Value.Substring(position);
                                Uri baseUri = new Uri(config.CDNBaseUrl.TrimEnd('/') + "/");
                                Uri imageCDN = new Uri(baseUri, ImageLoadBalanceFolder.Images.ToString() + "/" + imagePath);
                                attr.Value = imageCDN.AbsoluteUri;
                            }
                        }
                    }
                }
                return doc.DocumentNode.OuterHtml;
            }
            return htmlstring;
        }

        public static string GetUrlByHtmlString(string htmlstring)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlstring);

            foreach (HtmlNode node in doc.DocumentNode.Descendants())
            {
                if (node.Name.ToLower() == "a")
                {
                    HtmlAttribute attr = node.Attributes["href"];
                    if (attr != null)
                    {
                        return attr.Value;
                    }
                }
            }
            return string.Empty;
        }

        public static string BindingDataByHtmlUrlString(string htmlstring, string binding_data)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlstring);

            foreach (HtmlNode node in doc.DocumentNode.Descendants())
            {
                if (node.Name.ToLower() == "a")
                {
                    HtmlAttribute attr = node.Attributes["href"];
                    if (attr != null)
                    {
                        attr.Value += binding_data;
                    }
                }
            }
            return doc.DocumentNode.InnerHtml;
        }

        public static List<string> GetHiDealBigPhotos(ViewHiDeal deal, MediaType type = MediaType.HiDealBigPhotos)
        {
            List<string> photo_list = new List<string>();
            switch (type)
            {
                case MediaType.HiDealBigPhotos:
                    if (!string.IsNullOrEmpty(deal.PrimaryBigPicture))
                    {
                        photo_list.Add(GetMediaBaseUrl() + deal.PrimaryBigPicture);
                    }
                    foreach (string item in deal.Picture.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        photo_list.Add(GetMediaBaseUrl() + item);
                    }
                    break;

                case MediaType.HiDealSecondaryBigPhoto:
                    if (!string.IsNullOrEmpty(deal.Picture))
                    {
                        foreach (string item in deal.Picture.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            photo_list.Add(GetMediaBaseUrl() + item);
                        }
                    }
                    break;
            }
            return photo_list;
        }

        public static List<string> GetHiDealBigPhotos(HiDealDeal deal, MediaType type = MediaType.HiDealBigPhotos)
        {
            List<string> photo_list = new List<string>();
            switch (type)
            {
                case MediaType.HiDealBigPhotos:
                    if (!string.IsNullOrEmpty(deal.PrimaryBigPicture))
                    {
                        photo_list.Add(GetMediaBaseUrl() + deal.PrimaryBigPicture);
                    }
                    if (!string.IsNullOrEmpty(deal.Picture))
                    {
                        foreach (string item in deal.Picture.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            photo_list.Add(GetMediaBaseUrl() + item);
                        }
                    }
                    break;

                case MediaType.HiDealSecondaryBigPhoto:
                    if (!string.IsNullOrEmpty(deal.Picture))
                    {
                        foreach (string item in deal.Picture.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            photo_list.Add(GetMediaBaseUrl() + item);
                        }
                    }
                    break;
            }
            return photo_list;
        }

        public static string GetHiDealPhoto(ViewHiDeal deal, MediaType type, bool usedefaultIfEmpty = false)
        {
            string photo = usedefaultIfEmpty ? (type == MediaType.HiDealPrimaryBigPhoto ? "../Themes/default/images/hidealsample.JPG" : "../Themes/default/images/hidealsample.JPG") : string.Empty;
            switch (type)
            {
                case MediaType.HiDealBigPhotos:
                case MediaType.HiDealPrimaryBigPhoto:
                    if (!string.IsNullOrEmpty(deal.PrimaryBigPicture))
                    {
                        photo = GetMediaBaseUrl() + deal.PrimaryBigPicture;
                    }
                    break;

                case MediaType.HiDealPrimarySmallPhoto:
                    if (!string.IsNullOrEmpty(deal.PrimarySmallPicture))
                    {
                        photo = GetMediaBaseUrl() + deal.PrimarySmallPicture;
                    }
                    break;

                case MediaType.HiDealSecondaryBigPhoto:
                    var pictures = deal.Picture.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    if (pictures.Length > 0)
                    {
                        photo = GetMediaBaseUrl() + pictures[0];
                    }
                    break;
            }
            return photo;
        }

        public static string GetHiDealPhoto(HiDealDeal deal, MediaType type, bool usedefaultIfEmpty = false)
        {
            string photo = usedefaultIfEmpty ? (type == MediaType.HiDealPrimaryBigPhoto ? "../Themes/default/images/hidealsample.JPG" : "../Themes/default/images/hidealsample.JPG") : string.Empty;
            switch (type)
            {
                case MediaType.HiDealBigPhotos:
                case MediaType.HiDealPrimaryBigPhoto:
                    if (!string.IsNullOrEmpty(deal.PrimaryBigPicture))
                    {
                        photo = GetMediaBaseUrl() + deal.PrimaryBigPicture;
                    }
                    break;

                case MediaType.HiDealPrimarySmallPhoto:
                    if (!string.IsNullOrEmpty(deal.PrimarySmallPicture))
                    {
                        photo = GetMediaBaseUrl() + deal.PrimarySmallPicture;
                    }
                    break;

                case MediaType.HiDealSecondaryBigPhoto:
                    if (!string.IsNullOrEmpty(deal.Picture))
                    {
                        var pictures = deal.Picture.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        if (pictures.Length > 0)
                        {
                            photo = GetMediaBaseUrl() + pictures[0];
                        }
                    }
                    break;
            }
            return photo;
        }

        public static string GetNoImagePath(MediaType type, DepartmentTypes d)
        {
            switch (d)
            {
                case DepartmentTypes.Delicacies:
                    return config.MediaBaseUrl + "noseller/D_" + type.ToString() + ".gif";

                default:
                    return config.MediaBaseUrl + "noseller/" + type.ToString() + ".jpg";
            }
        }

        public static string GenerateMediaPath(string head, params string[] element)
        {
            string ret = (head != null) ? head.Trim() : string.Empty;
            if (element != null)
            {
                foreach (string s in element)
                {
                    ret += "," + (s != null ? s.Trim() : string.Empty);
                }
            }

            return ret;
        }

        public static string[] GetMediaPathsFromRawData(string rawData, MediaType type)
        {
            string[] paths = Helper.GetRawPathsFromRawData(rawData);
            for (int i = 0; i < paths.Length; i++)
            {
                paths[i] = GetMediaPath(paths[i], type);
            }

            return paths;
        }

        public static string GetEventImageUrl(string img)
        {
            string[] bannerimagePaths = GetMediaPathsFromRawData(img, MediaType.EventPromo);
            return (bannerimagePaths == null || bannerimagePaths.Length == 0)
                ? string.Empty
                : bannerimagePaths[0];
        }
        public static string GetDealPic(IViewPponDeal deal)
        {
            string picUr = GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).FirstOrDefault();
            return picUr;
        }
        /// <summary>
        /// 取得M版用的方型檔次主圖
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="notFoundTryPlanB"></param>
        /// <returns></returns>
        public static string GetDealAppPic(IViewPponDeal deal, bool notFoundTryPlanB)
        {
            if (string.IsNullOrEmpty(deal.AppDealPic) == false)
            {
                string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(deal.AppDealPic, MediaType.PponDealPhoto);
                if (imgPaths != null && imgPaths.Length > 0)
                {
                    return imgPaths[0];
                }                
            }
            if (notFoundTryPlanB && string.IsNullOrEmpty(deal.EventImagePath) == false)
            {
                string[] imgPaths = GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto);
                if (imgPaths != null && imgPaths.Length > 0)
                {
                    return imgPaths[0];
                }
            }
            return NoAppPic;
        }

        public static string GetEventMobileImageUrl(string img)
        {
            if (string.IsNullOrEmpty(img) || string.IsNullOrWhiteSpace(img))
            {
                return string.Empty;
            }

            var startString = "/Images/";
            var endString = "\" /";

            var startIdx = img.IndexOf(startString, StringComparison.Ordinal) + startString.Length;
            var endIdx = img.IndexOf(endString, StringComparison.Ordinal);

            //排除不合理的狀況
            if (startIdx > endIdx || startIdx > 100)
            {
                return string.Empty;
            }

            var imgFileName = img.Substring(startIdx, endIdx - startIdx);

            if (!string.IsNullOrEmpty(imgFileName))
            {
                return config.SiteUrl + startString + imgFileName;
            }

            return string.Empty;
        }

        /// <summary>
        /// 取Html第一張圖的src
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public static string GetHtmlFirstSrc(string img)
        {
            if (string.IsNullOrEmpty(img) || string.IsNullOrWhiteSpace(img))
            {
                return string.Empty;
            }

            var startString = "src=\"";
            var endString = "\"";

            var startIdx = img.IndexOf(startString, StringComparison.Ordinal) + startString.Length;
            // 取src="之後的所有字串，找第一個"號的位置
            var endIdx = img.Substring(startIdx, img.Length - startIdx) 
                .IndexOf(endString, StringComparison.Ordinal);

            //排除不合理的狀況
            if (endIdx < 0)
            {
                return string.Empty;
            }

            var imgFileName = img.Substring(startIdx, endIdx);

            if (!string.IsNullOrEmpty(imgFileName))
            {
                return imgFileName;
            }

            return string.Empty;
        }

        public static string CreateDirectory(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            return directoryPath;
        }

        public static void UploadToSeller(IHttpPostedFileAdapter pFile, string baseDirectoryPath, string destAppendingDirPath, string fileName, Image imgData = null)
        {
            foreach (string item in folder)
            {
                CreateDirectory(Path.Combine(baseDirectoryPath, destAppendingDirPath, item));
                if (pFile != null)
                {
                    pFile.SaveAs(fileName.Replace(destAppendingDirPath, Path.Combine(destAppendingDirPath, item)));
                }
                else if (imgData != null)
                {
                    imgData.Save(fileName.Replace(destAppendingDirPath, Path.Combine(destAppendingDirPath, item)));
                }
            }
        }

        // TODO: needs rewrite
        public static void ConvertImage(string tempFilePath, string filePath, string destFileName, UploadFileType type)
        {
            System.Drawing.Size size = System.Drawing.Size.Empty;

            string dirSuffix = @"\";
            string[] subfolder = type.Equals(UploadFileType.SelectedStore) ? new string[1] : folder;

            for (int i = 0; i < subfolder.Length; i++)
            {
                switch (type)
                {
                    case UploadFileType.SellerPhoto:
                        switch (i)
                        {
                            case 1:
                                size = new System.Drawing.Size(121, 89);
                                break;

                            case 2:
                                size = new System.Drawing.Size(151, 112);
                                break;
                        }
                        dirSuffix = folder[i].ToString() + @"\";
                        break;

                    case UploadFileType.DelicaciesSellerPhoto:
                        switch (i)
                        {
                            case 0:
                                size = new System.Drawing.Size(320, 200);
                                break;

                            case 1:
                                size = new System.Drawing.Size(210, 70);
                                break;

                            case 2:
                                size = new System.Drawing.Size(224, 140);
                                break;
                        }
                        dirSuffix = folder[i].ToString() + @"\";
                        break;

                    case UploadFileType.ItemPhoto:
                        switch (i)
                        {
                            case 0:
                                size = new System.Drawing.Size(600, 400);
                                break;

                            case 1:
                                size = new System.Drawing.Size(80, 60);
                                break;

                            case 2:
                                size = new System.Drawing.Size(150, 99);
                                break;
                        }
                        dirSuffix = folder[i].ToString() + @"\item\";
                        break;

                    case UploadFileType.DelicaciesItemPhoto:
                        switch (i)
                        {
                            case 0:
                                size = new System.Drawing.Size(408, 255);
                                break;

                            case 1:
                                size = new System.Drawing.Size(96, 60);
                                break;

                            case 2:
                                size = new System.Drawing.Size(192, 120);
                                break;
                        }
                        dirSuffix = folder[i].ToString() + @"\item\";
                        break;

                    case UploadFileType.CityPhoto:
                        switch (i)
                        {
                            case 0:
                                size = new System.Drawing.Size(152, 71);
                                break;
                        }
                        break;

                    case UploadFileType.SelectedStore:
                        size = new System.Drawing.Size(454, 0);
                        dirSuffix = string.Empty;
                        break;
                }

                CreateDirectory(filePath + @"\" + dirSuffix);

                object[] param;
                if (size != System.Drawing.Size.Empty)
                {
                    param = new object[5];
                    param[0] = "-resize";
                    param[1] = size.Width + (size.Height != 0 ? ("x" + size.Height + "!") : string.Empty);
                    param[2] = "-strip";
                    param[3] = tempFilePath;
                    param[4] = filePath + @"\" + dirSuffix + destFileName;
                }
                else
                {
                    param = new object[3];
                    param[0] = "-strip";
                    param[1] = tempFilePath;
                    param[2] = filePath + @"\" + dirSuffix + destFileName;
                }

                ImageMagickObject.MagickImageClass img = new ImageMagickObject.MagickImageClass();
                img.Convert(param);
            }
        }

        /// <summary>
        /// 大圖輪播壓Logo
        /// </summary>
        /// <param name="pFile"></param>
        /// <param name="savePath"></param>
        /// <param name="imgData"></param>
        public static void PrintWatermark(IHttpPostedFileAdapter pFile, string savePath, Image imgData = null)
        {
            Image backgroundImage = (pFile != null) ? Image.FromStream(pFile.InputStream) : imgData;
            PrintLogo(ref backgroundImage);
            string contentType = "";
            if (imgData != null)
            {
                contentType = ImageCodecInfo.GetImageDecoders().First(c => c.FormatID == backgroundImage.RawFormat.Guid).MimeType;
            }
            else
            {
                contentType = pFile.ContentType;
            }
            ImageCodecInfo jgpEncoder = GetImageCodecInfo(contentType);
            Encoder encoder = Encoder.Quality;
            EncoderParameter encoderParameter = new EncoderParameter(encoder, 90L);
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = encoderParameter;

            backgroundImage.Save(savePath, jgpEncoder, encoderParameters);
            backgroundImage.Dispose();
        }

        public static void PrintLogo(ref Image backgroundImage)
        {
            string logoFileName = "DealLOGO_WM.png";
            string watermarkImagePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Images/"), logoFileName);
            Image watermarkImage = Image.FromFile(watermarkImagePath);

            float dpiX = backgroundImage.HorizontalResolution;
            float dpiY = backgroundImage.VerticalResolution;

            if (dpiX > 96 || dpiY > 96)
            {

                Bitmap bmp = new Bitmap(backgroundImage);
                bmp.SetResolution(96, 96);
                backgroundImage = bmp;
            }
            if (backgroundImage.PixelFormat == PixelFormat.Indexed ||
                backgroundImage.PixelFormat == PixelFormat.Format1bppIndexed ||
                backgroundImage.PixelFormat == PixelFormat.Format4bppIndexed ||
                backgroundImage.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                //為了解決「無法從具備索引像素格式的影像中建立圖形物件 (Graphic Object)。」的問題，所以要Clone一個改成24bppRgb
                Bitmap tempBmp = new Bitmap(backgroundImage);
                backgroundImage = tempBmp.Clone(new Rectangle(0, 0, tempBmp.Width, tempBmp.Height), PixelFormat.Format24bppRgb);
            }
            
            //設定背景圖片
            Graphics gr = System.Drawing.Graphics.FromImage(backgroundImage);

            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            gr.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;

            //logo圖片位置
            ImageAttributes imgAttributes = new ImageAttributes();
            ColorMatrix cmatrix = new ColorMatrix();
            imgAttributes.SetColorMatrix(cmatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            gr.DrawImage(watermarkImage, new Rectangle(backgroundImage.Width - watermarkImage.Width, 0, watermarkImage.Width, watermarkImage.Height), 0, 0, watermarkImage.Width, watermarkImage.Height, GraphicsUnit.Pixel, imgAttributes);
            gr.Dispose();

        }

        public static ImageCodecInfo GetImageCodecInfo(string contentType)
        {
            // 設定圖檔類型
            ImageFormat format = GetImageFormat(contentType);
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        public static EncoderParameters GetEncoderParameters(long compresssLevel = 90L)
        {
            Encoder encoder = Encoder.Quality;
            EncoderParameter encoderParameter = new EncoderParameter(encoder, compresssLevel);
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = encoderParameter;
            return encoderParameters;
        }

        public static ImageFormat GetImageFormat(string contentType)
        {
            ImageFormat format = ImageFormat.Jpeg;
            switch (contentType)
            {
                case "image/jpeg":
                default:
                    format = ImageFormat.Jpeg;
                    break;

                case "image/png":
                    format = ImageFormat.Png;
                    break;

                case "image/gif":
                    format = ImageFormat.Gif;
                    break;
            }
            return format;
        }

        public static Image ResizeImage(Image image, Size size, bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }

        /// <summary>
        /// 指定長寬裁剪
        /// 按模版比例最大範圍的裁剪圖片並缩放至模版尺寸
        /// </summary>
        /// <param name="fromFile">原圖</param>
        /// <param name="maxWidth">最大寬(單位:px)</param>
        /// <param name="maxHeight">最大高(單位:px)</param>
        /// <param name="quality">質量（範圍0-100）</param>
        /// <param name="isCutCenter">是否裁取置中圖片</param>
        public static Image CutForCustom(Image initImage, int maxWidth, int maxHeight, int quality, bool isCutCenter)
        {
            //建立模板
            System.Drawing.Image templateImage = new System.Drawing.Bitmap(maxWidth, maxHeight);

            //模版的寬高比例
            double templateRate = (double)maxWidth / maxHeight;
            //原圖片的寬高比例
            double initRate = (double)initImage.Width / initImage.Height;

            //原圖與模版比例相等，直接缩放
            if (templateRate == initRate)
            {
                //按模版大小生成最终圖片
                System.Drawing.Graphics templateG = System.Drawing.Graphics.FromImage(templateImage);
                templateG.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                templateG.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                templateG.Clear(Color.White);
                templateG.DrawImage(initImage, new System.Drawing.Rectangle(0, 0, maxWidth, maxHeight), new System.Drawing.Rectangle(0, 0, initImage.Width, initImage.Height), System.Drawing.GraphicsUnit.Pixel);

                initImage.Dispose();
                templateG.Dispose();
            }
            else //原圖與模版比例不等，裁剪後缩放
            {
                //裁剪對象
                System.Drawing.Image pickedImage = null;
                System.Drawing.Graphics pickedG = null;

                //定位
                Rectangle fromR = new Rectangle(0, 0, 0, 0);//原圖裁剪定位
                Rectangle toR = new Rectangle(0, 0, 0, 0);//目標定位

                //寬為標轉進行裁剪
                if (templateRate > initRate)
                {
                    //裁剪對象實例化
                    pickedImage = new System.Drawing.Bitmap(initImage.Width, (int)System.Math.Floor(initImage.Width / templateRate));
                    pickedG = System.Drawing.Graphics.FromImage(pickedImage);

                    //裁剪源定位
                    fromR.X = 0;
                    fromR.Y = (int)System.Math.Floor((initImage.Height - initImage.Width / templateRate) / 2);
                    fromR.Width = initImage.Width;
                    fromR.Height = (int)System.Math.Floor(initImage.Width / templateRate);

                    //裁剪目標定位
                    toR.X = 0;
                    toR.Y = 0;
                    toR.Width = initImage.Width;
                    toR.Height = (int)System.Math.Floor(initImage.Width / templateRate);
                }
                //高為標準進行剪
                else
                {
                    pickedImage = new System.Drawing.Bitmap((int)System.Math.Floor(initImage.Height * templateRate), initImage.Height);
                    pickedG = System.Drawing.Graphics.FromImage(pickedImage);

                    // 裁取置中圖片
                    if (isCutCenter)
                    {
                        fromR.X = (int)System.Math.Floor((initImage.Width - initImage.Height * templateRate) / 2);
                    }
                    else
                    {
                        fromR.X = 0; // 裁取靠左圖片
                    }
                    fromR.Y = 0;
                    fromR.Width = (int)System.Math.Floor(initImage.Height * templateRate);
                    fromR.Height = initImage.Height;

                    toR.X = 0;
                    toR.Y = 0;
                    toR.Width = (int)System.Math.Floor(initImage.Height * templateRate);
                    toR.Height = initImage.Height;
                }

                //設置质量
                pickedG.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                pickedG.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                //裁剪
                pickedG.DrawImage(initImage, toR, fromR, System.Drawing.GraphicsUnit.Pixel);

                //按模版大小生成最终圖片
                System.Drawing.Graphics templateG = System.Drawing.Graphics.FromImage(templateImage);
                templateG.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                templateG.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                templateG.Clear(Color.White);
                templateG.DrawImage(pickedImage, new System.Drawing.Rectangle(0, 0, maxWidth, maxHeight), new System.Drawing.Rectangle(0, 0, pickedImage.Width, pickedImage.Height), System.Drawing.GraphicsUnit.Pixel);

                //質量控制
                //獲取系统編碼類型數組,包含了jpeg,bmp,png,gif,tiff
                ImageCodecInfo[] icis = ImageCodecInfo.GetImageEncoders();
                ImageCodecInfo ici = null;
                foreach (ImageCodecInfo i in icis)
                {
                    if (i.MimeType == "image/jpeg" || i.MimeType == "image/bmp" || i.MimeType == "image/png" || i.MimeType == "image/gif")
                    {
                        ici = i;
                    }
                }
                EncoderParameters ep = new EncoderParameters(1);
                ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)quality);

                //釋放資源
                initImage.Dispose();
                templateG.Dispose();

                pickedG.Dispose();
                pickedImage.Dispose();
            }

            return templateImage;
        }

        /// <summary>
        /// 指定截切正方型圖
        /// </summary>
        /// <param name="initImage">原圖</param>
        /// <param name="maxLength">最大寬與高(單位:px)</param>        
        /// <param name="quality">質量（範圍0-100）</param>
        public static Image CropSquare(Image initImage, int maxLength, int quality)
        {
            if (maxLength > initImage.Width && initImage.Width >= initImage.Height)
            {
                maxLength = initImage.Height;
            }
            if (maxLength > initImage.Height && initImage.Height >= initImage.Width)
            {
                maxLength = initImage.Width;
            }
            return CutForCustom(initImage, maxLength, maxLength, quality, true);
        }

        /// <summary>
        /// 圖檔壓縮
        /// </summary>
        /// <param name="img">原圖</param>
        /// <param name="compressRate">縮小倍率</param>
        /// <param name="compressedMs">傳入一個空串流</param>
        /// <returns>壓縮圖</returns>
        public static Image CompressImage(Image img, double compressRate, MemoryStream compressedMs)
        {
            var contentType = ImageCodecInfo.GetImageDecoders().First(c => c.FormatID == img.RawFormat.Guid).MimeType;
            var compressedImg = CutForCustom(img, (int)(img.Width * compressRate), (int)(img.Height * compressRate), 100, true);
            ImageCodecInfo jgpEncoder = GetImageCodecInfo(contentType);
            Encoder encoder = Encoder.Quality;
            EncoderParameter encoderParameter = new EncoderParameter(encoder, 100L);
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = encoderParameter;
            compressedImg.Save(compressedMs, jgpEncoder, encoderParameters);
            compressedImg = Image.FromStream(compressedMs);
            return compressedImg;
        }

        public static bool DownloadImage(Uri url, out byte[] data)
        {
            using (var client = new WebClient())
            {
                try
                {
                    data = client.DownloadData(url);
                }
                catch (Exception)
                {
                    data = null;
                    return false;
                }
            }

            return data != null && data.Length != 0;
        }

        public static Dictionary<int, double> GetPcpImageCompressRate()
        {
            Dictionary<int, double> compressRate = new Dictionary<int, double>();
            var imageTypeList = config.PcpCompressImageTypeList.Split(',');
            foreach (var tempData in imageTypeList.Select(item => item.Split('|')).Where(tempData => tempData.Length == 2))
            {
                int tempType;
                double tempRate;
                if (int.TryParse(tempData[0], out tempType) && double.TryParse(tempData[1], out tempRate))
                {
                    compressRate.Add(tempType, tempRate);
                }
            }
            return compressRate;
        }

        public static void DeleteCDNImage(string filePath)
        {
            if (config.MediaCDNEnabled || config.ImagesCDNEnabled)
            {
                string url = string.Format("{0}{1}{2}", config.MASCDNAPIUrl, config.MASCDNSlug, filePath);
                var auth = string.Format("Token {0}", config.MASCDNAPIKey);
                StartWebRequest(url, "DELETE", auth);
            }
        }

        public static void StartWebRequest(string url, string method, string auth)
        {
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = method;
            req.Accept = "application/json";
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", auth);
            req.BeginGetResponse(new AsyncCallback(FinishWebRequest), req);
        }

        public static void FinishWebRequest(IAsyncResult result)
        {
            try
            {
                using (HttpWebResponse response = (result.AsyncState as HttpWebRequest).EndGetResponse(result) as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        logger.WarnFormat("CDN圖檔刪除失敗，StatusCode:{0}；Uri:{1}", response.StatusCode, response.ResponseUri.AbsoluteUri);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("CDN圖檔刪除出現錯誤，Uri:{0}；ex:{1}", ((HttpWebRequest)result.AsyncState).RequestUri, ex.Message);
            }
        }

        public static Image CropImage(Image image, Rectangle cropArea)
        {
            Bitmap bmp = image as Bitmap;

            // Check if it is a bitmap:
            if (bmp == null)
            {
                throw new ArgumentException("No valid bitmap");
            }

            // Crop the image:
            Bitmap cropBmp = bmp.Clone(cropArea, bmp.PixelFormat);

            // Release the resources:
            image.Dispose();

            return cropBmp;
        }

        public static void CropImage(string srcFileName, string destFileName, Rectangle cropArea)
        {
            ImageCodecInfo jgpEncoder = GetImageCodecInfo("image/jpeg");
            Encoder encoder = Encoder.Quality;
            EncoderParameter encoderParameter = new EncoderParameter(encoder, 90L);
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = encoderParameter;

            CropImage(Image.FromFile(srcFileName), cropArea)
                .Save(destFileName, jgpEncoder, encoderParameters);
        }

        public static Point GetImageSizeFast(string fileName)
        {
            using (Stream stream = File.OpenRead(fileName))
            {
                using (Image sourceImage = Image.FromStream(stream, false, false))
                {
                    return new Point(sourceImage.Width, sourceImage.Height);
                }
            }
        }

        public static void CopyFile(UploadFileType type, string fromPath, string fromFileName, string toPath, string toFileName)
        {
            //去版號(移除?ts=XXXXXXXX)
            fromFileName = fromFileName.Contains("?") ? fromFileName.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries)[0] : fromFileName;
            toFileName = toFileName.Contains("?") ? toFileName.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries)[0] : toFileName;

            string fromfilePathName = string.Empty;
            string tofilePathName = string.Empty;
            string tofilePath = string.Empty;

            switch (type)
            {
                case UploadFileType.PponEvent:
                    fromfilePathName = Path.Combine(HttpContext.Current.Server.MapPath("~/media/"), Path.Combine(fromPath, fromFileName));
                    tofilePathName = Path.Combine(HttpContext.Current.Server.MapPath("~/media/"), Path.Combine(toPath, toFileName));
                    tofilePath = Path.Combine(HttpContext.Current.Server.MapPath("~/media/"), toPath);
                    break;

                case UploadFileType.HiDealPrimarySmallPhoto:
                    fromfilePathName = HttpContext.Current.Server.MapPath("~/media/" + fromPath) + fromFileName;
                    tofilePathName = HttpContext.Current.Server.MapPath("~/media/" + toPath) + toFileName;
                    tofilePath = HttpContext.Current.Server.MapPath("~/media/" + toPath);
                    break;

                case UploadFileType.HiDealPrimaryBigPhoto:
                    fromfilePathName = HttpContext.Current.Server.MapPath("~/media/" + fromPath) + fromFileName;
                    tofilePathName = HttpContext.Current.Server.MapPath("~/media/" + toPath) + toFileName;
                    tofilePath = HttpContext.Current.Server.MapPath("~/media/" + toPath);
                    break;

                case UploadFileType.YahooBigImage:
                    fromfilePathName = Path.Combine(HttpContext.Current.Server.MapPath("~/media/"), Path.Combine(fromPath, fromFileName));
                    tofilePathName = Path.Combine(HttpContext.Current.Server.MapPath("~/media/"), Path.Combine(toPath, toFileName));
                    tofilePath = Path.Combine(HttpContext.Current.Server.MapPath("~/media/"), toPath);
                    break;
            }
            if (File.Exists(fromfilePathName))
            {
                if (!Directory.Exists(tofilePath))
                {
                    Directory.CreateDirectory(tofilePath);
                }
                File.Copy(fromfilePathName, tofilePathName, true);
            }
            CopyFileUNC(fromPath, fromFileName, toPath, toFileName);
        }

        /// <summary>
        /// 複製檔案loadbalance
        /// </summary>
        /// <param name="fromPath">來源路徑</param>
        /// <param name="fromFileName">來源檔名</param>
        /// <param name="toPath">目的路徑</param>
        /// <param name="toFileName">目的檔名</param>
        private static void CopyFileUNC(string fromPath, string fromFileName, string toPath, string toFileName)
        {
            if (config.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            string fromfilePathName = string.Empty;
                            string tofilePathName = string.Empty;
                            string tofilePath = string.Empty;
                            string basePath = Path.Combine(account.UNCPath, ImageLoadBalanceFolder.Media.ToString());

                            fromfilePathName = Path.Combine(basePath, Path.Combine(fromPath, fromFileName));
                            tofilePathName = Path.Combine(basePath, Path.Combine(toPath, toFileName));
                            tofilePath = Path.Combine(basePath, toPath);

                            if (File.Exists(fromfilePathName))
                            {
                                if (!Directory.Exists(tofilePath))
                                {
                                    Directory.CreateDirectory(tofilePath);
                                }
                                File.Copy(fromfilePathName, tofilePathName, true);
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        logger.ErrorFormat("圖片同步出錯，CopyFile，目前Server:{0}，{1}", Dns.GetHostName(), error.Message);
                    }
                }
            }
        }


        #region member pic
        //之後如果要切到CDN，可能從這幾隻method著手

        public static string GetMemberPicRootPath()
        {
            return Helper.MapPath("~/images/members/");
        }

        public static string GetMemberPicBaseUrl()
        {
            return Helper.CombineUrl(config.SiteUrl,  "images/members/");
        }

        /// <summary>
        /// 取得會員的圖片目錄
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string GetMemberPicFolder(int userId)
        {
            if (userId == 0)
            {
                return string.Empty;
            }
            //1111111111是會員編號起始的數字
            return ((userId - 1111111110) / 10000).ToString("0000");
        }
        #endregion

        
        /// <summary>
        /// 蓋板廣告處理圖片
        /// </summary>
        public class EventActivityImageProcessor
        {
            public const string EventActivityImageUrl = "/images/eventActiviry/";
            private static string GetEventActivityFolder()
            {
                string folder = Helper.MapPath("~/images/eventActiviry/");
                if (Directory.Exists(folder) == false)
                {
                    Directory.CreateDirectory(folder);
                }
                return folder;
            }
            public static void Upload(IHttpPostedFileAdapter pFile, string fileName, out string cacheBustingFileName)
            {
                cacheBustingFileName = string.Empty;
                string extension = Path.GetExtension(pFile.FileName);
                string filePath = Path.Combine(GetEventActivityFolder(), fileName + extension);                
                pFile.SaveAs(filePath);
                cacheBustingFileName = string.Format("{0}{1}?ts={2}", fileName, extension, DateTime.Now.Ticks);
            }
            public static void Delete(string fileName)
            {                
                string filePath = Path.Combine(GetEventActivityFolder(), Helper.GetFileNameWithoutCacheBusting(fileName));
                
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
            }
        }


        #region CustomerService圖片上傳

        public static void UploadBtyeToImageWithResize(byte[] image, ImageFormat format, string dirPath, string folder, string fileName, int width = 0,bool small=true)
        {
            UploadBtyeToImage(image, ImageFormat.Png, dirPath, folder, fileName);
            if (width != 0)
            {
                ResizeImageForRatio(dirPath, folder, fileName, width,small);
            }
        }

        /// <summary>
        /// 上傳檔案(使用byte)
        /// </summary>
        /// <param name="image">base64string轉成btye[]</param>
        /// <param name="format">轉出的圖片格式</param>
        /// <param name="baseDirectoryPath">相對路徑(此路徑不含個別檔案夾)</param>
        /// <param name="folder">檔案夾</param>
        /// <param name="fileName">檔名</param>
        public static void UploadBtyeToImage(byte[] image, ImageFormat format, string baseDirectoryPath, string folder, string fileName)
        {
            string path = Path.Combine(baseDirectoryPath, folder);
            string fullpath = Path.Combine(path, fileName);
            ImageFacade.CreateDirectory(Path.Combine(path));

            MemoryStream ms = new MemoryStream(image);
            Bitmap bmp = new Bitmap(ms);
            bmp.Save(fullpath, format);
        }


        /// <summary>
        /// 縮放圖片(等比例縮放)
        /// </summary>
        /// <param name="folder">資料夾</param>
        /// <param name="dirPath">相對位置</param>
        /// <param name="fileName">檔案名稱</param>
        /// <param name="width"></param>
        private static void ResizeImageForRatio(string dirPath, string folder, string fileName, int width,bool small)
        {
            string path = Path.Combine(Path.Combine(dirPath, folder), fileName);
            System.Drawing.Image image = System.Drawing.Image.FromFile(path);
            int iwidth = image.Width;
            int iheight = image.Height;
            if (iwidth > width)
            {
                System.Drawing.Image resizeimage = ImageFacade.ResizeImage(image, new System.Drawing.Size(width, iheight * width / iwidth), true);
                image.Dispose();
                resizeimage.Save(path);
                resizeimage.Dispose();
            }
            else
            {
                image.Dispose();
            }
            if (small)
            {
                string targetPath = Path.Combine(Path.Combine(dirPath, folder), "Small");
                string smallPath = Path.Combine(Path.Combine(Path.Combine(dirPath, folder), "Small"), fileName);
                ImageFacade.CreateDirectory(targetPath);
                File.Copy(path, smallPath);
                System.Drawing.Image smallImage = System.Drawing.Image.FromFile(smallPath);
                int swidth = smallImage.Width;
                int sheight = smallImage.Height;
                int changewidth = 320;

                if (swidth > changewidth)
                {
                    System.Drawing.Image smallresizeimage = ImageFacade.ResizeImage(smallImage, new System.Drawing.Size(changewidth, sheight * changewidth / swidth), true);

                    smallImage.Dispose();
                    smallresizeimage.Save(smallPath);
                    smallresizeimage.Dispose();
                }
            }
        }

        #endregion 
    }
}
﻿using System.Linq;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Models.CustomerService;
using System.Collections.Generic;
using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using LunchKingSite.BizLogic.Component.API;
using System.Web;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.BizLogic.Component;
using System.Net.Mail;
using System.Reflection;
using LunchKingSite.Core.Component.Template;

namespace LunchKingSite.BizLogic.Facade
{
    public class CustomerServiceFacade
    {
        private static readonly LunchKingSite.Core.IServiceProvider sp;
        private static readonly ISellerProvider ssp;
        private static readonly IMemberProvider mp;
        private static readonly IHumanProvider hp;
        private static readonly ISysConfProvider conf;
        private static readonly ILog log;
        private static readonly IPponProvider _pponProvider;
        private static readonly IOrderProvider op;
        private static int pageRow = 50;

        static CustomerServiceFacade()
        {
            sp = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();
            ssp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            conf = ProviderFactory.Instance().GetConfig();
            log = LogManager.GetLogger(typeof(CustomerServiceFacade));
            _pponProvider = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        #region API

        /// <summary>
        /// 取得首頁新資料提示
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static List<NewDetail> GetNtfyValuesCount(int userId)
        {
            int csolCount = sp.GetCustomerServiceOutsideLogCountByuserId(userId);

            List<NewDetail> nDetail = new List<NewDetail>();

            if (csolCount > 0)
            {
                nDetail.Add(new NewDetail()
                {
                    Category = "Service",
                    Count = csolCount
                });

            }

            return nDetail;

        }

        /// <summary>
        /// 取得問題類別列表
        /// </summary>
        /// <returns></returns>
        public static CategoryData GetCategoryList()
        {
            CategoryData data = new CategoryData();
            data.Category = new List<ParentCategory>();

            CustomerServiceCategoryCollection parentCategory = sp.GetCustomerServiceCategoryForFrontEnd();
            foreach (var item in parentCategory.Where(p => p.IsNeedOrderId == false).ToList())
            {
                data.Category.Add(new ParentCategory()
                {
                    ParentId = item.CategoryId,
                    ParentName = item.CategoryName,
                });
            }

            return data;
        }

        /// <summary>
        /// 取得前台顯示問題分類
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static CategoryData GetCategoryList(int? categoryId)
        {
            CategoryData data = new CategoryData();
            data.Category = new List<ParentCategory>();

            var parentCategory = sp.GetCustomerServiceCategoryListByCategoryId(categoryId).ToList();
            foreach (var item in parentCategory)
            {
                //查詢主分類時排除訂單問題
                if ((categoryId == 0 || categoryId == null) && item.IsNeedOrderId)
                {
                    continue;
                }

                if (!item.IsShowFrontEnd)
                {
                    continue;
                }

                data.Category.Add(new ParentCategory()
                {
                    ParentId = item.CategoryId,
                    ParentName = item.CategoryName,
                });
            }
            return data;
        }

        /// <summary>
        /// 取得客服對話ByServiceNo (包含接續已結案的對話)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="serviceNo"></param>
        /// <param name="pageCnt">分頁功能，預設為1;(一頁抓50筆)</param>
        /// <param name="isGetAll">false:使用分頁抓取;true:一次性全抓;</param>
        /// <returns></returns>
        public static CommunicateData GetCommunicateListByServiceNo(int userId, string serviceNo, bool isGetAll = false, int pageCnt = 1)
        {
            //案件查詢
            ViewCustomerServiceList vcsm = sp.GetViewCustomerServiceMessageByServiceNo(serviceNo);

            if (!vcsm.IsLoaded)
            {
                return new CommunicateData();
            }

            //對話記錄
            ViewCustomerServiceOutsideLogCollection csol = sp.GetCustomerServiceOutsideLogByuserIdAndParentServiceNo(userId, vcsm.ParentServiceNo);

            var details = GetCommunicateDetails(csol, pageCnt, isGetAll);

            var data = new CommunicateData();

            double total = csol.Count;

            data.TotalPage = Convert.ToInt16(Math.Ceiling(total / pageRow));
            data.Status = vcsm.CustomerServiceStatus;
            data.CategoryId = vcsm.Category;
            data.CategoryName = vcsm.MainCategoryName;
            data.Communicate.AddRange(details);

            SetOutSideLogIsReadToYes(csol);

            return data;
        }
        /// <summary>
        /// 取得尚未讀取的客服訊息並回寫讀取狀態
        /// </summary>
        /// <param name="csol">ViewCustomerServiceOutsideLogCollection</param>
        private static void SetOutSideLogIsReadToYes(ViewCustomerServiceOutsideLogCollection csol)
        {

            var csolNotRead = csol.Where(p => p.IsRead == (int)IsRead.No);
            foreach (var item in csolNotRead)
            {
                CustomerServiceOutsideLog cso = sp.CustomerServiceOutsideLogGet(item.Id);
                cso.IsRead = (int)IsRead.Yes;
                sp.CustomerServiceOutsideLogSet(cso);
            }

        }

        /// <summary>
        /// 處理OutSideLog返回CommunicateDetails
        /// </summary>
        /// <param name="csol">ViewCustomerServiceOutsideLogCollection</param>
        /// <param name="pageCnt">分頁功能，預設為1;(一頁抓50筆)</param>
        /// <param name="isGetAll">false:使用分頁抓取;true:一次性全抓;</param>
        /// <returns></returns>
        private static List<CommunicateDetail> GetCommunicateDetails(ViewCustomerServiceOutsideLogCollection csol, int pageCnt, bool isGetAll)
        {
            List<CommunicateDetail> details = new List<CommunicateDetail>();

            if (csol.Any())
            {
                //一次取全部
                if (isGetAll)
                {
                    #region 一次取全部

                    foreach (var item in csol.OrderBy(p => p.CreateTime))
                    {
                        string smallUrl = item.File == null
                            ? ""
                            : conf.SiteUrl + "/Images/Service/OutSide/" + item.ServiceNo + "/Small/" + item.File;
                        int width = 0;
                        int height = 0;
                        if (!string.IsNullOrEmpty(smallUrl))
                        {
                            string path = Helper.MapPath(conf.OutSideUrl);
                            string realPath = Path.Combine(
                                Path.Combine(Path.Combine(path, item.ServiceNo), "Small"), item.File ?? string.Empty);
                            if (File.Exists(realPath))
                            {
                                System.Drawing.Image smallImage = System.Drawing.Image.FromFile(realPath);
                                width = smallImage.Width;
                                height = smallImage.Height;
                            }
                        }

                        details.Add(new CommunicateDetail()
                        {
                            ServiceNo = item.ServiceNo,
                            Content = item.Content,
                            ImageUrl =
                                item.File == null
                                    ? ""
                                    : conf.SiteUrl + "/Images/Service/OutSide/" + item.ServiceNo + "/" + item.File,
                            SmallImageUrl = smallUrl,
                            SmallImageWidth = width,
                            SmallImageHieght = height,
                            CreateName = item.Name,
                            CreateTime = ApiSystemManager.DateTimeToDateTimeString(item.CreateTime),
                            Method = item.Method ?? (int)methodConvert.None,
                        });
                    }

                    #endregion
                }
                else
                {
                    #region 分頁取

                    foreach (var item in csol.Skip((pageCnt - 1) * pageRow).Take(pageRow).OrderBy(p => p.CreateTime))
                    {
                        string smallUrl = item.File == null
                            ? ""
                            : conf.SiteUrl + "/Images/Service/OutSide/" + item.ServiceNo + "/Small/" + item.File;
                        int width = 0;
                        int height = 0;
                        if (!string.IsNullOrEmpty(smallUrl))
                        {
                            string path = Helper.MapPath(conf.OutSideUrl);
                            string realPath = Path.Combine(
                                Path.Combine(Path.Combine(path, item.ServiceNo), "Small"), item.File ?? string.Empty);
                            if (File.Exists(realPath))
                            {
                                System.Drawing.Image smallImage = System.Drawing.Image.FromFile(realPath);
                                width = smallImage.Width;
                                height = smallImage.Height;
                            }
                        }

                        details.Add(new CommunicateDetail()
                        {
                            ServiceNo = item.ServiceNo,
                            Content = item.Content,
                            ImageUrl =
                                item.File == null
                                    ? ""
                                    : conf.SiteUrl + "/Images/Service/OutSide/" + item.ServiceNo + "/" + item.File,
                            SmallImageUrl = smallUrl,
                            SmallImageWidth = width,
                            SmallImageHieght = height,
                            CreateName = item.Name,
                            CreateTime = ApiSystemManager.DateTimeToDateTimeString(item.CreateTime),
                            Method = item.Method ?? (int)methodConvert.None,
                        });
                    }

                    #endregion
                }
            }

            return details;
        }

        /// <summary>
        /// 取得客服對話ByOrderGuid
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderGuid"></param>
        /// <param name="isGetAll"></param>
        /// <param name="pageCnt"></param>
        /// <returns></returns>
        public static CommunicateData GetCommunicateListByOrderGuid(int userId, string orderGuid, bool isGetAll = false, int pageCnt = 1)
        {
            CommunicateData data = new CommunicateData { IsGetBanner = true };

            Guid orderguid;
            if (!Guid.TryParse(orderGuid, out orderguid))
            {
                return data;
            }

            //案件查詢
            ViewCustomerServiceList vcsm = sp.GetViewCustomerServiceMessageByUserIdAndOrderGuid(userId, orderguid);

            if (vcsm.IsLoaded)
            {
                #region 有此OrderGuid案件

                string imagePathData = PponFacade.GetPponDealFirstImagePath(vcsm.ImagePath);
                //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                if ((vcsm.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                {
                    //多檔次子檔訂單需取得主檔的圖片
                    if (vcsm.BusinessHourGuid != null)
                    {
                        CouponEventContent mainDealContent = _pponProvider.CouponEventContentGetBySubDealBid((Guid)vcsm.BusinessHourGuid);
                        if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                        {
                            imagePathData = PponFacade.GetPponDealFirstImagePath(mainDealContent.ImagePath);
                        }
                    }
                }

                data.Name = vcsm.ItemName;
                data.OrderId = vcsm.OrderId;
                data.OrderGuid = vcsm.OrderGuid.ToString();
                data.ModifyTime = vcsm.ModifyTime == null
                        ? ApiSystemManager.DateTimeToDateTimeString(vcsm.CreateTime)
                        : ApiSystemManager.DateTimeToDateTimeString(vcsm.ModifyTime.Value);
                data.MainImagePath = imagePathData;
                data.Status = vcsm.CustomerServiceStatus;
                data.CategoryId = vcsm.SubCategory;
                data.CategoryName = vcsm.SubCategoryName;

                #endregion
            }
            else
            {
                #region 查無案件，但查得到訂單

                var vclmc = MemberFacade.ViewCouponListMainGetByOrderGuid(userId, orderguid);
                var firstCouponListMain = vclmc.FirstOrDefault();

                if (firstCouponListMain != null)
                {
                    string imagePathData = PponFacade.GetPponDealFirstImagePath(firstCouponListMain.ImagePath);
                    //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                    if ((firstCouponListMain.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                    {
                        //多檔次子檔訂單需取得主檔的圖片
                        CouponEventContent mainDealContent = _pponProvider.CouponEventContentGetBySubDealBid(firstCouponListMain.BusinessHourGuid);
                        if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                        {
                            imagePathData = PponFacade.GetPponDealFirstImagePath(mainDealContent.ImagePath);
                        }
                    }

                    data.Name = firstCouponListMain.ItemName;
                    data.OrderId = firstCouponListMain.OrderId;
                    data.OrderGuid = orderguid.ToString();
                    data.MainImagePath = imagePathData;
                    data.Status = (int)statusConvert.wait;
                }

                #endregion
            }

            //對話記錄
            ViewCustomerServiceOutsideLogCollection csol = sp.GetCustomerServiceOutsideLogByorderGuid(userId, orderguid);

            double total = csol.Count;
            data.TotalPage = Convert.ToInt16(Math.Ceiling(total / pageRow));

            if (csol.Any())
            {
                if (isGetAll)
                {
                    #region 一次取全部

                    foreach (var item in csol.OrderBy(p => p.CreateTime))
                    {
                        string smallUrl = item.File == null ? "" : conf.SiteUrl + "/Images/Service/OutSide/" + item.ServiceNo + "/Small/" + item.File;
                        int width = 0;
                        int height = 0;
                        if (!string.IsNullOrEmpty(smallUrl))
                        {
                            string path = Helper.MapPath(conf.OutSideUrl);
                            string realPath = Path.Combine(Path.Combine(Path.Combine(path, item.ServiceNo), "Small"), item.File ?? string.Empty);
                            if (File.Exists(realPath))
                            {
                                System.Drawing.Image smallImage = System.Drawing.Image.FromFile(realPath);
                                width = smallImage.Width;
                                height = smallImage.Height;

                            }
                        }
                        data.Communicate.Add(new CommunicateDetail()
                        {
                            ServiceNo = item.ServiceNo,
                            Content = item.Content,
                            ImageUrl = item.File == null ? "" : conf.SiteUrl + "/Images/Service/OutSide/" + item.ServiceNo + "/" + item.File,
                            SmallImageUrl = smallUrl,
                            SmallImageWidth = width,
                            SmallImageHieght = height,
                            CreateTime = ApiSystemManager.DateTimeToDateTimeString(item.CreateTime),
                            CreateName = item.Name,
                            Method = item.Method ?? (int)methodConvert.None,
                        });
                    }

                    #endregion
                }
                else
                {
                    #region 分頁取

                    foreach (var item in csol.Skip((pageCnt - 1) * pageRow).Take(pageRow).OrderBy(p => p.CreateTime))
                    {
                        string smallUrl = item.File == null ? "" : conf.SiteUrl + "/Images/Service/OutSide/" + item.ServiceNo + "/Small/" + item.File;
                        int width = 0;
                        int height = 0;
                        if (!string.IsNullOrEmpty(smallUrl))
                        {
                            string path = Helper.MapPath(conf.OutSideUrl);
                            string realPath = Path.Combine(Path.Combine(Path.Combine(path, item.ServiceNo), "Small"), item.File ?? string.Empty);
                            if (File.Exists(realPath))
                            {
                                System.Drawing.Image smallImage = System.Drawing.Image.FromFile(realPath);
                                width = smallImage.Width;
                                height = smallImage.Height;
                            }
                        }
                        data.Communicate.Add(new CommunicateDetail()
                        {
                            ServiceNo = item.ServiceNo,
                            Content = item.Content,
                            ImageUrl = item.File == null ? "" : conf.SiteUrl + "/Images/Service/OutSide/" + item.ServiceNo + "/" + item.File,
                            SmallImageUrl = smallUrl,
                            SmallImageWidth = width,
                            SmallImageHieght = height,
                            CreateName = item.Name,
                            CreateTime = ApiSystemManager.DateTimeToDateTimeString(item.CreateTime),
                            Method = item.Method ?? (int)methodConvert.None,
                        });
                    }

                    #endregion
                }

                #region 取得尚未讀取的客服訊息並回寫讀取狀態
                var csolNotRead = csol.Where(p => p.IsRead == (int)IsRead.No);
                foreach (var item in csolNotRead)
                {
                    CustomerServiceOutsideLog cso = sp.CustomerServiceOutsideLogGet(item.Id);
                    cso.IsRead = (int)IsRead.Yes;
                    sp.CustomerServiceOutsideLogSet(cso);
                }
                #endregion
            }

            return data;
        }

        /// <summary>
        /// 取得問題紀錄列表(本站)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type">1.一般問題 2.訂單問題</param>
        /// <returns></returns>
        public static DataList GetDataList(int userId, int type)
        {
            var vcslCol = sp.GetViewCustomerServiceMessageByUserId(userId);

            return IssueListFactory(type, vcslCol);
        }

        /// <summary>
        /// 生成 一般問題 or 訂單問題 列表
        /// </summary>
        /// <param name="issueType">1.一般問題 2.訂單問題</param>
        /// <param name="vcslCol">ViewCustomerServiceMessageCollection</param>
        /// <returns></returns>
        private static DataList IssueListFactory(int issueType, ViewCustomerServiceListCollection vcslCol)
        {
            var data = new DataList { IssueList = new List<Issue>() };
            var haveNews = false;

            if (issueType == (int)ServiceIssueType.GeneralIssue)
            {
                #region 一般問題

                var generalIssuesCol = vcslCol.Where(p => p.Category != 1 && (p.OrderGuid == Guid.Empty || p.OrderGuid == null))
                    .OrderByDescending(p => p.CreateTime).ToList();
                foreach (var vcsl in generalIssuesCol)
                {
                    //重覆的ParentServiceNo，第二筆為舊案件，有接續案件使用最新案件(編號、類別)作為對話 header
                    if (data.IssueList.Select(x => x.ParentServiceNo).Contains(vcsl.ParentServiceNo))
                    {
                        continue;
                    }

                    haveNews = false;
                    string modifyTime;
                    var content = string.Empty;

                    //對話訊息
                    var vcsolCol = sp.GetCustomerServiceOutsideLogParentServiceNo(vcsl.ParentServiceNo);

                    var firstMsg = vcsolCol.OrderByDescending(p => p.CreateTime).FirstOrDefault();
                    var newMsg = vcsolCol.Where(p => p.IsRead == (int)IsRead.No && p.Method != (int)methodConvert.user)
                        .OrderByDescending(p => p.CreateTime).ToList().FirstOrDefault();

                    //有未讀訊息
                    if (newMsg != null)
                    {
                        haveNews = true;
                        modifyTime = ApiSystemManager.DateTimeToDateTimeString(newMsg.CreateTime);
                        content = newMsg.Content ?? string.Empty;
                    }
                    else
                    {
                        if (vcsolCol.Any() && firstMsg != null)
                        {
                            modifyTime = ApiSystemManager.DateTimeToDateTimeString(firstMsg.CreateTime);
                            content = firstMsg.Content ?? string.Empty;
                        }
                        else
                        {
                            modifyTime = vcsl.ModifyTime == null
                                ? ApiSystemManager.DateTimeToDateTimeString(vcsl.CreateTime)
                                : ApiSystemManager.DateTimeToDateTimeString((DateTime)vcsl.ModifyTime);
                        }
                    }

                    data.IssueList.Add(new Issue()
                    {
                        ServiceNo = vcsl.ServiceNo,
                        Name = vcsl.MainCategoryName ?? string.Empty,
                        OrderId = "",
                        OrderGuid = "",
                        ModifyTime = modifyTime,
                        NewMessage = haveNews,
                        MainImagePath = "",
                        Content = content,
                        ParentServiceNo = vcsl.ParentServiceNo
                    });

                }

                #endregion
            }
            else
            {
                #region 訂單問題

                var orderIssuesCol = vcslCol.Where(p => p.OrderGuid != Guid.Empty && p.OrderGuid != null)
                    .OrderByDescending(p => p.CreateTime).ToList();

                foreach (var vcsl in orderIssuesCol)
                {
                    //重覆的ParentServiceNo，第二筆為舊案件，有接續案件使用最新案件(編號、類別)作為對話 header
                    if (data.IssueList.Select(x => x.ParentServiceNo).Contains(vcsl.ParentServiceNo))
                    {
                        continue;
                    }

                    haveNews = false;
                    string modifyTime;
                    var content = string.Empty;

                    var imagePathData = PponFacade.GetPponDealFirstImagePath(vcsl.ImagePath);

                    //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                    if ((vcsl.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0 && vcsl.BusinessHourGuid != null)
                    {
                        //多檔次子檔訂單需取得主檔的圖片
                        CouponEventContent mainDealContent =
                            _pponProvider.CouponEventContentGetBySubDealBid((Guid)vcsl.BusinessHourGuid);
                        if (mainDealContent.IsLoaded)
                        {
                            imagePathData = PponFacade.GetPponDealFirstImagePath(mainDealContent.ImagePath);
                        }
                    }

                    //對話訊息
                    var vcsolCol = sp.GetCustomerServiceOutsideLogParentServiceNo(vcsl.ParentServiceNo);
                    var firstMsg = vcsolCol.OrderByDescending(p => p.CreateTime).FirstOrDefault();
                    var newMsg = vcsolCol.Where(p => p.IsRead == (int)IsRead.No && p.Method != (int)methodConvert.user)
                        .OrderByDescending(p => p.CreateTime).ToList().FirstOrDefault();

                    //有未讀訊息
                    if (newMsg != null)
                    {
                        haveNews = true;
                        modifyTime = ApiSystemManager.DateTimeToDateTimeString(newMsg.CreateTime);
                        content = newMsg.Content ?? string.Empty;
                    }
                    else
                    {
                        if (vcsolCol.Any() && firstMsg != null)
                        {
                            modifyTime = ApiSystemManager.DateTimeToDateTimeString(firstMsg.CreateTime);
                            content = firstMsg.Content ?? string.Empty;
                        }
                        else
                        {
                            modifyTime = vcsl.ModifyTime == null
                                ? ApiSystemManager.DateTimeToDateTimeString(vcsl.CreateTime)
                                : ApiSystemManager.DateTimeToDateTimeString((DateTime)vcsl.ModifyTime);
                        }
                    }

                    data.IssueList.Add(new Issue()
                    {
                        ServiceNo = vcsl.ServiceNo,
                        Name = vcsl.ItemName,
                        OrderId = vcsl.OrderId,
                        OrderGuid = vcsl.OrderGuid.ToString(),
                        ModifyTime = modifyTime,
                        NewMessage = haveNews,
                        MainImagePath = imagePathData,
                        Content = content,
                        ParentServiceNo = vcsl.ParentServiceNo
                    });

                }

                #endregion
            }

            //前面sort CreateTime用來取最新案件編號、分類，這裡sort ModifyTime用來做最新異動呈現
            data.IssueList = data.IssueList.OrderByDescending(x => x.ModifyTime).ToList();
            data.News = haveNews;

            return data;
        }

        /// <summary>
        /// 建立一般問題
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userId"></param>
        /// <param name="sendData"></param>
        /// <param name="service"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public static bool CreateIssue(string userName, int userId, ApiSendData sendData, out ServiceData service, out string errMsg)
        {
            errMsg = string.Empty;
            service = new ServiceData();

            var mainCategoryId = sendData.CategoryId;
            var subCategoryId = 0; //一般問題預設未選擇子分類

            int source = sendData.Source == (int)Source.app
                     ? sendData.UserId.ToLower().Contains("ios")
                        ? (int)Source.ios
                        : (int)Source.android
                     : sendData.Source;
            try
            {
                #region 案件

                CustomerServiceMessage csm;

                //問新問題
                if (string.IsNullOrEmpty(sendData.ServiceNo))
                {
                    csm = CreateIssueMain(mainCategoryId, subCategoryId, sendData.Mobile, sendData.Name, sendData.Email, source, userName, userId, null, null, sendData.IssueFromType);
                }
                else
                {
                    //抓取現有資料流
                    csm = sp.CustomerServiceMessageGet(sendData.ServiceNo);
                    if (csm.IsLoaded)
                    {
                        if (csm.Status == (int)statusConvert.complete)
                        {
                            //已結案，再建立新案件
                            csm = CreateIssueMain(mainCategoryId, subCategoryId, sendData.Mobile, sendData.Name, sendData.Email, source, userName,
                                userId, null, csm.ServiceNo, sendData.IssueFromType);
                        }
                    }
                    else
                    {
                        //查無案件，重新發問
                        csm = CreateIssueMain(mainCategoryId, subCategoryId, sendData.Mobile, sendData.Name, sendData.Email, source, userName, userId);
                    }
                }

                sp.CustomerServiceMessageSet(csm);

                #endregion

                #region 問題

                var csol = CreateIssueDetail(csm, sendData.Content, sendData.Image, userName, userId);
                sp.CustomerServiceOutsideLogSet(csol);

                #endregion

                service.ServiceNo = csm.ServiceNo;
            }
            catch (Exception ex)
            {
                SystemFacade.SetApiLog(string.Format("{0}.{1}", "CreateIssue", MethodBase.GetCurrentMethod().Name),
                        "CustomerServiceFacade", sendData, "CreateIssue 錯誤! " + ex, Helper.GetClientIP());
                errMsg = ex.Message;
            }

            return errMsg.Length == 0;
        }

        /// <summary>
        /// 建立訂單問題
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userId"></param>
        /// <param name="sendData"></param>
        /// <param name="createNew">結案前是否另開案件</param>
        /// <param name="outsideLogId"></param>
        /// <returns></returns>
        public static bool CreateOrderIssue(string userName, int userId, ApiSendData sendData, bool createNew, out string serviceNo)
        {
            Guid orderGuid;
            Guid.TryParse(sendData.OrderGuid, out orderGuid);

            var mainCategoryId = 1; //訂單問題預設主分類就是訂單問題
            var subCategoryId = sendData.CategoryId == 0 || sendData.CategoryId == 1 ? 0 : sendData.CategoryId; //相容舊版App所帶的categoryId

            int source = sendData.Source == (int)Source.app
                     ? sendData.UserId.ToLower().Contains("ios")
                        ? (int)Source.ios
                        : (int)Source.android
                     : sendData.Source;

            try
            {
                CustomerServiceMessage csm = sp.GetLastCustomerMessageByOrderGuid(orderGuid);
                if (!csm.IsLoaded)
                {
                    //案件不存在，開新案
                    csm = CreateIssueMain(mainCategoryId, subCategoryId, sendData.Mobile, sendData.Name, sendData.Email, source, userName, userId, orderGuid, null, sendData.IssueFromType);
                    sp.CustomerServiceMessageSet(csm);
                }
                else
                {
                    if (csm.Status == (int)statusConvert.complete || createNew)
                    {
                        //已結案，另開新案
                        csm = CreateIssueMain(mainCategoryId, subCategoryId, sendData.Mobile, sendData.Name, sendData.Email, source, userName, userId, orderGuid, csm.ServiceNo, sendData.IssueFromType);
                        sp.CustomerServiceMessageSet(csm);
                    }
                }

                //未結案
                var newLog = CreateIssueDetail(csm, sendData.Content, sendData.Image, userName, userId);
                sp.CustomerServiceOutsideLogSet(newLog);
                serviceNo = csm.ServiceNo;

                //處理中案件再次詢問，寄信通知客服
                if (newLog.Status != (int)statusConvert.wait)
                {
                    EmailFacade.SendNewCustomerMessageToCs(userId, csm.ServiceNo);
                }
            }
            catch (Exception ex)
            {
                SystemFacade.SetApiLog(string.Format("{0}.{1}", "CreateOrderIssue", MethodBase.GetCurrentMethod().Name),
                        "CustomerServiceFacade", sendData, "CreateOrderIssue 錯誤! " + ex, Helper.GetClientIP());
                serviceNo = "";
                return false;
            }
            return true;
        }

        #endregion

        #region webmethod

        /// <summary>
        /// 取得問題類別序列化
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static string GetServiceMessageCategory(int categoryId)
        {
            var serviceCates = sp.GetCustomerServiceCategoryListByCategoryId(categoryId).ToList();
            return new JsonSerializer().Serialize(serviceCates);
        }

        /// <summary>
        /// 後台回覆客服問題
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="serviceNo"></param>
        /// <param name="content"></param>
        /// <param name="method"></param>
        /// <param name="file"></param>
        /// <param name="mailTo"></param>
        /// <param name="mobileTo"></param>
        /// <returns></returns>
        public static ReplyData ReplyCustomer(string userName, string serviceNo, string content, int method, HttpPostedFileBase file, string mailTo, string mobileTo)
        {
            ReplyData data = new ReplyData();
            DateTime createTime = DateTime.Now;
            int userId = 0;
            userId = MemberFacade.GetUniqueId(userName);

            var entity = sp.GetViewCustomerServiceMessageByServiceNo(serviceNo);

            if (entity.IsLoaded)
            {
                CustomerServiceOutsideLog csoi = new CustomerServiceOutsideLog();
                csoi.Content = content;
                csoi.ServiceNo = serviceNo;
                csoi.Type = entity.MessageType;
                csoi.Status = entity.CustomerServiceStatus;
                csoi.CreateTime = createTime;
                csoi.CreateId = userId;
                csoi.CreateName = userName;
                csoi.IsRead = (int)IsRead.No;
                csoi.Method = method;

                if (file != null)
                {
                    csoi.File = serviceNo + "_" + createTime.ToString("MMddHHmmss") + ".png";

                    byte[] arr;
                    MemoryStream ms = file.InputStream as MemoryStream;
                    if (ms == null)
                    {
                        ms = new MemoryStream();
                        file.InputStream.CopyTo(ms);
                    }
                    arr = ms.ToArray();
                    string path = Helper.MapPath(conf.OutSideUrl);
                    ImageFacade.UploadBtyeToImageWithResize(arr, ImageFormat.Png, path, serviceNo, csoi.File, 640);
                }

                sp.CustomerServiceOutsideLogSet(csoi);


                Member mem = mp.MemberGet(userName);
                if (mem.IsLoaded)
                {
                    data.name = mem.LastName + mem.FirstName;
                }

                data.account = userName;
                data.content = content;
                data.date = csoi.CreateTime.ToString("yyyy-MM-dd HH:mm");
                data.method = Helper.GetEnumDescription((methodConvert)method);
                data.image = csoi.File;
                data.link = csoi.File == null ? "" : conf.SiteUrl + "/Images/Service/OutSide/" + serviceNo + "/" + csoi.File;


                #region SendMail

                CustomerServiceMail template = TemplateFactory.Instance().GetTemplate<CustomerServiceMail>();
                template.SiteUrl = conf.SiteUrl;
                template.SiteServiceUrl = "/Ppon/NewbieGuide.aspx";
                template.ServiceName = string.IsNullOrEmpty(data.name) == true ? "客服專員" : data.name;

                template.MemberName = string.IsNullOrEmpty(entity.MemberName) == true ? "會員" : entity.MemberName;
                if (entity.OrderGuid == null)
                {
                    template.ContentDes = "您所詢問關於「" + entity.MainCategoryName + "」" + "的問題，回覆內容如下：";
                    template.SiteServiceRedirectUrl = "/Ppon/ServiceReferralRedirection.aspx?ServiceType=1&ServicePara=" + entity.ServiceNo;
                }
                else
                {
                    template.ContentDes = "您所詢問關於「訂單編號" + entity.OrderId + "」" + "的問題，回覆內容如下：";
                    template.SiteServiceRedirectUrl = "/Ppon/ServiceReferralRedirection.aspx?ServiceType=2&ServicePara=" + entity.OrderGuid.ToString();

                }
                if (!string.IsNullOrEmpty(csoi.File))
                {
                    template.Content = content + "<br />" + "<a href='" + conf.SiteUrl + "/" + "Images/Service/OutSide/" + entity.ServiceNo + "/" + csoi.File + "' target='_blank'>查看附件</a>";
                }
                else
                {
                    template.Content = content;
                }

                if (!string.IsNullOrEmpty(mailTo))
                {
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(conf.ServiceEmail, conf.ServiceName);
                    msg.To.Add(mailTo);
                    msg.Subject = "17Life客服中心已回覆您的問題";
                    msg.Body = template.ToString();
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }


                #endregion

                string body = string.Empty;

                #region SendPush
                if (entity.UserId != null)
                {
                    string orderGuid = entity.OrderGuid == null ? "" : entity.OrderGuid.Value.ToString();
                    body = "【客服通知】";
                    NotificationFacade.PushCustomerServiceMessage(entity.UserId.Value, body, serviceNo, orderGuid, false);
                }
                #endregion

                #region SendSMS
                if (method == 3)
                {
                    if (entity.OrderGuid != null)
                    {
                        body = "17Life客服已回覆您所詢問關於「訂單編號" + entity.OrderId + "」" + "的問題";
                    }
                    else
                    {
                        body = "17Life客服已回覆您所詢問關於「" + entity.MainCategoryName + "」" + "的問題";
                    }

                    SMS sms = new SMS();

                    sms.SendMessage(string.Empty, body, string.Empty, mobileTo, userName);
                }
                #endregion

                #region 儲存中台
                if (entity.IssueFromType == (int)IssueFromType.TaiShin)
                {
                    string imgUrl = csoi.File == null ? "" : conf.SiteUrl + "/Images/Service/OutSide/" + serviceNo + "/" + csoi.File;
                    LifeEntrepotApi.CustomerServiceReply(serviceNo, content, imgUrl, userId.ToString(), createTime);
                }
                #endregion
            }

            return data;
        }

        /// <summary>
        /// 轉換案件物件
        /// </summary>
        /// <param name="vcsl"></param>
        /// <param name="employeeUserId"></param>
        /// <returns></returns>
        public static CaseData GetCaseData(ViewCustomerServiceList vcsl, int employeeUserId)
        {
            var caseData = new CaseData();
            var serviceNo = vcsl.ServiceNo;

            #region 案件分類

            caseData.Category = sp.GetCustomerServiceCategory().Select(x => new KeyValuePair<int, string>(x.CategoryId, x.CategoryName)).ToList();
            caseData.SubCategory = sp.GetCustomerServiceCategoryListByCategoryId(vcsl.Category).Select(x => new KeyValuePair<int, string>(x.CategoryId, x.CategoryName)).ToList();

            #endregion

            #region 案件基本資料

            var workerOne = string.Empty;
            if (vcsl.ServicePeopleId != null)
            {
                var member = MemberFacade.GetMember((int)vcsl.ServicePeopleId);
                workerOne = member.LastName + member.FirstName;
            }

            var workerTwo = string.Empty;
            var workerTwoList = new List<KeyValuePair<string, string>>();
            if (vcsl.SecServicePeopleId != null)
            {
                var member = MemberFacade.GetMember((int)vcsl.SecServicePeopleId);
                workerTwo = member.LastName + member.FirstName;
            }
            else
            {
                const string depRoleName = "CustomerCare";
                var serviceEmps = mp.ViewRoleMemberCollectionGet(depRoleName).Where(x => x.UserId != employeeUserId);
                workerTwoList.AddRange(serviceEmps.Select(x => new KeyValuePair<string, string>(x.UserId.ToString(), x.Name)).Distinct().ToList());
            }

            bool isShowNotify = false;
            bool isShowSecNotify = false;
            bool isShowSelectWorkerTwo = false;
            if (vcsl.CustomerServiceStatus != 0)
            {
                bool worker = vcsl.ServicePeopleId == employeeUserId;
                bool secWorker = vcsl.SecServicePeopleId == employeeUserId;
                bool hasSecWorker = vcsl.SecServicePeopleId > 0;
                isShowNotify = secWorker;
                isShowSecNotify = worker && hasSecWorker;
                isShowSelectWorkerTwo = worker && !hasSecWorker;
            }

            caseData.BaseData = new CustomerServiceCase
            {
                ServiceNo = serviceNo,
                Status = (statusConvert)vcsl.CustomerServiceStatus,
                StatusDesc = Helper.GetEnumDescription((statusConvert)vcsl.CustomerServiceStatus),
                WorkerOne = workerOne,
                WorkerTwo = workerTwo,
                Category = vcsl.Category,
                SubCategory = vcsl.SubCategory,
                QuestoinLevel = vcsl.CasePriority,
                QuestionFrom = vcsl.MessageType,
                QuestionDeal = vcsl.CustomerServiceStatus,
                WorkerTwoList = workerTwoList,
                IsShowNotify = isShowNotify,
                IsShowSecNotify = isShowSecNotify,
                IsShowSelectWorkerTwo = isShowSelectWorkerTwo
            };

            #endregion

            #region 商家與客服紀錄

            caseData.InsideMessage = new List<CustomerServiceConversation>();
            ViewCustomerServiceInsideLogCollection insideLogCol = sp.GetInsideLogByServiceNo(serviceNo);
            foreach (var iLog in insideLogCol)
            {
                bool isByCustomerService = true;
                string name = string.Empty;
                if (iLog.Status == (int)statusConvert.transferback)
                {
                    var vcsm = sp.GetViewCustomerServiceMessageByServiceNo(serviceNo);
                    if (vcsm.IsLoaded)
                    {
                        name = vcsm.SellerName;
                    }
                    isByCustomerService = false;
                }
                else
                {
                    name = iLog.Name;
                }

                caseData.InsideMessage.Add(new CustomerServiceConversation
                {
                    Name = name,
                    Status = Helper.GetEnumDescription((statusConvert)iLog.Status),
                    Content = string.IsNullOrEmpty(iLog.Content) ? string.Empty : iLog.Content.Replace("\r\n", "<br>"),
                    AttachedFilesName = iLog.File,
                    AttachedFilesNavigateUrl = string.Format("/Images/Service/InSide/{0}/{1}", iLog.ServiceNo, iLog.File),
                    CreateTime = iLog.CreateTime.ToString("yyyy-MM-dd HH:mm"),
                    Memo = iLog.Memo,
                    IsByCustomerService = isByCustomerService
                });
            }



            #endregion

            #region 客人與客服紀錄

            caseData.OutsideMessage = new List<CustomerServiceConversation>();
            ViewCustomerServiceOutsideLogCollection outsideLogCol = sp.GetOutsideLogByServiceNo(serviceNo);
            foreach (var oLog in outsideLogCol)
            {
                caseData.OutsideMessage.Add(new CustomerServiceConversation
                {
                    Name = oLog.Name,// + " " + oLog.CreateName,
                    Content = string.IsNullOrEmpty(oLog.Content) ? string.Empty : oLog.Content.Replace("\r\n", "<br>"),
                    AttachedFilesName = oLog.File,
                    AttachedFilesNavigateUrl = string.Format("/Images/Service/InSide/{0}/{1}", oLog.ServiceNo, oLog.File),
                    CreateTime = oLog.CreateTime.ToString("yyyy-MM-dd HH:mm"),
                    IsByCustomerService = oLog.Method != (int)methodConvert.user
                });
            }

            #endregion

            return caseData;
        }

        /// <summary>
        /// 以案件編號取得案件
        /// </summary>
        /// <param name="serviceNo"></param>
        /// <param name="employeeUserId"></param>
        /// <returns></returns>
        public static CaseData GetCaseDataByServiceNo(string serviceNo, int employeeUserId)
        {
            var vcsl = sp.GetViewCustomerServiceMessageByServiceNo(serviceNo);
            if (vcsl.IsLoaded)
            {
                return GetCaseData(vcsl, employeeUserId);
            }

            return null;
        }

        #endregion

        /// <summary>
        /// 取得二線客服 姓名
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static string GetSellerMappingEmpName(Guid sellerGuid)
        {
            var vsme = hp.SellerMappingEmployeeGetBySeller(sellerGuid);
            if (vsme.IsLoaded)
            {
                return vsme.EmpName;
            }
            return string.Empty;
        }

        /// <summary>
        /// 取得客服收件人
        /// </summary>
        /// <param name="vcsl"></param>
        /// <returns></returns>
        public static string GetCsContacts(ViewCustomerServiceList vcsl)
        {
            //客服主管
            var mailTo = conf.CsMasterEmailAddress;

            //案件認領者
            if (vcsl.ServicePeopleId != null)
            {
                var mem = mp.MemberGetbyUniqueId((int)vcsl.ServicePeopleId);
                if (mem.IsLoaded)
                {
                    if (!string.IsNullOrEmpty(mailTo))
                    {
                        mailTo += "," + mem.UserName;
                    }
                    else
                    {
                        mailTo = mem.UserName;
                    }
                }
            }

            return mailTo;
        }

        /// <summary>
        /// 取得廠商收件人
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="contactType"></param>
        /// <returns></returns>
        public static KeyValuePair<bool, string> GetSellerContacts(Guid? sellerGuid, ProposalContact contactType)
        {
            var mailTo = string.Empty;
            List<string> mails = new List<string>();

            if (sellerGuid == null)
            {
                return new KeyValuePair<bool, string>(false, "商家資料錯誤");
            }
            Seller seller = ssp.SellerGet((Guid)sellerGuid);
            if (seller == null)
            {
                return new KeyValuePair<bool, string>(false, "商家資料錯誤");
            }
            List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
            if (contacts == null)
            {
                return new KeyValuePair<bool, string>(false, "查無商家聯絡人資料 " + sellerGuid);
            }

            string mailList = string.Empty;
            foreach (var c in contacts)
            {
                if (c.Type == ((int)contactType).ToString())
                {
                    mails.Add(c.ContactPersonEmail);
                }
            }
            mailTo = string.Join(",", mails);
            mailTo = RegExRules.CheckMultiEmail(mailTo);
            if (string.IsNullOrEmpty(mailTo))
            {
                return new KeyValuePair<bool, string>(false,
                        string.Format("商家:{0}<a href='{1}'>{2}</a> 聯絡人mail有誤，無法發送信件。聯絡人Email: \"{3}\"", seller.SellerId, conf.SiteUrl + "/sal/SellerContent.aspx?sid=" + seller.Guid, seller.SellerName, string.Join(",", mails)));
            }

            return new KeyValuePair<bool, string>(true, mailTo);
        }

        /// <summary>
        /// 匯整商家Email 
        /// </summary>
        /// <param name="deliveryType"></param>
        /// <param name="contactType"></param>
        /// <returns></returns>
        public static List<KeyValuePair<Guid, string>> GetClearSellerEmail(DeliveryType deliveryType, ProposalContact contactType)
        {
            ViewSellerContactCollection sellerContactsCol = ssp.GetSellerContactsByDeliveryType(deliveryType);
            var dicSellerContacts = sellerContactsCol.ToDictionary(x => x.SellerGuid); //商家與商家間的mail不可放一起寄

            List<KeyValuePair<Guid, string>> result = new List<KeyValuePair<Guid, string>>();
            foreach (var dicSellerContact in dicSellerContacts)
            {
                var sellerGuid = dicSellerContact.Key;
                ViewSellerContact vsc = dicSellerContact.Value;
                List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(vsc.Contacts);
                foreach (var c in contacts)
                {
                    if (c.Type == ((int)contactType).ToString())
                    {
                        var mailList = c.ContactPersonEmail.Replace(" ", ",");
                        var mailTo = RegExRules.CheckMultiEmail(mailList); //一個seller對應自家的多筆聯絡人mail，並用 "," 分隔
                        if (!string.IsNullOrEmpty(mailTo))
                        {
                            result.Add(new KeyValuePair<Guid, string>(sellerGuid, mailTo));
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 以問題分類編號取得罐頭訊息
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static MainModel GetSample(int categoryId)
        {
            var sampleList = sp.CustomerServiceCategorySampleGetByCategoryId(categoryId);
            MainModel main = new MainModel();
            main.sampleList = new List<SampleModel>();
            foreach (var item in sampleList)
            {
                main.sampleList.Add(new SampleModel
                {
                    id = item.Id,
                    content = item.Content,//.Replace("\r\n", "<br>"),
                    subject = item.Subject
                });
            }
            return main;
        }

        #region 共用方法

        private static CustomerServiceMessage CreateIssueMain(int categoryId, int subCatetoryId, string phone, string name, string email,
            int source, string userName, int userId, Guid? orderGuid = null, string parentServiceNo = null, int? issueFromType = null)
        {
            CustomerServiceMessage csm = new CustomerServiceMessage
            {
                ServiceNo = GetNewServiceNo(),
                Category = categoryId,
                SubCategory = subCatetoryId,
                Phone = phone,
                Name = name,
                Email = email,
                Status = (int)statusConvert.wait,
                OrderGuid = orderGuid,
                CreateId = userName,
                CreateTime = DateTime.Now,
                MessageType = (int)messageConvert.online,
                Priority = (int)priorityConvert.normal,
                UserId = userId,
                Source = source,
                ParentServiceNo = parentServiceNo ?? GetNewServiceNo(), //接續單號
                IssueFromType = issueFromType ?? (int)IssueFromType.Local
            };

            return csm;
        }

        private static CustomerServiceOutsideLog CreateIssueDetail(CustomerServiceMessage csm, string content, string image, string createName, int? userId)
        {
            CustomerServiceOutsideLog csol = new CustomerServiceOutsideLog
            {
                ServiceNo = csm.ServiceNo,
                Content = content,
                Type = csm.MessageType,
                Status = csm.Status,
                CreateName = createName,
                IsRead = (int)IsRead.No,
                Method = (int)methodConvert.user,
                CreateTime = DateTime.Now,
                CreateId = userId
            };

            //處理圖片
            if (!string.IsNullOrEmpty(image))
            {
                csol.File = csm.ServiceNo + "_" + DateTime.Now.ToString("MMddHHmmss") + ".png";
                string path = Helper.MapPath(conf.OutSideUrl);
                byte[] arr = Convert.FromBase64String(image);

                ImageFacade.UploadBtyeToImageWithResize(arr, ImageFormat.Png, path, csm.ServiceNo, csol.File, 640);
            }

            return csol;
        }

        /// <summary>
        /// 取得新案件編號
        /// </summary>
        /// <returns></returns>
        public static string GetNewServiceNo()
        {
            string random = new Random().Next(0, 999).ToString().PadLeft(3, '0');
            string newServiceNo = "C" + DateTime.Now.ToString("yyyyMMddHHmmss") + random;

            return newServiceNo;
        }

        #endregion

        #region 台新

        /// <summary>
        /// 取得問題紀錄列表(台新)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DataList GetTsIssueList(GetTsIssueListCollection data)
        {
            var vcslCol = sp.GetViewCustomerServiceMessageByTsMemberNoAndIssueFromType(
                data.TsMemberNo, data.IssueFromType);

            return IssueListFactory(data.IssueType, vcslCol);
        }

        /// <summary>
        /// 建立一般客服案件+問題
        /// </summary>
        /// <param name="issue"></param>
        /// <returns></returns>
        public static string CreateTsIssue(int userId, PreTsIssue issue)
        {
            string returnServiceNo = string.Empty;

            try
            {
                #region 案件

                //一般問題 預設未選擇子分類(0)
                issue.SetCategoryId(issue.CategoryId, 0);

                CustomerServiceMessage message;

                //問新問題
                if (string.IsNullOrEmpty(issue.ServiceNo))
                {
                    message = CreateTsIssueMain(userId, issue);
                }
                else
                {
                    //抓取現有資料流
                    message = sp.CustomerServiceMessageGet(issue.ServiceNo);
                    if (message.IsLoaded)
                    {
                        if (message.Status == (int)statusConvert.complete)
                        {
                            //已結案，再建立新案件
                            message = CreateTsIssueMain(userId, issue, null, message.ServiceNo);
                        }
                    }
                    else
                    {
                        //查無案件，重新發問
                        message = CreateTsIssueMain(userId, issue);
                    }
                }

                sp.CustomerServiceMessageSet(message);

                #endregion

                #region 問題

                var csol = CreateTsIssueDetail(message, issue);
                sp.CustomerServiceOutsideLogSet(csol);

                #endregion

                returnServiceNo = message.ServiceNo;
            }
            catch (Exception ex)
            {
                SystemFacade.SetApiLog(string.Format("{0}.{1}", "CreateTsIssue", MethodBase.GetCurrentMethod().Name),
                        "CustomerServiceFacade", issue, "CreateIssueMain 錯誤! " + ex, Helper.GetClientIP());
            }

            return returnServiceNo;

        }

        /// <summary>
        /// 建立CustomerServiceMessage物件
        /// </summary>
        /// <param name="issue"></param>
        /// <param name="orderGuid">訂單編號</param>
        /// <param name="parentServiceNo">接續單號</param>
        /// <returns></returns>
        private static CustomerServiceMessage CreateTsIssueMain(int userId, PreTsIssue issue, Guid? orderGuid = null, string parentServiceNo = null)
        {
            var serviceNo = "TS" + DateTime.Now.ToString("yyyyMMddHHmmss");

            return new CustomerServiceMessage
            {
                IssueFromType = issue.IssueFromType,
                TsMemberNo = issue.TsMemberNo,
                ServiceNo = serviceNo,
                Category = issue.MainCategoryId,
                SubCategory = issue.SubCategoryId,
                Phone = issue.Mobile,
                Name = issue.Name,
                Email = issue.Email,
                Status = (int)statusConvert.wait,
                OrderGuid = orderGuid,
                CreateId = userId.ToString(),
                CreateTime = DateTime.Now,
                MessageType = (int)messageConvert.online,
                Priority = (int)priorityConvert.normal,
                UserId = userId,
                Source = (int)Source.web,
                ParentServiceNo = parentServiceNo ?? serviceNo //接續單號
            }; ;

        }
        /// <summary>
        /// 建立CustomerServiceOutsideLog物件
        /// </summary>
        /// <param name="message"></param>
        /// <param name="issue"></param>
        /// <returns></returns>
        private static CustomerServiceOutsideLog CreateTsIssueDetail(CustomerServiceMessage message, PreTsIssue issue)
        {
            var userName = conf.TmallDefaultUserName;
            var userId = mp.MemberUniqueIdGet(userName);

            return CreateIssueDetail(message, issue.Content, issue.Image, userName, userId);
        }

        /// <summary>
        /// 建立訂單問題(台新)
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userId"></param>
        /// <param name="sendData"></param>
        /// <returns></returns>
        public static string CreateTsOrderIssue(PreTsIssue issue)
        {

            string returnServiceNo = string.Empty;

            try
            {
                #region 案件

                Guid orderGuid;
                Guid.TryParse(issue.OrderGuid, out orderGuid);
                var o = op.OrderGet(orderGuid);

                //訂單問題預設主分類就是訂單問題(1)
                issue.SetCategoryId(1, issue.CategoryId);

                var message = sp.GetLastCustomerMessageByOrderGuid(orderGuid);

                if (!message.IsLoaded)
                {
                    //案件不存在，開新案
                    message = CreateTsIssueMain(o.UserId, issue, orderGuid);
                    sp.CustomerServiceMessageSet(message);
                }
                else
                {
                    if (message.Status == (int)statusConvert.complete)
                    {
                        //已結案，另開新案
                        message = CreateTsIssueMain(o.UserId, issue, orderGuid, message.ServiceNo);
                        sp.CustomerServiceMessageSet(message);
                    }
                }

                #endregion

                #region 問題

                //未結案
                var newLog = CreateTsIssueDetail(message, issue);
                sp.CustomerServiceOutsideLogSet(newLog);

                #endregion


                //處理中案件再次詢問，寄信通知客服
                if (newLog.Status != (int)statusConvert.wait)
                {
                    EmailFacade.SendNewCustomerMessageToCs(issue.TsMemberNo, message.ServiceNo);
                }

                returnServiceNo = message.ServiceNo;
            }
            catch (Exception ex)
            {
                SystemFacade.SetApiLog(string.Format("{0}.{1}", "CreateTsOrderIssue", MethodBase.GetCurrentMethod().Name),
                        "CustomerServiceFacade", issue, "CreateTsOrderIssue 錯誤! " + ex, Helper.GetClientIP());
            }

            return returnServiceNo;
        }

        /// <summary>
        /// 取得客服對話ByServiceNo (包含接續已結案的對話)(台新)
        /// </summary>
        /// <param name="preCommunicate"></param>
        /// <param name="preCommunicate.pageCnt">分頁功能，預設為1;(一頁抓50筆)</param>
        /// <param name="preCommunicate.isGetAll">false:使用分頁抓取;true:一次性全抓;</param>
        /// <returns></returns>
        public static CommunicateData GetTsCommunicateListByServiceNo(PreCommunicate preCommunicate)
        {
            //案件查詢
            var vcsm = sp.GetViewCustomerServiceMessageByServiceNo(preCommunicate.ServiceNo);

            if (!vcsm.IsLoaded)
            {
                return new CommunicateData();
            }

            //對話記錄
            var csol = sp.GetCustomerServiceOutsideLogByTsMemberNoAndParentServiceNo(
                preCommunicate.TsMemberNo, vcsm.ParentServiceNo, preCommunicate.IssueFromType);

            var details = GetCommunicateDetails(csol, preCommunicate.PageCount, preCommunicate.IsGetAll);

            foreach (var detail in details)
            {
                DateTime dt;
                ApiSystemManager.TryDateTimeStringToDateTime(detail.CreateTime, out dt);
                detail.CreateTime = dt.ToString("yyyy-MM-dd HH:mm");
            }
            var data = new CommunicateData();

            double total = csol.Count;

            data.TotalPage = Convert.ToInt16(Math.Ceiling(total / pageRow));
            data.Status = vcsm.CustomerServiceStatus;
            data.CategoryId = vcsm.Category;
            data.CategoryName = vcsm.MainCategoryName;
            data.Communicate.AddRange(details);

            SetOutSideLogIsReadToYes(csol);

            return data;
        }

        /// <summary>
        /// 取得客服對話ByOrderGuid
        /// </summary>
        /// <param name="preCommunicate"></param>
        /// <param name="preCommunicate.orderGuid">訂單編號</param>
        /// <param name="preCommunicate.pageCnt">分頁功能，預設為1;(一頁抓50筆)</param>
        /// <param name="preCommunicate.isGetAll">false:使用分頁抓取;true:一次性全抓;</param>
        /// <returns></returns>
        public static CommunicateData GetTsCommunicateListByOrderGuid(PreCommunicate preCommunicate)
        {
            var data = new CommunicateData { IsGetBanner = true };

            Guid orderguid;
            if (!Guid.TryParse(preCommunicate.OrderGuid, out orderguid))
            {
                return data;
            }

            //案件查詢
            var vcsm = sp.GetViewCustomerServiceMessageByTsMemberNoAndOrderGuid(
                preCommunicate.TsMemberNo, orderguid, preCommunicate.IssueFromType);

            if (vcsm.IsLoaded)
            {
                #region 有此OrderGuid案件

                string imagePathData = PponFacade.GetPponDealFirstImagePath(vcsm.ImagePath);
                //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                if ((vcsm.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                {
                    //多檔次子檔訂單需取得主檔的圖片
                    if (vcsm.BusinessHourGuid != null)
                    {
                        CouponEventContent mainDealContent = _pponProvider.CouponEventContentGetBySubDealBid((Guid)vcsm.BusinessHourGuid);
                        if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                        {
                            imagePathData = PponFacade.GetPponDealFirstImagePath(mainDealContent.ImagePath);
                        }
                    }
                }

                data.Name = vcsm.ItemName;
                data.OrderId = vcsm.OrderId;
                data.OrderGuid = vcsm.OrderGuid.ToString();
                data.ModifyTime = vcsm.ModifyTime == null
                        ? ApiSystemManager.DateTimeToDateTimeString(vcsm.CreateTime)
                        : ApiSystemManager.DateTimeToDateTimeString(vcsm.ModifyTime.Value);
                data.MainImagePath = imagePathData;
                data.Status = vcsm.CustomerServiceStatus;
                data.CategoryId = vcsm.SubCategory;
                data.CategoryName = vcsm.SubCategoryName;

                #endregion
            }
            else
            {
                #region 查無案件，但查得到訂單

                //天貓
                var userName = conf.TmallDefaultUserName;
                var userId = mp.MemberUniqueIdGet(userName);

                var vclmc = MemberFacade.ViewCouponListMainGetByOrderGuid(userId, orderguid);
                var firstCouponListMain = vclmc.FirstOrDefault();

                if (firstCouponListMain != null)
                {
                    string imagePathData = PponFacade.GetPponDealFirstImagePath(firstCouponListMain.ImagePath);
                    //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                    if ((firstCouponListMain.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                    {
                        //多檔次子檔訂單需取得主檔的圖片
                        CouponEventContent mainDealContent = _pponProvider.CouponEventContentGetBySubDealBid(firstCouponListMain.BusinessHourGuid);
                        if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                        {
                            imagePathData = PponFacade.GetPponDealFirstImagePath(mainDealContent.ImagePath);
                        }
                    }

                    data.Name = firstCouponListMain.ItemName;
                    data.OrderId = firstCouponListMain.OrderId;
                    data.OrderGuid = orderguid.ToString();
                    data.MainImagePath = imagePathData;
                    data.Status = (int)statusConvert.wait;
                }

                #endregion
            }

            //對話記錄
            var csol = sp.GetCustomerServiceOutsideLogByTsMemberNoAndOrderGuid(preCommunicate.TsMemberNo, orderguid, preCommunicate.IssueFromType);

            var details = GetCommunicateDetails(csol, preCommunicate.PageCount, preCommunicate.IsGetAll);
            data.Communicate.AddRange(details);

            double total = csol.Count;
            data.TotalPage = Convert.ToInt16(Math.Ceiling(total / pageRow));

            SetOutSideLogIsReadToYes(csol);

            return data;
        }

        #endregion

    }


}


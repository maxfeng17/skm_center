﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.BizLogic.Facade
{
    public class NotificationFacade
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(NotificationFacade));
        private static INotificationProvider np;
        private static IPponProvider pp;
        private static IPCPProvider pcp;
        private static IMemberProvider mp;
        private static ISysConfProvider config;
        private static ILocationProvider lp;

        public const string PushMGMCardCardType = "cardType";
        public const string PushMGMCardMsgId = "msgId";
        public const string PushMGMCardShowTab = "showTab";
        protected static readonly int _androidRegistrationIdRange;

        static NotificationFacade()
        {
            np = ProviderFactory.Instance().GetProvider<INotificationProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            config = ProviderFactory.Instance().GetConfig();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            _androidRegistrationIdRange = config.AndroidRegistrationIdRange;
        }

        #region 紀錄推播需要的使用者裝置資訊紀錄處理

        public static bool SetIDeviceTokenByType(string token, int cityId, SubscriptNotification notification, MobileOsType mobileType, string deviceId)
        {
            IdriverTokenCollection delCollection = new IdriverTokenCollection();
            //尚未找到資料，以裝置編號查詢。
            IdriverTokenCollection collection =
                np.IdriverTokenGetListe(IdriverToken.Columns.DeviceId + "=" + deviceId,
                                                           IdriverToken.Columns.MobileOsType + "=" +
                                                           (int)mobileType);

            if (collection.Count > 0)
            {
                //有資料，將token不相等的資料全部刪除
                delCollection.AddRange(collection.Where(x => x.Token != token));
                //刪除舊資料
                np.IdriverTokenDeleteList(delCollection);
            }


            IdriverToken idriverToken = new IdriverToken();
            //檢查token是否已存在
            IdriverToken oldData = np.IdriverTokenGetByTokenString(token);
            if (oldData.IsLoaded)
            {
                //有舊資料，修改內容
                idriverToken = oldData;
            }
            else
            {
                //新增紀錄
                idriverToken.Token = token;
                idriverToken.CreateTime = DateTime.Now;
            }
            idriverToken.DeviceId = deviceId;
            idriverToken.MobileOsType = (int)mobileType;
            idriverToken.CityId = cityId;
            idriverToken.ModifyTime = DateTime.Now;
            idriverToken.DeviceTokenStatus = (int)DeviceTokenStatus.Valid;
            idriverToken.Enabled = Convert.ToBoolean((int)notification);

            np.IdriverTokenSet(idriverToken);
            return true;
        }

        public static DeviceToken GetSubscriptionDeviceToken(string token, MobileOsType mobileType, string device, string trigger)
        {
            //檢查該裝置是否已存在
            var deviceIdentyfierInfo = lp.DeviceIdentyfierInfoListGet(device);

            DeviceToken deviceToken = (deviceIdentyfierInfo.Any())
                ? FindDeviceToken(token, mobileType, device, trigger) : GenerateDeviceToken(token, mobileType, device, trigger);

            return deviceToken;
        }

        public static DeviceToken GenerateDeviceToken(string token, MobileOsType mobileType, string device, string trigger)
        {

            DeviceToken deviceToken = new DeviceToken
            {
                Device = device,
                MobileOsType = (int)mobileType,
                Token = token,
                TokenStatus = (int)DeviceTokenStatus.Valid,
                CreateTime = DateTime.Now,
            };

            //增加異常監控機制
            var tokenCol = np.DeviceTokenCollectionGetListByToken(token);
            if (tokenCol != null && tokenCol.Any(x => x.TokenStatus == (int)DeviceTokenStatus.Valid))
            {
                //有效Token重覆，改用此筆Token回傳
                deviceToken = tokenCol.First(x => x.TokenStatus == (int)DeviceTokenStatus.Valid);
            }
            else
            {
                //無有效Token，新增裝置
                np.DeviceTokenSet(deviceToken);
            }
            return deviceToken;
        }
        public static DeviceToken FindDeviceToken(string token, MobileOsType mobileType, string device, string trigger)
        {
            DeviceToken deviceToken = np.DeviceTokenGet(device, mobileType);
            if (deviceToken.IsLoaded)
            {
                deviceToken.Token = token;
                deviceToken.TokenStatus = (int)DeviceTokenStatus.Valid;
                deviceToken.ModifyTime = DateTime.Now;

                np.DeviceTokenSet(deviceToken);
            }
            else
            {
                deviceToken = GenerateDeviceToken(token, mobileType, device, trigger);
            }
            return deviceToken;
        }

        #endregion 紀錄推播需要的使用者裝置資訊紀錄處理

        #region 好康訂閱推播相關
        /// <summary>
        /// 依據頻道或其下某個category的Id來進行推播的訂閱，由於可能傳入個種層級的categoryId，所以此FUNCTION會解析，若與頻道無關，則新增會失敗
        /// </summary>
        /// <param name="categoryId">categoryType為PponChannel的category或其子類別的ID</param>
        /// <param name="token">裝置推播用的token</param>
        /// <param name="enabled">設定或取消</param>
        /// <param name="mobileType">裝置的類型</param>
        /// <param name="device">裝置唯一識別</param>
        /// <returns></returns>
        public static bool SetSubscriptionNotice(int categoryId, string token, bool enabled, MobileOsType mobileType, string device)
        {
            //先設定或取出現有的使用者設備
            DeviceToken deviceToken = GetSubscriptionDeviceToken(token, mobileType, device, "SubscriptionNotice");
            //確認傳入的categoryId是否為某個頻道或頻道下的子分類
            List<int> categoryPaths = CategoryManager.PponChannelCategoryTree.GetCategoryPath(categoryId);
            //目前只能訂閱頻道、頻道下一個階層的資料，若不符合前述規定，則無法訂閱
            if ((categoryPaths.Count == 0) || (categoryPaths.Count > 2))
            {
                return false;
            }

            int channelId = categoryPaths.First();
            int? subCategoryId = null;
            if (categoryPaths.Count == 2)
            {
                subCategoryId = categoryPaths[1];
            }

            //先將所有的紀錄清為不收訊息
            pp.SubscriptionNoticeSetEnabled(deviceToken.Id, false);

            //查詢現有的紀錄
            SubscriptionNotice notice = pp.SubscriptionNoticeGet(deviceToken.Id, channelId, subCategoryId);
            if (notice.IsLoaded)
            {
                //有舊資料，進行異動
                notice.Enabled = enabled;
                notice.ModifyTime = DateTime.Now;
            }
            else
            {
                notice.DeviceId = deviceToken.Id;
                notice.ChannelCategoryId = channelId;
                notice.SubcategoryId = subCategoryId;
                notice.Enabled = enabled;
                notice.CreateTime = DateTime.Now;
            }
            pp.SubscriptionNoticeSet(notice);

            return true;
        }
        #endregion 好康訂閱推播相關


        #region 好康收藏的推播相關
        public static bool SetMemberCollectNotice(string token, bool enabled, MobileOsType mobileType, string device, int userId)
        {
            //先設定或取出現有的使用者設備
            DeviceToken deviceToken = GetSubscriptionDeviceToken(token, mobileType, device, "MemberCollectNotice");

            //開啟推播通知或取消
            if (enabled)
            {
                //移除所有舊紀錄然後新增新資料
                mp.MemberCollectNoticeRemove(deviceToken.Id);

                MemberCollectNotice notice = new MemberCollectNotice();
                notice.UserId = userId;
                notice.DeviceId = deviceToken.Id;
                notice.CreateTime = DateTime.Now;

                mp.MemberCollectNoticeSet(notice);
            }
            else
            {
                //不收到，移除所有舊紀錄
                mp.MemberCollectNoticeRemove(deviceToken.Id);
            }
            return true;
        }

        #endregion 好康收藏的推播相關


        #region 推播NotificationJob

        /// <summary>
        /// 推播指定時間內的推播訊息
        /// </summary>
        /// <param name="pushTimeStart"></param>
        /// <param name="pushTimeEnd"></param>
        public static void PushAppSchedulMessage(DateTime pushTimeStart, DateTime pushTimeEnd)
        {
            PushAppCollection pushList = np.GetPushAppListByPeriod(pushTimeStart, pushTimeEnd);

            foreach (PushApp push in pushList)
            {
                Thread thread = new Thread(() => PushAppMessage(push, false));
                thread.Start();
            }
        }

        /// <summary>
        /// 利用mq, 在規範的上班時間內發送訂單預定推播
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="message"></param>
        /// <param name="orderGuid"></param>
        public static void PushMemberOrderMessageOnWorkTime(int userId, string message, Guid orderGuid)
        {
            if (config.PersonalPushMessageEnabled)
            {
                string title = "【訂單通知】";
                Action<int, string, Guid> act =
                    delegate (int aUserId, string aMessage, Guid aOrderGuid)
                    {
                        Dictionary<string, object> args = new Dictionary<string, object>();
                        args["orderGuid"] = aOrderGuid;
                        PushMemberMessageByMq(aUserId, aMessage, ActionEventType.ShowOrderDetail, args, title);
                    };
                act.BeginInvoke(userId, message, orderGuid, null, null);
            }
        }

        /// <summary>
        /// 不在深夜擾人的推播不直接推，先存到資料表，再由job發送
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="message"></param>
        /// <param name="orderGuid"></param>
        public static void PushMember24HOrderShipmentNotification(int userId, string title, string message, Guid orderGuid)
        {
            //範例
            //NotificationFacade.PushMember24HOrderShipmentNotification(
            //    MemberFacade.GetUniqueId("unicorn@17life.com"),
            //        "訂單出貨通知",
            //        "您的訂單A0-10-323-190211-030已出貨（物流資訊：黑貓宅急便900516811481）", Guid.Parse("131223a9-6307-4dc9-b90f-d8a0738d04c9"));
            if (config.PersonalPushMessageEnabled == false)
            {
                return;
            }
            Action<int, string, Guid> act =
                delegate (int aUserId, string aMessage, Guid aOrderGuid)
                {
                    Dictionary<string, object> args = new Dictionary<string, object>();
                    args["orderGuid"] = aOrderGuid;
                    PushService ps = ProviderFactory.Instance().Resolve<PushService>(PushService._PERSON_PUSH_SERVICE);
                    ps.PushMemberMessageByQueued(userId, title, message, ActionEventType.ShowOrderDetail, args);
                };
            act.BeginInvoke(userId, message, orderGuid, null, null);
        }

        /// <summary>
        /// 推播指定的推播訊息
        /// </summary>
        /// <param name="pushId">pushId</param>
        /// <param name="pushOnly"></param>
        /// <param name="deviceTokens"></param>
        public static void PushAppSchedulMessage(int pushId, bool pushOnly,
            DeviceTokenCollection deviceTokens = null)
        {
            PushApp push = np.GetPushApp(pushId);

            Thread thread = new Thread(() => PushAppMessage(push, pushOnly, deviceTokens));
            thread.Start();
        }

        public static void PushMemberMessageForMessageCenter(int userId, string message, int cardType, string msgId,
            bool pushOnlyForTest = false)
        {
            Action<int, string, int, string, bool> act =
                delegate (int aUserId, string aMessage, int aCardType, string aMsgId, bool aPushOnlyForTest)
                {

                    Dictionary<string, object> args = new Dictionary<string, object>();
                    args[PushMGMCardShowTab] = "messageCenter";
                    PushMemberMessage(aUserId, aMessage, ActionEventType.RemotePushMessageCenter, args, PushMethodType.Push);

                    if (pushOnlyForTest == false)
                    {
                        Dictionary<string, object> args2 = new Dictionary<string, object>();
                        args2[PushMGMCardCardType] = aCardType;
                        args2[PushMGMCardMsgId] = aMsgId;
                        PushMemberMessage(aUserId, aMessage, ActionEventType.RemotePushMGMCard, args2, PushMethodType.Store);
                    }
                };
            act.BeginInvoke(userId, message, cardType, msgId, pushOnlyForTest, null, null);
        }

        //推播訂單狀態變更的訊息
        public static void PushMemberOrderMessage(int userId, string message, Guid orderGuid)
        {
            string title = "【訂單通知】";
            Action<int, string, Guid> act =
                delegate (int aUserId, string aMessage, Guid aOrderGuid)
                {
                    {
                        Dictionary<string, object> args = new Dictionary<string, object>();
                        args["orderGuid"] = aOrderGuid;
                        PushMemberMessage(aUserId, aMessage, ActionEventType.ShowOrderDetail, args, PushMethodType.Push, title);
                    }
                    {
                        Dictionary<string, object> args = new Dictionary<string, object>();
                        args["orderGuid"] = aOrderGuid;
                        PushMemberMessage(aUserId, aMessage, ActionEventType.ShowOrderDetail, args, PushMethodType.Store, title);
                    }
                };
            act.BeginInvoke(userId, message, orderGuid, null, null);
        }

        public static void PushCustomerServiceMessage(int userId, string message, string serviceNo, string orderGuid, bool pushOnlyForTest = false)
        {
            Action<int, string, bool> act =
                delegate (int aUserId, string aMessage, bool aPushOnlyForTest)
                {
                    Dictionary<string, object> args = new Dictionary<string, object>();
                    args["Type"] = "ShowServiceDetail";
                    if (string.IsNullOrEmpty(orderGuid))
                    {
                        args["serviceType"] = (int)PushMethod.serviceNo;
                        args["servicePara"] = serviceNo;
                    }
                    else
                    {
                        args["serviceType"] = (int)PushMethod.orderGuid;
                        args["servicePara"] = orderGuid;
                    }

                    PushMemberMessage(aUserId, aMessage, ActionEventType.RemotePushCustomerService, args, PushMethodType.Push);

                    if (pushOnlyForTest == false)
                    {
                        Dictionary<string, object> args2 = new Dictionary<string, object>();
                        if (string.IsNullOrEmpty(orderGuid))
                        {
                            args2["serviceType"] = (int)PushMethod.serviceNo;
                            args2["servicePara"] = serviceNo;
                        }
                        else
                        {
                            args2["serviceType"] = (int)PushMethod.orderGuid;
                            args2["servicePara"] = orderGuid;
                        }
                        PushMemberMessage(aUserId, aMessage, ActionEventType.RemotePushCustomerService, args2, PushMethodType.Store);
                    }
                };
            act.BeginInvoke(userId, message, pushOnlyForTest, null, null);
        }

        public static void PushCustomUrlMessage(int userId, string message, string customUrl)
        {
            Action<int, string, string> act =
                delegate (int aUserId, string aMessage, string aCustomUrl)
                {
                    Dictionary<string, object> args = new Dictionary<string, object>();
                    args["customUrl"] = customUrl;
                    PushMemberMessage(aUserId, aMessage, ActionEventType.RemotePushCustomUrl, args, PushMethodType.Push | PushMethodType.Store);                    
                };
            act.BeginInvoke(userId, message, customUrl, null, null);
        }

        private static void PushMemberMessage(int userId, string message, ActionEventType messageType,
            Dictionary<string, object> args, PushMethodType pushMethod, string title = null)
        {
            if (string.IsNullOrEmpty(title))
            {
                title = message;//預設做法 若有要另外給title再給值
            }

            PushService ps = ProviderFactory.Instance().Resolve<PushService>(PushService._PERSON_PUSH_SERVICE);
            ps.PushMemberMessage(userId, title, message, messageType, args, pushMethod);
        }

        private static void PushMemberMessageByMq(int userId, string message, ActionEventType messageType,
           Dictionary<string, object> args, string title)
        {
            PushService ps = ProviderFactory.Instance().Resolve<PushService>(PushService._PERSON_PUSH_SERVICE);
            ps.PushMemberMessageByMq(userId, title, message, messageType, args);
        }

        /// <summary>
        /// 推撥訊息
        /// </summary>
        /// <param name="push"></param>
        /// <param name="pushOnly"></param>
        /// <param name="deviceTokens"></param>
        public static void PushAppMessage(PushApp push, bool pushOnly, DeviceTokenCollection deviceTokens = null)
        {
            if (push.IsLoaded)
            {
                using (PushService ps = new PushService())
                {
                    ps.PushMessage2(push, pushOnly, deviceTokens);
                }
            }
        }

        #region 推播點擊
        /// <summary>
        /// 修改pushId的推播任務，將點擊數據+1，
        /// </summary>
        /// <param name="pushId"></param>
        /// <param name="deviceType"></param>
        /// <returns></returns>
        public static int PushAppViewCountAddOne(int pushId, ApiUserDeviceType deviceType)
        {
            switch (deviceType)
            {
                case ApiUserDeviceType.Android:
                    return np.PushAppAndroidViewCountAdd(pushId, 1);
                case ApiUserDeviceType.IOS:
                    return np.PushAppIOSViewCountAdd(pushId, 1);
                default:
                    //推播目前只有IOS與ANDROID
                    break;
            }
            return 0;
        }
        #endregion 推播點擊

        #region 收藏檔次即期推撥
        /// <summary>
        /// 收藏檔次即期間隔推撥通知
        /// </summary>
        /// <param name="interval">即期間隔</param>
        public static void PushMemberCollectDeal(int interval = 1)
        {
            Thread thread = new Thread(() => PushMemberCollectDealByDate(DateTime.Today.AddHours(12).AddDays(interval)));
            thread.Start();
        }

        /// <summary>
        /// 收藏檔次即期日推撥通知
        /// </summary>
        /// <param name="dueDate">即期日</param>
        public static void PushMemberCollectDeal(DateTime dueDate)
        {
            Thread thread = new Thread(() => PushMemberCollectDealByDate(dueDate));
            thread.Start();
        }

        /// <summary>
        /// 收藏檔次即期日推撥通知
        /// </summary>
        /// <param name="dueDate">即期日</param>
        private static void PushMemberCollectDealByDate(DateTime dueDate)
        {
            using (PushService ps = new PushService())
            {
                //重新定義事件處理常式
                ps.OnDeviceSubscriptionChanged += PushService_OnDeviceSubscriptionChanged;
                ps.OnDeviceSubscriptionExpired += PushService_OnDeviceSubscriptionExpired;

                //1. 取得即將到期檔次的所有個人收藏
                ViewMemberCollectDealPushCollection col = mp.ViewMemberCollectDealPushGetListByDate(dueDate);

                //建立Indexer
                Dictionary<int, List<ViewMemberCollectDealPush>> indexer = col.GroupBy(x => x.MemberUniqueId).ToDictionary(g => g.Key, g => g.ToList());

                //TODO: indexer測試
                foreach (int uid in indexer.Keys)
                {
                    var mdcs = indexer[uid];
                    var businessHourGuid = mdcs.Select(a => a.BusinessHourGuid)
                                                 .Distinct()
                                                 .ToList();



                    //2. 組織訊息
                    var msgs = (from a in mdcs
                                where a.AppNotice == true
                                select new PushMessageViewModel
                                {
                                    Bid = a.BusinessHourGuid,
                                    Bhoe = a.BusinessHourOrderTimeE,
                                    CityId = a.CityId,
                                    AppTitle = string.IsNullOrWhiteSpace(a.AppTitle) ? null : a.AppTitle,
                                    CouponUsage = string.IsNullOrWhiteSpace(a.CouponUsage) ? null : a.CouponUsage,
                                    Title = string.IsNullOrWhiteSpace(a.Title) ? null : a.Title,
                                    Name = string.IsNullOrWhiteSpace(a.Name) ? null : a.Name,
                                    SubjectName = string.IsNullOrWhiteSpace(a.SubjectName) ? null : a.SubjectName,
                                }).Distinct(new GenericComparer<PushMessageViewModel>(a => a.Bid)).ToList();


                    //取得不重複有效的註冊手機Token
                    var iosTokens = (from a in mdcs
                                     where a.TokenStatus == (int)DeviceTokenStatus.Valid
                                     && !(a.Token == null || a.Token.Trim() == string.Empty)
                                     && a.MobileOsType == (int)MobileOsType.iOS
                                     select a.Token).Distinct().ToArray();

                    var androidTokens = (from a in mdcs
                                         where a.TokenStatus == (int)DeviceTokenStatus.Valid
                                         && !(a.Token == null || a.Token.Trim() == string.Empty)
                                         && a.MobileOsType == (int)MobileOsType.Android
                                         select a.Token).Distinct().ToArray();

                    string message = string.Empty;
                    Dictionary<string, object> messageData = null;

                    //推撥iOS
                    if (iosTokens.Length > 0)
                    {
                        message = PushService.BuildMessage(msgs, MobileOsType.iOS, out messageData);
                        PushMessage(ps, iosTokens, MobileOsType.iOS, message, messageData);
                    }
                    //推撥Android
                    if (androidTokens.Length > 0)
                    {
                        message = PushService.BuildMessage(msgs, MobileOsType.Android, out messageData);
                        PushMessage(ps, androidTokens, MobileOsType.Android, message, null); //android不需要傳messageData
                    }

                    //4. 記錄推撥時間
                    foreach (var bid in businessHourGuid)
                    {
                        mp.MemberCollectDealSetPushTime(uid, bid, DateTime.Now);
                    }
                }
            }
        }

        /// <summary>
        /// 推撥訊息
        /// </summary>
        /// <param name="ps"></param>
        /// <param name="Tokens"></param>
        /// <param name="osType"></param>
        /// <param name="message"></param>
        /// <param name="messageData"></param>
        /// <returns></returns>
        private static int PushMessage(PushService ps, string[] Tokens, MobileOsType osType, string message, Dictionary<string, object> messageData)
        {
            int pushCount = 0;

            //Notification noti = null;

            switch (osType)
            {
                case MobileOsType.iOS:
                    foreach (var token in Tokens)
                    {
                        AppleNotification notification = new AppleNotification()
                            .ForDeviceToken(token)
                            .WithAlert(message)
                            .WithSound("default")
                            .WithBadge(1);

                        //塞入參數
                        foreach (var data in messageData)
                        {
                            //((AppleNotification)noti).WithCustomItem(data.Key, data.Value);
                            notification.WithCustomItem(data.Key, data.Value);
                        }

                        ps.PushNotification(notification);
                        pushCount++;
                    }

                    break;
                case MobileOsType.Android:
                    GcmNotification gcmNotification = new GcmNotification();
                    foreach (var token in Tokens)
                    {
                        gcmNotification.RegistrationIds.Add(token);
                        pushCount++;

                        if ((pushCount % _androidRegistrationIdRange) == 0)
                        {
                            ps.PushNotification(gcmNotification
                                                        .WithCollapseKey("NONE")
                                                        .WithJson(message));
                            gcmNotification = new GcmNotification();
                        }
                    }

                    if (gcmNotification.RegistrationIds.Count > 0)
                    {
                        ps.PushNotification(gcmNotification.WithJson(message));
                    }

                    break;
            }

            return pushCount;
        }

        // this class is used to compare two objects of type Usuers to remove 
        // all objects that are duplicates only by field IdUser
        class GenericComparer<T> : IEqualityComparer<T>
        {
            private Func<T, object> _funcDistinct;
            public GenericComparer(Func<T, object> funcDistinct)
            {
                this._funcDistinct = funcDistinct;
            }

            public bool Equals(T x, T y)
            {
                return _funcDistinct(x).Equals(_funcDistinct(y));
            }

            public int GetHashCode(T obj)
            {
                return this._funcDistinct(obj).GetHashCode();
            }
        }

        #endregion

        #region 事件處理常式
        /// <summary>
        /// DeviceSubscriptionExpired事件處理常式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="expiredDeviceSubscriptionId"></param>
        /// <param name="timestamp"></param>
        /// <param name="notification"></param>
        static void PushService_OnDeviceSubscriptionExpired(object sender, string expiredDeviceSubscriptionId, DateTime timestamp, INotification notification)
        {
            DeviceToken token = np.DeviceTokenGet(expiredDeviceSubscriptionId);
            if (token.IsLoaded)
            {
                token.TokenStatus = (int)DeviceTokenStatus.Expired;
                np.DeviceTokenSet(token);
            }
        }

        /// <summary>
        /// DeviceSubscriptionChanged事件處理常式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oldSubscriptionId"></param>
        /// <param name="newSubscriptionId"></param>
        /// <param name="notification"></param>
        static void PushService_OnDeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            DeviceToken token = np.DeviceTokenGet(oldSubscriptionId);
            if (token.IsLoaded)
            {
                token.Token = newSubscriptionId;
                np.DeviceTokenSet(token);
            }
        }
        #endregion



        #endregion 推播NotificationJob

        #region 推播PCPNotificationJob
        /// <summary>
        /// 推撥狀態為新任務的PcpAssignment
        /// </summary>
        public static void PushPcpAssignment()
        {
            var pushAssignments = np.PcpAssignmentGet(DateTime.Now, AssignmentStatus.Ready, AssignmentSendType.Push, AssignmentSendType.SMS);

            foreach (PcpAssignment pcp in pushAssignments)
            {
                try
                {
                    PushPcpAssignment(pcp);
                }
                catch (Exception ex)
                {
                    logger.Warn("PushPcpAssignment error pcp.Id=" + pcp.Id, ex);
                    np.PcpAssignmentSetStatus(pcp.Id, AssignmentStatus.Failure);
                }
            }
        }

        /// <summary>
        /// 推撥單則PcpAssignment
        /// </summary>
        /// <param name="pcp"></param>
        public static void PushPcpAssignment(PcpAssignment pcp)
        {
            //Thread thread = new Thread(() => PushPcpMessage(pcp));
            //thread.Start();
            //用 Thread 的:
            //不用 Thread:
            //  如果系統重啟，目前的code無法等待thread 都完成
            //
            np.PcpAssignmentSetStatus(pcp.Id, AssignmentStatus.Executing);
            PushPcpMessage(pcp);
        }

        /// <summary>
        /// 推撥PcpAssignment
        /// </summary>
        /// <param name="pcpa"></param>
        private static void PushPcpMessage(PcpAssignment pcpa)
        {
            if (pcpa.IsLoaded)
            {
                if (pcpa.SendType == (int)AssignmentSendType.Push)
                {
                    using (PushService ps = new PushService())
                    {
                        ps.PushMessage(pcpa);
                    }
                }
                else if (pcpa.SendType == (int)AssignmentSendType.SMS)
                {
                    SmsPcpMessage(pcpa);
                }
                np.PcpAssignmentSetStatus(pcpa.Id, AssignmentStatus.Complete);
            }
        }

        private static void SmsPcpMessage(PcpAssignment pcpa)
        {
            if (pcpa.SendType != (int)AssignmentSendType.SMS)
            {
                throw new Exception("SmsPcpMessage只處理簡訊");
            }

            List<PcpAssignmentMember> assignmentMembers = np.PcpAssignmentMemberGetList(pcpa.Id);
            Dictionary<string, PcpAssignmentMemberModel> assignmentMemberModels = new Dictionary<string, PcpAssignmentMemberModel>();
            foreach (PcpAssignmentMember assMem in assignmentMembers)
            {
                PcpAssignmentMemberModel model = new PcpAssignmentMemberModel
                {
                    AssignmentMemberId = assMem.Id,
                    MemberId = assMem.MemberId
                };
                if (assMem.MemberType == (int)AssignmentMemberType.WebSiteMember)
                {
                    model.MemberType = AssignmentMemberType.WebSiteMember;
                    Member mem = MemberFacade.GetMember(assMem.MemberId);
                    if (mem.IsLoaded == false)
                    {
                        logger.WarnFormat("SellerMember資料找不到, Memberid={0}, PcpAssignmentId={1}", assMem.MemberId, pcpa.Id);
                        continue;
                    }
                    model.Mobile = mem.Mobile;
                }
                else if (assMem.MemberType == (int)AssignmentMemberType.SellerMember)
                {
                    model.MemberType = AssignmentMemberType.SellerMember;
                    SellerMember mem = pcp.SellerMemberGet(assMem.MemberId);
                    if (mem.IsLoaded == false)
                    {
                        logger.WarnFormat("SellerMember資料找不到, SellerMemberid={0}, PcpAssignmentId={1}", assMem.MemberId, pcpa.Id);
                        continue;
                    }
                    model.Mobile = mem.Mobile;
                }
                assignmentMemberModels.Add(model.Mobile, model);
            }
            if (assignmentMemberModels.Count > 0)
            {
                Guid taskId = Guid.NewGuid();
                new SMS().SendMessage(pcpa.Message, assignmentMemberModels.Keys.ToArray(), taskId);

                List<SmsLog> smsLogs = pp.SmsLogGetListByTaskId(taskId);

                //update smslog_id
                DateTime now = DateTime.Now;
                foreach (SmsLog smsLog in smsLogs)
                {
                    if (assignmentMemberModels.ContainsKey(smsLog.Mobile))
                    {
                        PcpAssignmentMember assmem = np.PcpAssignmentMemberGet(assignmentMemberModels[smsLog.Mobile].AssignmentMemberId);
                        if (assmem.IsLoaded)
                        {
                            assmem.SmsLogId = smsLog.Id;
                            assmem.SendTime = now;
                            np.PcpAssignmentMemberSet(assmem);
                        }
                    }
                }
            }

        }

        #endregion

        public static UserDeviceInfoModel GetDefaultUserDeviceInfoModel(int userId)
        {
            return np.GetUserDeviceInfoModels(userId)
                .OrderByDescending(t => t.LastUsedTime).FirstOrDefault();
        }


        /// <summary>
        /// 推撥訊息字典 (與訊息中心共用)
        /// </summary>
        /// <param name="actionEventPushMsg"></param>
        /// <returns></returns>
        public static dynamic GetPokeball(ActionEventPushMessage actionEventPushMsg)
        {
            return GetPokeball(ActionEventPushMessageModel.Create(actionEventPushMsg));
        }
        public static dynamic GetPokeball(ViewPushRecordMessage viewPushRecordMessage)
        {
            return GetPokeball(ActionEventPushMessageModel.Create(viewPushRecordMessage));
        }
        public static dynamic GetPokeball(ActionEventPushMessageModel actionEventPushMsg)
        {
            dynamic msg = null;
            switch (actionEventPushMsg.EventType)
            {
                case (int)ActionEventType.BeaconPush:
                case (int)ActionEventType.RemotePushBid:
                    if (actionEventPushMsg.BusinessHourGuid.HasValue)
                    {
                        var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(actionEventPushMsg.BusinessHourGuid.Value);
                        if (DateTime.Now < deal.BusinessHourOrderTimeE)
                        {
                            msg = new PushBidPokeball { BId = (Guid)actionEventPushMsg.BusinessHourGuid };
                        }
                    }
                    break;
                case (int)ActionEventType.RemotePushChannel:
                    msg = new PushChannelPokeball { ChannelId = actionEventPushMsg.ChannelId, Area = actionEventPushMsg.AreaId, Reload = false };
                    break;
                case (int)ActionEventType.RemotePushVourcher:
                    msg = new PushVourcherPokeball { VourcherId = actionEventPushMsg.VourcherId };
                    break;
                case (int)ActionEventType.RemotePushSeller:
                    msg = new PushSellerPokeball { SellerId = actionEventPushMsg.SellerId };
                    break;
                case (int)ActionEventType.RemotePushEvnetPromo:
                    if (actionEventPushMsg.EventPromoId != null)
                    {
                        var promo = pp.GetEventPromo((int)actionEventPushMsg.EventPromoId);
                        if (promo != null && promo.EndDate > DateTime.Now)
                        {
                            if (config.EnableCurationTwoInApp)
                            {
                                msg = new PushCurationPokeball { EventId = (int)actionEventPushMsg.EventPromoId, EventType = promo.Type };
                            }
                            else
                            {
                                msg = new PushEventPromoPokeball { EventPromoId = (int)actionEventPushMsg.EventPromoId };
                            }
                        }
                    }
                    break;
                case (int)ActionEventType.RemotePushBrandPromo:
                    if (actionEventPushMsg.BrandPromoId != null && config.EnableCurationTwoInApp)
                    {
                        var brand = pp.GetBrand((int)actionEventPushMsg.BrandPromoId);
                        if (brand != null && brand.EndTime > DateTime.Now)
                        {
                            msg = new PushCurationPokeball { EventId = (int)actionEventPushMsg.BrandPromoId, EventType = (int)EventPromoEventType.CurationTwo };
                        }
                    }
                    break;
                case (int)ActionEventType.RemotePushCustomUrl:
                    msg = new PushCustomUrlPokeball { Url = actionEventPushMsg.CustomUrl };
                    break;
                case (int)ActionEventType.RemotePushMessageCenter:
                    msg = new { Type = "ShowMessageCenter" };
                    break;
                case (int)ActionEventType.RemotePushMGMCard:
                    msg = new
                    {
                        Type = "ShowMGMCard",
                        CardType = actionEventPushMsg.CardType.GetValueOrDefault(),
                        MsgId = actionEventPushMsg.MsgId
                    };
                    break;
                case (int)ActionEventType.RemotePushCustomerService:
                    msg = new
                    {
                        Type = "ShowServiceDetail",
                        ServiceType = actionEventPushMsg.ServiceType,
                        ServicePara = actionEventPushMsg.ServicePara
                    };
                    break;
                case (int)ActionEventType.ShowOrderDetail:
                    if (actionEventPushMsg.OrderGuid != null)
                    {
                        msg = new
                        {
                            Type = "ShowOrderDetail",
                            Guid = actionEventPushMsg.OrderGuid.Value
                        };
                    }
                    break;
            }


            return msg;
        }
        /// <summary>
        /// 產生要求app告知訊息已讀的pokeball
        /// </summary>
        /// <param name="pushRecordId"></param>
        /// <returns></returns>
        public static dynamic GetPokeballForReadMessage(int pushRecordId)
        {
            return new
            {
                Type = "MarkMessageRead",
                MessageId = pushRecordId,
                TriggerType = 1
            };
        }
    }

    #region class

    public class PcpAssignmentMemberModel
    {
        public long AssignmentMemberId { get; set; }
        public string Mobile { get; set; }
        public int MemberId { get; set; }
        public AssignmentMemberType MemberType { get; set; }

        public override string ToString()
        {
            return AssignmentMemberId.ToString();
        }

        public override int GetHashCode()
        {
            return AssignmentMemberId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            PcpAssignmentMemberModel castObj = obj as PcpAssignmentMemberModel;
            if (castObj == null)
            {
                return false;
            }

            return GetHashCode().Equals(castObj.GetHashCode());
        }
    }

    public class ActionEventPushMessageModel
    {
        public static ActionEventPushMessageModel Create(ActionEventPushMessage item)
        {
            return new ActionEventPushMessageModel
            {
                BusinessHourGuid = item.BusinessHourGuid,
                EventType = item.EventType,
                ChannelId = item.ChannelId,
                AreaId = item.AreaId,
                SellerId = item.SellerId,
                VourcherId = item.VourcherId,
                EventPromoId = item.EventPromoId,
                BrandPromoId = item.BrandPromoId,
                CustomUrl = item.CustomUrl,
                CardType = item.CardType,
                MsgId = item.MsgId,
                ServiceType = item.ServiceType,
                ServicePara = item.ServicePara,
                OrderGuid = item.OrderGuid
            };
        }

        public static ActionEventPushMessageModel Create(ViewPushRecordMessage item)
        {
            return new ActionEventPushMessageModel
            {
                BusinessHourGuid = item.BusinessHourGuid,
                EventType = item.EventType,
                ChannelId = item.ChannelId,
                AreaId = item.AreaId,
                SellerId = item.SellerId,
                VourcherId = item.VourcherId,
                EventPromoId = item.EventPromoId,
                BrandPromoId = item.BrandPromoId,
                CustomUrl = item.CustomUrl,
                CardType = item.CardType,
                MsgId = item.MsgId,
                ServiceType = item.ServiceType,
                ServicePara = item.ServicePara,
                OrderGuid = item.OrderGuid
            };
        }

        public Guid? BusinessHourGuid { get; set; }
        public int EventType { get; set; }
        public int? ChannelId { get; set; }
        public int? AreaId { get; set; }
        public string SellerId { get; set; }
        public int? VourcherId { get; set; }
        public int? EventPromoId { get; set; }
        public int? BrandPromoId { get; set; }
        public string CustomUrl { get; set; }
        public int? CardType { get; set; }
        public string MsgId { get; set; }
        public int? ServiceType { get; set; }
        public string ServicePara { get; set; }
        public Guid? OrderGuid { get; set; }
    }

    //TODO code 再找其它地方放置
    [Flags]
    public enum PushMethodType
    {
        None = 0,
        Push = 1,
        Store = 2
    }

    #endregion
}
﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Transactions;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.Payment;
using LunchKingSite.Core.Component.Template;
using PaymentType = LunchKingSite.Core.PaymentType;
using Peztemp = LunchKingSite.DataOrm.Peztemp;
using System.Text.RegularExpressions;
using System.Web;
using LunchKingSite.Core.ModelCustom;
using System.Net;
using LunchKingSite.BizLogic.Models;

namespace LunchKingSite.BizLogic.Facade
{
    public class PaymentFacade
    {
        protected static IOrderProvider op;
        protected static IPponProvider pp;
        protected static IMemberProvider mp;
        protected static ISysConfProvider cp;
        protected static IVerificationProvider vp;
        protected static IHiDealProvider hp;
        private static ILocationProvider lp;
        private static IItemProvider ip;
        private static ISellerProvider sp;
        private static IHumanProvider hmp;
        private static ISerializer serializer;


        private static ILog logger = LogManager.GetLogger(typeof(PaymentFacade));

        private static readonly string TimeFormate = @"{0:yyyy/MM/dd HH:mm}";
        private const int DateMinute = 1440;//一天的分鐘數
        private static object CouponLock = new object();
        private const string _PAYMENT_DTO_KEY = "paymentDto";
        static PaymentFacade()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            cp = ProviderFactory.Instance().GetConfig();
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();            
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            hmp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            serializer = ProviderFactory.Instance().GetSerializer();
        }

        #region Payment基本控制

        public static PaymentTransaction NewTransaction(string transId, Guid orderGuid, PaymentType paymentType, decimal amt,
            string authCode, PayTransType transType, DateTime transTime, string createId, string msg, int status, OrderClassification classification = OrderClassification.LkSite, int? receivableId = null)
        {
            return NewTransaction(transId, orderGuid, paymentType, amt, authCode, transType, transTime, createId, msg, status, PayTransResponseType.Initial, classification, receivableId);
        }

        public static PaymentTransaction NewTransaction(string transId, Guid orderGuid, PaymentType paymentType, decimal amt, string authCode,
            PayTransType transType, DateTime transTime, string createId, string msg, int status, PayTransResponseType result, 
            OrderClassification classification = OrderClassification.LkSite, 
            int? receivableId = null)
        {
            PaymentTransaction pt = new PaymentTransaction();

            pt.TransId = transId;
            pt.OrderGuid = orderGuid;
            pt.PaymentType = (int)paymentType;
            pt.Amount = amt;
            pt.AuthCode = authCode;
            pt.TransType = (int)transType;
            pt.TransTime = transTime;
            pt.CreateId = createId;
            pt.Message = msg;
            pt.Status = status;
            pt.Result = (int)result;
            pt.OrderClassification = (int)classification;
            pt.ReceivableId = receivableId;
            
            op.PaymentTransactionInsert(pt);
            return pt;
        }

        public static PaymentTransaction NewTransaction(string transId, Guid orderGuid, PaymentType paymentType, decimal amt, string authCode,
            PayTransType transType, DateTime transTime, string createId, string msg, int status, PayTransResponseType result,
            OrderClassification classification, int? receivableId, PaymentAPIProvider apiProvider)
        {
            PaymentTransaction pt = new PaymentTransaction();

            pt.TransId = transId;
            pt.OrderGuid = orderGuid;
            pt.PaymentType = (int)paymentType;
            pt.Amount = amt;
            pt.AuthCode = authCode;
            pt.TransType = (int)transType;
            pt.TransTime = transTime;
            pt.CreateId = createId;
            pt.Message = msg;
            pt.Status = status;
            pt.Result = (int)result;
            pt.OrderClassification = (int)classification;
            pt.ReceivableId = receivableId;
            pt.ApiProvider = (int)apiProvider;

            op.PaymentTransactionInsert(pt);
            return pt;
        }

        public static void UpdateTransaction(string transId, Guid? orderGuid, PaymentType paymentType, decimal amt,
                                             string authCode, PayTransType transType, DateTime transTime, string msg, int status, int result)
        {
            PaymentTransaction pt = op.PaymentTransactionGet(transId, paymentType, transType);
            if (pt.IsLoaded == false)
            {
                string errorMessage = string.Format("UpdateTransaction error. orderGuid={0}, paymentType={1}, transId={2}",
                    orderGuid, paymentType, transId);
                logger.Warn(errorMessage);
                throw new Exception(errorMessage);
            }
            pt.OrderGuid = orderGuid;
            pt.Amount = amt;
            pt.AuthCode = authCode;
            pt.TransTime = transTime;
            pt.Message = msg;
            pt.Result = result;
            pt.Status = status;

            op.PaymentTransactionSet(pt);
        }

        public static bool UpdateTransaction(int id, Guid? orderGuid, decimal amt, string authCode, DateTime transTime, string msg, int status,
            int result)
        {
            PaymentTransaction pt = op.PaymentTransactionGet(id);
            if (pt.IsLoaded == false)
            {
                string errorMessage = string.Format("UpdateTransaction error. id={0}", id);
                logger.Warn(errorMessage);
                return false;
            }
            pt.OrderGuid = orderGuid;
            pt.Amount = amt;
            pt.AuthCode = authCode;
            pt.TransTime = transTime;
            pt.Message = msg;
            pt.Result = result;
            pt.Status = status;
            op.PaymentTransactionSet(pt);
            return true;
        }

        public static bool UpdateTransaction(int id, int status)
        {
            PaymentTransaction pt = op.PaymentTransactionGet(id);
            pt.Status = status;
            op.PaymentTransactionSet(pt);
            return true;
        }

        public static void UpdateTransaction(string transId, Guid? orderGuid, PaymentType paymentType, decimal amt,
            string authCode, PayTransType transType, DateTime transTime, string msg, int status, int result,
            PaymentAPIProvider apiProvider, int? bankId = null, int? installment = null)
        {
            PaymentTransaction pt = op.PaymentTransactionGet(transId, paymentType, transType);
            if (pt.IsLoaded == false)
            {
                string errorMessage = string.Format("UpdateTransaction error. orderGuid={0}, paymentType={1}, transId={2}",
                    orderGuid, paymentType, transId);
                logger.Warn(errorMessage);
                throw new Exception(errorMessage);
            }
            pt.OrderGuid = orderGuid;
            pt.Amount = amt;
            pt.AuthCode = authCode;
            pt.TransTime = transTime;
            pt.Message = msg;
            pt.Result = result;
            pt.Status = status;
            pt.ApiProvider = (int)apiProvider;
            pt.BankId = bankId;
            pt.Installment = installment;
            op.PaymentTransactionSet(pt);
        }

        public static PaymentTransactionCollection GetPaymentTransactionListForRefundingAndUpdateStatusToRequested(bool manual = false)
        {
            var pts = op.PaymentTransactionGetListForRefunding(manual);
            foreach (var pt in pts)
            {
                pt.Status = Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Requested);
            }
            op.PaymentTransactionSetAll(pts);
            return pts;
        }

        #endregion Payment基本控制

        public static string SaveOrderStatusLog(OrderLogStatus situation, string message, Guid oid, string username)
        {
            return SaveOrderStatusLog(situation, message, oid, username, null);
        }

        public static string SaveOrderStatusLog(OrderLogStatus situation, string message, Guid oid, string username, string couponIds)
        {
            var olCol = op.ViewPponOrderStatusLogGetListPaging(1, -1, ViewPponOrderStatusLog.Columns.OrderLogCreateTime + " desc ",
                                              ViewPponOrderStatusLog.Columns.OrderGuid + " = " + oid);
            if ((olCol.Count == 0) || (olCol.Count > 0 && (olCol.First().OrderLogStatus != (int)situation ||
                                                           olCol.First().OrderLogMessage != message ||
                                                           olCol.First().OrderLogCreateTime.Date != DateTime.Now.Date)))
            {
                OrderStatusLog orderLog = new OrderStatusLog();
                orderLog.OrderGuid = oid;
                orderLog.Status = (int)situation;
                orderLog.CreateId = username;
                orderLog.CreateTime = DateTime.Now;
                orderLog.Message = message;
                orderLog.CouponIds = couponIds;
                op.OrderStatusLogSet(orderLog);
            }

            return string.Empty;
        }

        public static bool SaveOrderStatusLog(OrderReturnList otl)
        {
            var status = OrderFacade.TransferOrderReturnStatusToOrderLogStatus((OrderReturnStatus)otl.Status);

            var orderLog = new OrderStatusLog
            {
                OrderGuid = otl.OrderGuid,
                OrderReturnId = otl.Id,
                Status = (int)status,
                CreateId = otl.ModifyId ?? otl.CreateId,
                CreateTime = otl.ModifyTime ?? otl.CreateTime,
                Message = otl.Message,
                Reason = otl.Reason,
                CouponIds = otl.CouponIds,
                VendorProgressStatus = otl.VendorProgressStatus,
                VendorProcessTime = otl.VendorProcessTime,
                OrderShipId = otl.OrderShipId,
                MessageUpdate = otl.MessageUpdate
            };

            return op.OrderStatusLogSet(orderLog);            
        }

        public static bool CancelPaymentTransactionByCouponForTrust(CashTrustLog ct, string createId)
        {
            return CancelPaymentTransactionByCouponForTrust(ct, createId, false);
        }

        public static bool CancelPaymentTransactionByCouponForTrust(CashTrustLog ct, string createId, bool isForced, OrderClassification orderClassification = OrderClassification.LkSite)
        {
            PaymentTransactionCollection ptc = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                                PaymentTransaction.Columns.OrderGuid + " = " + ct.OrderGuid,
                                                PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Authorization);
            //增加判斷品生活跟17Life的查詢 edited by jaguar
            if (orderClassification == OrderClassification.LkSite)
            {
                return CancelPaymentTransactionByCouponForTrustByLkSite(ct, createId, isForced, ptc);
            }
            else
            {
                return CancelPaymentTransactionByCouponForTrustByHiDeal(ct, createId, isForced, ptc);
            }
        }

        /// <summary>
        /// 品生活的退貨
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="createId"></param>
        /// <param name="isForced"></param>
        /// <param name="ptc"></param>
        /// <returns></returns>
        private static bool CancelPaymentTransactionByCouponForTrustByHiDeal(CashTrustLog ct, string createId, bool isForced, PaymentTransactionCollection ptc)
        {
            DepartmentTypes dType = (ptc.Count > 0) ? Helper.GetPaymentTransactionDepartmentTypes(ptc.First().Status) : DepartmentTypes.Ppon;
            HiDealOrder o = hp.HiDealOrderGet(ct.OrderGuid);
            ViewHiDealCoupon deal = hp.ViewHiDealCouponGet(ct.CouponId.Value);

            if (isForced || ct.Status == (int)TrustStatus.Initial || ct.Status == (int)TrustStatus.Trusted
                            || (int.Equals((int)DeliveryType.ToHouse, deal.DeliveryType) && ct.Status == (int)TrustStatus.Verified))
            {
                if (ptc.Count() > 0 && ct.Amount > 0)
                {
                    IEnumerable<PaymentTransaction> ptPcashCol = from i in ptc where (i.PaymentType == (int)PaymentType.PCash) select i;
                    PaymentTransaction ptPcash = (ptPcashCol.Count() > 0) ? ptPcashCol.First() : null;

                    #region 處理購物金退回

                    if (Helper.IsFlagSet(o.OrderStatus, OrderStatus.ATMOrder) && ct.Scash + ct.Atm > 0)
                    {
                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                        NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.SCash, ct.Scash + ct.Atm, null,
                                PayTransType.Refund, DateTime.Now, createId, "以購物金退貨", status, PayTransResponseType.OK);

                        #region New Scash

                        OrderFacade.ScashReturned(ct.Scash + ct.Atm, o.UserId, ct.OrderGuid, "以購物金退貨:" + deal.SellerName, createId, OrderClassification.LkSite, ct.UninvoicedAmount, IsUnivoicedAmountInvoiced(ct.CouponId));

                        #endregion New Scash

                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退購物金計NT$" + (ct.Scash + ct.Atm).ToString("F0"), createId, true);
                    }
                    else if (ct.Scash + ct.CreditCard > 0)
                    {
                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                        NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.SCash, ct.Scash + ct.CreditCard, null,
                            PayTransType.Refund, DateTime.Now, createId, "以購物金退貨", status, PayTransResponseType.OK);

                        #region New Scash

                        OrderFacade.ScashReturned(ct.Scash + ct.CreditCard, o.UserId, ct.OrderGuid, "以購物金退貨:" + deal.SellerName, createId, OrderClassification.LkSite, ct.UninvoicedAmount, IsUnivoicedAmountInvoiced(ct.CouponId));

                        #endregion New Scash

                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退購物金計NT$" + (ct.Scash + ct.CreditCard).ToString("F0"), createId, true);
                    }

                    #endregion 處理購物金退回

                    #region 處理Pcash退回

                    if (ptPcash != null && ct.Pcash > 0)
                    {
                        string pezResult = PCashWorker.DeCheckOut(ptc.First().TransId, ptPcash.AuthCode, ct.Pcash);
                        PayTransResponseType responseType = PayTransResponseType.OK;
                        PayTransPhase transPhase = PayTransPhase.Created;
                        if (pezResult == "0000")
                        {
                            transPhase = PayTransPhase.Successful;
                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退PCash計NT$" + ct.Pcash.ToString("F0"), createId, true);
                        }
                        else
                        {
                            transPhase = PayTransPhase.Failed;
                            responseType = PayTransResponseType.GenericError;
                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退PCash失敗" + pezResult, createId, true);
                        }
                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), transPhase);
                        NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.PCash, ct.Pcash, ptPcash.AuthCode, PayTransType.Refund, DateTime.Now, createId, pezResult, status, responseType);
                    }

                    #endregion 處理Pcash退回

                    #region 處理紅利退回

                    if (ct.Bcash > 0)
                    {
                        int exchangeRate = 10;
                        MemberPromotionDeposit addBonus = new MemberPromotionDeposit();
                        addBonus.OrderGuid = ct.OrderGuid;
                        addBonus.StartTime = DateTime.Now;
                        addBonus.ExpireTime = DateTime.Now.AddYears(2);
                        addBonus.PromotionValue = (double)ct.Bcash * exchangeRate;
                        addBonus.CreateTime = DateTime.Now;
                        addBonus.UserId = o.UserId;

                        if (o.IsLoaded && o.OrderId != null)
                        {
                            addBonus.Action = "取消訂單:" + deal.Name.TrimToMaxLength(20, "...");
                        }
                        else
                        {
                            addBonus.Action = "紅利退貨";
                        }

                        addBonus.CreateId = "sys";
                        mp.MemberPromotionDepositSet(addBonus);

                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                        NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.BonusPoint, ct.Bcash, null,
                                                        PayTransType.Refund, DateTime.Now, createId, "以紅利退貨", status, PayTransResponseType.OK);
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退紅利計NT$" + ct.Bcash.ToString("F0"), createId, true);
                    }

                    #endregion 處理紅利退回

                    #region 信託紀錄

                    #region CashTrustLog

                    ct.MarkOld();
                    ct.ModifyTime = DateTime.Now;
                    ct.ReturnedTime = ct.ModifyTime;

                    if (isForced)
                    {
                        if (Helper.IsFlagSet((TrustSpecialStatus)ct.SpecialStatus, TrustSpecialStatus.VerificationLost))
                        {
                            ct.SpecialStatus -= (int)TrustSpecialStatus.VerificationLost;
                        }
                        ct.SpecialStatus |= (int)TrustSpecialStatus.ReturnForced;
                        ct.SpecialOperatedTime = DateTime.Now;
                    }
                    ct.Status = (int)TrustStatus.Returned;
                    mp.CashTrustLogSet(ct);

                    #endregion CashTrustLog

                    #region CashTrustStatusLog

                    CashTrustStatusLog ctStatus = new CashTrustStatusLog();
                    ctStatus.TrustId = ct.TrustId;
                    ctStatus.TrustProvider = ct.TrustProvider;
                    ctStatus.Amount = ct.CreditCard + ct.Atm + ct.Scash;
                    ctStatus.Status = (int)TrustStatus.Returned;
                    ctStatus.ModifyTime = DateTime.Now;
                    ctStatus.CreateId = createId;
                    if (isForced)
                    {
                        ctStatus.SpecialStatus = ct.SpecialStatus;
                        ctStatus.SpecialOperatedTime = DateTime.Now;
                    }
                    ctStatus.MarkNew();
                    mp.CashTrustStatusLogSet(ctStatus);

                    #endregion CashTrustStatusLog

                    if (ct.CreditCard > 0)
                    {
                        #region UserCashTrustLog

                        UserCashTrustLog ctUser = new UserCashTrustLog();
                        ctUser.UserId = ct.UserId;
                        ctUser.Amount = ct.CreditCard;
                        ctUser.BankStatus = (int)TrustBankStatus.Initial;
                        ctUser.CreateTime = DateTime.Now;
                        ctUser.Message = "以購物金代替刷卡金退回";
                        ctUser.TrustProvider = ct.TrustProvider;
                        ctUser.TrustId = ct.TrustId;
                        //ctUser.CouponId = ct.CouponId;
                        //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                        ctUser.Type = (int)UserTrustType.Return;
                        ctUser.MarkNew();
                        mp.UserCashTrustLogSet(ctUser);

                        #endregion UserCashTrustLog
                    }

                    if (ct.Atm > 0)
                    {
                        #region UserCashTrustLog

                        UserCashTrustLog ctUser = new UserCashTrustLog();
                        ctUser.UserId = ct.UserId;
                        ctUser.Amount = ct.Atm;
                        ctUser.BankStatus = (int)TrustBankStatus.Initial;
                        ctUser.CreateTime = DateTime.Now;
                        ctUser.Message = "以購物金代替ATM退回";
                        ctUser.TrustProvider = ct.TrustProvider;
                        ctUser.TrustId = ct.TrustId;
                        //ctUser.CouponId = ct.CouponId;
                        //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                        ctUser.Type = (int)UserTrustType.Return;
                        ctUser.MarkNew();
                        mp.UserCashTrustLogSet(ctUser);

                        #endregion UserCashTrustLog
                    }

                    if (ct.Scash > 0)
                    {
                        #region UserCashTrustLog

                        UserCashTrustLog ctUser = new UserCashTrustLog();
                        ctUser.UserId = ct.UserId;
                        ctUser.Amount = ct.Scash;
                        ctUser.BankStatus = (int)TrustBankStatus.Initial;
                        ctUser.CreateTime = DateTime.Now;
                        ctUser.Message = "退回既有購物金";
                        ctUser.TrustProvider = ct.TrustProvider;
                        ctUser.TrustId = ct.TrustId;
                        //ctUser.CouponId = ct.CouponId;
                        //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                        ctUser.Type = (int)UserTrustType.Return;
                        ctUser.MarkNew();
                        mp.UserCashTrustLogSet(ctUser);

                        #endregion UserCashTrustLog
                    }

                    #endregion 信託紀錄

                    #region 取消核銷狀態

                    if (isForced && ct.CouponId != null)
                    {
                        vp.UndoVerificationLog(ct.CouponId.Value, OrderClassification.HiDeal);
                        op.CancelEinvoiceSetByUndo(ct.OrderGuid, ct.CouponId.Value, false);
                    } 

                    #endregion 取消核銷狀態

                    return true;
                }
                else
                {
                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "查無付款紀錄或付款總額為零，無法退貨", createId, true);
                }
            }
            else
            {
                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "憑證編號#" + ct.CouponSequenceNumber + "狀態無法退貨", createId, true);
            }

            return false;
        }

        private static bool CancelPaymentTransactionByCouponForTrustByLkSite(CashTrustLog ct, string createId, bool isForced, PaymentTransactionCollection ptc)
        {
            //KEY:主要控制下面"退回"dType
            DepartmentTypes dType = (ptc.Count > 0) ? Helper.GetPaymentTransactionDepartmentTypes(ptc.First().Status) : DepartmentTypes.Ppon;
            Order o = op.OrderGet(ct.OrderGuid);
            ViewPponDeal deal = pp.ViewPponDealGet(o.ParentOrderId.Value);

            if (isForced || ct.Status == (int)TrustStatus.Initial || ct.Status == (int)TrustStatus.Trusted
                || (deal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, deal.DeliveryType.Value) && ct.Status == (int)TrustStatus.Verified))
            {
                //新增天貓退貨、新光退貨(0元檔，但可部分或全退)
                if (ptc.Count() > 0 && (ct.Amount > 0 || (deal.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0) || Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) || Helper.IsFlagSet(deal.GroupOrderStatus.Value, GroupOrderStatus.SKMDeal))
                {
                    IEnumerable<PaymentTransaction> ptPcashCol = from i in ptc where (i.PaymentType == (int)PaymentType.PCash) select i;
                    PaymentTransaction ptPcash = (ptPcashCol.Count() > 0) ? ptPcashCol.First() : null;

                    #region 處理退款
                    int returnedScash = ct.Scash + ct.CreditCard + ct.Atm + ct.Tcash + ct.FamilyIsp + ct.SevenIsp;

                    #region 處理購物金退回


                    if (returnedScash > 0)
                    {
                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                        NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.SCash, returnedScash, null,
                            PayTransType.Refund, DateTime.Now, createId, "以購物金退貨", status, PayTransResponseType.OK);

                        OrderFacade.ScashReturned(returnedScash, o.UserId, ct.OrderGuid, "以購物金退貨:" + o.SellerName, 
                            createId, OrderClassification.LkSite, ct.UninvoicedAmount, IsUnivoicedAmountInvoiced(ct.CouponId));

                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退購物金計NT$" + returnedScash.ToString("F0"), createId, true);
                    }

                    #endregion 處理購物金退回

                    #region 處理Pcash退回

                    if (ptPcash != null && ct.Pcash > 0)
                    {
                        string pezResult = PCashWorker.DeCheckOut(ptc.First().TransId, ptPcash.AuthCode, ct.Pcash);
                        PayTransResponseType responseType = PayTransResponseType.OK;
                        PayTransPhase transPhase = PayTransPhase.Created;
                        if (pezResult == "0000")
                        {
                            transPhase = PayTransPhase.Successful;
                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退PCash計NT$" + ct.Pcash.ToString("F0"), createId, true);
                        }
                        else
                        {
                            transPhase = PayTransPhase.Failed;
                            responseType = PayTransResponseType.GenericError;
                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退PCash失敗" + pezResult, createId, true);
                        }
                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), transPhase);
                        NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.PCash, ct.Pcash, ptPcash.AuthCode, PayTransType.Refund, DateTime.Now, createId, pezResult, status, responseType);
                    }

                    #endregion 處理Pcash退回

                    #region 處理Pscash退回

                    if (ct.Pscash > 0)
                    {
                        OrderFacade.PscashReturned(ct.Pscash, o.UserId, ct.OrderGuid, "以Pscash退貨:" + o.SellerName, createId);

                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                        NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.Pscash, ct.Pscash, null,
                                                        PayTransType.Refund, DateTime.Now, createId, "以Pscash退貨", status, PayTransResponseType.OK);
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退Pscash計NT$" + ct.Pscash.ToString("F0"), createId, true);
                    }

                    #endregion 處理紅利退回

                    #region 處理紅利退回

                    if (ct.Bcash > 0)
                    {
                        int exchangeRate = 10;
                        MemberPromotionDeposit addBonus = new MemberPromotionDeposit();
                        addBonus.OrderGuid = ct.OrderGuid;
                        addBonus.StartTime = DateTime.Now;
                        addBonus.ExpireTime = DateTime.Now.AddYears(2);
                        addBonus.PromotionValue = (double)ct.Bcash * exchangeRate;
                        addBonus.CreateTime = DateTime.Now;
                        addBonus.UserId = o.UserId;

                        if (o.IsLoaded && o.ParentOrderId != null)
                        {
                            addBonus.Action = "取消訂單:" + deal.ItemName.TrimToMaxLength(20, "...");
                        }
                        else
                        {
                            addBonus.Action = "紅利退貨";
                        }

                        addBonus.CreateId = "sys";
                        mp.MemberPromotionDepositSet(addBonus);

                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                        NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.BonusPoint, ct.Bcash, null,
                                                        PayTransType.Refund, DateTime.Now, createId, "以紅利退貨", status, PayTransResponseType.OK);
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退紅利計NT$" + ct.Bcash.ToString("F0"), createId, true);
                    }

                    #endregion 處理紅利退回

                    #region 處理Lcash退回

                    if (ct.Lcash > 0)
                    {
                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                        NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.LCash, ct.Lcash, null, PayTransType.Refund, DateTime.Now, createId, "代銷退貨", status, PayTransResponseType.OK);
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "代銷退貨金額計NT$" + ct.Lcash.ToString("F0"), createId, true);
                    }

                    #endregion 處理Lcash退回

                    #endregion 處理退款

                    #region 信託紀錄

                    #region CashTrustLog

                    ct.MarkOld();
                    ct.ModifyTime = DateTime.Now;
                    ct.ReturnedTime = ct.ModifyTime;

                    if (isForced)
                    {
                        if (Helper.IsFlagSet((TrustSpecialStatus)ct.SpecialStatus, TrustSpecialStatus.VerificationLost))
                        {
                            ct.SpecialStatus -= (int)TrustSpecialStatus.VerificationLost;
                        }
                        ct.SpecialStatus |= (int)TrustSpecialStatus.ReturnForced;
                        ct.SpecialOperatedTime = DateTime.Now;
                    }
                    ct.Status = (int)TrustStatus.Returned;
                    mp.CashTrustLogSet(ct);

                    #endregion CashTrustLog

                    #region CashTrustStatusLog

                    CashTrustStatusLog ctStatus = new CashTrustStatusLog();
                    ctStatus.TrustId = ct.TrustId;
                    ctStatus.TrustProvider = ct.TrustProvider;
                    ctStatus.Amount = returnedScash;
                    ctStatus.Status = (int)TrustStatus.Returned;
                    ctStatus.ModifyTime = DateTime.Now;
                    ctStatus.CreateId = createId;
                    if (isForced)
                    {
                        ctStatus.SpecialStatus = ct.SpecialStatus;
                        ctStatus.SpecialOperatedTime = DateTime.Now;
                    }
                    ctStatus.MarkNew();
                    mp.CashTrustStatusLogSet(ctStatus);


                    CashTrustStatusLog ctPscashStatus = new CashTrustStatusLog();
                    ctPscashStatus.TrustId = ct.TrustId;
                    ctPscashStatus.TrustProvider = ct.TrustProvider;
                    ctPscashStatus.Amount = ct.Pscash;
                    ctPscashStatus.Status = (int)TrustStatus.Returned;
                    ctPscashStatus.ModifyTime = DateTime.Now;
                    ctPscashStatus.CreateId = createId;
                    if (isForced)
                    {
                        ctPscashStatus.SpecialStatus = ct.SpecialStatus;
                        ctPscashStatus.SpecialOperatedTime = DateTime.Now;
                    }
                    ctPscashStatus.MarkNew();
                    mp.CashTrustStatusLogSet(ctPscashStatus);

                    #endregion CashTrustStatusLog

                    if (returnedScash > 0)
                    {
                        #region UserCashTrustLog

                        UserCashTrustLog ctUser = new UserCashTrustLog();
                        ctUser.UserId = ct.UserId;
                        ctUser.Amount = returnedScash;
                        ctUser.BankStatus = (int)TrustBankStatus.Initial;
                        ctUser.CreateTime = DateTime.Now;
                        ctUser.Message = "以購物金退回";
                        ctUser.TrustProvider = ct.TrustProvider;
                        ctUser.TrustId = ct.TrustId;
                        //ctUser.CouponId = ct.CouponId;
                        //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                        ctUser.Type = (int)UserTrustType.Return;
                        ctUser.MarkNew();
                        mp.UserCashTrustLogSet(ctUser);

                        #endregion UserCashTrustLog
                    }

                    if (ct.Pscash > 0)
                    {
                        #region UserCashTrustLog

                        UserCashTrustLog ctUser = new UserCashTrustLog();
                        ctUser.UserId = ct.UserId;
                        ctUser.Amount = ct.Pscash;
                        ctUser.BankStatus = (int)TrustBankStatus.Initial;
                        ctUser.CreateTime = DateTime.Now;
                        ctUser.Message = "以Pscash購物金退回";
                        ctUser.TrustProvider = ct.TrustProvider;
                        ctUser.TrustId = ct.TrustId;
                        ctUser.Type = (int)UserTrustType.Return;
                        ctUser.MarkNew();
                        mp.UserCashTrustLogSet(ctUser);

                        #endregion UserCashTrustLog
                    }

                    #endregion 信託紀錄

                    #region 取消核銷狀態

                    if (isForced && ct.CouponId != null)
                    {
                        vp.UndoVerificationLog(ct.CouponId.Value, OrderClassification.LkSite);
                        op.CancelEinvoiceSetByUndo(ct.OrderGuid, ct.CouponId.Value, Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon));
                    }

                    #endregion 取消核銷狀態

                    #region 退貨商品數量回補

                    ItemFacade.RefundAccessoryMemberQuantityByOrderDetail(op.OrderDetailGet(ct.OrderDetailGuid));

                    #endregion 退貨商品數量回補

                    return true;
                }
                else
                {
                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "查無付款紀錄或付款總額為零，無法退貨", createId, true);
                }
            }
            else
            {
                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "憑證編號#" + ct.CouponSequenceNumber + "狀態無法退貨", createId, true);
            }

            return false;
        }

        //信用卡信託退款ByCoupon
        public static bool RefundCreditcardByCouponForTrust(CashTrustLog ct, string createId)
        {
            string refundCoupon = (!string.IsNullOrEmpty(ct.CouponSequenceNumber)) ? ct.CouponSequenceNumber + " " : string.Empty;
            PaymentTransactionCollection ptScash = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                                    PaymentTransaction.Columns.OrderGuid + " = " + ct.OrderGuid,
                                                    PaymentTransaction.Columns.PaymentType + " = " + (int)PaymentType.SCash,
                                                    PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Refund);
            Order o = op.OrderGet(ct.OrderGuid);

            if (ct.Status == (int)TrustStatus.Returned)
            {
                if (ptScash.Count() > 0 && ct.CreditCard > 0)
                {
                    if (OrderFacade.GetSCashSum(o.UserId) >= ct.CreditCard)
                    {
                        DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(ptScash.First().Status);

                        PaymentTransactionCollection ptCreditcard = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                        PaymentTransaction.Columns.TransId + " = " + ptScash.First().TransId,
                                        PaymentTransaction.Columns.PaymentType + " = " + (int)PaymentType.Creditcard);

                        IEnumerable<PaymentTransaction> ptRefundTodayCol = ptCreditcard.Where(x => x.TransType == (int)PayTransType.Refund
                                                                                                && Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Created);

                        decimal creditAmount = ptCreditcard.Where(x => x.TransType == (int)PayTransType.Authorization && Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Successful).Sum(x => x.Amount)
                                             - ptCreditcard.Where(x => x.TransType == (int)PayTransType.Refund).Sum(x => x.Amount);

                        if (creditAmount > 0)
                        {
                            DateTime authTime = ptCreditcard.Where(x => x.TransType == (int)PayTransType.Authorization && (Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Successful)).First().TransTime.Value;

                            OrderFacade.ScashUsed(ct.CreditCard, o.UserId, ct.OrderGuid, "轉刷退取回購物金:" + o.SellerName, createId, (OrderClassification)ct.OrderClassification);

                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "轉刷退取回購物金NT$" + ct.CreditCard.ToString("F0"), createId, true);

                            if (ptRefundTodayCol.Count() > 0)
                            {
                                UpdateTransaction(ptRefundTodayCol.First().Id, ptRefundTodayCol.First().OrderGuid.Value,
                                   ct.CreditCard + ptRefundTodayCol.First().Amount, ptRefundTodayCol.First().AuthCode,
                                   authTime, ptRefundTodayCol.First().Message + refundCoupon, ptRefundTodayCol.First().Status, (int)PayTransResponseType.Initial);
                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "購物金轉刷退總計NT$" + (ct.CreditCard + ptRefundTodayCol.First().Amount).ToString("F0"), createId, true);
                            }
                            else
                            {
                                NewTransaction(ptCreditcard.First().TransId, ptCreditcard.First().OrderGuid.Value, PaymentType.Creditcard, ct.CreditCard,
                                    ptCreditcard.First().AuthCode, PayTransType.Refund, authTime, createId, "購物金轉刷退: " + refundCoupon,
                                    Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Created),
                                    PayTransResponseType.Initial, (OrderClassification)ptCreditcard.First().OrderClassification.Value, null, (PaymentAPIProvider)ptCreditcard.First().ApiProvider.Value);

                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "購物金轉刷退計NT$" + ct.CreditCard.ToString("F0"), createId, true);
                            }

                            #region 信託紀錄

                            #region CashTrustLog

                            ct.MarkOld();
                            ct.Status = (int)TrustStatus.Refunded;
                            ct.ModifyTime = DateTime.Now;
                            
                            //檢查退款方式為刷退 非購物金轉刷退 才更新returned_time
                            var rf = op.ReturnFormGetListByOrderGuid(o.Guid).FirstOrDefault(x => x.ProgressStatus == (int)ProgressStatus.Processing);
                            if (rf != null && rf.RefundType == (int)RefundType.Cash)
                            {
                                ct.ReturnedTime = ct.ModifyTime;
                            }

                            mp.CashTrustLogSet(ct);

                            #endregion CashTrustLog

                            #region CashTrustStatusLog

                            CashTrustStatusLog ctStatus = new CashTrustStatusLog();
                            ctStatus.TrustId = ct.TrustId;
                            ctStatus.TrustProvider = ct.TrustProvider;
                            ctStatus.Amount = ct.CreditCard;
                            ctStatus.Status = (int)TrustStatus.Refunded;
                            ctStatus.ModifyTime = DateTime.Now;
                            ctStatus.CreateId = createId;
                            ctStatus.MarkNew();
                            mp.CashTrustStatusLogSet(ctStatus);

                            #endregion CashTrustStatusLog

                            if (ct.CreditCard > 0)
                            {
                                #region UserCashTrustLog

                                UserCashTrustLog ctUser = new UserCashTrustLog();
                                ctUser.UserId = ct.UserId;
                                ctUser.Amount = (-1) * ct.CreditCard;
                                ctUser.BankStatus = (int)TrustBankStatus.Initial;
                                ctUser.CreateTime = DateTime.Now;
                                ctUser.Message = "將購物金以刷卡金退回";
                                ctUser.TrustProvider = ct.TrustProvider;
                                ctUser.TrustId = ct.TrustId;
                                //ctUser.CouponId = ct.CouponId;
                                //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                                ctUser.Type = (int)UserTrustType.Refund;
                                ctUser.MarkNew();
                                mp.UserCashTrustLogSet(ctUser);

                                #endregion UserCashTrustLog
                            }

                            #endregion 信託紀錄

                            return true;
                        }
                        else
                        {
                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "購物金並非由此張訂單刷卡購得，無法刷退", createId, true);
                        }
                    }
                    else
                    {
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "購物金不足，無法刷退", createId, true);
                    }
                }
                else
                {
                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "此訂單無退貨購物金供刷退", createId, true);
                }
            }
            else
            {
                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "憑證編號#" + ct.CouponSequenceNumber + "狀態無法退款", createId, true);
            }

            return false;
        }

        //信託退ATM ByCoupon
        public static bool RefundAtmByCouponForTrust(CashTrustLog ct, string createId)
        {
            PaymentTransactionCollection ptScash = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                                   PaymentTransaction.Columns.OrderGuid + " = " + ct.OrderGuid,
                                                   PaymentTransaction.Columns.PaymentType + " = " + (int)PaymentType.SCash,
                                                   PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Refund);
            Order o = op.OrderGet(ct.OrderGuid);
            int refundAmount = ct.Atm + ct.CreditCard + ct.Tcash + ct.FamilyIsp + ct.SevenIsp;

            if (ct.Status == (int)TrustStatus.Returned)
            {                
                if (ptScash.Count() > 0 && refundAmount > 0)
                {
                    if (OrderFacade.GetSCashSum(o.UserId) >= refundAmount)
                    {
                        DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(ptScash.First().Status);
                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);

                        IEnumerable<PaymentTransaction> ptCashCol = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                     PaymentTransaction.Columns.TransId + " = " + ptScash.First().TransId)
                                     .Where(x => x.PaymentType == (int)PaymentType.ATM || x.PaymentType == (int)PaymentType.Creditcard
                                         || x.PaymentType == (int)PaymentType.TaishinPay || x.PaymentType == (int)PaymentType.LinePay || x.PaymentType == (int)PaymentType.FamilyIsp || x.PaymentType == (int)PaymentType.SevenIsp
                                         );

                        if (ptCashCol.Count() > 0)
                        {
                            IEnumerable<PaymentTransaction> ptRefundTodayCol = ptCashCol.Where(x => x.TransType == (int)PayTransType.Refund
                                                                                             && Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Created);

                            OrderFacade.ScashUsed(refundAmount, o.UserId, ct.OrderGuid, "轉退ATM取回購物金:" + o.SellerName, createId, (OrderClassification)ct.OrderClassification);

                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "轉退ATM取回購物金計NT$" + refundAmount.ToString("F0"), createId, true);
                            CtAtmRefund atmR = op.CtAtmRefundGetLatest(ct.OrderGuid);

                            if (ptRefundTodayCol.Count() > 0)
                            {
                                UpdateTransaction(ptRefundTodayCol.First().TransId, ptRefundTodayCol.First().OrderGuid.Value, PaymentType.ATM,
                                    refundAmount + ptRefundTodayCol.First().Amount, ptRefundTodayCol.First().AuthCode, PayTransType.Refund,
                                    DateTime.Now, ptRefundTodayCol.First().Message, status, (int)PayTransResponseType.OK);
                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "購物金轉退ATM總計NT$" + (refundAmount + ptRefundTodayCol.First().Amount).ToString("F0"), createId, true);

                                if (atmR != null && atmR.Status < (int)AtmRefundStatus.AchProcess)
                                {
                                    atmR.Amount += refundAmount + Convert.ToInt32(ptRefundTodayCol.First().Amount);
                                    op.CtAtmRefundSet(atmR);
                                }
                            }
                            else
                            {
                                NewTransaction(ptCashCol.First().TransId, ptCashCol.First().OrderGuid.Value, PaymentType.ATM, refundAmount,
                                    ptCashCol.First().AuthCode, PayTransType.Refund, DateTime.Now, createId, "購物金轉退ATM", status, PayTransResponseType.OK);
                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "購物金轉退ATM計NT$" + refundAmount.ToString("F0"), createId, true);

                                if (atmR != null && atmR.Status < (int)AtmRefundStatus.AchProcess)
                                {
                                    atmR.Amount += refundAmount;
                                    atmR.Status = (int)AtmRefundStatus.Request;
                                    op.CtAtmRefundSet(atmR);
                                }
                            }

                            #region 信託紀錄

                            #region CashTrustLog

                            ct.MarkOld();
                            ct.Status = (int)TrustStatus.Refunded;
                            ct.ModifyTime = DateTime.Now;
                            ct.SpecialStatus = (atmR != null && atmR.Status == (int)AtmRefundStatus.RefundSuccess)
                                                    ? (int)Helper.SetFlag(false, ct.SpecialStatus, TrustSpecialStatus.AtmRefunding)
                                                    : (int)Helper.SetFlag(true, ct.SpecialStatus, TrustSpecialStatus.AtmRefunding);
                            ct.SpecialOperatedTime = DateTime.Now;

                            //檢查退款方式為退ATM 非購物金轉退ATM 才更新returned_time
                            var rf = op.ReturnFormGetListByOrderGuid(o.Guid).FirstOrDefault(x => x.ProgressStatus == (int)ProgressStatus.Processing ||
                                                                                                 x.ProgressStatus == (int)ProgressStatus.AtmQueueSucceeded);
                            if (rf != null && rf.RefundType == (int)RefundType.Atm)
                            {
                                ct.ReturnedTime = ct.ModifyTime;
                            }

                            mp.CashTrustLogSet(ct);

                            #endregion CashTrustLog

                            #region CashTrustStatusLog

                            CashTrustStatusLog ctStatus = new CashTrustStatusLog();
                            ctStatus.TrustId = ct.TrustId;
                            ctStatus.TrustProvider = ct.TrustProvider;
                            ctStatus.Amount = refundAmount;
                            ctStatus.Status = (int)TrustStatus.Refunded;
                            ctStatus.ModifyTime = DateTime.Now;
                            ctStatus.CreateId = createId;
                            ctStatus.MarkNew();
                            mp.CashTrustStatusLogSet(ctStatus);

                            #endregion CashTrustStatusLog

                            if (refundAmount > 0)
                            {
                                #region UserCashTrustLog

                                UserCashTrustLog ctUser = new UserCashTrustLog();
                                ctUser.UserId = ct.UserId;
                                ctUser.Amount = (-1) * refundAmount;
                                ctUser.BankStatus = (int)TrustBankStatus.Initial;
                                ctUser.CreateTime = DateTime.Now;
                                ctUser.Message = "將購物金以ATM退回";
                                ctUser.TrustProvider = ct.TrustProvider;
                                ctUser.TrustId = ct.TrustId;
                                //ctUser.CouponId = ct.CouponId;
                                //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                                ctUser.Type = (int)UserTrustType.Refund;
                                ctUser.MarkNew();
                                mp.UserCashTrustLogSet(ctUser);

                                #endregion UserCashTrustLog
                            }

                            #endregion 信託紀錄

                            return true;
                        }
                        else
                        {
                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "並非使用現金付款，無法退ATM", createId, true);
                        }
                    }
                    else
                    {
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "購物金不足，無法退ATM", createId, true);
                    }
                }
                else
                {
                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "此訂單無退貨購物金以供退ATM", createId, true);
                }
            }
            else
            {
                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "憑證編號#" + ct.CouponSequenceNumber + "狀態無法退款", createId, true);
            }

            return false;
        }

        /// <summary>
        /// 第三方退款
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="createId"></param>
        /// <param name="returnFormId"></param>
        /// <param name="realtimeRefundSuccess">若第三方有支援即時退款Api, 則回傳執行結果</param>
        /// <returns></returns>
        public static bool RefundTcashByCouponForTrust(CashTrustLog ct, string createId, int returnFormId, out bool realtimeRefundSuccess, out bool canRetry, out string reason, bool immediateRefund)
        {
            realtimeRefundSuccess = false;
            canRetry = false;
            reason = string.Empty;
            string refundCoupon = (!string.IsNullOrEmpty(ct.CouponSequenceNumber)) ? ct.CouponSequenceNumber + " " : string.Empty;
            PaymentTransactionCollection ptScash = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                                    PaymentTransaction.Columns.OrderGuid + " = " + ct.OrderGuid,
                                                    PaymentTransaction.Columns.PaymentType + " = " + (int)PaymentType.SCash,
                                                    PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Refund);
            Order o = op.OrderGet(ct.OrderGuid);

            if (ct.Status == (int)TrustStatus.Returned)
            {
                if (ptScash.Any() && ct.Tcash > 0)
                {
                    if (OrderFacade.GetSCashSum(o.UserId) >= ct.Tcash)
                    {
                        PaymentType paymentType = (PaymentType)GetPaymentTypeByThirdPartyPayment((ThirdPartyPayment)ct.ThirdPartyPayment);
                        DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(ptScash.First().Status);

                        PaymentTransactionCollection ptTcashCol = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                        PaymentTransaction.Columns.TransId + " = " + ptScash.First().TransId,
                                        PaymentTransaction.Columns.PaymentType + " = " + (int)paymentType);

                        var ptRefundTodayCol = ptTcashCol
                            .Where(x => x.TransType == (int)PayTransType.Refund && Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Created).ToList();

                        decimal tcashAmount = ptTcashCol.Where(x => x.TransType == (int)PayTransType.Authorization && Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Successful).Sum(x => x.Amount)
                                             - ptTcashCol.Where(x => x.TransType == (int)PayTransType.Refund).Sum(x => x.Amount);

                        if (tcashAmount > 0)
                        {
                            DateTime authTime = ptTcashCol.Where(x => x.TransType == (int)PayTransType.Authorization && (Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Successful)).First().TransTime.Value;

                            OrderFacade.ScashUsed(ct.Tcash, o.UserId, ct.OrderGuid
                                , string.Format("轉退{0}取回購物金:{1}", Helper.GetEnumDescription((ThirdPartyPayment)ct.ThirdPartyPayment), o.SellerName)
                                , createId, (OrderClassification)ct.OrderClassification);

                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment
                                , string.Format("轉退{0}取回購物金NT${1}", Helper.GetEnumDescription((ThirdPartyPayment)ct.ThirdPartyPayment)
                                , ct.Tcash.ToString("F0")), createId, true);

                            int ptRefundId;
                            if (ptRefundTodayCol.Any())
                            {
                                UpdateTransaction(ptRefundTodayCol.First().Id, ptRefundTodayCol.First().OrderGuid.Value,
                                   ct.Tcash + ptRefundTodayCol.First().Amount, ptRefundTodayCol.First().AuthCode,
                                   authTime, ptRefundTodayCol.First().Message + refundCoupon, ptRefundTodayCol.First().Status, (int)PayTransResponseType.Initial);

                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment
                                    , string.Format("購物金轉退{0}總計NT${1}", Helper.GetEnumDescription((ThirdPartyPayment)ct.ThirdPartyPayment),
                                    (ct.Tcash + ptRefundTodayCol.First().Amount).ToString("F0")), createId, true);

                                ptRefundId = ptRefundTodayCol.First().Id;
                            }
                            else
                            {
                                var ptTemp = NewTransaction(ptTcashCol.First().TransId, ptTcashCol.First().OrderGuid.Value, paymentType, ct.Tcash
                                    , ptTcashCol.First().AuthCode, PayTransType.Refund, authTime, createId
                                    , string.Format("購物金轉退{0}:{1}", Helper.GetEnumDescription((ThirdPartyPayment)ct.ThirdPartyPayment), refundCoupon)
                                    , Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Created)
                                    , PayTransResponseType.Initial, (OrderClassification)ptTcashCol.First().OrderClassification.Value, null
                                    , (PaymentAPIProvider)ptTcashCol.First().ApiProvider.Value);

                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment
                                    , string.Format("購物金轉退{0}計NT${1}", Helper.GetEnumDescription((ThirdPartyPayment)ct.ThirdPartyPayment)
                                    , ct.CreditCard.ToString("F0")), createId, true);

                                ptRefundId = ptTemp.Id;
                            }

                            //第三方退款處理
                            switch (paymentType)
                            {
                                case PaymentType.LinePay:

                                    var linePayTransLog = op.LinePayTransLogGetByTransId(ptTcashCol.First().TransId);
                                    var refundLog = new LinePayRefundLog
                                    {
                                        LinePayTransLogId = linePayTransLog.Id,
                                        PaymentTransactionId = ptRefundId,
                                        RefundStatus = (byte)LinePayRefundStatus.Created,
                                        RefundMsg = "create request.",
                                        CreateTime = DateTime.Now,
                                        RetryTime = 0,
                                        TrustId = ct.TrustId,
                                        ReturnFormId = returnFormId
                                    };
                                    op.LinePayRefundLogSet(refundLog, createId);
                                    realtimeRefundSuccess = LinePayUtility.Refund(refundLog, linePayTransLog, ct, out canRetry, out reason, createId);
                                    break;
                                case PaymentType.TaishinPay:
                                    logger.ErrorFormat("台新儲值支付退款失敗!! 目前尚未支援退回儲值帳戶，應退至ATM!");
                                    //TODO 台新 未實作
                                    break;
                            }

                            #region 信託紀錄

                            #region CashTrustLog

                            ct.MarkOld();
                            ct.Status = (int)TrustStatus.Refunded;
                            ct.ModifyTime = DateTime.Now;

                            //檢查退款方式為刷退 非購物金轉刷退 才更新returned_time
                            var rf = op.ReturnFormGetListByOrderGuid(o.Guid).FirstOrDefault(x => x.ProgressStatus == (int)ProgressStatus.Processing);
                            if (rf != null && rf.RefundType == (int)RefundType.Tcash)
                            {
                                ct.ReturnedTime = ct.ModifyTime;
                            }

                            mp.CashTrustLogSet(ct);

                            #endregion CashTrustLog

                            #region CashTrustStatusLog

                            CashTrustStatusLog ctStatus = new CashTrustStatusLog();
                            ctStatus.TrustId = ct.TrustId;
                            ctStatus.TrustProvider = ct.TrustProvider;
                            ctStatus.Amount = ct.Tcash;
                            ctStatus.Status = (int)TrustStatus.Refunded;
                            ctStatus.ModifyTime = DateTime.Now;
                            ctStatus.CreateId = createId;
                            ctStatus.MarkNew();
                            mp.CashTrustStatusLogSet(ctStatus);

                            #endregion CashTrustStatusLog

                            if (ct.Tcash > 0)
                            {
                                #region UserCashTrustLog

                                UserCashTrustLog ctUser = new UserCashTrustLog();
                                ctUser.UserId = ct.UserId;
                                ctUser.Amount = (-1) * ct.Tcash;
                                ctUser.BankStatus = (int)TrustBankStatus.Initial;
                                ctUser.CreateTime = DateTime.Now;
                                ctUser.Message = string.Format("將購物金以{0}退回", Helper.GetEnumDescription((ThirdPartyPayment)ct.ThirdPartyPayment));
                                ctUser.TrustProvider = ct.TrustProvider;
                                ctUser.TrustId = ct.TrustId;
                                //ctUser.CouponId = ct.CouponId;
                                //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                                ctUser.Type = (int)UserTrustType.Refund;
                                ctUser.MarkNew();
                                mp.UserCashTrustLogSet(ctUser);

                                #endregion UserCashTrustLog
                            }

                            #endregion 信託紀錄

                            return true;
                        }
                        else
                        {
                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment,
                                string.Format("購物金並非由此張訂單{0}購得，無法退{0}", Helper.GetEnumDescription((ThirdPartyPayment)ct.ThirdPartyPayment)), createId, true);
                        }
                    }
                    else
                    {
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment
                            , string.Format("購物金不足，無法退{0}", Helper.GetEnumDescription((ThirdPartyPayment)ct.ThirdPartyPayment)), createId, true);
                    }
                }
                else
                {
                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment
                        , string.Format("此訂單無退貨購物金供退{0}", Helper.GetEnumDescription((ThirdPartyPayment)ct.ThirdPartyPayment)), createId, true);
                }
            }
            else
            {
                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "憑證編號#" + ct.CouponSequenceNumber + "狀態無法退款", createId, true);
            }

            return false;
        }

        //撤消取消付款交易
        public static bool UndoCancelPaymentTransaction(CashTrustLog ct, string createId)
        {
            if (ct.Pcash == 0)
            {
                PaymentTransactionCollection ptc = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                    PaymentTransaction.Columns.OrderGuid + " = " + ct.OrderGuid);
                Order o = op.OrderGet(ct.OrderGuid);
                if (ptc.Count() > 0 && o != null)
                {
                    DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(ptc.First().Status);
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);

                    double remainderBcash = Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(o.UserId) / 10);
                    decimal remainerPscash = OrderFacade.GetPscashBalanceSum(o.UserId);
                    decimal remainderScash = OrderFacade.GetSCashSum(o.UserId);
                    int scashGot = ct.Scash + ct.CreditCard + ct.Atm + ct.Tcash + ct.FamilyIsp + ct.SevenIsp;
                    

                    if (ct.Status == (int)TrustStatus.Returned)
                    {
                        #region returned
                        if ((ct.Bcash <= remainderBcash &&
                            scashGot <= remainderScash &&
                            ct.Pscash <= remainerPscash &&
                            (ct.Bcash > 0 || scashGot > 0 || ct.Pscash > 0)) ||
                            ct.Lcash > 0 || (ct.Amount == 0 && (ct.SpecialStatus == (int)TrustSpecialStatus.Giveaway || ct.SpecialStatus == (int)TrustSpecialStatus.SpecialDiscount)))
                        {
                            #region 取回紅利金

                            if (ct.Bcash > 0)
                            {
                                int exchangeRate = 10;
                                logger.Info(ct.CouponSequenceNumber);
                                MemberFacade.DeductibleMemberPromotionDeposit(o.UserId, Convert.ToDouble(ct.Bcash * exchangeRate),
                                                            "恢復訂單", BonusTransactionType.OrderAmtRedeeming, o.Guid, createId, false, false);
                                NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.BonusPoint, ct.Bcash, null,
                                    PayTransType.Authorization, DateTime.Now, createId, "扣回紅利以恢復訂單", status, PayTransResponseType.OK);
                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "扣回紅利計NT$" + ct.Bcash.ToString("F0"), createId, true);
                            }

                            #endregion 取回紅利金

                            #region 取回Pscash

                            if (ct.Pscash > 0)
                            {
                                OrderFacade.PscashUsed(ct.Pscash, o.UserId, ct.OrderGuid, "扣回Pscash以恢復訂單:" + o.SellerName,
                                    createId);
                                NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.Pscash, ct.Pscash, null,
                                    PayTransType.Authorization, DateTime.Now, createId, "扣回Pscash以恢復訂單", status, PayTransResponseType.OK);
                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "扣回Pscash計NT$" + ct.Pscash.ToString("F0"), createId, true);
                            }

                            #endregion 取回Pscash

                            #region 取回購物金

                            if (scashGot > 0)
                            {
                                OrderFacade.ScashUsed(scashGot, o.UserId, ct.OrderGuid, "扣回購物金以恢復訂單:" + o.SellerName,
                                    createId, (OrderClassification)ct.OrderClassification);
                                NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.SCash, scashGot, null,
                                    PayTransType.Authorization, DateTime.Now, createId, "扣回購物金以恢復訂單", status, PayTransResponseType.OK);
                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "扣回購物金計NT$" + scashGot.ToString("F0"), createId, true);
                            }

                            #endregion 取回購物金

                            #region 處理Lcash取消退貨

                            if (ct.Lcash > 0)
                            {
                                NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.LCash, ct.Lcash, null, PayTransType.Authorization, DateTime.Now, createId, "取消代銷退貨以恢復訂單", status, PayTransResponseType.OK);
                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "取消外部付款折抵金額計NT$" + ct.Lcash.ToString("F0"), createId, true);
                            }

                            #endregion 處理Lcash取消退貨

                            #region 信託紀錄

                            #region CashTrustLog

                            ct.MarkOld();
                            ct.Status = (ct.TrustedTime == null) ? (int)TrustStatus.Initial : (int)TrustStatus.Trusted;
                            ct.ModifyTime = DateTime.Now;
                            ct.SpecialStatus |= (int)TrustSpecialStatus.ReturnCanceled;
                            ct.SpecialOperatedTime = DateTime.Now;
                            mp.CashTrustLogSet(ct);

                            #endregion CashTrustLog

                            #region CashTrustStatusLog

                            CashTrustStatusLog ctStatus = new CashTrustStatusLog();
                            ctStatus.TrustId = ct.TrustId;
                            ctStatus.TrustProvider = ct.TrustProvider;
                            ctStatus.Amount = scashGot;
                            ctStatus.Status = (ct.TrustedTime == null) ? (int)TrustStatus.Initial : (int)TrustStatus.Trusted;
                            ctStatus.ModifyTime = DateTime.Now;
                            ctStatus.CreateId = createId;
                            ctStatus.MarkNew();
                            mp.CashTrustStatusLogSet(ctStatus);

                            #endregion CashTrustStatusLog

                            if (scashGot > 0)
                            {
                                #region UserCashTrustLog

                                UserCashTrustLog ctUser = new UserCashTrustLog();
                                ctUser.UserId = ct.UserId;
                                ctUser.Amount = (-1) * scashGot;
                                ctUser.BankStatus = (int)TrustBankStatus.Initial;
                                ctUser.CreateTime = DateTime.Now;
                                ctUser.Message = "恢復訂單取回購物金";
                                ctUser.TrustProvider = ct.TrustProvider;
                                ctUser.TrustId = ct.TrustId;
                                //ctUser.CouponId = ct.CouponId;
                                //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                                ctUser.Type = (int)UserTrustType.Buy;
                                ctUser.MarkNew();
                                mp.UserCashTrustLogSet(ctUser);

                                #endregion UserCashTrustLog
                            }

                            if (ct.Pscash > 0)
                            {
                                #region UserCashTrustLog

                                UserCashTrustLog ctUser = new UserCashTrustLog();
                                ctUser.UserId = ct.UserId;
                                ctUser.Amount = (-1) * ct.Pscash;
                                ctUser.BankStatus = (int)TrustBankStatus.Initial;
                                ctUser.CreateTime = DateTime.Now;
                                ctUser.Message = "恢復訂單取回Pscash";
                                ctUser.TrustProvider = ct.TrustProvider;
                                ctUser.TrustId = ct.TrustId;
                                ctUser.Type = (int)UserTrustType.Buy;
                                ctUser.MarkNew();
                                mp.UserCashTrustLogSet(ctUser);

                                #endregion UserCashTrustLog
                            }

                            #endregion 信託紀錄

                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Order, "恢復憑證:#" + ct.CouponSequenceNumber, createId, true);
                            return true;
                        }
                        else
                        {
                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "剩餘紅利/購物金不足以取回，無法恢復訂單", createId, true);
                        }

                        #endregion returned
                    }
                    else if (ct.Status == (int)TrustStatus.Refunded)
                    {
                        #region refunded
                        PaymentTransactionCollection ptCredit = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                            PaymentTransaction.Columns.TransId + " = " + ptc.First().TransId,
                            PaymentTransaction.Columns.PaymentType + " = " + (int)PaymentType.Creditcard,
                            PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Refund);

                        IEnumerable<PaymentTransaction> ptCol = (ptCredit.Count() > 0) ? ptCredit.Where(x => Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Created) : null;
                        PaymentTransaction pt = (ptCol.Count() > 0) ? ptCol.First() : null;

                        if (pt != null)
                        {
                            if (ct.CreditCard <= pt.Amount &&
                                ct.Bcash <= remainderBcash &&
                                ct.Scash <= remainderScash &&
                                (ct.CreditCard > 0 || ct.Bcash > 0 || ct.Scash > 0))
                            {
                                #region 取回紅利金

                                if (ct.Bcash > 0)
                                {
                                    int exchangeRate = 10;
                                    MemberFacade.DeductibleMemberPromotionDeposit(o.UserId, Convert.ToDouble(ct.Bcash * exchangeRate),
                                                                "恢復訂單", BonusTransactionType.OrderAmtRedeeming, o.Guid, createId, false, false);
                                    NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.BonusPoint, ct.Bcash, null,
                                        PayTransType.Authorization, DateTime.Now, createId, "扣回紅利以恢復訂單", status, PayTransResponseType.OK);
                                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "扣回紅利計NT$" + ct.Bcash.ToString("F0"), createId, true);
                                }

                                #endregion 取回紅利金

                                #region 取回Pscash

                                if (ct.Pscash > 0)
                                {
                                    OrderFacade.PscashUsed(ct.Pscash, o.UserId, ct.OrderGuid, "扣回Pscash以恢復訂單:" + o.SellerName,
                                        createId);
                                    NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.Pscash, ct.Pscash, null,
                                        PayTransType.Authorization, DateTime.Now, createId, "扣回Pscash以恢復訂單", status, PayTransResponseType.OK);
                                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "扣回Pscash計NT$" + ct.Pscash.ToString("F0"), createId, true);
                                }

                                #endregion 取回Pscash

                                #region 取回購物金

                                if (ct.Scash > 0)
                                {
                                    OrderFacade.ScashUsed(ct.Scash, o.UserId, ct.OrderGuid, "扣回購物金以恢復訂單:" + o.SellerName,
                                        createId, (OrderClassification)ct.OrderClassification);
                                    NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.SCash, ct.Scash, null,
                                        PayTransType.Authorization, DateTime.Now, createId, "扣回購物金以恢復訂單", status, PayTransResponseType.OK);
                                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "扣回購物金計NT$" + ct.Scash.ToString("F0"), createId, true);
                                }

                                #endregion 取回購物金

                                #region 取回刷卡金

                                if (ct.CreditCard > 0)
                                {
                                    pt.Amount -= ct.CreditCard;
                                    if (pt.Amount <= 0)
                                    {
                                        pt.Status = Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Canceled);
                                    }

                                    op.PaymentTransactionSet(pt);

                                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "扣回刷卡金計NT$" + ct.CreditCard.ToString("F0"), createId, true);
                                }

                                #endregion 取回刷卡金

                                #region 信託紀錄

                                #region CashTrustLog

                                ct.MarkOld();
                                ct.Status = (ct.TrustedTime == null) ? (int)TrustStatus.Initial : (int)TrustStatus.Trusted;
                                ct.ModifyTime = DateTime.Now;
                                ct.SpecialStatus |= (int)TrustSpecialStatus.ReturnCanceled;
                                ct.SpecialOperatedTime = DateTime.Now;
                                mp.CashTrustLogSet(ct);

                                #endregion CashTrustLog

                                #region CashTrustStatusLog

                                CashTrustStatusLog ctStatus = new CashTrustStatusLog();
                                ctStatus.TrustId = ct.TrustId;
                                ctStatus.TrustProvider = ct.TrustProvider;
                                ctStatus.Amount = ct.Scash;
                                ctStatus.Status = (ct.TrustedTime == null) ? (int)TrustStatus.Initial : (int)TrustStatus.Trusted;
                                ctStatus.ModifyTime = DateTime.Now;
                                ctStatus.CreateId = createId;
                                ctStatus.MarkNew();
                                mp.CashTrustStatusLogSet(ctStatus);

                                #endregion CashTrustStatusLog

                                if (ct.Scash > 0)
                                {
                                    #region UserCashTrustLog

                                    UserCashTrustLog ctUser = new UserCashTrustLog();
                                    ctUser.UserId = ct.UserId;
                                    ctUser.Amount = (-1) * ct.Scash;
                                    ctUser.BankStatus = (int)TrustBankStatus.Initial;
                                    ctUser.CreateTime = DateTime.Now;
                                    ctUser.Message = "恢復訂單取回購物金";
                                    ctUser.TrustProvider = ct.TrustProvider;
                                    ctUser.TrustId = ct.TrustId;
                                    //ctUser.CouponId = ct.CouponId;
                                    //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                                    ctUser.Type = (int)UserTrustType.Buy;
                                    ctUser.MarkNew();
                                    mp.UserCashTrustLogSet(ctUser);

                                    #endregion UserCashTrustLog
                                }

                                if (ct.Pscash > 0)
                                {
                                    #region UserCashTrustLog

                                    UserCashTrustLog ctUser = new UserCashTrustLog();
                                    ctUser.UserId = ct.UserId;
                                    ctUser.Amount = (-1) * ct.Pscash;
                                    ctUser.BankStatus = (int)TrustBankStatus.Initial;
                                    ctUser.CreateTime = DateTime.Now;
                                    ctUser.Message = "恢復訂單取回Pscash";
                                    ctUser.TrustProvider = ct.TrustProvider;
                                    ctUser.TrustId = ct.TrustId;
                                    ctUser.Type = (int)UserTrustType.Buy;
                                    ctUser.MarkNew();
                                    mp.UserCashTrustLogSet(ctUser);

                                    #endregion UserCashTrustLog
                                }

                                #endregion 信託紀錄

                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Order, "恢復憑證:#" + ct.CouponSequenceNumber, createId, true);
                                return true;
                            }
                            else
                            {
                                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "剩餘紅利金/購物金/刷卡金不足以取回，無法恢復訂單", createId, true);
                            }
                        }
                        else
                        {
                            CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "查無本日刷退紀錄或刷卡金不足以取回，無法恢復訂單", createId, true);
                        }

                        #endregion refunded
                    }
                    else
                    {
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "非退貨狀態，無法恢復訂單", createId, true);
                    }
                }
                else
                {
                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "查無付款紀錄或訂單", createId, true);
                }
            }
            else
            {
                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "付款類別無法恢復訂單", createId, true);
            }

            return false;
        }

        public static void CancelPaymentTransactionByTransId(string transId, string createId, string msg)
        {
            PaymentTransactionCollection ptc = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                                    PaymentTransaction.Columns.TransId + " = " + transId,
                                                    PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Authorization);

            IEnumerable<PaymentTransaction> ptCreditcardCol = from i in ptc where (i.PaymentType == (int)PaymentType.Creditcard) select i;
            PaymentTransaction ptCreditcard = (ptCreditcardCol.Count() > 0) ? ptCreditcardCol.First() : null;
            IEnumerable<PaymentTransaction> ptPcashCol = from i in ptc where (i.PaymentType == (int)PaymentType.PCash) select i;
            PaymentTransaction ptPcash = (ptPcashCol.Count() > 0) ? ptPcashCol.First() : null;
            IEnumerable<PaymentTransaction> ptBonusCol = from i in ptc where (i.PaymentType == (int)PaymentType.BonusPoint) select i;
            PaymentTransaction ptBonus = (ptBonusCol.Count() > 0) ? ptBonusCol.First() : null;
            IEnumerable<PaymentTransaction> ptScashCol = from i in ptc where (i.PaymentType == (int)PaymentType.SCash) select i;
            PaymentTransaction ptScash = (ptScashCol.Count() > 0) ? ptScashCol.First() : null;
            IEnumerable<PaymentTransaction> ptLinePayCol = from i in ptc where (i.PaymentType == (int)PaymentType.LinePay) select i;
            PaymentTransaction ptLinePay = (ptLinePayCol.Count() > 0) ? ptLinePayCol.First() : null;

            if (ptPcash != null)
            {
                string authcode = string.Empty;
                string pezResult = PCashWorker.DeCheckOut(transId, authcode, ptPcash.Amount);
                PayTransResponseType responseType = PayTransResponseType.OK;
                PayTransPhase transPhase = PayTransPhase.Created;
                if (pezResult == "0000")
                {
                    transPhase = PayTransPhase.Successful;
                }
                else
                {
                    transPhase = PayTransPhase.Failed;
                    responseType = PayTransResponseType.GenericError;
                }

                DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(ptPcash.Status);

                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), transPhase);
                if (ptPcash.OrderGuid != null)
                {
                    NewTransaction(transId, ptPcash.OrderGuid.Value, PaymentType.PCash, ptPcash.Amount, authcode, PayTransType.Refund, DateTime.Now, createId, msg + pezResult, status, responseType);
                }
            }

            if (ptCreditcard != null)
            {
                DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(ptCreditcard.Status);
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Canceled);
                UpdateTransaction(transId, Guid.Empty, PaymentType.Creditcard, ptCreditcard.Amount, ptCreditcard.AuthCode, 
                    PayTransType.Authorization, DateTime.Now, msg + "取消信用卡授權", status, (int)ptCreditcard.Result);
            }

            if (ptBonus != null)
            {
                DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(ptBonus.Status);
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                NewTransaction(transId, ptBonus.OrderGuid.Value, PaymentType.BonusPoint, ptBonus.Amount, null, PayTransType.Refund, DateTime.Now, createId, msg + "以紅利退貨", status, PayTransResponseType.OK);
            }
            if (ptScash != null)
            {
                DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(ptScash.Status);
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                NewTransaction(transId, ptScash.OrderGuid.Value, PaymentType.SCash, ptScash.Amount, null, PayTransType.Refund, DateTime.Now, createId, msg + "以17Life購物金退貨", status, PayTransResponseType.OK);
            }
        }

        public static bool CancelPaymentTransactionByCouponForIncompleteOrder(CashTrustLog ct, string createId)
        {
            PaymentTransactionCollection ptc = op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                                PaymentTransaction.Columns.OrderGuid + " = " + ct.OrderGuid,
                                                PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Authorization);

            DepartmentTypes dType = (ptc.Count > 0) ? Helper.GetPaymentTransactionDepartmentTypes(ptc.First().Status) : DepartmentTypes.Ppon;
            Order o = op.OrderGet(ct.OrderGuid);

            if (ptc.Count() > 0)
            {
                #region 處理購物金退回

                if (ct.Scash > 0)
                {
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                    NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.SCash, ct.Scash, null,
                        PayTransType.Refund, DateTime.Now, createId, "以購物金退未完成單", status, PayTransResponseType.OK);

                    #region New Scash

                    OrderFacade.ScashReturned(ct.Scash, o.UserId, ct.OrderGuid, "以購物金退未完成單:" + o.SellerName, createId, OrderClassification.LkSite, ct.UninvoicedAmount, IsUnivoicedAmountInvoiced(ct.CouponId));

                    #endregion New Scash

                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退購物金計NT$" + (ct.Scash).ToString("F0"), createId, true);

                    #region UserCashTrustLog

                    UserCashTrustLog ctUser = new UserCashTrustLog();
                    ctUser.UserId = ct.UserId;
                    ctUser.Amount = ct.Scash;
                    ctUser.BankStatus = (int)TrustBankStatus.Initial;
                    ctUser.CreateTime = DateTime.Now;
                    ctUser.Message = "以購物金退未完成單";
                    ctUser.TrustProvider = ct.TrustProvider;
                    ctUser.TrustId = ct.TrustId;
                    //ctUser.CouponId = ct.CouponId;
                    //ctUser.CouponSequenceNumber = ct.CouponSequenceNumber;
                    ctUser.Type = (int)UserTrustType.Return;
                    ctUser.MarkNew();
                    mp.UserCashTrustLogSet(ctUser);

                    #endregion UserCashTrustLog
                }

                #endregion 處理購物金退回


                #region 處理Pscash退回
                if (ct.Pscash > 0)
                {
                    
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                    NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.Pscash, ct.Pscash, null,
                                                    PayTransType.Refund, DateTime.Now, createId, "以Pscash退貨", status, PayTransResponseType.OK);

                    OrderFacade.PscashReturned(ct.Pscash, o.UserId, ct.OrderGuid, "以Pscash退貨:" + o.SellerName, createId);

                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退Pscash計NT$" + ct.Pscash.ToString("F0"), createId, true);


                    #region UserCashTrustLog

                    UserCashTrustLog ctUser = new UserCashTrustLog();
                    ctUser.UserId = ct.UserId;
                    ctUser.Amount = ct.Pscash;
                    ctUser.BankStatus = (int)TrustBankStatus.Initial;
                    ctUser.CreateTime = DateTime.Now;
                    ctUser.Message = "以Pscash購物金退回";
                    ctUser.TrustProvider = ct.TrustProvider;
                    ctUser.TrustId = ct.TrustId;
                    ctUser.Type = (int)UserTrustType.Return;
                    ctUser.MarkNew();
                    mp.UserCashTrustLogSet(ctUser);

                    #endregion UserCashTrustLog
                }
                #endregion 處理Pscash退回

                #region 處理Pcash退回

                if (ct.Pcash > 0)
                {
                    string pAuthCode = ptc.Where(x => x.PaymentType == (int)PaymentType.PCash).First().AuthCode;
                    string pezResult = PCashWorker.DeCheckOut(ptc.First().TransId, pAuthCode, ct.Pcash);
                    PayTransResponseType responseType = PayTransResponseType.OK;
                    PayTransPhase transPhase = PayTransPhase.Created;
                    if (pezResult == "0000")
                    {
                        transPhase = PayTransPhase.Successful;
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退PCash計NT$" + ct.Pcash.ToString("F0"), createId, true);
                    }
                    else
                    {
                        transPhase = PayTransPhase.Failed;
                        responseType = PayTransResponseType.GenericError;
                        CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退PCash失敗" + pezResult, createId, true);
                    }
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), transPhase);
                    NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.PCash, ct.Pcash, pAuthCode, PayTransType.Refund, DateTime.Now, createId, pezResult, status, responseType);
                }

                #endregion 處理Pcash退回

                #region 處理紅利退回

                if (ct.Bcash > 0)
                {
                    int exchangeRate = 10;
                    MemberPromotionDeposit addBonus = new MemberPromotionDeposit();
                    addBonus.OrderGuid = ct.OrderGuid;
                    addBonus.StartTime = DateTime.Now;
                    addBonus.ExpireTime = DateTime.Now.AddYears(2);
                    addBonus.PromotionValue = (double)ct.Bcash * exchangeRate;
                    addBonus.CreateTime = DateTime.Now;
                    addBonus.UserId = o.UserId;

                    if (o.IsLoaded && o.ParentOrderId != null)
                    {
                        addBonus.Action = "退未完成單:" + ct.ItemName;
                    }
                    else
                    {
                        addBonus.Action = "紅利退貨";
                    }

                    addBonus.CreateId = "sys";
                    mp.MemberPromotionDepositSet(addBonus);

                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                    NewTransaction(ptc.First().TransId, ct.OrderGuid, PaymentType.BonusPoint, ct.Bcash, null, PayTransType.Refund, DateTime.Now, createId, "以紅利退未完成單", status, PayTransResponseType.OK);
                    CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "退紅利計NT$" + ct.Bcash.ToString("F0"), createId, true);
                }

                #endregion 處理紅利退回

                #region 處理折價券退回

                op.DiscountCodeClean(ct.OrderGuid);

                #endregion 處理折價券退回

                #region 處理商品數量退回

                ItemFacade.RefundAccessoryMemberQuantityByOrderDetail(op.OrderDetailGet(ct.OrderDetailGuid));

                #endregion 處理商品數量退回

                #region 未完成單不開發票
                
                EinvoiceMainCollection invoices = op.EinvoiceMainGetListByOrderGuid(o.Guid);
                foreach (var invoice in invoices)
                {
                    invoice.InvoiceStatus = (int)EinvoiceType.NotComplete;
                    op.EinvoiceSetMain(invoice);
                }
                
                #endregion 未完成單不開發票

                //設為已取消訂單
                GroupOrder gpo = op.GroupOrderGetByOrderGuid(o.ParentOrderId.GetValueOrDefault());
                OrderFacade.SetOrderStatus(o.Guid, "sys", OrderStatus.Cancel, true, gpo.BusinessHourGuid);
                //[取消中]狀態移除
                if (o.IsCanceling && cp.EnableCancelPaidByIspOrder)
                {
                    o.IsCanceling = false;
                    op.OrderSet(o);
                }
                
                return true;
            }
            else
            {
                CommonFacade.AddAudit(ct.OrderGuid, AuditType.Payment, "查無付款紀錄或付款總額為零，無法退貨", createId, true);
            }

            return false;
        }

        public static bool CancelPaymentTransactionByOrderForIncompleteOrder(ShoppingCartGroup cartPtGroup, string createId)
        {
            return CancelPaymentTransactionByOrderForIncompleteOrder(op.OrderGetListByShoppingCartGroupGuid(cartPtGroup.Guid), createId);
        }

        public static bool CancelPaymentTransactionByOrderForIncompleteOrder(Order o, string createId)
        {
            return CancelPaymentTransactionByOrderForIncompleteOrder(new List<Order> { o }, createId);
        }

        private static PaymentTransaction GetPaymentTransaction(
            PaymentTransactionCollection ptc, PaymentType paymentType, PayTransType transType, PayTransPhase transPhase)
        {
            if (ptc == null || ptc.Count == 0)
            {
                return null;
            }
            return ptc.FirstOrDefault(t =>
                t.PaymentType == (int)paymentType &&
                t.TransType == (int)transType &&
                Helper.GetPaymentTransactionPhase(t.Status) == transPhase);
        }

        private static bool CancelPaymentTransactionByOrderForIncompleteOrder(IList<Order> orders, string createId)
        {
            if (orders == null || orders.Count == 0)
            {
                throw new Exception("CancelPaymentTransactionByOrderForIncompleteOrderInTheCart 不接受orders為空值.");
            }
            if (orders.Any(t => t.CartGuid != null) && orders.Any(t => t.CartGuid != orders.First().CartGuid))
            {
                throw new Exception("CancelPaymentTransactionByOrderForIncompleteOrderInTheCart 只處理同一購物車裡的多訂單.");
            }            
            Dictionary<Guid, PaymentTransactionCollection> ptcDict = new Dictionary<Guid, PaymentTransactionCollection>();
            decimal refundTotalAmount = 0;
            foreach (Order o in orders)
            {
                PaymentTransactionCollection ptc = op.PaymentTransactionGetListByOrderGuid(o.Guid);
                ptcDict.Add(o.Guid, ptc);
                refundTotalAmount += ptc.Sum(t => t.Amount);
            }

            if (refundTotalAmount == 0 )
            {
                StringBuilder sb = new StringBuilder();
                foreach (Order o in orders)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(o.Guid);
                }
                CommonFacade.AddAudit(sb.ToString(), AuditType.Payment, "查無付款紀錄或付款總額為零，無法退貨", createId, true);
            }

            #region 處理購物金退回

            foreach (Order o in orders)
            {
                PaymentTransactionCollection ptc = ptcDict[o.Guid];
                PaymentTransaction ptScash = GetPaymentTransaction(ptc, PaymentType.SCash, PayTransType.Authorization, PayTransPhase.Successful);

                if (ptScash == null || ptScash.Amount == 0)
                {
                    continue;
                }
                using (var tran = TransactionScopeBuilder.CreateReadCommitted())
                {
                    op.ScashWithdrawalDelete(o.Guid);
                    op.ScashDepositDelete(o.Guid);                    
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon), 
                        PayTransPhase.Successful);
                    NewTransaction(ptScash.TransId, o.Guid, PaymentType.SCash, ptScash.Amount, null, PayTransType.Refund, DateTime.Now, createId,
                        "取消購物金扣抵", status, PayTransResponseType.OK);

                    CommonFacade.AddAudit(o.Guid, AuditType.Payment, "取消購物金扣抵計NT$" + ptScash.Amount.ToString("F0"), createId, true);
                    tran.Complete();
                }
            }

            #endregion 處理購物金退回

            #region 處理Pscash退回


            foreach (Order o in orders)
            {
                PaymentTransactionCollection ptc = ptcDict[o.Guid];
                PaymentTransaction ptPscash = GetPaymentTransaction(ptc, PaymentType.Pscash, PayTransType.Authorization, PayTransPhase.Successful);

                if (ptPscash == null || ptPscash.Amount == 0)
                {
                    continue;
                }
                using (var tran = TransactionScopeBuilder.CreateReadCommitted())
                {
                    op.PscashWithdrawalDelete(o.Guid);
                    op.PscashDepositDelete(o.Guid);
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon),
                        PayTransPhase.Successful);
                    NewTransaction(ptPscash.TransId, o.Guid, PaymentType.Pscash, ptPscash.Amount, null, PayTransType.Refund, DateTime.Now, createId,
                        "取消Pscash扣抵", status, PayTransResponseType.OK);

                    CommonFacade.AddAudit(o.Guid, AuditType.Payment, "取消Pscash扣抵計NT$" + ptPscash.Amount.ToString("F0"), createId, true);
                    tran.Complete();
                }
            }

            #endregion 處理Pscash退回

            #region 處理Pcash退回
            foreach (Order o in orders)
            {
                PaymentTransactionCollection ptc = ptcDict[o.Guid];
                PaymentTransaction ptPcash = GetPaymentTransaction(ptc, PaymentType.PCash, PayTransType.Authorization, PayTransPhase.Successful);

                if (ptPcash != null && ptPcash.Amount > 0)
                {
                    string pAuthCode = ptPcash.AuthCode;
                    string pezResult = PCashWorker.DeCheckOut(ptc.First().TransId, pAuthCode, ptPcash.Amount);
                    PayTransResponseType responseType = PayTransResponseType.OK;
                    PayTransPhase transPhase;
                    if (pezResult == "0000")
                    {
                        transPhase = PayTransPhase.Successful;
                        CommonFacade.AddAudit(o.Guid, AuditType.Payment, "退PCash計NT$" + ptPcash.Amount.ToString("F0"), createId, true);
                    }
                    else
                    {
                        transPhase = PayTransPhase.Failed;
                        responseType = PayTransResponseType.GenericError;
                        CommonFacade.AddAudit(o.Guid, AuditType.Payment, "退PCash失敗" + pezResult, createId, true);
                    }
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon),
                        transPhase);
                    NewTransaction(ptPcash.TransId, o.Guid, PaymentType.PCash, ptPcash.Amount, pAuthCode, PayTransType.Refund, DateTime.Now, createId,
                        pezResult, status, responseType);
                }
            }
            #endregion 處理Pcash退回

            #region 處理Lcash退回

            foreach (Order o in orders)
            {
                PaymentTransactionCollection ptc = ptcDict[o.Guid];
                PaymentTransaction ptLcash = GetPaymentTransaction(ptc, PaymentType.LCash, PayTransType.Authorization, PayTransPhase.Successful);

                if (ptLcash != null && ptLcash.Amount > 0)
                {
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon), PayTransPhase.Successful);
                    NewTransaction(ptLcash.TransId, o.Guid, PaymentType.LCash, ptLcash.Amount, null, PayTransType.Refund, DateTime.Now, createId,
                        "取消代銷扣抵", status, PayTransResponseType.OK);
                    CommonFacade.AddAudit(o.Guid, AuditType.Payment, "取消代銷扣抵計NT$" + ptLcash.Amount.ToString("F0"), createId, true);
                }
            }


            #endregion 處理Lcash退回

            #region 處理紅利退回

            foreach (Order o in orders)
            {
                PaymentTransactionCollection ptc = ptcDict[o.Guid];
                PaymentTransaction ptBcash = GetPaymentTransaction(ptc, PaymentType.BonusPoint, PayTransType.Authorization, PayTransPhase.Successful);

                if (ptBcash == null || ptBcash.Amount == 0)
                {
                    continue;
                }
                MemberPromotionWithdrawalCollection bcashCol = mp.MemberPromotionWithdrawalGetListByOrderGuid(o.Guid);
                foreach (var bcash in bcashCol)
                {
                    bcash.Type = (int)BonusTransactionType.InvalidOrderAmtRedeeming;
                    mp.MemberPromotionWithdrawalSet(bcash);
                }
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon),
                    PayTransPhase.Successful);
                NewTransaction(ptBcash.TransId, o.Guid, PaymentType.BonusPoint, ptBcash.Amount, null, PayTransType.Refund, DateTime.Now, createId,
                    "取消紅利金扣抵", status, PayTransResponseType.OK);
                CommonFacade.AddAudit(o.Guid, AuditType.Payment, "取消紅利金扣抵計NT$" + ptBcash.Amount.ToString("F0"), createId, true);
            }

            #endregion 處理紅利退回

            #region 處理折價券退回

            foreach (Order o in orders)
            {
                PaymentTransactionCollection ptc = ptcDict[o.Guid];
                PaymentTransaction ptDcash = GetPaymentTransaction(ptc, PaymentType.DiscountCode, PayTransType.Authorization, PayTransPhase.Successful);

                if (ptDcash != null && ptDcash.Amount > 0)
                {
                    op.DiscountCodeClean(o.Guid);
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon),
                        PayTransPhase.Canceled);
                    ptDcash.Status = status;
                    op.PaymentTransactionSet(ptDcash);
                }
            }

            #endregion 處理折價券退回

            #region 處理刷卡取消授權

            List<PaymentTransaction> ptCredits = new List<PaymentTransaction>();
            foreach (Order o in orders)
            {
                PaymentTransactionCollection ptc = ptcDict[o.Guid];
                PaymentTransaction ptCredit = GetPaymentTransaction(ptc, PaymentType.Creditcard, PayTransType.Authorization, PayTransPhase.Successful);
                PaymentTransaction ptCreditCharging = GetPaymentTransaction(ptc, PaymentType.Creditcard, PayTransType.Charging, PayTransPhase.Successful);
                PaymentTransaction ptCreditRefund = GetPaymentTransaction(ptc, PaymentType.Creditcard, PayTransType.Refund, PayTransPhase.Successful);
                if (ptCredit != null && ptCreditCharging == null && ptCreditRefund == null)
                {
                    ptCredits.Add(ptCredit);
                }
            }

            CartPaymentTransaction cartPt = CartPaymentTransaction.ConvertFrom(ptCredits).FirstOrDefault();
            if (cartPt != null)
            {
                CreditCardUtility.AuthenticationReverse(cartPt);
                foreach (var ptCredit in ptCredits)
                {
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon),
                        PayTransPhase.Canceled);
                    ptCredit.Status = status;
                    op.PaymentTransactionSet(ptCredit);
                }
            }

            #endregion 處理刷卡取消授權

            #region 處理第三方支付 [未成立訂單] 取消授權&退款

            #region LinePay
            foreach (Order o in orders)
            {
                PaymentTransactionCollection ptc = ptcDict[o.Guid];
                PaymentTransaction ptLinePay = GetPaymentTransaction(ptc, PaymentType.LinePay, PayTransType.Authorization, PayTransPhase.Successful);

                if (ptLinePay != null && ptLinePay.ApiProvider != null)
                {
                    var linePayTrans = op.LinePayTransLogGetByTransId(ptLinePay.TransId);
                    if (linePayTrans.IsLoaded)
                    {
                        PayTransResponseType result = PayTransResponseType.GenericError;
                        string message = "LinePay 未成立訂單退款";
                        if (LinePayUtility.BuyFailRefund(linePayTrans, (int)ptLinePay.Amount, false, true))
                        {
                            result = PayTransResponseType.OK;
                        }
                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon),
                            PayTransPhase.Successful);
                        NewTransaction(ptLinePay.TransId, o.Guid, PaymentType.BonusPoint, ptLinePay.Amount, null,
                            PayTransType.Refund, DateTime.Now, createId, message, status, result);
                    }
                }
            }

            #endregion LinePay

            #region TaishinPay

            //TODO 待退貨API實作退款

            foreach (Order o in orders)
            {
                PaymentTransactionCollection ptc = ptcDict[o.Guid];
                PaymentTransaction ptTaishinPay = GetPaymentTransaction(ptc, PaymentType.TaishinPay, PayTransType.Authorization, PayTransPhase.Successful);

                if (ptTaishinPay != null && ptTaishinPay.ApiProvider != null)
                {
                    var taishinPayTrans = op.ThirdPartyPayTransLogGetByOrgTransId(PaymentAPIProvider.TaishinPay, ptTaishinPay.TransId);
                    if (taishinPayTrans.IsLoaded)
                    {
                        if (taishinPayTrans.PaymentStatus == (int)TaishinPayTransStatus.PaySuccess)
                        {
                            //寄信成功壓OK
                            bool isSendSuccess = OrderFacade.SendTaishinPayCancelOrderNoticeMail(o, ptc);
                            PayTransResponseType result = PayTransResponseType.Initial;
                            int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon),
                                                                PayTransPhase.Failed);
                            string message = "TaishinPay 未成立訂單退款";
                            if (isSendSuccess)
                            {                                
                                result = PayTransResponseType.OK;
                                status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon),
                                    PayTransPhase.Successful);                                
                            }
                            NewTransaction(taishinPayTrans.TransId, o.Guid, PaymentType.TaishinPay, taishinPayTrans.Amount, null,
                                PayTransType.Refund, DateTime.Now, createId, message, status, result);
                        }
                    }
                }
            }

            #endregion TaishinPay

            #endregion 處理第三方支付取消授權&退款

            #region 信託相關資料設為未完成單
            foreach (Order o in orders)
            {
                CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(o.Guid, OrderClassification.LkSite);
                foreach (var ct in ctCol)
                {
                    #region cash_trust_log

                    ct.Status = (int)TrustStatus.ATM; //借用ATM的狀態設為未完成單
                    mp.CashTrustLogSet(ct);

                    #endregion cash_trust_log

                    #region user_cash_trust_log

                    UserCashTrustLogCollection uctCol = mp.UserCashTrustLogGet(ct.TrustId);
                    foreach (var uct in uctCol)
                    {
                        uct.Status = (int)TrustStatus.ATM; //借用ATM的狀態設為未完成單
                        mp.UserCashTrustLogSet(uct);
                    }

                    #endregion user_cash_trust_log

                    #region 處理商品數量退回

                    ItemFacade.RefundAccessoryMemberQuantityByOrderDetail(op.OrderDetailGet(ct.OrderDetailGuid));

                    #endregion 處理商品數量退回
                }

            }
            #endregion 信託相關資料設為未完成單

            #region 未完成單不開發票
            foreach (Order o in orders)
            {
                EinvoiceMainCollection invoices = op.EinvoiceMainGetListByOrderGuid(o.Guid);
                foreach (var invoice in invoices)
                {
                    invoice.InvoiceStatus = (int)EinvoiceType.NotComplete;
                    op.EinvoiceSetMain(invoice);
                }
            }
            #endregion 未完成單不開發票

            foreach (Order o in orders)
            {
                //設為已取消訂單
                GroupOrder gpo = op.GroupOrderGetByOrderGuid(o.ParentOrderId.GetValueOrDefault());
                OrderFacade.SetOrderStatus(o.Guid, "sys", OrderStatus.Cancel, true, gpo.BusinessHourGuid);
                OrderFacade.SetOrderStatus(o.Guid, "sys", OrderStatus.Complete, false, gpo.BusinessHourGuid);
            }
            return true;
        }


        public static bool IsRefundCashOnly(ViewPponCashTrustLog viewCtLog)
        {
            return IsRefundCashOnly(mp.CashTrustLogGet(viewCtLog.TrustId));
        }

        /// <summary>
        /// 判斷是否刷退
        /// </summary>
        /// <param name="ctLog">CashTrustLog</param>
        /// <returns></returns>
        public static bool IsRefundCashOnly(CashTrustLog ctLog)
        {
            bool isRefundCashOnly = false;

            // 墨攻核銷 或 信託華泰 一律刷退
            if (((TrustProvider)ctLog.TrustProvider).EqualsAny(TrustProvider.Sunny, TrustProvider.Hwatai))
            {
                isRefundCashOnly = true;
            }

            // 免稅檔次
            if (ctLog.OrderClassification == (int)OrderClassification.LkSite)
            {
                ViewPponDeal deal = (ctLog.BusinessHourGuid != null) ? pp.ViewPponDealGetByBusinessHourGuid(ctLog.BusinessHourGuid.Value) : null;
                if (deal != null && deal.GroupOrderStatus != null && Helper.IsFlagSet(deal.GroupOrderStatus.Value, GroupOrderStatus.NoTax))
                {
                    isRefundCashOnly = true;
                }
            }
            else if (ctLog.OrderClassification == (int)OrderClassification.HiDeal)
            {
                ViewHiDealCouponListSequenceCollection coupons = hp.GetHiDealCouponListSequenceListByOid(ctLog.OrderGuid);
                if (coupons.Count() > 0 && coupons.First().IsTaxFree.Value)
                {
                    isRefundCashOnly = true;
                }
            }
            else if (ctLog.FamilyIsp > 0 || ctLog.SevenIsp > 0)
            {
                isRefundCashOnly = true;
            }
            return isRefundCashOnly;
        }

        private static bool IsUnivoicedAmountInvoiced(int? couponId)
        {
            if (couponId != null)
            {
                EinvoiceMain invoice = op.EinvoiceMainGetByCouponid(couponId.Value);
                if (invoice != null && invoice.Id > 0 && invoice.VerifiedTime == null)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 取得 "允許ATM 轉帳的日期時間" . 在Pay頁會判斷, 如果過了這個時間就不能ATM 轉帳, 以免清冊產生遺漏
        /// </summary>
        /// <returns></returns>
        public static DateTime AtmAllowDateGet(DateTime orderDateE)
        {
            return new DateTime(orderDateE.AddDays(-1).Year, orderDateE.AddDays(-1).Month, orderDateE.AddDays(-1).Day, 12, 0, 0);
        }

        public static ATMRemittanceData ATMRemittanceDataGetByOrderGuid(Guid orderGuid)
        {
            Order order = op.OrderGet(orderGuid);
            if (!order.IsLoaded)
            {
                return null;
            }

            PaymentTransactionCollection ptc = op.PaymentTransactionGetList(0, 0, PaymentTransaction.Columns.Id,
                                                                            PaymentTransaction.Columns.OrderGuid + "=" +
                                                                            orderGuid,
                                                                            PaymentTransaction.Columns.OrderGuid + "=" +
                                                                            orderGuid,
                                                                            PaymentTransaction.Columns.TransType + "=" +
                                                                            (int)PayTransType.Authorization,
                                                                            PaymentTransaction.Columns.PaymentType + "=" +
                                                                            (int)PaymentType.ATM);

            if (ptc.Count == 0)
            {
                return null;
            }
            PaymentTransaction pt_atm1 = ptc.First();
            ATMRemittanceData rtn = new ATMRemittanceData();
            if (pt_atm1 != null && pt_atm1.Id != 0)
            {
                rtn.BankName = cp.AtmAccountBankName;
                rtn.BankCode = cp.AtmAccountBankCode;
                if (pt_atm1.Message.Length >= 14)
                {
                    rtn.BankAccount = pt_atm1.Message.Insert(3, "-").Insert(8, "-").Insert(13, "-");
                }
                else
                {
                    rtn.BankAccount = pt_atm1.Message;
                }

                rtn.Amount = pt_atm1.Amount;
                //兌換期限是訂單的購買日當天晚上23:59:00
                rtn.RemittanceDeadline = order.CreateTime.Date.AddMinutes(DateMinute - 1);
            }
            return rtn;
        }

        #region 中信ATM

        /// <summary>
        /// 取得確認碼
        /// </summary>
        /// <param name="checkcode">需確認字串</param>
        /// <param name="money">金額</param>
        /// <returns></returns>
        public static string CtAtmCheckCode(string checkcode, string money)
        {
            char[] key1 = checkcode.ToCharArray();
            char[] key2 = money.ToCharArray();
            int[] mask1 = new int[3] { 3, 7, 1 };
            //虛擬帳號產出規則修改, 由規則44改為54
            int[] mask2 = new int[3] { 3, 7, 1 };

            int sum1 = 0;
            int sum2 = 0;
            for (int i = 0; i < key1.Length; i++)
            {
                sum1 += (Convert.ToInt32(key1[i].ToString()) * mask1[i % mask1.Length]) % 10;
            }

            for (int i = 0; i < key2.Length; i++)
            {
                sum2 += (Convert.ToInt32(key2[i].ToString()) * mask2[i % mask2.Length]) % 10;
            }

            return ((10 - (sum1 % 10 + sum2 % 10) % 10) % 10).ToString();
        }

        /// <summary>
        /// 產出虛擬帳號
        /// </summary>
        /// <param name="amount">ATM金額</param>
        /// <returns>虛擬帳號 + 1碼檢查碼</returns>
        public static string CtAtmAccount(int amount)
        {
            string checkcode = cp.ChinaTrustAtmId +
                               DateTime.Now.Year.ToString().Substring(3, 1) +
                               DateTime.Now.DayOfYear.ToString().PadLeft(3, '0') +
                               (op.CtAtmGetMaxSi() % 1000).ToString().PadLeft(4, '0');
            return checkcode + CtAtmCheckCode(checkcode, amount.ToString().PadLeft(10, '0'));
        }

        public static void UpdateAtmStatus(AtmStatus atm, Guid orderGuid)
        {
            op.CtAtmUpdateStatus((int)atm, orderGuid);
        }

        /// <summary>
        /// 判斷檔次ATM數量是否滿
        /// </summary>
        /// <param name="bid">Business Guid</param>
        /// <returns></returns>
        public static bool CtAtmCheck(Guid bid)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            return CtAtmCheck(deal);
        }

        public static bool CtAtmCheck(IViewPponDeal deal)
        {
            if (deal.BusinessHourAtmMaximum > 0)
            {
                if (op.CtAtmGetDealCount(deal.BusinessHourGuid) < deal.BusinessHourAtmMaximum)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 判斷檔次是否有設 ATM
        /// </summary>
        /// <param name="bid">Business Guid</param>
        /// <returns></returns>
        public static bool CtAtmCheckZero(Guid bid)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            return deal.BusinessHourAtmMaximum == 0;
        }

        /// <summary>
        /// 產生虛擬帳號並記錄
        /// 
        /// 用回傳長度來驗證是否成功，14碼
        /// </summary>
        /// <param name="pid">Payment Transaction ID</param>
        /// <param name="userId">使用者編號</param>
        /// <param name="amount">金額</param>
        /// <param name="bid">Business Guid</param>
        /// <param name="orderId">Order ID</param>
        /// <param name="orderGuid">Order Guid</param>
        /// <returns></returns>
        public static string GetAtmAccount(string pid, int userId, int amount, Guid bid, string orderId, Guid orderGuid)
        {
            string result;

            if (op.CtAtmGetTodayCount(DateTime.Now) < 9990) //9990為防止超賣
            {
                if (CtAtmCheck(bid))
                {
                    //取得ATM虛擬帳號
                    result = CtAtmAccount(amount);
                    CtAtm atm = new CtAtm();
                    atm.BusinessGuid = bid;
                    atm.OrderId = orderId;
                    atm.OrderGuid = orderGuid;
                    atm.PaymentId = pid;
                    atm.UserId = userId;
                    atm.Amount = amount;
                    atm.CreateTime = DateTime.Now;
                    atm.VirtualAccount = result;
                    atm.Senq = result.Substring(5, 9);
                    atm.Status = (int)AtmStatus.Initial;
                    op.CtAtmSet(atm);
                }
                else
                {
                    result = "-2"; //deal上限已滿
                }
            }
            else
            {
                result = "-1";    //當日上限已滿
            }
            return result;
        }

        /// <summary>
        /// AP2AP存入
        /// </summary>
        /// <param name="seq">交易序號</param>
        /// <param name="type">帳務別 1: 現金 2: 轉帳 3: 本交(票據) 4: 次交(票據) 5: 撤票</param>
        /// <param name="istr">內容</param>
        /// <returns>httpstatus</returns>
        public static int CtAtmLogSave(string seq, string type, string istr)
        {
            try
            {
                if (op.CtAtmAp2ApLogCheck(seq))
                {
                    CtAtmMessageInform ctm = new CtAtmMessageInform();
                    decimal amount = Convert.ToDecimal(istr.Substring(34, 13));
                    string memo = istr.Substring(20, 14);
                    ctm.SEQNo = seq;
                    ctm.InfoType = type;
                    ctm.ResponseStats = "200";
                    ctm.ReceiveDate = DateTime.Now;
                    ctm.AccountNoReceive = istr.Substring(0, 12);   //轉入實體帳號
                    ctm.AccountType = istr.Substring(12, 1);        //帳務別
                    ctm.AccountDate = istr.Substring(13, 7);        //記帳日期
                    ctm.Memo1 = memo;                               //繳款人識別碼
                    ctm.TransactionAmount = amount;                 //交易金額
                    ctm.TransactionDate = istr.Substring(49, 7);    //交易日期
                    ctm.TransactionTime = istr.Substring(56, 6);    //交易時間
                    ctm.CrossBankNo = istr.Substring(62, 10);       //跨行序號
                    ctm.DebitCredit = istr.Substring(72, 1);        //借貸別
                    ctm.AmountType = istr.Substring(73, 1);         //正負別
                    ctm.ServiceManNo = istr.Substring(74, 5);       //櫃員代號
                    ctm.TransactionID = istr.Substring(79, 5);      //交易代碼
                    ctm.TransactionType = istr.Substring(84, 1);    //交易設備種類
                    ctm.AccountNoSend = istr.Substring(85, 19);     //轉出帳號
                    ctm.Memo2 = istr.Substring(104, 11);            //註記二
                    ctm.CashIncrease = istr.Substring(115, 35);     //前五碼為企業識碼，其餘為保留欄位
                    ctm.Remitter = istr.Substring(150, 80);         //付款人姓名
                    ctm.RemittanceMemo = istr.Substring(230, 80);   //匯款附言
                    ctm.MessageContent = istr;                      //本次訊息的內容全文

                    CtAtm atm = op.CtAtmGetBySeqno(memo.Trim(), Convert.ToInt32(amount));

                    PaymentTransactionCollection ptCol = op.PaymentTransactionGetListPaging(-1, -1, PaymentTransaction.Columns.Id, PayTransStatusFlag.None, 0,
                        PaymentTransaction.Columns.TransId + "=" + atm.PaymentId,
                        PaymentTransaction.Columns.TransType + "=" + (int)PayTransType.Authorization);

                    IEnumerable<PaymentTransaction> ptScashCol = ptCol.Where(x => x.PaymentType == (int)PaymentType.SCash);
                    PaymentTransaction ptScash = (ptScashCol.Count() > 0) ? ptScashCol.Where(x => x.PaymentType == (int)PaymentType.SCash).First() : null;
                    IEnumerable<PaymentTransaction> ptATMCol = ptCol.Where(x => x.PaymentType == (int)PaymentType.ATM);
                    PaymentTransaction ptATM = (ptATMCol.Count() > 0) ? ptATMCol.Where(x => x.PaymentType == (int)PaymentType.ATM).First() : null;
                    if (atm.Si > 0 && ptATM != null)
                    {
                        //必須在匯款截止日內將款項匯入
                        if (Convert.ToInt32(Helper.ToChineseDateTime(atm.CreateTime.AddDays(cp.AtmRemitDeadline))) > Convert.ToInt32(ctm.TransactionDate))
                        {
                            Order o = op.OrderGet(atm.OrderGuid);
                            OrderDetailCollection odCol = op.OrderDetailGetList(atm.OrderGuid);
                            ctm.ProcessStatus = Convert.ToInt16(AtmProcessStatus.Done);

                            OrderFacade.SetOrderStatus(atm.OrderGuid, atm.UserId, OrderStatus.Complete, true,
                                new SalesInfoArg
                                {
                                    BusinessHourGuid = atm.BusinessGuid,
                                    Total = odCol.OrderedTotal,
                                    Quantity = odCol.OrderedQuantity
                                });

                            #region ATM訂單成立時間必須在付款當下

                            o.CreateTime = DateTime.Now;
                            op.OrderSet(o);
                            
                            #endregion ATM訂單成立時間必須在付款當下

                            atm.Status = Convert.ToInt32(AtmStatus.Paid);       //設為已付款
                            atm.PayTime = DateTime.Now;
                            op.CtAtmSet(atm);

                            #region ATM轉購物金
                            
                            Guid sOrderGuid = OrderFacade.MakeSCachOrder(amount, amount, "17Life購物金購買(系統)",
                                CashPointStatus.Approve, o.UserId, o.SellerName, CashPointListType.Income, o.CreateId, o.Guid);

                            bool invoiced = (DateTime.Now < cp.NewInvoiceDate);    //Scash will not be invoiced initially after the NewInvoiceDate.
                            int depositId = OrderFacade.NewScashDeposit(sOrderGuid, amount, o.UserId, o.Guid,
                                                        "購買購物金-" + o.SellerName, o.CreateId, invoiced, OrderClassification.CashPointOrder);
                            OrderFacade.NewScashWithdrawal(depositId, amount, o.Guid, "折抵購物金-" + o.SellerName, o.CreateId, OrderClassification.CashPointOrder);
                            
                            ptATM.MarkOld();
                            ptATM.OrderGuid = sOrderGuid;
                            ptATM.Message += "|" + o.Guid.ToString();
                            ptATM.Status = Helper.SetPaymentTransactionPhase(ptATM.Status, PayTransPhase.Successful);
                            op.PaymentTransactionSet(ptATM);

                            NewTransaction(ptATM.TransId, ptATM.OrderGuid.Value, PaymentType.ATM, ptATM.Amount, null, PayTransType.Charging, 
                                DateTime.Now, "sys", string.Empty, ptATM.Status, PayTransResponseType.OK);
                            
                            if (ptScash == null)
                            {
                                int status = Helper.SetPaymentTransactionPhase(
                                    Helper.SetPaymentTransactionDepartmentTypes(
                                        0, Helper.GetPaymentTransactionDepartmentTypes(ptATM.Status)), PayTransPhase.Successful);
                                NewTransaction(atm.PaymentId, o.Guid, PaymentType.SCash, amount, null, PayTransType.Authorization, 
                                    DateTime.Now, MemberFacade.GetUserName(o.UserId), "17購物金折抵", status, PayTransResponseType.OK);
                            }
                            else
                            {
                                ptATM.MarkOld();
                                ptScash.Amount += amount;
                                op.PaymentTransactionSet(ptScash);
                            }
                            
                            #endregion ATM轉購物金
                            
                            #region 信託設定

                            int uninvoiced = (-1) * Convert.ToInt32(op.ViewScashTransactionGetList(0, 0, ViewScashTransaction.Columns.CreateTime,
                                ViewScashTransaction.Columns.Amount + "<0", ViewScashTransaction.Columns.OrderGuid + "=" + atm.OrderGuid,
                                ViewScashTransaction.Columns.Invoiced + "=false").Sum(x => x.Amount).Value);

                            CashTrustLogCollection ctl = mp.CashTrustLogGetListByOrderGuid((Guid)atm.OrderGuid, OrderClassification.LkSite);
                            foreach (var c in ctl)
                            {
                                c.Status = (int)TrustStatus.Initial;
                                c.ModifyTime = DateTime.Now;
                                mp.UserCashTrustSetToInitialFromAtm(c.TrustId);           //設為initial

                                #region 紀錄ATM未開立發票金額

                                if (uninvoiced > 0)
                                {
                                    c.UninvoicedAmount = (uninvoiced - c.Atm > 0) ? c.Atm : uninvoiced;
                                    uninvoiced -= c.UninvoicedAmount;
                                }

                                #endregion 紀錄ATM未開立發票金額

                                #region 異動cash_trust_log status 增加cash_trust_status_log紀錄

                                var ctsl = new CashTrustStatusLog();
                                ctsl.TrustId = c.TrustId;
                                ctsl.TrustProvider = c.TrustProvider;
                                ctsl.Amount = c.CreditCard + c.Scash + c.Atm;
                                ctsl.ModifyTime = c.ModifyTime;
                                ctsl.Status = c.Status;
                                ctsl.CreateId = "sys";

                                #endregion #region 異動cash_trust_log status 增加cash_trust_status_log紀錄

                                if (mp.CashTrustLogSet(c)) //設為initial
                                {
                                    mp.CashTrustStatusLogSet(ctsl);
                                }
                            }

                            #region 紀錄購物金未開立發票金額

                            ctl = mp.CashTrustLogGetListByOrderGuid((Guid)atm.OrderGuid, OrderClassification.LkSite);
                            foreach (var c in ctl)
                            {
                                if (uninvoiced > 0)
                                {
                                    int newScash = (uninvoiced - c.Scash > 0) ? c.Scash : uninvoiced;
                                    c.UninvoicedAmount += newScash;
                                    uninvoiced -= newScash;
                                }
                                mp.CashTrustLogSet(c);
                            }

                            #endregion 紀錄購物金未開立發票金額

                            pp.SetAtmCouponToQueue((Guid)atm.OrderGuid);               //將atm簡訊設成queue

                            #endregion 信託設定

                            #region 發票設定

                            ctl = mp.CashTrustLogGetListByOrderGuid((Guid)atm.OrderGuid, OrderClassification.LkSite);
                            bool IsGroupCouponDeal = Helper.IsFlagSet(op.GetHourStatusBybid(ctl.FirstOrDefault().BusinessHourGuid.Value).BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon); //憑證成套販售
                            if (DateTime.Now < cp.NewInvoiceDate)
                            {
                                EinvoiceMain invoice = op.EinvoiceMainGet(o.Guid);
                                if (invoice != null)
                                {
                                    invoice.MarkOld();
                                    invoice.InvoiceStatus = (int)EinvoiceType.Initial;
                                    op.EinvoiceSetMain(invoice);
                                }
                            }
                            else
                            {
                                if (ctl.Count() > 0)
                                {
                                    if (ctl.First().CouponId == null)
                                    {
                                        int uninvoicedTotal = ctl.Sum(x => x.UninvoicedAmount);
                                        EinvoiceMain invoice = op.EinvoiceMainGet(o.Guid);
                                        if (invoice != null)
                                        {
                                            invoice.MarkOld();
                                            invoice.OrderAmount = uninvoicedTotal;
                                            invoice.InvoiceStatus = (int)EinvoiceType.Initial;
                                            invoice.OrderTime = o.CreateTime;
                                            invoice.VerifiedTime = o.CreateTime;
                                            op.EinvoiceSetMain(invoice);
                                        }
                                    }
                                    else
                                    {
                                        foreach (var c in ctl)
                                        {
                                            if (c.UninvoicedAmount > 0 && !IsGroupCouponDeal)
                                            {
                                                EinvoiceMain invoice = op.EinvoiceMainGetByCouponid(c.CouponId.Value);
                                                if (invoice != null)
                                                {
                                                    invoice.MarkOld();
                                                    invoice.OrderAmount = c.UninvoicedAmount;
                                                    invoice.InvoiceStatus = (int)EinvoiceType.Initial;
                                                    invoice.CouponId = c.CouponId;
                                                    invoice.OrderTime = o.CreateTime;
                                                    op.EinvoiceSetMain(invoice);
                                                }
                                            }
                                        }
                                    }

                                    if (IsGroupCouponDeal)
                                    {
                                        int UninvoicedAmount = ctl.Sum(x => x.UninvoicedAmount);
                                        if (UninvoicedAmount > 0)
                                        {
                                            EinvoiceMain invoice = op.EinvoiceMainGet(o.Guid);
                                            if (invoice != null)
                                            {
                                                invoice.MarkOld();
                                                invoice.OrderAmount = UninvoicedAmount;
                                                invoice.InvoiceStatus = (int)EinvoiceType.Initial;
                                                invoice.OrderTime = o.CreateTime;
                                                op.EinvoiceSetMain(invoice);
                                            }
                                        }
                                    }
                                }
                            }

                            #endregion 發票設定

                            #region 寄信

                            ViewPponDeal pponDeal = pp.ViewPponDealGetByBusinessHourGuid(atm.BusinessGuid);

                            bool dealOn = pponDeal.OrderedQuantity >= pponDeal.BusinessHourOrderMinimum;

                            if (pponDeal.ItemPrice == 0)
                            {
                                var couponId = ctl.First().CouponId == null ? 0 : Convert.ToInt32(ctl.First().CouponId);
                                
                                OrderFacade.SendZeroDealSuccessMail(o.MemberName, MemberFacade.GetUserEmail(o.UserId), pponDeal, 
                                    ctl.First().CouponId == null ? null : pp.ViewPponCouponGet(couponId),
                                    (GroupOrderStatus)pponDeal.GroupOrderStatus.Value,
                                    (CouponCodeType)pponDeal.CouponCodeType.Value);
                            }
                            else
                            {
                                OrderFacade.SendPponMail(MailContentType.Authorized, (Guid)atm.OrderGuid);    //付款成功信
                            }

                            #endregion 寄信

                            #region 付款成功推播
                           
                            var msg = string.Format("17Life商品(訂單：{0})已出貨囉，查看詳情", o.OrderId);
                            NotificationFacade.PushMemberOrderMessageOnWorkTime(o.UserId, msg, o.Guid);
                            logger.Info("宅配發送已出貨推播 VendorBillingSystemShip.SaveOrderShip.Push: " +
                                        string.Format("UserId={0}, pmsg={1}, Guid={2}", o.UserId, msg, o.OrderId));
                            
                            #endregion

                            #region 檢查訂單所屬檔次是否需更新結檔份數

                            int slug;
                            if (pponDeal.BusinessHourOrderTimeE < DateTime.Now && atm.PayTime > pponDeal.BusinessHourOrderTimeE 
                            && !string.IsNullOrEmpty(pponDeal.Slug) && int.TryParse(pponDeal.Slug, out slug))
                            {
                                //計算訂單份數
                                var orderCount = ctl.Count(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight));
                                
                                //更新結檔份數
                                var groupOrder = op.GroupOrderGetByBid(pponDeal.BusinessHourGuid);
                                var updateSlug = slug + orderCount;
                                groupOrder.Slug = updateSlug.ToString();
                                op.GroupOrderSet(groupOrder);
                                CommonFacade.AddAudit(pponDeal.BusinessHourGuid, AuditType.GroupOrder, string.Format("因ATM訂單OrderGuid:{0}完成付款時間晚於結檔時間 需異動結檔份數Slug：{1} -> {2}", atm.OrderGuid, slug, updateSlug), "sys", false);
                            }

                            #endregion 檢查訂單所屬檔次是否需更新結檔份數

                        }
                        else
                        {
                            ctm.ProcessStatus = Convert.ToInt16(AtmProcessStatus.NotFound);
                        }
                    }
                    else
                    {
                        ctm.ProcessStatus = Convert.ToInt16(AtmProcessStatus.NotFound);
                    }

                    if (ptATM != null & ctm.ProcessStatus != Convert.ToInt16(AtmProcessStatus.Done))
                    {
                        ptATM.MarkOld();
                        ptATM.Status = Helper.SetPaymentTransactionPhase(ptATM.Status, PayTransPhase.Failed);
                        op.PaymentTransactionSet(ptATM);
                    }

                    op.CtAtmLogSet(ctm);
                    return 200;
                }
                else
                {   //重複
                    return 250;
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger("LogToDb").Error(string.Format("PaymentFacade.CtAtmLogSave error:Exception:{0}", ex));
                return 543;
            }
        }

        /// <summary>
        /// 用於超取未完成訂單，找時間改成ATM能共用
        /// </summary>
        /// <param name="o"></param>
        /// <param name="bid"></param>
        /// <param name="amount"></param>
        /// <param name="pType"></param>
        /// <returns></returns>
        public static bool OrderComplete(ViewPponOrder vpo, decimal amount, PaymentType pType)
        {
            try
            {
                var transId = op.PaymentTransactionGet(vpo.Guid).TransId;
                
                PaymentTransactionCollection ptCol = op.PaymentTransactionGetListPaging(-1, -1, PaymentTransaction.Columns.Id, PayTransStatusFlag.None, 0,
                                       PaymentTransaction.Columns.TransId + "=" + transId,
                                       PaymentTransaction.Columns.TransType + "=" + (int)PayTransType.Authorization);

                IEnumerable<PaymentTransaction> ptScashCol = ptCol.Where(x => x.PaymentType == (int)PaymentType.SCash);
                PaymentTransaction ptScash = (ptScashCol.Count() > 0) ? ptScashCol.Where(x => x.PaymentType == (int)PaymentType.SCash).First() : null;
                IEnumerable<PaymentTransaction> ptISPCol = ptCol.Where(x => x.PaymentType == (int)pType);
                PaymentTransaction ptIspCash = (ptISPCol.Count() > 0) ? ptISPCol.Where(x => x.PaymentType == (int)pType).First() : null;

                Order o = op.OrderGet(vpo.Guid);
                OrderDetailCollection odCol = op.OrderDetailGetList(vpo.Guid);

                OrderFacade.SetOrderStatus(vpo.Guid, vpo.UserId, OrderStatus.Complete, true,
                    new SalesInfoArg
                    {
                        BusinessHourGuid = vpo.BusinessHourGuid,
                        Total = odCol.OrderedTotal,
                        Quantity = odCol.OrderedQuantity
                    });

                o.CreateTime = DateTime.Now;
                op.OrderSet(o);

                //轉成購物金
                Guid sOrderGuid = OrderFacade.MakeSCachOrder(amount, amount, "17Life購物金購買(系統)",
                    CashPointStatus.Approve, vpo.UserId, vpo.SellerName, CashPointListType.Income, vpo.CreateId, vpo.Guid);

                bool invoiced = false;    //Scash will not be invoiced initially after the NewInvoiceDate.
                int depositId = OrderFacade.NewScashDeposit(sOrderGuid, amount, vpo.UserId, vpo.Guid,
                                            "購買購物金-" + vpo.SellerName, vpo.CreateId, invoiced, OrderClassification.CashPointOrder);
                OrderFacade.NewScashWithdrawal(depositId, amount, vpo.Guid, "折抵購物金-" + vpo.SellerName, vpo.CreateId, OrderClassification.CashPointOrder);

                ptIspCash.MarkOld();
                ptIspCash.OrderGuid = sOrderGuid;
                ptIspCash.Message += "|" + o.Guid.ToString();
                ptIspCash.Status = Helper.SetPaymentTransactionPhase(ptIspCash.Status, PayTransPhase.Successful);
                op.PaymentTransactionSet(ptIspCash);

                PaymentFacade.NewTransaction(ptIspCash.TransId, ptIspCash.OrderGuid.Value, pType, ptIspCash.Amount, null, PayTransType.Charging,
                    DateTime.Now, "sys", string.Empty, ptIspCash.Status, PayTransResponseType.OK);

                if (ptScash == null)
                {
                    int status = Helper.SetPaymentTransactionPhase(
                        Helper.SetPaymentTransactionDepartmentTypes(
                            0, Helper.GetPaymentTransactionDepartmentTypes(ptIspCash.Status)), PayTransPhase.Successful);
                    PaymentFacade.NewTransaction(ptIspCash.TransId, vpo.Guid, PaymentType.SCash, amount, null, PayTransType.Authorization,
                        DateTime.Now, MemberFacade.GetUserName(vpo.UserId), "17購物金折抵", status, PayTransResponseType.OK);
                }
                else
                {
                    ptIspCash.MarkOld();
                    ptScash.Amount += amount;
                    op.PaymentTransactionSet(ptScash);
                }

                #region 信託設定

                int uninvoiced = (-1) * Convert.ToInt32(op.ViewScashTransactionGetList(0, 0, ViewScashTransaction.Columns.CreateTime,
                    ViewScashTransaction.Columns.Amount + "<0", ViewScashTransaction.Columns.OrderGuid + "=" + vpo.Guid,
                    ViewScashTransaction.Columns.Invoiced + "=false").Sum(x => x.Amount).Value);

                CashTrustLogCollection ctl = mp.CashTrustLogGetListByOrderGuid(vpo.Guid, OrderClassification.LkSite);

                foreach (var c in ctl)
                {
                    c.Status = (int)TrustStatus.Initial;
                    c.ModifyTime = DateTime.Now;
                    mp.UserCashTrustSetToInitialFromAtm(c.TrustId);           //設為initial

                    #region 紀錄未開立發票金額

                    if (uninvoiced > 0)
                    {
                        c.UninvoicedAmount = (uninvoiced - c.FamilyIsp - c.SevenIsp - c.Atm > 0) ? c.FamilyIsp + c.SevenIsp + c.Atm : uninvoiced;
                        uninvoiced -= c.UninvoicedAmount;
                    }

                    #endregion 紀錄未開立發票金額

                    #region 異動cash_trust_log status 增加cash_trust_status_log紀錄

                    var ctsl = new CashTrustStatusLog();
                    ctsl.TrustId = c.TrustId;
                    ctsl.TrustProvider = c.TrustProvider;
                    ctsl.Amount = c.CreditCard + c.Scash + c.Atm + c.FamilyIsp + c.SevenIsp;
                    ctsl.ModifyTime = c.ModifyTime;
                    ctsl.Status = c.Status;
                    ctsl.CreateId = "sys";

                    #endregion #region 異動cash_trust_log status 增加cash_trust_status_log紀錄

                    if (mp.CashTrustLogSet(c)) //設為initial
                    {
                        mp.CashTrustStatusLogSet(ctsl);
                    }
                }

                #region 紀錄購物金未開立發票金額

                ctl = mp.CashTrustLogGetListByOrderGuid((Guid)ptIspCash.OrderGuid, OrderClassification.LkSite);
                foreach (var c in ctl)
                {
                    if (uninvoiced > 0)
                    {
                        int newScash = (uninvoiced - c.Scash > 0) ? c.Scash : uninvoiced;
                        c.UninvoicedAmount += newScash;
                        uninvoiced -= newScash;
                    }
                    mp.CashTrustLogSet(c);
                }

                #endregion 紀錄購物金未開立發票金額

                #endregion 信託設定

                #region 發票設定
                //reload
                ctl = mp.CashTrustLogGetListByOrderGuid(vpo.Guid, OrderClassification.LkSite);
                bool isGroupCouponDeal = Helper.IsFlagSet(op.GetHourStatusBybid(ctl.FirstOrDefault().BusinessHourGuid.Value).BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon); //憑證成套販售


                if (ctl.Count() > 0)
                {
                    if (ctl.First().CouponId == null)
                    {
                        int uninvoicedTotal = ctl.Sum(x => x.UninvoicedAmount);
                        EinvoiceMain invoice = op.EinvoiceMainGet(vpo.Guid);
                        if (invoice != null)
                        {
                            invoice.MarkOld();
                            invoice.OrderAmount = uninvoicedTotal;
                            invoice.InvoiceStatus = (int)EinvoiceType.Initial;
                            invoice.OrderTime = vpo.CreateTime;
                            invoice.VerifiedTime = DateTime.Now; //超取壓當下的接回來的時間
                            op.EinvoiceSetMain(invoice);
                        }
                    }
                    else
                    {
                        foreach (var c in ctl)
                        {
                            if (c.UninvoicedAmount > 0 && !isGroupCouponDeal)
                            {
                                EinvoiceMain invoice = op.EinvoiceMainGetByCouponid(c.CouponId.Value);
                                if (invoice != null)
                                {
                                    invoice.MarkOld();
                                    invoice.OrderAmount = c.UninvoicedAmount;
                                    invoice.InvoiceStatus = (int)EinvoiceType.Initial;
                                    invoice.CouponId = c.CouponId;
                                    invoice.OrderTime = vpo.CreateTime;
                                    op.EinvoiceSetMain(invoice);
                                }
                            }
                        }
                    }

                    if (isGroupCouponDeal)
                    {
                        int UninvoicedAmount = ctl.Sum(x => x.UninvoicedAmount);
                        if (UninvoicedAmount > 0)
                        {
                            EinvoiceMain invoice = op.EinvoiceMainGet(vpo.Guid);
                            if (invoice != null)
                            {
                                invoice.MarkOld();
                                invoice.OrderAmount = UninvoicedAmount;
                                invoice.InvoiceStatus = (int)EinvoiceType.Initial;
                                invoice.OrderTime = vpo.CreateTime;
                                op.EinvoiceSetMain(invoice);
                            }
                        }
                    }
                }


                #endregion 發票設定

                ViewPponDeal pponDeal = pp.ViewPponDealGetByBusinessHourGuid(vpo.BusinessHourGuid);

                bool dealOn = pponDeal.OrderedQuantity >= pponDeal.BusinessHourOrderMinimum;

                #region 檢查訂單所屬檔次是否需更新結檔份數

                int slug;
                if (pponDeal.BusinessHourOrderTimeE < DateTime.Now
                && !string.IsNullOrEmpty(pponDeal.Slug) && int.TryParse(pponDeal.Slug, out slug))
                {
                    //計算訂單份數
                    var orderCount = ctl.Count(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight));

                    //更新結檔份數
                    var groupOrder = op.GroupOrderGetByBid(pponDeal.BusinessHourGuid);
                    var updateSlug = slug + orderCount;
                    groupOrder.Slug = updateSlug.ToString();
                    op.GroupOrderSet(groupOrder);
                    CommonFacade.AddAudit(pponDeal.BusinessHourGuid, AuditType.GroupOrder, string.Format("因超取訂單OrderGuid:{0}完成付款時間晚於結檔時間 需異動結檔份數Slug：{1} -> {2}", vpo.Guid, slug, updateSlug), "sys", false);
                }

                #endregion 檢查訂單所屬檔次是否需更新結檔份數
            }
            catch(Exception ex)
            {
                return false;
            }

            return true;
        }

        public static CtAtmRefundCollection CtAtmRefundGetList()
        {
            op.CtAtmAchMarkRefundItem();
            return op.CtAtmRefundGetList();
        }

        public static ViewCtAtmRefundCollection ViewCtAtmRefundGetList()
        {
            op.CtAtmAchMarkRefundItem();
            return op.ViewCtAtmRefundGetList();
        }

        public static CtAtmCollection CtAtmGetList(DateTime d)
        {
            return op.CtAtmGetList(d);
        }

        public static CtAtmRefundCollection CtAtmGetRefundList(DateTime d)
        {
            return op.CtAtmGetRefundList(d);
        }

        public static ViewCtAtmRefundCollection ViewCtAtmRefundGetListIn(int[] list)
        {
            return op.ViewCtAtmRefundGetListIn(list);
        }

        public static CtAtmRefundCollection CtAtmGetRefundFailList()
        {
            string[] list = new string[] { "04:無此帳號", "05:收受者統編錯誤", "22:帳戶已結清", "23:靜止戶", "24:凍結戶", "25:帳戶存款遭法院強制執行", "99:其它" };
            return op.CtAtmGetRefundFailListIn(list);
        }

        public static CtAtmP1logCollection CtAtmGetP1LogList()
        {
            op.CtAtmAchMarkRefundItem();
            return op.CtAtmGetP1LogList();
        }

        /// <summary>
        /// 存入ACH P1 資料
        /// </summary>
        /// <param name="body"></param>
        /// <param name="name">使用者</param>
        /// <param name="t">target time</param>
        /// <param name="count">筆數</param>
        /// <param name="total">金額</param>
        /// <param name="list">各筆資料</param>
        /// <returns></returns>
        public static bool CtAtmAchLogSave(string body, string name, DateTime t, int count, int total, string list)
        {
            CtAtmP1log p1 = new CtAtmP1log();
            p1.Body = body;
            p1.CreateTime = DateTime.Now;
            p1.TargetDate = t;
            p1.UserName = name;
            p1.Count = count;
            p1.Amount = total;
            p1.Refundlist = list;
            return op.CtAtmAchP1LogSet(p1);
        }

        public static bool CtAtmRefundSet(CtAtmRefund refund)
        {
            return op.CtAtmRefundSet(refund);
        }

        public static string CtAtmAchR1LogSave(string body, string name)
        {
            CtAtmR1log r1Log = new CtAtmR1log();
            r1Log.Body = body;
            r1Log.CreateTime = DateTime.Now;
            r1Log.UserName = name;
            op.CtAtmAchR1LogSet(r1Log);
            return CtAtmAchR1LogParse(body);
        }

        /// <summary>
        /// 處理ACH回覆資料
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        public static string CtAtmAchR1LogParse(string body)
        {
            string result = string.Empty;
            string failList = string.Empty;
            string[] R1Line = body.Split("\r\n");
            int iCount;
            int iTotal;
            DateTime date;
            if (R1Line.Length > 2)             //have body
            {
                //head
                if (R1Line[0].Length == 160 && R1Line[R1Line.Length - 1].Length == 160)   //head=160
                {
                    if (R1Line[0].Substring(0, 7) == "BOFACHR" && R1Line[R1Line.Length - 1].Substring(0, 7) == "EOFACHR")
                    {
                        date = Helper.ChineseDateTimeParse(R1Line[0].Substring(9, 8));
                        iCount = int.Parse(R1Line[R1Line.Length - 1].Substring(31, 8));
                        iTotal = int.Parse(R1Line[R1Line.Length - 1].Substring(39, 16));
                        for (int i = 1; i <= iCount; i++)
                        {
                            string bankCode = R1Line[i].Substring(12, 7);
                            string userAccount = R1Line[i].Substring(19, 14);
                            int amount = int.Parse(R1Line[i].Substring(54, 10));
                            string reason = R1Line[i].Substring(64, 2);
                            string si = R1Line[i].Substring(108, 20).Trim();
                            CtAtmRefund refund = op.CtAtmRefundGetForAchReturn(si, bankCode, userAccount, amount, date);
                            if (refund.IsLoaded)
                            {
                                refund.FailReason = CtAtmAchRefundReason(reason);
                                refund.RefundResponseTime = DateTime.Now;
                                if (reason == "00")
                                {
                                    refund.Status = (int)AtmRefundStatus.RefundSuccess;
                                    op.CtAtmRefundSet(refund);
                                    PaymentFacade.UpdateAtmStatus(AtmStatus.Refund, refund.OrderGuid);
                                }
                                else
                                {
                                    refund.Status = (int)AtmRefundStatus.RefundFail;
                                    op.CtAtmRefundSet(refund);
                                    PaymentFacade.UpdateAtmStatus(AtmStatus.RefundFail, refund.OrderGuid);
                                }

                                switch (reason)
                                {
                                    case "00":
                                        break;

                                    case "04":
                                    case "05":
                                    case "22":
                                    case "23":
                                    case "24":
                                    case "25":
                                        //OrderFacade.SendPponMail(MailContentType.ReturnFailure, refund.OrderGuid, refund.FailReason);  //退貨失敗信
                                        break;

                                    default:
                                        failList += "銀行代碼:" + bankCode + " 帳號:" + userAccount + " 金額:" + amount + " 結果:" + refund.FailReason + "</br>";
                                        break;
                                }

                                result += "銀行代碼:" + bankCode + " 帳號:" + userAccount + " 金額:" + amount + " 結果:" + refund.FailReason + "\r\n";
                            }
                            else
                            {
                                result += "銀行代碼:" + bankCode + " 帳號:" + userAccount + " 金額:" + amount + " 查無資料\r\n";
                            }
                        }

                        //寄信通知
                        if (failList.Length > 0)
                        {
                            try
                            {
                                MailMessage msg = new MailMessage();
                                msg.To.Add("it@17life.com.tw");
                                msg.To.Add(cp.PlanningEmail);
                                msg.To.Add(cp.FinanceEmail);
                                msg.Subject = "[系統] ATM 退貨失敗需處理名單";
                                msg.From = new MailAddress(cp.AdminEmail);
                                msg.IsBodyHtml = true;
                                msg.Body = "在 " + DateTime.Now.ToString() + " 時處理的ATM退貨，有以下失敗需處理。</br></br>" + failList;

                                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    else
                    {
                        result = "格式錯誤，表頭格式錯誤";
                    }
                }
                else
                {
                    result = "格式錯誤，長度不為160";
                }
            }
            else
            {
                //error
                result = "格式錯誤，列數過少";
            }

            return result;
        }

        public static string CtAtmAchRefundReason(string reason)
        {
            string result = "未知錯誤";
            switch (reason)
            {
                case "00":
                    result = "00:退款成功";
                    break;

                case "01":
                    result = "01:存款不足";
                    break;

                case "02":
                    result = "02:非委託用戶";
                    break;

                case "03":
                    result = "03:已終止委託用戶";
                    break;

                case "04":
                    result = "04:無此帳號";
                    break;

                case "05":
                    result = "05:收受者統編錯誤";
                    break;

                case "06":
                    result = "06:無此用戶號碼";
                    break;

                case "07":
                    result = "07:用戶號碼不符";
                    break;

                case "22":
                    result = "22:帳戶已結清";
                    break;

                case "23":
                    result = "23:靜止戶";
                    break;

                case "24":
                    result = "24:凍結戶";
                    break;

                case "25":
                    result = "25:帳戶存款遭法院強制執行";
                    break;

                case "91":
                    result = "91:票交所未交易或匯入失敗資料";
                    break;

                case "99":
                    result = "99:其它";
                    break;

                default:
                    result = "未知錯誤";
                    break;
            }
            return result;
        }

        #endregion 中信ATM

        #region DiscountCode

        /// <summary>
        /// 寫入DiscountCode的使用紀錄
        /// </summary>
        /// <param name="discountCode">現金卷編號</param>
        /// <param name="orderGuid">使用的訂單編號</param>
        /// <param name="classification">訂單類型</param>
        /// <param name="useAmount">使用金額</param>
        /// <param name="userId">使用會員編號</param>
        /// <param name="orderAmount">訂單總金額(包含運費等有的沒的)</param>
        /// <param name="orderCost">訂單扣除其他付款方式後，剩餘要以現金支付的金額(刷卡或ATM)</param>
        /// <param name="receivableId">應收帳款單編號</param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool DiscountCodeUseSet(string discountCode, Guid orderGuid, OrderClassification classification, decimal useAmount,
            int userId, decimal orderAmount, decimal orderCost, int? receivableId, out string message)
        {
            message = string.Empty;
            var discount = op.DiscountCodeGetByCodeWithoutUseTime(discountCode);
            if ((!discount.IsLoaded) || (discount.Amount == null))
            {
                message = "17life現金卷不存在";
                return false;
            }

            if (useAmount > discount.Amount)
            {
                message = "欲扣抵金額大於現金卷額度。";
                return false;
            }

            if (userId == 0)
            {
                message = "會員登入資料有誤。";
                return false;
            }

            discount.UseTime = DateTime.Now;
            discount.UseAmount = useAmount;
            discount.UseId = userId;
            discount.OrderGuid = orderGuid;
            discount.OrderAmount = orderAmount;
            discount.OrderCost = orderCost;
            discount.OrderClassification = (int)classification;
            discount.ReceivableId = receivableId;
            op.DiscountCodeSet(discount);

            return true;
        }

        #endregion DiscountCode

        #region 防盜刷

        public static List<Creditcard> GetCreditcardOrderInfo(string strType, string strMain, string strRes, string strRefer, string strBl, string strAllOrders
            , string strSDate, string strEDate, string strSTime, string strETime)
        {
            Dictionary<string, string> condition = new Dictionary<string, string>();

            #region 主要查詢項目類別

            if (strType != "def")
            {
                condition["type"] = strType;
            }

            if (!string.IsNullOrEmpty(strType))
            {
                if (strType == "creditcard")
                {
                    condition["creditcard"] = new Security(SymmetricCryptoServiceProvider.AES).Encrypt(strMain);
                }
                else if (strType == "order")
                {

                    ViewOrderMemberBuildingSellerCollection odr = op.ViewOrderMemberBuildingSellerGetListPaging(1, 1, "", strMain, null, "", null, null, null, false);

                    Guid oid = new Guid();
                    Guid.TryParse(odr[0].OrderGuid.ToString(), out oid);
                    if (oid != new Guid())
                    {
                        condition["orderguid"] = oid.ToString();
                    }
                }
                else if (strType == "memberid")
                {
                    Member mem = mp.MemberGet(strMain);
                    condition["memberid"] = mem.UniqueId.ToString();
                }
                else if (strType == "membername")
                {
                    MemberCollection mems = mp.MemberGetListByName(strMain);
                    string idList = string.Empty;
                    if (mems.Count > 0)
                    {
                        for (int i = 0; i < mems.Count; i++)
                        {
                            if (i != 0)
                            {
                                idList += ",";
                            }
                            idList += mems[i].UniqueId.ToString();
                        }
                        condition["membername"] = idList;
                    }
                }
                else if (strType == "info")
                {
                    condition["info"] = new Security(SymmetricCryptoServiceProvider.AES).Encrypt(strMain);
                }
            }

            #endregion 主要查詢項目類別

            #region 時間

            if (!string.IsNullOrEmpty(strSDate) || !string.IsNullOrEmpty(strEDate))
            {
                if (string.IsNullOrEmpty(strSDate))
                {
                    condition["time"] = strEDate + " " + strETime;
                }
                else if (string.IsNullOrEmpty(strEDate))
                {
                    condition["time"] = strSDate + " " + strSTime;
                }
                else
                {
                    string sTime = string.Empty, eTime = string.Empty;
                    DateTime st = DateTime.Now;
                    DateTime.TryParse(strSDate + " " + strSTime, out st);
                    DateTime et = DateTime.Now;
                    DateTime.TryParse(strEDate + " " + strETime, out et);

                    if (DateTime.Compare(st, et) > 0)
                    {
                        sTime = string.Format(TimeFormate, et);
                        eTime = string.Format(TimeFormate, st);
                    }
                    else if (DateTime.Compare(st, et) == 0)
                    {
                        sTime = string.Format(TimeFormate, st);
                        eTime = string.Format(TimeFormate, et.AddDays(1));
                    }
                    else
                    {
                        sTime = string.Format(TimeFormate, st);
                        eTime = string.Format(TimeFormate, et);
                    }
                    condition["time"] = sTime;
                    condition["time2"] = eTime;
                }
            }

            #endregion 時間

            #region 刷卡結果

            int result = -99999999;
            if (strRes != "def" && int.TryParse(strRes, out result))
            {
                if (result == 99999999)
                {
                    condition["result"] = "0";
                }
                else
                {
                    condition["result"] = result.ToString();
                }
            }

            #endregion 刷卡結果

            #region 是否照會銀行

            if (strRefer == "refer")
            {
                condition["reference"] = true.ToString();
            }

            #endregion 是否照會銀行

            #region 是否為黑名單

            if (strBl == "blacklist")
            {
                condition["locked"] = true.ToString();
            }

            #endregion 是否為黑名單

            #region 尋找該會員所有訂單

            if (strAllOrders == "allorders")
            {
                condition["allorders"] = true.ToString();
            }

            #endregion 尋找該會員所有訂單

            return Creditcard.GetCreditcardList(op.GetCreditcardOrderList(condition));
        }

        /// <summary>
        /// 照會成功
        /// </summary>
        /// <param name="aryId"></param>
        /// <param name="strUserId"></param>
        /// <returns></returns>
        public static bool SetCreditcardOrderRefer(string[] aryId, string strUserId)
        {
            int id = -1;
            foreach (string sid in aryId)
            {
                int.TryParse(sid, out id);
                op.UpdateCreditcardOrderByIdWithRefer(id, strUserId, false);

                

                #region 照會成功備註
                CreditcardOrder creditcardOrder = op.CreditcardOrderGet(id);
                if (creditcardOrder != null)
                {
                    OrderUserMemoList userMemo = new OrderUserMemoList();
                    userMemo.OrderGuid = creditcardOrder.OrderGuid;
                    userMemo.DealType = (int)VbsDealType.Ppon;
                    userMemo.UserMemo = "**照會成功，請正常出貨**";
                    userMemo.CreateId = strUserId;
                    userMemo.CreateTime = DateTime.Now;
                    op.OrderUserMemoListSet(userMemo);
                }
                #endregion 照會成功備註

                try
                {
                    ViewPponOrderDetail ods = op.ViewPponOrderDetailGetListByOrderGuid(creditcardOrder.OrderGuid).FirstOrDefault(x => x.OrderDetailStatus == (int)OrderDetailStatus.None);
                    ViewOrderMemberBuildingSeller odr = op.ViewOrderMemberBuildingSellerGet(creditcardOrder.OrderGuid);
                    //抓取訂單出貨資訊
                    ViewOrderShipList osInfo = op.ViewOrderShipListGetListByOrderGuid(creditcardOrder.OrderGuid).FirstOrDefault();

                    if (osInfo.ShipTime == null)
                    {
                        Seller seller = sp.SellerGet(odr.SellerGuid);
                        ShipReferNoticeSellerEmail template = TemplateFactory.Instance().GetTemplate<ShipReferNoticeSellerEmail>();
                        template.Seller_Name = odr.SellerName;
                        //template.Deal_Name = ods.ItemName;
                        template.Refer_Count = "1";
                        template.Unique_id = odr.UniqueId.ToString();
                        template.Business_hour_guid = ods.BusinessHourGuid.ToString().Trim();

                        #region #region 抓取商家聯絡人mail
                        List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                        List<string> mails = new List<string>();
                        string userEmail = string.Empty;
                        if (contacts != null)
                        {
                            foreach (var c in contacts)
                            {
                                if (c.Type == ((int)ProposalContact.Normal).ToString() || c.Type == ((int)ProposalContact.ReturnPerson).ToString())
                                {
                                    if (!string.IsNullOrEmpty(c.ContactPersonEmail))
                                    {
                                        if (!mails.Contains(c.ContactPersonEmail))
                                        {
                                            mails.Add(c.ContactPersonEmail);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        userEmail = string.Join(",", mails);

                        var mailTo = RegExRules.CheckMultiEmail(userEmail);
                        if (string.IsNullOrEmpty(mailTo))
                        {
                            string errorMsg = string.Format("商家:{0}<a href='{1}'>{2}</a> 聯絡人mail有誤，無法發送照會成功信件。聯絡人Email: \"{3}\"", seller.SellerId, cp.SiteUrl + "/sal/SellerContent.aspx?sid=" + seller.Guid, seller.SellerName, seller.SellerEmail + "," + seller.ReturnedPersonEmail);
                            try
                            {
                                EmailFacade.SendErrorEmail(odr.SellerGuid, errorMsg);
                            }
                            catch { }
                            logger.Warn(errorMsg);

                        }
                        else
                        {
                            string[] mailList = mailTo.Split(",");
                            foreach (string m in mailList)
                            {
                                MailMessage msg = new MailMessage();
                                msg.IsBodyHtml = true;
                                msg.From = new MailAddress(cp.CsReturnEmail);
                                msg.To.Add(m);
                                msg.Subject = "您有新的照會完成_" + DateTime.Now.ToShortDateString();
                                msg.Body = template.ToString();

                                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                            }

                        }
                        
                    }

                    //照會完成,更新最後出貨日
                    IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(osInfo.ProductGuid);
                    var shippingDateModel = OrderFacade.GetShippingDateInfo(DateTime.Now, theDeal);
                    OrderProductDelivery opd = op.OrderProductDeliveryGetByOrderGuid(osInfo.OrderGuid);
                    if (opd.IsLoaded && opd.LastShipDate == null)
                    {
                        opd.LastShipDate = shippingDateModel.LastShipDate;
                        opd.ModifyTime = DateTime.Now;
                        op.OrderProductDeliverySet(opd);
                    }
                    else
                    {
                        logger.Info(osInfo.OrderGuid.ToString() + "沒更新最後出貨日");
                    }
                    
                }
                catch (Exception ex)
                {
                    logger.Info(ex.Message);
                }
            }


            return true;
        }

        /// <summary>
        /// 照會失敗
        /// </summary>
        /// <param name="aryId"></param>
        /// <param name="strUserId"></param>
        /// <returns></returns>
        public static bool SetCreditcardOrderReferFail(string[] aryId, string strUserId)
        {
            int id = -1;
            foreach (string sid in aryId)
            {
                int.TryParse(sid, out id);
                op.UpdateCreditcardOrderByIdWithRefer(id, strUserId, false);

                #region 照會失敗備註
                CreditcardOrder creditcardOrder = op.CreditcardOrderGet(id);
                if (creditcardOrder != null)
                {
                    OrderUserMemoList userMemo = new OrderUserMemoList();
                    userMemo.OrderGuid = creditcardOrder.OrderGuid;
                    userMemo.DealType = (int)VbsDealType.Ppon;
                    userMemo.UserMemo = "**照會失敗，請勿出貨**";
                    userMemo.CreateId = strUserId;
                    userMemo.CreateTime = DateTime.Now;
                    op.OrderUserMemoListSet(userMemo);
                }
                #endregion 照會失敗備註
            }
            return true;
        }

        public static bool SetCreditcardOrderLocked(string creditcardInfo, string strUserId, bool isLocked)
        {
            #region 將卡號設定為黑名單

            op.UpdateCreditcardOrderByInfoWithLocked(creditcardInfo, strUserId, isLocked);

            #endregion 將卡號設定為黑名單

            List<int> memUserId = new List<int>();

            CreditcardOrderCollection cdOrders = op.GetCreditcardOrderByInfo(creditcardInfo);
            foreach (CreditcardOrder cd in cdOrders)
            {
                if (memUserId.IndexOf(cd.MemberId) > -1)
                {
                    continue;
                }
                else
                {
                    memUserId.Add(cd.MemberId);
                    SetMemberBlacklistFlag(cd.MemberId, isLocked);
                }
            }
            memUserId.Clear();
            return true;
        }
        /// <summary>
        /// 1.判斷資料是否存在，存在設為黑名單，不存在加入黑名單
        /// 2.
        /// </summary>
        /// <param name="creditcardInfo"></param>
        /// <param name="creditcardType"></param>
        /// <param name="strUserId"></param>
        public static void AddCreditcardOrder(string creditcardInfo, string creditcardType, string strUserId)
        {
            CreditcardOrderCollection cdOrders = op.GetCreditcardOrderByInfo(creditcardInfo);
            if (cdOrders != null && cdOrders.Count > 0)
            {
                SetCreditcardOrderLocked(creditcardInfo, strUserId, true);
            }
            else
            {
                CreditCardUtility.AddCreditcardOrderByAntiCreditcardFraud(creditcardInfo,creditcardType,strUserId);
            }
        }

        public static bool SetMemberBlacklistFlag(int memId, bool isLocked)
        {
            bool isRtnResult = true;
            Member mem = mp.MemberGetbyUniqueId(memId);
            if (string.IsNullOrEmpty(mem.UserName))
            {
                isRtnResult = false;
            }
            else
            {
                mem.Status = Convert.ToInt32(
                    Helper.SetFlag(isLocked, mem.Status, (int)MemberStatusFlag.IsBlacklisted));
                isRtnResult = mp.MemberSet(mem);
            }
            return isRtnResult;
        }

        #endregion 防盜刷

        #region Pay Utility

        /// <summary>
        /// pay頁 / shoppingcart pay 頁使用，將前端傳來的選項資料，考慮多選項與是否成套販售，轉換成 ItemEntry 的 List 回傳
        /// </summary>
        /// <param name="itemOptions"></param>
        /// <param name="ticketId"></param>
        /// <param name="theDeal"></param>
        /// <param name="selectedStoreGuid"></param>
        /// <param name="infoStr"></param>
        /// <returns></returns>
        private static List<ItemEntry> PraseSubItemsAndAccessories(string itemOptions, string ticketId,
            IViewPponDeal theDeal, Guid selectedStoreGuid, string infoStr)
        {
            List<ItemEntry> result = new List<ItemEntry>();
            int combopack = theDeal.ComboPackCount ?? 1;
            if (theDeal.ShoppingCart.GetValueOrDefault() == false && theDeal.ComboPackCount > 1)
            {
                logger.ErrorFormat("檔次 {0} 設定錯誤，沒有勾選購物車，卻設定了成套販售。", theDeal.BusinessHourGuid);
                combopack = 1;
            }

            if (itemOptions != null && itemOptions.Length > 1)
            {
                string[] kk = itemOptions.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string kz in kk)
                {
                    string[] yy = kz.Split(new string[] { "|#|" }, StringSplitOptions.None);
                    string[] accs = yy[0].Split(',');
                    if (yy[0] == "")
                    {
                        //檔次沒有選項
                        accs = new string[] { };
                    }

                    ItemEntry item = new ItemEntry();
                    item.Id = theDeal.ItemGuid;
                    item.Name = theDeal.ItemName;
                    item.UnitPrice = theDeal.ItemPrice;
                    item.StoreGuid = selectedStoreGuid;
                    if (combopack > 1)
                    {
                        //成套販售
                        int accessoryCount = (accs.Length / combopack);
                        for (int i = 0; i < accs.Length; i = i + accessoryCount)
                        {
                            ItemEntry subItem = new ItemEntry();
                            subItem.Name = theDeal.ItemName;
                            for (int j = i; j < i + accessoryCount; j++)
                            {
                                string[] names = accs[j].Split(new string[] { "|^|" }, StringSplitOptions.RemoveEmptyEntries);
                                subItem.SelectedAccessory.Add(new AccessoryEntry(names[0], Guid.Parse(names[1])));
                            }
                            item.SubItems.Add(subItem);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < accs.Length; i++)
                        {
                            string[] names = accs[i].Split(new string[] { "|^|" },
                                                            StringSplitOptions.RemoveEmptyEntries);
                            item.SelectedAccessory.Add(new AccessoryEntry(names[0], Guid.Parse(names[1])));
                        }
                    }

                    foreach (string a in accs)
                    {
                        string[] names = a.Split(new string[] { "|^|" }, StringSplitOptions.RemoveEmptyEntries);
                        TempSession ts = new TempSession();
                        ts.SessionId = "Accessory" + ticketId;
                        ts.Name = names[1];
                        ts.ValueX = int.Equals(1, combopack) ? yy[1] : "1";
                        pp.TempSessionSet(ts);
                    }
                    //todo:A型上線的緊急處理，之後購物車可能要調整
                    item.OrderQuantity = Helper.IsFlagSet(theDeal.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon) ? 1 : (int)(double.Parse(yy[1]) / combopack);
                    item.Remark = infoStr;
                    result.Add(item);
                }
            }

            return result;
        }

        /// <summary>
        /// shoppingcart pay 與 payService.asmx 手機使用，依某種規則轉換選項到成套販售選項
        /// </summary>
        /// <param name="comboPack"></param>
        /// <param name="itemOptions"></param>
        /// <returns></returns>
        private static string ProcessComboPack(int comboPack, string itemOptions)
        {
            if (1 == comboPack)
            {
                return itemOptions;
            }
            else
            {
                string result = string.Empty;
                string temp = string.Empty;
                int countNow = 0;
                string[] kk = itemOptions.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string kz in kk)
                {
                    string[] yy = kz.Split(new string[] { "|#|" }, StringSplitOptions.None);
                    int itemCount = int.Parse(yy[1]);

                    while (itemCount > 0)
                    {
                        int neededCount = comboPack - (countNow % comboPack);
                        int addCount = itemCount > neededCount ? neededCount : itemCount;

                        for (int x = 0; x < addCount; x++)
                        {
                            temp += "," + yy[0];
                            itemCount--;
                            countNow++;
                        }

                        if (int.Equals(0, countNow % comboPack))
                        {
                            result += "||" + temp.Substring(1) + "|#|" + comboPack.ToString();
                            temp = string.Empty;
                        }
                    }
                }

                return result.Length > 0 ? result.Substring(2) : string.Empty;
            }
        }

        /// <summary>
        /// shoppingcart pay / pay service頁使用，將前端傳來的選項資料，考慮多選項與是否成套販售，轉換成 ItemEntry 的 List 回傳。
        ///
        /// 如果想Merge ProcessComboPack & PraseSubItemsAndAccessories 這2個methods，
        /// 建議先完善 PaymentFacadeFixture 測試再來調整
        ///
        /// 也可以將 Pay 類似的需求也整合在一起
        ///
        /// </summary>
        /// <param name="itemOptions"></param>
        /// <param name="ticketId"></param>
        /// <param name="theDeal"></param>
        /// <param name="selectedStoreGuid"></param>
        /// <param name="infoStr"></param>
        /// <returns></returns>
        public static List<ItemEntry> ProcessSubItemsAndAccessoriesWithComboPack(string itemOptions, string ticketId,
            IViewPponDeal theDeal, Guid selectedStoreGuid, string infoStr)
        {
            /* ProcessComboPack 的功用
            牛|^|3e714704-841a-44d4-a658-9e568a7e62ba,小|^|bfc8e474-9aad-495d-83a0-1c43d3d2e93f|#|1
            ||
            豬|^|0a92e693-0458-48ca-9bfa-163ce4562d57,中|^|f8dc6d22-e87d-42b6-b719-941b0199a5ec|#|2
            ||
            雞|^|ff118190-8bfc-4f5e-ad7c-116a6980998d,大|^|3078da57-7a3b-4c74-8949-16cb2968e814|#|1

            牛|^|3e714704-841a-44d4-a658-9e568a7e62ba,小|^|bfc8e474-9aad-495d-83a0-1c43d3d2e93f,
            豬|^|0a92e693-0458-48ca-9bfa-163ce4562d57,中|^|f8dc6d22-e87d-42b6-b719-941b0199a5ec,
            豬|^|0a92e693-0458-48ca-9bfa-163ce4562d57,中|^|f8dc6d22-e87d-42b6-b719-941b0199a5ec,
            雞|^|ff118190-8bfc-4f5e-ad7c-116a6980998d,大|^|3078da57-7a3b-4c74-8949-16cb2968e814
            |#|4
            */
            itemOptions = ProcessComboPack(theDeal.ComboPackCount ?? 1, itemOptions);

            List<ItemEntry> ies = PraseSubItemsAndAccessories(
                itemOptions, ticketId, theDeal, selectedStoreGuid, infoStr);
            return ies;
        }

        public static string GetTransactionId()
        {
            TransactionidLog tlog = null;
            bool exist = true;

            do
            {
                string rf = "";

                do
                {
                    rf = Path.GetRandomFileName().Replace(".", string.Empty);
                    rf = rf.ToLower().IndexOf("null") >= 0 ? "" : rf;
                } while (string.IsNullOrWhiteSpace(rf));

                string tic = DateTime.Now.Ticks.ToString();
                string tid = rf + tic.Substring(tic.Length + rf.Length - 20, 20 - rf.Length);

                tlog = new TransactionidLog
                {
                    TransactionId = tid,
                    CreateTime = DateTime.Now,
                    ServerName = Environment.MachineName
                };

                try
                {
                    op.TransactionidLogSet(tlog);
                    exist = false;
                }
                catch (Exception ex)
                {
                    logger.Info(ex.Message);
                }
            } while (exist);

            return tlog.TransactionId;
        }

        public static string GetTransactionIdByOrderGuid(Guid orderGuid)
        {
            var col = op.PaymentTransactionGetListByOrderGuid(orderGuid);
            if (col.Count > 0)
            {
                return col[0].TransId;
            }
            return null;
        }

        public static PaymentType? GetPaymentTypeByThirdPartyPayment(ThirdPartyPayment tpp)
        {
            switch (tpp)
            {
                case ThirdPartyPayment.LinePay:
                    return PaymentType.LinePay;
                case  ThirdPartyPayment.TaishinPay:
                    return PaymentType.TaishinPay;
            }
            return null;
        }

        #endregion Pay Utility
        

        public static int GetBlackCreditcardOrderTimes(string creditCardId)
        {
            int orderTimes = 0;
            CreditcardOrderCollection cdOrders = op.GetCreditcardOrderByInfo(creditCardId);
            if (cdOrders != null)
            {
                foreach (CreditcardOrder cd in cdOrders)
                {
                    if (cd.CreditcardInvalidTimes == 1)
                    {
                        orderTimes++;
                    }
                }
            }
            
            return orderTimes;
        }

        #region MakeOrder

        /// <summary>
        /// 17Life APP MakeOrder & SKM App MakeOrder
        /// </summary>
        /// <param name="paymentDto"></param>
        /// <param name="userName"></param>
        /// <param name="supportGroupCoupon"></param>
        public static PayResult PaymentForApp(PaymentDTO paymentDto, string userName, bool supportGroupCoupon = false)
        {
            var deliveryInfo = paymentDto.DeliveryInfo;
            int userId = MemberFacade.GetUniqueId(userName);
            if (userId == 0)
            {
                return new PayResult
                {
                    ReturnCode = ApiReturnCode.BadUserInfo,
                    ResultCode = ApiResultCode.BadUserInfo,
                    Message = string.Format("user {0} not found", userName)
                };
            }
            ApplePayData appleData = null;
            if (paymentDto.ApplePayTokenData != null)
            {
                logger.Info("apple pay user:" + userId.ToString());
                appleData = ApplePayUtility.DecryptToken(paymentDto.ApplePayTokenData);
            }
            int? mobilePayType = null;
            if (appleData != null)
            {
                paymentDto.ApplePayData = appleData;
                mobilePayType = !string.IsNullOrEmpty(appleData.PaymentData.EmvData) ? (int)MobilePayType.ApplePayEMV : (int)MobilePayType.ApplePay;
            }

            #region Check Order

            #region MasterPass

            if (paymentDto.IsMasterPass)
            {
                if (paymentDto.MasterPassTransData == null || !paymentDto.MasterPassTransData.IsValid())
                {
                    return new PayResult
                    {
                        ReturnCode = ApiReturnCode.PaymentError,
                        ResultCode = ApiResultCode.MasterpassDataError,
                        Message = "MasterPass 資料不正確"
                    };
                }

                MasterpassCardInfoLog mpclog = ChannelFacade.GetMasterpassCardInfoLog(paymentDto.MasterPassTransData.VerifierCode,
                    paymentDto.MasterPassTransData.AuthToken);

                if (!mpclog.IsLoaded)
                {
                    return new PayResult
                    {
                        ReturnCode = ApiReturnCode.PaymentError,
                        ResultCode = ApiResultCode.MasterpassDataNotFound,
                        Message = "無法取得MasterPass資料"
                    };
                }

                if (mpclog.IsUsed)
                {
                    return new PayResult
                    {
                        ReturnCode = ApiReturnCode.PaymentError,
                        ResultCode = ApiResultCode.MasterpassDataIsUsed,
                        Message = "MasterPass 交易已使用"
                    };
                }

                paymentDto.CreditcardNumber = mpclog.CardNumber;
                paymentDto.CreditcardExpireMonth = mpclog.ExpiryMonth.PadLeft(2, '0');
                paymentDto.CreditcardExpireYear = mpclog.ExpiryYear.Length == 4 ? mpclog.ExpiryYear.Substring(2, 2) : mpclog.ExpiryYear;
                paymentDto.CreditcardSecurityCode = "000";
            }

            #endregion MasterPass

            if (paymentDto.PCash > 0)
            {
                if (MemberFacade.GetPayeasyCashPoint(userId) < paymentDto.PCash)
                {
                    return new PayResult
                    {
                        ReturnCode = ApiReturnCode.PaymentError,
                        ResultCode = ApiResultCode.PaymentError,
                        Message = "Payeasy購物金不足"
                    };
                }
            }
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDto.BusinessHourGuid, true);
            if (Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb)
                && GameFacade.HasGameDealBuyPermission(userId, theDeal.BusinessHourGuid, paymentDto.OrderGuid) == false)
            {
                return new PayResult
                {
                    ReturnCode = ApiReturnCode.OutofLimitOrderError,
                    ResultCode = ApiResultCode.OutofLimitOrderError,
                    Message = I18N.Message.OutofLimitOrderError
                };
            }

            //Check 是否可使用折價券
            if (!string.IsNullOrEmpty(paymentDto.DiscountCode))
            {
                if (!PromotionFacade.GetDiscountUseType(theDeal.BusinessHourGuid))
                {
                    return new PayResult
                    {
                        ReturnCode = ApiReturnCode.PaymentError,
                        ResultCode = ApiResultCode.PaymentError,
                        Message = "優惠商品不適用折價券"
                    };
                }
            }

            //是否通過銀行限定驗證
            bool isCheckLimitBank = true;

            //是否為銀行限定檔
            bool isLimitBank = (theDeal.BankId ?? 0) > 0;
            if (isLimitBank)
            {
                #region 銀行限定 驗證信用卡

                int dealBankId = theDeal.BankId ?? (int)LimitBankIdModel.None;

                //銀行限定檔不可使用折價券、紅利金、購物金
                if (paymentDto.SCash != 0 || paymentDto.BCash != 0 || !string.IsNullOrEmpty(paymentDto.DiscountCode))
                {
                    isCheckLimitBank = false;
                }
                else if (paymentDto.CreditCardGuid != null) //記憶卡號
                {
                    Guid cardGuid;
                    if (Guid.TryParse(paymentDto.CreditCardGuid.ToString(), out cardGuid))
                    {
                        MemberCreditCard mcc = mp.MemberCreditCardGetByGuidUserId(cardGuid, userId);
                        if (!mcc.IsLoaded || mcc.BankId != dealBankId)
                        {
                            isCheckLimitBank = false;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(paymentDto.CreditcardNumber)) //直接輸卡號
                {
                    var bankInfo = CreditCardPremiumManager.GetCreditCardBankInfo(paymentDto.CreditcardNumber);
                    if (!bankInfo.IsLoaded || bankInfo.BankId != dealBankId)
                    {
                        isCheckLimitBank = false;
                    }
                }

                //未通過銀行限定驗證
                if (isCheckLimitBank == false)
                {
                    return new PayResult
                    {
                        ReturnCode = ApiReturnCode.PaymentError,
                        ResultCode = ApiResultCode.PaymentError,
                        Message = string.Format("此好康限定，{0}付款。", Helper.GetEnumDescription((LimitBankIdModel)dealBankId))
                    };
                }

                #endregion
            }

            bool isGroupCoupon = cp.IsGroupCouponOn && Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
            //針對舊版API調整成套票券數量判斷
            paymentDto.Quantity = isGroupCoupon ? 1 : paymentDto.Quantity;
            if (cp.EnableGroupCouponNewMakeOrderApi)
            {
                if (isGroupCoupon && !supportGroupCoupon)
                {
                    return new PayResult
                    {
                        ReturnCode = ApiReturnCode.PaymentError,
                        ResultCode = ApiResultCode.PaymentError,
                        Message = "此檔次限最新版本APP才可以購買，請更新您的APP。"
                    };
                }
            }

            //檢查品生活visa檔是否未用visa特定卡購買
            if (theDeal.IsVisaNow)
            {
                HiDealVisaCardType cardType = CreditCardPremiumManager.GetPiinlifeVisaCardType(paymentDto.CreditcardNumber);
                if (cardType != HiDealVisaCardType.Signature &&
                    cardType != HiDealVisaCardType.Infinite &&
                    cardType != HiDealVisaCardType.Platinum)
                {
                    return new PayResult
                    {
                        ReturnCode = ApiReturnCode.PaymentError,
                        ResultCode = ApiResultCode.PaymentError,
                        Message = "Visa優先選購專區，只提供Visa御璽卡、Visa無限卡、Visa白金卡優先購買。"
                    };
                }
            }

            // check branch 非0元檔次，檢查分店編號是否正確
            // 通用券檔次無須檢查分店
            bool noRestrictedStore = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
            bool isItem = theDeal.DeliveryType.GetValueOrDefault() == (int)DeliveryType.ToHouse;
            var isCheckBranch = CheckBranch(paymentDto.BusinessHourGuid, isItem, paymentDto.SelectedStoreGuid, paymentDto.Quantity, noRestrictedStore, theDeal.SaleMultipleBase);
            if ((theDeal.ItemPrice != 0) && !isCheckBranch)
            {
                return new PayResult
                {
                    ReturnCode = ApiReturnCode.PaymentError,
                    ResultCode = ApiResultCode.PaymentError,
                    Message = "請選擇還有數量的分店"
                };
            }

            // check option
            if (!CheckOptions(paymentDto.ItemOptions))
            {
                return new PayResult
                {
                    ReturnCode = ApiReturnCode.PaymentError,
                    ResultCode = ApiResultCode.PaymentError,
                    Message = "請選擇還有數量的選項"
                };
            }

            // check 送貨方式
            if (theDeal.DeliveryType == (int)DeliveryType.ToHouse)
            {
                if (deliveryInfo.ProductDeliveryType == ProductDeliveryType.Normal)
                {
                    if (!theDeal.EnableDelivery)
                    {
                        return new PayResult
                        {
                            ReturnCode = ApiReturnCode.PaymentError,
                            ResultCode = ApiResultCode.PaymentError,
                            Message = "此商品未提供宅配到府服務"
                        };
                    }
                }
                else
                {
                    var dealIspServiceChannel = PponFacade.GetIspServiceChannelBySellerGuid(theDeal.SellerGuid);
                    if (deliveryInfo.ProductDeliveryType == ProductDeliveryType.FamilyPickup && !dealIspServiceChannel.Contains(ServiceChannel.FamilyMart))
                    {
                        return new PayResult
                        {
                            ReturnCode = ApiReturnCode.PaymentError,
                            ResultCode = ApiResultCode.PaymentError,
                            Message = "此商品未提供全家超商取貨服務"
                        };
                    }

                    if (deliveryInfo.ProductDeliveryType == ProductDeliveryType.SevenPickup && !dealIspServiceChannel.Contains(ServiceChannel.SevenEleven))
                    {
                        return new PayResult
                        {
                            ReturnCode = ApiReturnCode.PaymentError,
                            ResultCode = ApiResultCode.PaymentError,
                            Message = "此商品未提供7-11超商取貨服務"
                        };
                    }

                    // check 超取組數 (組數= Quantity/ComboPackCount)
                    if (paymentDto.Quantity / (theDeal.ComboPackCount ?? 1) > theDeal.IspQuantityLimit)
                    {
                        return new PayResult
                        {
                            ReturnCode = ApiReturnCode.PaymentError,
                            ResultCode = ApiResultCode.PaymentError,
                            Message = "超商取貨超過組數上限"
                        };
                    }
                }
            }

            // add for empty address
            if (isItem && paymentDto.DeliveryInfo.ProductDeliveryType == ProductDeliveryType.Normal)
            {
                if (string.IsNullOrWhiteSpace(paymentDto.DeliveryInfo.WriteAddress) || paymentDto.DeliveryInfo.WriteAddress.IndexOf("請選擇") >= 0)
                {
                    return new PayResult
                    {
                        ReturnCode = ApiReturnCode.PaymentError,
                        ResultCode = ApiResultCode.PaymentError,
                        Message = "請選擇收件人地址"
                    };
                }
            }

            //使用17Life購物金付全額仍需紀錄invoice_mode (invoice_mode不為0)
            int sc_price = Convert.ToInt32(theDeal.ItemPrice) + paymentDto.SCash;

            if (sc_price == 0)
            {
                paymentDto.DonationReceiptsType = DonationReceiptsType.None;//0
            }

            //關於公益檔的檢查
            bool isKindDeal = theDeal.GroupOrderStatus != null && Helper.IsFlagSet(theDeal.GroupOrderStatus.Value, GroupOrderStatus.KindDeal);
            if (isKindDeal)
            {
                if (paymentDto.SCash > 0 ||
                    paymentDto.BCash > 0 ||
                    string.IsNullOrEmpty(paymentDto.ATMAccount) == false ||
                    string.IsNullOrEmpty(paymentDto.DiscountCode) == false)
                {
                    return new PayResult
                    {
                        ReturnCode = ApiReturnCode.InputError,
                        ResultCode = ApiResultCode.InputError,
                        Message = "公益檔只允許信用卡支付"
                    };
                }
            }

            //檔次館別
            paymentDto.DeliveryInfo.DealAccBusinessGroupId = theDeal.DealAccBusinessGroupId ?? 0;
            //分類
            paymentDto.DeliveryInfo.RootDealType = theDeal.DealType == null ? 0 : DealTypeFacade.GetRootNode((int)theDeal.DealType);

            //檢查手機載具
            //if(paymentDto.DeliveryInfo.CarrierType == CarrierType.Phone)
            //{
            //    bool isValidPhoneCarrierId = MemberFacade.IsValidPhoneCarrierId(paymentDto.DeliveryInfo.CarrierId);
            //    if (!isValidPhoneCarrierId)
            //    {
            //        return new PayResult
            //        {
            //            ReturnCode = ApiReturnCode.InputError,
            //            ResultCode = ApiResultCode.InputError,
            //            Message = "手機條碼載具格式錯誤"
            //        };
            //    }
            //}

            #region 增加載具類型檢核
            //20190911:iOS有低機率送上的類型會跟載具不符
            if (paymentDto.DeliveryInfo != null)
            {
                if (cp.CheckPhoneCarrierValid)
                {
                    if (paymentDto.DeliveryInfo.CarrierType == CarrierType.Phone && !paymentDto.DeliveryInfo.CarrierId.StartsWith("/"))
                    {
                        return new PayResult
                        {
                            ReturnCode = ApiReturnCode.InputError,
                            ResultCode = ApiResultCode.InputError,
                            Message = "載具格式與類型不符"
                        };
                    }
                    else if (paymentDto.DeliveryInfo.CarrierType == CarrierType.PersonalCertificate)
                    {
                        string pattern = @"^[A-Z][A-Z]\d{14}$";
                        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(pattern);
                        if (!regex.IsMatch(paymentDto.DeliveryInfo.CarrierId))
                        {
                            return new PayResult
                            {
                                ReturnCode = ApiReturnCode.InputError,
                                ResultCode = ApiResultCode.InputError,
                                Message = "載具格式與類型不符"
                            };
                        }
                    }
                }
            }

            #endregion

            #endregion Check Order

            #region 發票問題應急修正

            //針對APP傳入參數的錯誤，先增加的修改，相容於正確判斷後傳來的參數。
            if (paymentDto.DeliveryInfo.ReceiptsType == DonationReceiptsType.None)
            {
                paymentDto.DeliveryInfo.ReceiptsType = DonationReceiptsType.DoNotContribute;
                if (string.IsNullOrWhiteSpace(paymentDto.DeliveryInfo.CompanyTitle))
                {
                    paymentDto.DeliveryInfo.InvoiceType = "2";
                    paymentDto.DeliveryInfo.IsEinvoice = true;
                }
                else
                {
                    paymentDto.DeliveryInfo.InvoiceType = "3";
                    paymentDto.DeliveryInfo.IsEinvoice = false;
                }
            }

            #endregion 發票問題應急修正

            #region CheckOut

            paymentDto.TicketId = string.Format("{0}_{1}",
                OrderFacade.MakeRegularTicketId(paymentDto.SessionId, paymentDto.BusinessHourGuid), Path.GetRandomFileName().Replace(".", ""));
            paymentDto.APIProvider = (PaymentAPIProvider)Enum.Parse(typeof(PaymentAPIProvider), cp.CreditCardAPIProvider);

            int quantity = (int)(Math.Ceiling((double)(paymentDto.Quantity) / (theDeal.ComboPackCount ?? 1)));

            if (PponFacade.CheckExceedOrderedQuantityLimit(theDeal, quantity))
            {
                return new PayResult
                {
                    ReturnCode = ApiReturnCode.OutofLimitOrderError,
                    ResultCode = ApiResultCode.OutofLimitOrderError,
                    Message = I18N.Message.OutofLimitOrderError
                };
            }

            if (DateTime.Now > theDeal.BusinessHourOrderTimeE)
            {
                return new PayResult
                {
                    ReturnCode = ApiReturnCode.DealClosed,
                    ResultCode = ApiResultCode.DealClosed,
                    Message = I18N.Message.DealClosed
                };
            }

            int minimumAmount;
            decimal amount = theDeal.ItemPrice * quantity + paymentDto.DeliveryInfo.Freight;
            var discountCodeStatus = PromotionFacade.GetDiscountCodeStatus(paymentDto.DiscountCode, DiscountCampaignUsedFlags.Ppon, amount, userName, out minimumAmount, theDeal.BusinessHourGuid.ToString());

            var rtnObject = new PayResult();

            if (!string.IsNullOrWhiteSpace(paymentDto.DiscountCode))
            {
                if (discountCodeStatus == DiscountCodeStatus.None)
                {
                    rtnObject.Message = I18N.Phrase.DiscountCodeNone;
                }
                else if (discountCodeStatus == DiscountCodeStatus.IsUsed)
                {
                    rtnObject.Message = I18N.Phrase.DiscountCodeIsUsed;
                }
                else if (discountCodeStatus == DiscountCodeStatus.UpToQuantity)
                {
                    rtnObject.Message = I18N.Phrase.DiscountCodeUptoQuantity;
                }
                else if (discountCodeStatus == DiscountCodeStatus.Disabled)
                {
                    rtnObject.Message = I18N.Phrase.DiscountCodeDisabled;
                }
                else if (discountCodeStatus == DiscountCodeStatus.OverTime)
                {
                    rtnObject.Message = I18N.Phrase.DiscountCodeOverTime;
                }
                else if (discountCodeStatus == DiscountCodeStatus.Restricted)
                {
                    rtnObject.Message = string.Format(I18N.Phrase.DiscountCodeRestricted, "品生活");
                }
                else if (discountCodeStatus == DiscountCodeStatus.UnderMinimumAmount)
                {
                    rtnObject.Message = string.Format(I18N.Phrase.DiscountCodeUnderMinimumAmount, minimumAmount);
                }
                else if (discountCodeStatus == DiscountCodeStatus.DiscountCodeCategoryWrong)
                {
                    rtnObject.Message = I18N.Phrase.DiscountCodeCategoryWrong;
                }
                else if (discountCodeStatus == DiscountCodeStatus.OwnerUseOnlyWrong)
                {
                    rtnObject.Message = I18N.Phrase.DiscountCodeOwnerUseOnlyWrong;
                }
                else if (discountCodeStatus == DiscountCodeStatus.CategoryUseLimit)
                {
                    rtnObject.Message = string.Format("僅限" + PromotionFacade.GetDiscountCategoryString(paymentDto.DiscountCode) + "使用");
                }
                else if (discountCodeStatus != DiscountCodeStatus.CanUse)
                {
                    rtnObject.Message = I18N.Phrase.DiscountCodeCanNotUse;
                }

                if (!string.IsNullOrWhiteSpace(rtnObject.Message))
                {
                    rtnObject.ReturnCode = ApiReturnCode.DiscountCodeIsUsed;
                    rtnObject.ResultCode = ApiResultCode.DiscountCodeIsUsed;
                    return rtnObject;
                }
            }

            string strResult = CheckData(theDeal, quantity, paymentDto.BCash, paymentDto.SCash, userName, userId,
                theDeal.IsDailyRestriction.GetValueOrDefault(), isGroupCoupon);
            if (string.IsNullOrWhiteSpace(strResult))
            {
                try
                {
                    var jsonSerializer = new JsonSerializer();
                    string infoStr = jsonSerializer.Serialize(paymentDto.DeliveryInfo);
                    List<ItemEntry> ies = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(
                        paymentDto.ItemOptions, paymentDto.TicketId, theDeal, paymentDto.SelectedStoreGuid, infoStr);
                    OrderFacade.PutItemToCart(paymentDto.TicketId, ies);
                }
                catch (Exception ex)
                {
                    rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                    rtnObject.ResultCode = ApiResultCode.PaymentError;
                    rtnObject.Message = "交易失敗！（error:120）";
                    logger.Error(String.Format("Something wrong while put items to cart : ticketId={0}, bid={1}", paymentDto.TicketId,
                        paymentDto.BusinessHourGuid), ex);
                    return rtnObject;
                }
            }
            else
            {
                rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                rtnObject.ResultCode = ApiResultCode.PaymentError;
                rtnObject.Message = strResult;
                return rtnObject;
            }

            #endregion CheckOut

            #region Make Order

            paymentDto.TransactionId = PaymentFacade.GetTransactionId();

            #region order amount

            //discountCode實際扣抵金額
            int discountAmount = 0;
            DiscountCode discountCode = null;

            if (string.IsNullOrEmpty(deliveryInfo.DiscountCode) == false)
            {
                if (PromotionFacade.GetDiscountCodeAmount(deliveryInfo.DiscountCode, out discountAmount, out discountCode) == false)
                {
                    rtnObject.ReturnCode = ApiReturnCode.DiscountCodeError;
                    rtnObject.ResultCode = ApiResultCode.DiscountCodeUsingError;
                    rtnObject.Message = "折價券已達使用數量上限";
                    return rtnObject;
                }
                if (deliveryInfo.PayType == PaymentType.Creditcard && paymentDto.Amount > 0 &&
                    paymentDto.CreditcardNumber != null && paymentDto.CreditcardNumber.Length > 6)
                {
                    var viewDiscountDetail = op.ViewDiscountDetailByCode(paymentDto.DeliveryInfo.DiscountCode);
                    if (Helper.IsFlagSet(viewDiscountDetail.Flag, DiscountCampaignUsedFlags.VISA))
                    {
                        if (CreditCardPremiumManager.IsVisaCardNumber(paymentDto.CreditcardNumber.Substring(0, 6)) == false)
                        {
                            rtnObject.ReturnCode = ApiReturnCode.DiscountCodeError;
                            rtnObject.ResultCode = ApiResultCode.DiscountCodeUsingError;
                            rtnObject.Message = "折價券限定VISA卡使用";
                            logger.Error("Temp Session Missing. discount code must using VISA card by masterpass.");
                            return rtnObject;
                        }
                    }
                }
            }

            //訂單總金額
            int orderTotalAmount = 0;
            int orderPendingAmount = 0;

            ShoppingCartCollection scc = op.ShoppingCartGetList(ShoppingCart.Columns.TicketId, paymentDto.TicketId);
            foreach (ShoppingCart sc in scc)
            {
                int x = (int)Math.Round(sc.ItemUnitPrice * ((theDeal.SaleMultipleBase ?? 0) > 0 ? 1 : sc.ItemQuantity));
                orderPendingAmount += x;
                orderTotalAmount += x;
            }
            int deliveryCharge = PponBuyFacade.GetDealDeliveryCharge(orderTotalAmount, theDeal);
            orderTotalAmount = orderTotalAmount + deliveryCharge;
            orderPendingAmount = orderPendingAmount + deliveryCharge;

            orderPendingAmount -= discountAmount;
            orderPendingAmount -= paymentDto.DeliveryInfo.BonusPoints;
            orderPendingAmount -= paymentDto.DeliveryInfo.PayEasyCash;
            orderPendingAmount -= (int)paymentDto.DeliveryInfo.SCash;

            if (orderPendingAmount < 0)
            {
                rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                rtnObject.ResultCode = ApiResultCode.PaymentError;
                rtnObject.Message = string.Format("付款金額為負數，交易失敗。");
                logger.WarnFormat("付款金額為負數，交易失敗。{0} < 0, ticketId:{1}", orderPendingAmount, paymentDto.TicketId);
                return rtnObject;
            }

            if (paymentDto.Amount > 0 && paymentDto.Amount != orderPendingAmount)
            {
                rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                rtnObject.ResultCode = ApiResultCode.PaymentError;
                rtnObject.Message = string.Format("支付金額與系統核算金額不符。");
                logger.WarnFormat("支付金額與系統核算金額不符。{0} <> {1}, ticketId:{2}", paymentDto.Amount, orderPendingAmount, paymentDto.TicketId);
                return rtnObject;
            }

            paymentDto.DeliveryCharge = deliveryCharge;

            #endregion order amount

            #region New Version Order Start

            #region DeliveryInfo

            DeliveryInfo di = new DeliveryInfo();
            di.DeliveryCharge = paymentDto.DeliveryCharge;
            if (theDeal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, theDeal.DeliveryType.Value))
            {
                di.ProductDeliveryType = deliveryInfo.ProductDeliveryType;
                di.DeliveryId = deliveryInfo.MemberDeliveryID;

                di.CustomerName = deliveryInfo.WriteName;
                di.CustomerMobile = deliveryInfo.Phone;


                #region 一般宅配地址
                if (deliveryInfo.ProductDeliveryType == ProductDeliveryType.Normal)
                {
                    string deliveryAddress = paymentDto.DeliveryInfo.WriteAddress;
                    if (string.IsNullOrWhiteSpace(paymentDto.DeliveryInfo.WriteZipCode))
                    {
                        //停用BUILDING，嘗試由 paymentDTO的addressInfo取得資料
                        LkAddress address = paymentDto.GetReceiverAddress();
                        if (address != null)
                        {
                            City township = CityManager.TownShipGetById(address.TownshipId);
                            if (township != null)
                            {
                                deliveryAddress = township.ZipCode + " " + address.CompleteAddress;
                            }
                        }
                    }
                    else
                    {
                        deliveryAddress = paymentDto.DeliveryInfo.WriteZipCode + " " + deliveryAddress;
                    }
                    di.DeliveryAddress = deliveryAddress;

                    //宅配商品檢查地址資料是否包含路段
                    if (!LocationUtility.CheckAddressBuilding(deliveryAddress))
                    {
                        //不包含路段，提示使用者補資料
                        rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                        rtnObject.ResultCode = ApiResultCode.PaymentError;
                        rtnObject.Message = "收件人地址內容錯誤，請再檢查看看是否包含足夠的資訊供送貨人員進行配送。";
                        return rtnObject;
                    }
                }
                #endregion
                #region 超取店舖資訊
                else
                {
                    //這裡也可確認門市是否有效
                    if (deliveryInfo.PickupStore == null || string.IsNullOrEmpty(deliveryInfo.PickupStore.StoreId))
                    {
                        rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                        rtnObject.ResultCode = ApiResultCode.PaymentError;
                        rtnObject.Message = "超商取貨店舖資訊有誤，請重新選取門市。";
                        return rtnObject;
                    }

                    di.PickupStore = new PickupStore
                    {
                        StoreId = deliveryInfo.PickupStore.StoreId,
                        StoreName = deliveryInfo.PickupStore.StoreName,
                        StoreTel = deliveryInfo.PickupStore.StoreTel,
                        StoreAddr = deliveryInfo.PickupStore.StoreAddr
                    };
                }
                #endregion
            }
            else
            {
                di.DeliveryAddress = PponBuyFacade.GetOrderDesc(paymentDto.DeliveryInfo);
            }

            #endregion

            string userMemo = paymentDto.DeliveryInfo.DeliveryDateString + paymentDto.DeliveryInfo.DeliveryTimeString + paymentDto.DeliveryInfo.Notes;

            //wms (pchome)
            if (theDeal.IsWms)
            {
                di.ProductDeliveryType = ProductDeliveryType.Wms;
            }
            // make order            
            paymentDto.OrderGuid = OrderFacade.MakeOrder(userName, paymentDto.TicketId, DateTime.Now, userMemo, di,
                theDeal, Helper.GetOrderFromType(), paymentDto.PrebuiltOrderGuid);

            //save wms (pchome)
            if (theDeal.IsWms)
            {
                PponBuyFacade.SaveWmsOrder(paymentDto.OrderGuid);
            }

            Order o = op.OrderGet(paymentDto.OrderGuid);

            o.OrderMemo = ((int)paymentDto.DeliveryInfo.ReceiptsType.GetValueOrDefault()).ToString() + "|" + paymentDto.DeliveryInfo.InvoiceType + "|" + paymentDto.DeliveryInfo.UnifiedSerialNumber + "|" + paymentDto.DeliveryInfo.CompanyTitle;
            o.MobilePayType = mobilePayType;
            op.OrderSet(o);

            #endregion New Version Order Start


            #region Paying by 17Life bonus cash

            if (paymentDto.DeliveryInfo.BonusPoints > 0)
            {
                decimal bonusCash = paymentDto.DeliveryInfo.BonusPoints;
                string action = "折抵:" + (theDeal.EventName.Length <= 30 ? theDeal.EventName : (theDeal.EventName.Substring(0, 30) + "..."));
                using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                {
                    //pt 2.3 bcash
                    MemberFacade.DeductibleMemberPromotionDeposit(o.UserId, Convert.ToDouble(bonusCash * 10), action,
                        BonusTransactionType.OrderAmtRedeeming, o.Guid, MemberFacade.GetUserName(o.UserId), false, false);
                    //pt 1.3 init bcash
                    OrderFacade.MakeTransaction(paymentDto.TransactionId, PaymentType.BonusPoint, paymentDto.DeliveryInfo.BonusPoints,
                        (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDto.PezId);
                    tx.Complete();
                }

                //means paid all by bonus cash
                if (paymentDto.Amount + paymentDto.DeliveryInfo.PayEasyCash +
                    paymentDto.DeliveryInfo.SCash + discountAmount == 0)
                {
                    return MakePayment(paymentDto, userName);
                }
            }

            #endregion Paying by 17Life bonus cash

            #region Check PCash

            if (paymentDto.DeliveryInfo.PayEasyCash > 0)
            {
                //check if one pez member do have enough PCash or redirect to Pay.aspx
                if (MemberFacade.GetPayeasyCashPoint(userId) >= paymentDto.DeliveryInfo.PayEasyCash)
                {
                    //僅建立transation 不預先扣款
                    int status = Helper.SetPaymentTransactionPhase(
                        Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department), PayTransPhase.Created);
                    //pt 1.2 init pcash
                    PaymentFacade.NewTransaction(paymentDto.TransactionId, o.Guid, PaymentType.PCash,
                        paymentDto.DeliveryInfo.PayEasyCash, string.Empty, PayTransType.Authorization, DateTime.Now,
                        userName, string.Empty, status, PayTransResponseType.OK);

                    //以PCash全額支付
                    if (paymentDto.Amount + paymentDto.DeliveryInfo.BonusPoints + discountAmount == 0)
                    {
                        return MakePayment(paymentDto, userName);
                    }
                }
                else
                {
                    rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                    rtnObject.ResultCode = ApiResultCode.PaymentError;
                    rtnObject.Message = "Payeasy購物金不足";
                    return rtnObject;
                }
            }

            #endregion Check PCash

            #region 17購物金折抵

            if (paymentDto.DeliveryInfo.SCash > 0)
            {
                decimal scash;
                decimal pscash;
                OrderFacade.PortionGeneralScash(paymentDto.DeliveryInfo.SCash, userId, out scash, out pscash);
                if (scash > 0)
                {
                    //pt 1.5 init scash
                    OrderFacade.MakeTransaction(paymentDto.TransactionId, PaymentType.SCash, (int)scash,
                        (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDto.PezId);
                }
                if (pscash > 0)
                {
                    OrderFacade.MakeTransaction(paymentDto.TransactionId, PaymentType.Pscash, (int)pscash,
                        (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDto.PezId);
                }
            }

            #endregion 17購物金折抵

            #region DiscountCode

            if (!string.IsNullOrWhiteSpace(paymentDto.DeliveryInfo.DiscountCode))
            {
                if (discountCode != null && discountCode.IsLoaded && discountCode.Amount.GetValueOrDefault() > 0)
                {
                    #region  檢查是否使用visa 限定折價券 且必須要有刷卡金額
                    if (paymentDto.Amount == 0 || paymentDto.IsPaidByATM)
                    {
                        ViewDiscountDetail view_discountCode_detail = op.ViewDiscountDetailByCode(paymentDto.DeliveryInfo.DiscountCode);
                        if (Helper.IsFlagSet(view_discountCode_detail.Flag, DiscountCampaignUsedFlags.VISA))
                        {
                            rtnObject.ReturnCode = ApiReturnCode.DiscountCodeError;
                            rtnObject.ResultCode = ApiResultCode.DiscountCodeUsingError;
                            rtnObject.Message = "很抱歉！該折價券僅限VISA信用卡使用，請您再試一次。";
                            return rtnObject;
                        }
                    }
                    #endregion
                    //pt 1.7 init discount_code
                    OrderFacade.MakeTransaction(paymentDto.TransactionId, PaymentType.DiscountCode, discountAmount, (DepartmentTypes)theDeal.Department,
                        userName, o.Guid, paymentDto.PezId);

                    try
                    {
                        //pt 2.7 do discount_code

                        var updateCount = op.DiscountCodeUpdateStatusSet((int)discountCode.CampaignId, discountCode.Code, discountAmount, userId,
                            o.Guid, orderTotalAmount, paymentDto.Amount);
                        #region 1111消費贈等值折價券

                        if (!string.IsNullOrWhiteSpace(paymentDto.DeliveryInfo.DiscountCode) && cp.AutoSendDiscountCodeEnabled)
                        {
                            PromotionFacade.AutoSendDiscountCode(userId, paymentDto.OrderGuid, userName);
                        }

                        #endregion

                        if (updateCount == 0)
                        {
                            rtnObject.ReturnCode = ApiReturnCode.DiscountCodeError;
                            rtnObject.ResultCode = ApiResultCode.DiscountCodeUsingError;
                            rtnObject.Message = "折價券已達使用數量上限";
                            return rtnObject;
                        }
                        else if (updateCount > 1)
                        {
                            //找無折價券或是已使用完畢
                            logger.Error(string.Format("折價券更新使用狀態ERROR：=>PayService.asmx.cs，更新{0}筆異常，" +
                                                       "UseTime:{1},UseAmount:{2},UseId:{3},OrderGuid:{4},OrderAmount{5},OrderCost:{6}",
                                updateCount, discountCode.Code, discountAmount, userId, o.Guid, orderTotalAmount, paymentDto.Amount));
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(string.Format("折價券更新使用狀態ERROR：PayService.asmx.cs=>UseTime:{0},UseAmount:{1},UseId:{2}," +
                                                   "OrderGuid:{3},OrderAmount{4},OrderCost:{5}",
                            discountCode.Code, discountAmount, userId, o.Guid, orderTotalAmount, paymentDto.Amount), ex);
                    }
                }
            }

            #region 消費滿一定金額，送折價券活動

            List<DiscountCampaign> campaignList = PromotionFacade.GetPromotionDiscountCampaignList(DiscountCampaignUsedFlags.FreeGiftWithPurchase);
            if (campaignList.Count > 0)
            {
                foreach (var campaign in campaignList)
                {
                    if (o.Total > campaign.FreeGiftDiscount.GetValueOrDefault() &&
                        DateTime.Now >= campaign.EventDateS.GetValueOrDefault() &&
                        DateTime.Now <= campaign.EventDateE.GetValueOrDefault())
                    {
                        string codeCode;
                        int codeAmount;
                        DateTime? codeEndTime;
                        int discountCodeId;
                        int codeMinimumAmount;
                        PromotionFacade.InstantGenerateDiscountCode(campaign.Id, o.UserId, true,
                            out codeCode, out codeAmount, out codeEndTime, out discountCodeId, out codeMinimumAmount);
                    }
                }
            }

            #endregion

            #endregion DiscountCode

            // 更新 Order 營業額, 即扣掉折價券的金額。
            if (discountCode != null && discountCode.IsLoaded)
            {
                o.Turnover = (int)(o.Total - discountCode.Amount.GetValueOrDefault());
                op.OrderSet(o);
            }

            //有金額或好康設定為0元好康
            if ((paymentDto.Amount > 0) || (theDeal.ItemPrice == 0))
            {
                //0元好康
                if ((cp.BypassPaymentProcess) || (theDeal.ItemPrice == 0))
                {
                    #region For test purpose

                    OrderFacade.MakeTransaction(paymentDto.TransactionId, PaymentType.ByPass, paymentDto.Amount, (DepartmentTypes)theDeal.Department,
                        userName, o.Guid, paymentDto.PezId);

                    #endregion For test purpose
                }
                else
                {
                    #region ATM 付款 Make PaymentTransaction
                    if (paymentDto.IsPaidByATM)
                    {
                        //pt 1.4 init atm
                        PaymentTransaction ptAtm = OrderFacade.MakeTransaction(paymentDto.TransactionId, PaymentType.ATM,
                            paymentDto.Amount, (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDto.PezId);
                        if (!string.IsNullOrWhiteSpace(paymentDto.ATMAccount))
                        {
                            CtAtm ca = op.CtAtmGetByVirtualAccount(paymentDto.ATMAccount);
                            ca.Status = (int)AtmStatus.Expired;
                            op.CtAtmSet(ca);
                        }

                        paymentDto.ATMAccount = PaymentFacade.GetAtmAccount(ptAtm.TransId, userId, paymentDto.Amount, theDeal.BusinessHourGuid,
                            o.OrderId, o.Guid);


                        if (Equals(14, paymentDto.ATMAccount.Length))
                        {
                            if (!string.IsNullOrWhiteSpace(ptAtm.TransId))
                            {
                                PaymentTransaction pt = op.PaymentTransactionGet(ptAtm.TransId, PaymentType.ATM, PayTransType.Authorization);
                                int status = Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Requested);

                                PaymentFacade.UpdateTransaction(ptAtm.TransId, pt.OrderGuid, PaymentType.ATM,
                                    Convert.ToDecimal(paymentDto.Amount), string.Empty, PayTransType.Authorization,
                                    DateTime.Now, paymentDto.ATMAccount, status, (int)PayTransResponseType.OK);
                            }
                        }
                    }
                    #endregion
                    #region 超取付款 Make PaymentTransaction
                    else if (paymentDto.MainPaymentType == ApiMainPaymentType.FamilyIsp)
                    {
                        if (deliveryInfo.ProductDeliveryType == ProductDeliveryType.FamilyPickup)
                        {
                            OrderFacade.MakeTransaction(paymentDto.TransactionId, PaymentType.FamilyIsp,
                                paymentDto.Amount, (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDto.PezId);
                        }
                        else
                        {
                            rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                            rtnObject.ResultCode = ApiResultCode.PaymentError;
                            rtnObject.Message = "全家付款，僅支援全家取貨!";
                            return rtnObject;
                        }
                    }
                    else if (paymentDto.MainPaymentType == ApiMainPaymentType.SevenIsp)
                    {
                        if (deliveryInfo.ProductDeliveryType == ProductDeliveryType.SevenPickup)
                        {
                            OrderFacade.MakeTransaction(paymentDto.TransactionId, PaymentType.SevenIsp,
                                paymentDto.Amount, (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDto.PezId);
                        }
                        else
                        {
                            rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                            rtnObject.ResultCode = ApiResultCode.PaymentError;
                            rtnObject.Message = "7-11付款，僅支援全家取貨!";
                            return rtnObject;
                        }
                    }
                    #endregion
                    #region LinePay
                    else if (paymentDto.MainPaymentType == ApiMainPaymentType.LinePay && cp.EnableLinePay)
                    {
                        if (cp.EnableLinePay)
                        {
                            string transactionId = paymentDto.TransactionId;
                            PaymentTransaction ptPay = OrderFacade.MakeTransaction(transactionId, PaymentType.LinePay, paymentDto.Amount, (DepartmentTypes)theDeal.Department, userName, o.Guid);
                            var lineTrans = LinePayUtility.MakeLinePayTranscation(paymentDto.TicketId, transactionId);
                            var reserve = new ReserveInputModel
                            {
                                ProductName = theDeal.CouponUsage,
                                ProductImageUrl = cp.LinePayUsingLogo,
                                Amount = paymentDto.Amount,
                                Currency = "TWD",
                                OrderId = o.OrderId,
                                ConfirmUrl = string.Format("open17pay://{0}/LinePayConfirm?TransId={1}&TicketId={2}", new Uri(cp.SiteUrl).Host, transactionId, paymentDto.TicketId),
                                CancelUrl = string.Format("open17pay://{0}/LinePayCancel?TransId={1}&LineCancel=true&TicketId={2}", new Uri(cp.SiteUrl).Host, transactionId, paymentDto.TicketId),
                                LangCd = cp.LinePayLangCd,
                                Capture = !lineTrans.IsCaptureSeparate
                            };
                            var reserveResult = LinePayUtility.CreateReserve(lineTrans, reserve, true);
                            if (reserveResult.Key == false)
                            {
                                rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                                rtnObject.ResultCode = ApiResultCode.PaymentError;
                                rtnObject.Message = "LinePay發生問題，請重新操作!";
                            }
                            else
                            {
                                ptPay.ApiProvider = (int)PaymentAPIProvider.LinePay;
                                ptPay.Status = Helper.SetPaymentTransactionPhase(ptPay.Status, PayTransPhase.Requested);
                                op.PaymentTransactionSet(ptPay);
                                rtnObject.ReturnCode = ApiReturnCode.Success;
                                rtnObject.ResultCode = ApiResultCode.Success;
                                rtnObject.Data = new { HppUrl = reserveResult.Value } ;
                                //儲存dto session
                                SetPaymentDTOToSession(paymentDto.TicketId, paymentDto);
                            }
                        }
                        else
                        {
                            rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                            rtnObject.ResultCode = ApiResultCode.PaymentError;
                            rtnObject.Message = "LinePay發生問題，請重新操作!";
                        }
                        return rtnObject;
                    }
                    #endregion
                    #region 其他支付方式 Make PaymentTransaction
                    else
                    {
                        //pt 1.1 init creditcard
                        OrderFacade.MakeTransaction(paymentDto.TransactionId, PaymentType.Creditcard,
                            paymentDto.Amount, (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDto.PezId);
                    }
                    #endregion
                }

                return MakePayment(paymentDto, userName);
            }
            else
            {
                //means paid by the combination of PCash and bonus cash
                return MakePayment(paymentDto, userName);
            }

            #endregion Make Order
        }

        public static PayResult MakePayment(PaymentDTO paymentDTO, string userName)
        {
            var rtnObject = new PayResult
            {
                ReturnCode = ApiReturnCode.Success,
                ResultCode = ApiResultCode.Success,
                Message = string.Empty
            };

            Member mem = mp.MemberGet(userName);
            int userId = mem.UniqueId;


            //取得記憶信用卡
            if (!(paymentDTO.CreditCardGuid == null || paymentDTO.CreditCardGuid == Guid.Empty))
            {
                var card = mp.MemberCreditCardGetByGuidUserId((Guid)paymentDTO.CreditCardGuid, userId);
                if (!card.IsLoaded)
                {
                    rtnObject.ReturnCode = ApiReturnCode.CreditcardNotFound;
                    rtnObject.ResultCode = ApiResultCode.CreditcardNotFound;
                    rtnObject.Message = I18N.Message.CreditcardInfoError;
                    return rtnObject;
                }

                paymentDTO.CreditcardNumber = MemberFacade.DecryptCreditCardInfo(card.CreditCardInfo, card.EncryptSalt);
                paymentDTO.CreditcardExpireYear = card.CreditCardYear;
                paymentDTO.CreditcardExpireMonth = card.CreditCardMonth;

                card.LastUseTime = DateTime.Now; //記錄最後使用時間
                mp.MemberCreditCardSet(card);
            }


            //有信用卡才會判斷是否走OTP
            if (!string.IsNullOrEmpty(paymentDTO.CreditcardNumber) && !paymentDTO.IsFinishOTP)
            {
                paymentDTO.IsOTP = PaymentFacade.IsOTP(userId, paymentDTO.CreditcardNumber, paymentDTO.DeliveryInfo.DealAccBusinessGroupId, paymentDTO.DeliveryInfo.RootDealType, paymentDTO.Amount,paymentDTO.BusinessHourGuid, paymentDTO.OrderGuid);
            }
            

            if (paymentDTO.IsOTP && !paymentDTO.IsFinishOTP)
            {
                var otp = new OTPAuthParam()
                {
                    Enabled = true,
                    PostBackUrl = cp.SSLSiteUrl + "/ppon/OTPLoading?oGuid=" + paymentDTO.OrderGuid.ToString() + "&ticketid=" + paymentDTO.TicketId + "&apiUserId=" + HttpContext.Current.Items[LkSiteContextItem._API_USER_ID].ToString(),
                    ReturnUrl = cp.SSLSiteUrl + "/api/PayService/taishinOTPResultUrl",
                };

                var authObject = new CreditCardAuthObject()
                {
                    TransactionId = paymentDTO.TransactionId,
                    Amount = paymentDTO.Amount,
                    CardNumber = paymentDTO.CreditcardNumber,
                    ExpireYear = paymentDTO.CreditcardExpireYear,
                    ExpireMonth = paymentDTO.CreditcardExpireMonth,
                    SecurityCode = paymentDTO.CreditcardSecurityCode,
                    Description = paymentDTO.TicketId.Split('_')[1],
                    OTP = otp
                };

                CreditCardAuthResult result = CreditCardUtility.Authenticate(authObject, paymentDTO.OrderGuid,  userName, OrderClassification.LkSite, CreditCardOrderSource.Ppon);
                if (result.TransPhase == PayTransPhase.Successful)
                {
                    rtnObject.ReturnCode = ApiReturnCode.Success;
                    rtnObject.ResultCode = ApiResultCode.Success;
                    if (result.APIProvider == PaymentAPIProvider.CathayPaymnetInstallmentWithOtp || result.APIProvider == PaymentAPIProvider.CathayPaymnetOTPGateway)
                    {
                        result.HppUrl = result.HppUrl + string.Format("?TransId={0}&TicketId={1}&appUserId={2}", paymentDTO.TransactionId, paymentDTO.TicketId, HttpContext.Current.Items[LkSiteContextItem._API_USER_ID]);
                    }
                    logger.Info("[APIProvider]" + result.APIProvider.ToString());
                    logger.Info("[HppUrl]" + result.HppUrl);
                    rtnObject.Data = new { IsOTP = result.IsOTP, HppUrl = result.HppUrl };

                    //儲存dto session
                    PaymentFacade.SetPaymentDTOToSession(paymentDTO.TicketId, paymentDTO);
                }
                else
                {
                    paymentDTO.IsOTP = false;
                    rtnObject.ReturnCode = ApiReturnCode.CreditcardInfoError;
                    rtnObject.ResultCode = ApiResultCode.PaymentCreditcardInfoError;
                    rtnObject.Message = I18N.Message.CreditcardInfoError;

                    int code = 0;
                    int.TryParse(result.ReturnCode.ToString(), out code);
                    DeleteWhiteList(userId, (int)PaymentType.Creditcard, code);
                }
                

                return rtnObject;
            }


            InvoiceVersion version = InvoiceVersion.CarrierEra;
            #region 針對新版發票處理預設的 空手道推展協會 與 創世基金會 的預設愛心碼

            bool isApplePay = paymentDTO.ApplePayTokenData != null;
            if (version == InvoiceVersion.CarrierEra)
            {
                switch (paymentDTO.DeliveryInfo.ReceiptsType)
                {
                    case DonationReceiptsType.Karate:
                        paymentDTO.DeliveryInfo.LoveCode = "23981";
                        if (paymentDTO.DeliveryInfo.InvoiceType == "3")
                        {
                            paymentDTO.DeliveryInfo.InvoiceType = "2";
                            SystemFacade.SetApiLog("PayService.MakePayment", string.Empty, new { message = "發票資料錯誤InvoiceType 傳入3 應為2，已修正。" }
                                , null);
                        }
                        break;
                    case DonationReceiptsType.GSWF:
                        paymentDTO.DeliveryInfo.LoveCode = "919";
                        if (paymentDTO.DeliveryInfo.InvoiceType == "3")
                        {
                            paymentDTO.DeliveryInfo.InvoiceType = "2";
                            SystemFacade.SetApiLog("PayService.MakePayment", string.Empty, new { message = "發票資料錯誤InvoiceType 傳入3 應為2，已修正。" }
                                , null);
                        }
                        break;
                    default:
                        //其他類型採用傳入值
                        break;
                }
                //避免使用者傳入含有空白的不正確愛心碼，將空白取代為空字串
                paymentDTO.DeliveryInfo.LoveCode =
                    paymentDTO.DeliveryInfo.LoveCode == null ? "" :
                        paymentDTO.DeliveryInfo.LoveCode.Replace(I18N.Phrase.Space, string.Empty);

                //更正APP傳入錯誤的 社團法人台灣一起夢想公益協會 愛心碼
                if (paymentDTO.DeliveryInfo.LoveCode == "6396")
                {
                    paymentDTO.DeliveryInfo.LoveCode = "510";
                }
            }

            #endregion



            if (string.IsNullOrWhiteSpace(userName) || mem.IsApproved == false)
            {
                rtnObject.ReturnCode = ApiReturnCode.UserNoSignIn;
                rtnObject.ResultCode = ApiResultCode.UserNoSignIn;
                rtnObject.Message = string.Format("user {0} not found", userName);
                return rtnObject;
            }
            else
            {
                // 避免使用者在交易過程中改變自己的 email，導致之後在用 email 反查 unique id 時查不到資料填 0
                if (int.Equals(0, userId))
                {
                    rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                    rtnObject.ResultCode = ApiResultCode.PaymentError;
                    rtnObject.Message = "交易失敗！（error:122）";
                    logger.Error("Can't find unique id. email = " + userName);
                    return rtnObject;
                }
                if (!string.IsNullOrWhiteSpace(paymentDTO.TransactionId))
                {
                    IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);
                    /*
                     * 理應不會出現 MainPaymentType = ApiMainPaymentType.None 且 paymentDTO.Amount > 0 的情境，舊版app來呼叫新的購買才會有此情況，
                     * 為相容舊dtoString，讓舊版dtoString能順利購買，而加了第一條的條件
                     */
                    if ((paymentDTO.MainPaymentType == ApiMainPaymentType.None && paymentDTO.Amount > 0 && !paymentDTO.IsPaidByATM) ||
                        paymentDTO.MainPaymentType == ApiMainPaymentType.CreditCard ||
                        paymentDTO.MainPaymentType == ApiMainPaymentType.Masterpass ||
                        paymentDTO.MainPaymentType == ApiMainPaymentType.ApplePay)
                    {
                        #region pay by credit card

                        Regex rex = new Regex(@"\D");
                        if ((!int.Equals(16, paymentDTO.CreditcardNumber.Length) ||
                             !int.Equals(2, paymentDTO.CreditcardExpireYear.Length) ||
                             !int.Equals(2, paymentDTO.CreditcardExpireMonth.Length) ||
                             rex.IsMatch(paymentDTO.CreditcardNumber) ||
                             rex.IsMatch(paymentDTO.CreditcardExpireYear) ||
                             rex.IsMatch(paymentDTO.CreditcardExpireMonth)) && !isApplePay)
                        {
                            rtnObject.ReturnCode = ApiReturnCode.CreditcardInfoError;
                            rtnObject.ResultCode = ApiResultCode.PaymentCreditcardInfoError;
                            rtnObject.Message = I18N.Message.CreditcardInfoError;
                            return rtnObject;
                        }
                        else
                        {
                            CreditCardOrderSource orderSource = CreditCardOrderSource.Ppon;
                            if (paymentDTO.IsMasterPass)
                            {
                                orderSource = CreditCardOrderSource.PiinLife; // 2014-10-23 決議 MasterPass 的交易都走 piinlife 的特店
                            }


                            CreditCardAuthResult result;
                            if (isApplePay)
                            {
                                //pt 2.1 crediitcard by applepay
                                result = CreditCardUtility.Authenticate(
                                    new ApplePayAuthObject()
                                    {
                                        TransactionId = paymentDTO.TransactionId,
                                        Amount = paymentDTO.ApplePayData.PaymentDataType == "EMV" ? paymentDTO.Amount : int.Parse(paymentDTO.ApplePayData.TransactionAmount.ToString()),
                                        CardNumber = paymentDTO.ApplePayData.ApplicationPrimaryAccountNumber,
                                        ExpireDate = paymentDTO.ApplePayData.ApplicationExpirationDate,
                                        Description = paymentDTO.TicketId.Split('_')[1],
                                        DeviceManufacturerIdentifier = paymentDTO.ApplePayData.DeviceManufacturerIdentifier,
                                        PaymentDataType = paymentDTO.ApplePayData.PaymentDataType,
                                        OnlinePaymentCryptogram = paymentDTO.ApplePayData.PaymentData.OnlinePaymentCryptogram,
                                        EciIndicator = paymentDTO.ApplePayData.PaymentData.EciIndicator,
                                        EmvData = paymentDTO.ApplePayData.PaymentData.EmvData,
                                        EncryptedPINData = paymentDTO.ApplePayData.PaymentData.EncryptedPINData,
                                    }, paymentDTO.OrderGuid, userName, GetOrderClassification(theDeal), orderSource, true);
                            }
                            else if (paymentDTO.IsOTP && paymentDTO.IsFinishOTP)
                            {
                                //3D OTP
                                var authObject = new CreditCardAuthObject()
                                {
                                    TransactionId = paymentDTO.TransactionId,
                                    Amount = paymentDTO.Amount,
                                    CardNumber = paymentDTO.CreditcardNumber,
                                    ExpireYear = paymentDTO.CreditcardExpireYear,
                                    ExpireMonth = paymentDTO.CreditcardExpireMonth,
                                    SecurityCode = paymentDTO.CreditcardSecurityCode,
                                    Description = paymentDTO.TicketId.Split('_')[1]
                                };


                                result = CreditCardUtility.QueryOTP(authObject
                                    , paymentDTO.OrderGuid, userName, GetOrderClassification(theDeal), orderSource);
                            }
                            else
                            {
                                // pt 2.1 do creditcard
                                var authObject = new CreditCardAuthObject()
                                {
                                    TransactionId = paymentDTO.TransactionId,
                                    Amount = paymentDTO.Amount,
                                    CardNumber = paymentDTO.CreditcardNumber,
                                    ExpireYear = paymentDTO.CreditcardExpireYear,
                                    ExpireMonth = paymentDTO.CreditcardExpireMonth,
                                    SecurityCode = paymentDTO.CreditcardSecurityCode,
                                    Description = paymentDTO.TicketId.Split('_')[1]
                                };

                                result = CreditCardUtility.Authenticate(authObject
                                    , paymentDTO.OrderGuid, userName, GetOrderClassification(theDeal), orderSource);


                                int riskScore;
                                CreditCardUtility.AutoBlockCreditcardFraudSuspect(
                                    authObject, result, paymentDTO.BusinessHourGuid, out riskScore);
                                if (riskScore >= -2 && cp.AutoBlockCreditcardFraudSuspect)
                                {
                                    CreditCardUtility.PushToLineBot(authObject, result, paymentDTO.BusinessHourGuid);
                                }
                            }

                            if (paymentDTO.IsMasterPass)
                            {
                                op.MasterpassCardInfoLogSetUsed(paymentDTO.MasterPassTransData.VerifierCode);
                            }

                            CheckResult(paymentDTO, result, userId);
                        }

                        #endregion pay by credit card
                    }

                    PaymentTransactionCollection ptc =
                        op.PaymentTransactionGetListByTransIdAndTransType(paymentDTO.TransactionId, PayTransType.Authorization);
                    PaymentTransaction ptCreditcard = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.Creditcard);
                    PaymentTransaction ptScash = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.SCash);
                    PaymentTransaction ptPscash = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.Pscash);
                    PaymentTransaction ptPcash = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.PCash);
                    PaymentTransaction ptDiscount = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.DiscountCode);
                    PaymentTransaction ptATM = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.ATM);
                    PaymentTransaction ptFamilyIsp = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.FamilyIsp);
                    PaymentTransaction ptSevenIsp = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.SevenIsp);
                    PaymentTransaction ptLinePay = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.LinePay);

                    PaymentTransaction ptLinePayCharging = null;
                    if (ptLinePay != null)
                    {
                        ptLinePayCharging = op.PaymentTransactionGet(paymentDTO.TransactionId, PaymentType.LinePay, PayTransType.Charging);
                    }

                    #region paying by pcash

                    if (paymentDTO.DeliveryInfo.PayEasyCash > 0 &&
                        (ptCreditcard == null || (ptCreditcard != null &&
                                                  Enum.Equals(PayTransResponseType.OK, (PayTransResponseType)ptCreditcard.Result))))
                    {
                        //check if one pez member do have enough PCash
                        if (MemberFacade.GetPayeasyCashPoint(userId) >= paymentDTO.DeliveryInfo.PayEasyCash)
                        {
                            ptc.Add(ptPcash);
                        }
                        else
                        {
                            rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                            rtnObject.ResultCode = ApiResultCode.PaymentError;
                            rtnObject.Message = "Payeasy購物金不足";
                            return rtnObject;
                        }
                    }

                    #endregion paying by pcash

                    PayTransResponseType theResponseType = PayTransResponseType.OK;
                    PaymentType errorPaymentType = PaymentType.PCash;
                    List<PaymentType> paymentTypes = new List<PaymentType>();

                    foreach (var pt in ptc)
                    {
                        paymentTypes.Add((PaymentType)pt.PaymentType);

                        if (pt.Result != (int)PayTransResponseType.OK)
                        {
                            theResponseType = PayTransResponseType.GenericError;
                            errorPaymentType = (PaymentType)pt.PaymentType;

                            if (pt.PaymentType == (int)PaymentType.Creditcard)
                            {
                                DeleteWhiteList(userId, pt.PaymentType, pt.Result ?? 0);
                            }
                        }
                    }

                    if (theResponseType == PayTransResponseType.OK)
                    {
                        Guid? orderGuid = ptc.FirstOrDefault().OrderGuid;
                        List<Coupon> coupons = new List<Coupon>();

                        if (theDeal.DealIsOrderable())
                        {
                            #region 檢查pcash transation

                            string authCode = string.Empty;
                            if (ptPcash != null)
                            {
                                //如扣款不成功
                                //pt 2.2 do pcash
                                if (!PponBuyFacade.GetPcashAuthCode(paymentDTO.PezId, paymentDTO.TransactionId, ptPcash.Amount, (DepartmentTypes)theDeal.Department, out authCode))
                                {
                                    //更新transation紀錄 並刪除購物車和session 顯示pcash扣款失敗頁面
                                    PaymentFacade.UpdateTransaction(ptPcash.TransId, Guid.Empty, PaymentType.PCash, ptPcash.Amount, ptPcash.AuthCode,
                                        PayTransType.Authorization, DateTime.Now, ptPcash.Message,
                                        Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department), PayTransPhase.Failed),
                                        (int)PayTransResponseType.GenericError);

                                    OrderFacade.DeleteItemInCart(paymentDTO.TicketId);

                                    pp.TempSessionDelete(paymentDTO.TicketId);
                                    paymentDTO.ResultPageType = (errorPaymentType == PaymentType.PCash) ? PaymentResultPageType.PcashFailed : PaymentResultPageType.CreditcardFailed;
                                    rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                                    rtnObject.ResultCode = ApiResultCode.PaymentError;
                                    rtnObject.Message = "交易失敗!（error:123）";
                                    return rtnObject;
                                }
                                else
                                {
                                    PaymentFacade.UpdateTransaction(ptPcash.TransId, Guid.Empty, PaymentType.PCash, ptPcash.Amount, authCode,
                                        PayTransType.Authorization, DateTime.Now, ptPcash.Message,
                                        Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department),
                                            PayTransPhase.Successful), (int)PayTransResponseType.OK);
                                }
                            }

                            #endregion 檢查pcash transation

                            #region Transaction Scope

                            bool orderOK = false;
                            Order theOrder = op.OrderGet(ptc.First().OrderGuid.Value);
                            OrderDetailCollection odCol = op.OrderDetailGetList(1, theDeal.MaxItemCount ?? 10, string.Empty, theOrder.Guid, OrderDetailTypes.Regular);
                            int atmAmount = ptATM == null ? 0 : (int)ptATM.Amount;
                            int familyIspAmount = ptFamilyIsp == null ? 0 : (int)ptFamilyIsp.Amount;
                            int sevenIspAmount = ptSevenIsp == null ? 0 : (int)ptSevenIsp.Amount;

                            int discountAmt = ptDiscount == null ? 0 : (int)ptDiscount.Amount;
                            int trustCreditCardAmount = ptCreditcard == null ? 0 : (int)ptCreditcard.Amount;
                            int linePayAmount = ptLinePay == null ? 0 : (int)ptLinePay.Amount;

                            #region 檢查付款金額與份數是否相符

                            int quantityTotal = (int)odCol.Sum(x => x.ItemQuantity * x.ItemUnitPrice) + paymentDTO.DeliveryCharge;
                            int paymentTotal = trustCreditCardAmount + (int)paymentDTO.DeliveryInfo.SCash + paymentDTO.DeliveryInfo.PayEasyCash +
                                               paymentDTO.DeliveryInfo.BonusPoints + discountAmt + atmAmount + familyIspAmount + sevenIspAmount + linePayAmount;

                            if (!int.Equals(quantityTotal, paymentTotal))
                            {
                                rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                                rtnObject.ResultCode = ApiResultCode.PaymentError;
                                rtnObject.Message = "交易失敗！（error:124）";
                                StringBuilder sb = new StringBuilder();
                                sb.AppendFormat("付款金額與份數不符: orderGuid={0}\r\n", theOrder.Guid);
                                sb.AppendFormat(",quantityTotal={0},odSum={1},DeliveryCharge={2}\r\n",
                                    quantityTotal, odCol.Sum(x => x.ItemQuantity * x.ItemUnitPrice), paymentDTO.DeliveryCharge);
                                sb.AppendFormat(
                                    "paymentTotal={0}, trustCreditCardAmount={1}, SCash={2}, PayEasyCash={3}, BonusPoints={4}, discountAmt={5},atmAmount={6}, familyIspAmount={7}, sevenIspAmount={8}\r\n",
                                    paymentTotal, trustCreditCardAmount,
                                    (int)paymentDTO.DeliveryInfo.SCash, paymentDTO.DeliveryInfo.PayEasyCash,
                                    paymentDTO.DeliveryInfo.BonusPoints, discountAmt, atmAmount, familyIspAmount, sevenIspAmount);
                                if (HttpContext.Current != null && HttpContext.Current.Items[LkSiteContextItem._API_USER_ID] != null)
                                {
                                    sb.AppendFormat("apiUserId: {0}\r\n", HttpContext.Current.Items[LkSiteContextItem._API_USER_ID]);
                                }
                                logger.ErrorFormat(sb.ToString());
                                paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;

                                return rtnObject;
                            }

                            //ApplePay檢查付款金額
                            if (isApplePay)
                            {
                                var appleAmount = paymentDTO.ApplePayData.PaymentDataType == "EMV" ? paymentDTO.Amount : int.Parse(paymentDTO.ApplePayData.TransactionAmount.ToString()) / 100;
                                var totalAmount = appleAmount + (int)paymentDTO.DeliveryInfo.SCash + paymentDTO.DeliveryInfo.PayEasyCash + paymentDTO.DeliveryInfo.BonusPoints + discountAmt;
                                if (!int.Equals(totalAmount, paymentTotal))
                                {
                                    rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                                    rtnObject.ResultCode = ApiResultCode.PaymentError;
                                    rtnObject.Message = "交易失敗！（error:124）";
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendFormat("Apple實際付款金額與份數不符: orderGuid={0}\r\n", theOrder.Guid);
                                    sb.AppendFormat(
                                        "paymentTotal={0}, trustApplePayAmount={1}, SCash={2}, PayEasyCash={3}, BonusPoints={4}, discountAmt={5},atmAmount={6}, familyIspAmount={7}, sevenIspAmount={8}\r\n",
                                        paymentTotal, appleAmount,
                                        (int)paymentDTO.DeliveryInfo.SCash, paymentDTO.DeliveryInfo.PayEasyCash,
                                        paymentDTO.DeliveryInfo.BonusPoints, discountAmt, atmAmount, familyIspAmount, sevenIspAmount);
                                    if (HttpContext.Current != null && HttpContext.Current.Items[LkSiteContextItem._API_USER_ID] != null)
                                    {
                                        sb.AppendFormat("apiUserId: {0}\r\n", HttpContext.Current.Items[LkSiteContextItem._API_USER_ID]);
                                    }
                                    logger.ErrorFormat(sb.ToString());
                                    paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;

                                    return rtnObject;
                                }
                            }

                            #endregion 檢查付款金額與份數是否相符

                            int quantity = odCol.Sum(x => x.ItemQuantity);

                            using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                            {
                                #region pcash扣款成功後 更新transation

                                if (ptPcash != null)
                                {
                                    PaymentFacade.UpdateTransaction(ptPcash.TransId, theOrder.Guid, PaymentType.PCash, ptPcash.Amount, authCode,
                                        PayTransType.Authorization, DateTime.Now, ptPcash.Message,
                                        Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department), PayTransPhase.Successful),
                                        (int)PayTransResponseType.OK);
                                }

                                #endregion pcash扣款成功後 更新transation

                                pp.TempSessionDelete(paymentDTO.TicketId);

                                #region Scash

                                if (paymentDTO.DeliveryInfo.SCash > 0 &&
                                    (ptCreditcard == null || (ptCreditcard != null &&
                                                              Enum.Equals(PayTransResponseType.OK, (PayTransResponseType)ptCreditcard.Result))))
                                {
                                    //pt 2.5
                                    if (ptScash != null && ptScash.Amount > 0)
                                    {
                                        OrderFacade.ScashUsed(ptScash.Amount, userId, theOrder.Guid, "購物金折抵:" + theDeal.SellerName,
                                            userName, OrderClassification.CashPointOrder);
                                    }
                                    if (ptPscash != null && ptPscash.Amount > 0)
                                    {
                                        OrderFacade.PscashUsed(ptPscash.Amount, userId, theOrder.Guid, "購物金折抵:" + theDeal.SellerName,
                                            userName);
                                    }
                                }

                                #endregion Scash

                                #region 扣除選項

                                TempSessionCollection tsc = pp.TempSessionGetList("Accessory" + paymentDTO.TicketId);
                                if (tsc.Count > 0)
                                {
                                    //多重選項
                                    foreach (TempSession ts in tsc)
                                    {
                                        if (ProposalFacade.IsVbsProposalNewVersion() && theDeal.IsHouseDealNewVersion())
                                        {
                                            Guid option_guid = Guid.Empty;
                                            Guid.TryParse(ts.Name, out option_guid);
                                            PponOption opt1 = pp.PponOptionGetByGuid(option_guid);
                                            int forOut = 0;
                                            int.TryParse(ts.ValueX, out forOut);
                                            if (opt1 != null && opt1.IsLoaded)
                                            {
                                                if (opt1.ItemGuid != null)
                                                {
                                                    ProductItem pdi = pp.ProductItemGet(opt1.ItemGuid.Value);
                                                    if (pdi != null && pdi.IsLoaded)
                                                    {

                                                        PponBuyFacade.UpdatePchomeInventory(theDeal, pdi);


                                                        int stock = pdi.Stock - forOut;
                                                        if (stock >= 0)
                                                        {
                                                            pdi.Stock = pdi.Stock - forOut;
                                                            pdi.Sales = pdi.Sales + forOut;
                                                            pp.ProductItemSet(pdi);

                                                            PponFacade.UpdateDealMaxItemCount(theDeal, opt1.ItemGuid.Value); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                                        }
                                                        else
                                                        {
                                                            StringBuilder sb = new StringBuilder();
                                                            sb.AppendLine(string.Format("{0} 庫存不足，{1}{2}", pdi.SpecName, theDeal.BusinessHourGuid, theDeal.ItemName));
                                                            logger.ErrorFormat(sb.ToString());

                                                            PponFacade.UpdateDealMaxItemCount(theDeal, opt1.ItemGuid.Value); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                                            paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            int forOut = 0;
                                            AccessoryGroupMember agm = ip.AccessoryGroupMemberGet(new Guid(ts.Name));
                                            if (!agm.IsLoaded)
                                            {
                                                continue;
                                            }

                                            if (agm.Quantity != null && int.TryParse(ts.ValueX, out forOut))
                                            {
                                                agm.Quantity = agm.Quantity - int.Parse(ts.ValueX);

                                                int? optId = PponOption.FindId(agm.Guid, theDeal.BusinessHourGuid);
                                                if (optId.HasValue)
                                                {
                                                    PponOption opt = pp.PponOptionGet(optId.Value);
                                                    if (opt != null && opt.IsLoaded)
                                                    {
                                                        opt.Quantity = agm.Quantity;
                                                        pp.PponOptionSet(opt);
                                                    }
                                                }
                                            }

                                            ip.AccessoryGroupMemberSet(agm);
                                        }
                                    }
                                }
                                else
                                {
                                    if (ProposalFacade.IsVbsProposalNewVersion() && theDeal.IsHouseDealNewVersion())
                                    {
                                        ProposalMultiDeal pmd = sp.ProposalMultiDealGetByBid(theDeal.BusinessHourGuid);
                                        if (pmd.IsLoaded)
                                        {
                                            ProposalMultiDealsSpec spec = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(pmd.Options).FirstOrDefault();
                                            if (spec != null)
                                            {
                                                ProductItem pdi = pp.ProductItemGet(spec.Items.FirstOrDefault().item_guid);
                                                if (pdi != null && pdi.IsLoaded)
                                                {
                                                    int stock = pdi.Stock - (quantity * theDeal.QuantityMultiplier.GetValueOrDefault(1));
                                                    if (stock >= 0)
                                                    {
                                                        pdi.Stock = pdi.Stock - (quantity * theDeal.QuantityMultiplier.GetValueOrDefault(1));
                                                        pdi.Sales = pdi.Sales + (quantity * theDeal.QuantityMultiplier.GetValueOrDefault(1));
                                                        pp.ProductItemSet(pdi);

                                                        PponFacade.UpdateDealMaxItemCount(theDeal, spec.Items.FirstOrDefault().item_guid); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                                    }
                                                    else
                                                    {
                                                        StringBuilder sb = new StringBuilder();
                                                        sb.AppendLine(string.Format("{0} 庫存不足，{1}{2}", pdi.SpecName, theDeal.BusinessHourGuid, theDeal.ItemName));
                                                        logger.ErrorFormat(sb.ToString());

                                                        PponFacade.UpdateDealMaxItemCount(theDeal, spec.Items.FirstOrDefault().item_guid); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                                        paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                pp.TempSessionDelete("Accessory" + paymentDTO.TicketId);

                                #endregion 扣除選項

                                #region 分店銷售數量

                                if (paymentDTO.DeliveryInfo.StoreGuid != Guid.Empty)
                                {
                                    int k = (int)(Math.Ceiling((double)paymentDTO.DeliveryInfo.Quantity / (theDeal.ComboPackCount ?? 1)));
                                    k = (theDeal.SaleMultipleBase ?? 0) > 0 ? theDeal.SaleMultipleBase.Value : k;
                                    pp.PponStoreUpdateOrderQuantity(theDeal.BusinessHourGuid, paymentDTO.DeliveryInfo.StoreGuid, k);
                                }

                                #endregion 分店銷售數量

                                #region 信用卡轉購物金

                                if (ptCreditcard != null)
                                {
                                    Guid sOrderGuid = OrderFacade.MakeSCachOrder(ptCreditcard.Amount, ptCreditcard.Amount, "17Life購物金購買(系統)",
                                        CashPointStatus.Approve, userId, theDeal.ItemName, CashPointListType.Income, userName, theOrder.Guid);

                                    //Scash will not be invoiced initially after the NewInvoiceDate.
                                    bool invoiced = (DateTime.Now < cp.NewInvoiceDate);
                                    int depositId = OrderFacade.NewScashDeposit(sOrderGuid, ptCreditcard.Amount, theOrder.UserId, theOrder.Guid,
                                        "購物金購買(系統):" + theDeal.SellerName, userName, invoiced, OrderClassification.CashPointOrder);

                                    OrderFacade.NewScashWithdrawal(depositId, ptCreditcard.Amount, theOrder.Guid, "購物金折抵(系統):" + theDeal.SellerName,
                                        userName, OrderClassification.CashPointOrder);

                                    ptCreditcard.OrderGuid = sOrderGuid;
                                    ptCreditcard.Message += "|" + theOrder.Guid.ToString();
                                    op.PaymentTransactionSet(ptCreditcard);

                                    if (ptScash == null)
                                    {
                                        OrderFacade.MakeTransaction(paymentDTO.TransactionId, PaymentType.SCash, int.Parse(ptCreditcard.Amount.ToString("F0")),
                                            (DepartmentTypes)theDeal.Department, userName, theOrder.Guid, paymentDTO.PezId);
                                    }
                                    else
                                    {
                                        PaymentFacade.UpdateTransaction(ptScash.TransId, ptScash.OrderGuid, PaymentType.SCash, ptScash.Amount + ptCreditcard.Amount,
                                            ptScash.AuthCode, PayTransType.Authorization, DateTime.Now, ptScash.Message, ptScash.Status, ptScash.Result.Value);
                                    }
                                }

                                #endregion 信用卡轉購物金


                                #region ThirdPartyPay 轉購物金
                                
                                if (ptLinePay != null)
                                {
                                    OrderFacade.ExchangeSCash(paymentDTO.TransactionId, ptLinePay, ptScash, theOrder, theDeal, userId, userName, ptLinePayCharging);
                                }

                                #endregion ThirdPartyPay 轉購物金

                                transScope.Complete();
                                orderOK = true;
                            }

                            #endregion Transaction Scope

                            #region Transaction Scope 交易失敗
                            if (!orderOK)
                            {
                                CreditCardUtility.AuthenticationReverse(paymentDTO.TransactionId);
                                if (ptPcash != null)
                                {
                                    PCashWorker.DeCheckOut(paymentDTO.TransactionId, authCode, ptPcash.Amount);
                                }
                                rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                                rtnObject.ResultCode = ApiResultCode.PaymentError;
                                rtnObject.Message = "交易失敗！（error:125）";
                                logger.Error("Order update failed.");
                                paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;
                                return rtnObject;
                            }
                            #endregion
                            #region Transaction Scope 交易成功
                            else
                            {
                                //加入照會(2016/7/21從CreditCardUtility移過來)
                                CreditcardOrder orderInfo = new CreditcardOrder();
                                CreditcardOrderCollection oldOrders = op.GetCreditcardOrderByOrderGuid(theOrder.Guid);
                                CreditcardResult result;
                                if (oldOrders != null && oldOrders.Count > 0)
                                {
                                    orderInfo = oldOrders[0];
                                    if (cp.EnableCreditcardRefer)
                                    {
                                        if (orderInfo.Result == 0 || orderInfo.Result == -1)   //0是授權正常，-1是初始值
                                        {
                                            if (orderInfo.CreditcardAmount >= cp.CreditcardReferAmount)
                                            {
                                                orderInfo.Result = (int)CreditcardResult.OverQuantity;

                                                if (PaymentFacade.IsCreditcardRefer(theOrder.Guid))
                                                {
                                                    orderInfo = PaymentFacade.SetCreditcardRefer(orderInfo, theOrder.Guid);
                                                }
                                            }
                                            else if (op.IsCreditcardOrderOverAmount(orderInfo.CreditcardInfo, orderInfo.CreditcardAmount, orderInfo.CreateTime,
                                                24, 10000))
                                            {
                                                orderInfo.Result = (int)CreditcardResult.OverQuantityWithin24Hr;
                                                if (PaymentFacade.IsCreditcardRefer(theOrder.Guid))
                                                {
                                                    orderInfo.IsReference = true;
                                                    orderInfo.ReferenceId = "sys";
                                                    orderInfo.ReferenceTime = DateTime.Now;
                                                }
                                            }
                                            else if (op.IsCreditcardOrderOverAmount(orderInfo.CreditcardInfo, orderInfo.CreditcardAmount, orderInfo.CreateTime,
                                                1, cp.CreditcardReferAmount))
                                            {
                                                orderInfo.Result = (int)CreditcardResult.OverQuantityWithin1Hr;
                                            }
                                            else if (PaymentFacade.IsSuspectCreditcardFraud(paymentDTO.DeliveryInfo.DeliveryType, paymentDTO.DeliveryInfo.ProductDeliveryType, orderInfo.ConsumerIp, theOrder.DeliveryAddress, userId, theOrder.SellerGuid, out result) && !paymentDTO.DeliveryInfo.IsApplePay)
                                            {
                                                orderInfo.Result = (int)result;

                                                if (PaymentFacade.IsCreditcardRefer(theOrder.Guid))
                                                {
                                                    //宅配
                                                    orderInfo = PaymentFacade.SetCreditcardRefer(orderInfo, theOrder.Guid);
                                                }
                                                else if (paymentDTO.DeliveryInfo.DeliveryType == DeliveryType.ToShop)
                                                {
                                                    //憑證
                                                    PaymentFacade.PponCreditcardFraud(theOrder.CreateTime, theOrder.CreateId, theOrder.UserId, theOrder.OrderId, theDeal.ItemName, orderInfo.ConsumerIp);
                                                }
                                            }

                                        }
                                    }
                                    op.SetCreditcardOrder(orderInfo);
                                }


                                //if weeklypay deal
                                int trust_checkout_type = 0;
                                if (((theDeal.BusinessHourStatus) & (int)BusinessHourStatus.WeeklyPay) > 0)
                                {
                                    trust_checkout_type = (int)TrustCheckOutType.WeeklyPay;
                                }

                                #region make cash trust log

                                #region payments
                                Dictionary<Core.PaymentType, int> payments = new Dictionary<Core.PaymentType, int>();

                                decimal scash = (ptScash != null && ptScash.IsLoaded) ? ptScash.Amount : 0;
                                decimal pscash = (ptPscash != null && ptPscash.IsLoaded) ? ptPscash.Amount : 0;
                                if (scash > 0)
                                {
                                    payments.Add(Core.PaymentType.SCash,(int) scash);
                                }
                                if (pscash > 0)
                                {
                                    payments.Add(Core.PaymentType.Pscash, (int)pscash);
                                }
                                if ((int)paymentDTO.DeliveryInfo.PayEasyCash > 0)
                                {
                                    payments.Add(Core.PaymentType.PCash, (int)paymentDTO.DeliveryInfo.PayEasyCash);
                                }
                                if ((int)paymentDTO.DeliveryInfo.BonusPoints > 0)
                                {
                                    payments.Add(Core.PaymentType.BonusPoint, (int)paymentDTO.DeliveryInfo.BonusPoints);
                                }
                                if ((int)trustCreditCardAmount > 0)
                                {
                                    payments.Add(Core.PaymentType.Creditcard, (int)trustCreditCardAmount);
                                }
                                if ((int)discountAmt > 0)
                                {
                                    payments.Add(Core.PaymentType.DiscountCode, (int)discountAmt);
                                }
                                if ((int)atmAmount > 0)
                                {
                                    payments.Add(Core.PaymentType.ATM, (int)atmAmount);
                                }
                                if (familyIspAmount > 0)
                                {
                                    payments.Add(Core.PaymentType.FamilyIsp, familyIspAmount);
                                }
                                if (sevenIspAmount > 0)
                                {
                                    payments.Add(Core.PaymentType.SevenIsp, sevenIspAmount);
                                }
                                if (linePayAmount > 0)
                                {
                                    payments.Add(Core.PaymentType.LinePay, linePayAmount);
                                }

                                #endregion payments

                                //判斷成套數量
                                int saleMultipleBase = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? theDeal.SaleMultipleBase ?? 0 : 0;

                                CashTrustInfo cti = new CashTrustInfo
                                {
                                    OrderGuid = theOrder.Guid,
                                    OrderDetails = odCol,
                                    CreditCardAmount = trustCreditCardAmount,
                                    SCashAmount = (int)scash,
                                    PscashAmount = (int)pscash,
                                    PCashAmount = paymentDTO.DeliveryInfo.PayEasyCash,
                                    BCashAmount = paymentDTO.DeliveryInfo.BonusPoints,
                                    ThirdPartyCashAmount = (int)linePayAmount,
                                    ThirdPartySystem = linePayAmount > 0 ? ThirdPartyPayment.LinePay : ThirdPartyPayment.None,
                                    Quantity = saleMultipleBase > 0 ? saleMultipleBase : quantity,
                                    DeliveryCharge = paymentDTO.DeliveryCharge,
                                    DeliveryType = theDeal.DeliveryType == null ? DeliveryType.ToShop : (DeliveryType)theDeal.DeliveryType.Value,
                                    DiscountAmount = discountAmt,
                                    AtmAmount = atmAmount,
                                    FamilyIspAmount = familyIspAmount,
                                    SevenIspAmount = sevenIspAmount,
                                    BusinessHourGuid = theDeal.BusinessHourGuid,
                                    CheckoutType = trust_checkout_type,
                                    TrustProvider = Helper.GetBusinessHourTrustProvider(theDeal.BusinessHourStatus),
                                    ItemName = theDeal.ItemName,
                                    ItemPrice = (int)theDeal.ItemPrice,
                                    ItemOriPrice = (int)theDeal.ItemOrigPrice,
                                    User = mp.MemberGet(userName),
                                    Payments = payments,
                                    PresentQuantity = theDeal.PresentQuantity ?? 0,
                                    IsGroupCoupon = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon),
                                    GroupCouponType = (GroupCouponDealType)(theDeal.GroupCouponDealType ?? 0)
                                };

                                CashTrustLogCollection ctCol = OrderFacade.MakeCashTrust(cti);

                                #endregion make cash trust log

                                if (theDeal.EntrustSell == (int)EntrustSellType.No)
                                {
                                    #region 新制憑證發票

                                    string creditCardTail = !string.IsNullOrEmpty(paymentDTO.CreditcardNumber) ? paymentDTO.CreditcardNumber.Substring(paymentDTO.CreditcardNumber.Length - 4) : string.Empty;

                                    if (isApplePay)
                                    {
                                        creditCardTail = paymentDTO.ApplePayData.ApplicationPrimaryAccountNumber.Substring(paymentDTO.ApplePayData.ApplicationPrimaryAccountNumber.Length - 4);
                                    }

                                    if (ctCol.Count() > 0 && paymentDTO.DeliveryInfo.DeliveryType == DeliveryType.ToShop)
                                    {
                                        foreach (var ct in ctCol)
                                        {
                                            int uninvoicedAmount = saleMultipleBase > 0 ? ((ctCol.Count() > 0) ? ctCol.Sum(x => x.UninvoicedAmount) : 0) : ct.UninvoicedAmount;
                                            //ATM or 超商未付款
                                            if (paymentDTO.IsPaidByATM ||
                                                paymentDTO.MainPaymentType == ApiMainPaymentType.FamilyIsp ||
                                                paymentDTO.MainPaymentType == ApiMainPaymentType.SevenIsp)
                                            {
                                                EinvoiceFacade.SetEinvoiceMain(theOrder, paymentDTO.DeliveryInfo, paymentDTO.TransactionId, 1,
                                                    theDeal.ItemName, uninvoicedAmount,
                                                    Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                    userName, false, GetOrderClassification(theDeal), creditCardTail, (int)EinvoiceType.NotComplete, version, Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon));
                                            }
                                            else if (uninvoicedAmount > 0)
                                            {
                                                EinvoiceFacade.SetEinvoiceMain(theOrder, paymentDTO.DeliveryInfo, paymentDTO.TransactionId, 1,
                                                    theDeal.ItemName, uninvoicedAmount,
                                                    Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                    userName, false, GetOrderClassification(theDeal), creditCardTail, (int)EinvoiceType.Initial, version, Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon));
                                            }
                                            if (Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    #endregion 新制憑證發票

                                    #region 新制宅配發票

                                    int uninvoicedTotal = (ctCol.Count() > 0) ? ctCol.Sum(x => x.UninvoicedAmount) : 0;
                                    if (paymentDTO.DeliveryInfo.DeliveryType == DeliveryType.ToHouse)
                                    {
                                        //ATM or 超商未付款
                                        if (paymentDTO.IsPaidByATM ||
                                            paymentDTO.MainPaymentType == ApiMainPaymentType.FamilyIsp ||
                                            paymentDTO.MainPaymentType == ApiMainPaymentType.SevenIsp)
                                        {
                                            EinvoiceFacade.SetEinvoiceMain(theOrder, paymentDTO.DeliveryInfo, paymentDTO.TransactionId, quantity,
                                                theDeal.ItemName, uninvoicedTotal,
                                                Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                userName, true, GetOrderClassification(theDeal), creditCardTail, (int)EinvoiceType.NotComplete, version);
                                        }
                                        else if (uninvoicedTotal > 0)
                                        {
                                            EinvoiceFacade.SetEinvoiceMain(theOrder, paymentDTO.DeliveryInfo, paymentDTO.TransactionId, quantity,
                                                theDeal.ItemName, uninvoicedTotal,
                                                Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                userName, true, GetOrderClassification(theDeal), creditCardTail, (int)EinvoiceType.Initial, version);
                                        }
                                    }

                                    #endregion 新制宅配發票
                                }
                                else
                                {
                                    //代收轉付以收據代替發票。
                                    OrderFacade.EntrustSellReceiptSet(paymentDTO.DeliveryInfo, theOrder, odCol, ctCol, theDeal.ItemName, userName);
                                }

                                // save delivery info
                                PponBuyFacade.SaveDeliveryInfo(mem.UniqueId, theOrder, BuyAddressInfo.Parse(paymentDTO.AddressInfo), theDeal.DeliveryType,
                                    paymentDTO.DeliveryInfo.CarrierType, paymentDTO.DeliveryInfo.CarrierId,
                                    paymentDTO.DeliveryInfo.InvoiceBuyerName, paymentDTO.DeliveryInfo.InvoiceBuyerAddress);

                                // set deal close time
                                OrderFacade.PponSetCloseTimeIfJustReachMinimum(theDeal, quantity);

                                #region 廠商序號(Peztemp)成套票券

                                //全家成套票券(咖啡寄杯)
                                if ((theDeal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0 &&
                                    Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) &&
                                    (theDeal.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
                                {
                                    if (!FamiGroupCoupon.MakeFamiportPincodeTrans(theDeal, theOrder, saleMultipleBase, userId))
                                    {
                                        throw new System.AggregateException("Famiport Get Pincode Fail.");
                                    }
                                }
                                
                                #endregion
                                //產生憑證號碼，含產生全家Peztemp
                                CouponFacade.GenCouponWithSmsWithGeneratePeztemp(theOrder, theDeal, paymentDTO.IsPaidByATM);


                                //萊爾富咖啡寄杯
                                if (cp.EnableHiLifeDealSetup && (theDeal.GroupOrderStatus & (int)GroupOrderStatus.HiLifeDeal) > 0 &&
                                    Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) &&
                                    (theDeal.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
                                {
                                    if (!HiLifeFacade.MakeHiLifePincode(theOrder, saleMultipleBase, userId))
                                    {
                                        throw new System.AggregateException("HiLife Insert Pincode Fail.");
                                    }
                                }

                                // send deal-on mail 購買成功信
                                if (paymentDTO.IsPaidByATM) //ATM 付款、ATM付款超商取貨
                                {
                                    PayEvents.OnOrderCreated(theDeal.BusinessHourGuid, userId, paymentDTO.OrderGuid, PaymentType.ATM);
                                    //加註訂單為ATM付款
                                    OrderFacade.SetOrderStatus(theOrder.Guid, userName, OrderStatus.ATMOrder, true,
                                        new SalesInfoArg
                                        {
                                            BusinessHourGuid = theDeal.BusinessHourGuid,
                                            Quantity = theDeal.OrderedQuantity.GetValueOrDefault(),
                                            Total = theDeal.OrderedTotal.GetValueOrDefault()
                                        });
                                    Member m = mp.MemberGet(userName);
                                    OrderFacade.SendATMOrderNotifyMail(m.DisplayName, m.UserEmail, theDeal.ItemName,
                                        paymentDTO.ATMAccount, atmAmount.ToString(), theDeal,theOrder, quantity);
                                }
                                else if (paymentDTO.MainPaymentType == ApiMainPaymentType.FamilyIsp ||
                                         paymentDTO.MainPaymentType == ApiMainPaymentType.SevenIsp)
                                {
                                    if (paymentDTO.MainPaymentType == ApiMainPaymentType.FamilyIsp)
                                    {
                                        PayEvents.OnOrderCreated(theDeal.BusinessHourGuid, userId, paymentDTO.OrderGuid, PaymentType.FamilyIsp);
                                    }
                                    else
                                    {
                                        PayEvents.OnOrderCreated(theDeal.BusinessHourGuid, userId, paymentDTO.OrderGuid, PaymentType.SevenIsp);
                                    }
                                    //超商付款、超商取貨
                                    OrderStatus orderStatus = OrderStatus.Nothing;
                                    if (paymentDTO.MainPaymentType == ApiMainPaymentType.FamilyIsp)
                                    {
                                        orderStatus = OrderStatus.FamilyIspOrder;
                                    }
                                    else if (paymentDTO.MainPaymentType == ApiMainPaymentType.SevenIsp)
                                    {
                                        orderStatus = OrderStatus.SevenIspOrder;
                                    }

                                    OrderFacade.SetOrderStatus(theOrder.Guid, userName, orderStatus, true,
                                        new SalesInfoArg
                                        {
                                            BusinessHourGuid = theDeal.BusinessHourGuid,
                                            Quantity = theDeal.OrderedQuantity.GetValueOrDefault(),
                                            Total = theDeal.OrderedTotal.GetValueOrDefault()
                                        });
                                    
                                    //如果付款金而為0表示該檔次為0元好康，不發信
                                    if (paymentDTO.Amount != 0)
                                    {
                                        OrderFacade.SendPponMail(MailContentType.Authorized, theOrder.Guid);
                                    }
                                }
                                else //其他支付方式付款、超商取貨但用其他支付方式付款(ex: 信用卡付款+超商取貨)
                                {
                                    PayEvents.OnOrderCreated(theDeal.BusinessHourGuid, userId, paymentDTO.OrderGuid, PaymentType.Creditcard);
                                    OrderFacade.SetOrderStatus(theOrder.Guid, userName, OrderStatus.Complete, true,
                                        new SalesInfoArg
                                        {
                                            BusinessHourGuid = theDeal.BusinessHourGuid,
                                            Quantity = odCol.OrderedQuantity,
                                            Total = odCol.OrderedTotal
                                        });

                                    //如果付款金而為0表示該檔次為0元好康，不發信
                                    if (paymentDTO.Amount != 0)
                                    {
                                        OrderFacade.SendPponMail(MailContentType.Authorized, theOrder.Guid);
                                    }
                                }

                                #region 建立pchome訂單
                                if (PponBuyFacade.AddPchomeOrder(theDeal, theOrder.Guid, theOrder) == false)
                                {
                                    rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                                    rtnObject.ResultCode = ApiResultCode.PaymentError;
                                    rtnObject.Message = "交易失敗！（error:126）";
                                    logger.Error("Create Pchome Order failed.");
                                    paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;
                                    return rtnObject;
                                }
                                #endregion 建立pchome訂單


                                #region 推薦送折價券
                                //有推薦人資料，發放推薦折價券
                                if (!string.IsNullOrWhiteSpace(paymentDTO.ReferenceId))
                                {
                                    PromotionFacade.ReferenceDiscountCheckAndPay(paymentDTO.ReferenceId, theOrder, theDeal, MemberFacade.GetUserName(theOrder.UserId));
                                }
                                #endregion 推薦送折價券

                                #region 首購送折價券
                                PromotionFacade.FirstBuyDiscountCheckAndPay(theOrder, theDeal);
                                #endregion

                                #region 記錄CPA

                                string cpaKey = paymentDTO.CpaKey;
                                if (string.IsNullOrEmpty(cpaKey) &&
                                    paymentDTO.CpaList != null && paymentDTO.CpaList.Length > 0)
                                {
                                    cpaKey = CpaUtility.MergeCpaKeysToOne(paymentDTO.CpaList);
                                }
                                if (string.IsNullOrEmpty(cpaKey) == false)
                                {
                                    CpaUtility.RecordCpaOrderByReferrer(cpaKey, theOrder);
                                }
                                logger.InfoFormat("成立訂單 {0}, CpaKey={1}, DeviceId={2}",
                                    paymentDTO.OrderGuid, cpaKey, paymentDTO.DeviceId);

                                #endregion
                            } //End orderOK
                            #endregion

                            paymentDTO.ResultPageType =
                                paymentTypes.Contains(PaymentType.Creditcard) || paymentTypes.Contains(PaymentType.ATM) ||
                                paymentTypes.Contains(PaymentType.FamilyIsp) || paymentTypes.Contains(PaymentType.FamilyIsp)
                                || paymentTypes.Contains(PaymentType.LinePay)
                                    ? PaymentResultPageType.CreditcardSuccess
                                    : PaymentResultPageType.PcashSuccess;

                            rtnObject.ReturnCode = ApiReturnCode.Success;
                            rtnObject.ResultCode = ApiResultCode.Success;
                            rtnObject.Data = new { IsATM = false, OrderGuid = orderGuid ?? Guid.Empty };

                            #region 交易成功 (0元檔)
                            //0元的DEAL處理回傳的變數
                            if ((paymentTypes.Contains(PaymentType.Creditcard)) && (theDeal.ItemPrice == 0))
                            {
                                paymentDTO.ResultPageType = PaymentResultPageType.ZeroPriceSuccess;

                                if (int.Equals(0, coupons.Count))
                                {
                                    rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                                    rtnObject.ResultCode = ApiResultCode.PaymentError;
                                    paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;
                                    rtnObject.Message = "很抱歉，這個序號無法被取得！";
                                    return rtnObject;
                                }
                                else
                                {
                                    //寄送完成通知
                                    string memberEmail = MemberFacade.GetUserEmail(theOrder.UserId);
                                    OrderFacade.SendZeroDealSuccessMail(theOrder.MemberName, memberEmail, theDeal,
                                        pp.ViewPponCouponGet(coupons[0].Id), (GroupOrderStatus)theDeal.GroupOrderStatus, (CouponCodeType)theDeal.CouponCodeType.Value);
                                    //零元檔次的回覆
                                    ApiFreeDealPayReply payReply = new ApiFreeDealPayReply();
                                    payReply.CouponCode = coupons[0].Code;
                                    payReply.PromoImage = ImageFacade.GetMediaPathsFromRawData(theDeal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 3).DefaultIfEmpty(cp.SiteUrl + "/Themes/default/images/17Life/G3/0act.jpg").First();
                                    var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(theDeal);
                                    AvailableInformation aif = ProviderFactory.Instance().GetSerializer()
                                        .Deserialize<AvailableInformation[]>(contentLite.Availability).FirstOrDefault();
                                    if (aif != null && !string.IsNullOrEmpty(aif.U))
                                    {
                                        payReply.PromoUrl = aif.U.Trim();
                                    }
                                    else
                                    {
                                        payReply.PromoUrl = cp.SiteUrl + "/ppon/default.aspx";
                                    }
                                    payReply.IsFami = (theDeal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0;
                                    payReply.IsCouponDownload = (theDeal.GroupOrderStatus & (int)GroupOrderStatus.PEZeventCouponDownload) > 0;
                                    payReply.OrderGuid = orderGuid ?? Guid.Empty;
                                    rtnObject.Data = payReply;
                                }

                            }
                            #endregion
                            #region 交易成另 (ATM)
                            else if (paymentDTO.IsPaidByATM)
                            {
                                if (Equals(14, paymentDTO.ATMAccount.Length))
                                {
                                    ApiFreeDealPayReply payReply = new ApiFreeDealPayReply();
                                    payReply.IsATM = paymentDTO.IsPaidByATM;
                                    payReply.AtmData.BankAccount = paymentDTO.ATMAccount;
                                    payReply.AtmData.Amount = atmAmount;

                                    //iOS再製作時沒有加上+0800結尾就送新版了，所以這邊先補
                                    //今晚 24 時前完成匯款
                                    string dateFormat = "{0:yyyyMMdd 235900 zz00}";
                                    if (HttpContext.Current.Items[LkSiteContextItem._API_USER_ID] != null &&
                                        (HttpContext.Current.Items[LkSiteContextItem._API_USER_ID].ToString().Equals("IOS2013233637") ||
                                         HttpContext.Current.Items[LkSiteContextItem._API_USER_ID].ToString().Equals("IOS2013921385")))
                                    {
                                        dateFormat = "{0:yyyy-MM-dd 23:59:00}";
                                    }

                                    payReply.AtmData.DeadlineDate = string.Format(dateFormat, DateTime.Today);
                                    payReply.OrderGuid = orderGuid ?? Guid.Empty;

                                    rtnObject.Data = payReply;
                                }
                                else if (string.Equals(paymentDTO.ATMAccount, "-2"))
                                {
                                    rtnObject.ReturnCode = ApiReturnCode.ATMDealsFull;
                                    rtnObject.ResultCode = ApiResultCode.ATMDealsFull;
                                    rtnObject.Message = I18N.Message.ATMDealsFull;
                                }
                            }
                            #endregion                            
                        } //End theDeal.DealIsOrderable
                        else
                        {
                            if (ptc.Count > 0)
                            {
                                PaymentFacade.CancelPaymentTransactionByTransId(ptc[0].TransId, "sys", "Unorderable. ");
                            }

                            rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                            rtnObject.ResultCode = ApiResultCode.PaymentError;
                            paymentDTO.ResultPageType = (errorPaymentType == PaymentType.PCash) ? PaymentResultPageType.PcashFailed : PaymentResultPageType.CreditcardFailed;
                            rtnObject.Message = "交易失敗！（error:126）";
                            return rtnObject;
                        }
                    } //End PayTransResponseType.OK
                    else
                    {
                        OrderFacade.DeleteItemInCart(paymentDTO.TicketId);
                        pp.TempSessionDelete(paymentDTO.TicketId);
                        paymentDTO.TransactionId = null;
                        paymentDTO.ResultPageType = (errorPaymentType == PaymentType.PCash) ? PaymentResultPageType.PcashFailed : PaymentResultPageType.CreditcardFailed;
                        rtnObject.ReturnCode = ApiReturnCode.CreditcardInfoError;
                        rtnObject.ResultCode = ApiResultCode.PaymentCreditcardInfoError;
                        rtnObject.Message = I18N.Message.CreditcardInfoError;
                        return rtnObject;
                    } //End 非 PayTransResponseType.OK
                } //End PaymentDTO.TransactionId is null
                else
                {
                    rtnObject.ReturnCode = ApiReturnCode.PaymentError;
                    rtnObject.ResultCode = ApiResultCode.PaymentError;
                    rtnObject.Message = "交易失敗！（error:127）";
                    logger.Error("TransactionID is missing.");
                    return rtnObject;
                }
            }
            return rtnObject;
        }

        public static void CheckResult(PaymentDTO paymentDTO, CreditCardAuthResult result, int userId)
        {
            PaymentTransaction pt = op.PaymentTransactionGet(paymentDTO.TransactionId, PaymentType.Creditcard, PayTransType.Authorization);
            int status = Helper.SetPaymentTransactionPhase(pt.Status, result.TransPhase);
            int rlt = 0;
            try
            {
                rlt = int.Parse(result.ReturnCode);
            }
            catch
            {
                rlt = -9527;
            }

            if (Helper.GetPaymentTransactionPhase(pt.Status) != PayTransPhase.Created)
            {
                logger.Error("trans_id: " + pt.TransId + " 被拒絕覆蓋已回覆的授權紀錄\n result: " + rlt + "\n message: " + result.TransMessage
                             + "\n api_provider: " + (int)result.APIProvider + "\n original_apiprovider: " + pt.ApiProvider);
            }
            else
            {
                PaymentFacade.UpdateTransaction(paymentDTO.TransactionId, pt.OrderGuid, PaymentType.Creditcard, Convert.ToDecimal(paymentDTO.Amount), result.AuthenticationCode,
                    result.TransType, result.OrderDate, result.TransMessage + " " + result.CardNumber, status, rlt, result.APIProvider);

                if (paymentDTO.IsMasterPass)
                {
                    MasterpassCardInfoLog mpclog = ChannelFacade.GetMasterpassCardInfoLog(paymentDTO.MasterPassTransData.VerifierCode,
                        paymentDTO.MasterPassTransData.AuthToken);

                    if (mpclog.IsLoaded && mpclog.IsUsed)
                    {
                        MasterPass mpass = new MasterPass();
                        try
                        {
                            mpass.MasterPassTransLog(mpclog.TransId, mpclog.Indicator, paymentDTO.Amount, result.AuthenticationCode
                                , rlt, pt.OrderGuid ?? Guid.Empty, userId, mpclog.OauthToken, mpclog.CardBrandId
                                , mpclog.CardBrandName, mpclog.CardHolder, mpclog.CardNumber);
                        }
                        catch (Exception error)
                        {
                            string errormessage = string.Format("masterpass log error:{0},masterpasstransid:{1},authenticationcode:{2},resultcode:{3}orderguid:{4}"
                                , error.Message, mpclog.TransId, result.AuthenticationCode, rlt, (pt.OrderGuid ?? Guid.Empty));
                            mpass.MasterPassTransErrorLog((pt.OrderGuid ?? Guid.Empty), userId, errormessage);
                            logger.Error(errormessage);
                        }
                    }
                }
            }
        }

        public static bool CheckBranch(Guid bid, bool isItem, Guid storeId, int itemQuantity, bool noRestrictedStore, int? saleMultipleBase)
        {
            //商品檔，回傳true
            if (isItem)
            {
                return true;
            }

            //通用券檔次 無須檢查分店 return true
            if (noRestrictedStore)
            {
                return true;
            }

            //非商品檔，分店編號錯誤回傳false
            if (Guid.Empty == storeId)
            {
                return false;
            }
            //有分店編號，檢查是否還有數量
            PponStore pponStore = pp.PponStoreGet(bid, storeId);
            //查無分店設定，回傳false
            if (!pponStore.IsLoaded)
            {
                return false;
            }
            //分店設定有設定數量控制，檢查是否超過上限，若是則不允許購買
            itemQuantity = (saleMultipleBase ?? 0) > 0 ? saleMultipleBase.Value : itemQuantity;
            if ((pponStore.TotalQuantity != null) && (pponStore.TotalQuantity.Value < pponStore.OrderedQuantity + itemQuantity))
            {
                return false;
            }

            return true;
        }

        public static bool CheckOptions(string itemOptions)
        {
            if (itemOptions.IndexOf("請選擇") > -1 || itemOptions.IndexOf("已售完") > -1)
            {
                return false;
            }

            return true;
        }

        public static string CheckData(IViewPponDeal viewPponDeal, int quantity, int bonusCash, decimal scash, string userName, int userId, bool isDailyRestriction, bool isGroupCoupon)
        {
            //todo:在確認是否依實際訂購量，成套票券目前1次只能買1組
            quantity = isGroupCoupon ? viewPponDeal.SaleMultipleBase.Value : quantity;
            if (isGroupCoupon && cp.EnableGroupCouponNewMakeOrderApi)
            {
                var info = PponFacade.GetApiGroupCouponInfo(viewPponDeal);
                if (quantity > (info.MaxQuantity * viewPponDeal.SaleMultipleBase))
                {
                    logger.InfoFormat("{0} 購買 {1}. 參數 {2}, {3} #2.1",
                        userName,
                        viewPponDeal.BusinessHourGuid,
                        quantity,
                        (info.MaxQuantity * viewPponDeal.SaleMultipleBase));
                    return "每單訂購數量最多" + info.MaxQuantity * viewPponDeal.SaleMultipleBase + "份，請重新輸入數量";
                }
            }
            else if (quantity > (viewPponDeal.MaxItemCount ?? int.MaxValue))
            {
                logger.InfoFormat("{0} 購買 {1}. 參數 {2}, {3} #2.2",
                    userName,
                    viewPponDeal.BusinessHourGuid,
                    quantity,
                    (viewPponDeal.MaxItemCount ?? int.MaxValue));
                return "每單訂購數量最多" + viewPponDeal.MaxItemCount + "份，請重新輸入數量.";
            }

            int already = pp.ViewPponOrderDetailGetCountByUser(userName, viewPponDeal.BusinessHourGuid, isDailyRestriction) *
                          (isGroupCoupon ? viewPponDeal.SaleMultipleBase ?? 1 : 1);
            if (viewPponDeal.ItemDefaultDailyAmount != null)
            {
                if (quantity + already > viewPponDeal.ItemDefaultDailyAmount.Value)
                {
                    logger.InfoFormat("{0} 購買 {1}. 參數 {2}, {3}, {4} #2.3",
                        userName,
                        viewPponDeal.BusinessHourGuid,
                        quantity,
                        already,
                        viewPponDeal.ItemDefaultDailyAmount.Value);
                    return "每人訂購數量最多" + viewPponDeal.ItemDefaultDailyAmount + "份，您已訂購" + already + "份，請重新輸入數量";
                }
            }

            if (bonusCash > Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(userName) / 10))
            {
                return "您的可用紅利點數換算可抵用金額最多為" + Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(userName) / 10) + "元，請重新輸入金額";
            }

            if (bonusCash < 0)
            {
                return "紅利點數金額不可為負，請重新輸入金額";
            }

            decimal userScash;
            decimal userPscash;

            if (scash > OrderFacade.GetSCashSum2(userId, out userScash, out userPscash))
            {
                return "您的可用17購物金可抵用金額最多為" + OrderFacade.GetSCashSum(userName).ToString() + "元，請重新輸入金額";
            }

            return string.Empty;
        }

        /// <summary>
        /// 零元檔 Make Order
        /// </summary>
        /// <param name="paymentDTO"></param>
        /// <param name="userName"></param>
        /// <param name="apiUserDeviceType"></param>
        /// <param name="isPrizeDeal"></param>
        /// <param name="drawRecordId"></param>
        /// <param name="passPreventingOversell">是否跳過預防超賣檢查</param>
        /// <returns></returns>
        public static ZeroPayResult ZeroPayMakeOrder(PaymentDTO paymentDTO, string userName, 
            ApiUserDeviceType apiUserDeviceType = ApiUserDeviceType.WebService, bool isPrizeDeal = false, int drawRecordId = 0, 
            bool passPreventingOversell = false, bool couponSequenceBetaVersion = false)
        {
            int userId = MemberFacade.GetUniqueId(userName);
            if (int.Equals(0, userId))
            {
                return new ZeroPayResult
                {
                    ReturnCode = ApiReturnCode.BadUserInfo,
                    ResultCode = ApiResultCode.BadUserInfo,
                    Message = string.Format("user {0} not found", userName)
                };
            }

            #region Check Order

            ViewPponDeal theDeal = pp.ViewPponDealGetByBusinessHourGuid(paymentDTO.BusinessHourGuid);
            paymentDTO.DonationReceiptsType = DonationReceiptsType.None;

            #endregion Check Order

            #region CheckOut

            paymentDTO.TicketId = OrderFacade.MakeRegularTicketId(paymentDTO.SessionId, paymentDTO.BusinessHourGuid) + "_" + Path.GetRandomFileName().Replace(".", "");
            string strResult = CheckOrderData( new CheckOrderDataOption
            {
                CheckMaxOrderQty = true,
                IsDailyRestriction = theDeal.IsDailyRestriction.GetValueOrDefault()
            }, theDeal, userName);

            if (!string.IsNullOrWhiteSpace(strResult))
            {
                return new ZeroPayResult
                {
                    ReturnCode = ApiReturnCode.PaymentError,
                    ResultCode = ApiResultCode.PaymentCheckOrderFail,
                    Message = strResult
                };
            }

            #endregion CheckOut

            #region Make Order

            paymentDTO.TransactionId = PaymentFacade.GetTransactionId();

            #region New Version Order Start

            OrderFromType orderfromtype;
            switch (apiUserDeviceType)
            {
                case ApiUserDeviceType.WebService:
                    orderfromtype = OrderFromType.ByWebService;
                    break;

                case ApiUserDeviceType.Android:
                    orderfromtype = OrderFromType.ByAndroid;
                    break;

                case ApiUserDeviceType.IOS:
                    orderfromtype = OrderFromType.ByIOS;
                    break;

                case ApiUserDeviceType.WinPhone:
                    orderfromtype = OrderFromType.ByWinPhone;
                    break;

                default:
                    orderfromtype = OrderFromType.ByWebService;
                    break;
            }

            Guid storeGuid = paymentDTO.SelectedStoreGuid;
            if (storeGuid == Guid.Empty && !Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore))
            {
                ViewPponStoreCollection stores = pp.ViewPponStoreGetListByBidWithNoLock(theDeal.BusinessHourGuid, VbsRightFlag.VerifyShop);
                storeGuid = (stores.Count > 0) ? stores[0].StoreGuid : Guid.Empty;
            }

            // make order
            Order theOrder = OrderFacade.MakeZeroOrder(userName, paymentDTO.TicketId, theDeal.OrderGuid, theDeal, storeGuid, orderfromtype);

            paymentDTO.OrderGuid = theOrder.Guid;

            #endregion New Version Order Start

            #endregion Make Order

            #region 預防超賣

            if (passPreventingOversell)
            {
                logger.InfoFormat("zeropay PreventingOversell pass. {0}", theDeal.BusinessHourGuid);
            }

            int preventingId = 0;
            if (!passPreventingOversell && !PponFacade.CheckPreventingOversell(theDeal, paymentDTO.Quantity, userId, out preventingId))
            {
                logger.InfoFormat("zeropay PreventingOversell return false. bid={0}, preventingId={1}", theDeal.BusinessHourGuid, preventingId);
                return new ZeroPayResult
                {
                    ReturnCode = ApiReturnCode.OutofLimitOrderError,
                    ResultCode = ApiResultCode.PaymentCheckOrderFail,
                    Message = "很抱歉!已超過最大訂購數量"
                };
            }

            #endregion 預防超賣

            #region Make Payment

            if (theDeal.DealIsOrderable())
            {
                #region make cash trust log

                //if weeklypay deal
                int trust_checkout_type = 0;
                if (((theDeal.BusinessHourStatus) & (int)BusinessHourStatus.WeeklyPay) > 0)
                {
                    trust_checkout_type = (int)TrustCheckOutType.WeeklyPay;
                }

                OrderDetailCollection odCol = op.OrderDetailGetList(1, theDeal.MaxItemCount ?? 10, "", theOrder.Guid, OrderDetailTypes.Regular);
                Member usr = mp.MemberGet(userName);
                CashTrustInfo cti = new CashTrustInfo
                {
                    OrderGuid = theOrder.Guid,
                    OrderDetails = odCol,
                    CreditCardAmount = 0,
                    SCashAmount = 0,
                    PCashAmount = 0,
                    BCashAmount = 0,
                    Quantity = 1,
                    DeliveryCharge = 0,
                    DeliveryType = theDeal.DeliveryType == null ? DeliveryType.ToShop : (DeliveryType)theDeal.DeliveryType.Value,
                    DiscountAmount = 0,
                    AtmAmount = 0,
                    BusinessHourGuid = theDeal.BusinessHourGuid,
                    CheckoutType = trust_checkout_type,
                    TrustProvider = Helper.GetBusinessHourTrustProvider(theDeal.BusinessHourStatus),
                    ItemName = theDeal.ItemName,
                    ItemPrice = 0,
                    User = usr,
                    OrderClass = GetOrderClassification(theDeal)
                };

                CashTrustLogCollection ctCol = OrderFacade.MakeCashTrust(cti);

                #endregion make cash trust log

                #region 抓序號(宅配檔就不抓)

                Coupon coupon = new Coupon();

                if (!Enum.Equals(DeliveryType.ToHouse, theDeal.DeliveryType))
                {
                    #region Coupon

                    lock (CouponLock)
                    {
                        Guid odid = odCol[0].Guid;

                        if ((Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal) && theDeal.CouponCodeType == (int)CouponCodeType.Pincode) 
                            || (!Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal) && Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.PEZevent)))
                        {
                            Peztemp pez = null;
                            if (theDeal.PinType == (int)PinType.Single)
                            {
                                pez = OrderFacade.GetPezTemp(theDeal.BusinessHourGuid);
                            }
                            else
                            {
                                pez = OrderFacade.GetPezTemp(theDeal.BusinessHourGuid, userName,
                                    Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.PEZeventWithUserId));
                            }

                            if (null == pez)
                            {
                                #region 取不到序號則不成立訂單
                                paymentDTO.ResultPageType = PaymentResultPageType.ZeroPriceFailed;
                                //update 超賣資料過期
                                SetPreventingOversellExpired(preventingId, isPrizeDeal, drawRecordId, PrizeDrawRecordStatus.Fail, passPreventingOversell);
                                return new ZeroPayResult
                                {
                                    ReturnCode = ApiReturnCode.PaymentError,
                                    ResultCode = ApiResultCode.PaymentGetCouponFail,
                                    Message = "很抱歉，這個序號無法被取得！"
                                };
                                #endregion 取不到序號則不成立訂單
                            }
                            else
                            {
                                coupon.OrderDetailId = odid;
                                coupon.Code = coupon.SequenceNumber = pez.PezCode;
                                coupon.Id = Convert.ToInt32(OrderFacade.GetCouponID(coupon));
                                pez.OrderDetailGuid = odid;
                                pez.OrderGuid = theOrder.Guid;
                                pez.Status = (int)PeztempStatus.Using;

                                try
                                {
                                    using (TransactionScope ts2 = new TransactionScope())
                                    {
                                        OrderFacade.SetPezTemp(pez);
                                        foreach (CashTrustLog ctl in ctCol)
                                        {
                                            OrderFacade.SaveTrustCoupon(ctl, coupon.Id, coupon.SequenceNumber);
                                        }

                                        ts2.Complete();
                                    }
                                }
                                catch (Exception e)
                                {
                                    paymentDTO.ResultPageType = PaymentResultPageType.ZeroPriceFailed;
                                    OrderFacade.DeletePeztempCoupon(pez.PezCode, (Guid)pez.Bid);
                                    logger.Error("Something wrong while trying to fill coupon_id in cash_trust_log", e);
                                    //update 超賣資料過期
                                    SetPreventingOversellExpired(preventingId, isPrizeDeal, drawRecordId, PrizeDrawRecordStatus.Fail, passPreventingOversell);
                                    return new ZeroPayResult
                                    {
                                        ReturnCode = ApiReturnCode.PaymentError,
                                        ResultCode = ApiResultCode.PaymentGetCouponFail,
                                        Message = "很抱歉，這個序號無法被取得！"
                                    };
                                }
                            }
                        }
                        else
                        {
                            List<Coupon> coupons = CouponFacade.GenCouponWithoutSms(theOrder, theDeal, couponSequenceBetaVersion);

                            #region 取不到序號則不成立訂單

                            if (int.Equals(0, coupons.Count))
                            {
                                //update 超賣資料過期
                                SetPreventingOversellExpired(preventingId, isPrizeDeal, drawRecordId, PrizeDrawRecordStatus.Fail, passPreventingOversell);
                                paymentDTO.ResultPageType = PaymentResultPageType.ZeroPriceFailed;
                                return new ZeroPayResult
                                {
                                    ReturnCode = ApiReturnCode.PaymentError,
                                    ResultCode = ApiResultCode.PaymentGetCouponFail,
                                    Message = "很抱歉，這個序號無法被取得！"
                                };
                            }
                            else
                            {
                                coupon = coupons[0];
                            }

                            #endregion 取不到序號則不成立訂單
                        }
                    }

                    #endregion Coupon
                }

                #endregion 抓序號(宅配檔就不抓)

                #region make payment transaction

                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department), PayTransPhase.Successful);
                PaymentTransaction ptCreditcard = PaymentFacade.NewTransaction(paymentDTO.TransactionId, theOrder.Guid,
                    PaymentType.Creditcard, 0, null, PayTransType.Authorization, DateTime.Now, userName, "0 元檔",
                    status, PayTransResponseType.OK);

                #endregion make payment transaction

                paymentDTO.ResultPageType = PaymentResultPageType.ZeroPriceSuccess;

                //SKM 跳過在此累加 SalesInfo，改由完整流程(扣點/開完發票)成功時才累加
                bool isSkmDeal = (theDeal.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0;
                SalesInfoArg sia = isSkmDeal
                    ? null 
                    : new SalesInfoArg
                    {
                        BusinessHourGuid = theDeal.BusinessHourGuid,
                        Quantity = odCol.OrderedQuantity,
                        Total = odCol.OrderedTotal
                    };
                OrderFacade.SetOrderStatus(theOrder.Guid, userName, OrderStatus.Complete, true, sia);

                //update 超賣資料過期
                SetPreventingOversellExpired(preventingId, isPrizeDeal, drawRecordId, PrizeDrawRecordStatus.PrizeDrawing, passPreventingOversell);
                // set deal close time
                OrderFacade.PponSetCloseTimeIfJustReachMinimum(theDeal, 1);

                //寄送完成通知
                OrderFacade.SendZeroDealSuccessMail(theOrder.MemberName, userName, theDeal, pp.ViewPponCouponGet(coupon.Id), (GroupOrderStatus)theDeal.GroupOrderStatus, (CouponCodeType)theDeal.CouponCodeType.Value);

                //零元檔次的回覆
                ApiFreeDealPayReply payReply = new ApiFreeDealPayReply();
                payReply.CouponId = coupon.Id;
                payReply.CouponCode = coupon.Code;
                payReply.SequenceNumber = coupon.SequenceNumber;
                payReply.OrderGuid = theOrder.Guid;
                payReply.PromoImage = ImageFacade.GetMediaPathsFromRawData(theDeal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 3).DefaultIfEmpty(cp.SiteUrl + "/Themes/default/images/17Life/G3/0act.jpg").First();

                if (!string.IsNullOrEmpty(theDeal.ActivityUrl))
                {
                    payReply.PromoUrl = System.Net.WebUtility.UrlDecode(theDeal.ActivityUrl.Trim());
                }

                payReply.IsFami = (theDeal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0;
                payReply.IsCouponDownload = (theDeal.GroupOrderStatus & (int)GroupOrderStatus.PEZeventCouponDownload) > 0;
                payReply.IsSkm = (theDeal.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0;
                payReply.CouponType = PponFacade.GetDealCouponType(theDeal);
                if (payReply.CouponType == DealCouponType.ThreeStageBarcode)
                {
                    //憑證類型為三段式條碼，產生條碼資料

                    FamiposBarcode famipos = OrderFacade.FamiposBarcodeGetCreate(coupon.OrderDetailId, theDeal.BusinessHourGuid, theDeal);
                    payReply.ThreeStageCode.Barcode1 = famipos.Barcode1;
                    payReply.ThreeStageCode.Barcode2 = famipos.Barcode2;
                    payReply.ThreeStageCode.Barcode3 = famipos.Barcode3;
                }

                if (payReply.CouponType == DealCouponType.SkmQrCode && ctCol.Any())
                {
                    var ctl = ctCol.First();

                    DateTime expireTime = DateTime.Now.AddMinutes(cp.SkmQrCodeAppExpireTime).AddSeconds(1);
                    var externalDeal = pp.ViewExternalDealGetByBid(theDeal.BusinessHourGuid);
                    ISkmQrcodeBase skmQr = new SkmQrcodeFatory().CreateFatory(new SkmQrcodeDataModel(ctl.TrustId, theDeal) {});
                    
                    //燒點檔次跟電子抵用金/電子贈品禮券不出示QRCode
                    if (cp.IsEnableCashAndGiftCouponOnStore)
                    {
                        if (skmQr == null
                            && (externalDeal.IsPrizeDeal
                                || externalDeal.ActiveType == (int)ActiveType.CashCoupon
                                || externalDeal.ActiveType == (int)ActiveType.GiftCertificate
                            ))
                        {
                            payReply.SkmQrCode = string.Empty;
                        }
                        else
                        {
                            payReply.SkmQrCode = skmQr.GetSkmQrCodeString();
                        }
                    }
                    else
                    {
                        payReply.SkmQrCode = skmQr == null && externalDeal.IsPrizeDeal ? string.Empty : skmQr.GetSkmQrCodeString();
                    }

                    payReply.SkmExpireTime = expireTime.ToString("yyyy/MM/dd HH:mm:ss");
                    payReply.ProductCode = (externalDeal.ProductCode == null) ? string.Empty : (externalDeal.ProductCode.ToLower() == "undefined" ? string.Empty : externalDeal.ProductCode);
                    payReply.SpecialItemNo = (externalDeal.SpecialItemNo == null) ? string.Empty : (externalDeal.SpecialItemNo.ToLower() == "undefined" ? string.Empty : externalDeal.SpecialItemNo);

                    payReply.SkmAvailabilities = SkmCacheFacade.GetSkmPponStoreListFromCache(theDeal.BusinessHourGuid);
                }

                return new ZeroPayResult
                {
                    ReturnCode = ApiReturnCode.Success,
                    ResultCode = ApiResultCode.Success,
                    Message = string.Empty,
                    Data = payReply,
                    TrustIds = ctCol.Select(item=>item.TrustId)
                };
            }
            else
            {
                CancelPaymentTransactionByTransId(paymentDTO.TransactionId, "sys", "Unorderable. ");
                paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;
                SetPreventingOversellExpired(preventingId, isPrizeDeal, drawRecordId, PrizeDrawRecordStatus.Fail, passPreventingOversell);
                return new ZeroPayResult
                {
                    ReturnCode = ApiReturnCode.PaymentError,
                    ResultCode = ApiResultCode.PaymentCanNotOrderDeal,
                    Message = "交易失敗！"
                };
            }

            #endregion Make Payment
        }

        private static bool SetPreventingOversellExpired(int pid,bool isPrizeDeal, int drawRecordId, PrizeDrawRecordStatus prizeDrawStatus, bool passPreventingOversell)
        {
            if (!passPreventingOversell && pid > 0)
            {
                op.SetPreventingOversellExpired(pid);
            }
            
            PponFacade.SetPrizeDrawRecord(isPrizeDeal, drawRecordId, prizeDrawStatus);
            return true;
        }

        /// <summary>
        /// Get Order Classification by ViewPponDeal.GroupOrderStatus
        /// </summary>
        /// <param name="vpd"></param>
        /// <returns></returns>
        public static OrderClassification GetOrderClassification(IViewPponDeal vpd)
        {
            if ((vpd.GroupOrderStatus & (int) GroupOrderStatus.SKMDeal) > 0)
            {
                return OrderClassification.Skm;
            }

            return OrderClassification.LkSite;
        }

        public static string CheckOrderData(CheckOrderDataOption option, ViewPponDeal vpd, string userName)
        {
            #region Standard Check

            if (option.Quantity > (vpd.MaxItemCount ?? int.MaxValue))
            {
                logger.InfoFormat("{0} 購買 {1} 每單訂購數量最多, 參數 {2},{3} #4",
                    userName,
                    vpd.BusinessHourGuid,
                    option.Quantity,
                    vpd.MaxItemCount);
                return "每單訂購數量最多" + vpd.MaxItemCount + "份，請重新輸入數量。";
            }

            if (PponFacade.CheckExceedOrderedQuantityLimit(vpd, option.Quantity))
            {
                return "很抱歉!已超過最大訂購數量";
            }

            if (DateTime.Now > vpd.BusinessHourOrderTimeE)
            {
                return "很抱歉~此好康已結束";
            }

            if (DateTime.Now < vpd.BusinessHourOrderTimeS)
            {
                return "很抱歉~此好康尚未開始";
            }

            #endregion

            #region Check MaxOrderQty

            if (option.CheckMaxOrderQty)
            {
                int already = pp.ViewPponOrderDetailGetCountByUser(userName, vpd.BusinessHourGuid, option.IsDailyRestriction);
                if (vpd.ItemDefaultDailyAmount != null)
                {
                    if (option.Quantity + already > vpd.ItemDefaultDailyAmount.Value)
                    {
                        logger.InfoFormat("{0} 購買 {1}. 參數 {2}, {3}, {4} #5",
                            userName,
                            vpd.BusinessHourGuid,
                            option.Quantity,
                            already,
                            vpd.ItemDefaultDailyAmount.Value);
                        return "每人訂購數量最多" + vpd.ItemDefaultDailyAmount + "份，您已訂購" + already + "份，請重新輸入數量.";
                    }
                }
            }

            #endregion

            #region Check BounsCash

            if (option.CheckBonusCash)
            {
                if (option.BonusCash > Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(userName) / 10))
                {
                    return "您的可用紅利點數換算可抵用金額最多為" + Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(userName) / 10) + "元，請重新輸入金額";
                }

                if (option.BonusCash < 0)
                {
                    return "紅利點數金額不可為負，請重新輸入金額";
                }
            }

            #endregion Check BounsCash

            #region CheckSCash

            if (option.CheckSCash)
            {
                if (option.SCash > OrderFacade.GetSCashSum(userName))
                {
                    return "您的可用17購物金可抵用金額最多為" + OrderFacade.GetSCashSum(userName) + "元，請重新輸入金額";
                }
            }

            #endregion BounsCash

            #region Check DiscountCode

            if (option.CheckDiscountCode && !string.IsNullOrWhiteSpace(option.DiscountCode))
            {
                int minimumAmount;
                decimal amount = vpd.ItemPrice * option.Quantity + option.Freight;
                var discountCodeStatus = PromotionFacade.GetDiscountCodeStatus(option.DiscountCode, DiscountCampaignUsedFlags.Ppon, amount, userName, out minimumAmount, vpd.BusinessHourGuid.ToString());
                if (discountCodeStatus == DiscountCodeStatus.None)
                {
                    return I18N.Phrase.DiscountCodeNone;
                }
                else if (discountCodeStatus == DiscountCodeStatus.UpToQuantity)
                {
                    return I18N.Phrase.DiscountCodeUptoQuantity;
                }
                else if (discountCodeStatus == DiscountCodeStatus.IsUsed)
                {
                    return I18N.Phrase.DiscountCodeIsUsed;
                }
                else if (discountCodeStatus == DiscountCodeStatus.Disabled)
                {
                    return I18N.Phrase.DiscountCodeDisabled;
                }
                else if (discountCodeStatus == DiscountCodeStatus.OverTime)
                {
                    return I18N.Phrase.DiscountCodeOverTime;
                }
                else if (discountCodeStatus == DiscountCodeStatus.Restricted)
                {
                    return string.Format(I18N.Phrase.DiscountCodeRestricted, "品生活");
                }
                else if (discountCodeStatus == DiscountCodeStatus.UnderMinimumAmount)
                {
                    return string.Format(I18N.Phrase.DiscountCodeUnderMinimumAmount, minimumAmount);
                }
                else if (discountCodeStatus == DiscountCodeStatus.DiscountCodeCategoryWrong)
                {
                    return I18N.Phrase.DiscountCodeCategoryWrong;
                }
                else if (discountCodeStatus == DiscountCodeStatus.OwnerUseOnlyWrong)
                {
                    return I18N.Phrase.DiscountCodeOwnerUseOnlyWrong;
                }
                else if (discountCodeStatus == DiscountCodeStatus.CategoryUseLimit)
                {
                    return string.Format("僅限" + PromotionFacade.GetDiscountCategoryString(option.DiscountCode) + "使用");
                }
                else if (discountCodeStatus != DiscountCodeStatus.CanUse)
                {
                    return I18N.Phrase.DiscountCodeCanNotUse;
                }
            }

            #endregion Check DiscountCode

            return string.Empty;
        }

        #endregion

        /// <summary>
        /// 測試卡號不給予加入黑名單功能
        /// </summary>
        /// <param name="creditcardInfo"></param>
        /// <returns></returns>
        public static bool CompareDefaultTestCreditcard(string creditcardInfo)
        {
            string testCardID = cp.DefaultTestCreditcard;
            if (creditcardInfo == testCardID.Replace("-", ""))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CheckUsingMasterPass(IViewPponDeal theDeal)
        {
            if ((theDeal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)
            {
                return false;
            }

            if ((theDeal.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0)
            {
                return false;
            }

            if (decimal.Equals(0, theDeal.ItemPrice))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 產生照會訊息
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static bool IsCreditcardRefer(Guid orderGuid)
        {
            Order o = op.OrderGet(orderGuid);
            GroupOrder gpo = op.GroupOrderGetByOrderGuid(o.ParentOrderId.GetValueOrDefault());
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(gpo.BusinessHourGuid);
            if (deal != null && deal.DeliveryType == (int)DeliveryType.ToHouse)
            {
                OrderUserMemoList userMemo = new OrderUserMemoList();
                userMemo.OrderGuid = orderGuid;
                userMemo.DealType = (int)VbsDealType.Ppon;
                userMemo.UserMemo = "請勿出貨。因訂單金額過高，為防範不肖人士盜刷，17Life將進行照會，待照會完畢會通知您進行出貨。";
                userMemo.CreateId = "sys";
                userMemo.CreateTime = DateTime.Now;
                op.OrderUserMemoListSet(userMemo);

                return true;
            }
            return false;
        }

        /// <summary>
        /// 設定為需照會
        /// </summary>
        /// <param name="orderInfo"></param>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static CreditcardOrder SetCreditcardRefer(CreditcardOrder orderInfo, Guid orderGuid)
        {
            orderInfo.IsReference = true;
            orderInfo.ReferenceId = "sys";
            orderInfo.ReferenceTime = DateTime.Now;

            //若要照會得先清除最後出貨日(因順序問題,在這邊做後續清空)
            OrderProductDelivery opd = op.OrderProductDeliveryGetByOrderGuid(orderGuid);
            if (opd.IsLoaded && opd.LastShipDate != null)
            {
                opd.LastShipDate = null;
            }
            else
            {
                logger.Info(orderGuid.ToString() + "最後出貨日沒清到");
            }
            op.OrderProductDeliverySet(opd);

            return orderInfo;
        }

        /// <summary>
        /// 憑證訂單風險名單通知信
        /// </summary>
        /// <param name="createTime"></param>
        /// <param name="createId"></param>
        /// <param name="userId"></param>
        /// <param name="orderId"></param>
        /// <param name="itemName"></param>
        /// <param name="consumerIp"></param>
        public static void PponCreditcardFraud(DateTime createTime, string createId, int userId, string orderId, string itemName, string consumerIp)
        {
            var mailTo = cp.PponCreditcardFraudEmail.Split(";").ToList();


            var sb = new StringBuilder();
            sb.Append("<table style='width:740; border: 1px solid #808080' align='center'>");
            sb.Append("<tr style='border: 1px solid #808080'>");
            sb.Append("<td style='border: 1px solid #808080'>交易日期</td>");
            sb.Append("<td style='border: 1px solid #808080'>帳號</td>");
            sb.Append("<td style='border: 1px solid #808080'>帳號ID</td>");
            sb.Append("<td style='border: 1px solid #808080'>訂單編號</td>");
            sb.Append("<td style='border: 1px solid #808080'>訂單名稱</td>");
            sb.Append("<td style='border: 1px solid #808080'>IP位置</td>");
            sb.Append("<td style='border: 1px solid #808080'>消費方式</td>");
            sb.Append("</tr>");

            sb.Append("<tr style='border: 1px solid #808080'>");
            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", createTime.ToString()));
            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", createId));
            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", userId.ToString()));
            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", orderId));
            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", itemName));
            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", consumerIp));
            sb.Append(string.Format("<td style='border: 1px solid #808080'>{0}</td>", "信用卡"));
            //sb.Append(string.Format("<td style='border: 1px solid #808080'><a href='{0}'>{1}</a></td>", orderHref, item.OrderId));
            sb.Append("</tr>");

            sb.Append("</table>");


            #region Send mail

            var template = TemplateFactory.Instance().GetTemplate<NotifyMailTo17>();
            template.MailContent = sb.ToString();

            try
            {
                MailMessage msg = new MailMessage();
                msg.Subject = "憑證訂單風險名單通知信";
                msg.From = new MailAddress(cp.SystemEmail);
                foreach (string to in mailTo)
                {
                    msg.To.Add(to);
                }
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            #endregion
        }


        /// <summary>
        /// 是否為風險IP/地址/訂單
        /// </summary>
        /// <param name="type"></param>
        /// <param name="ptype"></param>
        /// <param name="ip"></param>
        /// <param name="address"></param>
        /// <param name="userId"></param>
        /// <param name="sellerGuid"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool IsSuspectCreditcardFraud(DeliveryType type, ProductDeliveryType ptype, string ip, string address, int userId, Guid sellerGuid, out CreditcardResult result)
        {
            //比對風險賣家
            string creditcardFraudSeller = cp.CreditcardFraudSeller;
            if (creditcardFraudSeller.Contains(sellerGuid.ToString()))
            {
                //首購賣家檔次
                int orderSellerCount = op.OrderGetCount(OrderStatus.Complete, OrderStatus.Complete, Order.Columns.UserId + "=" + userId, Order.Columns.SellerGuid + "=" + sellerGuid);
                if (orderSellerCount <= 0)
                {
                    //3個月前無成功訂單
                    int orderCount = op.OrderGetCount(OrderStatus.Complete, OrderStatus.Complete, Order.Columns.UserId + "=" + userId, Order.Columns.CreateTime + "<" + DateTime.Now.AddMonths(-3));
                    if (orderCount <= 0)
                    {
                        result = CreditcardResult.CreditcardFraudOrder;
                        return true;
                    }
                }
                
                
            }


            //比對風險帳號
            CreditcardFraudDatum cfd = op.CreditcardFraudDataGet(false, "", "", "", userId, ((int)CreditcardFraudDataType.Account).ToString());
            if (cfd.IsLoaded)
            {
                result = CreditcardResult.CreditcardFraudAccount;
                return true;
            }


            //比對風險IP
            cfd = op.CreditcardFraudDataGet(false, ip, "", "", null, ((int)CreditcardFraudDataType.CustomerCare).ToString() + "," + ((int)CreditcardFraudDataType.IP).ToString());
            if (cfd.IsLoaded)
            {
                result = CreditcardResult.CreditcardFraudIp;
                return true;
            }

            //比對風險地址
            if (type == DeliveryType.ToHouse && ptype == ProductDeliveryType.Normal)
            {
                address = Helper.AddrConvertRegular(address);
                cfd = op.CreditcardFraudDataGet(false, "", address, "", null, ((int)CreditcardFraudDataType.CustomerCare).ToString());
                if (cfd.IsLoaded)
                {
                    result = CreditcardResult.CreditcardFraudAddress;
                    return true;
                }
                else
                {
                    result = CreditcardResult.Approve;//隨便給一個,反正不會用到
                    return false;
                }
            }
            else
            {
                //憑證及超取不須比對地址
                result = CreditcardResult.Approve;//隨便給一個,反正不會用到
                return false;
            }

            //string a = sec.Decrypt(orderInfo.CreditcardInfo);//解密
            //string b = sec.Encrypt(orderInfo.CreditcardInfo);//加密
        }

        /// <summary>
        /// 是否走OTP
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static bool IsOTP(int userId, string cardNumber, int dealAccBusinessGroupId, int rootDealType, int amount, Guid bid, Guid orderGuid)
        {
            if (!cp.EnableOTP || (!cp.EnabledTaiShinPaymnetGateway && !cp.EnabledCathayPaymnetGateway))
            {
                //OTP開關
                return false;
            }
            else if (!string.IsNullOrEmpty(cp.EnableOTPUser) && !cp.EnableOTPUser.Contains(userId.ToString()))
            {
                //不再限定人員內
                return false;
            }
            else if (string.IsNullOrEmpty(cp.EnableOTPUser) || (!string.IsNullOrEmpty(cp.EnableOTPUser) && cp.EnableOTPUser.Contains(userId.ToString())))
            {
                //不在系統裡視為國外卡,一律OTP
                CreditcardBankInfo bank = op.CreditcardBankInfoGetByBin(cardNumber.Substring(0, 6));
                if (bank == null || !bank.IsLoaded)
                {
                    return true;
                }

                //設定的檔次強制OTP
                if (cp.ForceOTPDeal.Contains(bid.ToString()))
                {
                    return true;
                }

                //旅遊+2000以上
                if (dealAccBusinessGroupId == 5 && amount > 2000)
                {
                    return true;
                }

                //需要強制OTP的分類
                List<DealTypeNode> ForceOTPDealTypeNode = DealTypeFacade.GetDealTypeNodes(true).Where(x => x.CodeName.Contains("3C") || x.CodeName.Contains("家電")).ToList();
                List<int> ForceOTPDealType = ForceOTPDealTypeNode.Select(x => x.CodeId).ToList();
                if (ForceOTPDealType.Contains(rootDealType))
                {
                    //3C家電+10000以上
                    if (amount>10000)
                    {
                        return true;
                    }

                    //1小時內有3C家電歷史訂單
                    List<int> recentDealTypes = pp.RecentOrderDealTypeGetByUserId(userId, orderGuid);
                    if (recentDealTypes.Any())
                    {
                        foreach (int dt in recentDealTypes)
                        {
                            int historyRootDealType = DealTypeFacade.GetRootNode((int)dt);
                            if (ForceOTPDealType.Contains(historyRootDealType))
                            {
                                return true;
                            }

                        }
                    }
                }

                //白名單不需要
                CreditcardOtpWhitelist cow = op.CreditcardOtpWhitelistGet(userId);
                if (cow.IsLoaded && cow.Status)
                {
                    return false;
                }

                //全面開放或是限定人員
                Security sec = new Security(SymmetricCryptoServiceProvider.AES);
                string creditcardInfo = sec.Encrypt(cardNumber);

                CreditcardOtp otp = op.CreditcardOtpGetByTime(userId, creditcardInfo);
                if (otp.IsLoaded)
                {
                    //3個月內有驗證過
                    return false;
                }
                else
                {
                    //未驗證或已超過3個月
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 註銷白名單
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pt"></param>
        public static void DeleteWhiteList(int userId, int type, int result)
        {
            if (type == (int)PaymentType.Creditcard && (result == (int)CreditcardResult.PickupCard || result == (int)CreditcardResult.LossCard || result == (int)CreditcardResult.StolenCard || result == (int)CreditcardResult.IsBlacklist))
            {
                CreditcardOtpWhitelist cow = op.CreditcardOtpWhitelistGet(userId);
                if (cow.IsLoaded)
                {
                    cow.Status = false;
                    cow.ModifyTime = DateTime.Now;
                    op.CreditcardOtpWhitelistSet(cow);
                } 
            }
        }

        public static PaymentDTO GetPaymentDTOFromSession(string ticketId)
        {
            var ts = pp.TempSessionGet(ticketId, _PAYMENT_DTO_KEY);
            if (ts == null || string.IsNullOrEmpty(ts.ValueX))
            {
                return null;
            }
            try
            {
                return serializer.Deserialize<PaymentDTO>(ts.ValueX);
            }
            catch (Exception ex)
            {
                logger.Warn("GetPaymentDTOFromSession error, content=" + ts.ValueX, ex);
                try
                {
                    pp.TempSessionDelete(ts.Id);
                }
                catch
                {
                    // ignored
                }
            }
            return null;
        }

        public static void SetPaymentDTOToSession(string ticketId, PaymentDTO paymentDTO)
        {
            var ts = pp.TempSessionGet(ticketId, _PAYMENT_DTO_KEY);
            if (ts == null)
            {
                ts = new TempSession();
                ts.SessionId = ticketId;
                ts.Name = _PAYMENT_DTO_KEY;
            }
            ts.ValueX = serializer.Serialize(paymentDTO);
            pp.TempSessionSet(ts);
        }

        public static void PushRecardOrder(PaymentDTO paymentDTO, int memberId, PayResult result)
        {
            try
            {
                if (result.ReturnCode == ApiReturnCode.Success && cp.PushOrderCpaWithInHour > 0 && !string.IsNullOrEmpty(paymentDTO.DeviceId))
                {
                    Guid orderGuid;

                    if (Guid.TryParse(result.Data.GetPropertyValue("OrderGuid").ToString(), out orderGuid))
                    {
                        int? deviceId = lp.DeviceIdentyfierInfoGetCurrentDeviceId(paymentDTO.DeviceId);
                        if (deviceId == null)
                        {
                            logger.WarnFormat("PushRecardOrder 找不到 DeviceIdentyfierInfo, deviceId={0}.", paymentDTO.DeviceId);
                            return;
                        }
                        List<DevicePushRecordReadModel> pushData =
                            lp.DevicePushRecordGetReadRecordWithInHour(memberId, deviceId.Value,
                                cp.PushOrderCpaWithInHour);
                        if (pushData.Any())
                        {
                            var first = pushData.First();
                            var last = pushData.Last();
                            var pushOrder = new DevicePushOrder
                            {
                                OrderGuid = orderGuid,
                                FirstPushRecordId = first.RecordId,
                                FirstActionId = first.ActionId,
                                FirstPushTime = first.CreateTime,
                                FirstReadTime = first.ReadTime,
                                FirstPushAppId = first.PushAppId,
                                LastPushRecordId = last.RecordId,
                                LastActionId = last.ActionId,
                                LastPushTime = last.CreateTime,
                                LastReadTime = last.ReadTime,
                                LastPushAppId = last.PushAppId,
                                IsAnalyzed = false
                            };
                            lp.DevicePushOrderSet(pushOrder);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("PushRecardOrder error.", ex);
            }
        }

        public static ApiReturnObject OTPMakePayment(OTPMakePaymentModel input)
        {
            string methodName = "LunchKingSite.WebApi.PayService.OTPMakePayment";
            var result = new PayResult
            {
                ReturnCode = ApiReturnCode.Success,
                ResultCode = ApiResultCode.Success,
                Message = string.Empty
            };
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            PaymentDTO paymentDTO = input.paymentDTO;
            string userName = MemberFacade.GetUserName(input.userId);
            if (paymentDTO != null)
            {
                //已跑過OTP
                paymentDTO.IsFinishOTP = true;
                //回去跑MakePayment
                result = PaymentFacade.MakePayment(paymentDTO, userName);

                rtnObject.Code = result.ReturnCode;
                rtnObject.Message = result.Message;
                rtnObject.Data = result.Data;

                //及剩下要完成的流程
                PaymentFacade.PushRecardOrder(paymentDTO, input.userId, result);
                paymentDTO.CreditcardNumber = Helper.MaskCreditCard(paymentDTO.CreditcardNumber);
                paymentDTO.CreditcardSecurityCode = Helper.MaskCreditCardSecurityCode(paymentDTO.CreditcardSecurityCode);
                SystemFacade.SetApiLog(methodName, input.apiUserId, paymentDTO, null);
            }
            else
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = "未定義錯誤";
                rtnObject.Data = null;
            }
            

            return rtnObject;
        }

        public static void OTPApiLog(string transId,string content)
        {
            OtpLog otpLog = new OtpLog
            {
                TransId = transId,
                Content = content,
                CreateTime = DateTime.Now,
                OrderFromType = (int)Helper.GetOrderFromType(),
                ServerName = Environment.MachineName
            };
            op.OtpLogSet(otpLog);
        }
    }
}
﻿using Amazon.DataPipeline.Model;
using EnterpriseDT.Net.Ftp;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using log4net;
using LunchKingSite.BizLogic.Models.EInvoice;

namespace LunchKingSite.BizLogic.Facade
{
    public class EinvoiceFacade
    {
        protected static IOrderProvider op;
        protected static IPponProvider pp;
        protected static IMemberProvider mp;
        protected static IHiDealProvider hp;
        protected static IAccountingProvider ap;
        protected static ISysConfProvider config;
        protected static ILog logger;
        private static bool ShowContactInfo
        {
            get
            {
                return DateTime.Now < config.ContactToPayeasy || DateTime.Now >= config.PayeasyToContact;
            }
        }

        private static string Com_Name
        {
            get
            {
                return !ShowContactInfo ? "康迅數位整合股份有限公司" : "康太數位整合股份有限公司";
            }
        }

        private static string Com_Id
        {
            get
            {
                return !ShowContactInfo ? config.PayeasyCompanyId : config.ContactCompanyId;
            }
        }

        const string CarrierTypeCode_Member = "EG0071";
        const string CarrierTypeCode_Phone = "3J0002";
        const string CarrierTypeCode_PersonalCertificate = "CQ0001";
        const string CarrierTypeCode_CreditCard = "EK0002";

        private static Random r = new Random();

        static EinvoiceFacade()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            config = ProviderFactory.Instance().GetConfig();
            logger = LogManager.GetLogger(typeof(EinvoiceFacade));
        }

        #region 新增品生活發票主檔(By Order)

        /// <summary>
        /// 品生活-電子發票載具(自2014/3/1起)
        /// </summary>
        /// <param name="orderGuid">訂單GUID</param>
        /// <param name="orderId">訂單編號</param>
        /// <param name="orderTime">建單日期</param>
        /// <param name="deliverinfo"></param>
        /// <param name="trans_id">付款的transId</param>
        /// <param name="itemName">商品名稱</param>
        /// <param name="order_amount">發票開立金額</param>
        /// <param name="noTax">稅率</param>
        /// <param name="userId">訂單會員編號</param>
        /// <param name="creator">資料建立人</param>
        /// <param name="isPponItem">品名為購物金(false)或商品名稱(true)</param>
        /// <param name="invoiceStatus">發票狀態 預設值為0</param>
        /// <param name="couponId">CouponId(憑證only)</param>
        /// <param name="orderClassification">訂單類型</param>
        public static void SetEinvoiceMain(Guid orderGuid, string orderId, DateTime orderTime, HiDealDeliveryInfo deliverinfo, string trans_id
            , string itemName, decimal order_amount, bool noTax, int userId, string creator, bool isPponItem, string sellerName, OrderClassification orderClassification
            , int? couponId, int invoiceStatus = (int)EinvoiceType.Initial)
        {
            EinvoiceMain main = new EinvoiceMain();

            main.InvoicePass = ReturnInvoicePass(); //回傳個人識別碼(4碼亂數)

            #region 發票載具 (自 2014/03/01 起加入發票載具及愛心碼資訊)

            // set InvoiceMode2, LoveCode, CarrierType & CarrierId
            main.Version = (int)InvoiceVersion.CarrierEra;
            main.InvoiceMode2 = (int)deliverinfo.InvoiceMode;
            main.CarrierType = (int)deliverinfo.CarrierType;
            if (deliverinfo.InvoiceMode == InvoiceMode2.Duplicate)
            {
                if (deliverinfo.CarrierType != CarrierType.None)
                {
                    main.CarrierId = deliverinfo.CarrierId;
                }
                else
                {
                    if (!string.IsNullOrEmpty(deliverinfo.LoveCode))
                    {
                        main.LoveCode = deliverinfo.LoveCode.Trim();
                    }
                    else
                    {
                        main.InvoiceBuyerName = deliverinfo.InvBuyerName;   //買受人姓名
                        main.InvoiceBuyerAddress = deliverinfo.InvAddress; //買受人地址
                    }
                }
            }
            else
            {
                main.InvoiceComId = deliverinfo.InvComId;    //三聯發票統編
                main.InvoiceComName = deliverinfo.InvComName;        //三聯發票抬頭
                main.InvoiceBuyerName = deliverinfo.InvBuyerName; //買受人姓名
                main.InvoiceBuyerAddress = deliverinfo.InvAddress; //買受人地址
            }

            // set Old EinvoiceMode default value
            //if (deliverinfo.InvoiceMode == InvoiceMode2.Triplicate)
            //{
            //    main.InvoiceMode = (int)EinvoiceMode.PaperInvoice3;
            //}
            //else
            //{
            main.InvoiceMode = 0; //not used
            //}

            #endregion

            main.TransId = trans_id;    //payment_transaction
            main.OrderTime = orderTime;  //訂單時間
            main.OrderId = orderId;   //OrderID
            main.OrderGuid = orderGuid;    //OrderGuid
            main.OrderItem = deliverinfo.Count + "X " + sellerName + "- " + (Regex.IsMatch(itemName, "【.*?】", RegexOptions.IgnoreCase) ? Regex.Match(itemName, "【.*?】", RegexOptions.IgnoreCase).ToString() : MaxLengthString(itemName, 150));    //份數和好康名稱
            main.OrderAmount = order_amount;    //刷卡金額
            main.OrderIsPponitem = isPponItem;
            main.InvoiceSumAmount = 0;              //加總金額(開立減折讓)
            main.InvoiceTax = noTax ? 0 : 0.05m;    //稅率(農產零稅，其他5%)
            main.UserId = userId;   //會員帳號
            if (main.InvoiceMode2 == (int)InvoiceMode2.Triplicate || (main.InvoiceMode2 == (int)InvoiceMode2.Duplicate && main.CarrierType == (int)CarrierType.None))
            {
                main.InvoiceRequestTime = orderTime; //紙本發票和三聯加註申請時間
            }
            // 紀錄可使用的折讓單類型，預設二聯=電子折讓；三聯=紙本
            if (main.InvoiceMode2 == (int)InvoiceMode2.Duplicate)
            {
                main.AllowanceStatus = (int)AllowanceStatus.ElecAllowance;
            }
            else
            {
                main.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
            }
            main.Creator = creator; //建檔人
            main.Message = "系統新增";  //訊息
            main.OrderClassification = (int)orderClassification;
            main.InvoiceStatus = invoiceStatus;
            main.CouponId = couponId;
            if (isPponItem || main.InvoiceMode2 == (int)InvoiceMode2.Triplicate)
            {
                main.VerifiedTime = orderTime;
            }
            SetEinvoiceMainWithLog(main, creator);
        }

        /// <summary>
        /// 建立發票
        /// </summary>
        /// <param name="orderGuid">訂單GUID</param>
        /// <param name="orderId">訂單編號</param>
        /// <param name="orderTime">建單日期</param>
        /// <param name="invoiceComId">三聯發票統編</param>
        /// <param name="invoiceComName">三聯發票抬頭</param>
        /// <param name="invoiceBuyerName">買受人姓名</param>
        /// <param name="invoiceBuyerAddress">買受人地址</param>
        /// <param name="einvoiceMode">發票類型</param>
        /// <param name="trans_id">付款的transId</param>
        /// <param name="itemQuantity">商品數量</param>
        /// <param name="itemName">商品名稱</param>
        /// <param name="order_amount">發票開立金額</param>
        /// <param name="noTax">稅率</param>
        /// <param name="userId">訂單會員編號</param>
        /// <param name="creator">資料建立人</param>
        /// <param name="isPponItem">品名為購物金(false)或商品名稱(true)</param>
        /// <param name="invoiceStatus">發票狀態 預設值為0</param>
        /// <param name="orderClassification">訂單類型</param>
        public static void SetEinvoiceMain(Guid orderGuid, string orderId, DateTime orderTime, string invoiceComId, string invoiceComName,
            string invoiceBuyerName, string invoiceBuyerAddress, InvoiceMode2 invoiceMode2, string trans_id, int itemQuantity, string itemName,
            decimal order_amount, bool noTax, int userId, string creator, bool isPponItem, string sellerName,
            OrderClassification orderClassification, int? couponId, int invoiceStatus = (int)EinvoiceType.Initial)
        {
            EinvoiceMain main = new EinvoiceMain();
            main.InvoiceComId = invoiceComId;    //三聯發票統編
            main.InvoiceComName = invoiceComName;        //三聯發票抬頭
            main.InvoiceBuyerName = invoiceBuyerName;   //買受人姓名
            main.InvoiceBuyerAddress = invoiceBuyerAddress; //買受人地址
            main.InvoicePass = ReturnInvoicePass(); //回傳個人識別碼(4碼亂數)
            main.InvoiceMode = 0; //not used
            main.InvoiceMode2 = (int)invoiceMode2; //
            main.TransId = trans_id;    //payment_transaction
            main.OrderTime = orderTime;  //訂單時間
            main.OrderId = orderId;   //OrderID
            main.OrderGuid = orderGuid;    //OrderGuid
            main.OrderItem = itemQuantity + "X " + sellerName + "- " + (Regex.IsMatch(itemName, "【.*?】", RegexOptions.IgnoreCase) ? Regex.Match(itemName, "【.*?】", RegexOptions.IgnoreCase).ToString() : MaxLengthString(itemName, 150));    //份數和好康名稱
            main.OrderAmount = order_amount;    //刷卡金額
            main.OrderIsPponitem = isPponItem;
            main.InvoiceSumAmount = 0;              //加總金額(開立減折讓)
            main.InvoiceTax = noTax ? 0 : 0.05m;    //稅率(農產零稅，其他5%)
            main.UserId = userId;   //會員帳號
            if (main.InvoiceMode2 == (int)InvoiceMode2.Triplicate || (main.InvoiceMode2 == (int)InvoiceMode2.Duplicate && main.CarrierType == (int)CarrierType.None))
            {
                main.InvoiceRequestTime = orderTime; //紙本發票和三聯加註申請時間
            }
            // 紀錄可使用的折讓單類型，預設二聯=電子折讓；三聯=紙本
            if (main.InvoiceMode2 == (int)InvoiceMode2.Duplicate)
            {
                main.AllowanceStatus = (int)AllowanceStatus.ElecAllowance;
            }
            else
            {
                main.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
            }
            main.Creator = creator; //建檔人
            main.Message = "系統新增";  //訊息
            main.OrderClassification = (int)orderClassification;
            main.InvoiceStatus = invoiceStatus;
            main.CouponId = couponId;
            main.Version = (int)InvoiceVersion.CarrierEra;
            if (isPponItem || main.InvoiceMode2 == (int)InvoiceMode2.Triplicate)
            {
                main.VerifiedTime = orderTime;
            }
            SetEinvoiceMainWithLog(main, creator);
        }

        #endregion 新增品生活發票主檔(By Order)

        #region 新增P好康發票主檔(By Order)

        public static void SetEinvoiceMain(Order o, PponDeliveryInfo deliverinfo, string trans_id, int itemQuantity, string itemName,
            decimal order_amount, bool noTax, string creator, bool isPponItem, OrderClassification orderClassification, string creditCardTail,
            int invoice_status = (int)EinvoiceType.Initial, InvoiceVersion runtimeVersion = InvoiceVersion.Old, bool IsGroupCoupon = false)
        {
            // 若回傳的deliveryInfo.version與預期的參數version不符，則須轉換成對應的內容
            if (deliverinfo.Version == InvoiceVersion.Old && runtimeVersion == InvoiceVersion.CarrierEra)
            {
                deliverinfo.Version = InvoiceVersion.CarrierEra;
                switch (deliverinfo.ReceiptsType)
                {
                    case DonationReceiptsType.Karate:
                        deliverinfo.LoveCode = "23981";
                        deliverinfo.CarrierType = CarrierType.None;
                        break;
                    case DonationReceiptsType.GSWF:
                        deliverinfo.LoveCode = "919";
                        deliverinfo.CarrierType = CarrierType.None;
                        break;
                    default:
                        if (deliverinfo.InvoiceType != "3")
                        {
                            deliverinfo.CarrierType = CarrierType.Member;
                            deliverinfo.CarrierId = o.UserId.ToString();
                        }
                        else
                        {
                            deliverinfo.CarrierType = CarrierType.None;
                        }
                        break;
                }
            }

            EinvoiceMain main = new EinvoiceMain();
            main.InvoiceComId = deliverinfo.InvoiceType == "3" ? deliverinfo.UnifiedSerialNumber : string.Empty;    //三聯發票統編
            main.InvoiceComName = deliverinfo.InvoiceType == "3" ? Helper.RemoveControlChar(deliverinfo.CompanyTitle) : string.Empty;         //三聯發票抬頭
            main.InvoiceBuyerName = deliverinfo.InvoiceBuyerName;   //買受人姓名 (新版xml發票一定要填入購買人名稱)
            string invoiceBuyerAddress = string.Empty;

            if (!string.IsNullOrEmpty(deliverinfo.InvoiceBuyerAddress))
            {
                var invoiceBuyerAddressArray = deliverinfo.InvoiceBuyerAddress.Split('@');
                if (invoiceBuyerAddressArray.Count() > 1 && invoiceBuyerAddressArray.Count() == 2)
                {
                    invoiceBuyerAddress = invoiceBuyerAddressArray[1];
                }
                else
                {
                    invoiceBuyerAddress = deliverinfo.InvoiceBuyerAddress;
                }
            }
            else
            {
                invoiceBuyerAddress = deliverinfo.InvoiceBuyerAddress;
            }
            main.InvoiceBuyerAddress = Helper.RemoveControlChar(invoiceBuyerAddress); //買受人地址
            main.InvoicePass = ReturnInvoicePass(); //回傳個人識別碼(4碼亂數)
            main.Version = (int)runtimeVersion;


            if (Enum.Equals(InvoiceVersion.Old, runtimeVersion))
            {
                #region 舊電子發票

                // set Old EinvoiceMode
                switch (deliverinfo.ReceiptsType)
                {
                    case DonationReceiptsType.None:
                        main.InvoiceMode = 0; //not used
                        break;
                    case DonationReceiptsType.Karate:
                        main.InvoiceMode = 0; //not used
                        break;
                    case DonationReceiptsType.GSWF:
                        main.InvoiceMode = 0; //not used
                        break;
                    case DonationReceiptsType.DoNotContribute:
                        int invoice_type;
                        if (int.TryParse(deliverinfo.InvoiceType, out invoice_type) && invoice_type == 3)
                        {
                            main.InvoiceMode = 0; //not used
                        }
                        else
                        {
                            main.InvoiceMode = 0; //not used
                        }
                        break;
                    default:
                        main.InvoiceMode = 0; //not use
                        break;
                }

                // set verifiedTime
                if (isPponItem || main.InvoiceMode2 == (int)InvoiceMode2.Triplicate || IsGroupCoupon)
                {
                    main.VerifiedTime = o.CreateTime;
                }

                // set invoice requestTime
                if (main.InvoiceMode2 == (int)InvoiceMode2.Triplicate || (main.InvoiceMode2 == (int)InvoiceMode2.Duplicate && main.CarrierType == (int)CarrierType.None))
                {
                    main.InvoiceRequestTime = o.CreateTime; //紙本發票和三聯加註申請時間
                }

                #endregion
            }
            else if (Enum.Equals(InvoiceVersion.CarrierEra, runtimeVersion))
            {
                #region 發票載具 (自 2014/03/01 起加入發票載具及愛心碼資訊)

                // set InvoiceMode2, LoveCode, CarrierType & CarrierId
                //手機傳入參數錯誤，捐贈誤記為三聯，多加判斷避免
                if (!string.IsNullOrEmpty(deliverinfo.LoveCode))
                {
                    main.InvoiceMode2 = (int)InvoiceMode2.Duplicate;
                    //新版捐贈發票，統一將舊的invoicemode記為donationA
                    main.InvoiceMode = 0; //not used
                }
                else
                {
                    main.InvoiceMode2 = deliverinfo.InvoiceType == "3" ? (int)InvoiceMode2.Triplicate : (int)InvoiceMode2.Duplicate;
                    main.InvoiceMode = 0; //not used
                }
                main.CarrierType = (int)deliverinfo.CarrierType;
                main.CarrierId = deliverinfo.CarrierId;
                main.LoveCode = !string.IsNullOrEmpty(deliverinfo.LoveCode) ? deliverinfo.LoveCode.Trim() : null;

                // set verifiedTime (宅配或者是三聯需要立即押verifierTime以利產生發票號碼，憑證是核銷時才開立發票)
                if (isPponItem || main.InvoiceMode2 == (int)InvoiceMode2.Triplicate || IsGroupCoupon)
                {
                    main.VerifiedTime = o.CreateTime;
                }

                // set invoice requestTime (二聯紙本發票和三聯需加註申請時間)
                if ((main.InvoiceMode2 == (int)InvoiceMode2.Duplicate && main.CarrierType == (int)CarrierType.None && string.IsNullOrWhiteSpace(main.LoveCode))
                    || main.InvoiceMode2 == (int)InvoiceMode2.Triplicate)
                {
                    main.InvoiceRequestTime = o.CreateTime;
                }
                #endregion
            }

            // 紀錄可使用的折讓單類型，預設二聯=電子折讓；三聯=紙本
            if (main.InvoiceMode2 == (int)InvoiceMode2.Duplicate)
            {
                main.AllowanceStatus = (int)AllowanceStatus.ElecAllowance;
            }
            else
            {
                main.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
            }

            main.TransId = trans_id;    //payment_transaction
            main.OrderTime = o.CreateTime;  //訂單時間
            main.OrderId = o.OrderId;   //OrderID
            main.OrderGuid = o.Guid;    //OrderGuid
            main.OrderItem = itemQuantity + "X " + (Regex.IsMatch(itemName, "【.*?】", RegexOptions.IgnoreCase) ? Regex.Match(itemName, "【.*?】", RegexOptions.IgnoreCase).ToString() : MaxLengthString(itemName, 150));    //份數和好康名稱
            main.OrderAmount = order_amount;    //刷卡金額
            main.OrderIsPponitem = isPponItem;
            main.InvoiceSumAmount = 0;              //加總金額(開立減折讓)
            main.InvoiceTax = noTax ? 0 : 0.05m;    //稅率(農產零稅，其他5%)
            main.UserId = o.UserId;   //會員帳號
            main.Creator = creator; //建檔人
            main.Message = "系統新增";  //訊息
            main.InvoiceStatus = invoice_status;
            main.OrderClassification = (int)orderClassification;
            main.CreditCardTail = creditCardTail;

            SetEinvoiceMainWithLog(main, creator);
        }

        #endregion 新增P好康發票主檔(By Order)

        #region 新增訂單發票主檔

        /// <summary>
        /// 建立發票
        /// </summary>
        /// <param name="orderGuid">訂單GUID</param>
        /// <param name="orderId">訂單編號</param>
        /// <param name="orderTime">建單日期</param>
        /// <param name="invoiceComId">三聯發票統編</param>
        /// <param name="invoiceComName">三聯發票抬頭</param>
        /// <param name="invoiceBuyerName">買受人姓名</param>
        /// <param name="invoiceBuyerAddress">買受人地址</param>
        /// <param name="einvoiceMode">發票類型</param>
        /// <param name="trans_id">付款的transId</param>
        /// <param name="itemName">商品名稱</param>
        /// <param name="couponId">couponId</param>
        /// <param name="order_amount">發票開立金額</param>
        /// <param name="noTax">稅率</param>
        /// <param name="userId">訂單會員編號</param>
        /// <param name="creator">資料建立人</param>
        /// <param name="isPponItem">品名為購物金(false)或商品名稱(true)</param>
        /// <param name="orderClassification">訂單類型</param>
        /// <param name="carriertype">載具類型</param>
        /// <param name="invoiceStatus">發票狀態</param>        
        public static void SetEinvoiceMain(Guid orderGuid, string orderId, DateTime orderTime, string invoiceComId, string invoiceComName,
            string invoiceBuyerName, string invoiceBuyerAddress, InvoiceMode2 invoiceMode2, string trans_id, string itemName, int? couponId,
            decimal order_amount, bool noTax, int userId, string creator, bool isPponItem,
            OrderClassification orderClassification, CarrierType carriertype, EinvoiceType invoiceStatus)
        {
            EinvoiceMain main = new EinvoiceMain();
            main.InvoiceComId = invoiceComId;    //三聯發票統編
            main.InvoiceComName = Helper.RemoveControlChar(invoiceComName);        //三聯發票抬頭
            main.InvoiceBuyerName = invoiceBuyerName;   //買受人姓名
            main.InvoiceBuyerAddress = Helper.RemoveControlChar(invoiceBuyerAddress); //買受人地址
            main.InvoicePass = ReturnInvoicePass(); //回傳個人識別碼(4碼亂數)
            //main.InvoiceMode = 0;//舊欄位，尚未移除，先放0
            main.TransId = trans_id;    //payment_transaction
            main.OrderTime = orderTime;  //訂單時間
            main.OrderId = orderId;   //OrderID
            main.OrderGuid = orderGuid;    //OrderGuid
            main.OrderItem = itemName;
            main.OrderAmount = order_amount;    //刷卡金額
            main.OrderIsPponitem = isPponItem;
            main.CouponId = couponId;
            main.InvoiceSumAmount = 0;              //加總金額(開立減折讓)
            main.InvoiceTax = noTax ? 0 : config.BusinessTax;    //稅率(農產零稅，其他5%)            
            main.UserId = userId;   //會員帳號
            if (invoiceMode2 == InvoiceMode2.Triplicate || (invoiceMode2 == InvoiceMode2.Duplicate && carriertype == CarrierType.None))
            {
                main.InvoiceRequestTime = orderTime; //紙本發票和三聯加註申請時間
            }
            // 紀錄可使用的折讓單類型，預設二聯=電子折讓；三聯=紙本
            if (invoiceMode2 == InvoiceMode2.Duplicate)
            {
                main.AllowanceStatus = (int)AllowanceStatus.ElecAllowance;
            }
            else
            {
                main.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
            }
            main.Creator = creator; //建檔人
            main.Message = "系統新增PCP熟客點";  //訊息
            main.OrderClassification = (int)orderClassification;
            main.InvoiceStatus = (int)invoiceStatus;// = (int)EinvoiceType.Initial
            if (isPponItem || main.InvoiceMode2 == (int)InvoiceMode2.Triplicate)
            {
                main.VerifiedTime = orderTime;
            }
            SetEinvoiceMainWithLog(main, creator);
        }

        #endregion

        #region 代銷開立發票(一般代銷不開立，台新中台才有)

        public static void PartnerEinvoiceMainSet(string orderId, string invoiceComId, string invoiceComName,
           string invoiceBuyerName, string invoiceBuyerAddress,string winnerEmail, string itemName, Dictionary<string, int> couponInfo, int order_amount, int noTax, int carriertype, string carrierId, AgentChannel partnerType)
        {
            if (!op.PartnerEinvoiceMainGet(orderId, (int)partnerType).Any(x => x.InvoiceStatus == (int)EinvoiceType.C0401 || x.InvoiceStatus == (int)EinvoiceType.Initial))
            {
                foreach (var coupon in couponInfo)
                {
                    int cid = 0;
                    int? couponId = int.TryParse(coupon.Key, out cid) ? (int?)cid : null;
                    PartnerEinvoiceMainSet(orderId, invoiceComId, invoiceComName, invoiceBuyerName, invoiceBuyerAddress, winnerEmail, itemName, couponId, coupon.Value, noTax, carriertype, carrierId, partnerType);
                }
            }
        }

        public static void PartnerEinvoiceMainSet(string orderId, string invoiceComId, string invoiceComName,
           string invoiceBuyerName, string invoiceBuyerAddress, string winnerMail, string itemName, int? couponId,
           decimal order_amount, int noTax, int carriertype, string carrierId, AgentChannel partnerType)
        {
            DateTime now = DateTime.Now;
          
            PartnerEinvoiceMain main = new PartnerEinvoiceMain();
            main.InvoiceMode2 = !string.IsNullOrEmpty(invoiceComId) ? (int)InvoiceMode2.Triplicate : (int)InvoiceMode2.Duplicate;
            main.InvoiceComId = invoiceComId;    //三聯發票統編
            main.InvoiceComName = Helper.RemoveControlChar(invoiceComName);        //三聯發票抬頭
            main.InvoiceBuyerName = invoiceBuyerName;   //買受人姓名
            main.InvoiceBuyerAddress = Helper.RemoveControlChar(invoiceBuyerAddress); //買受人地址
            main.WinnerMail = winnerMail;
            main.InvoicePass = ReturnInvoicePass(); //回傳個人識別碼(4碼亂數)
            //main.OrderTime = orderTime;  //訂單時間
            main.PartnerOrderId = orderId;   //OrderID
            main.OrderItem = itemName;
            main.OrderAmount = order_amount;    //刷卡金額
            main.CouponId = couponId;
            main.InvoiceSumAmount = 0;              //加總金額(開立減折讓)
            main.InvoiceTax = noTax == 0 ? 0 : config.BusinessTax;    //稅率(農產零稅，其他5%)            
            main.VerifiedTime = (main.InvoiceMode2 == (int)InvoiceMode2.Triplicate || couponId == null) ? (DateTime?)now : null;
            main.InvoiceRequestTime = main.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? now : main.InvoiceRequestTime;
            main.InvoiceStatus = (int)EinvoiceType.Initial;
            main.CarrierId = main.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? string.Empty : carrierId;
            main.CarrierType = main.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? (int)CarrierType.None : carriertype;
            main.PartnerType = (int)partnerType;
            op.PartnerEinvoiceMainSet(main);

        }

        public static void PartnerEinvoiceMainCancel(string orderId, EinvoiceType invoiceStatus, Dictionary<string, int> couponInfo, int order_amount, AgentChannel partnerType)
        {
            foreach (var coupon in couponInfo)
            {
                PartnerEinvoiceMainCancel(orderId, invoiceStatus, coupon.Key, coupon.Value, partnerType);
            }
        }

        public static void PartnerEinvoiceMainCancel(string orderId, EinvoiceType invoiceStatus, string couponId,
           decimal refundAmt, AgentChannel partnerType, string message = "")
        {
            DateTime now = DateTime.Now;
            var main = op.PartnerEinvoiceMainGet(orderId, (int)partnerType).Where(x => ((!string.IsNullOrEmpty(couponId) && x.CouponId.ToString() == couponId) || string.IsNullOrEmpty(couponId))).FirstOrDefault();
            if (main.InvoiceStatus == (int)EinvoiceType.C0401)
            {
                //check amount
                main.InvoiceSumAmount = main.InvoiceSumAmount - refundAmt;
                main.InvoiceStatus = (int)invoiceStatus;
                main.CancelDate = DateTime.Now;
                op.PartnerEinvoiceMainSet(main);
            }
            else if (main.InvoiceStatus == (int)EinvoiceType.Initial && string.IsNullOrEmpty(main.InvoiceNumber))
            {
                main.InvoiceStatus = (int)EinvoiceType.NotComplete;
                main.CancelDate = DateTime.Now;
                op.PartnerEinvoiceMainSet(main);
            }

            //add einvoice detail
            PartnerEinvoiceDetail detail = new PartnerEinvoiceDetail();
            detail.MainId = main.Id;
            detail.InvoiceAmount = refundAmt;
            detail.InvoiceType = (int)invoiceStatus;
            detail.CreateTime = DateTime.Now;
            detail.Creator = "sys";
            detail.Status = false;
            detail.Message = message;
            op.PartnerEinvoiceDetailSet(detail);
        }

        public static void PartnerEinvoiceMainVoid(string orderId, string invoiceComId, string invoiceComName,
          string invoiceBuyerName, string invoiceBuyerAddress, string itemName, Dictionary<string, int> couponInfo, int noTax, int carriertype, string carrierId, AgentChannel partnerType)
        {
            //check data
            if (!op.PartnerEinvoiceMainGet(orderId, (int)partnerType).Any(x => x.InvoiceStatus == (int)EinvoiceType.Initial && !string.IsNullOrEmpty(x.InvoiceNumber)))
            {
                foreach (var coupon in couponInfo)
                {
                    PartnerEinvoiceMainVoid(orderId, invoiceComId, invoiceComName, invoiceBuyerName, invoiceBuyerAddress, itemName, coupon.Key, coupon.Value, noTax, carriertype, carrierId, partnerType);
                }
            }
        }


        public static void PartnerEinvoiceMainVoid(string orderId, string invoiceComId, string invoiceComName,
           string invoiceBuyerName, string invoiceBuyerAddress, string itemName, string couponId,
           decimal order_amount, int noTax, int carriertype, string carrierId, AgentChannel partnerType)
        {
            DateTime now = DateTime.Now;
            //check
            var main_list = op.PartnerEinvoiceMainGet(orderId, (int)partnerType);
            foreach (var item in main_list.Where(x => (x.InvoiceStatus == (int)EinvoiceType.Initial || x.InvoiceStatus == (int)EinvoiceType.C0401 || x.InvoiceStatus == (int)EinvoiceType.C0701) && ((!string.IsNullOrEmpty(couponId) && x.CouponId.ToString() == couponId) || string.IsNullOrEmpty(couponId))))
            {
                bool isIntertemporal = IsIntertemporal(item.InvoiceNumberTime);
                if (!string.IsNullOrEmpty(invoiceComId) && item.InvoiceMode2 == (int)InvoiceMode2.Duplicate && item.InvoiceStatus == (int)EinvoiceType.C0401)
                {
                    //作廢二聯並新開三聯
                    var invoiceStatus = isIntertemporal ? EinvoiceType.C0501 : EinvoiceType.D0401;
                    PartnerEinvoiceMainCancel(orderId, invoiceStatus, couponId, order_amount, partnerType, "二聯轉三聯");
                    PartnerEinvoiceMainSet(orderId, invoiceComId, invoiceComName, invoiceBuyerName, invoiceBuyerAddress, item.WinnerMail, itemName, item.CouponId, item.OrderAmount, noTax, carriertype, carrierId, partnerType);
                }
                else if (string.IsNullOrEmpty(invoiceComId) && item.InvoiceMode2 == (int)InvoiceMode2.Triplicate)
                {
                    //不支援3轉2
                }
                else if (isIntertemporal || item.InvoiceStatus == (int)EinvoiceType.Initial)
                {
                    if (!string.IsNullOrEmpty(invoiceComId))
                    {
                        //未開立發票的2轉3
                        item.VerifiedTime = item.VerifiedTime == null ? now : item.VerifiedTime;
                        item.InvoiceMode2 = (int)InvoiceMode2.Triplicate;
                    }
                    if (carriertype == (int)CarrierType.None)
                    {
                        item.InvoiceRequestTime = item.InvoiceRequestTime == null && carriertype == (int)CarrierType.None ? now : item.InvoiceRequestTime;
                    }
                    else
                    {
                        //未印製前可取消
                        item.InvoiceRequestTime = item.InvoicePapered ? item.InvoiceRequestTime : null;
                    }
                    item.InvoiceStatus = item.InvoiceStatus == (int)EinvoiceType.C0401 ? (int)EinvoiceType.C0701 : item.InvoiceStatus;
                    item.CarrierId = carrierId;
                    item.CarrierType = carriertype;

                    item.InvoiceComId = invoiceComId;
                    item.InvoiceComName = invoiceComName;
                    item.InvoiceBuyerName = invoiceBuyerName;
                    item.InvoiceBuyerAddress = invoiceBuyerAddress;
                    item.OrderItem = itemName;
                    item.OrderAmount = order_amount;
                    item.InvoiceSumAmount = item.InvoiceStatus == (int)EinvoiceType.Initial ? item.InvoiceSumAmount : order_amount;
                    item.InvoiceTax = noTax == 0 ? 0 : (decimal)0.05;

                    //add einvoice detail
                    if (item.DirtyColumns.Count > 0)
                    {
                        op.PartnerEinvoiceMainSet(item);
                        PartnerEinvoiceDetail detail = new PartnerEinvoiceDetail();
                        detail.MainId = item.Id;
                        detail.InvoiceAmount = order_amount;
                        detail.InvoiceType = (int)EinvoiceType.C0701;
                        detail.CreateTime = now;
                        detail.Creator = "sys";
                        detail.Status = false;
                        op.PartnerEinvoiceDetailSet(detail);
                    }
                }
                else
                {
                    logger.Info(string.Format("[未做任何變更]{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", orderId, invoiceComId, invoiceComName, invoiceBuyerName, invoiceBuyerAddress, itemName, couponId,
           order_amount.ToString(), noTax.ToString(), carriertype.ToString(), carrierId, partnerType.ToString()));
                }
            }
        }

        public static PartnerEinvoiceMainCollection PartnerEinvoiceMainGet(string orderId, string invoiceNumber, AgentChannel partnerType)
        {
            return op.PartnerEinvoiceMainGet(orderId, (int)partnerType);
        }

        public static IEnumerable<PartnerEinvoiceMain> PartnerWinnerEinvoiceGet(string dateCode, int partnerType)
        {
            var es = op.EinvoiceSerialCollectionGetByDate(dateCode);
            if (es.Count > 0)
            {
                var s_date = es.FirstOrDefault().StartDate;
                var e_date = es.LastOrDefault().EndDate;
                return op.PartnerEinvoiceMainGetByDate(s_date, e_date).Where(x => x.InvoiceWinning && x.PartnerType == partnerType);
            }
            return null;
        }

        public static string GeneratePartnerEinvoiceNumber(DateTime date_start, DateTime date_end, DateTime? date_index)
        {
            int count = 0;
            DateTime now = DateTime.Now;
            
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            var es = op.EinvoiceSerialCollectionGetByDate(datecode).Where(x => date_start >= x.StartDate && date_start < x.EndDate).FirstOrDefault();

            var data = op.PartnerEinvoiceMainGetByDate(date_start, date_end).Where(x => x.InvoiceStatus == (int)EinvoiceType.Initial);
            foreach (var main in data)
            {
                main.InvoiceNumber = es.HeadCode + (es.NextSerial).ToString().PadLeft(8, '0');   //發票號碼
                main.InvoiceNumberTime = DateTime.Now;       //發票時間(48小時內需上傳)
                main.InvoiceSumAmount = main.OrderAmount;   //金額(加總金額)
                main.InvoiceStatus = (int)EinvoiceType.C0401;
                op.PartnerEinvoiceMainSet(main);
                PartnerEinvoiceDetail detail = new PartnerEinvoiceDetail();
                detail.MainId = main.Id;        //主檔ID
                detail.InvoiceAmount = main.OrderAmount;    //開立發票金額
                detail.InvoiceType = (int)EinvoiceType.C0401;   //發票類型(開立)
                detail.Creator = config.SystemEmail;  //建檔人
                detail.CreateTime = now;        //建檔時間
                detail.Message = "系統新增";    //訊息
                op.PartnerEinvoiceDetailSet(detail);
                es.NextSerial = es.NextSerial + 1;
                op.EinvoiceSetSerial(es);
                count += 1;
            }

            return count + "筆代銷發票號碼產生";
        }

        public static bool GeneratePartnerEinvoiceUploadFile(DateTime date_start, DateTime date_end)
        {
            DateTime now = DateTime.Now;
            var vcm = op.ViewPartnerEinvoiceMaindetailCollectionGetByDate(date_start, date_end);
            int fileserial = 0;
            int d_count = 0;
            //改用dic 紀錄所有類型
            Dictionary<EinvoiceType, int> invTypeCount = new Dictionary<EinvoiceType, int>();

            foreach (var item in vcm)
            {
                EinvoiceTransModel model = new EinvoiceTransModel(item);
                switch (item.InvoiceStatus)
                {
                    case (int)EinvoiceType.C0401:
                        UploadEinvoiceFile(model.ReturnC0401(), fileserial, EinvoiceType.C0401, now, d_count);
                        break;
                    case (int)EinvoiceType.C0501:
                        UploadEinvoiceFile(model.ReturnC0501(now), fileserial, EinvoiceType.C0501, now, d_count);
                        break;
                    case (int)EinvoiceType.C0701:
                        UploadEinvoiceFile(model.ReturnC0701(now), fileserial, EinvoiceType.C0701, now, d_count);
                        break;
                    case (int)EinvoiceType.D0401:
                        int allowanceCount = op.EinvoiceDetailGetCount(item.Id, (int)EinvoiceType.D0401); //註記折讓次數
                        string allowanceNumber = "17P" + (allowanceCount.ToString().PadLeft(2, '0')) + (item.DetailId.ToString().PadLeft(11, '0'));
                        UploadEinvoiceFile(model.ReturnD0401(now, item.DetailInvoiceAmount, allowanceNumber), fileserial, EinvoiceType.D0401, now, d_count);
                        break;
                    //case (int)EinvoiceType.D0501:
                        //UploadEinvoiceFile(ReturnD0501(item), fileserial, EinvoiceType.D0501, now, d_count);
                        //break;
                }
                d_count += 1;
            }
            return true;
        }

        private static bool IsIntertemporal(DateTime? invoiceNumberTime)
        {
            DateTime now = DateTime.Now;
            return invoiceNumberTime.HasValue && ((now.Year * 12 + now.Month) - (invoiceNumberTime.Value.Year * 12 + invoiceNumberTime.Value.Month)) <= invoiceNumberTime.Value.Month % 2;
        }

        #endregion

        #region 產生發票號碼，新增開立發票資料

        public static string GenerateEinvoiceNumber(DateTime date_start, DateTime date_end, DateTime? date_index)            //請款日期起迄
        {
            DateTime now = (date_index != null) ? date_index.Value : DateTime.Now;
            int count = 0;
            int current = 0;
            int v_count = 0;
            //設定einvoice_serial的年月碼(用來撈取某年月的發票字軌，可設定是否開立為隔月發票)
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            string datecodeForVendor = (DateTime.Now.AddMonths(-1).Month % 2 == 0 ? DateTime.Now.AddMonths(-2) : DateTime.Now.AddMonths(-1)).ToString("yyyyMM");
            //請款1
            //發票序號資料(抓取請款日的發票序號)
            var es = op.EinvoiceSerialCollectionGetByDate(datecode).Where(x => date_start >= x.StartDate && date_start < x.EndDate);
            var es2 = op.EinvoiceSerialCollectionGetByDate(datecodeForVendor).Where(x => date_start >= x.StartDate && date_start < x.EndDate);
            //訂單資料(抓取時間內，發票號碼為null)
            var data = op.EinvoiceMainCollectionGet(date_start, date_end, config.NewInvoiceDate, true);
            var vendor = op.VendorEinvoiceMainCollectionGetNew();

            if (data.Count > es.Sum(x => (x.VendorMinSerial ?? x.MaxSerial) - x.NextSerial) || vendor.Count > es2.Sum(x => x.MaxSerial - x.VendorNextSerial))
            {
                SetJobMessage(datecode, "發票號碼不足!!", EinvoiceType.EinvoiceJobMessage);
                SendMail(config.ItTesterEmail, "電子發票警示訊息", "發票號碼不足!!");
                return "發票碼不足";
            }
            else if (data.Any(x => x.InvoiceMode2 != (int)InvoiceMode2.Duplicate && x.InvoiceMode2 != (int)InvoiceMode2.Triplicate && x.CarrierType != (int)CarrierType.None && x.CarrierType != (int)CarrierType.Member && x.CarrierType != (int)CarrierType.PersonalCertificate && x.CarrierType != (int)CarrierType.Phone))
            {
                SetJobMessage(datecode, "包含非發票類型檔案!!", EinvoiceType.EinvoiceJobMessage);
                SendMail(config.ItTesterEmail, "電子發票警示訊息", "包含非發票類型檔案!!");
                return "包含非發票類型檔案";
            }
            else
            {
                //產生發票資料 EinvoiceDetail
                count = data.Count;
                v_count = vendor.Count;
                foreach (EinvoiceSerial sitem in es)
                {
                    int consumerMaxSerial = sitem.VendorMinSerial ?? sitem.MaxSerial;
                    //是否還有票號
                    if (consumerMaxSerial != sitem.NextSerial)
                    {
                        //需開立發票數大於剩餘票號
                        if (count > (consumerMaxSerial - sitem.NextSerial + current))
                        {
                            for (int i = current; i < consumerMaxSerial - sitem.NextSerial + current + 1; i++)
                            {
                                EinvoiceMain einvoiceMain = data[i];
                                //建立發票號碼
                                UpdateEinvoiceWithNum(einvoiceMain, sitem.HeadCode + (sitem.NextSerial + i - current).ToString().PadLeft(8, '0'), sitem.Id, now);

                                #region 資策會訂單的處理
                                //資策會的訂單
                                if (einvoiceMain.UserId == ApiLocationDealManager.IDEASMember.UniqueId)
                                {
                                    //標註需通知資策會已異動
                                    op.CompanyUserOrderUpdateWaitUpdate(einvoiceMain.OrderGuid, true);
                                }
                                #endregion 資策會訂單的處理

                            }

                            //更新票號
                            current = current + consumerMaxSerial - sitem.NextSerial + 1;
                            sitem.NextSerial = consumerMaxSerial;
                        }
                        else
                        {
                            //需開立發票小於剩餘票號
                            for (int i = current; i < count; i++)
                            {
                                EinvoiceMain einvoiceMain = data[i];
                                UpdateEinvoiceWithNum(einvoiceMain, sitem.HeadCode + (sitem.NextSerial + i - current).ToString().PadLeft(8, '0'), sitem.Id, now);
                                #region 資策會訂單的處理
                                //資策會的訂單
                                if (einvoiceMain.UserId == ApiLocationDealManager.IDEASMember.UniqueId)
                                {
                                    //標註需通知資策會已異動
                                    op.CompanyUserOrderUpdateWaitUpdate(einvoiceMain.OrderGuid, true);
                                }
                                #endregion 資策會訂單的處理
                            }

                            sitem.NextSerial = sitem.NextSerial + count - current;
                            current = count;

                        }
                    }

                    op.EinvoiceSetSerial(sitem);
                }

                foreach (EinvoiceSerial sitem in es2)
                {
                    if (sitem.MaxSerial != sitem.VendorNextSerial && sitem.VendorNextSerial + v_count <= sitem.MaxSerial)
                    {
                        current = 0;
                        for (int j = 0; j < vendor.Count; j++)
                        {
                            VendorEinvoiceMain vem = vendor[j];
                            //建立發票號碼
                            vem.InvoiceNumber = sitem.HeadCode + (sitem.VendorNextSerial + j).ToString().PadLeft(8, '0');
                            vem.InvoiceNumberTime = DateTime.Now.AddMonths(-1).GetLastDayOfMonth();
                            vem.InvoiceStatus = (int)EinvoiceType.C0401;
                            op.VendorEinvoiceMainSet(vem);
                            current += 1;
                        }
                        sitem.VendorNextSerial = sitem.VendorNextSerial + current;
                    }
                    op.EinvoiceSetSerial(sitem);
                }

                decimal comparedailyincome = 0;//op.EinvoiceCompareDailyIncome(date_start, date_end);
                decimal sumamount = data.Sum(item => item.OrderAmount);
                EinvoiceLog log = new EinvoiceLog();
                log.DateCode = datecode;
                log.InvoiceDateStart = date_start;
                log.InvoiceDateEnd = date_end;
                log.InvoiceCount = count;
                log.InvoiceSum = (int)sumamount;
                log.ReferenceSum = (int)comparedailyincome;
                log.InvoiceMode1Total = data.Count(item => item.InvoiceMode == 1);
                log.InvoiceMode1Sum = (int)data.Where(x => x.InvoiceMode == 1).Sum(x => x.OrderAmount);
                log.InvoiceMode2Total = data.Count(item => item.InvoiceMode == 2);
                log.InvoiceMode2Sum = (int)data.Where(x => x.InvoiceMode == 2).Sum(x => x.OrderAmount);
                log.InvoiceMode17Total = data.Count(item => item.InvoiceMode == 17);
                log.InvoiceMode17Sum = (int)data.Where(x => x.InvoiceMode == 17).Sum(x => x.OrderAmount);
                log.InvoiceMode18Total = data.Count(item => item.InvoiceMode == 18);
                log.InvoiceMode18Sum = (int)data.Where(x => x.InvoiceMode == 18).Sum(x => x.OrderAmount);
                log.InvoiceMode21Total = data.Count(item => item.InvoiceMode == 21);
                log.InvoiceMode21Sum = (int)data.Where(x => x.InvoiceMode == 21).Sum(x => x.OrderAmount);
                log.InvoiceMode22Total = data.Count(item => item.InvoiceMode == 1);
                log.InvoiceMode22Sum = (int)data.Where(x => x.InvoiceMode == 1).Sum(x => x.OrderAmount);
                log.LogMessage = "開立日期:" + now.ToString("yyyy/MM/dd") + ",";
                log.InvoiceType = (int)EinvoiceType.DailyReport;
                log.CreateTime = DateTime.Now;
                op.EinvoiceLogSet(log);
                return count + "筆發票號碼產生";
            }
        }

        #endregion 產生發票號碼，新增開立發票資料

        #region 更新發票主檔，新增開立發票

        public static void UpdateEinvoiceWithNum(EinvoiceMain main, string invoice_num, int serial_id, DateTime now)
        {
            main.InvoiceNumber = invoice_num;   //發票號碼
            main.InvoiceNumberTime = now;       //發票時間(48小時內需上傳)
            main.InvoiceSerialId = serial_id;       //發票號碼序號ID
            main.InvoiceSumAmount = main.OrderAmount;   //金額(加總金額)
            main.InvoiceStatus = (int)EinvoiceType.C0401;
            SetEinvoiceMainWithLog(main, "sys");
            EinvoiceDetail detail = new EinvoiceDetail();
            detail.MainId = main.Id;        //主檔ID
            detail.InvoiceAmount = main.OrderAmount;    //開立發票金額
            detail.InvoiceType = (int)EinvoiceType.C0401;   //發票類型(開立)
            detail.Creator = config.SystemEmail;  //建檔人
            detail.CreateTime = now;        //建檔時間
            detail.Message = "系統新增";    //訊息
            op.EinvoiceSetDetail(detail);
        }

        #endregion 更新發票主檔，新增開立發票

        #region 新增折讓發票

        public static string AddEinvoiceAllowance(DateTime date_start, DateTime date_end, bool elecAllowanceOnly = true)
        {
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            DateTime datebase = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start);
            DateTime now = DateTime.Now;
            int count = 0;
            int void_count = 0;
            decimal amount = 0;
            decimal void_amount = 0;

            #region 一張訂單對應一張發票的折讓(2012/7/1以前的訂單,以及宅配、成套訂單符合此規則)

            //payment_transaction實際刷退時間可能與cash_trust_log紀錄之時間不一致
            //如立即退貨及當天即買即退訂單 皆為當天更新cash_trust_log 退款時間 但實際於隔天才完成刷退狀況
            //故將折讓作業拉長處理為兩天前資料
            int buffer = 0;
            if (date_end.Subtract(date_start).Days == 1)
            {
                buffer = -1;
            }

            //抓取時間內已刷退成功
            //20190227:折讓月處理只看退貨單成立不看實際刷退，避免跨日時漏處理
            var data = elecAllowanceOnly ? op.EinvoiceAllowanceGetList(date_start.AddDays(buffer), date_end)
                        .Where(x => (x.OrderTime < config.NewInvoiceDate || x.OrderIsPponitem == true || Helper.IsFlagSet(x.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon))
                                 && (x.Amount.HasValue && x.Amount > 0)) : op.EinvoiceAllowanceGetListV2(date_start.AddDays(buffer), date_end)
                        .Where(x => (x.OrderTime < config.NewInvoiceDate || x.OrderIsPponitem == true || Helper.IsFlagSet(x.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon))
                                 && (x.Amount.HasValue && x.Amount > 0));
            if (elecAllowanceOnly)
            {
                data = data.Where(x => x.AllowanceStatus.GetValueOrDefault((int)AllowanceStatus.PaperAllowance) == (int)AllowanceStatus.ElecAllowance);
            }
            foreach (var item in data)
            {
                var detail = new EinvoiceDetail();
                detail.MainId = item.Id;    //發票主檔ID
                detail.InvoiceAmount = item.Amount.Value;    //刷退金額
                detail.TransTime = item.TransTime.Value;    //刷退成功的交易時間
                detail.PaymentTransId = item.PaymentTransId;    //paymenttransaction_id(折讓可能多筆，需PK來判斷重複)
                detail.Creator = config.SystemEmail;  //建檔人
                detail.CreateTime = now;    //建檔時間
                detail.Message = string.Format("{0}-發票折讓", item.AllowanceStatus.GetValueOrDefault((int)AllowanceStatus.PaperAllowance) == (int)AllowanceStatus.PaperAllowance ? "紙本折讓" : "電子折讓");
                detail.ReturnFormId = item.ReturnFormId;    //紀錄退貨單id 避免同一張退貨單進行購物金轉退現金 重複處理折讓/作廢

                if (item.InvoiceMode != -1)
                {
                    if (item.InvoiceNumberTime.Value < datebase.AddDays(1 - datebase.Day))//非當期的僅能做折讓
                    {
                        if (item.InvoiceSumAmount >= (item.Amount))
                        {
                            detail.InvoiceType = (int)EinvoiceType.D0401;   //發票類型(折讓)
                            op.EinvoiceSetDetail(detail);
                            op.EinvoiceUpdateSum(item.Amount.Value, item.Id, (int)EinvoiceType.D0401);  //更新主檔加總金額
                            amount += item.Amount.Value;
                            count++;
                        }
                    }
                    //當期
                    else
                    {
                        if ((item.InvoiceSumAmount == item.Amount) && (item.Amount == item.OrderAmount) && !item.InvoicePapered)//全額退費採用作廢 //201706後印製紙本都走折讓
                        {
                            if (!elecAllowanceOnly)
                            {
                                detail.InvoiceType = (int)EinvoiceType.C0501;   //發票類型(作廢) 
                                op.EinvoiceUpdateSum(item.Amount.Value, item.Id, (int)EinvoiceType.C0501);  //更新主檔加總金額
                                op.EinvoiceSetDetail(detail);
                                void_amount += item.Amount.Value;
                                void_count++;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        //部分退貨採用折讓
                        else if (item.InvoiceSumAmount >= (item.Amount))
                        {
                            detail.InvoiceType = (int)EinvoiceType.D0401;   //發票類型(折讓)
                            op.EinvoiceSetDetail(detail);
                            op.EinvoiceUpdateSum(item.Amount.Value, item.Id, (int)EinvoiceType.D0401);  //更新主檔加總金額
                            amount += item.Amount.Value;
                            count++;
                        }
                    }
                }
            }

            #endregion 一張訂單對應一張發票的折讓(2012/7/1以前的訂單,以及宅配、成套訂單符合此規則)

            #region 一張訂單對應多張發票的折讓(2012/7/1以後的憑證訂單符合此規則)+2016/12/13新增未開發票購物金規則

            var dataInstore = op.EinvoiceAllowanceInstoreIncludeScashCollectionGet(date_start, date_end).ToList();
            if (elecAllowanceOnly)
            {
                dataInstore = dataInstore.Where(x => x.AllowanceStatus.GetValueOrDefault((int)AllowanceStatus.PaperAllowance) == (int)AllowanceStatus.ElecAllowance).ToList();
            }
            foreach (var item in dataInstore)
            {
                EinvoiceDetail detail = new EinvoiceDetail();
                detail.MainId = item.MainId;    //發票主檔ID
                detail.InvoiceAmount = item.CashAmount.Value;    //刷退金額
                detail.TransTime = item.RefundTime;    //刷退成功的交易時間
                detail.TrustId = item.TrustId;    //paymenttransaction_id(折讓可能多筆，需PK來判斷重複)
                detail.Creator = config.SystemEmail;  //建檔人
                detail.CreateTime = now;    //建檔時間
                detail.Message = string.Format("憑證{0}-發票折讓", item.AllowanceStatus.GetValueOrDefault((int)AllowanceStatus.PaperAllowance) == (int)AllowanceStatus.PaperAllowance ? "紙本折讓" : "電子折讓");
                if (item.InvoiceMode != -1)
                {
                    if (item.InvoiceNumberTime.Value < datebase.AddDays(1 - datebase.Day))//非當月的僅能做折讓
                    {
                        if (item.InvoiceSumAmount >= (item.OrderAmount))
                        {
                            detail.InvoiceType = (int)EinvoiceType.D0401;   //發票類型(折讓)
                            op.EinvoiceSetDetail(detail);
                            op.EinvoiceUpdateSum(item.OrderAmount, item.MainId, (int)EinvoiceType.D0401);  //更新主檔加總金額
                            amount += item.OrderAmount;
                            count++;
                        }
                    }
                    else
                    {
                        if (item.InvoiceSumAmount == (item.OrderAmount) && !item.InvoicePapered)//全額退費採用作廢 //201706後當期印製紙本也走折讓
                        {
                            if (!elecAllowanceOnly)
                            {
                                detail.InvoiceType = (int)EinvoiceType.C0501;   //發票類型(作廢)
                                op.EinvoiceUpdateSum(item.OrderAmount, item.MainId, (int)EinvoiceType.C0501);  //更新主檔加總金額
                                op.EinvoiceSetDetail(detail);
                                void_amount += item.OrderAmount;
                                void_count++;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        //部分退貨採用折讓
                        else if (item.InvoiceSumAmount >= (item.OrderAmount))
                        {
                            detail.InvoiceType = (int)EinvoiceType.D0401;   //發票類型(折讓)
                            op.EinvoiceSetDetail(detail);
                            op.EinvoiceUpdateSum(item.OrderAmount, item.MainId, (int)EinvoiceType.D0401);  //更新主檔加總金額
                            amount += item.OrderAmount;
                            count++;
                        }
                    }
                }
            }

            #endregion 一張訂單對應多張發票的折讓(2012/7/1以後的憑證訂單符合此規則)+2016/12/13新增未開發票購物金規則

            SetJobMessage(datecode, data.Count() + dataInstore.Count + "筆資料;" + count + "筆折讓單產生($" + amount + ");" + void_count + "筆作廢單產生(" + void_amount + ");", EinvoiceType.EinvoiceJobMessage);
            return data.Count() + dataInstore.Count + "筆資料;" + count + "筆折讓單產生;" + void_count + "筆作廢單產生;";
        }

        #endregion 新增折讓發票

        #region 產生作廢折讓

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date_start"></param>
        /// <param name="date_end"></param>
        public static void GenerateVoidAllowanceEinovice(DateTime date_start, DateTime date_end)
        {
            var now = DateTime.Now;
            var rids = op.GetCancelReturnForm(date_start, date_end).Select(x => x.Id).ToList();
            int d_count = 0;
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            int fileserial = op.EinvoiceLogGetCount(datecode, (int)EinvoiceType.D0501);
            foreach (var rid in rids)
            {
                var item = op.EinvoiceMainDetailGet(rid);
                if (!item.IsLoaded)
                {
                    continue;
                }

                if (item.InvoiceType == (int)EinvoiceType.D0401)
                {
                    EinvoiceMain main = op.EinvoiceMainGetById(item.Id);
                    main.InvoiceSumAmount += item.DetailInvoiceAmount;
                    main.InvoiceStatus = main.InvoiceSumAmount == main.OrderAmount ? (int)EinvoiceType.C0401 : main.InvoiceStatus;
                    UploadEinvoiceFile(ReturnD0501(item), fileserial, EinvoiceType.D0501, now, d_count);
                    EinvoiceDetail detail = new EinvoiceDetail();
                    detail.MainId = item.Id;
                    detail.InvoiceAmount = item.DetailInvoiceAmount;
                    detail.InvoiceType = (int)EinvoiceType.D0501;
                    detail.Creator = config.SystemEmail;
                    detail.CreateTime = now;
                    detail.Message = "取消退貨，作廢原有折讓單";
                    detail.ReturnFormId = rid;
                    detail.Status = true;
                    detail.TransTime = now;
                    detail.InvoiceFileSerial = fileserial + 1;
                    op.EinvoiceSetDetail(detail);
                    d_count += 1;
                    SetEinvoiceMainWithLog(main, "sys", now);
                }

            }

            SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，共:{3}筆",
                      EinvoiceType.D0501.ToString() + "作廢折讓發票", date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), d_count), EinvoiceType.D0501);

        }

        /// <summary>
        /// 簡單產生所有發票類型
        /// </summary>
        /// <param name="xdoc"></param>
        /// <param name="item"></param>
        /// <param name="type"></param>
        /// <param name="now"></param>
        /// <param name="d_count"></param>
        public static void UploadEinvoiceFile(XDocument xdoc, int fileserial, EinvoiceType type, DateTime now,int d_count)
        {
            string folder_new = string.Format(@"C:\eInvoiceText\{0}\SRC\", type.ToString());
            if (!Directory.Exists(folder_new))
            {
                Directory.CreateDirectory(folder_new);
            }

            try
            {
                xdoc.Save(string.Format("{0}{1}{2}{3}-{4}.xml", folder_new, type.ToString(), now.ToString("-yyMMdd-HHmmss-"), (fileserial + 1).ToString().PadLeft(3, '0'), (d_count + 1)));

                //d_amount += item.OrderAmount;

            }
            catch (Exception error)
            {
                //builder.AppendLine(string.Format("{0},{1},{2}", item.Id, item.InvoiceNumber, error.Message));
            }
        }
        #endregion

        #region 產出每月物流發票
        public static void GenerateVendorEinvoice()
        {
            var vbsbills = ap.GetShippingFeeOfLastMonth().AsEnumerable();
            VendorEinvoiceMainCollection vemc = new VendorEinvoiceMainCollection();
            if (vbsbills.Count() > 0)
            {
                foreach (var bill in vbsbills.Select(s => new { Sid = s.Field<Guid>("seller_guid"), CompanyName = s.Field<string>("CompanyName"), ComId = s.Field<string>("invoice_com_id"), Fee = s.Field<int>("fee"), OverdueAmount = s.Field<int>("overdue_amount"), WmsAmount = s.Field<int>("wms_amount"), AccountantName = s.Field<string>("accountant_name"), SellerAddress = s.Field<string>("seller_address") }))
                {
                    VendorEinvoiceMain vem = new VendorEinvoiceMain();
                    vem.IsNew = true;
                    vem.InvoiceComId = !string.IsNullOrEmpty(bill.ComId) && bill.ComId.Length == 8 ? bill.ComId : string.Empty;
                    vem.InvoiceComName = !string.IsNullOrEmpty(bill.ComId) && bill.ComId.Length == 8 ? bill.CompanyName : string.Empty;
                    vem.InvoiceStatus = (int)EinvoiceType.Initial;
                    vem.InvoiceMode2 = !string.IsNullOrEmpty(bill.ComId) && bill.ComId.Length == 8 ? (int)InvoiceMode2.Triplicate : (int)InvoiceMode2.Duplicate;
                    vem.InvoicePass = ReturnInvoicePass();
                    vem.InvoicePapered = false;
                    vem.OrderAmount = bill.Fee + (bill.OverdueAmount < 0 ? bill.OverdueAmount * -1 : 0) + +(bill.WmsAmount < 0 ? bill.WmsAmount * -1 : 0);
                    vem.InvoiceTax = (decimal)0.05;
                    vem.CarrierType = (int)CarrierType.None;
                    vem.SellerGuid = bill.Sid;
                    vem.AccountantName = bill.AccountantName;
                    vem.SellerAddress = bill.SellerAddress;
                    vem.ShippingFee = bill.Fee;
                    vem.WmsAmount = bill.WmsAmount < 0 ? bill.WmsAmount * -1 : 0;
                    vem.OverdueAmount = bill.OverdueAmount < 0 ? bill.OverdueAmount * -1 : 0;
                    vem.IsUpload = false;

                    vemc.Add(vem);
                }
                op.VendorEinvoiceMainCollectionSet(vemc);
            }

            vemc = op.VendorEinvoiceMainCollectionGetNew();

            //回壓
            foreach (var vem in vemc)
            {
                List<int> bill_ids = vbsbills.Where(x => x.Field<Guid>("seller_guid") == vem.SellerGuid).FirstOrDefault().Field<string>("bill_ids").Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList();
                BillCollection bills = ap.BillGetList(bill_ids);
                bills.ForEach(x => x.VendorEinvoiceId = vem.Id);
                ap.BillSet(bills);
            }

        }
        #endregion

        #region 產生上傳電子檔(開立、折讓、作廢)

        public static void GenerateUploadFiles(DateTime date_start, DateTime date_end, DateTime now)            //請款日期起迄
        {
            StringBuilder builder = new StringBuilder();
            string folder_new = @"C:\eInvoiceText\C0401\SRC\";

            if (!Directory.Exists(folder_new))
            {
                Directory.CreateDirectory(folder_new);
            }
            //發票年月
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            bool exists = true;
            bool voidMode = false;
            int total_loop = 0;
            while (exists)
            {
                //抓取未上傳的開立發票檔 (因為之前電子發票軟體只能一次接收最大500份資料，sql裡設定抓top 500)
                //新版發票後 請改為下列 (三聯發票可上傳)
                ViewEinvoiceMaindetailCollection emc = op.EinvoiceMainDetailCollectionGet(date_start, date_end, EinvoiceMain.Columns.InvoiceNumberTime, (int)EinvoiceType.C0401, true);
                //撈出前日註銷需再次重新上傳的發票
                if (emc.Count == 0)
                {
                    var voidEmc = op.EinvoiceMainDetailCollectionGetVoid(date_start.AddDays(-1), date_end.AddDays(-1), true).Where(x => x.InvoiceNumberTime != null);
                    emc.AddRange(voidEmc);
                    voidMode = true;
                }

                exists = emc.Count > 0;
                if (exists)
                {
                    int d_count = 0;
                    decimal d_amount = 0;
                    //已開立發票txt檔序號
                    int fileserial = op.EinvoiceLogGetCount(datecode, (int)EinvoiceType.C0401);
                    foreach (ViewEinvoiceMaindetail item in emc)
                    {
                        decimal salesamount = (Math.Round((item.OrderAmount / (1 + item.InvoiceTax)), 0));
                        try
                        {
                            XDocument new_version_einvoice_xml = ReturnC0401(item, ReturnInvoiceTime(item.InvoiceNumberTime.Value,item.VerifiedTime.Value), salesamount);
                            new_version_einvoice_xml.Save(string.Format("{0}C0401{1}{2}-{3}.xml", folder_new, now.ToString("-yyMMdd-HHmmss-"), (fileserial + 1).ToString().PadLeft(3, '0'), (d_count + 1)));
                            d_count++;
                            d_amount += item.OrderAmount;
                            op.EinvoiceUpdateFileSerial(item.DetailId, fileserial + 1, now);
                            if (d_count >= 500 && voidMode)
                            {
                                d_count = 0;
                                fileserial += 1;
                            }
                        }
                        catch (Exception error)
                        {
                            builder.AppendLine(string.Format("{0},{1},{2}", item.Id, item.InvoiceNumber, error.Message));
                        }
                    }

                    if (voidMode)
                    {
                        EinvoiceMainCollection newEmc = op.EinvoiceMainGetByVoid(date_start.AddDays(-1), date_end.AddDays(-1), true);
                        if (newEmc.Count > 0)
                        {
                            foreach (EinvoiceMain em in newEmc)
                            {
                                em.InvoiceStatus = (int)EinvoiceType.C0401;
                                if(em.CarrierType == (int)CarrierType.None && em.InvoiceRequestTime == null && !string.IsNullOrEmpty(em.LoveCode))
                                {
                                    em.InvoiceRequestTime = now;
                                }
                            }
                            SetEinvoiceMainCollectionWithLog(newEmc);
                            SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，發票號碼:{3}~{4}，共:{5}筆，總金額:{6}",
EinvoiceType.C0401.ToString() + "開立發票(重開)", date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), emc.First().InvoiceNumber, emc.Last().InvoiceNumber, d_count, d_amount), EinvoiceType.C0401);
                        }
                    }
                    else
                    {
                        SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，發票號碼:{3}~{4}，共:{5}筆，總金額:{6}",
EinvoiceType.C0401.ToString() + "開立發票", date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), emc.First().InvoiceNumber, emc.Last().InvoiceNumber, d_count, d_amount), EinvoiceType.C0401);
                    }

                    total_loop++;
                    if (total_loop > 500)
                    {
                        SendMail(config.ItTesterEmail, "產生開立錯誤", "迴圈錯誤");
                        exists = false;
                    }
                }
            }
            string error_invoice = string.Empty;
            if (!string.IsNullOrEmpty(error_invoice = builder.ToString()))
            {
                SendMail(config.ItTesterEmail, "產生開立錯誤", error_invoice);
            }
        }

        public static void ReGenerateUploadFilesByDate(DateTime date_start, DateTime date_end)
        {
            StringBuilder builder = new StringBuilder();
            string folder_new = @"C:\eInvoiceText\C0401\SRC\";

            if (!Directory.Exists(folder_new))
            {
                Directory.CreateDirectory(folder_new);
            }
            //發票年月
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            bool exists = true;
            int total_loop = 0;
            while (exists)
            {
                //抓取未上傳的開立發票檔 (因為之前電子發票軟體只能一次接收最大500份資料，sql裡設定抓top 500)
                //新版發票後 請改為下列 (三聯發票可上傳)
                ViewEinvoiceMaindetailCollection emc = op.EinvoiceMainDetailCollectionGet(date_start, date_end, EinvoiceMain.Columns.InvoiceNumberTime, (int)EinvoiceType.C0401, true);
                exists = emc.Count > 0;
                if (exists)
                {
                    //StringBuilder sb = new StringBuilder();
                    int d_count = 0;
                    decimal d_amount = 0;
                    //已開立發票txt檔序號
                    int fileserial = op.EinvoiceLogGetCount(datecode, (int)EinvoiceType.C0401);
                    foreach (ViewEinvoiceMaindetail item in emc)
                    {
                        decimal salesamount = (Math.Round((item.OrderAmount / (1 + item.InvoiceTax)), 0));
                        try
                        {
                            XDocument new_version_einvoice_xml = ReturnC0401(item, ReturnInvoiceTime(item.InvoiceNumberTime.Value, item.VerifiedTime.Value), salesamount);
                            new_version_einvoice_xml.Save(string.Format("{0}C0401{1}{2}-{3}.xml", folder_new, item.InvoicedTime.Value.ToString("-yyMMdd-HHmmss-"), (fileserial + 1).ToString().PadLeft(3, '0'), (d_count + 1)));
                            d_count++;
                            d_amount += item.OrderAmount;
                            op.EinvoiceUpdateFileSerial(item.DetailId, fileserial + 1, item.InvoicedTime.Value);
                        }
                        catch (Exception error)
                        {
                            builder.AppendLine(string.Format("{0},{1},{2}", item.Id, item.InvoiceNumber, error.Message));
                        }
                    }
                    SetJobMessage(datecode, string.Format("補產發票種類:{0}，資料間隔:{1}~{2}，發票號碼:{3}~{4}，共:{5}筆，總金額:{6}",
   EinvoiceType.C0401.ToString() + "開立發票", date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), emc.First().InvoiceNumber, emc.Last().InvoiceNumber, d_count, d_amount), EinvoiceType.C0401);

                    total_loop++;
                    if (total_loop > 500)
                    {
                        SendMail(config.ItTesterEmail, "產生開立錯誤", "迴圈錯誤");
                        exists = false;
                    }
                }
            }
            string error_invoice = string.Empty;
            if (!string.IsNullOrEmpty(error_invoice = builder.ToString()))
            {
                SendMail(config.ItTesterEmail, "產生開立錯誤", error_invoice);
            }
        }

        public static void GenerateEinvoiceUploadFileById(List<int> list_id, EinvoiceType einvoiceType)
        {
            DateTime now = DateTime.Now;
            string folder_new = string.Format(@"C:\eInvoiceText\{0}\SRC\", einvoiceType.ToString());
            StringBuilder builder = new StringBuilder();

            if (!Directory.Exists(folder_new))
            {
                Directory.CreateDirectory(folder_new);
            }
            string datecode = (now.Month % 2 == 0 ? now.AddMonths(-1) : now).ToString("yyyyMM");
            int fileserial = op.EinvoiceLogGetCount(datecode, (int)einvoiceType);
            int d_count = 0;
            decimal d_amount = 0;
            foreach (int id in list_id)
            {
                //抓取開立發票檔
                try
                {
                    ViewEinvoiceMaindetail vem = op.EinvoiceMainDetailGet(id, (int)einvoiceType);
                    XDocument new_version_einvoice_xml;
                    switch (einvoiceType)
                    {
                        case EinvoiceType.C0501:
                            new_version_einvoice_xml = ReturnC0501(vem, now);
                            op.EinvoiceUpdateFileSerial(id, fileserial + 1, now);
                            break;
                        case EinvoiceType.D0401:
                            decimal salesamount = (Math.Round((vem.DetailInvoiceAmount / (1 + vem.InvoiceTax)), 0));
                            int allowanceCount = op.EinvoiceDetailGetCount(vem.Id, (int)EinvoiceType.D0401); //註記折讓次數
                            string allowanceNumber = "17P" + (allowanceCount.ToString().PadLeft(2, '0')) + (vem.DetailId.ToString().PadLeft(11, '0'));
                            new_version_einvoice_xml = ReturnD0401(vem, now, salesamount, allowanceNumber);
                            op.EinvoiceDetailUpdateAllowanceNumber(id, fileserial + 1, allowanceNumber, now);
                            break;
                        case EinvoiceType.C0701:
                            new_version_einvoice_xml = ReturnC0701(vem);
                            op.EinvoiceUpdateFileSerial(id, fileserial + 1, now);
                            break;
                        default:
                            return;
                    }
                    new_version_einvoice_xml.Save(string.Format("{0}{1}{2}{3}-{4}.xml", folder_new, einvoiceType.ToString(), now.ToString("-yyMMdd-HHmmss-"), (fileserial + 1).ToString().PadLeft(3, '0'), (d_count + 1)));
                    d_count++;
                    d_amount += vem.OrderAmount;
                }
                catch (Exception error)
                {
                    builder.AppendLine(string.Format("{0},{1}", id,  error.Message));
                }
            }
            SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，發票號碼:{3}~{4}，共:{5}筆，總金額:{6}",
                einvoiceType.ToString() + "作廢發票", now.ToString("yyyy/MM/dd"), now.ToString("yyyy/MM/dd"), string.Empty, string.Empty, d_count, d_amount), einvoiceType);
        }

        //註銷已上傳至財政部的電子發票檔，注意!! 註銷狀況為特例，註銷之發票應該要重新上傳，einvoice_detail invoicetype=1開立的status在註銷後，不做變更(保持在已開立)
        public static void GenerateVoidUploadFileById(List<int> list_id)
        {
            DateTime now = DateTime.Now;
            string folder_new = @"C:\eInvoiceText\C0701\SRC\";

            if (!Directory.Exists(folder_new))
            {
                Directory.CreateDirectory(folder_new);
            }
            string datecode = (now.Month % 2 == 0 ? now.AddMonths(-1) : now).ToString("yyyyMM");
            int fileserial = op.EinvoiceLogGetCount(datecode, (int)EinvoiceType.C0701);
            int d_count = 0;
            decimal d_amount = 0;
            foreach (int id in list_id)
            {
                //抓取開立發票檔
                ViewEinvoiceMaindetail emc = op.EinvoiceMainDetailGet(id, (int)EinvoiceType.C0701);
                XDocument new_version_einvoice_xml = ReturnC0701(emc);
                new_version_einvoice_xml.Save(string.Format("{0}C0701{1}{2}-{3}.xml", folder_new, now.ToString("-yyMMdd-HHmmss-"), (fileserial + 1).ToString().PadLeft(3, '0'), (d_count + 1)));
                d_count++;
                d_amount += emc.OrderAmount;
            }
            SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，發票號碼:{3}~{4}，共:{5}筆，總金額:{6}",
                EinvoiceType.C0701.ToString() + "註銷發票", now.ToString("yyyy/MM/dd"), now.ToString("yyyy/MM/dd"), string.Empty, string.Empty, d_count, d_amount), EinvoiceType.C0701);
        }

        /// <summary>
        /// 產生註銷檔
        /// </summary>
        /// <param name="date_start"></param>
        /// <param name="date_end"></param>
        public static void GenerateVoidUploadFiles(DateTime date_start, DateTime date_end)
        {
            string folder_new = @"C:\eInvoiceText\C0701\SRC\";

            if (!Directory.Exists(folder_new))
            {
                Directory.CreateDirectory(folder_new);
            }

            DateTime now = DateTime.Now;
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            string datecode2 = (now.Month % 2 == 0 ? now.AddMonths(-1) : now).ToString("yyyyMM");
            //註銷僅能用在當期，次期暫時設最多5天緩衝
            if (datecode != datecode2 && now.AddDays(-config.InvoiceBufferDays) > date_start)
            {
                return;
            }

            ViewEinvoiceMaindetailCollection vemc = op.EinvoiceMainDetailCollectionGetVoid(date_start, date_end, false);

            if (vemc.Count <= 0)
            {
                return;
            }

            int fileserial = op.EinvoiceLogGetCount(datecode, (int)EinvoiceType.C0701);
            int d_count = 0;
            decimal d_amount = 0;
            foreach (ViewEinvoiceMaindetail vem in vemc)
            {
                string voidReason = "變更載具，註銷重開";
                XDocument new_version_einvoice_xml = ReturnC0701(vem, voidReason);
                new_version_einvoice_xml.Save(string.Format("{0}C0701{1}{2}-{3}.xml", folder_new, now.ToString("-yyMMdd-HHmmss-"), (fileserial + 1).ToString().PadLeft(3, '0'), (d_count + 1)));
                d_count++;
                d_amount += vem.OrderAmount;

                if (d_count >= 500)
                {
                    d_count = 0;
                    fileserial += 1;
                }

                //set detail
                EinvoiceDetail detail = new EinvoiceDetail();
                detail.MainId = vem.Id;    //發票主檔ID
                detail.InvoiceAmount = vem.InvoiceSumAmount;    //刷退金額
                detail.InvoiceType = (int)EinvoiceType.C0701;   //發票類型(作廢)
                detail.Creator = config.SystemEmail;  //建檔人
                detail.CreateTime = now;    //建檔時間
                detail.Message = "註銷重開";    //訊息
                op.EinvoiceSetDetail(detail);
            }

            //set invoicemain status initial
            EinvoiceMainCollection newEmc = op.EinvoiceMainGetByVoid(date_start, date_end, false);
            if (newEmc.Count > 0)
            {
                foreach (EinvoiceMain em in newEmc)
                {
                    em.InvoiceStatus = (int)EinvoiceType.Initial;
                }
                SetEinvoiceMainCollectionWithLog(newEmc);
            }

            SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，發票號碼:{3}~{4}，共:{5}筆，總金額:{6}",
            EinvoiceType.C0701.ToString() + "註銷發票", date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), string.Empty, string.Empty, vemc.Count, d_amount), EinvoiceType.C0701);
        }

        public static void GenerateUploadFilesById(List<int> list_id, DateTime now)
        {
            int d_count = 0;
            foreach (int id in list_id)
            {
                //抓取發票檔
                ViewEinvoiceMaindetail vem = op.EinvoiceMainDetailGet(id, 0);
                if (vem != null)
                {
                    EinvoiceType type = (EinvoiceType)vem.InvoiceType;
                    string folder_new = string.Format(@"C:\eInvoiceText\{0}\SRC\", type.ToString());
                    if (!Directory.Exists(folder_new))
                    {
                        Directory.CreateDirectory(folder_new);
                    }
                    string datecode = (vem.DetailCreateTime.Month % 2 == 0 ? vem.DetailCreateTime.AddMonths(-1) : now).ToString("yyyyMM");
                    int fileserial = op.EinvoiceLogGetCount(datecode, (int)type);

                    decimal salesamount = (Math.Round((vem.DetailInvoiceAmount / (1 + vem.InvoiceTax)), 0));

                    XDocument new_version_einvoice_xml = new XDocument();

                    switch (type)
                    {
                        case EinvoiceType.C0401:
                            new_version_einvoice_xml = ReturnC0401(vem, ReturnInvoiceTime(vem.InvoiceNumberTime.Value, vem.VerifiedTime.Value), salesamount);
                            break;
                        case EinvoiceType.D0401:
                            int allowanceCount = op.EinvoiceDetailGetCount(vem.Id, (int)EinvoiceType.D0401); //註記折讓次數
                            string allowanceNumber = "17P" + (allowanceCount.ToString().PadLeft(2, '0')) + (vem.DetailId.ToString().PadLeft(11, '0'));
                            new_version_einvoice_xml = ReturnD0401(vem, now, salesamount, allowanceNumber);
                            break;
                        default:
                            break;
                    }

                    new_version_einvoice_xml.Save(string.Format("{0}{1}{2}{3}-{4}.xml", folder_new, type.ToString() ,now.ToString("-yyMMdd-HHmmss-"), (fileserial + 1).ToString().PadLeft(3, '0'), (d_count + 1)));
                    d_count++;
                    op.EinvoiceUpdateFileSerial(vem.DetailId, fileserial + 1, now);
                }
            }
        }

        public static void GenerateAllowanceUploadFiles(DateTime date_start, DateTime date_end, DateTime now) //刷退日期起迄
        {
            StringBuilder builder = new StringBuilder();
            string folder_new = @"C:\eInvoiceText\D0401\SRC\";

            if (!Directory.Exists(folder_new))
            {
                Directory.CreateDirectory(folder_new);
            }
            bool exists = true;
            int total_loop = 0;
            //發票年月
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            while (exists)
            {
                //折讓 抓出einvoice_detail status=0
                //新版發票後 請改為下列 (三聯發票可上傳)
                ViewEinvoiceMaindetailCollection emc = op.EinvoiceMainDetailCollectionGet(date_start, date_end, ViewEinvoiceMaindetail.Columns.DetailTransTime, (int)EinvoiceType.D0401, true);
                exists = emc.Count > 0;
                if (exists)
                {
                    int d_count = 0;
                    decimal d_amount = 0;
                    //已開立發票txt檔序號
                    int fileserial = op.EinvoiceLogGetCount(datecode, (int)EinvoiceType.D0401);
                    foreach (ViewEinvoiceMaindetail item in emc)
                    {
                        decimal salesamount = (Math.Round((item.DetailInvoiceAmount / (1 + item.InvoiceTax)), 0));

                        try
                        {
                            int allowanceCount = op.EinvoiceDetailGetCount(item.Id, (int)EinvoiceType.D0401); //註記折讓次數
                            string allowanceNumber = "17P" + (allowanceCount.ToString().PadLeft(2, '0')) + (item.DetailId.ToString().PadLeft(11, '0'));
                            XDocument new_version_einvoice_xml = ReturnD0401(item, now, salesamount, allowanceNumber);
                            new_version_einvoice_xml.Save(string.Format("{0}D0401{1}{2}-{3}.xml", folder_new, now.ToString("-yyMMdd-HHmmss-"), (fileserial + 1).ToString().PadLeft(3, '0'), (d_count + 1)));
                            d_count++;
                            d_amount += item.OrderAmount;
                            //更新einvoice_detail status=1
                            op.EinvoiceDetailUpdateAllowanceNumber(item.DetailId, fileserial + 1, allowanceNumber, now);
                        }
                        catch (Exception error)
                        {
                            builder.AppendLine(string.Format("{0},{1},{2}", item.Id, item.InvoiceNumber, error.Message));
                        }
                    }
                    SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，共:{3}筆，總金額:{4}",
                         EinvoiceType.D0401.ToString() + "折讓發票", date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), d_count, d_amount), EinvoiceType.D0401);
                    total_loop++;
                    if (total_loop > 500)
                    {
                        SendMail(config.ItTesterEmail, "產生折讓錯誤", "迴圈錯誤");
                        exists = false;
                    }
                }
            }
            string error_invoice = string.Empty;
            if (!string.IsNullOrEmpty(error_invoice = builder.ToString()))
            {
                SendMail(config.ItPAD, "產生折讓錯誤", error_invoice);
            }
        }

        public static void GenerateCancelUploadFiles(DateTime date_start, DateTime date_end, DateTime now) //刷退日期起迄
        {
            StringBuilder builder = new StringBuilder();
            string folder_new = @"C:\eInvoiceText\C0501\SRC\";

            if (!Directory.Exists(folder_new))
            {
                Directory.CreateDirectory(folder_new);
            }
            bool exists = true;
            int total_loop = 0;
            //發票年月
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            while (exists)
            {
                //折讓
                //新版發票後 請改為下列 (三聯發票可上傳)
                ViewEinvoiceMaindetailCollection emc = op.EinvoiceMainDetailCollectionGet(date_start, date_end, ViewEinvoiceMaindetail.Columns.DetailTransTime, (int)EinvoiceType.C0501, true);
                exists = emc.Count > 0;
                if (exists)
                {
                    int d_count = 0;
                    decimal d_amount = 0;
                    //已開立發票txt檔序號
                    int fileserial = op.EinvoiceLogGetCount(datecode, (int)EinvoiceType.C0501);
                    foreach (ViewEinvoiceMaindetail item in emc)
                    {
                        try
                        {
                            XDocument new_version_einvoice_xml = ReturnC0501(item, now);
                            new_version_einvoice_xml.Save(string.Format("{0}C0501{1}{2}-{3}.xml", folder_new, now.ToString("-yyMMdd-HHmmss-"), (fileserial + 1).ToString().PadLeft(3, '0'), (d_count + 1)));
                            d_count++;
                            d_amount += item.OrderAmount;
                            op.EinvoiceUpdateFileSerial(item.DetailId, fileserial + 1, now);
                        }
                        catch (Exception error)
                        {
                            builder.AppendLine(string.Format("{0},{1},{2}", item.Id, item.InvoiceNumber, error.Message));
                        }
                    }
                    SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，共:{3}筆，總金額:{4}",
                          EinvoiceType.C0501.ToString() + "作廢發票", date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), d_count, d_amount), EinvoiceType.C0501);

                    total_loop++;
                    if (total_loop > 500)
                    {
                        SendMail(config.ItTesterEmail, "產生作廢錯誤", "迴圈錯誤");
                        exists = false;
                    }
                }
            }
            string error_invoice = string.Empty;
            if (!string.IsNullOrEmpty(error_invoice = builder.ToString()))
            {
                SendMail(config.ItPAD, "產生作廢錯誤", error_invoice);
            }
        }

        #region 廠商對開發票

        public static void GenerateUploadVendorEinvoiceFiles(DateTime date_start, DateTime date_end, DateTime now)
        {
            StringBuilder builder = new StringBuilder();
            string folder_new = @"C:\eInvoiceText\C0401\SRC\";

            if (!Directory.Exists(folder_new))
            {
                Directory.CreateDirectory(folder_new);
            }
            //發票年月
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            bool exists = true;
            int total_loop = 0;
            while (exists)
            {
                var vemc = op.VendorEinvoiceMainCollectionGetUpload().Where(x => x.InvoiceStatus == (int)EinvoiceType.C0401);

                exists = vemc.Count() > 0;
                if (exists)
                {
                    int d_count = 0;
                    decimal d_amount = 0;
                    //已開立發票txt檔序號
                    int fileserial = op.EinvoiceLogGetCount(datecode, (int)EinvoiceType.C0401);
                    foreach (var vem in vemc)
                    {
                        EinvoiceC0401Main main = new EinvoiceC0401Main();
                        main.InvoiceNumber = vem.InvoiceNumber;
                        main.InvoiceNumberTime = vem.InvoiceNumberTime.Value;
                        main.OrderAmount = vem.OrderAmount;
                        main.InvoiceTax = vem.InvoiceTax;
                        main.InvoicePass = vem.InvoicePass;
                        main.InvoiceMode2 = vem.InvoiceMode2;
                        main.CarrierType = (int)CarrierType.None;
                        main.UserId = "0";
                        main.BuyerComId = vem.InvoiceComId;
                        main.BuyerComName = vem.InvoiceComName;
                        main.Details = new List<EinvoiceC0401Detail>();

                        //注意稅差，用總額減別各別算
                        var orderAmountNoTax = Math.Round(vem.OrderAmount / (1 + vem.InvoiceTax), 0);
                        var shippingFeeNoTax = vem.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? Math.Round(vem.ShippingFee / (1 + vem.InvoiceTax), 0) : vem.ShippingFee;
                        var overdueAmountNoTax = vem.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? Math.Round(vem.OverdueAmount / (1 + vem.InvoiceTax), 0) : vem.OverdueAmount;
                        var wmsAmountNoTax = vem.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? Math.Round(vem.WmsAmount / (1 + vem.InvoiceTax), 0) : vem.WmsAmount;
                        var dic = new Dictionary<string, decimal>();
                        dic.Add("shippingFee", shippingFeeNoTax);
                        dic.Add("overdueAmount", overdueAmountNoTax);
                        dic.Add("wmsAmount", wmsAmountNoTax);
                        if (dic.Sum(x => x.Value) != orderAmountNoTax)
                        {
                            var maxValue = dic.Values.Max();
                            dic[dic.Aggregate((x, y) => x.Value > y.Value ? x : y).Key] = orderAmountNoTax - (dic.Sum(x => x.Value) - maxValue);
                        }

                        EinvoiceC0401Detail detail = new EinvoiceC0401Detail();
                        if (shippingFeeNoTax > 0)
                        {
                            detail.OrderIsPponitem = true;
                            detail.OrderItem = "物流處理費";
                            detail.Amount = (int)dic["shippingFee"];
                            main.Details.Add(detail);
                        }

                        if (overdueAmountNoTax > 0)
                        {
                            detail = new EinvoiceC0401Detail();
                            detail.OrderIsPponitem = true;
                            detail.OrderItem = "罰款收入";
                            detail.Amount = (int)dic["overdueAmount"];
                            main.Details.Add(detail);
                        }

                        if (wmsAmountNoTax > 0)
                        {
                            detail = new EinvoiceC0401Detail();
                            detail.OrderIsPponitem = true;
                            detail.OrderItem = "倉儲費用";
                            detail.Amount = (int)dic["wmsAmount"];
                            main.Details.Add(detail);
                        }

                        try
                        {
                            XDocument new_version_einvoice_xml = ReturnC0401(main, main.InvoiceNumberTime);
                            new_version_einvoice_xml.Save(string.Format("{0}C0401{1}{2}-{3}.xml", folder_new, now.ToString("-yyMMdd-HHmmss-"), (fileserial + 1).ToString().PadLeft(3, '0'), (d_count + 1)));
                            d_count++;
                            d_amount += vem.OrderAmount;
                            vem.IsUpload = true;
                            op.VendorEinvoiceMainSet(vem);
                            if (d_count >= 500)
                            {
                                d_count = 0;
                                fileserial += 1;
                            }
                        }
                        catch (Exception error)
                        {
                            builder.AppendLine(string.Format("{0},{1}", main.InvoiceNumber, error.Message));
                        }
                    }

                    SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，發票號碼:{3}~{4}，共:{5}筆，總金額:{6}","廠商對開發票", date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), vemc.First().InvoiceNumber, vemc.Last().InvoiceNumber, d_count, d_amount), EinvoiceType.C0401);


                    total_loop++;
                    if (total_loop > 500)
                    {
                        SendMail(config.ItTesterEmail, "產生開立錯誤", "迴圈錯誤");
                        exists = false;
                    }
                }
            }
            string error_invoice = string.Empty;
            if (!string.IsNullOrEmpty(error_invoice = builder.ToString()))
            {
                SendMail(config.ItTesterEmail, "產生開立錯誤", error_invoice);
            }
        }

        public static void GenerateVendorCancelUploadFiles(DateTime date_start, DateTime date_end, DateTime now)
        {
            StringBuilder builder = new StringBuilder();
            string folder_new = @"C:\eInvoiceText\C0501\SRC\";

            if (!Directory.Exists(folder_new))
            {
                Directory.CreateDirectory(folder_new);
            }
            bool exists = true;
            int total_loop = 0;
            //發票年月
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");

            //新版發票後 請改為下列 (三聯發票可上傳)
            VendorEinvoiceMainCollection emc = op.VendorEinvoiceCollectionGetByCancelDate(date_start, date_end);
            exists = emc.Count > 0;
            if (exists)
            {
                int d_count = 0;
                decimal d_amount = 0;
                //已開立發票txt檔序號
                int fileserial = op.EinvoiceLogGetCount(datecode, (int)EinvoiceType.C0501);
                foreach (VendorEinvoiceMain vem in emc.Where(x => x.InvoiceStatus == (int)EinvoiceType.C0501 && !x.IsUpload))
                {
                    try
                    {
                        ViewEinvoiceMaindetail item = new ViewEinvoiceMaindetail();
                        item.InvoiceNumber = vem.InvoiceNumber;
                        item.InvoiceNumberTime = item.VerifiedTime = vem.InvoiceNumberTime;
                        item.InvoiceMode2 = vem.InvoiceMode2;
                        item.InvoiceComId = vem.InvoiceComId;

                        XDocument new_version_einvoice_xml = ReturnC0501(item, now);
                        new_version_einvoice_xml.Save(string.Format("{0}C0501{1}{2}-{3}.xml", folder_new, now.ToString("-yyMMdd-HHmmss-"), (fileserial + 1).ToString().PadLeft(3, '0'), (d_count + 1)));
                        d_count++;
                        d_amount += vem.OrderAmount;

                        vem.IsUpload = true;
                    }
                    catch (Exception error)
                    {
                        builder.AppendLine(string.Format("{0},{1},{2}", vem.Id, vem.InvoiceNumber, error.Message));
                    }
                }
                op.VendorEinvoiceMainCollectionSet(emc);
                SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，共:{3}筆，總金額:{4}",
                        EinvoiceType.C0501.ToString() + "廠商對開作廢發票", date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), d_count, d_amount), EinvoiceType.C0501);

                total_loop++;
                if (total_loop > 500)
                {
                    SendMail(config.ItTesterEmail, "廠商對開發票產生作廢錯誤", "迴圈錯誤");
                    exists = false;
                }
            }

            string error_invoice = string.Empty;
            if (!string.IsNullOrEmpty(error_invoice = builder.ToString()))
            {
                SendMail(config.ItPAD, "廠商對開發票產生作廢錯誤", error_invoice);
            }
        }

        #endregion

        #endregion 產生上傳電子檔(開立、折讓、作廢)

        #region 新版 : C0401開立

        public static XDocument ReturnC0401(ViewEinvoiceMaindetail item, DateTime now, decimal salesamount)
        {
            KeyValuePair<XNamespace, string> xml_namespaces = GetXmlNameSpace(EinvoiceType.C0401);
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XDocument(new XDeclaration("1.0", "utf-8", "true"),
                    new XElement(xml_namespaces.Key + "Invoice",
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                    new XAttribute(xsi + "schemaLocation", xml_namespaces.Value),
                    ReturnC0401Main(item, now, xml_namespaces.Key), ReturnC0401Details(item, now, salesamount, xml_namespaces.Key), ReturnC0401Amount(item, salesamount, xml_namespaces.Key))
                );
        }

        private static XDocument ReturnC0401(EinvoiceC0401Main main, DateTime now)
        {
            KeyValuePair<XNamespace, string> xml_namespaces = GetXmlNameSpace(EinvoiceType.C0401);
            decimal salesamount = (Math.Round((main.OrderAmount / (1 + main.InvoiceTax)), 0));
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XDocument(new XDeclaration("1.0", "utf-8", "true"),
                    new XElement(xml_namespaces.Key + "Invoice",
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                    new XAttribute(xsi + "schemaLocation", xml_namespaces.Value),
                    ReturnC0401Main(main, now, xml_namespaces.Key), ReturnC0401Details(main, xml_namespaces.Key), ReturnC0401Amount(main, salesamount, xml_namespaces.Key))
                );
        }

        public static XElement ReturnC0401Main(ViewEinvoiceMaindetail item, DateTime now, XNamespace xmlns)
        {
            XElement main = new XElement(xmlns + "Main",
                             new XElement(xmlns + "InvoiceNumber", item.InvoiceNumber), //發票號碼
                             new XElement(xmlns + "InvoiceDate", now.ToString("yyyyMMdd")), //發票日期
                             new XElement(xmlns + "InvoiceTime", now.ToString("HH:mm:ss")), //發票時間
                             new XElement(xmlns + "Seller", //賣方資訊
                                 new XElement(xmlns + "Identifier", Com_Id),
                                 new XElement(xmlns + "Name", Com_Name)),
                             new XElement(xmlns + "Buyer", //買方資訊
                                 new XElement(xmlns + "Identifier", item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? item.InvoiceComId : "00000000"), //三聯式填入統編，二聯式填入8個0
                                 new XElement(xmlns + "Name", item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? MaxLengthString(item.InvoiceComName, 60) : item.UserId.ToString())), //三聯式填入公司抬頭，二聯式填入買方姓名
                             new XElement(xmlns + "InvoiceType", DateTime.Now >= config.EinvoiceMIGv312.AddDays(1) ? "07" : "05"), //發票類別 01三聯:02二聯(item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? "01" : "02") //改為05 不分二聯三聯 // 2015年開始改為07
                             new XElement(xmlns + "DonateMark", string.IsNullOrEmpty(item.LoveCode) ? 0 : 1) //捐贈註記 0非捐贈，1捐贈                           
                             );
            switch (item.CarrierType)
            {
                //3J0002 手機條碼 ； EJ0018 17LIFE : 自然人憑證載具 CQ0001
                case (int)CarrierType.Member:
                    main.Add(
                         new XElement(xmlns + "CarrierType", CarrierTypeCode_Member), //載具類別號碼
                             new XElement(xmlns + "CarrierId1", item.UserId), //載具顯碼
                             new XElement(xmlns + "CarrierId2", item.UserId), //載具隱碼
                             new XElement(xmlns + "PrintMark", "N") //紙本發票已列印註記
                        );
                    break;
                case (int)CarrierType.Phone:
                    main.Add(
                        new XElement(xmlns + "CarrierType", CarrierTypeCode_Phone),
                            new XElement(xmlns + "CarrierId1", item.CarrierId),
                            new XElement(xmlns + "CarrierId2", item.CarrierId),
                            new XElement(xmlns + "PrintMark", "N")
                       );
                    break;
                case (int)CarrierType.PersonalCertificate:
                    main.Add(
                       new XElement(xmlns + "CarrierType", CarrierTypeCode_PersonalCertificate),
                           new XElement(xmlns + "CarrierId1", item.CarrierId),
                           new XElement(xmlns + "CarrierId2", item.CarrierId),
                           new XElement(xmlns + "PrintMark", "N")
                      );
                    break;
                case (int)CarrierType.None:
                default:
                    if (!string.IsNullOrEmpty(item.LoveCode))
                    {
                        main.Add(
                       new XElement(xmlns + "PrintMark", "N"), //列印紙本註記，捐贈填N
                       new XElement(xmlns + "NPOBAN", item.LoveCode) //捐贈對象
                     );
                    }
                    else
                    {
                        main.Add(
                           new XElement(xmlns + "PrintMark", "Y")
                         );
                    }
                    break;
            }
            int invouce_pass;
            if (int.TryParse(item.InvoicePass, out invouce_pass))
            {
                main.Add(new XElement(xmlns + "RandomNumber", item.InvoicePass));
            }
            else
            {
                main.Add(new XElement(xmlns + "RandomNumber", new Random(Guid.NewGuid().GetHashCode()).Next(1, 9999).ToString().PadLeft(4, '0')));
            }
            return main;
        }

        private static XElement ReturnC0401Main(EinvoiceC0401Main item, DateTime now, XNamespace xmlns)
        {
            XElement main = new XElement(xmlns + "Main",
                             new XElement(xmlns + "InvoiceNumber", item.InvoiceNumber), //發票號碼
                             new XElement(xmlns + "InvoiceDate", now.ToString("yyyyMMdd")), //發票日期
                             new XElement(xmlns + "InvoiceTime", now.ToString("HH:mm:ss")), //發票時間
                             new XElement(xmlns + "Seller", //賣方資訊
                                 new XElement(xmlns + "Identifier", Com_Id),
                                 new XElement(xmlns + "Name", Com_Name)),
                             new XElement(xmlns + "Buyer", //買方資訊
                                 new XElement(xmlns + "Identifier", item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? item.BuyerComId : "00000000"), //三聯式填入統編，二聯式填入8個0
                                 new XElement(xmlns + "Name", item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? MaxLengthString(item.BuyerComName, 60) : item.UserId.ToString())), //三聯式填入公司抬頭，二聯式填入買方姓名
                             new XElement(xmlns + "InvoiceType", DateTime.Now >= config.EinvoiceMIGv312.AddDays(1) ? "07" : "05"), //發票類別 01三聯:02二聯(item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? "01" : "02") //改為05 不分二聯三聯 // 2015年開始改為07
                             new XElement(xmlns + "DonateMark", string.IsNullOrEmpty(item.LoveCode) ? 0 : 1) //捐贈註記 0非捐贈，1捐贈                           
                             );
            switch (item.CarrierType)
            {
                //3J0002 手機條碼 ； EJ0018 17LIFE : 自然人憑證載具 CQ0001
                case (int)CarrierType.Member:
                    main.Add(
                         new XElement(xmlns + "CarrierType", CarrierTypeCode_Member), //載具類別號碼
                             new XElement(xmlns + "CarrierId1", item.UserId), //載具顯碼
                             new XElement(xmlns + "CarrierId2", item.UserId), //載具隱碼
                             new XElement(xmlns + "PrintMark", "N") //紙本發票已列印註記
                        );
                    break;
                case (int)CarrierType.Phone:
                    main.Add(
                        new XElement(xmlns + "CarrierType", CarrierTypeCode_Phone),
                            new XElement(xmlns + "CarrierId1", item.CarrierId),
                            new XElement(xmlns + "CarrierId2", item.CarrierId),
                            new XElement(xmlns + "PrintMark", "N")
                       );
                    break;
                case (int)CarrierType.PersonalCertificate:
                    main.Add(
                       new XElement(xmlns + "CarrierType", CarrierTypeCode_PersonalCertificate),
                           new XElement(xmlns + "CarrierId1", item.CarrierId),
                           new XElement(xmlns + "CarrierId2", item.CarrierId),
                           new XElement(xmlns + "PrintMark", "N")
                      );
                    break;
                case (int)CarrierType.None:
                default:
                    if (!string.IsNullOrEmpty(item.LoveCode))
                    {
                        main.Add(
                       new XElement(xmlns + "PrintMark", "N"), //列印紙本註記，捐贈填N
                       new XElement(xmlns + "NPOBAN", item.LoveCode) //捐贈對象
                     );
                    }
                    else
                    {
                        main.Add(
                           new XElement(xmlns + "PrintMark", "Y")
                         );
                    }
                    break;
            }
            int invouce_pass;
            if (int.TryParse(item.InvoicePass, out invouce_pass))
            {
                main.Add(new XElement(xmlns + "RandomNumber", item.InvoicePass));
            }
            else
            {
                main.Add(new XElement(xmlns + "RandomNumber", new Random(Guid.NewGuid().GetHashCode()).Next(1, 9999).ToString().PadLeft(4, '0')));
            }
            return main;
        }

        public static XElement ReturnC0401Details(ViewEinvoiceMaindetail item, DateTime now, decimal salesamount, XNamespace xmlns)
        {
            return new XElement(xmlns + "Details",
                                new XElement(xmlns + "ProductItem",
                                new XElement(xmlns + "Description", item.InvoiceTax == 0m ? "水果" : (item.OrderIsPponitem ? MaxLengthString(item.OrderItem, 256) : "17Life購物金")), //品名
                                new XElement(xmlns + "Quantity", 1), //數量
                                new XElement(xmlns + "UnitPrice", (int)(item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? salesamount : item.OrderAmount)), //單價
                                new XElement(xmlns + "Amount", (int)(item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? salesamount : item.OrderAmount)), //金額
                                new XElement(xmlns + "SequenceNumber", 1) //明細項次
                                ));
        }

        private static XElement ReturnC0401Details(EinvoiceC0401Main item, XNamespace xmlns)
        {
            List<XElement> orderItem = new List<XElement>();
            int sequenceNumber = 1;
            foreach (var detail in item.Details)
            {
                orderItem.Add(new XElement(xmlns + "ProductItem",
                                new XElement(xmlns + "Description", item.InvoiceTax == 0m ? "水果" : (detail.OrderIsPponitem ? MaxLengthString(detail.OrderItem, 256) : "17Life購物金")), //品名
                                new XElement(xmlns + "Quantity", 1), //數量
                                new XElement(xmlns + "UnitPrice", detail.Amount), //單價
                                new XElement(xmlns + "Amount", detail.Amount), //金額
                                new XElement(xmlns + "SequenceNumber", sequenceNumber) //明細項次
                                ));
                sequenceNumber++;
            }

            return new XElement(xmlns + "Details", orderItem
                                );
        }

        public static XElement ReturnC0401Amount(ViewEinvoiceMaindetail item, decimal salesamount, XNamespace xmlns)
        {
            return new XElement(xmlns + "Amount",
                                new XElement(xmlns + "SalesAmount", (int)(item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? salesamount : item.OrderAmount)), //應稅銷售額合計
                                new XElement(xmlns + "FreeTaxSalesAmount", 0), //免稅銷售額合計
                                new XElement(xmlns + "ZeroTaxSalesAmount", 0), //零稅率銷售額合計
                                new XElement(xmlns + "TaxType", item.InvoiceTax == 0m ? 3 : 1), //課稅別 1應稅 :3免稅
                                new XElement(xmlns + "TaxRate", item.InvoiceTax), //稅率 0.05應稅 :免稅
                                new XElement(xmlns + "TaxAmount", (int)(item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? (item.OrderAmount - salesamount) : 0)), //營業稅額
                                new XElement(xmlns + "TotalAmount", (int)(item.OrderAmount)) //總計
                                );
        }

        private static XElement ReturnC0401Amount(EinvoiceC0401Main main, decimal salesamount, XNamespace xmlns)
        {
            return new XElement(xmlns + "Amount",
                                new XElement(xmlns + "SalesAmount", (int)(main.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? salesamount : main.OrderAmount)), //應稅銷售額合計
                                new XElement(xmlns + "FreeTaxSalesAmount", 0), //免稅銷售額合計
                                new XElement(xmlns + "ZeroTaxSalesAmount", 0), //零稅率銷售額合計
                                new XElement(xmlns + "TaxType", main.InvoiceTax == 0m ? 3 : 1), //課稅別 1應稅 :3免稅
                                new XElement(xmlns + "TaxRate", main.InvoiceTax), //稅率 0.05應稅 :免稅
                                new XElement(xmlns + "TaxAmount", (int)(main.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? (main.OrderAmount - salesamount) : 0)), //營業稅額
                                new XElement(xmlns + "TotalAmount", (int)(main.OrderAmount)) //總計
                                );
        }
        #endregion

        #region 新版 : D0401折讓

        public static XDocument ReturnD0401(ViewEinvoiceMaindetail item, DateTime now, decimal salesamount, string allowanceNumber)
        {
            KeyValuePair<XNamespace, string> xml_namespaces = GetXmlNameSpace(EinvoiceType.D0401);
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XDocument(new XDeclaration("1.0", "utf-8", "true"),
                        new XElement(xml_namespaces.Key + "Allowance",
                        new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                        new XAttribute(xsi + "schemaLocation", xml_namespaces.Value),
                       ReturnD0401Main(item, item.DetailTransTime.Value, xml_namespaces.Key, allowanceNumber), ReturnD0401Details(item, now, salesamount, xml_namespaces.Key), ReturnD0401Amount(item, salesamount, xml_namespaces.Key))
                   );
        }

        public static XElement ReturnD0401Main(ViewEinvoiceMaindetail item, DateTime now, XNamespace xmlns, string allowanceNumber)
        {
            return new XElement(xmlns + "Main",
                        new XElement(xmlns + "AllowanceNumber", allowanceNumber), //折讓證明單號碼 16碼內 不得重複
                        new XElement(xmlns + "AllowanceDate", now.ToString("yyyyMMdd")), //折讓證明單日期
                          new XElement(xmlns + "Seller", new XElement(xmlns + "Identifier", Com_Id), new XElement(xmlns + "Name", Com_Name)), //賣方資訊
                          new XElement(xmlns + "Buyer", //買方資訊
                                 new XElement(xmlns + "Identifier", item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? item.InvoiceComId : "00000000"), //三聯式填入統編，二聯式填入8個0
                                 new XElement(xmlns + "Name", item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? MaxLengthString(item.InvoiceComName, 60) : item.UserId.ToString())), //三聯式填入公司抬頭，二聯式填入買方姓名
                          new XElement(xmlns + "AllowanceType", "2") //折讓種類 2賣方折讓證明單通知 ； 1買方開立折讓證明單
                        );
        }

        public static XElement ReturnD0401Details(ViewEinvoiceMaindetail item, DateTime now, decimal salesamount, XNamespace xmlns)
        {
            return new XElement(xmlns + "Details",
                        new XElement(xmlns + "ProductItem",
                            new XElement(xmlns + "OriginalInvoiceDate", ReturnInvoiceTime(item.InvoiceNumberTime.Value, item.VerifiedTime.Value).ToString("yyyyMMdd")), //原發票日期
                            new XElement(xmlns + "OriginalInvoiceNumber", item.InvoiceNumber), //原發票號碼
                            new XElement(xmlns + "OriginalDescription", item.InvoiceTax == 0m ? "水果" : (item.OrderIsPponitem ? MaxLengthString(item.OrderItem, 256) : "17Life購物金")), //原品名
                            new XElement(xmlns + "Quantity", 1), //數量
                            new XElement(xmlns + "UnitPrice", (int)(item.OrderAmount)), //單價
                            new XElement(xmlns + "Amount", (int)Math.Round(item.InvoiceTax > 0 ? salesamount : item.DetailInvoiceAmount, 0, MidpointRounding.AwayFromZero)), //金額 未稅的折讓金額
                            new XElement(xmlns + "Tax", item.InvoiceTax > 0 ? (int)Math.Round((item.DetailInvoiceAmount - salesamount), 0, MidpointRounding.AwayFromZero) : 0), //營業稅額
                            new XElement(xmlns + "AllowanceSequenceNumber", 1), //折讓證明單明細序號
                            new XElement(xmlns + "TaxType", item.InvoiceTax == 0m ? 3 : 1) //課稅別
                            ));
        }

        public static XElement ReturnD0401Amount(ViewEinvoiceMaindetail item, decimal salesamount, XNamespace xmlns)
        {
            return new XElement(xmlns + "Amount",
                            new XElement(xmlns + "TaxAmount", item.InvoiceTax > 0 ? (int)Math.Round((item.DetailInvoiceAmount - salesamount), 0, MidpointRounding.AwayFromZero) : 0), //營業稅額合計
                            new XElement(xmlns + "TotalAmount", (int)Math.Round(item.InvoiceTax > 0 ? salesamount : item.DetailInvoiceAmount, 0, MidpointRounding.AwayFromZero)) //金額(不含稅之進貨額)
                            );
        }

        #endregion

        #region 手動開立作廢折讓、作廢發票
        #region 新版 : C0501作廢發票

        public static XDocument ReturnC0501(ViewEinvoiceMaindetail item, DateTime now)
        {
            KeyValuePair<XNamespace, string> xml_namespaces = GetXmlNameSpace(EinvoiceType.C0501);
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XDocument(new XDeclaration("1.0", "utf-8", "true"),
                   new XElement(xml_namespaces.Key + "CancelInvoice",
                   new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                   new XAttribute(xsi + "schemaLocation", xml_namespaces.Value),
                    new XElement(xml_namespaces.Key + "CancelInvoiceNumber", item.InvoiceNumber), //作廢發票號碼
                    new XElement(xml_namespaces.Key + "InvoiceDate", ReturnInvoiceTime(item.InvoiceNumberTime.Value, item.VerifiedTime.Value).ToString("yyyyMMdd")), //發票日期
                    new XElement(xml_namespaces.Key + "BuyerId", item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? item.InvoiceComId : "00000000"), //買方統一編號
                    new XElement(xml_namespaces.Key + "SellerId", Com_Id), //賣方統一編號
                    new XElement(xml_namespaces.Key + "CancelDate", now.ToString("yyyyMMdd")), //作廢時間
                    new XElement(xml_namespaces.Key + "CancelTime", now.ToString("HH:mm:ss")), //作廢時間
                    new XElement(xml_namespaces.Key + "CancelReason", "退貨發票作廢") //作廢原因
                   ));
        }

        public static XDocument ReturnC0501(EinvoiceMain item)
        {
            KeyValuePair<XNamespace, string> xml_namespaces = GetXmlNameSpace(EinvoiceType.C0501);
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XDocument(new XDeclaration("1.0", "utf-8", "true"),
                   new XElement(xml_namespaces.Key + "CancelInvoice",
                   new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                   new XAttribute(xsi + "schemaLocation", xml_namespaces.Value),
                    new XElement(xml_namespaces.Key + "CancelInvoiceNumber", item.InvoiceNumber), //作廢發票號碼
                    new XElement(xml_namespaces.Key + "InvoiceDate", ReturnInvoiceTime(item.InvoiceNumberTime.Value, item.VerifiedTime.Value).ToString("yyyyMMdd")), //發票日期
                    new XElement(xml_namespaces.Key + "BuyerId", item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? item.InvoiceComId : "00000000"), //買方統一編號
                    new XElement(xml_namespaces.Key + "SellerId", Com_Id), //賣方統一編號
                    new XElement(xml_namespaces.Key + "CancelDate", DateTime.Now.ToString("yyyyMMdd")), //作廢時間
                    new XElement(xml_namespaces.Key + "CancelTime", DateTime.Now.ToString("HH:mm:ss")), //作廢時間
                    new XElement(xml_namespaces.Key + "CancelReason", "退貨發票作廢") //作廢原因
                   ));
        }
        #endregion

        #region 新版 : D0501作廢折讓

        public static XDocument ReturnD0501(ViewEinvoiceMaindetail item)
        {
            int d_count = op.EinvoiceDetailGetCount(item.Id, (int)EinvoiceType.D0401);
            KeyValuePair<XNamespace, string> xml_namespaces = GetXmlNameSpace(EinvoiceType.D0501);
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XDocument(new XDeclaration("1.0", "utf-8", "true"),
                 new XElement(xml_namespaces.Key + "CancelAllowance",
                     new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                   new XAttribute(xsi + "schemaLocation", xml_namespaces.Value),
                  new XElement(xml_namespaces.Key + "CancelAllowanceNumber", "17P" + (d_count.ToString().PadLeft(2, '0')) + (item.DetailId.ToString().PadLeft(11, '0'))),  //作廢折讓單號碼
                  new XElement(xml_namespaces.Key + "AllowanceDate", item.InvoicedTime.Value.ToString("yyyyMMdd")), //折讓證明單日期
                  new XElement(xml_namespaces.Key + "BuyerId", item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? item.InvoiceComId : "00000000"), //買方統一編號
                  new XElement(xml_namespaces.Key + "SellerId", Com_Id), //賣方統一編號
                  new XElement(xml_namespaces.Key + "CancelDate", DateTime.Now.ToString("yyyyMMdd")), //作廢時間
                  new XElement(xml_namespaces.Key + "CancelTime", DateTime.Now.ToString("HH:mm:ss")), //作廢時間
                  new XElement(xml_namespaces.Key + "CancelReason", "作廢折讓單")
              ));
        }

        #endregion

        //後台2聯改3聯產生作廢
        public static void GenerateCancelFiles(int id, int type, string createId)
        {
            if (type == (int)EinvoiceType.C0501 || type == (int)EinvoiceType.D0401)
            {
                DateTime now = DateTime.Now;
                EinvoiceMain data = op.EinvoiceMainGetById(id);
                string datecode = ReturnInvoiceTime(data.InvoiceNumberTime.Value, data.VerifiedTime.Value).ToString("yyyyMM");

                EinvoiceDetail detail = new EinvoiceDetail();
                detail.MainId = data.Id;    //發票主檔ID
                detail.InvoiceAmount = data.InvoiceSumAmount;    //刷退金額
                detail.InvoiceType = type;   //發票類型(作廢)
                detail.Creator = createId;  //建檔人
                detail.CreateTime = now;    //建檔時間
                detail.TransTime = now;
                detail.Message = data.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? "變更抬頭作廢重開" : (type == (int)EinvoiceType.C0501) ? "二聯改三聯作廢" : "二聯改三聯折讓";    //訊息
                op.EinvoiceSetDetail(detail);

                op.EinvoiceUpdateSum(data.OrderAmount, data.Id, type);  //更新主檔加總金額

                EinvoiceChangeLog log = new EinvoiceChangeLog();
                log.MainId = data.Id;
                log.OrderGuid = data.OrderGuid;
                log.CreateId = createId;
                log.CreateTime = now;
                log.ActionType = string.IsNullOrEmpty(data.LoveCode) ? (int)EinvoiceAction.Invoice2To3 : (int)EinvoiceAction.DonateTo3;
                op.EinvoiceChangeLogSet(log);

                SetJobMessage(datecode, string.Format("發票種類:{0}，發票號碼:{1}，總金額:{2}", (type == (int)EinvoiceType.D0401) ? "D0401折讓發票" :
                    "C0501作廢發票", data.InvoiceNumber, data.OrderAmount), (type == (int)EinvoiceType.D0401) ? EinvoiceType.D0401 : EinvoiceType.C0501);
            }
        }

        #endregion 手動開立作廢折讓、作廢發票

        #region 新版 : C0701註銷發票
        public static XDocument ReturnC0701(ViewEinvoiceMaindetail item, string voidReason = "")
        {
            KeyValuePair<XNamespace, string> xml_namespaces = GetXmlNameSpace(EinvoiceType.C0701);
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XDocument(new XDeclaration("1.0", "utf-8", "true"),
                    new XElement(xml_namespaces.Key + "VoidInvoice",
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                    new XAttribute(xsi + "schemaLocation", xml_namespaces.Value),
                    new XElement(xml_namespaces.Key + "VoidInvoiceNumber", item.InvoiceNumber),
                    new XElement(xml_namespaces.Key + "InvoiceDate", ReturnInvoiceTime(item.InvoiceNumberTime.Value, item.VerifiedTime.Value).ToString("yyyyMMdd")),
                    new XElement(xml_namespaces.Key + "BuyerId", item.InvoiceMode2 == ((int)InvoiceMode2.Triplicate) ? item.InvoiceComId : "00000000"),
                    new XElement(xml_namespaces.Key + "SellerId", Com_Id),
                    new XElement(xml_namespaces.Key + "VoidDate", DateTime.Now.ToString("yyyyMMdd")),
                    new XElement(xml_namespaces.Key + "VoidTime", DateTime.Now.ToString("HH:mm:ss")),
                    new XElement(xml_namespaces.Key + "VoidReason", voidReason == "" ? "開立錯誤，註銷重開" : voidReason)
                    )
                );
        }

        #endregion

        #region 產生紙本發票

        #region 作廢折讓流程
        /// <summary>
        /// 發生反核銷時取消發票
        /// </summary>
        /// <param name="oGuid"></param>
        /// <param name="couponId"></param>
        public static void CancelEinvoiceSetByUndo(Guid oGuid, int couponId)
        {
            //todo:已開立發票的作廢流程待補? 會計表示不想重開，取消核銷有可能是店家問題，消費者不一定要配合寄回折讓單
            //那對我們而言 原本那張發票改開成購物金形式
            try
            {
                EinvoiceMainCollection emc = op.EinvoiceMainCollectionGet(EinvoiceMain.OrderGuidColumn.ColumnName, oGuid.ToString(), true);
                EinvoiceMain em = emc.FirstOrDefault(x => x.CouponId == couponId && x.InvoiceStatus == (int)EinvoiceType.C0401);
                if (string.IsNullOrEmpty(em.InvoiceNumber))
                {
                    em.VerifiedTime = null;
                }
                else
                {
                    //已開立發票走折讓or作廢+新開立
                    //EinvoiceMain newInv = new EinvoiceMain();
                    //newInv = em.Clone();
                    //newInv.Id = 0;
                    //newInv.InvoiceNumber = null;
                    //newInv.InvoiceNumberTime = null;
                    //newInv.VerifiedTime = newInv.InvoiceRequestTime = null;
                    //newInv.InvoiceSerialId = 0;
                    //newInv.InvoiceFileSerial = 0;
                    //newInv.Message = "取消核銷";

                    //op.EinvoiceSetMain(newInv);

                    //int einvType = IsInvoiceIntertemporal(em) ? (int)EinvoiceType.D0401 : (int)EinvoiceType.C0501; //跨期折讓 當期作廢

                    //detail 作廢方式 -> 開立一筆InvType為作廢的Detail 開立發票號碼時會再創建一筆新的mainId的Detail
                    //EinvoiceFacade.GenerateCancelFiles(inv.Id, einvType); //含作廢Main

                    //string InValidMark = ((einvType == (int)EinvoiceType.D0401) ? "折讓發票:" : "作廢發票:") + inv.InvoiceNumber;
                    //CommonFacade.AddAudit(View.OrderId, AuditType.Order, InValidMark, View.CurrentUser, true);
                }
                op.EinvoiceSetMain(em);
            }
            catch
            {

            }
        }
        #endregion 作廢折讓流程
        public static Dictionary<string, string> GenerateInvoiceForPrint(DateTime date_start, DateTime date_end, InvoiceMode2 invoice_type,bool isCopy,bool hasWinning,string creator,string fileName, InvoiceQueryOption query)
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            DateTime now = DateTime.Now;
            int d_count = 0;
            decimal d_amount = 0;
            EinvoiceMainCollection data = op.EinvoicePaperCollectionGetByDate(date_start, date_end, invoice_type, isCopy,hasWinning, query);
            var partnerData = op.PartnerEinvoicePaperCollectionGetByDate(date_start, date_end, invoice_type, isCopy, hasWinning, query).Select(x => new EinvoiceTransModel(x));

            var pezData = data.Where(x => x.InvoiceNumberTime >= config.ContactToPayeasy && x.InvoiceNumberTime < config.PayeasyToContact).OrderBy(x => x.InvoiceBuyerAddress).Select(x => new EinvoiceTransModel(x));
            var e7Data = data.Where(x => x.InvoiceNumberTime >= config.PayeasyToContact).Select(x => new EinvoiceTransModel(x));

            List<EinvoiceTransModel> contactData = new List<EinvoiceTransModel>();
            contactData.AddRange(e7Data);
            contactData.AddRange(partnerData);

            Dictionary<string, IEnumerable<EinvoiceTransModel>> d2 = new Dictionary<string, IEnumerable<EinvoiceTransModel>>();
            d2.Add(BillBuyerType.PayEasy.ToString(), pezData);
            d2.Add(BillBuyerType.Contact.ToString(), contactData.OrderBy(x => x.InvoiceBuyerAddress));
          
            foreach (KeyValuePair<string, IEnumerable<EinvoiceTransModel>> c_item in d2)
            {
                XDocument xml_doc = new XDocument(new XDeclaration("1.0", "utf-8", "true"));
                XElement root = new XElement("PaperInvoice");
                if (c_item.Value.ToList().Count > 0)
                {
                    foreach (EinvoiceTransModel item in c_item.Value)
                    {
                        string serNo = now.ToString("yyyyMMdd") + d_count.ToString().PadLeft(4, '0');
                        root.Add(item.PaperInvoiceElement(serNo, isCopy));
                        if (pezData.Any(x => x.InvoiceNumber == item.InvoiceNumber) || e7Data.Any(x => x.InvoiceNumber == item.InvoiceNumber))
                        {
                            if (!isCopy)
                            {
                                op.EinvoiceUpdatePaperTime(item.Id, now);
                            }
                            item.CopyPrint = isCopy;
                            var log = GetEinvoiceChangeLogCollection(item, creator, now);
                            op.EinvoiceChangeLogCollectionSet(log);
                        }
                        else if (partnerData.Any(x => x.InvoiceNumber == item.InvoiceNumber))
                        {
                            if (!isCopy)
                            {
                                op.PartnerEinvoiceUpdatePaperTime(item.InvoiceNumber, now);
                            }
                        }
                        d_count++;
                        d_amount += item.OrderAmount;
                    }
                    xml_doc.Add(root);
                    if (data.Count > 0)
                    {
                        // xml_doc.Save(@"C:\eInvoiceText\" + invoice_type + date_start.ToString("-MMdd") + date_end.ToString("-MMdd") + now.ToString("-yyyyMMdd-HHmmss") + ".xml");
                        SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，共:{3}筆，總金額:{4}，主機:{5}",
                             ((int)invoice_type + "紙本列印發票" + (isCopy ? "[副本]" : string.Empty)), date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), d_count, d_amount, Environment.MachineName), (dynamic)invoice_type, creator, !isCopy ? fileName : string.Empty);
                    }
                    list.Add(c_item.Key + ".xml", xml_doc.ToString());
                }
            }
            return list;
        }

        public static Dictionary<string, string> GenerateVendorInvoiceForPrint(DateTime date_start, DateTime date_end, InvoiceMode2 invoice_type, bool isCopy, string creator, string fileName)
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");
            DateTime now = DateTime.Now;
            int d_count = 0;
            decimal d_amount = 0;
            VendorEinvoiceMainCollection data = op.VendorEinvoicePaperCollectionGetByDate(date_start, date_end, invoice_type, isCopy);

            Dictionary<string, IEnumerable<VendorEinvoiceMain>> d2 = new Dictionary<string, IEnumerable<VendorEinvoiceMain>>();
            d2.Add("Vendor", data);

            foreach (KeyValuePair<string, IEnumerable<VendorEinvoiceMain>> c_item in d2)
            {
                XDocument xml_doc = new XDocument(new XDeclaration("1.0", "utf-8", "true"));
                XElement root = new XElement("PaperInvoice");
                if (c_item.Value.ToList().Count > 0)
                {
                    foreach (VendorEinvoiceMain vem in c_item.Value)
                    {
                        string serNo = now.ToString("yyyyMMdd") + d_count.ToString().PadLeft(4, '0');
                        var item = new EinvoiceTransModel(vem);

                        root.Add(item.PaperInvoiceElement(serNo, isCopy));
                        //注意稅差 要用總額扣
                        if (vem.ShippingFee + vem.OverdueAmount + vem.WmsAmount > 0)
                        {
                            var orderAmountNoTax = Math.Round(vem.OrderAmount / (1 + item.InvoiceTax), 0);
                            var shippingFeeNoTax = vem.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? Math.Round(vem.ShippingFee / (1 + item.InvoiceTax), 0) : vem.ShippingFee;
                            var overdueAmountNoTax = vem.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? Math.Round(vem.OverdueAmount / (1 + item.InvoiceTax), 0) : vem.OverdueAmount;
                            var wmsAmountNoTax = vem.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? Math.Round(vem.WmsAmount / (1 + item.InvoiceTax), 0) : vem.WmsAmount;
                            var dic = new Dictionary<string, decimal>();
                            dic.Add("shippingFee", shippingFeeNoTax);
                            dic.Add("overdueAmount", overdueAmountNoTax);
                            dic.Add("wmsAmount", wmsAmountNoTax);
                            if (dic.Sum(x => x.Value) != orderAmountNoTax)
                            {
                                var maxValue = dic.Values.Max();
                                dic[dic.Aggregate((x, y) => x.Value > y.Value ? x : y).Key] = orderAmountNoTax - (dic.Sum(x => x.Value) - maxValue);
                            }
                            root.Elements("Invoice").Descendants().Last().AddAfterSelf(
                               new XElement("ShippingFee", dic["shippingFee"].ToString("0.##")),
                               new XElement("OverdueAmount", dic["overdueAmount"].ToString("0.##")),
                               new XElement("WmsAmount", dic["wmsAmount"].ToString("0.##"))
                           );
                        }

                        if (!isCopy)
                        {
                            vem.InvoicePaperedTime = now;
                            vem.InvoicePapered = true;
                            op.VendorEinvoiceMainSet(vem);
                        }

                        d_count++;
                        d_amount += vem.OrderAmount;
                    }
                    xml_doc.Add(root);
                    if (data.Count > 0)
                    {
                        // xml_doc.Save(@"C:\eInvoiceText\" + invoice_type + date_start.ToString("-MMdd") + date_end.ToString("-MMdd") + now.ToString("-yyyyMMdd-HHmmss") + ".xml");
                        SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，共:{3}筆，總金額:{4}，主機:{5}",
                             ("廠商對開發票" + (isCopy ? "[副本]" : string.Empty)), date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), d_count, d_amount, Environment.MachineName), (dynamic)invoice_type, creator, !isCopy ? fileName : string.Empty);
                    }
                    list.Add(c_item.Key + ".xml", xml_doc.ToString());
                }
            }

            return list;
        }

        public static string GenerateInvoiceMedia(DateTime date_start, DateTime date_end)
        {
            string datecode = (date_start.Month % 2 == 0 ? date_start.AddMonths(-1) : date_start).ToString("yyyyMM");

            int d_count = 0;
            decimal d_amount = 0;
            EinvoiceMainCollection data = op.EinvoicePaperMediaCollectionGetByDate(date_start, date_end);
            StringBuilder sb = new StringBuilder();
            foreach (EinvoiceMain item in data)
            {
                d_count++;
                decimal salesamount = (Math.Round((item.OrderAmount / (1 + item.InvoiceTax)), 0));
                sb.Append(
                    "31" +
                    "150305400" +
                    d_count.ToString().PadLeft(7, '0') +
                    (item.InvoiceNumberTime.Value.Year - 1911) + item.InvoiceNumberTime.Value.ToString("MM") +
                    item.InvoiceComId.PadLeft(8, ' ') +
                    Com_Id +
                    item.InvoiceNumber +
                    ((item.InvoiceMode2 == (int)InvoiceMode2.Triplicate) ? (salesamount).ToString().PadLeft(12, '0') : ((int)item.OrderAmount).ToString().PadLeft(12, '0')) +
                    (item.InvoiceTax == 0m ? "3" : "1") +
                    ((item.InvoiceMode2 == (int)InvoiceMode2.Triplicate) ? ((int)(item.OrderAmount - salesamount)).ToString().PadLeft(10, '0') : (0).ToString().PadLeft(10, '0')) +
                    string.Empty.PadLeft(9, ' ') +
                   "\r\n"
                    );
                d_amount += item.OrderAmount;
            }

            using (StreamWriter writer = new StreamWriter(@"C:\eInvoiceText\" + EinvoiceType.Paper3Einvoice + "Media" + date_start.ToString("-MMdd") + DateTime.Now.ToString("-yyyyMMdd-HHmmss") + ".txt", false, Encoding.GetEncoding(950)))
            {
                writer.Write(sb.ToString());
                SetJobMessage(datecode, string.Format("發票種類:{0}，資料間隔:{1}~{2}，共:{3}筆，總金額:{4}",
                    "三聯發票媒體檔", date_start.ToString("yyyy/MM/dd"), date_end.ToString("yyyy/MM/dd"), d_count, d_amount), EinvoiceType.Paper3EinvoiceMedia);
            }
            return sb.ToString();
        }

        public static string GenerateInvoiceForPrintByNumber(string[] list, bool isPrinted, string userId = "sys", bool isCopy = false)
        {
            DateTime now = DateTime.Now;

            XDocument xml_doc = new XDocument(new XDeclaration("1.0", "utf-8", "true"));
            XElement root = new XElement("PaperInvoice");
            int d_count = 1;
            foreach (string num in list)
            {
                EinvoiceMain item = op.EinvoiceMainGet(num);
                string serNo = now.ToString("yyyyMMdd") + d_count.ToString().PadLeft(4, '0');
                if (item != null && item.IsLoaded && (item.InvoiceRequestTime != null || item.InvoiceWinnerresponseTime != null) && (isPrinted || (!isPrinted && !item.InvoicePapered)))
                {
                    root.Add(PaperInvoiceElement(item, serNo, isCopy));
                    if (!isCopy)
                    {
                        item.InvoicePaperedTime = now;
                        op.EinvoiceUpdatePaperTime(item.Id, now);
                    }
                    item.CopyPrint = isCopy;
                    item.SinglePrint = true;
                    var log = GetEinvoiceChangeLogCollection(item, userId, now);
                    op.EinvoiceChangeLogCollectionSet(log);
                }
                else
                {
                    var vem = op.VendorEinvoiceMainGet(num);
                    if (vem != null && vem.IsLoaded && (isPrinted || (!isPrinted && !vem.InvoicePapered)))
                    {
                        root.Add(new EinvoiceTransModel(vem).PaperInvoiceElement(serNo, isCopy));

                        //注意稅差 要用總額扣
                        if (vem.ShippingFee + vem.OverdueAmount + vem.WmsAmount > 0)
                        {
                            var orderAmountNoTax = Math.Round(vem.OrderAmount / (1 + item.InvoiceTax), 0);
                            var shippingFeeNoTax = vem.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? Math.Round(vem.ShippingFee / (1 + item.InvoiceTax), 0) : vem.ShippingFee;
                            var overdueAmountNoTax = vem.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? Math.Round(vem.OverdueAmount / (1 + item.InvoiceTax), 0) : vem.OverdueAmount;
                            var wmsAmountNoTax = vem.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? Math.Round(vem.WmsAmount / (1 + item.InvoiceTax), 0) : vem.WmsAmount;
                            var dic = new Dictionary<string, decimal>();
                            dic.Add("shippingFee", shippingFeeNoTax);
                            dic.Add("overdueAmount", overdueAmountNoTax);
                            dic.Add("wmsAmount", wmsAmountNoTax);
                            if (dic.Sum(x=>x.Value) != orderAmountNoTax)
                            {
                                var maxValue = dic.Values.Max();
                                dic[dic.Aggregate((x, y) => x.Value > y.Value ? x : y).Key] = orderAmountNoTax - (dic.Sum(x => x.Value) - maxValue);
                            }
                            root.Elements("Invoice").Descendants().Last().AddAfterSelf(
                               new XElement("ShippingFee", dic["shippingFee"].ToString("0.##")),
                               new XElement("OverdueAmount", dic["overdueAmount"].ToString("0.##")),
                               new XElement("WmsAmount", dic["wmsAmount"].ToString("0.##"))
                           );
                        }

                        if (!isCopy)
                        {
                            vem.InvoicePaperedTime = now;
                            vem.InvoicePapered = true;
                            op.VendorEinvoiceMainSet(vem);
                        }
                    }
                    else
                    {
                        var pem = op.PartnerEinvoiceMainGet(num);
                        if (pem != null && pem.IsLoaded && (pem.InvoiceRequestTime != null || pem.InvoiceWinnerresponseTime != null) && (isPrinted || (!isPrinted && !pem.InvoicePapered)))
                        {
                            root.Add(new EinvoiceTransModel(pem).PaperInvoiceElement(serNo, isCopy));
                            if (!isCopy)
                            {
                                pem.InvoicePaperedTime = now;
                                pem.InvoicePapered = true;
                                op.PartnerEinvoiceMainSet(pem);
                            }
                        }
                    }
                }
                d_count += 1;
            }
            xml_doc.Add(root);
            string path = @"C:\eInvoiceText\";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            xml_doc.Save(@"C:\eInvoiceText\RePrintEinvoice" + DateTime.Now.ToString("-yyyyMMdd-HHmmss") + ".xml");
            return xml_doc.ToString();
        }

        public static string WithdrawInvoicePaperByNumber(string[] list, string userId = "sys")
        {
            int updateCount = 0;
            Dictionary<string, string> ff = new Dictionary<string, string>();
            DateTime now = DateTime.Now;
            foreach (string num in list.Distinct())
            {
                EinvoiceMain item = op.EinvoiceMainGet(num);
                if (item.IsLoaded)
                {
                    if (item.InvoiceMode2 == (int)InvoiceMode2.Triplicate && item.InvoicePapered)
                    {
                        ReturnForm rf = op.ReturnFormGetListByOrderGuid(item.OrderGuid).OrderByDescending(x => x.CreateTime).FirstOrDefault();
                        if (rf != null && rf.ProgressStatus != (int)ProgressStatus.Canceled && item.InvoiceRequestTime == null)
                        {
                            item.InvoicePaperedTime = null;
                            item.InvoicePapered = false;
                            updateCount += 1;
                            SetEinvoiceMainWithLog(item, userId, now);
                            rf.CreditNoteType = (int)AllowanceStatus.None;
                            op.ReturnFormSet(rf);
                        }
                        else
                        {
                            ff.Add(item.InvoiceNumber, "noReturn");
                        }
                    }
                    else
                    {
                        ff.Add(item.InvoiceNumber, item.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? "noPrint" : "notTrip");
                    }
                }
                else
                {
                    ff.Add(num, "noNum");
                }
            }

            string notTrip = ff.Any(x => x.Value == "notTrip") ? string.Format("<br/>以下發票號碼不為三聯式發票，將不做狀態更新：<span style='color: blue;'>{0}</span>", string.Join("、", ff.Where(x => x.Value == "notTrip").Select(s => s.Key))) : "";
            string noPrint = ff.Any(x => x.Value == "noPrint") ? string.Format("<br/>以下發票號碼未印製，將不做狀態更新：<span style='color: blue;'>{0}</span>", string.Join("、", ff.Where(x => x.Value == "noPrint").Select(s => s.Key))) : "";
            string noReturn = ff.Any(x => x.Value == "noReturn") ? string.Format("<br/>此發票不在退貨單上，無法抽回發票：<span style='color: blue;'>{0}</span>", string.Join("、", ff.Where(x => x.Value == "noReturn").Select(s => s.Key))) : "";
            string noNum = ff.Any(x => x.Value == "noNum") ? string.Format("<br/>無此發票號碼，請重新檢查：<span style='color: blue;'>{0}</span>", string.Join("、", ff.Where(x => x.Value == "noNum").Select(s => s.Key))) : "";

            string result = string.Format("已抽回並更新狀態數: <span style='color: blue;'>{0}</span>筆{1}{2}{3}{4}", updateCount.ToString(), notTrip, noPrint, noReturn, noNum);

            return result;
        }

        public static XElement PaperInvoiceElement(EinvoiceMain item, string serNo, bool isCopy = false,bool isVendor = false)
        {
            bool isTriplicate = item.InvoiceMode2 == (int)InvoiceMode2.Triplicate;
            int index = item.OrderItem.IndexOf('X') + 1;
            decimal salesamount = Math.Round((item.OrderAmount / (1 + item.InvoiceTax)), 0);
            XElement invoice = new XElement("Invoice",
                new XElement("SerNo", serNo),
                new XElement("InvoiceNumber", item.InvoiceNumber),
                new XElement("InvoiceNumberTime", ReturnInvoiceTime(item.InvoiceNumberTime.Value, item.VerifiedTime.Value).ToString("yyyy/MM/dd HH:mm:ss")),
                                new XElement("InvoiceBuyerName", isTriplicate ? item.InvoiceComName : string.Empty),
                new XElement("InvoiceComName", isTriplicate ? item.InvoiceComName : string.Empty),
                new XElement("InvoiceComId", isTriplicate ? item.InvoiceComId : string.Empty),
                new XElement("IsTriplicate", isTriplicate),
                new XElement("ItemName", (item.OrderIsPponitem ? item.OrderItem : string.Format("17Life購物金-{0}",
                    item.OrderItem.Substring(index, item.OrderItem.Length - index)))),
                new XElement("InvoiceTax", item.InvoiceTax),
                new XElement("InvoicePass", item.InvoicePass),
                new XElement("SaleAmount", isTriplicate ? salesamount.ToString("0.##") : item.OrderAmount.ToString("0.##")),
                new XElement("TaxAmount", isTriplicate ? (item.OrderAmount - salesamount).ToString("0.##") : "0"),
                new XElement("Amount", item.OrderAmount.ToString("0.##")),
                new XElement("OrderId", item.OrderId),
                new XElement("OrderItem", item.OrderItem),
                new XElement("Receiver", item.InvoiceBuyerName),
                new XElement("Address", item.InvoiceBuyerAddress),
                new XElement("CreditCardTail", item.CreditCardTail),
                new XElement("IsCopy", isCopy ? "1" : "0"),
                new XElement("IsVendor", isVendor ? "1" : "0")
                );
            return invoice;

        }
        #endregion 產生紙本發票

        #region 中獎發票設定

        public static void SetEinvoiceWinningStatus(DateTime d_start, DateTime d_end, string number)
        {
            op.EinvoiceMainCollectionSetWinningStatus(d_start, d_end, number);
        }

        #endregion 中獎發票設定

        #region 發信
        public static string SendEinvoiceWinnerEmail(DateTime date_start, DateTime date_end, bool toFtp, EinvoiceMailType mailtype, bool hasconfirmed, InvoiceVersion invoiceVersion = InvoiceVersion.Old)
        {
            int i = 0;
            DateTime now = DateTime.Now;
            string filename = string.Empty;
            string mailname = string.Empty;
            StringBuilder sb = new StringBuilder();

            if (mailtype == EinvoiceMailType.EvincoeWinnerMail)
            {
                filename = invoiceVersion == InvoiceVersion.Old ? "einvoicewinnermail" : "einvoicecarrierwinnermail";
                mailname = "中獎";
                ViewEinvoiceMailCollection data = op.EinvoiceWinnerMailCollectionGet(date_start, date_end);
                //ViewPponDealCollection deals_allcity = OrderFacade.GetRandomTodayPponDeal(3, PponCityGroup.DefaultPponCityGroup.AllCountry.CityId);
                foreach (var outitem in data.Where(x => x.Version == (int)invoiceVersion && (hasconfirmed ? (!x.InvoiceWinnerresponseTime.HasValue) : (true))).GroupBy(x => x.BusinessHourGuid))
                {
                    foreach (var item in outitem)
                    {
                        i++;
                        decimal saleamount = Math.Round((item.OrderAmount / (1 + item.InvoiceTax)), 0);
                        string order_amount = (item.InvoiceMode2 == (int)InvoiceMode2.Triplicate) ? saleamount.ToString("F0") : item.OrderAmount.ToString("F0");
                        sb.AppendLine(item.MemberEmail + "," + NoComma(item.Cname) + "," + item.InvoiceNumber + "," + item.InvoiceNumberTime.Value.AddYears(-1911).ToString("yyyy年").TrimStart(new char[] { '0' }) + item.InvoiceNumberTime.Value.ToString("MM月dd日") + "," +
                             ((item.InvoiceMode2 == (int)InvoiceMode2.Triplicate) ? NoComma(item.InvoiceComName) : NoComma(item.InvoiceBuyerName)) + "," + ( string.IsNullOrEmpty(item.InvoiceBuyerName) ? "請於下方確認頁連結，填寫真實姓名" : "") + "," +
                             NoNull(item.InvoiceComId) + "," + NoComma(item.InvoiceBuyerAddress) + "," + (string.IsNullOrEmpty(item.InvoiceBuyerAddress) ? "請於下方確認頁連結，填寫寄送地址" : "") +  "," + NoComma(item.InvoiceWinnerresponsePhone) + "," +
                             (string.IsNullOrEmpty(item.InvoiceWinnerresponsePhone) ? "請於下方確認頁連結，填寫連絡電話" : "") + "," + item.OrderId + "," +
                             order_amount + "," + NoComma(item.OrderItem) + "," +
                             ((item.InvoiceMode2 == (int)InvoiceMode2.Triplicate) ? (item.OrderAmount - saleamount).ToString("F0") : "0") + "," +
                              (item.InvoiceTax > 0 ? "V" : string.Empty) + "," + (item.InvoiceTax == 0 ? "V" : string.Empty) + "," + item.OrderAmount.ToString("F0") + "," +
                              CommonFacade.GetChineseAmountString((int)item.OrderAmount) + I18N.Phrase.Dollar + "," + NoComma(item.ItemName) + "," + NoComma(item.OrderItem) + "," +
                              config.ContactCompanyId + "," + config.SiteUrl + config.SiteServiceUrl);
                    }
                }

                if (config.EnbaledTaishinEcPayApi)
                {
                    //var partnerData = op.ViewPartnerEinvoiceWinnerMailCollectionGet(date_start, date_end);
                    //foreach (var item in partnerData)
                    //{
                    //    i++;
                    //    decimal saleamount = Math.Round((item.OrderAmount / (1 + item.InvoiceTax)), 0);
                    //    string order_amount = (item.InvoiceMode2 == (int)InvoiceMode2.Triplicate) ? saleamount.ToString("F0") : item.OrderAmount.ToString("F0");
                    //    sb.AppendLine(item.WinnerMail + "," + NoComma(item.InvoiceBuyerName) + "," + item.InvoiceNumber + "," + item.InvoiceNumberTime.Value.AddYears(-1911).ToString("yyyy年").TrimStart(new char[] { '0' }) + item.InvoiceNumberTime.Value.ToString("MM月dd日") + "," +
                    //         ((item.InvoiceMode2 == (int)InvoiceMode2.Triplicate) ? NoComma(item.InvoiceComName) : NoComma(item.InvoiceBuyerName)) + "," + (string.IsNullOrEmpty(item.InvoiceBuyerName) ? "請於下方確認頁連結，填寫真實姓名" : "") + "," +
                    //         NoNull(item.InvoiceComId) + "," + NoComma(item.InvoiceBuyerAddress) + "," + (string.IsNullOrEmpty(item.InvoiceBuyerAddress) ? "請於下方確認頁連結，填寫寄送地址" : "") + "," + NoComma(item.InvoiceWinnerresponsePhone) + "," +
                    //         (string.IsNullOrEmpty(item.InvoiceWinnerresponsePhone) ? "請於下方確認頁連結，填寫連絡電話" : "") + "," + item.PartnerOrderId + "," +
                    //         order_amount + "," + NoComma(item.OrderItem) + "," +
                    //         ((item.InvoiceMode2 == (int)InvoiceMode2.Triplicate) ? (item.OrderAmount - saleamount).ToString("F0") : "0") + "," +
                    //          (item.InvoiceTax > 0 ? "V" : string.Empty) + "," + (item.InvoiceTax == 0 ? "V" : string.Empty) + "," + item.OrderAmount.ToString("F0") + "," +
                    //          CommonFacade.GetChineseAmountString((int)item.OrderAmount) + I18N.Phrase.Dollar + "," + NoComma(item.OrderItem) + "," + NoComma(item.OrderItem) + "," +
                    //          config.ContactCompanyId + "," + config.SiteUrl + config.SiteServiceUrl);
                    //}
                }
               
                #region 產生信件

                using (StreamWriter writer = new StreamWriter(config.InvoiceMailFolder + filename + ".csv", false, Encoding.GetEncoding(950)))
                {
                    writer.Write(sb.ToString());
                }
                sb.Clear();
                sb.AppendLine("[SYMBOL]");
                sb.AppendLine("FIELD00=%%RCPT_ADDR%%");
                sb.AppendLine("FIELD01=%%RCPT_NAME%%");
                sb.AppendLine("FIELD02=%%invoice_number%%");
                sb.AppendLine("FIELD03=%%invoice_number_time%%");
                sb.AppendLine("FIELD04=%%invoice_buyer_name%%");
                sb.AppendLine("FIELD05=%%invoice_buyer_name_warning%%");
                sb.AppendLine("FIELD06=%%invoice_com_id%%");
                sb.AppendLine("FIELD07=%%invoice_buyer_address%%");
                sb.AppendLine("FIELD08=%%invoice_buyer_address_warning%%");
                sb.AppendLine("FIELD09=%%invoice_winnerresponse_phone%%");
                sb.AppendLine("FIELD10=%%invoice_winnerresponse_phone_warning%%");
                sb.AppendLine("FIELD11=%%order_id%%");
                sb.AppendLine("FIELD12=%%order_amount1%%");
                sb.AppendLine("FIELD13=%%order_item%%");
                sb.AppendLine("FIELD14=%%invoice_taxamount%%");
                sb.AppendLine("FIELD15=%%invoice_hastax%%");
                sb.AppendLine("FIELD16=%%invoice_notax%%");
                sb.AppendLine("FIELD17=%%order_amount2%%");
                sb.AppendLine("FIELD18=%%order_camount%%");
                sb.AppendLine("FIELD19=%%item_name%%");
                sb.AppendLine("FIELD20=%%item_name2%%");
                sb.AppendLine("FIELD21=%%company_id%%");
                sb.AppendLine("FIELD22=%%site_service_url%%");
                sb.AppendLine();

                sb.AppendLine("[HEADER]");
                sb.AppendLine("SenderAddr=" + config.ServiceEmail);
                sb.AppendLine("SenderName=17Life");
                sb.AppendLine("RecipientAddr=%%RCPT_ADDR%%");
                sb.AppendLine("RecipientName=%%RCPT_NAME%%");
                sb.AppendLine("Subject=電子發票" + mailname + "通知信");
                sb.AppendLine("ReplyToAddress=" + config.ServiceEmail);
                sb.AppendLine();
                sb.AppendLine("[SCHEDULE]");
                sb.AppendLine("TIME=" + now.ToString("yyyy-MM-dd HH:mm:ss"));
                sb.AppendLine("TASKNAME=" + filename + date_start.ToString("YYYY-MMdd-") + date_end.ToString("MMdd-") + now.ToString("MMddHHmm"));
                using (StreamWriter writer = new StreamWriter(config.InvoiceMailFolder + filename + ".ini", false, Encoding.GetEncoding(950)))
                {
                    writer.Write(sb.ToString());
                }
                string result = string.Empty;
                if (toFtp)
                {
                    result = CommonFacade.FtpCelloPoint("電子發票" + mailname + "通知信", config.InvoiceMailFolder, config.InvoiceMailFolder, config.EmailTemplateDirectory, filename, true);
                }
                return i + "筆" + mailname + "通知信產生。" + result;

                #endregion
            }
            else
            {
                filename = invoiceVersion == InvoiceVersion.Old ? "einvoiceinfomail" : "einvoicecarrierinfomail";
                mailname = "開立";
                IEnumerable<ViewEinvoiceMail> data = op.EinvoiceMailCollectionGet(date_start, date_end).Where(x => x.Version.Equals((int)invoiceVersion));
                foreach (var outitem in data.GroupBy(x => new { x.MemberEmail, x.Cname }))
                {
                    i++;
                    string order_id = string.Empty;
                    var orders = outitem.Where(x => x.MemberEmail.Equals(outitem.Key.MemberEmail)).GroupBy(x => new { x.OrderId, x.OrderGuid, x.OrderClassification });
                    foreach (var od in orders)
                    {
                        order_id += od.Key.OrderClassification.Equals((int)OrderClassification.HiDeal) ?
                            string.Format("<a target='_blank' href='{0}/piinlife/member/CouponList.aspx?oid={1}'>{2}</a>", config.SiteUrl, od.Key.OrderGuid, od.Key.OrderId) :
                            string.Format("<a target='_blank' href='{0}/user/CouponDetail.aspx?oid={1}'>{2}</a>", config.SiteUrl, od.Key.OrderGuid, od.Key.OrderId);
                        order_id += "、";
                    }
                    if (order_id.Length > 0)
                    {
                        order_id = order_id.Substring(0, order_id.Length - 1);
                    }
                    sb.AppendLine(outitem.Key.MemberEmail + "," + outitem.Key.Cname + "," + order_id + "," + config.SiteUrl + "," + config.SiteUrl + config.SiteServiceUrl);
                }

                #region 產生信件

                using (StreamWriter writer = new StreamWriter(config.InvoiceMailFolder + filename + ".csv", false, Encoding.GetEncoding(950)))
                {
                    writer.Write(sb.ToString());
                }
                sb.Clear();
                sb.AppendLine("[SYMBOL]");
                sb.AppendLine("FIELD00=%%RCPT_ADDR%%");
                sb.AppendLine("FIELD01=%%RCPT_NAME%%");
                sb.AppendLine("FIELD02=%%order_id%%");
                sb.AppendLine("FIELD03=%%site_url%%");
                sb.AppendLine("FIELD04=%%site_service_url%%");

                sb.AppendLine();
                sb.AppendLine("[HEADER]");
                sb.AppendLine("SenderAddr=" + config.ServiceEmail);
                sb.AppendLine("SenderName=17Life");
                sb.AppendLine("RecipientAddr=%%RCPT_ADDR%%");
                sb.AppendLine("RecipientName=%%RCPT_NAME%%");
                sb.AppendLine("Subject=發票" + mailname + "通知");
                sb.AppendLine();
                sb.AppendLine("[SCHEDULE]");
                sb.AppendLine("TIME=" + now.ToString("yyyy-MM-dd HH:mm:ss"));
                sb.AppendLine("TASKNAME=" + filename + now.ToString("-yyyy") + date_start.ToString("-MMdd-") + date_end.ToString("MMdd-") + now.ToString("MMddHHmm"));
                using (StreamWriter writer = new StreamWriter(config.InvoiceMailFolder + filename + ".ini", false, Encoding.GetEncoding(950)))
                {
                    writer.Write(sb.ToString());
                }
                string result = string.Empty;
                if (toFtp)
                {
                    result = CommonFacade.FtpCelloPoint("電子發票" + mailname + "通知信", config.InvoiceMailFolder, config.InvoiceMailFolder, config.EmailTemplateDirectory, filename, true);
                }
                return i + "筆" + mailname + "通知信產生。" + result;

                #endregion
            }

        }

        public static void SendEinvoiceWinnerEmail()
        {
            string datecode = DateTime.Now.AddMonths(-2).Month % 2 == 0 ? DateTime.Now.AddMonths(-3).ToString("yyyyMM") : DateTime.Now.AddMonths(-2).ToString("yyyyMM");
            var es = op.EinvoiceSerialCollectionGetByDate(datecode).FirstOrDefault();
            if (es != null && es.WinnerMailSendStatus == (int)WinnerMailSendStatus.Ready)
            {
                bool mailToFtp = true;
                bool mailHasConfirmed = false; //中獎人預帶會壓true，但還是要通知
                string result = string.Empty;
                try
                {
                    string carrierVersion = SendEinvoiceWinnerEmail(es.StartDate, es.EndDate, mailToFtp, EinvoiceMailType.EvincoeWinnerMail, mailHasConfirmed, InvoiceVersion.CarrierEra);
                    es.WinnerMailSendStatus = (int)WinnerMailSendStatus.Success;
                    result = string.Format("新制發票載具- {0}", carrierVersion);
                }
                catch (Exception ex)
                {
                    es.WinnerMailSendStatus = (int)WinnerMailSendStatus.Faild;
                    result = ex.Message;

                    MailMessage msg = new MailMessage();
                    msg.IsBodyHtml = true;
                    msg.From = new MailAddress(config.AdminEmail);
                    msg.To.Add(config.ItPAD);
                    msg.Subject = Environment.MachineName + ": [系統]中獎發票通知失敗";
                    msg.Body = ex.Message;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
                op.EinvoiceSetSerial(es);
                SetJobMessage(es.DateCode, result, (int)EinvoiceType.Winning, "sys");
            }
        }

        #endregion 發信

        #region 簡易function

        public static void SendMail(string email, string subject, string content)
        {
            MailMessage msg = new MailMessage();
            msg.To.Add(email);
            msg.Subject = string.Format(Environment.MachineName + ": " + subject);
            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.IsBodyHtml = true;
            msg.Body = content;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }

        public static void SetJobMessage(string datecode, string message, dynamic type,string creator = "",string fileName ="")
        {
            EinvoiceLog log = new EinvoiceLog();
            log.DateCode = datecode;
            log.CreateTime = DateTime.Now;
            log.InvoiceType = (int)type;
            log.LogMessage = message;
            log.CreateId = creator;
            log.FileName = !string.IsNullOrEmpty(fileName) ? fileName : log.FileName;
            op.EinvoiceLogSet(log);
        }

        public static string MaxLengthString(string input, int length)
        {
            return string.IsNullOrEmpty(input) ? string.Empty : (input.Length > length ? input.Substring(0, length) : input);
        }

        //中英文字計算padding(中文2bytes,英數為1byte)
        public static string GetPadString(string input, int length)
        {
            //Encoding big5
            return input.PadRight(length - (Encoding.GetEncoding(950).GetBytes(input).Length - input.Length), ' ');
        }

        //回傳個人識別碼(4位英數字)  統一改為四碼數字比較簡單
        public static string ReturnInvoicePass()
        {
            Random r = new Random(Guid.NewGuid().GetHashCode());
            return r.Next(0, 9999).ToString().PadLeft(4, '0');
        }

        public static string NoComma(string input, string comma = ",")
        {
            return string.IsNullOrEmpty(input) ? string.Empty : input.Replace(comma, string.Empty);
        }

        public static string NoNull(string input)
        {
            return string.IsNullOrEmpty(input) ? string.Empty : input;
        }

        /// <summary>
        /// 傳入品名 (1X 物品名稱)，去除數量後回傳 
        /// </summary>
        public static string OrderItemRemoveQuantity(string input)
        {
            if (input.Contains("X"))
            {
                List<string> ele = input.Split("X").ToList();
                ele.RemoveAt(0);
                return string.Join("X", ele);
            }
            else
            {
                return input;
            }
        }

        //取得發票的購買者字串, 兩個條件. (1) 2聯式發票則回傳空值 (2) 紙本三聯式的話, 回傳公司名稱, 否則回傳購買 "人"
        public static string GetInvoiceBuyer(EinvoiceMain inv)
        {
            string result = string.Empty;
            if (inv.IsEinvoice3)
            {
                if (string.IsNullOrEmpty(inv.InvoiceComName) == false)
                {
                    return inv.InvoiceComName;
                }
                else if (string.IsNullOrEmpty(inv.InvoiceBuyerName) == false)
                {
                    return inv.InvoiceBuyerName;
                }
            }
            return result;
        }

        /// <summary>
        /// 取得xml namespace
        /// </summary>
        /// <param name="einvoice_type"></param>
        /// <returns></returns>
        public static KeyValuePair<XNamespace, string> GetXmlNameSpace(EinvoiceType einvoice_type)
        {
            string type = string.Empty;
            switch (einvoice_type)
            {
                case EinvoiceType.C0401://開立 新版 : C0401
                    type = "C0401";
                    break;
                case EinvoiceType.D0401://折讓 新版 : D0401
                    type = "D0401";
                    break;
                case EinvoiceType.C0501://開立作廢 新版 : C0501
                    type = "C0501";
                    break;
                case EinvoiceType.D0501://折讓作廢 新版 : D0501
                    type = "D0501";
                    break;
                case EinvoiceType.C0701://註銷發票
                    type = "C0701";
                    break;
            }
            XNamespace xmlns = "urn:GEINV:eInvoiceMessage:" + type + ":3.1";
            string xsi_location = "urn:GEINV:eInvoiceMessage:" + type + ": 3.1 " + type + ".xsd";
            return new KeyValuePair<XNamespace, string>(xmlns, xsi_location);
        }
        #endregion 簡易function

        #region Set VerifiedTime of EinvoiceMain

        public static void SetEinvoiceCouponVerifiedTime(int couponId, OrderClassification orderClassification, DateTime verifiedTime, bool isJf = false)
        {
            op.EinvoiceMainVerifiedTimeSet(couponId, orderClassification, verifiedTime, isJf);
            if (config.EnbaledTaishinEcPayApi)
            {
                op.PartnerEinvoiceMainVerifiedTimeSet(couponId, verifiedTime);
            }
        }
        
        #endregion Set VerifiedTime of EinvoiceMain

        #region 發票

        /// <summary>
        /// TrustId 對應的宅配發票
        /// </summary>
        public static EinvoiceMainCollection EinvoiceGetListByToHouseTrustIds(string trustIds)
        {
            return op.EinvoiceGetListByToHouseTrustIds(trustIds);
        }

        /// <summary>
        /// TrustId 對應的憑證發票
        /// </summary>
        public static EinvoiceMainCollection EinvoiceGetListByToShopTrustIds(string trustIds)
        {
            return op.EinvoiceGetListByToShopTrustIds(trustIds);
        }

        /// <summary>
        /// 取得該發票對應的 CashTrustLog
        /// </summary>
        public static CashTrustLog CashTrustLogGetByInvoiceNumber(string invNum)
        {
            EinvoiceMain inv = op.EinvoiceMainGet(invNum);
            CashTrustLog trust = new CashTrustLog();
            switch ((int)inv.OrderClassification)
            {
                case (int)OrderClassification.LkSite:
                    ViewPponOrder o = pp.ViewPponOrderGet(inv.OrderGuid);
                    DealProperty d = pp.DealPropertyGet(o.BusinessHourGuid);
                    switch ((DeliveryType)d.DeliveryType)
                    {
                        case DeliveryType.ToHouse:
                            //好康宅配，要用OrderGuid去Join CashTrustLog
                            trust = mp.CashTrustLogGetByOrderId(o.Guid);
                            break;
                        case DeliveryType.ToShop:
                            if (o.CreateTime >= config.NewInvoiceDate && inv.CouponId != null)
                            {
                                trust = mp.CashTrustLogGetByCouponId((int)inv.CouponId, OrderClassification.LkSite);
                            }
                            else
                            {
                                trust = mp.CashTrustLogGetByOrderId(o.Guid);
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case (int)OrderClassification.HiDeal:
                    HiDealOrder ho = hp.HiDealOrderGet(inv.OrderGuid);
                    HiDealDeliveryType deliveryType = (HiDealDeliveryType)hp.HiDealOrderDetailGetListByOrderGuid(inv.OrderGuid).FirstOrDefault().DeliveryType;
                    switch (deliveryType)
                    {
                        case HiDealDeliveryType.ToHouse:
                            trust = mp.CashTrustLogGetByOrderId(inv.OrderGuid);
                            break;
                        case HiDealDeliveryType.ToShop:
                            if (ho.CreateTime >= config.NewInvoiceDate)
                            {
                                trust = mp.CashTrustLogGetByCouponId((int)inv.CouponId, OrderClassification.HiDeal);
                            }
                            else
                            {
                                trust = mp.CashTrustLogGetByOrderId(inv.OrderGuid);
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            return trust;
        }

        public static bool SetEinvoiceMainWithLog(EinvoiceMain em, string createId = "sys", DateTime? now = null)
        {
            now = now ?? DateTime.Now;
            var log = GetEinvoiceChangeLogCollection(em, createId, now.Value);
            op.EinvoiceSetMain(em);
            op.EinvoiceChangeLogCollectionSet(log);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool SetEinvoiceMainCollectionWithLog(EinvoiceMainCollection emc, string createId = "sys", DateTime? now = null)
        {
            now = now ?? DateTime.Now;
            var log = new EinvoiceChangeLogCollection();
            foreach (var em in emc)
            {
                log.AddRange(GetEinvoiceChangeLogCollection(em, createId, now.Value));
                if (log.Count >= 500)
                {
                    op.EinvoiceChangeLogCollectionSet(log);
                    log.Clear();
                }
            }
            op.EinvoiceMainCollectionSet(emc);
            op.EinvoiceChangeLogCollectionSet(log);
            return true;
        }

        public static EinvoiceChangeLogCollection GetEinvoiceChangeLogCollection(EinvoiceMain em, string createId, DateTime now)
        {
            EinvoiceChangeLogCollection list = new EinvoiceChangeLogCollection();
            if (em.IsLoaded)
            {
                EinvoiceChangeLog log = new EinvoiceChangeLog();
                foreach (var item in em.DirtyColumns)
                {
                    if (log.ActionType == (int)EinvoiceAction.Init)
                    {
                        log.MainId = em.Id;
                        log.OrderGuid = em.OrderGuid;
                        log.CreateId = createId;
                        log.CreateTime = now;
                    }
                    if (item.ColumnName == EinvoiceMain.Columns.InvoiceNumber)
                    {
                        if (em.InvoiceRequestTime != null)
                        {
                            log.CreateTime = em.InvoiceRequestTime.Value;
                            log.ActionType = em.InvoiceMode2 == (int)InvoiceMode2.Duplicate ? (int)EinvoiceAction.RequestDupl : (int)EinvoiceAction.RequestTrip;
                            list.Add(log);
                            log = new EinvoiceChangeLog();
                        }
                        log.ActionType = (int)EinvoiceAction.New;
                        list.Add(log);
                        log = new EinvoiceChangeLog();
                    }
                    if (item.ColumnName == EinvoiceMain.Columns.InvoiceRequestTime)
                    {
                        log.ActionType = em.InvoiceRequestTime == null ? (int)EinvoiceAction.RequestReverse : (em.InvoiceMode2 == (int)InvoiceMode2.Duplicate ? (int)EinvoiceAction.RequestDupl : (int)EinvoiceAction.RequestTrip);
                        list.Add(log);
                        log = new EinvoiceChangeLog();
                    }
                    if (item.ColumnName == EinvoiceMain.Columns.InvoiceWinning)
                    {
                        log.ActionType = (int)EinvoiceAction.Winning;
                        list.Add(log);
                        log = new EinvoiceChangeLog();
                    }
                    if (item.ColumnName == EinvoiceMain.Columns.InvoiceWinnerresponseTime)
                    {
                        log.ActionType = (int)EinvoiceAction.WinningResponse;
                        list.Add(log);
                        log = new EinvoiceChangeLog();
                    }
                    if (item.ColumnName == EinvoiceMain.Columns.InvoicePaperedTime)
                    {
                        log.ActionType = em.InvoicePaperedTime == null ? (int)EinvoiceAction.Withdraw : (em.SinglePrint ? (int)EinvoiceAction.SinglePrint : (int)EinvoiceAction.Print);
                        list.Add(log);
                        log = new EinvoiceChangeLog();
                    }
                    if (item.ColumnName == EinvoiceMain.Columns.InvoiceStatus)
                    {
                        switch (em.InvoiceStatus)
                        {
                            case (int)EinvoiceType.C0501:
                                log.ActionType = (int)EinvoiceAction.Cancel;
                                break;
                            case (int)EinvoiceType.C0701:
                                log.ActionType = (int)EinvoiceAction.Void;
                                break;
                            case (int)EinvoiceType.D0401:
                                log.ActionType = (int)EinvoiceAction.Allowance;
                                break;
                            case (int)EinvoiceType.D0501:
                                log.ActionType = (int)EinvoiceAction.CancelAllowance;
                                break;
                            default:
                                continue;
                        }
                        list.Add(log);
                        log = new EinvoiceChangeLog();
                    }
                }
                if (em.CopyPrint)
                {
                    log.ActionType = em.SinglePrint ? (int)EinvoiceAction.SinglePrintCopy : (int)EinvoiceAction.PrintCopy;
                    list.Add(log);
                }
            }

            return list;
        }

        public static string GetEinvoiceActionTypeDescription(ViewEinvoiceChangeLog log)
        {
            switch (log.ActionType)
            {
                case (int)EinvoiceAction.RequestDupl:
                    return "申請【紙本】二聯式發票";
                case (int)EinvoiceAction.RequestTrip:
                    return "申請【紙本】三聯式發票";
                case (int)EinvoiceAction.RequestReverse:
                    return "取消申請【紙本】發票(申請退貨)";
                case (int)EinvoiceAction.New:
                    return "發票產出：" + log.InvoiceNumber;
                case (int)EinvoiceAction.Print:
                    return "印出發票正本";
                case (int)EinvoiceAction.PrintCopy:
                    return "印出發票副本";
                case (int)EinvoiceAction.SinglePrint:
                    return "單筆-印出發票正本";
                case (int)EinvoiceAction.SinglePrintCopy:
                    return "單筆-印出發票副本";
                case (int)EinvoiceAction.Cancel:
                    return "發票作廢";
                case (int)EinvoiceAction.Void:
                    return "發票註銷";
                case (int)EinvoiceAction.Withdraw:
                    return "抽回發票";
                case (int)EinvoiceAction.Winning:
                    return "發票中獎";
                case (int)EinvoiceAction.Invoice2To3:
                    return "二聯作廢改三聯";
                case (int)EinvoiceAction.DonateTo3:
                    return "捐贈作廢改三聯";
                default:
                    break;
            }
            return "";
        }

        /// <summary>
        /// 取得發票變更歷程
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static List<EinvoiceChangeLogMain> GetEinvoiceChangeLogMainData(Guid orderGuid)
        {
            var log = op.ViewEinvoiceChangeLogCollectionGet(orderGuid);
            EinvoiceMainCollection emc = op.EinvoiceMainInvoicedOnlyGetListByOrderGuid(orderGuid);
            List<EinvoiceChangeLogMain> list = new List<EinvoiceChangeLogMain>();

            foreach (var em in emc.OrderBy(x => x.InvoiceNumber).ThenBy(x => x.InvoiceNumber))
            {
                if (log.Any(x => x.MainId == em.Id))
                {
                    if (!string.IsNullOrEmpty(em.InvoiceNumber))
                    {
                        EinvoiceChangeLogMain m = new EinvoiceChangeLogMain();
                        m.InvoiceNumber = em.InvoiceNumber;
                        m.InvoiceType = GetEinvoiceStatusDesc(em);
                        m.InvoiceWinning = !em.IsEinvoice3 && em.InvoiceWinning; //
                        m.OrderItemName = em.OrderItem;

                        #region Detail

                        m.Detail = new List<EinvoiceChangeLogDetail>();
                        EinvoiceChangeLogDetail d = new EinvoiceChangeLogDetail();

                        if (!log.Any(x => x.MainId == em.Id && x.ActionType == (int)EinvoiceAction.New))
                        {
                            d.Message = "發票產出：" + em.InvoiceNumber;
                            d.CreateTime = em.InvoiceNumberTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
                            d.CreateId = "sys";
                            m.Detail.Add(d);
                        }
                        if (!log.Any(x => x.MainId == em.Id && (x.ActionType == (int)EinvoiceAction.RequestDupl || x.ActionType == (int)EinvoiceAction.RequestTrip)) && em.InvoiceRequestTime.HasValue)
                        {
                            d = new EinvoiceChangeLogDetail();
                            d.Message = em.InvoiceMode2 == (int)InvoiceMode2.Duplicate ? "申請【紙本】二聯式發票" : "申請【紙本】三聯式發票";
                            d.CreateTime = em.InvoiceRequestTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
                            d.CreateId = "";
                            m.Detail.Add(d);
                        }
                        if (!log.Any(x => x.MainId == em.Id && (x.ActionType == (int)EinvoiceAction.Print || x.ActionType == (int)EinvoiceAction.SinglePrint)) && em.InvoicePaperedTime.HasValue)
                        {
                            d = new EinvoiceChangeLogDetail();
                            d.Message = "印出發票";
                            d.CreateTime = em.InvoicePaperedTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
                            d.CreateId = "";
                            m.Detail.Add(d);
                        }

                        foreach (var item in log.Where(x => x.InvoiceNumber == em.InvoiceNumber).OrderBy(x => x.CreateTime))
                        {
                            d = new EinvoiceChangeLogDetail();
                            d.Message = GetEinvoiceActionTypeDescription(item);
                            d.CreateTime = item.CreateTime.ToString("yyyy/MM/dd HH:mm:ss");
                            d.CreateId = item.CreateId;
                            m.Detail.Add(d);
                        }

                        #endregion
                        list.Add(m);
                    }
                }
                else
                {
                    //以前沒有log的 就用main來組
                    EinvoiceChangeLogMain m = new EinvoiceChangeLogMain();
                    m.InvoiceNumber = em.InvoiceNumber;
                    m.InvoiceType = GetEinvoiceStatusDesc(em);
                    m.Detail = new List<EinvoiceChangeLogDetail>();
                    EinvoiceChangeLogDetail d = new EinvoiceChangeLogDetail();
                    if (!string.IsNullOrEmpty(em.InvoiceNumber))
                    {
                        d.Message = "發票產出：" + em.InvoiceNumber;
                        d.CreateTime = em.InvoiceNumberTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
                        d.CreateId = "sys";
                        m.Detail.Add(d);
                    }
                    if (em.InvoiceRequestTime != null)
                    {
                        d = new EinvoiceChangeLogDetail();
                        d.Message = em.InvoiceMode2 == (int)InvoiceMode2.Duplicate ? "申請【紙本】二聯式發票" : "申請【紙本】三聯式發票";
                        d.CreateTime = em.InvoiceRequestTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
                        d.CreateId = "";
                        m.Detail.Add(d);
                    }
                    if (em.InvoicePapered)
                    {
                        d = new EinvoiceChangeLogDetail();
                        d.Message = "印出發票";
                        d.CreateTime = em.InvoicePaperedTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
                        d.CreateId = "";
                        m.Detail.Add(d);
                    }
                    list.Add(m);
                }
            }
            return list;
        }

        /// <summary>
        /// 取得發票狀態
        /// </summary>
        /// <param name="em"></param>
        /// <returns></returns>
        public static string GetEinvoiceStatusDesc(EinvoiceMain em)
        {
            string einvoiceStatusDesc = string.Empty;
            switch (em.InvoiceStatus)
            {
                case (int)EinvoiceType.Initial:
                    if (!string.IsNullOrEmpty(em.InvoiceNumber))
                    {
                        einvoiceStatusDesc = "(重開)";
                    }

                    break;
                case (int)EinvoiceType.C0501:
                    einvoiceStatusDesc = "(作廢)";
                    break;
                case (int)EinvoiceType.D0401:
                    einvoiceStatusDesc = string.Format("({0}折讓)", em.AllowanceStatus == (int)AllowanceStatus.ElecAllowance ? "電子" : "紙本");
                    break;
                case (int)EinvoiceType.C0701:
                    if (!string.IsNullOrEmpty(em.InvoiceNumber))
                    {
                        einvoiceStatusDesc = "(註銷)";
                    }

                    break;
                default:
                    einvoiceStatusDesc = string.Empty;
                    break;
            }
            return einvoiceStatusDesc;
        }
        #endregion



        /// <summary>
        /// 取得捐贈機構名稱
        /// </summary>
        /// <param name="einvoice"></param>
        /// <returns></returns>
        public static string GetDonateCodeName(EinvoiceMain einvoice)
        {
            return LoveCodeManager.Instance.QueryLoveCodeString(einvoice.LoveCode);
        }

        /// <summary>
        /// 新版發票列印改抓einvoice_main.VerifiedTime
        /// 但為避免舊發票需要抓InvoiceNumberTime，所以寫一個小config去設定什麼日期之前的使用InvoiceNumberTime
        /// </summary>
        /// <param name="InvoiceNumberTime"></param>
        /// <param name="VerifiedTime"></param>
        /// <returns></returns>
        public static DateTime ReturnInvoiceTime(DateTime InvoiceNumberTime, DateTime VerifiedTime)
        {
            return VerifiedTime;
        }

        #region 寄送媒體檔

        public static void GenerateInvocieMedia()
        {
            try
            {
                Dictionary<string, byte[]> file = new Dictionary<string, byte[]>();
                StringBuilder sb = new StringBuilder();
                var contact = op.GetEinvoiceMedia(config.ContactCompanyId).AsEnumerable();
                var pez = op.GetEinvoiceMedia(config.PayeasyCompanyId).AsEnumerable();
                //todo: check if no data
                foreach (var item in contact)
                {
                    for (int i = 1; i <= item.ItemArray.Count() - 1; i++)
                    {
                        sb.Append(item[i]);
                    }
                    sb.Append(' ', 9);
                    sb.Append(Environment.NewLine);
                }

                file.Add("combine.txt", Encoding.UTF8.GetBytes(sb.ToString()));
                byte[] contactZip = Helper.ZipFile(file);
                file.Clear();
                sb.Clear();

                foreach (var item in pez)
                {
                    for (int i = 1; i <= item.ItemArray.Count() - 1; i++)
                    {
                        sb.Append(item[i]);
                    }
                    sb.Append(' ', 9);
                    sb.Append(Environment.NewLine);
                }

                file.Add("combine.txt", Encoding.UTF8.GetBytes(sb.ToString()));
                byte[] pezZip = Helper.ZipFile(file);

                //using (streamwriter file = new streamwriter(@"c:\invoicemedia\hereiam.txt"))
                //{
                //    file.writeline(sb.tostring());
                //}

                DateTime now = DateTime.Now;
                MailMessage msg = new MailMessage();
                msg.IsBodyHtml = true;
                msg.From = new MailAddress(config.AdminEmail);
                msg.To.Add(config.EinvoiceMediaEmail);
                msg.Subject = string.Format("{0}年{1}-{2}月媒體檔", now.AddMonths(-2).Year, now.AddMonths(-2).Month, now.AddMonths(-1).Month);
                Attachment ach = new Attachment(new MemoryStream(contactZip), string.Format("17Life{0}_{1}月媒體檔.zip", now.AddMonths(-2).Month, now.AddMonths(-1).Month));
                Attachment ach2 = new Attachment(new MemoryStream(pezZip), string.Format("Payeasy{0}_{1}月媒體檔.zip", now.AddMonths(-2).Month, now.AddMonths(-1).Month));
                msg.Attachments.Add(ach);
                msg.Attachments.Add(ach2);
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        private class EinvoiceC0401Main
        {
            public string InvoiceNumber { get; set; }
            public DateTime InvoiceNumberTime { get; set; }
            public string BuyerComId { get; set; }
            public string BuyerComName { get; set; }
            public decimal OrderAmount { get; set; }
            public decimal InvoiceTax { get; set; }
            public string UserId { get; set; }
            public int InvoiceMode2 { get; set; }
            public string LoveCode { get; set; }
            public int CarrierType { get; set; }
            public string CarrierId { get; set; }
            public string InvoicePass { get; set; }
            public List<EinvoiceC0401Detail> Details { get; set; }
        }

        private class EinvoiceC0401Detail
        {
            public bool OrderIsPponitem { get; set; }
            public string OrderItem { get; set; }
            public int Amount { get; set; }
        }

        public sealed class EinvoiceChangeLogMain
        {
            /// <summary>
            /// 發票號碼
            /// </summary>
            public string InvoiceNumber { get; set; }
            /// <summary>
            /// 狀態
            /// </summary>
            public string InvoiceType { get; set; }
            /// <summary>
            /// 發票項目
            /// </summary>
            public string OrderItemName { get; set; }
            /// <summary>
            /// 中獎
            /// </summary>
            public bool InvoiceWinning { get; set; }
            /// <summary>
            /// 歷程
            /// </summary>
            public List<EinvoiceChangeLogDetail> Detail { get; set; }
        }

        /// <summary>
        /// 發票歷程
        /// </summary>
        public sealed class EinvoiceChangeLogDetail
        {
            public string Message { get; set; }
            public string CreateTime { get; set; }
            public string CreateId { get; set; }
        }
    }
}
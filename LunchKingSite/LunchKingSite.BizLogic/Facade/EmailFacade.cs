﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Winnovative.WnvHtmlConvert;
using PponCity = LunchKingSite.BizLogic.Component.PponCity;
using PponCityGroup = LunchKingSite.BizLogic.Component.PponCityGroup;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.BizLogic.Models.CustomerService;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.Core.ModelCustom;
using System.Web;
using LunchKingSite.BizLogic.Models.Wms;

namespace LunchKingSite.BizLogic.Facade
{
    public class EmailFacade
    {
        private static readonly EmailAgent ea = new EmailAgent();
        private static readonly ILog log;
        private static readonly IPponProvider pp;
        private static readonly IPponEntityProvider pep;
        private static readonly IMemberProvider mp;
        private static readonly ISysConfProvider config;
        private static readonly ICmsProvider cp;
        private static readonly ILocationProvider lp;
        private static readonly ISellerProvider sp;
        private static readonly IOrderProvider op;
        private static readonly Core.IServiceProvider csp;
        private static IHiDealProvider hp;
        private static readonly IHumanProvider humanProv;
        private static readonly IMGMProvider mgm;
        private static readonly ISystemProvider sys;
        private static readonly IAccountingProvider ap;
        private static readonly ISerializer serializer;
        private static readonly IWmsProvider wp;

        private const int DivideInto = 10;

        private delegate void AsyncSendMail(StringBuilder operatemabody, Guid sellerguid, string sellername, List<MultipleSalesModel> model, string sellerid, string storeguid);

        static EmailFacade()
        {
            log = LogManager.GetLogger(typeof(EmailFacade));
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            config = ProviderFactory.Instance().GetConfig();
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            csp = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();
            humanProv = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            mgm = ProviderFactory.Instance().GetProvider<IMGMProvider>();
            sys = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            serializer = ProviderFactory.Instance().GetSerializer();
            wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();

        }

        /// <summary>
        /// 處理郵件主旨亂碼問題
        /// </summary>
        /// <param name="subject"></param>
        /// <returns></returns>
        private static string FormatMailSubject(string subject)
        {
            MatchCollection matches = Regex.Matches(subject, "[\u0020-\u007e]+");
            for (int i = matches.Count - 1; i >= 0; i--)
            {
                Match m = matches[i];
                string mStr = m.Value;
                if (m.Length % 3 == 1)
                {
                    mStr = " " + mStr + " ";
                }
                else if (m.Length % 3 == 2)
                {
                    mStr += " ";
                }

                subject = subject.Substring(0, m.Index) + mStr + subject.Substring(m.Index + m.Length);
            }
            return subject;
        }

        #region 好康使用通知

        #region 寄送使用通知

        public static void SendToCounsumerMail()
        {
            DateTime today = DateTime.Today;
            ViewPponDealCollection vpdc = pp.ViewPponDealGetListByDeliverTimeStart(DateTime.Today);

            Parallel.ForEach(vpdc, vpd =>
            {
                Dictionary<string, string> dicMailInfo = new Dictionary<string, string>();
                DateTime start = Convert.ToDateTime(vpd.BusinessHourDeliverTimeS);
                DateTime end = Convert.ToDateTime(vpd.BusinessHourDeliverTimeE);
                TimeSpan ts = new TimeSpan(end.Ticks - start.Ticks);
                TimeSpan tsWithStart = new TimeSpan(today.Ticks - start.Ticks);
                TimeSpan tsWithEnd = new TimeSpan(end.Ticks - today.Ticks);
                //先判斷兌換區間
                //  1. 超過3個月(91天以上)
                //  2. 介於六周至3個月之間(43~90天之間)
                //  3. 六周以內(42天以內)
                try
                {
                    if (ts.TotalDays > 90)
                    {
                        dicMailInfo = ActGreaterThenOrEqualTime(vpd, tsWithStart, tsWithEnd);
                    }
                    else if (ts.TotalDays > 42 && ts.TotalDays <= 90)
                    {
                        dicMailInfo = ActWithinTime(vpd, tsWithStart, tsWithEnd);
                    }
                    else if (ts.TotalDays <= 42)
                    {
                        dicMailInfo = ActSmallerThanOrEqualTime(vpd, tsWithStart, tsWithEnd);
                    }
                }
                catch (Exception e)
                {
                    log.Warn("好康使用通知 - 取得 Template Error (EmailFacade.SendToCounsumerMail):" + e);
                }

                //根據每一檔取出該檔有下訂單者之記錄，
                if (!string.IsNullOrEmpty(dicMailInfo["subject"]))
                {
                    try
                    {
                        DataTable dtMembers = mp.GetMemberListByDepartmentAndBusinessHourAndCouponUsageExcludeRefunding(DeliveryType.ToShop, vpd.BusinessHourGuid, Guid.Empty, 0);

                        foreach (DataRow dr in dtMembers.Rows)
                        {
                            var userName = dr["member_email"].ToString();
                            var userEmail = MemberFacade.GetUserEmail(userName);
                            PostMan.Instance().Send(
                                    ea.GetMailMessage(config.ServiceEmail
                                                      , userEmail
                                                      , dicMailInfo["subject"]
                                                      , dicMailInfo["temp"]
                                                            .Replace("{member_name}", dr["member_name"].ToString())
                                                            .Replace("{order_detail_item_name}", dr["item_name"].ToString())
                                                      , config.ServiceName
                                    ), SendPriorityType.Immediate);
                        }
                    }
                    catch (Exception e)
                    {
                        log.Warn("好康使用通知 - 寄送 Error (EmailFacade.SendToCounsumerMail):" + e);
                    }
                }
            }
            );
        }

        #endregion 寄送使用通知

        #region 超過三個月(90天)

        private static Dictionary<string, string> ActGreaterThenOrEqualTime(ViewPponDeal vpd, TimeSpan tsStart, TimeSpan tsEnd)
        {
            Dictionary<string, string> rtnDic = new Dictionary<string, string>();
            rtnDic.Add("subject", string.Empty);
            rtnDic.Add("temp", string.Empty);
            if (tsEnd.TotalDays > 30) //至結束前一個月
            {
                if ((int)tsStart.TotalDays % 21 == 0 && tsStart.TotalDays > 20)   //開跑後隔三周(三周、六周、九周依此類推)，取得"使用"提醒樣版
                {
                    rtnDic["subject"] = "好康使用提醒！" + vpd.ItemName;
                    rtnDic["temp"] = GetPponToConsumerWarningMail(vpd);
                }
            }
            else if ((int)tsEnd.TotalDays == 30)  //結束前一個月，取得"到期"提醒樣版
            {
                rtnDic["subject"] = "好康到期提醒！" + vpd.ItemName;
                rtnDic["temp"] = GetPponToConsumerExpiredMail(vpd);
            }
            return rtnDic;
        }

        #endregion 超過三個月(90天)

        #region 介於六周至三個月之間(43~90天)

        private static Dictionary<string, string> ActWithinTime(ViewPponDeal vpd, TimeSpan tsStart, TimeSpan tsEnd)
        {
            Dictionary<string, string> rtnDic = new Dictionary<string, string>();
            rtnDic.Add("subject", string.Empty);
            rtnDic.Add("temp", string.Empty);

            if ((int)tsStart.TotalDays == 21)    //開始後3週，取得"使用"提醒樣版
            {
                rtnDic["subject"] = "好康使用提醒！" + vpd.ItemName;
                rtnDic["temp"] = GetPponToConsumerWarningMail(vpd);
            }

            if ((int)tsEnd.TotalDays == 14)   //結束前2週，取得"到期"提醒樣版
            {
                rtnDic["subject"] = "好康到期提醒！" + vpd.ItemName;
                rtnDic["temp"] = GetPponToConsumerExpiredMail(vpd);
            }
            return rtnDic;
        }

        #endregion 介於六周至三個月之間(43~90天)

        #region 六周以內(42天)

        private static Dictionary<string, string> ActSmallerThanOrEqualTime(ViewPponDeal vpd, TimeSpan tsStart, TimeSpan tsEnd)
        {
            Dictionary<string, string> rtnDic = new Dictionary<string, string>();
            rtnDic.Add("subject", string.Empty);
            rtnDic.Add("temp", string.Empty);

            if ((int)tsStart.TotalDays == 7)    //開始後1週，取得"使用"提醒樣版
            {
                rtnDic["subject"] = "好康使用提醒！" + vpd.ItemName;
                rtnDic["temp"] = GetPponToConsumerWarningMail(vpd);
            }

            if ((int)tsEnd.TotalDays == 7)   //結束前1週，取得"到期"提醒樣版
            {
                rtnDic["subject"] = "好康到期提醒！" + vpd.ItemName;
                rtnDic["temp"] = GetPponToConsumerExpiredMail(vpd);
            }
            return rtnDic;
        }

        #endregion 六周以內(42天)

        #region 取出提醒知的 Template

        #region 好康到期提醒

        public static string GetPponToConsumerExpiredMail(ViewPponDeal vpd)
        {
            string rtnTemp = string.Empty;
            try
            {
                PponToConsumerExpiredMail template = TemplateFactory.Instance().GetTemplate<PponToConsumerExpiredMail>();
                template.TheMemberName = "{member_name}";
                template.TheDeal = vpd;
                template.PromoDeals = OrderFacade.GetPromoDealsForNoticeMail(vpd.BusinessHourGuid);
                template.ServerConfig = config;
                rtnTemp = template.ToString();
            }
            catch (Exception e)
            {
                log.Warn("好康到期提醒通知 Error:" + e);
            }
            return rtnTemp;
        }

        #endregion 好康到期提醒

        #region 好康使用提醒

        public static string GetPponToConsumerWarningMail(ViewPponDeal vpd)
        {
            string rtnTemp = string.Empty;
            try
            {
                PpontoConsumerWarningMail template = TemplateFactory.Instance().GetTemplate<PpontoConsumerWarningMail>();
                template.TheMemberName = "{member_name}";
                template.TheDeal = vpd;
                template.ItemName = vpd.ItemName;
                rtnTemp = template.ToString();
            }
            catch (Exception e)
            {
                log.Warn("好康使用提醒通知 Error:" + e);
            }
            return rtnTemp;
        }

        #endregion 好康使用提醒

        #endregion 取出提醒知的 Template

        #endregion 好康使用通知

        #region 賣家／分店停業通知

        /// <summary>
        /// 賣家若停止營業，寄送停業通知給尚未使用憑證的消費者（該賣家相關檔次）
        /// </summary>
        public static void SendSellerCloseDownNoticeToMember(Guid sellerGuid, string sellerName, DateTime closeDate)
        {
            //抓出該Seller底下所有未使用的憑證
            ViewPponCouponCollection viewCoupon = pp.ViewPponCouponGetListUnusedBySellerGuid(sellerGuid);
            SendCloseDownNotice(viewCoupon, closeDate, true, sellerName, string.Empty);
        }

        /// <summary>
        /// 分店若停止營業，寄送停業通知給尚未使用憑證的消費者（只限該分店相關檔次）
        /// </summary>
        public static void SendStoreCloseDownNoticeToMember(Guid sellerGuid, string sellerName, Guid storeGuid, string storeName, DateTime closeDate)
        {
            //抓出賣家底下該店舖所有未使用的憑證
            ViewPponCouponCollection viewCoupon = pp.ViewPponCouponGetListUnusedBySellerStoreGuid(sellerGuid, storeGuid);
            SendCloseDownNotice(viewCoupon, closeDate, false, sellerName, storeName);
        }

        //第三個參數代表的是, 關的是賣家還是分店. 這影響到寄送給客服的信件內容
        private static void SendCloseDownNotice(ViewPponCouponCollection coupons, DateTime closeDate, bool isSellerClose, string sellerName, string storeName)
        {
            var vpcs = coupons.Where(x => x.BusinessHourDeliverTimeS < DateTime.Now && x.BusinessHourDeliverTimeE > DateTime.Now).GroupBy
            (x => new
            {
                Bid = x.BusinessHourGuid,
                ItemName = x.ItemName,
                Email = x.MemberEmail,
                MemberName = x.MemberName,
                StoreName = x.StoreName
            }
            );

            foreach (var v in vpcs)
            {
                CloseDownNoticeForCustomer template = TemplateFactory.Instance().GetTemplate<CloseDownNoticeForCustomer>();
                template.ItemName = v.Key.ItemName;
                template.CloseDate = closeDate;
                template.StoreName = v.Key.StoreName;
                template.MemberName = v.Key.MemberName;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;

                var userName = v.Key.Email; //來自ViewPponCoupon的MemberEmail，即member.user_name
                var userEmail = MemberFacade.GetUserEmail(userName);

                try
                {
                    if (RegExRules.CheckEmail(userEmail))
                    {
                        MailMessage msg = new MailMessage();
                        msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                        msg.To.Add(userEmail);
                        msg.Subject = "商家結束營業通知";
                        msg.Body = template.ToString();
                        msg.IsBodyHtml = true;
                        PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    }
                }
                catch (Exception)
                {

                }
            }

            #region 加寄給客服

            var vpcService = coupons.GroupBy
            (x => new
            {
                Bid = x.BusinessHourGuid,
                ItemName = x.ItemName
            }
            );

            foreach (var v in vpcService)
            {
                CloseDownNoticeForCustomer template = TemplateFactory.Instance().GetTemplate<CloseDownNoticeForCustomer>();
                template.ItemName = v.Key.ItemName;
                template.CloseDate = closeDate;
                template.StoreName = string.Empty;
                template.MemberName = "客服";
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;

                try
                {
                    MailMessage message = new MailMessage();
                    message.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                    message.To.Add(config.CsReturnEmail);
                    //信件歸檔專員
                    message.To.Add(config.CsMailDealerEmail);
                    message.Subject = string.Format("商家結束營業通知");
                    message.Body = template.ToString();
                    message.IsBodyHtml = true;
                    PostMan.Instance().Send(message, SendPriorityType.Immediate);
                }
                catch (Exception)
                {

                }
            }

            #endregion
        }

        #endregion

        #region 檔次延長或停止兌換

        public static void SendDealChangeExpiredDateNoticeToMember(DateTime useEndDate, DateTime changeExpireDate, Guid bid, string itemName)
        {
            ViewPponCouponCollection viewCoupon = pp.ViewPponCouponGetListUnusedByDeal(bid);
            //須有未兌換的憑證，且 "有效期限截止日" 不等於 "兌換截止日"
            if (useEndDate.Date != changeExpireDate)
            {
                SendExpireDateNotice(bid, itemName, viewCoupon, useEndDate, changeExpireDate);
            }
        }

        public static void SendStoreChangeExpiredDateNoticeToMember(DateTime useEndDate, DateTime changeExpireDate, Guid bid, Guid storeGuid, string itemName)
        {
            ViewPponCouponCollection viewCoupon = pp.ViewPponCouponGetListUnusedByDealStoreGuid(bid, storeGuid);
            if (useEndDate.Date != changeExpireDate)
            {
                SendExpireDateNotice(bid, itemName, viewCoupon, useEndDate, changeExpireDate);
            }
        }

        private static void SendExpireDateNotice(Guid bid, string itemName, ViewPponCouponCollection coupons, DateTime useEndDate, DateTime changeExpireDate)
        {
            #region Variables

            string extend = "延長";
            string stop = "停止";
            string extendContent = @"您日前所訂購的 <a href='{3}'>{0} {2}</a>，
            因銷售熱烈，故兌換期延長至<span style='color: #F60;'> {1} </span>。
            為保障您的權益，建議您提早兌換，以免優惠過期喔！";
            string stopContent = @"很抱歉通知您，日前所訂購的 <a href='{3}'>{0} {2}</a>，
            我們接獲通知，店家因故提前至<span style='color: #F60;'> {1} </span>結束兌換。
            如期限內未能兌換完畢之憑證，為保障您的權益，後續將主動全額退費，待完成退款作業後，
            系統會再次發送通知給您，若造成任何困擾及不便之處，懇請多加包涵及見諒。<br/>
            (退款方式將優先退回17Life購物金到您所屬的帳戶中，若有相關問題或轉換其他退款方式，您可以隨時與客服中心聯繫。謝謝。)";
            string dealUrl = string.Format(@"{0}/{1}", config.SiteUrl, bid.ToString());
            #endregion

            var vpcs = coupons.GroupBy
            (x => new
            {
                Bid = x.BusinessHourGuid,
                ItemName = x.ItemName,
                Email = x.MemberEmail,
                MemberName = x.MemberName,
                StoreName = x.StoreName
            }
            );

            bool isExtend = changeExpireDate >= useEndDate;

            foreach (var v in vpcs)
            {
                ChangeExpiredDateNoticeForCustomer template = TemplateFactory.Instance().GetTemplate<ChangeExpiredDateNoticeForCustomer>();
                template.CloseDate = changeExpireDate;
                template.MemberName = v.Key.MemberName;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;
                template.MemberName = v.Key.MemberName;

                if (isExtend)
                {
                    //延長兌換
                    template.Content = string.Format(extendContent, v.Key.ItemName, changeExpireDate.ToShortDateString(), v.Key.StoreName, dealUrl);
                }
                else
                {
                    //提前結束兌換
                    template.Content = string.Format(stopContent, v.Key.ItemName, changeExpireDate.ToShortDateString(), v.Key.StoreName, dealUrl);
                }

                try
                {
                    var userName = v.Key.Email; //來自ViewPponCoupon的MemberEmail，即member.user_name
                    var userEmail = MemberFacade.GetUserEmail(userName);

                    if (RegExRules.CheckEmail(userEmail))
                    {
                        MailMessage msg = new MailMessage();
                        msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                        msg.To.Add(userEmail);
                        msg.Subject = string.Format("活動{0}兌換通知(" + v.Key.ItemName + ")", isExtend ? extend : stop);
                        msg.Body = template.ToString();
                        msg.IsBodyHtml = true;
                        PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    }
                }
                catch (Exception)
                {

                }
            }

            #region 加寄給客服

            var vpcService = coupons.GroupBy
            (x => new
            {
                Bid = x.BusinessHourGuid,
                ItemName = x.ItemName
            }
            );

            foreach (var v in vpcService)
            {
                ChangeExpiredDateNoticeForCustomer template = TemplateFactory.Instance().GetTemplate<ChangeExpiredDateNoticeForCustomer>();
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;
                template.CloseDate = changeExpireDate;
                template.MemberName = "客服";

                if (isExtend)
                {
                    //延長兌換
                    template.Content = string.Format(extendContent, v.Key.ItemName, changeExpireDate.ToShortDateString(), string.Empty, dealUrl);
                }
                else
                {
                    //提前結束兌換
                    template.Content = string.Format(stopContent, v.Key.ItemName, changeExpireDate.ToShortDateString(), string.Empty, dealUrl);
                }

                try
                {
                    MailMessage message = new MailMessage();
                    message.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                    message.To.Add(config.ServiceEmail);
                    message.To.Add(config.CsdEmail);
                    //信件歸檔專員
                    message.To.Add(config.CsMailDealerEmail);
                    message.Subject = string.Format("活動{0}兌換通知(" + v.Key.ItemName + ")", isExtend ? extend : stop);
                    message.Body = template.ToString();
                    message.IsBodyHtml = true;
                    PostMan.Instance().Send(message, SendPriorityType.Immediate);
                }
                catch (Exception)
                {

                }
            }

            #endregion
        }

        #endregion

        #region coupon mail
        public static void SendCouponToMember(string emailTo, Member member, int couponId)
        {
            string requestUrl = WebUtility.GetSiteRoot() + "/ppon/coupon_print.aspx?s=1&";

            ViewPponCoupon coupon = ProviderFactory.Instance().GetProvider<IPponProvider>().ViewPponCouponGet(couponId);
            //ViewPponCouponGet 用 LoadByParam 來撈資料，無資料亦傳回 IsNew = true, IsLoaded = true
            if (coupon.CouponId == 0)
            {
                throw new Exception("取得憑證失敗,可能是憑證已不存在. 編號 " + couponId);
            }
            string userDisplayName = null;
            if (member.IsLoaded)
            {
                userDisplayName = member.DisplayName;
            }

            if (string.Equals(coupon.MemberEmail, member.UserName, StringComparison.OrdinalIgnoreCase) == false)
            {

                throw new Exception(string.Format("取得憑證失敗,您沒有取得憑證的權限. 編號 {0}, 使用者: {1} ", couponId, member.UserName));

            }

            //判斷是否為送禮憑證
            if (mgm.GiftGetByCouponId(couponId).IsLoaded)
            {
                throw new Exception(string.Format("取得憑證失敗,此為送禮憑證. 編號 {0}", couponId));
            }

            byte[] pdfBuffer;
            if (config.NewCouponPdfWriter == false)
            {
                requestUrl += "cid=" + couponId + "&u=" + member.UserName;

                PdfConverter pdfc = new PdfConverter();
                pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
                pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
                pdfc.PdfDocumentOptions.TopMargin = 15;

                // disable unnecessary features to enhance performance
                pdfc.ScriptsEnabled = pdfc.ScriptsEnabledInImage = false;
                pdfc.ActiveXEnabled = pdfc.ActiveXEnabledInImage = false;
                pdfc.PdfDocumentOptions.JpegCompressionEnabled = false;

                pdfBuffer = pdfc.GetPdfFromUrlBytes(requestUrl);
            }
            else
            {
                CouponDocumentBase doc;
                ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(coupon.BusinessHourGuid);
                if (Helper.IsFlagSet((GroupOrderStatus)coupon.GroupOrderStatus.Value, GroupOrderStatus.FamiDeal))
                {
                    doc = new FamiCouponDocument(coupon, (CouponCodeType)Enum.ToObject(typeof(CouponCodeType), vpd.CouponCodeType), vpd);
                }
                else if (Helper.IsFlagSet((GroupOrderStatus)coupon.GroupOrderStatus.Value, GroupOrderStatus.HiLifeDeal))
                {
                    doc = new HiLifeCouponDocument(coupon, (CouponCodeType)Enum.ToObject(typeof(CouponCodeType), vpd.CouponCodeType), vpd);
                }
                else
                {
                    doc = new PponCouponDocument(coupon, vpd);
                }

                pdfBuffer = doc.GetBuffer();
            }

            string subject = string.Format("優惠憑證 {0} {1}", coupon.SequenceNumber, coupon.CouponUsage);
            MemoryStream ms = new MemoryStream(pdfBuffer);
            Attachment attachCoupon = new Attachment(ms, coupon.SequenceNumber, "application/pdf");

            MailMessage mailMsg = ea.GetMailMessage(config.ServiceEmail, emailTo, subject,
                GetCouponMessageText(userDisplayName, coupon.BusinessHourGuid), config.ServiceName);

            mailMsg.Attachments.Add(attachCoupon);
            attachCoupon.NameEncoding = Encoding.UTF8;
            PostMan.Instance().Send(mailMsg, SendPriorityType.Immediate);
        }

        /// <summary>
        /// emailTo 可以跟 userName 不同，會員可以指定寄到任意合法的電子信箱
        /// </summary>
        /// <param name="emailTo"></param>
        /// <param name="userName"></param>
        /// <param name="couponId"></param>
        public static void SendCouponToMember(string emailTo, string userName, int couponId)
        {
            Member member = mp.MemberGet(userName);
            SendCouponToMember(emailTo, member, couponId);
        }

        private static string GetCouponMessageText(string userDisplayName, Guid bid)
        {
            PponGetCouponMail template = TemplateFactory.Instance().GetTemplate<PponGetCouponMail>();
            template.UserName = userDisplayName;
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;
            template.PromoDeals = OrderFacade.GetPromoDealsForNoticeMail(bid);
            template.ServerConfig = config;
            return template.ToString();
        }
        #endregion

        #region 未登入查詢訂單Mail

        public static bool SendCouponListToMail(string eMail, string mailTo, MemberCouponListMail template)
        {
            var msg = new MailMessage();
            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(new MailAddress(eMail, mailTo));
            msg.Subject = "您的17Life購買紀錄";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;

            try
            {
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region new edm

        #region db

        public static EdmMain GetNewEdmMainByCityIdDate(PponCity city, DateTime deliverydate, EdmMainType edmtype, bool status)
        {
            return pp.GetEdmMainByCityDate(city.CityId, deliverydate, edmtype, status);
        }
        /// <summary>
        /// 取得某日(通常是今日)還沒上傳的每日EDM主檔資料
        /// </summary>
        /// <param name="date"></param>
        /// <param name="edmtype"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static List<EdmMain> GetNewDailyEdmMainListWasNotUploaded(DateTime date)
        {
            return pp.GetNewEdmMainListByDateWasNotUploaded(date, EdmMainType.Daily, true);
        }

        public static EdmMainCollection GetNewEdmMainByCityIdDates(PponCity city, DateTime deliverydateS, DateTime deliverydateE, EdmMainType edmtype, bool status)
        {
            return pp.GetEdmMainByCityDates(city.CityId, deliverydateS, deliverydateE, edmtype, status);
        }

        public static EdmMainCollection GetNewEdmMainByCity(PponCity city, EdmMainType edmtype, bool status)
        {
            return pp.GetEdmMainByCity(city.CityId, edmtype, status);
        }

        public static List<EdmDetail> GetNewEdmDetailsByPid(int pid)
        {
            return pp.GetEdmDetailCollectionByPid(pid).ToList();
        }

        public static List<EdmAreaDetail> GetEdmAreaDetailsByPid(int pid)
        {
            return pp.GetEdmAreaCollectionByPid(pid).ToList();
        }

        public static EdmMain GetNewEdmMainById(int id)
        {
            return pp.GetEdmMainById(id);
        }

        public static EdmMain GetNewEdmMainByCityId(int cid)
        {
            return pp.GetEdmMainByCityId(cid);
        }

        public static int SaveNewEdmMain(EdmMain edmmain)
        {
            pp.SaveEdmMain(edmmain);
            return edmmain.Id;
        }

        public static void SaveNewEdmDetail(EdmDetail edmdetail)
        {
            pp.SaveEdmDetail(edmdetail);
            List<int> detail_content_list = new List<int>() { (int)EdmDetailType.MainDeal_1_Items, (int)EdmDetailType.MainDeal_2_Items, (int)EdmDetailType.Area_1_Items, (int)EdmDetailType.Area_2_Items,
                (int)EdmDetailType.Area_3_Items };
            if (detail_content_list.Any(x => x == edmdetail.Type))
            {
                pp.SetSubjectNameByBid(edmdetail.Bid.Value.ToString(), edmdetail.Title.TrimToMaxLength(145, "..."));
            }
        }
        public static void SaveEdmAreaDetail(EdmAreaDetail edmdetail)
        {
            pp.SaveEdmAreaDetail(edmdetail);
            
        }

        public static void DeleteNewEdmDetailByPid(int pid)
        {
            pp.DeleteEdmDetailByPid(pid);
        }
        public static void EnableEdmByPid(int mainId, bool enabled)
        {
            var edmMain = pp.GetEdmMainById(mainId);
            edmMain.Pause = (enabled == false);
            pp.SaveEdmMain(edmMain);
        }
        public static void DeleteEdmAreaDetailByPid(int pid)
        {
            pp.DeleteEdmAreaDetailByPid(pid);
        }

        public static void DisableNewEdm(int id)
        {
            pp.DisableEdmMain(id, false);
        }

        public static void AddEdmDetailByCity(int cityid, string cityname, DateTime deliverydate, EdmDetailType main_type, EdmDetailType item_type, int column, int row, bool displaycityname, int maincityid, List<EdmDetail> edmdetail_list, int sortType = (int)EDMDetailsSortType.BySequence)
        {
            List<ViewPponDealTimeSlot> totalDeal = ViewPponDealTimeSlotGetListByCityDate(cityid, deliverydate).ToList();

            //品生活edm需檢查visa檔次若上檔時間距離現在少於3天則不可出現於edm
            if (cityid == PponCityGroup.DefaultPponCityGroup.Piinlife.CityId || maincityid == PponCityGroup.DefaultPponCityGroup.Piinlife.CityId)
            {
                var multipleMainDealPreviewlist_visa = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(PponCityGroup.DefaultPponCityGroup.Piinlife.ChannelId.Value, null, 158).ToList(); // visa cid:158
                List<Guid> cityMainDeal_visa = (from x in multipleMainDealPreviewlist_visa where x.PponDeal.BusinessHourOrderTimeS.AddDays(3) > DateTime.Now select x.PponDeal.BusinessHourGuid).ToList();
                totalDeal = totalDeal.Where(x => !cityMainDeal_visa.Contains(x.BusinessHourGuid)).ToList();
            }

            ViewPponDealTimeSlotCollection tempSpaCol = new ViewPponDealTimeSlotCollection();//特定城市SPA檔

            //依區域撈取SPA檔
            IEnumerable<ViewPponDealTimeSlot> deals;
            if (maincityid != PponCityGroup.DefaultPponCityGroup.PponSelect.CityId && maincityid != PponCityGroup.DefaultPponCityGroup.Travel.CityId && maincityid != PponCityGroup.DefaultPponCityGroup.AllCountry.CityId && maincityid != PponCityGroup.DefaultPponCityGroup.Family.CityId && maincityid != PponCityGroup.DefaultPponCityGroup.Tmall.CityId)
            {
                if (cityid == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId && maincityid != cityid)
                {
                    //依區域撈取SPA檔

                    #region 女性專區區域分類CityID

                    List<CategoryNode> spaAreaNodes = new List<CategoryNode>();
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTaipei);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTaoyuan);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleHsinchu);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTaichung);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTainan);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleKaohsiung);

                    #endregion 女性專區區域分類CityID

                    CategoryNode node = spaAreaNodes.FirstOrDefault(x => x.CityId == maincityid);
                    var multipleMainDealPreviewlist = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.ChannelId.Value, node.CategoryId, null);

                    deals = totalDeal.Where(x => multipleMainDealPreviewlist.Select(y => y.PponDeal.BusinessHourGuid).Contains(x.BusinessHourGuid));
                    tempSpaCol.AddRange(deals);

                    totalDeal = tempSpaCol.ToList();
                }
            }

            //spa專區全國宅配區塊優先撈彩妝保養類別(categoryId:124)
            if (cityid == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId && maincityid == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                var multipleMainDealPreviewlist_all = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(PponCityGroup.DefaultPponCityGroup.AllCountry.ChannelId.Value, null, null).ToList();
                var multipleMainDealPreviewlist_spcategory = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(PponCityGroup.DefaultPponCityGroup.AllCountry.ChannelId.Value, null, 124).ToList();

                List<Guid> citySpaMainDeal_all = (from x in multipleMainDealPreviewlist_all select x.PponDeal.BusinessHourGuid).ToList();
                List<Guid> citySpaMainDeal_spcategory = (from x in multipleMainDealPreviewlist_spcategory select x.PponDeal.BusinessHourGuid).ToList();

                deals = totalDeal.Where(x => citySpaMainDeal_spcategory.Contains(x.BusinessHourGuid));
                tempSpaCol.AddRange(deals);

                deals = totalDeal.Where(x => citySpaMainDeal_all.Except(citySpaMainDeal_spcategory).Contains(x.BusinessHourGuid));
                tempSpaCol.AddRange(deals);

                totalDeal = tempSpaCol.ToList();
            }

            //spa專區品生活區塊(優先挑選美體紓壓156 and 彩妝保養154)
            if (cityid == PponCityGroup.DefaultPponCityGroup.Piinlife.CityId && maincityid == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                var multipleMainDealPreviewlist_all = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(PponCityGroup.DefaultPponCityGroup.Piinlife.ChannelId.Value, null, null).ToList();
                var multipleMainDealPreviewlist_spa = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(PponCityGroup.DefaultPponCityGroup.Piinlife.ChannelId.Value, null, 156).ToList(); // 美體紓壓 cid:156
                var multipleMainDealPreviewlist_makeup = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(PponCityGroup.DefaultPponCityGroup.Piinlife.ChannelId.Value, null, 154).ToList(); // 彩妝保養 cid:154

                List<Guid> citySpaMainDeal_all = (from x in multipleMainDealPreviewlist_all select x.PponDeal.BusinessHourGuid).ToList();
                List<Guid> citySpaMainDeal_spa = (from x in multipleMainDealPreviewlist_spa select x.PponDeal.BusinessHourGuid).ToList();
                List<Guid> citySpaMainDeal_makeup = (from x in multipleMainDealPreviewlist_makeup select x.PponDeal.BusinessHourGuid).ToList();

                deals = totalDeal.Where(x => citySpaMainDeal_spa.Contains(x.BusinessHourGuid));
                tempSpaCol.AddRange(deals);

                deals = totalDeal.Where(x => citySpaMainDeal_makeup.Contains(x.BusinessHourGuid));
                tempSpaCol.AddRange(deals);

                deals = totalDeal.Where(x => citySpaMainDeal_all.Except(citySpaMainDeal_spa).Except(citySpaMainDeal_makeup).Contains(x.BusinessHourGuid));
                tempSpaCol.AddRange(deals);

                totalDeal = tempSpaCol.ToList();
            }

            List<IViewPponDeal> viewppondeals = new List<IViewPponDeal>();
            switch (sortType)
            {
                case (int)EDMDetailsSortType.BySequence:
                    deals = totalDeal.Where(x => !edmdetail_list.Any(y => y.Bid == x.BusinessHourGuid)).Take(row * column);
                    break;
                case (int)EDMDetailsSortType.ByOrderedQuantity:
                    viewppondeals = ViewPponDealManager.DefaultManager.ViewPponDealGetListByGuidList(totalDeal.Select(x => x.BusinessHourGuid).ToList());
                    deals = totalDeal.Join(viewppondeals, x => x.BusinessHourGuid, y => y.BusinessHourGuid, (x, y) => new { ViewPponDealTimeSlot = x, Quantity = y.OrderedQuantity ?? 0 })
                        .OrderByDescending(x => x.Quantity).Select(x => x.ViewPponDealTimeSlot).Where(x => !edmdetail_list.Any(y => y.Bid == x.BusinessHourGuid)).Take(row * column);
                    break;
                case (int)EDMDetailsSortType.ByOrderedTotal:
                    viewppondeals = ViewPponDealManager.DefaultManager.ViewPponDealGetListByGuidList(totalDeal.Select(x => x.BusinessHourGuid).ToList());
                    deals = totalDeal.Join(viewppondeals, x => x.BusinessHourGuid, y => y.BusinessHourGuid, (x, y) => new { ViewPponDealTimeSlot = x, Total = y.OrderedTotal ?? 0 })
                        .OrderByDescending(x => x.Total).Select(x => x.ViewPponDealTimeSlot).Where(x => !edmdetail_list.Any(y => y.Bid == x.BusinessHourGuid)).Take(row * column);
                    break;
                case (int)EDMDetailsSortType.ByOrderedDate:
                    viewppondeals = ViewPponDealManager.DefaultManager.ViewPponDealGetListByGuidList(totalDeal.Select(x => x.BusinessHourGuid).ToList());
                    deals = totalDeal.Join(viewppondeals, x => x.BusinessHourGuid, y => y.BusinessHourGuid, (x, y) => new { ViewPponDealTimeSlot = x, Date = y.BusinessHourOrderTimeS })
                        .OrderByDescending(x => x.Date).Select(x => x.ViewPponDealTimeSlot).Where(x => !edmdetail_list.Any(y => y.Bid == x.BusinessHourGuid)).Take(row * column);
                    break;
                default:
                    deals = totalDeal.Where(x => !edmdetail_list.Any(y => y.Bid == x.BusinessHourGuid)).Take(row * column);
                    break;
            }


            if (cityid == PponCityGroup.DefaultPponCityGroup.Travel.CityId && maincityid == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                edmdetail_list.Add(new EdmDetail() { Pid = 0, Type = (int)main_type, CityId = cityid, CityName = cityname, ColumnNumber = 4, RowNumber = row, DisplayCityName = displaycityname, SortType = sortType });
            }
            else
            {
                if (cityid == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId && deals.Count() <= 0)
                {
                    edmdetail_list.Add(new EdmDetail() { Pid = 0, Type = (int)main_type, CityId = cityid, CityName = cityname, ColumnNumber = 0, RowNumber = 0, DisplayCityName = displaycityname, SortType = sortType });
                }
                else
                {
                    edmdetail_list.Add(new EdmDetail() { Pid = 0, Type = (int)main_type, CityId = cityid, CityName = cityname, ColumnNumber = column, RowNumber = row, DisplayCityName = displaycityname, SortType = sortType });
                }
            }

            if (deals.Count() > 0)
            {
                List<Guid> soldOutList = new List<Guid>();
                deals.ForEach(x =>
                {
                    var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(x.BusinessHourGuid);
                    if (string.IsNullOrEmpty(x.SubjectName))
                    {
                        x.SubjectName = (deal != null && deal.IsLoaded) ? ((string.IsNullOrEmpty(deal.AppTitle)) ? deal.ItemName : deal.AppTitle) : x.EventName;
                    }


                    //排除存量條件 2016/08/16 Erin add rule
                    //週六週日依頻道排除
                    if (deliverydate.DayOfWeek == DayOfWeek.Saturday || deliverydate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        //旅遊 < 30 排除
                        if (maincityid == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
                        {
                            if (deal.OrderTotalLimit - deal.OrderedQuantity <= 30)
                            {
                                soldOutList.Add(x.BusinessHourGuid);
                            }
                        }
                        //玩美 + 宅配 < 100 排除
                        else if (maincityid == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId || maincityid == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
                        {
                            if (deal.OrderTotalLimit - deal.OrderedQuantity <= 100)
                            {
                                soldOutList.Add(x.BusinessHourGuid);
                            }
                        }
                        //所有在地美食 < 100 排除
                        else if (maincityid == PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId || maincityid == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity.CityId || maincityid == PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId || maincityid == PponCityGroup.DefaultPponCityGroup.Hsinchu.CityId
                              || maincityid == PponCityGroup.DefaultPponCityGroup.Taichung.CityId || maincityid == PponCityGroup.DefaultPponCityGroup.Tainan.CityId || maincityid == PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId)
                        {
                            if (deal.OrderTotalLimit - deal.OrderedQuantity <= 100)
                            {
                                soldOutList.Add(x.BusinessHourGuid);
                            }
                        }
                        else //品生活、全家...etc < 0 排除
                        {
                            if (deal.OrderTotalLimit - deal.OrderedQuantity <= 0)
                            {
                                soldOutList.Add(x.BusinessHourGuid);
                            }
                        }
                    }
                    else //週一到週五 < 0 排除
                    {
                        if (deal.OrderTotalLimit - deal.OrderedQuantity <= 0)
                        {
                            soldOutList.Add(x.BusinessHourGuid);
                        }
                    }
                });
                foreach (var item in deals)
                {
                    //排除限制檔次
                    if (!soldOutList.Contains(item.BusinessHourGuid))
                    {
                        edmdetail_list.Add(new EdmDetail() { Pid = 0, Title = item.SubjectName, Type = (int)item_type, Bid = item.BusinessHourGuid });
                    }
                }
            }
        }

        public static void AddEdmDetailForPiinLife(DateTime deliverydate, EdmDetailType main_type, EdmDetailType item_type, int column, int row, bool displaycityname, int maincityid, List<EdmDetail> edmdetail_list)
        {
            SystemCodeCollection systemcodecollection = hp.HiDealGetAllCityFromSystemCode();
            int codeId = systemcodecollection.OrderBy(x => x.Seq).First().CodeId;
            var deals = hp.GetViewHiDealTimeslotCollectionByDate(codeId, deliverydate).ToList();

            //SPA專區EDM 品生活區塊(優先挑選美體紓壓 and 彩妝保養)
            if (maincityid == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                List<ViewHiDealTimeslot> tempCol = new List<ViewHiDealTimeslot>();

                ViewHiDealCollection M7deals = hp.ViewHiDealGetTodayDeals(codeId, "7", null, true /*isMain*/ , ViewHiDeal.Columns.CitySeq);
                ViewHiDealCollection M9deals = hp.ViewHiDealGetTodayDeals(codeId, "9", null, true /*isMain*/ , ViewHiDeal.Columns.CitySeq);

                tempCol.AddRange(deals.Where(x => M7deals.Select(y => y.DealId).Contains(x.Id)));
                tempCol.AddRange(deals.Where(x => M9deals.Select(y => y.DealId).Contains(x.Id)).Except(tempCol));
                tempCol.AddRange(deals.Except(tempCol));

                deals = tempCol;
            }

            deals = deals.Where(x => !edmdetail_list.Any(y => y.Bid == x.HiDealGuid)).Take(row * column).ToList();

            edmdetail_list.Add(new EdmDetail() { Pid = 0, Type = (int)main_type, CityId = systemcodecollection.OrderBy(x => x.Seq).First().CodeId, CityName = "品生活", ColumnNumber = column, RowNumber = row, DisplayCityName = displaycityname });
            if (deals.Count() > 0)
            {
                foreach (var item in deals)
                {
                    edmdetail_list.Add(new EdmDetail() { Pid = 0, Title = item.PromoLongDesc, Type = (int)item_type, Bid = item.HiDealGuid });
                }
            }
        }

        public static void AddEdmDetailForAD(DateTime deliverydate, EdmDetailType main_type, PponCity city, List<EdmDetail> edmdetail_list)
        {
            ViewCmsRandomCollection ad_list = EmailFacade.GetNewEdmADByCityIdDate(city, deliverydate);
            edmdetail_list.Add(new EdmDetail() { Pid = 0, AdId = (ad_list.Count > 0) ? ad_list.First().Pid : 0, Type = (int)EdmDetailType.AD, DisplayCityName = false, CityName = "AD" });
        }

        public static void AddEdmDetailForPEZ(List<EdmDetail> edmdetail_list)
        {
            edmdetail_list.Add(new EdmDetail() { Pid = 0, Type = (int)EdmDetailType.PEZ, CityName = "PEZ", DisplayCityName = false });
        }

        public static ViewCmsRandomCollection GetNewEdmADByCityIdDate(PponCity city, DateTime deliverydate)
        {
            return cp.GetViewCmsRandomCollectionByCityIdDate(deliverydate, city.CityId, RandomCmsType.NewEdmAd);
        }

        public static CmsRandomContent GetNewEdmAdById(int id)
        {
            return cp.CmsRandomContentGetById(id);
        }

        public static CmsContent GetPEZCmsContent(string pez_name)
        {
            return cp.CmsContentGet(pez_name);
        }

        public static ViewHiDealTimeslotCollection GetViewHiDealTimeslotCollectionByDate(int cityid, DateTime date)
        {
            return hp.GetViewHiDealTimeslotCollectionByDate(cityid, date);
        }

        public static ViewPponDeal ViewPponDealGetByBusinessHourGuid(Guid bid)
        {
            return pp.ViewPponDealGetByBusinessHourGuid(bid);
        }

        public static ViewPponDealTimeSlotCollection ViewPponDealTimeSlotGetListByCityDate(int cityid, DateTime deliverydate)
        {
            return pp.ViewPponDealTimeSlotGetListByCityDate(cityid, deliverydate);
        }
        public static ViewPponDealTimeSlotCollection ViewPponDealTimeSlotGetWmsListByCityDate(DateTime deliverydate)
        {
            return pp.ViewPponDealTimeSlotGetWmsListByCityDate(deliverydate);
        }

        public static ViewPponDealTimeSlotCollection ViewPponDealTimeSlotByFastShip(int cityid, DateTime deliverydate, string notContainBid)
        {
            return pp.ViewPponDealTimeSlotByFastShip(cityid, deliverydate, notContainBid);
        }

        public static SystemCodeCollection GetPiinLifeCityCode()
        {
            return hp.HiDealGetAllCityFromSystemCode();
        }

        public static HiDealDeal GetHiDealById(int id)
        {
            return hp.HiDealDealGet(id);
        }

        public static HiDealDeal GetHiDealByHiDelaGuid(Guid hid)
        {
            return hp.HiDealDealGet(hid);
        }

        #endregion db

        public static List<EdmDetail> GetNewEdmDetailsByCityIdDate(PponCity city, DateTime deliverydate)
        {
            List<EdmDetail> edmdetail_list = new List<EdmDetail>();
            if (city == PponCityGroup.DefaultPponCityGroup.PBeautyLocation)
            {
                #region NewEDM版型規則

                if (deliverydate.DayOfWeek == DayOfWeek.Saturday || deliverydate.DayOfWeek == DayOfWeek.Sunday)
                {
                    #region sat,sun

                    /* 完美休閒
                      main 完美休閒   1*1+4*2
                           完美休閒  4*2
                           完美休閒  0*0
                           完美休閒  0*0
                          品生活 1
                        */
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 1, 1, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 2, 4, false, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId, PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 2, 4, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId, PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 0, 0, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId, PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 0, 0, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));

                    #endregion sat,sun
                }
                else
                {
                    #region Mon~Fri

                    /* 完美休閒
                      main 完美休閒   1*1+4*2
                           完美休閒  4*2
                           完美休閒  0*0
                           完美休閒  0*0
                          品生活 1
                        */
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 1, 1, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 2, 4, false, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId, PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 2, 4, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId, PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 0, 0, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId, PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 0, 0, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);

                    #endregion Mon~Fri
                }

                #endregion NewEDM版型規則
            }
            else if (city == PponCityGroup.DefaultPponCityGroup.Travel)
            {
                #region NewEDM版型規則

                if (deliverydate.DayOfWeek == DayOfWeek.Saturday || deliverydate.DayOfWeek == DayOfWeek.Sunday)
                {
                    #region sat,sun

                    /* 旅遊
                  main 旅遊  1*4+1*4
                       旅遊  1*2
                       旅遊  0*0
                       旅遊  0*0
                       品生活 1
                    */
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 1, 4, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 1, 4, false, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Travel.CityId, PponCityGroup.DefaultPponCityGroup.Travel.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 2, 4, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Travel.CityId, PponCityGroup.DefaultPponCityGroup.Travel.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 0, 0, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Travel.CityId, PponCityGroup.DefaultPponCityGroup.Travel.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 0, 0, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));

                    #endregion sat,sun
                }
                else
                {
                    #region Mon~Fri

                    /* 旅遊
                  main 旅遊  1*4+1*4
                       旅遊  1*2
                       旅遊  0*0
                       旅遊  0*0
                       品生活 1
                    */
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 1, 4, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 1, 4, false, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Travel.CityId, PponCityGroup.DefaultPponCityGroup.Travel.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 1, 2, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Travel.CityId, PponCityGroup.DefaultPponCityGroup.Travel.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 0, 0, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Travel.CityId, PponCityGroup.DefaultPponCityGroup.Travel.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 0, 0, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);

                    #endregion Mon~Fri
                }

                #endregion NewEDM版型規則
            }
            else if (city == PponCityGroup.DefaultPponCityGroup.AllCountry)
            {
                #region NewEDM版型規則

                if (deliverydate.DayOfWeek == DayOfWeek.Saturday || deliverydate.DayOfWeek == DayOfWeek.Sunday)
                {
                    #region sat,sun

                    /* 宅配
                       Main宅配  1*1+4*2
                       宅配  4*2
                       24h  3*2
                       72h  3*2
                       品生活 1
                    */
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 1, 1, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 2, 4, false, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.AllCountry.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 2, 4, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityId, PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 2, 3, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityId, PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 2, 3, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));

                    #endregion sat,sun
                }
                else
                {
                    #region Mon~Fri

                    /* 宅配
                       Main宅配  1*2+4*3
                       宅配  3*3
                       24h  3*3
                       72h  3*3
                       品生活 1
                    */
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 2, 1, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 3, 4, false, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.AllCountry.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 3, 3, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityId, PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 3, 3, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityId, PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 3, 3, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);

                    #endregion Mon~Fri
                }

                #endregion NewEDM版型規則
            }
            else if (city == PponCityGroup.DefaultPponCityGroup.PponSelect)
            {
                #region NewEDM版型規則

                if (deliverydate.DayOfWeek == DayOfWeek.Saturday || deliverydate.DayOfWeek == DayOfWeek.Sunday)
                {
                    #region sat,sun

                    /* 宅配
                       Main宅配  1*1+4*2
                       宅配  4*2
                       宅配  4*2
                       宅配  2*2
                       品生活 1
                    */
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.PponSelect.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 1, 1, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.PponSelect.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 2, 4, false, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.PponSelect.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 2, 4, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.PponSelect.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 2, 4, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.PponSelect.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 2, 2, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));

                    #endregion sat,sun
                }
                else
                {
                    #region Mon~Fri

                    /* 宅配
                       Main宅配  1*2+4*3
                       宅配  4*3
                       宅配  2*3
                       旅遊  0*0
                       品生活 1
                    */
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.PponSelect.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 2, 1, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.PponSelect.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 3, 4, false, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.PponSelect.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 3, 4, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.PponSelect.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 3, 2, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.PponSelect.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 0, 0, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);

                    #endregion Mon~Fri
                }

                #endregion NewEDM版型規則
            }
            else if (city == PponCityGroup.DefaultPponCityGroup.Piinlife)
            {

                #region NewEDM版型規則

                if (deliverydate.DayOfWeek == DayOfWeek.Saturday || deliverydate.DayOfWeek == DayOfWeek.Sunday)
                {
                    #region sat,sun

                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Family.CityId, PponCityGroup.DefaultPponCityGroup.Family.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 0, 0, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Family.CityId, PponCityGroup.DefaultPponCityGroup.Family.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 0, 0, false, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Family.CityId, PponCityGroup.DefaultPponCityGroup.Family.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 0, 0, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Family.CityId, PponCityGroup.DefaultPponCityGroup.Family.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 0, 0, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Family.CityId, PponCityGroup.DefaultPponCityGroup.Family.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 0, 0, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));

                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.PponPiinLife_1, EdmDetailType.PponPiinLife_Item_1, 2, 4, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));

                    #endregion sat,sun
                }
                else
                {
                    #region Mon~Fri

                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Family.CityId, PponCityGroup.DefaultPponCityGroup.Family.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 0, 0, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Family.CityId, PponCityGroup.DefaultPponCityGroup.Family.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 0, 0, false, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Family.CityId, PponCityGroup.DefaultPponCityGroup.Family.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 0, 0, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Family.CityId, PponCityGroup.DefaultPponCityGroup.Family.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 0, 0, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Family.CityId, PponCityGroup.DefaultPponCityGroup.Family.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 0, 0, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);

                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.PponPiinLife_1, EdmDetailType.PponPiinLife_Item_1, 2, 4, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);

                    #endregion Mon~Fri
                }

                #endregion NewEDM版型規則
            }
            else
            {
                #region NewEDM版型規則

                if (deliverydate.DayOfWeek == DayOfWeek.Saturday || deliverydate.DayOfWeek == DayOfWeek.Sunday)
                {
                    #region sat,sun

                    /* 在地
                       在地 1+4*2 
                       宅配  4*2
                       玩美  3*2
                       旅遊  2*2
                       品生活 1
                    */
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 1, 1, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 2, 4, false, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.AllCountry.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 2, 4, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId, PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 2, 3, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Travel.CityId, PponCityGroup.DefaultPponCityGroup.Travel.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 2, 2, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));

                    #endregion sat,sun
                }
                else
                {
                    #region Mon~Fri

                    /* 在地
                       在地 1+3*3
                       完美休閒   2*3
                       旅遊  1*3
                       宅配  3*3
                       品生活 1
                    */
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, 1, 1, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(city.CityId, city.CityName, deliverydate, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, 3, 3, false, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, PponCityGroup.DefaultPponCityGroup.AllCountry.CityName, deliverydate, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, 3, 3, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId, PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityName, deliverydate, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, 3, 2, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Travel.CityId, PponCityGroup.DefaultPponCityGroup.Travel.CityName, deliverydate, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, 3, 1, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);

                    #endregion Mon~Fri
                }

                #endregion NewEDM版型規則
            }
            if (city != PponCityGroup.DefaultPponCityGroup.Piinlife)
            {

                if (deliverydate.DayOfWeek == DayOfWeek.Saturday || deliverydate.DayOfWeek == DayOfWeek.Sunday)
                {
                    #region sat,sun

                    AddEdmDetailForAD(deliverydate, EdmDetailType.AD, city, edmdetail_list);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Piinlife.CityId, PponCityGroup.DefaultPponCityGroup.Piinlife.CityName, deliverydate, EdmDetailType.PponPiinLife_1, EdmDetailType.PponPiinLife_Item_1, 1, 1, true, city.CityId, edmdetail_list, ((deliverydate.DayOfWeek == DayOfWeek.Saturday) ? (int)EDMDetailsSortType.ByOrderedTotal : (int)EDMDetailsSortType.BySequence));

                    #endregion sat,sun
                }
                else
                {
                    #region Mon~Fri

                    AddEdmDetailForAD(deliverydate, EdmDetailType.AD, city, edmdetail_list);
                    AddEdmDetailByCity(PponCityGroup.DefaultPponCityGroup.Piinlife.CityId, PponCityGroup.DefaultPponCityGroup.Piinlife.CityName, deliverydate, EdmDetailType.PponPiinLife_1, EdmDetailType.PponPiinLife_Item_1, 1, 1, true, city.CityId, edmdetail_list, (int)EDMDetailsSortType.ByOrderedDate);

                    #endregion Mon~Fri
                }

            }
            return edmdetail_list;
        }
        /// <summary>
        /// 重設 sole edm 要發送的 email清單
        /// </summary>
        /// <param name="emails"></param>
        public static void ResetSoloEdmSendEmails(int mainId, IEnumerable<string> emails)
        {
            IPponEntityProvider pep = ProviderFactory.Instance().GetDefaultProvider<IPponEntityProvider>();
            pep.DeleteSoleEdmSendEmailsByMainId(mainId);
            pep.SaveSoloEdmSendEmails(emails.Select(t => new SoloEdmSendEmail { Email = t, MainId = mainId }).ToList());
        }

        public static int GetSoloEdmSendListCountById(int mainId)
        {
            IPponEntityProvider pep = ProviderFactory.Instance().GetDefaultProvider<IPponEntityProvider>();
            return pp.SoloEdmSendCountGetByMainId(mainId) +
                pep.GetSoloEdmSendEmailCount(mainId);
        }

        public static string GetCleanHtmlCode(int id, string path, bool utfEncoding, params string[] querystring)
        {
            //回傳html code，省略.net產生的tag
            string query = string.Empty;
            foreach (string item in querystring)
            {
                query += "&" + item;
            }

            try
            {
                HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(string.Format("{0}/ppon/{1}.aspx?eid={2}{3}", 
                    config.SiteUrl, path, id, query));
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                        delegate { return true; };
                }
                string html = (new StreamReader(wr.GetResponse().GetResponseStream(), (utfEncoding ? Encoding.UTF8 : Encoding.GetEncoding("Big5")))).ReadToEnd();
                html = Regex.Replace(html, "<input[^>]*id=\"__VIEWSTATE\"[^>]*>", string.Empty, RegexOptions.IgnoreCase);
                html = Regex.Replace(html, "<form[^>]*>", string.Empty, RegexOptions.IgnoreCase).Replace("</form>", string.Empty).Replace("style=\"display: none\"", string.Empty);
                return html;
            }
            catch (Exception ex)
            {
                throw (new Exception("WebRequest error:eid:" + id + ";query:" + query, ex));
            }
        }

        public static string GetSoloEdmHtmlCode(int id, string path, bool utfEncoding, params string[] querystring)
        {
            string query = string.Empty;
            foreach (string item in querystring)
            {
                query += "&" + item;
            }
            try
            {
                HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(string.Format("{0}/Event/{1}?eid={2}{3}", 
                    config.SiteUrl, path, id, query));
                string html = (new StreamReader(wr.GetResponse().GetResponseStream(), (utfEncoding ? Encoding.UTF8 : Encoding.GetEncoding("Big5")))).ReadToEnd();
                html = Regex.Replace(html, "<input[^>]*id=\"__VIEWSTATE\"[^>]*>", string.Empty, RegexOptions.IgnoreCase);
                html = Regex.Replace(html, "<form[^>]*>", string.Empty, RegexOptions.IgnoreCase).Replace("</form>", string.Empty).Replace("style=\"display: none\"", string.Empty);
                return html;
            }
            catch(Exception ex)
            {
                throw (new Exception("WebRequest error:eid:" + id + ";query:" + query, ex));
            }
        }

        private static string GetNewEdmPath()
        {
            if (config.NewEdmPath.StartsWith("/"))
            {
                return Helper.MapPath(config.NewEdmPath);
            }
            return config.NewEdmPath;
        }

        public static string GenerateNewEdmFile(int id, string user_name, bool compress, string[] test_emails = null)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Reset();

            StringBuilder sb = new StringBuilder();
            string errormessage = string.Empty;
            string locationString = string.Empty;

            int totalcount = 0;
            EasyDigitCipher cipher = new EasyDigitCipher();
            if (!Directory.Exists(GetNewEdmPath()))
            {
                Directory.CreateDirectory(GetNewEdmPath());
            }

            EdmMain edmmain = pp.GetEdmMainById(id);
            PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(edmmain.CityId);

            locationString = "17Life " + city.CityName;
            if (city.CityCode == "PIN")
            {
                locationString = "PiinLife品生活";
            }
            else if (city.CityCode != "TRA" && city.CityCode != "PBU" && city.CityCode != "ALL" &&
                city.CityCode != "FAM" && city.CityCode != "com" && city.CityCode != "SLT")
            {
                if (city.CityName == "桃園")
                {
                    locationString = "17Life 美食-" + "桃竹苗";
                }
                else if (city.CityName == "中彰")
                {
                    locationString = "17Life 美食-" + "中彰投";
                }
                else if (city.CityName == "高屏")
                {
                    locationString = "17Life 美食-" + "雲嘉南高";
                }
                else
                {
                    locationString = "17Life 美食-" + city.CityName;
                }   
            }

            if (city.CityName == "旅遊")
            {
                locationString = "17Life " + "旅遊．玩美．休閒";
            }

            if ((edmmain.Id != 0 && !edmmain.Pause) || test_emails != null)
            {
                sw.Start();
                DateTime time = test_emails == null ? edmmain.DeliveryDate : DateTime.Now;
                string filename = string.Format("Eid-{0}-{1}-{2}-{3}", id, city == null ? "ERR" : city.CityCode, edmmain.DeliveryDate.ToString("MMddHHmm"), DateTime.Now.ToString("MMddHHmmss"));

                List<int> csvFileList = new List<int>();

                #region generate html

                string html = GetCleanHtmlCode(id, "onlinenewedm", true, "pa=" + cipher.Encrypt(edmmain.DeliveryDate.ToString("yyyyddMM")));
                Encoding utf8 = new UTF8Encoding(false);


                using (StreamWriter writer = new StreamWriter(GetNewEdmPath() + "/" + filename + ".html", false, utf8))
                {
                    if (compress)
                    {
                        html = MinifyHtml(html);
                    }
                    writer.Write(html);
                }


                #endregion generate html

                #region generate ini

                using (StreamWriter writer = new StreamWriter(GetNewEdmPath() + "/" + filename + ".ini", false, Encoding.UTF8))
                {
                    sb.AppendLine("[SYMBOL]");
                    sb.AppendLine("FIELD00=%%RCPT_ADDR%%");
                    sb.AppendLine("FIELD01=%%RCPT_NAME%%");
                    sb.AppendLine("FIELD02=%%UnSubscription%%");
                    sb.AppendLine("FIELD03=%%OpenLogId%%");
                    sb.AppendLine();
                    sb.AppendLine("[HEADER]");
                    sb.AppendLine("SenderAddr=" + config.EdmEmail);
                    sb.AppendLine("SenderName=" + locationString);
                    sb.AppendLine("RecipientAddr=%%RCPT_ADDR%%");
                    sb.AppendLine("RecipientName" + locationString);
                    sb.AppendLine("ReplyToAddress=" + config.ServiceEmail);
                    sb.AppendLine("Subject=" + Helper.GetMaxString(edmmain.Subject, 45, "..."));
                    sb.AppendLine();
                    sb.AppendLine("[SCHEDULE]");
                    sb.AppendLine("TIME=" + time.ToString("yyyy-MM-dd HH:mm:ss"));
                    sb.AppendLine("TASKNAME=" + filename);
                    writer.Write(sb.ToString());
                    sb.Clear();
                }


                #endregion generate ini

                #region generate csv

                if (test_emails == null)
                {
                    //job會跑這邊，產生csvFileList上傳到edm用的ftp
                    totalcount = GetSubscriptionCollectionByCityId(edmmain, sb, filename);
                }
                else
                {
                    foreach (string item in test_emails)
                    {
                        totalcount++;
                        sb.AppendLine(item + "," + item + "," + GetUnSubsrciptionString(item, edmmain.CityId));
                    }

                    using (StreamWriter writer = new StreamWriter(GetNewEdmPath() + "/" + filename + ".csv", false, Encoding.GetEncoding(950)))
                    {
                        writer.Write(sb.ToString());
                    }
                    sb.Clear();
                }


                sw.Stop();

                #endregion generate csv


                try
                {
                    if (config.EnableNewEdmUploadFtp)
                    {
                        CommonFacade.FtpCelloPoint("17P EDM", GetNewEdmPath(), GetNewEdmPath(), GetNewEdmPath(), filename, true);
                    }
                    if (test_emails == null)
                    {
                        edmmain.UploadDate = DateTime.Now;
                        edmmain.Message += user_name + ":upload " + DateTime.Now.ToString("MMdd HH:mm") + ";";
                        pp.SaveEdmMain(edmmain);
                    }
                }
                catch (Exception error)
                {
                    errormessage = error.Message;
                    edmmain.Message += error.Message + ": " + DateTime.Now.ToString("MMdd HH:mm") + ";";
                    pp.SaveEdmMain(edmmain);
                }
                
                errormessage = (test_emails != null ? "測試" : string.Empty) + edmmain.CityName + ":發送" 
                    + totalcount + "份;產生時間:" + sw.Elapsed.TotalSeconds + "秒" + errormessage;
            }
            else
            {
                errormessage = string.Format("{0}:{1}", city.CityName, (edmmain.Pause ? "已設定暫停發送;" : "查無資料;"));
            }

            return errormessage;
        }


        private static Dictionary<string, string> unSubsrciptionEmailCodes = null;
        public static string GetUnSubsrciptionString(string email, int city_id)
        {
            if (unSubsrciptionEmailCodes == null)
            {
                string codesFilePath = Path.Combine(config.WebAppDataPath, "UnSubsrciptionEmailCode.txt");
                if (File.Exists(codesFilePath))
                {
                    try
                    {
                        unSubsrciptionEmailCodes = serializer.Deserialize<Dictionary<string, string>>(File.ReadAllText(codesFilePath));
                    }
                    catch (Exception ex)
                    {
                        log.Warn("解析 UnSubsrciptionEmailCode.txt 失敗");
                    }
                }
                if (unSubsrciptionEmailCodes == null)
                {
                    unSubsrciptionEmailCodes = new Dictionary<string, string>();
                }
            }

            string encryptCode;
            if (unSubsrciptionEmailCodes.TryGetValue(email, out encryptCode) == false)
            {
                encryptCode = Helper.EncryptEmail(email);
            }
            
            return config.SiteUrl + string.Format("/ppon/unsubscription2.aspx?m={0}", encryptCode);
        }

        public static int GetSubscriptionCollectionByCityId(EdmMain edmMain, StringBuilder sb, string filename)
        {           
            int totalcount = 0;
            
            List<SubscriptionModel> mailList = pp.SubscriptionGetListByCategoryId(
                PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(edmMain.CityId).CategoryId);

            if (mailList.Count > 0)
            {
                if (config.EnableNewEdmSendRules)
                {
                    mailList = mailList.Where(x => (x.Status & (int)SubscribeType.Default) > 0).ToList();
                }
                else
                {
                    mailList = mailList.ToList();
                }
            }

            foreach (var item in mailList)
            {
                if (item.Reliable == null)
                {
                    if (!RegExRules.CheckEmail(item.Email))
                    {
                        pp.SubscriptionUpdateReliable(item.Email, false);
                        continue;
                    }
                    else
                    {
                        item.Reliable = true;
                        pp.SubscriptionUpdateReliable(item.Email, true);
                    }
                }

                if (item.Reliable != null && item.Reliable == true)
                {
                    sb.AppendLine(item.Email + "," + item.Email + "," + GetUnSubsrciptionString(item.Email, edmMain.CityId));
                    totalcount++;
                }
            }            

            try
            {
                using (StreamWriter writer = new StreamWriter(GetNewEdmPath() + "/" + filename + ".csv", false, Encoding.GetEncoding(950)))
                {
                    writer.Write(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                log.Warn(string.Format("Error while generating {0} file: ", filename), ex);
            }

            return totalcount;
        }

        public static DateTime GetEdmSendTime(int cityId, DateTime deliverydate)
        {
            if (cityId == PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId)
            {
                return deliverydate.AddHours(6);
            }
            else if (cityId == PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId)
            {
                return deliverydate.AddHours(7).AddMinutes(1);
            }
            else if (cityId == PponCityGroup.DefaultPponCityGroup.Hsinchu.CityId)
            {
                return deliverydate.AddHours(7).AddMinutes(2);
            }
            else if (cityId == PponCityGroup.DefaultPponCityGroup.Taichung.CityId)
            {
                return deliverydate.AddHours(7).AddMinutes(3);
            }
            else if (cityId == PponCityGroup.DefaultPponCityGroup.Tainan.CityId)
            {
                return deliverydate.AddHours(7).AddMinutes(4);
            }
            else if (cityId == PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId)
            {
                return deliverydate.AddHours(7).AddMinutes(5);
            }
            else if (cityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                return deliverydate.AddHours(7).AddMinutes(6);
            }
            else if (cityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                return deliverydate.AddHours(7).AddMinutes(7);
            }
            else if (cityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
            {
                return deliverydate.AddHours(6).AddMinutes(10);
            }
            else if (cityId == PponCityGroup.DefaultPponCityGroup.PponSelect.CityId)
            {
                return deliverydate.AddHours(7).AddMinutes(9);
            }
            else if (cityId == PponCityGroup.DefaultPponCityGroup.Piinlife.CityId)
            {
                return deliverydate.AddHours(12).AddMinutes(30);
            }
            else
            {
                return deliverydate.AddHours(12);
            }
        }

        public static string GetEdmCpa(int cityId)
        {
            var cpa = "17_systemEDM";

            if (cityId == CategoryManager.Default.PponSelect.CityId)
            {
                cpa = "eDM_delivery";
            }
            else if (cityId == CategoryManager.Default.Taipei.CityId)
            {
                cpa = "eDM_TP";
            }
            else if (cityId == CategoryManager.Default.Taoyuan.CityId)
            {
                cpa = "eDM_TY";
            }
            else if (cityId == CategoryManager.Default.Hsinchu.CityId)
            {
                cpa = "eDM_XZ";
            }
            else if (cityId == CategoryManager.Default.Taichung.CityId)
            {
                cpa = "eDM_TC";
            }
            else if (cityId == CategoryManager.Default.Tainan.CityId)
            {
                cpa = "eDM_TN";
            }
            else if (cityId == CategoryManager.Default.Kaohsiung.CityId)
            {
                cpa = "eDM_GX";
            }
            else if (cityId == CategoryManager.Default.Beauty.CityId)
            {
                cpa = "eDM_PBU";
            }
            else if (cityId == CategoryManager.Default.Travel.CityId)
            {
                cpa = "eDM_TRA";
            }
            else if (cityId == CategoryManager.Default.Delivery.CityId)
            {
                cpa = "eDM_ALL";
            }
            else if (cityId == CategoryManager.Default.Piinlife.CityId)
            {
                cpa = "eDM_piinlife";
            }

            return cpa;
        }
        /// <summary>
        /// 產生並上傳 每日新版EDM檔案
        /// </summary>
        /// <param name="username"></param>
        /// <param name="emails"></param>
        /// <returns></returns>
        public static string GenerateDailyEdm(string username, string[] emails = null)
        {
            //刪除前日以前的檔案
            if (Directory.Exists(GetNewEdmPath()))
            {
                foreach (var filepath in Directory.GetFiles(GetNewEdmPath()))
                {
                    var fileinfo = new FileInfo(filepath);
                    if (fileinfo.CreationTime < DateTime.Now.AddDays(-2))
                    {
                        fileinfo.Delete();
                    }
                }
            }
            string result = string.Empty;

            List<EdmMain> edmMains = GetNewDailyEdmMainListWasNotUploaded(DateTime.Today);

            foreach (var edmMain in edmMains)
            {
                List<EdmDetail> edmdetaildata = EmailFacade.GetNewEdmDetailsByPid(edmMain.Id);
                if (edmdetaildata.Count == 0)
                {
                    //沒選檔則不發送
                    result += string.Format("{0}:{1}", edmMain.CityName, "查無檔次資料<br />");
                }
                else
                {
                    result += GenerateNewEdmFile(edmMain.Id, username, true, emails) + "<br/>";
                }

            }
            return result;
        }

        public static Dictionary<EdmDetailType, EdmDetailType> GetNewEdmTypePair()
        {
            Dictionary<EdmDetailType, EdmDetailType> type_list = new Dictionary<EdmDetailType, EdmDetailType>();
            type_list.Add(EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items);
            type_list.Add(EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items);
            type_list.Add(EdmDetailType.Area_1, EdmDetailType.Area_1_Items);
            type_list.Add(EdmDetailType.Area_2, EdmDetailType.Area_2_Items);
            type_list.Add(EdmDetailType.Area_3, EdmDetailType.Area_3_Items);
            type_list.Add(EdmDetailType.PponPiinLife_1, EdmDetailType.PponPiinLife_Item_1);
            return type_list;
        }

        public static string MinifyHtml(string html)
        {
            string mini_html = Regex.Replace(html, @">\s+<", "><", RegexOptions.Compiled);
            mini_html = Regex.Replace(mini_html, @"\n\s+", string.Empty, RegexOptions.Compiled);
            return mini_html;
        }

        #endregion new edm

        #region solo edm
        public static SoloEdmMainCollection GetSoloEdmMainByType(int type)
        {
            return pp.GetSoloEdmMainByType(type);
        }

        public static SoloEdmMainCollection GetSoloEdmMainByDate(DateTime sendDate)
        {
            return pp.GetSoloEdmMainByDate(sendDate);
        }

        public static SoloEdmMain GetSoloEdmMainById(int eid)
        {
            return pp.GetSoloEdmMainById(eid);
        }

        public static SoloEdmBrandCollection GetSoloEdmBrandById(int eid)
        {
            return pp.GetSoloEdmBrandById(eid);
        }

        public static SoloEdmPponCollection GetSoloEdmPponById(int eid)
        {
            return pp.GetSoloEdmPponById(eid);
        }

        public static void SaveSoloEdmMain(SoloEdmMain soloedm)
        {
            pp.SaveSoloEdmMain(soloedm);
        }

        public static void SaveSoloEdmBrand(SoloEdmBrand soloEdmBrand)
        {
            pp.SaveSoloEdmBrand(soloEdmBrand);
        }
        public static void SaveSoloEdmPpon(SoloEdmPpon soloEdmPpon)
        {
            pp.SaveSoloEdmPpon(soloEdmPpon);
        }

        public static void SaveSoloEdmSendUser(SoloEdmSendUser sendUser)
        {
            pp.SaveSoloEdmSendUser(sendUser);
        }

        public static void SoloEdmSetAllMembers(int MainId)
        {
            pp.SoloEdmSetAllMembers(MainId);
        }

        public static void DeleteSoloEdmBrandByMainId(int MainId)
        {
            pp.DeleteSoloEdmBrandByMainId(MainId);
        }
        public static void DeleteSoloEdmPponByMainId(int MainId)
        {
            pp.DeleteSoloEdmPponByMainId(MainId);
        }
        public static void DeleteSoloEdmSendUserByMainId(int mainId)
        {
            pp.DeleteSoloEdmSendUserByMainId(mainId);
        }

        public static void EdmStatusUpdate()
        {
            int tmpCount = 0-config.RemoveMonthsCount;
            //抓出最後一筆ID
            int total_id = pp.SubscriptionGetLatestId();
            //分100次更新資料
            int range = (int)Math.Ceiling(total_id / ((decimal)500));

            for (int i = 1; i < total_id; i = i + range)
            {
                pp.SubscriptionStatusSleepUpdate(tmpCount, i, i + range);
            }

            for (int i = 1; i < total_id; i = i + range)
            {
                pp.SubscriptionStatusDefaultUpdate(tmpCount, i, i + range);
            }
            
        }

        public static string SendSoloEdm()
        {
            var edmList = pp.GetSoloEdmMainByDate(DateTime.Now).Where(x=>x.UploadDate==null);
            var msg = string.Empty;
            foreach (var edm in edmList)
            {
                msg = GenerateSoloEdmFile(edm.Id);
            }
            return msg;
        }

        public static string GenerateSoloEdmFile(int eid,string[] testEmails = null)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Reset();

            StringBuilder sb = new StringBuilder();
            string errormessage = string.Empty;
            var soloEdm = pp.GetSoloEdmMainById(eid);
            string locationString = "17Life優惠情報";
            int totalcount = 0;
            sw.Start();
            DateTime time = testEmails == null ? soloEdm.SendDate : DateTime.Now;
            string filename = string.Format("Eid-{0}-{1}-{2}-{3}",
                eid, "SoloEDM", soloEdm.SendDate.ToString("MMddHHmm"), DateTime.Now.ToString("MMddHHmmss"));
            EasyDigitCipher cipher = new EasyDigitCipher();
            List<int> csvFileList = new List<int>();

            #region generate html

            string html = GetSoloEdmHtmlCode(eid, "onlinesoloedm", true, "pa=" + cipher.Encrypt(soloEdm.SendDate.ToString("yyyyddMM")));
            Encoding utf8 = new UTF8Encoding(false);

            using (StreamWriter writer = new StreamWriter(GetNewEdmPath() + "/" + filename + ".html", false, utf8))
            {
                html = MinifyHtml(html);
                writer.Write(html);
            }

            #endregion generate html

            #region generate ini

            using (StreamWriter writer = new StreamWriter(GetNewEdmPath() + "/" + filename + ".ini", false, Encoding.UTF8))
            {
                sb.AppendLine("[SYMBOL]");
                sb.AppendLine("FIELD00=%%RCPT_ADDR%%");
                sb.AppendLine("FIELD01=%%RCPT_NAME%%");
                sb.AppendLine();
                sb.AppendLine("[HEADER]");
                sb.AppendLine("SenderAddr=" + config.EdmEmail);
                sb.AppendLine("SenderName=" + locationString);
                sb.AppendLine("Subject=" + Helper.GetMaxString(soloEdm.Subject, 45, "..."));
                sb.AppendLine();
                sb.AppendLine("[SCHEDULE]");
                sb.AppendLine("author=mkd@17life.com");
                sb.AppendLine("TASKNAME=" + filename);
                sb.AppendLine("CheckLink=false");
                sb.AppendLine("TIME=" + time.ToString("yyyy-MM-dd HH:mm:ss"));

                writer.Write(sb.ToString());
                sb.Clear();
            }

            #endregion generate ini

            #region generate csv

            if (testEmails == null)
            {
                //var userList = pp.GetSoloEdmSendUserById(eid);
                //foreach (var userid in userList)
                //{
                //    var user = MemberFacade.GetMember(userid.UserId);
                //    totalcount++;
                //    sb.AppendLine(user.UserEmail + "," + user.UserEmail);
                //}
                List<string> emails = pep.GetSoloEdmSendEmails(eid);
                foreach (string email in emails)
                {
                    totalcount++;
                    sb.AppendLine(string.Format("{0},{0}", email));
                }
            }
            else
            {
                foreach (string item in testEmails)
                {
                    totalcount++;
                    sb.AppendLine(item + "," + item );
                }
            }

            using (StreamWriter writer = new StreamWriter(GetNewEdmPath() + "/" + filename + ".csv", false, Encoding.GetEncoding(950)))
            {
                writer.Write(sb.ToString());
            }

            sb.Clear();

            sw.Stop();

            #endregion generate csv

            if (config.EnableNewEdmUploadFtp)
            {
                try
                {
                    CommonFacade.FtpCelloPoint("17P SOLOEDM", GetNewEdmPath(), GetNewEdmPath(), GetNewEdmPath(), filename, true);
                    if (testEmails == null)
                    {
                        soloEdm.UploadDate = DateTime.Now;
                        pp.SaveSoloEdmMain(soloEdm);
                        //pp.DeleteSoloEdmSendUserByMainId(soloEdm.Id);
                        //pep.DeleteSoleEdmSendEmailsByMainId(soloEdm.Id);
                    }    
                }
                catch (Exception error)
                {
                    errormessage = error.Message;
                }
            }

            var message =  "17Life優惠情報" + (errormessage == string.Empty ? (testEmails == null ? (":發送" + totalcount + "份;產生時間:" + sw.Elapsed.TotalSeconds + "秒") : ("測試發送" + totalcount + "份;產生時間: " + sw.Elapsed.TotalSeconds + "秒")) : errormessage);

            return message;
        }

        #endregion

        #region business_mail

        public static void SendChangeSellerMail(Seller seller, Seller tempseller)
        {
            if (seller == null || tempseller == null || !config.IsSendWarnEmail)
            {
                return;
            }

            bool IsSendmail = false;
            StringBuilder operatemabody = new StringBuilder();
            if (seller.CompanyName != tempseller.CompanyName)
            {
                operatemabody.Append("簽約公司名稱：" + seller.CompanyName + "／");
                IsSendmail = true;
            }

            if (seller.SignCompanyID != tempseller.SignCompanyID)
            {
                operatemabody.Append("簽約公司ID/統一編號：" + seller.SignCompanyID + "／");
                IsSendmail = true;
            }

            if (seller.CompanyBankCode != tempseller.CompanyBankCode)
            {
                operatemabody.Append("銀行代號：" + seller.CompanyBankCode + "／");
                IsSendmail = true;
            }

            if (seller.CompanyBranchCode != tempseller.CompanyBranchCode)
            {
                operatemabody.Append("分行代號：" + seller.CompanyBranchCode + "／");
                IsSendmail = true;
            }

            if (seller.CompanyAccount != tempseller.CompanyAccount)
            {
                operatemabody.Append("帳 號：" + seller.CompanyAccount + "／");
                IsSendmail = true;
            }

            if (seller.CompanyAccountName != tempseller.CompanyAccountName)
            {
                operatemabody.Append("戶 名：" + seller.CompanyAccountName + "／");
                IsSendmail = true;
            }

            if (seller.CompanyID != tempseller.CompanyID)
            {
                operatemabody.Append("受款人ID/統一編號：" + seller.CompanyID + "／");
                IsSendmail = true;
            }

            if (operatemabody.Length > 0)
            {
                operatemabody.Remove(operatemabody.Length - 1, 1);
            }

            List<MultipleSalesModel> model = SellerFacade.SellerSaleGetBySellerGuid(seller.Guid);

            //非同步
            if (IsSendmail)
            {
                AsyncSendMail asyncsend = new AsyncSendMail(WarningMailSended);
                IAsyncResult result;
                result = asyncsend.BeginInvoke(operatemabody, tempseller.Guid, seller.SellerName, model, seller.SellerId, "", null, null);
            }
        }

        private static void WarningMailSended(StringBuilder operatemabody, Guid sellerguid, string sellername, List<MultipleSalesModel> model, string sellerid, string storeguid = "")
        {
            MailMessage msg = new MailMessage();
            Guid _storeguid = string.IsNullOrEmpty(storeguid) ? Guid.Empty : new Guid(storeguid);
            msg.IsBodyHtml = true;
            msg.From = new MailAddress(config.SystemEmail);

            if (operatemabody.Length > 0)
            {
                msg.Subject = "財務資料修改異動通知";
                msg.To.Clear();
                ViewPponDealStoreCollection vpdsDelList = pp.ViewPponDealStoreGetListDeliverTime(DateTime.Now, sellerguid, _storeguid);

                operatemabody.Append("<br/>相關的檔次：<br/>");
                foreach (ViewPponDealStore vpds in vpdsDelList)
                {
                    List<string> citylist = new JsonSerializer().Deserialize<List<string>>(vpds.CityList);
                    string cityStr = "";
                    foreach (string c in citylist)
                    {
                        PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(int.Parse(c));
                        cityStr += "[" + city.CityName + "]";
                    }
                    operatemabody.Append(@"<a href='" + config.SiteUrl + "/controlroom/ppon/setup.aspx?bid=" +
                        vpds.ProductGuid + "'>" + cityStr + "-" + vpds.UniqueId + "-" + vpds.ProductName + "</a><br/>");
                }
                string title = @"您好：<br/>以下賣家／分店已更改匯款資料，請您確認檔次是否已點選Save，完成作業後，請轉信告知相關人員需修改哪些電子工單。<br/>謝謝！<br/><br/>"
                                + string.Format("賣家：{0}({1})<br/>更改資料：", sellername, sellerid);
                operatemabody.Insert(0, title);
                msg.Body = operatemabody.ToString();


                foreach (var m in model)
                {
                    //開發業務
                    string sales = ProposalFacade.PponAssistantEmailGet(m.DevelopeSalesDeptId);
                    if (!msg.To.Contains(new MailAddress(sales)))
                        msg.To.Add(sales);

                    //經營業務
                    sales = ProposalFacade.PponAssistantEmailGet(m.OperationSalesDeptId);
                    if (!msg.To.Contains(new MailAddress(sales)))
                        msg.To.Add(sales);
                }



                if (config.IsSendWarnEmail && vpdsDelList.Count > 0)
                {
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
            }
        }
        #endregion

        #region "訂單凍結請款通知信"
        /// <summary>
        /// 訂單凍結請款通知信
        /// </summary>
        /// <param name="freetype"></param>
        /// <param name="oid"></param>
        /// <param name="freezeReason"></param>
        public static void SendFreezeMail(FreezeType freetype, Guid oid, string freezeReason)
        {
            List<string> mails = new List<string>();
            string subject = createFreezeMailSuject(freetype);
            string content = "";



            mails.Add(config.SalesSpecialAssistantEmail);//特助email


            switch (freetype)
            {
                case FreezeType.FreezeAccounting:
                case FreezeType.CancelFreezeAccounting:
                    DealAccounting das = pp.DealAccountingGet(oid);
                    ViewDealPropertyBusinessHourContent dpbs = pp.ViewDealPropertyBusinessHourContentGetByBid(oid);
                    if (das != null)
                    {
                        if (RegExRules.CheckEmail(dpbs.Email))
                        {
                            mails.Add(dpbs.Email);//業務email        

                            //取得業務的主管
                            ViewEmployee emp = humanProv.ViewEmployeeGet("email", dpbs.Email);
                            var emps = humanProv.EmployeeCollectionGetByFilter("dept_id", emp.DeptId).Where(x => x.DeptManager != null);
                            if (emps != null)
                            {
                                IEnumerable<Employee> manager = emps.Where(x => x.DeptManager.Contains(emp.DeptId));
                                foreach (Employee m in manager)
                                {
                                    var _manager = humanProv.ViewEmployeeGet("emp_id", m.EmpId);
                                    if (RegExRules.CheckEmail(_manager.Email))
                                    {
                                        mails.Add(_manager.Email);//業務的主管email        
                                    }
                                }
                            }
                        }


                        //取得該檔次的所有訂單
                        CashTrustLogCollection ctlogs = mp.CashTrustLogGetListByBid(oid);
                        List<Guid> guid = ctlogs.Select(x => x.BusinessHourGuid ?? Guid.Empty).Distinct().ToList();
                        ViewDealPropertyBusinessHourContentCollection vdps = pp.ViewDealPropertyBusinessHourContentGetList(guid);


                        string _freezeReason = "";
                        string _freezeUserId = "";
                        DateTime _freezeDateTime = DateTime.Now;
                        string _cancelfreezeReason = "";
                        string _cancelfreezeUserId = "";
                        DateTime _cancelfreezeDateTime = DateTime.Now;
                        if (oid != null)
                        {
                            AuditCollection a = cp.AuditGetList(oid.ToString(), true);
                            if (a != null)
                            {
                                var f = a.Where(x => x.ReferenceType.Equals((int)AuditType.DealAccounting)).Where(x => x.Message.Contains("系統凍結請款")).FirstOrDefault();
                                if (f != null)
                                {
                                    _freezeReason = f.Message;
                                    _freezeUserId = f.CreateId;
                                    _freezeDateTime = f.CreateTime;
                                }

                                var c = a.Where(x => x.ReferenceType.Equals((int)AuditType.DealAccounting)).Where(x => x.Message.Contains("系統取消凍結請款")).FirstOrDefault();
                                if (c != null)
                                {
                                    _cancelfreezeReason = c.Message;
                                    _cancelfreezeUserId = c.CreateId;
                                    _cancelfreezeDateTime = c.CreateTime;
                                }
                            }
                        }


                        content = createFreezeDealMailContent(freetype, vdps, freezeReason, oid, _freezeReason, _freezeUserId, _freezeDateTime, _cancelfreezeReason, _cancelfreezeUserId, _cancelfreezeDateTime);

                        SendFreezeMails(subject, content, mails);
                    }
                    break;
                case FreezeType.FreezeOrder:
                case FreezeType.CancelFreezeOrder:
                    ViewPponOrder order = pp.ViewPponOrderGet(oid);
                    ViewDealPropertyBusinessHourContent dealPropertyBusiness = pp.ViewDealPropertyBusinessHourContentGetByBid(order.BusinessHourGuid);
                    CashTrustLog ctlog = mp.CashTrustLogGetByOrderId(oid);
                    if (ctlog != null)
                    {
                        if (RegExRules.CheckEmail(dealPropertyBusiness.Email))
                        {
                            mails.Add(dealPropertyBusiness.Email);//業務email

                            //取得業務的主管
                            ViewEmployee emp = humanProv.ViewEmployeeGet("email", dealPropertyBusiness.Email);
                            var emps = humanProv.EmployeeCollectionGetByFilter("dept_id", emp.DeptId).Where(x => x.DeptManager != null);
                            if (emps != null)
                            {
                                IEnumerable<Employee> manager = emps.Where(x => x.DeptManager.Contains(emp.DeptId));
                                foreach (Employee m in manager)
                                {
                                    var _manager = humanProv.ViewEmployeeGet("emp_id", m.EmpId);
                                    if (RegExRules.CheckEmail(_manager.Email))
                                    {
                                        mails.Add(_manager.Email);//業務的主管email        
                                    }
                                }
                            }
                        }

                        string _freezeReason = "";
                        string _freezeUserId = "";
                        DateTime _freezeDateTime = DateTime.Now;
                        string _cancelfreezeReason = "";
                        string _cancelfreezeUserId = "";
                        DateTime _cancelfreezeDateTime = DateTime.Now;
                        if (oid != null)
                        {
                            AuditCollection a = cp.AuditGetList(oid.ToString(), true);
                            if (a != null)
                            {
                                var f = a.Where(x => x.ReferenceType.Equals((int)AuditType.Freeze)).Where(x => x.Message.Contains("系統凍結請款")).FirstOrDefault();
                                if (f != null)
                                {
                                    _freezeReason = f.Message;
                                    _freezeUserId = f.CreateId;
                                    _freezeDateTime = f.CreateTime;
                                }

                                var c = a.Where(x => x.ReferenceType.Equals((int)AuditType.Freeze)).Where(x => x.Message.Contains("系統取消凍結請款")).FirstOrDefault();
                                if (c != null)
                                {
                                    _cancelfreezeReason = c.Message;
                                    _cancelfreezeUserId = c.CreateId;
                                    _cancelfreezeDateTime = c.CreateTime;
                                }
                            }
                        }
                        content = string.Format(createFreezeOrderMailContent(freetype, order.OrderGuid), order.OrderId, _freezeDateTime, _freezeReason, _freezeUserId, _cancelfreezeDateTime, _cancelfreezeUserId);
                        if (content != "")
                        {
                            SendFreezeMails(subject, content, mails);
                        }

                    }
                    break;
                default:
                    break;
            }
        }
        private static void SendFreezeMails(string subject, string content, List<string> mails)
        {
            MailMessage msg = new MailMessage();

            msg.Subject = subject;
            msg.To.Clear();
            msg.Body = content;
            foreach (string mail in mails)
            {
                msg.To.Add(mail);
            }
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }
        private static string createFreezeMailSuject(FreezeType freetype)
        {
            string _subject = "";
            switch (freetype)
            {
                case FreezeType.FreezeAccounting:
                    //整檔訂單凍結，信件內容
                    _subject = "凍結請款通知";
                    break;
                case FreezeType.FreezeOrder:
                    //單一訂單凍結，信件內容
                    _subject = "凍結請款通知";
                    break;
                case FreezeType.CancelFreezeAccounting:
                    //整檔訂單取消凍結，信件內容
                    _subject = "取消凍結請款通知";
                    break;
                case FreezeType.CancelFreezeOrder:
                    //單一訂單取消凍結，信件內容
                    _subject = "取消凍結請款通知";
                    break;
                default:
                    _subject = "";
                    break;
            }
            return _subject;
        }
        private static string createFreezeOrderMailContent(FreezeType freetype, Guid? oid)
        {
            string _content = "";
            switch (freetype)
            {
                case FreezeType.FreezeOrder:
                    //單一訂單凍結，信件內容
                    _content = @"親愛的業務同仁，您好：<br/><br/>以下訂單因某異動原因而由財務/客服部判定是為無法請款之訂單：<br/><br/>" +
                                "<u>訂單資訊</u><br/><br/>" +
                                "凍結請款訂單編號：{0}<br/>" +
                                "凍結請款日期：{1}<br/>" +
                                "凍結請款原因：{2}<br/>" +
                                "建立者：{3}<br/><br/>" +
                                "請您收到此封信息後，協助廠商瞭解與處理，並回覆相關部門做確認，以利取消凍結請款，恢復該筆訂單請款資格。如有任何問題，請您向相關的部門與同仁取得聯繫。<br/><br/>" +
                                "謝謝，辛苦了。<br/>";
                    break;
                case FreezeType.CancelFreezeOrder:
                    //單一訂單取消凍結，信件內容
                    _content = @"親愛的業務同仁，您好：<br/><br/>十分感謝您的協助與處理，以下訂單已恢復請款資格，並列入可請款範圍。<br/><br/>" +
                                "<u>訂單資訊</u><br/><br/>" +
                                "凍結請款訂單編號：{0}<br/>" +
                                "凍結請款日期：{1}<br/>" +
                                "凍結請款原因：{2}<br/>" +
                                "建立者：{3}<br/><br/><br/>" +
                                "取消凍結日期：{4}<br/>" +
                                "建立者：{5}<br/>";
                    break;
                default:
                    _content = "";
                    break;
            }
            return _content;
        }
        private static string createFreezeDealMailContent(FreezeType freetype, ViewDealPropertyBusinessHourContentCollection vdps,
                                                            string reason, Guid bid, string _freezeReason, string _freezeUserId, DateTime _freezeDateTime,
                                                            string _cancelfreezeReason, string _cancelfreezeUserId, DateTime _cancelfreezeDateTime)
        {
            string _content = "";
            switch (freetype)
            {
                case FreezeType.FreezeAccounting:
                    //整檔訂單凍結，信件內容
                    _content = @"親愛的業務同仁，您好：<br/><br/>該檔所有訂單因某異動原因而由財務/客服部判定是為無法請款之訂單群：<br/><br/>" +
                                "<u>訂單資訊</u><br/><br/>";
                    foreach (ViewDealPropertyBusinessHourContent vdp in vdps)
                    {
                        _content += "凍結請款檔號：" + vdp.UniqueId + "<br/>" +
                                    "凍結請款日期：" + _freezeDateTime + "<br/>" +
                                    "凍結請款原因：" + _freezeReason + "<br/>" +
                                    "建立者：" + _freezeUserId + "<br/><br/>";
                    }

                    _content += "請您收到此封信息後，協助廠商瞭解與處理，並回覆相關部門做確認，以利取消凍結請款，恢復該檔訂單請款資格。如有任何問題，請您向相關的部門與同仁取得聯繫。<br/><br/>" +
                                "謝謝，辛苦了。<br/>";
                    break;
                case FreezeType.CancelFreezeAccounting:
                    //整檔訂單取消凍結，信件內容
                    _content = @"親愛的業務同仁，您好：<br/><br/>十分感謝您的協助與處理，目前以下該檔所有訂單皆已恢復請款資格，並列入可請款範圍。<br/><br/>" +
                                "<u>訂單資訊</u><br/><br/>";

                    foreach (ViewDealPropertyBusinessHourContent vdp in vdps)
                    {
                        _content += "凍結請款檔號：" + vdp.UniqueId + "<br/>" +
                                    "凍結請款日期：" + _freezeDateTime.ToString() + "<br/>" +
                                    "凍結請款原因：" + _freezeReason + "<br/>" +
                                    "建立者：" + _freezeUserId + "<br/><br/>" +
                                    "取消凍結日期：" + _cancelfreezeDateTime.ToString() + "<br/>" +
                                    "建立者：" + _cancelfreezeUserId + "<br/>";
                    }
                    break;
                default:
                    _content = "";
                    break;
            }
            return _content;
        }
        #endregion

        #region 分期訂單退貨異常處理
        public static void SendRefundTypeChangeMail(string orderId)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(config.SystemEmail);
            msg.To.Add(config.LionTravelServiceEmail);
            msg.Subject = "回收退貨異常通知";
            msg.Body = string.Format("退貨訂單編號:{0}，回收件數與退貨單上份數不一致，系統調整此筆分期訂單由[刷退]改[退購物金]，請與消費者再做確認。", orderId);
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }
        #endregion

        #region sys notify
        public static void SendApiWarning(string path)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(config.SystemEmail);
            msg.To.Add(config.ItPAD);
            msg.Subject = "API Call Failed";
            msg.Body = string.Format("API call failed , check:{0}", path);
            msg.IsBodyHtml = false;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }
        #endregion

        #region 後臺檔次設定-頁面確認信 (發送文字稿(COD))
        public static bool SendSetupPageConfirmMail(string senderMail, string subject, string mailContent,string deDeptId,string opDeptId)
        {
            MailMessage msg = new MailMessage()
            {
                From = new MailAddress(senderMail),
                Subject = FormatMailSubject(subject),
                Body = mailContent,
                IsBodyHtml = true
            };

            
            //開發業務
            string sales = ProposalFacade.PponAssistantEmailGet(deDeptId);
            if (!msg.To.Contains(new MailAddress(sales)))
                msg.To.Add(sales);

            //經營業務
            sales = ProposalFacade.PponAssistantEmailGet(opDeptId);
            if (!msg.To.Contains(new MailAddress(sales)))
                msg.To.Add(sales);

            if (deDeptId != EmployeeChildDept.S010.ToString() && humanProv.DepartmentGet(deDeptId).ParentDeptId != EmployeeDept.M000.ToString())
            {
                //cod
                msg.To.Add(new MailAddress(config.CodLocalEmail));
            }

            msg.CC.Add(new MailAddress(config.SupportingLeaderEmail));//創編組長信箱
            return PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }

        /// <summary>
        /// 頁面確認信&文字稿信
        /// </summary>
        /// <param name="senderMail"></param>
        /// <param name="subject"></param>
        /// <param name="mailContent"></param>
        /// <param name="vps"></param>
        /// <param name="isToShop"></param>
        /// <param name="deSalesEmp"></param>
        /// <param name="opSalesEmp"></param>
        /// <returns></returns>
        public static bool SendSetupPageMail(string senderMail, string subject, string mailContent, ViewProposalSeller vps, bool isToShop, ViewEmployee deSalesEmp, ViewEmployee opSalesEmp)
        {
            MailMessage msg = new MailMessage()
            {
                From = new MailAddress(senderMail),
                Subject = FormatMailSubject(subject),
                Body = mailContent,
                IsBodyHtml = true
            };

            #region #region 抓取商家聯絡人mail
            List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(vps.Contacts);
            List<string> mails = new List<string>();
            string userEmail = string.Empty;
            if (contacts != null)
            {
                foreach (var c in contacts)
                {
                    if (c.Type == ((int)ProposalContact.Normal).ToString())
                    {
                        if (!string.IsNullOrEmpty(c.ContactPersonEmail))
                        {
                            if (!mails.Contains(c.ContactPersonEmail))
                            {
                                mails.Add(c.ContactPersonEmail);
                            }
                        }
                    }
                }
            } 
            #endregion

            userEmail = string.Join(",", mails);

            var mailTo = RegExRules.CheckMultiEmail(userEmail);
            if (string.IsNullOrEmpty(mailTo))
            {
                string errorMsg = string.Format("商家:{0}<a href='{1}'>{2}</a> 聯絡人mail有誤，無法發送頁確信件。聯絡人Email: \"{3}\"", vps.SellerId, config.SiteUrl + "/sal/SellerContent.aspx?sid=" + vps.SellerGuid, vps.SellerName, userEmail);
                try
                {
                    SendErrorEmail(vps.SellerGuid, errorMsg);
                }
                catch { }
                log.Warn(errorMsg);
                return false;
            }
            else
            {
                string[] mailList = mailTo.Split(",");
                foreach (string m in mailList)
                {
                    msg.To.Add(m);
                }

                //新增業務
                msg.CC.Add(deSalesEmp.Email);
                if (opSalesEmp.IsLoaded)
                {
                    msg.CC.Add(opSalesEmp.Email);
                }

                //開發業務
                string sales = ProposalFacade.PponAssistantEmailGet(deSalesEmp.DeptId);
                if (!msg.CC.Contains(new MailAddress(sales)))
                    msg.CC.Add(sales);

                //經營業務
                sales = ProposalFacade.PponAssistantEmailGet(opSalesEmp.DeptId);
                if (!msg.CC.Contains(new MailAddress(sales)))
                    msg.CC.Add(sales);


                //憑證檔次需要附加資料附件
                if (isToShop)
                {
                    //操作說明手冊-核銷
                    string operationPath = System.Web.HttpContext.Current.Server.MapPath(config.OperationPath);
                    msg.Attachments.Add(new Attachment(operationPath));
                    //操作說明手冊-對帳
                    string ReconciliationPath = System.Web.HttpContext.Current.Server.MapPath(config.ReconciliationPath);
                    msg.Attachments.Add(new Attachment(ReconciliationPath));
                    //憑證核銷紀錄表
                    string writeOffPath = System.Web.HttpContext.Current.Server.MapPath(config.WriteOffPath);
                    msg.Attachments.Add(new Attachment(writeOffPath));
                }

                return PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }            
        }

        

        #endregion

        #region 行銷活動相關信件 (有時效性，活動結束function即可刪除)

        #endregion

        #region 客服與廠商信件往來通知

        /// <summary>
        /// [通知廠商] 寄送轉單訊息給廠商 (即時)
        /// </summary>
        /// <param name="insideLogId">轉單編號</param>
        /// <returns></returns>
        public static void SendTransferMailToSeller(int insideLogId)
        {
            if (!config.EnableTransferMailNotify)
            {
                return;
            }

            //轉單資訊
            var insideLog = csp.CustomerServiceInsideLogGet(insideLogId);
            if (insideLog == null || insideLog.Status != (int)statusConvert.transfer)
            {
                return;
            }

            //客服案件資訊
            var vcsl = csp.GetViewCustomerServiceMessageByServiceNo(insideLog.ServiceNo);
            if (!vcsl.IsLoaded)
            {
                log.Error("[寄送轉單訊息給廠商] 查無客服案件資訊 insideLogId: " + insideLogId);
                return;
            }

            var contactType = vcsl.DeliveryType == (int)DeliveryType.ToHouse ? ProposalContact.ReturnPerson : ProposalContact.Normal;
            var checkResult = CustomerServiceFacade.GetSellerContacts(vcsl.SellerGuid, contactType);
            if (checkResult.Key)
            {
                var mailTo = checkResult.Value;

                #region Send mail

                CustomerServiceTransferMailToSeller template = TemplateFactory.Instance().GetTemplate<CustomerServiceTransferMailToSeller>();
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = "/Ppon/NewbieGuide.aspx";

                template.ServiceNo = insideLog.ServiceNo;
                template.ServiceNoUrl = string.Format("{0}/vbs/ServiceProcessDetail?serviceNo={1}", config.SSLSiteUrl, insideLog.ServiceNo);
                template.OrderId = vcsl.OrderId;
                template.ItemName = vcsl.ItemName;
                template.ProblemDescription = vcsl.MainCategoryName;
                template.SubProblemDescription = vcsl.SubCategoryName;
                template.Content = insideLog.Content.Replace("\r\n", "<br>");

                //問題等級
                var problemLevelDesc = Helper.GetEnumDescription((priorityConvert)vcsl.CasePriority);
                var problemLevelOnSubject = vcsl.CasePriority == (int)priorityConvert.normal ? string.Empty : "[" + problemLevelDesc + "] ";
                template.PriorityDescription = problemLevelDesc;

                //二線客服 
                var serviceName = vcsl.SellerGuid == null
                    ? string.Empty
                    : CustomerServiceFacade.GetSellerMappingEmpName((Guid)vcsl.SellerGuid);
                template.ServiceName = string.IsNullOrEmpty(serviceName) ? "客服專員" : serviceName;

                //廠商名稱
                template.SellerName = string.IsNullOrEmpty(vcsl.SellerName) ? "廠商" : vcsl.SellerName;

                try
                {
                    string[] mailList = mailTo.Split(",");
                    foreach (string m in mailList)
                    {
                        MailMessage msg = new MailMessage();
                        msg.Subject = string.Format("{0}17Life【{1}】客服案件新增通知", problemLevelOnSubject, template.SellerName);
                        msg.From = new MailAddress(config.CsTransferEmailAddress, config.CsTransferEmailDisplayName);
                        msg.To.Add(m);
                        msg.Body = template.ToString();
                        msg.IsBodyHtml = true;
                        PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    }
                    


                }
                catch (Exception ex)
                {
                    insideLog.Memo = "寄送失敗!";
                    log.Error(ex.Message);
                    return;
                }
                insideLog.Memo = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " 成功寄送通知";

                #endregion
            }
            else
            {
                insideLog.Memo = checkResult.Value;
                SendErrorEmail((Guid)vcsl.SellerGuid, checkResult.Value);
                log.Error(checkResult.Value);
            }
            csp.CustomerServiceInsideLogSet(insideLog);
        }

        /// <summary>
        /// [通知客服] 寄送轉單回覆訊息給客服 (即時)
        /// </summary>
        /// <param name="insideLogId"></param>
        public static void SendTransferMailToCs(int insideLogId)
        {
            if (!config.EnableTransferMailNotify)
            {
                return;
            }

            //轉單資訊
            var insideLog = csp.CustomerServiceInsideLogGet(insideLogId);
            if (insideLog == null || insideLog.Status != (int)statusConvert.transferback)
            {
                return;
            }

            //客服案件資訊
            var vcsl = csp.GetViewCustomerServiceMessageByServiceNo(insideLog.ServiceNo);
            if (!vcsl.IsLoaded)
            {
                log.Error("[寄送轉單回覆訊息給客服] 查無客服案件資訊 insideLogId: " + insideLogId);
                return;
            }

            //收件客服
            var mailTo = CustomerServiceFacade.GetCsContacts(vcsl);
            if (string.IsNullOrEmpty(mailTo))
            {
                return;
            }

            #region Send mail

            CustomerServiceTransferMailToCs template = TemplateFactory.Instance().GetTemplate<CustomerServiceTransferMailToCs>();
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = "/Ppon/NewbieGuide.aspx";

            template.ServiceNo = insideLog.ServiceNo;
            template.ServiceNoUrl = string.Format("{0}/controlroom/Service/ServiceDetail.aspx?serviceNo={1}", config.SSLSiteUrl, insideLog.ServiceNo);
            template.OrderId = vcsl.OrderId;
            template.ItemName = vcsl.ItemName;
            template.ProblemDescription = vcsl.MainCategoryName;
            template.SubProblemDescription = vcsl.SubCategoryName;
            template.Content = insideLog.Content.Replace("\r\n", "<br>");

            //問題等級
            var problemLevelDesc = Helper.GetEnumDescription((priorityConvert)vcsl.CasePriority);
            var problemLevelOnSubject = vcsl.CasePriority == (int)priorityConvert.normal ? string.Empty : "[" + problemLevelDesc + "] ";
            template.PriorityDescription = problemLevelDesc;

            //二線客服 
            var serviceName = vcsl.SellerGuid == null
                ? string.Empty
                : CustomerServiceFacade.GetSellerMappingEmpName((Guid)vcsl.SellerGuid);
            template.ServiceName = string.IsNullOrEmpty(serviceName) ? "客服專員" : serviceName;

            //廠商名稱
            template.SellerName = string.IsNullOrEmpty(vcsl.SellerName) ? "廠商" : vcsl.SellerName;

            try
            {
                MailMessage msg = new MailMessage();
                msg.Subject = string.Format("{0}17Life【{1}】客服案件回覆通知", problemLevelOnSubject, template.SellerName);
                msg.From = new MailAddress(config.CsTransferEmailAddress, config.CsTransferEmailDisplayName);
                msg.To.Add(mailTo);
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return;
            }

            insideLog.Memo = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " 成功寄送通知";
            csp.CustomerServiceInsideLogSet(insideLog);

            #endregion

        }

        /// <summary>
        /// [通知客服] 轉單後24小時，商家已讀未回者，通知客服聯繫商家 (每00分、30分 批次) CustomerServiceTransferNoReplyJob
        /// </summary>
        public static int SendTransferNoReplyToCs(DateTime startTime, DateTime endTime)
        {
            int sendCount = 0;

            //已讀未回轉單
            var serviceNoList = csp.GetCustomerServiceInsideLogServiceNoListBy24HNoReply(startTime, endTime);
            foreach (var serviceNo in serviceNoList)
            {
                //案件資訊
                ViewCustomerServiceList vcsl = csp.GetViewCustomerServiceMessageByServiceNo(serviceNo);
                if (!vcsl.IsLoaded)
                {
                    return 0;
                }

                //轉單訊息
                CustomerServiceInsideLogCollection insideLogCol = csp.CustomerServiceInsideLogGet(serviceNo);
                if (!insideLogCol.Any())
                {
                    return 0;
                }

                #region Send mail

                CustomerServiceTransferNoReplyToCs template = TemplateFactory.Instance().GetTemplate<CustomerServiceTransferNoReplyToCs>();
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = "/Ppon/NewbieGuide.aspx";

                //上方案件資訊
                template.ServiceNo = serviceNo;
                template.ServiceNoUrl = string.Format("{0}/controlroom/Service/ServiceDetail.aspx?serviceNo={1}", config.SSLSiteUrl, serviceNo);
                template.OrderId = vcsl.OrderId;
                template.ItemName = vcsl.ItemName;
                template.ProblemDescription = vcsl.MainCategoryName;
                template.SubProblemDescription = vcsl.SubCategoryName;
                template.SellerName = string.IsNullOrEmpty(vcsl.SellerName) ? "廠商" : vcsl.SellerName;
                template.PriorityDescription = Helper.GetEnumDescription((priorityConvert)vcsl.CasePriority);

                //下方案件轉單記錄
                var insideLogs = new List<CustomerServiceTransferNoReplyToCs.InsideLog>();
                foreach (var insideLog in insideLogCol.OrderBy(x => x.CreateTime))
                {
                    insideLogs.Add(new CustomerServiceTransferNoReplyToCs.InsideLog
                    {
                        CreateName = MemberFacade.GetFullName(insideLog.CreateId),
                        CreateTime = insideLog.CreateTime.ToString("yyyy-MM-dd HH:mm"),
                        Content = insideLog.Content,
                        StatusDesc = Helper.GetEnumDescription((statusConvert)insideLog.Status)
                    });
                }
                template.InsideLogs = insideLogs;

                //二線客服 
                var lineTwoServiceName = "客服專員";
                var lineTwoServiceMail = string.Empty;
                if (vcsl.SellerGuid != null)
                {
                    var sme = humanProv.SellerMappingEmployeeGet((Guid)vcsl.SellerGuid);
                    if (sme.IsLoaded)
                    {
                        var emp = humanProv.ViewEmployeeGet(ViewEmployee.Columns.EmpId, sme.EmpId);
                        if (emp.IsLoaded)
                        {
                            lineTwoServiceName = emp.EmpName;
                            lineTwoServiceMail = MemberFacade.GetUserName(emp.UserId);
                        }
                    }
                }
                template.ServiceName = lineTwoServiceName;

                var mailCol = new MailAddressCollection();

                //收件客服 (客服主管、案件認領人)
                var mailTo = CustomerServiceFacade.GetCsContacts(vcsl);
                if (!string.IsNullOrEmpty(mailTo))
                {
                    mailCol.Add(mailTo);
                }
                //二線客服
                if (!string.IsNullOrEmpty(lineTwoServiceMail))
                {
                    mailCol.Add(lineTwoServiceMail);
                }
                if (!mailCol.Any())
                {
                    return 0;
                }

                try
                {
                    MailMessage msg = new MailMessage();
                    msg.Subject = string.Format("17Life【{0}】已讀未回超過24小時案件通知", template.SellerName);
                    msg.From = new MailAddress(config.CsTransferEmailAddress, config.CsTransferEmailDisplayName);
                    msg.To.Add(mailCol.ToString());
                    msg.Body = template.ToString();
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);

                    sendCount++;
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                    return 0;
                }

                #endregion
            }
            return sendCount;
        }

        public static void SendNotifyToCs(string serviceNo, statusConvert status)
        {
            ViewCustomerServiceList vcsl = csp.GetViewCustomerServiceMessageByServiceNo(serviceNo);
            if (!vcsl.IsLoaded)
            {
                return;
            }

            var mailUserId = 0;
            switch(status)
            {
                case statusConvert.ServiceNotify:
                    mailUserId = vcsl.ServicePeopleId ?? 0;
                    break;
                case statusConvert.secServiceNotify:
                    mailUserId = vcsl.SecServicePeopleId ?? 0;
                    break;
                default:
                    break;
            }

            if (mailUserId <= 0)
            {
                return;
            }

            var mem = mp.MemberGetbyUniqueId(mailUserId);

            try
            {
                MailMessage msg = new MailMessage();
                msg.Subject = string.Format("邀請您一同處理案件：{0}", serviceNo);
                msg.From = new MailAddress(config.CsTransferEmailAddress, config.CsTransferEmailDisplayName);
                msg.To.Add(mem.UserName);
                msg.Body = string.Format(@"Dear {0} {1}，<br/><br/>
有一件客服案件需要請你協助處理，案件編號為 <a href='{2}/ControlRoom/Service/ServiceDetail.aspx?serviceNo={3}'>{3}</a>，<br/>請直接點擊上面的案件編號超連結，便可進入案件頁。<br/><br/>本信件為系統自動發送與統計，請勿直接回覆，若您有任何問題請與您的服務專員連繫，謝謝。", mem.LastName, mem.FirstName, config.SiteUrl, serviceNo);
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return;
            }
        }

        /// <summary>
        /// [通知客服] 處理中案件再次詢問通知 (即時)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="serviceNo"></param>
        public static void SendNewCustomerMessageToCs(int userId, string serviceNo)
        {
            SendNewCustomerMessageToCs(userId.ToString(), serviceNo);
        }

        public static void SendNewCustomerMessageToCs(string userId, string serviceNo)
        {
            //案件資訊
            ViewCustomerServiceList vcsl = csp.GetViewCustomerServiceMessageByServiceNo(serviceNo);
            if (!vcsl.IsLoaded)
            {
                return;
            }

            //對話訊息
            ViewCustomerServiceOutsideLogCollection viewOutsideLogCol = csp.GetCustomerServiceOutsideLogByuserIdAndSerivceNo(userId, serviceNo);
            if (!viewOutsideLogCol.Any())
            {
                return;
            }

            //收件客服
            var mailTo = CustomerServiceFacade.GetCsContacts(vcsl);
            if (string.IsNullOrEmpty(mailTo))
            {
                return;
            }

            #region Send mail

            NewCustomerMessageToCs template = TemplateFactory.Instance().GetTemplate<NewCustomerMessageToCs>();
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = "/Ppon/NewbieGuide.aspx";

            template.ServiceNo = serviceNo;
            template.ServiceNoUrl = string.Format("{0}/controlroom/Service/ServiceDetail.aspx?serviceNo={1}", config.SSLSiteUrl, serviceNo);

            //對話訊息
            var outsideLogs = new List<NewCustomerMessageToCs.OutsideLog>();
            foreach (var viewOutsideLog in viewOutsideLogCol.OrderBy(x => x.CreateTime))
            {
                outsideLogs.Add(new NewCustomerMessageToCs.OutsideLog
                {
                    CustomerName = viewOutsideLog.Name,
                    CreateName = viewOutsideLog.CreateName,
                    CreateTime = viewOutsideLog.CreateTime.ToString("yyyy-MM-dd HH:mm"),
                    Content = viewOutsideLog.Content,
                    Method = viewOutsideLog.Method
                });
            }
            template.OutsideLogs = outsideLogs;

            //二線客服 
            var serviceName = vcsl.SellerGuid == null
                ? string.Empty
                : CustomerServiceFacade.GetSellerMappingEmpName((Guid)vcsl.SellerGuid);
            template.ServiceName = string.IsNullOrEmpty(serviceName) ? "客服專員" : serviceName;

            try
            {
                MailMessage msg = new MailMessage();
                msg.Subject = string.Format("17Life【{0}】消費者再次詢問案件通知", serviceNo);
                msg.From = new MailAddress(config.CsTransferEmailAddress, config.CsTransferEmailDisplayName);
                msg.To.Add(mailTo);
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return;
            }

            #endregion
        }


        /// <summary>
        /// [通知廠商] 有新訂單準備出貨 (批次) in DealNotify Job
        /// </summary>
        /// <param name="days">最近n日訂單</param>
        public static void SendNewShipOrderToSeller(double days = 1)
        {
            if (!config.EnableNewOrderShipMailNotify)
            {
                return;
            }

            //所有宅配新訂單(未出貨不含24H)
            var newShipOrderList = op.ViewNewShipOrderToSellerCollectionGet(days).Where(x => !x.IsWms).ToList();
            LoadShipOrderData(newShipOrderList);
            //所有宅配新訂單(24H)
            var newShipWmsOrderList = op.ViewNewShipOrderToSellerCollectionGet(days).Where(x => x.IsWms).ToList();
            LoadShipOrderData(newShipWmsOrderList, true);
        }

        private static void LoadShipOrderData(List<ViewNewShipOrderToSeller> newShipOrderList, bool isWms = false)
        {
            var sellerGroup = newShipOrderList.Select(x => x.SellerGuid).Distinct().ToList();
            var sellerOrdersDic = sellerGroup.ToDictionary(
                sellerGuid => sellerGuid,
                sellerGuid => newShipOrderList.Where(s => s.SellerGuid == sellerGuid).ToList());

            foreach (var sellerGuid in sellerGroup)
            {
                //確認商家收件人
                var checkContacts = CustomerServiceFacade.GetSellerContacts(sellerGuid, ProposalContact.ReturnPerson);
                if (checkContacts.Key)
                {
                    var mailTo = checkContacts.Value;

                    #region 商家待出清單

                    var newOrders = new List<NewShipOrderToSellerMail.NewOrderShip>();
                    foreach (var shipOrder in sellerOrdersDic[sellerGuid])
                    {
                        string shipTypeDesc = string.Empty;
                        string lastShipDate = string.Empty;

                        var input = new ShippingDateInputModel
                        {
                            ShipType = shipOrder.ShipType,
                            ShippingdateType = shipOrder.ShippingdateType,
                            AfterOrderedDays = shipOrder.Shippingdate,
                            AfterDealStartDays = shipOrder.ProductUseDateStartSet,
                            AfterDealEndDays = shipOrder.ProductUseDateEndSet,
                            OrderCreateTime = shipOrder.CreateTime,
                            BusinessHourOrderTimeE = shipOrder.BusinessHourOrderTimeE,
                            BusinessHourDeliverTimeE = shipOrder.BusinessHourDeliverTimeE
                        };
                        var shipTypeModel = OrderFacade.GetShippingDateInfo(input);

                        if (shipTypeModel != null)
                        {
                            var ld = shipTypeModel.LastShipDate;
                            shipTypeDesc = shipTypeModel.ShipTypeDesc;
                            lastShipDate = ld == null ? string.Empty : ((DateTime)ld).ToString("yyyy/MM/dd");
                        }

                        newOrders.Add(
                            new NewShipOrderToSellerMail.NewOrderShip
                            {
                                OrderId = shipOrder.OrderId,
                                ShipTypeDesc = shipTypeDesc,
                                LastShipDate = lastShipDate,
                                ItemName = shipOrder.ItemName,
                                Count = shipOrder.TotalCount ?? 0,
                                ReceiveName = shipOrder.MemberName
                            });
                    }

                    #endregion

                    #region Send mail

                    if (newOrders.Any())
                    {
                        var template = TemplateFactory.Instance().GetTemplate<NewShipOrderToSellerMail>();
                        template.HttpsSiteUrl = config.SSLSiteUrl;
                        template.SiteUrl = config.SiteUrl;
                        template.SiteServiceUrl = "/Ppon/NewbieGuide.aspx";
                        template.VbsShipUrl = "/vbs/ship/ShipDealList";
                        template.SellerName = sellerOrdersDic[sellerGuid].First().SellerName;
                        template.OrderShipList = newOrders.OrderBy(x=>x.LastShipDate).ThenBy(x=>x.ShipTypeDesc).ToList();
                        template.IsWms = isWms;
                        try
                        {
                            string[] mailList = mailTo.Split(",");
                            foreach (string m in mailList)
                            {
                                MailMessage msg = new MailMessage();
                                msg.Subject = string.Format("17Life【{0}】新增出貨訂單通知({1})", template.SellerName, isWms ? "24H到貨" : "一般宅配");
                                msg.From = new MailAddress(config.NewOrderShipEmailAddress, config.NewOrderShipEmailDisplayName);
                                msg.To.Add(m);
                                msg.Body = template.ToString();
                                msg.IsBodyHtml = true;
                                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                            }
                            
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.Message);
                            return;
                        }
                    }

                    #endregion
                }
                else
                {
                    SendErrorEmail(sellerGuid, checkContacts.Value);
                    log.Error(checkContacts.Value);
                }
            }
        }

        /// <summary>
        /// [通知廠商] 有退/換貨商品 (宅配Only) (即時)
        /// </summary>
        public static void SendProductRefundOrExchangeToSeller(OrderReturnType type, int returnOrExchangeId)
        {
            if (!config.EnableNewReturnOrExchangeMailNotify)
            {
                return;
            }

            var returnInfo = new NewReturnOrExchangeToSellerMail.ProductRefundOrExchangeModel();
            returnInfo.IsExchange = type == OrderReturnType.Exchange;

            string mailSubject = string.Empty;

            #region 退貨
            if (type == OrderReturnType.Refund)
            {
                mailSubject = "<重要退貨>17Life【{0}】消費者退貨訂單通知<{1}>";
                var vbsOrderReturn = op.ViewOrderReturnFormListGet(returnOrExchangeId);
                if (!vbsOrderReturn.IsLoaded)
                {
                    return;
                }

                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vbsOrderReturn.ProductGuid);
                if (vpd == null)
                {
                    return;
                }

                if (vpd.DeliveryType != (int)DeliveryType.ToHouse)
                {
                    return; //非宅配不寄 
                }


                if (vpd.IsWms && config.WmsRefundEnable && (string.IsNullOrEmpty(config.WmsRefundAccount) || (!string.IsNullOrEmpty(config.WmsRefundAccount) && vbsOrderReturn.OrderCreateId == config.WmsRefundAccount)))
                {
                    return; //排除24H
                }

                Dictionary<int, string> orderItemLists = op.GetReturnOrderProductOptionDesc(new List<int> { returnOrExchangeId });
                var specAndQuanty = orderItemLists.ContainsKey(returnOrExchangeId) ? orderItemLists[vbsOrderReturn.ReturnFormId] : string.Empty;

                returnInfo.CreateTime = vbsOrderReturn.ReturnApplicationTime.ToString("yyyy/MM/dd");
                returnInfo.OrderId = vbsOrderReturn.OrderId;
                returnInfo.DealUniqueId = vbsOrderReturn.ProductId;
                returnInfo.ReciverName = vbsOrderReturn.MemberName;
                returnInfo.ItemName = vbsOrderReturn.DealName;
                returnInfo.SpecAndQuanty = specAndQuanty;
                returnInfo.Reason = vbsOrderReturn.ReturnReason;
                returnInfo.SellerGuid = vpd.SellerGuid;
                returnInfo.SellerName = vbsOrderReturn.SellerName;
                returnInfo.VbsUrl = config.SSLSiteUrl + "/vbs/ship/ReturnOrderProcessList";
                returnInfo.VbsDealIdUrl = config.SSLSiteUrl + "/vbs/ship/ReturnOrderProcessList?dealId=" + vbsOrderReturn.ProductId;
                returnInfo.VbsOrderIdUrl = config.SSLSiteUrl + "/vbs/ship/ReturnOrderProcessList?orderId=" + vbsOrderReturn.OrderId;
                returnInfo.LastReturnDate = DateTime.Now.AddDays(7).ToString("yyyy/MM/dd");
            }
            #endregion
            #region 換貨
            else if (type == OrderReturnType.Exchange)
            {
                mailSubject = "<重要換貨>17Life【{0}】消費者換貨訂單通知<{1}>";
                var vbsExchange = op.ViewOrderReturnListGet(returnOrExchangeId);
                if (!vbsExchange.IsLoaded)
                {
                    return;
                }

                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vbsExchange.BusinessHourGuid);
                if (vpd == null)
                {
                    return;
                }

                if (vpd.DeliveryType != (int)DeliveryType.ToHouse)
                {
                    return; //非宅配不寄
                }

                returnInfo.CreateTime = vbsExchange.ReturnCreateTime.ToString("yyyy/MM/dd");
                returnInfo.OrderId = vbsExchange.OrderId;
                returnInfo.DealUniqueId = vbsExchange.UniqueId;
                returnInfo.ReciverName = vbsExchange.MemberName;
                returnInfo.ItemName = vbsExchange.ItemName;
                returnInfo.SpecAndQuanty = "";
                returnInfo.Reason = vbsExchange.Reason;
                returnInfo.SellerGuid = vbsExchange.SellerGuid;
                returnInfo.SellerName = vbsExchange.SellerName;
                returnInfo.VbsUrl = config.SSLSiteUrl + "/vbs/ship/ExchangeOrderProcessList";
                returnInfo.VbsDealIdUrl = config.SSLSiteUrl + "/vbs/ship/ExchangeOrderProcessList?dealId=" + vbsExchange.UniqueId;
                returnInfo.VbsOrderIdUrl = config.SSLSiteUrl + "/vbs/ship/ExchangeOrderProcessList?orderId=" + vbsExchange.OrderId;
            }
            #endregion

            //確認商家收件人
            var checkContacts = CustomerServiceFacade.GetSellerContacts(returnInfo.SellerGuid, ProposalContact.ReturnPerson);
            if (checkContacts.Key)
            {
                var mailTo = checkContacts.Value;

                #region Send mail

                var template = TemplateFactory.Instance().GetTemplate<NewReturnOrExchangeToSellerMail>();
                template.HttpsSiteUrl = config.SSLSiteUrl;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = "/Ppon/NewbieGuide.aspx";
                template.ReturnInfo = new List<NewReturnOrExchangeToSellerMail.ProductRefundOrExchangeModel>() { returnInfo };

                try
                {
                    string[] mailList = mailTo.Split(",");
                    foreach (string m in mailList)
                    {
                        MailMessage msg = new MailMessage();
                        msg.Subject = string.Format(mailSubject, returnInfo.SellerName, DateTime.Now.ToString("yyyy/MM/dd"));
                        msg.From = new MailAddress(config.NewReturnOrExchangepEmailAddress, config.NewReturnOrExchangeEmailDisplayName);
                        msg.To.Add(m);
                        msg.Body = template.ToString();
                        msg.IsBodyHtml = true;
                        PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    }
                        
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                }

                #endregion
            }
            else
            {
                SendErrorEmail(returnInfo.SellerGuid, checkContacts.Value);
                log.Error(checkContacts.Value);
            }
        }

        /// <summary>
        /// [通知廠商] 24H逆物流已送達
        /// </summary>
        public static void SendWmsProductRefundToSeller(int returnFormId)
        {
            if (!config.EnableNewReturnOrExchangeMailNotify)
            {
                return;
            }

            var returnInfo = new NewReturnOrExchangeToSellerMail.ProductRefundOrExchangeModel();

            string mailSubject = string.Empty;

            #region 退貨
           
            mailSubject = "<重要退貨>17Life【{0}】24H到貨/ 退貨確認收件通知<{1}>";
            var vbsOrderReturn = op.ViewOrderReturnFormListGet(returnFormId);
            if (!vbsOrderReturn.IsLoaded)
            {
                return;
            }

            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vbsOrderReturn.ProductGuid);
            if (vpd == null)
            {
                return;
            }

            if (vpd.DeliveryType != (int)DeliveryType.ToHouse)
            {
                return; //非宅配不寄 
            }


            if (!(vpd.IsWms && config.WmsRefundEnable && (string.IsNullOrEmpty(config.WmsRefundAccount) || (!string.IsNullOrEmpty(config.WmsRefundAccount) && vbsOrderReturn.OrderCreateId == config.WmsRefundAccount))))
            {
                return;
            }


            Dictionary<int, string> orderItemLists = op.GetReturnOrderProductOptionDesc(new List<int> { returnFormId });
            var specAndQuanty = orderItemLists.ContainsKey(returnFormId) ? orderItemLists[vbsOrderReturn.ReturnFormId] : string.Empty;

            returnInfo.CreateTime = vbsOrderReturn.ReturnApplicationTime.ToString("yyyy/MM/dd");
            returnInfo.OrderId = vbsOrderReturn.OrderId;
            returnInfo.DealUniqueId = vbsOrderReturn.ProductId;
            returnInfo.ReciverName = vbsOrderReturn.MemberName;
            returnInfo.ItemName = vbsOrderReturn.DealName;
            returnInfo.SpecAndQuanty = specAndQuanty;
            returnInfo.Reason = vbsOrderReturn.ReturnReason;
            returnInfo.SellerGuid = vpd.SellerGuid;
            returnInfo.SellerName = vbsOrderReturn.SellerName;
            returnInfo.VbsUrl = config.SSLSiteUrl + "/vbs/ship/ReturnOrderProcessList";
            returnInfo.VbsDealIdUrl = config.SSLSiteUrl + "/vbs/ship/ReturnOrderProcessList?dealId=" + vbsOrderReturn.ProductId;
            returnInfo.VbsOrderIdUrl = config.SSLSiteUrl + "/vbs/ship/ReturnOrderProcessList?orderId=" + vbsOrderReturn.OrderId;
            //returnInfo.LastReturnDate = DateTime.Now.AddDays(7).ToString("yyyy/MM/dd");
            
            #endregion
            
            //確認商家收件人
            var checkContacts = CustomerServiceFacade.GetSellerContacts(returnInfo.SellerGuid, ProposalContact.ReturnPerson);
            if (checkContacts.Key)
            {
                var mailTo = checkContacts.Value;

                #region Send mail

                var wmsRefundOrder = wp.WmsRefundOrderGetByReturnFormId(returnFormId);
                List<WmsRefundTrace.LogisticTraceInfo> shipInfos = new JsonSerializer().Deserialize<WmsRefundTrace>(wmsRefundOrder.ShipInfo).Info;
                VacationCollection recentHolidays = pp.GetVacationListByPeriod(DateTime.Now, DateTime.Now.AddMonths(1));
                var template = TemplateFactory.Instance().GetTemplate<NewReturnOrExchangeToSellerWmsMail>();
                template.HttpsSiteUrl = config.SSLSiteUrl;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = "/Ppon/NewbieGuide.aspx";
                template.VbsUrl = config.SSLSiteUrl + "/vbs/ship/ReturnOrderProcessList";
                template.OrderId = vbsOrderReturn.OrderId;
                template.ReturnInfo = new List<NewReturnOrExchangeToSellerMail.ProductRefundOrExchangeModel>() { returnInfo };
                List<NewReturnOrExchangeToSellerWmsMail.ShipInfo> shipinfos = new List<NewReturnOrExchangeToSellerWmsMail.ShipInfo>();
                foreach (WmsRefundTrace.LogisticTraceInfo info in shipInfos)
                {
                    shipinfos.Add(new NewReturnOrExchangeToSellerWmsMail.ShipInfo
                    {
                        ShipId = info.ShipId,
                        LogisticName = info.LogisticName,
                        ShipCompanySite = op.ShipCompanyGetByName(info.LogisticName).ShipWebsite
                    });
                }
                template.ShipInfos = shipinfos;
                template.ShipReturnDate = wmsRefundOrder != null && wmsRefundOrder.ArrivalTime.HasValue ? wmsRefundOrder.ArrivalTime.Value.ToString("yyyy/MM/dd") : string.Empty;
                template.CheckDeadLine = wmsRefundOrder != null && wmsRefundOrder.ArrivalTime.HasValue ? OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, wmsRefundOrder.ArrivalTime.Value, wmsRefundOrder.ArrivalTime.Value.AddDays(3)).ToString("yyyy/MM/dd") : string.Empty;

                try
                {
                    string[] mailList = mailTo.Split(",");
                    foreach (string m in mailList)
                    {
                        MailMessage msg = new MailMessage();
                        msg.Subject = string.Format(mailSubject, returnInfo.SellerName, DateTime.Now.ToString("yyyy/MM/dd"));
                        msg.From = new MailAddress(config.NewReturnOrExchangepEmailAddress, config.NewReturnOrExchangeEmailDisplayName);
                        msg.To.Add(m);
                        msg.Body = template.ToString();
                        msg.IsBodyHtml = true;
                        PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    }
                        
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                }

                #endregion
            }
            else
            {
                SendErrorEmail(returnInfo.SellerGuid, checkContacts.Value);
                log.Error(checkContacts.Value);
            }
        }

        /// <summary>
        /// [通知廠商] 逾期/即將逾期出貨訂單 (批次) in DealNotify Job
        /// </summary>
        public static void SendExpiringOrdersToSeller(bool isExpired)
        {
            var viewExpiringOrders = isExpired
                    ? op.ViewOverdueOrderCollectionGet().Cast<IViewExpiringOrder>().ToList()
                    : op.ViewExpiringOrderCollectionGet().Cast<IViewExpiringOrder>().ToList();

            if (viewExpiringOrders.Any())
            {
                //By商家分群
                var sellerGroup = viewExpiringOrders.Select(x => x.SellerGuid).Distinct().ToList();
                var sellerOrderDic = sellerGroup.ToDictionary(
                    sellerGuid => sellerGuid,
                    sellerGuid => viewExpiringOrders.Where(s => s.SellerGuid == sellerGuid).OrderBy(x => x.LastShipDate).ThenBy(x => x.UniqueId).ToList());

                foreach (var sellerGuid in sellerGroup)
                {
                    //確認商家收件人
                    var checkContacts = CustomerServiceFacade.GetSellerContacts(sellerGuid, ProposalContact.ReturnPerson);
                    if (checkContacts.Key)
                    {
                        var mailTo = checkContacts.Value;

                        #region 逾期/即將逾期出貨訂單

                        var expiringOrders = new List<ExpiringOrderToSellerMail.ExpiringOrder>();
                        string sellerName = string.Empty;
                        int i = 0;
                        int totalFineAmount = 0;

                        foreach (var order in sellerOrderDic[sellerGuid].OrderBy(x => x.UniqueId))
                        {
                            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(order.BusinessHourGuid);
                            if (vpd == null)
                            {
                                log.Error("逾期/即將逾期出貨訂單，查無vpd, bid: " + order.BusinessHourGuid);
                                continue;
                            }
                            sellerName = vpd.SellerName;

                            var shippingDateInfo = OrderFacade.GetShippingDateInfo(DateTime.Now, vpd);

                            if (order.OrderList == null)
                            {
                                continue;
                            }

                            //每筆訂單一列資料
                            string[] orderGuidList = order.OrderList.TrimEnd(",").Split(",");
                            foreach (var orderGuid in orderGuidList)
                            {
                                Guid oGuid;
                                if (!Guid.TryParse(orderGuid, out oGuid))
                                {
                                    log.Error("逾期/即將逾期出貨訂單，Parse OrderList 失敗: " + orderGuid);
                                }
                                else
                                {
                                    string orderId = string.Empty;
                                    string deliveryTypeDesc = string.Empty;
                                    string vbsShipUrl = string.Empty;
                                    var vopd = op.ViewOrderProductDeliveryGetByOrderGuid(oGuid);
                                    if (vopd.IsLoaded)
                                    {
                                        orderId = vopd.OrderId;
                                        deliveryTypeDesc =
                                            Helper.GetEnumDescription((ProductDeliveryType)vopd.ProductDeliveryType);
                                        if (vopd.ProductDeliveryType == (int)ProductDeliveryType.Normal)
                                        {
                                            vbsShipUrl = config.SSLSiteUrl + "/vbs/ship/ShipOrderList/" + (vpd.MainBid ?? vpd.BusinessHourGuid) +
                                                         "/Ppon?selQueryOption=orderId&queryKeyWord=" + orderId + "&pageNumber=1";
                                        }
                                        else
                                        {
                                            int d = 1; //待出貨
                                            if (!string.IsNullOrEmpty(vopd.PreShipNo) && !vopd.IsShipmentPdf)
                                            {
                                                d = 2; //已產生配送編號、未列印配送編號=>待列印出貨標籤
                                            }
                                            if (vopd.IsShipmentPdf)
                                            {
                                                d = 3; //已列印配送編號=>待回報出貨
                                            }
                                            vbsShipUrl = config.SSLSiteUrl + "/vbs/ship/ispshiporderlist?d=" + d;
                                        }
                                    }
                                    else
                                    {
                                        log.Error("逾期/即將逾期出貨訂單，查無ViewOrderProductDelivery: " + oGuid);
                                    }

                                    var expiringOrder = new ExpiringOrderToSellerMail.ExpiringOrder
                                    {
                                        SeqNo = ++i,
                                        StartOrderDate = vpd.BusinessHourOrderTimeS.ToString("yyyy/MM/dd"),
                                        EndOrderDate = vpd.BusinessHourOrderTimeE.ToString("yyyy/MM/dd"),
                                        ShippingCondition =
                                            shippingDateInfo != null ? shippingDateInfo.ShipTypeDesc : string.Empty,
                                        LastShipDate =
                                            order.LastShipDate != null
                                                ? ((DateTime) order.LastShipDate).ToString("yyyy/MM/dd")
                                                : string.Empty,
                                        DealUniqueId = vpd.UniqueId ?? 0,
                                        ItemName = vpd.ItemName,
                                        //OrderQty = order.OrderCount ?? 0,
                                        //VbsUrl = config.SSLSiteUrl + "/vbs/ship/ShipBatchImport/" + (vpd.MainBid ?? vpd.BusinessHourGuid) + "/Ppon",
                                        VbsShipUrl = vbsShipUrl,
                                        FineAmount = 200, //(order.OrderCount ?? 0) * 200
                                        OrderId = orderId,
                                        DeliveryTypeDesc = deliveryTypeDesc,
                                    };

                                    totalFineAmount += expiringOrder.FineAmount;
                                    expiringOrders.Add(expiringOrder);
                                }
                            }
                        }

                        
                        #endregion
                        #region Send mail

                        var titlePrefix = isExpired ? "<重要已逾期>" : string.Empty;
                        var titleSuffix = isExpired ? "已逾期出貨訂單通知" : "即將逾期訂單通知";

                        if (expiringOrders.Any())
                        {
                            var template = TemplateFactory.Instance().GetTemplate<ExpiringOrderToSellerMail>();
                            template.IsExpired = isExpired;
                            template.SellerName = sellerName;
                            template.VbsShipUrl = config.SSLSiteUrl + "/vbs/ship/ShipDealList";
                            template.ExpiringOrders = expiringOrders;
                            template.HttpsSiteUrl = config.SSLSiteUrl;
                            template.SiteUrl = config.SiteUrl;
                            template.SiteServiceUrl = config.SiteUrl + "/Ppon/NewbieGuide.aspx";
                            template.TotalFineAmount = totalFineAmount;

                            try
                            {
                                string[] mailList = mailTo.Split(",");
                                foreach (string m in mailList)
                                {
                                    MailMessage msg = new MailMessage();
                                    msg.Subject = string.Format("{0}17Life【{1}】{2}", titlePrefix, sellerName, titleSuffix);
                                    msg.From = new MailAddress(config.NewOrderShipEmailAddress, config.NewOrderShipEmailDisplayName);
                                    msg.To.Add(m);
                                    msg.Body = template.ToString();
                                    msg.IsBodyHtml = true;
                                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                                }
                                    
                            }
                            catch (Exception ex)
                            {
                                log.Error(ex.Message);
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        SendErrorEmail(sellerGuid, checkContacts.Value);
                        log.Error(checkContacts.Value);
                    }
                }
            }
        }

        public static void SendFinalReturnNotifyToSellers()
        {
            if (!config.EnableNewReturnOrExchangeMailNotify)
            {
                return;
            }

            List<int> progressStatusLists = new List<int>();
            progressStatusLists.Add((int)ProgressStatus.Processing);
            List<int> vendorProgressStatusLists = new List<int>();
            vendorProgressStatusLists.Add((int)VendorProgressStatus.Processing);
            vendorProgressStatusLists.Add((int)VendorProgressStatus.Retrieving);

            var returnFormList = op.ViewOrderReturnFormListGetList(DeliveryType.ToHouse, null, progressStatusLists,
            vendorProgressStatusLists, null, null, null, null, null, null, null, 0).Where(x => x.ReturnApplicationTime.Date == DateTime.Now.Date.AddDays(-config.NotifyVendorProcessReturnDays));
           
            foreach (var sid in returnFormList.GroupBy(x => x.SellerGuid).Select(s => s.Key))
            {
                var returnInfo = new List<NewReturnOrExchangeToSellerMail.ProductRefundOrExchangeModel>();
                foreach (var vbsOrderReturn in returnFormList.Where(x => x.SellerGuid == sid))
                {
                    var info = new NewReturnOrExchangeToSellerMail.ProductRefundOrExchangeModel();
                    info.IsExchange = false;

                    //檢查結黨的是否能抓到
                    var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vbsOrderReturn.ProductGuid);
                    if (vpd == null)
                    {
                        return;
                    }

                    Dictionary<int, string> orderItemLists = op.GetReturnOrderProductOptionDesc(new List<int> { vbsOrderReturn.ReturnFormId });
                    var specAndQuanty = orderItemLists.ContainsKey(vbsOrderReturn.ReturnFormId) ? orderItemLists[vbsOrderReturn.ReturnFormId] : string.Empty;

                    info.CreateTime = vbsOrderReturn.ReturnApplicationTime.ToString("yyyy/MM/dd");
                    info.OrderId = vbsOrderReturn.OrderId;
                    info.DealUniqueId = vbsOrderReturn.ProductId;
                    info.ReciverName = vbsOrderReturn.MemberName;
                    info.ItemName = vbsOrderReturn.DealName;
                    info.SpecAndQuanty = specAndQuanty;
                    info.Reason = vbsOrderReturn.ReturnReason;
                    info.SellerGuid = vbsOrderReturn.SellerGuid;
                    info.SellerName = vbsOrderReturn.SellerName;
                    info.VbsUrl = config.SSLSiteUrl + "/vbs/ship/ReturnOrderProcessList";
                    info.VbsDealIdUrl = config.SSLSiteUrl + "/vbs/ship/ReturnOrderProcessList?dealId=" + vbsOrderReturn.ProductId;
                    info.VbsOrderIdUrl = config.SSLSiteUrl + "/vbs/ship/ReturnOrderProcessList?orderId=" + vbsOrderReturn.OrderId;
                    info.LastReturnDate = vbsOrderReturn.ReturnApplicationTime.Date.AddDays(6).ToString("yyyy/MM/dd");

                    returnInfo.Add(info);
                }

                //確認商家收件人
                var checkContacts = CustomerServiceFacade.GetSellerContacts(sid, ProposalContact.ReturnPerson);
                if (checkContacts.Key)
                {
                    var mailTo = checkContacts.Value;

                    #region Send mail

                    var template = TemplateFactory.Instance().GetTemplate<NewReturnOrExchangeToSellerMail>();
                    template.HttpsSiteUrl = config.SSLSiteUrl;
                    template.SiteUrl = config.SiteUrl;
                    template.SiteServiceUrl = "/Ppon/NewbieGuide.aspx";
                    template.ReturnInfo = returnInfo;
                    
                    try
                    {
                        string[] mailList = mailTo.Split(",");
                        foreach (string m in mailList)
                        {
                            MailMessage msg = new MailMessage();
                            msg.Subject = "【最後通知】退貨處理期限即將到期，請於三天內完成退貨";
                            msg.From = new MailAddress(config.NewReturnOrExchangepEmailAddress, config.NewReturnOrExchangeEmailDisplayName);
                            msg.To.Add(m);
                            msg.Body = template.ToString();
                            msg.IsBodyHtml = true;
                            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                        }
                            
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message);
                    }

                    #endregion
                }
                else
                {
                    SendErrorEmail(sid, checkContacts.Value);
                    log.Error(checkContacts.Value);
                }
            }
        }

        public static void SendNotCompleteReturnMailToCSD(VendorProgressStatus status)
        {
            if (!config.EnableNewReturnOrExchangeMailNotify)
            {
                return;
            }

            List<int> progressStatusLists = new List<int>();
            progressStatusLists.Add((int)ProgressStatus.Processing);
            progressStatusLists.Add((int)ProgressStatus.ConfirmedForCS);
            progressStatusLists.Add((int)ProgressStatus.ConfirmedForUnArrival);


            List<int> vendorProgressStatusLists = new List<int>();
            string subject = string.Empty;
            string body = string.Empty;
            if (status == VendorProgressStatus.Processing)
            {
                vendorProgressStatusLists.Add((int)VendorProgressStatus.Processing);
                vendorProgressStatusLists.Add((int)VendorProgressStatus.Retrieving);
                vendorProgressStatusLists.Add((int)VendorProgressStatus.ConfirmedForCS);
                vendorProgressStatusLists.Add((int)VendorProgressStatus.ConfirmedForUnArrival);
                subject = "【請客服聯繫廠商】退貨未完成";
                body = @"親愛的客服同仁，<br/><br/>下方表格是已經過了[最晚完成退貨日期]的所有退貨訂單，請協助聯繫廠商，謝謝。<br/><br/><table width='850' cellspacing='0' border='1' bordercolor='#000000' style='border-collapse:collapse';><tr style='font-size: 14px; line-height: 24px; background: #eee; color: #444444;'><td>商家名稱</td><td class='auto-style1'>退貨<br/>申請日</td><td class='auto-style1'>訂單編號</td><td class='auto-style1'>檔號</td><td class='auto-style1'>商品名稱</td><td class='auto-style1'>退貨<br/>規格數量</td><td class='auto-style1'>退貨<br/>原因與備註</td><td class='auto-style1'>最晚完成<br/>退貨日期</td></tr>";
            }
            else if (status == VendorProgressStatus.Unreturnable)
            {
                vendorProgressStatusLists.Add((int)VendorProgressStatus.Unreturnable);
                subject = string.Format("訂單無法完成退貨通知<{0}>",DateTime.Now.ToString("yyyy/MM/dd"));
                body = @"Dear 客服同仁，<br/><br/>商家壓記無法退貨完成的訂單如下，請協助處理，謝謝：<br/><br/><table width='850' cellspacing='0' border='1' bordercolor='#000000' style='border-collapse:collapse';><tr style='font-size: 14px; line-height: 24px; background: #eee; color: #444444;'><td>商家名稱</td><td class='auto-style1'>訂單編號</td><td class='auto-style1'>商品名稱</td><td class='auto-style1'>退貨原因</td><td class='auto-style1'>無法退貨原因</td><td class='auto-style1'>廠商壓記無法退貨日</td></tr>";
            }

            var returnFormList = op.ViewOrderReturnFormListGetList(DeliveryType.ToHouse, null, progressStatusLists,
            vendorProgressStatusLists, null, null, null, null, null, null, null, 0).Where(x => status == VendorProgressStatus.Processing ? x.ReturnApplicationTime.Date.AddDays(7) < DateTime.Now : true);

            foreach (var vbsOrderReturn in returnFormList)
            {
                var info = new NewReturnOrExchangeToSellerMail.ProductRefundOrExchangeModel();
                info.IsExchange = false;

                //檢查結黨的是否能抓到
                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vbsOrderReturn.ProductGuid);
                if (vpd == null)
                {
                    return;
                }

                Dictionary<int, string> orderItemLists = op.GetReturnOrderProductOptionDesc(new List<int> { vbsOrderReturn.ReturnFormId });
                var specAndQuanty = orderItemLists.ContainsKey(vbsOrderReturn.ReturnFormId) ? orderItemLists[vbsOrderReturn.ReturnFormId] : string.Empty;

                info.VbsUrl = config.SSLSiteUrl + "/vbs/ship/ReturnOrderList/" + (vpd.MainBid ?? vpd.BusinessHourGuid) + "/Ppon";

                if (status == VendorProgressStatus.Processing)
                {
                    body += string.Format(@"<tr style='font-size: 12px; line-height: 24px; color: #444444;'><td class='auto-style1'><a href='{0}'>{1}</a></td><td class='auto-style1'>{2}</td><td class='auto-style1'><a href='{3}'>{4}</a></td><td>{5}</td><td class='auto-style1'>{6}</td><td class='auto-style1'>{7}</td><td class='auto-style1'>{8}</td><td class='auto-style1'>{9}</td></tr>", config.SiteUrl + "/controlroom/seller/seller_add.aspx?sid=" + vbsOrderReturn.SellerGuid, vbsOrderReturn.SellerName, vbsOrderReturn.ReturnApplicationTime.ToString("yyyy/MM/dd"), config.SiteUrl + "/controlroom/order/order_detail.aspx?oid=" + vbsOrderReturn.OrderGuid, (vpd.IsWms ? "<font color='red'>24H</font><br />" : string.Empty) + vbsOrderReturn.OrderId, vbsOrderReturn.ProductId,  vbsOrderReturn.DealName, specAndQuanty, vbsOrderReturn.ReturnReason, vbsOrderReturn.ReturnApplicationTime.Date.AddDays(6).ToString("yyyy/MM/dd"));
                }
                else if (status == VendorProgressStatus.Unreturnable)
                {
                    body += string.Format(@"<tr style='font-size: 12px; line-height: 24px; color: #444444;'><td class='auto-style1'><a href='{0}'>{1}</a></td><td class='auto-style1'><a href='{2}'>{3}</a></td><td class='auto-style1'>{4}</td><td>{5}</td><td class='auto-style1'>{6}</td><td class='auto-style1'>{7}</td></tr>", config.SiteUrl + "/controlroom/seller/seller_add.aspx?sid=" + vbsOrderReturn.SellerGuid, vbsOrderReturn.SellerName, config.SiteUrl + "/controlroom/order/order_detail.aspx?oid=" + vbsOrderReturn.OrderGuid, vpd.IsWms ? "<font color='red'>24H</font><br />" : string.Empty + vbsOrderReturn.OrderId, vbsOrderReturn.DealName, vbsOrderReturn.ReturnReason, vbsOrderReturn.VendorMemo, vbsOrderReturn.VendorProcessTime.HasValue ? vbsOrderReturn.VendorProcessTime.Value.ToString("yyyy/MM/dd") : string.Empty);
                }
            }

            body += "</table>";

            #region Send mail

            try
            {
                MailMessage msg = new MailMessage();
                msg.Subject = subject;
                msg.From = new MailAddress(config.NewReturnOrExchangepEmailAddress, config.NewReturnOrExchangeEmailDisplayName);
                msg.To.Add(config.CsdEmail);
                msg.Body = body;
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }

            #endregion
        }

        #endregion

        #region 一般通用通知信

        /// <summary>
        /// 寄出一般通知信
        /// </summary>
        /// <param name="subject">信件主旨</param>
        /// <param name="greeting">信件問候語</param>
        /// <param name="mailContent">信件內文</param>
        /// <param name="mailTo">收件人</param>
        public static void SendNowCommonNotification(string subject, string greeting, string mailContent, List<string> mailTo)
        {
            var template = TemplateFactory.Instance().GetTemplate<CommonNotification>();
            template.HttpsSiteUrl = config.SSLSiteUrl;
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = "/Ppon/NewbieGuide.aspx";
            template.Model = new CommonNotification.CommonNotificationModel
            {
                Subject = subject,
                Greeting = greeting,
                Message = mailContent
            };

            try
            {
                MailMessage msg = new MailMessage();
                msg.Subject = subject;
                msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                foreach (var email in mailTo)
                {
                    msg.To.Add(email);
                }
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }

        #endregion

        #region 一次性通知

        /// <summary>
        /// 通知商家 Google shopping Ads 相關規定
        /// </summary>
        public static int GsaNotifyToSeller()
        {
            //匯整所有宅配商家一般聯絡資訊
            List<KeyValuePair<Guid, string>> sellerEmailList = CustomerServiceFacade.GetClearSellerEmail(DeliveryType.ToHouse, ProposalContact.Normal);
            
            foreach (var sellerContact in sellerEmailList)
            {
                var template = TemplateFactory.Instance().GetTemplate<GsaNotifyToSeller>();
                template.HttpsSiteUrl = config.SSLSiteUrl;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = "/Ppon/NewbieGuide.aspx";

                try
                {
                    MailMessage msg = new MailMessage();
                    msg.Subject = "《17Life快訊》商家必讀";
                    msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                    msg.To.Add(sellerContact.Value);
                    msg.Body = template.ToString();
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
                catch (Exception ex)
                {
                    log.Error("GsaNotifyToSeller Exception, " + ex.Message);
                }
                
            }
            return sellerEmailList.Count;
        }

        #endregion


        /// <summary>
        /// 商家email有誤時需通知業務
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="bid"></param>
        /// <param name="errorMsg"></param>
        public static void SendErrorEmail(Guid sellerGuid, string errorMsg)
        {
            SellerSaleCollection ssc = sp.SellerSaleGetBySellerGuid(sellerGuid);
            MailMessage msg = new MailMessage();
           

            string deptId = string.Empty;
            foreach (SellerSale ss in ssc)
            {
                ViewEmployee emp = humanProv.ViewEmployeeGet(ViewEmployee.Columns.UserId, ss.SellerSalesId);
                if (emp.IsLoaded)
                {
                    deptId = emp.DeptId;
                    msg.To.Add(emp.Email);
                }
            }

            if (!string.IsNullOrEmpty(deptId))
            {
                string sales = ProposalFacade.PponAssistantEmailGet(deptId);
                if (!msg.To.Contains(new MailAddress(sales)))
                    msg.To.Add(sales);

            }
            msg.From = new MailAddress(config.SystemEmail);
            msg.Subject = "商家Email錯誤通知_" + DateTime.Now.ToString("yyyy/MM/dd");
            msg.Body = HttpUtility.HtmlDecode(errorMsg);
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }
    }
}
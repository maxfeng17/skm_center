﻿using System.Net.Mail;
using iTextSharp.text;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.BookingSystem;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using DayType = LunchKingSite.Core.DayType;


namespace LunchKingSite.BizLogic.Facade
{
    public class BookingSystemFacade
    {
        private static IBookingSystemProvider bsp;
        private static IMemberProvider mp;
        private static IPponProvider pp;
        private static ISysConfProvider config;

        static BookingSystemFacade()
        {
            bsp = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        #region 後台分店設定訂位資料

        /// <summary>
        /// 新增一筆商家訂位設定
        /// </summary>
        /// <param name="store"></param>
        /// <param name="timeSlotValues"></param>
        public static int MakeBookingSystemStoreDefaultSetting(BookingSystemStore store, Dictionary<string, int> timeSlotValues, ref List<KeyValuePair<string, string>> changelogs)
        {
            int bookingId = bsp.BookingSystemStoreSet(store);

            if (bookingId > 0)
            {
                BookingSystemDate bookingSystemDate;
                int bookingSystemDateId;
                bookingSystemDate = bsp.BookingSystemDateGetByStoreBookingId(bookingId);

                if (bookingSystemDate.IsLoaded)
                {
                    bookingSystemDateId = bookingSystemDate.Id;
                }
                else
                {
                    bookingSystemDate = new BookingSystemDate();
                    bookingSystemDate.BookingSystemStoreBookingId = bookingId;
                    bookingSystemDate.DayType = (int)DayType.DefaultSet;
                    bookingSystemDate.IsOpening = true;
                    bookingSystemDateId = bsp.BookingSystemDateSet(bookingSystemDate);
                }

                if (bookingSystemDateId > 0)
                {
                    MakeBookingSystemTimeSlotData(bookingSystemDateId, timeSlotValues, ref changelogs);
                }

                //精簡設定:指定日期若符合欲調整之dayType 則一併更新指定日期之訂位設定
                var dayTypes = GetBookingDayType(store);

                MakeBookingSystemAssignDateSettingByDayType(store.BookingId, dayTypes, true, timeSlotValues, ref changelogs, false);
            }
            return bookingId;
        }

        /// <summary>
        /// 新增/修改一筆指定 (日期/星期) 訂位資訊
        /// </summary>
        public static void MakeBookingSystemDateSetting(int storeBookingId, DayType dayType, DateTime? effectiveDay, bool isOpening, Dictionary<string, int> timeSlotValues, ref List<KeyValuePair<string, string>> changelogs)
        {
            if (dayType == DayType.DefaultSet)
            {
                return;
            }

            BookingSystemDate bookingSystemDate = bsp.BookingSystemDateGet(storeBookingId, dayType, effectiveDay);

            if (bookingSystemDate.IsLoaded)
            {
                if (bookingSystemDate.IsOpening != isOpening)
                {
                    if (dayType == DayType.AssignDay)
                    {
                        changelogs.Add(new KeyValuePair<string, string>("開放預約", ((effectiveDay.HasValue) ? effectiveDay.Value.ToString("yyyy/MM/dd") : "") + bookingSystemDate.IsOpening.ToString() + "-->" + isOpening.ToString()));
                    }
                    else
                    {
                        changelogs.Add(new KeyValuePair<string, string>("WeekDaySetting", dayType.ToString() + bookingSystemDate.IsOpening.ToString() + "-->" + isOpening.ToString()));
                    }

                }

                bookingSystemDate.IsOpening = isOpening;
                bsp.BookingSystemDateSet(bookingSystemDate);
            }
            else
            {
                bookingSystemDate.BookingSystemStoreBookingId = storeBookingId;
                bookingSystemDate.DayType = (int)dayType;
                bookingSystemDate.EffectiveDate = effectiveDay;
                bookingSystemDate.IsOpening = isOpening;
                bsp.BookingSystemDateSet(bookingSystemDate);
                if (dayType == DayType.AssignDay)
                {
                    changelogs.Add(new KeyValuePair<string, string>("開放預約", ((effectiveDay.HasValue) ? effectiveDay.Value.ToString("yyyy/MM/dd") : "") + bookingSystemDate.IsOpening.ToString() + "-->Create"));
                }
                else
                {
                    changelogs.Add(new KeyValuePair<string, string>("WeekDaySetting", dayType.ToString() + "-->Create"));
                }

            }

            MakeBookingSystemTimeSlotData(bookingSystemDate.Id, timeSlotValues, ref changelogs);
            
            //詳細設定:指定日期若符合欲調整之dayType 則一併更新指定日期之訂位設定
            if (dayType != DayType.AssignDay)
            {
                MakeBookingSystemAssignDateSettingByDayType(storeBookingId, new List<DayType> { dayType }, isOpening, timeSlotValues, ref changelogs, true);
            }
        }

        //指定日期若符合欲調整之dayType 則一併更新指定日期之訂位設定
        private static void MakeBookingSystemAssignDateSettingByDayType(int bookingId, IEnumerable<DayType> dayType, bool isOpening,
            Dictionary<string, int> timeSlotValues, ref List<KeyValuePair<string, string>> changelogs, bool isDetailSetting)
        {
            if (!dayType.Any() || dayType.Contains(DayType.AssignDay))
            {
                return;
            }
            
            var assignDays = bsp.BookingSystemDateGetList(bookingId)
                                .Where(x => x.DayType == (int)DayType.AssignDay && 
                                            x.EffectiveDate.HasValue && 
                                            DateTime.Compare(DateTime.Today, x.EffectiveDate.Value) <= 0)
                                .ToList();
            if (!assignDays.Any())
            {
                return;
            }

            foreach (var assignDay in assignDays)
            {
                //檢查為精簡設定 or 詳細設定
                if (dayType.Contains(GetBookingDayType(assignDay.EffectiveDate.Value)))
                {
                    MakeBookingSystemTimeSlotData(assignDay.Id, timeSlotValues, ref changelogs);
                }
                else if (!isDetailSetting)
                {
                    isOpening = false;
                }
                else
                {
                    continue;
                }
                //精簡設定:須一併調整 bookingSystemDate isOpening狀態，不在更新的DayType中需更新為不開放預約 
                //詳細設定:不在更新的DayType中不須更新
                if (assignDay.IsOpening != isOpening)
                { 
                    MakeBookingSystemDateSetting(bookingId, DayType.AssignDay, assignDay.EffectiveDate, isOpening, timeSlotValues, ref changelogs);
                }
            }
        }

        //用途:調整可預約日期及人數 抓取目前已預約人數紀錄 以利檢查欲修改之日期是否影響已預約紀錄並加以提醒
        public static Dictionary<DayType, ILookup<string, int>> GetReservationListInfo(int bookingId)
        {
            //抓取此時段及訂位人數 
            return bsp.ViewBookingSystemReservationListGetList(bookingId)
                .Where(x =>
                {
                    if (x.IsCancel)
                    {
                        return false;
                    }
                    var tempHourMins = x.TimeSlot.Split(":");
                    if (tempHourMins.Length != 2)
                    {
                        return false;
                    }
                    var tempDate = new DateTime(x.ReservationDate.Year, x.ReservationDate.Month, x.ReservationDate.Day,
                        int.Parse(tempHourMins[0]), int.Parse(tempHourMins[1]), 0);

                    return DateTime.Compare(DateTime.Now, tempDate) < 0;
                })
                .GroupBy(x => new { x.ReservationDate, x.TimeSlot })
                .Select(x=> new { DayType = GetBookingDayType(x.Key.ReservationDate), x.Key.TimeSlot, ReservationPeople = x.Sum(p => p.NumberOfPeople)})
                .GroupBy(x => x.DayType)
                .ToDictionary(x => x.Key, x => x.ToLookup(t => t.TimeSlot, t => t.ReservationPeople));
        }

        /// <summary>
        /// 該分店是否已設定訂位系統設定
        /// </summary>
        /// <param name="storeGuid">StoreGuid</param>
        /// <returns>IsThisStoreHaveSettingInBookingSystem</returns>
        public static bool GetBookingSystemStoreSettingByStoreGuid(Guid storeGuid, BookingType bookingType)
        {
            var bookingstore = bsp.BookingSystemStoreGetByStoreGuid(storeGuid, bookingType);

            return (bookingstore.IsLoaded);
        }

        public static BookingSystemTimeSlotCollection GetBookingSystemTimeSlotListByBookingSystemDateId(int bookingSystemDateId)
        {
            return bsp.BookingSystemTimeSlotGet(bookingSystemDateId);
        }

        public static BookingSystemStore GetBookingSystemStoreByStoreGuid(Guid storeGuid, BookingType bookingType)
        {
            return bsp.BookingSystemStoreGetByStoreGuid(storeGuid, bookingType);
        }

        public static void MakeBookingSystemTimeSlotData(int bookingSystemDateId, Dictionary<string, int> timeSlotValues, ref List<KeyValuePair<string, string>> changelogs)
        {
            var timeSlotCol = GetBookingSystemTimeSlotListByBookingSystemDateId(bookingSystemDateId);
            
            foreach (var item in timeSlotCol)
            {
                if (timeSlotValues.Any(x => x.Key == item.TimeSlot))
                {
                    continue;
                }
                changelogs.Add(new KeyValuePair<string, string>(item.TimeSlot, item.MaxNumberOfPeople + "-->0"));
                item.MaxNumberOfPeople = 0;
                bsp.BookingSystemTimeSlotSet(item);
            }

            foreach (KeyValuePair<string, int> readTimeSlotValue in timeSlotValues)
            {
                BookingSystemTimeSlot bookingSystemTimeSlot;
                if (timeSlotCol.Any(x => x.TimeSlot == readTimeSlotValue.Key))
                {
                    bookingSystemTimeSlot = timeSlotCol.First(x => x.TimeSlot == readTimeSlotValue.Key);
                }
                else
                {
                    bookingSystemTimeSlot = new BookingSystemTimeSlot();
                }
                if (bookingSystemTimeSlot.MaxNumberOfPeople != readTimeSlotValue.Value)
                {
                    changelogs.Add(new KeyValuePair<string, string>(readTimeSlotValue.Key, bookingSystemTimeSlot.MaxNumberOfPeople + "-->" + readTimeSlotValue.Value));
                }
                
                bookingSystemTimeSlot.BookingSystemDateId = bookingSystemDateId;
                bookingSystemTimeSlot.TimeSlot = readTimeSlotValue.Key;
                bookingSystemTimeSlot.MaxNumberOfPeople = readTimeSlotValue.Value;
                bsp.BookingSystemTimeSlotSet(bookingSystemTimeSlot);
            }
        }

        public static bool BookingSystemCouponIsExistBySequenceOrderDetailGuid(string sequence, Guid orderDetailGuid)
        {
            return bsp.BookingSystemCouponIsExistBySequenceOrderDetailGuid(sequence, orderDetailGuid);
        }

        public static ViewBookingSystemCouponReservationCollection GetViewBookingSystemCouponReservationBySequenceNumber(Guid orderDetailGuid, string sequenceNumber)
        {
            return bsp.ViewBookingSystemCouponReservationGetBySequenceNumber(orderDetailGuid, sequenceNumber);
        }

        public static ViewBookingSystemCouponReservationCollection GetViewBookingSystemCouponReservationBySequenceNumber(string orderKey, string sequenceNumber)
        {
            return bsp.ViewBookingSystemCouponReservationGetBySequenceNumber(orderKey, sequenceNumber);
        }

        public static ViewBookingSystemCouponReservationCollection GetViewBookingSystemCouponReservationTraveLockRecordByOrderKey(string orderKey)
        {
            return bsp.ViewBookingSystemCouponReservationGetByOrderKey(orderKey).Where(ViewBookingSystemCouponReservation.Columns.BookingType, (int)BookingType.Travel);
        }

        public static void UpdateBookingSystemReservation(BookingSystemStore bookingSystemStore, DateTime effectiveDate)
        {
            List<ViewBookingSystemStoreDate> viewBookingSystemStoreDateList = bsp.ViewBookingSystemStoreDateGetByBookingSystemStoreGuid(bookingSystemStore.StoreGuid).ToList();

            ViewBookingSystemStoreDate assignBookingSystemDate = viewBookingSystemStoreDateList.Single(x => x.DayType == (int)DayType.AssignDay && x.EffectiveDate == effectiveDate);
            var assignTimeSlots = bsp.BookingSystemTimeSlotGet(assignBookingSystemDate.BookingSystemDateId).ToList();

            #region DayOfWeekSetting

            if (viewBookingSystemStoreDateList.Any(x => x.DayType == (int)GetBookingDayType(effectiveDate)))
            {
                ViewBookingSystemStoreDate dayOfWeekBookingSystemDate = viewBookingSystemStoreDateList.Single(x => x.DayType == (int)GetBookingDayType(effectiveDate));
                var dayOfWeekReservationCol = bsp.BookingSystemReservationGetListByBookingSystemDateAndDay(dayOfWeekBookingSystemDate.BookingSystemDateId, effectiveDate);
                var dayOfWeekTimeSlots = bsp.BookingSystemTimeSlotGet(dayOfWeekBookingSystemDate.BookingSystemDateId).ToList();

                foreach (var reservation in dayOfWeekReservationCol)
                {
                    if (assignTimeSlots.Any(x => x.TimeSlot == dayOfWeekTimeSlots.First(y => y.Id == reservation.TimeSlotId).TimeSlot))
                    {
                        reservation.TimeSlotId = assignTimeSlots.First(x => x.TimeSlot == dayOfWeekTimeSlots.First(y => y.Id == reservation.TimeSlotId).TimeSlot).Id;
                        bsp.VbsBookingSystemReservationSet(reservation);
                    }
                    else
                    {
                        BookingSystemTimeSlot newTimeSlot = new BookingSystemTimeSlot
                        {
                            BookingSystemDateId = assignBookingSystemDate.BookingSystemDateId,
                            TimeSlot = dayOfWeekTimeSlots.First(y => y.Id == reservation.TimeSlotId).TimeSlot,
                            MaxNumberOfPeople = (dayOfWeekReservationCol.Where(x => !x.IsCancel && x.TimeSlotId == reservation.TimeSlotId).GroupBy(z => z.TimeSlotId).Select(x => new { MaxOfNumber = x.Sum(p => p.NumberOfPeople) })).First().MaxOfNumber
                        };
                        bsp.BookingSystemTimeSlotSet(newTimeSlot);
                        assignTimeSlots = bsp.BookingSystemTimeSlotGet(assignBookingSystemDate.BookingSystemDateId).ToList();
                    }
                }
            }

            #endregion DayOfWeekSetting

            #region DefaultSetting

            ViewBookingSystemStoreDate defaultBookingSystemDate = viewBookingSystemStoreDateList.Single(x => x.DayType == (int)DayType.DefaultSet);
            var defaultReservationCol = bsp.BookingSystemReservationGetListByBookingSystemDateAndDay(defaultBookingSystemDate.BookingSystemDateId, effectiveDate);
            var defaultTimeSlots = bsp.BookingSystemTimeSlotGet(defaultBookingSystemDate.BookingSystemDateId).ToList();

            foreach (var reservation in defaultReservationCol)
            {
                if (assignTimeSlots.Any(x => x.TimeSlot == defaultTimeSlots.First(y => y.Id == reservation.TimeSlotId).TimeSlot))
                {
                    reservation.TimeSlotId = assignTimeSlots.First(x => x.TimeSlot == defaultTimeSlots.First(y => y.Id == reservation.TimeSlotId).TimeSlot).Id;
                    bsp.VbsBookingSystemReservationSet(reservation);
                }
                else
                {
                    BookingSystemTimeSlot newTimeSlot = new BookingSystemTimeSlot
                    {
                        BookingSystemDateId = assignBookingSystemDate.BookingSystemDateId,
                        TimeSlot = defaultTimeSlots.First(y => y.Id == reservation.TimeSlotId).TimeSlot,
                        MaxNumberOfPeople = (defaultReservationCol.Where(x => !x.IsCancel && x.TimeSlotId == reservation.TimeSlotId).GroupBy(z => z.TimeSlotId).Select(x => new { MaxOfNumber = x.Sum(p => p.NumberOfPeople) })).First().MaxOfNumber
                    };
                    bsp.BookingSystemTimeSlotSet(newTimeSlot);
                    assignTimeSlots = bsp.BookingSystemTimeSlotGet(assignBookingSystemDate.BookingSystemDateId).ToList();
                }
            }

            #endregion DefaultSetting
        }

        #endregion

        #region 前台訂位系統

        public static void CancelReservation(int reservationId)
        {
            bsp.SetReservationRecordCancel(reservationId);
        }

        /// <summary>
        /// 查詢店家該日期是否有開店, 回傳 BookingSystemDateId
        /// </summary>
        /// <param name="bookingType">憑證/旅遊</param>
        /// <param name="storeGuid">Store GUID</param>
        /// <param name="queryDate">Query Date</param>
        /// <param name="dateId">Booking_System_Date.Id</param>
        /// <returns>True: Open, False: Close</returns>
        public static bool IsOpenReservationServiceByDate(BookingType bookingType, string storeGuid, DateTime queryDate, out int dateId)
        {
            Guid guid = new Guid();
            if (Guid.TryParse(storeGuid, out guid))
            {
                BookingSystemStore store = bsp.BookingSystemStoreGetByStoreGuid(guid, bookingType);
                if (store.IsLoaded)
                {
                    ViewBookingSystemStoreDateCollection dateCol = bsp.ViewBookingSystemStoreDateGetByDate(store, queryDate).OrderByAsc("Day_Type");
                    foreach (ViewBookingSystemStoreDate dateReader in dateCol)
                    {
                        dateId = dateReader.BookingSystemDateId;
                        if (dateReader.DayType == (int)DayType.AssignDay)
                        {
                            return dateReader.IsOpening;
                        }
                        else if (dateReader.DayType == (int)bsp.DayTypeParse(queryDate))
                        {
                            return dateReader.IsOpening;
                        }
                        else if (dateReader.DayType == (int)DayType.DefaultSet)
                        {
                            return CheckDayTypeIsOpen(store, queryDate);
                        }
                    }
                }
            }
            dateId = 0;
            return false;
        }

        public static bool IsFullReservation(string storeGuid, BookingType bookingType, DateTime queryDate)
        {
            Guid guid = new Guid();
            if (!Guid.TryParse(storeGuid, out guid))
            {
                return true;
            }

            BookingSystemStore store = bsp.BookingSystemStoreGetByStoreGuid(guid, bookingType);
            if (store.IsLoaded)
            {
                ViewBookingSystemReservationInfoDayCollection InfoDay = bsp.ViewBookingSystemReservationInfoDayGetByDate(store.BookingId, queryDate);
                if (!InfoDay.Any())
                {
                    return false;
                }

                //無預約紀錄的MaxOfNumber上限值問題
                int queryDateMaxPeopleNumber = default(int);
                var viewBookingSystemMaxNumberList = bsp.ViewBookingSystemMaxNumberListGet(store.BookingId);
                if (viewBookingSystemMaxNumberList.Any(x => x.EffectiveDate == queryDate.Date))
                {
                    queryDateMaxPeopleNumber = viewBookingSystemMaxNumberList.First(x => x.EffectiveDate == queryDate.Date).MaxNumberOfPeople ?? 0;
                }
                else if (viewBookingSystemMaxNumberList.Any(x => !x.EffectiveDate.HasValue))
                {
                    queryDateMaxPeopleNumber = viewBookingSystemMaxNumberList.First(x => !x.EffectiveDate.HasValue).MaxNumberOfPeople ?? 0;
                }

                return (queryDateMaxPeopleNumber - (InfoDay.FirstOrDefault().NumberOfPeople ?? 0) > 0) ? false : true;
            }
            else
            {
                return true;
            }
        }

        public static bool CheckStoreBookingSetting(string orderKey, BookingType bookingType)
        {
            CashTrustLogCollection cashTrustLogCol = mp.CashTrustLogGetCouponSequencesWithUnused(Guid.Parse(orderKey));
            if (cashTrustLogCol.Any() && !bsp.BookingSystemStoreGetByStoreGuid((Guid)cashTrustLogCol.FirstOrDefault().StoreGuid, bookingType).IsLoaded)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool CheckCouponsStatus(string orderKey, BookingType bookingType, BookingSystemServiceName serviceName = BookingSystemServiceName._17Life)
        {
            if (serviceName == BookingSystemServiceName._17Life || serviceName == BookingSystemServiceName.HiDeal)
            {
                CashTrustLogCollection cashTrustLogCol = mp.CashTrustLogGetCouponSequencesWithUnused(Guid.Parse(orderKey));
                BookingSystemCouponCollection bookingSystemCouponCol = bsp.BookingSystemCouponGetByOrderKey(orderKey);
                return cashTrustLogCol.Count > cashTrustLogCol.Where(p => bookingSystemCouponCol.Select(x => x.CouponSequenceNumber).Contains(p.CouponSequenceNumber)).Count() ? true : false;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 消費者新增一筆訂位記錄
        /// </summary>
        /// <param name="reservationDate">訂位日期</param>
        /// <param name="numberOfPeople">訂位人數</param>
        /// <param name="timeSlotId">訂位時段id</param>
        /// <param name="remark">備註</param>
        /// <param name="contactName">訂位聯絡人姓名</param>
        /// <param name="contactGender">訂位聯絡人稱謂</param>
        /// <param name="contactNumber">聯絡人電話</param>
        /// <param name="orderKey">外來鍵(與17Lifet串接則存Order Guid)</param>
        /// <returns>PK</returns>
        public static BookingSystemMakeReservationRecordResult MakeReservationRecord(BookingType bookingType, DateTime reservationDate, int numberOfPeople,
            int timeSlotId, string remark, string contactName, bool contactGender, string contactNumber, string orderKey, string memberKey, string couponInfo, string couponlink, BookingSystemServiceName serviceName, string[] couponsSequence, string email, Guid storeGuid)
        {
            Guid orderGuid;
            BookingSystemCouponCollection coupons;
            if (Guid.TryParse(orderKey, out orderGuid))
            {
                if (CheckDoubleBookingCoupon(orderKey, couponsSequence))
                {
                    return BookingSystemMakeReservationRecordResult.DoubleUsingCoupon;
                }
                //撈取憑證資訊
                coupons = MakeInsertDataForBookingSystemCoupon(serviceName, orderKey, couponsSequence);
                if (coupons.Count != couponsSequence.Length)
                {
                    return BookingSystemMakeReservationRecordResult.DoubleUsingCoupon;
                }
            }
            else
            {
                coupons = new BookingSystemCouponCollection();
            }

            //SendReservationSuccessMail
            string sendEmail = string.Empty;
            string sendName = string.Empty;
            string gender = (contactGender) ? "先生" : "小姐";

            if (string.IsNullOrEmpty(memberKey))
            {
                sendEmail = email;
                sendName = contactName;
            }
            else
            {
                Member m = mp.MemberGet(int.Parse(memberKey));
                sendEmail = m.UserEmail;
                sendName = m.DisplayName;
            }

            BookingSystemReservation reservation = new BookingSystemReservation();
            reservation.TimeSlotId = timeSlotId;
            reservation.ReservationDate = reservationDate;
            reservation.NumberOfPeople = numberOfPeople;
            reservation.Remark = remark;
            reservation.ContactName = contactName;
            reservation.ContactGender = contactGender;
            reservation.ContactNumber = contactNumber;
            reservation.OrderKey = orderKey;
            reservation.MemberKey = memberKey;
            reservation.ApiId = (int)serviceName;
            reservation.CouponInfo = couponInfo;
            reservation.CouponLink = couponlink;
            reservation.IsLock = (bookingType == BookingType.Travel) ? true : false;
            reservation.ContactEmail = sendEmail;

            reservation = bsp.BookingSystemReservationSet(reservation);

            if (reservation.Id > 0)
            {

                foreach (BookingSystemCoupon coupon in coupons)
                {
                    bsp.BookingSystemCouponSetWithReservationId(coupon, reservation.Id);
                }

                if (string.IsNullOrEmpty(sendEmail))
                {
                    return BookingSystemMakeReservationRecordResult.Success;
                }

                DateTime? ReservationTime = BookingSystemFacade.GetTimeSlot(timeSlotId);
                if (ReservationTime == null)
                {
                    return BookingSystemMakeReservationRecordResult.Success;
                }

                ViewBookingSystemStore store = BookingSystemFacade.GetBookingSystemStoreInfo(storeGuid, BookingType.Coupon);
                if (!store.IsLoaded)
                {
                    return BookingSystemMakeReservationRecordResult.Success;
                }

                var reservationSuccessMailInfo = new ReservationSuccessMailInfomation
                {
                    MemberName = sendName + " " + gender,
                    ReservationDate = reservationDate,
                    ReservationTimeSlot = (DateTime)ReservationTime,
                    NumberOfPeople = numberOfPeople,
                    MemberEmail = sendEmail,
                    ContactName = contactName,
                    ContactNumber = contactNumber,
                    Remark = remark,
                    CouponsSequence = couponsSequence.ToList(),
                    CouponInfo = couponInfo,
                    CouponLink = couponlink,
                    SellerName = store.SellerName,
                    StoreName = store.StoreName,
                    StoreAddress = store.AddressString
                };

                BookingSystemFacade.SendReservationSuccessMail(sendEmail, reservationSuccessMailInfo);

                return BookingSystemMakeReservationRecordResult.Success;
            }
            else
            {
                return BookingSystemMakeReservationRecordResult.Fail;
            }

        }

        public static bool CheckDoubleBookingCoupon(string orderKey, string[] couponsSequence)
        {
            BookingSystemCouponCollection bookedCoupons = bsp.BookingSystemCouponGetByOrderKey(orderKey);
            return bookedCoupons.Any(x => couponsSequence.Contains(x.Prefix + x.CouponSequenceNumber));
        }

        public static int GetTimeSlotMaxNumberOfPeople(int timeSlotId)
        {
            return bsp.BookingSystemTimeSlotGetById(timeSlotId).MaxNumberOfPeople;
        }

        public static int GetReservationPeopleOfNumberByDate(int timeSlotId, DateTime date)
        {
            return bsp.ReservationPeopleOfNumberGetByDate(timeSlotId, date);
        }

        public static string GetFullBookingDate(int advanceReservationMinDay, Guid? storeGuid, BookingType bookingType)
        {
            DateTime indexDay = DateTime.Now.AddDays((double)advanceReservationMinDay);
            DateTime maxDate = DateTime.Now.AddDays(15);
            List<string> fullDate = new List<string>();

            if (!storeGuid.HasValue)
            {
                return string.Empty;
            }

            BookingSystemStore store = bsp.BookingSystemStoreGetByStoreGuid((Guid)storeGuid, bookingType);
            if (!store.IsLoaded)
            {
                return string.Empty;
            }

            while (indexDay <= maxDate)
            {
                int dateId = 0;
                if (!BookingSystemFacade.CheckDayTypeIsOpen(store, indexDay))
                {
                    fullDate.Add(indexDay.ToString("yyyy/MM/dd"));
                }
                else if (!BookingSystemFacade.IsOpenReservationServiceByDate(bookingType, storeGuid.ToString(), indexDay, out dateId))
                {
                    fullDate.Add(indexDay.ToString("yyyy/MM/dd"));
                }
                else if (BookingSystemFacade.IsFullReservation(storeGuid.ToString(), bookingType, indexDay))
                {
                    fullDate.Add(indexDay.ToString("yyyy/MM/dd"));
                }
                indexDay = indexDay.AddDays(1);
            }
            return string.Join(",", fullDate);
        }

        public static BookingSystemServiceName GetBookingSystemServiceName(string apiKey, BookingSystemApiServiceType serviceType)
        {
            return bsp.BookingSystemApiGetBookingSystemServiceName(Helper.Decrypt(apiKey), serviceType);
        }

        public static BookingSystemCouponCollection MakeInsertDataForBookingSystemCoupon(BookingSystemServiceName serviceName, string orderKey, string[] couponsSequence)
        {
            BookingSystemCouponCollection coupons = new BookingSystemCouponCollection();
            if (serviceName == BookingSystemServiceName._17Life || serviceName == BookingSystemServiceName.HiDeal)
            {
                CashTrustLogCollection cashTrustLogCol = mp.CashTrustLogGetCouponSequencesWithUnused(Guid.Parse(orderKey));
                int[] status = { (int)TrustStatus.Initial, (int)TrustStatus.Trusted };

                foreach (string sequence in couponsSequence)
                {
                    CashTrustLog cashTrustLog = cashTrustLogCol.Where(x => x.CouponSequenceNumber == (string.IsNullOrEmpty(x.Prefix) ? sequence : sequence.Replace(x.Prefix, "")) && status.Contains(x.Status)).FirstOrDefault();
                    if (cashTrustLog != null)
                    {
                        BookingSystemCoupon coupon = new BookingSystemCoupon();
                        coupon.CouponSequenceNumber = cashTrustLog.CouponSequenceNumber;
                        coupon.OrderDetailGuid = cashTrustLog.OrderDetailGuid;
                        coupon.Prefix = cashTrustLog.Prefix;
                        coupons.Add(coupon);
                    }
                }
            }
            return coupons;
        }

        public static BookingSystemCouponCollection GetBookingSystemCouponsByReservationId(int reservationId)
        {
            return bsp.BookingSystemCouponGetListByReservationId(reservationId);
        }

        public static ViewBookingSystemStore GetBookingSystemStoreInfo(Guid storeGuid, BookingType bookingType)
        {
            return bsp.ViewBookingSystemStoreGetByStoreGuid(storeGuid, bookingType);
        }

        public static DateTime? GetTimeSlot(int timeSlotId)
        {
            BookingSystemTimeSlot timeSlot = bsp.BookingSystemTimeSlotGetById(timeSlotId);
            if (timeSlot.IsLoaded)
            {
                string[] time = timeSlot.TimeSlot.Split(':');
                return DateTime.Now.SetTime(int.Parse(time[0]), int.Parse(time[1]), 0);
            }
            else
            {
                return null;
            }
        }

        public static string BookingSystemApiCheckeExistAndInsert(string serviceName, BookingSystemApiServiceType serviceType)
        {
            if (bsp.BookingSystemApiCheckExistByServiceName(serviceName, serviceType))
            {
                return Helper.Encrypt(serviceName);
            }
            else
            {
                BookingSystemApi api = new BookingSystemApi();
                api.ApiKey = Helper.Encrypt(serviceName);
                api.ServiceName = serviceName;
                api.ServiceType = (int)serviceType;
                return bsp.BookingSystemApiSet(api).ApiKey;
            }
        }

        #endregion

        #region  商家系統 - 訂房鎖定

        public static List<ReserveCouponInfo> GetAccessibleReserveCoupon(bool is17LifeEmployee, List<IVbsVendorAce> allowedDealUnits, string firstCouponSequence, string secoundCouponSequence)
        {
            IEnumerable<ReserveCouponData> pponCouponInfo;
            IEnumerable<ReserveCouponData> piinCouponInfo;

            var result = new List<ReserveCouponInfo>();

            #region initialize ReserveCouponData

            if (is17LifeEmployee)
            {
                pponCouponInfo = bsp.GetReserveCouponWithPpon(firstCouponSequence, secoundCouponSequence);
                piinCouponInfo = bsp.GetReserveCouponWithPiinLife(firstCouponSequence, secoundCouponSequence);
            }
            else
            {
                if (!TryGetPponReserveCouponInfo(firstCouponSequence, secoundCouponSequence, allowedDealUnits, out pponCouponInfo))
                {
                    pponCouponInfo = new List<ReserveCouponData>();

                }
                if (!TryGetPiinReserveCouponInfo(firstCouponSequence, secoundCouponSequence, allowedDealUnits, out piinCouponInfo))
                {
                    piinCouponInfo = new List<ReserveCouponData>();

                }

            }

            #endregion initialize ReserveCouponData

            #region PponCoupon

            foreach (var couponInfo in pponCouponInfo)
            {
                var item = new ReserveCouponInfo();
                item.CouponId = couponInfo.CouponId;
                item.CouponUsage = couponInfo.CouponUsage;
                item.CouponSequence = couponInfo.CouponSequence;
                item.EventName = couponInfo.EventName;
                item.MemberName = VerificationFacade.SetPayerNameHidden(couponInfo.MemberName);
                item.DealUniqueId = couponInfo.DealUniqueId;
                item.ExchangePeriodStartTime = couponInfo.ExchangePeriodStartTime;
                item.ExchangePeriodEndTime = couponInfo.ExchangePeriodEndTime;

                item.IsReservatoinLock = couponInfo.IsReservatoinLock;

                item.DealType = couponInfo.DealType;
                var bookingSystemReserveLockStatusLogList =
                    bsp.BookingSystemReserveLockStatusLogListGet(
                      new string[] { BookingSystemReserveLockStatusLog.Columns.DealType + "=" + (int)couponInfo.DealType, BookingSystemReserveLockStatusLog.Columns.CouponId + "=" + couponInfo.CouponId }
                    ).ToList();

                if (bookingSystemReserveLockStatusLogList.Any())
                {
                    var reserveLockStatus = bookingSystemReserveLockStatusLogList.OrderByDescending(x => x.ModifyTime).First();
                    item.LastModifityTime = reserveLockStatus.ModifyTime;
                    item.CouponReservationDate = reserveLockStatus.CouponReservationDate;
                }
                item.ReserveCouponStatus = ReserveCouponStatus.Valid;

                if (couponInfo.IsDealReserveLock)
                {
                    if (couponInfo.IsReservatoinLock)
                    {
                        item.ReserveCouponStatus = ReserveCouponStatus.Lock;
                    }
                    else
                    {
                        if (!couponInfo.IsReservatoinLock && bookingSystemReserveLockStatusLogList.Any())
                        {
                            item.ReserveCouponStatus = ReserveCouponStatus.UnLock;
                        }

                        switch (couponInfo.CouponStatus)
                        {
                            case (int)TrustStatus.Refunded:
                            case (int)TrustStatus.Returned:
                                item.ReserveCouponStatus = ReserveCouponStatus.Returned;
                                break;
                            case (int)TrustStatus.Trusted:
                            case (int)TrustStatus.Initial:
                                if (couponInfo.OrderGuid.HasValue)
                                {
                                    var returningCtlog = mp.PponRefundingCashTrustLogGetListByOrderGuid(couponInfo.OrderGuid.Value)
                                                            .FirstOrDefault(x => x.CouponId == couponInfo.CouponId);

                                    if (returningCtlog != null)
                                    {
                                        item.ReserveCouponStatus = ReserveCouponStatus.Returning;
                                    }
                                }
                                break;
                        }
                    }
                }
                else
                {
                    item.ReserveCouponStatus = ReserveCouponStatus.Invalid;
                }
                result.Add(item);
            }

            #endregion PponCoupon

            #region PiinLifeCoupon

            foreach (var couponInfo in piinCouponInfo)
            {
                var item = new ReserveCouponInfo();
                item.CouponId = couponInfo.CouponId;
                item.CouponUsage = couponInfo.CouponUsage;
                item.CouponSequence = couponInfo.CouponSequence;
                item.EventName = couponInfo.EventName;
                item.MemberName = VerificationFacade.SetPayerNameHidden(couponInfo.MemberName);
                item.DealUniqueId = couponInfo.DealUniqueId;

                item.IsReservatoinLock = couponInfo.IsReservatoinLock;

                item.DealType = couponInfo.DealType;
                var bookingSystemReserveLockStatusLogList =
                    bsp.BookingSystemReserveLockStatusLogListGet(
                      new string[] { BookingSystemReserveLockStatusLog.Columns.DealType + "=" + (int)couponInfo.DealType, BookingSystemReserveLockStatusLog.Columns.CouponId + "=" + couponInfo.CouponId }
                    ).ToList();

                if (bookingSystemReserveLockStatusLogList.Any())
                {
                    var reserveLockStatus = bookingSystemReserveLockStatusLogList.OrderByDescending(x => x.ModifyTime).First();
                    item.LastModifityTime = reserveLockStatus.ModifyTime;
                    item.CouponReservationDate = reserveLockStatus.CouponReservationDate;
                }

                item.ReserveCouponStatus = ReserveCouponStatus.Valid;
                if (couponInfo.IsDealReserveLock)
                {
                    if (couponInfo.IsReservatoinLock)
                    {
                        item.ReserveCouponStatus = ReserveCouponStatus.Lock;
                    }
                    else if (!couponInfo.IsReservatoinLock && bookingSystemReserveLockStatusLogList.Any())
                    {
                        item.ReserveCouponStatus = ReserveCouponStatus.UnLock;
                    }
                    else
                    {
                        if (couponInfo.CouponStatus == (int)TrustStatus.Returned ||
                            couponInfo.CouponStatus == (int)TrustStatus.Refunded)
                        {
                            item.ReserveCouponStatus = ReserveCouponStatus.Returned;
                        }
                    }
                }
                else
                {
                    item.ReserveCouponStatus = ReserveCouponStatus.Invalid;
                }
                result.Add(item);
            }

            #endregion PiinLifeCoupon

            return result;
        }

        public static List<ReserveCouponInfo> GetAccessibleReserveLockCoupon(bool is17LifeEmployee, int couponLockSearchType, List<IVbsVendorAce> allowedDealUnits, DateTime searchStartTime, DateTime searchEndTime)
        {
            IEnumerable<ReserveCouponData> pponCouponInfo;
            IEnumerable<ReserveCouponData> piinCouponInfo;

            var result = new List<ReserveCouponInfo>();

            #region initialize ReserveCouponData

            if (is17LifeEmployee)
            {
                pponCouponInfo = bsp.GetReserveLockCouponWithPpon(couponLockSearchType, searchStartTime, searchEndTime);
                piinCouponInfo = bsp.GetReserveLockCouponWithPiinLife(couponLockSearchType, searchStartTime, searchEndTime);
            }
            else
            {
                if (!TryGetPponReserveLockCouponInfo(couponLockSearchType, searchStartTime, searchEndTime, allowedDealUnits, out pponCouponInfo))
                {
                    pponCouponInfo = new List<ReserveCouponData>();

                }
                if (!TryGetPiinReserveLockCouponInfo(couponLockSearchType, searchStartTime, searchEndTime, allowedDealUnits, out piinCouponInfo))
                {
                    piinCouponInfo = new List<ReserveCouponData>();

                }

            }

            #endregion initialize ReserveCouponData

            #region PponCoupon

            foreach (var couponInfo in pponCouponInfo)
            {
                var item = new ReserveCouponInfo();
                item.CouponId = couponInfo.CouponId;
                item.CouponSequence = couponInfo.CouponSequence;
                item.CouponUsage = couponInfo.CouponUsage;
                item.EventName = couponInfo.EventName;
                item.MemberName = VerificationFacade.SetPayerNameHidden(couponInfo.MemberName);
                item.DealUniqueId = couponInfo.DealUniqueId;

                item.IsReservatoinLock = couponInfo.IsReservatoinLock;

                item.DealType = couponInfo.DealType;
                var bookingSystemReserveLockStatusLogList =
                    bsp.BookingSystemReserveLockStatusLogListGet(
                      new string[] { BookingSystemReserveLockStatusLog.Columns.DealType + "=" + (int)couponInfo.DealType, BookingSystemReserveLockStatusLog.Columns.CouponId + "=" + couponInfo.CouponId }
                    ).ToList();

                if (bookingSystemReserveLockStatusLogList.Any())
                {
                    var reserveLockStatus = bookingSystemReserveLockStatusLogList.OrderByDescending(x => x.ModifyTime).First();
                    item.LastModifityTime = reserveLockStatus.ModifyTime;
                    item.CouponReservationDate = reserveLockStatus.CouponReservationDate;
                }

                item.ReserveCouponStatus = ReserveCouponStatus.Valid;
                if (couponInfo.IsDealReserveLock)
                {
                    if (couponInfo.IsReservatoinLock)
                    {
                        item.ReserveCouponStatus = ReserveCouponStatus.Lock;
                    }
                    else
                    {
                        if (!couponInfo.IsReservatoinLock && bookingSystemReserveLockStatusLogList.Any())
                        {
                            item.ReserveCouponStatus = ReserveCouponStatus.UnLock;
                        }

                        switch (couponInfo.CouponStatus)
                        {
                            case (int)TrustStatus.Refunded:
                            case (int)TrustStatus.Returned:
                                item.ReserveCouponStatus = ReserveCouponStatus.Returned;
                                break;
                            case (int)TrustStatus.Trusted:
                            case (int)TrustStatus.Initial:
                                if (couponInfo.OrderGuid.HasValue)
                                {
                                    var returningCtlog = mp.PponRefundingCashTrustLogGetListByOrderGuid(couponInfo.OrderGuid.Value)
                                                            .FirstOrDefault(x => x.CouponId == couponInfo.CouponId);

                                    if (returningCtlog != null)
                                    {
                                        item.ReserveCouponStatus = ReserveCouponStatus.Returning;
                                    }
                                }
                                break;
                        }
                    }
                }
                else
                {
                    item.ReserveCouponStatus = ReserveCouponStatus.Invalid;
                }
                result.Add(item);
            }

            #endregion PponCoupon

            #region PiinLifeCoupon

            foreach (var couponInfo in piinCouponInfo)
            {
                var item = new ReserveCouponInfo();
                item.CouponId = couponInfo.CouponId;
                item.CouponUsage = couponInfo.CouponUsage;
                item.CouponSequence = couponInfo.CouponSequence;
                item.EventName = couponInfo.EventName;
                item.MemberName = VerificationFacade.SetPayerNameHidden(couponInfo.MemberName);
                item.DealUniqueId = couponInfo.DealUniqueId;

                item.IsReservatoinLock = couponInfo.IsReservatoinLock;

                item.DealType = couponInfo.DealType;
                var bookingSystemReserveLockStatusLogList =
                    bsp.BookingSystemReserveLockStatusLogListGet(
                      new string[] { BookingSystemReserveLockStatusLog.Columns.DealType + "=" + (int)couponInfo.DealType, BookingSystemReserveLockStatusLog.Columns.CouponId + "=" + couponInfo.CouponId }
                    ).ToList();

                if (bookingSystemReserveLockStatusLogList.Any())
                {
                    var reserveLockStatus = bookingSystemReserveLockStatusLogList.OrderByDescending(x => x.ModifyTime).First();
                    item.LastModifityTime = reserveLockStatus.ModifyTime;
                    item.CouponReservationDate = reserveLockStatus.CouponReservationDate;
                }

                item.ReserveCouponStatus = ReserveCouponStatus.Valid;
                if (couponInfo.IsDealReserveLock)
                {
                    if (couponInfo.IsReservatoinLock)
                    {
                        item.ReserveCouponStatus = ReserveCouponStatus.Lock;
                    }
                    else if (!couponInfo.IsReservatoinLock && bookingSystemReserveLockStatusLogList.Any())
                    {
                        item.ReserveCouponStatus = ReserveCouponStatus.UnLock;
                    }
                    else
                    {
                        if (couponInfo.CouponStatus == (int)TrustStatus.Returned ||
                            couponInfo.CouponStatus == (int)TrustStatus.Refunded)
                        {
                            item.ReserveCouponStatus = ReserveCouponStatus.Returned;
                        }
                    }
                }
                else
                {
                    item.ReserveCouponStatus = ReserveCouponStatus.Invalid;
                }
                result.Add(item);
            }

            #endregion PiinLifeCoupon

            return result;
        }



        private static bool TryGetPponReserveCouponInfo(string firstCouponSequence, string secoundCouponSequence,
                                                        List<IVbsVendorAce> allowedDealUnits,
                                                        out IEnumerable<ReserveCouponData> pponCouponInfo)
        {
            List<Guid> bids = allowedDealUnits.Where(x => x.IsPponDeal.Value).Select(x => x.MerchandiseGuid.Value).Distinct().ToList();

            List<ReserveCouponData> tempCoupons = bsp.GetReserveCouponWithPpon(firstCouponSequence, secoundCouponSequence).Where(x => bids.Contains(x.DealGuid)).ToList();

            IEnumerable<ReserveCouponData> result = tempCoupons.Where(x => false).ToList();   //initialize empty result

            List<Guid> Bids = tempCoupons.Select(x => x.DealGuid).ToList();
            ViewPponDealCollection vpds = pp.ViewPponDealGetByBusinessHourGuidList(Bids);

            foreach (var ace in allowedDealUnits)
            {                
                Guid merchandiseGuid = ace.MerchandiseGuid.Value;
                Guid? storeGuid = ace.StoreGuid;

                if (ace.IsRestrictedStore)
                {
                    //限定分店使用
                    if (!storeGuid.HasValue || storeGuid == Guid.Empty)
                    {
                        continue;
                    }
                    result = result.Concat(tempCoupons.Where(
                    x => x.DealGuid == merchandiseGuid
                    && x.StoreGuid == storeGuid.Value));
                }
                else
                {
                    //通用券
                    var coupon = tempCoupons.Where(x => x.DealGuid == merchandiseGuid);
                    if (!coupon.All(x => result.Contains(x)))
                    { 
                        result = result.Concat(coupon);
                    }
                }
            }


            if (result.Any())
            {
                pponCouponInfo = result;
                return true;
            }

            pponCouponInfo = null;  //when result.Count() == 0, CopyToDataTable() throws exception because of the unknown DataTable schema
            return false;
        }

        private static bool TryGetPiinReserveCouponInfo(string firstCouponSequence, string secoundCouponSequence,
                                                        List<IVbsVendorAce> allowedDealUnits,
                                                        out IEnumerable<ReserveCouponData> piinCouponInfo)
        {

            List<Guid> pguids = allowedDealUnits.Where(x => x.IsPiinDeal.Value).Select(x => x.MerchandiseGuid.Value).Distinct().ToList();

            IEnumerable<ReserveCouponData> tempCoupons =
                bsp.GetReserveCouponWithPiinLife(firstCouponSequence, secoundCouponSequence).Where(x => pguids.Contains(x.DealGuid));

            IEnumerable<ReserveCouponData> result = tempCoupons.Where(x => false).ToList();   //initialize empty result

            foreach (var ace in allowedDealUnits)
            {
                Guid merchandiseGuid = ace.MerchandiseGuid.Value;
                Guid? storeGuid = ace.StoreGuid;
                if (!storeGuid.HasValue || storeGuid == Guid.Empty)
                {
                    continue;
                }
                else
                {                   
                    result = result.Concat(tempCoupons.Where(
                        x => x.DealGuid == merchandiseGuid
                        && x.StoreGuid == storeGuid.Value));
                }
            }

            if (result.Any())
            {
                piinCouponInfo = result;
                return true;
            }

            piinCouponInfo = null;  //when result.Count() == 0, CopyToDataTable() throws exception because of the unknown DataTable schema
            return false;
        }

        private static bool TryGetPponReserveLockCouponInfo(int couponLockSearchType, DateTime searchStartTime, DateTime searchEndTime,
                                                List<IVbsVendorAce> allowedDealUnits,
                                                out IEnumerable<ReserveCouponData> pponCouponInfo)
        {
            List<Guid> bids = allowedDealUnits.Where(x => x.IsPponDeal.Value).Select(x => x.MerchandiseGuid.Value).Distinct().ToList();

            List<ReserveCouponData> tempCoupons = bsp.GetReserveLockCouponWithPpon(couponLockSearchType, searchStartTime, searchEndTime).Where(x => bids.Contains(x.DealGuid)).ToList();

            IEnumerable<ReserveCouponData> result = tempCoupons.Where(x => false).ToList();   //initialize empty result

            foreach (var ace in allowedDealUnits)
            {
                Guid merchandiseGuid = ace.MerchandiseGuid.Value;
                Guid? storeGuid = ace.StoreGuid;
                if (ace.IsRestrictedStore)
                {
                    //限定分店使用
                    if (!storeGuid.HasValue || storeGuid == Guid.Empty)
                    {
                        continue;
                    }
                    result = result.Concat(tempCoupons.Where(
                    x => x.DealGuid == merchandiseGuid
                    && x.StoreGuid == storeGuid.Value));
                }
                else
                {
                    //通用券                    
                    var coupon = tempCoupons.Where(x => x.DealGuid == merchandiseGuid);
                    if (!coupon.All(x => result.Contains(x)))
                    {
                        result = result.Concat(coupon);
                    }
                }
            }


            if (result.Any())
            {
                pponCouponInfo = result;
                return true;
            }

            pponCouponInfo = null;  //when result.Count() == 0, CopyToDataTable() throws exception because of the unknown DataTable schema
            return false;
        }

        private static bool TryGetPiinReserveLockCouponInfo(int couponLockSearchType, DateTime searchStartTime, DateTime searchEndTime,
                                                        List<IVbsVendorAce> allowedDealUnits,
                                                        out IEnumerable<ReserveCouponData> piinCouponInfo)
        {

            List<Guid> pguids = allowedDealUnits.Where(x => x.IsPiinDeal.Value).Select(x => x.MerchandiseGuid.Value).Distinct().ToList();

            IEnumerable<ReserveCouponData> tempCoupons =
                bsp.GetReserveLockCouponWithPiinLife(couponLockSearchType, searchStartTime, searchEndTime).Where(x => pguids.Contains(x.DealGuid));

            IEnumerable<ReserveCouponData> result = tempCoupons.Where(x => false).ToList();   //initialize empty result

            foreach (var ace in allowedDealUnits)
            {
                Guid merchandiseGuid = ace.MerchandiseGuid.Value;
                Guid? storeGuid = ace.StoreGuid;
                if (!storeGuid.HasValue || storeGuid == Guid.Empty)
                {
                    continue;
                }
                else
                {
                    result = result.Concat(tempCoupons.Where(
                        x => x.DealGuid == merchandiseGuid
                        && x.StoreGuid == storeGuid.Value));
                }
            }

            if (result.Any())
            {
                piinCouponInfo = result;
                return true;
            }

            piinCouponInfo = null;  //when result.Count() == 0, CopyToDataTable() throws exception because of the unknown DataTable schema
            return false;
        }

        #endregion 商家系統 - 訂房鎖定

        public static string GetApiKey(BookingSystemServiceName serviceName)
        {
            return bsp.BookingSystemApiGetApiKeyById((int)serviceName);
        }

        #region E-Mail

        public static void SendReservationSuccessMail(string toMail, ReservationSuccessMailInfomation mailInfo)
        {
            MailMessage msg = new MailMessage();
            var template = TemplateFactory.Instance().GetTemplate<ReservationSuccessMail>();

            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;

            template.MemberName = mailInfo.MemberName;
            template.ReservationDate = mailInfo.ReservationDate.ToString("yyyy/MM/dd");
            template.ReservationDateOfWeek = ChineseByEnWeek(mailInfo.ReservationDate);
            template.ReservationTimeSlot = mailInfo.ReservationTimeSlot.ToString("HH:mm");
            template.NumberOfPeople = mailInfo.NumberOfPeople;
            template.MemberEmail = mailInfo.MemberEmail;
            template.ContactName = mailInfo.ContactName;
            template.ContactNumber = mailInfo.ContactNumber;
            template.Remark = mailInfo.Remark;
            template.CouponSequence = string.Join(", ", mailInfo.CouponsSequence);
            template.IsConcurCoupon = !string.IsNullOrEmpty(mailInfo.CouponInfo);
            template.CouponInfo = mailInfo.CouponInfo;
            template.CouponLink = mailInfo.CouponLink;
            template.SellerName = mailInfo.SellerName;
            template.StoreName = mailInfo.StoreName;
            template.StoreAddress = mailInfo.StoreAddress;

            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(toMail);
            msg.Subject =  "預約成功(" + mailInfo.SellerName + ")";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }

        public static void SendTravelSuccessMail(string toMail, TravelSuccessMailInfomation mailInfo)
        {
            MailMessage msg = new MailMessage();
            var template = TemplateFactory.Instance().GetTemplate<TravelSuccessMail>();

            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;

            template.MemberName = mailInfo.MemberName;
            template.ReservationDate = mailInfo.ReservationDate.ToString("yyyy/MM/dd");
            template.ReservationDateOfWeek = ChineseByEnWeek(mailInfo.ReservationDate);
            template.NumberOfPeople = mailInfo.NumberOfPeople;
            template.MemberEmail = mailInfo.MemberEmail;
            template.ContactName = mailInfo.ContactName;
            template.ContactNumber = mailInfo.ContactNumber;
            template.Remark = mailInfo.Remark;
            template.CouponSequence = string.Join(", ", mailInfo.CouponsSequence);
            template.IsConcurCoupon = !string.IsNullOrEmpty(mailInfo.CouponInfo);
            template.CouponInfo = mailInfo.CouponInfo;
            template.CouponLink = mailInfo.CouponLink;
            template.SellerName = mailInfo.SellerName;
            template.StoreName = mailInfo.StoreName;
            template.StoreAddress = mailInfo.StoreAddress;

            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(toMail);
            msg.Subject = "預約成功(" + mailInfo.SellerName + ")";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);

        }

        public static void SendReservationCancelMail(string toMail, ReservationCancelMailInfomation mailInfo)
        {
            MailMessage msg = new MailMessage();
            var template = TemplateFactory.Instance().GetTemplate<ReservationCancelMail>();
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;
            template.MemberName = mailInfo.MemberName;
            template.SellerName = mailInfo.SellerName;
            template.StoreName = mailInfo.StoreName;

            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(toMail);
            msg.Subject = "預約已取消(" + mailInfo.SellerName + ")";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);

        }

        public static void SendReservationRemindMail(string toMail, ReservationRemindMailInfomation mailInfo)
        {
            MailMessage msg = new MailMessage();
            var template = TemplateFactory.Instance().GetTemplate<ReservationRemindMail>();
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;
            template.MemberName = mailInfo.MemberName;
            template.ReservationDate = mailInfo.ReservationDate.ToString("yyyy/MM/dd");
            template.ReservationDateOfWeek = ChineseByEnWeek(mailInfo.ReservationDate);
            template.ReservationTimeSlot = mailInfo.ReservationTimeSlot.ToString("HH:mm");
            template.NumberOfPeople = mailInfo.NumberOfPeople;
            template.MemberEmail = mailInfo.MemberEmail;
            template.ContactName = mailInfo.ContactName;
            template.ContactNumber = mailInfo.ContactNumber;
            template.Remark = mailInfo.Remark;
            template.CouponSequence = string.Join(", ", mailInfo.CouponsSequence);
            template.IsConcurCoupon = !string.IsNullOrEmpty(mailInfo.CouponInfo);
            template.CouponInfo = mailInfo.CouponInfo;
            template.CouponLink = mailInfo.CouponLink;
            template.SellerName = mailInfo.SellerName;
            template.StoreName = mailInfo.StoreName;
            template.StoreAddress = mailInfo.StoreAddress;

            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(toMail);
            msg.Subject = "預約提醒(" + mailInfo.SellerName + ")";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);

        }

        public static void SendTravelRemindMail(string toMail, TravelRemindMailInfomation mailInfo)
        {
            MailMessage msg = new MailMessage();
            var template = TemplateFactory.Instance().GetTemplate<TravelRemindMail>();
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;
            template.MemberName = mailInfo.MemberName;
            template.ReservationDate = mailInfo.ReservationDate.ToString("yyyy/MM/dd");
            template.ReservationDateOfWeek = ChineseByEnWeek(mailInfo.ReservationDate);
            template.NumberOfPeople = mailInfo.NumberOfPeople;
            template.MemberEmail = mailInfo.MemberEmail;
            template.ContactName = mailInfo.ContactName;
            template.ContactNumber = mailInfo.ContactNumber;
            template.Remark = mailInfo.Remark;
            template.CouponSequence = string.Join(", ", mailInfo.CouponsSequence);
            template.IsConcurCoupon = !string.IsNullOrEmpty(mailInfo.CouponInfo);
            template.CouponInfo = mailInfo.CouponInfo;
            template.CouponLink = mailInfo.CouponLink;
            template.SellerName = mailInfo.SellerName;
            template.StoreName = mailInfo.StoreName;
            template.StoreAddress = mailInfo.StoreAddress;

            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(toMail);
            msg.Subject = "預約提醒(" + mailInfo.SellerName + ")";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);

        }

        public static void SendSettingChangeLogMail(string toMail, BookingSettingChangeLogMailInfomation mailInfo)
        {
            MailMessage msg = new MailMessage();
            var template = TemplateFactory.Instance().GetTemplate<BookingSettingChangeLogMail>();
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;
            template.VbsAccount = mailInfo.VbsAccount;
            template.SellerName = mailInfo.SellerName;
            template.StoreName = mailInfo.StoreName;
            template.ModifyTime = mailInfo.ModifyTime.ToString("yyyy/MM/dd HH:mm:ss");

            var logs = new List<string>();
            foreach (var log in mailInfo.ChangeSettingLogs.Select(x => x.Key + "：" + x.Value))
            {
                logs.Add(log);
            }

            template.ChangeSettingLogs = string.Join("<br />", logs);

            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(toMail);
            msg.Subject = "商家系統更改預約資料(" + mailInfo.SellerName + mailInfo.StoreName + ")";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);

        }

        #endregion E-Mail

        public static bool IsReservationLock(int couponId)
        {
            BookingSystemReserveLockStatusLogCollection bookingLogs = bsp.BookingSystemReserveLockStatusLogGetByCouponId(couponId, (int)ReserveCouponStatus.Lock);
            return bookingLogs.Any();
        }

        private static string ChineseByEnWeek(DateTime dt)
        {
            string result;
            switch (dt.DayOfWeek.ToString())
            {
                case "Monday":
                    result = "一";
                    break;
                case "Tuesday":
                    result = "二";
                    break;
                case "Wednesday":
                    result = "三";
                    break;
                case "Thursday":
                    result = "四";
                    break;
                case "Friday":
                    result = "五";
                    break;
                case "Saturday":
                    result = "六";
                    break;
                case "Sunday":
                    result = "日";
                    break;
                default:
                    result = "";
                    break;
            }
            return result;
        }

        public static bool CheckDayTypeIsOpen(BookingSystemStore store, DateTime queryDate)
        {
            //check assignDay
            var bookingSystemDate = bsp.BookingSystemDateGet(store.BookingId, DayType.AssignDay, queryDate.Date);
            if (bookingSystemDate.IsLoaded)
            {
                return bookingSystemDate.IsOpening;
            }
            //check DayOfWeek

            var DayOfWeekBookingSystemDate = bsp.BookingSystemDateGet(store.BookingId, GetBookingDayType(queryDate.Date), null);
            if (DayOfWeekBookingSystemDate.IsLoaded)
            {
                return DayOfWeekBookingSystemDate.IsOpening;
            }


            //check defaultSetting
            switch (bsp.DayTypeParse(queryDate))
            {
                case DayType.MON:
                    return store.Mon;
                case DayType.TUE:
                    return store.Tue;
                case DayType.WED:
                    return store.Wed;
                case DayType.THU:
                    return store.Thu;
                case DayType.FRI:
                    return store.Fri;
                case DayType.SAT:
                    return store.Sat;
                case DayType.SUN:
                    return store.Sun;
                default:
                    return true;
            }
        }

        public static DayType GetBookingDayType(DateTime date)
        {
            DayType day;
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    day = DayType.MON;
                    break;
                case DayOfWeek.Tuesday:
                    day = DayType.TUE;
                    break;
                case DayOfWeek.Wednesday:
                    day = DayType.WED;
                    break;
                case DayOfWeek.Thursday:
                    day = DayType.THU;
                    break;
                case DayOfWeek.Friday:
                    day = DayType.FRI;
                    break;
                case DayOfWeek.Saturday:
                    day = DayType.SAT;
                    break;
                case DayOfWeek.Sunday:
                    day = DayType.SUN;
                    break;
                default:
                    day = DayType.AssignDay;
                    break;
            }
            return day;
        }

        private static IEnumerable<DayType> GetBookingDayType(BookingSystemStore store)
        {
            var dayTypes = new List<DayType>();

            if (store.Sun)
            {
                dayTypes.Add(DayType.SUN);
            }
            if (store.Mon)
            {
                dayTypes.Add(DayType.MON);
            }
            if (store.Tue)
            {
                dayTypes.Add(DayType.TUE);
            }
            if (store.Wed)
            {
                dayTypes.Add(DayType.WED);
            }
            if (store.Thu)
            {
                dayTypes.Add(DayType.THU);
            }
            if (store.Fri)
            {
                dayTypes.Add(DayType.FRI);
            }
            if (store.Sat)
            {
                dayTypes.Add(DayType.SAT);
            }

            return dayTypes;
        }

    }
}

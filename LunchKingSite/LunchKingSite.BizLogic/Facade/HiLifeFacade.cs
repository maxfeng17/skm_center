﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models.FamiPort;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.DataOrm;
using Newtonsoft.Json.Linq;
using System.IO;
using Amazon.SimpleEmail.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core.ModelCustom;
using Newtonsoft.Json;
using HiLifePincode = LunchKingSite.Core.Models.Entities.HiLifePincode;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;

namespace LunchKingSite.BizLogic.Facade
{
    public class HiLifeFacade
    {
        private static IHiLifeProvider hiLife = ProviderFactory.Instance().GetProvider<IHiLifeProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(HiLifeFacade).Name);
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();


        public static ApiResult PincodeCheckAndVerify(string pincode, HiLifeVerifyLog entity)
        {
            ApiResult result = new ApiResult();
            var pincodeData = hiLife.GetHiLifePincode(pincode);
            if (pincodeData == null)
            {
                entity.IsSuccess = false;
                SaveVerifyLog(entity, "not found pincode");
                result.Code = ApiResultCode.InvalidCoupon;
                result.Message = "找不到該pincode";
                return result;
            }
            //塞入訂單guid
            entity.OrderGuid = pincodeData.OrderGuid;

            if (pincodeData.ReturnStatus == (byte)HiLifeReturnStatus.Success)
            {
                entity.IsSuccess = false;
                SaveVerifyLog(entity, "pincode id returned");
                result.Code = ApiResultCode.InvalidCoupon;
                result.Message = "該pincode已被退貨";
                return result;
            }

            if (pincodeData.IsVerified)
            {
                entity.IsSuccess = true;
                SaveVerifyLog(entity, "pincode id verified");
                result.Code = ApiResultCode.CouponUsed;
                result.Message = "此pincode已被使用";
                return result;
            }

           

            var pez = hiLife.GetPeztemp(pincodeData.PeztempId);
            if (pez == null)
            {
                entity.IsSuccess = false;
                SaveVerifyLog(entity, "not found peztemp");
                result.Code = ApiResultCode.InvalidCoupon;
                return result;
            }

            Guid bid = Guid.Empty;
            Guid.TryParse(pez.Bid.ToString(), out bid);
            BusinessHour bh = sp.BusinessHourGet(bid);
            if (!(bh.BusinessHourDeliverTimeS <= DateTime.Now && bh.BusinessHourDeliverTimeE >= DateTime.Now))
            {
                entity.IsSuccess = false;
                SaveVerifyLog(entity, "invalid interval");
                result.Code = ApiResultCode.InvalidInterval;
                return result;
            }

            string remark;
            //17life核銷
            if (OrderFacade.SetHiLifePincodeVerify(pincodeData, pez, Helper.GetClientIP(), entity.StoreCode + "_" + entity.MachineCode, out remark))
            {
                entity.IsSuccess = true;
                SaveVerifyLog(entity, string.Empty);
                result.Code = ApiResultCode.Success;
            }
            else
            {
                entity.IsSuccess = false;
                SaveVerifyLog(entity, "verify fail");
                result.Code = ApiResultCode.CouponPartialFail;
            }

            return result;
        }

        public static void SaveParseErrorLog(string remark, Guid bathGuid, string input,Guid? orderGuid,string batchFileName, HiLifeVerifyType verifyType)
        {
            HiLifeVerifyLog entity = new HiLifeVerifyLog();
            entity.IsSuccess = false;
            entity.HilifeInput = input;
            entity.BatchId = bathGuid;
            entity.Remark = remark;
            entity.VerifyType = (int)verifyType;
            entity.OrderGuid = orderGuid;
            entity.BatchFileName = batchFileName;
            entity.CreateTime = DateTime.Now;
            hiLife.SetHiLifeVerifyLog(new List<HiLifeVerifyLog>() { entity });

            logger.Info(remark);
        }

        public static void SaveVerifyLog(HiLifeVerifyLog entity, string errorRemark)
        {
            if (string.IsNullOrEmpty(errorRemark))
            {
                entity.Remark = "verify ok";
            }
            else
            {
                entity.Remark = errorRemark;
                logger.Info(errorRemark);//有錯才log
            }
            entity.CreateTime = DateTime.Now;
            hiLife.SetHiLifeVerifyLog(new List<HiLifeVerifyLog>() { entity });
        }


        public static string WebGetRequest(string action , string para)
        {
            ApiResult apiResult = new ApiResult();
            var js = new JsonSerializer();
            try
            {
                string targetUrl = config.HiLifeTransferServerDomain + action + para;

                HttpWebRequest request = WebRequest.Create(targetUrl) as HttpWebRequest;
                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Timeout = 60000;
                
                string result = "";

                // 取得回應資料
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                }
                
                return result;
            }
            catch (Exception ex)
            {
                apiResult.Code = ApiResultCode.Error;
                apiResult.Message = ex.ToString();

                return js.Serialize(apiResult);


            }
            
            
        }

        public static string ApiRequest(string action, string jsonData)
        {
            byte[] dataBytes = Encoding.UTF8.GetBytes(jsonData);
            string apiUrl = config.HiLifeTransferServerDomain + action;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(apiUrl);
            request.ContentType = "text/html";
            request.ContentLength = dataBytes.Length;
            request.Method = WebRequestMethods.Http.Post;

            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(dataBytes, 0, dataBytes.Length);
                requestStream.Flush();
            }

            string result = "";
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        result = reader.ReadToEnd();
                    }
                }
            }
            return result;
        }


        /// <summary>
        ///  註銷Pincode (17Life call 17中繼Server)
        /// </summary>
        /// <returns></returns>
        public static bool ReturnPincode(List<HiLifePincode> pincodes, Order o, HiLifeReturnType returnType = HiLifeReturnType.UserReturn)
        {
            string returnedDate = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            string headContent = string.Empty;

            //log
            DateTime requestTime = DateTime.Now;
            List<HiLifeReturnLog> logList = new List<HiLifeReturnLog>();
            Guid batchId = Guid.NewGuid();
            foreach (var p in pincodes)
            {
                HiLifeReturnLog rlog = new HiLifeReturnLog
                {
                    CreateTime = requestTime,
                    OrderGuid = o.Guid,
                    Pincode = p.Pincode,
                    ReturnDate = returnedDate,
                    BatchId = batchId,
                    IpAddress = Helper.GetClientIP(),
                    ReturnType = (byte)returnType
                };
                logList.Add(rlog);
                headContent += p.Pincode.PadRight(20,'_') + returnedDate + "|";
            }
            

            try
            {
                if (config.EnableHiLifeApiTransfer)
                {                    

                    var js = new JsonSerializer();
                    List<ReturnDetail> result = new List<ReturnDetail>();
                    string requestResult = string.Empty;

                    int idx = 0;
                    while (idx <= headContent.Split('|').Count() - 1)
                    {
                        int batchLimit = 50;//hiLife退貨API串參數約65個就會爆掉

                        string tempHeadContent = string.Join("|", headContent.Split('|').Skip(idx).Take(batchLimit));
                        if (tempHeadContent.Substring(tempHeadContent.Length - 1, 1) != "|")
                        {
                            tempHeadContent = tempHeadContent + "|";
                        }

                        var tempResult = WebGetRequest("api/HiLife/WebApi_discard", "?Head=" + tempHeadContent);
                        requestResult += tempResult;

                        ApiHiLifeReturnResult apiResult = js.Deserialize<ApiHiLifeReturnResult>(tempResult);

                        if (apiResult.State == "0000")
                        {
                            result.AddRange(apiResult.PingCodeList);
                            idx += batchLimit;
                        }
                        else
                        {
                            pincodes.ForEach(x => x.ReturnStatus = (int)HiLifeReturnStatus.Fail);
                            hiLife.UpdateHiLifePincodeReturnStatus(pincodes);
                            logger.ErrorFormat("HiLifeFacade.Error.ApiResult:{0}", apiResult.Message);
                            return false;
                        }
                    }

                    //測試用
                    //string requestResult = "{\"State\":\"0000\",\"PingCodeList\":[{\"Code\":\"0053822351745DAA281\",\"Active\":\"Y\",\"Reason\":\"OK\"},{\"Code\":\"005382235460F17F276\",\"Active\":\"Y\",\"Reason\":\"OK\"}],\"Message\":\"OK\"}";

                    //save log
                    result.ForEach(r =>
                    {
                        var rlog = logList.FirstOrDefault(p => p.Pincode == r.Code);
                        if (rlog != null)
                        {
                            rlog.Active = r.Active;
                            rlog.Pincode = r.Code;
                            rlog.Reason = r.Reason;
                            rlog.HilifeReturn = requestResult;
                        }
                    });
                    hiLife.AddHiLifeNetReturnLog(logList);


                    //save pincode fail status 
                    if (result.Any(p => p.Active == HiLifeReturnReplyCode.N.ToString())) //只要有任何一筆失敗就全失敗
                    {
                        result.ForEach(r =>
                        {
                            var apincode = pincodes.FirstOrDefault(p => p.Pincode == r.Code);
                            if (apincode != null)
                            {
                                if (r.Active == HiLifeReturnReplyCode.N.ToString())
                                {
                                    //同一批序號內只要有一筆fail 其餘都不會被進行註銷 (仍是以HiLifeReturnStatus.Default顯示)
                                    //其中失敗的一筆pincode.returnStatus=Fail + returnLog.Active=N & Resaon
                                    //其他筆則是pincode.returnStatus=Default + returnLog.Active=Y & Resaon (代表是可進行註銷，只是整批註銷序號內有其中一筆失敗的狀況)
                                    apincode.ReturnStatus = (byte)HiLifeReturnStatus.Fail;
                                    logger.ErrorFormat("HiLifeFacade.Error.API回傳退貨失敗:order_guid:{0},pincode:{1}", apincode.OrderGuid, apincode.Pincode);
                                }
                                else if (r.Active == HiLifeReturnReplyCode.Y.ToString())
                                {
                                    apincode.ReturnStatus = (byte)HiLifeReturnStatus.Default;
                                }
                            }
                        });
                        hiLife.UpdateHiLifePincodeReturnStatus(pincodes);
                        return false;
                    }
                    else
                    {
                        //save pincode Success status 
                        result.ForEach(r =>
                        {
                            var apincode = pincodes.FirstOrDefault(p => p.Pincode == r.Code);
                            if (apincode != null)
                            {
                                apincode.ReturnStatus = (byte)HiLifeReturnStatus.Success;
                            }
                        });
                        hiLife.UpdateHiLifePincodeReturnStatus(pincodes);
                        return true;
                    }
                   
                }
                else
                {
                    //沒開啟串接API直接當成功
                    foreach (var rlog in logList)
                    {
                        rlog.Active = HiLifeReturnReplyCode.Y.ToString();
                        rlog.Reason = "OK";
                        rlog.HilifeReturn = "{\"STATE\":\"0000\",\"MESSAGE\":\"OK\",\"PingCodeList\":[{\"Code\":\"" + rlog.Pincode + "\",\"Active\":\"" + HiLifeReturnReplyCode.Y.ToString() + "\",\"Reason\":\"OK\"}]}";
                    }
                    hiLife.AddHiLifeNetReturnLog(logList);
                    foreach (var p in pincodes)
                    {
                        p.ReturnStatus = (byte)HiLifeReturnStatus.Success;
                    }
                    hiLife.UpdateHiLifePincodeReturnStatus(pincodes);
                    return true;
                }

                
            }
            catch (Exception ex)
            {
                pincodes.ForEach(x => x.ReturnStatus = (int)HiLifeReturnStatus.Fail);
                hiLife.UpdateHiLifePincodeReturnStatus(pincodes);
                logger.ErrorFormat("HiLifeFacade.ReturnPincode:{0}, {1}", ex.Message, ex.StackTrace);
                return false;
            }
        }
        
        /// <summary>
        /// 購買時紀錄pincode資料
        /// </summary>
        /// <param name="vpd"></param>
        /// <param name="o"></param>
        /// <param name="qty"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool MakeHiLifePincode(Order o, int qty, int userId)
        {
            try
            {
                // pezcode.count=coupon.count=qty
                var couponList = hiLife.GetViewCouponPeztempList(o.Guid);
                if (couponList.Any() && couponList.Count() == qty)
                {
                    //BatchId
                    Guid batchId = Guid.NewGuid();
                    List<HiLifePincode> pincodeList = new List<HiLifePincode>();
                    foreach (var c in couponList)
                    {
                        HiLifePincode entity = new HiLifePincode
                        {
                            OrderGuid = o.Guid,
                            UserId = userId,
                            ModifyTime = DateTime.Now,
                            CreateTime = DateTime.Now,
                            CouponId = c.Id,
                            PeztempId = c.PeztempId,
                            Pincode = c.PeztempCode,
                            BatchId = batchId
                        };
                        pincodeList.Add(entity);
                    }
                    if (hiLife.SetHiLifePincode(pincodeList))
                    {
                        return true;
                    }
                }
                else
                {
                    logger.ErrorFormat("HiLifeFacade.MakeHiLifePincode:購買pincode數與使用pincode數不一致，可能為pincode重複");
                }

            }
            catch (Exception ex)
            {
                logger.ErrorFormat("HiLifeFacade.MakeHiLifePincode:{0}, {1}", ex.Message, ex.StackTrace);
            }
            
            return false;
        }

        public static void SetHiLifeApiLog(bool isCreate, string apiName, string content, string result, HiLifeApiLog log)
        {
            JsonSerializer js = new JsonSerializer();

            if (isCreate)
            {
                //api start
                log.ApiName = apiName;
                log.Content = content;
                log.StartTime = DateTime.Now;
                log.ServerName = Environment.MachineName;
                hiLife.SetHiLifeApiLog(log);
            }
            else
            {
                //api end
                log.EndTime = DateTime.Now;
                log.Result = result;
                hiLife.SetHiLifeApiLog(log);
            }
            
        }

    }
}

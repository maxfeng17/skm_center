﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace LunchKingSite.BizLogic.Facade
{
    public class EIPFacade
    {
        protected static IMemberProvider mp;
        protected static IEIPProvider eip;
        protected static IEasyFlowProvider ef;
        protected static ISysConfProvider config;
        protected static ISystemProvider systemp;
        protected static IHumanProvider hp;

        static EIPFacade()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            eip = ProviderFactory.Instance().GetProvider<IEIPProvider>();
            ef = ProviderFactory.Instance().GetProvider<IEasyFlowProvider>();
            config = ProviderFactory.Instance().GetConfig();
            systemp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        }


        #region Employee
        /// <summary>
        /// 員工中文姓名
        /// </summary>
        /// <param name="userName">員工email</param>
        /// <returns></returns>
        public static string GetCNName(string userName)
        {
            string CNName = "";
            var emp = ef.EmployeeyGetByEmail(userName).OrderByDescending(x => x.WorkBeginDate).FirstOrDefault();
            if (emp != null)
            {
                CNName = emp.CnName;
            }

            return CNName;
        }

        /// <summary>
        /// 是否為主管
        /// </summary>
        /// <param name="userName">員工email</param>
        /// <returns></returns>
        public static bool IsDirector(string userName)
        {
            bool flag = false;
            Guid EmployeeId = GetEmployeeId(userName);
            var emp = ef.EmployeeyGet().Where(x=>x.DirectorId ==EmployeeId).FirstOrDefault() ;
            if (emp != null)
            {
                flag = true;
            }
            if (!flag)
            {
                //IT也當成主管好了
                string[] roles = Roles.GetRolesForUser(userName);
                if (roles.Contains("Administrator") || roles.Contains("Itstaff"))
                {
                    flag = true;
                }
            }
            return flag;
        }
        /// <summary>
        /// 員工中文姓名
        /// </summary>
        /// <param name="employeeId">員工guid</param>
        /// <returns></returns>
        public static string GetCNName(Guid employeeId)
        {
            string CNName = "";
            var emp = ef.EmployeeyGetByEmployeeId(employeeId);
            if (emp != null)
            {
                CNName = emp.CnName;
            }

            return CNName;
        }
        /// <summary>
        /// 員工guid
        /// </summary>
        /// <param name="userName">員工email</param>
        /// <returns></returns>
        public static Guid GetEmployeeId(string userName)
        {
            Guid EmployeeId = Guid.Empty;
            var emp = ef.EmployeeyGetByEmail(userName).OrderByDescending(x => x.WorkBeginDate).FirstOrDefault();
            if (emp != null)
            {
                EmployeeId = emp.EmployeeId;
            }

            return EmployeeId;
        }
        /// <summary>
        /// 員工email
        /// </summary>
        /// <param name="employeeId">員工guid</param>
        /// <returns></returns>
        public static string GetEmailByEmployeeId(Guid employeeId)
        {
            string EmployeeEmail = "";
            var emp = ef.EmployeeyGetByEmployeeId(employeeId);
            if (emp != null)
            {
                EmployeeEmail = emp.Email;
            }

            return EmployeeEmail;
        }

        /// <summary>
        /// 取得人員的主管
        /// </summary>
        public static Director GetDirector(string userName)
        {
            var emp = ef.EmployeeyGetByEmail(userName).OrderByDescending(x => x.WorkBeginDate).FirstOrDefault();
            if (emp != null)
            {
                string did = (emp.DirectorId ?? Guid.Empty).ToString();
                Guid directorId = Guid.Empty;
                Guid.TryParse(did, out directorId);
                var director = ef.EmployeeyGetByEmployeeId(directorId);
                if (director != null)
                {
                    return new Director
                    {
                        UserEmail = director.Email,
                        UserName = director.CnName
                    };
                }
                else
                {
                    return null;
                }
            }
            return null;
        }
        #endregion

        #region privilege
        public static List<SystemFunction> EIPSystemFunctionGet(string mail)
        {
            List<SystemFunction> OrganizationPrivilege = new List<SystemFunction>();
            List<int> FuncIds = new List<int>();
            // 取得群組賦予權限的成員清單
            foreach (string orgName in Roles.GetRolesForUser(mail))
            {
                OrgPrivilegeCollection orgPrivilegeList = hp.GetOrgPrivilegeListByOrgName(orgName);
                foreach (OrgPrivilege orgPrivilege in orgPrivilegeList)
                {
                    SystemFunction function = systemp.GetSystemFunction(orgPrivilege.FuncId);
                    if (function.IsLoaded)
                    {
                        //EIP使用
                        if (function.EipUsed ?? false)
                        {
                            if (!OrganizationPrivilege.Contains(function))
                            {
                                OrganizationPrivilege.Add(function);
                                FuncIds.Add(function.Id);
                            }
                        }
                    }
                }
            }

            // 取得成員本身賦予權限的成員清單
            ViewPrivilegeCollection userPris = hp.ViewPrivilegeGetList(mail);
            foreach (ViewPrivilege Pris in userPris)
            {
                if (FuncIds.Contains(Pris.FuncId))
                {
                    continue;
                }
                SystemFunction function = systemp.GetSystemFunction(Pris.FuncId);
                if (function.IsLoaded)
                {
                    //EIP使用
                    if (function.EipUsed ?? false)
                    {
                        if (!OrganizationPrivilege.Contains(function))
                        {
                            OrganizationPrivilege.Add(function);
                            FuncIds.Add(function.Id);
                        }
                    }
                }
            }

            return OrganizationPrivilege;
        }
        #endregion privilege

        #region Task
        public static TaskFlowCollection GetTaskRunningFlow(string userName)
        {
            return eip.TaskFlowRunningGet(GetEmployeeId(userName));
        }
        public static TaskFlowCollection GetTaskFlowApply(string userName)
        {
            return eip.TaskFlowApplyGet(GetEmployeeId(userName));
        }

        

        public static TaskFlowContent GetTaskFlowContentByProcesskId(Guid processkId)
        {
            return eip.TaskFlowContentRunningGet(processkId);
        }
        public static TaskFlowContent GetTaskFlowContentByTaskId(Guid taskId)
        {
            return eip.TaskFlowContentGetByTaskId(taskId);
        }

        /// <summary>
        /// 產生第一關的流程
        /// </summary>
        /// <param name="_ApplyId">申請人</param>
        /// <param name="_ExecId">執行人(應該是主管)</param>
        /// <param name="_RealExecId">實際執行人(應該是主管)(可能是代理人)</param>
        /// <param name="_content">表單內容</param>
        /// <returns></returns>
        public static TaskResult createProcess(int _categoryName, string _subject, 
                                                string _ApplyUser, string _ExecUser, string _RealExecUser, 
                                                string _content, string _url)
        {
            Guid ApplyUser = GetEmployeeId(_ApplyUser);
            Guid ExecUser = GetEmployeeId(_ExecUser);
            Guid RealExecUser = GetEmployeeId(_RealExecUser);

            Guid TskId = Guid.NewGuid();
            Guid PId = Guid.NewGuid();

            TaskFlow task = new TaskFlow()
            {
                ProcessId = PId,
                ApplyUser = ApplyUser,
                ExecUser = ExecUser,
                AgentUser = ExecUser,
                RealExecUser = RealExecUser,
                CategoryName = _categoryName,
                Subject = _subject,
                CreateTime = DateTime.Now,
                ModifyTime = DateTime.Now,
                EipTaskStatus = (int)EIPTaskStatus.Running,
                Url = _url + PId
            };
            eip.TaskFlowSet(task);


            TaskFlowContent taskFlowContent = new TaskFlowContent()
            {
                ProcessId = PId,
                TaskId = TskId,
                ParentTaskId = Guid.Empty,
                ApplyUser = ApplyUser,
                ExecUser = ApplyUser,
                RealExecUser = ApplyUser,
                DataContent = _content,
                CreateId = RealExecUser,
                CreateTime = DateTime.Now,
                ExecTime = DateTime.Now,
                EipTaskStatus = (int)EIPTaskStatus.Apply
            };

            eip.TaskFlowContentSet(taskFlowContent);

            return new TaskResult {
                ProcessId = PId,
                TaskId = TskId
            };
        }

        /// <summary>
        /// 產生之後的流程
        /// </summary>
        /// <param name="_ApplyId">申請人</param>
        /// <param name="_ExecId">執行人</param>
        /// <param name="_RealExecId">實際執行人(可能是代理人)</param>
        /// <param name="_content">表單內容</param>
        /// <returns></returns>
        public static TaskResult createNextProcess(TaskResult tskResult, string _ApplyUser, string _ExecUser, 
                                                    string _RealExecUser, string _content, EIPTaskStatus _EIPTaskStatus)
        {
            Guid ApplyUser = GetEmployeeId(_ApplyUser);
            Guid ExecUser = GetEmployeeId(_ExecUser);
            Guid RealExecUser = GetEmployeeId(_RealExecUser);

            Guid TaskId = Guid.NewGuid();

            TaskFlowContent taskFlowContent = new TaskFlowContent()
            {
                ProcessId = tskResult.ProcessId,
                TaskId = TaskId,
                ParentTaskId = tskResult.TaskId,
                ApplyUser = ApplyUser,
                ExecUser = ExecUser,
                DataContent = _content,
                CreateId = RealExecUser,
                CreateTime = DateTime.Now,
                EipTaskStatus = (int)_EIPTaskStatus
            };

            eip.TaskFlowContentSet(taskFlowContent);

            return new TaskResult
            {
                ProcessId = tskResult.ProcessId,
                TaskId = TaskId
            };
        }

        /// <summary>
        /// 審核流程
        /// </summary>
        /// <param name="taskId">taskId</param>
        /// <param name="_RealExecUser">實際執行人員(理論上是主管，也可能是代理人)</param>
        /// <param name="EipTaskStatus">表單狀態</param>
        /// <param name="dataContent">表單內容</param>
        /// <returns></returns>
        public static TaskResult approveProcess(Guid taskId, string _RealExecUser, EIPTaskStatus EipTaskStatus, string dataContent)
        {
            TaskFlowContent tfc = eip.TaskFlowContentGetByTaskId(taskId);
            Guid RealExecUser = GetEmployeeId(_RealExecUser);

            Guid ProcessId = Guid.Empty;
            if (tfc != null)
            {
                ProcessId = tfc.ProcessId;
                tfc.RealExecUser = RealExecUser;
                tfc.DataContent = dataContent;
                tfc.ExecTime = DateTime.Now;
                tfc.EipTaskStatus = (int)EipTaskStatus;

                eip.TaskFlowContentSet(tfc);

                TaskFlow tf = eip.TaskFlowGetByProcessId(ProcessId);

                if (tf != null)
                {
                    tf.RealExecUser = RealExecUser;
                    tf.EipTaskStatus = (int)EipTaskStatus;
                    tf.ModifyTime = DateTime.Now;

                    eip.TaskFlowSet(tf);
                }

                return new TaskResult
                {
                    ProcessId = ProcessId,
                    TaskId = tfc.TaskId
                };
            }

            return null;
        }

        #endregion
    }

    public class Director
    {
        public string UserEmail { get; set; }
        public string UserName { get; set; }
    }

    public class TaskResult
    {
        public Guid ProcessId { get; set; }
        public Guid TaskId { get; set; }
    }

    public class TaskFlowRunning
    {
        public string CategoryName { get; set; }
        public string Subject { get; set; }
        public string CreateUser { get; set; }
        public DateTime ApplyTime { get; set; }
        public string TaskStatus { get; set; }
        public string Url { get; set; }

        public string ExecUser { get; set; }
    }

    public class SystemFunctionApplyContent
    {
        public string applyId{get;set;}
        public string applyName{get;set;}
        public string applyReson{get;set;}
        public string approveDirectorName{get;set;}
        public string approveDirectorEMail{get;set;}
        public string applyTime { get; set; }
    }

    public class DocumentQueryList
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public string Url { get; set; }
        public string DocName { get; set; }
        public string CategoryName { get; set; }
        public string EditAuth { get; set; }
        public string ReadAuth { get; set; }
        public string EditAuthPerson { get; set; }
        public string ReadAuthPerson { get; set; }
        public string EditAuthName { get; set; }
        public string ReadAuthName { get; set; }
        public bool EnableEdit { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateUser { get; set; }
        public DateTime ModifyTime { get; set; }
        public string ModifyUser { get; set; }      
    }

    public class MVCPager
    {
        public int totalRecords { get; set; }
        public int currentPage { get; set; }
        public int pageSize { get; set; }
        public int startIdx { get; set; }
        public Dictionary<string, string> queryFilter { get; set; }
    }

    public class EIPLayoutData
    {
        public string UserName { get; set; }
        public string UserCNName { get; set; }
        public string MVCController { get; set; }
        public string MvcAction { get; set; }
        public string siteUrl { get; set; }
        public List<SystemFunction> OrganizationPrivilege { get; set; }
    }

}

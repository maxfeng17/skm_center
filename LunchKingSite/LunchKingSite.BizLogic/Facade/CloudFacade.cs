﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Facade
{
    public class CloudFacade
    {
        private static ILog logger = LogManager.GetLogger(typeof(CloudFacade));
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public static void DeleteDealImagesAtS3(Guid bid, DealCloudStorage storage,StringBuilder sbLog)
        {
            string mediaFolder = Helper.MapPath("~/media");
            //先異動db裡的資料
            List<DealCloudImageReference> imgrefs = pp.DealCloudImageReferenceGetList(bid);
            using (var tran = TransactionScopeBuilder.CreateReadCommitted())
            {
                foreach (var imgref in imgrefs)
                {
                    pp.DealCloudImageReferenceDelete(imgref);
                }
                storage.S3MarkDelete = false;
                storage.S3IsReady = false;
                storage.S3ContentDescription = string.Empty;
                pp.DealCloudStorageSet(storage);
                tran.Complete();
            }

            //刪掉在s3 media 的圖
            try
            {
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                List<CdnImageInfo> cdnEventImages = DealCloudImageParser.ParseEventImages(mediaFolder, vpd.EventImagePath);
                foreach (CdnImageInfo cdnInfo in cdnEventImages)
                {
                    AWSUtility.Delete(cdnInfo.CdnFolder, cdnInfo.CdnFileName);
                }
            }
            catch (Exception)
            {
                sbLog.AppendFormat("刪CDN S3上 {0} 圖檔失敗\r\n", bid);
            }
            //刪掉在s3 imageu 裡的圖
            try
            {
                foreach (var imgref in imgrefs)
                {
                    //檢查圖有沒有被其他參考使用
                    int refCount = pp.DealCloudImageReferenceGetCount(imgref.ImageKey);
                    if (refCount == 0)
                    {
                        AWSUtility.Delete(imgref.ImageKey);
                    }
                }
            }
            catch (Exception)
            {
                string imgList = string.Join(",", imgrefs.Select(t => t.ImageKey).ToArray());
                sbLog.AppendFormat("CDN S3檔次介紹的圖檔刪除發生錯誤，明細如下:{0}", imgList);
            }
        }

        public static void UploadDealImagesToS3(Guid bid, DealCloudStorage storage)
        {
            string mediaFolder = Helper.MapPath("~/media");
            string imageFolder = Helper.MapPath("~/images");

            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            List<CdnImageInfo> cdnEventImages = DealCloudImageParser.ParseEventImages(mediaFolder, vpd.EventImagePath);
            foreach (CdnImageInfo cdnInfo in cdnEventImages)
            {
                AWSUtility.Upload(cdnInfo.CdnFolder, cdnInfo.FilePath, cdnInfo.CdnFileName);
            }
            string newDesc;
            string pponDeac = ViewPponDealManager.DefaultManager.GetDealDescription(vpd);
            List<CdnImageInfo> cndContentImages = DealCloudImageParser.ParseDescription(imageFolder, pponDeac, out newDesc);
            foreach (CdnImageInfo cdnInfo in cndContentImages)
            {
                AWSUtility.Upload(cdnInfo.CdnFolder, cdnInfo.FilePath, cdnInfo.CdnFileName);
            }
            using (var tran = TransactionScopeBuilder.CreateReadCommitted())
            {
                storage.S3MarkSync = false;
                storage.S3IsReady = true;
                storage.S3ContentDescription = newDesc;
                storage.ModifyId = "";
                storage.ModifyTime = DateTime.Now;
                pp.DealCloudStorageSet(storage);

                foreach (CdnImageInfo cdnInfo in cndContentImages)
                {
                    DealCloudImageReference imgref = new DealCloudImageReference
                    {
                        ImageKey = cdnInfo.ImageKey,
                        BusinessHourGuid = bid
                    };
                    pp.DealCloudImageReferenceSet(imgref);
                }
                tran.Complete();
            }
        }

        protected class DealCloudImageParser
        {
            static Regex linkParser = new Regex(@"\b(?:https?:\/\/|www\.)[\p{L}\w.\/ %!~()\，\'\！\【\】\％\—\[\]\‧\˙\．\、\《\》\－\（\）\＆+-]+",
                RegexOptions.Compiled | RegexOptions.IgnoreCase);

            public static List<CdnImageInfo> ParseEventImages(string mediaFolder, string eventImagePath)
            {
                List<CdnImageInfo> result = new List<CdnImageInfo>();
                if (string.IsNullOrEmpty(eventImagePath))
                {
                    return result;
                }
                string[] segs = eventImagePath.Split('|');
                foreach (string seg in segs)
                {
                    string folder = seg.Split(',')[0];
                    string fileName = seg.Split(',')[1];
                    result.Add(new CdnImageInfo
                    {
                        FilePath = Path.Combine(mediaFolder, folder, fileName),
                        CdnFileName = fileName,
                        CdnFolder = "media/" + folder
                    });
                }
                return result;
            }

            public static List<CdnImageInfo> ParseDescription(string imageFolder, string description, out string newDesc)
            {
                string s3CdnUrl = string.Format("http://{0}.s3-website-ap-northeast-1.amazonaws.com/", config.AWSBucketName);

                List<CdnImageInfo> result = new List<CdnImageInfo>();
                newDesc = description;
                if (string.IsNullOrEmpty(description))
                {
                    return result;
                }
                Match m = linkParser.Match(description);
                while (m.Success)
                {
                    string url = m.Value;
                    int pos = url.IndexOf(ImageLoadBalanceFolder.ImagesU.ToString(), StringComparison.OrdinalIgnoreCase);
                    if (pos > 0)
                    {
                        string imagePath = m.Value.Substring(pos);
                        Uri baseUri = new Uri(VirtualPathUtility.AppendTrailingSlash(s3CdnUrl));
                        Uri cdnUrl = new Uri(baseUri, "images/" + imagePath.ToLower());
                        newDesc = newDesc.Replace(m.Value, cdnUrl.AbsoluteUri);

                        result.Add(new CdnImageInfo
                        {
                            CdnFolder = Helper.GetUriRequestFolder(cdnUrl),
                            CdnFileName = Helper.GetUriRequestFile(cdnUrl),
                            FilePath = Path.Combine(imageFolder, imagePath.Replace('/', '\\'))
                        });
                    }
                    m = m.NextMatch();
                }


                return result;
            }
        }

        public class CdnImageInfo
        {
            public string FilePath { get; set; }
            public string CdnFolder { get; set; }
            public string CdnFileName { get; set; }
            public string CdnBaseUrl { get; set; }

            public string ImageKey
            {
                get { return CombinePath(CdnFolder, CdnFileName); }
            }

            private static string CombinePath(params string[] paths)
            {
                return string.Join("/", paths).Replace("\\", "/");
            }
        }
    }
}

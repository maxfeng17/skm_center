﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Web;
using System.IO.Compression;
using System.Net.Cache;
using log4net;
using LunchKingSite.Core.Component.Template;
using System.Net.Mail;
using LunchKingSite.Core.Enumeration;
using System.Data;
using System.Runtime.Caching;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Models.Ppon;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using System.Transactions;

namespace LunchKingSite.BizLogic.Facade
{
    public class PromotionFacade
    {
        private static ISellerProvider _sp;
        private static IOrderProvider _op;
        private static IPponProvider _pp;
        private static ISystemProvider _ss;
        private static ISysConfProvider _config;
        private static ILocationProvider _lp;
        private static IMarketingProvider _mkp;
        private static IHiDealProvider _hp = null;
        private static IEventProvider _ep = null;
        private static IMemberProvider _mp = null;
        private static IQuestionProvider _qp;
        private static IDealProvider _dp;
        private static IPponEntityProvider _pep;
        protected static ILog logger = LogManager.GetLogger("PromotionFacade");


        /// <summary>
        /// APP端現行能處理的商品主題活動頁格式
        /// </summary>
        public static List<EventPromoTemplateType> AppEventPromoTemplateTypes { get; set; }

        static PromotionFacade()
        {
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _qp = ProviderFactory.Instance().GetProvider<IQuestionProvider>();
            _mkp = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
            _dp = ProviderFactory.Instance().GetProvider<IDealProvider>();
            _pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
            AppEventPromoTemplateTypes = new List<EventPromoTemplateType>()
                {
                    EventPromoTemplateType.Commercial,EventPromoTemplateType.JoinPiinlife,EventPromoTemplateType.PponOnly,EventPromoTemplateType.Zero,
                    EventPromoTemplateType.VourcherOnly,EventPromoTemplateType.SkmEvent,EventPromoTemplateType.VoteMix,
                    EventPromoTemplateType.NewJoinPiinlife,EventPromoTemplateType.OnlyNewPiinlife
                };
        }

        #region public methods

        /// <summary>
        /// 即時產生折價券，並回傳discount code和狀態(判斷是否為即時發送類別)
        /// </summary>
        /// <param name="campaign_id">discount code活動ID</param>
        /// <param name="discount">回傳discount code</param>
        /// <returns>回傳狀態</returns>
        public static InstantGenerateDiscountCampaignResult InstantGenerateDiscountCode(int campaign_id, out DiscountCode discount)
        {
            discount = new DiscountCode();
            DiscountCampaign campaign = _op.DiscountCampaignGet(campaign_id);
            if (campaign.Id == 0)
            {
                return InstantGenerateDiscountCampaignResult.NotExist;
            }
            else if ((campaign.Flag & (int)DiscountCampaignUsedFlags.InstantGenerate) == 0)
            {
                return InstantGenerateDiscountCampaignResult.TypeMistake;
            }
            else if (!campaign.ApplyTime.HasValue)
            {
                return InstantGenerateDiscountCampaignResult.Unaudited;
            }
            else if (campaign.CancelTime.HasValue)
            {
                return InstantGenerateDiscountCampaignResult.Cancel;
            }
            else if (DateTime.Now < campaign.StartTime)
            {
                return InstantGenerateDiscountCampaignResult.NotStarted;
            }
            else if (DateTime.Now < campaign.EndTime)
            {
                int current_total = _op.DiscountCodeGetCount(DiscountCode.Columns.CampaignId + "=" + campaign_id);
                if (current_total < campaign.Qty)
                {
                    DiscountCode dc = new DiscountCode();
                    dc.CampaignId = campaign.Id;
                    dc.Amount = campaign.Amount;
                    dc.Code = PromotionFacade.GetDiscountCode(dc.Amount.Value);
                    _op.DiscountCodeSet(dc);
                    discount = dc;
                    return InstantGenerateDiscountCampaignResult.Success;
                }
                else
                {
                    return InstantGenerateDiscountCampaignResult.Exceed;
                }
            }
            else
            {
                return InstantGenerateDiscountCampaignResult.Expire;
            }
        }

        /// <summary>
        /// 即時產生折價券並記錄擁有者名稱，並回傳discount code和狀態(判斷是否為即時發送類別)
        /// </summary>
        /// <param name="campaignId">discount code活動ID</param>
        /// <param name="memberUniqueId">使用者 UniqueId</param>
        /// <param name="checkAlreadySent">檢查是否已發送過</param>
        /// <param name="code">回傳discount code</param>
        /// <param name="amount">discount 金額</param>
        /// <param name="codeEndTime">折價券到期時間</param>
        /// <param name="discountCodeId">折價券Id</param>
        /// <param name="sender"></param>
        /// <returns>回傳狀態</returns>
        public static InstantGenerateDiscountCampaignResult InstantGenerateDiscountCode(int campaignId, int memberUniqueId, bool checkAlreadySent, out string code, out int amount, out DateTime? codeEndTime, out int discountCodeId, out int minimumAmount, int? referrerId = null, string sender = "sys")
        {
            code = string.Empty;
            amount = 0;
            codeEndTime = null;
            discountCodeId = 0;
            minimumAmount = 0;
            DiscountCampaign campaign = _op.DiscountCampaignGet(campaignId);
            if (campaign.Id == 0)
            {
                return InstantGenerateDiscountCampaignResult.NotExist;
            }
            else if ((campaign.Flag & (int)DiscountCampaignUsedFlags.InstantGenerate) == 0)
            {
                return InstantGenerateDiscountCampaignResult.TypeMistake;
            }
            else if (!campaign.ApplyTime.HasValue)
            {
                return InstantGenerateDiscountCampaignResult.Unaudited;
            }
            else if (campaign.CancelTime.HasValue)
            {
                return InstantGenerateDiscountCampaignResult.Cancel;
            }
            else if (DateTime.Now < campaign.StartTime)
            {
                return InstantGenerateDiscountCampaignResult.NotStarted;
            }
            else if (DateTime.Now < campaign.EndTime)
            {
                int currentTotal = _op.DiscountCodeGetCount(DiscountCode.Columns.CampaignId + "=" + campaignId);
                if (currentTotal < campaign.Qty)
                {
                    if (checkAlreadySent)
                    {
                        if (_op.DiscountCodeGetListByOwner(campaignId, memberUniqueId).Any())
                        {
                            return InstantGenerateDiscountCampaignResult.AlreadySent;
                        }
                    }
                    var dc = new DiscountCode();
                    dc.CampaignId = campaign.Id;
                    dc.Amount = campaign.Amount;
                    dc.Owner = memberUniqueId;
                    dc.Sender = sender;
                    dc.SendDate = DateTime.Now;
                    dc.Code = code = GetDiscountCode(dc.Amount.Value);
                    dc.ReferrerId = referrerId;
                    if (campaign.Amount != null)
                    {
                        amount = (int)campaign.Amount;
                    }

                    codeEndTime = campaign.EndTime;
                    _op.DiscountCodeSet(dc);
                    discountCodeId = dc.Id;
                    minimumAmount = (campaign.MinimumAmount == null) ? 0 : (int)campaign.MinimumAmount;
                    return InstantGenerateDiscountCampaignResult.Success;
                }
                else
                {
                    return InstantGenerateDiscountCampaignResult.Exceed;
                }
            }
            else
            {
                return InstantGenerateDiscountCampaignResult.Expire;
            }
        }

        public static string GetDiscountCode(decimal amount)
        {
            char[] d = ((int)amount).ToString().ToCharArray();
            for (int i = 0; i < d.Length; i++)
            {
                d[i] = (char)(int.Parse(d[i].ToString()) + 65);
            }

            string code = System.IO.Path.GetRandomFileName().Replace(".", "").Substring(1);

            string rd = "";
            foreach (char c in d)
            {
                rd = rd + c.ToString();
            }
            rd = rd.PadLeft(3, '0');
            char[] rds = rd.ToCharArray();
            for (int i = 0; i <= 2; i++)
            {
                if (i == 0)
                {
                    code = code.Insert(3, rds[i].ToString());
                }

                if (i == 1)
                {
                    code = code.Insert(6, rds[i].ToString());
                }

                if (i == 2)
                {
                    code = code.Insert(9, rds[i].ToString());
                }
            }

            return code.ToUpper();
        }

        /// <summary>
        /// 取得DiscountCode的金額，若編碼錯誤回傳-1
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static decimal GetAmountFromDiscountCode(string code, out int minimum_amount)
        {
            minimum_amount = 0;
            try
            {
                var discountCode = _op.ViewDiscountDetailByCode(code);
                minimum_amount = discountCode.MinimumAmount ?? 0;
                decimal rtn = discountCode.Amount ?? 0;
                if ((rtn >= 1000) || (rtn < 0) || discountCode.Id == 0)
                {
                    return -1;
                }
                return rtn;
            }
            catch
            {
                return -1;
            }
        }

        public static KeyValuePair<decimal, decimal> GetAmountFromDiscountCode(string code, string userName)
        {
            ViewDiscountDetail discountCode;
            return GetAmountDiscounFromDiscountCode(code, userName, out discountCode);
        }

        public static bool CheckDiscountCodeCategory(int discountCampaignId, string dealKey)
        {
            List<int> discountCategoryList = new List<int>();
            EventPromoCollection epc = _pp.EventPromoGetListByDiscountCampaignId(discountCampaignId);

            //沒有連結任何商品主題活動
            if (epc.Count == 0)
            {
                return true;
            }

            foreach (EventPromo ep in epc)
            {
                if (string.IsNullOrEmpty(ep.BindCategoryList))
                {
                    continue;
                }
                List<int> categoryArray = ep.BindCategoryList.Split(',').Select(int.Parse).ToList();
                discountCategoryList.AddRange(categoryArray);
            }

            if (discountCategoryList.Count > 0)
            {
                Guid bid = Guid.Empty;
                Guid.TryParse(dealKey, out bid);
                discountCategoryList = discountCategoryList.Distinct().ToList();
                List<int> dealCategorys = _pp.ViewAllDealCategoryGetListByBid(bid).Select(x => x.Cid).Distinct().ToList();
                var resultCategoryList = discountCategoryList.Intersect<int>(dealCategorys);
                if (resultCategoryList.Any())
                {
                    return true;
                }
            }

            return (_pp.ViewEventPromoItemDealIdGetListByEventPromoId(epc.Select(x => x.Id).ToList(), dealKey).Count > 0) ? true : false;
        }

        public static KeyValuePair<decimal, decimal> GetAmountDiscounFromDiscountCode(string code, string userName, out ViewDiscountDetail discountCode)
        {
            discountCode = _op.ViewDiscountDetailByCode(code);
            try
            {
                int userId = MemberFacade.GetUniqueId(userName);
                if (discountCode.Id == 0)
                {
                    return new KeyValuePair<decimal, decimal>(-1, 0);
                }
                decimal rtn = discountCode.Amount ?? 0;
                decimal min = 0;

                if (Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.SingleSerialKey))
                {
                    if (Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.CustomSerialKey))
                    {
                        var onTimeDiscountCode = _op.ViewDiscountDetailByCustomCodeOnTime(code);
                        if (onTimeDiscountCode.IsLoaded)
                        {
                            if (onTimeDiscountCode.UseTime != null)
                            {
                                //當期有資料但沒有未使用序號
                                return new KeyValuePair<decimal, decimal>(-6, 0);
                            }
                            discountCode = onTimeDiscountCode;
                            rtn = discountCode.Amount ?? 0;
                        }
                    }

                    if (!Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.CanReused))
                    {
                        if (_op.ViewDiscountDetailCountByCampiagnIdUserId(discountCode.Id, userId) > 0)
                        {
                            return new KeyValuePair<decimal, decimal>(-2, 0);
                        }
                    }

                }
                else if (discountCode.UseTime.HasValue)
                {
                    return new KeyValuePair<decimal, decimal>(-2, 0);
                }

                //未審核過的折價券視為無效
                if (discountCode.ApplyTime == null)
                {
                    return new KeyValuePair<decimal, decimal>(-1, 0);
                }

                // -4 not in valid time
                if (!(discountCode.StartTime <= DateTime.Now && DateTime.Now < discountCode.EndTime))
                {
                    return new KeyValuePair<decimal, decimal>(-4, 0);
                }

                if ((rtn < 0))
                {
                    return new KeyValuePair<decimal, decimal>(-1, 0);
                }

                if (Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.MinimumAmount))
                {
                    min = discountCode.MinimumAmount ?? 0;
                }

                return new KeyValuePair<decimal, decimal>(rtn, min);
            }
            catch
            {
                return new KeyValuePair<decimal, decimal>(-1, 0);
            }
        }

        public static KeyValuePair<decimal, decimal> GetAmountCheckDepFromDiscountCode(string code, string userName, int dep,
            string dealKey, out int flag, out string msg, bool fromWeb = false)
        {
            ViewDiscountDetail discountCode;
            Guid bid;
            KeyValuePair<decimal, decimal> result = GetAmountDiscounFromDiscountCode(code, userName, out discountCode);
            flag = discountCode.Flag;
            msg = string.Empty;
            if (discountCode.Id != 0 && !Helper.IsFlagSet(discountCode.Flag, dep))
            {
                return new KeyValuePair<decimal, decimal>(-3, 0);
            }
            if (discountCode.Id != 0 && Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.OwnerUseOnly)
                && MemberFacade.GetUniqueId(userName) != discountCode.Owner)
            {
                return new KeyValuePair<decimal, decimal>(-7, 0);
            }
            if (discountCode.Id != 0 && Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.CategoryLimit) &&
                CheckDiscountCategoryLimit(discountCode.Id, dealKey, out msg) == false)
            {
                return new KeyValuePair<decimal, decimal>(-8, 0);
            }
            if (discountCode.Id != 0 && Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.AppUseOnly) && fromWeb) //APP限定折價券
            {
                return new KeyValuePair<decimal, decimal>(-9, 0);
            }
            if (result.Key > -1 && !string.IsNullOrEmpty(dealKey) && !CheckDiscountCodeCategory(discountCode.Id, dealKey))
            {
                return new KeyValuePair<decimal, decimal>(-5, 0);
            }

            if (Guid.TryParse(dealKey, out bid))
            {
                if (discountCode.MinGrossMargin != null && discountCode.MinGrossMargin > PponFacade.GetDealMinimumGrossMargin(bid) * 100)
                {
                    return new KeyValuePair<decimal, decimal>(-5, 0);
                }
                var dealData = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                if (dealData.IsBankDeal != null && dealData.IsBankDeal.Value && !Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.CategoryLimit))
                {
                    return new KeyValuePair<decimal, decimal>(-5, 0);
                }
            }

            return result;
        }

        public static bool CheckDiscountCategoryLimit(int campaignId, string dealKey, out string msg)
        {
            Guid bid;
            msg = string.Empty;
            if (Guid.TryParse(dealKey, out bid))
            {
                Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(bid);

                if (_mkp.IsDiscountCategoryCampaignExist(mainBid, campaignId))
                {
                    return true;
                }

                DiscountCategoryCollection dcc = _op.DiscountCategoryGetList(campaignId);
                if (dcc.Any())
                {
                    msg = GetDiscountCategoryString(dcc);
                }
                else
                {
                    var discountEventPromoLink = _op.DiscountEventPromoLinkGetList(campaignId);
                    if (discountEventPromoLink.Count > 0)
                    {
                        if (_mkp.IsDiscountEventPromoLinkCampaignExist(mainBid,
                            discountEventPromoLink.Select(x => x.EventPromoId).ToList()))
                        {
                            return true;
                        }

                        //若有綁定商品主題活動、策展1.0，顯示活動名稱
                        msg = MarketingFacade.GetCurationTitle(discountEventPromoLink.Last().EventPromoId, (int)EventPromoEventType.Curation);
                    }

                    var discountBarndLink = _op.DiscountBrandLinkGetList(campaignId);
                    if (discountBarndLink.Count > 0)
                    {
                        if (_mkp.IsDiscountBrandLinkCampaignExist(mainBid,
                            discountBarndLink.Select(x => x.BrandId).ToList()))
                        {
                            return true;
                        }

                        //若有綁定策展2.0，顯示活動名稱
                        msg = MarketingFacade.GetCurationTitle(discountBarndLink.Last().BrandId, (int)EventPromoEventType.CurationTwo);
                    }
                }
            }
            return false;
        }

        public static void UpDateDiscountUseType()
        {
            var pponDealList = ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true);
            var campaigns = _op.DiscountCampaignGetByDayForShow();
            List<DiscountManager.CampaignRule> campaignRules = new List<DiscountManager.CampaignRule>();
            foreach (var campaignId in campaigns.Select(t => t.Id))
            {
                var campaignRule = DiscountManager.GetCampaignRule(campaignId);
                campaignRules.Add(campaignRule);
            }
            foreach (var deal in pponDealList)
            {
                Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(deal.BusinessHourGuid);
                DiscountUseType discountUseType = _op.DiscountUseTypeGet(mainBid);
                discountUseType.Bid = mainBid;
                var newUseType = GetDiscountUseTypeForShow(mainBid, campaignRules);
                if (discountUseType.IsLoaded == false ||
                    (discountUseType.IsLoaded && newUseType != discountUseType.UseType))
                {
                    discountUseType.UseType = newUseType;
                    _op.DiscountUseTypeSet(discountUseType);
                }
            }
        }

        private static bool GetDiscountUseType(List<DiscountCampaign> campaigns, Guid bid, out List<string> matchCampaignNames)
        {
            List<DiscountManager.CampaignRule> campaignRules = new List<DiscountManager.CampaignRule>();
            foreach(var campaignId in campaigns.Select(t=>t.Id))
            {
                var campaignRule = DiscountManager.GetCampaignRule(campaignId);
                campaignRules.Add(campaignRule);
            }
            return GetDiscountUseType(campaignRules, bid, out matchCampaignNames);
        }

        private static bool GetDiscountUseType(List<DiscountManager.CampaignRule> campaignRules, Guid bid,
            out List<string> matchCampaignNames)
        {
            matchCampaignNames = new List<string>();
            if (bid == Guid.Empty)
            {
                return false;
            }

            var dealTerm = DiscountManager.GetDealDiscountTerm(bid);
            if (dealTerm == null || dealTerm.Banned)
            {
                return false;
            }

            List<int> matchIds = new List<int>();
            foreach(var campaignRule in campaignRules)
            {
                if (campaignRule.Match(dealTerm))
                {
                    matchIds.Add(campaignRule.CampaignId);
                    matchCampaignNames.Add(string.Format("{0}-{1}", campaignRule.CampaignId, campaignRule.CampaignName));
                }
            }
            return matchIds.Count > 0;
        }

        public static bool GetDiscountUseTypeForShow(Guid bid, List<DiscountManager.CampaignRule> campaignRules)
        {
            List<string> matchCampaignNames;
            return GetDiscountUseType(campaignRules, bid, out matchCampaignNames);
        }

        public static bool GetDiscountUseType(Guid bid)
        {
            List<string> matchCampaignNames;            
            return GetDiscountUseType(bid, out matchCampaignNames);
        }

        public static bool GetDiscountUseType(Guid bid, out List<string> matchCampaignNames)
        {
            var campaigns = _op.DiscountCampaignGetByDayForBuy();
            return GetDiscountUseType(campaigns.ToList(), bid, out matchCampaignNames);
        }

        public static void ClearDiscountCampaignGetByDayForBuyCache()
        {
            List<int> campaignIds = _op.DiscountCampaignGetByDayForBuy().Select(t=>t.Id).ToList();
            DiscountManager.ClearCampaignRuleCache(campaignIds);
        }

        public static bool GetDiscountUseTypeLite(Guid bid)
        {
            if (bid != Guid.Empty)
            {
                //母檔BID
                Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(bid);

                //不可使用折價券就不做處理
                if (_op.DiscountLimitEnabledGetByBid(mainBid))
                {
                    return false;
                }

                return true;
            }
            return false;
        }

        public static string GetDiscountCategoryString(string code)
        {
            var discountCode = _op.ViewDiscountDetailByCode(code);
            if (discountCode != null && discountCode.IsLoaded)
            {
                return GetDiscountCategoryString(discountCode.Id, false, false);
            }
            return string.Empty;
        }

        private static ConcurrentDictionary<string, string> _campaignDescDict = new ConcurrentDictionary<string, string>();

        /// <summary>
        /// 折價券使用範圍
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="isShowMoreDesc">是否顯示更多資訊(如:部份不適用)</param>
        /// <param name="isAppUseOnly">是否限定APP</param>
        /// <param name="cacheEnabled"></param>
        /// <returns></returns>
        public static string GetDiscountCategoryString(int campaignId, bool isShowMoreDesc = true, bool isAppUseOnly = false, bool cacheEnabled = true)
        {
            string cacheKey = string.Format("{0}.{1}.{2}", campaignId, isShowMoreDesc, isAppUseOnly);
            string desc;
            if (cacheEnabled)
            {
                if (_campaignDescDict.TryGetValue(cacheKey, out desc))
                {
                    return desc;
                }
            }

            var discountEventPromoLink = _op.DiscountEventPromoLinkGetList(campaignId);
            var discountBarndLink = _op.DiscountBrandLinkGetList(campaignId);

            if (discountBarndLink.Count > 0)
            {
                //若有綁定策展2.0，顯示活動名稱
                desc = MarketingFacade.GetCurationTitle(discountBarndLink[0].BrandId, (int)EventPromoEventType.CurationTwo);
            }
            else if (discountEventPromoLink.Count > 0)
            {
                //若有綁定商品主題活動、策展1.0，顯示活動名稱 
                desc = MarketingFacade.GetCurationTitle(discountEventPromoLink[0].EventPromoId, (int)EventPromoEventType.Curation);
            }
            else
            {
                DiscountCategoryCollection dcc = _op.DiscountCategoryGetList(campaignId);
                desc = GetDiscountCategoryString(dcc);
                if (string.IsNullOrEmpty(desc))
                {
                    desc += "全站";
                }
                if (isShowMoreDesc)
                {
                    desc += "(部份不適用)";
                }
            }

            if (isAppUseOnly)
            {
                desc += "(App限定)";
            }
            if (cacheEnabled)
            {
                _campaignDescDict.TryAdd(cacheKey, desc);
            }
            return desc;
        }

        public static string GetDiscountCategoryString(DiscountCategoryCollection dcc)
        {
            List<string> categories = new List<string>();
            foreach (var channel in dcc.GroupBy(x => x.ChannelId))
            {
                string cate = CategoryManager.CategoryGetById(channel.Key).Name;
                if (channel.Any(x => x.CategoryId != channel.Key))
                {
                    cate += " ";
                    foreach (var item in channel)
                    {
                        cate += CategoryManager.CategoryGetById(item.CategoryId).Name + "/";
                    }
                    cate = cate.TrimEnd('/');
                }
                categories.Add(cate);
            }
            return string.Join("、", categories);
        }

        public static Dictionary<decimal, int> GetMemberDiscountCode(string userName)
        {
            int userId = MemberFacade.GetUniqueId(userName);
            return GetMemberDiscountCode(userId);
        }

        public static Dictionary<decimal, int> GetMemberDiscountCode(int userId)
        {
            IEnumerable<ViewDiscountDetail> discountList = GetMemberDiscountList(userId);
            return discountList.Where(x => (!x.CancelTime.HasValue)).GroupBy(x => x.Amount.Value, (x, g) => new { Key = x, dCount = g.Count() }).ToDictionary(x => x.Key, y => y.dCount);
        }

        /// <summary>
        /// 取得會員歸戶且可使用的折價券
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static List<ViewDiscountDetail> GetMemberDiscountList(int userId, Guid? bid = null)
        {
            var discountList = _op.ViewDiscountDetailGetUnUsedList(userId, DiscountCampaignType.PponDiscountTicket);

            List<ViewDiscountDetail> result = new List<ViewDiscountDetail>();
            if (discountList.Count == 0)
            {
                return result;
            }
            if (bid != null)
            {
                Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(bid.Value); //母檔BID

                var dealDiscountTerm = DiscountManager.GetDealDiscountTerm(mainBid);
                if (dealDiscountTerm.Banned)
                {
                    return result;
                }

                foreach (var discount in discountList)
                {
                    int campaignId = discount.Id;
                    var campaignRule = DiscountManager.GetCampaignRule(campaignId, false);
                    if (campaignRule != null && campaignRule.Match(dealDiscountTerm))
                    {
                        if (campaignRule.Match(dealDiscountTerm))
                        {
                            result.Add(discount);
                        }
                    }
                }
            }
            else
            {
                result = discountList.ToList();
            }
            return result;
        }

        /// <summary>
        /// PCP抵用券檢查
        /// </summary>
        /// <param name="discountCodeId">discount_code.Id</param>
        /// <param name="type"></param>
        /// <param name="amount">結帳金額</param>
        /// <param name="discountAmount">熟客卡折抵金額</param>
        /// <param name="userName"></param>
        /// <param name="minimumAmount"></param>
        /// <param name="discountCodeAmount"></param>
        /// <returns></returns>
        public static DiscountCodeStatus GetDiscountCodeStatusForPcp(int discountCodeId, DiscountCampaignUsedFlags type,
            double? amount, double discountAmount, string userName, out int minimumAmount, out int discountCodeAmount)
        {
            discountCodeAmount = 0;
            var discountCode = _op.ViewDiscountDetailByDiscountCodeId(discountCodeId);
            var status = GetDiscountCodeStatus(discountCode, type, amount == null ? 0 : (decimal)amount, userName, out minimumAmount, string.Empty);

            if (status != DiscountCodeStatus.CanUse)
            {
                return status;
            }

            if (discountCode.Type != (int)DiscountCampaignType.RegularsTicket &&
                discountCode.Type != (int)DiscountCampaignType.FavorTicket)
            {
                return DiscountCodeStatus.None;
            }

            if (!discountCode.CardCombineUse && discountAmount > 0)
            {
                return DiscountCodeStatus.CanNotCardCombineUse;
            }
            discountCodeAmount = (discountCode.Amount == null) ? 0 : (int)discountCode.Amount;
            return status;
        }

        public static DiscountCodeStatus GetDiscountCodeStatus(string code, DiscountCampaignUsedFlags type,
            decimal amount, string userName, out int minimumAmount, string dealKey)
        {
            var discountCode = _op.ViewDiscountDetailByCode(code);
            var status = GetDiscountCodeStatus(discountCode, type, amount, userName, out minimumAmount, dealKey);

            if (status != DiscountCodeStatus.CanUse)
            {
                return status;
            }

            if (discountCode.Type != (int)DiscountCampaignType.SuperDiscountTicket &&
                discountCode.Type != (int)DiscountCampaignType.PponDiscountTicket)
            {
                return DiscountCodeStatus.None;
            }

            return status;
        }

        public static bool GetDiscountCodeAmount(string code, out int discountAmount, out DiscountCode discountCode)
        {
            if (!string.IsNullOrWhiteSpace(code))
            {
                discountCode = _op.DiscountCodeGetByCodeWithoutUseTime(code);

                if (discountCode != null && discountCode.Id > 0 && discountCode.IsLoaded)
                {
                    discountAmount = discountCode.Amount == null ? 0 : (int)discountCode.Amount.Value;
                    return true;
                }
            }
            discountAmount = 0;
            discountCode = new DiscountCode();
            return false;
        }

        public static DiscountCodeStatus GetDiscountCodeStatus(ViewDiscountDetail discountCode, DiscountCampaignUsedFlags type, decimal amount, string userName, out int minimumAmount, string dealKey)
        {
            int userId = MemberFacade.GetUniqueId(userName);
            minimumAmount = 0;

            #region 判斷基礎狀態

            if (!discountCode.IsLoaded)
            {
                // 無此coupon code
                return DiscountCodeStatus.None;
            }
            if (discountCode.CancelTime != null)
            {
                // 作廢
                return DiscountCodeStatus.Disabled;
            }
            if (discountCode.EndTime < DateTime.Now)
            {
                // 已過期
                return DiscountCodeStatus.OverTime;
            }

            #endregion

            #region 統一序號 & 統一自定活動短碼

            //如果折價券為統一序號，1.需判斷使用者是否有用過，2.是否有剩餘可使用的折價券
            //如果是自定短碼, 需先判斷 1.是否為以過期活動 2.當期是否還有可用序號 3. 是否有未來活動
            if ((discountCode.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) > 0)
            {
                if ((discountCode.Flag & (int)DiscountCampaignUsedFlags.CustomSerialKey) > 0)
                {
                    if (discountCode.EndTime < DateTime.Now)
                    {
                        //最新一筆活動結束時間已過期
                        return DiscountCodeStatus.OverTime;
                    }

                    //重新取回一筆當期序號 (未使用優先，否則取得一筆已使用)
                    var onTimeDiscountCode = _op.ViewDiscountDetailByCustomCodeOnTime(discountCode.Code);
                    if (onTimeDiscountCode.IsLoaded)
                    {
                        if (onTimeDiscountCode.UseTime != null)
                        {
                            //當期有資料但沒有未使用序號
                            return DiscountCodeStatus.UpToQuantity;
                        }
                        discountCode = onTimeDiscountCode;
                    }
                    else
                    {
                        //當期沒有序號代表這是未來期序號 (未開始)
                        //如果該筆已被使用則重新抓一筆未使用的
                        if (discountCode.UseTime != null)
                        {
                            var nextTimeDiscountCode = _op.ViewDiscountDetailByCustomCodeNextTime(discountCode.Code);
                            if (nextTimeDiscountCode.IsLoaded)
                            {
                                discountCode = nextTimeDiscountCode;
                            }
                        }
                    }
                }
                else
                {
                    discountCode = _op.ViewDiscountDetailByCodeWithoutUseTime(discountCode.Code);
                    if (!discountCode.IsLoaded)
                    {
                        return DiscountCodeStatus.UpToQuantity;
                    }
                }
            }

            #endregion 統一序號 & 統一自定活動短碼

            if (discountCode.UseTime != null)
            {
                // 已被使用
                return DiscountCodeStatus.IsUsed;
            }
            if ((discountCode.StartTime > DateTime.Now) && (discountCode.ApplyTime == null))
            {
                //生效日尚未到達，且尚未啟用
                return DiscountCodeStatus.NotAppplyAndStart;
            }
            if (discountCode.StartTime <= DateTime.Now && discountCode.ApplyTime == null)
            {
                //日期已到生效日期但尚未啟用
                return DiscountCodeStatus.NotApply;
            }
            if (discountCode.StartTime > DateTime.Now && discountCode.ApplyTime != null)
            {
                // 已啟用，但時間未到生效日期
                return DiscountCodeStatus.NotStartly;
            }
            if (!Helper.IsFlagSet(discountCode.Flag, type))
            {
                // 館別受限
                return DiscountCodeStatus.Restricted;
            }
            if (Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.SingleSerialKey) && _op.ViewDiscountDetailCountByCampiagnIdUserId(discountCode.Id, userId) > 0)
            {
                if (!Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.CanReused))
                {
                    //已使用過同活動的折價券(統一序號)
                    return DiscountCodeStatus.IsUsed;
                }
            }
            if (Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.MinimumAmount) && (discountCode.MinimumAmount ?? 0) > amount)
            {
                minimumAmount = discountCode.MinimumAmount ?? 0;
                //低於最低訂單金額
                return DiscountCodeStatus.UnderMinimumAmount;
            }
            if (discountCode.Amount > amount)
            {
                minimumAmount = (int)discountCode.Amount;
                //低於最低訂單金額
                return DiscountCodeStatus.UnderMinimumAmount;
            }
            if (!string.IsNullOrEmpty(dealKey) && type == DiscountCampaignUsedFlags.Ppon)
            {
                if (!CheckDiscountCodeCategory(discountCode.Id, dealKey))
                {
                    return DiscountCodeStatus.DiscountCodeCategoryWrong;
                }
                Guid bid;
                if (Guid.TryParse(dealKey, out bid))
                {
                    var dealData = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                    if (dealData.IsBankDeal != null && dealData.IsBankDeal.Value && ((discountCode.Flag & (int)DiscountCampaignUsedFlags.CategoryLimit) == 0))
                    {
                        return DiscountCodeStatus.DiscountCodeCategoryWrong;
                    }
                }
            }
            if (Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.OwnerUseOnly) && discountCode.Owner != userId)
            {
                return DiscountCodeStatus.OwnerUseOnlyWrong;
            }

            // 使用範圍限制
            string msg = string.Empty;
            if (!string.IsNullOrEmpty(dealKey) && Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.CategoryLimit) && !CheckDiscountCategoryLimit(discountCode.Id, dealKey, out msg))
            {
                return DiscountCodeStatus.CategoryUseLimit;
            }

            return DiscountCodeStatus.CanUse;
        }

        /// <summary>
        /// 折價券Id 是否存在
        /// </summary>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        public static InstantGenerateDiscountCampaignResult IsExistDiscountCampaignId(int campaignId)
        {
            var campaign = _op.DiscountCampaignGet(campaignId);
            if (campaign.Id == 0)
            {
                return InstantGenerateDiscountCampaignResult.NotExist;
            }
            else if ((campaign.Flag & (int)DiscountCampaignUsedFlags.InstantGenerate) == 0)
            {
                return InstantGenerateDiscountCampaignResult.TypeMistake;
            }
            else if (DateTime.Now > campaign.EndTime)
            {
                return InstantGenerateDiscountCampaignResult.Expire;
            }
            else if (!campaign.ApplyTime.HasValue)
            {
                return InstantGenerateDiscountCampaignResult.Unaudited;
            }
            else if (campaign.CancelTime.HasValue)
            {
                return InstantGenerateDiscountCampaignResult.Cancel;
            }
            else
            {
                return InstantGenerateDiscountCampaignResult.Success;
            }
        }

        #region Sitemap

        /// <summary>
        /// 建立近期好康XML檔
        /// </summary>
        public static void SitemapCreateCurrentDeal(DateTime todayTime)
        {
            XNamespace xname = "http://www.sitemaps.org/schemas/sitemap/0.9";
            var doc = new XDocument(new XDeclaration("1.0", "utf-8", null));
            var fileNamebyDay = Path.Combine(Helper.MapPath(_config.SiteMapCreatePath, true), "SitemapGetCurrentDeal.Xml");
            try
            {
                //連結路徑
                var googleImage = XNamespace.Get("http://www.google.com/schemas/sitemap-image/1.1");
                var root = new XElement(xname + "urlset", new XAttribute(XNamespace.Xmlns + "image", googleImage.NamespaceName));

                var fileupdateTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");

                #region 17Life 首頁

                SitemapPatternXmlWithImage(xname, googleImage, root, _config.SiteUrl, fileupdateTime, Changefreq.daily.ToString(), Priority._0point8,
                    _config.DefaultMetaOgImage.Split(','), _config.DefaultMetaOgTitle, _config.DefaultTitle);

                #endregion

                #region 各館別

                //美食
                SitemapPatternXml(xname, root, string.Format("{0}/channel/{1}", _config.SiteUrl, CategoryManager.Default.PponDeal.CategoryId),
                    fileupdateTime, Changefreq.daily.ToString(), Priority._0point8);
                //宅配好物
                SitemapPatternXml(xname, root, string.Format("{0}/channel/{1}", _config.SiteUrl, CategoryManager.Default.Delivery.CategoryId),
                    fileupdateTime, Changefreq.daily.ToString(), Priority._0point8);
                //旅遊渡假
                SitemapPatternXml(xname, root, string.Format("{0}/channel/{1}", _config.SiteUrl, CategoryManager.Default.Travel.CategoryId),
                    fileupdateTime, Changefreq.daily.ToString(), Priority._0point8);
                //女性專區
                SitemapPatternXml(xname, root, string.Format("{0}/channel/{1}", _config.SiteUrl, CategoryManager.Default.Beauty.CategoryId),
                    fileupdateTime, Changefreq.daily.ToString(), Priority._0point8);
                //品生活
                SitemapPatternXml(xname, root, string.Format("{0}/piinlife/default.aspx", _config.SiteUrl), fileupdateTime,
                    Changefreq.daily.ToString(), Priority._0point8);

                #endregion

                #region 策展

                HashSet<string> curationLinks = new HashSet<string>();
                ThemeCurationMain theme = _pp.GetNowThemeCurationMainCollection().FirstOrDefault(x => x.ThemeType == (int)ThemeType.Channel);
                if (theme != null)
                {
                    CurationsByTheme themeCurations = PromotionFacade.GetCurationsByTheme(theme.Id, true);
                    foreach (var allCurations in themeCurations.AllCurations)
                    {
                        foreach (var curation in allCurations.Curations)
                        {
                            if (curationLinks.Contains(curation.CurationLink))
                            {
                                continue;
                            }
                            curationLinks.Add(curation.CurationLink);
                        }
                    }
                }

                foreach (string curationLink in curationLinks)
                {
                    SitemapPatternXml(xname, root, curationLink, fileupdateTime,
                        Changefreq.daily.ToString(), Priority._0point8);
                }

                #endregion

                #region 各地區在地好康

                CategoryNode[] cityList = { CategoryManager.Default.Taipei,
                                              CategoryManager.Default.Taoyuan,
                                              CategoryManager.Default.Hsinchu,
                                              CategoryManager.Default.Taichung,
                                              CategoryManager.Default.Tainan,
                                              CategoryManager.Default.Kaohsiung
                                          };

                foreach (CategoryNode city in cityList)
                {
                    SitemapPatternXml(xname, root,
                        string.Format("{0}/channel/{1}?aid={2}", _config.SiteUrl, CategoryManager.Default.PponDeal.CategoryId, city.CategoryId),
                        fileupdateTime, Changefreq.daily.ToString(), Priority._0point8);
                }

                #endregion

                #region 今日新檔

                var deals = GetOnlineMainDealNoSkmNoTmallNoHidden();
                HashSet<string> keywords = new HashSet<string>();
                foreach (var deal in deals)
                {
                    // 若為品生活檔次，轉址為品生活頁面
                    bool isPiinlife = (PponDealPreviewManager.GetDealCategoryIdList(deal.BusinessHourGuid)
                                              .IndexOf(CategoryManager.Default.Piinlife.CategoryId) > -1);

                    //string[] dealImageUrl = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto);
                    string[] dealImageUrl = new string[0];//
                    SitemapPatternXmlWithImage(xname, googleImage, root, isPiinlife
                            ? string.Format("{0}/piinlife/{1}", _config.SiteUrl, deal.BusinessHourGuid)
                            : string.Format("{0}/deal/{1}", _config.SiteUrl, deal.BusinessHourGuid)
                        , fileupdateTime, Changefreq.daily.ToString(), Priority._0point5, dealImageUrl
                        , (string.IsNullOrEmpty(deal.AppTitle)) ? deal.CouponUsage : deal.AppTitle,
                        (string.IsNullOrEmpty(deal.PicAlt)) ? deal.SellerName : deal.PicAlt);

                    foreach (string tag in deal.Tags)
                    {
                        if (keywords.Contains(tag) == false)
                        {
                            keywords.Add(tag);
                        }
                    }
                }

                #endregion

                #region 關鍵字搜尋

                //讀取外部關鍵字
                string extraKeywordsFilePath = Helper.MapPath("~/share/App_Data/SiteMapExtraKeywords.txt");

                if (File.Exists(extraKeywordsFilePath))
                {
                    List<string> extraKeywrods = new List<string>(File.ReadAllLines(extraKeywordsFilePath));
                    foreach (string extraKeywrod in extraKeywrods)
                    {
                        if (keywords.Contains(extraKeywrod) == false)
                        {
                            keywords.Add(extraKeywrod);
                        }
                    }
                }

                foreach (string keyword in keywords)
                {
                    SitemapPatternXml(xname, root,
                        string.Format("{0}/ppon/pponsearch.aspx?search={1}", _config.SiteUrl, HttpUtility.UrlEncode(keyword)),
                        fileupdateTime, Changefreq.weekly.ToString(), Priority._0point5);
                }

                #endregion

                doc.Add(root);
                doc.Save(fileNamebyDay);
                CompressXmlfile(Helper.MapPath(_config.SiteMapCreatePath, true), "SitemapGetCurrentDeal.Xml");
                CreateSitemapIndex();
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Gen CurrentDeal SiteMap Xml Error!! {0}", ex.Message);
            }
        }
        /// <summary>
        /// 取得目前線上的母檔、排除新光檔、天貓檔、隱藏檔
        /// </summary>
        /// <returns></returns>
        public static List<IViewPponDeal> GetOnlineMainDealNoSkmNoTmallNoHidden()
        {
            List<IViewPponDeal> result = new List<IViewPponDeal>();
            var deals = ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true);
            foreach (var deal in deals)
            {
                //排除天貓
                if ((deal.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
                {
                    continue;
                }

                //排除未設定 city id
                if (string.IsNullOrEmpty(deal.CityList))
                {
                    continue;
                }

                var pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityForDealChannel(new JsonSerializer().Deserialize<List<string>>(deal.CityList), 0);
                if (pponCity == null)
                {
                    continue;
                }

                var cityId = pponCity.CityId;

                // 若為品生活檔次，轉址為品生活頁面
                bool isPiinlife = (PponDealPreviewManager.GetDealCategoryIdList(deal.BusinessHourGuid)
                                       .IndexOf(CategoryManager.Default.Piinlife.CategoryId) > -1);

                //排除隱藏檔
                var dealTimeSlot = ViewPponDealManager.DefaultManager.DealTimeSlotGetByBidCityAndTime(deal.BusinessHourGuid, cityId, DateTime.Now);
                if (dealTimeSlot != null)
                {
                    if (dealTimeSlot.Status == (int)DealTimeSlotStatus.NotShowInPponDefault)
                    {
                        continue;
                    }
                }

                var categoryIds = PponDealPreviewManager.GetDealCategoryIdList(deal.BusinessHourGuid);
                if (_config.NotShowSkmDealsInWeb && categoryIds.Contains(_config.SkmCategordId))
                {
                    continue;
                }
                result.Add(deal);
            }
            return result;
        }

        public static Dictionary<string, int> GetGetOnlineMainDealNoSkmNoTmallNoHiddenKeywordDict()
        {
            Dictionary<string, int> keywords = new Dictionary<string, int>();
            List<IViewPponDeal> deals = PromotionFacade.GetOnlineMainDealNoSkmNoTmallNoHidden();
            foreach (var deal in deals)
            {
                foreach (string tag in deal.Tags)
                {
                    if (string.IsNullOrWhiteSpace(tag))
                    {
                        continue;
                    }
                    if (keywords.ContainsKey(tag) == false)
                    {
                        keywords.Add(tag, 0);
                    }
                    keywords[tag]++;
                }
            }
            return keywords;
        }

        /// <summary>
        /// 壓縮XML檔
        /// </summary>
        public static void CompressXmlfile(string filepath, string fileName)
        {
            string Extension = Path.GetExtension(fileName);

            //壓縮xml檔
            string GetCompressfileName = filepath + "/" + fileName;
            //如果目錄下有存在XML檔案就執行
            if (File.Exists(GetCompressfileName))
            {
                if (File.Exists(GetCompressfileName + @".gz"))
                {
                    File.Delete(GetCompressfileName + @".gz");
                }

                byte[] buffer;
                using (FileStream infile = new FileStream(GetCompressfileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    buffer = new byte[infile.Length];
                    // 讀取文件確保文件可讀取  
                    int count = infile.Read(buffer, 0, buffer.Length);
                    if (count != buffer.Length)
                    {
                        return;
                    }
                }
                using (Stream fs = File.Create(filepath + "/" + Path.ChangeExtension(fileName + Extension, ".gz")))
                {
                    using (GZipStream gZip = new GZipStream(fs, CompressionMode.Compress, true))
                    {
                        gZip.Write(buffer, 0, buffer.Length);
                    }
                }
            }

        }

        /// <summary>
        /// 把壓縮成.gz的XML檔放至sitemap索引檔中
        /// </summary>
        public static void CreateSitemapIndex()
        {
            try
            {
                string siteUrl = _config.SiteUrl;
                string path = Helper.MapPath(_config.SiteMapCreatePath);
                string sitemapindexname = Path.Combine(path, "SitemapIndex.Xml");

                XmlDocument xdoc = new XmlDocument();
                xdoc.AppendChild(xdoc.CreateXmlDeclaration("1.0", "UTF-8", null));
                XmlElement siteMapIndexMain = xdoc.CreateElement("sitemapindex");
                XmlAttribute xmlnsName = xdoc.CreateAttribute("xmlns");
                xmlnsName.Value = "http://www.sitemaps.org/schemas/sitemap/0.9";
                siteMapIndexMain.Attributes.Append(xmlnsName);
                xdoc.AppendChild(siteMapIndexMain);

                #region  取壓縮檔案 :: 找出目錄下是否有壓縮檔(.gz)，如有就加入索引檔裡

                string baseUrl = _config.SiteMapBaseUrl;
                if (string.IsNullOrEmpty(baseUrl))
                {
                    baseUrl = siteUrl;
                }
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                foreach (FileInfo fileInfo in dirInfo.GetFiles())
                {
                    if (fileInfo.Name == @"SitemapGetCurrentDeal.Xml.gz")
                    {
                        string fileupdateTime = fileInfo.LastWriteTime.ToString("yyyy-MM-ddTHH:mm:sszzz");
                        string url = Helper.CombineUrl(baseUrl, fileInfo.Name);
                        siteMapIndexMain.AppendChild(ForSitemapIndexPatternXml(xdoc, url, fileupdateTime));
                    }
                }

                #endregion

                // 將建立的 XML 節點儲存為檔案
                using (XmlTextWriter xmlWriter = new XmlTextWriter(sitemapindexname, Encoding.UTF8))
                {
                    xmlWriter.Formatting = Formatting.Indented;
                    xdoc.Save(xmlWriter);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void SitemapPatternXml(XNamespace xname, XElement root, string loc, string lastmod, string changefreq, string priority)
        {
            var url = new XElement(xname + "url",
                new XElement(xname + "loc", loc), new XElement(xname + "lastmod", lastmod), new XElement(xname + "changefreq", changefreq), new XElement(xname + "priority", priority));
            root.Add(url);
        }

        private static void SitemapPatternXmlWithImage(XNamespace xname, XNamespace image, XElement root, string loc, string lastmod, string changefreq, string priority, string[] imageUrlList, string caption, string title)
        {
            var url = new XElement(xname + "url",
                new XElement(xname + "loc", loc),
                new XElement(xname + "lastmod", lastmod),
                new XElement(xname + "changefreq", changefreq),
                new XElement(xname + "priority", priority)
                );

            foreach (var imageUrl in imageUrlList)
            {
                if (!string.IsNullOrEmpty(imageUrl))
                {
                    var imageElement = new XElement(image + "image");

                    string urlString = (imageUrl.IndexOf("?") > -1) ? imageUrl.Substring(0, imageUrl.IndexOf("?")) : imageUrl;
                    imageElement.Add(new XElement(image + "loc", urlString));

                    if (!string.IsNullOrEmpty(caption))
                    {
                        imageElement.Add(new XElement(image + "caption", caption));
                    }
                    if (!string.IsNullOrEmpty(title))
                    {
                        imageElement.Add(new XElement(image + "title", title));
                    }

                    if (imageElement.Elements().Count() > 0)
                    {
                        url.Add(imageElement);
                    }
                }
            }

            root.Add(url);
        }

        private static XmlElement SitemapPatternXml(XmlDocument xdoc, string loc, string lastmod, string changefreq, string priority)
        {
            XmlElement PatternXml = xdoc.CreateElement("url");
            XmlElement PatternChild1 = xdoc.CreateElement("loc");
            PatternChild1.InnerText = loc;
            PatternXml.AppendChild(PatternChild1);

            XmlElement PatternChild2 = xdoc.CreateElement("lastmod");
            PatternChild2.InnerText = lastmod;
            PatternXml.AppendChild(PatternChild2);

            XmlElement PatternChild3 = xdoc.CreateElement("changefreq");
            PatternChild3.InnerText = changefreq;
            PatternXml.AppendChild(PatternChild3);

            XmlElement PatternChild4 = xdoc.CreateElement("priority");
            PatternChild4.InnerText = priority;
            PatternXml.AppendChild(PatternChild4);

            return PatternXml;
        }

        private static XmlElement ForSitemapIndexPatternXml(XmlDocument xdoc, string loc, string lastmod)
        {

            XmlElement IndexPatternXml = xdoc.CreateElement("sitemap");
            XmlElement PatternChild1 = xdoc.CreateElement("loc");
            PatternChild1.InnerText = loc;
            IndexPatternXml.AppendChild(PatternChild1);

            XmlElement PatternChild2 = xdoc.CreateElement("lastmod");
            PatternChild2.InnerText = lastmod;
            IndexPatternXml.AppendChild(PatternChild2);

            return IndexPatternXml;
        }

        #endregion

        #region Ppon Deal Feed

        #region Main
        public static void CreatePponDealFeed()
        {
            FeedPriceStrategy googlePriceStrategy = FeedPriceStrategy.LowestAverage;
            if (_config.EnableLowestComboDealPriceForGoogleShopping)
            {
                googlePriceStrategy = FeedPriceStrategy.LowestComboDealPrice;
            }
            else if (_config.EnableGSASubstituteDiscountPriceForPrice)
            {
                googlePriceStrategy = FeedPriceStrategy.LowestAverageWithDiscountPrice;
            }

            {
                var deals = ViewPponDealGetListForDealFeed(_config.GoogleProductFeedSet == PponDealType.ToHouse);
                CreatePponDealFeedXml(deals, (int)PponDealFeedType.Google, 
                    googlePriceStrategy, "GoogleProductFeed.Xml");
            }

            {
                var deals = ViewPponDealGetListForDealFeed(_config.GoogleProductFeedSet == PponDealType.ToHouse);
                CreatePponDealFeedXml(deals, PponDealFeedType.FB,
                     FeedPriceStrategy.LowestAverageWithDiscountPrice, "FBProductFeed.Xml");
            }
            //for yahoo
            {
                var deals = ViewPponDealGetListForDealFeed(_config.GoogleProductFeedSet == PponDealType.ToHouse, 6, 8);
                CreatePponDealFeedXml(deals, PponDealFeedType.Yahoo,
                     FeedPriceStrategy.LowestAverageWithDiscountPrice, "YahooProductFeed.Xml");
            }

        }
        #endregion

        public static IList<IViewPponDeal> ViewPponDealGetListForDealFeed(bool homeDeliveryOnly, 
            int? couponoDealGrossProfitLimit = null, int? homeDeliveryGrossProfitLimit = null)
        {
            List<IViewPponDeal> result = new List<IViewPponDeal>();
            //order get data way
            //result.AddRange(_pp.ViewPponDealGetListForDealFeed((int)PponDealType.All));

            if (homeDeliveryOnly == false)
            {
                result.AddRange(ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true).Where(t =>
                        t.IsHiddenInRecommendDeal == false &&
                        Helper.IsFlagSet(t.GroupOrderStatus.GetValueOrDefault(), GroupOrderStatus.Completed) == false &&
                        t.OrderedQuantity < t.OrderTotalLimit &&
                        string.IsNullOrEmpty(t.AppDealPic) == false &&
                        t.ItemPrice > 0
                        ));
            }
            else
            {
                result.AddRange(ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true).Where(t =>
                        t.IsHiddenInRecommendDeal == false &&
                        Helper.IsFlagSet(t.GroupOrderStatus.GetValueOrDefault(), GroupOrderStatus.Completed) == false &&
                        t.OrderedQuantity < t.OrderTotalLimit &&
                        string.IsNullOrEmpty(t.AppDealPic) == false &&
                        t.ItemPrice > 0 &&
                        t.DeliveryType == (int)DeliveryType.ToHouse
                        ));
            }

            if (couponoDealGrossProfitLimit != null && homeDeliveryGrossProfitLimit != 0)
            {
                List<IViewPponDeal> filteredResult = new List<IViewPponDeal>();
                foreach (var deal in result)
                {
                    var grossMargin = PponFacade.GetDealMinimumGrossMargin(deal.BusinessHourGuid) * 100;
                    if (deal.DeliveryType.GetValueOrDefault() == (int)DeliveryType.ToHouse && grossMargin >= homeDeliveryGrossProfitLimit)
                    {
                        filteredResult.Add(deal);
                    }
                    else if (deal.DeliveryType.GetValueOrDefault() == (int)DeliveryType.ToShop && grossMargin >= couponoDealGrossProfitLimit)
                    {
                        filteredResult.Add(deal);
                    }
                }
                result = filteredResult;
            }
            return result;            
        }

        #region Product Feed Xml
        private static void CreatePponDealFeedXml(IList<IViewPponDeal> vpdc, PponDealFeedType feedType,
            FeedPriceStrategy priceStrategy, string feedFileName)
        {

            string feedFilePath = Path.Combine(Helper.MapPath(_config.SiteMapCreatePath, true), feedFileName); // 和Sitemap放相同位置

            string folder = Helper.MapPath("~/share/App_Data");
            if (Directory.Exists(folder) == false)
            {
                Directory.CreateDirectory(folder);
            }
            E7GoogleCategoryMapper mapper = new E7GoogleCategoryMapper(folder);

            try
            {
                XElement channelXml = new XElement("channel");
                XDocument xdoc = new XDocument(new XDeclaration("1.0", "utf-8", null));
                XNamespace gn = "http://base.google.com/ns/1.0";
                XElement root = new XElement("rss",
                    new XAttribute(XNamespace.Xmlns + "g", gn),
                    new XAttribute("version", "2.0"),
                    PponDealFeedPatternXml(vpdc, gn, channelXml, feedType, mapper, priceStrategy)
                );
                xdoc.Add(root);
                xdoc.Save(feedFilePath);
                if (feedType == (int)PponDealFeedType.Google)
                {
                    CompressXmlfile(Helper.MapPath(_config.SiteMapCreatePath, true), feedFileName); //compression
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("{0} {1}", feedType, ex.Message);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vpdc"></param>
        /// <param name="gn"></param>
        /// <param name="channelXml"></param>
        /// <param name="feedType"></param>
        /// <param name="mapper"></param>
        /// <returns></returns>
        private static XElement PponDealFeedPatternXml(IList<IViewPponDeal> vpdc, XNamespace gn, XElement channelXml,
            PponDealFeedType feedType, E7GoogleCategoryMapper mapper, FeedPriceStrategy priceStrategy)
        {
            channelXml = new XElement("channel");
            channelXml.Add(new XElement("title", "17Life，團購、宅配24小時出貨、優惠券、即買即用"));
            channelXml.Add(new XElement("link", "https://www.17life.com"));
            channelXml.Add(new XElement("discription", "17Life-吃喝玩樂團購3折起，全家便利商店5折專區免費索取兌換！全台美食餐券、旅遊住宿、宅配24H快速出貨、SPA專區，還有品生活獨享的頂級精品、用餐優惠，即買即用，盡在17Life與你一起享受生活！"));

            Dictionary<Guid, DealGoogleMetadata> dealGoogleMetadatas = _pep.DealGoogleMetadataGetDict();
            Dictionary<Guid, List<Guid>> pponOptionsItemGuids = _pp.PponOptionGetItemGuidDictByOnlineDeals();
            Dictionary<Guid, string> proposalMultiDealOptions = _sp.ProposalMultiDealGetOptionsDictByOnlineDeals();
            Dictionary<Guid, ViewProductItem> viewProductItems = _pp.ViewProductItemListGetByWithMpnOrGtin();
            Dictionary<int, string> gsaCodes = SystemCodeManager.GetDealType().ToDictionary(t => t.CodeId, t => t.Code);
            Dictionary<Guid, decimal> dealConverationRates = _pep.GetDealWeeklyConverationRateDict(DateTime.Today.AddDays(-1));

            for (int i = 0; i < vpdc.Count; i++)
            {
                IViewPponDeal vpd = vpdc[i];
                XElement itemXml = new XElement("item");
                string[] appDealPic = new string[2];
                string[] googleDealPic = new string[2];
                string description = string.Empty;
                decimal price;
                //特價
                decimal? salePprice = null;
                int? multipack = null;
                
                string title = string.Empty;
                string condition = string.Empty;
                //string productTypeString = string.Empty;

                string cidsString = string.Empty;
                string categoryName = string.Empty;
                int[] cids;

                List<Guid> sellerGuids = new List<Guid>();

                sellerGuids.Add(vpd.SellerGuid);
                //效能地獄
                List<ViewProductItem> vpi = PponFacade.GetProductItemGuidByBid(vpd.BusinessHourGuid, sellerGuids
                    , pponOptionsItemGuids, proposalMultiDealOptions, viewProductItems);

                description = ProposalFacade.RemoveSpecialCharacter(vpd.EventTitle);
                if (description != vpd.EventTitle)
                {
                    logger.WarnFormat("檔次 {0} 的 EventTitle 包含特殊別字元，請確認。", vpd.BusinessHourGuid);
                }

                //組數
                if (vpd.IsQuantityMultiplier.GetValueOrDefault() && vpd.QuantityMultiplier.GetValueOrDefault() > 1)
                {
                    multipack = vpd.QuantityMultiplier;
                }
                price = vpd.ItemPrice;
                title = vpd.ItemName;
                if (priceStrategy == FeedPriceStrategy.LowestAverageWithDiscountPrice)
                {
                    if (vpd.DiscountPrice != null)
                    {
                        price = vpd.DiscountPrice.Value;
                    }
                    else
                    {
                        price = vpd.ItemPrice;
                    }
                }
                else if (priceStrategy == FeedPriceStrategy.LowestComboDealPrice)
                {
                    if ((vpd.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                    {
                        var combodeals =
                            ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(vpd.BusinessHourGuid);
                        ViewComboDeal comboDeal = combodeals.Where(t => t.Slug == null && t.OrderedQuantity < t.OrderTotalLimit)
                            .OrderBy(t => t.Sequence).FirstOrDefault();
                        if (comboDeal != null)
                        {
                            price = comboDeal.ItemPrice;
                            title = comboDeal.Title;
                            //組數
                            if (comboDeal.IsQuantityMultiplier && comboDeal.QuantityMultiplier.GetValueOrDefault() > 1)
                            {
                                multipack = comboDeal.QuantityMultiplier;
                            }
                        }
                    }
                    salePprice = vpd.DiscountPrice;
                }

                if (title != ProposalFacade.RemoveSpecialCharacter(title))
                {
                    title = ProposalFacade.RemoveSpecialCharacter(title);
                    logger.WarnFormat("檔次 {0} 的 title 包含特殊別字元，請確認。", vpd.BusinessHourGuid);
                }

                if (vpd.DealType != null)
                {
                    categoryName = SystemCodeManager.GetBreadcrumbDealTypeNames(vpd.DealType.GetValueOrDefault());
                }
                if (vpd.ItemName.IndexOf("福利品", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    condition = "used";
                }
                else
                {
                    condition = "new";
                }
                if (!string.IsNullOrEmpty(vpd.CategoryList) && (vpd.CategoryList.Length > 2))
                {
                    cidsString = vpd.CategoryList.Substring(1, vpd.CategoryList.Length - 2);
                    cids = Array.ConvertAll(cidsString.Split(","), s => int.Parse(s));
                }
                else
                {
                    cids = new int[0];
                }                

                itemXml.Add(new XElement(gn + "id", vpd.BusinessHourGuid));
                if (vpd.IsWms)
                {
                    itemXml.Add(new XElement(gn + "title", "【24H】" + title));
                }
                else
                {
                    itemXml.Add(new XElement(gn + "title", title));
                }
                itemXml.Add(new XElement(gn + "description", description));

                if (feedType == PponDealFeedType.Google)
                {
                    if (vpd.IsWms)
                    {
                        itemXml.Add(new XElement(gn + "link", PponDealFeedLink(vpd.BusinessHourGuid, cids, feedType, "G_shopping24H")));
                    }
                    else
                    {
                        itemXml.Add(new XElement(gn + "link", PponDealFeedLink(vpd.BusinessHourGuid, cids, feedType, "G_shopping")));
                    }
                    itemXml.Add(new XElement(gn + "display_ads_link", PponDealFeedLink(vpd.BusinessHourGuid, cids, feedType, "G_gdn_rem")));
                }
                else if (feedType == PponDealFeedType.FB)
                {
                    itemXml.Add(new XElement(gn + "link", new XCData(PponDealFeedLink(vpd.BusinessHourGuid, cids, feedType, null))));
                }
                else if (feedType == PponDealFeedType.Yahoo)
                {
                    itemXml.Add(new XElement(gn + "link", PponDealFeedLink(vpd.BusinessHourGuid, cids, feedType, "YH_native")));
                }
                if (feedType == PponDealFeedType.Google || feedType == PponDealFeedType.FB)
                {
                    if (!string.IsNullOrEmpty(vpd.RemoveBgPic) && vpd.RemoveBgPic.Contains(","))
                    {
                        googleDealPic = vpd.RemoveBgPic.Split(",");
                        itemXml.Add(new XElement(gn + "image_link", string.Format("{0}/media/{1}/{2}", _config.SiteUrl, googleDealPic[0], googleDealPic[1])));
                    }
                    else
                    {
                        continue;
                        //if (!string.IsNullOrEmpty(vpd.AppDealPic) && vpd.AppDealPic.Contains(","))
                        //{
                        //    appDealPic = vpd.AppDealPic.Split(",");
                        //    itemXml.Add(new XElement(gn + "image_link", string.Format("{0}/media/{1}/{2}", _config.SiteUrl, appDealPic[0], appDealPic[1])));
                        //}
                        //else
                        //{
                        //    itemXml.Add(new XElement(gn + "image_link", vpd.AppDealPic));
                        //}
                    }
                }
                else if (feedType == PponDealFeedType.Yahoo)
                {
                    itemXml.Add(new XElement(gn + "image_link", PponFacade.GetPponDealFirstImagePath(vpd.EventImagePath, true)));
                }

                itemXml.Add(new XElement(gn + "availability", "in stock"));
                itemXml.Add(new XElement(gn + "condition", condition));
                //先從銷售分類取得
                int? googleProductCategory = GetGoogleProductCategoryFromDealType(vpd, gsaCodes);
                if (googleProductCategory == null)
                {
                    googleProductCategory = mapper.ConvertToGoogleCategory(cids);
                }
                itemXml.Add(new XElement(gn + "google_product_category",
                    googleProductCategory == null ? string.Empty : googleProductCategory.Value.ToString()));
                itemXml.Add(new XElement(gn + "product_type", categoryName));

                string brand = null;
                string mpn = null;
                string gtin = null;
                DealGoogleMetadata dealGoogleMetadata;
                if (dealGoogleMetadatas.TryGetValue(vpd.BusinessHourGuid, out dealGoogleMetadata))
                {
                    if (string.IsNullOrEmpty(dealGoogleMetadata.Brand) == false)
                    {
                        brand = dealGoogleMetadata.Brand;
                    }
                    if (string.IsNullOrEmpty(dealGoogleMetadata.Gtin) == false)
                    {
                        gtin = dealGoogleMetadata.Gtin;
                    }
                    if (string.IsNullOrEmpty(dealGoogleMetadata.Mpn) == false)
                    {
                        mpn = dealGoogleMetadata.Mpn;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(gtin))
                    {
                        var tmpgtin = vpi.FirstOrDefault(x => x.Gtins != "" & x.Gtins != null);
                        if (tmpgtin != null)
                        {
                            gtin = tmpgtin.Gtins;
                        }
                    }
                    if (string.IsNullOrEmpty(mpn))
                    {
                        var tmpMpn = vpi.FirstOrDefault(x => x.Mpn != "" & x.Mpn != null);
                        if (tmpMpn != null)
                        {
                            mpn = tmpMpn.Mpn;
                        }
                    }
                }
                if (string.IsNullOrEmpty(brand))
                {
                    brand = "17Life生活電商";
                }
                itemXml.Add(new XElement(gn + "brand", brand));
                if (string.IsNullOrEmpty(gtin) && string.IsNullOrEmpty(mpn))
                {
                    itemXml.Add(new XElement(gn + "identifier_exists", "false"));
                    itemXml.Add(new XElement(gn + "gtin", string.Empty));
                    itemXml.Add(new XElement(gn + "mpn", string.Empty));
                }
                else
                {
                    itemXml.Add(new XElement(gn + "gtin", gtin ?? string.Empty));
                    itemXml.Add(new XElement(gn + "mpn", mpn ?? string.Empty));
                }
                //custom_label_0 轉換率
                //轉換率1-2%、轉換率2-3%、轉換率3-4%、轉換率>4%
                
                //decimal weeklyConverationRate;
                //if (dealConverationRates.TryGetValue(vpd.BusinessHourGuid, out weeklyConverationRate))
                //{
                //    if (weeklyConverationRate < 0.01m)
                //    {
                //        itemXml.Add(new XElement(gn + "custom_label_0", string.Empty));
                //    }
                //    else if (weeklyConverationRate >= 0.01m && weeklyConverationRate < 0.02m)
                //    {
                //        itemXml.Add(new XElement(gn + "custom_label_0", "轉換率1-2%"));
                //    }
                //    else if (weeklyConverationRate >= 0.02m && weeklyConverationRate < 0.03m)
                //    {
                //        itemXml.Add(new XElement(gn + "custom_label_0", "轉換率2-3%"));
                //    }
                //    else if (weeklyConverationRate >= 0.03m && weeklyConverationRate < 0.04m)
                //    {
                //        itemXml.Add(new XElement(gn + "custom_label_0", "轉換率3-4%"));
                //    }
                //    else if (weeklyConverationRate > 0.04m)
                //    {
                //        itemXml.Add(new XElement(gn + "custom_label_0", "轉換率>4%"));
                //    }
                //}
                //else
                {
                    itemXml.Add(new XElement(gn + "custom_label_0", string.Empty));
                }
                //custom_label_1 毛利率
                
                var grossMargin = PponFacade.GetDealMinimumGrossMargin(vpd.BusinessHourGuid) * 100;
                //毛利普(毛利率12-15%)、毛利中(毛利率15-20%)、毛利高(毛利率20-25%)、毛利高高(毛利率>25%)
                if (grossMargin >= 12 && grossMargin < 15)
                {
                    itemXml.Add(new XElement(gn + "custom_label_1", "毛利普"));
                }
                else if (grossMargin >= 15 && grossMargin < 20)
                {
                    itemXml.Add(new XElement(gn + "custom_label_1", "毛利中"));
                }
                else if (grossMargin >= 20 && grossMargin < 25)
                {
                    itemXml.Add(new XElement(gn + "custom_label_1", "毛利高"));
                }
                else if (grossMargin >= 25)
                {
                    itemXml.Add(new XElement(gn + "custom_label_1", "毛利高高"));
                }
                else
                {
                    itemXml.Add(new XElement(gn + "custom_label_1", string.Empty));
                }

                if (vpd.IsWms)
                {
                    itemXml.Add(new XElement(gn + "custom_label_2", "是"));
                }
                else
                {
                    itemXml.Add(new XElement(gn + "custom_label_2", "否"));
                }

                itemXml.Add(new XElement(gn + "price", Convert.ToInt32(price) + " TWD"));
                if (salePprice != null)
                {
                    //itemXml.Add(new XElement(gn + "sale_price", Convert.ToInt32(salePprice.Value) + " TWD"));
                }
                //組數
                if (multipack.GetValueOrDefault() > 1)
                {
                    itemXml.Add(new XElement(gn + "multipack", multipack));
                }
                channelXml.Add(itemXml);
            }
            return channelXml;
        }

        private static int? GetGoogleProductCategoryFromDealType(IViewPponDeal deal, Dictionary<int, string> gsaCodes)
        {
            if (deal.DealTypeDetail != null)
            {
                if (gsaCodes.ContainsKey(deal.DealTypeDetail.Value))
                {
                    int gsaCode;
                    if (int.TryParse(gsaCodes[deal.DealTypeDetail.Value], out gsaCode))
                    {
                        return gsaCode;
                    }
                }
            }
            return null;
        }

        #endregion

        #region Product Feed Utility
        private static string ConvertHtmltoText(string htmltxt)
        {
            string text = string.Empty;
            text = Regex.Replace(HttpUtility.HtmlDecode(htmltxt), "<.*?>", string.Empty);
            text = new string(text.Where(c => !char.IsControl(c)).ToArray());
            text = text.Replace("\t", string.Empty);
            text = text.Replace("\r\n", string.Empty);

            return text;
        }

        private static string PponDealFeedLink(Guid bid, int[] cids, PponDealFeedType feedType, string rsrc)
        {
            if (feedType == PponDealFeedType.Google)
            {
                return string.Format("{0}/deal/{1}?rsrc={2}", _config.SiteUrl, bid, 
                    string.IsNullOrEmpty(rsrc) ? string.Empty : rsrc);
            }
            else if (feedType == PponDealFeedType.FB)
            {
                return string.Format("{0}/deal/{1}?rsrc=FB_DPA&utm_source=fb&utm_medium=DPA&utm_campaign={2}", _config.SiteUrl, bid, fbLinkUtmCampaign(bid, cids));
            }
            else if (feedType == PponDealFeedType.Yahoo)
            {
                return string.Format("{0}/deal/{1}", _config.SiteUrl, bid);
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion

        #region Facebook Utm Campaign
        private static string fbLinkUtmCampaign(Guid bid, int[] cids)
        {
            string fbLinkUtmCampaign = PponDealAndFBUtmCampaignMapping(cids);
            if (!string.IsNullOrEmpty(fbLinkUtmCampaign))
            {
                return fbLinkUtmCampaign + "_" + bid;
            }
            else
            {
                return string.Empty;
            }
        }

        private static string PponDealAndFBUtmCampaignMapping(int[] cids)
        {
            string fbUtmCampaignString = string.Empty;
            List<string> fbUtmCampaignMain = new List<string>();
            List<string> fbUtmCampaignArea = new List<string>();
            string tempFbUtmCampaign;

            if (Array.Exists(cids, x => x == 88) || Array.Exists(cids, x => x == 89) || Array.Exists(cids, x => x == 90) || Array.Exists(cids, x => x == 148))
            {
                //Main
                foreach (int s in cids)
                {
                    tempFbUtmCampaign = fbUtmCampaignMainHardCode(s);
                    if (!string.IsNullOrEmpty(tempFbUtmCampaign))
                    {
                        fbUtmCampaignMain.Add(tempFbUtmCampaign);
                    }
                }
            }
            else if (Array.Exists(cids, x => x == 87))
            {
                //Area
                foreach (int s in cids)
                {
                    tempFbUtmCampaign = fbUtmCampaignAreaHardCode(s);
                    if (!string.IsNullOrEmpty(tempFbUtmCampaign))
                    {
                        fbUtmCampaignArea.Add(tempFbUtmCampaign);
                    }
                }
            }

            if (fbUtmCampaignMain.Count > 0)
            {
                return fbUtmCampaignMain[0];
            }
            else if (fbUtmCampaignArea.Count > 0)
            {
                return fbUtmCampaignArea[0];
            }
            else
            {
                return string.Empty;
            }
        }

        private static string fbUtmCampaignMainHardCode(int cid)
        {
            switch (cid)
            {
                case 88:     //宅配
                    return "d";

                case 89:     //玩美‧休閒   
                    return "beauty";

                case 90:     //旅遊
                    return "trip";

                case 148:     //品生活 
                    return "piin";

                default:
                    return string.Empty;
            }
        }

        private static string fbUtmCampaignAreaHardCode(int cid)
        {
            switch (cid)
            {
                case 94:     //台北
                    return "tp";

                case 95:     //桃園  
                    return "ty";

                case 96:     //竹苗
                    return "sc";

                case 97:     //中彰 
                    return "tc";

                case 98:     //嘉南
                    return "tn";

                case 99:     //高屏
                    return "ks";

                default:
                    return string.Empty;
            }
        }
        #endregion

        #region Google Category
        private static string PponDealAndGoogleCategoryMapping(int[] cids)
        {
            string googleCategoryString = string.Empty;
            int[] googleCategory = new int[cids.Count()];
            List<int> newGoogleCategory = new List<int>();
            int tempGoogleCategory;

            foreach (int s in cids)
            {
                tempGoogleCategory = GoogleCategoryHardCode(s);
                if (tempGoogleCategory != 0)
                {
                    newGoogleCategory.Add(tempGoogleCategory);
                }
            }

            if (newGoogleCategory.Count > 0)
            {
                if (newGoogleCategory[0] != 0)
                {
                    return newGoogleCategory[0].ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        private static int GoogleCategoryHardCode(int cid)
        {
            //資料來源 https://docs.google.com/spreadsheets/d/1iCz-k62ili42PGGVVsgho9aMLPB9_aOsfG2zqniepcs/edit#gid=1126580700
            switch (cid)
            {
                case 110:     //休憩    
                    return 53;

                case 129:     //特色民宿    
                    return 53;

                case 130:     //湯泉之旅    
                    return 53;

                case 149:     //五星美饌    
                    return 422;

                case 150:     //餐廳美食    
                    return 422;

                case 151:     //甜點飲品    
                    return 422;

                case 152:     //頂級食材    
                    return 422;

                case 153:     //精品配件    
                    return 167;

                case 154:     //彩妝保養    
                    return 567;

                case 155:     //沐浴香氛    
                    return 5663;

                case 156:     //美體舒壓    
                    return 5663;

                case 157:     //設計好物    
                    return 536;

                case 172:     //餐廳美食    
                    return 422;

                case 173:     //甜點飲品    
                    return 422;

                case 174:     //美髮沙龍    
                    return 486;

                case 177:     //展演票券    
                    return 53;

                case 179:     //美顏SPA    
                    return 5663;

                case 180:     //美體SPA    
                    return 5663;

                case 181:     //美髮沙龍    
                    return 486;

                case 182:     //美甲美睫    
                    return 478;

                case 183:     //運動健身    
                    return 990;

                case 209:     //中式    
                    return 422;

                case 210:     //西式    
                    return 422;

                case 211:     //異國    
                    return 422;

                case 212:     //火鍋    
                    return 422;

                case 213:     //燒烤    
                    return 422;

                case 214:     //吃到飽    
                    return 422;

                case 215:     //飲料甜點    
                    return 1876;

                case 216:     //輕食下午茶    
                    return 422;

                case 217:     //烘培蛋糕    
                    return 1876;

                case 218:     //展演票券    
                    return 53;

                case 219:     //休閒娛樂    
                    return 53;

                case 223:     //飯店旅館    
                    return 53;

                case 713:     //日式    
                    return 422;

                case 721:     //活力養生    
                    return 5611;

                case 722:     //室內居家    
                    return 536;

                case 725:     //花藝禮品    
                    return 2559;

                case 730:     //季節賞味    
                    return 422;

                case 743:     //生活家電    
                    return 604;

                case 744:     //廚房家電    
                    return 730;

                case 745:     //美容家電    
                    return 2915;

                case 747:     //童裝鞋包    
                    return 182;

                case 748:     //童書玩具    
                    return 1253;

                case 749:     //寵物用品    
                    return 2;

                case 752:     //冷凍調理    
                    return 5814;

                case 753:     //生猛海鮮    
                    return 4629;

                case 754:     //蔬菜水果    
                    return 430;

                case 755:     //在地美食    
                    return 422;

                case 757:     //飲料/沖泡    
                    return 413;

                case 759:     //進口零食    
                    return 423;

                case 762:     //收納達人    
                    return 636;

                case 763:     //日用耗品    
                    return 630;

                case 764:     //居家用品    
                    return 630;

                case 767:     //時尚彩妝    
                    return 477;

                case 768:     //臉部保養    
                    return 567;

                case 769:     //美體保養    
                    return 473;

                case 782:     //義式    
                    return 422;

                case 784:     //免預約    
                    return 422;

                case 787:     //假日不加價    
                    return 53;

                case 788:     //賀歲．迎春精選    
                    return 422;

                case 793:     //疲憊感OUT    
                    return 5663;

                case 795:     //台北威斯汀六福皇宮    
                    return 422;

                case 796:     //聚餐首選    
                    return 422;

                case 797:     //飯店美食    
                    return 422;

                case 801:     //Motel    
                    return 53;

                case 808:     //早午餐    
                    return 422;

                case 825:     //品牌連鎖    
                    return 422;

                case 826:     //假日可用    
                    return 422;

                case 827:     //主題特色餐廳    
                    return 422;

                case 840:     //肉食主義    
                    return 422;

                case 848:     //一日遊    
                    return 53;

                case 853:     //內睡衣/塑身    
                    return 213;

                case 854:     //穿搭配件    
                    return 166;

                case 859:     //養生蔬食    
                    return 422;

                case 862:     //蔬活食尚    
                    return 422;

                case 863:     //17來包棟    
                    return 53;

                case 865:     //盛夏野餐日    
                    return 422;

                case 867:     //國外渡假去    
                    return 53;

                case 883:     //異國零食    
                    return 423;

                case 885:     //BUFFET 無限饗宴    
                    return 422;

                case 890:     //抵用券    
                    return 422;

                case 893:     //衛生紙/尿布專區    
                    return 537;

                case 900:     //汽車美容    
                    return 913;

                case 901:     //親子寵物    
                    return 53;

                case 902:     //整復推拿    
                    return 5663;

                case 903:     //課程服務    
                    return 53;

                case 907:     //行動電源    
                    return 276;

                case 2166:     //展演票券
                    return 53;

                case 2167:     //餐券
                    return 53;

                case 2168:     //遊樂票券
                    return 53;

                case 2169:     //展演票券
                    return 53;

                case 2170:     //餐券
                    return 53;

                case 2171:     //遊樂票券
                    return 53;

                case 2179:     //影音設備
                    return 222;

                case 2182:     //儲存卡碟
                    return 2414;

                case 2230:     //金燦耶誕
                    return 422;

                case 2232:     //手機
                    return 267;

                case 2233:     //穿戴裝置
                    return 222;

                case 2235:     //床包/床罩
                    return 569;

                case 2236:     //棉被/涼被/毯類/睡袋
                    return 1985;

                case 2237:     //枕頭
                    return 2700;

                case 2238:     //保潔墊/床墊
                    return 4420;

                case 2239:     //傢俱/家飾/收納
                    return 436;

                case 2242:     //婚宴專區
                    return 5441;

                case 2246:     //雨天專屬
                    return 630;

                case 2252:     //美齒潔牙
                    return 526;

                case 2254:     //窈窕保健
                    return 5611;

                case 2255:     //美肌美顏
                    return 5611;

                case 2256:     //養生調理
                    return 5611;

                case 2257:     //銀髮保健
                    return 5611;

                case 2258:     //男性保健
                    return 5611;

                case 2259:     //媽咪幼童保健
                    return 5611;

                case 2260:     //美髮造型
                    return 2441;

                case 2261:     //國泰商旅專區 台北慕軒&和逸
                    return 422;

                case 2262:     //蛋糕/點心
                    return 1876;

                case 2265:     //米/麵
                    return 431;

                case 2266:     //食材/南北貨
                    return 422;

                case 2267:     //果乾/堅果
                    return 433;

                case 2268:     //即食美味
                    return 5814;

                case 2269:     //生鮮肉品
                    return 4628;

                case 2270:     //嬰幼兒生活用品
                    return 537;

                case 2290:     //國賓大飯店
                    return 422;

                case 2292:     //假日適用
                    return 422;

                case 100097:  //電風扇
                    return 608;

                case 100098:  //液晶電視
                    return 405;

                case 100102:  //平板筆電
                    return 4745;

                case 100103:  //手機殼
                    return 2353;

                case 100104:  //保護貼
                    return 2353;

                case 100105:  //喇叭
                    return 1420;

                case 100106:  //耳機
                    return 229;

                case 100107:  //相機
                    return 141;

                case 100108:  //網路設備
                    return 342;

                case 100109:  //USB周邊
                    return 279;

                case 100110:  //冷暖空調
                    return 1626;

                case 100111:  //洗/乾衣機
                    return 2706;

                case 100112:  //電冰箱
                    return 686;

                case 100113:  //電鍋/電子鍋
                    return 4720;

                case 100115:  //除濕機
                    return 607;

                case 100116:  //吸塵器
                    return 619;

                case 100117:  //電蚊拍/捕蚊燈
                    return 728;

                case 100118:  //烤箱/微波爐
                    return 683;

                case 100120:  //印表機
                    return 351;

                case 100121:  //錄音筆
                    return 244;

                case 100128:  //成套票券
                    return 53;

                case 100129:  //成套票券
                    return 53;

                case 100131:  //女裝上著
                    return 1604;

                case 100132:  //女裝下著
                    return 1604;

                case 100133:  //洋裝/套裝/外套
                    return 1604;

                case 100134:  //運動機能
                    return 5322;

                case 100135:  //潮流男裝
                    return 1604;

                case 100136:  //男仕內著
                    return 1604;

                case 100137:  //時尚鞋款
                    return 187;

                case 100138:  //包包/皮夾
                    return 3032;

                case 100139:  //襪類/內搭褲
                    return 213;

                case 100140:  //品牌outlet
                    return 166;

                case 100141:  //廚房用品
                    return 638;

                case 100142:  //衛浴用品
                    return 574;

                case 100143:  //清潔/防霉
                    return 623;

                case 100144:  //露營/戶外活動
                    return 630;

                case 100145:  //旅遊休閒
                    return 630;

                case 100146:  //美體塑身
                    return 990;

                case 100148:  //保冷/保溫瓶
                    return 3800;

                case 100151:  //交通租賃
                    return 53;

                case 100152:  //活動行程
                    return 53;

                case 100154:  //吹風機
                    return 490;

                case 100155:  //刮鬍刀
                    return 532;

                case 100156:  //舒壓按摩
                    return 471;

                case 100157:  //投影機
                    return 397;

                case 100158:  //滑鼠
                    return 304;

                case 100159:  //鍵盤
                    return 303;

                case 100160:  //充電線
                    return 259;

                case 100161:  //電腦設備
                    return 278;

                case 100163:  //果汁機/調理機
                    return 750;

                case 100164:  //咖啡機
                    return 736;

                case 100165:  //檯燈/照明設備
                    return 4636;

                case 100167:  //電動牙刷
                    return 527;

                case 100172:  //自拍神器
                    return 143;

                case 100173:  //充電頭
                    return 277;

                case 100174:  //點心機
                    return 5289;

                case 100175:  //飲水設備
                    return 3293;

                case 100189:  //行車紀錄器
                    return 222;

                case 100190:  //衛星導航
                    return 1891;

                case 100191:  //防雨用品
                    return 3927;

                case 100192:  //機車/相關周邊
                    return 5511;

                case 100193:  //自行車/周邊
                    return 3214;

                case 100194:  //汽車百貨
                    return 2870;

                case 100195:  //車用3C
                    return 890;

                case 100196:  //安全帽
                    return 2110;

                case 100198:  //電暖器/電毯
                    return 611;

                case 100199:  //電磁爐
                    return 747;

                case 100201:  //辦公室設備
                    return 922;

                case 100202:  //熨斗/掛燙機
                    return 2706;

                case 100206:  //成套票券
                    return 53;

                case 100208:  //清淨機
                    return 606;

                case 100210:  //大尺碼專區
                    return 1604;

                default:
                    return 0;
            }
        }
        #endregion

        #endregion

        /// <summary>
        /// 產生折價券通知信
        /// </summary>
        /// <param name="eventName">活動名稱</param>
        /// <param name="code">折價券序號</param>
        /// <param name="amount">折價券金額</param>
        /// <param name="endDate">折價券使用截止日期</param>
        /// <returns></returns>
        public static string GetEventCoupontSendMailTemplate(string member_name, PromoItem item, EventActivity activity)
        {
            EventCoupontSendMail template = TemplateFactory.Instance().GetTemplate<EventCoupontSendMail>();
            template.ServerConfig = _config;
            template.MemberName = !string.IsNullOrWhiteSpace(member_name) ? member_name : I18N.Phrase.Member;
            template.EventName = activity.Eventname;
            template.Code = item.Code;
            template.StartDate = activity.StartDate.ToString("yyyy/MM/dd HH:mm");
            template.EndDate = activity.EndDate.AddSeconds(-1).ToString("yyyy/MM/dd HH:mm");
            template.Explanation = HttpUtility.HtmlDecode(activity.Description1);
            return template.ToString();
        }

        public static void GenerateEventPromoItemByPeriod(string event_code, DateTime dateStart, DateTime dateEnd)
        {
            EventPromoCollection commList = _pp.GetEventPromoList(EventPromoTemplateType.Commercial); // 取出event_promo建好的各館別資料
            CategoryCollection cates = _sp.CategoryGetList((int)CategoryType.CommercialCircle);
            CategoryDealCollection cdcs = _pp.CategoryDealsGetList(dateStart, dateEnd.AddDays(1), CategoryType.CommercialCircle);
            CategoryVourcherCollection cvcs = _pp.CategoryVourcherGetList(dateStart, dateEnd, CategoryType.CommercialCircle);
            foreach (EventPromo comm in commList)
            {
                int code = int.TryParse(comm.Url.Replace(event_code, string.Empty), out code) ? code : 0;
                if (code != 0)
                {
                    int cateId = cates.Where(x => x.Code.Value == code).FirstOrDefault().Id;

                    #region get ppon category deals

                    EventPromoItemCollection exist_items = _pp.GetEventPromoItemList(comm.Id, EventPromoItemType.Ppon);
                    int seq = 1;

                    foreach (CategoryDeal deal in cdcs.Where(x => x.Cid == cateId))
                    {
                        ViewPponDeal vpd = _pp.ViewPponDealGet(ViewPponDeal.Columns.BusinessHourGuid + "=" + deal.Bid);
                        if (vpd.IsLoaded && vpd.UniqueId.HasValue && ((vpd.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) == 0))
                        {
                            if (!exist_items.Any(x => x.ItemId == vpd.UniqueId.Value))
                            {
                                EventPromoItem promo_item = new EventPromoItem();
                                promo_item.Seq = seq;
                                promo_item.MainId = comm.Id;
                                promo_item.ItemId = vpd.UniqueId.Value;
                                promo_item.Title = vpd.ItemName;
                                promo_item.Description = vpd.EventTitle;
                                promo_item.Status = (vpd.BusinessHourOrderTimeS.Date <= dateEnd.Date && dateEnd.Date <= vpd.BusinessHourOrderTimeE.Date);
                                promo_item.ItemType = (int)EventPromoItemType.Ppon;
                                promo_item.Cdt = DateTime.Now;
                                promo_item.Creator = "sys";
                                _pp.SaveEventPromoItem(promo_item);
                                seq++;
                            }
                        }
                    }
                    foreach (EventPromoItem item in exist_items.OrderBy(x => x.Seq))
                    {
                        IViewPponDeal exist_vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(item.ItemId);
                        if (exist_vpd.BusinessHourOrderTimeE < DateTime.Now)
                        {
                            // 已結檔的設定狀態為關閉
                            item.Status = false;
                            item.Seq = 9999;
                        }
                        else
                        {
                            // 已存在的promo_item需重新排序
                            item.Seq = seq;
                            if (exist_vpd.BusinessHourOrderTimeS.Date == DateTime.Now.Date)
                            {
                                item.Status = true;
                            }
                            seq++;
                        }
                        _pp.SaveEventPromoItem(item);
                    }

                    #endregion

                    #region get vourcher category deals

                    EventPromoItemCollection v_exist_items = _pp.GetEventPromoItemList(comm.Id, EventPromoItemType.Vourcher);
                    int v_seq = 1;
                    foreach (CategoryVourcher cv in cvcs.Where(x => x.Cid == cateId))
                    {
                        VourcherEvent vourcher = _ep.VourcherEventGetById(cv.VourcherId);
                        if (vourcher.IsLoaded && vourcher.Status == (int)VourcherEventStatus.EventChecked)
                        {
                            if (!v_exist_items.Any(x => x.ItemId == vourcher.Id))
                            {
                                EventPromoItem promo_item = new EventPromoItem();
                                LunchKingSite.DataOrm.Seller seller = _sp.SellerGet(vourcher.SellerGuid);
                                promo_item.Seq = v_seq;
                                promo_item.MainId = comm.Id;
                                promo_item.ItemId = vourcher.Id;
                                promo_item.Title = seller.SellerName;
                                promo_item.Description = vourcher.Contents;
                                promo_item.Status = vourcher.StartDate <= dateEnd;
                                promo_item.ItemType = (int)EventPromoItemType.Vourcher;
                                promo_item.Cdt = DateTime.Now;
                                promo_item.Creator = "sys";
                                _pp.SaveEventPromoItem(promo_item);
                                v_seq++;
                            }
                        }
                    }
                    foreach (EventPromoItem item in v_exist_items.OrderBy(x => x.Seq))
                    {
                        VourcherEvent exist_vourcher = _ep.VourcherEventGetById(item.ItemId);
                        if (((DateTime)exist_vourcher.EndDate).AddDays(1) < DateTime.Now)
                        {
                            // 已結檔的設定狀態為關閉
                            item.Status = false;
                            item.Seq = 9999;
                        }
                        else
                        {
                            // 已存在的promo_item需重新排序
                            item.Seq = v_seq;

                            if (exist_vourcher.StartDate.HasValue && exist_vourcher.StartDate.GetValueOrDefault().Date == DateTime.Now.Date)
                            {
                                item.Status = true;
                            }

                            v_seq++;
                        }
                        _pp.SaveEventPromoItem(item);
                    }

                    #endregion



                }
            }
        }


        #region 商品主題活動相關

        /// <summary>
        /// 以活動的id查詢 活動進行中的eventPromo資料
        /// 進行中之定義:現在日期為startDate到endDate間，且status為true
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public static EventPromo EventPromoGetByIdInProgress(int eventId)
        {
            DateTime sysTime = DateTime.Now;
            EventPromoCollection eventPromos = _pp.EventPromoGetList(EventPromo.Columns.Id + " = " + eventId,
                                           EventPromo.Columns.Status + " = " + true,
                                           EventPromo.Columns.StartDate + " <= " + sysTime.Date,
                                           EventPromo.Columns.EndDate + " >= " + sysTime.Date, EventPromo.Columns.Type + "=" + (int)EventPromoType.Ppon);
            if (eventPromos.Count > 0)
            {
                return eventPromos.First();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 取得主要且進行中的商品活動資料(主要活動意思為 parentId 為 null的)
        /// </summary>
        /// <param name="forApp">是否只限定APP需要的</param>
        /// <returns></returns>
        public static EventPromoCollection EventPromoGetMainListInProgress(bool forApp)
        {
            DateTime sysTime = DateTime.Now;
            List<string> condition = new List<string>()
                {
                    EventPromo.Columns.StartDate + " <= " + sysTime,
                    EventPromo.Columns.EndDate + " >= " + sysTime,
                    EventPromo.Columns.Status + " = " + true,
                    EventPromo.Columns.ParentId + " is null",
                    EventPromo.Columns.Type+"="+(int)EventPromoType.Ppon
                };

            if (forApp)
            {
                string type = string.Join(",",
                                          AppEventPromoTemplateTypes.ConvertAll(x => ((int)x).ToString()).ToArray());
                condition.Add(EventPromo.Columns.TemplateType + " in " + type);
                condition.Add(EventPromo.Columns.ShowInApp + "= true");
            }

            EventPromoCollection eventPromos = _pp.EventPromoGetList(condition.ToArray());
            return eventPromos;


        }



        /// <summary>
        /// 取出商品主題活動的進行中的子項目，也就是parent_id欄位等於傳入參數的項目，且status狀態為true，目前時間也位於活動進行中
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="forApp"></param>
        /// <returns></returns>
        public static EventPromoCollection EventPromoGetSubListInProgress(int parentId, bool forApp)
        {
            DateTime sysTime = DateTime.Now;
            List<string> condition = new List<string>()
                {
                    EventPromo.Columns.StartDate + " <= " + sysTime.Date,
                    EventPromo.Columns.EndDate + " >= " + sysTime.Date,
                    EventPromo.Columns.Status + " = " + true,
                    EventPromo.Columns.ParentId + " = " + parentId,
                    EventPromo.Columns.Type+"="+(int)EventPromoType.Ppon
                };

            if (forApp)
            {
                string type = string.Join(",",
                                          AppEventPromoTemplateTypes.ConvertAll(x => ((int)x).ToString()).ToArray());
                condition.Add(EventPromo.Columns.TemplateType + " in " + type);
            }

            EventPromoCollection eventPromos = _pp.EventPromoGetList(condition.ToArray());
            return eventPromos;
        }


        public static SkmEventPromoInvoiceContentCollection GetSkmEventPromoInvoiceContentCollection()
        {
            return _ep.SkmEventPromoInvoiceContentCollectionGet();
        }

        public static DiscountEventPromoLinkCollection DiscountEventIdGetList(int id)
        {
            return _op.DiscountEventIdGetList(id);
        }

        public static void SetSkmEventPromoInvoiceContent(string cardNumber, string storeName, string invoiceNumber, string contactUserName, string contactAddress, string contactMobile, string userName)
        {
            SkmEventPromoInvoiceContent skmEventPromoInvoice = new SkmEventPromoInvoiceContent();

            skmEventPromoInvoice.TaishinCardNumber = cardNumber;
            skmEventPromoInvoice.SellerName = storeName;
            skmEventPromoInvoice.InvoiceNumber = invoiceNumber;
            skmEventPromoInvoice.LoginUserName = contactUserName;
            skmEventPromoInvoice.LoginUserAddress = contactAddress;
            skmEventPromoInvoice.LoginUserMobile = contactMobile;
            skmEventPromoInvoice.CreateTime = DateTime.Now;
            skmEventPromoInvoice.UniqueId = _mp.MemberUniqueIdGet(userName);

            _ep.SkmEventPromoInvoiceContentSet(skmEventPromoInvoice);

        }

        /// <summary>
        /// 依據傳入的商品主題活動 id 與 類型 取出此商品主題活動下EventPromoItem資料，只取出團購商品的部分
        /// </summary>
        /// <param name="mainId">商品主題活動頁的編號</param>
        /// <param name="type">商品類型</param>
        /// <returns></returns>
        public static EventPromoItemCollection EvnetPromoItemGetByMainId(int mainId, EventPromoItemType type)
        {
            EventPromoItemCollection itemCollection = _pp.EventPromoItemGetList(EventPromoItem.Columns.MainId + " = " + mainId,
                                           EventPromoItem.Columns.Status + " = " + true,
                                           EventPromoItem.Columns.ItemType + " = " + type);
            return itemCollection;
        }

        public static ViewEventPromoCollection GetViewEventPromoListByUrlAndItemDate(string url, DateTime date, bool? status)
        {
            List<string> paramsList = new List<string>();
            paramsList.Add(string.Format("{0} = {1}", ViewEventPromo.Columns.Url, url));
            paramsList.Add(string.Format("{0} <= {1}", ViewEventPromo.Columns.ItemStartDate, date.Date));
            paramsList.Add(string.Format("{0} >= {1}", ViewEventPromo.Columns.ItemEndDate, date.Date));

            if (status.HasValue)
            {
                paramsList.Add(string.Format("{0} = {1}", ViewEventPromo.Columns.Status, status));
            }

            return _pp.ViewEventPromoGetList(0, 0, ViewEventPromo.Columns.EventId + "," + ViewEventPromo.Columns.Seq,
                                      paramsList.ToArray());
        }

        public static ViewEventPromoVourcherCollection ViewEventPromoVourcherGetListByUrlAndItemDate(string url,
                                                                                                     DateTime date,
                                                                                                     bool? status)
        {
            List<string> paramsList = new List<string>();
            paramsList.Add(string.Format("{0} = {1}", ViewEventPromoVourcher.Columns.Url, url));
            paramsList.Add(string.Format("{0} <= {1}", ViewEventPromoVourcher.Columns.ItemStartDate, date.Date));
            paramsList.Add(string.Format("{0} >= {1}", ViewEventPromoVourcher.Columns.ItemEndDate, date.Date));
            if (status.HasValue)
            {
                paramsList.Add(string.Format("{0} = {1}", ViewEventPromoVourcher.Columns.PromoStatus, status));
            }

            return _pp.ViewEventPromoVourcherGetList(0, 0, ViewEventPromoVourcher.Columns.Id + "," + ViewEventPromoVourcher.Columns.Seq,
                                      paramsList.ToArray());
        }

        private static ViewEventPromo GenerationViewEventPromo(EventPromo eventPromo, ViewEventPromoCategory vepCategory)
        {
            ViewEventPromo vep = new ViewEventPromo();
            vep.EventId = eventPromo.Id;
            vep.EventTitle = eventPromo.Title;
            vep.Url = eventPromo.Url;
            vep.Cpa = eventPromo.Cpa;
            vep.StartDate = eventPromo.StartDate;
            vep.EndDate = eventPromo.EndDate;
            vep.MainPic = eventPromo.MainPic;
            vep.BgPic = eventPromo.BgPic;
            vep.BgColor = eventPromo.BgColor;
            vep.EventDescription = eventPromo.Description;
            vep.EventStatus = eventPromo.Status;
            vep.BtnOriginal = eventPromo.BtnOriginal;
            vep.BtnHover = eventPromo.BtnHover;
            vep.BtnActive = eventPromo.BtnActive;
            vep.BtnFontColor = eventPromo.BtnFontColor;
            vep.BusinessHourGuid = vepCategory.BusinessHourGuid;
            vep.Title = vepCategory.Title;
            vep.Description = vepCategory.Description;
            vep.Seq = (int)vepCategory.Seq;
            vep.Status = true;
            vep.ItemId = vepCategory.ItemId;
            vep.BusinessHourOrderTimeS = vepCategory.BusinessHourOrderTimeS;
            vep.BusinessHourOrderTimeE = vepCategory.BusinessHourOrderTimeE;
            vep.ItemOrigPrice = vepCategory.ItemOrigPrice;
            vep.ItemPrice = vepCategory.ItemPrice;
            vep.Discount = vepCategory.Discount;
            vep.ImagePath = vepCategory.ImagePath;

            return vep;
        }

        /// <summary>
        /// 依據eventPromo資料，查詢 此商品主題活動 狀態為 status = true 的團購資料
        /// </summary>
        /// <param name="eventPromo"></param>
        /// <returns></returns>
        public static ViewEventPromoCollection ViewEventPromoGetList(EventPromo eventPromo)
        {
            ViewEventPromoCollection vepc = new ViewEventPromoCollection();
            if (string.IsNullOrEmpty(eventPromo.BindCategoryList))
            {
                vepc = _pp.ViewEventPromoGetList(eventPromo.Id);
            }
            else
            {
                List<int> categoryList = eventPromo.BindCategoryList.Split(',').Select(int.Parse).ToList();
                ViewEventPromoCategoryCollection vepCategoryCol = _pp.ViewEventPromoCategoryGetList(categoryList);

                foreach (ViewEventPromoCategory vepCategory in vepCategoryCol)
                {
                    vepc.Add(GenerationViewEventPromo(eventPromo, vepCategory));
                }
            }
            return vepc;
        }

        /// <summary>
        /// 取得商品主題活動/策展1.0版型頁面名稱
        /// </summary>
        /// <param name="promo"></param>
        /// <returns></returns>
        public static string GetCurationPageName(EventPromo promo)
        {
            var pageName = string.Empty;
            switch ((EventPromoTemplateType)promo.TemplateType)
            {
                case EventPromoTemplateType.PponOnly:
                    pageName = "Exhibition";
                    break;
                case EventPromoTemplateType.JoinPiinlife:
                    pageName = "ExhibitionJoinPiinlife";
                    break;
                case EventPromoTemplateType.Commercial:
                    pageName = "ExhibitionCommercial";
                    break;
                case EventPromoTemplateType.Kind:
                    pageName = "ExhibitionKind";
                    break;
                case EventPromoTemplateType.Zero:
                    pageName = "ExhibitionZero";
                    break;
                case EventPromoTemplateType.SkmEvent:
                    pageName = "SkmExhibitionCommercial";
                    break;
                case EventPromoTemplateType.VoteMix:
                    pageName = "ExhibitionVote";
                    break;
                case EventPromoTemplateType.NewJoinPiinlife:
                    pageName = "ExhibitionJoinPiinlife";
                    break;
                case EventPromoTemplateType.OnlyNewPiinlife:
                    pageName = "ExhibitionNewPiinlife";
                    break;
            }
            return pageName;
        }

        /// <summary>
        /// 取得商品主題活動/策展1.0預覽連結
        /// </summary>
        /// <param name="promo"></param>
        /// <returns></returns>
        public static string GetCurationPreviewLink(EventPromo promo)
        {
            var pageName = GetCurationPageName(promo);
            return string.Format("{0}/event/{1}.aspx?u={2}&p=show_me_the_preview", _config.SiteUrl, pageName, promo.Url);
        }

        /// <summary>
        /// 取得商品主題活動/策展1.0連結
        /// </summary>
        /// <param name="promo"></param>
        /// <returns></returns>
        public static string GetCurationLink(EventPromo promo)
        {
            var pageName = GetCurationPageName(promo);
            var rsrc = !string.IsNullOrEmpty(promo.Cpa) ? "&rsrc=" + promo.Cpa : string.Empty;
            return string.Format("{0}/event/{1}.aspx?u={2}{3}&type={4}&eventid={5}",
                _config.SiteUrl, pageName, promo.Url, rsrc, (int)promo.Type, promo.Id);
        }

        #endregion 商品主題活動相關

        #region 策展2.0

        /// <summary>
        /// 取得策展2.0連結
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        public static string GetCurationTwoLink(Brand brand)
        {
            var cpa = !string.IsNullOrEmpty(brand.Cpa) ? "cpa=" + brand.Cpa + "&" : string.Empty;
            return string.Format("{0}/event/brandevent/{1}?{2}type={3}&eventid={4}",
                _config.SiteUrl, brand.Url, cpa, (int)EventPromoEventType.CurationTwo, brand.Id);
        }

        /// <summary>
        /// 取得策展2.0連結
        /// </summary>
        /// <param name="brandId"></param>
        /// <param name="cpa"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetCurationTwoLink(int brandId, string cpa, string url)
        {
            var cpaUrl = !string.IsNullOrEmpty(cpa) ? "cpa=" + cpa + "&" : string.Empty;
            return string.Format("{0}/event/brandevent.aspx?u={1}&{2}type={3}&eventid={4}",
                _config.SiteUrl, url, cpaUrl, (int)EventPromoEventType.CurationTwo, brandId);
        }

        public static DiscountCampaignCollection DiscountCampaignGetList(int brandId)
        {
            Brand brand = _pp.GetBrand(brandId);
            if (!string.IsNullOrEmpty(brand.DiscountList))
            {
                return _op.DiscountCampaignGetList(brand.DiscountList.Split(",").Select(int.Parse).ToList());
            }
            else
            {
                return null;
            }
        }

        public static DiscountCampaignCollection DiscountCampaignGetList(Brand brand)
        {
            if (!string.IsNullOrEmpty(brand.DiscountList))
            {
                return _op.DiscountCampaignGetList(brand.DiscountList.Split(",").Select(int.Parse).ToList());
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 取得策展折價券列表(若有userId則包含領取狀態)
        /// </summary>
        /// <param name="curationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static List<CurationDiscountCampaign> GetCurationDiscountCampaignList(int curationId, int? userId = null)
        {
            var campaigns = new List<CurationDiscountCampaign>();

            var dcList = GetDiscountCampaignListByEvent(EventPromoEventType.CurationTwo, curationId);

            foreach (var dc in dcList)
            {
                var statusType = GetDiscountCampaignStatus(dc, userId);
                if (statusType == DiscountCampaignStatus.Expired)
                {
                    continue; //過期不顯示至前台
                }

                var cdc = new CurationDiscountCampaign
                {
                    DiscountId = dc.Id,
                    Amount = dc.Amount ?? 0,
                    LimitedQty = dc.Qty ?? 0,
                    IsShowLimitedQty = dc.Qty <= _config.DisciuntLimitedCount,
                    MinimumAmount = dc.MinimumAmount ?? 0,
                    StatusType = (int)statusType
                };
                campaigns.Add(cdc);
            }

            return campaigns.OrderBy(x => x.Amount).ToList();
        }

        public static bool SimpleCheckAnyDiscountCampaignForCuration(int curationId)
        {
            var campaigns = new List<CurationDiscountCampaign>();

            var dcList = GetDiscountCampaignListByEvent(EventPromoEventType.CurationTwo, curationId);

            foreach (var dc in dcList)
            {
                if (dc.EndTime < DateTime.Now)
                {
                    continue;
                }
                return true;
            }

            return false;
        }

        /// <summary>
        /// 取得折價券領取狀態
        /// </summary>
        /// <param name="dc"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DiscountCampaignStatus GetDiscountCampaignStatus(DiscountCampaign dc, int? userId = null)
        {
            if (dc.EndTime < DateTime.Now)
            {
                return DiscountCampaignStatus.Expired;
            }

            if (userId != null)
            {
                var userOwnDc = _op.DiscountCodeGetListByOwner(dc.Id, (int)userId);
                if (userOwnDc.Any())
                {
                    return DiscountCampaignStatus.Received;
                }
            }

            int currentTotal = _op.DiscountCodeGetCount(DiscountCode.Columns.CampaignId + "=" + dc.Id);
            if (currentTotal >= dc.Qty)
            {
                return DiscountCampaignStatus.Complete;
            }
            return DiscountCampaignStatus.Available;
        }

        /// <summary>
        /// 取得策展折價券
        /// </summary>
        /// <param name="type"></param>
        /// <param name="curationId"></param>
        /// <returns></returns>
        public static DiscountCampaignCollection GetDiscountCampaignListByEvent(EventPromoEventType type, int curationId)
        {
            var discountCampaignCol = new DiscountCampaignCollection();

            if (type == EventPromoEventType.CurationTwo)
            {
                var brand = _pp.GetBrand(curationId);
                if (brand != null)
                {
                    List<int> campaignIds = MarketingFacade.GetDiscountListFromStringToList(brand.DiscountList);
                    if (campaignIds.Any())
                    {
                        return _op.DiscountCampaignGetList(campaignIds);
                    }
                }
            }

            return discountCampaignCol;
        }

        /// <summary>
        /// 取得主題策展列表
        /// </summary>
        /// <returns></returns>
        public static CurationsByTheme GetCurationsByTheme(int mainId = 1, bool isChannel = false)
        {
            DateTime now = DateTime.Now;

            ViewThemeCurationCollection vThemeCuration = _pp.GetViewThemeCurationByMainId(mainId);
            if (!vThemeCuration.Any())
            {
                return null;
            }

            var groupsDic = vThemeCuration.OrderBy(x => x.GroupSeq).GroupBy(x => x.GroupName)
                .ToDictionary(g => g.Key, g => g.ToList());

            var allDailyList = new List<OneDayCuration>();
            foreach (var group in groupsDic)
            {
                List<ViewThemeCuration> curationList = group.Value.OrderBy(x => x.ListSeq).ToList();
                ViewThemeCuration firstGroup = curationList.First();

                //濾除未啟用及，未達活動時間區間群組
                if (!firstGroup.GroupEnabled)
                {
                    continue;
                }

                var oneDayCuration = new OneDayCuration();
                oneDayCuration.IsActive = firstGroup.GroupEnabled && now <= firstGroup.GroupEnd; //前台是否顯示
                oneDayCuration.EventDate = firstGroup.GroupStart;
                oneDayCuration.GroupName = firstGroup.GroupName;
                oneDayCuration.IsTopGroup = firstGroup.IsTopGroup;

                var isCurationListOnline = false;
                foreach (var curation in curationList)
                {
                    //濾除未啟用及，未達活動時間區間策展
                    if (!curation.BrandIsActive || curation.CurationEnd < now)
                    {
                        continue;
                    }

                    var themeCurationInfo = new ThemeCurationInfo();

                    //先看群組時間，在檢查單一策展時間，決定是否顯示敬請期待
                    themeCurationInfo.IsActive = (!oneDayCuration.IsActive) ? false : ((firstGroup.GroupStart > now) ? false : curation.CurationStart < now);
                    themeCurationInfo.CurationLink = GetCurationTwoLink(curation.CurationId, curation.Cpa, curation.Url);

                    themeCurationInfo.Type = (EventPromoEventType)curation.CurationType;
                    themeCurationInfo.EventId = curation.CurationId;
                    themeCurationInfo.ImageUrl = isChannel
                        ? ImageFacade.GetEventImageUrl(curation.CurationChannelImage)
                        : ImageFacade.GetEventImageUrl(curation.CurationImage);
                    themeCurationInfo.ShowInWeb = curation.ShowInWeb;
                    themeCurationInfo.ShowInApp = curation.ShowInApp;
                    oneDayCuration.Curations.Add(themeCurationInfo);

                    if (now < curation.CurationEnd && curation.BrandIsActive)
                    {
                        isCurationListOnline = true;
                    }
                }

                if (!isCurationListOnline)
                {
                    oneDayCuration.IsActive = isCurationListOnline;//同一個群組的策展全部下架，則前台不顯示
                }

                allDailyList.Add(oneDayCuration);
            }

            var todayCurations = allDailyList.FirstOrDefault(x => x.IsTopGroup);

            var vtheme = vThemeCuration.First();
            var curationsByTheme = new CurationsByTheme();
            curationsByTheme.ThemeMainId = mainId;
            curationsByTheme.MainImageUrl = ImageFacade.GetEventImageUrl(vtheme.MainImage) ?? string.Empty;
            curationsByTheme.MobileMainImageUrl = ImageFacade.GetEventImageUrl(vtheme.MobileMainImage) ?? string.Empty;
            curationsByTheme.TodayCurations = todayCurations;
            curationsByTheme.AllCurations = allDailyList.ToList();

            return curationsByTheme;
        }

        /// <summary>
        /// 取得Web、M版右下角策展小Icon和連結(可多個)
        /// </summary>
        /// <returns></returns>
        public static string GetNowEventHKLIconHtml()
        {
            var eventList = _pp.GetNowThemeCurationMainCollection().Where(x => x.ThemeType == (int)ThemeType.Normal).OrderBy(x => x.StartDate).ToList();
            var isMobileBroswer = CommonFacade.ToMobileVersion();
            int count = (isMobileBroswer) ? 0 : 1;
            int space = (isMobileBroswer) ? 50 : 43;
            string nowPage = (isMobileBroswer) ? "exhibitionlistmobile" : "exhibitionlist";
            eventList = (isMobileBroswer) ? eventList.Where(x => x.ShowInApp).ToList() : eventList.Where(x => x.ShowInWeb).ToList();

            StringBuilder tempHtml = new StringBuilder();
            foreach (var theme in eventList)
            {
                string shtml;
                if (string.IsNullOrEmpty(theme.ThemeOtherPic) == false)
                {
                    string imgUrl = ImageFacade.GetMediaUrl(theme.ThemeOtherPic, MediaType.ThemeImage);
                    shtml = string.Format(
                        "<a href='/event/{0}?id={1}&rsrc={2}'><div class='HKL_Button_to_event_page' style='display: none; cursor: pointer;margin-bottom:{3}px;background: url({4}) no-repeat scroll;background-size: contain;'></div></a>",
                        nowPage, theme.Id, "17_20161111", space * count, imgUrl);

                    tempHtml.Append(shtml);
                    count++; //有圖才計算
                }
            }
            return tempHtml.ToString();
        }

        /// <summary>
        /// 取得策展頻道Icon和連結
        /// </summary>
        /// <returns></returns>
        public static string GetNowEventChannelIconHtml()
        {
            var channelList = _pp.GetNowThemeCurationMainCollection().Where(x => x.ThemeType == (int)ThemeType.Channel);
            if (channelList.Any())
            {
                var latestEvent = channelList.OrderByDescending(x => x.ModifyTime).FirstOrDefault();
                if (!string.IsNullOrEmpty(latestEvent.ChannelImage))
                {
                    return string.Format("<a href='/event/ThemeCurationChannelMobile'><img src='{0}' /><span id='m_navToday'>{1}</span></a>",
                        ImageFacade.GetMediaUrl(latestEvent.ChannelImage, MediaType.ThemeImage), latestEvent.ChannelTitle);
                }
            }

            return "";
        }

        /// <summary>
        /// App首頁取得策展頻道Banner
        /// </summary>
        /// <returns></returns>
        public static List<CurationEntryInfo> GetAppNowCurationBanner()
        {
            List<CurationEntryInfo> curationInfos = new List<CurationEntryInfo>();
            List<ThemeCurationMain> eventList = _pp.GetNowThemeCurationMainCollection().Where(x => x.ThemeType == (int)ThemeType.Normal).OrderByDescending(x => x.ModifyTime).ToList();
            string nowPage = "ExhibitionListApp.aspx";
            eventList = eventList.Where(x => x.ShowInApp).ToList();
            foreach (var theme in eventList)
            {
                if (string.IsNullOrEmpty(theme.AppIndexImage))
                {
                    continue;
                }
                string redirectUrl = string.Format("{0}/event/{1}?id={2}", _config.SiteUrl, nowPage, theme.Id);
                if (string.IsNullOrEmpty(theme.RedirectUrl) == false)
                {
                    redirectUrl = theme.RedirectUrl;
                }                
                CurationEntryInfo tmpData = new CurationEntryInfo();
                tmpData.ImageUrl = ImageFacade.GetMediaPath(theme.AppIndexImage, MediaType.ThemeImage);
                tmpData.LinkUrl = redirectUrl;
                tmpData.IsOpen = true;
                tmpData.IconImageUrl = string.IsNullOrEmpty(theme.ThemeOtherPic)
                    ? string.Empty : ImageFacade.GetMediaPath(theme.ThemeOtherPic, MediaType.ThemeImage);
                curationInfos.Add(tmpData);
            }
            return curationInfos;
        }

        /// <summary>
        /// 重設個人策展折價券(測試用)
        /// </summary>
        /// <param name="type"></param>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        public static int ResetCurationDiscountByUser(int type, int eventId, int userId)
        {
            var dcList = GetDiscountCampaignListByEvent(EventPromoEventType.CurationTwo, eventId);
            var campaignIdList = dcList.Select(x => x.Id).ToList();
            int count = 0;
            foreach (var campaignId in campaignIdList)
            {
                var userOwnDcCol = _op.DiscountCodeGetListByOwner(campaignId, userId);
                foreach (var userOwnDc in userOwnDcCol)
                {
                    userOwnDc.Owner = null;
                    _op.DiscountCodeSet(userOwnDc);
                    count++;
                }
            }
            return count;
        }

        #endregion  策展2.0

        /// <summary>
        /// 滿額贈活動累積消費金額
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <param name="cid">分類Id</param>
        public static int GetSpecialConsumingAmount(int userId)
        {
            DateTime dateStart = DateTime.Parse(_config.SpecialConsumingDiscountPeriod.Split('|')[0]);
            DateTime dateEnd = DateTime.Parse(_config.SpecialConsumingDiscountPeriod.Split('|')[1]);
            int cid = _config.SpecialConsumingDiscountCid;
            return _mp.CashTrustLogConsumingAmoutGetByPeriod(userId, dateStart, dateEnd, cid);
        }

        /// <summary>
        /// 滿額贈活動(根據指定消費額度，滿額即可獲得優惠活動贈品)
        /// </summary>
        /// <param name="amount">滿額贈活動累積消費金額</param>
        /// <returns>Key: 已獲得贈品數(總消費額/指定消費額度)；Value: 還差多少可再獲得一次活動贈品(指定消費額度-(總消費額%指定消費額度))</returns>
        public static KeyValuePair<int, int> GetSpecialConsumingDiscount(int amount)
        {
            int consumingAmount = _config.SpecialConsumingDiscountAmount;
            return new KeyValuePair<int, int>(amount / consumingAmount, consumingAmount - (amount % consumingAmount));
        }

        #region WeChat
        /// <summary>
        /// 回傳WeChat的AccessToken和OpenId (存放在SystemCode)
        /// </summary>
        /// <returns></returns>
        public static SystemCode GetWeChatAccessToken()
        {
            SystemCode system_code = _ss.SystemCodeGetByCodeGroupId(WeChatSystemCodeType.WeChatAccessToken.ToString(), 1);
            //需判斷是否過期
            if (string.IsNullOrEmpty(system_code.CodeGroup) || system_code.ModifyTime < DateTime.Now)
            {
                return UpdateWeChatAccessToken();
            }
            else
            {
                return system_code;
            }
        }
        /// <summary>
        /// 更新WeChat的AccessToken
        /// </summary>
        /// <returns></returns>
        public static SystemCode UpdateWeChatAccessToken()
        {
            SystemCode system_code = new SystemCode();
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(string.Format("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}", _config.WeChatAppId, _config.WeChatAppSecret));
            request.Method = "Get";
            using (WebResponse response = request.GetResponse())
            {
                Stream return_stream = response.GetResponseStream();
                var json_serializer = new JsonSerializer();
                Dictionary<string, string> json_dictionary = json_serializer.Deserialize<Dictionary<string, string>>(return_stream.ReadToEnd());
                if (json_dictionary.ContainsKey("access_token"))
                {
                    int expire_seconds;
                    if (int.TryParse(json_dictionary["expires_in"], out expire_seconds))
                    {
                        system_code = _ss.SystemCodeGetByCodeGroupId(WeChatSystemCodeType.WeChatAccessToken.ToString(), 1);
                        if (string.IsNullOrEmpty(system_code.CodeGroup))
                        {
                            system_code.CodeGroup = WeChatSystemCodeType.WeChatAccessToken.ToString();
                            system_code.CodeGroupName = "WeChat AccessToken";
                            system_code.CodeId = 1;
                            system_code.Enabled = true;
                            system_code.CreateId = "sys@17life.com";
                            system_code.CreateTime = DateTime.Now;
                            //有效期限
                            system_code.ModifyTime = DateTime.Now.AddSeconds(expire_seconds);
                            //accesstoken
                            system_code.ModifyId = json_dictionary["access_token"];
                            system_code.ShortName = "WeChat AccessToken";
                        }
                        else
                        {
                            system_code.ModifyTime = DateTime.Now.AddSeconds(expire_seconds);
                            system_code.ModifyId = json_dictionary["access_token"];
                        }
                        system_code.CodeName = WeChatSystemCodeType.WeChatAccessToken.ToString();
                        _ss.SystemCodeSet(system_code);
                    }
                }
                else if (json_dictionary.ContainsKey("errcode"))
                {
                    logger.ErrorFormat("Get WeChat AccessToken Error!! {0},{1}", json_dictionary["errcode"], json_dictionary["errmsg"]);
                }
                return system_code;
            }
        }
        /// <summary>
        ///  透過WeChat傳送文本訊息給使用者
        /// </summary>
        /// <param name="user_openid">接收者帳號</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public static string PostWeChatTextMessage(string user_openid, string message)
        {

            var json_serializer = new JsonSerializer();
            string json = json_serializer.Serialize(new { touser = user_openid, msgtype = WeChatMsgType.text.ToString(), text = new { content = message } });
            return PostToWeChat(json, WeChatSystemCodeType.WeChatPostMessage);
        }
        /// <summary>
        /// 透過WeChat傳送圖文訊息給使用者
        /// </summary>
        /// <param name="user_openid">接收者帳號</param>
        /// <param name="items">訊息</param>
        /// <returns></returns>
        public static string PostWeChatArticleMessage(string user_openid, IEnumerable<EventPromoItem> items)
        {
            var json_serializer = new JsonSerializer();
            string json = json_serializer.Serialize(new { touser = user_openid, msgtype = WeChatMsgType.news.ToString(), news = new { articles = items.Select(x => new { title = x.Title, description = x.Description, url = x.ItemUrl, picurl = x.ItemPicUrl }) } });
            return PostToWeChat(json, WeChatSystemCodeType.WeChatPostMessage);
        }
        /// <summary>
        /// 透過WeChat傳送圖片訊息給使用者
        /// </summary>
        /// <param name="user_openid">接收者帳號</param>
        /// <param name="id">圖片id</param>
        /// <returns></returns>
        public static string PostWeChatImageMessage(string user_openid, int id)
        {
            var json_serializer = new JsonSerializer();
            string json = json_serializer.Serialize(new { touser = user_openid, msgtype = WeChatMsgType.image.ToString(), image = new { media_id = id } });
            return PostToWeChat(json, WeChatSystemCodeType.WeChatPostMessage);
        }

        public static string PostToWeChat(string json, WeChatSystemCodeType type)
        {
            SystemCode wechat_system_code = GetWeChatAccessToken();
            var json_serializer = new JsonSerializer();
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(string.Format(
            type == WeChatSystemCodeType.WeChatPostMessage ?
            "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={0}" :
            "https://api.weixin.qq.com/cgi-bin/menu/create?access_token={0}", wechat_system_code.ModifyId));
            request.Method = "Post";
            request.ContentType = "application/json; charset=UTF-8";
            using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(json);
                writer.Flush();
                writer.Close();
            }
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                Dictionary<string, string> json_dictionary = json_serializer.Deserialize<Dictionary<string, string>>(reader.ReadToEnd());
                return string.Format("{0}，{1}:{2}", type.ToString(), json_dictionary["errcode"], json_dictionary["errmsg"]);
            }
        }
        /// <summary>
        /// 刪除所有WeChat Menu
        /// </summary>
        public static void RemoveWeChatMenu()
        {
            SystemCode wechat_system_code = GetWeChatAccessToken();
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(string.Format("https://api.weixin.qq.com/cgi-bin/menu/delete?access_token={0}", wechat_system_code.ModifyId));
            request.Method = "Get";
            using (WebResponse response = request.GetResponse())
            {
                Stream return_stream = response.GetResponseStream();
                var json_serializer = new JsonSerializer();
                Dictionary<string, string> json_dictionary = json_serializer.Deserialize<Dictionary<string, string>>(return_stream.ReadToEnd());
                logger.ErrorFormat("WeChat Menu has been deleted!! {0},{1}", json_dictionary["errcode"], json_dictionary["errmsg"]);
            }
        }
        /// <summary>
        /// WeChat產生選單
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static string CreateWeChatMenu(string json)
        {
            return PostToWeChat(json, WeChatSystemCodeType.WeChatCreateMenu);
        }
        /// <summary>
        /// 上傳圖片到WeChat
        /// </summary>
        /// <param name="file_path">檔案路徑</param>
        /// <returns></returns>
        public static string UploadWeChatImage(string file_path)
        {
            string result = string.Empty;
            if (File.Exists(file_path))
            {
                SystemCode wechat_system_code = GetWeChatAccessToken();
                using (WebClient client = new WebClient())
                {
                    byte[] image_byte = client.UploadFile(new Uri(string.Format("http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token={0}&type={1}", wechat_system_code.ModifyId, WeChatMsgType.image.ToString()))
                        , file_path
                        );
                    result = Encoding.Default.GetString(image_byte);
                }
            }
            else
            {
                result = "file is not existed";
            }
            return result;
        }
        #endregion

        #region SkmEventPrizeWinner

        public static SkmEventPrizeWinner GetSkmEventPrizeWinner(string name, string idNumber)
        {
            int namelength = name.Length;

            if (namelength <= 1 || namelength >= 5)
            {
                return null;
            }
            string markName = string.Empty;
            string markNameSec = string.Empty;
            string markIDNumber = string.Empty;

            if (namelength == 2)
            {
                markName = name.Substring(0, 1) + "x";
                markNameSec = "x" + name.Substring(1, 1);
            }
            else if (namelength == 3)
            {
                markName = name.Substring(0, 1) + "x" + name.Substring(2, 1);
            }
            else if (namelength == 4)
            {
                markName = name.Substring(0, 1) + "xx" + name.Substring(3, 1);
            }
            else
            {
                return null;
            }

            markIDNumber = idNumber.Substring(0, 3) + "xxxxx" + idNumber.Substring(8, 2);

            SkmEventPrizeWinnerCollection skmEventPrizeWinnerCol = _ep.SkmEventPrizeWinnerCollectionGet();

            if (namelength == 2)
            {
                if (skmEventPrizeWinnerCol.Any(x => string.Equals(x.PrizeName, markName) && string.Equals(x.PrizeIdNumber, markIDNumber)))
                {

                    return
                        skmEventPrizeWinnerCol.First(
                            x => string.Equals(x.PrizeName, markName) && string.Equals(x.PrizeIdNumber, markIDNumber));

                }

                if (skmEventPrizeWinnerCol.Any(x => string.Equals(x.PrizeName, markNameSec) && string.Equals(x.PrizeIdNumber, markIDNumber)))
                {

                    return
                        skmEventPrizeWinnerCol.First(
                            x => string.Equals(x.PrizeName, markNameSec) && string.Equals(x.PrizeIdNumber, markIDNumber));

                }
            }
            else
            {
                if (skmEventPrizeWinnerCol.Any(x => string.Equals(x.PrizeName, markName) && string.Equals(x.PrizeIdNumber, markIDNumber)))
                {

                    return
                        skmEventPrizeWinnerCol.First(
                            x => string.Equals(x.PrizeName, markName) && string.Equals(x.PrizeIdNumber, markIDNumber));

                }
            }


            return null;
        }

        #endregion SkmEventPrizeWinner

        public static List<DiscountCampaign> GetPromotionDiscountCampaignList(DiscountCampaignUsedFlags flag)
        {
            DiscountCampaignCollection campaignList = _op.DiscountCampaignGetEventList(flag);
            return campaignList.Where(x => x.ApplyTime.HasValue).ToList();
        }

        public static DiscountCampaign GetPromotionDiscountDetail(string code, int campaignId)
        {
            ViewDiscountDetail discountCode = _op.ViewDiscountDetailByCode(code, campaignId);
            var tmpCampaign = _op.DiscountCampaignGet(discountCode.Id);
            return tmpCampaign;
        } 

        #region 邀請送折價券

        /// <summary>
        /// 分享邀請紀錄
        /// </summary>
        /// <param name="origUrl"></param>
        /// <param name="shortUrl"></param>
        /// <param name="userId"></param>
        /// <param name="username"></param>
        /// <param name="bid"></param>
        /// <param name="eventActiveId"></param>
        /// <param name="type"></param>
        public static void SetDiscountReferrerUrl(string origUrl, string shortUrl, int userId, string username, Guid? bid, int eventActiveId,
            DiscountReferrerUrlType type = DiscountReferrerUrlType.Deal)
        {
            DiscountReferrerUrl dru = _op.DiscountReferrerUrlGet(origUrl);
            if (!dru.IsLoaded && !string.IsNullOrWhiteSpace(shortUrl))
            {
                dru.OrigUrl = origUrl;
                dru.ShortUrl = shortUrl;
                dru.Type = (int)type;
                dru.UserId = userId;
                dru.CreateTime = DateTime.Now;
                dru.CreateId = username;
                if (dru.CreateId != null && dru.CreateId.Length > 30)
                {
                    dru.CreateId = dru.CreateId.Substring(0, 30);
                }
                dru.Bid = bid;
                dru.EventActivityId = eventActiveId;
                _op.DiscountReferrerUrlSet(dru);
            }
        }

        /// <summary>
        /// 邀請親友首購贈送折價券
        /// </summary>
        /// <param name="referenceId">推薦人userid</param>
        /// <param name="o">訂單</param>
        /// <param name="theDeal">檔次</param>
        /// <returns></returns>
        public static bool ReferenceDiscountCheckAndPay(string referenceId, Order o, IViewPponDeal theDeal, string userName)
        {
            int minAmount = 50;
            //有傳入referenceId，非公益檔，購買金額大於$50
            if ((!string.IsNullOrEmpty(referenceId)) && o.Subtotal > minAmount && (theDeal.GroupOrderStatus & (int)GroupOrderStatus.KindDeal) == 0)
            {
                //拆解referenceId 取得 外部ID 與 外部來源編號
                string[] referenceDatas = referenceId.Split('|');
                string externalId;
                int iSingleSign;
                try
                {
                    externalId = referenceDatas[1];
                    iSingleSign = int.Parse(referenceDatas[0]);
                }
                catch (Exception e)
                {
                    logger.Error(string.Format("傳入referenceId錯誤{0}", referenceId), e);
                    return false;
                }

                //取得推薦人的member_user_id
                int referenceUserId = _mp.MemberLinkGetByExternalId(externalId, (SingleSignOnSource)iSingleSign).UserId;
                if ((referenceUserId == 0))
                {
                    return false;
                }

                //若有查到推薦人資料 且 推薦人不為本人，則寫入邀請發送折價券名單
                if (referenceUserId != o.UserId)
                {
                    // set discount_referrer
                    DiscountReferrer referrer = new DiscountReferrer();
                    referrer.ReferrerUserId = referenceUserId;
                    referrer.OrderGuid = o.Guid;
                    referrer.Status = (int)DiscountReferrerStatus.Initial;
                    referrer.CreateId = userName;
                    referrer.CreateTime = DateTime.Now;
                    _op.DiscountReferrerSet(referrer);

                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 依據參數設定值發送批次折價券
        /// </summary>
        /// <param name="amountList">發送的折價券金額/數量(key=金額,value=數量)</param>
        /// <param name="userId">UserId</param>
        /// <param name="startTime">折價券兌換起始日</param>
        /// <param name="endTime">折價券兌換截止日</param>
        /// <param name="qty">折價券數量</param>
        /// <param name="multiple">最低訂單金額限制/折價券面額 之倍數</param>
        /// <param name="flag">折價券設定(親友邀請or首購)</param>
        public static void GenerateDiscountCodeByAmountList(Dictionary<int, int> amountList, int userId, DateTime startTime, 
            DateTime endTime, int qty, int multiple, DiscountCampaignUsedFlags flag, string name)
        {
            foreach (var item in amountList)
            {
                if (item.Key > 0)
                {
                    for (int j = 0; j < item.Value; j++)
                    {
                        GenerateDiscountCodeByCampaign(startTime, endTime, qty, 
                            item.Key, item.Key * _config.ReferrerAndFirstBuyDiscountMinAmountSet, userId, flag, name);
                    }
                }
            }
        }

        public static void EnsureGenerateDiscountCodeByAmountList(Dictionary<int, int> amountList, DateTime startTime,
            DateTime endTime, int qty, int multiple, DiscountCampaignUsedFlags flag, string name)
        {
            foreach (var item in amountList)
            {
                if (item.Key > 0)
                {
                    for (int j = 0; j < item.Value; j++)
                    {
                        EnsureGenerateDiscountCodeByCampaign(startTime, endTime, qty, item.Key, 
                            item.Key * _config.ReferrerAndFirstBuyDiscountMinAmountSet, flag, name);
                    }
                }
            }
        }

        private static void EnsureGenerateDiscountCodeByCampaign(DateTime startTime, DateTime endTime, int qty, int amount, 
            int minAmount, DiscountCampaignUsedFlags flag, string name)
        {
            DiscountCampaign campaign = _op.DiscountCampaignGetByReference(flag, amount, endTime);
            if (campaign == null || !campaign.IsLoaded)
            {
                try
                {
                    // 建立折價券活動(使用於P好康館、最低消費金額限制、邀請親友購買、限本人、即時產)
                    campaign = new DiscountCampaign();
                    campaign.Type = (int)DiscountCampaignType.PponDiscountTicket;
                    campaign.Name = name;
                    campaign.CreateTime = DateTime.Now;
                    campaign.CreateId = _config.SystemEmail;
                    campaign.StartTime = startTime;
                    campaign.EndTime = endTime;
                    campaign.ApplyTime = DateTime.Now;
                    campaign.ApplyId = _config.SystemEmail;
                    campaign.Flag = campaign.Flag | (int)flag;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.Ppon;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.MinimumAmount;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.OwnerUseOnly;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.InstantGenerate;
                    campaign.Qty = qty;
                    campaign.Amount = amount;
                    campaign.MinimumAmount = minAmount;
                    campaign.MinGrossMargin = _config.DiscountCodeMinGrossMarginSet;
                    _op.DiscountCampaignSet(campaign);
                }
                catch (Exception ex)
                {
                    logger.Error("DiscountCampaignSet error.", ex);
                    throw;
                }
            }
        }

        /// <summary>
        /// 取得邀請贈送折價券設定的金額&數量
        /// </summary>
        /// <param name="referenceId"></param>
        /// <param name="o"></param>
        /// <param name="theDeal"></param>
        /// <param name="user_name"></param>
        public static Dictionary<int, int> GetReferrerDiscountAmountList(string discountSetting)
        {
            Dictionary<int, int> amountList = new Dictionary<int, int>();
            string[] referr = discountSetting.Split('|');
            for (int i = 0; i < referr.Length; i++)
            {
                int amount = int.TryParse(referr[i].Split('#')[0], out amount) ? amount : 0;
                int count = int.TryParse(referr[i].Split('#')[1], out count) ? count : 0;

                amountList.Add(amount, count);
            }
            return amountList;
        }

        /// <summary>
        /// 發送折價券(默認為限本人&即時產)
        /// </summary>
        /// <param name="orderTime"></param>
        /// <param name="endTime"></param>
        /// <param name="qty"></param>
        /// <param name="amount"></param>
        /// <param name="minAmount"></param>
        /// <param name="orderUserId"></param>
        private static void GenerateDiscountCodeByCampaign(DateTime startTime, DateTime endTime, int qty, int amount, int minAmount, 
            int referenceUserId, DiscountCampaignUsedFlags flag, string name)
        {
            logger.InfoFormat("GenerateDiscountCodeByCampaign, startTime={0}, endTime={1}, qty={2}, amount={3}, minAmount={4}, referenceUserId={5}, flag={6}, name={7}", 
                startTime, endTime, qty, amount, minAmount, referenceUserId, flag, name);
            DiscountCampaign campaign = _op.DiscountCampaignGetByReference(flag, amount, endTime);
            if (campaign == null || !campaign.IsLoaded)
            {
                //底下的程式碼己重複，目前活動會先產生
                try
                {
                    // 建立折價券活動(使用於P好康館、最低消費金額限制、邀請親友購買、限本人、即時產)
                    campaign = new DiscountCampaign();
                    campaign.Type = (int)DiscountCampaignType.PponDiscountTicket;
                    campaign.Name = name;
                    campaign.CreateTime = DateTime.Now;
                    campaign.CreateId = _config.SystemEmail;
                    campaign.StartTime = startTime;
                    campaign.EndTime = endTime;
                    campaign.ApplyTime = DateTime.Now;
                    campaign.ApplyId = _config.SystemEmail;
                    campaign.Flag = campaign.Flag | (int)flag;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.Ppon;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.MinimumAmount;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.OwnerUseOnly;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.InstantGenerate;
                    campaign.Qty = qty;
                    campaign.Amount = amount;
                    campaign.MinimumAmount = minAmount;
                    campaign.MinGrossMargin = _config.DiscountCodeMinGrossMarginSet;
                    _op.DiscountCampaignSet(campaign);
                } 
                catch (Exception ex)
                {
                    logger.Error("DiscountCampaignSet error.", ex);
                    throw;
                }
            }

            string code;
            int d_amount, discountCodeId, minimumAmount;
            DateTime? d_endTime;
            PromotionFacade.InstantGenerateDiscountCode(campaign.Id, referenceUserId, false, out code, out d_amount, out d_endTime, out discountCodeId, out minimumAmount);
        }

        /// <summary>
        /// 邀請親友贈送折價券通知信
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <param name="discountCodeList">折價券</param>
        /// <returns></returns>
        public static void SendReferenceDiscountMsg(int userId, Dictionary<int, int> discountCodeList)
        {
            var member = MemberFacade.GetMember(userId);

            ReferenceDiscountCodeMail t1 = TemplateFactory.Instance().GetTemplate<ReferenceDiscountCodeMail>();
            t1.MemberName = member.DisplayName;
            t1.DiscountCodeList = discountCodeList;
            t1.PromoDeals = OrderFacade.GetRandomDeal(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, _config.PromoMailDealsCount, Convert.ToInt32(_config.PromoMailDealsCount * _config.PromoMailDealsProportion));
            t1.ServerConfig = _config;
            if (RegExRules.CheckEmail(member.UserEmail))
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(_config.PponServiceEmail, _config.ServiceName);
                msg.To.Add(member.UserEmail);
                msg.Subject = string.Format("{0}{1}元折價券，已匯入您的帳戶囉！", I18N.Phrase.ReferenceDiscountCodePay,
                    discountCodeList.Select(x => x.Key * x.Value).Sum());
                msg.Body = t1.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        /// <summary>
        /// 查找需要發送的折價券名單
        /// </summary>
        /// <returns></returns>
        public static void GenerateDiscountReferrer(decimal expireMonths, DateTime date, int qty)
        {
            // 取得折價券應設定的起始&截止日期
            DateTime startDate;
            DateTime endDate;
            logger.InfoFormat("GenerateDiscountReferrer expireMonths={0}, date={1}, qty={2}", expireMonths, date, qty);
            GetDiscountReferrerExpireDate(date, expireMonths, out startDate, out endDate);
            logger.InfoFormat("GetDiscountReferrerExpireDate, startDate={0}, endDate={1}", startDate, endDate);
            // 查找等待發送的邀請訂單
            DiscountReferrerCollection referrers = _op.DiscountReferrerGetList(DiscountReferrerStatus.Initial);
            logger.InfoFormat("referrers.count={0}", referrers.Count);
            Dictionary<int, int> amountList = GetReferrerDiscountAmountList(_config.ReferrerDiscountCodeSet);
            logger.InfoFormat("amountList={0}", Newtonsoft.Json.JsonConvert.SerializeObject(amountList));
            int sendCount = 0;
            int cancelCount = 0;
            int otherCount = 0;
            int totalCount = 0;

            ///先行產生活動
            EnsureGenerateDiscountCodeByAmountList(
                amountList, startDate, endDate, qty, 8, DiscountCampaignUsedFlags.ReferenceAndPay, I18N.Phrase.ReferenceDiscountCodePay);

            foreach (DiscountReferrer r in referrers)
            {
                totalCount++;
                // 需 憑證已核銷/宅配商品已過鑑賞期 才可發送折價券
                CashTrustLogCollection ctl = _mp.CashTrustLogGetListByOrderGuid(r.OrderGuid, OrderClassification.LkSite);
                if ((ctl.Any(x => x.DeliveryType == (int)DeliveryType.ToShop) && ctl.Any(x => x.Status == (int)TrustStatus.Verified)) ||
                    (ctl.Any(x => x.DeliveryType == (int)DeliveryType.ToHouse) && PponOrderManager.CheckIsOverTrialPeriod(r.OrderGuid)))
                {
                    // 發送折價券
                    sendCount++;
                    GenerateDiscountCodeByAmountList(amountList, r.ReferrerUserId, startDate, endDate, qty, 8, DiscountCampaignUsedFlags.ReferenceAndPay, I18N.Phrase.ReferenceDiscountCodePay);
                    // 註記邀請折價券狀態為已發送
                    SetDiscountReferrer(r, (int)DiscountReferrerStatus.Send);
                    // 寄送邀請親友贈送折價券通知信
                    SendReferenceDiscountMsg(r.ReferrerUserId, amountList);
                }
                else if (ctl.Any(x => x.Status == (int)TrustStatus.Returned) || ctl.Any(x => x.Status == (int)TrustStatus.Refunded) ||
                         (r.CreateTime < DateTime.Now.AddDays(-2) && ctl.Any(x => x.Status == (int)TrustStatus.ATM)))
                {
                    cancelCount++;
                    SetDiscountReferrer(r, (int)DiscountReferrerStatus.Cancel);
                }
                else
                {
                    otherCount++;
                }
            }

            if (totalCount != 0 && (sendCount + cancelCount) == 0)
            {
                //it's log for debug
                foreach (DiscountReferrer r in referrers)
                {
                    // 需 憑證已核銷/宅配商品已過鑑賞期 才可發送折價券                    
                    CashTrustLogCollection ctls = _mp.CashTrustLogGetListByOrderGuid(r.OrderGuid, OrderClassification.LkSite);
                    var ctl = ctls.FirstOrDefault();
                    if (ctl.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        logger.InfoFormat("referrer={0} 宅配, 已過鑑賞期={1}，判定時期={2}",
                            r.Id, PponOrderManager.CheckIsOverTrialPeriod(r.OrderGuid), 
                            PponOrderManager.GetLastShippedBufferTime(r.OrderGuid));
                    }
                    else if (ctl.DeliveryType == (int)DeliveryType.ToShop)
                    {
                        var verifyCtl = ctls.FirstOrDefault(t => t.Status == (int)TrustStatus.Verified);
                        if (verifyCtl == null)
                        {
                            logger.InfoFormat("referrer {0} 憑證, 沒有已核銷的ctl", r.Id);
                        }
                        else
                        {
                            logger.InfoFormat("referrer={0}, order={1}, 核銷ctl={2} 憑證, status={3}",
                                r.Id, verifyCtl.TrustId, r.OrderGuid, verifyCtl.Status);
                        }
                    }
                }
            }
            logger.InfoFormat("total={0}, send={1}, cancel={2}, other={3}", totalCount, sendCount, cancelCount, otherCount);
        }

        /// <summary>
        /// 設定邀請折價券狀態
        /// </summary>
        /// <returns></returns>
        private static void SetDiscountReferrer(DiscountReferrer r, int statusType)
        {
            r.Status = statusType;
            r.ModifyId = _config.SystemEmail;
            r.ModifyTime = DateTime.Now;
            _op.DiscountReferrerSet(r);
        }

        /// <summary>
        /// 依據指定日期取得折價券應設定的起始日期
        /// </summary>
        /// <param name="date">指定日期</param>
        /// <param name="month">預期至少可使用月份(可設定0.5個月=15天)</param>
        /// <param name="startDate">回傳起日</param>
        /// <param name="month">回傳迄日</param>
        /// <returns></returns>
        public static void GetDiscountReferrerExpireDate(DateTime date, decimal month, out DateTime startDate, out DateTime endDate)
        {
            if (month > 0)
            {
                if (month < 1)
                {
                    int mid = 15;
                    if (date.Day <= mid)
                    {
                        // ex: 2014/01/15 => 2014/01/01 00:00 ~ 2014/01/31 23:59
                        startDate = new DateTime(date.Year, date.Month, 1);
                        endDate = new DateTime(date.Year, date.Month, 1).AddMonths(1).AddMinutes(-1);
                    }
                    else
                    {
                        // ex: 2014/01/16 => 2014/01/16 00:00 ~ 2014/02/15 23:59
                        startDate = new DateTime(date.Year, date.Month, mid).AddDays(1);
                        endDate = new DateTime(date.Year, date.Month, mid).AddMonths(1).AddDays(1).AddMinutes(-1);
                    }
                }
                else
                {
                    // 以當月1日作為起日
                    startDate = new DateTime(date.Year, date.Month, 1);
                    endDate = startDate.AddMonths(Convert.ToInt32(month) + 1).AddMinutes(-1);
                }
            }
            else
            {
                startDate = new DateTime();
                endDate = new DateTime();
            }
        }

        #endregion 邀請送折價券

        public static EventActivity GetEventActivity(int id)
        {
            return _ep.EventActivityGet(id);
        }

        #region 首購送折價券

        /// <summary>
        /// 首購送折價券
        /// </summary>
        /// <param name="referenceId">推薦人userid</param>
        /// <param name="o">訂單</param>
        /// <param name="theDeal">檔次</param>
        /// <returns></returns>
        public static bool FirstBuyDiscountCheckAndPay(Order o, IViewPponDeal theDeal)
        {
            if (_config.FirstBuyDiscountCodeEnabled)
            {
                int minAmount = 50;
                //有傳入referenceId，非公益檔，且為第一次購買，購買金額大於$50
                if (((o.OrderStatus & (int)OrderStatus.FirstOrder) == (int)OrderStatus.FirstOrder)
                    && o.Subtotal > minAmount && (theDeal.GroupOrderStatus & (int)GroupOrderStatus.KindDeal) == 0)
                {
                    // 取得折價券應設定的起始&截止日期
                    DateTime startDate;
                    DateTime endDate;
                    int qty = 99999;
                    decimal expireMonths = _config.FirstBuyDiscountExpireMonths;
                    GetDiscountReferrerExpireDate(DateTime.Now, expireMonths, out startDate, out endDate);

                    // 發送折價券
                    Dictionary<int, int> amountList = GetReferrerDiscountAmountList(_config.FirstBuyDiscountCodeSet);
                    GenerateDiscountCodeByAmountList(amountList, o.UserId, startDate, endDate, qty, 10, DiscountCampaignUsedFlags.FirstBuy, I18N.Phrase.FirstBuyDiscountCodePay);

                    // 寄送首購贈送折價券通知信
                    SendFirstBuyDiscountMsg(o.UserId, amountList);

                    // 更新折價券資料
                    var discount = GetMemberDiscountCode(o.UserId);
                    CookieManager.SetMemberDiscount(discount);

                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 邀請親友贈送折價券通知信
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <param name="discountCodeList">折價券</param>
        /// <returns></returns>
        public static void SendFirstBuyDiscountMsg(int userId, Dictionary<int, int> discountCodeList)
        {
            var member = MemberFacade.GetMember(userId);

            FirstBuyDiscountCodeMail t1 = TemplateFactory.Instance().GetTemplate<FirstBuyDiscountCodeMail>();
            t1.MemberName = member.DisplayName;
            t1.DiscountCodeList = discountCodeList;
            t1.PromoDeals = OrderFacade.GetRandomDeal(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, _config.PromoMailDealsCount, Convert.ToInt32(_config.PromoMailDealsCount * _config.PromoMailDealsProportion));
            t1.ServerConfig = _config;
            if (RegExRules.CheckEmail(member.UserEmail))
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(_config.PponServiceEmail, _config.ServiceName);
                msg.To.Add(member.UserEmail);
                msg.Subject = string.Format("{0}{1}元折價券，已匯入您的帳戶囉！", I18N.Phrase.FirstBuyDiscountCodePay, discountCodeList.Select(x => x.Key * x.Value).Sum());
                msg.Body = t1.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Async);
            }
        }

        #endregion

        #endregion

        #region 活動領取折價券

        /// <summary>
        /// 活動贈送折價券通知信
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="discountCodeList">折價券</param>
        /// <param name="imgUrl"></param>
        /// <param name="subject"></param>
        public static void DiscountCodeGiftSendMail(string userName, Dictionary<int, int> discountCodeList, string imgUrl, string subject)
        {
            var member = MemberFacade.GetMember(userName);

            DiscountCodeGiftSendMail t1 = TemplateFactory.Instance().GetTemplate<DiscountCodeGiftSendMail>();
            t1.MemberName = member.DisplayName;
            t1.DiscountCodeList = discountCodeList;
            t1.PromoDeals = OrderFacade.GetRandomDeal(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, _config.PromoMailDealsCount, Convert.ToInt32(_config.PromoMailDealsCount * _config.PromoMailDealsProportion));
            t1.ServerConfig = _config;
            t1.imgUrl = imgUrl;
            if (RegExRules.CheckEmail(member.UserEmail))
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(_config.PponServiceEmail, _config.ServiceName);
                msg.To.Add(member.UserEmail);
                msg.Subject = string.Format("{0}{1}元折價券，已匯入您的帳戶囉！", subject, discountCodeList.Select(x => x.Key * x.Value).Sum());
                msg.Body = t1.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        /// <summary>
        /// 產生隨機亂碼
        /// </summary>
        /// <returns></returns>
        public static string GenerateRandomCode(int length)
        {
            string code = System.IO.Path.GetRandomFileName().Replace(".", "").Substring(1, length).ToUpper();
            DiscountEvent de = _op.DiscountEventGet(DiscountEvent.Columns.EventCode, code);
            if (de.IsLoaded)
            {
                return GenerateRandomCode(length);
            }
            return code;
        }

        #endregion

        #region 1111自動補發折價券

        /// <summary>
        /// 折價券自動補發
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="oid">折價券</param>
        /// <param name="userName"></param>
        public static void AutoSendDiscountCode(int userId, Guid oid, string userName)
        {
            var discountCode = _op.DiscountCodeGetByOrderGuid(oid);
            var campaign = GetPromotionDiscountDetail(discountCode.Code, discountCode.CampaignId ?? 0);
            if (campaign.StartTime.HasValue && campaign.EndTime.HasValue && (campaign.Flag & (int)DiscountCampaignUsedFlags.AutoSend) > 0)
            {
                if (campaign.StartTime.Value < DateTime.Now && campaign.EndTime.Value > DateTime.Now)
                {
                    string code;
                    int amount, discountCodeId, minimumAmount;
                    DateTime? endTime;
                    Dictionary<int, int> amountList = new Dictionary<int, int>();
                    PromotionFacade.InstantGenerateDiscountCode(campaign.Id, userId, false, out code, out amount, out endTime, out discountCodeId, out minimumAmount);
                    //amountList.Add(amount, 1);
                    //PromotionFacade.DiscountCodeGiftSendMail(userName, amount, minimumAmount, campaign.Name, "[17Life雙11專屬無限次回饋]");
                }
            }
        }

        /// <summary>
        /// 1111活動贈送折價券通知信
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="discountName"></param>
        /// <param name="subject"></param>
        /// <param name="discountAmount"></param>
        /// <param name="minimumAmount"></param>
        public static void DiscountCodeGiftSendMail(string userName, int discountAmount, int minimumAmount, string discountName, string subject)
        {
            var member = MemberFacade.GetMember(userName);

            DiscountCodeAutoSendMail t1 = TemplateFactory.Instance().GetTemplate<DiscountCodeAutoSendMail>();
            t1.MemberName = member.DisplayName;
            t1.eventName = subject;
            t1.discountAmount = discountAmount.ToString();
            t1.discountLimit = minimumAmount.ToString();
            t1.discountName = discountName;
            t1.ServerConfig = _config;

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(_config.PponServiceEmail, _config.ServiceName);
            msg.To.Add(member.UserEmail);
            msg.Subject = string.Format("{0}，已加碼至您帳戶！", subject + discountName);
            msg.Body = t1.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }


        #endregion

        #region 新版折價券券

        public static void DiscountLimitSet(Guid bid, string userName, DiscountLimitType type)
        {
            var tmpData = _op.DiscountLimitGetByBid(bid);

            if (!tmpData.IsNew || type != DiscountLimitType.Disabled)
            {
                tmpData.Bid = bid;
                tmpData.Type = (int)type;
                tmpData.ModifyTime = DateTime.Now;
                tmpData.ModifyId = userName;
                if (tmpData.IsNew)
                {
                    tmpData.CreateTime = DateTime.Now;
                    tmpData.CreateId = userName;
                    tmpData.GrossMargin =
                        _pp.ViewDealBaseGrossMarginGet(bid).GrossMargin;
                }
                _op.DiscountLimitSet(tmpData);
            }
        }


        /// <summary>
        /// 折價券限制管別(分類)
        /// </summary>
        public static Dictionary<Dictionary<int, string>, Dictionary<Dictionary<int, string>, Dictionary<int, string>>> GetCategorieList()
        {
            var channelList = CategoryManager.PponChannelCategoryTree.CategoryNodes.OrderBy(x => x.Seq);
            Dictionary<Dictionary<int, string>, Dictionary<Dictionary<int, string>, Dictionary<int, string>>> categoryDictionary = new Dictionary<Dictionary<int, string>, Dictionary<Dictionary<int, string>, Dictionary<int, string>>>();
            foreach (var channel in channelList)
            {
                Dictionary<int, string> channels = new Dictionary<int, string>();
                Dictionary<int, string> areas = new Dictionary<int, string>();
                Dictionary<int, string> categorie = new Dictionary<int, string>();
                Dictionary<Dictionary<int, string>, Dictionary<int, string>> categories = new Dictionary<Dictionary<int, string>, Dictionary<int, string>>();
                channels.Add(channel.CategoryId, channel.CategoryName);
                List<CategoryTypeNode> areaCategories = channel.NodeDatas.Where(x => x.NodeType == CategoryType.PponChannelArea).ToList();
                if (areaCategories.Count > 0)
                {
                    CategoryTypeNode areaNode = areaCategories.First();
                    foreach (var area in areaNode.CategoryNodes)
                    {
                        areas.Add(area.CategoryId, area.CategoryName);
                    }
                }
                List<CategoryTypeNode> categorieList = channel.NodeDatas.Where(x => x.NodeType == CategoryType.DealCategory).ToList();
                if (categorieList.Count > 0)
                {
                    CategoryTypeNode categoryNode = categorieList.First();
                    foreach (var c in categoryNode.CategoryNodes)
                    {
                        categorie.Add(c.CategoryId, c.CategoryName);
                    }
                }
                categories.Add(areas, categorie);
                categoryDictionary.Add(channels, categories);
            }
            return categoryDictionary;
        }
        #endregion

        #region SEO

        public static PromoSeoKeyword GetPromoSeoKeyword(int promoType, int activityId)
        {
            return _pp.GetPromoSeoKeyword(promoType, activityId);
        }

        public static void SavePromoSeoKeyword(PromoSeoKeyword seo)
        {
            _pp.SavePromoSeoKeyword(seo);
        }

        public static void DeletePromoSeoKeyword(PromoSeoKeyword seo)
        {
            _pp.DeletePromoSeoKeyword(seo);
        }



        #endregion

        #region 熱門關鍵字

        /// <summary>
        /// 取得前臺熱門關鍵字
        /// </summary>
        /// <returns></returns>
        public static HotTagModel GetPromoSearchKeys()
        {
            PromoSearchKeyCollection pskCol = _pp.PromoSearchKeyGetColl(PromoSearchKeyUseMode.APP);
            List<HotTagItem> items = new List<HotTagItem>();
            foreach (var psk in pskCol)
            {
                items.Add(new HotTagItem
                {
                    Keyword = psk.KeyWord,
                    PicUrl = Helper.ReplaceToHttps(psk.PicUrl),
                    NavigateUrl = Helper.CombineUrl(_config.SiteUrl, "/ppon/pponsearch.aspx?search=" + HttpUtility.HtmlEncode(psk.KeyWord))
                });
            }
            return new HotTagModel
            {
                Items = items
            };
        }

        /// <summary>
        /// 取得APP熱門關鍵字
        /// </summary>
        /// <param name="takeCnt"></param>
        /// <returns></returns>
        public static List<AppHotTag> GetAppPromoSearchKeys(int takeCnt = 0)
        {
            PromoSearchKeyCollection pskCol = _pp.PromoSearchKeyGetColl(PromoSearchKeyUseMode.APP, takeCnt);
            List<AppHotTag> hotTags = new List<AppHotTag>();
            foreach (var psk in pskCol)
            {
                hotTags.Add(new AppHotTag
                {
                    Keyword = psk.KeyWord,
                    PicUrl = psk.PicUrl
                });
            }
            return hotTags;
        }

        #endregion 熱門關鍵字

        #region 問卷調查

        /// <summary>
        /// 繳交問卷
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        public static ApiResultCode AnswerQuestion(QuestionAnswerModel answer)
        {
            if (!Enum.IsDefined(typeof(OrderFromType), (int)answer.FromType))
            {
                return ApiResultCode.InputError;
            }

            var ev = _qp.GetQuestionEvent(answer.EventToken);
            if (ev == null)
            {
                return ApiResultCode.DataNotFound;
            }

            if (ev.LoginMode == (byte)QuestionEventLoginMode.MemberOnly
                && (answer.UserId == 0 || !MemberFacade.GetMember(answer.UserId).IsLoaded))
            {

                return ApiResultCode.UserNoSignIn;

            }

            if (answer.UserId > 0)
            {
                var checkAnswer = _qp.GetQuestionAnswerByEventId(answer.UserId, ev.Id);
                if (checkAnswer.Any())
                {
                    return ApiResultCode.DataIsExist;
                }
            }

            var qa = new QuestionAnswer
            {
                UserId = answer.UserId,
                EventId = ev.Id,
                AnswerTime = DateTime.Now,
                IpAddress = answer.IpAddress,
                OrderFromType = answer.FromType
            };

            var opts = new List<QuestionAnswerOption>();
            foreach (var o in answer.Volume)
            {
                opts.Add(new QuestionAnswerOption
                {
                    OptionId = o.OptionId,
                    AnswerContent = o.AnswerContent
                });
            }

            var result = _qp.AnswerQuestion(qa, opts);

            if (result)
            {
                SendDiscountCodeByQuestion(ev.Id, qa.Id);
            }

            return result ? ApiResultCode.Success : ApiResultCode.SaveFail;
        }

        /// <summary>
        /// 問卷完成後發送折價券
        /// </summary>
        /// <param name="eventId">question_event.id</param>
        /// <param name="answerId">question_answer.id</param>
        /// <returns></returns>
        public static bool SendDiscountCodeByQuestion(int eventId, int answerId)
        {
            var eds = _qp.GetQuestionEventDiscount(eventId);
            if (eds.Any())
            {
                using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                {
                    var answer = _qp.GetQuestionAnswer(answerId);
                    if (answer == null || answer.SentDiscountCode)
                    {
                        return false;
                    }

                    foreach (var ed in eds)
                    {
                        var sendLog = _qp.GetQuestionAnswerDiscountLog(answer.UserId, ed.Id);
                        if (sendLog.Any())
                        {
                            continue;
                        }

                        for (int i = 0; i < ed.Qty; i++)
                        {
                            string code;
                            int amount, discountCodeId, minimumAmount;
                            DateTime? codeEndTime;

                            PromotionFacade.InstantGenerateDiscountCode(ed.DiscountCampaign, answer.UserId, ed.Qty == 1
                                , out code, out amount, out codeEndTime, out discountCodeId, out minimumAmount);


                            var dlog = new QuestionAnswerDiscountLog
                            {
                                AnswerId = answerId,
                                EventDiscountId = ed.Id,
                                DiscountCampaign = ed.DiscountCampaign,
                                DiscountCodeId = discountCodeId,
                                UserId = answer.UserId,
                                CreateTime = DateTime.Now
                            };

                            _qp.SaveQuestionAnswerDiscountLog(dlog);
                        }
                    }

                    answer.SentDiscountCode = true;
                    answer.SentDiscountTime = DateTime.Now;
                    _qp.SaveQuestionAnswer(answer);
                    ts.Complete();
                }
            }

            return true;
        }

        #endregion 問卷調查

        #region GMC & Microdata

        /// <summary>
        /// [GMC / Microdata]判斷檔次是否顯示最低單價
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static bool IsLowestAvgPriceDeal(Guid bid)
        {
            var gmc = _dp.GetGmcAvgPriceDeal(bid);
            return gmc != null && gmc.Bid != Guid.Empty;
        }

        /// <summary>
        /// [GMC]取得需顯示最低單價的檔次BID
        /// </summary>
        /// <returns></returns>
        public static List<Guid> GetLowestAvgPriceDeal()
        {
            return _dp.GetGmcAvgPriceDealAllBid();
        }

        #endregion
    }
}

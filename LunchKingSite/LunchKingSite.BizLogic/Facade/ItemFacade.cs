﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LunchKingSite.BizLogic.Facade
{
    public class ItemFacade
    {
        private static IItemProvider _ip;
        private static IOrderProvider _op;
        private static IPponProvider _pp;
        private static ISysConfProvider _config;
        static ItemFacade()
        {
            _ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _config = ProviderFactory.Instance().GetConfig();
        }

        public static string GetDiscountSpokenNumberString(decimal origPrice, decimal discountedPrice)
        {
            return GetDiscountSpokenNumberString(discountedPrice / origPrice);
        }

        public static string GetDiscountSpokenNumberString(decimal discountPercent)
        {
            if (discountPercent <= 0 || discountPercent >= 1)
            {
                return null;
            }

            decimal p = Math.Round(discountPercent, 2) * 10;
            return p == decimal.Floor(p) ? decimal.Floor(p).ToString("N0") : (p * 10).ToString("N0");
        }

        public static string ConvertAccessoryGroupToSetupText(AccessoryGroupCollection groups)
        {
            if (groups == null || groups.Count == 0)
            {
                return null;
            }

            string setupText = string.Empty;
            foreach (var ag in groups)
            {
                // prepend a blank line if it's not the first group
                setupText += (setupText != string.Empty ? "\n" : "") + ag.AccessoryGroupName + ":\n";
                // load data first
                if (ag.AccessoryMembers == null)
                {
                    var members = _ip.AccessoryGroupMemberGetList(ag.Guid); // load members
                    var accessories = _ip.AccessoryGetList(members.Select(x => x.AccessoryGuid).ToArray()); // load associated accessories
                    ag.AccessoryMembers = new List<KeyValuePair<Accessory, AccessoryGroupMember>>();
                    foreach (var m in members) // map both
                    {
                        ag.AccessoryMembers.Add(new KeyValuePair<Accessory, AccessoryGroupMember>(accessories.First(x => x.Guid == m.AccessoryGuid), m));
                    }
                }

                foreach (var kv in ag.AccessoryMembers)
                {
                    setupText += (kv.Value.Quantity.HasValue ? "[" + kv.Value.Quantity + "] " : "") + kv.Key.AccessoryName + "\n";
                }
            }

            return setupText;
        }

        public static bool UpdateAccessoryGroupViaSetupText(Guid bid, Guid sellerGuid, Guid itemGuid, string accessoryCategoryName, AccessoryGroupCollection newGroups)
        {
            bool isUpdate = false;
            //AccessoryGroupCollection newGroups = ParseAccessoryGroupText(sellerGuid, accessoryCategoryName, content);

            if (newGroups == null)
            {
                return false;
            }

            // build new ItemAccessoryGroupList
            ItemAccessoryGroupListCollection iaglCol = new ItemAccessoryGroupListCollection();

            // delete original ItemAccessoryGroupList
            _ip.ItemAccessoryGroupListDelete(ItemAccessoryGroupList.ItemGuidColumn.ColumnName, itemGuid);

            // update new list
            if (newGroups.Count == 0)
            {
                isUpdate = true;
            }

            for (int i = 0; i < newGroups.Count; i++)
            {
                newGroups[i].SellerGuid = sellerGuid;
                if (newGroups[i].AccessoryMembers != null)
                {
                    _ip.AccessoryGroupSet(newGroups[i]);
                    iaglCol.Add(new ItemAccessoryGroupList() { AccessoryGroupGuid = newGroups[i].Guid, ItemGuid = itemGuid, Guid = new Guid(), Sequence = i });
                    foreach (var kv in newGroups[i].AccessoryMembers)
                    {
                        if (kv.Key != null && kv.Value != null)
                        {                            
                            _ip.AccessoryCategorySet(kv.Key.Category);
                            _ip.AccessorySet(kv.Key);
                            _ip.AccessoryGroupMemberSet(kv.Value);
                            isUpdate = true;
                        }
                    }
                }
            }

            foreach (var iag in iaglCol)
            {
                _ip.ItemAccessoryGroupListSet(iag);
            }

            int orderCount = _pp.ViewPponOrderGetCount(ViewPponOrder.Columns.BusinessHourGuid + "=" + bid);
            //若該檔次已開賣 則清除選項關聯
            if (orderCount > 0)
            {
                _ip.AccessoryGroupPurgeReference(sellerGuid);
            }
            //若未開賣則直接刪除選項關聯
            else
            {
                _ip.AccessoryGroupPurgeNotReferenced(sellerGuid);
            }

            return isUpdate;
        }
        
        public static AccessoryGroupCollection ProcessAccessoryGroupText(Guid bid, Guid sellerGuid, string accessoryCategoryName, string content)
        {
            AccessoryGroupCollection groups = new AccessoryGroupCollection();
            AccessoryGroup grp = null;
            bool isGroupName = true;
            int seq = 1;
            Regex re = new Regex(@"^\s*(\[(?<qty>-?\d+)\])*\s*((?<itemno>.+)\!)*\s*(?<name>.+)", RegexOptions.Compiled);
            foreach (var line in content.Split('\n').Select(x => x.Trim()))
            {
                if (!string.IsNullOrEmpty(line))
                {
                    if (isGroupName) // the line is group name
                    {
                        if (grp != null)
                        {
                            groups.Add(grp);
                        }

                        grp = new AccessoryGroup() { AccessoryGroupName = line.TrimEnd(':').TrimEnd('：'), SellerGuid = sellerGuid, Guid = Guid.NewGuid(), AccessoryGroupType = (int)ItemAccessoryListType.Single, Status = 0 };
                        isGroupName = false;
                    }
                    else // the line defines members
                    {
                        if (grp.AccessoryMembers == null)
                        {
                            grp.AccessoryMembers = new List<KeyValuePair<Accessory, AccessoryGroupMember>>();
                        }

                        //多重選項增加貨號,已全半形驚嘆號區隔,故這邊先全部替換成半形驚嘆號 
                        var match = re.Match(line.Replace('！', '!'));
                        Accessory acc = _ip.AccessoryGet(Accessory.Columns.AccessoryName, match.Groups["name"].Value);
                        if (acc == null || !acc.IsLoaded || 
                            acc.AccessoryName != match.Groups["name"].Value)//「綠色-s」與「綠色-S」要視為不同的
                        {
                            acc = new Accessory() { AccessoryName = match.Groups["name"].Value, Guid = Guid.NewGuid(), CreateTime = DateTime.Now, CreateId = "sys" };
                            acc.Category = _ip.AccessoryCategoryGet(accessoryCategoryName);
                            if (acc.Category == null || !acc.Category.IsLoaded)
                            {
                                acc.Category = new AccessoryCategory() { AccessoryCategoryName = accessoryCategoryName, CreateTime = DateTime.Now, CreateId = "sys" };
                            }

                            acc.AccessoryCategoryGuid = acc.Category.Guid;
                        }
                        else
                        {
                            acc.Category = _ip.AccessoryCategoryGet(accessoryCategoryName);
                        }

                        AccessoryGroupMember agm = new AccessoryGroupMember() { Sequence = seq++, AccessoryGroupGuid = grp.Guid, Guid = Guid.NewGuid(), AccessoryGuid = acc.Guid, PponBid = bid };
                        if (match.Groups["qty"].Success)
                        {
                            int q;
                            agm.Quantity = int.TryParse(match.Groups["qty"].Value, out q) ? q : 0;
                        }

                        grp.AccessoryMembers.Add(new KeyValuePair<Accessory, AccessoryGroupMember>(acc, agm));
                    }
                }
                else // extra empty lines
                {
                    seq = 1;
                    isGroupName = true;
                }
            }

            if (grp != null)
            {
                groups.Add(grp);
            }

            return groups;
        }

        /// <summary>
        /// 好康憑證退貨回寫多選項(accessory_group_member.quantity)數量資訊By PponCoupon。
        /// </summary>
        /// <param name="viewPponCoupon">好康憑證(ViewPponCoupon)</param>
        public static void RefundAccessoryMemberQuantityByCoupon(ViewPponCoupon viewPponCoupon)
        {
            //**企劃確認購買人(份)數不做退貨完成後數量處理, 但要在此修正上線前的檔次都要照原邏輯執行**
            var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(viewPponCoupon.BusinessHourGuid);
            if (deal.BusinessHourOrderTimeS < _config.StopRefundQuantityCalculateDate)
            { 
                #region 多選項數量退貨回補

                RefundAccessoryMemberQuantitySet(viewPponCoupon.ItemName, Guid.Parse(viewPponCoupon.ItemGuid.ToString()));

                #endregion 多選項數量退貨回補

                #region 分店銷售數量回補

                RefundPponStoreOrderedQuantitySet(viewPponCoupon.ItemName, viewPponCoupon.BusinessHourGuid);

                #endregion 分店銷售數量回補
            }
        }

        /// <summary>
        /// 訂單明細退貨回寫多選項(accessory_group_member.quantity)數量資訊By OrderDetail。
        /// </summary>
        /// <param name="orderDetail">orderDetail(訂單明細)</param>
        public static void RefundAccessoryMemberQuantityByOrderDetail(OrderDetail orderDetail)
        {
            //**企劃確認購買人(份)數不做退貨完成後數量處理, 但要在此修正上線前的檔次都要照原邏輯執行**
            var subOrder = _op.OrderGet(orderDetail.OrderGuid);
            var parentOrder = _op.OrderGet(subOrder.ParentOrderId.Value);
            var deal = _pp.ViewPponDealGet(parentOrder.Guid);
            if (deal.BusinessHourOrderTimeS < _config.StopRefundQuantityCalculateDate)
            { 

                #region 多選項數量退貨回補

                RefundAccessoryMemberQuantitySet(orderDetail.ItemName, Guid.Parse(orderDetail.ItemGuid.ToString()));

                #endregion 多選項數量退貨回補

                #region 分店銷售數量回補

                if (subOrder.ParentOrderId.HasValue)
                {
                    RefundPponStoreOrderedQuantitySet(orderDetail.ItemName, deal.BusinessHourGuid);            
                }

                #endregion 分店銷售數量回補
            }
        }
        /// <summary>
        /// 退貨多選項數量
        /// </summary>
        /// <param name="compareitemName">商品Item名稱</param>
        /// <param name="itemGuid">ItemID</param>
        private static void RefundAccessoryMemberQuantitySet(string compareitemName, Guid itemGuid)
        {
            ViewItemAccessoryGroupCollection viewItemAccessoryGroups = _ip.ViewItemAccessoryGroupGetList(itemGuid);
            foreach (var viewItemAccessoryGroup in viewItemAccessoryGroups)
            {
                //(L)  更改格式為 (L * quantity) 故調整
                if (compareitemName.IndexOf("(" + viewItemAccessoryGroup.AccessoryName + " x ") >= 0 || compareitemName.IndexOf("(" + viewItemAccessoryGroup.AccessoryName + ")") >= 0)
                {
                    //表示有對應到AccessoryName
                    AccessoryGroupMember member = _ip.AccessoryGroupMemberGet(viewItemAccessoryGroup.AccessoryGroupMemberGuid);
                    member.Quantity++;
                    _ip.AccessoryGroupMemberSet(member);

                    //PponOption退貨回補
                    if (member.PponBid != null)
                    {                        
                        int? optId = PponOption.FindId(member.Guid, (Guid)member.PponBid);
                        if (optId.HasValue)
                        {
                            PponOption opt = _pp.PponOptionGet(optId.Value);
                            if (opt != null && opt.IsLoaded)
                            {
                                opt.Quantity = member.Quantity;
                                _pp.PponOptionSet(opt);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Ppon退貨分店已訂購數回寫
        /// </summary>
        /// <param name="compareitemName">商品Item名稱</param>
        /// <param name="businessHourGuid">檔次ID</param>
        private static void RefundPponStoreOrderedQuantitySet(string compareitemName, Guid businessHourGuid)
        {
            ViewPponStoreCollection viewPponStores = _pp.ViewPponStoreGetListByBidWithNoLock(businessHourGuid, VbsRightFlag.VerifyShop);
            foreach (var viewpponstore in viewPponStores)
            {
                //檢查ItemName中有該分店的字串，EX:  "(《店名》"字串
                if (compareitemName.IndexOf("(" + viewpponstore.StoreName) >= 0)
                {
                    PponStore refundPponStore = _pp.PponStoreGet(viewpponstore.BusinessHourGuid, viewpponstore.StoreGuid);
                    refundPponStore.OrderedQuantity = refundPponStore.OrderedQuantity.GetValueOrDefault();
                    refundPponStore.OrderedQuantity--;
                    refundPponStore.CouponSequenceCount--;
                    _pp.PponStoreSet(refundPponStore);
                }
            }

        }

    }
}

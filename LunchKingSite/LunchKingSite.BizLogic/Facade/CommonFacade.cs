﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using NPOI.SS.UserModel;
using EnterpriseDT.Net.Ftp;
using System.Net.Mail;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Description;
using LunchKingSite.BizLogic.Model.OAuth;
using Org.BouncyCastle.Asn1.Ocsp;

namespace LunchKingSite.BizLogic.Facade
{
    public class CommonFacade
    {
        private static readonly ILog log;
        protected static ICmsProvider cp;
        protected static ISysConfProvider config;
        protected static ISystemProvider ss;
        protected static IHumanProvider hp;
        protected static readonly ILog OauthLog;

        static CommonFacade()
        {
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            config = ProviderFactory.Instance().GetConfig();
            ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            log = LogManager.GetLogger(typeof(CommonFacade));
            OauthLog = LogManager.GetLogger("Oauth");
        }

        public static bool AddAudit(string refId, AuditType type, string msg, string createId, bool commonVisible)
        {
            string cid = createId != null ? createId.Trim() : null;
            msg = msg != null ? msg.Trim() : null;

            if (cid != null && msg != null)
            {
                Audit r = new Audit();
                r.ReferenceId = refId;
                r.ReferenceType = (int)type;
                r.CommonVisible = commonVisible;
                r.Message = msg;
                r.CreateId = MemberFacade.GetShortUserName(createId);
                r.CreateTime = DateTime.Now;
                return cp.AuditSet(r);
            }
            return false;
        }

        public static bool AddAudit(Guid refId, AuditType type, string msg, string createId, bool commonVisible)
        {
            return AddAudit(refId.ToString(), type, msg, createId, commonVisible);
        }

        #region 字串數字基礎函數

        /// <summary>
        /// 傳入一個數字, 轉換成國字 (e.g. 1006=>壹仟零陸) 後回傳. 注意, 未帶單位, 需要單位 (例如: 元) 請自帶
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string GetChineseAmountString(int number)
        {
            string[] chineseNumber = { "零", "壹", "貳", "參", "肆", "伍", "陸", "柒", "捌", "玖" };
            string[] smallUnit = { "", "拾", "佰", "仟" };
            string[] bigUnit = { "", "萬", "億", "兆", "京" };
            StringBuilder ret = new StringBuilder();
            string inputNumber = number.ToString();
            int idx = inputNumber.Length;
            bool needAppendZero = false;
            foreach (char c in inputNumber)
            {
                idx--;
                //小單位 (拾/佰/仟)
                if (c > '0')
                {
                    if (needAppendZero)
                    {
                        ret.Append(chineseNumber[0]);
                        needAppendZero = false;
                    }
                    ret.Append(chineseNumber[(int)(c - '0')] + smallUnit[idx % 4]);
                }
                else
                {
                    needAppendZero = true;
                }
                //處理大單位(萬/億/兆)
                int bigUnitNum = (int)Math.Floor((decimal)idx / 4);
                if (idx % 4 == 0)
                {
                    ret.Append(bigUnit[bigUnitNum]);
                }
            }
            return ret.Length == 0 ? chineseNumber[0] : ret.ToString();
        }

        /// <summary>
        /// 傳入一個數字, 轉換成8位數之國字 (e.g. 1006=>零千零百零拾零萬壹仟零百零拾陸) 後回傳. 注意, 未帶單位, 需要單位 (例如: 元) 請自帶
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string GetFullChineseAmountString(int number)
        {
            string[] chineseNumber = { "零", "壹", "貳", "參", "肆", "伍", "陸", "柒", "捌", "玖" };
            string[] smallUnit = { "", "拾", "百", "千" };
            string[] bigUnit = { "", "萬", "億", "兆", "京" };
            StringBuilder ret = new StringBuilder();
            StringBuilder tempNumber = new StringBuilder();
            for (var i = number.ToString().Length - 1; i <= 6; i++)
            {
                tempNumber.Append('0');
            }
            tempNumber.Append(number.ToString());
            string inputNumber = tempNumber.ToString();
            int idx = inputNumber.Length;
            foreach (char c in inputNumber)
            {
                idx--;
                //小單位 (拾/佰/仟)
                if (c > '0')
                {
                    ret.Append(chineseNumber[(int)(c - '0')] + smallUnit[idx % 4]);
                }
                else
                {
                    ret.Append(chineseNumber[0] + smallUnit[idx % 4]);
                }
                //處理大單位(萬/億/兆)
                int bigUnitNum = (int)Math.Floor((decimal)idx / 4);
                if (idx % 4 == 0)
                {
                    ret.Append(bigUnit[bigUnitNum]);
                }
            }
            return ret.Length == 0 ? chineseNumber[0] : ret.ToString();
        }

        #endregion 字串數字基礎函數

        #region Ftp上傳CelloPoint
        /// <summary>
        /// 上傳FTP至CelloPoint電子郵件功能
        /// </summary>
        /// <param name="mission_title">任務名稱</param>
        /// <param name="ini_path">ini檔案放置folder</param>
        /// <param name="csv_path">csv檔案放置folder</param>
        /// <param name="html_path">html檔案放置folder</param>
        /// <param name="filename">檔名</param>
        /// <param name="reply_exception_mail">是否回傳exception內容</param>
        /// <returns></returns>
        public static string FtpCelloPoint(string mission_title, string ini_path, string csv_path, string html_path, string filename, bool reply_exception_mail = false)
        {
            string result = string.Empty;
            if (config.MailServiceType == MailServerSendType.PostMan)
            {
                Dictionary<int, string> ini_dir = new Dictionary<int, string>();
                Dictionary<int, string> csv_dir = new Dictionary<int, string>();
                string senderaddr = string.Empty;
                string sendername = string.Empty;
                string subject = string.Empty;
                string replyToAddr = string.Empty;
                string html_content = string.Empty;
                int rcptaddr_index = 0;
                string line = string.Empty;
                using (StreamReader reader_html = new StreamReader(Path.Combine(html_path, filename + ".html"), Encoding.GetEncoding("utf-8")))
                {
                    html_content = reader_html.ReadToEnd();
                }
                //因為CelloPoint ini格式固定，就不另外引用dll來解析ini
                using (StreamReader reader_ini = new StreamReader(Path.Combine(ini_path, filename + ".ini"), Encoding.GetEncoding("big5")))
                {
                    while ((line = reader_ini.ReadLine()) != null)
                    {
                        Match m_serial = Regex.Match(line, "^FIELD[0-9][0-9]=");
                        Match m_variable = Regex.Match(line, "%%.+%%$");
                        Match m_senderaddr = Regex.Match(line, "^SenderAddr=.+");
                        Match m_sendername = Regex.Match(line, "^SenderName=.+");
                        Match m_subject = Regex.Match(line, "^Subject=.+");
                        Match m_replyToAddr = Regex.Match(line, "^ReplyToAddress=.+");
                        if (m_variable.Success && m_serial.Success)
                        {
                            Match m_index = Regex.Match(m_serial.Value, "[0-9][0-9]");
                            if (m_index.Success)
                            {
                                ini_dir[int.Parse(m_index.Value)] = m_variable.Value;
                                if (m_variable.Value == "%%RCPT_ADDR%%")
                                {
                                    rcptaddr_index = int.Parse(m_index.Value);
                                }
                            }
                        }
                        else if (m_senderaddr.Success)
                        {
                            senderaddr = m_senderaddr.Value.Replace("SenderAddr=", string.Empty);
                        }
                        else if (m_sendername.Success)
                        {
                            sendername = m_sendername.Value.Replace("SenderName=", string.Empty);
                        }
                        else if (m_subject.Success)
                        {
                            subject = m_subject.Value.Replace("Subject=", string.Empty);
                        }
                        else if (m_replyToAddr.Success)
                        {
                            replyToAddr = m_replyToAddr.Value.Replace("ReplyToAddress=", string.Empty);
                        }
                    }
                }
                int total = 0;
                int ini_dir_length = ini_dir.Count;
                using (StreamReader reader_csv = new StreamReader(Path.Combine(csv_path, filename + ".csv"), Encoding.GetEncoding("big5")))
                {
                    while ((line = reader_csv.ReadLine()) != null)
                    {
                        string[] data = line.Split(new string[] { "," }, StringSplitOptions.None);
                        if (ini_dir_length == data.Length)
                        {
                            string real_html = html_content;
                            for (int i = 0; i < data.Length; i++)
                            {
                                real_html = real_html.Replace(ini_dir[i], data[i]);
                            }
                            MailMessage msg = new MailMessage();
                            msg.To.Add(data[rcptaddr_index]);
                            msg.Subject = subject;
                            msg.From = new MailAddress(senderaddr, sendername);
                            msg.IsBodyHtml = true;
                            msg.Body = real_html;
                            msg.ReplyToList.Add(replyToAddr);
                            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                            total++;
                        }
                        //此功能是模擬CelloPoint 如果超出100筆就跳出
                        if (total > 20)
                        {
                            result = "已超過模擬CelloPoint上限20筆";
                            break;
                        }
                    }
                }
                result = string.Format("經由{0}，上傳{1}任務完成，{2}筆", config.MailServiceType.ToString(), mission_title, total);
                return result;
            }
            else
            {
                FTPClient ftp = new FTPClient();
                try
                {
                    ftp.RemoteHost = config.EmailServerFTPHost;
                    ftp.ControlPort = config.EmailServerFTPPort;
                    ftp.ConnectMode = FTPConnectMode.PASV;
                    ftp.Timeout = 600000;   //將Timeout改為 600秒以放寬 timeout 時間
                    ftp.Connect();
                    ftp.Login(config.EmailServerFTPUser, config.EmailServerFTPPw);
                    if (ftp.Connected)
                    {
                        log.InfoFormat("上傳FTP至CelloPoint電子郵件 put -> path: ini({0}), csv:({1}), html:({2}), filename:{3}", ini_path, csv_path, html_path, filename);
                        ftp.TransferType = FTPTransferType.BINARY;
                        ftp.Put(Path.Combine(ini_path, filename + ".ini"), filename + ".ini");
                        ftp.Put(Path.Combine(csv_path, filename + ".csv"), filename + ".csv");
                        ftp.Put(Path.Combine(html_path, filename + ".html"), filename + ".html", false);
                    }
                    result = string.Format("經由{0}，上傳{1}任務完成", config.MailServiceType.ToString(), mission_title);
                    log.Info(result);
                }
                catch (Exception ex)
                {
                    log.WarnFormat("上傳FTP至CelloPoint電子郵件功能 FtpCelloPoint 錯誤:{0} host:{1}:{2}, {3}", ex.Message, config.EmailServerFTPHost, config.EmailServerFTPPort, ex.StackTrace);
                    result = ex.Message;
                    if (reply_exception_mail)
                    {
                        MailMessage msg = new MailMessage();
                        msg.To.Add(config.ItTesterEmail);
                        msg.Subject = string.Format("{0}:{1}FTP錯誤", Environment.MachineName, mission_title);
                        msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                        msg.IsBodyHtml = true;
                        msg.Body = ex.ToString();
                        PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    }
                }
                finally
                {
                    ftp.Quit();
                }
                return result;
            }
        }


        #endregion

        #region 權限控管

        public static void CreateSystemFunctionPrivilege()
        {
            // 先清除暫存的功能列表
            FunctionPrivilegeManager.ClearManagerData();
        }

        public static bool IsInSystemFunctionPrivilege(string email, SystemFunctionType type)
        {
            if (FunctionPrivilegeManager.IsInSystemFunctionOrgPrivilegeByUserName(email, type))
            {
                return true;
            }
            return FunctionPrivilegeManager.IsInSystemFunctionPrivilege(email, type);
        }
        public static bool IsInSystemFunctionPrivilege(string email, SystemFunctionType type, string path)
        {
            if (FunctionPrivilegeManager.IsInSystemFunctionOrgPrivilegeByUserName(email, type, path))
            {
                return true;
            }
            return FunctionPrivilegeManager.IsInSystemFunctionPrivilege(email, type, path);
        }

        public static bool IsInSystemFunctionPrivilege(string email, string actionName, SystemFunctionType type)
        {
            if (FunctionPrivilegeManager.IsInSystemFunctionOrgPrivilegeByUserName(email, actionName, type))
            {
                return true;
            }
            return FunctionPrivilegeManager.IsInSystemFunctionPrivilege(email, actionName, type);
        }

        #endregion 權限控管

        #region OAuth

        public static OauthToken OauthTokenGet(int userId)
        {
            return ss.OauthTokenGet(userId);
        }

        public static OauthToken OauthTokenGet(string accessToken)
        {
            return ss.OauthTokenGet(accessToken);
        }

        public static TokenResult CheckToken(OAuthTokenModel oauthToken, TokenScope[] scope, bool isTokenContextItemNull)
        {
            if (oauthToken == null)
            {
                return TokenResult.NotFound;
            }
            return CheckToken(oauthToken.Token, scope, isTokenContextItemNull, oauthToken.Permissions);
        }

        public static TokenResult CheckToken(OauthToken token, TokenScope[] scope, bool isTokenContextItemNull, OauthTokenPermissionCollection permissions = null)
        {
            try
            {
                if (!token.IsLoaded)
                {
                    return TokenResult.NotFound;
                }
                if (token.IsValid == false)
                {
                    return TokenResult.Expired;
                }
                if (token.ExpiredTime < DateTime.Now)
                {
                    return TokenResult.Expired;
                }
                if (permissions == null)
                {
                    permissions = ss.OauthTokenPermissionGetList(token.Id);
                }
                if (permissions.Count == 0)
                {
                    return TokenResult.NoAuth;
                }
                else
                {
                    //permissions.All(x => x.Scope != (int)scope)
                    List<TokenScope> permissionScope = permissions.Select(x => (TokenScope)x.Scope).ToList();
                    if (!scope.Intersect(permissionScope).Any())
                    {
                        return TokenResult.NoAuth;
                    }
                }

                return TokenResult.OK;
            }
            catch(Exception ex)
            {
                OauthLog.ErrorFormat("{0}({1})-Json:{2}, stackTrace:{3}", ex.Message, isTokenContextItemNull, new JsonSerializer().Serialize(token), ex.StackTrace);
                return TokenResult.Error;
            }
        }
        #endregion

        /// <summary>
        /// 解析NPOI Cell Value
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static string GetSheetCellValueString(NPOI.SS.UserModel.Cell cell)
        {
            string value = string.Empty;
            if (cell != null)
            {
                switch (cell.CellType)
                {
                    case CellType.BOOLEAN:
                        value = cell.BooleanCellValue ? "是" : "否";
                        break;

                    case CellType.NUMERIC:
                        value = cell.NumericCellValue.ToString();
                        break;

                    case CellType.STRING:
                        value = cell.StringCellValue;
                        break;

                    case CellType.BLANK:
                    case CellType.ERROR:
                    case CellType.FORMULA:
                    case CellType.Unknown:
                    default:
                        break;
                }
            }
            return value;
        }

        public static string GetStringByByteLength(string sourceString, int length, string suffix)
        {
            if (string.IsNullOrEmpty(sourceString))
            {
                return string.Empty;
            }
            byte[] b = System.Text.Encoding.UTF8.GetBytes(sourceString);
            if (b.Length == 0)
            {
                return string.Empty;
            }

            if (b.Length > length)
            {
                string[] checkWords = { "?", "�" };
                string tmpStr = System.Text.Encoding.UTF8.GetString(b, 0, length);
                if (checkWords.Contains(tmpStr.Substring(tmpStr.Length - 1, 1)))
                {
                    tmpStr = tmpStr.Substring(0, tmpStr.Length - 1);
                }
                return tmpStr + suffix;
            }
            else
            {
                return sourceString;
            }
        }

        #region API回傳編碼轉換(ApiResultCode)
        /// <summary>
        /// 轉換TokenResult 成對應的 ApiResultCode
        /// </summary>
        /// <param name="tokenResult"></param>
        /// <returns></returns>
        public static ApiResultCode TokenResultToApiResultCode(TokenResult tokenResult)
        {
            ApiResultCode resultCode = ApiResultCode.Error;
            switch (tokenResult)
            {
                case TokenResult.OK:
                    resultCode = ApiResultCode.Success;
                    break;
                case TokenResult.Expired:
                    resultCode = ApiResultCode.OAuthTokenExpired;
                    break;
                case TokenResult.NotFound:
                    resultCode = ApiResultCode.OAuthTokerNotFound;
                    break;
                case TokenResult.NoAuth:
                    resultCode = ApiResultCode.OAuthTokerNoAuth;
                    break;
                default:
                    resultCode = ApiResultCode.Error;
                    break;
            }
            return resultCode;
        }
        #endregion #region API回傳編碼轉換(ApiResultCode)

        public static bool ToMobileVersion()
        {
            //M版是否啟用
            if (!config.IsNewMobileSetting) { return false; }

            if (System.Web.HttpContext.Current == null)
            {
                return false;
            }
            var request = System.Web.HttpContext.Current.Request;

            var userAgent = request.UserAgent;           

            if (string.IsNullOrWhiteSpace(userAgent)) { return false; }
            userAgent = userAgent.ToLower();

            if (mobileList.Any(userAgent.Contains)) {
                return true;
            }
            //else if (userAgent.ToLower().Contains("android"))
            //{
            //    //目前只有安卓有此COOKIES
            //    System.Web.HttpCookie cookie = request.Cookies.Get(LkSiteCookie.ToMobile.ToString("g"));
            //    if (cookie == null) { return true; }
            //    else
            //    {
            //        int toMobile;
            //        if (int.TryParse(cookie.Value, out toMobile))
            //        {
            //            return toMobile==1;
            //        }
            //    }
            //}
            else
            {
                //目前只有安卓有此COOKIES
                HttpCookie cookie = request.Cookies.Get(LkSiteCookie.ToMobile.ToString("g"));
                if (cookie != null)
                {
                    int toMobile;
                    if (int.TryParse(cookie.Value, out toMobile))
                    {
                        return toMobile == 1;
                    }
                }
            }

            return false;
        }

        //bb10=blackberry
        //android要區分是否為平版，所以不寫在mobileList內
        static List<string> mobileList = new List<string> { "iphone", "ipod", "bb10", "windows phone", "IEMobile" };
        /// <summary>
        /// 給商品頁用
        /// 跟 CommonFacade裏的那組不同的地方在，
        /// 如果偵測沒有 ToMobile 的cookie，且偵測有android字樣，認定需用M版開啟。
        /// </summary>
        /// <returns></returns>
        public static bool IsMobileVersion()
        {
            if (HttpContext.Current == null)
            {
                return false;
            }

            var request = HttpContext.Current.Request;

            var userAgent = request.UserAgent;
            if (string.IsNullOrWhiteSpace(userAgent)) { return false; }
            userAgent = userAgent.ToLower();

            if (mobileList.Any(userAgent.Contains))
            {
                return true;
            }

            //目前只有安卓有此COOKIES
            System.Web.HttpCookie cookie = request.Cookies.Get(LkSiteCookie.ToMobile.ToString("g"));
            if (cookie != null)
            {
                int toMobile;
                if (int.TryParse(cookie.Value, out toMobile))
                {
                    return toMobile == 1;
                }
            }
            if (userAgent.Contains("android"))
            {
                return true;
            }
            return false;
        }

        public static bool AndroidCheckSize()
        {
            //M版是否啟用
            if (!config.IsNewMobileSetting) { return false; }

            var request = System.Web.HttpContext.Current.Request;

            var url = request.Url.ToString();
            //有參數的都不量尺寸
            if (url.Contains("?"))
            {
                if (url.Contains("bid"))
                {
                    return false;
                }
            }

            var userAgent = request.UserAgent;

            if (string.IsNullOrWhiteSpace(userAgent)) { return false; }            
            //目前只有安卓有此COOKIES
            System.Web.HttpCookie cookie = request.Cookies.Get(LkSiteCookie.ToMobile.ToString("g"));

            return userAgent.ToLower().Contains("android") && (cookie==null);
        }

        /// <summary>
        /// get loading balance server host list
        /// </summary>
        /// <returns></returns>
        public static List<string> GetWebServerHostList()
        {
            SortedDictionary<string, string> serverIPs =
                ProviderFactory.Instance().GetSerializer().Deserialize<SortedDictionary<string, string>>(config.ServerIPs);
            return serverIPs.Keys.ToList();
        }

        /// <summary>
        /// get loading balance server IP address list
        /// </summary>
        /// <returns></returns>
        public static List<string> GetWebServerIpList()
        {
            SortedDictionary<string, string> serverIPs =
                ProviderFactory.Instance().GetSerializer().Deserialize<SortedDictionary<string, string>>(config.ServerIPs);
            return serverIPs.Values.ToList();
        }

        /// <summary>
        /// Call WebService
        /// </summary>
        /// <param name="wsUrl">WebService URL</param>
        /// <param name="intTimeout">WebService Timeout (秒)</param>
        /// <param name="wsNamespace">WebService Namespace</param>
        /// <param name="wsClassName">WebService Class Name</param>
        /// <param name="wsMethodName">WebService Method Name</param>
        /// <param name="wsArgs">WebService 傳入的參數, 以 object[] 方式傳入</param>
        /// <returns>Return Object</returns>
        public static object InvokeWebService(string wsUrl, int intTimeout, string @wsNamespace, string wsClassName, string wsMethodName, object[] wsArgs)
        {
            try
            {
                WebClient webClinet = new WebClient();
                webClinet.Credentials = System.Net.CredentialCache.DefaultCredentials;
                //讀取 WebService 的 wsdl 檔案
                ServiceDescription sdServiceDesc;
                using (Stream stream = webClinet.OpenRead(wsUrl + "?WSDL"))
                {
                    if (stream == null)
                    {
                        throw new Exception("Invoke Error");
                    }
                    sdServiceDesc = ServiceDescription.Read(stream);
                }

                //import WebService wsdl
                ServiceDescriptionImporter sdiServiceDespImport = new ServiceDescriptionImporter();
                sdiServiceDespImport.AddServiceDescription(sdServiceDesc, "", "");
                CodeNamespace cnNamespace = new CodeNamespace(@wsNamespace);

                //指定要編譯的程式
                CodeCompileUnit ccuCompileUnit = new CodeCompileUnit();
                ccuCompileUnit.Namespaces.Add(cnNamespace);
                sdiServiceDespImport.Import(cnNamespace, ccuCompileUnit);

                //以 C# 來進行 Compile
                CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");

                //設定編譯參數
                CompilerParameters cplist = new CompilerParameters
                {
                    GenerateExecutable = false, 
                    GenerateInMemory = true
                };
                cplist.ReferencedAssemblies.Add("System.dll");
                cplist.ReferencedAssemblies.Add("System.XML.dll");
                cplist.ReferencedAssemblies.Add("System.Web.Services.dll");
                cplist.ReferencedAssemblies.Add("System.Data.dll");

                //取得編譯結果
                CompilerResults cr = provider.CompileAssemblyFromDom(cplist, ccuCompileUnit);
                provider.Dispose();

                //if 編譯錯誤則回傳錯誤訊息
                if (cr.Errors.HasErrors)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (CompilerError ce in cr.Errors)
                    {
                        sb.Append(ce);
                        sb.Append(Environment.NewLine);
                    }
                    throw new Exception(sb.ToString());
                }


                //取得編譯後產出的 Assembly
                System.Reflection.Assembly assembly = cr.CompiledAssembly;
                Type tType = assembly.GetType(@wsNamespace + "." + wsClassName, true, true);
                object obj = Activator.CreateInstance(tType);

                //如果有 overload 需指定參數型態
                Type[] tArgsType = null;

                if (wsArgs == null)
                {
                    tArgsType = new Type[0];
                }
                else
                {
                    int tArgsTypeLength = wsArgs.Length;
                    tArgsType = new Type[tArgsTypeLength];
                    for (int i = 0; i < tArgsTypeLength; i++)
                    {
                        tArgsType[i] = wsArgs[i].GetType();
                    }
                }

                System.Reflection.MethodInfo mi = tType.GetMethod(wsMethodName, tArgsType);

                System.Web.Services.Protocols.SoapHttpClientProtocol wsRequest = (System.Web.Services.Protocols.SoapHttpClientProtocol)obj;
                wsRequest.PreAuthenticate = true;
                wsRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;

                //設定 WebService timeout 時間
                System.Reflection.PropertyInfo propInfo = obj.GetType().GetProperty("Timeout");
                intTimeout = intTimeout * 1000;
                propInfo.SetValue(obj, intTimeout, null);

                //執行該webservice方法
                return mi.Invoke(obj, wsArgs);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("{0}({2})->{1}", ex.Message, ex.StackTrace, wsUrl);
                throw new Exception("Invoke Error");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Models;
using SubSonic;
using log4net;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.Regulars;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Component;
using System.Web.Mvc;
using LunchKingSite.I18N;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Facade
{
    public class PcpFacade
    {
        private static ISysConfProvider config;
        private static IPCPProvider pcp;
        private static ISellerProvider sp;
        private static IMemberProvider mp;
        private static ILog logger;
        private static IVerificationProvider vp;
        static PcpFacade()
        {
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            config = ProviderFactory.Instance().GetConfig();
            logger = LogManager.GetLogger("PcpFacade");
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
        }

        #region view_vbs_seller_member

        /// <summary>
        /// App端顯示名字邏輯(Server處理)
        /// Last name(姓)和First name(名)都填入中文，顯示Last name+First name
        /// Last name(姓)和First name(名)任一欄位有英文，顯示First name+Last name
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public static string GetRegularMemberName(string firstName, string lastName, VendorRole role)
        {
            string memberName = string.Empty;
            Regex NumandEG = new Regex(@"^[A-Za-z0-9]+$");
            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
            {
                if (NumandEG.IsMatch(firstName) || NumandEG.IsMatch(lastName))
                {
                    memberName= string.IsNullOrEmpty(firstName + lastName) ? "--" : firstName + lastName;
                }
                else
                {
                    memberName = string.IsNullOrEmpty(lastName + firstName) ? "--" : lastName + firstName;
                }
            }
            else
            {
                memberName = string.IsNullOrEmpty(firstName + lastName) ? "--" : firstName + lastName;
            }

            if (role == VendorRole.PcpEmployee)
            {
                if (!string.IsNullOrEmpty(firstName + lastName))
                {
                    memberName = memberName.Substring(0, 1) + new String('*', (memberName.Count() - 1));
                }
            }
            return memberName;
        }

        public static ViewVbsSellerMemberConsumptionCollection GetViewVbsSellerMemberConsumption(int cardGroupId, string search, int page, int pageSize, int orderByField)
        {
            return sp.ViewVbsSellerMemberConsumptionCollectionGet(cardGroupId, search, page, pageSize, orderByField);
        }

        public static int GetViewVbsSellerMemberCount(int sellerUserId, SellerMemberType sellerMemberType, string criteria, bool newMember)
        {
            List<string> filters = new List<string>();
            //只查詢新會員，限定一個月內為新會員
            if (newMember)
            {
                filters.Add(string.Format("{0} > {1}", ViewVbsSellerMember.Columns.RequestTime, DateTime.Now.AddMonths(-1)));
            }

            return sp.ViewVbsSellerMemberCount(sellerUserId, sellerMemberType, criteria, filters.ToArray());
        }

        #endregion

        #region view_pcp_order_detail
        public static ViewPcpOrderInfoCollection GetViewPcpOrderInfoCollectionByUserIdAndCardGroupId(int userId, int cardGroupId)
        {
            return pcp.ViewPcpOrderInfoCollectionGet(userId, cardGroupId);
        }

        public static ViewPcpOrderInfoCollection GetViewPcpOrderInfoCollectionByCardGroupId(int cardGroupId, Guid? sellerGuid, DateTime? queryTime, int pageNumber, int pageSize)
        {
            return pcp.ViewPcpOrderInfoCollectionGet(cardGroupId, sellerGuid, queryTime, pageNumber, pageSize);
        }
        #endregion

        #region membership_card_group

        public static MembershipCardGroup GetMembershipCardGroupBySellerGuid(Guid sellerGuid)
        {
            MembershipCardGroup result = new MembershipCardGroup();

            var cardGroup = pcp.MembershipCardGroupGetBySellerGuid(sellerGuid);
            if (cardGroup.Id > 0)
            {
                //自己店有開卡用自己的CardGroupId
                result = cardGroup;
            }
            else
            {
                //自己店沒開卡，就找把自己設為適用店家的卡
                var cardGroupStore = pcp.MembershipCardGroupStoreGetBySellerGuid(sellerGuid);
                if (cardGroupStore != null && cardGroupStore.CardGroupId > 0)
                {
                    //有被設為適用店家
                    result = pcp.MembershipCardGroupGet(cardGroupStore.CardGroupId);
                }
                else
                {
                    //沒有任何卡可用
                }
            }
            return result;
        }

        public static bool SwitchCardGroupStatus(int cardGroupId, bool enable)
        {
            try
            {
                var result = false;
                var cardGroup = pcp.MembershipCardGroupGet(cardGroupId);
                if (enable)
                {
                    if (cardGroup.Status == (int)MembershipCardGroupStatus.Unpublished)
                    {
                        //未發布→發布

                        #region 檢查必填是否都有填(傳照片、填商家介紹、...等等)
                        MembershipCardGroupPermissionCollection permissions = pcp.MembershipCardGroupPermissionGetList(cardGroupId);
                        bool isVipOn = PcpFacade.IsRegularsServiceOn(cardGroupId, permissions, MembershipService.Vip);
                        bool isPointOn = PcpFacade.IsRegularsServiceOn(cardGroupId, permissions, MembershipService.Point);
                        bool isDepositOn = PcpFacade.IsRegularsServiceOn(cardGroupId, permissions, MembershipService.Deposit);
                        bool isVipSet = PcpFacade.IsRegularsServiceSet(cardGroupId, MembershipService.Vip);
                        bool isPointSet = PcpFacade.IsRegularsServiceSet(cardGroupId, MembershipService.Point);
                        bool isDepositSet = PcpFacade.IsRegularsServiceSet(cardGroupId, MembershipService.Deposit);

                        bool isSellerIntroSet = pcp.PcpIntroGetByCardGroupId(PcpIntroType.Seller, cardGroupId).Any();
                        var corporateImages = PcpFacade.GetPcpImageListByCardGroupIdAndType(cardGroupId, PcpImageType.CorporateImage);
                        bool isCorporateImageSet = corporateImages.Any();
                        bool isApplyStoreSet = true;//hardCode
                        #endregion

                        bool allowPublish = (isSellerIntroSet && isCorporateImageSet && isApplyStoreSet) &&
                                            ((isVipOn && isVipSet) || (isPointOn && isPointSet) || (isDepositOn && isDepositSet));
                        if (allowPublish)
                        {
                            cardGroup.Status = (int)MembershipCardGroupStatus.Published;
                            cardGroup.PublishTime = DateTime.Now;
                            result = pcp.MembershipCardGroupSet(cardGroup) > 0;
                        }
                    }
                    else
                    {
                        //隱藏→發布
                        cardGroup.Status = (int)MembershipCardGroupStatus.Published;
                        result = pcp.MembershipCardGroupSet(cardGroup) > 0;
                    }
                }
                else
                {
                    //發布→隱藏
                    cardGroup.Status = (int)MembershipCardGroupStatus.Hidden;
                    result = pcp.MembershipCardGroupSet(cardGroup) > 0;
                }

                return result;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SwitchCardGroupStatus Error:cardGroupId={0}\n{1}", cardGroupId, ex);
                return false;
            }
        }
        #endregion

        #region membership_card_group_store
        /// <summary>
        /// 取得可以設為適用分店的所有清單
        /// </summary>
        /// <param name="cardGroupId"></param>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static List<RegularsApplyStore> GetRegularsApplyStoreList(int cardGroupId, Guid sellerGuid)
        {
            List<RegularsApplyStore> applyStoreList = new List<RegularsApplyStore>();

            //根據賣家結構樹撈出組織
            var sellerList = SellerFacade.GetChildrenSellers(sellerGuid, true).Where(x => x.TempStatus != (int)SellerTempStatus.Returned && //不可以是退回重審的店家
                                                                                          x.IsCloseDown == false && //正常營業
                                                                                          !string.IsNullOrWhiteSpace(x.StoreAddress)//沒地址有可能是總店，必須排除；如果有實體店應該出現卻被排除，須補齊該店地址資料
                                                                                    ).ToList();
            var sellerGuidList = sellerList.Select(x => x.Guid).ToList();

            List<MembershipCardGroupStore> groupStoreList = pcp.MembershipCardGroupStoreGet(sellerGuidList).ToList();

            //已被其他熟客卡設為"適用分店"
            var alreadyUsedByOtherCard = groupStoreList.Where(x => x.CardGroupId != cardGroupId).Select(x => x.StoreGuid);
            //已自己開卡
            var alreadyPublishCard = pcp.MembershipCardGroupGetBySellerGuids(sellerGuidList).ToList().Where(x => x.Id != cardGroupId).Select(x => x.SellerGuid);

            int index = 0;
            var applyStores = from store in sellerList
                              where !alreadyUsedByOtherCard.Contains(store.Guid)
                                 && !alreadyPublishCard.Contains(store.Guid)
                              select new RegularsApplyStore()
                              {
                                  Index = index++,
                                  StoreGuid = store.Guid,
                                  StoreName = store.SellerName,
                                  StoreAddress = CityManager.CityTownShopStringGet(store.StoreTownshipId.GetValueOrDefault()) + store.StoreAddress,
                                  StoreTel = store.StoreTel,
                                  IsSelected = groupStoreList.Any(x => x.StoreGuid == store.Guid)
                              };
            applyStoreList = applyStores.ToList();
            return applyStoreList;
        }

        public static bool RegularsApplyStoreSave(int cardGroupId, RegularsApplyStoreInputModel storeGuidList)
        {
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    MembershipCardGroupStoreCollection collection = new MembershipCardGroupStoreCollection();
                    foreach (var storeGuid in storeGuidList.StoreGuidList)
                    {
                        Guid guid = new Guid();
                        if (Guid.TryParse(storeGuid, out guid))
                        {
                            collection.Add(new MembershipCardGroupStore()
                            {
                                CardGroupId = cardGroupId,
                                StoreGuid = guid
                            });
                        }
                    }

                    if (collection.Count() > 0)
                    {
                        pcp.MembershipCardGroupStoreDel(cardGroupId);
                        pcp.MembershipCardGroupStoreSet(collection);
                    }
                    else
                    {
                        return false;
                    }

                    trans.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("RegularsApplyStoreSave Error:cardGroupId={0}\n{1}", cardGroupId, ex);
                return false;
            }
        }

        public static bool MembershipCardGroupStoreGuidIsExists(int groupId, Guid storeGuid)
        {
            return pcp.MembershipCardGroupStoreGuidIsExists(groupId, storeGuid);
        }

        #endregion

        #region view_membership_card_group
        /// <summary>
        /// 取得帳號擁有編輯權限，且有開啟熟客卡功能的店家列表
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public static List<ViewMembershipCardGroup> GetSellerListWhichEnabledRegularsSystemByAccountId(string accountId)
        {
            var result = new List<ViewMembershipCardGroup>();

            #region 店家自己開卡
            var sellerGuidList = mp.ResourceAclGetListByAccountId(accountId)
                                   .Where(x => x.ResourceType == (int)ResourceType.Seller || x.ResourceType == (int)ResourceType.Store)
                                   .Select(x => x.ResourceGuid).ToList();
            var cardGroup = pcp.ViewMembershipCardGroupGetList(sellerGuidList);
            result.AddRange(cardGroup);
            #endregion

            #region 店家被設為適用分店
            var cardGroupIdList = pcp.MembershipCardGroupStoreGet(sellerGuidList).Select(x => x.CardGroupId).ToList();//被設為適用分店
            var cardGroupApply = pcp.ViewMembershipCardGroupGetList(cardGroupIdList);
            result.AddRange(cardGroupApply);
            #endregion

            return result.GroupBy(x => x.CardGroupId).Select(x => x.FirstOrDefault()).ToList();
        }

        #endregion

        #region pcp_user_token, pcp_polling_user_status

        public static PcpUserToken GetPcpUserToken(int userId)
        {
            PcpUserToken puk = null;

            if (pcp.PcpCheckUserTokenIsExistById(userId, ref puk)) //當天是否存在
            {
                puk.IsTokenRefreshed = true;
                puk.RefreshToken = GenerateToken(userId);
                puk.ModifiedTime = DateTime.Now;
                puk.ExpiredTime = DateTime.Now.AddMinutes(10);
                pcp.PcpUserTokenSet(puk);
            }
            else
            {
                puk = new PcpUserToken
                {
                    AccessToken = GenerateToken(userId),
                    CreatedTime = DateTime.Now,
                    IsTokenRefreshed = false,
                    UserId = userId,
                    ExpiredTime = DateTime.Now.AddMinutes(10)
                };
                pcp.PcpUserTokenSet(puk);
            }

            //old polling_user_status alive=0
            var userPollingCollection = pcp.PcpPollingStatusGetAllById(userId);
            userPollingCollection.ForEach(x => x.Alive = (int)PcpPollingUserAlive.Dead);
            pcp.PcpPollingStatusCollectionSet(userPollingCollection);

            //insert new polling status
            PcpPollingUserStatus ppus = new PcpPollingUserStatus
            {
                UserId = userId,
                UserToken = puk.RefreshToken ?? puk.AccessToken,
                ExpiredTime = puk.ExpiredTime,
                Action = (int)PcpPollingUserActionType.Waiting,
                Alive = (int)PcpPollingUserAlive.Alive,
                CreatedTime = DateTime.Now
            };
            pcp.PcpPollingUserStatusSet(ppus);

            return puk;

        }

        public static PcpUserToken GetPcpUserTokenByToken(string userToken)
        {
            return pcp.PcpUserTokenGetByToken(userToken);
        }

        public static PcpPollingUserStatus GetPollUserStatusByTokenWithLog(string userToken, bool needCheckAlive, int userId)
        {
            logger.InfoFormat("PcpPollingUserStatusCollection DeBug:userId={0}|userToken={1}|DateTime={2}", userId, userToken, DateTime.Now);
            var ppus = new PcpPollingUserStatus();
            var ppsc = pcp.PcpPollingStatusCollectionGetByToken(userToken);
            string json = string.Empty;
            if (ppsc.Count() > 0)
            {
                json = JsonConvert.SerializeObject(ppsc);
                logger.InfoFormat("PcpPollingUserStatusCollection DeBug:userId{0}|userToken={1}|DateTime={2}|Count={3}|List={4}", userId, userToken, DateTime.Now, ppsc.Count(), json);

                if (needCheckAlive)
                {
                    ppus = ppsc.Where(p => p.Alive == 1).OrderByDescending(p => p.ExpiredTime).FirstOrDefault();
                }
                else
                {
                    ppus = ppsc.OrderByDescending(p => p.ExpiredTime).FirstOrDefault();
                }
                json = JsonConvert.SerializeObject(ppus);
                logger.InfoFormat("PcpPollingUserStatusCollection DeBug:userId={0}|userToken={1}|DateTime={2}|Count={3}|List={4}", userId, userToken, DateTime.Now, ppus == null ? 0 : 1, json);
            }
            else
            {
                logger.InfoFormat("PcpPollingUserStatusCollection DeBug:userId={0}|userToken={1}|DateTime={2}|Count={3}", userId, userToken, DateTime.Now, 0);
            }
            return ppus;
        }

        public static PcpPollingUserStatus GetPollUserStatusByToken(string userToken, bool needCheckAlive)
        {
            return pcp.PcpPollingStatusGetByToken(userToken, needCheckAlive);
        }

        public static PcpPollingUserStatus GetPollingStatusById(int userId)
        {
            return pcp.PcpPollingStatusGetById(userId);
        }

        private static string GenerateToken(int userId)
        {
            return Guid.NewGuid().ToString("N") + "_" + Security.MD5Hash(userId.ToString() + DateTime.Now);
        }

        #endregion

        #region User Agree Status

        public static PcpUserAgreeStatus GetPcpUserAgreeStatusById(int userId)
        {
            return pcp.PcpAgreeStatusGetById(userId);
        }

        #endregion

        #region pcp_image
        public static List<PcpImage> GetPcpImageListByCardGroupIdAndType(int cardGroupId, PcpImageType type)
        {
            List<PcpImage> imageList = pcp.PcpImageGetByCardGroupId(cardGroupId).ToList();
            if (type != PcpImageType.None)
            {
                imageList = imageList.Where(x => x.ImageType == (byte)type).ToList();
            }
            return imageList;
        }

        /// <summary>
        /// 熟客卡圖片上傳相關更新PcpImage，並回傳需要刪除的檔案名稱
        /// </summary>
        /// <returns></returns>
        public static bool PcpImageSave(int sellerUserId, int cardGroupId, PcpImageType imgType, Dictionary<string, int> fileNameToSeqDic, out List<string> toDeleteFiles)
        {
            try
            {
                DateTime nowTime = DateTime.Now;
                toDeleteFiles = new List<string>();

                PcpImageCollection imageCollection = pcp.PcpImageGetByCardGroupId(cardGroupId, imgType);

                var originFileNames = imageCollection.Select(x => x.ImageUrl.Split(",")[1]).ToList();
                var newFileNames = fileNameToSeqDic.Select(x => x.Key).ToList();

                //待刪除實體檔清單
                toDeleteFiles = originFileNames.Where(x => !newFileNames.Contains(x)).ToList();

                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    #region pcp_image資料更新
                    foreach (var pcpImage in imageCollection)
                    {
                        var fileName = pcpImage.ImageUrl.Split(",")[1];
                        int seq = 0;
                        if (fileNameToSeqDic.TryGetValue(fileName, out seq))
                        {
                            #region 修改
                            pcpImage.Sequence = seq;
                            pcpImage.ModifyTime = nowTime;
                            pcpImage.ModifyUserId = sellerUserId;

                            pcp.PcpImageSet(pcpImage);
                            #endregion
                        }
                        else
                        {
                            #region 註解刪除
                            pcp.PcpImageDelete(pcpImage.Id, sellerUserId);
                            #endregion
                        }
                    }

                    var toAddPcpImage = newFileNames.Where(x => !originFileNames.Contains(x)).ToList();
                    foreach (var fileName in toAddPcpImage)
                    {
                        #region 新增
                        int seq = 0;
                        fileNameToSeqDic.TryGetValue(fileName, out seq);
                        PcpImage newImage = new PcpImage()
                        {
                            SellerUserId = sellerUserId,
                            ImageType = (byte)imgType,
                            CreateTime = nowTime,
                            ImageUrl = string.Format("{0},{1}", cardGroupId, fileName),
                            IsDelete = false,
                            ModifyTime = nowTime,
                            ModifyUserId = sellerUserId,
                            Sequence = seq,
                            GroupId = cardGroupId
                        };

                        pcp.PcpImageSet(newImage);
                        #endregion
                    }
                    #endregion

                    trans.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                toDeleteFiles = new List<string>();
                logger.ErrorFormat("PcpImageSave Error:sellerUserId={0}；cardGroupId={1}\n{2}", sellerUserId, cardGroupId, ex);
                return false;
            }
        }

        #endregion

        #region pcp_intro
        //改寫自BackendUtility的public static bool SellerIntroSet(int cardGroupId, string[] intro)
        public static bool SellerIntroSet(int cardGroupId, RegularsSellerIntroInputModel model)
        {
            pcp.PcpIntroDeleteByCardGroupId(PcpIntroType.Seller, cardGroupId);
            var introCol = new PcpIntroCollection();
            introCol.Add(new PcpIntro
            {
                IntroType = (byte)PcpIntroType.Seller,
                GroupId = cardGroupId,
                IntroContent = model.Intro,
                CreateTime = DateTime.Now
            });
            return pcp.PcpIntroSet(introCol);
        }
        #endregion

        #region pcp_marquee
        public static bool RegularsSellerMarqueeSave(int cardGroupId, RegularsSellerMarqueeInputModel inputModel)
        {
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    Dictionary<int, PcpMarquee> originalDic = pcp.PcpMarqueeGetListByCardGroupId(cardGroupId).GroupBy(x => x.Id).ToDictionary(x => x.Key, x => x.FirstOrDefault());
                    List<string> newList = inputModel.Marquees;

                    #region 新增或修改
                    foreach (var marqueeAndId in inputModel.Marquees)
                    {
                        var split = marqueeAndId.Split("###");//Marquee###Id
                        string marqueeText = split[0];
                        int id = Convert.ToInt32(split[1]);

                        var marquee = new PcpMarquee();

                        if (originalDic.TryGetValue(id, out marquee))
                        {
                            //修改
                            marquee.Marquee = marqueeText;
                        }
                        else
                        {
                            //新增
                            marquee = new PcpMarquee()
                            {
                                Marquee = marqueeText,
                                GroupId = cardGroupId
                            };
                        }
                        pcp.PcpMarqueeSet(marquee);
                    }
                    #endregion

                    #region 刪除
                    var oldIds = originalDic.Select(x => x.Key).ToList();
                    var newIds = inputModel.Marquees.Select(x => Convert.ToInt32(x.Split("###")[1])).ToList();
                    var toDeleted = oldIds.Where(x => !newIds.Contains(x)).ToList();
                    foreach (var delId in toDeleted)
                    {
                        pcp.PcpMarqueeDelete(cardGroupId, delId);
                    }
                    #endregion

                    trans.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("RegularsSellerMarqueeSave Error:cardGroupId={0}\n{1}", cardGroupId, ex);
                return false;
            }
        }
        #endregion

        #region pcp_point_collect_rule
        /// <summary>
        /// 第一次點數設定
        /// </summary>
        /// <param name="sellerUserId"></param>
        /// <param name="cardGroupId"></param>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        public static bool PointFirstSettingSave(int sellerUserId, int cardGroupId, PointFirstSettingInputModel inputModel)
        {
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    var nowTime = DateTime.Now;

                    //集點
                    var addCollectSuccess = PcpPointCollectRuleSave(sellerUserId, cardGroupId, inputModel.CollectValue);
                    //兌點
                    var addExchangeSuccess = PcpPointExchangeRuleSave(sellerUserId, cardGroupId, new PointExchangeRuleParentInputModel() { ExchangeRules = inputModel.ExchangeRules });
                    //注意事項
                    var addRemarkSuccess = PcpPointRemarkSave(sellerUserId, cardGroupId, new PointRemarkParentInputModel() { RemarkList = inputModel.Remarks });

                    if (addCollectSuccess && addExchangeSuccess && addRemarkSuccess)
                    {
                        trans.Complete();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("PointFirstSettingSave Error:sellerUserId={0}\ncardGroupId={1}\n{2}", sellerUserId, cardGroupId, ex);
                return false;
            }
        }

        public static bool PcpPointCollectRuleSave(int sellerUserId, int cardGroupId, int? price)
        {
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    var collectRule = pcp.PcpPointCollectRuleGetListByCardGroupId(cardGroupId).FirstOrDefault();
                    var nowTime = DateTime.Now;

                    if (price.HasValue && price > 0)
                    {
                        if (collectRule != null)
                        {
                            if (collectRule.CollectValue != Convert.ToInt32(price))
                            {
                                string previousContent = string.Format(Message.RegularsPointCollectRule, collectRule.CollectValue);

                                //修改
                                collectRule.Type = (int)PcpPointCollectRuleType.ByPrice;
                                collectRule.CollectValue = Convert.ToInt32(price);
                                collectRule.ModifiedId = sellerUserId;
                                collectRule.ModifiedTime = nowTime;

                                pcp.PcpPointCollectRuleSet(collectRule);

                                //change_log
                                string content = string.Format(Message.RegularsPointCollectRule, collectRule.CollectValue);
                                PcpPointRuleChangeLog log = new PcpPointRuleChangeLog()
                                {
                                    GroupId = cardGroupId,
                                    RuleId = collectRule.Id,
                                    RuleType = (int)PcpPointRuleChangeLogRuleType.CollectRule,
                                    ChangeType = (int)PcpPointRuleChangeLogChangeType.Update,
                                    Content = content,
                                    PreviousContent = previousContent,
                                    ModifyTime = nowTime,
                                    ModifyId = sellerUserId
                                };

                                pcp.PcpPointRuleChangeLogSet(log);
                            }
                        }
                        else
                        {
                            //新增
                            collectRule = new PcpPointCollectRule()
                            {
                                CardGroupId = cardGroupId,
                                CollectValue = Convert.ToInt32(price),
                                Enabled = true,
                                CreateId = sellerUserId,
                                CreateTime = nowTime,
                                ModifiedId = sellerUserId,
                                ModifiedTime = nowTime
                            };

                            pcp.PcpPointCollectRuleSet(collectRule);

                            //change_log
                            string content = string.Format(Message.RegularsPointCollectRule, collectRule.CollectValue);
                            PcpPointRuleChangeLog log = new PcpPointRuleChangeLog()
                            {
                                GroupId = cardGroupId,
                                RuleId = collectRule.Id,
                                RuleType = (int)PcpPointRuleChangeLogRuleType.CollectRule,
                                ChangeType = (int)PcpPointRuleChangeLogChangeType.Create,
                                Content = content,
                                ModifyTime = nowTime,
                                ModifyId = sellerUserId
                            };

                            pcp.PcpPointRuleChangeLogSet(log);
                        }
                    }

                    trans.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("PcpPointCollectRuleSave Error:sellerUserId={0}\ncardGroupId={1}\n{2}", sellerUserId, cardGroupId, ex);
                return false;
            }
        }

        public static bool PointCollectRuleEnable(int sellerUserId, int cardGroupId, int ruleId, bool enable)
        {
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    //開關
                    var collectRule = pcp.PcpPointCollectRuleGet(cardGroupId, ruleId);
                    collectRule.Enabled = enable;
                    collectRule.ModifiedId = sellerUserId;
                    collectRule.ModifiedTime = DateTime.Now;

                    pcp.PcpPointCollectRuleSet(collectRule);

                    //change_log
                    string content = enable ? "啟用發送點數" : "停止發送點數";
                    PcpPointRuleChangeLog log = new PcpPointRuleChangeLog()
                    {
                        GroupId = cardGroupId,
                        RuleId = ruleId,
                        RuleType = (int)PcpPointRuleChangeLogRuleType.CollectRule,
                        ChangeType = (int)PcpPointRuleChangeLogChangeType.Update,
                        Content = content,
                        ModifyTime = DateTime.Now,
                        ModifyId = sellerUserId
                    };

                    pcp.PcpPointRuleChangeLogSet(log);

                    trans.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("PointCollectRuleEnable Error:sellerUserId={0}\ncardGroupId={1}\n{2}", sellerUserId, cardGroupId, ex);
                return false;
            }

        }
        public static bool CheckIsFirstSetting(int cardGroupId)
        {
            var result = pcp.PcpPointCollectRuleGetListByCardGroupId(cardGroupId).Count() == 0;
            return result;
        }
        #endregion

        #region pcp_point_exchange_rule
        public static bool PcpPointExchangeRuleSave(int sellerUserId, int cardGroupId, PointExchangeRuleParentInputModel inputModel)
        {
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    DateTime nowTime = DateTime.Now;
                    Dictionary<int, PcpPointExchangeRule> originalDic = pcp.PcpPointExchangeRuleGetListByCardGroupId(cardGroupId).GroupBy(x => x.Id).ToDictionary(x => x.Key, x => x.FirstOrDefault());

                    List<int> oldList = originalDic.Keys.ToList();
                    List<int> newList = inputModel.ExchangeRules.Where(x => x.Id != 0).Select(x => x.Id).ToList();

                    //var toUpdateList = oldList.Intersect(newList);
                    //var toAddList = newList.Except(oldList);
                    var toDeleteList = oldList.Except(newList);

                    #region 新增或修改 (含變更紀錄)
                    foreach (var exchangeRule in inputModel.ExchangeRules)
                    {
                        PcpPointExchangeRule rule = new PcpPointExchangeRule();//原本的
                        originalDic.TryGetValue(exchangeRule.Id, out rule);

                        if (rule != null)
                        {
                            if (rule.Point != exchangeRule.Point ||
                                rule.ItemName != exchangeRule.ItemName)
                            {
                                PcpPointExchangeRule before = rule.Clone();//change_log用

                                //修改
                                rule.Point = exchangeRule.Point;
                                rule.ItemName = exchangeRule.ItemName;
                                rule.ModifiedId = sellerUserId;
                                rule.ModifiedTime = nowTime;

                                pcp.PcpPointExchangeRuleSet(rule);

                                //change_log
                                var content = string.Format(Message.RegularsPointExchangeRule, rule.Point, rule.ItemName);
                                var previousContent = string.Format(Message.RegularsPointExchangeRule, before.Point, before.ItemName);
                                PcpPointRuleChangeLog log = new PcpPointRuleChangeLog()
                                {
                                    GroupId = cardGroupId,
                                    RuleId = before.Id,
                                    RuleType = (int)PcpPointRuleChangeLogRuleType.ExchangeRule,
                                    ChangeType = (int)PcpPointRuleChangeLogChangeType.Update,
                                    Content = content,
                                    PreviousContent = previousContent,
                                    ModifyTime = nowTime,
                                    ModifyId = sellerUserId
                                };

                                pcp.PcpPointRuleChangeLogSet(log);
                            }
                        }
                        else
                        {
                            //新增
                            rule = new PcpPointExchangeRule()
                            {
                                CardGroupId = cardGroupId,
                                Point = exchangeRule.Point,
                                ItemName = exchangeRule.ItemName,
                                Enabled = true,
                                CreateId = sellerUserId,
                                CreateTime = nowTime,
                                ModifiedId = sellerUserId,
                                ModifiedTime = nowTime
                            };

                            pcp.PcpPointExchangeRuleSet(rule);

                            //change_log
                            var content = string.Format(Message.RegularsPointExchangeRule, rule.Point, rule.ItemName);
                            PcpPointRuleChangeLog log = new PcpPointRuleChangeLog()
                            {
                                GroupId = cardGroupId,
                                RuleId = rule.Id,
                                RuleType = (int)PcpPointRuleChangeLogRuleType.ExchangeRule,
                                ChangeType = (int)PcpPointRuleChangeLogChangeType.Create,
                                Content = content,
                                ModifyTime = nowTime,
                                ModifyId = sellerUserId
                            };

                            pcp.PcpPointRuleChangeLogSet(log);
                        }
                    }
                    #endregion

                    #region 刪除 (含變更紀錄)
                    foreach (var id in toDeleteList)
                    {
                        pcp.PcpPointExchangeRuleDelete(cardGroupId, id);

                        //change_log
                        PcpPointExchangeRule before = new PcpPointExchangeRule();//原本的
                        originalDic.TryGetValue(id, out before);
                        var previousContent = string.Format(Message.RegularsPointExchangeRule, before.Point, before.ItemName);
                        PcpPointRuleChangeLog log = new PcpPointRuleChangeLog()
                        {
                            GroupId = cardGroupId,
                            RuleId = before.Id,
                            RuleType = (int)PcpPointRuleChangeLogRuleType.ExchangeRule,
                            ChangeType = (int)PcpPointRuleChangeLogChangeType.Delete,
                            PreviousContent = previousContent,
                            ModifyTime = nowTime,
                            ModifyId = sellerUserId
                        };

                        pcp.PcpPointRuleChangeLogSet(log);
                    }
                    #endregion

                    trans.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("PcpPointExchangeRuleSave Error:sellerUserId={0}\ncardGroupId={1}\n{2}", sellerUserId, cardGroupId, ex);
                return false;
            }
        }

        /// <summary>
        /// 會員優惠儲存
        /// </summary>
        /// <param name="sellerUserId"></param>
        /// <param name="cardGroupId"></param>
        /// <param name="inputModel"></param>
        /// <remarks>
        /// 從PCPController SetCards移植
        /// </remarks>
        /// <returns></returns>
        public static bool SaveCard(int sellerUserId, int cardGroupId, SaveCardInputModel inputModel, out string errorMsg)
        {
            try
            {
                errorMsg = string.Empty;
                int cardVersionId = inputModel.VersionId;

                #region 有傳卡片資料進來則進行卡片異動作業

                if (inputModel.Card != null)
                {
                    DateTime openTime = LunchKingSite.BizLogic.Component.API.ApiSystemManager.DateTimeFromDateTimeString(inputModel.Card.OpenTime);
                    DateTime closeTime, quarterTimeStart;
                    Helper.GetQuarterDateRange(openTime.Year, (Quarter)Helper.GetQuarterByDate(openTime), out quarterTimeStart, out closeTime);

                    #region 卡片樣式的檢查(MembershipCardGroupVersion)，上一季有的話用上一季的
                    //※現在用卡片樣式的只有改版前的熟客卡店家，新增的店家已不用，此region以後要整個移除)

                    if (inputModel.VersionId == 0)
                    {
                        var cardVersion = new MembershipCardGroupVersion
                        {
                            CardGroupId = cardGroupId,
                            Status = (int)MembershipCardStatus.Draft,
                            OpenTime = openTime,
                            CloseTime = closeTime,
                            ImagePath = string.Empty,
                            BackgroundColorId = BonusFacade.DefaultBackgroundColorId,
                            BackgroundColor = string.Empty,
                            BackgroundImageId = BonusFacade.DefaultCardBackgroundImageId,
                            BackgroundImage = string.Empty,
                            IconImageId = BonusFacade.DefaultCardIconImageId,
                            IconImage = string.Empty
                        };

                        //如果該季還沒開過卡就沒有卡片樣式
                        //所以取得最新的卡片樣式(上一季)，如果上一季有就用上一季的
                        MembershipCardGroupVersion version = pcp.MembershipCardGroupVersionGetCardGroupLastData(cardGroupId);
                        if (version.IsLoaded)
                        {
                            cardVersion.ImagePath = version.ImagePath;
                            cardVersion.BackgroundColorId = version.BackgroundColorId;
                            cardVersion.BackgroundColor = version.BackgroundColor;
                            cardVersion.BackgroundImageId = version.BackgroundImageId;
                            cardVersion.BackgroundImage = version.BackgroundImage;
                            cardVersion.IconImageId = version.IconImageId;
                            cardVersion.IconImage = version.IconImage;
                        }

                        pcp.MembershipCardGroupVersionSet(cardVersion);
                        cardVersionId = cardVersion.Id;
                    }
                    #endregion

                    var card = (inputModel.Card.CardId.GetValueOrDefault() != 0) ? BonusFacade.MembershipCardGet((int)inputModel.Card.CardId) : new MembershipCard();

                    //「未開立過的優惠」或「下一季優惠」才可編輯存檔
                    if (!card.IsLoaded || openTime > DateTime.Now)
                    {
                        if (!card.IsLoaded)
                        {
                            card.CardGroupId = cardGroupId;
                            card.VersionId = cardVersionId;
                            card.Level = inputModel.Card.Level;

                            //狀態為作廢則enabled設為取消 TODO:須把enabled拿掉 改以status取代
                            card.Enabled = inputModel.Card.Status != MembershipCardStatus.Cancel;
                            card.Status = (byte)MembershipCardStatus.Open;

                            card.OpenTime = openTime;
                            card.CloseTime = closeTime;

                            card.CreateId = sellerUserId;
                            card.CreateTime = DateTime.Now;
                        }
                        card.PaymentPercent = inputModel.Card.PaymentPercent == 0 ? 1 : inputModel.Card.PaymentPercent;
                        card.Others = inputModel.Card.OtherPremiums;
                        card.OrderNeeded = inputModel.Card.OrderNeeded;
                        card.AmountNeeded = (decimal?)inputModel.Card.AmountNeeded;
                        card.ConditionalLogic = inputModel.Card.ConditionalLogic;
                        card.Instruction = inputModel.Card.Instruction;
                        card.ModifyId = sellerUserId;
                        card.ModifyTime = DateTime.Now;

                        pcp.MembershipCardSet(card);
                        //修改所有卡片皆相同的資料(是否可合併優惠、平假日是否可用，等等)
                        pcp.MembershipCardUpdateSynchronizationData(cardGroupId, cardVersionId, (AvailableDateType)inputModel.AvailableDateType, inputModel.CombineUse);

                        if (inputModel.Card.Level == (int)MembershipCardLevel.Level1 &&
                            DateTime.Now.IsBetween(card.OpenTime, card.CloseTime) &&
                            inputModel.Card.Status == MembershipCardStatus.Open && card.Enabled && card.Id > 0)
                        {
                            //將有領取零級卡者升級一級卡片
                            pcp.UserMembershipCardUpdateLevelZeroToOne(cardGroupId, card.Id);
                        }
                    }
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                errorMsg = ex.ToString();
                return false;
            }
        }
        #endregion

        #region pcp_point_remark
        public static bool PcpPointRemarkSave(int sellerUserId, int cardGroupId, PointRemarkParentInputModel inputModel)
        {
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    DateTime nowTime = DateTime.Now;
                    Dictionary<int, PcpPointRemark> originalDic = pcp.PcpPointRemarkGetListByCardGroupId(cardGroupId).GroupBy(x => x.Id).ToDictionary(x => x.Key, x => x.FirstOrDefault());

                    List<int> oldList = originalDic.Keys.ToList();
                    List<int> newList = inputModel.RemarkList.Where(x => x.Id != 0).Select(x => x.Id).ToList();

                    var toDeleteList = oldList.Except(newList);

                    #region 新增或修改
                    foreach (var obj in inputModel.RemarkList)
                    {
                        PcpPointRemark remark = new PcpPointRemark();
                        originalDic.TryGetValue(obj.Id, out remark);

                        if (remark != null)
                        {
                            if (remark.Remark != obj.RemarkText)
                            {
                                //修改
                                remark.Remark = obj.RemarkText;
                                remark.ModifyId = sellerUserId;
                                remark.ModifyTime = nowTime;

                                pcp.PcpPointRemarkSet(remark);
                            }
                        }
                        else
                        {
                            //新增
                            remark = new PcpPointRemark()
                            {
                                GroupId = cardGroupId,
                                Remark = obj.RemarkText,
                                ModifyId = sellerUserId,
                                ModifyTime = nowTime
                            };

                            pcp.PcpPointRemarkSet(remark);
                        }
                    }
                    #endregion

                    #region 刪除
                    foreach (var id in toDeleteList)
                    {
                        pcp.PcpPointRemarkDelete(cardGroupId, id);
                    }
                    #endregion

                    trans.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("PcpPointRemarkSave Error:sellerUserId={0}\ncardGroupId={1}\n{2}", sellerUserId, cardGroupId, ex);
                return false;
            }
        }
        #endregion

        #region pcp_user_point

        /// <summary>
        /// 取得使用者剩餘點數大於零的紀錄清單
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static PcpUserPointCollection GetUserRemainPointCollection(int groupId, int userId)
        {
            var userPointCollection = pcp.PcpUserRemainPointListGetByGroupIdAndUserId(groupId, userId);
            return userPointCollection;
        }

        public static int GetUserRemainPoint(int groupId, int userId)
        {
            var userPoint = GetUserRemainPointCollection(groupId, userId).Sum(x => x.RemainPoint);
            return userPoint;
        }
        #endregion

        #region view_pcp_user_point
        public static ViewPcpUserPointCollection GetViewPcpUserPointCollection(int userId, int cardGroupId)
        {
            return pcp.ViewPcpUserPointCollectionGet(userId, cardGroupId).OrderByAsc("user_point_id");
        }

        public static ViewPcpUserPointCollection GetViewPcpUserPointCollectionByCardGroupId(int groupId, Guid? sellerGuid, DateTime? queryTime, int pageNumber, int pageSize)
        {
            return pcp.ViewPcpUserPointCollectionGetByCardGroupId(groupId, sellerGuid, queryTime, pageNumber, pageSize);
        }
        #endregion

        #region pcp_deposit_menu
        public static bool SellerDepositMenuAdd(int cardGroupId, SellerDepositAddMenuInputModel inputModel)
        {
            var nowDatetime = DateTime.Now;
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    #region 新增活動
                    PcpDepositMenu menu = new PcpDepositMenu()
                    {
                        GroupId = cardGroupId,
                        Title = inputModel.Title,
                        STime = inputModel.STime,
                        ETime = inputModel.ETime,
                        Enabled = (int)PcpDepositMenuEnabled.Enabled,
                        CreatedId = Convert.ToString(inputModel.SellerUserId),
                        CreatedTime = nowDatetime
                    };
                    pcp.PcpDepositMenuSet(menu);
                    #endregion

                    #region 活動裡面新增品項
                    PcpDepositItemCollection collection = new PcpDepositItemCollection();
                    foreach (var item in inputModel.ItemList)
                    {
                        collection.Add(new PcpDepositItem()
                        {
                            DepositMenuId = menu.Id,
                            ItemName = item,
                            CreatedId = Convert.ToString(inputModel.SellerUserId),
                            CreatedTime = nowDatetime,
                            Size = ""
                        });
                    }
                    pcp.PcpDepositItemCollectionSet(collection);
                    #endregion

                    trans.Complete();
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SellerDepositMenuAdd Error:cardGroupId={0}\n{1}", cardGroupId, ex);
                return false;
            }
        }

        public static bool SellerDepositMenuEdit(int cardGroupId, SellerDepositEditMenuInputModel inputModel)
        {
            var nowDatetime = DateTime.Now;
            var depositAmounts = GetViewPcpUserDepositTotalAndRemain(cardGroupId, inputModel.MenuId);
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    if (depositAmounts.TotalAmount > 0)
                    {
                        #region 已有寄杯 只能編輯(預設數量、結束時間、是否啟用)
                        var menu = pcp.PcpDepositMenuGet(inputModel.MenuId);
                        menu.DefaultAmount = inputModel.DefaultAmount;
                        menu.ETime = inputModel.ETime;
                        menu.ModifiedTime = nowDatetime;
                        #endregion
                        pcp.PcpDepositMenuSet(menu);
                    }
                    else
                    {
                        #region 尚未有寄杯 都可以編輯
                        //活動
                        var menu = pcp.PcpDepositMenuGet(inputModel.MenuId);
                        menu.Title = inputModel.Title;
                        menu.DefaultAmount = inputModel.DefaultAmount;
                        menu.STime = inputModel.STime;
                        menu.ETime = inputModel.ETime;
                        menu.ModifiedTime = nowDatetime;
                        pcp.PcpDepositMenuSet(menu);

                        //商品
                        var oriItemIdList = pcp.ViewPcpSellerMenuItemCollectionGetByCardGroupIdAndMenuId(cardGroupId, menu.Id).Select(x => Convert.ToInt32(x.ItemId)).ToList();
                        var newItemList = new List<int>();
                        PcpDepositItemCollection collection = new PcpDepositItemCollection();

                        foreach (var itemNameAndId in inputModel.ItemList)
                        {
                            string itemName = itemNameAndId.Split("###")[0];//item_name###item_id
                            int itemId = Convert.ToInt32(itemNameAndId.Split("###")[1]);//item_name###item_id

                            if (itemId == 0)
                            {
                                #region 增加商品
                                collection.Add(new PcpDepositItem()
                                {
                                    DepositMenuId = menu.Id,
                                    ItemName = itemName,
                                    CreatedId = "",//TODO
                                    CreatedTime = nowDatetime,
                                    Size = ""
                                });
                                #endregion
                            }
                            else if (oriItemIdList.Contains(itemId))
                            {
                                #region 修改商品
                                var item = pcp.PcpDepositItemGet(itemId);
                                item.ItemName = itemName;
                                #endregion
                                newItemList.Add(itemId);
                                collection.Add(item);
                            }
                        }

                        pcp.PcpDepositItemCollectionSet(collection);
                        #region 刪除商品
                        foreach (var toDelete in oriItemIdList.Except(newItemList))
                        {
                            pcp.PcpDepositItemDelete(toDelete);
                        }
                        #endregion
                        #endregion
                    }
                    trans.Complete();
                }

                return true;
            }
            catch (Exception ex)
            {
                //log
                logger.ErrorFormat("SellerDepositMenuEdit Error:cardGroupId={0}&menuId={1}\n{2}", cardGroupId, inputModel.MenuId, ex);
                return false;
            }
        }

        public static bool SellerDepositMenuEnable(int cardGroupId, int menuId, int enable)
        {
            return pcp.PcpDepositMenuEnable(cardGroupId, menuId, enable);
        }

        public static bool SellerDepositMenuDelete(int cardGroupId, int menuId, out string message)
        {
            bool result = false;
            message = string.Empty;
            var depositAmounts = GetViewPcpUserDepositTotalAndRemain(cardGroupId, menuId);
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    if (depositAmounts.TotalAmount > 0)
                    {
                        message = "活動已有寄杯，不可刪除";
                    }
                    else
                    {
                        #region 刪除商品
                        bool successDelItem = true;
                        var items = pcp.ViewPcpSellerMenuItemCollectionGetByCardGroupIdAndMenuId(cardGroupId, menuId);
                        foreach (var item in items)
                        {
                            bool delResult = pcp.PcpDepositItemDelete(Convert.ToInt32(item.ItemId));
                            if (!delResult)
                            {
                                successDelItem = false;
                            }
                        }
                        #endregion

                        #region 刪除活動
                        bool successDelMenu = pcp.PcpDepositMenuDelete(cardGroupId, menuId);
                        #endregion

                        if (!successDelItem || !successDelMenu)
                        {
                            message = "刪除失敗";
                        }
                        else
                        {
                            trans.Complete();
                            result = true;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SellerDepositMenuDelete Error:cardGroupId={0}&menuId={1}\n{2}", cardGroupId, menuId, ex);
                return false;
            }
        }
        #endregion

        #region pcp_deposit_template
        public static List<PcpDepositTemplate> PcpDepositTemplateGetListByCardGroupId(int cardGroupId)
        {
            List<PcpDepositTemplate> list = new List<PcpDepositTemplate>();
            list = pcp.PcpDepositTemplateGetListByCardGroupId(cardGroupId);
            return list;
        }

        public static bool PcpDepositTemplateAddOrEdit(int cardGroupId, DepositTemplateEditInputModel inputModel)
        {
            var success = false;

            var template = pcp.PcpDepositTemplateGet(cardGroupId, inputModel.Id);

            if (template.Template != inputModel.Template)
            {
                template.Template = inputModel.Template;

                if (template.Id == 0)
                {
                    template.GroupId = cardGroupId;
                    template.Seq = pcp.PcpDepositTemplateGetCurrentSeq(cardGroupId, inputModel.Type);
                    template.Type = inputModel.Type;
                }
                success = pcp.PcpDepositTemplateSet(template);
            }
            else
            {
                success = true;
            }

            return success;
        }

        public static bool PcpDepositTemplateDelete(int cardGroupId, int templateId)
        {
            return pcp.PcpDepositTemplateDelete(cardGroupId, templateId);
        }
        #endregion

        #region pcp_user_deposit_log

        public static PcpUserDepositLog GetPcpUserDepositLogByUSerIdOrderByTime(int userId)
        {
            return pcp.PcpUserDepositLogGetByUSerIdOrderByTime(userId);
        }
        public static PcpUserDepositLogCollection GetPcpUserDepositLogDetailByUSerId(int userId)
        {
            return pcp.PcpUserDepositLogDetailGetByUSerId(userId);
        }
        #endregion

        #region pcp_user_deposit
        public static PcpUserDeposit GetUserDepositItemByUserAndId(int userId, int depositId)
        {
            return pcp.UserDepositItemGetByUserAndId(userId, depositId);
        }
        #endregion

        #region pcp_user_deposit_coupon
        public static PcpDepositCouponCollection GetPcpDepositCouponCollection(int depositId)
        {
            return pcp.PcpDepositCouponCollectionGet(depositId);
        }
        #endregion

        #region view_pcp_seller_menu_item
        public static ViewPcpSellerMenuItemCollection GetViewPcpSellerMenuItemCollectionByCardGroupId(int cardGroupId)
        {
            return pcp.ViewPcpSellerMenuItemCollectionGetByCardGroupId(cardGroupId);
        }
        public static ViewPcpSellerMenuItemCollection GetViewPcpSellerMenuItemCollectionByCardGroupIdAndMenuId(int cardGroupId, int menuId)
        {
            return pcp.ViewPcpSellerMenuItemCollectionGetByCardGroupIdAndMenuId(cardGroupId, menuId);
        }
        public static List<ViewPcpSellerMenuItem> GetViewPcpSellerMenuItemCollectionByCardGroupIdInEffective(int cardGroupId)
        {
            return pcp.ViewPcpSellerMenuItemCollectionGetByCardGroupIdInEffective(cardGroupId).ToList()
                      .OrderByDescending(x => x.MenuId).ThenBy(x => x.ItemId).ToList();
        }
        #endregion

        #region view_pcp_user_deposit
        /// <summary>
        /// 取得店家所有活動的寄杯數量與剩餘數量
        /// </summary>
        /// <param name="cardGroupId">熟客卡GroupId</param>
        /// <returns></returns>
        public static ViewPcpUserDepositCollection GetViewPcpUserDepositTotalAndRemain(int cardGroupId)
        {
            var result = pcp.ViewPcpUserDepositGetDepositTotalAndRemain(cardGroupId);
            return result;
        }
        /// <summary>
        /// 取得店家活動的寄杯數量與剩餘數量
        /// </summary>
        /// <param name="cardGroupId">熟客卡GroupId</param>
        /// <param name="menuId">活動Id</param>
        /// <returns></returns>
        public static ViewPcpUserDeposit GetViewPcpUserDepositTotalAndRemain(int cardGroupId, int menuId)
        {
            var result = pcp.ViewPcpUserDepositGetDepositTotalAndRemain(cardGroupId, menuId);
            return result;
        }

        public static ViewPcpUserDepositCollection GetViewPcpUserDepositListByUserIdAndCardGroupId(int userId, int cardGroupId)
        {
            return pcp.ViewPcpUserDepositCollectionGetByUserAndCardGroup(userId, cardGroupId);
        }
        public static List<ViewPcpUserDeposit> GetViewPcpUserDepositListWhichStillHaveRemain(int userId, int cardGroupId)
        {
            return pcp.ViewPcpUserDepositCollectionGetByUserAndCardGroup(userId, cardGroupId).ToList().Where(x => x.RemainAmount > 0).ToList();
        }

        public static ViewPcpUserDeposit GetViewPcpUserDepositCollectionByUserAndDepositId(int userId, int userDepositItemId)
        {
            return pcp.ViewPcpUserDepositCollectionGetByUserAndDepositId(userId, userDepositItemId);
        }

        #endregion

        #region view_pcp_user_deposit_coupon
        public static ViewPcpUserDepositCouponCollection GetViewPcpUserDepositCouponCollectionByUserIdAndCardGroupId(int userId, int cardGroupId)
        {
            return pcp.ViewPcpUserDepositCouponCollectionGetByUserIdAndCardGroupId(userId, cardGroupId);
        }
        #endregion

        #region view_pcp_user_deposit_log

        public static ViewPcpUserDepositLogCollection GetViewPcpUserDepositLogCollectionByCardGroupId(int cardGroupId)
        {
            return pcp.ViewPcpUserDepositLogCollectionGetByCardGroupId(cardGroupId).OrderByAsc("create_time");
        }

        public static ViewPcpUserDepositLogCollection GetViewPcpUserDepositLogCollectionByUserIdAndCardGroupId(int userId, int cardGroupId)
        {
            return pcp.ViewPcpUserDepositLogCollectionGetByUserIdAndCardGroupId(userId, cardGroupId).OrderByAsc("create_time");
        }

        #endregion

        #region SellerMembershipService

        /// <summary>
        /// 回傳單一賣家擁有哪些熟客卡服務
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public List<MembershipService> GetSellerMembershipPermission(int cardGroupId)
        {
            List<MembershipService> result = new List<MembershipService>();
            var col = pcp.MembershipCardGroupPermissionGetList(cardGroupId);
            if (col.Any())
            {
                result.AddRange(col.Select(x => (MembershipService)x.Scope).ToList());
            }
            return result;
        }

        /// <summary>
        /// 確認單一賣家是否可開啟熟客卡服務
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public bool CheckSellerMembershipPermission(int cardGroupId)
        {
            return pcp.MembershipCardGroupPermissionGet(cardGroupId, MembershipService.Vip).IsLoaded;
        }

        /// <summary>
        /// 傳入Seller guid list, 回傳可開啟熟客卡服務的賣家Guid
        /// </summary>
        /// <param name="sellerGuids"></param>
        /// <returns></returns>
        public List<int> FilterSellerMembershipPermission(List<int> cardGroupIds)
        {
            var smpc = pcp.MembershipCardGroupPermissionGetList(cardGroupIds).ToList();
            return
                smpc.Where(x => x.Scope == (int)MembershipService.Vip).Select(x => x.CardGroudId).Distinct().ToList();
        }

        #endregion

        #region VbsCompanyDetail

        /// <summary>
        /// 新增熟客卡公司資訊
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="sellerUserId"></param>
        /// <returns></returns>
        public static VbsCompanyDetail AddVbsCompanyDetail(Guid sellerGuid, int sellerUserId)
        {
            var seller = SellerFacade.SellerGet(sellerGuid);
            if (!seller.IsLoaded)
            {
                return new VbsCompanyDetail();
            }

            var m = MemberFacade.GetMember(sellerUserId);
            if (!m.IsLoaded)
            {
                return new VbsCompanyDetail();
            }

            var detail = new VbsCompanyDetail
            {
                UserId = sellerUserId,
                CreateId = config.SystemUserId,
                CreateTime = DateTime.Now,
                SellerGuid = sellerGuid
            };

            return pcp.VbsCompanyDetailSet(detail);
        }

        public static VbsCompanyDetail GetVbsCompanyDetail(Guid sellerGuid)
        {
            return pcp.VbsCompanyDetailGet(sellerGuid);
        }

        #endregion VbsCompanyDetail

        #region user_get_card_daily_report
        public static UserGetCardDailyReportCollection ImmediatelyCountUserGetCard(int cardGroupId, DateTime start, DateTime end)
        {
            return pcp.ImmediatelyCountUserGetCard(cardGroupId, start, end);
        }
        #endregion

        #region 寄杯、集點、熟客優惠交易完成資訊

        public static PcpTransResultModel GetPcpTransResult(int userId)
        {
            PcpTransResultModel trans = new PcpTransResultModel();

            //30分鐘內最新一筆
            var pcpOrder = pcp.PcpOrderGetByUserId(userId)
                                .Where(x => DateTime.Now < x.OrderTime.AddMinutes(30))
                                .OrderByDescending(x => x.OrderTime).FirstOrDefault();

            if (pcpOrder != null && pcpOrder.OrderType != null)
            {
                switch ((PcpOrderType)pcpOrder.OrderType)
                {
                    case PcpOrderType.MembershipCard:
                        trans = GetPcpOfferTransResult(pcpOrder);
                        break;
                    case PcpOrderType.Deposit:
                        trans = GetPcpDepositTransResult(pcpOrder);
                        break;
                    case PcpOrderType.Point:
                        trans = GetPcpPointTransResult(pcpOrder);
                        break;
                }
            }

            return trans;
        }


        private static PcpTransResultModel GetPcpPointTransResult(PcpOrder order)
        {
            //集點
            var userPointLog = pcp.ViewPcpUserPointCollectionGetByUserId(order.UserId)
                             .Where(x => DateTime.Now < x.CreateTime.AddMinutes(30))
                             .OrderByDescending(x => x.CreateTime).ToList();

            StringBuilder sb = new StringBuilder();
            string transMsg = string.Empty;
            string tranTitle = string.Empty;

            if (userPointLog.Any())
            {
                sb.AppendLine(string.Format("您在{0}", userPointLog.First().StoreName));

                if (userPointLog.First().SetType == (int)PcpUserPointSetType.Collect)
                {
                    var collectLog = userPointLog.Where(x => x.SetType == (int)PcpUserPointSetType.Collect).ToList();
                    if (collectLog.Any())
                    {
                        sb.AppendLine(string.Format("消費金額{0}元", Decimal.ToInt32(collectLog.First().Price ?? 0)));
                        sb.Append(string.Format("獲得點數{0}點", collectLog.First().TotalPoint));
                        transMsg = sb.ToString();
                        tranTitle = "集點成功";
                    }
                }
                else if (userPointLog.First().SetType == (int)PcpUserPointSetType.Exchange)
                {
                    var exchangeLog = userPointLog.Where(x => x.SetType == (int)PcpUserPointSetType.Exchange).ToList();
                    if (exchangeLog.Any())
                    {
                        var exchangeRule = pcp.PcpPointExchangeRuleGet(userPointLog.First().CardGroupId, (int)exchangeLog.First().PointRuleId);
                        sb.AppendLine(string.Format("兌換{0}x{1}", exchangeRule.ItemName, exchangeLog.First().Count));
                        sb.Append(string.Format("使用點數{0}點", Math.Abs(exchangeLog.First().TotalPoint)));
                        transMsg = sb.ToString();
                        tranTitle = "兌點成功";
                    }
                }
                else //更正
                {
                    var exchangeLog = userPointLog.Where(x => x.SetType == (int)PcpUserPointSetType.Correct).ToList();
                    if (exchangeLog.Any())
                    {
                        sb.AppendLine(string.Format("更正點數:{0}", exchangeLog.First().TotalPoint));
                        sb.Append(string.Format("更正原因:{0}", exchangeLog.First().ItemName));
                        transMsg = sb.ToString();
                        tranTitle = "更正點數成功";
                    }
                }
            }

            int groupId = 0;
            var mcg = GetMembershipCardGroupBySellerGuid(userPointLog.First().SellerGuid);
            groupId = mcg.Id;

            PcpTransResultModel result = new PcpTransResultModel();
            result.Type = PcpTransactionType.Point;
            result.ResultMessage = transMsg;
            result.GroupId = groupId;
            result.Title = tranTitle;

            return result;
        }

        private static PcpTransResultModel GetPcpDepositTransResult(PcpOrder order)
        {
            //寄杯明細
            var userDepositLog =
                pcp.ViewPcpUserDepositLogCollectionGetByUserId(order.UserId)
                   .Where(x => DateTime.Now < x.CreateTime.AddMinutes(30))
                   .OrderByDescending(x => x.CreateTime).ToList();

            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("您在{0}", userDepositLog.First().SellerName));

            //活動
            var userDepositItems =
                pcp.ViewPcpUserDepositCollectionGetByUserAndCardGroup(userDepositLog.First().UserId, (int)userDepositLog.First().GroupId).ToList();

            List<string> contents = new List<string>();
            string transMsg = string.Empty;
            string tranTitle = string.Empty;

            if (userDepositLog.First().DepositType == (int)PcpUserDepositLogType.Add)
            {
                //寄杯and兌換
                var addLog = userDepositLog.Where(x => x.DepositType == (int)PcpUserDepositLogType.Add);
                if (addLog.Any())
                {
                    sb.AppendLine("「" + userDepositLog.First().Title + "」活動");
                    contents.Add(string.Format("購買{0}x{1}", addLog.First().ItemName, addLog.First().Amount));

                    var afterUse = userDepositLog.Where(x => x.CreateTime == addLog.First().CreateTime
                                                  && x.DepositType == (int)PcpUserDepositLogType.Use).ToList(); //購買完，順便兌換
                    if (afterUse.Any())
                    {
                        contents.Add(string.Format("兌換x{0}", Math.Abs(afterUse.First().Amount)));
                    }
                    contents.Add(string.Format("寄杯x{0}", userDepositItems.First(x => x.UserDepositId == addLog.First().DepositId).RemainAmount));
                    sb.AppendLine(string.Join(",", contents));

                    tranTitle = "寄杯成功";
                    transMsg = sb.ToString();
                }
            }
            else if (userDepositLog.First().DepositType == (int)PcpUserDepositLogType.Use)
            {
                //兌換
                var useLog = userDepositLog.Where(x => x.DepositType == (int)PcpUserDepositLogType.Use
                                            && x.CreateTime == userDepositLog.First().CreateTime).ToList();
                if (useLog.Any())
                {
                    foreach (var ul in useLog)
                    {
                        contents = new List<string>();
                        contents.Add(string.Format("兌換{0}x{1}", ul.ItemName, Math.Abs(ul.Amount)));
                        contents.Add(string.Format("寄杯x{0}", userDepositItems.First(x => x.UserDepositId == ul.DepositId).RemainAmount));
                        sb.AppendLine("「" + userDepositLog.First().Title + "」活動:" + string.Join(",", contents));
                        sb.AppendLine();
                    }

                    tranTitle = "寄杯成功";
                    transMsg = sb.ToString();
                }
            }
            else
            {
                //更正
                var correctLog = userDepositLog.Where(x => x.DepositType == (int)PcpUserDepositLogType.Correct).ToList();
                if (correctLog.Any())
                {
                    sb.AppendLine("「" + userDepositLog.First().Title + "」活動");
                    sb.AppendLine(string.Format("更正寄杯:{0}", correctLog.First().Amount > 0 ? "+" + correctLog.First().Amount.ToString() : correctLog.First().Amount.ToString()));
                    sb.AppendLine(string.Format("更正原因:{0}", correctLog.First().Description));

                    tranTitle = "更正寄杯成功";
                    transMsg = sb.ToString();
                }
            }

           
            int groupId = 0;
            var mcg = GetMembershipCardGroupBySellerGuid(userDepositLog.First().SellerGuid);
            groupId = mcg.Id;

            PcpTransResultModel result = new PcpTransResultModel();
            result.Type = PcpTransactionType.Deposit;
            result.ResultMessage = transMsg;
            result.GroupId = groupId;
            result.Title = tranTitle;

            return result;
        }

        private static PcpTransResultModel GetPcpOfferTransResult(PcpOrder order)
        {
            StringBuilder sb = new StringBuilder();
            
            var storeName = sp.SellerGet(order.StoreGuid).SellerName;
            sb.Append(string.Format("您在{0}", storeName));
            sb.AppendLine(string.Format("消費金額{0}元", Decimal.ToInt32(order.Amount ?? 0)));
            var userMembershipCard = pcp.UserMembershipCardGet((int)order.CardId);
            var memberCard = pcp.MembershipCardGet(userMembershipCard.CardId);
            sb.AppendLine(string.Format("目前等級的為{0}",
                 Helper.GetEnumDescription((MembershipCardLevel)Enum.ToObject(typeof(MembershipCardLevel), memberCard.Level))));

            int groupId = 0;
            var mcg = GetMembershipCardGroupBySellerGuid(order.StoreGuid);
            groupId = mcg.Id;

            PcpTransResultModel result = new PcpTransResultModel();
            result.Type = PcpTransactionType.Membership;
            result.ResultMessage = sb.ToString();
            result.GroupId = groupId;
            result.Title = "核銷成功";

            return result;
        }

        #endregion

        #region 熟客系統底層
        public static bool IsRegularsServiceOn(int cardGroupId, MembershipCardGroupPermissionCollection permissions, MembershipService scope)
        {
            bool isServiceOn = false;
            if (cardGroupId > 0 && permissions != null)
            {
                isServiceOn = permissions.Where(x => x.Scope == (int)scope).Count() > 0;
            }
            return isServiceOn;
        }
        /// <summary>
        /// 檢查熟客系統相關功能，有無設定
        /// </summary>
        /// <param name="cardGroupId">熟客功能PK</param>
        /// <param name="scope">服務種類</param>
        /// <returns></returns>
        public static bool IsRegularsServiceSet(int cardGroupId, MembershipService scope)
        {
            bool isServiceSet = false;
            switch (scope)
            {
                case MembershipService.Vip:
                    var membershipCards = pcp.MembershipCardGetByGroupId(cardGroupId)
                                             .Where(x => x.OpenTime <= DateTime.Now && DateTime.Now < x.CloseTime && x.Status == (int)MembershipCardStatus.Open).ToList();
                    isServiceSet = membershipCards.Any() && membershipCards.FirstOrDefault(x => x.Level == (int)MembershipCardLevel.Level1) != null;
                    break;
                case MembershipService.Point:
                    isServiceSet = pcp.PcpPointExchangeRuleGetListByCardGroupId(cardGroupId).Any(x => x.Enabled == true);
                    break;
                case MembershipService.Deposit:
                    isServiceSet = GetViewPcpSellerMenuItemCollectionByCardGroupId(cardGroupId).ToList().Any(x => x.Enabled == 1);
                    break;
                default:
                    break;
            }

            return isServiceSet;
        }
        #endregion

        #region Member_Consumption_Detail 消費/核銷紀錄

        public static List<SelectListItem> GetUseTimeRangeDropDownList(int defaultValue)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Value = "0", Text = "24小時以內", Selected = ((int)ConsumptionDetailUseTimeRange.Hour24 == defaultValue) });
            list.Add(new SelectListItem() { Value = "1", Text = "72小時以內", Selected = ((int)ConsumptionDetailUseTimeRange.Hour72 == defaultValue) });
            list.Add(new SelectListItem() { Value = "2", Text = "一週以內", Selected = ((int)ConsumptionDetailUseTimeRange.AWeek == defaultValue) });
            list.Add(new SelectListItem() { Value = "3", Text = "一個月以內", Selected = ((int)ConsumptionDetailUseTimeRange.AMonth == defaultValue) });
            list.Add(new SelectListItem() { Value = "4", Text = "三個月以內", Selected = ((int)ConsumptionDetailUseTimeRange.ThreeMonths == defaultValue) });
            list.Add(new SelectListItem() { Value = "5", Text = "全部使用紀錄", Selected = ((int)ConsumptionDetailUseTimeRange.All == defaultValue) });
            return list;
        }

        public static List<SelectListItem> GetUseServiceItemDropDownList(int defaultValue, bool isNeedAll)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (isNeedAll)
            {
                list.Add(new SelectListItem() { Value = "0", Text = "全部服務", Selected = (0 == defaultValue) });
            }

            list.Add(new SelectListItem() { Value = "1", Text = "會員優惠", Selected = ((int)MembershipService.Vip == defaultValue) });
            list.Add(new SelectListItem() { Value = "2", Text = "寄杯功能", Selected = ((int)MembershipService.Deposit == defaultValue) });
            list.Add(new SelectListItem() { Value = "3", Text = "集點功能", Selected = ((int)MembershipService.Point == defaultValue) });
            return list;
        }

        /// <summary>
        /// 取得消費明細資料
        /// </summary>
        /// <param name="userId">使用者userId</param>
        /// <param name="cardId">熟客卡號</param>
        /// <param name="cardGroupId"></param>
        /// <param name="useTimeRange">消費時間範圍</param>
        /// <param name="useServiceItem">消費項目</param>
        /// <returns></returns>
        public static ConsumptionDetailModel GetMemberConsumptionDetailData(int userId, int cardGroupId, int useTimeRange, int useServiceItem)
        {
            ConsumptionDetailModel model = new ConsumptionDetailModel();
            model.CardGroupId = cardGroupId;
            model.UserId = userId;

            #region 資料篩選邏輯
            DateTime? queryTime = DateTime.Now;
            switch (useTimeRange)
            {
                case (int)ConsumptionDetailUseTimeRange.Hour24:
                    queryTime = queryTime.Value.AddDays(-1);
                    break;
                case (int)ConsumptionDetailUseTimeRange.Hour72:
                    queryTime = queryTime.Value.AddDays(-3);
                    break;
                case (int)ConsumptionDetailUseTimeRange.AWeek:
                    queryTime = queryTime.Value.AddDays(-7);
                    break;
                case (int)ConsumptionDetailUseTimeRange.AMonth:
                    queryTime = queryTime.Value.AddMonths(-1);
                    break;
                case (int)ConsumptionDetailUseTimeRange.ThreeMonths:
                    queryTime = queryTime.Value.AddMonths(-3);
                    break;
                case (int)ConsumptionDetailUseTimeRange.All:
                    queryTime = null;
                    break;
            }

            switch (useServiceItem)
            {
                case (int)MembershipService.Vip:   //會員優惠明細格式
                    model.CardDetailList = GetViewPcpOrderInfoCollectionByUserIdAndCardGroupId(userId, cardGroupId);
                    model.ViewList = GetCardData(model.CardDetailList.ToList());
                    break;
                case (int)MembershipService.Deposit: //寄杯明細格式
                    model.DepositDetailList = GetViewPcpUserDepositLogCollectionByUserIdAndCardGroupId(userId, cardGroupId).ToList();
                    foreach (var depositItemId in model.DepositDetailList.GroupBy(p => p.DepositId).Select(p => p.Key))
                    {
                        model.ViewList.AddRange(GetDepositData(model.DepositDetailList.Where(p => p.DepositId == depositItemId).ToList()));
                    }
                    break;
                case (int)MembershipService.Point://集點明細格式
                    model.PointDetailList = GetViewPcpUserPointCollection(userId, cardGroupId);
                    model.ViewList = GetPointData(model.PointDetailList.ToList());
                    break;
                default:
                    //會員優惠明細格式
                    model.CardDetailList = GetViewPcpOrderInfoCollectionByUserIdAndCardGroupId(userId, cardGroupId);
                    model.ViewList.AddRange(GetCardData(model.CardDetailList.ToList()));
                    //寄杯明細格式
                    model.DepositDetailList = GetViewPcpUserDepositLogCollectionByUserIdAndCardGroupId(userId, cardGroupId).ToList();
                    foreach (var depositItemId in model.DepositDetailList.GroupBy(p => p.DepositId).Select(p => p.Key))
                    {
                        model.ViewList.AddRange(GetDepositData(model.DepositDetailList.Where(p => p.DepositId == depositItemId).ToList()));
                    }

                    //集點明細格式
                    model.PointDetailList = GetViewPcpUserPointCollection(userId, cardGroupId);
                    model.ViewList.AddRange(GetPointData(model.PointDetailList.ToList()));
                    break;
            }

            //照時間篩選
            if (queryTime.HasValue)
            {
                model.ViewList = model.ViewList.Where(p => p.CreateTime >= queryTime.Value).ToList();
            }

            //照時間排序
            model.ViewList = model.ViewList.OrderByDescending(p => p.CreateTime).ToList();

            #endregion

            return model;
        }

        /// <summary>
        /// 取得核銷紀錄
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="useServiceItem"></param>
        /// <param name="useTimeRange"></param>
        /// <param name="cardGroupId"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public static ViewVerificationListModel GetVerificationList(ViewVerificationListModel model, Guid? sellerGuid, int useServiceItem, int useTimeRange, int cardGroupId, int pageNumber, int pageSize)
        {
            #region 資料篩選邏輯
            DateTime? queryTime = DateTime.Now;
            switch (useTimeRange)
            {
                case (int)ConsumptionDetailUseTimeRange.Hour24:
                    queryTime = queryTime.Value.AddDays(-1);
                    break;
                case (int)ConsumptionDetailUseTimeRange.Hour72:
                    queryTime = queryTime.Value.AddDays(-3);
                    break;
                case (int)ConsumptionDetailUseTimeRange.AWeek:
                    queryTime = queryTime.Value.AddDays(-7);
                    break;
                case (int)ConsumptionDetailUseTimeRange.AMonth:
                    queryTime = queryTime.Value.AddMonths(-1);
                    break;
                case (int)ConsumptionDetailUseTimeRange.ThreeMonths:
                    queryTime = queryTime.Value.AddMonths(-3);
                    break;
                case (int)ConsumptionDetailUseTimeRange.All:
                    queryTime = null;
                    break;
            }

            switch (useServiceItem)
            {
                case (int)MembershipService.Vip:   //會員優惠明細格式
                    model.VerifyModel.CardDetailList = GetViewPcpOrderInfoCollectionByCardGroupId(cardGroupId, sellerGuid, queryTime, pageNumber, pageSize);//全部會員資訊
                    model.TotalCount = model.VerifyModel.CardDetailList.Count();
                    if (model.TotalCount > 0)
                    {
                        model.StartDate = model.VerifyModel.CardDetailList.LastOrDefault().OrderTime;
                        model.EndDate = model.VerifyModel.CardDetailList.FirstOrDefault().OrderTime;
                    }

                    //Linq分頁
                    var tempData = model.VerifyModel.CardDetailList.Skip((pageNumber - 1) * pageSize).Take(pageSize);
                    foreach (int aUser in model.VerifyModel.CardDetailList.GroupBy(p => p.UserId).Select(p => p.Key))
                    {
                        model.VerifyModel.ViewList.AddRange(GetCardData(tempData.Where(p => p.UserId == aUser).ToList()));
                    }
                    break;
                case (int)MembershipService.Deposit: //寄杯明細格式
                    model.VerifyModel.DepositDetailList = GetViewPcpUserDepositLogCollectionByCardGroupId(cardGroupId).ToList();//全部會員資訊
                    List<DetailViewModel> tempList = new List<DetailViewModel>();
                    foreach (int aUser in model.VerifyModel.DepositDetailList.GroupBy(p => p.UserId).Select(p => p.Key)) //by user
                    {
                        var aUserData = model.VerifyModel.DepositDetailList.Where(p => p.UserId == aUser);
                        foreach (var depositItemId in aUserData.GroupBy(p => p.DepositId).Select(p => p.Key)) // by user and item
                        {
                            tempList.AddRange(GetDepositData(aUserData.Where(p => p.DepositId == depositItemId).ToList()));
                        }
                    }

                    //適用店家篩選
                    if (sellerGuid.HasValue)
                    {
                        tempList = tempList.Where(p => p.StoreGuid == sellerGuid.Value).ToList();
                    }

                    //照時間篩選
                    if (queryTime.HasValue)
                    {
                        tempList = tempList.Where(p => p.CreateTime >= queryTime.Value).ToList();
                    }

                    model.TotalCount = tempList.Count();
                    if (model.TotalCount > 0)
                    {
                        model.StartDate = tempList.OrderByDescending(p=>p.CreateTime).LastOrDefault().CreateTime;
                        model.EndDate = tempList.OrderByDescending(p => p.CreateTime).FirstOrDefault().CreateTime;
                    }

                    //Linq分頁
                    model.VerifyModel.ViewList.AddRange(tempList.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList());
                    break;
                case (int)MembershipService.Point://集點明細格式
                    model.VerifyModel.PointDetailList = GetViewPcpUserPointCollectionByCardGroupId(cardGroupId, sellerGuid, queryTime, pageNumber, pageSize);//全部會員資訊
                    model.TotalCount = model.VerifyModel.PointDetailList.Count();
                    if (model.TotalCount > 0)
                    {
                        model.StartDate = model.VerifyModel.PointDetailList.LastOrDefault().CreateTime;
                        model.EndDate = model.VerifyModel.PointDetailList.FirstOrDefault().CreateTime;
                    }

                    //Linq分頁
                    var tempData1 = model.VerifyModel.PointDetailList.Skip((pageNumber - 1) * pageSize).Take(pageSize);
                    foreach (int aUser in model.VerifyModel.PointDetailList.GroupBy(p => p.UserId).Select(p => p.Key))
                    {
                        model.VerifyModel.ViewList.AddRange(GetPointData(tempData1.Where(p => p.UserId == aUser).ToList()));
                    }
                    break;
                default:
                    //已拿掉全部服務
                    break;
            }
            //照時間排序
            model.VerifyModel.ViewList = model.VerifyModel.ViewList.OrderByDescending(p => p.CreateTime).ToList();

            #endregion

            return model;
        }
        
        /// <summary>
        /// 計算集點明細資料
        /// </summary>
        /// <param name="PointDetailList"></param>
        /// <returns></returns>
        public static List<DetailViewModel> GetPointData(List<ViewPcpUserPoint> PointDetailList)
        {
            ConsumptionDetailModel model = new ConsumptionDetailModel();

            #region 計算集點明細資料
            foreach (var i in PointDetailList)
            {
                var d = new DetailViewModel()
                {
                    CategoryType = MembershipService.Point,
                    CreateTime = i.CreateTime,
                    CreateTimeN = i.CreateTime.ToString("yyyy/MM/dd HH:mm"),
                    StoreName = i.StoreName,
                    VerifyAccount = i.AccountId,
                    VerifyName = i.AccountName,
                    MemberName = PcpFacade.GetRegularMemberName(i.MemberFirstName, i.MemberLastName, VendorRole.PcpAdmin),
                    StoreGuid = i.SellerGuid
                };
                switch (i.SetType)
                {
                    case (int)PcpUserPointSetType.Collect://新增
                        d.TageName = "發送點數";
                        d.SetType = PcpUserPointSetType.Collect;
                        d.OriginalAmount1 = Convert.ToInt32(i.Price);//消費金額N1
                        d.ChangeAmount2 = i.TotalPoint;//發送點數N2 免N3
                        break;
                    case (int)PcpUserPointSetType.Correct://更正
                        d.TageName = "更正點數";
                        d.SetType = PcpUserPointSetType.Correct;
                        d.OriginalAmount1 = i.FinalPoint - i.TotalPoint;
                        d.ChangeAmount2 = i.TotalPoint;
                        d.RemainAmount3 = i.FinalPoint;
                        break;
                    case (int)PcpUserPointSetType.Exchange://兌換
                        d.TageName = "兌換點數";
                        d.SetType = PcpUserPointSetType.Exchange;
                        d.OriginalAmount1 = i.FinalPoint - i.TotalPoint;
                        d.ChangeAmount2 = i.TotalPoint;
                        d.RemainAmount3 = i.FinalPoint;
                        d.ItemName = i.ItemName;
                        break;
                };
                model.ViewList.Add(d);
            }
            #endregion

            return model.ViewList;
        }

        /// <summary>
        /// 計算寄杯明細資料
        /// </summary>
        /// <param name="DepositDetailList"></param>
        /// <returns></returns>
        public static List<DetailViewModel> GetDepositData(List<ViewPcpUserDepositLog> DepositDetailList)
        {
            ConsumptionDetailModel model = new ConsumptionDetailModel();
            #region 計算寄杯明細資料 
            int currentTotal = 0;//當下總寄杯數
            List<int> notSingleUseId = new List<int>();
            foreach (var i in DepositDetailList)
            {
                var d = new DetailViewModel()
                {
                    CategoryType = MembershipService.Deposit,
                    LogType = (PcpUserDepositLogType)Enum.Parse(typeof(PcpUserDepositLogType), i.DepositType.ToString()),
                    Title = i.Title,
                    ItemName = i.ItemName,
                    CreateTime = i.CreateTime,
                    CreateTimeN = i.CreateTime.ToString("yyyy/MM/dd HH:mm"),
                    StoreName = i.SellerName,
                    VerifyAccount = i.AccountId,
                    VerifyName = i.AccountName,
                    MemberName = PcpFacade.GetRegularMemberName(i.MemberFirstName, i.MemberLastName, VendorRole.PcpAdmin),
                    StoreGuid = i.SellerGuid
                };
                switch (i.DepositType)
                {
                    case (int)PcpUserDepositLogType.Add://會一次產生兩筆, 要注意用createTime做groupby
                        if (currentTotal == 0) { currentTotal = i.Amount; }//+
                        var currentUse = DepositDetailList.Where(p => p.CreateTime == i.CreateTime && p.DepositType == (int)PcpUserDepositLogType.Use).FirstOrDefault();

                        d.TageName = Helper.GetDescription(PcpUserDepositLogType.Add);
                        d.OriginalAmount1 = currentTotal;//購買數量
                        d.ChangeAmount2 = (currentUse != null) ? currentUse.Amount : 0;//現場兌換
                        d.RemainAmount3 = (currentUse != null) ? (currentTotal + currentUse.Amount) : currentTotal;//剩餘寄杯
                        model.ViewList.Add(d);

                        if (currentUse != null)
                        {
                            notSingleUseId.Add(currentUse.LogId);
                            currentTotal += currentUse.Amount;//扣掉[現場]兌換的 (currentUse.Amount會是負數)
                        }
                        break;
                    case (int)PcpUserDepositLogType.Use://單筆
                                                        // 不存在相同時間的一筆, 代表是單獨兌換
                                                        // 相同時間的兩筆, 一定是新增寄杯+現場兌換
                        if (!notSingleUseId.Contains(i.LogId))
                        {
                            d.TageName = Helper.GetDescription(PcpUserDepositLogType.Use);
                            d.OriginalAmount1 = currentTotal;//寄杯數量
                            d.ChangeAmount2 = i.Amount;//現場兌換 
                            d.RemainAmount3 = (currentTotal + i.Amount); //繼續寄杯 
                            model.ViewList.Add(d);

                            currentTotal += i.Amount;//扣掉兌換 (i.Amount會是負數)
                        }

                        break;
                    case (int)PcpUserDepositLogType.Correct://單筆
                        d.TageName = Helper.GetDescription(PcpUserDepositLogType.Correct);
                        d.OriginalAmount1 = currentTotal;//原有數量
                        d.ChangeAmount2 = i.Amount;//更正數量 
                        d.RemainAmount3 = (currentTotal + i.Amount); //更正後數量
                        model.ViewList.Add(d);

                        currentTotal += i.Amount;//變動量 (i.Amount會是正負數)
                        break;
                }
            }
            #endregion
            return model.ViewList;
        }

        /// <summary>
        /// 使用熟客卡優惠明細資料
        /// </summary>
        /// <param name="CardDetailList"></param>
        /// <returns></returns>
        public static List<DetailViewModel> GetCardData(List<ViewPcpOrderInfo> CardDetailList)
        {
            ConsumptionDetailModel model = new ConsumptionDetailModel();
            #region 整理會員優惠明細資料
            foreach (var i in CardDetailList)
            {
                model.ViewList.Add(new DetailViewModel()
                {
                    TageName = "使用熟客優惠",
                    CategoryType = MembershipService.Vip,
                    OriginalAmount1 = Convert.ToInt32(i.Amount),//消費金額N1
                    CreateTime = i.OrderTime,
                    CreateTimeN = i.OrderTime.ToString("yyyy/MM/dd HH:mm"),
                    StoreName = i.SellerName,
                    VerifyAccount = i.AccountId,
                    VerifyName = i.AccountName,
                    MemberName = PcpFacade.GetRegularMemberName(i.MemberFirstName, i.MemberLastName, VendorRole.PcpAdmin),
                    StoreGuid = i.StoreGuid
                });
            }
            #endregion
            return model.ViewList;
        }

        #endregion

        #region 團購、熟客系統核銷紀錄

        /// <summary>
        /// 團購紀錄
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="day"></param>
        /// <param name="pageNumber"></param>
        /// <param name="only"></param>
        /// <param name="mailTo"></param>
        /// <param name="vms"></param>
        /// <returns></returns>
        public static VerificationCouponListByStoreModel GetVerificationCouponListByStore(string storeId, int? day, int? pageNumber, string only, string mailTo, VbsMembership vms)
        {
            bool isPostBack = day != null || pageNumber != null;
            pageNumber = pageNumber == null ? 0 : pageNumber - 1;
            day = day ?? 1;
            const int pageSize = 10;
            var now = DateTime.Today;

            var deals = (new VbsVendorAclMgmtModel(vms.AccountId)).GetAllowedDealUnits(null, DeliveryType.ToShop, VbsRightFlag.VerifyShop)
                                    .Where(x => now >= x.DealUseStartTime && now <= x.DealUseEndTime.Value.AddDays(day.Value));

            var model = new VerificationCouponListByStoreModel();
            if (deals.Count() > 0)
            {
                deals.GroupBy(x => new { x.StoreGuid, x.StoreName })
                     .Select(s => new { StoreGuid = s.Key.StoreGuid.Value, StoreName = s.Key.StoreName })
                     .ForEach(x => model.StoreList.Add(x.StoreGuid.ToString(), x.StoreName.Length > 9 ? x.StoreName.Substring(9, x.StoreName.Length - 9) : x.StoreName));

                var isSeller = deals.Any(x => x.IsSellerPermissionAllow);
                Guid? storeGuid = null;
                if (isSeller)
                {
                    model.StoreList.Add("all", "所有分店");
                    model.IsSellerPermission = true;
                }

                if (!string.IsNullOrEmpty(storeId) && storeId != "all")
                {
                    storeGuid = isSeller ? Guid.Parse(storeId) : storeGuid;
                    model.StoreId = storeGuid.ToString();
                }

                storeGuid = !isSeller || storeId == "all" ? null : storeGuid;
                only = !isPostBack && !isSeller ? "on" : only;
                model.IsOnlyAccount = !string.IsNullOrEmpty(only);

                var bids = deals.GroupBy(x => new { x.MerchandiseId, x.MerchandiseGuid })
                                .Select(s => new { MerchandiseId = s.Key.MerchandiseId, MerchandiseGuid = s.Key.MerchandiseGuid })
                                .ToDictionary(d => d.MerchandiseId, d => d.MerchandiseGuid);
                List<VbsVerificationCoupon> list = new List<VbsVerificationCoupon>();
                foreach (var bid in bids)
                {
                    var items = vp.GetCashTrustLogGetListByProductGuid(bid.Value.Value, OrderClassification.LkSite)
                                    .Where(x => x.Status == (int)TrustStatus.Verified
                                            && (storeGuid != null
                                                ? x.StoreGuid == storeGuid
                                                : (isSeller
                                                    ? true
                                                    : model.StoreList.Any(y => y.Key == x.StoreGuid.ToString())))
                                                    && (only == "on" ? x.VerifyCreateId == vms.AccountId : true));
                    items.ForEach(x => list.Add(new VbsVerificationCoupon
                    {
                        DealName = string.Format(@"[{0}]{1}", bid.Key, x.ItemName),
                        VerifyCount = 1,
                        CouponId = x.CouponSequenceNumber,
                        VerifiedDateTime = x.VerifyTime,
                        VerifyAccount = x.VerifyCreateId,
                        VerifiedStore = x.StoreName,
                    }));
                }

                DateTime? newVerifyTime;
                List<VbsVerificationCoupon> data = new List<VbsVerificationCoupon>();
                if (list.Count > 0)
                {
                    if (!isSeller)
                    {
                        list.Where(x => (day > 0 ? x.VerifiedDateTime >= now.AddDays(-1 * day.Value) : true)
                                      && x.VerifyAccount == vms.AccountId)
                            .OrderByDescending(x => x.VerifiedDateTime)
                            .ForEach(x => data.Add(x));
                        newVerifyTime = data.Count > 0 ? data.Max(m => m.VerifiedDateTime) : null;
                        if (data.Count > 0)
                        {
                            data.ForEach(x => x.IsNew = x.VerifiedDateTime >= newVerifyTime.Value.AddSeconds(-3));
                        }
                        list.Where(x => (day > 0 ? x.VerifiedDateTime >= now.AddDays(-1 * day.Value) : true)
                                      && x.VerifyAccount != vms.AccountId)
                                      .OrderByDescending(x => x.VerifiedDateTime)
                                      .ForEach(x => data.Add(x));
                        if (newVerifyTime == null)
                        {
                            newVerifyTime = data.Max(m => m.VerifiedDateTime);
                            data.ForEach(x => x.IsNew = x.VerifiedDateTime >= newVerifyTime.Value.AddSeconds(-3));
                        }
                    }
                    else
                    {
                        newVerifyTime = list.Max(m => m.VerifiedDateTime);
                        list.ForEach(x => x.IsNew = x.VerifiedDateTime >= newVerifyTime.Value.AddSeconds(-3));
                        data = list.Where(x => (day > 0 ? x.VerifiedDateTime >= now.AddDays(-1 * day.Value) : true)).OrderByDescending(x => x.VerifiedDateTime).ToList();
                    }
                }

                if (!string.IsNullOrEmpty(mailTo))
                {
                    //寄送憑證清冊
                    var dummy = VerificationFacade.GetVbsVerificationCouponInfosExcelByStore(data,
                                        string.Format("{0},{1},{2}", vms.AccountId, vms.Name, mailTo));
                }

                model.Coupons = new PagerList<VbsVerificationCoupon>(data.Skip(pageNumber.Value * pageSize).Take(pageSize),
                    new PagerOption(pageSize, pageNumber.Value * pageSize, VbsVerificationCoupon.Columns.VerifiedDateTime, false),
                    pageNumber.Value, data.Count);
            }
            return model;
        }

        #endregion
    }
}

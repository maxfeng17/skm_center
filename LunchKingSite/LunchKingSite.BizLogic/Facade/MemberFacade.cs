﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using log4net;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.BizLogic.Models;
using Group = LunchKingSite.BizLogic.Models.Group;
using System.Runtime.Caching;
using LunchKingSite.BizLogic.Models.API;
using LunchKingSite.BizLogic.Models.PponOrder;
using LunchKingSite.BizLogic.Models.BounPoints;

namespace LunchKingSite.BizLogic.Facade
{
    public class MemberFacade
    {
        private static IMemberProvider mp;
        private static IOrderProvider op;
        private static ILocationProvider lp;
        private static ISysConfProvider config;
        private static IPponProvider pp;
        private static IEventProvider ep;
        private static IHiDealProvider hp;
        private static IHumanProvider hump;
        private static IOAuthProvider oap;
        private static ISystemProvider systemp;

        private static IPCPProvider pcp;
        private static Core.IServiceProvider csp;
        private static ILog logger;
        private static string[] folder = { "original", "small", "large" };
        private static EmailAgent ea = new EmailAgent();
        private static string sMainDealTitle = string.Empty;
        private static string sMainDealName = string.Empty;
        private static string sEdmCode = "/17_systemEDM";
        private static string sEdmTitleCode = "&amp;type=title";
        private static string sEdmSubTitleCode = "&amp;type=subtitle";
        private static string sEdmImageCode = "&amp;type=image";
        private static string sEdmTakeCode = "&amp;type=takealook";
        private static string UniquIdItemKey = "UniquIdItemKey::";
        private static string memberCacheKey = "Member::{0}";
        private static int iPonDealNumber = Convert.ToInt16(SubscribeQuantitySent.PponEdmDealNo);     //從資料庫中取出4筆p好康，第一筆為Main Deal，其餘為Side Deal
        private static int iPeautyDealNumber = Convert.ToInt16(SubscribeQuantitySent.PeautyEdmDealNo); //從資料庫中取出1筆p玩美
        private static string sMainDealReasons = string.Empty;

        private static Guid _prebzGuid = Guid.Empty;
        public const string _USER_NOT_LOGIN = "userNotLogin";
        public const int _MEMBER_PIC_SIZE_LIMIT = 1024 * 1024 * 5;
        public static bool IsMobileAuthEnabled
        {
            get { return true; }
        }

        /// <summary>
        /// 訂單篩選條件 [v1.0]  
        /// 依據顯示的權重排序訂單類表顯示類別，供顯示訂單狀態時最為排序的順序
        /// </summary>
        private static List<CouponListFilterType> _couponListFilterTypes = new List<CouponListFilterType>
                                                                               {
                                                                                   CouponListFilterType.NotCompleted,
                                                                                   CouponListFilterType.Cancel,
                                                                                   CouponListFilterType.Expired,
                                                                                   CouponListFilterType.Used,
                                                                                   CouponListFilterType.NotUsed,
                                                                                   CouponListFilterType.NotExpired,
                                                                                   CouponListFilterType.Product,
                                                                                   CouponListFilterType.Event,
                                                                                   CouponListFilterType.None,
                                                                                   CouponListFilterType.Kind,
                                                                               };
        /// <summary>
        /// 訂單篩選條件 [v2.0] 
        /// </summary>
        /// <returns></returns>
        public static List<NewCouponListFilterType> _newCouponListFilterTypes()
        {
            var filterTypes = new List<NewCouponListFilterType>();
            filterTypes.Add(NewCouponListFilterType.None);
            if (config.EnableDepositCoffee)
            {
                filterTypes.Add(NewCouponListFilterType.DepositCoffee);
            }

            filterTypes.Add(NewCouponListFilterType.FamiCoffee);
            filterTypes.Add(NewCouponListFilterType.NotUsed);
            filterTypes.Add(NewCouponListFilterType.NotShipped);
            filterTypes.Add(NewCouponListFilterType.Fami);
            filterTypes.Add(NewCouponListFilterType.CouponOnly);
            filterTypes.Add(NewCouponListFilterType.Product);
            filterTypes.Add(NewCouponListFilterType.Event);
            filterTypes.Add(NewCouponListFilterType.Cancel);
            filterTypes.Add(NewCouponListFilterType.OverdueNotUsed);
            filterTypes.Add(NewCouponListFilterType.GroupCoupon);
            filterTypes.Add(NewCouponListFilterType.GroupCouponReady);
            filterTypes.Add(NewCouponListFilterType.GroupCouponNotUsed);
            return filterTypes;
        }

        /// <summary>
        /// 訂單篩選條件 [v3.0] 
        /// </summary>
        /// <returns></returns>
        public static List<MemberOrderMainFilterType> MemberOrderMainFilterTypes()
        {
            return new List<MemberOrderMainFilterType>
            {
                MemberOrderMainFilterType.All,
                MemberOrderMainFilterType.Goods,
                MemberOrderMainFilterType.Ppon,
                MemberOrderMainFilterType.Coffee
            };
        }

        /// <summary>
        /// 靜態建構子
        /// </summary>
        static MemberFacade()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            config = ProviderFactory.Instance().GetConfig();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            hump = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            systemp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            csp = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();
            oap = ProviderFactory.Instance().GetProvider<IOAuthProvider>();
            logger = LogManager.GetLogger(typeof(MemberFacade));
        }

        public Guid prebzGuid
        {
            get
            {
                return _prebzGuid;
            }
            set
            {
                _prebzGuid = value;
            }
        }

        /// <summary>
        /// 私有建構子
        /// </summary>
        private MemberFacade()
        {
        }

        #region 會員利金、購物金、PCash

        #region 紅利

        /// <summary>
        /// 依據輸入想要補入或扣除的紅利點數，產生紅利點數使用紀錄，餘額不足就不扣除。
        /// </summary>
        /// <param name="mpd">欲補入或扣除的紅利點數資訊</param>
        /// <param name="userName">使用紅利的會員</param>
        /// <param name="isMailToService">處理完成是否寄通知信給客服</param>
        /// <returns></returns>
        public static void MemberPromotionProcess(MemberPromotionDeposit mpd, string userName, bool isMailToService)
        {
            if (mpd != null)
            {
                if (mpd.PromotionValue > 0)
                {
                    mp.MemberPromotionDepositSet(mpd);
                }
                else if (mpd.PromotionValue < 0)
                {
                    DeductibleMemberPromotionDeposit(userName, (-1 * mpd.PromotionValue), mpd.Action, BonusTransactionType.ManualAdjustment,
                                           null, mpd.CreateId, true);
                }
            }

            string auditMsg = "補紅利 " + mpd.PromotionValue + " 點";
            CommonFacade.AddAudit(userName, AuditType.Member, auditMsg, mpd.CreateId, true);

            if (isMailToService)
            {
                SendMailToService("補紅利", auditMsg + " (" + MemberFacade.GetShortUserName(mpd.CreateId) + ")", false, SendPriorityType.Normal);
            }
        }

        public static void SendMailToService(string subject, string body, bool isHtml, SendPriorityType priority)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(config.AdminEmail);
            msg.To.Add(config.ServiceEmail);
            msg.Subject = subject;
            msg.Body = body;
            msg.IsBodyHtml = isHtml;
            PostMan.Instance().Send(msg, priority);
        }

        /// <summary>
        /// 依據輸入想要扣除的紅利點數，產生紅利點數使用紀錄，餘額不足就不扣除。
        /// </summary>
        /// <param name="userName">使用紅利的會員</param>
        /// <param name="deductibleMemberValue">欲扣除的紅利點數</param>
        /// <param name="action">說明文字</param>
        /// <param name="withdrawalType">扣除的型態</param>
        /// <param name="orderGuid">依據哪個訂單進行扣款的，可為NULL</param>
        /// <param name="createId">建立資料的使用者</param>
        /// <param name="isPassStartTime">TRUE:則不考慮紅利點數是否已生效，有就扣  FALSE:只扣除已生效的紅利點數</param>
        /// <param name="classification">訂單分類 預設值為 OrderClassification.LkSite </param>
        /// <param name="receivableId">應收帳款單號，預設為null</param>
        /// <returns></returns>
        public static bool DeductibleMemberPromotionDeposit(string userName, double deductibleMemberValue, string action, BonusTransactionType withdrawalType,
            Guid? orderGuid, string createId, bool isPassStartTime, OrderClassification classification = OrderClassification.LkSite, int? receivableId = null)
        {
            int userId = GetUniqueId(userName);
            return DeductibleMemberPromotionDeposit(userId,
                                                    deductibleMemberValue,
                                                    action, withdrawalType,
                                                    orderGuid,
                                                    createId, isPassStartTime,
                                                    false, classification, receivableId);
        }

        /// <summary>
        /// 依據輸入想要扣除的紅利點數，產生紅利點數使用紀錄
        /// </summary>
        /// <param name="userId">使用紅利的會員</param>
        /// <param name="deductibleMemberValue">欲扣除的紅利點數</param>
        /// <param name="action">說明文字</param>
        /// <param name="withdrawalType">扣除的型態</param>
        /// <param name="orderGuid">依據哪個訂單進行扣款的，可為NULL</param>
        /// <param name="createId">建立資料的使用者</param>
        /// <param name="isPassStartTime">TRUE:則不考慮紅利點數是否已生效，有就扣  FALSE:只扣除已生效的紅利點數</param>
        /// <param name="deductionWithNotEnough">TRUE:則額度不足扣到0  FALSE:額度不足就不扣</param>
        /// <param name="classification">訂單來源的類型</param>
        /// <param name="receivableId">應收帳款單號</param>
        /// <returns></returns>
        public static bool DeductibleMemberPromotionDeposit(int userId, double deductibleMemberValue, string action,
            BonusTransactionType withdrawalType, Guid? orderGuid, string createId, bool isPassStartTime,
            bool deductionWithNotEnough, OrderClassification classification = OrderClassification.LkSite, int? receivableId = null)
        {
            MemberPromotionWithdrawalCollection saveCol = GetDeductibleMemberPromotionDeposit(
                userId, deductibleMemberValue, action, withdrawalType, orderGuid,
                createId, isPassStartTime, deductionWithNotEnough, classification, receivableId);
            if ((saveCol == null) || (saveCol.Count == 0))
            {
                return false;
            }

            if (config.EnableParallelTestGetDeductibleMemberPromotionDepositBetaVersion)
            {
                MemberPromotionWithdrawalCollection saveCol2 = GetDeductibleMemberPromotionDepositBetaVersion(
                               userId, deductibleMemberValue, action, withdrawalType, orderGuid,
                               createId, isPassStartTime, deductionWithNotEnough, classification, receivableId);
                if (saveCol.Count != saveCol2.Count)
                {
                    logger.Warn("平行測試 候選版錯誤:DeductibleMemberPromotionDeposit, saveCol.Count不一致");
                }
                else
                {
                    for (int i = 0; i < saveCol.Count; i++)
                    {
                        if (saveCol[i].DepositId != saveCol2[i].DepositId)
                        {
                            logger.Warn("平行測試 候選版錯誤:DeductibleMemberPromotionDeposit, saveCol[" + i + "]-DepositId 不一致");
                        }
                        else if (saveCol[i].PromotionValue != saveCol2[i].PromotionValue)
                        {
                            logger.Warn("平行測試 候選版錯誤:DeductibleMemberPromotionDeposit, saveCol[" + i + "]-PromotionValue 不一致");
                        }
                        else if (saveCol[i].OrderGuid != saveCol2[i].OrderGuid)
                        {
                            logger.Warn("平行測試 候選版錯誤:DeductibleMemberPromotionDeposit, saveCol[" + i + "]-OrderGuid 不一致");
                        }
                        else if (saveCol[i].CreateId != saveCol2[i].CreateId)
                        {
                            logger.Warn("平行測試 候選版錯誤:DeductibleMemberPromotionDeposit, saveCol[" + i + "]-CreateId 不一致");
                        }
                        else if (saveCol[i].Type != saveCol2[i].Type)
                        {
                            logger.Warn("平行測試 候選版錯誤:DeductibleMemberPromotionDeposit, saveCol[" + i + "]-Type 不一致");
                        }
                        else if (saveCol[i].Action != saveCol2[i].Action)
                        {
                            logger.Warn("平行測試 候選版錯誤:DeductibleMemberPromotionDeposit, saveCol[" + i + "]-Action 不一致");
                        }
                        else if (saveCol[i].OrderClassification != saveCol2[i].OrderClassification)
                        {
                            logger.Warn("平行測試 候選版錯誤:DeductibleMemberPromotionDeposit, saveCol[" + i + "]-OrderClassification33 不一致");
                        }
                    }
                }
            }

            mp.MemberPromotionWithdrawalCollectionSet(saveCol);
            return true;
        }

        /// <summary>
        /// 依據輸入想要扣除的紅利點數，回傳新增member_promotion_withdrawal用的collection紀錄
        /// </summary>
        /// <param name="userId">使用紅利的會員</param>
        /// <param name="deductibleMemberValue">欲扣除的紅利點數</param>
        /// <param name="action">說明文字</param>
        /// <param name="withdrawalType">扣除的型態</param>
        /// <param name="orderGuid">依據哪個訂單進行扣款的，可為NULL</param>
        /// <param name="createId">建立資料的使用者</param>
        /// <param name="isPassStartTime">TRUE:則不考慮紅利點數是否已生效，有就扣  FALSE:只扣除已生效的紅利點數</param>
        /// <param name="deductionWithNotEnough">TRUE:則額度不足扣到0  FALSE:額度不足就不扣</param>
        /// <param name="classification">訂單來源的類型</param>
        /// <param name="receivableId">應收帳款單號</param>
        /// <param name="memberPromotionType"></param>
        /// <returns></returns>
        public static MemberPromotionWithdrawalCollection GetDeductibleMemberPromotionDeposit(int userId, double deductibleMemberValue, string action,
            BonusTransactionType withdrawalType, Guid? orderGuid, string createId, bool isPassStartTime, bool deductionWithNotEnough,
            OrderClassification classification, int? receivableId, MemberPromotionType memberPromotionType = MemberPromotionType.Bouns)
        {
            MemberPromotionWithdrawalCollection saveCol = new MemberPromotionWithdrawalCollection();
            if (deductibleMemberValue == 0)
            {
                return saveCol;
            }

            string orderStr = ViewMemberPromotionTransaction.Columns.ExpireTime;

            ViewMemberPromotionTransactionCollection memberPromotionColl = new ViewMemberPromotionTransactionCollection();
            int tmallUserId = MemberFacade.GetUniqueId(config.TmallDefaultUserName);



            if (userId == tmallUserId)
            {
                string[] filter = {
                                  ViewMemberPromotionTransaction.Columns.MemberUserId + "=" + userId,
                                  ViewMemberPromotionTransaction.Columns.PromotionType + "=" +(int) memberPromotionType,
                                  ViewMemberPromotionTransaction.Columns.DepositOrWithdrawal + "=1",
                                  ViewMemberPromotionTransaction.Columns.Balance + ">0",
                                  ViewMemberPromotionTransaction.Columns.ExpireTime + ">" + DateTime.Now.ToString("yyyy/MM/dd")
                              };

                memberPromotionColl = mp.ViewMemberPromotionTransactionGetListTop(orderStr, filter);
            }
            else
            {
                string[] filter = {
                                  ViewMemberPromotionTransaction.Columns.MemberUserId + "=" + userId,
                                  ViewMemberPromotionTransaction.Columns.PromotionType + "=" +(int) memberPromotionType,
                                  ViewMemberPromotionTransaction.Columns.DepositOrWithdrawal + "=1",
                                  ViewMemberPromotionTransaction.Columns.Balance + ">0"
                              };

                memberPromotionColl = mp.ViewMemberPromotionTransactionGetList(orderStr, filter);
            }


            double dedBonus = deductibleMemberValue;
            DateTime createTime = DateTime.Now;

            foreach (ViewMemberPromotionTransaction memberPromotion in memberPromotionColl)
            {
                //須考慮起始日因素，則當起始日未到時，不可扣除該紅利
                if (memberPromotion.ExpireTime < DateTime.Now)
                {
                    continue;
                }

                if ((!isPassStartTime) && (memberPromotion.StartTime > DateTime.Now))
                {
                    continue;
                }

                if (dedBonus == 0)
                {
                    break;
                }

                MemberPromotionWithdrawal withdrawal = new MemberPromotionWithdrawal();
                withdrawal.DepositId = memberPromotion.Id;
                withdrawal.Type = (int)withdrawalType;
                if (withdrawalType.Equals(BonusTransactionType.OrderAmtRedeeming))
                {
                    withdrawal.OrderGuid = orderGuid;
                }
                else if (withdrawalType.Equals(BonusTransactionType.UserRedeeming))
                {
                    withdrawal.RedeemOrderGuid = orderGuid;
                }
                withdrawal.Action = action;
                withdrawal.CreateTime = createTime;
                withdrawal.CreateId = createId;

                if (memberPromotion.Balance >= dedBonus)
                {
                    withdrawal.PromotionValue = dedBonus;
                    dedBonus = 0;
                }
                else
                {
                    withdrawal.PromotionValue = memberPromotion.Balance.Value;
                    dedBonus = dedBonus - memberPromotion.Balance.Value;
                }
                withdrawal.OrderClassification = (int)classification;
                withdrawal.ReceivableId = receivableId;
                withdrawal.Type = (int)memberPromotionType;
                saveCol.Add(withdrawal);
            }

            if ((dedBonus > 0) && (!deductionWithNotEnough)) //無法扣完
            {
                return null;
            }

            return saveCol;
        }

        /// <summary>
        /// 產生紅利BounsPoint扣款的項目
        /// 功能同 GetDeductibleMemberPromotionDeposit，候選版本
        /// </summary>
        /// <param name="userId">使用紅利的會員</param>
        /// <param name="deductibleMemberValue">欲扣除的紅利點數</param>
        /// <param name="action">說明文字</param>
        /// <param name="withdrawalType">扣除的型態</param>
        /// <param name="orderGuid">依據哪個訂單進行扣款的，可為NULL</param>
        /// <param name="createId">建立資料的使用者</param>
        /// <param name="isPassStartTime">TRUE:則不考慮紅利點數是否已生效，有就扣  FALSE:只扣除已生效的紅利點數</param>
        /// <param name="deductionWithNotEnough">TRUE:則額度不足扣到0  FALSE:額度不足就不扣</param>
        /// <param name="classification">訂單來源的類型</param>
        /// <param name="receivableId">應收帳款單號</param>
        /// <param name="memberPromotionType"></param>
        /// <returns></returns>
        public static MemberPromotionWithdrawalCollection GetDeductibleMemberPromotionDepositBetaVersion(int userId, double deductibleMemberValue, string action,
            BonusTransactionType withdrawalType, Guid? orderGuid, string createId, bool isPassStartTime, bool deductionWithNotEnough,
            OrderClassification classification, int? receivableId, MemberPromotionType memberPromotionType = MemberPromotionType.Bouns)
        {
            if (deductibleMemberValue == 0)
            {
                return new MemberPromotionWithdrawalCollection();
            }
            MemberPromotionWithdrawalCollection saveCol = new MemberPromotionWithdrawalCollection();
            MemberPromotionDepositCollection mpdCol = mp.MemberPromotionDepositGetAvailableList(userId);
            if (mpdCol.Count == 0)
            {
                return new MemberPromotionWithdrawalCollection();
            }
            MemberPromotionWithdrawalCollection mpwCol = mp.MemberPromotionWithdrawalGetAvailableList(userId);

            double dedBonus = deductibleMemberValue;
            DateTime createTime = DateTime.Now;
            foreach (MemberPromotionDeposit mpd in mpdCol)
            {
                if (dedBonus == 0)
                {
                    break;
                }
                double withdralAmount = mpwCol.Where(t => t.DepositId == mpd.Id).Select(t => t.PromotionValue).DefaultIfEmpty(0).Sum();
                double balance = mpd.PromotionValue - withdralAmount;
                if (balance == 0)
                {
                    continue;
                }
                MemberPromotionWithdrawal withdrawal = new MemberPromotionWithdrawal();
                withdrawal.DepositId = mpd.Id;
                withdrawal.Type = (int)withdrawalType;
                if (withdrawalType.Equals(BonusTransactionType.OrderAmtRedeeming))
                {
                    withdrawal.OrderGuid = orderGuid;
                }
                else if (withdrawalType.Equals(BonusTransactionType.UserRedeeming))
                {
                    withdrawal.RedeemOrderGuid = orderGuid;
                }
                withdrawal.Action = action;
                withdrawal.CreateTime = createTime;
                withdrawal.CreateId = createId;

                if (balance >= dedBonus)
                {
                    withdrawal.PromotionValue = dedBonus;
                    dedBonus = 0;
                }
                else
                {
                    withdrawal.PromotionValue = balance;
                    dedBonus = dedBonus - balance;
                }
                withdrawal.OrderClassification = (int)classification;
                withdrawal.ReceivableId = receivableId;
                withdrawal.Type = (int)memberPromotionType;
                saveCol.Add(withdrawal);
            }
            if ((dedBonus > 0) && (!deductionWithNotEnough)) //無法扣完
            {
                return new MemberPromotionWithdrawalCollection();
            }
            return saveCol;
        }

        public static double GetMemberPromotionValue(string userName)
        {
            ViewMemberPromotionTransactionCollection transactions = mp.ViewMemberPromotionTransactionGetList(userName, ViewMemberPromotionTransaction.Columns.CreateTime);

            if (transactions.Count > 0)
            {
                for (int i = transactions.Count - 1; i >= 0; i--)
                {
                    if (transactions[i].RunSum != null)
                    {
                        return transactions[i].RunSum.Value;
                    }
                }
            }
            return 0;
        }

        public static double GetAvailableMemberPromotionValue(int userId)
        {
            string userName = MemberFacade.GetUserName(userId);
            return GetAvailableMemberPromotionValue(userName);
        }

        /// <summary>
        /// 取得會員可使用的紅利金點數 (10點為1元)
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static double GetAvailableMemberPromotionValue(string userName)
        {
            decimal n = mp.ViewMemberPromotionTransactionGetSum(userName, MemberPromotionType.Bouns);
            return (double)n;
        }

        /// <summary>
        /// 取消訂單時，扣除因此訂單產生的紅利，紅利不足則扣到0為止
        /// </summary>
        /// <param name="orderGuid">退貨訂單Guid</param>
        /// <param name="createId">退貨會員編號</param>
        public static void MemberPromotionDeductionWhenOrderCancel(Guid orderGuid, string createId)
        {
            //抓出邀請親友完成首次好康購買的資料
            var mpdc = mp.MemberPromotionDepositGetListByOrderGuid(orderGuid)
                .Where(x => x.Action == I18N.Phrase.ReferenceBonusActionForReferencedUser).ToList();
            if (mpdc.Count > 0)
            {
                var deposit = mpdc[0];
                DeductibleMemberPromotionDeposit(deposit.UserId, deposit.PromotionValue, "邀請親友完成首次好康購買-退貨扣回",
                                                 BonusTransactionType.SystemDeduction, null, createId, true,
                                                 true, OrderClassification.Other, null);
            }
        }

        /// <summary>
        /// 當PponDeal結束時，處理相關MemberPromotion的東西
        /// 當deal沒有成立，需將發出去的紅利扣除，作法為產生一筆負項的記錄
        /// 當deal沒有成立，需將使用者購買該筆deal的紅利負項紀錄，標計為無效
        /// </summary>
        /// <param name="pponDeal">要處理的pponDeal物件</param>
        public static void MemberPromotionProcessAfterPponDealClosed(ViewPponDeal pponDeal)
        {
            if (pponDeal.OrderGuid == null)
            {
                return;
            }
            //判斷deal若沒有成立
            PponDealStage stage = pponDeal.GetDealStage();
            if (stage == PponDealStage.ClosedAndFail)
            {
                DateTime nowDateTime = DateTime.Now;
                //當deal沒有成立，需將使用者購買該筆deal的紅利負項紀錄，標計為無效
                MemberPromotionWithdrawalCollection withdrawalColl =
                    mp.MemberPromotionWithdrawalGetListByPponDealOrderGuid(pponDeal.OrderGuid.Value);
                foreach (MemberPromotionWithdrawal withdrawal in withdrawalColl)
                {
                    withdrawal.Type = (int)BonusTransactionType.InvalidOrderAmtRedeeming;
                }

                //當deal沒有成立，需將使用者獲得的紅利扣除
                MemberPromotionDepositCollection depositCollection =
                    mp.MemberPromotionDepositGetListByPponDealOrderGuid(pponDeal.OrderGuid.Value);
                foreach (MemberPromotionDeposit deposit in depositCollection)
                {
                    MemberPromotionWithdrawal addWithdrawal = new MemberPromotionWithdrawal();
                    addWithdrawal.DepositId = deposit.Id;
                    addWithdrawal.PromotionValue = deposit.PromotionValue;
                    addWithdrawal.Type = (int)BonusTransactionType.PayOffAdjustment;
                    addWithdrawal.Action = I18N.Phrase.PayOffBonusActionForDealFail;
                    addWithdrawal.CreateTime = nowDateTime;
                    addWithdrawal.CreateId = config.ServiceEmail;

                    withdrawalColl.Add(addWithdrawal);
                }

                mp.MemberPromotionWithdrawalCollectionSet(withdrawalColl);
            }
            else //deal若成立
            {
                #region 移到PaymentGatePresenter中

                #endregion 移到PaymentGatePresenter中
            }
        }

        public static int ConvertBonusToMoney(double bonus)
        {
            return (int)Math.Round(bonus / 10);
        }

        /// <summary>
        /// 紅利於deal結束後一天生效
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static DateTime GetOrderBonusStartDate(ViewPponDeal deal)
        {
            return deal.BusinessHourOrderTimeE.AddDays(1);//紅利於deal結束後一天生效
        }

        /// <summary>
        /// 紅利有效期限
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static DateTime GetOrderBonusExpireDate(ViewPponDeal deal)
        {
            return deal.BusinessHourOrderTimeE.AddDays(31);//紅利有效期限為30日
        }

        #endregion 紅利

        #region 購物金

        public static List<UserScashTransactionInfo> GetUserScashTransactions(ViewScashTransactionCollection scashTransactions)
        {
            return GetUserScashTransactions(scashTransactions, new List<PezScashTransaction>());
        }

        public static List<UserScashTransactionInfo> GetUserScashTransactions(
            ViewScashTransactionCollection scashLogs, List<PezScashTransaction> pscashLogs)
        {
            List<UserScashTransactionInfo> infos = new List<UserScashTransactionInfo>();
            foreach (var log in scashLogs)
            {
                var info = new UserScashTransactionInfo
                {
                    Amount = log.Amount,
                    CreateTime = log.CreateTime,
                    Message = log.Message,
                    SellerName = log.SellerName,
                    OrderId = log.OrderId,
                    OrderClassification = log.OrderClassification,
                    OrderGuid = log.OrderGuid,
                    OrderStatus = log.OrderStatus,
                    SubTotal = 0,
                    WithdrawalId = log.WithdrawalId
                };
                if (log.SellerName != null && log.Message.IndexOf(log.SellerName) > -1)
                {
                    info.Message = info.Message.Replace(
                        log.SellerName, log.CouponUsage == null ? string.Empty : log.CouponUsage);
                }
                infos.Add(info);
            }
            foreach (var log in pscashLogs)
            {
                var info = new UserScashTransactionInfo
                {
                    Amount = log.Amount,
                    CreateTime = log.TheTime,
                    Message = log.Title,
                    OrderClassification = null,
                    OrderGuid = null,
                    OrderStatus = 0,
                    OrderId = string.Empty,
                    SellerName = string.Empty,
                    SubTotal = 0,
                    WithdrawalId = log.WithdrawalId.GetValueOrDefault(-1)
                };
                infos.Add(info);
            }
            infos = infos.OrderBy(t => t.CreateTime).ToList();

            decimal? userTotal = 0;
            foreach (var info in infos)
            {
                userTotal = userTotal + info.Amount;
                info.SubTotal = userTotal;
            }
            infos = infos.OrderByDescending(t => t.CreateTime).ToList();
            return infos;
        }

        #endregion 購物金

        /// <summary>
        /// PCash
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static decimal GetPayeasyCashPoint(int userId)
        {
            //因 自2015/5/1起，pcash無法使用，所以所有查詢都回傳0，避免再跟pez查詢
            return 0;
        }

        #endregion

        #region 會員訂閱 EDM

        /// <summary>
        /// 訂閱edm(區域或城市擇一填寫，另一者填寫0)
        /// </summary>
        /// <param name="userName">使用者email</param>
        /// <param name="category">分類</param>
        /// <param name="isSubscribe">是否訂閱(可用於取消訂閱)</param>
        /// <returns></returns>
        public static Subscription SubscribeCheckSet(string userName, CategoryNode category, bool isSubscribe)
        {
            if (category == null)
            {
                logger.WarnFormat("SubscribeCheckSet 的參數category沒有預期的資料, userName={0}, isSubscribe={1}", userName, isSubscribe);
                return null;
            }
            Subscription subscribe = SubscribeCheckSet(userName, category.CategoryId, isSubscribe);
            return subscribe;
        }

        /// <summary>
        /// 新會員預設訂閱信
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="area"></param>
        public static void SubscribeForNewMember(string userName, int area)
        {
            SubscribeCheckSet(userName, CategoryManager.Default.FindByCity(area), true);
            SubscribeCheckSet(userName, CategoryManager.Default.Delivery, true);
            SubscribeCheckSet(userName, CategoryManager.Default.Travel, true);

            // area為 基隆市(JL)、台北市(TP)、新北市(TA)、桃園縣(TY) 者，再增加訂閱品生活
            string[] cityCodes = { "JL", "TP", "TA", "TY" };
            if (CityManager.Citys.Where(x => x.Code.EqualsAny(cityCodes)).Any(x => x.Id == area))
            {
                SubscribeCheckSet(userName, CategoryManager.Default.Piinlife, true);
            }
        }

        /// <summary>
        /// 訂閱edm
        /// </summary>
        /// <param name="userName">使用者user_name</param>
        /// <param name="categoryId">分類id</param>
        /// <param name="isSubscribe">是否訂閱(可用於取消訂閱)</param>
        /// <returns></returns>
        private static Subscription SubscribeCheckSet(string userName, int categoryId, bool isSubscribe)
        {
            Subscription subscription = pp.SubscriptionGet(userName, categoryId);
            if (isSubscribe)
            {
                if (!subscription.IsLoaded)
                {
                    // 訂閱
                    subscription.Email = userName;
                    subscription.Status = (int)SubscribeType.Default;
                    subscription.CategoryId = categoryId;
                    subscription.CreateTime = DateTime.Now;
                    pp.SubscriptionSet(subscription);
                }
                else if (Helper.IsFlagSet(subscription.Status.Value, SubscribeType.Unsubscribe))
                {
                    // 重新訂閱
                    subscription.Status = (int)Helper.SetFlag(false, subscription.Status.Value, SubscribeType.Unsubscribe);
                    subscription.ResubscriptionTime = DateTime.Now;
                    pp.SubscriptionSet(subscription);
                }
            }
            else if (subscription.IsLoaded && !Helper.IsFlagSet(subscription.Status.Value, SubscribeType.Unsubscribe))
            {
                // 取消訂閱
                subscription.Status = (int)Helper.SetFlag(true, subscription.Status.Value, SubscribeType.Unsubscribe);
                subscription.UnsubscriptionTime = DateTime.Now;
                pp.SubscriptionSet(subscription);
            }
            return subscription;
        }

        /// <summary>
        /// 檢查訂閱狀態
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="categoryId"></param>
        /// <returns>True:已訂閱 False:未訂閱</returns>
        public static bool SubscribeCheck(string userName, int categoryId)
        {
            Subscription subscription = pp.SubscriptionGet(userName, categoryId);
            if (!subscription.IsLoaded)
            {
                // 沒有訂閱記錄
                return false;
            }
            if (Helper.IsFlagSet(subscription.Status.Value, SubscribeType.Unsubscribe))
            {
                // 已取消訂閱
                return false;
            }
            return true;
        }

        #region 各deal的照片

        private static string[] GetMediaPathsFromRawData(string rawData, MediaType type)
        {
            string[] paths = Helper.GetRawPathsFromRawData(rawData);
            for (int i = 0; i < paths.Length; i++)
            {
                paths[i] = GetMediaPath(paths[i], type);
            }

            return paths;
        }

        private static string GetMediaPath(string rawPath, MediaType type)
        {
            if (!string.IsNullOrEmpty(rawPath))
            {
                // if path is an url, use it instead of parse it
                if (rawPath.IndexOf("http://", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    return rawPath;
                }

                string infix = string.Empty;
                switch (type)
                {
                    case MediaType.SellerPhotoOriginal:
                    case MediaType.ItemPhotoOriginal:
                        infix = folder[0].ToString() + "/";
                        break;

                    case MediaType.SellerPhotoSmall:
                    case MediaType.ItemPhotoSmall:
                        infix = folder[1].ToString() + "/";
                        break;

                    case MediaType.SellerPhotoLarge:
                    case MediaType.ItemPhotoLarge:
                        infix = folder[2].ToString() + "/";
                        break;

                    default:
                        break;
                }

                switch (type)
                {
                    case MediaType.ItemPhotoLarge:
                    case MediaType.ItemPhotoOriginal:
                    case MediaType.ItemPhotoSmall:
                        infix += "item/";
                        break;

                    default:
                        break;
                }

                string[] fields = rawPath.Split(',');
                return config.MediaBaseUrl + (fields.Length > 0 ? fields[0] + "/" + infix + (fields.Length > 1 ? fields[1] : string.Empty) : string.Empty);
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion 各deal的照片

        public static string GetUnsubscriptionUrl(string Mail, string City)
        {
            string rtnUrl = config.SiteUrl.ToString() + "/ppon/unsubscription.aspx";

            string code = City + Mail + "17Life_Ppon";
            string querystring = @"?email={mail}&mc={city}&code={code}";
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] b = md5.ComputeHash(Encoding.UTF8.GetBytes(code));
            querystring = querystring.Replace("{mail}", Mail)
                .Replace("{city}", City)
                .Replace("{code}", BitConverter.ToString(b).Replace("-", string.Empty));
            return rtnUrl + querystring;
        }

        protected static string GetCity(string CityCode)
        {
            string City = string.Empty;
            switch (CityCode)
            {//TP,TA,TY,XZ,TC,TN,GX
                case "TP":
                case "TA":
                    City = I18N.Phrase.PponCityTaipeiCity;
                    break;

                case "TY":
                    City = I18N.Phrase.PponCityTaoyuan;
                    break;

                case "XZ":
                    City = I18N.Phrase.PponCityHsinchu;
                    break;

                case "TC":
                    City = I18N.Phrase.PponCityTaichung;
                    break;

                case "TN":
                    City = I18N.Phrase.PponCityTainan;
                    break;

                case "GX":
                    City = I18N.Phrase.PponCityKaohsiung;
                    break;

                case "ALL":
                    City = I18N.Phrase.PponCityAllCountry;
                    break;

                case "TRA":
                    City = I18N.Phrase.PponCityTravel;
                    break;

                case "PBU":
                    City = I18N.Phrase.PponCityPeauty;
                    break;

                default:
                    City = string.Empty;
                    break;
            }
            return City;
        }

        public static PponPickupStores GetOrUpdatePponPickupStore(int userId, PponPickupStore pickupStoreFromUI)
        {
            var info = mp.MemberCheckoutInfoGet(userId);
            bool isNew = false;
            if (info.IsLoaded == false)
            {
                info = new MemberCheckoutInfo
                {
                    UserId = userId,
                    CreateTime = DateTime.Now
                };
            }
            else
            {
                info.ModifyTime = DateTime.Now;
            }
            bool isDirty = false;
            if (pickupStoreFromUI != null)
            {
                if (pickupStoreFromUI.ProductDeliveryType == ProductDeliveryType.FamilyPickup)
                {
                    if (info.FamilyStoreId != pickupStoreFromUI.StoreId)
                    {
                        isDirty = true;
                        info.FamilyStoreId = pickupStoreFromUI.StoreId;
                    }
                    if (info.FamilyStoreName != pickupStoreFromUI.StoreName)
                    {
                        isDirty = true;
                        info.FamilyStoreName = pickupStoreFromUI.StoreName;
                    }
                    if (info.FamilyStoreTel != pickupStoreFromUI.StoreTel)
                    {
                        isDirty = true;
                        info.FamilyStoreTel = pickupStoreFromUI.StoreTel;
                    }
                    if (info.FamilyStoreAddr != pickupStoreFromUI.StoreAddr)
                    {
                        isDirty = true;
                        info.FamilyStoreAddr = pickupStoreFromUI.StoreAddr;
                    }
                }
                else if (pickupStoreFromUI.ProductDeliveryType == ProductDeliveryType.SevenPickup)
                {
                    if (info.SevenStoreId != pickupStoreFromUI.StoreId)
                    {
                        isDirty = true;
                        info.SevenStoreId = pickupStoreFromUI.StoreId;
                    }
                    if (info.SevenStoreName != pickupStoreFromUI.StoreName)
                    {
                        isDirty = true;
                        info.SevenStoreName = pickupStoreFromUI.StoreName;
                    }
                    if (info.SevenStoreTel != pickupStoreFromUI.StoreTel)
                    {
                        isDirty = true;
                        info.SevenStoreTel = pickupStoreFromUI.StoreTel;
                    }
                    if (info.SevenStoreAddr != pickupStoreFromUI.StoreAddr)
                    {
                        isDirty = true;
                        info.SevenStoreAddr = pickupStoreFromUI.StoreAddr;
                    }
                }
                //在訪客購買時 uerId = 0, 此時不儲存
                if (isDirty && userId > 0)
                {
                    mp.MemberCheckoutInfoSet(info);
                }
            }

            return new PponPickupStores
            {
                FamilyStore = new PickupStore
                {
                    StoreId = info.FamilyStoreId ?? string.Empty,
                    StoreName = info.FamilyStoreName ?? string.Empty,
                    StoreTel = info.FamilyStoreTel ?? string.Empty,
                    StoreAddr = info.FamilyStoreAddr ?? string.Empty
                },
                SevenStore = new PickupStore
                {
                    StoreId = info.SevenStoreId ?? string.Empty,
                    StoreName = info.SevenStoreName ?? string.Empty,
                    StoreTel = info.SevenStoreTel ?? string.Empty,
                    StoreAddr = info.SevenStoreAddr ?? string.Empty
                }
            };
        }

        #endregion

        #region 會員帳號資料相關

        /// <summary>
        /// 取 UserName 去除 @domain 的部份
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static string GetShortUserName(string email)
        {
            return (email != null) ? email.IndexOf('@') > 0 ? email.Remove(email.IndexOf('@')) : email : string.Empty;
        }
        /// <summary>
        /// 新會員信箱認證信
        /// </summary>
        /// <param name="memMail"></param>
        /// <param name="memUid"></param>
        /// <param name="authKey"></param>
        /// <param name="authCode"></param>
        public static void SendAccountConfirmMail(string memMail, string memUid, string authKey, string authCode)
        {
            Member mem = mp.MemberGet(memMail);
            if (mem.IsLoaded == false)
            {
                throw new Exception("找不到會員資料 " + memMail);
            }
            MailMessage msg = new MailMessage();

            if (mem.IsGuest == false)
            {
                //正式會員
                AccountConfirmMail template = TemplateFactory.Instance().GetTemplate<AccountConfirmMail>();
                template.MemberUid = memUid;
                template.MemberMail = memMail;
                template.AuthKey = authKey;
                template.AuthCode = authCode;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;
                template.HttpsSiteUrl = config.SiteUrl.Replace("http", "https");
                msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                msg.To.Add(memMail);
                msg.Subject = "17Life電子信箱認證信";
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
            }
            else
            {
                //訪客會員
                AccountConfirmMailForGuest template = TemplateFactory.Instance().GetTemplate<AccountConfirmMailForGuest>();
                template.MemberUid = memUid;
                template.MemberMail = memMail;
                template.AuthKey = authKey;
                template.AuthCode = authCode;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;
                template.HttpsSiteUrl = config.SiteUrl.Replace("http", "https");
                msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                msg.To.Add(memMail);
                msg.Subject = "17Life電子信箱認證信";
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
            }
            PostMan.Instance().Send(msg, SendPriorityType.Async);
        }

        /// <summary>
        /// 會員認證成功通知信
        /// </summary>
        /// <param name="sendTo"></param>
        /// <param name="userId"></param>
        public static void SendMemberRegisterSuccessMail(string sendTo, int userId)
        {
            MailMessage msg = new MailMessage();
            //正式會員
            MemberRegisterSuccessMail template = TemplateFactory.Instance().GetTemplate<MemberRegisterSuccessMail>();
            template.UserId = userId;
            template.PromoDeals = OrderFacade.GetPromoDealsForNoticeMail(userId);
            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(sendTo);
            msg.Subject = "您已成功註冊17Life會員";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }

        public static void ResetMemberAuthInfo(MemberAuthInfo mai)
        {
            string[] authPair = GenEmailAuthCode();
            if (mai.ForgetPasswordDate.GetValueOrDefault() == DateTime.Now.Date)
            {
                mai.PasswordQueryTime = mai.PasswordQueryTime.HasValue ? mai.PasswordQueryTime.Value + 1 : 1;
            }
            else
            {
                mai.PasswordQueryTime = 1;
            }
            mai.ForgetPasswordDate = DateTime.Now;
            mai.ForgetPasswordKey = authPair[0];
            mai.ForgetPasswordCode = authPair[1];
            mp.MemberAuthInfoSet(mai);
        }

        public static void ChangeMemberAuthInfoMail(int userId, string mail)
        {
            var mai = mp.MemberAuthInfoGet(userId);
            mai.Email = mail;
            mp.MemberAuthInfoSet(mai);
        }

        /// <summary>
        /// 取得會員Email啟動的認證碼
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static string GetMemberAuthKey(string userName)
        {
            var mai = mp.MemberAuthInfoGetByEmail(userName);
            if (mai.IsLoaded == false)
            {
                return string.Empty;
            }
            return mai.AuthKey;
        }
        /// <summary>
        /// 產生認證信的驗證碼
        /// </summary>
        /// <param name="codeLength"></param>
        /// <param name="keyLength"></param>
        /// <returns></returns>
        public static string[] GenEmailAuthCode(int codeLength = 50, int keyLength = 50)
        {
            int times = (int)Math.Ceiling((double)(codeLength + keyLength) / 32);
            string tempBase = string.Empty;

            for (int x = 0; x < times; x++)
            {
                tempBase += Guid.NewGuid().ToString("N");
            }

            return new string[] { tempBase.Substring(0, codeLength), tempBase.Substring(codeLength, keyLength) };
        }
        /// <summary>
        /// 新會員重設密碼信
        /// </summary>
        /// <param name="memMail"></param>
        /// <param name="memUid"></param>
        /// <param name="authKey"></param>
        /// <param name="authCode"></param>
        public static void SendForgetPasswordMail(string memMail, string memUid, string authKey, string authCode, string display = "email")
        {
            MailMessage msg = new MailMessage();
            ForgetPasswordMail template = TemplateFactory.Instance().GetTemplate<ForgetPasswordMail>();
            template.MemberUid = memUid;
            template.AuthKey = authKey;
            template.AuthCode = authCode;
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;
            template.Display = display;
            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(memMail);
            msg.Subject = "重設密碼通知";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate, MailTemplateType.MemberForgetPassword);
        }

        /// <summary>
        /// 修改會員資料-關於購買人資料的部分
        /// </summary>
        /// <param name="userName">會員帳號</param>
        /// <param name="lastName">姓</param>
        /// <param name="firstName">名</param>
        /// <param name="phoneNumber">手機號碼</param>
        /// <param name="userEmail">通訊Email</param>
        /// <param name="townshipId">鄉鎮市區Id</param>
        /// <param name="addressDesc">地址(不包含路段之前的部分)</param>
        /// <returns></returns>
        public static bool MemberPurchaserSet(string userName, string lastName, string firstName, string phoneNumber, string userEmail,
            int townshipId, string addressDesc, CarrierType carrierType = CarrierType.Member, string carrierId = "", string InvoiceBuyerName = "", string InvoiceBuyerAddress = "")
        {
            var member = mp.MemberGet(userName);
            if (member.IsLoaded)
            {
                if (!string.IsNullOrWhiteSpace(lastName))
                {
                    member.LastName = lastName;
                }

                if (!string.IsNullOrWhiteSpace(firstName))
                {
                    member.FirstName = firstName;
                }

                if (!string.IsNullOrWhiteSpace(phoneNumber))
                {
                    member.Mobile = phoneNumber;
                }

                if (!string.IsNullOrWhiteSpace(carrierId) && (carrierType == CarrierType.Phone || carrierType == CarrierType.PersonalCertificate))
                {
                    member.CarrierType = (int)carrierType;
                    member.CarrierId = carrierId;
                }
                else if (carrierType == CarrierType.None)
                {
                    member.CarrierType = (int)CarrierType.None;
                    member.InvoiceBuyerName = InvoiceBuyerName;
                    member.InvoiceBuyerAddress = InvoiceBuyerAddress;
                }
                else
                {
                    member.CarrierType = (int)CarrierType.Member;
                    member.CarrierId = member.UniqueId.ToString();
                }


                Guid buildingGuid = lp.BuildingGet(Building.Columns.CityId, townshipId).Guid;
                if (buildingGuid != Guid.Empty && string.IsNullOrWhiteSpace(addressDesc) == false)
                {
                    member.BuildingGuid = buildingGuid;
                    member.CompanyAddress = CityManager.CityTownShopStringGet(townshipId) + addressDesc;
                }

                if (string.IsNullOrEmpty(userEmail) == false && member.UserEmail != userEmail)
                {
                    member.UserEmail = userEmail;
                }
                mp.MemberSet(member);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 新增會員維護的收件人地址
        /// </summary>
        /// <param name="userId">會員Id</param>
        /// <param name="contactName">收件人姓名</param>
        /// <param name="phoneNumber">連絡電話</param>
        /// <param name="townshipId">鄉鎮市區id</param>
        /// <param name="addressDesc">地址(不包含路段之前的部分)</param>
        /// <returns></returns>
        public static MemberDelivery MemberAddresseeSet(int userId, string contactName, string phoneNumber,
            int townshipId, string addressDesc)
        {
            Guid buildingGuid = lp.BuildingGet(Building.Columns.CityId, townshipId).Guid;
            if (buildingGuid == Guid.Empty || string.IsNullOrWhiteSpace(addressDesc))
            {
                return null;
            }
            MemberDelivery delivery = new MemberDelivery();
            delivery.UserId = userId;
            delivery.AddressAlias = contactName;
            delivery.BuildingGuid = buildingGuid;
            delivery.Mobile = phoneNumber;
            delivery.Address = CityManager.CityTownShopStringGet(townshipId) + addressDesc;
            delivery.CreateTime = DateTime.Now;
            delivery.ContactName = contactName;
            return mp.SetMemberDelivery(delivery);
        }

        /// <summary>
        /// 供API新增收件人至資料庫
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="apiDelivery"></param>
        /// <returns></returns>
        public static MemberDelivery MemberAddresseeSet(string userName, ApiMemberDelivery apiDelivery)
        {
            var delivery = new MemberDelivery();
            delivery.UserId = GetUniqueId(userName);
            delivery.AddressAlias = apiDelivery.ContactName;
            if (apiDelivery.Address != null)
            {
                delivery.BuildingGuid = apiDelivery.Address.BuildingGuid;
                delivery.Address = apiDelivery.Address.CompleteAddress;
            }
            else
            {
                delivery.BuildingGuid = config.DefaultBuildingGuid;
            }
            delivery.Mobile = apiDelivery.Mobile;
            delivery.CreateTime = DateTime.Now;
            delivery.ContactName = apiDelivery.ContactName;
            return mp.SetMemberDelivery(delivery);
        }

        /// <summary>
        /// 可從手機號碼或Email取得會員資料
        /// 支援一級快取
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static Member GetMember(string userName)
        {
            string key = string.Format(memberCacheKey, userName);
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Items[key] == null)
                {
                    if (RegExRules.CheckMobile(userName))
                    {
                        HttpContext.Current.Items[key] = GetMemberByMobile(userName);
                    }
                    else
                    {
                        HttpContext.Current.Items[key] = mp.MemberGet(userName);
                    }
                }
                return HttpContext.Current.Items[key] as Member;
            }
            if (RegExRules.CheckMobile(userName))
            {
                return GetMemberByMobile(userName);
            }
            return mp.MemberGet(userName);
        }

        public static Member GetMemberByMobile(string mobile)
        {
            MobileMember mm = GetMobileMember(mobile);
            if (mm.IsLoaded == false)
            {
                return new Member();
            }
            return GetMember(mm.UserId);
        }

        public static Member GetMember(int userId)
        {
            string key = string.Format(memberCacheKey, userId);
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Items[key] == null)
                {
                    HttpContext.Current.Items[key] = mp.MemberGet(userId);
                }
                return HttpContext.Current.Items[key] as Member;
            }
            return mp.MemberGet(userId);
        }

        public static void SaveMember(Member mem)
        {
            mem.ModifyTime = DateTime.Now;
            mp.MemberSet(mem);
        }

        public static Member GetMemberByAccessToken(string token)
        {
            string key = string.Format(memberCacheKey, token);
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Items[key] == null)
                {
                    HttpContext.Current.Items[key] = mp.MemberGetByAccessToken(token);
                }
                return HttpContext.Current.Items[key] as Member;
            }
            return mp.MemberGetByAccessToken(token);
        }

        public static MobileMember GetMobileMember(string mobileOrUserName)
        {
            if (RegExRules.CheckMobile(mobileOrUserName))
            {
                return mp.MobileMemberGet(mobileOrUserName);
            }
            return mp.MobileMemberGetByUserName(mobileOrUserName);
        }

        public static MobileMember GetMobileMember(int userId)
        {
            return mp.MobileMemberGet(userId);
        }

        public static string GetUserName(int userId)
        {
            return mp.MemberUserNameGet(userId);
        }

        public static string GetUserEmail(int userId)
        {
            return mp.MemberGet(userId).UserEmail;
        }

        public static string GetUserEmail(string userName)
        {
            return GetMember(userName).UserEmail;
        }

        public static string GetMemberBirthday(string userName)
        {
            return GetMember(userName).Birthday.ToString();
        }

        public static bool SaveMemberBirthday(string userName, string birth)
        {
            Member mem = mp.MemberGetByUserName(userName);
            try
            {
                mem.Birthday = DateTime.Parse(birth);
                SaveMember(mem);
                return true;
            }
            catch (Exception ex)
            {
                logger.Warn("UpdateMemberBirthday Fail, ex=" + ex);
                return false;
            }
        }

        public static int GetUniqueId(string userName, bool throwExceptionIfUserNotFound)
        {
            int userId = GetUniqueId(userName);
            if (throwExceptionIfUserNotFound && userId == 0)
            {
                throw new Exception(string.Format("user {0} not found.", userName));
            }

            //撈不到會回傳0
            if (HttpContext.Current == null)
            {
                return mp.MemberUniqueIdGet(userName);
            }

            if (HttpContext.Current.Items[UniquIdItemKey + userName] == null)
            {
                HttpContext.Current.Items[UniquIdItemKey + userName] = mp.MemberUniqueIdGet(userName);
            }
            return (int)HttpContext.Current.Items[UniquIdItemKey + userName];
        }

        public static int GetUniqueId(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                return 0;
            }
            //撈不到會回傳0
            if (HttpContext.Current == null)
            {
                return mp.MemberUniqueIdGet(userName);
            }

            if (HttpContext.Current.Items[UniquIdItemKey + userName] == null)
            {
                HttpContext.Current.Items[UniquIdItemKey + userName] = mp.MemberUniqueIdGet(userName);
            }
            return (int)HttpContext.Current.Items[UniquIdItemKey + userName];
        }

        public static EinvoiceTriple GetEinvoiceTriple(int userId)
        {
            return mp.EinvoiceTripleGetByUserid(userId);
        }

        public static bool SaveEinvoiceTriple(EinvoiceTriple EinvoiceTriple)
        {
            return mp.SaveEinvoiceTriple(EinvoiceTriple);
        }

        public static bool EinvoiceTripleDelete(int Id)
        {
            return mp.EinvoiceTripleDelete(Id);
        }

        public static EinvoiceWinner EinvoiceWinnerGetByUserid(int userId)
        {
            return mp.EinvoiceWinnerGetByUserid(userId);
        }

        public static bool SaveEinvoiceWinner(EinvoiceWinner winnerData)
        {
            return mp.SaveEinvoiceWinner(winnerData);
        }

        #region 會員 Profile

        /// <summary>
        /// 全名
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static string GetFullName(string userName)
        {
            Member member = mp.MemberGet(userName);
            return member.DisplayName;
        }

        public static string GetFullName(int uniqueId)
        {
            Member member = mp.MemberGet(uniqueId);
            return member.DisplayName;
        }

        /// <summary>
        /// 更新會員的照片
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postedFile"></param>
        /// <param name="memberPicUrl"></param>
        public static UpdateMemberPicStatus UpdateMemberPic(int userId, IHttpPostedFileAdapter postedFile, out string memberPicUrl)
        {
            memberPicUrl = string.Empty;
            if (postedFile.ContentLength > _MEMBER_PIC_SIZE_LIMIT)
            {
                return UpdateMemberPicStatus.ExceedSizeLimit;
            }

            Stream picBuffer = postedFile.InputStream;
            string mimeType = postedFile.ContentType;

            Member mem = mp.MemberGetbyUniqueId(userId);
            if (mem.IsLoaded == false)
            {
                return UpdateMemberPicStatus.MemberNotFound;
            }
            try
            {
                string memberPicPath = Path.Combine(ImageFacade.GetMemberPicRootPath(), ImageFacade.GetMemberPicFolder(mem.UniqueId));
                if (Directory.Exists(memberPicPath) == false)
                {
                    try
                    {
                        Directory.CreateDirectory(memberPicPath);
                    }
                    catch
                    {
                        throw new Exception("UpdateMemberPic 建立路徑失敗 " + memberPicPath);
                    }
                }

                string picName = Guid.NewGuid() + ".jpg";
                if (string.IsNullOrEmpty(mimeType))
                {
                    throw new Exception("UpdateMemberPic mimeType 不允許空值");
                }
                if (mimeType.Equals("image/png", StringComparison.OrdinalIgnoreCase) == false
                    && mimeType.Equals("image/jpeg", StringComparison.OrdinalIgnoreCase) == false)
                {
                    throw new Exception(string.Format("mimetype {0} is not allow to save.", mimeType));
                }

                string saveImageFileName = Path.Combine(memberPicPath, picName);

                var codecInfo = ImageFacade.GetImageCodecInfo("image/jpeg");
                var encoderParam = ImageFacade.GetEncoderParameters();

                picBuffer.Seek(0, SeekOrigin.Begin);
                //ImageFacade.CutForCustom 會把 origImage Dispose掉，在不改原method情況下，每次使用需要init
                Image origImage = Image.FromStream(picBuffer);
                Image saveImage = ImageFacade.CropSquare(origImage, 400, 9);
                saveImage.Save(saveImageFileName, codecInfo, encoderParam);

                DeleteMemberPicFile(mem);

                mem.Pic = string.Format("{0}/{1}",
                    ImageFacade.GetMemberPicFolder(mem.UniqueId), picName);
                memberPicUrl = GetMemberPicUrl(mem);
                SaveMember(mem);

                return UpdateMemberPicStatus.OK;
            }
            catch (Exception ex)
            {
                logger.Warn("UpdateMemberPic Fail, ex=" + ex);
                return UpdateMemberPicStatus.SaveFail;
            }
        }

        public static string RefreshFacebookMemberPic(int userId)
        {
            //取得fb的id
            string fbId = string.Empty;
            Member m = mp.MemberGet(userId);
            if (!m.IsLoaded)
            {
                return string.Empty;
            }

            MemberLink fbLink = mp.MemberLinkGet(userId, SingleSignOnSource.Facebook);
            if (!fbLink.IsLoaded)
            {
                return string.Empty;
            }

            fbId = fbLink.ExternalUserId;
            WebRequest request = WebRequest.Create(string.Format("http://graph.facebook.com/{0}{1}/picture?type=large",
                FacebookUtility._FACEBOOK_API_VER_2_10, fbId));
            m.Pic = request.GetResponse().ResponseUri.ToString();
            mp.MemberPicSet(m.UniqueId, m.Pic);
            return m.Pic;
        }

        /// <summary>
        /// 取得會員的大頭照網址
        /// </summary>
        /// <param name="mem"></param>
        /// <returns></returns>
        public static string GetMemberPicUrl(Member mem)
        {
            if (mem == null || string.IsNullOrEmpty(mem.Pic))
            {
                return string.Empty;
            }
            if (mem.Pic.StartsWith("/"))
            {
                return Helper.CombineUrl(config.SiteUrl, mem.Pic.Substring(1));
            }
            if (mem.Pic.StartsWith("http", StringComparison.OrdinalIgnoreCase))
            {
                return mem.Pic;
            }
            return Helper.CombineUrl(ImageFacade.GetMemberPicBaseUrl(), mem.Pic);
        }

        public static string GetMemPicFilePath(Member mem)
        {
            if (mem == null || string.IsNullOrEmpty(mem.Pic))
            {
                return string.Empty;
            }
            if (mem.Pic.StartsWith("/"))
            {
                return Helper.CombineUrl(config.SiteUrl, mem.Pic.Substring(1));
            }
            if (mem.Pic.StartsWith("http", StringComparison.OrdinalIgnoreCase))
            {
                return mem.Pic;
            }
            return Path.Combine(ImageFacade.GetMemberPicRootPath(), ImageFacade.GetMemberPicFolder(mem.UniqueId));
        }

        public static void DeleteMemberPicFile(Member mem)
        {
            if (mem == null || string.IsNullOrEmpty(mem.Pic) || mem.Pic.StartsWith("/"))
            {
                return;
            }
            if (mem.Pic.StartsWith("http", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }
            string filePath = Path.Combine(ImageFacade.GetMemberPicRootPath(), mem.Pic);
            if (File.Exists(filePath))
            {
                try
                {
                    File.Delete(filePath);
                }
                catch (Exception ex)
                {
                    logger.WarnFormat("刪除會員圖片失敗, userId={0}, file={1}, ex={2}", mem.UniqueId, filePath, ex);
                }
            }
        }

        public static string GetUserMobile(string Name)
        {
            Member m = mp.MemberGet(Name);
            return m.Mobile;
        }

        public static string GetUserMobile(int uniqueId)
        {
            Member m = mp.MemberGet(uniqueId);
            return m.Mobile;
        }

        /// <summary>
        /// 會員地址
        /// </summary>
        /// <param name="vmbpc"></param>
        /// <returns></returns>
        public static string GetMemberAddress(ViewMemberBuildingCityParentCity vmbpc)
        {
            string city = string.Empty;
            if (!(vmbpc.PcCityName == "DEFAULT" || vmbpc.CityName == "DEFAULT"))
            {
                city = vmbpc.PcCityName + vmbpc.CityName;
            }

            string address = string.Empty;
            if (!string.IsNullOrEmpty(vmbpc.CompanyAddress))
            {
                address = vmbpc.CompanyAddress;
            }

            if (!string.IsNullOrEmpty(vmbpc.PcCityName))
            {
                address = address.Replace(vmbpc.PcCityName, string.Empty);
            }

            if (!string.IsNullOrEmpty(vmbpc.CityName))
            {
                address = address.Replace(vmbpc.CityName, string.Empty);
            }

            return city + address;
        }

        /// <summary>
        /// 更新會員資料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="lastName"></param>
        /// <param name="firstName"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static string UpdateMemberInfo(int userId, string lastName, string firstName, string email, string mobile)
        {
            string errMsg = string.Empty;

            Member mem = mp.MemberGet(userId);

            if (mem.IsLoaded)
            {
                if (!string.IsNullOrEmpty(lastName))
                {
                    mem.LastName = lastName;
                }
                if (!string.IsNullOrEmpty(firstName))
                {
                    mem.FirstName = firstName;
                }
                if (!string.IsNullOrEmpty(email))
                {
                    mem.UserEmail = email;
                }
                if (!string.IsNullOrEmpty(mobile))
                {
                    mem.Mobile = mobile;
                }
                if (!mp.MemberSet(mem))
                {
                    errMsg = "更新失敗";
                }
            }
            else
            {
                errMsg = "查無資料";
            }

            return errMsg;
        }

        #endregion

        #endregion 會員帳號資料相關       

        #region 會員密碼

        /// <summary>
        /// 重設密碼(預設亂碼)，
        /// 這個動作必需在 SetMemberActive 之後
        /// </summary>
        /// <param name="userName">userName</param>
        /// <param name="newPassword">新密碼</param>
        /// <param name="reason">修改密碼的原因</param>
        /// <returns></returns>
        public static bool ResetMemberPassword(string userName, string newPassword, ResetPasswordReason reason)
        {
            return ResetMemberPassword(userName, string.Empty, newPassword, reason);
        }

        public static bool ResetMemberPassword(string userName, string oldPassword, string newPassword, ResetPasswordReason reason,
            bool increaseCookierVersion = false)
        {
            Member mem = MemberFacade.GetMember(userName);
            if (mem.IsLoaded == false)
            {
                return false;
            }
            return ResetMemberPassword(mem, oldPassword, newPassword, reason, increaseCookierVersion);
        }

        public static void UnlockMember(Member mem)
        {
            mem.IsLockedOut = false;
            mp.MemberSet(mem);
        }

        /// <summary>
        /// 重設密碼
        /// </summary>
        /// <param name="mem">Member</param>
        /// <param name="oldPassword">舊密碼(不符合則無法設定)</param>
        /// <param name="newPassword">新密碼</param>
        /// <param name="reason">修改密碼的原因</param>
        /// <param name="increaseCookierVersion">
        ///     如果設為true，那member記在db跟記在cookie的版號都會同時加一，也就是該user就不需要特意的登出登入。
        ///     目前網站的流程不啟用這個設定。
        /// </param>
        /// <returns></returns>
        public static bool ResetMemberPassword(Member mem, string oldPassword, string newPassword, ResetPasswordReason reason,
            bool increaseCookierVersion = false)
        {
            bool isValid = false;
            string userName = mem.UserName;
            try
            {
                if (RegExRules.CheckPassword(newPassword) == false)
                {
                    return false;
                }
                MembershipUser mu = Membership.GetUser(userName);
                if (mu.IsApproved)
                {
                    UnlockMember(mem);
                    isValid = mu.ChangePassword(!string.IsNullOrWhiteSpace(oldPassword) ? oldPassword : mu.ResetPassword(), newPassword);
                }
                //修改密碼後，版號遞增，搭配其它機制來檢查目前的版號跟登入時記在Cookie版號是否一致
                mem.Version++;
                mp.MemberSet(mem);

                CancelAppLoginToken(mem.UniqueId);

                // add version @ cookie
                if (increaseCookierVersion)
                {
                    if (HttpContext.Current != null)
                    {
                        int version = 0;
                        bool keepCookie = false;
                        HttpCookie cookie =
                            HttpContext.Current.Request.Cookies[LkSiteCookie.CookieVersion.ToString()];
                        if (cookie != null)
                        {
                            int tempData;
                            if (int.TryParse(cookie.Value, out tempData))
                            {
                                version = tempData + 1;
                            }
                            else
                            {
                                try
                                {
                                    CookieModel data =
                                        new JsonSerializer().Deserialize<CookieModel>(Helper.Decrypt(cookie.Value));
                                    keepCookie = data.IsKeepCookie;
                                    if (int.TryParse(data.Value, out version))
                                    {
                                        version++;
                                    }
                                }
                                catch (Exception)
                                {
                                    logger.Warn("偵測 ResetMemberPassword null reference 的錯誤 , cookie.Value=" + cookie.Value);
                                    throw;
                                }

                            }
                        }
                        HttpContext.Current.Response.Cookies.Add(
                            CookieManager.MakeCookie(LkSiteCookie.CookieVersion.ToString(), version.ToString(CultureInfo.InvariantCulture), keepCookie));
                    }
                }
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.ChangePassword, true, reason.ToString(),
                    Helper.GetOrderFromType());
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("ResetMemberPassword Error: user={0}, ex={1}", mem.UserName, ex);
                throw;
            }
            return isValid;
        }

        /// <summary>
        /// 會員取得可以重設密碼的方式
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="emailEnabled"></param>
        /// <param name="mobileEnabled"></param>
        /// <param name="emailDisplay"></param>
        /// <param name="mobileDisplay"></param>
        public static void ChecMemberForgetOptions(int userId, out bool emailEnabled, out bool mobileEnabled, out string emailDisplay,
            out string mobileDisplay)
        {
            emailEnabled = false;
            mobileEnabled = false;
            emailDisplay = string.Empty;
            mobileDisplay = string.Empty;
            MobileMember mm = GetMobileMember(userId);
            Member mem = GetMember(userId);
            if (mm.IsLoaded)
            {
                mobileEnabled = true;
                mobileDisplay = Helper.MaskMobile(mm.MobileNumber);
                if (RegExRules.CheckEmail(mem.UserEmail))
                {
                    emailEnabled = true;
                    emailDisplay = Helper.MaskEmail(mem.UserEmail);
                }
            }
            else if (RegExRules.CheckEmail(mem.UserName))
            {
                emailEnabled = true;
                emailDisplay = Helper.MaskEmail(mem.UserName);
            }
        }

        #endregion

        #region 權限管理相關

        public static List<SystemFunction> MemberPrivilegeGetByEmail(string email)
        {
            List<SystemFunction> memberPrivilege = new List<SystemFunction>();
            PrivilegeCollection privilegeList = hump.GetPrivilegeListByEmail(email);
            foreach (Privilege privilege in privilegeList)
            {
                SystemFunction function = systemp.GetSystemFunction(privilege.FuncId);
                if (function.IsLoaded)
                {
                    memberPrivilege.Add(function);
                }
            }

            return memberPrivilege;
        }

        public static List<SystemFunction> OrganizationPrivilegeGetByOrgName(string orgName)
        {
            List<SystemFunction> OrganizationPrivilege = new List<SystemFunction>();
            OrgPrivilegeCollection orgPrivilegeList = hump.GetOrgPrivilegeListByOrgName(orgName);
            foreach (OrgPrivilege orgPrivilege in orgPrivilegeList)
            {
                SystemFunction function = systemp.GetSystemFunction(orgPrivilege.FuncId);
                if (function.IsLoaded)
                {
                    OrganizationPrivilege.Add(function);
                }
            }
            return OrganizationPrivilege;
        }

        /// <summary>
        /// 根據權限及群組名稱[新增]其子群組及成員對應權限
        /// </summary>
        /// <param name="funcId">權限id</param>
        /// <param name="orgName">群組名稱</param>
        /// <param name="funcName">權限名稱</param>
        /// <param name="createId">createId</param>
        /// <returns>新增權限</returns>
        public static void InsertOrgPrivilege(int funcId, string orgName, string createId)
        {
            OrgPrivilege orgPrivilege = hump.GetOrgPrivilege(orgName, funcId);
            if (!orgPrivilege.IsLoaded)
            {
                orgPrivilege.OrgName = orgName;
                orgPrivilege.FuncId = funcId;
                orgPrivilege.CreateId = createId;
                orgPrivilege.CreateTime = DateTime.Now;
                hump.SetOrgPrivilege(orgPrivilege);
            }

            PrivilegeCollection privileges = new PrivilegeCollection();
            // 查找群組是否有成員，有則新增權限
            foreach (MembershipUser user in GetMembershipByUserId(Roles.GetUsersInRole(orgName).ToList()))
            {
                // 若不具有該權限，則新增一筆資料
                //InsertPrivilegeForUser(funcId, createId, user.Email);
                Privilege userPri = hump.GetPrivilege(user.Email, funcId);
                if (!userPri.IsLoaded)
                {
                    Member member = mp.MemberGet(user.Email);
                    if (member.IsLoaded)
                    {
                        userPri.FuncId = funcId;
                        userPri.UserId = member.UniqueId;
                        userPri.CreateId = createId;
                        userPri.CreateTime = DateTime.Now;
                        privileges.Add(userPri);
                    }
                }
            }
            hump.PrivilegeSetList(privileges);
        }

        /// <summary>
        /// 根據權限及群組名稱[新增]其子群組及成員對應權限
        /// </summary>
        /// <param name="funcId">權限id</param>
        /// <param name="orgName">群組名稱</param>
        /// <param name="funcName">權限名稱</param>
        /// <param name="createId">createId</param>
        /// <returns>新增權限</returns>
        public static void InsertOrgPrivileges(int funcId, string orgName, string createId)
        {
            List<Organization> allChildOrgList = new List<Organization>();
            Organization firstOrg = hump.GetOrganization(orgName);
            if (firstOrg.IsLoaded)
            {
                allChildOrgList.Add(firstOrg);
            }

            while (allChildOrgList.Count > 0)
            {
                Organization org = allChildOrgList.First();
                InsertOrgPrivilege(funcId, org.Name, createId);
                allChildOrgList.Remove(org);

                OrganizationCollection childOrgList = hump.GetOrganizationListByParentOrgName(org.Name);
                foreach (Organization childOrg in childOrgList)
                {
                    allChildOrgList.Add(childOrg);
                }
            }
        }

        /// <summary>
        /// 根據會員id取得會員資料(由membership memberid 取得 email)
        /// </summary>
        /// <param name="memberList"></param>
        /// <returns></returns>
        public static List<MembershipUser> GetMembershipByUserId(List<string> memberIdList)
        {
            List<MembershipUser> userList = new List<MembershipUser>();
            foreach (string member in memberIdList)
            {
                MembershipUser user = Membership.GetUser(member, true);
                if (user != null)
                {
                    userList.Add(user);
                }
            }
            return userList;
        }

        /// <summary>
        /// 成員新增權限
        /// </summary>
        /// <param name="funcId">功能權限id</param>
        /// <param name="funcName">功能權限名稱</param>
        /// <param name="createId">createId</param>
        /// <param name="email">Email</param>
        public static void InsertPrivilegeForUser(int funcId, string createId, string email)
        {
            Privilege userPri = hump.GetPrivilege(email, funcId);
            if (!userPri.IsLoaded)
            {
                Member member = mp.MemberGet(email);
                if (member.IsLoaded)
                {
                    userPri.FuncId = funcId;
                    userPri.UserId = member.UniqueId;
                    userPri.CreateId = createId;
                    userPri.CreateTime = DateTime.Now;
                    hump.SetPrivilege(userPri);
                }
            }
        }

        /// <summary>
        /// 新增群組權限，父群組若多了子群組擁有的權限，則增加子群組權限 (以及增加子成員所擁有的權限)
        /// </summary>
        /// <param name="orgName">群組名稱</param>
        /// <param name="funcIds">欲新增的功能清單</param>
        /// <param name="createId">建立人員</param>
        public static void InsertOrgPrivilegeByFuncIds(string orgName, List<int> funcIds, string createId)
        {
            List<Organization> childOrgs = OrganizationGetChildList(orgName);
            foreach (Organization child in childOrgs)
            {
                hump.OrgPrivilegeSetListByOrgName(child.Name, funcIds, createId);
            }
        }

        /// <summary>
        /// 根據群組名稱找到所有子群組(包含本身)
        /// </summary>
        /// <param name="parentOrgName">父群組名稱</param>
        /// <returns></returns>
        public static List<Organization> OrganizationGetChildList(string parentOrgName)
        {
            List<Organization> dataList = new List<Organization>();
            Organization mainOrg = hump.GetOrganization(parentOrgName);
            if (mainOrg.IsLoaded)
            {
                dataList.Add(mainOrg);
                OrganizationCollection orgList = hump.GetOrganizationListByParentOrgName(parentOrgName);
                while (orgList.Count > 0)
                {
                    dataList = dataList.Union(orgList).ToList();
                    foreach (Organization subOrg in orgList)
                    {
                        orgList = hump.GetOrganizationListByParentOrgName(subOrg.Name);
                    }
                }
            }
            return dataList;
        }

        /// <summary>
        /// 取得會員權限
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static RoleCollection GetMemberRoleCol(string name)
        {
            return mp.RoleGetList(name);
        }

        #endregion 權限管理相關

        #region 會員訂單列表相關 ViewCouponLintMain

        /// <summary>
        /// 依Order GUID查尋會員訂單資料
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static ViewCouponListMainCollection ViewCouponListMainGetByOrderGuid(int uniqueId, Guid guid)
        {
            return mp.GetCouponListMainListByOrderID(uniqueId, op.OrderGet(guid).OrderId);
        }

        /// <summary>
        /// 依orderId查尋會員訂單資料
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public static ViewCouponListMainCollection ViewCouponListMainGetByOrderGuid(int uniqueId, string orderId)
        {
            return mp.GetCouponListMainListByOrderID(uniqueId, orderId);
        }

        /// <summary>
        /// [v3.0] 會員訂單總覽
        /// </summary>
        /// <param name="uniqueId">未給uniquleId就只給訂單主分類</param>
        /// <returns></returns>
        public static List<MemberOrderOverview> GetMemberOrderOverview(int? uniqueId = null)
        {
            var overview = new List<MemberOrderOverview>();

            //全部
            overview.Add(
                new MemberOrderOverview
                {
                    MainFilterType = MemberOrderMainFilterType.All,
                    MainFilterName = Helper.GetEnumDescription(MemberOrderMainFilterType.All),
                    MainFilterImage = string.Empty,
                    OrderCount = mp.GetMainFilterOrderCount(uniqueId, MemberOrderMainFilterType.All)
                });

            //購物商品
            overview.Add(
                new MemberOrderOverview
                {
                    MainFilterType = MemberOrderMainFilterType.Goods,
                    MainFilterName = Helper.GetEnumDescription(MemberOrderMainFilterType.Goods),
                    MainFilterImage = string.Empty,
                    OrderCount = mp.GetMainFilterOrderCount(uniqueId, MemberOrderMainFilterType.Goods)
                });

            //電子票券
            overview.Add(
                new MemberOrderOverview
                {
                    MainFilterType = MemberOrderMainFilterType.Ppon,
                    MainFilterName = Helper.GetEnumDescription(MemberOrderMainFilterType.Ppon),
                    MainFilterImage = string.Empty,
                    OrderCount = mp.GetMainFilterOrderCount(uniqueId, MemberOrderMainFilterType.Ppon)
                });

            //咖啡
            overview.Add(
                new MemberOrderOverview
                {
                    MainFilterType = MemberOrderMainFilterType.Coffee,
                    MainFilterName = Helper.GetEnumDescription(MemberOrderMainFilterType.Coffee),
                    MainFilterImage = string.Empty,
                    OrderCount = mp.GetMainFilterOrderCount(uniqueId, MemberOrderMainFilterType.Coffee)
                });

            return overview;
        }

        /// <summary>
        /// [v3.0] 取得訂單分類 (此版未使用至正式站)
        /// </summary>
        /// <param name="uniqueId">未給uniquleId就只給訂單母子分類</param>
        /// <returns></returns>
        public static List<OrderFilter> GetOrderFilters(int? uniqueId = null)
        {
            var orderFilters = new List<OrderFilter>();

            orderFilters.Add(
                new OrderFilter
                {
                    MainFilterType = MemberOrderMainFilterType.All,
                    MainFilterTypeDesc = Helper.GetEnumDescription(MemberOrderMainFilterType.All),
                    OrderCount = mp.GetMainFilterOrderCount(uniqueId, MemberOrderMainFilterType.All),
                    DefaultSubFilter = MemberOrderSubFilterType.GoodsReadyToShip,
                    SubFilters = new List<SubFilter>()
                });

            #region 購物商品
            orderFilters.Add(
                new OrderFilter
                {
                    MainFilterType = MemberOrderMainFilterType.Goods,
                    MainFilterTypeDesc = Helper.GetEnumDescription(MemberOrderMainFilterType.Goods),
                    OrderCount = mp.GetMainFilterOrderCount(uniqueId, MemberOrderMainFilterType.Goods),
                    DefaultSubFilter = MemberOrderSubFilterType.GoodsShipped,
                    SubFilters = new List<SubFilter>
                    {
                        new SubFilter
                        {
                            SubFilterType = MemberOrderSubFilterType.GoodsReadyToShip,
                            SubFilterTypeDesc = Helper.GetEnumDescription(MemberOrderSubFilterType.GoodsReadyToShip)
                        },
                        new SubFilter
                        {
                            SubFilterType = MemberOrderSubFilterType.GoodsShipped,
                            SubFilterTypeDesc = Helper.GetEnumDescription(MemberOrderSubFilterType.GoodsShipped)
                        },
                        new SubFilter
                        {
                            SubFilterType = MemberOrderSubFilterType.GoodsCancel,
                            SubFilterTypeDesc = Helper.GetEnumDescription(MemberOrderSubFilterType.GoodsCancel)
                        }
                    }
                });
            #endregion

            #region 電子票券
            orderFilters.Add(
                new OrderFilter
                {
                    MainFilterType = MemberOrderMainFilterType.Ppon,
                    MainFilterTypeDesc = Helper.GetEnumDescription(MemberOrderMainFilterType.Ppon),
                    OrderCount = mp.GetMainFilterOrderCount(uniqueId, MemberOrderMainFilterType.Ppon),
                    DefaultSubFilter = MemberOrderSubFilterType.PponCanUse,
                    SubFilters = new List<SubFilter>
                    {
                        new SubFilter
                        {
                            SubFilterType = MemberOrderSubFilterType.PponCanUse,
                            SubFilterTypeDesc = Helper.GetEnumDescription(MemberOrderSubFilterType.PponCanUse)
                        },
                        new SubFilter
                        {
                            SubFilterType = MemberOrderSubFilterType.PponUsed,
                            SubFilterTypeDesc = Helper.GetEnumDescription(MemberOrderSubFilterType.PponUsed)
                        },
                        new SubFilter
                        {
                            SubFilterType = MemberOrderSubFilterType.PponCancel,
                            SubFilterTypeDesc = Helper.GetEnumDescription(MemberOrderSubFilterType.PponCancel)
                        }
                    }
                });
            #endregion

            #region 咖啡寄杯
            orderFilters.Add(
                new OrderFilter
                {
                    MainFilterType = MemberOrderMainFilterType.Coffee,
                    MainFilterTypeDesc = Helper.GetEnumDescription(MemberOrderMainFilterType.Coffee),
                    OrderCount = mp.GetMainFilterOrderCount(uniqueId, MemberOrderMainFilterType.Coffee),
                    DefaultSubFilter = MemberOrderSubFilterType.Coffee,
                    SubFilters = new List<SubFilter>
                    {
                        new SubFilter
                        {
                            SubFilterType = MemberOrderSubFilterType.Coffee,
                            SubFilterTypeDesc = Helper.GetEnumDescription(MemberOrderSubFilterType.Coffee)
                        }
                    }
                });
            #endregion

            return orderFilters;
        }

        /// <summary>
        /// [v3.0] APP 訂單列表
        /// </summary>
        public static List<ApiMemberOrder> GetApiMemberOrdersByUser(int uniqueId, MemberOrderSubFilterType subFilter, int pageLength)
        {
            var mainList = mp.GetCouponListMainColBySubFilter(uniqueId, subFilter, pageLength).ToList();

            //if (subFilter == MemberOrderSubFilterType.PponCanUse)
            //{
            //    //未逾期憑證的精確判斷
            //    mainList = mainList.Where(main => !PponDealApiManager.IsPastFinalExpireDate(main)).ToList();
            //}

            var orderList = new List<ApiMemberOrder>();
            foreach (var main in mainList.OrderByDescending(x => x.CreateTime))
            {
                //排除新光檔
                if (main.GroupOrderStatus != null && ((int)main.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0) continue;

                var order = new ApiMemberOrder();

                //共用
                order.OrderGuid = main.Guid;
                order.OrderId = main.OrderId;
                bool isSquare = subFilter != MemberOrderSubFilterType.Coffee; //是否顯示正方型圖
                order.MainImagePath = GetSquareImagePathByOrder(main, isSquare);
                order.ItemName = main.CouponUsage;
                order.ProductDeliveryType = main.ProductDeliveryType;
                if (OrderFacade.IsIspPaymentOrder(main.OrderStatus))
                {
                    var ispOrder = OrderFacade.GetIspPaymentOrder(main.OrderId);
                    order.BuyDate = ispOrder.IsLoaded && ispOrder.DeliveryTime != null
                        ? ApiSystemManager.DateTimeToDateTimeString((DateTime)ispOrder.DeliveryTime)
                        : ApiSystemManager.DateTimeToDateTimeString(main.CreateTime);
                }
                else
                {
                    order.BuyDate = ApiSystemManager.DateTimeToDateTimeString(main.CreateTime);
                }
                order.CsMessageCount = csp.GetCustomerServiceOutsideLogCountByorderGuid(main.UniqueId, main.Guid);
                order.OrderStatusDesc = PponDealApiManager.GetOrderTypeDesc(main);
                order.IsShowOrderStatus =
                    //取消訂單、ATM未付款 要顯示訂單狀態
                    subFilter == MemberOrderSubFilterType.PponCancel || subFilter == MemberOrderSubFilterType.GoodsCancel ||
                    Helper.IsFlagSet(main.OrderStatus, (int)OrderStatus.ATMOrder) &&
                    !Helper.IsFlagSet(main.OrderStatus, (int)OrderStatus.Complete);
                order.OrderTags = new List<int>();
                if (main.IsDepositCoffee)
                {
                    order.OrderTags.Add((int)DealLabelSystemCode.DepositCoffee);
                }
                if (Helper.IsFlagSet(main.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
                {
                    order.OrderTags.Add((int)DealLabelSystemCode.GroupCoupon);
                }

                //憑證
                if (main.DeliveryType == (int)DeliveryType.ToShop)
                {
                    order.PponUseStartDate = main.BusinessHourDeliverTimeS != null
                        ? ApiSystemManager.DateTimeToDateTimeString((DateTime)main.BusinessHourDeliverTimeS) : string.Empty;

                    order.PponUseEndDate = main.ChangedExpireDate != null
                        ? ApiSystemManager.DateTimeToDateTimeString((DateTime)main.ChangedExpireDate)
                        : main.BusinessHourDeliverTimeE != null
                            ? ApiSystemManager.DateTimeToDateTimeString((DateTime)main.BusinessHourDeliverTimeE)
                            : string.Empty;

                    order.PponCouponCount = CouponFacade.GetCouponCanUsedCount(main);
                    order.PponTotalCouponCount = main.TotalCount ?? 0;
                    order.PponGroupCouponAppChannel = main.GroupCouponAppStyle;
                }

                //宅配
                else if (main.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(main.BusinessHourGuid, true);
                    ShipInfo shipinfo = OrderFacade.GetOrderShipInfo(main);
                    order.GoodsLastShippingDate = shipinfo.LastShippingDate;
                    if (!string.IsNullOrWhiteSpace(deal.LabelIconList))
                    {
                        string[] ss = deal.LabelIconList.Split(',');
                        if (ss.Contains(((int)DealLabelSystemCode.TwentyFourHoursArrival).ToString()))
                        {
                            //order.GoodsLastShippingDate = "24H到貨不適用";
                            //24H 二次覆蓋時間
                            order.GoodsLastShippingDate = OrderFacade.GetShipDate(main.CreateTime);
                        }
                    }
                    order.GoodsActualShippingDate = shipinfo.ActualShippingDate;
                    order.GoodsShipStatus = shipinfo.ShipStatus;
                    order.Is24H = deal.CategoryIds != null && deal.CategoryIds.Contains(
                        ViewPponDealManager._24H_ARRIVAL_CATEGORY_ID);
                }
                orderList.Add(order);
            }
            return orderList;
        }

        /// <summary>
        /// [v3.0] Web 訂單列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mainFilter"></param>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <returns></returns>
        public static ViewCouponListMainCollection ViewCouponListMainGetListByUser(int userId, MemberOrderMainFilterType mainFilter,
            int pageStart, int pageLength)
        {
            ViewCouponListMainCollection data = mp.GetCouponListMainColByMainFilter(userId, mainFilter, pageStart, pageLength);
            return data;
        }


        /// <summary>
        /// 取得正方型/長方型檔次圖片
        /// </summary>
        public static string GetSquareImagePathByDeal(IViewPponDeal deal, bool isSquare)
        {
            var imagePath = isSquare && !string.IsNullOrEmpty(deal.AppDealPic)
                ? deal.AppDealPic : deal.EventImagePath;

            return PponFacade.GetPponDealFirstImagePath(imagePath);
        }

        /// <summary>
        /// 取得正方型/長方型訂單檔次圖
        /// </summary>
        public static string GetSquareImagePathByOrder(ViewCouponListMain main, bool isSquare)
        {
            var imagePath = string.Empty;
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(main.BusinessHourGuid);
            //多檔次圖片一律以母檔為準
            if (Helper.IsFlagSet(main.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
            {
                if (deal.MainBid != null)
                {
                    IViewPponDeal mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)deal.MainBid);
                    imagePath = isSquare && !string.IsNullOrEmpty(mainDeal.AppDealPic)
                        ? mainDeal.AppDealPic
                        : mainDeal.EventImagePath;
                }
            }
            else
            {
                imagePath = isSquare && !string.IsNullOrEmpty(deal.AppDealPic)
                    ? deal.AppDealPic
                    : main.ImagePath;
            }

            return PponFacade.GetPponDealFirstImagePath(imagePath);
        }

        #region 舊版訂單 [v1.0] [v2.0]

        /// <summary>
        /// [v1.0] 查詢會員訂單列表資料
        /// </summary>
        /// <param name="pageStart">第幾頁</param>
        /// <param name="pageLength">每頁尺寸</param>
        /// <param name="uniqueId">會員Id</param>
        /// <param name="filterType">查詢的資料分類</param>
        /// <param name="orderGuid">訂單編號，可為空</param>
        /// <param name="isLatest">是否只取最近四個月的資料</param>
        /// <param name="withPiinlife"></param>
        /// <returns></returns>
        public static ViewCouponListMainCollection ViewCouponListMainGetListByUser(int pageStart, int pageLength,
            int uniqueId, CouponListFilterType filterType, Guid? orderGuid, bool isLatest, bool withPiinlife)
        {
            ViewCouponListMainCollection data = mp.GetCouponListMainListByUser(pageStart, pageLength, uniqueId,
                string.Empty, GetFilterStringForViewCouponListMain(filterType, orderGuid, isLatest), withPiinlife);
            return data;
        }

        /// <summary> 
        /// [v2.0] 查詢會員訂單列表資料
        /// </summary>
        /// <param name="pageStart">第幾頁</param>
        /// <param name="pageLength">每頁尺寸</param>
        /// <param name="userId">會員Id</param>
        /// <param name="newFilterType">查詢的資料分類</param>
        /// <param name="orderGuid">訂單編號，可為空</param>
        /// <param name="isLatest">是否只取最近四個月的資料</param>
        /// <param name="withPiinlife">包含品生活檔次</param>
        /// <returns></returns>
        public static ViewCouponListMainCollection ViewCouponListMainGetListByUser(int pageStart, int pageLength, int userId,
            NewCouponListFilterType newFilterType, Guid? orderGuid, bool isLatest, bool withPiinlife)
        {
            ViewCouponListMainCollection data = mp.GetCouponListMainListByUser(pageStart, pageLength, userId,
                string.Empty, GetFilterStringForViewCouponListMain(newFilterType, orderGuid, isLatest), withPiinlife);
            return data;
        }

        /// <summary>
        /// [v2.0] 查詢憑證待用資料
        /// </summary>
        /// <param name="pageStart">第幾頁</param>
        /// <param name="pageLength">每頁尺寸</param>
        /// <param name="userId">會員Id</param>
        /// <param name="newFilterType">查詢的資料分類</param>
        /// <param name="orderGuid">訂單編號，可為空</param>
        /// <param name="withPiinlife">包含品生活檔次</param>
        /// <returns></returns>
        public static ViewCouponOnlyListMainCollection ViewCouponListMainCanUseGetListByUser(int pageStart, int pageLength, int userId,
            NewCouponListFilterType newFilterType, Guid? orderGuid, bool withPiinlife)
        {
            ViewCouponOnlyListMainCollection data = mp.GetCouponOnlyListMainListByUser(pageStart, pageLength, userId,
                string.Empty, GetFilterStringForViewCouponListMainForCanUse(newFilterType, orderGuid), withPiinlife);
            return data;
        }

        public static ViewCouponListSequenceCollection GetCouponListSequenceByOidList(List<Guid> oidList)
        {
            return mp.GetCouponListSequenceByOidList(oidList);
        }

        /// <summary>
        /// [v1.0] 依據傳入條件，回傳會員訂單數量
        /// </summary>
        /// <returns></returns>
        public static int ViewCouponOnlyListMainGetCount(int userId, NewCouponListFilterType newFilterType, Guid? orderGuid)
        {
            return mp.GetCouponOnlyListMainCount(userId, GetFilterStringForViewCouponListMainForCanUse(newFilterType, orderGuid));
        }

        /// <summary>
        /// [v1.1] 依據傳入條件，回傳會員訂單數量
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="filterType"></param>
        /// <param name="orderGuid"></param>
        /// <param name="isLatest"></param>
        /// <returns></returns>
        public static int ViewCouponListMainGetCount(int userId, CouponListFilterType filterType, Guid? orderGuid, bool isLatest)
        {
            return mp.GetCouponListMainCount(userId, GetFilterStringForViewCouponListMain(filterType, orderGuid, isLatest));
        }

        /// <summary>
        /// [v2.0] 依據傳入條件，回傳會員訂單數量
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newFilterType"></param>
        /// <param name="orderGuid"></param>
        /// <param name="isLatest"></param>
        /// <returns></returns>
        public static int ViewCouponListMainGetCount(int userId, NewCouponListFilterType newFilterType, Guid? orderGuid, bool isLatest)
        {
            return mp.GetCouponListMainCount(userId, GetFilterStringForViewCouponListMain(newFilterType, orderGuid, isLatest));
        }

        /// <summary>
        /// [v2.0] 回傳ViewCouponListMain的NewCouponListFilterType
        /// 由於可能一個ViewCouponListMain擁有兩個type，所以回傳list
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static List<NewCouponListFilterType> GetViewCouponListMainGetCountNewFilterTypes(ViewCouponListMain main)
        {
            List<NewCouponListFilterType> types = new List<NewCouponListFilterType>();

            foreach (var type in _newCouponListFilterTypes())
            {
                if (ViewCouponListMainIsFilterType(main, type))
                {
                    types.Add(type);
                }
            }
            return types;
        }

        /// <summary>
        /// [v1.0] 回傳ViewCouponListMain的CouponListFilterType
        /// 由於可能一個ViewCouponListMain擁有兩個type，所以回傳list
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static List<CouponListFilterType> GetViewCouponListMainGetCountFilterTypes(ViewCouponListMain main)
        {
            List<CouponListFilterType> types = new List<CouponListFilterType>();

            foreach (var type in _couponListFilterTypes)
            {
                if (ViewCouponListMainIsFilterType(main, type))
                {
                    types.Add(type);
                }
            }
            return types;
        }

        /// <summary>
        /// [v2.0] 依據要查詢的訂單類別與相關條件，查詢會員訂單列表的查詢條件
        /// </summary>
        /// <param name="newFilterType"></param>
        /// <param name="orderGuid"></param>
        /// <param name="isLatest"></param>
        /// <returns></returns>
        protected static string GetFilterStringForViewCouponListMain(NewCouponListFilterType newFilterType, Guid? orderGuid, bool isLatest)
        {
            string filter = string.Empty;
            if (orderGuid != null && !orderGuid.Equals(Guid.Empty))
            {
                filter += " and " + ViewCouponListMain.Columns.Guid + " = '" + orderGuid.ToString() + "'";
            }
            else
            {
                if (isLatest)
                {

                    //OrderStatus.PartialCancel 524288
                    filter += @" and (
(delivery_type = 2 
		and create_time > DATEADD(month, -4,  GETDATE()))
	or 
		(delivery_type = 1
		and create_time > DATEADD(month, -4,  GETDATE()))
	or (delivery_type = 1 and remain_count > 0 and item_price > 0 
        and getdate() < business_hour_deliver_time_e
        and (order_status & 512 = 0 or order_status & 524288 > 0))
)

";
                    //" and " + ViewCouponListMain.Columns.CreateTime + ">=dateadd(month,-4,getdate()) ";
                }
            }

            switch (newFilterType)
            {
                #region 全部訂單
                case NewCouponListFilterType.None://全部訂單，但須排除已使用、已過期、金額等於0的全家憑證
                    filter += " and not(("
                                + ViewCouponListMain.Columns.RemainCount + "=0 "
                                + "or cast(" + ViewCouponListMain.Columns.BusinessHourDeliverTimeE + " As Date) < cast(getdate() As Date))"
                                + " and " + ViewCouponListMain.Columns.Status + " & " + (int)GroupOrderStatus.FamiDeal + "> 0 "
                                + " and " + ViewCouponListMain.Columns.ItemPrice + " = 0)";
                    break;
                #endregion
                #region 憑證待使用
                case NewCouponListFilterType.NotUsed://尚有憑證，有憑證，非取消訂單，大於0元 type 1
                    filter += " and (" + ViewCouponListMain.Columns.RemainCount + ">0 or(" + ViewCouponListMain.Columns.RemainCount + " =0 and " + ViewCouponListMain.Columns.TotalCount + " =0))"
                            + " and " + ViewCouponListMain.Columns.ItemPrice + ">0 "
                            + " and " + ViewCouponListMain.Columns.DeliveryType + " != " + (int)DeliveryType.ToHouse
                            + " and ( "
                                      + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + "=0 "
                                + "or (" + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " >0 "
                                        + " and " + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.PartialCancel + " >0 "
                                   + ")"
                                + " ) "
                            + " and isnull(" + ViewCouponListMain.Columns.ChangedExpireDate + "," + ViewCouponListMain.Columns.BusinessHourDeliverTimeE + ")>='" + DateTime.Now.ToString("yyyy-MM-dd") + "'" //以調整有效期限為優先
                            + " and " + ViewCouponListMain.Columns.Status + " & " + (int)GroupOrderStatus.FamiDeal + " =0 ";
                    break;
                #endregion
                #region 宅配未出貨
                case NewCouponListFilterType.NotShipped: //宅配未出貨 未出貨紀錄約定2013-07-01之後的訂單才顯示。 type 2
                    filter += " and " + ViewCouponListMain.Columns.DeliveryType + "=" + (int)DeliveryType.ToHouse
                            + " and " + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " =0 "
                            + " and " + ViewCouponListMain.Columns.Status + " & " + (int)GroupOrderStatus.KindDeal + "=0 "
                            + " and " + ViewCouponListMain.Columns.CreateTime + " >= '2013-07-01' "
                            + " and not ( " + ViewCouponListMain.Columns.ShipTime + " is not null and " + ViewCouponListMain.Columns.ShipTime + " <= SYSDATETIME() ) ";
                    break;
                #endregion
                #region 全家憑證待使用
                case NewCouponListFilterType.Fami://全家優惠憑證待使用，須排除已使用、已過期的全家憑證 type 3
                    filter += " and " + ViewCouponListMain.Columns.RemainCount + " >0 "
                            + " and " + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " =0 "
                            + " and cast(" + ViewCouponListMain.Columns.BusinessHourDeliverTimeE + " As Date) >= cast(getdate() As Date)"
                            + " and " + ViewCouponListMain.Columns.Status + " & " + (int)GroupOrderStatus.FamiDeal + " >0 "
                            + " and " + ViewCouponListMain.Columns.BusinessHourStatus + " & " + (int)BusinessHourStatus.GroupCoupon + " =0 ";
                    break;
                #endregion
                #region 好康憑證
                case NewCouponListFilterType.CouponOnly://純憑證檔（排除活動、零元、全家、宅配、捐款）： type 4
                    filter += " and " + ViewCouponListMain.Columns.ItemPrice + ">0"
                            + " and " + ViewCouponListMain.Columns.DeliveryType + " != " + (int)DeliveryType.ToHouse
                            + " and " + ViewCouponListMain.Columns.Status + " & " + (int)GroupOrderStatus.FamiDeal + " =0 "
                            + " and ( "
                                   + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " =0 "
                                + " or ("
                                        + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " >0 "
                                        + " and " + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.PartialCancel + " >0 "
                                   + " ) "
                                + " ) ";
                    break;
                #endregion
                #region 宅配商品
                case NewCouponListFilterType.Product://無憑證，非取消單 :::ViewCouponListMain.Columns.Department + ">3，並過濾公益檔次 :::sam 取消已結檔判定  type 5
                    {
                        var filterList = new List<string>
                        {
                            string.Format(" and {0} = {1} ", ViewCouponListMain.Columns.DeliveryType,(int)DeliveryType.ToHouse),
                            string.Format(" and {0} & {1} = 0 ", ViewCouponListMain.Columns.OrderStatus, (int)OrderStatus.Cancel),
                            string.Format(" and {0} & {1} = 0", ViewCouponListMain.Columns.Status, (int)GroupOrderStatus.KindDeal),
                            string.Format(" and {0} & {1} = 0", ViewCouponListMain.Columns.Status, (int)GroupOrderStatus.FamiDeal)
                        };
                        if (config.EnabledExchangeProcess)
                        {
                            filterList.Add(string.Format(" and {0} not in ({1}, {2})", ViewCouponListMain.Columns.ExchangeStatus, (int)OrderLogStatus.ExchangeProcessing, (int)OrderLogStatus.SendToSeller));
                        }

                        filter += string.Join("", filterList);
                    }
                    break;
                #endregion
                #region 活動與抽獎
                case NewCouponListFilterType.Event://活動與抽獎, 0元好康 , 不包含全家 新光 type 6
                    filter += " and " + ViewCouponListMain.Columns.ItemPrice + " =0 "
                            + " and " + ViewCouponListMain.Columns.Status + " & " + (int)GroupOrderStatus.FamiDeal + " =0 "
                            + " and " + ViewCouponListMain.Columns.Status + " & " + (int)GroupOrderStatus.SKMDeal + " =0 "
                            + " and " + ViewCouponListMain.Columns.BusinessHourDeliverTimeE + " is not NULL";
                    break;

                #endregion
                #region 已取消訂單
                case NewCouponListFilterType.Cancel://取消訂單 type 7
                    filter += " and ("
                                    + " ( "
                                        + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " >0 "
                                        + " and " + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.PartialCancel + " =0 "
                                    + " ) "
                                    + " or ( "
                                        + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + ">0 "
                                        + " and " + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.PartialCancel + " >0 "
                                        + " and " + ViewCouponListMain.Columns.RemainCount + " =0 "
                                    + " ) "
                                + ")"
                                + " and " + ViewCouponListMain.Columns.Status + " & " + (int)GroupOrderStatus.FamiDeal + " =0 ";
                    break;
                #endregion
                #region 憑證逾期未使用
                case NewCouponListFilterType.OverdueNotUsed://憑證檔（排除活動、零元、全家、宅配、捐款）逾期未使用： type 8
                    filter += " and ("
                                    + ViewCouponListMain.Columns.RemainCount + ">0 "
                                    + " or (" + ViewCouponListMain.Columns.RemainCount + " =0 "
                                        + " and " + ViewCouponListMain.Columns.TotalCount + " =0 "
                                    + " ) "
                                + " ) "
                            + " and " + ViewCouponListMain.Columns.ItemPrice + ">0 "
                            + " and " + ViewCouponListMain.Columns.DeliveryType + " != " + (int)DeliveryType.ToHouse
                            + " and ( "
                                    + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " =0 "
                                    + " or ("
                                        + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " >0 "
                                        + " and " + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.PartialCancel + " >0 "
                                    + " ) "
                                + ")"
                            + " and isnull(" + ViewCouponListMain.Columns.ChangedExpireDate + "," + ViewCouponListMain.Columns.BusinessHourDeliverTimeE + ")<'" + DateTime.Now.ToString("yyyy-MM-dd") + "'"; //以調整有效期限為優先
                    break;
                #endregion
                #region 成套票券
                case NewCouponListFilterType.GroupCoupon:
                    //商品券 憑證、成單、成套販售
                    {
                        var filterList = new List<string>
                        {
                            string.Format(" and {0}>0 ",ViewCouponListMain.Columns.TotalCount),
                            string.Format(" and {0} = {1} ",ViewCouponListMain.Columns.DeliveryType,(int)DeliveryType.ToShop),
                            string.Format(" and {0}&{1} >0 ",ViewCouponListMain.Columns.BusinessHourStatus,(int)BusinessHourStatus.GroupCoupon),
                            string.Format(" and {0}&{1}>0 ",ViewCouponListMain.Columns.OrderStatus,(int)OrderStatus.Complete),
                            string.Format(" and {0}&{1} = 0",ViewCouponListMain.Columns.Status, (int)GroupOrderStatus.FamiDeal),
                            string.Format(" and {0} = 0", ViewCouponListMain.Columns.IsDepositCoffee),
                        };

                        filter += string.Join("", filterList);
                    }
                    break;
                #endregion
                #region 成套-待使用
                case NewCouponListFilterType.GroupCouponReady:
                    //商品券待使用 剩餘數量、憑證、成單、成套販售、有效其間
                    {
                        var filterList = new List<string>
                        {
                            string.Format(" and {0}>0 ",ViewCouponListMain.Columns.RemainCount),
                            string.Format(" and {0}>0 ",ViewCouponListMain.Columns.TotalCount),
                            string.Format(" and {0} = {1} ",ViewCouponListMain.Columns.DeliveryType,(int)DeliveryType.ToShop),
                            string.Format(" and {0}&{1} >0 ",ViewCouponListMain.Columns.BusinessHourStatus,(int)BusinessHourStatus.GroupCoupon),
                            string.Format(" and {0}&{1}>0 ",ViewCouponListMain.Columns.OrderStatus,(int)OrderStatus.Complete),
                            string.Format(" and {0} >= '{1:yyyy/MM/dd HH:mm:ss}'",ViewCouponListMain.Columns.BusinessHourDeliverTimeE,DateTime.Now),
                            string.Format(" and {0}&{1} = 0",ViewCouponListMain.Columns.Status, (int)GroupOrderStatus.FamiDeal),
                            string.Format(" and {0} = 0", ViewCouponListMain.Columns.IsDepositCoffee),
                        };

                        filter += string.Join("", filterList);
                    }
                    break;
                #endregion
                #region 成套-逾期未使用
                case NewCouponListFilterType.GroupCouponNotUsed:
                    //商品券 憑證、成單、成套販售
                    {
                        var filterList = new List<string>
                        {
                            string.Format(" and {0}>0 ",ViewCouponListMain.Columns.RemainCount),
                            string.Format(" and {0}>0 ",ViewCouponListMain.Columns.TotalCount),
                            string.Format(" and {0} = {1} ",ViewCouponListMain.Columns.DeliveryType,(int)DeliveryType.ToShop),
                            string.Format(" and {0}&{1} >0 ",ViewCouponListMain.Columns.BusinessHourStatus,(int)BusinessHourStatus.GroupCoupon),
                            string.Format(" and {0}&{1}>0 ",ViewCouponListMain.Columns.OrderStatus,(int)OrderStatus.Complete),
                            string.Format(" and {0} < '{1:yyyy/MM/dd HH:mm:ss}'",ViewCouponListMain.Columns.BusinessHourDeliverTimeE,DateTime.Now),
                            string.Format(" and {0}&{1} = 0",ViewCouponListMain.Columns.Status, (int)GroupOrderStatus.FamiDeal),
                            string.Format(" and {0} = 0", ViewCouponListMain.Columns.IsDepositCoffee),
                        };

                        filter += string.Join("", filterList);
                    }
                    break;
                #endregion
                #region 評價訂單
                //case NewCouponListFilterType.GroupCouponToEvaluate:
                //    //TODO: 可評價訂單 --- Zoey

                //    break;
                //case NewCouponListFilterType.CouponToEvaluate:
                //    //TODO: 可評價訂單 --- Zoey

                //break;
                #endregion
                #region 新光
                case NewCouponListFilterType.SKM://新光
                    filter += " and " + ViewCouponListMain.Columns.ItemPrice + "=0 "
                            + " and " + ViewCouponListMain.Columns.RemainCount + ">0 "
                            + " and " + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " =0 "
                            + " and " + ViewCouponListMain.Columns.Status + "&" + (int)GroupOrderStatus.SKMDeal + ">0 ";
                    break;
                #endregion
                #region 全家寄杯
                case NewCouponListFilterType.FamiCoffee: //全家寄杯
                    {
                        var filterList = new List<string>
                        {
                            string.Format(" and {0} > 0 ", ViewCouponListMain.Columns.ItemPrice),
                            string.Format(" and {0} & {1} > 0", ViewCouponListMain.Columns.OrderStatus, (int)OrderStatus.Complete),
                            string.Format(" and {0} & {1} > 0", ViewCouponListMain.Columns.Status, (int)GroupOrderStatus.FamiDeal),
                        };

                        filter += string.Join("", filterList);
                    }
                    break;
                #endregion
                #region 咖啡寄杯
                case NewCouponListFilterType.DepositCoffee: //所有寄杯
                    {
                        var filterList = new List<string>
                        {
                            string.Format(" and {0} & {1} > 0", ViewCouponListMain.Columns.OrderStatus, (int)OrderStatus.Complete),
                            string.Format(" and ( {0} = 1", ViewCouponListMain.Columns.IsDepositCoffee),
                            string.Format(" or ( {0} > 0 ", ViewCouponListMain.Columns.ItemPrice),
                            string.Format(" and {0} & {1} > 0))", ViewCouponListMain.Columns.Status, (int)GroupOrderStatus.FamiDeal),
                        };

                        filter += string.Join("", filterList);
                    }
                    break;
                #endregion
                #region 換貨處理中訂單
                case NewCouponListFilterType.Exchanging://換貨處理中訂單
                    {
                        var filterList = new List<string>
                        {
                            string.Format(" and {0} = {1} ", ViewCouponListMain.Columns.DeliveryType,(int)DeliveryType.ToHouse),
                            string.Format(" and {0} & {1} = 0 ", ViewCouponListMain.Columns.OrderStatus, (int)OrderStatus.Cancel),
                            string.Format(" and {0} & {1} > 0", ViewCouponListMain.Columns.OrderStatus, (int)OrderStatus.Complete),
                            string.Format(" and {0} in ({1}, {2})", ViewCouponListMain.Columns.ExchangeStatus, (int)OrderLogStatus.ExchangeProcessing, (int)OrderLogStatus.SendToSeller),
                        };

                        filter += string.Join("", filterList);
                    }
                    break;
                #endregion
                default:
                    break;
            }
            return filter;
        }

        public static void SetMemberRiskScore(int userId, int riskScore)
        {
            MemberProperty memProp = mp.MemberPropertyGetByUserId(userId);
            if (memProp.IsLoaded)
            {
                memProp.RiskScore = riskScore;
                mp.MemberPropertySet(memProp);
            }
            else
            {
                Member mem = MemberFacade.GetMember(userId);
                memProp = new MemberProperty();
                memProp.UserId = mem.UniqueId;
                memProp.RiskScore = riskScore;
                memProp.RegisterTime = mem.CreateTime;
                mp.MemberPropertySet(memProp);
            }
        }

        /// <summary>
        /// [v1.0] 依據要查詢的訂單類別與相關條件，查詢會員訂單列表的查詢條件
        /// </summary>
        /// <param name="filterType"></param>
        /// <param name="orderGuid"></param>
        /// <param name="isLatest"></param>
        /// <returns></returns>
        protected static string GetFilterStringForViewCouponListMain(CouponListFilterType filterType, Guid? orderGuid, bool isLatest)
        {
            string filter = string.Empty;
            if (orderGuid != null && !orderGuid.Equals(Guid.Empty))
            {
                filter += " and " + ViewCouponListMain.Columns.Guid + " = '" + orderGuid.ToString() + "'";
            }
            else
            {
                if (isLatest)
                {
                    filter += " and " + ViewCouponListMain.Columns.CreateTime + ">=dateadd(month,-4,getdate()) ";
                }
            }

            switch (filterType)
            {
                case CouponListFilterType.Expired://使用期限已過，有憑證，已結檔，非取消訂單::ViewCouponListMain.Columns.Department + "<4
                    filter += " and " + ViewCouponListMain.Columns.BusinessHourDeliverTimeE + "<getdate() and " + ViewCouponListMain.Columns.DeliveryType + "<" + (int)DeliveryType.ToHouse + " and " + ViewCouponListMain.Columns.BusinessHourOrderTimeE + "<getdate() and " +
                         ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.Cancel + "=0 ";
                    break;

                case CouponListFilterType.NotExpired://使用期限未過，有憑證，已結檔，非取消訂單
                    filter += " and " + ViewCouponListMain.Columns.BusinessHourDeliverTimeE + ">=getdate() and " + ViewCouponListMain.Columns.DeliveryType + "<" + (int)DeliveryType.ToHouse + " and " + ViewCouponListMain.Columns.BusinessHourOrderTimeE + ">=getdate() and " +
                         ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.Cancel + "=0 ";
                    break;

                case CouponListFilterType.NotUsed://尚有憑證，有憑證，非取消訂單，大於0元
                    filter += " and (" + ViewCouponListMain.Columns.RemainCount + ">0 or(" + ViewCouponListMain.Columns.RemainCount + "=0 and " + ViewCouponListMain.Columns.TotalCount + "=0)) and " +
                        ViewCouponListMain.Columns.ItemPrice + ">0 and " + ViewCouponListMain.Columns.DeliveryType + "<2 and " +
                         " ( " + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.Cancel + "=0 or (" + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.Cancel + ">0 and " + ViewCouponListMain.Columns.OrderStatus + "&524288>0 ))";
                    break;

                case CouponListFilterType.Used://憑證用完，有憑證，非取消訂單 :::sam 取消已結檔判定
                    filter += " and " + ViewCouponListMain.Columns.RemainCount + "=0 and " + ViewCouponListMain.Columns.TotalCount + ">0 and " + ViewCouponListMain.Columns.DeliveryType + "<2 and " + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.Cancel + "=0 ";
                    break;

                case CouponListFilterType.Cancel://取消訂單
                    filter += " and ((" + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.Cancel + ">0 and " + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.PartialCancel + "=0) or (" + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.Cancel + ">0 and " + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.PartialCancel + ">0 and " + ViewCouponListMain.Columns.RemainCount + "=0))";
                    break;

                case CouponListFilterType.Product://無憑證，非取消單 :::ViewCouponListMain.Columns.Department + ">3，並過濾公益檔次 :::sam 取消已結檔判定
                    filter += " and " + ViewCouponListMain.Columns.DeliveryType + ">" + (int)DeliveryType.ToShop + " and " + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.Cancel + "=0 and " + ViewCouponListMain.Columns.Status + "&" + (int)GroupOrderStatus.KindDeal + "=0 ";
                    break;

                case CouponListFilterType.NotCompleted://未完成付款;包括逾期未付款與等待繼續付款 (ATM且非取消單且未完成單)
                    filter += " and " + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.ATMOrder + ">0 and " + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.Cancel + "=0 and " + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.Complete + "=0 ";
                    break;

                case CouponListFilterType.Event://優惠活動;包含全家優惠憑證、抽獎好康)]： 包含 0元好康 、全家專區檔次(排除已過期且未使用的訂單，保留已使用的 (訂單金額為0且未過期) :::sam 取消已結檔判定
                    filter += " and " + ViewCouponListMain.Columns.ItemPrice + "=0 and " +
                              ViewCouponListMain.Columns.Status + "&" + (int)GroupOrderStatus.FamiDeal + " = 0 ";
                    break;

                case CouponListFilterType.Kind://公益檔次
                    filter += " and " + ViewCouponListMain.Columns.OrderStatus + "&" + (int)OrderStatus.Cancel + "=0 and " + ViewCouponListMain.Columns.Status + "&" + (int)GroupOrderStatus.KindDeal + ">0 ";
                    break;
                default:
                    break;
            }
            return filter;
        }

        /// <summary>
        /// [v2.0] 依據要查詢的待使用憑證與相關條件，查詢會員待使用憑證列表的查詢條件
        /// </summary>
        /// <returns></returns>
        protected static string GetFilterStringForViewCouponListMainForCanUse(NewCouponListFilterType newFilterType, Guid? orderGuid)
        {
            string filter = string.Empty;
            if (orderGuid != null && !orderGuid.Equals(Guid.Empty))
            {
                filter += " and " + ViewCouponOnlyListMain.Columns.Guid + " = '" + orderGuid.ToString() + "'";
            }

            //預設為待使用憑證資料
            filter += " and return_status in (-1, " + (int)ProgressStatus.Canceled + ") "
                    + " and cancel_status not in (" + (int)OrderLogStatus.ExchangeCancel + "," + (int)OrderLogStatus.ExchangeFailure + "," + (int)OrderLogStatus.ExchangeProcessing + "," + (int)OrderLogStatus.ExchangeSuccess + "," + (int)OrderLogStatus.SendToSeller + ") "
                    + " and ( " + ViewCouponOnlyListMain.Columns.Department + "<= 3 and " + ViewCouponOnlyListMain.Columns.ExpirationDate + " >= getdate()) "
                    + " and not( " + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.ATMOrder + "> 0 and " + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Complete + " = 0)"
                    + " and " + ViewCouponOnlyListMain.Columns.RemainCount + ">0 "
                    + " and (( cast(" + ViewCouponOnlyListMain.Columns.BusinessHourOrderTimeE + " As Date) <= cast(getdate() As Date) and " + ViewCouponOnlyListMain.Columns.Slug + " >= " + ViewCouponOnlyListMain.Columns.BusinessHourOrderMinimum + " ) "
                        + " or ( cast(" + ViewCouponOnlyListMain.Columns.BusinessHourOrderTimeE + " As Date) > cast(getdate() As Date))) "
                    + " and " + ViewCouponOnlyListMain.Columns.DeliveryType + " != " + (int)DeliveryType.ToHouse;

            switch (newFilterType)
            {
                case NewCouponListFilterType.None://全部訂單，但須排除已使用、已過期的全家憑證
                    filter += " and not((" + ViewCouponListMain.Columns.RemainCount + "=0 or cast(" + ViewCouponListMain.Columns.BusinessHourDeliverTimeE + " As Date) < cast(getdate() As Date) )"
                            + " and " + ViewCouponListMain.Columns.Status + " & " + (int)GroupOrderStatus.FamiDeal + ">0) ";
                    break;
                case NewCouponListFilterType.NotUsed://尚有憑證，有憑證，非取消訂單，大於0元 type 1
                    filter += " and " + ViewCouponOnlyListMain.Columns.ItemPrice + ">0 "
                            + " and ( "
                                      + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + "=0 "
                                + "or (" + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " >0 "
                                        + " and " + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.PartialCancel + " >0 "
                                   + ")"
                                + " ) "
                            + " and cast(" + ViewCouponOnlyListMain.Columns.BusinessHourDeliverTimeE + " As Date) >= cast(getdate() As Date) ";
                    break;
                case NewCouponListFilterType.Fami://全家優惠憑證待使用，須排除已使用、已過期的全家憑證 type 3
                    filter += " and " + ViewCouponOnlyListMain.Columns.ItemPrice + " =0 "
                            + " and " + ViewCouponOnlyListMain.Columns.RemainCount + " >0 "
                            + " and " + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " =0 "
                            + " and " + ViewCouponOnlyListMain.Columns.Status + " & " + (int)GroupOrderStatus.FamiDeal + " >0 "
                            + " and cast(" + ViewCouponOnlyListMain.Columns.BusinessHourDeliverTimeE + " As Date) >= cast(getdate() As Date) ";
                    break;
                case NewCouponListFilterType.CouponOnly://純憑證檔（排除活動、零元、全家、宅配、捐款）： type 4
                    filter += " and " + ViewCouponOnlyListMain.Columns.ItemPrice + ">0"
                            + " and " + ViewCouponOnlyListMain.Columns.Status + " & " + (int)GroupOrderStatus.FamiDeal + " =0 "
                            + " and ( "
                                   + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " =0 "
                                + " or ("
                                        + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " >0 "
                                        + " and " + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.PartialCancel + " >0 "
                                   + " ) "
                                + " ) ";
                    break;
                case NewCouponListFilterType.Event://活動與抽獎, 0元好康 , 不包含全家 新光 type 6
                    filter += " and " + ViewCouponOnlyListMain.Columns.ItemPrice + " =0 "
                            + " and " + ViewCouponOnlyListMain.Columns.Status + " & " + (int)GroupOrderStatus.FamiDeal + " =0 "
                            + " and " + ViewCouponOnlyListMain.Columns.Status + " & " + (int)GroupOrderStatus.SKMDeal + " =0 "
                            + " and " + ViewCouponOnlyListMain.Columns.BusinessHourDeliverTimeE + " is not NULL";
                    break;
                case NewCouponListFilterType.Cancel://取消訂單 type 7
                    filter += " and (" + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " >0 "
                                + " and " + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.PartialCancel + " =0 ) ";
                    break;
                case NewCouponListFilterType.GroupCoupon:
                    //商品券 憑證、成單、成套販售
                    {
                        var filterList = new List<string>
                        {
                            string.Format(" and {0}>0 ",ViewCouponOnlyListMain.Columns.TotalCount),
                            string.Format(" and {0} = {1} ",ViewCouponOnlyListMain.Columns.DeliveryType,(int)DeliveryType.ToShop),
                            string.Format(" and {0}&{1} >0 ",ViewCouponOnlyListMain.Columns.BusinessHourStatus,(int)BusinessHourStatus.GroupCoupon),
                            string.Format(" and {0}&{1}>0 ",ViewCouponOnlyListMain.Columns.OrderStatus,(int)OrderStatus.Complete),
                        };

                        filter += string.Join("", filterList);
                    }
                    break;
                case NewCouponListFilterType.GroupCouponReady:
                    //商品券待使用 剩餘數量、憑證、成單、成套販售、有效其間
                    {
                        var filterList = new List<string>
                        {
                            string.Format(" and {0}>0 ",ViewCouponOnlyListMain.Columns.RemainCount),
                            string.Format(" and {0}>0 ",ViewCouponOnlyListMain.Columns.TotalCount),
                            string.Format(" and {0} = {1} ",ViewCouponOnlyListMain.Columns.DeliveryType,(int)DeliveryType.ToShop),
                            string.Format(" and {0}&{1} >0 ",ViewCouponOnlyListMain.Columns.BusinessHourStatus,(int)BusinessHourStatus.GroupCoupon),
                            string.Format(" and {0}&{1}>0 ",ViewCouponOnlyListMain.Columns.OrderStatus,(int)OrderStatus.Complete),
                            string.Format(" and {0} >= '{1:yyyy/MM/dd HH:mm:ss}'"
                            ,ViewCouponOnlyListMain.Columns.BusinessHourDeliverTimeE,DateTime.Now),
                        };

                        filter += string.Join("", filterList);
                    }
                    break;
                case NewCouponListFilterType.SKM://新光
                    filter += " and " + ViewCouponOnlyListMain.Columns.ItemPrice + "=0 "
                            + " and " + ViewCouponOnlyListMain.Columns.RemainCount + ">0 "
                            + " and " + ViewCouponOnlyListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " =0 "
                            + " and " + ViewCouponOnlyListMain.Columns.Status + "&" + (int)GroupOrderStatus.SKMDeal + ">0 ";
                    break;
                default:
                    break;
            }
            return filter;
        }

        /// <summary>
        /// [v2.0] 比較ViewCouponListMain資料，確認是否為NewCouponListFilterType type
        /// </summary>
        /// <param name="main"></param>
        /// <param name="filterType"></param>
        /// <returns></returns>
        public static bool ViewCouponListMainIsFilterType(ViewCouponListMain main, NewCouponListFilterType filterType)
        {
            DateTime sysDate = DateTime.Now;
            DeliveryType dealDeliveryType = main.DeliveryType == null
                                                ? DeliveryType.ToShop
                                                : (DeliveryType)main.DeliveryType.Value;

            switch (filterType)
            {
                case NewCouponListFilterType.NotUsed:
                    if ((main.RemainCount > 0 || (main.RemainCount == 0 && main.TotalCount == 0)) && //可使用憑證數>0
                       main.ItemPrice > 0 &&
                       dealDeliveryType != DeliveryType.ToHouse &&
                       (main.Status & (int)GroupOrderStatus.FamiDeal) == 0 &&
                       ((main.OrderStatus & (int)OrderStatus.Cancel) == 0 ||
                        ((main.OrderStatus & (int)OrderStatus.Cancel) > 0 &&
                        (main.OrderStatus & (int)OrderStatus.PartialCancel) > 0)) &&
                        !PponDealApiManager.IsPastFinalExpireDate(main) &&
                        !main.IsDepositCoffee) //未逾期
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.NotShipped:
                    //宅配未出貨 宅配出貨資料由20130701之後的訂單才開始統計
                    if (dealDeliveryType == DeliveryType.ToHouse &&
                        main.CreateTime >= DateTime.ParseExact("2013-07-01", "yyyy-MM-dd", null) &&
                        (main.OrderStatus & (int)OrderStatus.Cancel) == 0 &&
                        (main.Status & (int)GroupOrderStatus.KindDeal) == 0 &&
                        !(main.ShipTime.HasValue && main.ShipTime <= DateTime.Now))
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.Fami:
                    if (main.ItemPrice == 0 &&
                        (main.OrderStatus & (int)OrderStatus.Cancel) == 0 &&
                        (main.Status & (int)GroupOrderStatus.FamiDeal) > 0 &&
                        main.RemainCount > 0 &&
                        !PponDealApiManager.IsPastFinalExpireDate(main)) //未過期
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.CouponOnly:
                    if (main.ItemPrice > 0 &&
                        dealDeliveryType != DeliveryType.ToHouse &&
                        (main.Status & (int)GroupOrderStatus.FamiDeal) == 0 &&
                        ((main.OrderStatus & (int)OrderStatus.Cancel) == 0 ||
                        ((main.OrderStatus & (int)OrderStatus.Cancel) > 0 && (main.OrderStatus & (int)OrderStatus.PartialCancel) > 0)) &&
                        !main.IsDepositCoffee)
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.Product:
                    if (dealDeliveryType == DeliveryType.ToHouse &&
                        (main.OrderStatus & (int)OrderStatus.Cancel) == 0 &&
                        (main.Status & (int)GroupOrderStatus.KindDeal) == 0 &&
                        (main.Status & (int)GroupOrderStatus.FamiDeal) == 0)
                    {
                        if (config.EnabledExchangeProcess)
                        {
                            return (main.ExchangeStatus != (int)OrderLogStatus.ExchangeProcessing &&
                                    main.ExchangeStatus != (int)OrderLogStatus.SendToSeller);
                        }
                        return true;
                    }
                    break;
                case NewCouponListFilterType.Event:
                    if (main.ItemPrice == 0 &&
                        (main.OrderStatus & (int)OrderStatus.Cancel) == 0 &&
                        (main.Status & (int)GroupOrderStatus.FamiDeal) == 0 &&
                        (main.Status & (int)GroupOrderStatus.SKMDeal) == 0 &&
                        main.PrintCount == 0 &&
                        main.BusinessHourDeliverTimeE != null)
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.Cancel:
                    if (((main.OrderStatus & (int)OrderStatus.Cancel) > 0 && (main.OrderStatus & (int)OrderStatus.PartialCancel) == 0) ||
                         ((main.OrderStatus & (int)OrderStatus.Cancel) > 0 && (main.OrderStatus & (int)OrderStatus.PartialCancel) > 0 &&
                         main.RemainCount == 0) && !main.IsDepositCoffee)
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.OverdueNotUsed:
                    if (PponDealApiManager.IsPastFinalExpireDate(main) && //已逾期 
                        main.ItemPrice > 0 &&
                        (main.RemainCount > 0 || (main.RemainCount == 0 && main.TotalCount == 0)) &&
                        dealDeliveryType != DeliveryType.ToHouse &&
                        (main.Status & (int)GroupOrderStatus.FamiDeal) == 0 &&
                        ((main.OrderStatus & (int)OrderStatus.Cancel) == 0 ||
                        ((main.OrderStatus & (int)OrderStatus.Cancel) > 0 && (main.OrderStatus & (int)OrderStatus.PartialCancel) > 0)) &&
                        !main.IsDepositCoffee)
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.GroupCoupon:
                    if (main.TotalCount > 0 &&
                        dealDeliveryType == DeliveryType.ToShop &&
                        (main.BusinessHourStatus & (int)BusinessHourStatus.GroupCoupon) > 0 &&
                        (main.OrderStatus & (int)OrderStatus.Complete) > 0 &&
                        (main.Status & (int)GroupOrderStatus.FamiDeal) == 0 && //非全家
                        (main.Status & (int)GroupOrderStatus.HiLifeDeal) == 0 && //非萊爾富
                        !main.IsDepositCoffee) //非丹堤寄杯
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.GroupCouponReady:
                    if (main.RemainCount > 0 && main.TotalCount > 0 &&
                        dealDeliveryType == DeliveryType.ToShop &&
                        (main.BusinessHourStatus & (int)BusinessHourStatus.GroupCoupon) > 0 &&
                        (main.OrderStatus & (int)OrderStatus.Complete) > 0 &&
                        !PponDealApiManager.IsPastFinalExpireDate(main) && //未過期
                        (main.Status & (int)GroupOrderStatus.FamiDeal) == 0 && //非全家
                        (main.Status & (int)GroupOrderStatus.HiLifeDeal) == 0 && //非萊爾富
                        !main.IsDepositCoffee) //非丹堤寄杯
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.GroupCouponNotUsed:
                    if (PponDealApiManager.IsPastFinalExpireDate(main) && //已逾期
                        main.RemainCount > 0 && main.TotalCount > 0 &&
                        dealDeliveryType == DeliveryType.ToShop &&
                        (main.BusinessHourStatus & (int)BusinessHourStatus.GroupCoupon) > 0 &&
                        (main.OrderStatus & (int)OrderStatus.Complete) > 0 &&
                        (main.Status & (int)GroupOrderStatus.FamiDeal) == 0 && //非全家
                        (main.Status & (int)GroupOrderStatus.HiLifeDeal) == 0 && //非萊爾富
                        !main.IsDepositCoffee) //非丹堤寄杯
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.SKM:
                    if (main.ItemPrice == 0 &&
                        (main.OrderStatus & (int)OrderStatus.Cancel) == 0 &&
                        main.RemainCount > 0 &&
                        (main.Status & (int)GroupOrderStatus.SKMDeal) > 0)
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.FamiCoffee: //只有全家寄杯
                    if (main.ItemPrice > 0 &&
                        (main.BusinessHourStatus & (int)BusinessHourStatus.GroupCoupon) > 0 &&
                        (main.Status & (int)GroupOrderStatus.FamiDeal) > 0)
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.DepositCoffee: //所有咖啡寄杯都在這裡
                    if (main.IsDepositCoffee ||
                        (main.ItemPrice > 0 &&
                        (main.BusinessHourStatus & (int)BusinessHourStatus.GroupCoupon) > 0 &&
                        (main.Status & (int)GroupOrderStatus.FamiDeal) > 0))
                    {
                        return true;
                    }
                    break;
                case NewCouponListFilterType.Exchanging:
                    if (dealDeliveryType == DeliveryType.ToHouse &&
                        (main.OrderStatus & (int)OrderStatus.Cancel) == 0 &&
                        (main.OrderStatus & (int)OrderStatus.Complete) > 0 &&
                        (main.ExchangeStatus == (int)OrderLogStatus.ExchangeProcessing ||
                         main.ExchangeStatus == (int)OrderLogStatus.SendToSeller))
                    {
                        return true;
                    }
                    break;
            }
            return false;
        }

        /// <summary>
        /// [v1.0] 比較ViewCouponListMain資料，確認是否為filterType type
        /// </summary>
        /// <param name="main"></param>
        /// <param name="filterType"></param>
        /// <returns></returns>
        public static bool ViewCouponListMainIsFilterType(ViewCouponListMain main, CouponListFilterType filterType)
        {
            DateTime sysDate = DateTime.Now;
            DeliveryType dealDeliveryType = main.DeliveryType == null
                                                ? DeliveryType.ToShop
                                                : (DeliveryType)main.DeliveryType.Value;
            decimal slug = 0;
            if (!decimal.TryParse(main.Slug, out slug))
            {
                slug = 0;
            }

            switch (filterType)
            {
                case CouponListFilterType.Expired://使用期限已過，有憑證，已結檔，非取消訂單::ViewCouponListMain.Columns.Department + "<4
                    if (main.BusinessHourDeliverTimeE < sysDate &&
                        dealDeliveryType != DeliveryType.ToHouse &&
                        main.BusinessHourOrderTimeE <= sysDate &&
                          (main.OrderStatus & (int)OrderStatus.Cancel) == 0)
                    {
                        return true;
                    }
                    break;

                case CouponListFilterType.NotExpired://使用期限未過，有憑證，已結檔，非取消訂單
                    if (main.BusinessHourDeliverTimeE >= sysDate && dealDeliveryType != DeliveryType.ToHouse &&
                        main.BusinessHourOrderTimeE <= sysDate && (main.OrderStatus & (int)OrderStatus.Cancel) == 0)
                    {
                        return true;
                    }
                    break;

                case CouponListFilterType.NotUsed://尚有憑證，有憑證，非取消訂單，大於0元
                    if ((main.RemainCount > 0 || (main.RemainCount == 0 && main.TotalCount == 0)) &&
                        main.ItemPrice > 0 && dealDeliveryType != DeliveryType.ToHouse &&
                        ((main.OrderStatus & (int)OrderStatus.Cancel) == 0 ||
                        ((main.OrderStatus & (int)OrderStatus.Cancel) > 0 && (main.OrderStatus & (int)OrderStatus.PartialCancel) > 0)))
                    {
                        return true;
                    }
                    break;

                case CouponListFilterType.Used://憑證用完，有憑證，非取消訂單
                    if (main.RemainCount == 0 && main.TotalCount > 0 && dealDeliveryType != DeliveryType.ToHouse &&
                         (main.OrderStatus & (int)OrderStatus.Cancel) == 0)
                    {
                        return true;
                    }
                    break;

                case CouponListFilterType.Cancel://取消訂單
                    if (((main.OrderStatus & (int)OrderStatus.Cancel) > 0 && (main.OrderStatus & (int)OrderStatus.PartialCancel) == 0) ||
                         ((main.OrderStatus & (int)OrderStatus.Cancel) > 0 && (main.OrderStatus & (int)OrderStatus.PartialCancel) > 0 && main.RemainCount == 0)
                         )
                    {
                        return true;
                    }
                    break;

                case CouponListFilterType.Product://宅配商品 無憑證，非取消單 :::ViewCouponListMain.Columns.Department + ">3，並過濾公益檔次 :::sam 取消已結檔判定
                    if (dealDeliveryType == DeliveryType.ToHouse && (main.OrderStatus & (int)OrderStatus.Cancel) == 0 &&
                        (main.Status & (int)GroupOrderStatus.KindDeal) == 0)
                    {
                        return true;
                    }
                    break;

                case CouponListFilterType.NotCompleted://未完成付款;包括逾期未付款與等待繼續付款 (ATM且非取消單且未完成單)
                    if ((main.OrderStatus & (int)OrderStatus.ATMOrder) > 0 && (main.OrderStatus & (int)OrderStatus.Cancel) == 0 && (main.OrderStatus & (int)OrderStatus.Complete) == 0)
                    {
                        return true;
                    }
                    break;

                case CouponListFilterType.Event://優惠活動;包含全家優惠憑證、抽獎好康)]： 包含 0元好康 、全家專區檔次(排除已過期且未使用的訂單，保留已使用的 (訂單金額為0且未過期)
                    if (main.ItemPrice == 0 && (main.OrderStatus & (int)OrderStatus.Cancel) == 0)
                    {
                        bool famiDisable = false;
                        //排除全家優惠不顯得的部分
                        if ((main.Status & (int)GroupOrderStatus.FamiDeal) > 0 && main.PrintCount == 0 &&
                            main.BusinessHourDeliverTimeE != null &&
                            main.BusinessHourDeliverTimeE.Value.AddDays(1) < sysDate)
                        {
                            famiDisable = true;
                        }

                        if (!famiDisable)
                        {
                            return true;
                        }
                    }
                    break;

                case CouponListFilterType.Kind://公益檔次
                    if ((main.OrderStatus & (int)OrderStatus.Cancel) == 0 && (main.Status & (int)GroupOrderStatus.KindDeal) > 0)
                    {
                        return true;
                    }
                    break;
            }
            return false;
        }

        #endregion

        /// <summary>
        /// [反向核銷] 查可核銷訂單數量
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static int ViewCouponListMainGetCountBySeller(int userId, Guid sellerGuid)
        {
            return mp.GetCouponListMainCount(userId, sellerGuid, GetFilterStringForViewCouponListMainByCanVerifyOrder());
        }

        /// <summary>
        /// [反向核銷] 查可核銷訂單
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="uniqueId"></param>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static ViewCouponListMainCollection ViewCouponListMainGetListBySeller(int pageStart, int pageLength, int uniqueId, Guid sellerGuid)
        {
            return mp.GetCouponListMainListByUserAndSellerGuid(pageStart, pageLength, uniqueId, sellerGuid,
                GetFilterStringForViewCouponListMainByCanVerifyOrder());
        }

        /// <summary>
        /// [反向核銷] 查可核銷憑證訂單FilterString 
        /// </summary>
        /// <returns></returns>
        protected static string GetFilterStringForViewCouponListMainByCanVerifyOrder()
        {
            var filterList = new List<string>
                {
                    string.Format(" and {0} = {1}", ViewCouponListMain.Columns.DeliveryType, (int)DeliveryType.ToShop),
                    string.Format(" and ({0} & {1} = 0 or {0} & {1} > 0 and {2} & {3} > 0) ",
                        ViewCouponListMain.Columns.OrderStatus, (int)OrderStatus.Cancel,
                        ViewCouponListMain.Columns.OrderStatus, (int)OrderStatus.PartialCancel),
                    string.Format(" and isnull({0}, {1}) >= cast(getdate() As Date)",
                        ViewCouponListMain.Columns.ChangedExpireDate, ViewCouponListMain.Columns.BusinessHourDeliverTimeE),
                    string.Format(" and {0} in (-1, {1})", ViewCouponListMain.Columns.ReturnStatus, (int)ProgressStatus.Canceled),
                };
            return String.Join("", filterList);
        }

        #endregion 會員訂單列表相關 ViewCouponLintMain

        #region 邀請送紅利

        /// <summary>
        /// 檢查是否符合發放紅利的規則，若符合則發放紅利，並回傳true
        /// </summary>
        /// <param name="referenceId"></param>
        /// <param name="o"></param>
        /// <param name="theDeal"></param>
        /// <param name="user_name"></param>
        public static bool ReferenceBonusCheckAndPay(string referenceId, Order o, ViewPponDeal theDeal, string user_name)
        {
            int referenceMemberBounsPointReward = 500;
            //有傳入referenceId，且為第一次購買，購買金額大於要發放的紅利金額
            if ((!string.IsNullOrEmpty(referenceId)) &&
                ((o.OrderStatus & (int)OrderStatus.FirstOrder) == (int)OrderStatus.FirstOrder) &&
                theDeal.ItemPrice * 10 > referenceMemberBounsPointReward) // 紅利點數與金額為1:10
            {
                //拆解referenceId 取得 外部ID 與 外部來源編號
                string[] referenceDatas = referenceId.Split('|');
                string externalId;
                int iSingleSign;
                try
                {
                    externalId = referenceDatas[1];
                    iSingleSign = int.Parse(referenceDatas[0]);
                }
                catch (Exception e)
                {
                    logger.Error(string.Format("傳入referenceId錯誤{0}", referenceId), e);
                    return false;
                }

                //取得推薦人的member_user_id
                int referenceUserId = mp.MemberLinkGetByExternalId(externalId, (SingleSignOnSource)iSingleSign).UserId;
                if ((referenceUserId == 0))
                {
                    return false;
                }

                //若有查到推薦人資料 且 推薦人不為本人，則進行紅利發放作業
                if (referenceUserId != o.UserId)
                {
                    MemberPromotionDeposit referenceBonus = new MemberPromotionDeposit();
                    referenceBonus.StartTime = GetOrderBonusStartDate(theDeal);
                    referenceBonus.ExpireTime = GetOrderBonusExpireDate(theDeal);
                    referenceBonus.PromotionValue = referenceMemberBounsPointReward;
                    referenceBonus.CreateTime = DateTime.Now;
                    referenceBonus.UserId = referenceUserId;// GetUniqueId(referenceUserName);
                    referenceBonus.OrderGuid = o.Guid;
                    referenceBonus.Action = I18N.Phrase.ReferenceBonusActionForReferencedUser;
                    referenceBonus.CreateId = user_name;

                    mp.MemberPromotionDepositSet(referenceBonus);
                    return true;
                }
            }
            return false;
        }

        #endregion 邀請送紅利

        #region 會員收藏功能

        public static bool MemberSetCollectDealExpireMessage(int memberUniqueId, bool isEnable)
        {
            try
            {
                mp.MemberSetCollectDealExpireMessage(memberUniqueId, isEnable);
                return isEnable;
            }
            catch (Exception ex)
            {
                logger.Info("MemberCollectDeal: Message(" + ex.Message + ") " + ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// check deal is collected.
        /// </summary>
        /// <param name="memberUniqueId"></param>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public static bool MemberCollectDealIsCollected(int memberUniqueId, Guid businessHourGuid)
        {
            return mp.MemberCollectDealIsCollected(memberUniqueId, businessHourGuid);
        }

        /// <summary>
        /// 加入收藏
        /// </summary>
        /// <param name="mcd"></param>
        /// <returns></returns>
        public static bool MemberCollectDealSave(MemberCollectDeal mcd)
        {
            try
            {
                mp.MemberCollectDealSave(mcd);
                return true;
            }
            catch (Exception ex)
            {
                logger.Info("MemberCollectDeal: Message(" + ex.Message + ") " + ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// 移除收藏
        /// </summary>
        /// <param name="memberUniqueId"></param>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public static bool MemberCollectDealRemove(int memberUniqueId, Guid businessHourGuid)
        {
            try
            {
                mp.MemberCollectDealRemove(memberUniqueId, businessHourGuid);
                return true;
            }
            catch (Exception ex)
            {
                logger.Info("MemberCollectDeal: Message(" + ex.Message + ") " + ex.StackTrace);
                return false;
            }
        }

        public static bool MemberCollectDealSetForApp(int userId, Guid bid, bool isCollecting, bool notice, byte collectType = (byte)MemberCollectDealType.Coupon)
        {
            bool result = false;
            int cityId = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
            //搜尋檔次資料確定cityId的值
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            if (string.IsNullOrEmpty(vpd.CityList) == false)
            {
                List<int> cityList = new JsonSerializer().Deserialize<List<int>>(vpd.CityList);
                if (cityList != null && cityList.Count > 0)
                {
                    cityId = cityList.First();
                }
            }

            if (isCollecting && bid != Guid.Empty)
            {
                MemberCollectDeal mcd = mp.MemberCollectDealGet(userId, bid);
                if (!mcd.IsLoaded)
                {
                    mcd.MemberUniqueId = userId;
                    mcd.BusinessHourGuid = bid;
                    mcd.CollectTime = DateTime.Now;
                    mcd.CityId = cityId;
                    mcd.CollectType = collectType;
                }
                mcd.CollectStatus = (byte)MemberCollectDealStatus.Collected;
                mcd.LastAccessTime = DateTime.Now;
                mcd.AppNotice = notice;
                result = MemberCollectDealSave(mcd);
            }
            else
            {
                result = MemberCollectDealRemove(userId, bid);
            }
            return result;
        }

        public static List<Guid> GetMemberCollectionDeal(int userId)
        {
            DateTime baseDate = PponDealHelper.GetTodayBaseDate();
            return mp.MemberCollectDealGetOnlineDealGuids(userId, baseDate.AddDays(-1), baseDate.AddDays(1));
        }

        #region 寄送收藏到期通知

        /// <summary>
        /// 將到期通知
        /// </summary>
        public static void SendCollectDealExpireMessage(double expireHours)
        {
            var query = ViewPponDealManager.DefaultManager.ViewPponDealGetList().Where(x => x.BusinessHourOrderTimeE > DateTime.Now
                && x.BusinessHourOrderTimeE.Subtract(DateTime.Now).TotalHours < expireHours
                && x.OrderTotalLimit > x.OrderedQuantity).ToList();

            foreach (ViewPponDeal vpd in query)
            {
                MemberCollection mc = mp.MemberGetListByCollectDealBid(vpd.BusinessHourGuid, MemberCollectDealExpireLogSendType.Expire);
                foreach (Member member in mc)
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                        mail.To.Add(member.UserEmail);
                        mail.Subject = string.Format("優惠即將結束，快把握機會：{0}", (string.IsNullOrEmpty(vpd.AppTitle) ? vpd.CouponUsage : vpd.AppTitle));

                        DealCollectSoldEnd template = TemplateFactory.Instance().GetTemplate<DealCollectSoldEnd>();
                        template.SiteUrl = config.SiteUrl;
                        template.SiteServiceUrl = config.SiteServiceUrl;
                        template.MemberName = member.DisplayName;
                        template.CouponUsage = (string.IsNullOrEmpty(vpd.AppTitle) ? vpd.CouponUsage : vpd.AppTitle);
                        template.EventTitle = vpd.EventTitle;
                        template.DealImg = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).Skip(1).DefaultIfEmpty(string.Format("{0}/Themes/PCweb/images/ppon-M1_pic.jpg", config.SiteUrl)).First();
                        template.DealDiscount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice);
                        template.ItemPrice = vpd.ItemPrice.ToString("###,###,###,##0");
                        template.OrigPrice = vpd.ItemOrigPrice.ToString("###,###,###,##0");
                        template.DealUrl = string.Format("{0}/{1}/cpa-Collect-End", config.SiteUrl, vpd.BusinessHourGuid.ToString());
                        template.OrderTimeEnd = vpd.BusinessHourOrderTimeE.ToString("yyyy-MM-dd HH:mm");
                        mail.Body = template.ToString();
                        mail.IsBodyHtml = true;
                        PostMan.Instance().Send(mail, SendPriorityType.Immediate);
                        MemberCollectDealExpireLogSave(member.UniqueId, vpd.BusinessHourGuid, MemberCollectDealExpireLogSendType.Expire);

                        //logger.InfoFormat("[收藏]將到期通知 To:{0} Subject:{1}", member.UserEmail, mail.Subject);
                    }
                }
            }
        }

        /// <summary>
        /// 將售完通知
        /// </summary>
        public static void SendCollectDealSoldOutMessage()
        {
            var query = ViewPponDealManager.DefaultManager.ViewPponDealGetList()
                .Where(x => (x.OrderedQuantity < x.OrderTotalLimit
                    && x.OrderedQuantity >= (Convert.ToInt32(x.OrderTotalLimit) * 0.9))).ToList();

            foreach (ViewPponDeal vpd in query)
            {
                MemberCollection mc = mp.MemberGetListByCollectDealBid(vpd.BusinessHourGuid, MemberCollectDealExpireLogSendType.SoldOut);
                foreach (Member member in mc)
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                        mail.To.Add(member.UserEmail);
                        mail.Subject = string.Format("太熱賣了！即將售完：{0}", (string.IsNullOrEmpty(vpd.AppTitle) ? vpd.CouponUsage : vpd.AppTitle));

                        DealCollectSoldOut template = TemplateFactory.Instance().GetTemplate<DealCollectSoldOut>();
                        template.SiteUrl = config.SiteUrl;
                        template.SiteServiceUrl = config.SiteServiceUrl;
                        template.MemberName = member.DisplayName;
                        template.CouponUsage = (string.IsNullOrEmpty(vpd.AppTitle) ? vpd.CouponUsage : vpd.AppTitle);
                        template.EventTitle = vpd.EventTitle;
                        template.DealImg = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).Skip(1).DefaultIfEmpty(string.Format("{0}/Themes/PCweb/images/ppon-M1_pic.jpg", config.SiteUrl)).First();
                        template.DealDiscount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice);
                        template.ItemPrice = vpd.ItemPrice.ToString("###,###,###,##0");
                        template.OrigPrice = vpd.ItemOrigPrice.ToString("###,###,###,##0");
                        template.SaleCount = vpd.GetAdjustedOrderedQuantity().Quantity.ToString("###,###,###,##0");
                        template.DealUrl = string.Format("{0}/{1}/cpa-Collect-SoldOut", config.SiteUrl, vpd.BusinessHourGuid.ToString());
                        mail.Body = template.ToString();
                        mail.IsBodyHtml = true;
                        PostMan.Instance().Send(mail, SendPriorityType.Immediate);
                        MemberCollectDealExpireLogSave(member.UniqueId, vpd.BusinessHourGuid, MemberCollectDealExpireLogSendType.SoldOut);

                        //logger.InfoFormat("[收藏]即將售完 To:{0} Subject:{1}", member.UserEmail, mail.Subject);
                    }
                }
            }
        }

        private static void MemberCollectDealExpireLogSave(int memberUniqueId, Guid BusinessHourGuid, MemberCollectDealExpireLogSendType type)
        {
            try
            {
                MemberCollectDealExpireLog log = new MemberCollectDealExpireLog();
                log.BusinessHourGuid = BusinessHourGuid;
                log.MemberUniqueId = memberUniqueId;
                log.SendType = (byte)type;
                log.SendTime = DateTime.Now;
                mp.MemberCollectDealExpireLogSave(log);
            }
            catch (Exception ex)
            {
                logger.Info("MemberCollectDealExpireLogSave: Message(" + ex.Message + ") " + ex.StackTrace);
            }
        }

        #endregion 寄送收藏到期通知

        #endregion 會員收藏功能

        #region 會員登入、MemberLink、手機綁定、AccountAudit

        public static void LogChangeEmail(int userId, string userName, string oldEmail)
        {
            mp.AccountAuditSet(userId, userName, Helper.GetClientIP(), AccountAuditAction.ChangeEmail, true,
                               string.Format("更換Email, 原 {0}", oldEmail), Helper.GetOrderFromType());
        }

        public static void LogAccountAudit(int userId, string userName, AccountAuditAction auditAction, bool success, string msg)
        {
            mp.AccountAuditSet(userId, userName, Helper.GetClientIP(), auditAction, success, msg, Helper.GetOrderFromType());
        }

        public static void AddMemberLink(int userId, SingleSignOnSource singleSignOnSource, string extId,
            string createId)
        {
            MemberLink link = new MemberLink
            {
                ExternalOrg = (int)singleSignOnSource,
                ExternalUserId = extId,
                UserId = userId,
                CreateId = createId,
                CreateTime = DateTime.Now
            };
            mp.MemberLinkSet(link);
        }

        public static bool BindMemberLink(int userId, SingleSignOnSource singleSignOnSource, string extId, out string errorMessage)
        {
            MemberLink ml = mp.MemberLinkGet(userId, singleSignOnSource);
            if (ml.IsLoaded)
            {
                errorMessage = string.Format("操作失敗，你己經綁定過{0}帳號了", singleSignOnSource.ToString());
                return false;
            }
            MemberLink ml2 = mp.MemberLinkGetByExternalId(extId, singleSignOnSource);
            if (ml2.IsLoaded)
            {
                errorMessage = string.Format("操作失敗，您想串接的{0}帳號{1}己經綁定在其他會員身上", extId, singleSignOnSource.ToString());
                return false;
            }
            try
            {
                string userName = GetUserName(userId);
                AddMemberLink(userId, singleSignOnSource, extId, userName);
                mp.AccountAuditSet(userId, userName, Helper.GetClientIP(), AccountAuditAction.BindMemberLink, true,
                    singleSignOnSource.ToString(), Helper.GetOrderFromType());
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
                errorMessage = "系統繁忙中，請稍後再試";
                return false;
            }
            errorMessage = null;
            return true;
        }

        public static bool BindMemberLinkReCreate(int userId, SingleSignOnSource singleSignOnSource, string extId, out string errorMessage)
        {
            try
            {
                mp.MemberLinkDelete(userId, singleSignOnSource);
                string userName = GetUserName(userId);
                AddMemberLink(userId, singleSignOnSource, extId, userName);
                mp.AccountAuditSet(userId, userName, Helper.GetClientIP(), AccountAuditAction.BindMemberLink, true,
                    singleSignOnSource.ToString(), Helper.GetOrderFromType());
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
                errorMessage = "系統繁忙中，請稍後再試";
                return false;
            }
            errorMessage = null;
            return true;
        }

        public static bool UnbindMemberLink(int userId, SingleSignOnSource singleSignOnSource, out string errorMessage)
        {
            try
            {
                MemberLinkCollection mlCol = mp.MemberLinkGetList(userId);
                if (mlCol.Count < 2)
                {
                    errorMessage = "因為只有一個串接方式，不能進行取消串接的動作";
                    return false;
                }
                if (singleSignOnSource == SingleSignOnSource.ContactDigitalIntegration)
                {
                    errorMessage = "不能取消17Life的登入方式";
                    return false;
                }
                MemberLink ml = mp.MemberLinkGet(userId, singleSignOnSource);
                if (ml.IsLoaded)
                {
                    mp.MemberLinkDelete(userId, singleSignOnSource);
                }
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
                errorMessage = "系統繁忙中，請稍後再試";
                return false;
            }
            errorMessage = null;
            return true;
        }

        public static bool SetMemberStatusFlag(int uniqueId, bool mark, MemberStatusFlag flag)
        {
            Member mem = mp.MemberGet(uniqueId);
            mem.Status = (int)Helper.SetFlag(mark, mem.Status, flag);
            mp.MemberSet(mem);
            return true;
        }

        public static bool IsReadyToBuy(string UserName, MemberBasicCheckType checkType)
        {
            //目前所有type的判斷都相同
            //檢查有無該使用者
            Member mem = mp.MemberGet(UserName);
            if (!mem.IsLoaded)
            {
                return false;
            }

            if (null == mem.BuildingGuid)
            {
                return false;
            }

            return true;
        }

        public static string CheckIsPEZUserID(string userName)
        {
            MemberLinkCollection mlc = mp.MemberLinkGetList(userName);
            foreach (MemberLink ml in mlc)
            {
                if (ml.ExternalOrg == (int)SingleSignOnSource.PayEasy)
                {
                    return ml.ExternalUserId;
                }
            }

            return string.Empty;
        }

        public static string CheckIsPEZUserID(int userId)
        {
            MemberLinkCollection mlc = mp.MemberLinkGetList(userId);
            foreach (MemberLink ml in mlc)
            {
                if (ml.ExternalOrg == (int)SingleSignOnSource.PayEasy)
                {
                    return ml.ExternalUserId;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// 手機會員綁定可否移除
        /// </summary>
        /// <param name="mm"></param>
        /// <returns></returns>
        public static bool IsMobileMemberRemovable(MobileMember mm)
        {
            MemberLinkCollection mlCol = mp.MemberLinkGetList(mm.UserId);
            return mlCol.Count > 1;
        }

        public static bool DeleteMobileMember(MobileMember mm)
        {
            if (mm.IsLoaded == false)
            {
                return false;
            }
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    mp.MemberLinkDelete(mm.UserId, SingleSignOnSource.Mobile17Life);
                    mp.MobileMemberDelete(mm.UserId);
                    mp.MobileAuthInfoDelete(mm.UserId);
                    trans.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("UnbindMobile error, mobile number=" + mm.MobileNumber, ex);
            }
            return false;

        }

        public static bool UnbindMobile(string mobileNumber)
        {
            MobileMember mm = mp.MobileMemberGet(mobileNumber);
            return UnbindMobile(mm);
        }

        public static bool UnbindMobile(MobileMember mm)
        {
            if (mm.IsLoaded == false)
            {
                return false;
            }
            try
            {
                string errorMessage;
                if (UnbindMemberLink(mm.UserId, SingleSignOnSource.Mobile17Life, out errorMessage))
                {
                    mp.MobileMemberDelete(mm.MobileNumber);
                    return true;
                }
            }
            catch (Exception ex)
            {
                logger.Error("UnbindMobile error, mobile number=" + mm.MobileNumber, ex);
            }
            return false;
        }

        public static void SetMemberActive(int userId)
        {
            Member mem = mp.MemberGet(userId);
            SetMemberActive(mem);
        }

        public static void SetMemberActive(Member mem)
        {
            mem.Status = mem.Status | (int)MemberStatusFlag.Active17Life;
            UpgradeGuestMember(mem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <param name="force">true 不檢查會員資料是否存在，忘記密碼需要啟用</param>
        /// <param name="needSetPassword"></param>
        /// <param name="resetPasswordKey"></param>
        /// <returns></returns>
        public static MemberValidateMobileCodeReply ValidateMobileCode(int userId, string mobile, string code,
            bool force, out bool needSetPassword, out string resetPasswordKey)
        {
            resetPasswordKey = null;
            needSetPassword = false;
            Member mem = null;
            MobileMember mm = null;
            try
            {
                mem = mp.MemberGet(userId);
                mm = mp.MobileMemberGet(userId);
                if (mem.IsLoaded == false)
                {
                    return MemberValidateMobileCodeReply.DataError;
                }
                if (force == false && mm.MobileNumber == mobile && mm.Status == (int)MobileMemberStatusType.Activated)
                {
                    return MemberValidateMobileCodeReply.ActivedAndSkip;
                }
                MobileAuthInfo mai = mp.MobileAuthInfoGet(userId, mobile);
                if (mai.IsLoaded == false)
                {
                    return MemberValidateMobileCodeReply.DataError;
                }
                if (mai.Code != code)
                {
                    return MemberValidateMobileCodeReply.CodeError;
                }
                if (DateTime.Now > mai.ValidTime)
                {
                    return MemberValidateMobileCodeReply.TimeError;
                }
                MobileMember mmTarget = MemberFacade.GetMobileMember(mobile);
                if (force == false && mmTarget.IsLoaded && Helper.IsFlagSet(mmTarget.Status, MobileMemberStatusType.Activated))
                {
                    return MemberValidateMobileCodeReply.MobileUsedByOtherMember;
                }
                if (mm.Status == (int)MobileMemberStatusType.None)
                {
                    if (mm.IsLoaded == false)
                    {
                        mm = new MobileMember
                        {
                            UserId = userId,
                            MobileNumber = mobile,
                            LastLoginDate = SqlDateTime.MinValue.Value,
                            LastPasswordChangedDate = SqlDateTime.MinValue.Value,
                            IsLockedOut = false,
                            CreateTime = DateTime.Now
                        };
                    }
                    //開通17Life帳號
                    SetMobileMemberNumberChecked(mem, mm);
                }
                else
                {
                    //更換手機號碼
                    mm.MobileNumber = mobile;
                    mm.ResetPasswordKey = Guid.NewGuid().ToString().Substring(0, 8);
                    mp.MobileMemberSet(mm);
                    //同時變更連絡電話
                    mem.Mobile = mobile;
                    mp.MemberSet(mem);
                }

                needSetPassword = mm.Status == (int)MobileMemberStatusType.NumberChecked;
                resetPasswordKey = mm.ResetPasswordKey;

                mp.MobileAuthInfoDelete(mm.UserId);

                mp.AccountAuditSet(mem.UserName, Helper.GetClientIP(), AccountAuditAction.ActiveMobile, true,
                    mm.MobileNumber, Helper.GetOrderFromType());
                return MemberValidateMobileCodeReply.Success;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("ValidateMobileCode 發生錯誤, userId={0}, mobile={1}, code={2}, ex={3}",
                    userId, mobile, code, ex);
                if (mem != null && mm != null)
                {
                    mp.AccountAuditSet(mem.UserName, Helper.GetClientIP(), AccountAuditAction.ActiveMobile, false,
                        mm.MobileNumber, Helper.GetOrderFromType());
                }
                return MemberValidateMobileCodeReply.OtherError;
            }
        }

        /// <summary>
        /// 手機會員的狀態直接改成啟用
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="mm"></param>
        /// <param name="creator"></param>
        public static void SetMobileMemberActive(Member mem, MobileMember mm, string creator)
        {
            SetMobileMemberNumberChecked(mem, mm);
            mm.Status = (int)MobileMemberStatusType.Activated;
            mp.MobileMemberSet(mm);
            mp.MobileAuthInfoDelete(mm.UserId);
            mp.AccountAuditSet(mem.UserName, Helper.GetClientIP(), AccountAuditAction.ActiveMobile, true,
                "actived by " + creator, Helper.GetOrderFromType());
        }

        private static void SetMobileMemberNumberChecked(Member mem, MobileMember mm)
        {
            MemberLink mlMobile17 = ProviderFactory.Instance().GetProvider<IMemberProvider>()
                .MemberLinkGet(mem.UniqueId, SingleSignOnSource.Mobile17Life);
            if (mlMobile17.IsLoaded == false)
            {
                //ResetMemberPassword(mem.UserName, null, code, ResetPasswordReason.Bind17Link, true);
                AddMemberLink(mem.UniqueId,
                    SingleSignOnSource.Mobile17Life, mem.UniqueId.ToString(), mem.UserName);
            }

            MemberLink ml17Life = ProviderFactory.Instance().GetProvider<IMemberProvider>()
                .MemberLinkGet(mem.UniqueId, SingleSignOnSource.ContactDigitalIntegration);
            if (ml17Life.IsLoaded)
            {
                mm.Password = mem.Password;
                mm.PasswordFormat = mem.PasswordFormat;
                mm.PasswordSalt = mem.PasswordSalt;

                mm.Status = (int)MobileMemberStatusType.Activated;
                mm.ActivedDate = DateTime.Now;

                mp.MobileMemberSet(mm);
            }
            else
            {
                mm.Status = (int)MobileMemberStatusType.NumberChecked;
                mm.ResetPasswordKey = Guid.NewGuid().ToString("N").Substring(0, 8);
                mp.MobileMemberSet(mm);
            }
        }

        public static void AddDefaultMemberAuthInfo(int userId, string userName)
        {
            MemberAuthInfo mai = mp.MemberAuthInfoGet(userId);
            if (mai.IsLoaded == false)
            {
                mai = new MemberAuthInfo
                {
                    UniqueId = userId,
                    Email = userName,
                    AuthDate = DateTime.Now
                };
                mp.MemberAuthInfoSet(mai);
            }
        }

        public static MemberAuthInfo GetOrAddMemberAuthInfo(int userId, string userName)
        {
            MemberAuthInfo mai = mp.MemberAuthInfoGet(userId);
            if (mai.IsLoaded == false)
            {
                mai = new MemberAuthInfo
                {
                    UniqueId = userId,
                    Email = userName,
                    AuthDate = DateTime.Now
                };
                mp.MemberAuthInfoSet(mai);
            }
            return mai;
        }

        public static MemberAuthInfo MemberAuthInfoGet(int uniqueId)
        {
            return mp.MemberAuthInfoGet(uniqueId);
        }

        /// <summary>
        /// 取消 會員在app上的登入token ，強迫會員重新登入
        /// </summary>
        /// <param name="userId"></param>
        public static void CancelAppLoginToken(int userId)
        {
            mp.LoginTicketSetExpirationTimeAsNowByUser(userId);
        }

        /// <summary>
        /// 取得手機號碼驗證資料
        /// 綁定時使用
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static MobileAuthInfo GetValidOrNewMobileAuthInfo(int userId, string mobile)
        {
            MobileAuthInfo mai = mp.MobileAuthInfoGet(userId, mobile);
            if (mai.IsLoaded == false)
            {
                mai.QueryTimes = 0;
                mai.Code = random.Next(100000, 999999).ToString();
                mai.Mobile = mobile;
                mai.UserId = userId;
            }
            else if (mai.ValidTime < DateTime.Now)
            {
                mai.Code = random.Next(100000, 999999).ToString();
            }
            return mai;
        }

        /// <summary>
        /// 認換手機簡訊是否未達限制可發送
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="sendLimit"></param>
        /// <param name="unbanDays"></param>
        /// <param name="sentCount"></param>
        /// <param name="nextCanSendSmsTime"></param>
        /// <returns></returns>
        public static bool CheckMobileAuthSmsCanSend(string mobile, int sendLimit, int unbanDays,
            out int sentCount, out DateTime nextCanSendSmsTime)
        {
            List<MobileAuthInfo> authInfos = mp.MobileAuthInfoGetList(mobile);
            DateTime maxPreviousSendDate = DateTime.MinValue;
            sentCount = 0;
            foreach (var mai in authInfos)
            {
                if (maxPreviousSendDate < mai.ValidTime.Date)
                {
                    maxPreviousSendDate = mai.ValidTime.Date;
                }
                if ((DateTime.Now.Date - mai.ValidTime.Date).TotalDays < unbanDays)
                {
                    sentCount += mai.QueryTimes;
                }
            }
            if (sentCount >= sendLimit)
            {
                nextCanSendSmsTime = maxPreviousSendDate.AddDays(unbanDays);
                return false;
            }
            nextCanSendSmsTime = DateTime.Now;
            return true;
        }

        public static void SetMobileAuthInfo(MobileAuthInfo mai)
        {
            mp.MobileAuthInfoSet(mai);
        }

        public static MemberSetPasswordReply SetMobleMemberPassword(int uniqueId, string password)
        {
            if (uniqueId == 0)
            {
                return MemberSetPasswordReply.DataError;
            }
            if (string.IsNullOrEmpty(password))
            {
                return MemberSetPasswordReply.EmptyError;
            }
            Member mem = mp.MemberGet(uniqueId);
            if (mem.IsLoaded == false)
            {
                return MemberSetPasswordReply.DataError;
            }
            MobileMember mm = mp.MobileMemberGet(uniqueId);
            if (mm.IsLoaded == false)
            {
                return MemberSetPasswordReply.DataError;
            }
            if (RegExRules.CheckPassword(password) == false)
            {
                return MemberSetPasswordReply.PasswordFormatError;
            }
            try
            {
                MembershipUser user = Membership.GetUser(mem.UserName);
                if (user == null)
                {
                    return MemberSetPasswordReply.DataError;
                }
                user.ResetPassword(password);
                mm.Status = (int)MobileMemberStatusType.Activated;
                mm.ResetPasswordKey = null;
                mm.IsLockedOut = false;
                mp.MobileMemberSet(mm);

                UnlockMember(mem);

                mp.AccountAuditSet(mem.UserName, Helper.GetClientIP(), AccountAuditAction.ChangePassword, true,
                    ResetPasswordReason.MemberForgot.ToString(), Helper.GetOrderFromType());

                return MemberSetPasswordReply.Success;
            }
            catch (Exception ex)
            {
                logger.Error("set password fail, userd = " + uniqueId, ex);
                return MemberSetPasswordReply.OtherError;
            }
        }

        public static void SetMobileMemberResetPasswordKeyEmpty(MobileMember mm)
        {
            mm.ResetPasswordKey = null;
            mp.MobileMemberSet(mm);
        }

        public static MemberActiveStatus GetMemberActiveStatus(string userName)
        {
            Member m = mp.MemberGet(userName);
            return GetMemberActiveStatus(m);
        }

        public static MemberActiveStatus GetMemberActiveStatus(Member m)
        {
            if (m.IsLoaded == false)
            {
                return MemberActiveStatus.UserNotFound;
            }
            bool is17Member = mp.MemberLinkGet(m.UniqueId, SingleSignOnSource.ContactDigitalIntegration).IsLoaded;
            if (is17Member == false)
            {
                return MemberActiveStatus.NotLife17Member;
            }
            if (Helper.IsFlagSet(m.Status, MemberStatusFlag.Active17Life))
            {
                return MemberActiveStatus.Active;
            }
            return MemberActiveStatus.Inactive;
        }

        /// <summary>
        /// 未認證17會員，進行訪客購買時，會轉成訪客會員，目的讓他走訪客會員的機制，1.購買完寄認證信 2.認證設密碼
        /// </summary>
        /// <param name="mem"></param>
        public static void DowngradeGuestMember(Member mem)
        {
            if (GetMemberActiveStatus(mem) == MemberActiveStatus.Inactive)
            {
                if (mem.IsGuest == false)
                {
                    mem.IsGuest = true;
                    mem.UpgradedTime = null;
                    mp.MemberSet(mem);
                }

            }
        }
        public static void UpgradeGuestMember(Member mem)
        {
            if (mem.IsGuest)
            {
                mem.IsGuest = false;
                mem.UpgradedTime = DateTime.Now;
            }
            mp.MemberSet(mem);
        }

        public static bool IsMemberMobileAuthed(string userName)
        {
            MobileMember mm = GetMobileMember(userName);
            bool isMobileAuthed = false;
            if (mm != null && mm.IsLoaded)
            {
                if (mm.Status == 2) //完成手機認證
                {
                    isMobileAuthed = true;
                }
            }
            return isMobileAuthed;
        }

        #endregion

        #region 台新儲值支付

        /// <summary>
        /// 取得台新儲值支付的點數
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="taishinPayLinked"></param>
        /// <returns></returns>
        public static decimal GetTaisihinPayCashPoint(int userId, out bool taishinPayLinked)
        {
            taishinPayLinked = false;
            if (config.EnableTaisihinPayCashPoint == false)
            {
                return 0;
            }
            MemberToken mt = mp.MemberTokenGet(userId, ThirdPartyPayment.TaishinPay);
            if (mt.IsLoaded == false)
            {
                return 0;
            }
            taishinPayLinked = true;
            string errMsg;
            var model = TaishinPayUtility.GetTaishinMemberInfo(userId, mt.AccessToken, out errMsg);
            if (model == null)
            {
                logger.WarnFormat("台新儲值支付，使用無效的token 查詢 , {0} / {1}, errMsg={2}", userId, mt.AccessToken, errMsg);
                return 0;
            }
            //taishinPayLinked = true;
            return model.AccountAmount;
        }

        public static string GetTaishinPayAccessToken(int userId)
        {
            MemberToken mt = mp.MemberTokenGet(userId, ThirdPartyPayment.TaishinPay);
            return mt.AccessToken;
        }

        public static string GetUserNameFromMemberLinkByExternalId(string externalId, SingleSignOnSource sourceOrg)
        {
            string userName = string.Empty;
            MemberLink memberLink = mp.MemberLinkGetByExternalId(externalId, sourceOrg);
            if (memberLink != null)
            {
                userName = GetUserName(memberLink.UserId);
            }
            return userName;
        }

        /// <summary>
        /// 解除綁定
        /// </summary>
        /// <param name="userId"></param>
        public static void UnbindTaishinPay(int userId)
        {
            string accessToken = GetTaishinPayAccessToken(userId);
            string memberUserName = GetUserName(userId);
            if (string.IsNullOrEmpty(accessToken) == false)
            {
                if (TaishinPayUtility.UnbindTaishinMember(accessToken, userId))
                {
                    mp.MemberTokenDelete(userId, ThirdPartyPayment.TaishinPay);
                    mp.AccountAuditSet(memberUserName, Helper.GetClientIP(), AccountAuditAction.TaishinPayUnbindUser, true, "解除綁定成功");
                }
                else
                {
                    mp.AccountAuditSet(memberUserName, Helper.GetClientIP(), AccountAuditAction.TaishinPayUnbindUser, false, "解除綁定失敗");
                }
            }
        }

        #endregion

        #region MGM

        public static bool MemberMsgSet(MemberMessage memberMsg)
        {
            return mp.MemberMsgSet(memberMsg);
        }

        public static MemberContact MemberContactGetByGuid(Guid guid)
        {
            return mp.MemberContactGetByGuid(guid);
        }

        /// <summary>
        /// 取得我的通訊錄
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static MemberContactsData GetMemberContactsDataById(int userId)
        {
            MemberContactsData memberContactData = new MemberContactsData
            {
                Contacts = new List<Contact>(),
                Groups = new List<Group>()
            };

            //朋友們資料
            //TODO:移除重複的朋友資料
            var data = mp.ViewMgmContactGroupGetById(userId);
            foreach (var item in data)
            {
                Contact ctc = new Contact
                {
                    Id = item.ContactId,
                    UserId = item.ContactUserId,
                    Name = item.ContactName,
                    NickName = item.ContactNickName,
                    Birthday = item.ContactBirthday,
                    Value = item.ContactValue,
                    GroupId = item.GroupId,
                    GroupName = item.GroupName
                };

                memberContactData.Contacts.Add(ctc);
            }

            memberContactData.Groups = data
                        .GroupBy(x => x.GroupId)
                        .Select(x => new Group
                        {
                            GroupId = x.First().GroupId,
                            GroupName = x.First().GroupName,
                            GroupNumber = x.Count(),
                            GroupImagePath = x.First().GroupImagePath,
                        }).ToList();

            memberContactData.Groups.RemoveAll(x => x.GroupId == null);

            return memberContactData;
        }

        /// <summary>
        /// 新增通訊錄好友
        /// </summary>
        /// <returns></returns>
        public static bool MemberContactSet(MemberContact memeberContact)
        {
            return mp.MemberContactSet(memeberContact);
        }

        /// <summary>
        /// 建立群組
        /// </summary>
        /// <returns></returns>
        public static bool MemberGroupSet(MemberGroup memeberGroup)
        {
            return mp.MemberGroupSet(memeberGroup);
        }

        /// <summary>
        /// 加入群組
        /// </summary>
        /// <returns></returns>
        public static bool MemberGroupSetByContact(int contactId, int groupId)
        {
            MemberContactGroup mContactGroup = new MemberContactGroup()
            {
                ContactId = contactId,
                GroupId = groupId
            };
            return mp.MemberContactGroupSet(mContactGroup);
        }

        #endregion

        #region 客服系統

        public static ServiceMessageCategoryCollection ServiceMessageCategoryGetListByParentId(int? parentId)
        {
            return mp.ServiceMessageCategoryCollectionGetListByParentId(parentId);
        }

        public static ServiceMessageCategory ServiceMessageCategoryGetByCategoryId(int cateId)
        {
            return mp.ServiceMessageCategoryGetByCategoryId(cateId);
        }

        #endregion

        #region 會員信用卡管理


        /// <summary>
        /// 判斷銀行是否可以分期
        /// </summary>
        /// <param name="bankid"></param>
        /// <returns></returns>
        private static string cacheBankIdByInstallment = "Installment";
        public static bool GetBankIdByInstallment(int bankid)
        {
            bool result = false;

            ObjectCache cache = MemoryCache.Default;
            List<int> bankIdList = cache[cacheBankIdByInstallment] as List<int>;

            if (bankIdList == null)
            {
                //取得可以分期的銀行資料
                bankIdList = op.CreditcardBankByInstallment();

                CacheItemPolicy policy = new CacheItemPolicy();

                //設定快取時間超過1小時候，回收快取
                policy.AbsoluteExpiration = DateTime.Now.AddHours(1);

                cache.Set(cacheBankIdByInstallment, bankIdList, policy);
            }

            if (bankIdList.Contains(bankid))
            {
                result = true;
            }

            return result;

        }

        public static MemberCreditCard GetMemberLastUsedCreditCardGuid(string userName)
        {
            var userId = GetUniqueId(userName);
            var cards = mp.MemberCreditCardGetList(userId);
            if (cards.Any())
            {
                return cards.ToList().OrderByDescending(x => x.LastUseTime).FirstOrDefault();
            }
            return null;
        }

        /// <summary>
        /// 查詢信用卡列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="bankId">限制銀行Id</param>
        /// <returns></returns>
        public static List<MemberCreditCardInfo> GetMemberCreditCardList(int userId, int bankId = 0)
        {
            var cardInfoList = new List<MemberCreditCardInfo>();
            var cards = mp.MemberCreditCardGetList(userId).OrderByDescending(x => x.LastUseTime).ThenByDescending(x => x.CreateTime).ToList();

            if (bankId != 0)
            {
                cards = cards.Where(x => x.BankId == bankId).ToList();
            }

            foreach (var card in cards)
            {
                var cardInfo = new MemberCreditCardInfo();
                cardInfo.CardGuid = card.Guid;
                cardInfo.CardName = card.CreditCardName;
                cardInfo.CardTail = card.CreditCardTail;
                cardInfo.ValidYear = card.CreditCardYear;
                cardInfo.ValidMonth = card.CreditCardMonth;
                cardInfo.CardType = card.CardType;
                cardInfo.BankId = card.BankId;
                cardInfo.BankName = CreditCardPremiumManager.GetCreditcardBankNameByBankId(card.BankId);
                cardInfo.IsLastUsed = false;
                cardInfo.IsInstallment = GetBankIdByInstallment(card.BankId);
                cardInfoList.Add(cardInfo);
            }

            if (cardInfoList.Any())
            {
                cardInfoList[0].IsLastUsed = true;
            }

            return cardInfoList;
        }
        /// <summary>
        /// 新增或異動記憶卡號
        /// cardGuid 是 Guid.Empty ，為新增
        /// </summary>
        /// <param name="cardGuid"></param>
        /// <param name="userName"></param>
        /// <param name="creditCardNo"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="creditCardName"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public static bool AddOrUpdateMemberCreditCard(Guid cardGuid, string userName, string creditCardNo, string year, string month, string creditCardName,
            out string errorMsg)
        {
            var isCheck = RegExRules.CheckCreditCardNumber(creditCardNo, config.CheckCreditCardEnabled, out errorMsg) &&
                          RegExRules.CheckCreditCardValidDate(year, month, out errorMsg);

            if (!isCheck)
            {
                return false;
            }

            mp.MemberCreditCardGet(Guid.Empty);
            MemberCreditCard mcc;
            if (cardGuid == Guid.Empty)
            {
                mcc = new MemberCreditCard();
                mcc.Guid = Guid.NewGuid();
                mcc.CreateTime = DateTime.Now;
            }
            else
            {
                mcc = mp.MemberCreditCardGet(cardGuid);
            }

            string salt = Guid.NewGuid().ToString("N").Substring(0, 16);
            mcc.UserId = GetUniqueId(userName);
            mcc.CreditCardInfo = EncryptCreditCardInfo(creditCardNo, salt);
            mcc.CreditCardTail = creditCardNo.Substring(creditCardNo.Length - 4, 4);
            mcc.CreditCardYear = year;
            mcc.CreditCardMonth = month;
            mcc.LastUseTime = DateTime.Now;
            mcc.EncryptSalt = salt;
            mcc.Bin = creditCardNo.Substring(0, 6);

            var bankName = string.Empty;
            var bankInfo = CreditCardPremiumManager.GetCreditCardBankInfo(creditCardNo);
            if (bankInfo.IsLoaded)
            {
                mcc.BankId = bankInfo.BankId;
                mcc.CardType = bankInfo.CardType ?? 0;
                bankName = bankInfo.BankName;
            }
            var isTaishinMall = ChannelFacade.GetOrderClassificationByHeaderToken() == AgentChannel.TaiShinMall;
            if (isTaishinMall && mcc.BankId != config.TaishinCreditCardBankId)
            {
                errorMsg = "限新增台新銀行信用卡";
                return false;
            }
            if (mcc.CardType == (int)CreditCardType.None)
            {
                mcc.CardType = (int)OrderFacade.GetCreditCardType(creditCardNo);
            }
            if (!string.IsNullOrEmpty(creditCardName))
            {
                mcc.CreditCardName = creditCardName;
            }
            else
            {
                mcc.CreditCardName = !string.IsNullOrEmpty(bankName) ? bankName : "信用卡";
            }

            var id = mp.MemberCreditCardSet(mcc);

            if (id <= 0)
            {
                errorMsg = "新增失敗";
                return false;
            }

            return true;
        }

        public static bool DeleteMemberCreditCard(string viewUserName, Guid eData, out string errorMessage)
        {
            var userId = GetUniqueId(viewUserName);
            var result = mp.MemberCreditCardDeleteByGuidUserId(eData, userId);
            if (result > 0)
            {
                errorMessage = null;
                return true;
            }

            errorMessage = "刪除失敗";
            return false;
        }

        public static string EncryptCreditCardInfo(string creditCardInfo, string salt)
        {
            byte[] saltBytes = Encoding.UTF8.GetBytes(salt);
            string strEncryptCardInfo = new Security(SymmetricCryptoServiceProvider.AES, null, saltBytes).Encrypt(creditCardInfo); // string had been base64 encoding

            return strEncryptCardInfo;
        }

        public static string DecryptCreditCardInfo(string creditCardInfo, string salt)
        {
            byte[] saltBytes = Encoding.UTF8.GetBytes(salt);
            string strDecryptCardInfo = new Security(SymmetricCryptoServiceProvider.AES, null, saltBytes).Decrypt(creditCardInfo);

            return strDecryptCardInfo;
        }

        /// <summary>
        /// 取得預設信用卡名稱
        /// </summary>
        /// <param name="cardNo"></param>
        /// <returns></returns>
        public static string GetDefaultCarditCardName(string cardNo)
        {
            //同APP邏輯
            var bankInfo = CreditCardPremiumManager.GetCreditCardBankInfo(cardNo);
            return string.IsNullOrEmpty(bankInfo.BankName) ? "信用卡" : bankInfo.BankName;
        }


        /// <summary>
        /// 更新最後使用的信用卡
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static void LastUseCarditCard(Guid guid, int UserId)
        {
            mp.MemberCreditCardSetLastUsed(guid, UserId);
        }


        #endregion

        public static bool FnVbsFlattenedAclLiteGetList(string accountId, Guid merchandiseGuid, Guid storeGuid)
        {
            var view = mp.FnVbsFlattenedAclLiteGetList(accountId).AsEnumerable().Where(x => x.Field<Guid>("merchandise_guid") == merchandiseGuid && x.Field<Guid>("store_guid") == storeGuid);

            if (view.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static List<string> ResourceAclGetListByResourceGuid(List<Guid> resourceGuid)
        {
            List<string> resourceacl = new List<string>();
            resourceacl = mp.ResourceAclGetListByResourceGuidList(resourceGuid).Where(x => x.ResourceType == 1 || x.ResourceType == 2).GroupBy(x => x.AccountId).Select(x => x.Key).OrderBy(x => x).ToList();


            return resourceacl;
        }
        public static bool ResourceAclGetListByAccounntId(List<Guid> resourceGuid, string AccountId, int resourcetype)
        {
            var resourceacl = mp.ResourceAclGetListByResourceGuidList(resourceGuid).Where(x => x.ResourceType == resourcetype && x.AccountId == AccountId);
            if (resourceacl.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 傳送憑證擋到指定mail
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="sequenceNumList">憑證序號</param>
        /// <param name="email"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int SendOrderPponCouponToEMail(Guid orderGuid, List<string> sequenceNumList, string email, int userId)
        {
            int rtn = 0;
            //憑證資料
            CashTrustLogCollection couponList = mp.CashTrustLogGetList(null, CashTrustLog.OrderGuidColumn + "=" + orderGuid,
                                                                       CashTrustLog.UserIdColumn + "=" +
                                                                       userId);
            Member member = mp.MemberGet(userId);
            foreach (var sequenceNum in sequenceNumList)
            {
                List<CashTrustLog> matchCoupons = couponList.Where(x => x.CouponSequenceNumber == sequenceNum).ToList();
                if (matchCoupons.Count > 0)
                {
                    CashTrustLog trustlog = matchCoupons.First();
                    if (trustlog.CouponId != null)
                    {
                        try
                        {
                            EmailFacade.SendCouponToMember(email, member, trustlog.CouponId.Value);
                            rtn++;
                        }
                        catch (Exception ex)
                        {
                            logger.Error(string.Format("發送會員訂單憑證至EMAIL {0}", ex));
                        }
                    }
                }
            }
            return rtn;
        }
        public static void CashTrustLogStoreGuidSet(string[] couponsSequence, string OrderGuid, string StoreGuid)
        {
            Guid oid = Guid.Empty;
            Guid.TryParse(OrderGuid, out oid);
            Guid sid = Guid.Empty;
            Guid.TryParse(StoreGuid, out sid);

            CashTrustLogCollection ctls = mp.CashTrustLogGetListByOrderGuid(oid);

            foreach (CashTrustLog ctl in ctls)
            {
                if (couponsSequence.Contains(ctl.CouponSequenceNumber))
                {
                    if (ctl == null)
                    {
                        ctl.StoreGuid = sid;
                        mp.CashTrustLogSet(ctl);
                    }
                }
            }
        }
        /// <summary>
        /// 是否在beta功能測試人員名單
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool IsTesterForBetaFeature(int userId)
        {
            if (userId != 0 && config.MDealTester.Contains(userId.ToString()))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 匯入紅利金給會員
        /// 紅利金 10點 = 1元
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="bounsValue"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="actionName"></param>
        /// <param name="modifyId"></param>
        /// <param name="actionGroupGuid"></param>
        public static int DepositMemberPromotion(int userId, int bonusValue, DateTime startTime, DateTime endTime,
            string actionName, string modifyId, Guid? actionGroupGuid)
        {
            var log = new
            {
                userId,
                bonusValue,
                startTime,
                endTime,
                actionName,
                modifyId,
                actionGroupGuid
            };
            if (bonusValue % 10 != 0 || bonusValue < 50 || bonusValue > 1000)
            {
                logger.WarnFormat("DepositMemberPromotion 可匯入會員的紅利金額，為50至1000點，也就是5元到100元之間, args={0}",
                    Newtonsoft.Json.JsonConvert.SerializeObject(log));
                throw new Exception("匯入紅利金發生錯誤。");
            }
            if ((endTime - startTime).TotalDays < 1)
            {
                logger.WarnFormat("DepositMemberPromotion 可用日期至少需要一天, args={0}",
                    Newtonsoft.Json.JsonConvert.SerializeObject(log));
                throw new Exception("匯入紅利金發生錯誤。");
            }
            Member mem = mp.MemberGet(userId);
            if (mem.IsLoaded == false)
            {
                logger.WarnFormat("DepositMemberPromotion 會員找不到, args={0}",
                    Newtonsoft.Json.JsonConvert.SerializeObject(log));
                throw new Exception("匯入紅利金發生錯誤。");
            }
            var deposit = new MemberPromotionDeposit()
            {
                StartTime = startTime,
                ExpireTime = endTime,
                PromotionValue = bonusValue,
                CreateTime = DateTime.Now,
                Action = actionName,
                CreateId = modifyId,
                UserId = userId,
                Type = (int)MemberPromotionType.Bouns,
                ActionGroupGuid = actionGroupGuid
            };
            mp.MemberPromotionDepositSet(deposit);
            return deposit.Id;
        }

        /// <summary>
        /// 24H出貨通知，包含簡訊與推播，避開深夜與凌晨發送
        /// </summary>
        /// <param name="orderGuid"></param>
        public static void Send24HOrderShipmentNotification(Order od, string message)
        {
            if (config.EnableP24HPersonalNotification == false && IsTesterForBetaFeature(od.UserId) == false)
            {
                return;
            }
            if (od.IsLoaded == false)
            {
                return;
            }
            string title = "訂單出貨通知";

            NotificationFacade.PushMember24HOrderShipmentNotification(od.UserId, title, message, od.Guid);
            Member mem = mp.MemberGet(od.UserId);
            MobileMember mm = mp.MobileMemberGet(od.UserId);

            string phoneNumber = null;
            if (mm.IsLoaded)
            {
                phoneNumber = mm.MobileNumber;
            }
            else if (RegExRules.CheckMobile(mem.Mobile))
            {
                phoneNumber = mem.Mobile;
            }
            if (string.IsNullOrEmpty(phoneNumber) == false)
            {
                new SMS().QueueMessage(message, phoneNumber, mem.UniqueId.ToString(), 8, 22);
            }
        }

        public static bool IsValidPhoneCarrierId(string carrierId)
        {
            if (config.CheckPhoneCarrierValid != true)
            {
                //不檢查，直接回成功
                return true;
            }
            string apiUrl = config.SSLSiteUrl + "/WebService/EinvoiceService.asmx/CheckCarrid";
            string TxID = System.DateTime.Now.ToString("yyyyMMddHHmmss"); //暫無用途
            string targetUrl = string.Format(@"https://www-vc.einvoice.nat.gov.tw/BIZAPIVAN/biz?version=1.0&action=bcv&barCode={0}&TxID={1}&appId=EINV1201410300635", System.Web.HttpUtility.UrlEncode(carrierId), TxID);

            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(targetUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Timeout = 10000;

            string resp = "";
            // 取得回應資料
            using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
                {
                    resp = sr.ReadToEnd();
                }
            }
            Newtonsoft.Json.Linq.JObject jb = Newtonsoft.Json.JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(resp);
            string data = jb.Property("code").Value.ToString() == "200" ? jb.Property("isExist").Value.ToString() : string.Empty;

            return data.ToUpper() == "Y";
        }

        /// <summary>
        /// 匯入購物金，記錄信託
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="amount"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static bool ImportScash(int userId, int amount, string title)
        {
            try
            {
                Member m = mp.MemberGetbyUniqueId(userId);
                using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                {
                    //cashPointOrder
                    Guid cashPointOrderGuid = OrderFacade.MakeSCachOrder(
                    amount, amount, title, CashPointStatus.Approve,
                    userId, title, CashPointListType.Income, m.UserName, Guid.Empty);

                    //scashDeposit
                    bool invoiced = false;
                    int depositId = OrderFacade.NewScashDeposit(cashPointOrderGuid, amount, userId, Guid.Empty, title,
                        m.UserName, invoiced, OrderClassification.CashPointOrder);


                    //信託
                    UserCashTrustLog ctUser = new UserCashTrustLog();
                    ctUser.UserId = userId;
                    ctUser.Amount = amount;
                    ctUser.BankStatus = (int)TrustBankStatus.Initial;
                    ctUser.CreateTime = DateTime.Now;
                    ctUser.Message = title;
                    ctUser.TrustProvider = (int)TrustProvider.TaiShin;
                    ctUser.TrustId = default(Guid);
                    ctUser.Type = (int)UserTrustType.Buy;
                    ctUser.MarkNew();
                    mp.UserCashTrustLogSet(ctUser);
                    tx.Complete();
                }                   
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("ImportScash fail, ex=", ex);
                return false;
            }           
        }

        /// <summary>
        /// 收回購物金，記錄信託
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="amount"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static bool ExportScash(int userId, int amount, string message, string createId)
        {
            try
            {
                Member mem = GetMember(userId);
                if (mem.IsLoaded == false || userId == 0)
                {
                    throw new Exception("找不到使用者");
                }

                OrderFacade.ScashUsed(amount, userId, Guid.Empty,
                    message, createId, OrderClassification.Other);

                #region 信託紀錄

                UserCashTrustLog ctUser = new UserCashTrustLog();
                ctUser.UserId = userId;
                ctUser.Amount = (-1) * amount;
                ctUser.BankStatus = (int)TrustBankStatus.Initial;
                ctUser.CreateTime = DateTime.Now;
                ctUser.Message = message;
                ctUser.TrustProvider = (int)TrustProvider.TaiShin;
                ctUser.TrustId = Guid.Empty;
                ctUser.Type = (int)UserTrustType.Refund;
                ctUser.MarkNew();
                mp.UserCashTrustLogSet(ctUser);

                #endregion 信託紀錄

                CommonFacade.AddAudit(mem.UserName , AuditType.Member, message, createId, true);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("ExportScash fail, ex=", ex);
                return false;
            }
        }

        private static Random random = new Random();
    }
}

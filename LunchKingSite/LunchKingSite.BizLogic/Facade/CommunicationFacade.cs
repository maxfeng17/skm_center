﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using log4net;

namespace LunchKingSite.BizLogic.Facade
{
    public class CommunicationFacade
    {
        protected static IPponProvider pp;
        protected static ISysConfProvider cp;

        static CommunicationFacade()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            cp = ProviderFactory.Instance().GetConfig();
        }

        public static void SmsLogSetToQueue(Guid bid)
        {
            pp.SmsLogSetToQueue(bid);
        }

        public static SmsSystem GetSmsSystemByRatio()
        {
            try
            {
                int firstCount = Convert.ToInt16(cp.SmsRatio.Split(':')[0]);
                int secondCount = Convert.ToInt16(cp.SmsRatio.Split(':')[1]);
                int[] typePool = new int[firstCount + secondCount];

                for (int i = 0; i < firstCount; i++)
                {
                    typePool[i] = 0;
                }

                for (int i = 0; i < secondCount; i++)
                {
                    typePool[firstCount + i] = 1;
                }

                if (typePool[new Random().Next(typePool.Length)] == 0)
                {
                    return SmsSystem.Sybase;
                }
                else
                {
                    return SmsSystem.iTe2;
                }
            }
            catch (Exception)
            {
                return cp.SmsSystem;
            }
        }
    }


    public interface ISmsSender
    {
        void Send(string message, string[] phoneNumbers, SmsExtraInfo ExtraInfo, out SmsSendReply smsReply);
        void Send(string message, string phoneNumber, SmsExtraInfo ExtraInfo, out SmsSendReply smsReply);
    }

    public abstract class SmsSenderBase : ISmsSender
    {
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        public abstract void Send(string message, string[] phoneNumbers, SmsExtraInfo ExtraInfo, out SmsSendReply smsReply);

        public void Send(string message, string phoneNumber, SmsExtraInfo extraInfo, out SmsSendReply smsReply)
        {
            Send(message, new string[] { phoneNumber }, extraInfo, out smsReply);
        }

        /// <summary>
        /// 將傳入的手機號碼轉成 "前面帶國碼" 的格式. 
        /// </summary>
        /// <param name="phone">以reference方式傳入手機號碼</param>
        /// <param name="addPlus">是否要在手機號碼前面加上 "+" 號</param>
        /// <returns></returns>
        public static bool TransPhone(ref string phone, bool addPlus)
        {
            bool result = false;
            if (RegExRules.CheckMobile(phone))
            {
                if (addPlus)
                {
                    phone = string.Format("{0}{1}{2}",
                        I18N.Phrase.PlusMark, I18N.Phrase.CountryCodeTaiwan, phone.Substring(1));
                }
                else
                {
                    phone = string.Format("{0}{1}",
                        I18N.Phrase.CountryCodeTaiwan, phone.Substring(1));
                }
                result = true;
            }
            return result;
        }

        protected List<TaiwanPhoneNumber> GetTaiwanPhoneNumbers(IEnumerable<string> destPhones, out List<string> illegalPhoneNumbers, bool addplus = true)
        {
            illegalPhoneNumbers = new List<string>();
            List<TaiwanPhoneNumber> result = new List<TaiwanPhoneNumber>();
            foreach (string destPhone in destPhones)
            {
                string tmp = destPhone;
                if (TransPhone(ref tmp, addplus))
                {
                    result.Add(
                        new TaiwanPhoneNumber
                        {
                            OriginalNumber = destPhone,
                            TranslatedNumber = tmp
                        }
                    );
                }
                else
                {
                    illegalPhoneNumbers.Add(destPhone);
                }
            }
            return result;
        }

    }

    public class SmsExtraInfo
    {
        public int? CouponId { get; set; }
        public Guid? DealGuid { get; set; }
        public string SenderId { get; set; }
    }

    public class SybaseSender : SmsSenderBase
    {
        public override void Send(string message, string[] phoneNumbers, SmsExtraInfo ExtraInfo, out SmsSendReply smsReply)
        {
            smsReply = SybaseSmsSubmit(phoneNumbers, message);
        }

        private SmsSendReply SybaseSmsSubmit(string[] destPhones, string message)
        {
            List<string> illegalPhoneNumbers;
            List<TaiwanPhoneNumber> destTaiwanPhones = GetTaiwanPhoneNumbers(destPhones, out illegalPhoneNumbers);
            if (destTaiwanPhones.Count == 0)
            {
                return new SmsSendReply
                {
                    IllegalPhoneNumbers = illegalPhoneNumbers
                };
            }
            string smsBody = new SybaseSmsCommand
            {
                Message = message,
                Phone = string.Join(",", destTaiwanPhones.Select(t => t.TranslatedNumber).ToArray())
            }.ToString();

            string header = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("17life17623:svqONdLU"));
            Encoding encode = Encoding.GetEncoding("Big5");
            byte[] byteArray = Encoding.GetEncoding("Big5").GetBytes(smsBody);

            string resultCode;
            string resultText;
            string smsOrderId;
            int smsOrderCount;

            //先用主機進行寄發
            WebRequest request = WebRequest.Create(config.SybaseUrl);
            bool submitResult = SendSybaseSmsRequest(request, byteArray, header, encode,
                out resultCode, out resultText, out smsOrderId, out smsOrderCount);
            if (!submitResult)
            {
                //代表主機未發送成功, 嘗試用備援機寄發
                request = WebRequest.Create(config.SybaseSecondUrl);
                submitResult = SendSybaseSmsRequest(request, byteArray, header, encode,
                    out resultCode, out resultText, out smsOrderId, out smsOrderCount);
            }
            if (submitResult)
            {
                return new SmsSendReply
                {
                    OrderId = smsOrderId,
                    OrderCount = smsOrderCount,
                    PhoneNumbers = destTaiwanPhones,
                    IllegalPhoneNumbers = illegalPhoneNumbers
                };
            }
            return SmsSendReply.Empty;
        }

        private bool SendSybaseSmsRequest(WebRequest request, byte[] byteArray, string header, Encoding encode
            , out string resultCode, out string resultText, out string smsOrderId, out int smsOrderCount)
        {
            resultCode = "";
            resultText = "";
            smsOrderId = "";
            smsOrderCount = 1;

            string result = "";
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers.Add("Authorization", header);
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    resultCode = ((HttpWebResponse)response).StatusCode.ToString();
                    Stream respStream = response.GetResponseStream();
                    if (respStream != null)
                    {
                        StreamReader reader = new StreamReader(respStream, encode);

                        result = reader.ReadToEnd();
                        reader.Close();
                        respStream.Close();
                    }
                }

                if (resultCode == "OK")
                {
                    string[] ar = result.Split('\n');
                    resultText = ar[5];
                    return ExtractSmsOrderInfo(ar[6], out smsOrderId, out smsOrderCount);
                }
                else
                {
                    string[] ar = result.Split('\n');
                    if (ar.Length >= 7)
                    {
                        resultText = ar[5];
                        return ExtractSmsOrderInfo(ar[6], out smsOrderId, out smsOrderCount);
                    }
                    return false;
                }
            }
            catch (WebException ex)
            {
                try
                {
                    dataStream = ex.Response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream, encode);
                    resultText = reader.ReadToEnd();
                }
                catch
                {
                    resultText = "unknown web exception.";
                }
                return false;
            }
            catch (Exception ex)
            {
                resultText = ex.Message + Environment.NewLine + ex.StackTrace;
                return false;
            }
        }

        private bool ExtractSmsOrderInfo(string line, out string orderId, out int orderCount)
        {
            orderId = "";
            orderCount = 1;
            string result = line.Split('=')[1].TrimEnd("<br>");
            string[] parts = result.Split(",");
            if (parts.Length > 0)
            {
                orderId = parts[0];
                orderCount = parts.Length;
                return true;
            }
            return false;
        }

        protected class SybaseSmsCommand
        {
            public string Message { get; set; }
            public string Phone { get; set; }
            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("[MSISDN]");
                sb.Append("List=");
                sb.Append(Phone);
                sb.AppendLine();

                sb.AppendLine("[MESSAGE]");
                sb.Append("Text=");
                sb.Append(Message);
                sb.AppendLine();

                sb.AppendLine("[SETUP]");
                {
                    sb.Append("DCS=");
                    sb.Append("BIG5");
                    sb.AppendLine();

                    sb.Append("SplitText=");
                    sb.Append("YES");
                    sb.AppendLine();

                    sb.Append("ValidityPeriod=");
                    sb.Append("10m");
                    sb.AppendLine();

                    sb.Append("AckType=");
                    sb.Append("Message");
                    sb.AppendLine();

                    //string callbackUrl = Helper.CombineUrl("http://uni2.tw", "service/sybasecallback.ashx");
                    string callbackUrl = Helper.CombineUrl(config.SiteUrl, "service/sybasecallback.ashx");
                    sb.Append("AckReplyAddress=");
                    sb.Append(callbackUrl);
                    LogManager.GetLogger("sms").Debug("設定簡訊回呼網址: " + callbackUrl);
                    sb.AppendLine();

                    sb.Append("MobileNotification=");
                    sb.Append("YES");
                    sb.AppendLine();
                }
                sb.AppendLine("[END]");

                return sb.ToString();
            }
        }
    }

    public class S2TSender : SmsSenderBase
    {
        public override void Send(string message, string[] phoneNumbers, SmsExtraInfo ExtraInfo, out SmsSendReply smsReply)
        {
            smsReply = S2TSmsSubmit(phoneNumbers, message);
        }

        private SmsSendReply S2TSmsSubmit(string[] destPhones, string message)
        {
            List<string> illegalPhoneNumbers;
            List<TaiwanPhoneNumber> destTaiwanPhones = GetTaiwanPhoneNumbers(destPhones, out illegalPhoneNumbers, false);
            if (destTaiwanPhones.Count == 0)
            {
                return new SmsSendReply
                {
                    IllegalPhoneNumbers = illegalPhoneNumbers
                };
            }

            XDocument doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "true"),
                new XElement("SMSREQUEST",
                    new XElement("USERNAME", "85269171717"),
                    new XElement("PASSWORD", "Sim217Life"),
                    new XElement("ORGCODE", "17Life"),
                    new XElement("DATA",
                        new XElement("ITEM",
                            new XElement("SCHEDULE", "0"),
                            new XElement("MULTIPLE", (destPhones.Count() > 1) ? (int)SmsMultiple.Multiple : (int)SmsMultiple.Single),
                            new XElement("MSG", message),
                            destTaiwanPhones.Select(x => new XElement("PHONE", x.TranslatedNumber))
                        )
                    ),
                    new XElement("REMARK", "ppon")
                )
            );
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(doc.Root.ToString());

            try
            {
                string r = string.Empty;
                if (config.S2tServerPriority == "1st")
                {
                    S2tSms3.SMPPServicesPortTypeClient ws = new S2tSms3.SMPPServicesPortTypeClient();
                    r = ws.sendSMPP("<?xml version='1.0' encoding='utf-8'?>" + xdoc.InnerXml);
                }
                else
                {
                    S2tSms2.SMPPServicesPortTypeClient ws = new S2tSms2.SMPPServicesPortTypeClient();
                    r = ws.sendSMPP("<?xml version='1.0' encoding='utf-8'?>" + xdoc.InnerXml);
                }

                XmlDocument result = new XmlDocument();
                result.LoadXml(XDocument.Parse(r).Root.ToString());
                bool submitResult = result.GetElementsByTagName("RESPONSE")[0].InnerText == "0";
                if (submitResult)
                {
                    return new SmsSendReply
                    {
                        OrderId = result.GetElementsByTagName("UID")[0].InnerText,
                        OrderCount = 1,
                        PhoneNumbers = destTaiwanPhones,
                        IllegalPhoneNumbers = illegalPhoneNumbers
                    };
                }
                return SmsSendReply.Empty;
            }
            catch (Exception)
            {
                return SmsSendReply.Empty;
            }
        }
    }

    public class iTe2Sender : SmsSenderBase
    {
        public override void Send(string message, string[] phoneNumbers, SmsExtraInfo ExtraInfo, out SmsSendReply smsReply)
        {
            smsReply = iTe2SmsSubmit(phoneNumbers, message);
        }

        private SmsSendReply iTe2SmsSubmit(string[] destPhones, string message)
        {
            try
            {
                foreach (string phones in destPhones)
                {
                    string dstaddr = phones; //接收人之手機號碼。格式為: +886912345678(國際)或09123456789。
                    string user = "17life";	//發送帳號
                    string pass = Convert.ToBase64String(Encoding.UTF8.GetBytes("17life@2015"));  //發送密碼，需使用UTF-8編碼後轉成base64 String
                    string msg = message; //簡訊內容，需使用UTF-8編碼
                    //是否要使用預約簡訊發送格式，是: 請輸入時間 格式:yyyy/MM/dd HH:mm 否:為空字串
                    string schedule = "";
                    //簡訊發送截止時間(簡訊失敗時間 格式:yyyy/MM/dd HH:mm  簡訊發送止時間需大於簡訊系統時間10分鐘
                    string stoptime = "";
                    bool islong = Encoding.GetEncoding("big5").GetBytes(msg).Length > 70;
                    //是否使用長簡訊格式發送簡訊，0:一般簡訊(中文70英文160) 1:長簡訊模式(中文67一拆英半153一拆)

                    FpMsService.fpMsServiceSoapClient w = new FpMsService.fpMsServiceSoapClient();

                    string strResult = strResult = w.SendSMS(dstaddr, msg, user, pass, schedule, stoptime, islong);

                    XmlDocument result = new XmlDocument();
                    result.LoadXml(XDocument.Parse(strResult).Root.ToString());
                    bool submitResult = result.GetElementsByTagName("ERR")[0].InnerText == "0";

                    List<string> illegalPhoneNumbers;
                    List<TaiwanPhoneNumber> destTaiwanPhones = GetTaiwanPhoneNumbers(destPhones, out illegalPhoneNumbers, false);

                    if (submitResult)
                    {
                        return new SmsSendReply
                        {
                            OrderId = result.GetElementsByTagName("SEQ")[0].InnerText,
                            OrderCount = 1,
                            PhoneNumbers = destTaiwanPhones,
                            IllegalPhoneNumbers = illegalPhoneNumbers,
                        };
                    }
                }

                return SmsSendReply.Empty;
            }
            catch (Exception)
            {
                return SmsSendReply.Empty;
            }
        }
    }

    public class TaiwanPhoneNumber
    {
        public string OriginalNumber { get; set; }
        public string TranslatedNumber { get; set; }
    }

    public class SmsSendReply
    {
        public string OrderId { get; set; }
        public int OrderCount { get; set; }
        public List<TaiwanPhoneNumber> PhoneNumbers { get; set; }
        public List<string> IllegalPhoneNumbers { get; set; }

        private static SmsSendReply empty = new SmsSendReply
        {
            OrderCount = 1,
            OrderId = string.Empty
        };

        public static SmsSendReply Empty
        {
            get { return empty; }
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            SmsSendReply castObj = obj as SmsSendReply;
            if (castObj == null)
            {
                return false;
            }

            return string.Equals(OrderId, castObj.OrderId);

        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}

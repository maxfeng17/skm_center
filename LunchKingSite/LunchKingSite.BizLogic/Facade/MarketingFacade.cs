﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.MarketingEntities;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.BizLogic.Facade
{
    public class MarketingFacade
    {
        private static IMarketingProvider mkp = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static ISystemProvider ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        private static IPponEntityProvider ppe = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
        private static IMarketingEntityProvider marketingEntity = ProviderFactory.Instance().GetProvider<IMarketingEntityProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(MarketingFacade));

        /// <summary>
        /// 依CpaKey為條件在CpaTracking取得符合資料，選取資料最新的Rsrc跟本次的Rsrc不同，即新增
        /// </summary>
        /// <param name="cpaKey"></param>
        /// <param name="rsrc"></param>
        /// <param name="userId"></param>
        /// <param name="url"></param>
        public static void TryAddCpaTracking(string cpaKey, string rsrc, int userId = 0, string url = "")
        {
            CpaTracking tracking = mkp.CpaTrackingGetLast(cpaKey);
            if (tracking == null || tracking.Rsrc.Equals(rsrc, StringComparison.OrdinalIgnoreCase) == false)
            {
                tracking = new CpaTracking
                {
                    CpaKey = cpaKey,
                    CreateTime = DateTime.Now,
                    Rsrc = rsrc,
                    UserId = userId,
                    Url = url
                };
                mkp.CpaTrackingSet(tracking);
            }
        }

        /// <summary>
        /// 取得策展活動名稱
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="eventType"></param>
        /// <returns></returns>
        public static string GetCurationTitle(int eventId, int? eventType = null)
        {
            const string defaultTitle = "17Life活動";
            //折價券活動編號欄位是2016年10月才加的，從前直接發送的現券金，活動編號預設值都是0，就不要再進去查了。 (Ex:campaign_id = 4444)
            if (eventId == 0)
            {
                return defaultTitle;
            }
            var eventPromo = GetApiEventPromoOrBrandByIdFromCache(eventId, eventType, false);
            return eventPromo != null ? eventPromo.Title : defaultTitle; //若查不到給預設
        }

        /// <summary>
        /// 由DB取得[商品主題活動][策展1.0][策展2.0]分類及檔次
        /// </summary>
        /// <param name="id"></param>
        /// <param name="eventType"></param>
        /// <param name="isVerifiedAvailable">是否驗證有效活動</param>
        /// <returns></returns>
        public static ApiEventPromo GetApiEventPromoOrBrandByIdFromDB(int id, int? eventType = null, bool isVerifiedAvailable = true)
        {
            return eventType == (int)EventPromoEventType.CurationTwo
                    ? GetApiBrandByIdFromDB(id, isVerifiedAvailable)
                    : GetApiEventPromoByIdFromDB(id, isVerifiedAvailable);
        }

        /// <summary>
        /// 由Cache優先取得[商品主題活動][策展1.0][策展2.0]分類及檔次 (若Cache無資料再找DB)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="eventType"></param>
        /// <param name="isVerifiedAvailable"></param>
        /// <returns></returns>
        public static ApiEventPromo GetApiEventPromoOrBrandByIdFromCache(int id, int? eventType = null, bool isVerifiedAvailable = true)
        {
            const int minutes = 30;
            const int extendExpireMinutes = 20;

            var cacheKey = (eventType == (int)EventPromoEventType.CurationTwo ? "B" : "E") + id;

            CacheDataStatus dataStatus;
            ApiEventPromo apiEventPromo = MemoryCache2.Get<ApiEventPromo>(cacheKey, out dataStatus); //資料過期、資料不存在、資料就是空資料
            if (dataStatus == CacheDataStatus.None)
            {
                apiEventPromo = eventType == (int)EventPromoEventType.CurationTwo
                    ? GetApiBrandByIdFromDB(id, isVerifiedAvailable)
                    : GetApiEventPromoByIdFromDB(id, isVerifiedAvailable);

                MemoryCache2.Set(cacheKey, apiEventPromo, minutes, extendExpireMinutes); //緩存30分鍾，但20~30分鍾取資料會觸發更新
            }
            else if (dataStatus == CacheDataStatus.Expired && config.MemoryCache2Mode == 2)
            {
                Func<ApiEventPromo> func;

                if (eventType == (int)EventPromoEventType.CurationTwo)
                {
                    func = delegate { return GetApiBrandByIdFromDB(id, isVerifiedAvailable); };
                }
                else
                {
                    func = delegate { return GetApiEventPromoByIdFromDB(id, isVerifiedAvailable); };
                }

                MemoryCache2.SetAsync(cacheKey, func, minutes, extendExpireMinutes);
            }
            return apiEventPromo;
        }

        /// <summary>
        /// 由DB取得[商品主題活動][策展1.0]分類及檔次
        /// </summary>
        /// <param name="eventPromoId"></param>
        /// <param name="isVerifiedAvailable">是否驗證有效活動</param>
        /// <returns></returns>
        public static ApiEventPromo GetApiEventPromoByIdFromDB(int eventPromoId, bool isVerifiedAvailable = true)
        {
            ViewEventPromoCollection promoItems = pp.ViewEventPromoGetList(eventPromoId, status: true);
            var promo = pp.GetEventPromo(eventPromoId);

            logger.DebugFormat("eventPromoId: {0} {1}/nd:{2}/sd:{3}/ed:{4}/show:{5}/status:{6} - {7}",
                eventPromoId, promo.IsLoaded,
                DateTime.Now.Date, promo.StartDate.Date, promo.EndDate.Date,
                promo.ShowInApp, promo.Status,
                promoItems.Count);

            if (promo.IsLoaded == false)
            {
                return null;
            }

            if (isVerifiedAvailable)
            {
                if (promo.Status == false ||  //後台設定此活動關閉
                    DateTime.Now < promo.StartDate || DateTime.Now > promo.EndDate ||
                    promo.ShowInApp == false)
                {
                    return null;
                }
            }

            ApiEventPromo apiEventPromo = new ApiEventPromo
            {
                Type = (EventPromoEventType)promo.EventType,
                EventId = eventPromoId,
                BannerType = promo.BannerType,
                Title = promo.Title,
                ChannelImageUrl = ImageFacade.GetEventImageUrl(promo.AppBannerImage), //頻道banner
                ImageUrl = ImageFacade.GetEventImageUrl(promo.AppPromoImage),        //首頁banner
                MobileMainPicUrl = ImageFacade.GetEventMobileImageUrl(promo.MobileMainPic), //策展內頁banner
                ShareUrl = PromotionFacade.GetCurationLink(promo),
                Description = promo.Description,
                Categories = new List<ApiEventPromoCategory>(),
                StartDate = promo.StartDate,
                EndDate = promo.EndDate,
                Status = promo.Status,
                DiscountList = new List<int>(),
                RelayImageUrl = string.Empty
            };
            SetApiEventPromoCategoriesAndDeals(promoItems, apiEventPromo);
            return apiEventPromo;
        }

        /// <summary>
        /// 由DB取得[策展2.0]分類及檔次
        /// </summary>
        /// <param name="brandId"></param>
        /// <param name="isVerifiedAvailable">是否驗證有效活動</param>
        /// <returns></returns>
        public static ApiEventPromo GetApiBrandByIdFromDB(int brandId, bool isVerifiedAvailable = true)
        {
            var brandItems = pp.ViewBrandCollectionByBrandId(brandId);
            var brandItemCategories = pp.GetBrandItemCategoryByBrandId(brandId).ToDictionary(t => t.Id, t => t);
            var brand = pp.GetBrand(brandId);

            logger.DebugFormat("brandId:{0} {1}/nd:{2}/sd:{3}/ed:{4}/show:{5}/status:{6} - {7}",
                brandId, brand.IsLoaded,
                DateTime.Now.Date, brand.StartTime.Date, brand.EndTime.Date,
                brand.ShowInApp, brand.Status,
                brandItems.Count);

            if (brand.IsLoaded == false)
            {
                return null;
            }

            if (isVerifiedAvailable)
            {
                if (brand.Status == false ||  //後台設定此活動關閉
                    DateTime.Now < brand.StartTime || DateTime.Now > brand.EndTime ||
                    brand.ShowInApp == false)
                {
                    return null;
                }
            }

            var discountList = GetDiscountListFromStringToList(brand.DiscountList);

            //過渡期程式，之後需移除
            string tempMobileMainPic = string.IsNullOrEmpty(brand.MobileMainPic) ? string.Empty : ImageFacade.GetHtmlFirstSrc(brand.MobileMainPic);
            if (tempMobileMainPic == string.Empty)
            {
                tempMobileMainPic = brand.MobileMainPic;
            }

            ApiEventPromo apiEventPromo = new ApiEventPromo
            {
                EventId = brandId,
                Type = EventPromoEventType.CurationTwo,
                Title = brand.BrandName,
                BannerType = brand.BannerType,
                ImageUrl = ImageFacade.GetEventImageUrl(brand.AppPromoImage),   //首頁banner
                Description = brand.Description ?? string.Empty,
                ChannelImageUrl = ImageFacade.GetEventImageUrl(brand.AppBannerImage), //頻道banner
                MobileMainPicUrl = ImageFacade.GetMediaPath(tempMobileMainPic, MediaType.DealPromoImage), //策展內頁banner
                ShareUrl = PromotionFacade.GetCurationTwoLink(brand),
                TotalCount = 0,
                Categories = new List<ApiEventPromoCategory>(),
                StartDate = brand.StartTime,
                EndDate = brand.EndTime,
                Status = brand.Status,
                DiscountList = discountList,
                RelayImageUrl = discountList.Any()
                    ? ImageFacade.GetMediaPath(brand.MobileRelayImage, MediaType.DealPromoImage)
                    : string.Empty
            };

            SetApiBrandCategoriesAndDeals(brandItems, apiEventPromo, brandItemCategories);
            return apiEventPromo;
        }

        public static void MarkMemberReferencesDeleted(List<ReferrerDeleteItem> items)
        {
            foreach (ReferrerDeleteItem item in items)
            {
                var mr = mkp.MemberReferralGet(item.Id);
                if (mr.IsLoaded )
                {
                    mr.IsDeleted = true;
                    mkp.MemberReferralSet(mr);
                }
            }
        }

        public static void UpdateMemberReferences(List<ReferrerUpdateItem> items)
        {
            foreach (ReferrerUpdateItem item in items)
            {
                var mr = mkp.MemberReferralGet(item.Id);
                if (mr.IsLoaded)
                {
                    mr.Name = item.Name;
                    mkp.MemberReferralSet(mr);
                }
            }
        }

        public static void UpdateCampaigns(List<CampaignUpdateItem> updateModels, string modifyId)
        {
            if (updateModels == null || updateModels.Count == 0)
            {
                return;
            }
            Dictionary<Guid, MemberReferral> referrals = new Dictionary<Guid, MemberReferral>();
            foreach (var item in updateModels)
            {
                if (item.ReferralGuid == Guid.Empty || string.IsNullOrWhiteSpace(item.Name) || string.IsNullOrEmpty(item.Code))
                {
                    continue;
                }
                MemberReferral referral;
                if (referrals.ContainsKey(item.ReferralGuid) == false)
                {
                    referral = mkp.MemberReferralGet(item.ReferralGuid);
                    if (referral.IsLoaded == false)
                    {
                        return;
                    }
                    referrals.Add(item.ReferralGuid, referral);
                }
                else
                {
                    referral = referrals[item.ReferralGuid];
                }                

                var campaign = mkp.ReferralCampaignGetByReferralGuidAndCode(item.ReferralGuid, item.Code);                
                if (campaign.IsLoaded == false)
                {
                    return;
                }
                campaign.ModifyId = modifyId;
                campaign.ModifyTime = DateTime.Now;
                campaign.Code = item.Code;
                campaign.Rsrc = string.Format("{0}_{1}", referral.Code, item.Code);
                campaign.Name = item.Name;
                campaign.SessionDuration = item.Duration;
                campaign.OneShotAction = item.OneShot;
                campaign.IsExternal = item.IsExternal;
                mkp.ReferralCampaignSet(campaign);
            }
        }

        public static void MarkReferralCampaignsDeleted(List<CampaignDeleteItem> deleteItems)
        {
            foreach (CampaignDeleteItem item in deleteItems)
            {
                ReferralCampaign rc = mkp.ReferralCampaignGet(item.Id);
                if (rc.IsLoaded)
                {
                    rc.IsDeleted = true;
                    mkp.ReferralCampaignSet(rc);
                }
            }
        }


        /// <summary>
        /// 將策展折價券string轉List
        /// </summary>
        /// <param name="strDiscountList"></param>
        /// <returns></returns>
        public static List<int> GetDiscountListFromStringToList(string strDiscountList)
        {
            var discountList = new List<int>();
            if (strDiscountList != null)
            {
                int x = 0;
                discountList.AddRange(strDiscountList.Split(',').Where(s => int.TryParse(s, out x)).Select(s => x));
            }
            return discountList;
        }

        /// <summary>
        /// 取得活動檔次列表
        /// </summary>
        /// <param name="eventType">活動類別</param>
        /// <param name="id">EventId、BrandId共用id</param>
        /// <param name="categoryKey">主類別</param>
        /// <param name="subCategoryKey">子類別</param>
        /// <param name="pagedOption">分頁操作參數</param>
        /// <param name="reload">是否reload</param>
        /// <returns></returns>
        public static ApiPagedEventPromoDeaList GetApiEventPromoDealList(
            int? eventType, int id, string categoryKey, string subCategoryKey, ApiPagedOption pagedOption, bool reload)
        {
            ApiPagedEventPromoDeaList result = new ApiPagedEventPromoDeaList();

            ApiEventPromo eventPromo;

            if (reload || config.MemoryCache2Mode == 0)
            {
                eventPromo = eventType == (int)EventPromoEventType.CurationTwo
                    ? GetApiBrandByIdFromDB(id)
                    : GetApiEventPromoByIdFromDB(id);
            }
            else
            {
                eventPromo = GetApiEventPromoOrBrandByIdFromDB(id, eventType);
            }

            if (eventPromo == null)
            {
                return result;
            }

            #region 策展2.0

            if (eventType == (int)EventPromoEventType.CurationTwo)
            {
                ApiEventPromoCategory category = eventPromo.Categories.FirstOrDefault(t => t.Key == categoryKey);
                if (category != null)
                {
                    foreach (var promoDeal in category.DealList)
                    {
                        result.DealList.Add(promoDeal);
                    }
                }
                else
                {
                    //取得全部檔次
                    var bidList = new List<Guid>();
                    foreach (var ct in eventPromo.Categories)
                    {
                        foreach (var promoDeal in ct.DealList)
                        {
                            //去除重複Bid
                            if (!bidList.Contains(promoDeal.Bid))
                            {
                                result.DealList.Add(promoDeal);
                                bidList.Add(promoDeal.Bid);
                            }
                        }
                    }
                }

                result.TotalCount = result.DealList.Count;
                result.DealList = pagedOption.GetPagedDataList(result.DealList);
                return result;
            }

            #endregion

            #region 策展1.0
            else
            {
                if (string.IsNullOrEmpty(categoryKey))
                {
                    //找全部
                    var bidList = new List<Guid>();
                    foreach (var category in eventPromo.Categories)
                    {
                        foreach (var subCategory in category.SubCategories)
                        {
                            foreach (var promoDeal in subCategory.DealList)
                            {
                                //if (!ConformLowGrossMargin(promoDeal.Bid)) continue;

                                //去除重複Bid
                                if (!bidList.Contains(promoDeal.Bid))
                                {
                                    result.DealList.Add(promoDeal);
                                    bidList.Add(promoDeal.Bid);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(subCategoryKey))
                    {
                        //找主分類
                        ApiEventPromoCategory category = eventPromo.Categories.FirstOrDefault(t => t.Key == categoryKey);
                        if (category == null)
                        {
                            return new ApiPagedEventPromoDeaList();
                        }
                        var bidList = new List<Guid>();
                        foreach (var subCategory in category.SubCategories)
                        {
                            foreach (var promoDeal in subCategory.DealList)
                            {
                                //if (!ConformLowGrossMargin(promoDeal.Bid)) continue;

                                //去除重複Bid
                                if (!bidList.Contains(promoDeal.Bid))
                                {
                                    result.DealList.Add(promoDeal);
                                    bidList.Add(promoDeal.Bid);
                                }
                            }
                        }
                    }
                    else
                    {
                        //找主分類+子分類
                        ApiEventPromoCategory category = eventPromo.Categories.FirstOrDefault(t => t.Key == categoryKey);
                        if (category == null)
                        {
                            return new ApiPagedEventPromoDeaList();
                        }
                        var subCategory = category.SubCategories.FirstOrDefault(t => t.Key == subCategoryKey);
                        if (subCategory == null)
                        {
                            return new ApiPagedEventPromoDeaList();
                        }
                        foreach (var promoDeal in subCategory.DealList)
                        {
                            result.DealList.Add(promoDeal);
                        }
                    }
                }
            }

            result.TotalCount = result.DealList.Count;
            result.DealList = pagedOption.GetPagedDataList(result.DealList);
            return result;

            #endregion
        }


        /// <summary>
        /// 取得所有有效策展資訊(測試用)
        /// </summary>
        /// <returns></returns>
        public static List<ApiEventPromo> GetAllApiCurations()
        {
            return GetApiEventPromoListByCityId(0, config.EnableCurationTwoInApp);
        }

        /// <summary>
        /// 隨機取得策展活動
        /// </summary>
        /// <param name="eventCount">取幾個活動</param>
        /// <returns></returns>
        public static List<ApiCurationInfo> GetRandomDefaultCurationTwo(int eventCount)
        {
            List<int> sortedEventIds = new List<int>();

            //從[system_data]抓出設定
            string dataName = "DefaultCurationTwoSortedEventIds";
            var data = ss.SystemDataGet(dataName);
            var eventIds = data.Data;
            if (!string.IsNullOrEmpty(eventIds))
            {
                sortedEventIds = Helper.SplitToIntList(eventIds);
            }

            List<ApiCurationInfo> result = new List<ApiCurationInfo>();
            if (sortedEventIds.Any() == false)
            {
                return result;
            }

            Random r = new Random();
            int indexEnd = 30;
            for (int i = 0; i < indexEnd; i++)
            {
                int eId = sortedEventIds[r.Next(sortedEventIds.Count)];
                var content = GetCurationInfoOldToNew((int)EventPromoEventType.CurationTwo, eId);

                if (content != null && result.All(x => x.EventId != eId))
                {
                    result.Add(content);
                }
                if (result.Count == eventCount)
                {
                    break;
                }
            }

            return result;
        }

        public static ApiCurationInfo GetCurationInfoOldToNew(int? eventType, int eventId, bool isFromCache = true)
        {
            ApiEventPromo apieventPromo = isFromCache
                ? GetApiEventPromoOrBrandByIdFromCache(eventId, eventType)
                : GetApiEventPromoOrBrandByIdFromDB(eventId, eventType);

            return ApiEventPromoToApiCuration(apieventPromo, ApiCurationInfoUsage.SingleEvent);
        }

        /// <summary>
        /// 取得首頁Banner策展資訊
        /// </summary>
        /// <returns></returns>
        public static List<ApiCurationInfo> GetApiCurationsByDefaultBanner()
        {
            List<ApiCurationInfo> curationInfos = new List<ApiCurationInfo>();
            List<ApiEventPromo> eventPromos = GetApiEventPromoListByCityId(0, config.EnableCurationTwoInApp);
            foreach (var eventPromo in eventPromos)
            {
                var curationInfo = ApiEventPromoToApiCuration(eventPromo, ApiCurationInfoUsage.MainPage);
                if (curationInfo == null)
                {
                    continue;
                }

                curationInfos.Add(curationInfo);
            }
            List<int> sortedEventIds = new List<int>();

            //從[system_data]抓出設定
            string dataName = "DefaultCurationTwoSortedEventIds";
            var data = ss.SystemDataGet(dataName);
            var eventIds = data.Data;
            if (!string.IsNullOrEmpty(eventIds))
            {
                sortedEventIds = Helper.SplitToIntList(eventIds);
            }

            //20170627 改為如果沒設定順序則不顯示
            List<ApiCurationInfo> result = new List<ApiCurationInfo>();
            foreach (int sortedEventId in sortedEventIds)
            {
                var curationInfo = curationInfos.FirstOrDefault(t => t.EventId == sortedEventId);
                if (curationInfo != null)
                {
                    result.Add(curationInfo);
                }
            }
            
            return result;
        }

        /// <summary>
        /// 取得頻道、區域策展資訊
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="areaId"></param>
        /// <param name="isAndroid"></param>
        /// <returns></returns>
        public static List<ApiCurationInfo> GetApiCurationsByChannel(int channelId, int? areaId, bool isAndroid = false)
        {
            List<ApiCurationInfo> curationInfos = new List<ApiCurationInfo>();
            List<ApiEventPromo> eventPromos = GetApiEventPromoListByChannel(channelId, areaId, config.EnableCurationTwoInApp);

            foreach (var eventPromo in eventPromos)
            {
                var curationInfo = ApiEventPromoToApiCuration(eventPromo, ApiCurationInfoUsage.Channel, isAndroid);
                if (curationInfo == null)
                {
                    continue;
                }

                curationInfos.Add(curationInfo);
            }

            return curationInfos;
        }
        public static List<ApiEventPromo> GetApiEventPromoListByChannel(int channelId, int? areaId, bool isEnableCurationTwoInApp)
        {
            PponCity pponcity;
            if (channelId == PponDealPreviewManager._MAIN_THEME_CATEGORY_ID)
            {
                pponcity = PponCityGroup.DefaultPponCityGroup.AllCountry;
            }
            else
            {
                pponcity = PponCityGroup.GetPponCityByChannel(channelId, areaId);
            }

            int cityId = pponcity == null ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId : pponcity.CityId;

            return GetApiEventPromoListByCityId(cityId, isEnableCurationTwoInApp);
        }

        /// <summary>
        /// 舊版model轉新版
        /// </summary>
        /// <param name="eventPromo"></param>
        /// <param name="usage"></param>
        /// <param name="isAndroid"></param>
        /// <returns></returns>
        public static ApiCurationInfo ApiEventPromoToApiCuration(ApiEventPromo eventPromo, ApiCurationInfoUsage usage, bool isAndroid = false)
        {
            if (eventPromo == null)
            {
                return null;
            }

            ApiCurationInfo curationInfo = new ApiCurationInfo();
            curationInfo.EventId = eventPromo.EventId;
            curationInfo.Type = eventPromo.Type;
            curationInfo.Title = eventPromo.Title ?? string.Empty;
            curationInfo.EventDescription = eventPromo.Description ?? string.Empty;
            curationInfo.TotalCount = eventPromo.TotalCount;
            curationInfo.Categories = eventPromo.Categories;
            curationInfo.EndDate = eventPromo.EndDate;

            if (usage == ApiCurationInfoUsage.MainPage)
            {
                if (Helper.IsFlagSet(eventPromo.BannerType, EventBannerType.MainUp))
                {
                    curationInfo.BannerImageUrl = eventPromo.ImageUrl;
                }
                if (Helper.IsFlagSet(eventPromo.BannerType, EventBannerType.MainDown))
                {
                    curationInfo.ContentImageUrl = eventPromo.ChannelImageUrl;
                }
            }
            else if (usage == ApiCurationInfoUsage.Channel)
            {
                if (Helper.IsFlagSet(eventPromo.BannerType, EventBannerType.ChannelUp))
                {
                    curationInfo.BannerImageUrl = eventPromo.ChannelImageUrl;
                }
                if (Helper.IsFlagSet(eventPromo.BannerType, EventBannerType.ChannelDown))
                {
                    curationInfo.ContentImageUrl = eventPromo.ChannelImageUrl;
                }
            }
            else
            {
                //商品主題或策展活動頁只會顯示 ChannelImageUrl
                curationInfo.BannerImageUrl = eventPromo.MobileMainPicUrl;
                curationInfo.ContentImageUrl = eventPromo.MobileMainPicUrl;
            }

            curationInfo.BannerImageUrl = curationInfo.BannerImageUrl ?? string.Empty;
            curationInfo.ContentImageUrl = curationInfo.ContentImageUrl ?? string.Empty;
            curationInfo.BannerType = eventPromo.BannerType;
            curationInfo.ShareUrl = eventPromo.ShareUrl;

            if (eventPromo.Type == EventPromoEventType.CurationTwo &&
                eventPromo.DiscountList != null && eventPromo.DiscountList.Any() &&
                PromotionFacade.SimpleCheckAnyDiscountCampaignForCuration(eventPromo.EventId))
            {
                curationInfo.HasAvailableDiscount = true;
                curationInfo.RelayImageUrl = eventPromo.RelayImageUrl;
            }
            else
            {
                //若無有效限折價券則為false
                curationInfo.HasAvailableDiscount = false;
                curationInfo.RelayImageUrl = string.Empty;
            }

            return curationInfo;
        }

        /// <summary>
        /// 儲存商品主題活動、策展1.0、策展2.0，同時重置MemoryCache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="eventType"></param>
        public static void SaveCurationAndResetMemoryCache<T>(T t, EventPromoEventType eventType = EventPromoEventType.Curation)
        {
            var cacheKey = eventType == EventPromoEventType.CurationTwo
                ? "B" + pp.SaveBrand(t as Brand)
                : "E" + pp.SaveEventPromo(t as EventPromo);

            SystemFacade.ClearCacheByObjectNameAndCacheKey("ApiEventPromo", cacheKey);
        }


        /// <summary>
        /// 自動更新所有活動時間內策展輪播商品
        /// </summary>
        /// <param ></param>
        /// <returns></returns>
        public static void UpdataBrandBanner()
        {
            var brands = pp.BrandGetList().Where(x=>x.StartTime < DateTime.Now && x.EndTime > DateTime.Now);

            foreach (var brand in brands)
            {
                Guid gid;
                if (!string.IsNullOrEmpty(brand.Mk1ActName) && Guid.TryParse(brand.Mk1ActName, out gid))
                {

                    int count = 0;
                    string tmpStr = "";
                    var tmpData = PponFacade.GetBidByBrandId(brand).Take(20).ToList();
                    var tmpCount = 0;

                    if (Guid.TryParse(brand.Mk1ActName, out gid))
                    {
                        if (PponFacade.GetDealIsColsed(gid))
                        {
                            brand.Mk1ActName = brand.Mk1WebImage = brand.Mk1AppImage = tmpData[tmpCount];
                            tmpCount++;
                        }  
                    }
                    
                    if (!string.IsNullOrEmpty(brand.Mk2ActName) && Guid.TryParse(brand.Mk2ActName, out gid))
                    {
                        if (PponFacade.GetDealIsColsed(gid))
                        {
                            brand.Mk2ActName = brand.Mk2WebImage = brand.Mk2AppImage = tmpData[tmpCount];
                            tmpCount++;
                        }
                    }  

                    if (!string.IsNullOrEmpty(brand.Mk3ActName) && Guid.TryParse(brand.Mk3ActName, out gid))
                    {
                        if (PponFacade.GetDealIsColsed(gid))
                        {
                            brand.Mk3ActName = brand.Mk3WebImage = brand.Mk3AppImage = tmpData[tmpCount];
                            tmpCount++;
                        }
                    }

                    if (!string.IsNullOrEmpty(brand.Mk4ActName) && Guid.TryParse(brand.Mk4ActName, out gid))
                    {
                        if (PponFacade.GetDealIsColsed(gid))
                        {
                            brand.Mk4ActName = brand.Mk4WebImage = brand.Mk4AppImage = tmpData[tmpCount];
                            tmpCount++;
                        }
                    }
                    
                    if (!string.IsNullOrEmpty(brand.Mk5ActName) && Guid.TryParse(brand.Mk5ActName, out gid))
                    {
                        if (PponFacade.GetDealIsColsed(gid))
                        {
                            brand.Mk5ActName = brand.Mk5WebImage = brand.Mk5AppImage = tmpData[tmpCount];
                            tmpCount++;
                        }
                    }
                    
                    if (brand.HotItemBid1 != null)
                    {
                        gid = (Guid)brand.HotItemBid1;
                        if (PponFacade.GetDealIsColsed(gid))
                        {
                            Guid tmpGuid;
                            brand.HotItemBid1 = Guid.TryParse(tmpData[tmpCount], out tmpGuid) ? tmpGuid : new Guid(); ;
                            tmpCount++;
                        }
                    }

                    if (brand.HotItemBid2 != null)
                    {
                        gid = (Guid)brand.HotItemBid2;
                        if (PponFacade.GetDealIsColsed(gid))
                        {
                            Guid tmpGuid;
                            brand.HotItemBid2 = Guid.TryParse(tmpData[tmpCount], out tmpGuid) ? tmpGuid : new Guid(); ;
                            tmpCount++;
                        }
                    }

                    if (brand.HotItemBid3 != null)
                    {
                        gid = (Guid)brand.HotItemBid3;
                        if (PponFacade.GetDealIsColsed(gid))
                        {
                            Guid tmpGuid;
                            brand.HotItemBid3 = Guid.TryParse(tmpData[tmpCount], out tmpGuid) ? tmpGuid : new Guid(); ;
                            tmpCount++;
                        }
                    }

                    if (brand.HotItemBid4 != null)
                    {
                        gid = (Guid)brand.HotItemBid4;
                        if (PponFacade.GetDealIsColsed(gid))
                        {
                            Guid tmpGuid;
                            brand.HotItemBid4 = Guid.TryParse(tmpData[tmpCount], out tmpGuid) ? tmpGuid : new Guid(); ;
                        }
                    }

                    SaveCurationAndResetMemoryCache(brand, EventPromoEventType.CurationTwo);
                }
            }
        }

        #region private

        /// <summary>
        /// 取得商品主題/策展1.0/策展2.0資料
        /// </summary>
        /// <param name="cityId">頻道編號</param>
        /// <param name="isEnableCurationTwoInApp">是否顯示策展2.0</param>
        /// <returns></returns>
        private static List<ApiEventPromo> GetApiEventPromoListByCityId(int cityId = 0, bool isEnableCurationTwoInApp = false)
        {
            List<ViewCmsRandom> cmsRandoms = CmsRandomFacade.GetViewCmsRandomsByCity(RandomCmsType.PponMasterPage, "curation", cityId);
            List<ApiEventPromo> result = new List<ApiEventPromo>();

            //排序，因策展2.0沒有排序功能 seq 一律為0，因此要往下排其他欄位
            var sortCmsRandoms = cmsRandoms.OrderBy(x => x.Seq).ThenByDescending(x => x.StartTime).ThenByDescending(x => x.CreatedOn);
            foreach (var cmsRandom in sortCmsRandoms)
            {
                if (cmsRandom.BannerStatus == false || cmsRandom.ShowInApp == null || cmsRandom.ShowInApp == false)
                {
                    continue;
                }

                ApiEventPromo apiEventPromo = new ApiEventPromo();

                var isEventPromo = cmsRandom.EventPromoId != null;//商品主題活動、策展1.0
                var isBrand = cmsRandom.BrandId != null;//策展2.0

                if (isEventPromo)
                {
                    //策展1.0
                    apiEventPromo = GetApiEventPromoOrBrandByIdFromCache((int)cmsRandom.EventPromoId, (int)EventPromoEventType.Curation);
                }
                else if (isBrand && isEnableCurationTwoInApp)
                {
                    //策展2.0
                    apiEventPromo = GetApiEventPromoOrBrandByIdFromCache((int)cmsRandom.BrandId, (int)EventPromoEventType.CurationTwo);
                }

                //濾掉活動資料未設定完整的情況
                if (apiEventPromo == null || apiEventPromo.TotalCount <= 0)
                {
                    continue; //需有匯入檔次
                }

                //if (cityId == 0 && string.IsNullOrEmpty(apiEventPromo.ImageUrl))
                //    continue; //首頁Banner需有圖

                //if (cityId > 0 && apiEventPromo.Type == EventPromoEventType.CurationTwo && string.IsNullOrEmpty(apiEventPromo.ChannelImageUrl)) 
                //    continue; //策展2.0頻道Banner需有圖

                var rtnApiEventPromo = new ApiEventPromo
                {
                    Type = apiEventPromo.Type,
                    EventId = apiEventPromo.EventId,
                    Title = apiEventPromo.Title,
                    BannerType = apiEventPromo.BannerType,
                    ImageUrl = apiEventPromo.ImageUrl,
                    ChannelImageUrl = apiEventPromo.ChannelImageUrl,
                    MobileMainPicUrl = apiEventPromo.MobileMainPicUrl,
                    Description = string.Empty,
                    ShareUrl = apiEventPromo.ShareUrl,
                    TotalCount = apiEventPromo.TotalCount,
                    Categories = apiEventPromo.Categories,
                    DiscountList = apiEventPromo.DiscountList
                };

                result.Add(rtnApiEventPromo);
            }
            return result;
        }

        /// <summary>
        /// 處理[策展1.0]檔次
        /// </summary>
        /// <param name="promoItems"></param>
        /// <param name="apiEventPromo"></param>
        private static void SetApiEventPromoCategoriesAndDeals(ViewEventPromoCollection promoItems, ApiEventPromo apiEventPromo)
        {
            int totalDealCount = 0;
            var itemList = new List<int>();
            StringBuilder sbLog = new StringBuilder();
            foreach (ViewEventPromo item in promoItems.OrderBy(t => t.Seq).ThenBy(t => t.Category).ThenBy(t => t.SubCategory))
            {
                try
                {
                    if (!item.Status)
                    {
                        continue; //後台檔次設定不顯示
                    }

                    //確認檔次處於上檔時間
                    if (DateTime.Now < item.BusinessHourOrderTimeS || DateTime.Now > item.BusinessHourOrderTimeE)
                    {
                        continue;
                    }

                    #region 處理 Category/SubCategory

                    ApiEventPromoCategory category = apiEventPromo.Categories.FirstOrDefault(t => t.Key == item.Category);
                    if (category == null)
                    {
                        category = new ApiEventPromoCategory
                        {
                            Key = item.Category,
                            SubCategories = new List<ApiEventPromoCategory>()
                        };
                        category.Title = string.IsNullOrEmpty(category.Key)
                            ? string.Empty
                            : category.Key.TrimStart(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.' });
                        apiEventPromo.Categories.Add(category);
                    }
                    ApiEventPromoCategory subCategory = category.SubCategories.FirstOrDefault(t => t.Key == item.SubCategory);
                    if (subCategory == null)
                    {
                        subCategory = new ApiEventPromoCategory
                        {
                            Key = item.SubCategory,
                            DealList = new List<ApiEventPromoDeal>()
                        };
                        subCategory.Title = string.IsNullOrEmpty(subCategory.Key)
                            ? string.Empty
                            : subCategory.Key.TrimStart(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.' });
                        category.SubCategories.Add(subCategory);
                    }
                    #endregion

                    var promoDeal = GetPromoDeal(item.ItemId, item.Seq);
                    if (promoDeal != null)
                    {
                        if (apiEventPromo.DiscountList.Any() && !ConformLowGrossMargin(promoDeal.Bid))
                        {
                            continue;
                        }

                        subCategory.DealList.Add(promoDeal);

                        //計算去除跨類別重覆的檔次數
                        if (!itemList.Contains(item.ItemId))
                        {
                            totalDealCount++;
                            itemList.Add(item.ItemId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    sbLog.AppendLine("ErrorId:" + item.BusinessHourGuid);
                    sbLog.AppendLine(ex.ToString());
                }
            }

            //總檔次數
            apiEventPromo.TotalCount = totalDealCount;

            if (sbLog.Length > 0)
            {
                logger.WarnFormat("SetApiEventPromoCategoriesAndDeals 發生錯誤, errorLog: {0}", sbLog);
            }
        }

        /// <summary>
        /// 處理[策展2.0]檔次
        /// </summary>
        /// <param name="brandItems"></param>
        /// <param name="apiEventPromo"></param>
        private static void SetApiBrandCategoriesAndDeals(ViewBrandCollection brandItems, ApiEventPromo apiEventPromo,
            Dictionary<int, BrandItemCategory> brandItemCategories)
        {
            int totalDealCount = 0;
            var itemList = new List<int>();
            StringBuilder sbLog = new StringBuilder();

            foreach (ViewBrand item in brandItems.OrderBy(x => x.Seq).ThenBy(x => x.CategoryId))
            {
                try
                {
                    if (!item.ItemStatus)
                    {
                        continue; //後台設定此檔次不顯示
                    }

                    //確認檔次處於上檔時間
                    if (DateTime.Now < item.BusinessHourOrderTimeS || DateTime.Now > item.BusinessHourOrderTimeE)
                    {
                        continue;
                    }

                    if (Helper.IsFlagSet(item.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
                    {
                        continue;
                    }

                    ApiEventPromoCategory category = apiEventPromo.Categories.FirstOrDefault(t => t.Key == item.CategoryId.ToString());
                    if (category == null)
                    {
                        category = new ApiEventPromoCategory
                        {
                            Key = item.CategoryId.ToString(),
                            Title = item.CategoryName,
                            SubCategories = new List<ApiEventPromoCategory>(),
                            DealList = new List<ApiEventPromoDeal>() //與策展1.0結構稍不同 (策展2.0沒有SubCategories，DealList上移一層)                            
                        };
                        if (brandItemCategories.ContainsKey(item.CategoryId))
                        {
                            category.Seq = brandItemCategories[item.CategoryId].Seq.GetValueOrDefault();
                        }
                        apiEventPromo.Categories.Add(category);
                    }

                    var promoDeal = GetPromoDeal(item.ItemId, item.Seq);
                    if (promoDeal != null)
                    {
                        //API策展商品不需檢查毛利率 2017/02/21 add
                        //if (apiEventPromo.DiscountList.Any() && !ConformLowGrossMargin(promoDeal.Bid)) continue;
                        category.DealList.Add(promoDeal);

                        //計算去除跨類別重覆的檔次數
                        if (!itemList.Contains(item.ItemId))
                        {
                            totalDealCount++;
                            itemList.Add(item.ItemId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    sbLog.AppendLine("ErrorId:" + item.ItemId);
                    sbLog.AppendLine(ex.ToString());
                }
            }
            apiEventPromo.Categories = apiEventPromo.Categories.OrderBy(t => t.Seq).ToList();
            //總檔次數
            apiEventPromo.TotalCount = totalDealCount;

            if (sbLog.Length > 0)
            {
                logger.WarnFormat("SetApiBrandCategoriesAndDeals 發生錯誤, errorLog: {0}", sbLog);
            }
        }

        private static ApiEventPromoDeal GetPromoDeal(int itemId, int itemSeq)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(itemId);
            if (deal.IsLoaded == false)
            {
                logger.DebugFormat("bid={0} deal is not loaded", itemId);
                return null;
            }

            var quantityAdj = deal.GetAdjustedSlug();
            string travelPlace = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(null, deal.BusinessHourGuid);

            var promoDeal = new ApiEventPromoDeal
            {
                Bid = deal.BusinessHourGuid,
                DealName = deal.CouponUsage,
                Seq = itemSeq,
                Price = deal.ItemPrice,
                OriginalPrice = deal.ItemOrigPrice,
                ExchangePrice = deal.ExchangePrice,
                ImagePath = PponFacade.GetPponDealFirstImagePath(deal.EventImagePath, true),
                SquareImagePath = (string.IsNullOrEmpty(deal.AppDealPic))
                    ? PponFacade.GetPponDealFirstImagePath(deal.EventImagePath, true)
                    : PponFacade.GetPponDealFirstImagePath(deal.AppDealPic),
                DealStartTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourOrderTimeS),
                DealEndTime = ApiSystemManager.DateTimeToDateTimeString(deal.ChangedExpireDate ?? deal.BusinessHourOrderTimeE),
                SoldNum = quantityAdj.Quantity,

                ShowUnit = quantityAdj.IsAdjusted ? @"已售出\d份" : @"\d人已購買",
                SoldOut = deal.OrderedQuantity >= deal.OrderTotalLimit,
                TravelPlace = travelPlace,
                IsMultiDeal = (deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0,
                BehaviorType = deal.CityList.Contains(CategoryManager.Default.Piinlife.CityId.ToString())
                    ? (int)BehaviorType.PiinLife
                    : deal.CityList.Contains(CategoryManager.Default.Family.CityId.ToString())
                        ? (int)BehaviorType.Fami
                        : (int)BehaviorType.Ppon,
                DealTags = PponDealApiManager.GetDealTags(deal, 2),
                Categories = PponDealPreviewManager.GetDealCategoryIdList(deal.BusinessHourGuid),
                DiscountPrice = (config.EnableDiscountPrice == false || deal.DiscountPrice == null)
                    ? (decimal?)null : decimal.Truncate(deal.DiscountPrice.Value),
                DeliveryType = (int)deal.DeliveryType
            };

            #region 處理檔次優惠價格與評價資訊

            //這裡的檔次不同於檔次列表，不會再轉過一手變成新版檔次列表，因此仍需特別處理優惠價格與評價。
            var discountStringModel = ViewPponDealManager.GetDealDiscountStringByIViewPponDeal(deal);
            promoDeal.DisplayPrice = discountStringModel.DisplayPrice;
            promoDeal.DiscountString = discountStringModel.DiscountString;
            promoDeal.DiscountDisplayType = discountStringModel.DiscountDisplayType;

            var evaluateStar = ViewPponDealManager.DefaultManager.GetPponDealEvaluateStarList(promoDeal.Bid);
            if (evaluateStar != null)
            {
                promoDeal.DealEvaluate = new DealEvaluate
                {
                    EvaluateNumber = evaluateStar.Cnt,
                    EvaluateStar = evaluateStar.Star
                };
            }

            #endregion

            return promoDeal;
        }

        


        /// <summary>
        /// 是否符合低毛利限制
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        private static bool ConformLowGrossMargin(Guid bid)
        {
            if (!config.EnableGrossMarginRestrictions)
            {
                return true;
            }

            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            var tempGrossMargin = ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(bid).BaseGrossMargin;
            var grossMargin = Math.Round(tempGrossMargin * 100, 2, MidpointRounding.AwayFromZero);

            return (grossMargin > Convert.ToDecimal(config.GrossMargin)) ||
                   Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.LowGrossMarginAllowedDiscount);
        }

        #endregion

        #region 成效報表

        public static List<FrontDealsDailyStat> GetFrontDealsDailyStateByRange(DateTime dateStart, DateTime dateEnd)
        {
            AppFrontDealManager manager = new AppFrontDealManager();
            List<FrontDealOrderInfo> orders = manager.GetFrontDealsOrders(dateStart, dateEnd);
            List<AppFrontPageView> appFrontPageViews = manager.GetFrontpageView(dateStart, dateEnd);

            var result = appFrontPageViews.GroupJoin(orders
                , afpv => new { UserId = afpv.UserId, CreateDay = (afpv.Date) }
                , o => new { UserId = o.UserId, CreateDay = (o.Date).Date }
                , (afpv, o) => new FrontDealsDailyStat
                {
                    UserId = afpv.UserId,
                    Date = (afpv.Date),
                    DeviceIos = afpv.DeviceIos,
                    DeviceAndroid = afpv.DeviceAndroid,
                    AppOrderCount = o.Sum(order => order.AppOrderCount),
                    AppOrderTurnoverTotal = o.Sum(order => order.AppOrderTurnoverTotal),
                    AppOrderGrossProfitTotal = o.Sum(order => order.AppOrderGrossProfitTotal),
                    AppFrontpageViewCount = afpv.AppFrontpageViewCount,
                    AppFrontpageViewTotal = afpv.AppFrontpageViewTotal,
                }).ToList();

            return result;
        }
        
        public static void ImportFrontDealsDailyState(DateTime startDateTime, DateTime endDateTime, List<FrontDealsDailyStat> frontDealsDailyStats)
        {
            DateTime now = DateTime.Now;
            foreach (var frontDealsDailyStat in frontDealsDailyStats)
            {
                frontDealsDailyStat.CreateTime = now;
            }
            ppe.ImportFrontDealsDailyState(startDateTime, endDateTime, frontDealsDailyStats);
        }

        #endregion

        public static void AddRushBuyTracking(string cpaKey, string bid, int userId = 0, string url = "")
        {
            RushbuyTracking rushBuyTracking = marketingEntity.RushBuyTrackingGetLast(cpaKey, bid);
            if (rushBuyTracking == null)
            {
                rushBuyTracking = new RushbuyTracking
                {
                    CpaKey = cpaKey,
                    CreateTime = DateTime.Now,
                    Bid = new Guid(bid),
                    UserId = userId,
                    Url = url
                };
                marketingEntity.SaveRushBuyTracking(rushBuyTracking);
            }
        }
    }


    #region 傳遞參數的 Models

    public class ReferrerUpdateItem
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class ReferrerDeleteItem
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
    }

    public class CampaignDeleteItem
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
    }

    public class CampaignUpdateItem {
        [JsonProperty("id")]
        public Guid ReferralGuid { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("duration")]
        public int Duration { get; set; }
        [JsonProperty("oneShot")]
        public bool OneShot { get; set; }
        [JsonProperty("isExternal")]
        public bool IsExternal { get; set; }

    }

    #endregion
}

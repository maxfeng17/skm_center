﻿using Ionic.Zip;
using log4net;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.BizLogic.Model.CouponList;
using LunchKingSite.BizLogic.Models.CustomerService;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.Core.Models;
using PaymentType = LunchKingSite.Core.PaymentType;
using LunchKingSite.DataOrm;
using Seller = LunchKingSite.DataOrm.Seller;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using SubSonic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.BizLogic.Models.BounPoints;
using Winnovative.WnvHtmlConvert;
using Famiport = LunchKingSite.DataOrm.Famiport;
using Peztemp = LunchKingSite.DataOrm.Peztemp;
using LunchKingSite.BizLogic.Models.FamiPort;
using LunchKingSite.BizLogic.Models.QueueModels;
using FamilyNetPincode = LunchKingSite.Core.Models.Entities.FamilyNetPincode;
using LunchKingSite.Core.Enumeration;
using NPOI.HSSF.Record;
using LunchKingSite.Core.Models.OrderEntities;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.BizLogic.Models.LineShopping;
using Newtonsoft.Json;
using System.Net.Http;

namespace LunchKingSite.BizLogic.Facade
{
    public class OrderFacade
    {
        private static IMemberProvider mp;
        private static IOrderProvider op;
        private static IOrderEntityProvider oep;
        private static ISellerProvider sp;
        private static IItemProvider ip;
        private static ISysConfProvider config;
        private static ILocationProvider lp;
        private static IPponProvider pp;
        private static IVerificationProvider vp;
        private static ILog logger = LogManager.GetLogger("OrderFacade");
        private static object thisLock = new object();
        private static IHiDealProvider hp;
        private static ISerializer ser;
        private static ISkmProvider skm;
        private static IMGMProvider mgmp;
        private static IFamiportProvider fami;
        private static IHiLifeProvider hiLife;
        private static IChannelProvider cp;
        private static ISystemProvider sysp;
        private static ISerializer serializer = ProviderFactory.Instance().GetSerializer();
        private static IOrderEntityProvider opEntities;
        private static IWmsProvider wp;

        static OrderFacade()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            opEntities = ProviderFactory.Instance().GetProvider<IOrderEntityProvider>();
            ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            config = ProviderFactory.Instance().GetConfig();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            ser = ProviderFactory.Instance().GetSerializer();
            skm = ProviderFactory.Instance().GetProvider<ISkmProvider>();
            mgmp = ProviderFactory.Instance().GetProvider<IMGMProvider>();
            fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
            cp = ProviderFactory.Instance().GetProvider<IChannelProvider>();
            sysp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            hiLife = ProviderFactory.Instance().GetProvider<IHiLifeProvider>();
            oep = ProviderFactory.Instance().GetProvider<IOrderEntityProvider>();
            wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        }

        #region OrderShip 出貨相關

        public static OrderShipCollection GetOrderShipByOrderGuid(Guid orderGuid)
        {
            return op.OrderShipGetListByOrderGuid(orderGuid);
        }

        /// <summary>
        /// 退貨折讓單狀態
        /// </summary>
        /// <param name="isPaperAllowance"></param>
        /// <returns></returns>
        public static string GetAllowanceDesc(bool? isPaperAllowance)
        {
            return isPaperAllowance == null ? "" : (isPaperAllowance.Value
                    ? "使用紙本折讓"
                    : "同意電子折讓");
        }

        public static string GetAllowanceDesc(int allowanceType)
        {
            switch (allowanceType)
            {
                case (int)AllowanceStatus.PaperAllowance:
                    return "使用紙本折讓";
                case (int)AllowanceStatus.ElecAllowance:
                    return "同意電子折讓";
                case (int)AllowanceStatus.None:
                    return "不需要折讓";
                default:
                    return "";
            }
        }

        /// <summary>
        /// 退貨的結案狀態
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public static string GetFinishStatusDescription(ReturnFormEntity form)
        {
            switch (form.ProgressStatus)
            {
                case ProgressStatus.Completed:
                case ProgressStatus.CompletedWithCreditCardQueued:
                    return "退貨已完成";
                case ProgressStatus.Unreturnable:
                    return "退貨失敗";
                case ProgressStatus.Canceled:
                    return "取消退貨";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// 取得最晚出貨日
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static ShippingDateModel GetShippingDateInfo(ShippingDateInputModel input)
        {
            var shipTypeModel = new ShippingDateModel();

            VacationCollection recentHolidays = pp.GetVacationListByPeriod(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(14));
            if (input.ShipType != null)
            {
                DateTime? lastShipDate = null;
                DealShipType dealShipType = (DealShipType)input.ShipType;
                switch (dealShipType)
                {
                    case DealShipType.Ship72Hrs:
                    case DealShipType.Wms:
                        shipTypeModel.ShipTypeDesc = "24小時出貨";
                        lastShipDate = PponExpandEndTime.IncrDaysExceptVocations(input.OrderCreateTime, 1, recentHolidays);
                        break;
                    case DealShipType.Normal:
                        //訂單成立後+(A)個工作天出貨
                        if (input.ShippingdateType == (int)DealShippingDateType.Normal)
                        {
                            var workingDays = input.AfterOrderedDays ?? 0;
                            shipTypeModel.ShipTypeDesc = "訂單成立後" + workingDays + "個工作天出貨";
                            lastShipDate = PponExpandEndTime.IncrDaysExceptVocations(input.OrderCreateTime, workingDays, recentHolidays);
                        }
                        //結檔+(C)個工作天出貨
                        else if (input.ShippingdateType == (int)DealShippingDateType.Special)
                        {
                            var workingDays = input.AfterDealEndDays ?? 0;
                            shipTypeModel.ShipTypeDesc = "結檔後" + workingDays + "個工作天出貨";
                            lastShipDate = PponExpandEndTime.IncrDaysExceptVocations(input.BusinessHourOrderTimeE, workingDays, recentHolidays);
                        }
                        else
                        {
                            return null;
                        }
                        break;
                    case DealShipType.LastShipment: //新宅配提案單這裡不會再出現，因舊檔次可能還有此資料，因此保留
                        shipTypeModel.ShipTypeDesc = "最後出貨日";
                        lastShipDate = input.BusinessHourDeliverTimeE;
                        break;
                    case DealShipType.LastDelivery: //新宅配提案單這裡不會再出現，因舊檔次可能還有此資料，因此保留
                        shipTypeModel.ShipTypeDesc = "最後到貨日";
                        if (input.BusinessHourDeliverTimeE != null)
                        {
                            lastShipDate = PponExpandEndTime.IncrDaysExceptVocations(((DateTime)input.BusinessHourDeliverTimeE).Date, 3, recentHolidays);
                        }
                        break;
                    default: //新宅配提案單的其他，即指定出貨期起訖日
                        shipTypeModel.ShipTypeDesc = "其他";
                        lastShipDate = input.BusinessHourDeliverTimeE;
                        break;
                }
                if (lastShipDate != null)
                {
                    var newDate = (DateTime)lastShipDate;
                    shipTypeModel.LastShipDate = new DateTime(newDate.Year, newDate.Month, newDate.Day, 23, 59, 59);
                }
                return shipTypeModel;
            }
            return null;
        }

        /// <summary>
        /// 取得最晚出貨日
        /// </summary>
        /// <param name="orderCreateTime"></param>
        /// <param name="vpd"></param>
        /// <returns></returns>
        public static ShippingDateModel GetShippingDateInfo(DateTime orderCreateTime, IViewPponDeal vpd)
        {
            var input = new ShippingDateInputModel();
            input.ShipType = vpd.ShipType;
            input.ShippingdateType = vpd.ShippingdateType;
            input.AfterOrderedDays = vpd.Shippingdate;
            input.AfterDealEndDays = vpd.ProductUseDateEndSet;
            input.OrderCreateTime = orderCreateTime;
            input.BusinessHourOrderTimeE = vpd.BusinessHourOrderTimeE;
            input.BusinessHourDeliverTimeE = vpd.BusinessHourDeliverTimeE;

            return GetShippingDateInfo(input);
        }

        /// <summary>
        /// 未出貨舊訂單壓最後出貨日
        /// </summary>
        /// <returns></returns>
        public static int SetLastShipDateOnOrderNotYetShipped()
        {
            var orLastShipDateOrders = op.ViewOrderNotYetShippedCollectionGetByNoLastShipDate();
            var opdCol = new OrderProductDeliveryCollection();
            foreach (var o in orLastShipDateOrders)
            {
                var input = new ShippingDateInputModel
                {
                    ShipType = o.ShipType,
                    ShippingdateType = o.ShippingdateType,
                    AfterOrderedDays = o.Shippingdate,
                    AfterDealEndDays = o.ProductUseDateEndSet,
                    OrderCreateTime = o.OrderCreateTime,
                    BusinessHourOrderTimeE = o.BusinessHourOrderTimeE,
                    BusinessHourDeliverTimeE = o.BusinessHourDeliverTimeE
                };
                var shippingDateInfo = GetShippingDateInfo(input);
                if (shippingDateInfo == null)
                {
                    continue;
                }

                opdCol.Add(new OrderProductDelivery
                {
                    OrderGuid = o.OrderGuid,
                    LastShipDate = shippingDateInfo.LastShipDate,
                    CreateTime = DateTime.Now
                });
            }

            if (opdCol.Any())
            {
                var isSuccess = op.OrderProductDeliveryCollectionBulkInsert(opdCol);
                return isSuccess ? opdCol.Count : 0;
            }
            return 0;
        }

        #endregion

        #region Confirm message related functions

        public static string GenerateRedeemConfirmMsg<T>(MailContentType mType, ViewPponDeal deal, ViewPponOrderDetailCollection orderDetailCol,
            decimal freight, bool isRefundCreditcard) where T : PponAuthorizedMail, new()
        {
            T t1 = TemplateFactory.Instance().GetTemplate<T>();
            t1.MailType = mType;
            t1.TheDeal = deal;
            t1.TheOrderDetailCol = orderDetailCol;
            t1.Freight = freight;
            //t1.ThePayTransCol = ptCol;
            t1.IsRefundCreditcard = isRefundCreditcard;
            t1.PromoDeals = GetPromoDealsForNoticeMail((orderDetailCol.Count() > 0 && orderDetailCol.First().BusinessHourGuid != null) ? (Guid)orderDetailCol.First().BusinessHourGuid : Guid.Empty);
            t1.ServerConfig = config;
            return t1.ToString();
        }

        /// <summary>
        /// TaishinPay 取消訂單通知
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ptc"></param>
        /// <param name="deal"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public static string GenerateTaishinPayCancelOrderMsg(Order o, PaymentTransactionCollection ptc, IViewPponDeal deal, string memberName)
        {
            TaishinPayCancelOrderNoticeMail t1 = TemplateFactory.Instance().GetTemplate<TaishinPayCancelOrderNoticeMail>();
            t1.PaymentTransactionCol = ptc;
            t1.PromoDeals = GetPromoDealsForNoticeMail(deal);
            t1.ServerConfig = config;
            t1.MemberName = memberName;
            t1.ItemName = deal.ItemName;
            t1.OrderDate = o.CreateTime;
            t1.OrderAmount = o.Subtotal;
            return t1.ToString();
        }

        /// <summary>
        /// 付款成功通知信
        /// </summary>
        /// <param name="deal">該檔Deal資訊</param>
        /// <param name="orderDetailCol">訂單資訊</param>
        /// <param name="orderGuid">訂單的guid</param>
        /// <returns>string of mail body</returns>
        /// <remarks>
        /// [2011/08/16]    Ahdaa  新增
        /// </remarks>
        public static string GeneratePaymentSuccessfulConfirmMsg(IViewPponDeal deal, ViewPponOrderDetailCollection orderDetailCol, Guid orderGuid)
        {
            PponPaymentSuccessful t1 = TemplateFactory.Instance().GetTemplate<PponPaymentSuccessful>();
            t1.TheDeal = deal;
            t1.TheCashTrustLogCol = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            t1.PromoDeals = GetPromoDealsForNoticeMail(deal);
            t1.OrderDetailCol = orderDetailCol;

            t1.PponCoupons = pp.ViewPponCouponGetListByOrderGuid(orderGuid);
            t1.ServerConfig = config;
            if (t1.TheCashTrustLogCol.Count > 0 && t1.TheCashTrustLogCol[0].StoreGuid != null)
            {
                Guid storeGuid = (Guid)t1.TheCashTrustLogCol[0].StoreGuid;
                var store = sp.SellerGet(storeGuid);
                ViewPponStore pponStore = pp.ViewPponStoreGetByBidAndStoreGuid(deal.BusinessHourGuid, storeGuid);
                t1.StoreDetails = PponCouponDocument.GetStoreDetailsAsHtml(store, pponStore);
                t1.StoreName = store.SellerName;
            }

            if (deal.DeliveryType == (int)DeliveryType.ToHouse)
            {
                //抓取訂單出貨資訊
                Order o = op.OrderGet(orderGuid);
                WmsOrder wo = ProviderFactory.Instance().GetDefaultProvider<IWmsProvider>().WmsOrderGet(o.Guid);
                ViewCouponListMain main = MemberFacade.ViewCouponListMainGetByOrderGuid(o.UserId, orderGuid).FirstOrDefault();
                if (main != null)
                {
                    ShipInfo shipInfo = OrderFacade.GetOrderShipInfo(main);
                    if (wo.IsLoaded)
                    {
                        t1.ShipInfo = "<p>24H到貨</p><p>出貨進度<br />收到訂單</p>";
                    }
                    else
                    {
                        t1.ShipInfo = PponOrderManager.GetShipInfo(shipInfo, "<br />", main);
                    }
                    if (main.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup || main.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
                    {
                        t1.IsIspOrderPickup = true;
                        switch (main.ProductDeliveryType)
                        {
                            case (int)ProductDeliveryType.FamilyPickup:
                                t1.IspShipInfo = "<p> 全家 " + main.FamilyStoreName + "<br />" + main.FamilyStoreAddr + "</p>";
                                break;
                            case (int)ProductDeliveryType.SevenPickup:
                                t1.IspShipInfo = "<p> 7-11 " + main.SevenStoreName + "<br />" + main.SevenStoreAddr + "</p>";
                                break;
                        }
                    }
                }
            }

            //adjust itemname for mail
            foreach (ViewPponOrderDetail od in orderDetailCol)
            {
                od.ItemName = GetOrderDetailItemName(OrderDetailItemModel.Create(od), new PponItemNameShowOption
                {
                    HideItemName = true
                });
            }

            List<int> citys = new Core.JsonSerializer().Deserialize<List<int>>(deal.CityList);
            if (citys.Contains(PponCityGroup.DefaultPponCityGroup.Skm.CityId) && deal.ItemPrice == 0)
            {
                t1.IsSkmZeroDeal = true;
                var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
                t1.Restrictions = contentLite.Restrictions;
            }
            else
            {
                t1.IsSkmZeroDeal = false;
            }

            return t1.ToString();
        }
        /// <summary>
        /// 公益檔付款成功通知信
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="orderDetailCol"></param>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static string GeneratePponKindPaymentSuccessfulConfirmMsg(ViewPponDeal deal, ViewPponOrderDetailCollection orderDetailCol, Guid orderGuid)
        {
            PponKindPaymentSuccessful t1 = TemplateFactory.Instance().GetTemplate<PponKindPaymentSuccessful>();
            t1.TheDeal = deal;
            t1.TheOrderDetailCol = orderDetailCol;
            t1.TheCashTrustLogCol = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            t1.PromoDeals = GetPromoDealsForNoticeMail(deal);
            t1.ServerConfig = config;
            return t1.ToString();
        }
        /// <summary>
        /// 根據通知信相關檔次取得應推薦的檔次內容(by bid)
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static List<IViewPponDeal> GetPromoDealsForNoticeMail(Guid bid)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            return GetPromoDealsForNoticeMail(deal);
        }
        /// <summary>
        /// 根據通知信相關檔次取得應推薦的檔次內容
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        public static List<IViewPponDeal> GetPromoDealsForNoticeMail(IViewPponDeal deal)
        {
            List<IViewPponDeal> deals = new List<IViewPponDeal>();
            if (deal != null)
            {
                List<int> citys = new Core.JsonSerializer().Deserialize<List<int>>(deal.CityList);

                if (citys.Contains(PponCityGroup.DefaultPponCityGroup.Skm.CityId))
                {
                    deals = GetRandomDeal(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, config.PromoMailDealsCount, config.PromoMailDealsCount); //新光檔次推薦宅配檔次
                }
                else if (!citys.Contains(PponCityGroup.DefaultPponCityGroup.Piinlife.CityId)) // 非品生活
                {
                    deals = GetRandomDeal(deal.BusinessHourGuid, config.PromoMailDealsCount, Convert.ToInt32(config.PromoMailDealsCount * config.PromoMailDealsProportion));
                }
                else if (citys.Contains(PponCityGroup.DefaultPponCityGroup.Piinlife.CityId)) // 品生活
                {
                    deals = GetRandomDeal(deal.BusinessHourGuid, config.PromoMailDealsCount, config.PromoMailDealsCount);
                }
            }
            return deals;
        }

        public static List<IViewPponDeal> GetPromoDealsForNoticeMail(int userId)
        {
            Member mem = MemberFacade.GetMember(userId);
            CategoryNode categoryNode = CategoryManager.Default.FindByCity(mem.CityId.GetValueOrDefault());
            return GetRandomDeal(categoryNode,
                config.PromoMailDealsCount, Convert.ToInt32(config.PromoMailDealsCount * config.PromoMailDealsProportion));
        }

        /// <summary>
        /// 退貨失敗通知信
        /// </summary>
        /// <param name="deal">該檔Deal資訊</param>
        /// <param name="orderDetailCol">訂單資訊</param>
        /// <param name="atmFailReason">ATM錯誤訊息</param>
        /// <returns>string of mail body</returns>
        /// <remarks>
        /// [2011/08/16]    Ahdaa  新增
        /// </remarks>
        public static string GenerateReturnFailureEmail(ViewPponDeal deal, ViewPponOrderDetailCollection orderDetailCol, string atmFailReason)
        {
            PponReturnFailureEmail t1 = TemplateFactory.Instance().GetTemplate<PponReturnFailureEmail>();
            t1.TheDeal = deal;
            t1.TheOrderDetailCol = orderDetailCol;
            t1.AtmFailReason = atmFailReason;
            t1.ServerConfig = config;
            t1.PromoDeals = GetPromoDealsForNoticeMail(deal);
            t1.ServerConfig = config;
            return t1.ToString();
        }

        /// <summary>
        /// 全部退貨通知信
        /// </summary>
        /// <returns>string of mail body</returns>
        /// <remarks>
        /// [2011/08/16]    Ahdaa  新增
        /// </remarks>
        public static string GenerateReturnAllEmail(ViewPponDeal deal, ViewPponOrderDetailCollection orderDetailCol, Order order, CashTrustLogCollection ctCol)
        {
            IList<ReturnFormEntity> returnForm = ReturnFormRepository.FindAllByOrder(order.Guid);
            int returnCount = returnForm.Count();
            var einvoice = op.EinvoiceMainGetListByOrderGuid(order.Guid);

            PponReturnAllMail t1 = TemplateFactory.Instance().GetTemplate<PponReturnAllMail>();
            t1.TheDeal = deal;
            t1.TheOrderDetailCol = orderDetailCol;
            t1.TheCashTrustLogCol = ctCol;
            t1.PromoDeals = GetPromoDealsForNoticeMail((ctCol.Count() > 0 && ctCol.First().BusinessHourGuid != null) ? (Guid)ctCol.First().BusinessHourGuid : Guid.Empty);
            t1.Order = order;
            t1.OrderGuid = order.Guid.ToString();
            t1.ReturnedCounts = returnCount;
            t1.IsShowAllowance = ReturnService.IsPaperAllowance(returnForm[0].Id); //改成判斷是否有預設紙本折讓
            t1.IsInvoiced = (einvoice.Where(x => x.InvoiceNumber != null).Where(x => x.InvoiceNumber != "").Count() > 0 ? true : false);
            t1.ServerConfig = config;
            return t1.ToString();
        }

        /// <summary>
        /// 部份退貨通知信
        /// </summary>
        /// <returns>string of mail body</returns>
        /// <remarks>
        /// [2011/08/16]    Ahdaa  新增
        /// </remarks>
        public static string GenerateReturnPartialEmail(ViewPponDeal deal, ViewPponOrderDetailCollection orderDetailCol, Guid orderGuid, CashTrustLogCollection ctCol)
        {
            PponReturnPartialEmail t1 = TemplateFactory.Instance().GetTemplate<PponReturnPartialEmail>();
            t1.TheDeal = deal;
            t1.TheOrderDetailCol = orderDetailCol;
            t1.TheCashTrustLogCol = ctCol;
            t1.PromoDeals = GetPromoDealsForNoticeMail((ctCol.Count() > 0 && ctCol.First().BusinessHourGuid != null) ? (Guid)ctCol.First().BusinessHourGuid : Guid.Empty);
            t1.ServerConfig = config;
            return t1.ToString();
        }

        public static string GenerateRefundNoticeEmail(MailContentType mailType, ViewPponOrderDetailCollection orderDetailCol, CashTrustLogCollection ctCol)
        {
            PponNoticeMail t1 = TemplateFactory.Instance().GetTemplate<PponNoticeMail>();
            t1.MailType = mailType;
            t1.TheOrderDetailCol = orderDetailCol;
            t1.ServerConfig = config;

            /* wtf... ctCol is null... */
            ViewPponOrder vporder = pp.ViewPponOrderGet(orderDetailCol[0].OrderGuid);
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vporder.BusinessHourGuid);
            t1.ItemName = deal.ItemName;

            return t1.ToString();
        }

        /// <summary>
        /// 購物金轉退現金
        /// </summary>
        public static string RefundCreditcardEmail(ViewPponDeal deal, ViewPponOrderDetailCollection orderDetailCol, Order order, CashTrustLogCollection ctCol)
        {
            PponRefundCreditcardEmail t1 = TemplateFactory.Instance().GetTemplate<PponRefundCreditcardEmail>();
            t1.TheDeal = deal;
            t1.TheOrderDetailCol = orderDetailCol;
            t1.TheCashTrustLogCol = ctCol;
            t1.PromoDeals = GetPromoDealsForNoticeMail((ctCol.Count() > 0 && ctCol.First().BusinessHourGuid != null) ? (Guid)ctCol.First().BusinessHourGuid : Guid.Empty);
            t1.Order = order;
            t1.ServerConfig = config;
            return t1.ToString();
        }
        #endregion

        #region ShoppingCartGroup
        public static ShoppingCartGroup MakeShoppingCartGroup(int UserId)
        {
            ShoppingCartGroup group = new ShoppingCartGroup()
            {
                Guid = Guid.NewGuid(),
                CartStatus = (int)CartStatus.Init,
                CreateTime = DateTime.Now,
                CreateId = UserId
            };
            op.ShoppingCartGroupSet(group);

            return group;
        }
        public static ShoppingCartGroupD MakeShoppingCartGroupD(ShoppingCartGroup group, int UserId)
        {
            ShoppingCartGroupD groupD = new ShoppingCartGroupD()
            {
                Guid = Guid.NewGuid(),
                CartGuid = group.Guid,
                SubCartId = "",
                CreateId = UserId,
                CreateTime = DateTime.Now
            };
            op.ShoppingCartGroupDSet(groupD);

            return groupD;
        }
        #endregion

        #region Order management related functions

        /// <summary>
        /// 只建立Order的外殼，給第3方支付使用
        /// 支付完成再進行 MakeOrder 的動作。
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="theDeal"></param>
        /// <param name="orderfromtype"></param>
        /// <returns></returns>
        public static Order MakeMainOrder(Member mem, IViewPponDeal theDeal, OrderFromType orderfromtype = OrderFromType.ByWeb)
        {
            Order mainOrder = MakeMainOrder(mem.UserName, mem.DisplayName, theDeal, orderfromtype);
            op.OrderSet(mainOrder);
            return mainOrder;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="ticketId"></param>
        /// <param name="timeSlot"></param>
        /// <param name="remark"></param>
        /// <param name="di"></param>
        /// <param name="theDeal"></param>
        /// <param name="orderfromtype"></param>
        /// <param name="prebuiltOrderGuid">當需要先預建訂單，進行第3方支付，然後再轉回主要建立訂單的流程，會賦與此值</param>
        /// <returns></returns>
        public static Guid MakeOrder(string username, string ticketId, DateTime timeSlot, string remark,
            DeliveryInfo di, IViewPponDeal theDeal, OrderFromType orderfromtype, Guid? prebuiltOrderGuid = null)
        {
            bool isAuthenticated = HttpContext.Current.User.Identity.IsAuthenticated;
            Order theOrder = null;
            if (prebuiltOrderGuid != null)
            {
                theOrder = op.OrderGet(prebuiltOrderGuid.Value);
                if (theOrder.IsLoaded == false)
                {
                    theOrder = null;
                }
            }
            //wms (pchome)  調整ProductDeliveryType
            if (theDeal.IsWms)
            {
                di.ProductDeliveryType = ProductDeliveryType.Wms;
            }

            theOrder = MakeAndReturnOrder(username, theDeal.BusinessHourGuid, ticketId, timeSlot, isAuthenticated, remark,
                di, theDeal.OrderGuid, theDeal, orderfromtype, theOrder);
            return theOrder.Guid;
        }

        public static Order MakeAndReturnOrder(string username, Guid bid, string ticketId, DateTime timeSlot, bool isAuthenticated,
            string remark, DeliveryInfo di, Guid? parentOrder, IViewPponDeal theDeal, OrderFromType orderfromtype, Order theOrder)
        {
            #region check parameter

            if (theDeal.IsLoaded == false)
            {
                throw new Exception("business hour not found!");
            }
            ViewShoppingCartItemCollection vsci = op.ViewShoppingCartItemGetList(ticketId);
            if (vsci.Count <= 0)
            {
                throw new Exception("no item in cart!");
            }
            ViewMemberBuildingCity mem = mp.ViewMemberBuildingCityGet(username);
            if (!mem.IsLoaded)
            {
                throw new Exception("not a valid member");
            }

            #endregion

            #region create order
            MemberStatusFlag memStUpd = MemberStatusFlag.None;
            if (theOrder == null)
            {
                theOrder = MakeMainOrder(mem.UserName, Helper.MemberName(mem), theDeal, orderfromtype);
            }
            theOrder = FillOrderInfoStage2(theOrder, mem, theDeal, timeSlot, remark, di, parentOrder, out memStUpd);
            OrderDetailItemCollection odItemCol;
            OrderDetailCollection odrDtlCol = Cart2OrderDetail(vsci, theOrder.Guid, username, out odItemCol);

            int multiSaleBase = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? theDeal.SaleMultipleBase ?? 1 : 1;
            Cart2OrderProduct(vsci, bid, theOrder.Guid, multiSaleBase);

            OrderDetailCollection extraDtl;
            theOrder = FillOrderInfoStage3(theOrder, odrDtlCol, theDeal, di.DeliveryCharge, out extraDtl);
            if (extraDtl != null)
            {
                odrDtlCol.AddRange(extraDtl);
            }

            OrderProductDelivery odp = MakeOrderProductDelivery(theOrder, theDeal, di);

            #endregion

            if (mem.IsGuest)
            {
                theOrder.GuestBuy = (int)GuestBuyType.GuestBuy;
            }
            else
            {
                if (isAuthenticated)
                {
                    theOrder.GuestBuy = (int)GuestBuyType.MemberBuy;
                }
                else
                {
                    theOrder.GuestBuy = (int)GuestBuyType.MemberGuestBuy;
                }
            }

            op.MakeOrder(theOrder, theDeal.BusinessHourGuid, odrDtlCol, odItemCol, vsci, memStUpd, odp);

            string orderId = GetNewOrderId(theDeal.SellerId, theOrder.CreateTime);
            op.OrderSetId(theOrder.Guid, orderId);
            theOrder.OrderId = orderId;

            //記錄最後出貨資訊
            if (theDeal.DeliveryType == (int)DeliveryType.ToHouse)
            {
                SaveMemberCheckInfo(mem.UniqueId, di);
            }

            return theOrder;
        }

        public static Order MakeZeroOrder(string username, string ticketId, Guid? parentOrder, IViewPponDeal theDeal, Guid storeGuid, OrderFromType orderFromType = OrderFromType.ByWeb)
        {
            #region check parameter
            if (theDeal == null || theDeal.IsLoaded == false)
            {
                throw new Exception("business hour not found!");
            }

            ViewMemberBuildingCity mem = mp.ViewMemberBuildingCityGet(username);
            if (!mem.IsLoaded)
            {
                throw new Exception("not a valid member");
            }

            #endregion

            #region create order

            #region basic info

            Order theOrder = new Order();
            theOrder.Guid = Helper.GetNewGuid(theOrder);
            theOrder.SellerGuid = theDeal.SellerGuid;
            theOrder.SellerName = theDeal.SellerName;
            theOrder.UserId = MemberFacade.GetUniqueId(mem.UserName);
            theOrder.MemberName = Helper.MemberName(mem);
            theOrder.CreateId = mem.UserName;
            theOrder.OrderFromType = (int)orderFromType;
            theOrder.CreateTime = DateTime.Now;
            theOrder.ParentOrderId = parentOrder;

            if (theOrder.CreateId.Length > Order.CreateIdColumn.MaxLength)
            {
                theOrder.CreateId = theOrder.CreateId.Substring(0, Order.CreateIdColumn.MaxLength);
            }

            if (theDeal.SettlementTime != null)
            {   //清算檔次
                theOrder.OrderSettlementTime = theDeal.SettlementTime;
            }

            #endregion

            #region delivery info

            theOrder.CityId = mem.CityId;
            theOrder.PhoneNumber = Helper.MemberPhone(mem);

            #endregion

            #region order status

            if (parentOrder == null)
            {
                theOrder.SetOrderStatusComplete();
            }

            #endregion

            OrderDetailCollection odrDtlCol = MakeOrderDetail(theDeal.ItemGuid, theDeal.ItemName, 1, 0, theOrder.Guid, username, null, null, null, storeGuid);

            MakeZeroOrderProduct(theOrder.Guid, theDeal.ItemName, 1);

            #endregion

            op.MakeOrderWithoutCarts(theOrder, theDeal.BusinessHourGuid, odrDtlCol, (MemberStatusFlag)mem.MemberStatus);

            string oid = GetNewOrderId(theDeal.SellerId, theOrder.CreateTime);
            op.OrderSetId(theOrder.Guid, oid);
            theOrder.OrderId = oid;


            return theOrder;
        }

        public static Guid MakeGroupOrder(string memberEmail, string memberName, Guid bizHourGuid, DateTime createTime, DateTime closeTime)
        {
            GroupOrder go = new GroupOrder();

            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bizHourGuid);

            if (!vpd.IsLoaded)
            {
                return Guid.Empty;
            }

            Order ord = MakeMainOrder(memberEmail, memberName, vpd);
            ord.OrderStatus = (int)OrderStatus.GroupOrder;

            go.OrderGuid = ord.Guid;
            go.BusinessHourGuid = bizHourGuid;
            go.MemberUserName = memberEmail;
            go.CreateTime = createTime;
            go.CloseTime = closeTime;
            go.Guid = Helper.GetNewGuid(go);
            go.Status = (int)GroupOrderStatus.NotNotified;

            int orderTotalLimit = (int)(vpd.OrderTotalLimit ?? 0);
            bool shouldSetBhModeToFull = orderTotalLimit > 0 && (op.OrderGetDailyOrderCount(vpd.SellerGuid, createTime.Date) + 1 >= orderTotalLimit); // should set business hour mode to full

            op.MakeGroupOrder(ord, go, shouldSetBhModeToFull);

            return go.Guid;
        }

        public static void CompleteGroupOrder(Guid bid)
        {
            var go = op.GroupOrderGetByBid(bid);
            go.Status = (int)Helper.SetFlag(true, go.Status ?? 0, GroupOrderStatus.Completed);
            op.GroupOrderSet(go);
        }

        public static void UpdateGroupOrderStatusForClosingDeal(Guid goGuid, int OrderedQuantity, decimal OrderTotalLimit)
        {
            GroupOrder go = op.GroupOrderGet(goGuid);
            if (!go.IsLoaded)
            {
                throw new Exception("completing a not existing group order?");
            }
            //new GroupOrderStatus for checking if the deal is soldout when closing the deal
            else
            {
                if (OrderedQuantity >= OrderTotalLimit)
                {
                    go.Status |= (int)GroupOrderStatus.SoldOut;
                    if (OrderedQuantity < 500)
                    {
                        go.Status |= (int)GroupOrderStatus.LowQuantityToShow;
                    }
                }
                go.Slug = OrderedQuantity.ToString();
                op.GroupOrderSet(go);
            }
        }

        protected static Order MakeMainOrder(string memberEmail, string memberName, IViewPponDeal vpd,
            OrderFromType orderFromType = OrderFromType.ByWeb)
        {
            Order outOrd = new Order();
            outOrd.Guid = Helper.GetNewGuid(outOrd);
            outOrd.SellerGuid = vpd.SellerGuid;
            outOrd.SellerName = vpd.SellerName;
            outOrd.UserId = MemberFacade.GetUniqueId(memberEmail);
            outOrd.MemberName = memberName;
            outOrd.Subtotal = outOrd.Total = 0M;
            outOrd.CreateId = memberEmail;
            outOrd.OrderFromType = (int)orderFromType;
            if (outOrd.CreateId.Length > Order.CreateIdColumn.MaxLength)
            {
                outOrd.CreateId = outOrd.CreateId.Substring(0, Order.CreateIdColumn.MaxLength);
            }

            outOrd.CreateTime = DateTime.Now;

            return outOrd;
        }

        protected static Order FillOrderInfoStage2(Order ordStage1, ViewMemberBuildingCity mem, IViewPponDeal vpd,
            DateTime deliveryTime, string remark, DeliveryInfo deliv, Guid? parentOrder, out MemberStatusFlag memStUpd)
        {
            if (deliv == null)
            {
                deliv = new DeliveryInfo(Helper.MemberName(mem), mem.Mobile, Helper.MemberDeliveryAddress(mem));
            }

            if (!string.IsNullOrEmpty(deliv.CustomerName))
            {
                ordStage1.MemberName = deliv.CustomerName;
            }
            ordStage1.CityId = mem.CityId;
            ordStage1.DeliveryAddress = deliv.DeliveryAddress;
            ordStage1.DeliveryTime = deliveryTime;
            ordStage1.MobileNumber = deliv.CustomerMobile;
            ordStage1.UserMemo = remark;

            if (parentOrder == null)
            {
                ordStage1.SetOrderStatusComplete();
            }
            if ((vpd.BusinessHourStatus & (int)BusinessHourStatus.IgnoreTime) > 0)
            {
                ordStage1.OrderStatus |= (int)OrderStatus.IgnoreTimeMask;
            }
            ordStage1.PhoneNumber = string.IsNullOrEmpty(deliv.CustomerLandLine) ? Helper.MemberPhone(mem) : deliv.CustomerLandLine + (string.IsNullOrEmpty(deliv.LandLineExtension) ? "" : " #" + deliv.LandLineExtension);
            ordStage1.ParentOrderId = parentOrder;

            memStUpd = (MemberStatusFlag)mem.MemberStatus;
            // if the user has not made an order before, flag it as ordered now)
            if (Helper.IsFlagSet(mem.MemberStatus, (int)MemberStatusFlag.HasOrdered) == false && vpd.ItemPrice > 0)
            {
                memStUpd |= MemberStatusFlag.HasOrdered;
                ordStage1.OrderStatus |= (int)OrderStatus.FirstOrder;
            }

            return ordStage1;
        }




        /// <summary>
        /// Prepare Order, this stage deals with total
        /// 產生運費的訂單明細
        /// </summary>
        /// <param name="ordStage2">The order after stage 2.</param>
        /// <param name="odCol">The items purchased in the order.</param>
        /// <param name="vpd"></param>
        /// <param name="deliveryCharge"></param>
        /// <param name="extraOrdDtl">OrderDetail entries (such as discount/delivery fee) calculated by system.</param>
        /// <returns></returns>
        protected static Order FillOrderInfoStage3(Order ordStage2, OrderDetailCollection odCol, IViewPponDeal vpd,
            int deliveryCharge, out OrderDetailCollection extraOrdDtl)
        {
            extraOrdDtl = null;
            if (ordStage2 == null)
            {
                return null;
            }

            ordStage2.Subtotal = (from x in odCol select x.Total.Value).Sum();
            ordStage2.Total = ordStage2.Subtotal;

            List<ActivityEntry> activities = new List<ActivityEntry>();

            if (deliveryCharge > 0)
            {
                // always make delivery charge to be the first activity
                activities.Insert(0, new ActivityEntry(I18N.Phrase.DeliveryCharge, deliveryCharge));
            }

            extraOrdDtl = activities.ToOrderDetailCollection(ordStage2.Guid);
            ordStage2.Total += (from i in activities select i.Amount).Sum();

            return ordStage2;
        }


        public static OrderProductDelivery MakeOrderProductDelivery(Order theOrder, IViewPponDeal vpd, DeliveryInfo di)
        {
            //宅配檔次才有最後出貨時間
            if (vpd.DeliveryType != (int)DeliveryType.ToHouse)
            {
                return null;
            }

            var opd = new OrderProductDelivery();
            opd.OrderGuid = theOrder.Guid;
            opd.ProductDeliveryType = (int)di.ProductDeliveryType;
            opd.DeliveryId = di.DeliveryId;
            opd.IspOrderType = (int)IspOrderType.B2C;

            if (di.ProductDeliveryType != ProductDeliveryType.Normal && di.PickupStore != null)
            {
                if (di.ProductDeliveryType == ProductDeliveryType.FamilyPickup)
                {
                    opd.FamilyStoreId = di.PickupStore.StoreId;
                    opd.FamilyStoreName = di.PickupStore.StoreName;
                    opd.FamilyStoreTel = di.PickupStore.StoreTel;
                    opd.FamilyStoreAddr = di.PickupStore.StoreAddr;
                }
                if (di.ProductDeliveryType == ProductDeliveryType.SevenPickup)
                {
                    opd.SevenStoreId = di.PickupStore.StoreId;
                    opd.SevenStoreName = di.PickupStore.StoreName;
                    opd.SevenStoreTel = di.PickupStore.StoreTel;
                    opd.SevenStoreAddr = di.PickupStore.StoreAddr;
                }
            }
            var shippingDateModel = GetShippingDateInfo(theOrder.CreateTime, vpd);
            if (shippingDateModel != null)
            {
                opd.LastShipDate = shippingDateModel.LastShipDate;
            }
            opd.CreateTime = DateTime.Now;
            return opd;
        }

        #endregion

        #region Send mails

        public static void SendSkmZeroDealSuccessMail(string mailTo, IViewPponDeal vpd, Guid orderGuid)
        {
            var odc = pp.ViewPponOrderDetailGetList(ViewPponOrderDetail.Columns.OrderGuid, orderGuid, ViewPponOrderDetail.Columns.OrderDetailCreateTime)
                        .Where(x => (x.OrderDetailStatus & (int)OrderDetailStatus.SystemEntry) == 0);

            var odcR = new ViewPponOrderDetailCollection();
            foreach (var od in odc)
            {
                odcR.Add(od);
            }

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                mail.To.Add(mailTo);
                mail.Subject = string.Format("活動完成通知({0})", vpd.ItemName);
                mail.Body = GeneratePaymentSuccessfulConfirmMsg(vpd, odcR, orderGuid);
                mail.IsBodyHtml = true;
                PostMan.Instance().Send(mail, SendPriorityType.Immediate);
            }
        }

        public static void SendZeroDealSuccessMail(string username, string mailTo, IViewPponDeal vpd, ViewPponCoupon coupon, GroupOrderStatus groupOrderStatus, CouponCodeType codeType)
        {
            if (string.IsNullOrEmpty(mailTo))
            {
                return;
            }

            if (Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.FamiDeal))
            {
                FamiDealSuccessMail(username, mailTo, vpd, coupon, groupOrderStatus, codeType);
            }
            else if (Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.SKMDeal))
            {
                //新光檔次不發信
            }
            else
            {
                var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(vpd);
                ZeroDealSuccessMail(username, mailTo, vpd.ItemName, vpd.SellerName, contentLite.Introduction,
                    coupon, groupOrderStatus, vpd.IsZeroActivityShowCoupon == true);
            }
        }

        private static void ZeroDealSuccessMail(string username, string mailTo, string dealName, string sellerName, string introduction, ViewPponCoupon coupon, GroupOrderStatus groupOrderStatus, bool isZeroActivityShowCoupon)
        {
            var content = TemplateFactory.Instance().GetTemplate<ZeroDealSuccessMail>();

            if (!string.IsNullOrEmpty(coupon.CouponCode) && Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.PEZevent))
            {
                content.Coupon = string.Format(I18N.Message.PEZeventCouponFormat, coupon) + "<br/>";
            }

            // 截止日期: 分店優先賣家(檔次)
            var changedExpireDate = coupon.StoreChangedExpireDate ?? coupon.BhChangedExpireDate ?? DateTime.MaxValue;

            // 結束營業日: 分店優先賣家
            var closeDownDate = coupon.StoreCloseDownDate ?? coupon.SellerCloseDownDate ?? DateTime.MaxValue;

            // min(截止日期, 結束營業日)
            var minDate = new DateTime((Math.Min(changedExpireDate.Ticks, closeDownDate.Ticks)));

            if (minDate != coupon.BusinessHourDeliverTimeE && minDate != DateTime.MaxValue)
            {
                content.ChangedExpireDate = minDate.ToString("yyyy/MM/dd");
            }
            else
            {
                content.ChangedExpireDate = string.Empty;
            }

            var stores = ProviderFactory.Instance().GetSerializer().Deserialize<AvailableInformation[]>(coupon.Availability);
            if (stores != null && stores.Length > 0)
            {
                content.StoreName = string.Format("【{0}】{1} {2} {3}", sellerName, stores[0].N, stores[0].P, stores[0].A);
            }
            else
            {
                content.StoreName = string.Empty;
            }
            content.Code = coupon.CouponCode;
            content.SequenceNumber = coupon.SequenceNumber;
            content.DeliveryDateStart = ((DateTime)coupon.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd");
            content.DeliveryDateEnd = ((DateTime)coupon.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd");
            content.IsZeroActivityShowCoupon = isZeroActivityShowCoupon;
            content.DealName = dealName;
            content.MailTo = username;
            content.Introduction = introduction;
            content.SellerName = sellerName;
            // 0元檔全部推薦宅配檔
            content.CityDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId).OrderBy(x => Guid.NewGuid()).Take(config.PromoMailDealsCount).ToList();
            if (!Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.PEZeventCouponDownload))
            {
                content.PEZeventCouponDownload = I18N.Message.PEZeventCouponDownloadMessage;
            }

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                mail.To.Add(mailTo);
                mail.Subject = string.Format("活動完成通知({0})", dealName);
                mail.Body = content.ToString();
                mail.IsBodyHtml = true;
                PostMan.Instance().Send(mail, SendPriorityType.Immediate);
            }
        }

        private static void FamiDealSuccessMail(string username, string mailTo, IViewPponDeal vpd, ViewPponCoupon coupon, GroupOrderStatus groupOrderStatus, CouponCodeType codeType)
        {
            if (Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.FamiDeal))
            {
                using (var mail = new MailMessage())
                {
                    mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                    mail.To.Add(mailTo);
                    mail.Subject = string.Format("活動完成通知({0})", vpd.ItemName);

                    var content = TemplateFactory.Instance().GetTemplate<FamiDealSuccessMail>();

                    if (codeType == CouponCodeType.BarcodeEAN13)
                    {
                        //設定圖檔大小/檔名/cid
                        float size = 3f, barcodeHight = 62f;
                        int imageWidth = 410, imageHigh = 300;
                        string imageName = "famiBarcodeEAN13.png";
                        string contentId = string.Format("17life_{0}", System.DateTime.Now.Ticks.ToString());

                        //取得BarcodeEAN13圖檔
                        System.Drawing.Bitmap barCodeBitmap = new Core.Component.BarCodeEAN13(coupon.CouponCode).Paint(size, imageWidth, imageHigh, barcodeHight);
                        var oStream = new MemoryStream();
                        barCodeBitmap.Save(oStream, System.Drawing.Imaging.ImageFormat.Png);
                        oStream.Seek(0, SeekOrigin.Begin);

                        var contentType = new System.Net.Mime.ContentType();
                        contentType.MediaType = "image/png";
                        contentType.Name = imageName;

                        var barcodeAttactment = new Attachment(oStream, contentType);
                        barcodeAttactment.NameEncoding = Encoding.UTF8;
                        barcodeAttactment.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                        barcodeAttactment.ContentDisposition.Inline = true;
                        barcodeAttactment.ContentDisposition.DispositionType = System.Net.Mime.DispositionTypeNames.Inline;
                        barcodeAttactment.ContentId = contentId;
                        mail.Attachments.Add(barcodeAttactment);

                        content.Coupon = string.Format("<img src='cid:{0}' width='{1}' height='{2}' alt='若未看見條碼請開啟顯示圖片' />", contentId, imageWidth, imageHigh);
                    }
                    else if (codeType == CouponCodeType.Pincode && config.EnableFami3Barcode)
                    {
                        content.EnableFami3Barcode = config.EnableFami3Barcode;
                        content.PinCode = coupon.CouponCode;
                        var famipos = FamiposBarcodeGetCreate(coupon.OrderDetailGuid, vpd.BusinessHourGuid, vpd);
                        //取得barcode39圖檔
                        var barCodeBitmap1 = new Core.Component.BarCode39(famipos.Barcode1, false).Paint();
                        var oStream1 = new MemoryStream();
                        barCodeBitmap1.Save(oStream1, System.Drawing.Imaging.ImageFormat.Png);
                        oStream1.Seek(0, SeekOrigin.Begin);

                        var barCodeBitmap2 = new Core.Component.BarCode39(famipos.Barcode2, false).Paint();
                        var oStream2 = new MemoryStream();
                        barCodeBitmap2.Save(oStream2, System.Drawing.Imaging.ImageFormat.Png);
                        oStream2.Seek(0, SeekOrigin.Begin);

                        var barCodeBitmap3 = new Core.Component.BarCode39(famipos.Barcode3, false).Paint();
                        var oStream3 = new MemoryStream();
                        barCodeBitmap3.Save(oStream3, System.Drawing.Imaging.ImageFormat.Png);
                        oStream3.Seek(0, SeekOrigin.Begin);

                        var contentId = DateTime.Now.Ticks.ToString();

                        var barcodeAttactment1 = new Attachment(oStream1
                            , new System.Net.Mime.ContentType { MediaType = "image/png", Name = "famiBarcode39_1" });
                        barcodeAttactment1.NameEncoding = Encoding.UTF8;
                        barcodeAttactment1.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                        barcodeAttactment1.ContentDisposition.Inline = true;
                        barcodeAttactment1.ContentDisposition.DispositionType = System.Net.Mime.DispositionTypeNames.Inline;
                        barcodeAttactment1.ContentId = string.Format("17life_{0}_1", contentId);

                        var barcodeAttactment2 = new Attachment(oStream2
                            , new System.Net.Mime.ContentType { MediaType = "image/png", Name = "famiBarcode39_2" });
                        barcodeAttactment2.NameEncoding = Encoding.UTF8;
                        barcodeAttactment2.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                        barcodeAttactment2.ContentDisposition.Inline = true;
                        barcodeAttactment2.ContentDisposition.DispositionType = System.Net.Mime.DispositionTypeNames.Inline;
                        barcodeAttactment2.ContentId = string.Format("17life_{0}_2", contentId);

                        var barcodeAttactment3 = new Attachment(oStream3
                            , new System.Net.Mime.ContentType { MediaType = "image/png", Name = "famiBarcode39_3" });
                        barcodeAttactment3.NameEncoding = Encoding.UTF8;
                        barcodeAttactment3.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                        barcodeAttactment3.ContentDisposition.Inline = true;
                        barcodeAttactment3.ContentDisposition.DispositionType = System.Net.Mime.DispositionTypeNames.Inline;
                        barcodeAttactment3.ContentId = string.Format("17life_{0}_3", contentId);

                        mail.Attachments.Add(barcodeAttactment1);
                        mail.Attachments.Add(barcodeAttactment2);
                        mail.Attachments.Add(barcodeAttactment3);

                        content.Coupon = string.Format(@"<img src='cid:{0}' alt='若未看見條碼請開啟顯示圖片' /><br />
                            <img src='cid:{1}' alt='若未看見條碼請開啟顯示圖片' /><br />
                            <img src='cid:{2}' alt='若未看見條碼請開啟顯示圖片' />"
                            , string.Format("17life_{0}_1", contentId)
                            , string.Format("17life_{0}_2", contentId)
                            , string.Format("17life_{0}_3", contentId));
                    }
                    else
                    {
                        content.Coupon = string.Format(I18N.Message.FamiPortPINCouponFormat, coupon.CouponCode) + "<br/>";
                    }

                    content.DealName = vpd.ItemName;
                    content.MailTo = username;
                    var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(vpd);
                    content.Introduction = contentLite.Introduction;
                    content.SellerName = vpd.SellerName;
                    mail.Body = content.ToString();
                    mail.IsBodyHtml = true;
                    PostMan.Instance().Send(mail, SendPriorityType.Immediate);
                }
            }
        }

        public static void SendPponMail(MailContentType mailType, Guid orderGuid)
        {
            SendPponMail(mailType, orderGuid, true, null);
        }

        public static void SendPponMail(MailContentType mailType, Guid orderGuid, bool isRefundCreditcard, CashTrustLogCollection ctCol)
        {
            Order o = op.OrderGet(orderGuid);
            if (o.IsLoaded)
            {
                Member mem = MemberFacade.GetMember(o.UserId);
                if (mem.IsFraudSuspect)
                {
                    return;
                }
            }
            SendPponMail(mailType, orderGuid, isRefundCreditcard, ctCol, string.Empty);
        }

        public static void SendPponMail(MailContentType mailType, Guid orderGuid, bool isRefundCreditcard,
            CashTrustLogCollection ctCol, string atmFailReason, string selectMemberEmail = null)
        {
            ViewPponDeal deal = pp.ViewPponDealGet(op.OrderGet(orderGuid).ParentOrderId.Value);
            ViewPponOrderDetailCollection odc = pp.ViewPponOrderDetailGetList(ViewPponOrderDetail.Columns.OrderGuid, orderGuid, ViewPponOrderDetail.Columns.OrderDetailCreateTime);
            List<ViewPponOrderDetail> regularEntries = odc.Where(i => (i.OrderDetailStatus & (int)OrderDetailStatus.SystemEntry) == 0).ToList();
            List<ViewPponOrderDetail> systemEntries = odc.Where(i => (i.OrderDetailStatus & (int)OrderDetailStatus.SystemEntry) > 0).ToList();

            if (odc.Count > 0)
            {
                Member m = mp.MemberGet(odc.First().MemberEmail);
                foreach (var item in odc)
                {
                    item.MemberName = m.DisplayName;
                }
            }
            ViewPponOrderDetailCollection odcR = new ViewPponOrderDetailCollection();
            foreach (var od in regularEntries)
            {
                odcR.Add(od);
            }

            decimal freight = 0;
            foreach (var od in systemEntries)
            {
                if (od.ItemName.IndexOf("運費") > -1)
                {
                    decimal sDiscouts = 0;
                    foreach (var od2 in systemEntries)
                    {
                        if (od2.ItemName.IndexOf("訂單總額滿") > -1)
                        {
                            sDiscouts = (decimal)od2.OrderDetailTotal;
                            break;
                        }
                    }
                    freight = (decimal)od.OrderDetailTotal - sDiscouts;
                }
            }
            //[2011/08/16]  Ahdaa   修改
            //              Gaby    退貨通知
            //將mail body以及subject整合至GetOrderMailSubjectAndBody函式中
            MailTemplateType mailTemplate;
            MailMessage mail = GetOrderMailSubjectAndBody(mailType, orderGuid, deal, odcR, freight, isRefundCreditcard,
                ctCol, atmFailReason, out mailTemplate);
            try
            {
                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);

                var orderDetailmemberMail = regularEntries.First().MemberEmail.Trim();
                var userEmail = MemberFacade.GetUserEmail(orderDetailmemberMail);
                mail.To.Add(userEmail);

                //20120910_Justin_如果有傳入User Input的email,且格式驗證OK又不跟原本Email重複, 則加寄此Email
                if (!string.IsNullOrEmpty(selectMemberEmail) && RegExRules.CheckEmail(selectMemberEmail) && selectMemberEmail.Trim() != userEmail)
                {
                    mail.To.Add(selectMemberEmail);
                }

                //依據Mail的種類決定要附加的檔案
                string requestUrl = string.Empty;
                PdfConverter pdfc;
                byte[] b;
                Stream s;
                Attachment at;
                string licenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
                switch (mailType)
                {
                    case MailContentType.ReturnApplicationformResend:
                        //20120910_Justin_把退貨申請書的檔案附加在Mail裡面
                        requestUrl = "/user/ReturnApplicationForm.aspx?oid=" + orderGuid.ToString() + "&u=" + HttpUtility.UrlEncode(odcR[0].MemberEmail);        //*********這邊要有會員名字
                        pdfc = new PdfConverter();
                        pdfc.LicenseKey = licenseKey;
                        pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                        pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                        pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
                        pdfc.ScriptsEnabled = pdfc.ScriptsEnabledInImage = false;
                        pdfc.ActiveXEnabled = pdfc.ActiveXEnabledInImage = false;
                        pdfc.PdfDocumentOptions.JpegCompressionEnabled = false;
                        b = pdfc.GetPdfFromUrlBytes(config.SiteUrl + requestUrl);
                        s = new MemoryStream(b);
                        at = new Attachment(s, "ReturnAppForm.pdf");
                        mail.Attachments.Add(at);
                        break;
                    case MailContentType.ReturnDiscountResend:
                        requestUrl = "/user/ReturnDiscount.aspx?oid=" + orderGuid.ToString() + "&u=" + HttpUtility.UrlEncode(odcR[0].MemberEmail);
                        pdfc = new PdfConverter();
                        pdfc.LicenseKey = licenseKey;
                        pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                        pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                        pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
                        pdfc.ScriptsEnabled = pdfc.ScriptsEnabledInImage = false;
                        pdfc.ActiveXEnabled = pdfc.ActiveXEnabledInImage = false;
                        pdfc.PdfDocumentOptions.JpegCompressionEnabled = false;
                        b = pdfc.GetPdfFromUrlBytes(config.SiteUrl + requestUrl);
                        s = new MemoryStream(b);
                        at = new Attachment(s, "ReturnDiscount.pdf");
                        mail.Attachments.Add(at);
                        break;
                }


                mail.IsBodyHtml = true;
                PostMan.Instance().Send(mail, SendPriorityType.Immediate, mailTemplate);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Something wrong while trying to send SendPponMail: Mail Address={0}, {1}, ex={2}",
                    string.Join(",", mail.To.Select(t => t.Address)), Helper.Object2String(mail, "\n"), e);
            }
        }

        public static MailMessage GetOrderMailSubjectAndBody(MailContentType mailType, Guid orderGuid, ViewPponDeal deal,
            ViewPponOrderDetailCollection odcR, decimal freight, bool isRefundCreditcard, CashTrustLogCollection ctCol,
            string atmFailReason, out MailTemplateType mailTemplate)
        {
            mailTemplate = MailTemplateType.Other;
            MailMessage mail = new MailMessage();
            Order o = op.OrderGet(orderGuid);
            switch (mailType)
            {
                case MailContentType.Authorized:      //付款成功
                    if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, (int)GroupOrderStatus.KindDeal))
                    {
                        mail.Subject = string.Format("捐款成功通知({0})", deal.ItemName);
                        mail.Body = GeneratePponKindPaymentSuccessfulConfirmMsg(deal, odcR, orderGuid);
                    }
                    else
                    {
                        mail.Subject = string.Format("付款成功通知({0})", deal.ItemName);
                        mail.Body = GeneratePaymentSuccessfulConfirmMsg(deal, odcR, orderGuid);
                        mailTemplate = MailTemplateType.MemberPponPaymentSuccessful;
                    }
                    break;
                case MailContentType.ReturnFailure:    //退貨失敗
                    mail.Subject = "退貨訂單處理通知";
                    mail.Body = GenerateReturnFailureEmail(deal, odcR, atmFailReason);
                    break;
                case MailContentType.ReturnAll:       //全部退貨
                    mail.Subject = "退貨訂單處理通知";
                    mail.Body = GenerateReturnAllEmail(deal, odcR, o, ctCol);
                    break;
                case MailContentType.ReturnPartial:  //部份退貨
                    mail.Subject = "退貨訂單處理通知";
                    mail.Body = GenerateReturnPartialEmail(deal, odcR, orderGuid, ctCol);
                    break;
                case MailContentType.RefundApply:
                    mail.Subject = "17Life客服中心已收到您的取消訂單申請";
                    mail.Body = GenerateRedeemConfirmMsg<PponAuthorizedMail>(mailType, deal, odcR, freight, isRefundCreditcard);
                    break;
                case MailContentType.RefundResult:
                    mail.Subject = "取消訂單處理完成通知";
                    mail.Body = GenerateRedeemConfirmMsg<PponAuthorizedMail>(mailType, deal, odcR, freight, isRefundCreditcard);
                    break;
                case MailContentType.RefundNoticePaper:
                case MailContentType.RefundNoticeDiscount:
                case MailContentType.RefundNoticeInvoice:
                case MailContentType.RefundNoticeDiscountInvoice:
                case MailContentType.RefundNoticeDiscountPaper:
                case MailContentType.RefundNoticeInvoicePaper:
                    mail.Subject = "退貨申請通知(尚缺退貨文件)";
                    mail.Body = GenerateRefundNoticeEmail(mailType, odcR, ctCol);
                    break;
                case MailContentType.ReturnApplicationformResend:
                    mail.Subject = "退貨申請書";
                    mail.Body = GenerateRefundNoticeEmail(mailType, odcR, ctCol);
                    break;
                case MailContentType.ReturnDiscountResend:
                    mail.Subject = "退回或折讓證明單";
                    mail.Body = GenerateRefundNoticeEmail(mailType, odcR, ctCol);
                    break;
                case MailContentType.RefundCreditcard:
                    mail.Subject = "購物金轉退現金通知";
                    mail.Body = RefundCreditcardEmail(deal, odcR, o, ctCol);
                    break;
                default:
                    mail.Subject = "客服通知";
                    mail.Body = GenerateRedeemConfirmMsg<PponAuthorizedMail>(mailType, deal, odcR, freight, isRefundCreditcard);
                    break;
            }
            return mail;
        }

        /// <summary>
        /// 新退貨:寄 "退貨申請通知信" 給消費者
        /// </summary>
        public static void SendCustomerReturnFormApplicationMail(int returnFormId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(returnFormId);

            if (returnForm == null)
            {
                return;
            }

            CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(returnForm.OrderGuid, OrderClassification.LkSite);
            ViewPponOrder o = pp.ViewPponOrderGet(returnForm.OrderGuid);
            ViewPponDeal d = pp.ViewPponDealGetByBusinessHourGuid(o.BusinessHourGuid);


            int itemCount = ctCol.Count(x => !Helper.IsFlagSet(x.SpecialStatus, (int)TrustSpecialStatus.Freight));
            int returnCount = returnForm.ReturnFormRefunds.Count();
            var einvoice = op.EinvoiceMainGetListByOrderGuid(o.Guid);
            var isPapered = einvoice.Any(x => x.InvoicePapered);
            RefundFormNoticeMail template;
            RefundFormNoticeMailP templateP;
            MailMessage mail = new MailMessage();

            MailTemplateType templateType = MailTemplateType.Other;
            if (returnForm.DeliveryType == DeliveryType.ToShop)
            {
                //新版憑證退貨通知信
                template = TemplateFactory.Instance().GetTemplate<RefundFormNoticeMail>();
                template.MemberName = o.MemberName;
                template.OrderGuid = returnForm.OrderGuid.ToString();
                template.OrderID = o.OrderId;
                template.ItemName = d.ItemName;
                template.IsShowAllowance = ReturnService.IsPaperAllowance(returnForm.Id) && isPapered; //改成判斷是否有預設紙本折讓且有印製
                template.IsInvoiced = (einvoice.Where(x => x.InvoiceNumber != null).Where(x => x.InvoiceNumber != "").Count() > 0 ? true : false);
                template.CouponCounts = itemCount.ToString();
                template.ReturnCouponCounts = returnCount.ToString();
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;
                template.UseNewRefund = true;
                mail.Body = template.ToString();
                templateType = MailTemplateType.MemberRefundFormNoticeMail;
            }
            else
            {
                int comboCount = ReturnService.QueryComboPackCount(returnForm.OrderGuid);   //為了顯示 "組" 這個字

                //新版宅配退貨通知信
                templateP = TemplateFactory.Instance().GetTemplate<RefundFormNoticeMailP>();
                templateP.MemberName = o.MemberName;
                templateP.OrderGuid = returnForm.OrderGuid.ToString();
                templateP.OrderID = o.OrderId;
                templateP.ItemName = d.IsWms ? "【24H到貨】" + d.ItemName : d.ItemName;
                templateP.WmsCnName = d.IsWms ? "【24H到貨】" : string.Empty;
                templateP.IsShowAllowance = ReturnService.IsPaperAllowance(returnForm.Id) && isPapered;//改成判斷是否有預設紙本折讓且有印製
                templateP.IsInvoiced = (einvoice.Where(x => x.InvoiceNumber != null).Where(x => x.InvoiceNumber != "").Count() > 0 ? true : false);
                templateP.CouponCounts = string.Format("{0}{1}", itemCount.ToString(), comboCount > 1 ? "組" : string.Empty);
                templateP.ReturnCouponCounts = string.Format("{0}{1}", returnCount.ToString(), comboCount > 1 ? "組" : string.Empty);
                templateP.SiteUrl = config.SiteUrl;
                templateP.SiteServiceUrl = config.CustomerSiteService;
                templateP.UseNewRefund = true;
                mail.Body = templateP.ToString();
                templateType = MailTemplateType.MemberRefundFormNoticeMailP;
            }

            try
            {
                mail.To.Add(MemberFacade.GetUserEmail(o.MemberEmail));
                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                mail.IsBodyHtml = true;
                mail.Subject = "退貨申請通知";
                PostMan.Instance().Send(mail, SendPriorityType.Async, templateType);
            }
            catch (Exception ex)
            {
                logger.Error("Error occured in sending RefundNoticeMail", ex);
            }
        }

        /// <summary>
        /// 寄送 "退貨完成通知" 給消費者
        /// </summary>
        public static void SendCustomerRefundCompleteNoticeMail(ReturnFormEntity returnEntity, ViewPponDeal deal)
        {
            ViewPponOrder order = pp.ViewPponOrderGet(returnEntity.OrderGuid);

            RefundCompleteNoticeMail template = TemplateFactory.Instance().GetTemplate<RefundCompleteNoticeMail>();
            RefundedAmount summary = returnEntity.GetRefundedAmount();
            var einvoice = op.EinvoiceMainGetListByOrderGuid(order.Guid);

            template.OrderID = order.OrderId;
            template.OrderGuid = order.Guid.ToString();
            template.MemberName = order.MemberName;
            template.ItemName = deal.ItemName;
            template.ReturnedCounts = returnEntity.ReturnFormRefunds.Count(x => x.IsRefunded == true);
            template.ReturnPcash = summary.PCash;
            template.ReturnCreditcard = summary.CreditCard;
            template.ReturnBcash = summary.BCash;
            template.ReturnScash = summary.SCash;
            template.ReturnPScash = summary.Pscash;
            template.ReturnAtm = summary.Atm;
            template.ReturnTcash = summary.TCash;
            //退貨完成的話，二聯才顯示折讓單下載扭
            template.IsShowAllowance = (einvoice.Any(x => x.AllowanceStatus == (int)AllowanceStatus.ElecAllowance) ? true : false);
            template.IsInvoiced = (einvoice.Where(x => x.InvoiceNumber != null).Where(x => x.InvoiceNumber != "").Count() > 0 ? true : false);

            if (summary.TCash > 0)
            {
                template.ThirdPartyPayment = Helper.GetEnumDescription(returnEntity.ThirdPartyPaymentSystem);
            }
            template.ReturnTotal = summary.PCash + summary.CreditCard + summary.BCash + summary.SCash + summary.Pscash + summary.Atm + summary.TCash;

            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                mail.To.Add(MemberFacade.GetUserEmail(order.MemberEmail));  //order.MemberEmail 即 Member UserName
                mail.Subject = "退貨成功通知";
                mail.Body = template.ToString();
                mail.IsBodyHtml = true;
                PostMan.Instance().Send(mail, SendPriorityType.Async);
            }
            catch (Exception ex)
            {
                logger.Error("Error occured in sending RefundCompleteNoticeMail", ex);
            }
        }

        /// <summary>
        /// 通知消費者 系統自動將未使用之過期憑證轉退維購物金
        /// </summary>
        public static void SendCustomerRefundScashCompleteNoticeMail(ReturnFormEntity returnEntity, ViewPponDeal deal)
        {
            ViewPponOrder order = pp.ViewPponOrderGet(returnEntity.OrderGuid);
            ExpiredCouponRefundScashNotify template = TemplateFactory.Instance().GetTemplate<ExpiredCouponRefundScashNotify>();
            RefundedAmount summary = returnEntity.GetRefundedAmount();
            template.MemberName = order.MemberName;
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;
            template.OrderId = order.OrderId;
            template.ItemUrl = config.SiteUrl + "/" + deal.BusinessHourGuid;
            template.ItemName = deal.ItemName;
            template.ItemPrice = Convert.ToInt32(deal.ItemPrice).ToString();
            template.RefundScash = summary.SCash.ToString();
            template.MemberScash = Convert.ToInt32(GetSCashSum(order.UserId)).ToString();
            template.PromoDeals = GetPromoDealsForNoticeMail(deal);

            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                mail.To.Add(MemberFacade.GetUserEmail(order.MemberEmail));
                mail.Subject = "您購買的好康[" + deal.ItemName + "]，已成功轉購物金";
                mail.Body = template.ToString();
                mail.IsBodyHtml = true;
                PostMan.Instance().Send(mail, SendPriorityType.Async);
            }
            catch (Exception ex)
            {
                logger.Error("Error occured in sending RefundCompleteNoticeMail", ex);
            }
        }

        /// <summary>
        /// 台新儲值支付 未成單退款通知
        /// </summary>
        public static bool SendTaishinPayCancelOrderNoticeMail(Order o, PaymentTransactionCollection ptc)
        {
            try
            {
                GroupOrder gpo = op.GroupOrderGetByOrderGuid(o.ParentOrderId.Value);
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(gpo.BusinessHourGuid);

                Member m = mp.MemberGetbyUniqueId(o.UserId);

                MailMessage mail = new MailMessage();
                mail.Subject = string.Format("訂單退貨處理通知({0})", deal.ItemName);
                mail.Body = GenerateTaishinPayCancelOrderMsg(o, ptc, deal, m.DisplayName);
                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                mail.To.Add(m.UserEmail);
                mail.IsBodyHtml = true;
                PostMan.Instance().Send(mail, SendPriorityType.Immediate);

                return true;
            }
            catch (Exception ex)
            {
                logger.Error("台新儲值支付，未成單退款通知信發生錯誤! " + ex);
                return false;
            }
        }

        /// <summary>
        /// email取得亂數檔次用
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static List<IViewPponDeal> GetRandomCityDeal(Guid bid)
        {
            bool all = false;
            bool beuty = false;
            bool travel = false;
            List<IViewPponDeal> cityDeal = new List<IViewPponDeal>();
            List<IDealTimeSlot> dc = ViewPponDealManager.DefaultManager.GetDealTimeSlotInEffectiveTime()
                .Where(x => x.BusinessHourGuid.Equals(bid)).ToList();
            foreach (var ds in dc)
            {
                if (ds.CityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
                {
                    all = true;
                }

                if (ds.CityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
                {
                    beuty = true;
                }

                if (ds.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
                {
                    travel = true;
                }
            }

            if (all)
            {
                foreach (var randomAllDeal in ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId).OrderBy(x => Guid.NewGuid()).Take(3))
                {
                    cityDeal.Add(randomAllDeal);
                }
            }
            else if (beuty)
            {
                foreach (var randomBeautyDeal in ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId).OrderBy(x => Guid.NewGuid()).Take(3))
                {
                    cityDeal.Add(randomBeautyDeal);
                }
            }
            else if (travel)
            {
                foreach (var randomTravelDeal in ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(PponCityGroup.DefaultPponCityGroup.Travel.CityId).OrderBy(x => Guid.NewGuid()).Take(3))
                {
                    cityDeal.Add(randomTravelDeal);
                }
            }
            else
            {
                foreach (var randomDeal in ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(((dc.Count == 0) ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId : dc.First().CityId)).OrderBy(x => Guid.NewGuid()).Take(3))
                {
                    cityDeal.Add(randomDeal);
                }
            }

            return cityDeal;
        }

        /// <summary>
        /// 通知信推薦好康
        /// </summary>
        /// <param name="bid">通知信內之檔次</param>
        /// <param name="allDealsCount">總檔次數</param>
        /// <param name="cityDealsCount">通知信檔次之城市檔次數</param>
        /// <returns></returns>
        public static List<IViewPponDeal> GetRandomDeal(Guid bid, int allDealsCount, int cityDealsCount)
        {
            List<IDealTimeSlot> slots = ViewPponDealManager.DefaultManager.GetDealTimeSlotInEffectiveTime()
                .Where(x => x.BusinessHourGuid == bid).ToList();

            // 1.取得檔次上檔在所有City，並取其中的第一筆City做為推薦依據
            // 2. 若DealTimeSlot快取中已查無此檔，則推薦TaipeiCity檔次
            // 3. 不足的部份(cityDealsCount < allDealsCount)搭配一定比例之宅配檔
            Dictionary<Guid, IViewPponDeal> promoDeals =
                ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot((slots.Count == 0)
                ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId
                : slots.First().CityId)
                .OrderBy(x => Guid.NewGuid())
                .Take(allDealsCount + 1)
                .Where(x => !x.BusinessHourGuid.Equals(bid))
                .GroupBy(x => x.BusinessHourGuid)
                .Select(group => @group.First())
                .Take(cityDealsCount)
                .ToDictionary(x => x.BusinessHourGuid, x => x);

            if (allDealsCount != cityDealsCount) //若總檔次數量=城市檔次表示不搭配宅配檔次
            {
                List<IViewPponDeal> allCounctrys =
                    ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(
                        PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
                        .Where(x => !x.BusinessHourGuid.Equals(bid))
                        .OrderBy(x => Guid.NewGuid())
                        .Take(allDealsCount + 1)
                        .ToList();
                int i = 0;
                while (promoDeals.Count < allCounctrys.Count && promoDeals.Count < allDealsCount)
                {
                    if (!promoDeals.ContainsKey(allCounctrys[i].BusinessHourGuid))
                    {
                        promoDeals.Add(allCounctrys[i].BusinessHourGuid, allCounctrys[i]);
                    }

                    i++;
                }
            }

            return new List<IViewPponDeal>(promoDeals.Values.ToList());
        }

        /// <summary>
        /// 通知信推薦好康(指定城市)
        /// </summary>
        /// <param name="cityId">指定城市CityId</param>
        /// <param name="allDealsCount">總檔次數</param>
        /// <param name="cityDealsCount">指定城市檔次數</param>
        /// <returns></returns>
        public static List<IViewPponDeal> GetRandomDeal(int cityId, int allDealsCount, int cityDealsCount)
        {
            Dictionary<Guid, IViewPponDeal> promoDeals = new Dictionary<Guid, IViewPponDeal>();

            // 推薦檔次取該城市之檔次外，另搭配一定比例之宅配檔
            promoDeals = ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(cityId).OrderBy(x => Guid.NewGuid()).Take(cityDealsCount).ToDictionary(x => x.BusinessHourGuid, x => x);

            List<IViewPponDeal> allCounctrys = ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId).OrderBy(x => Guid.NewGuid()).Take(allDealsCount).ToList();
            int i = 0;
            while (promoDeals.Count < allCounctrys.Count && promoDeals.Count < allDealsCount)
            {
                if (!promoDeals.ContainsKey(allCounctrys[i].BusinessHourGuid))
                {
                    promoDeals.Add(allCounctrys[i].BusinessHourGuid, allCounctrys[i]);
                }

                i++;
            }
            return new List<IViewPponDeal>(promoDeals.Values.ToList());
        }

        /// <summary>
        /// 推薦好康(指定城市)
        /// </summary>
        /// <param name="categoryNode">分類，但其實也是要取其CityId</param>
        /// <param name="allDealsCount">總檔次數</param>
        /// <param name="cityDealsCount">指定城市檔次數，不足以宅配檔補</param>
        /// <returns></returns>
        public static List<IViewPponDeal> GetRandomDeal(CategoryNode categoryNode, int allDealsCount, int cityDealsCount)
        {
            var pponMmDeals = ViewPponDealManager.DefaultManager
                .ViewPponDealGetListForPponByCityWithDataSlot(categoryNode.CityId.GetValueOrDefault());

            var deliveryMmDeals = ViewPponDealManager.DefaultManager
                .ViewPponDealGetListForPponByCityWithDataSlot(CategoryManager.Default.Delivery.CityId.GetValueOrDefault());

            List<IViewPponDeal> result = new List<IViewPponDeal>();
            int pponGetCount = cityDealsCount;
            int deliverGetCount = allDealsCount - cityDealsCount;
            if (cityDealsCount > pponMmDeals.Count)
            {
                pponGetCount = pponMmDeals.Count;
                deliverGetCount = allDealsCount - pponMmDeals.Count;
            }

            result.AddRange(
                pponMmDeals.OrderBy(t => Guid.NewGuid()).Take(pponGetCount)
                );
            result.AddRange(
                deliveryMmDeals.OrderBy(t => Guid.NewGuid()).Take(deliverGetCount)
                );

            return result;
        }

        public static ViewPponOrderDetail TransCouponToOrder(ViewPponCoupon vpc)
        {
            ViewPponOrderDetail vpod = new ViewPponOrderDetail();
            vpod.ItemName = vpc.ItemName;
            vpod.MemberName = vpc.MemberName;
            vpod.DeliveryAddress = vpc.DeliveryAddress;
            vpod.MemberEmail = vpc.MemberEmail;
            return vpod;
        }

        public static void SendPponDealFailMail(ViewPponDeal deal)
        {
            PponDealFailMail template = TemplateFactory.Instance().GetTemplate<PponDealFailMail>();
            template.TheDeal = deal;
            template.config = config;

            OrderCollection orderCol = op.OrderGetListPaging(0, 0, OrderStatus.Nothing, null, Order.Columns.ParentOrderId + "=" + deal.OrderGuid);
            foreach (var o in orderCol)
            {
                CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(o.Guid, OrderClassification.LkSite);
                if (ctCol.Count > 0)
                {
                    bool haveToSend = false;
                    foreach (var ct in ctCol)
                    {
                        if (ct.Status == (int)TrustStatus.Initial || ct.Status == (int)TrustStatus.ATM)
                        {
                            haveToSend = true;
                        }
                    }
                    if (haveToSend)
                    {
                        List<ViewPponOrderDetail> odCol =
                            pp.ViewPponOrderDetailGetList(ViewPponOrderDetail.Columns.OrderGuid, o.Guid, ViewPponOrderDetail.Columns.OrderDetailCreateTime)
                            .Where(x => x.OrderDetailStatus == (int)OrderDetailStatus.None).ToList();

                        if (odCol.Count > 0)
                        {
                            template.TheOrderDetail = odCol.First();
                            template.PromoDeals = GetPromoDealsForNoticeMail((odCol.Count > 0 && odCol.First().BusinessHourGuid != null) ? (Guid)odCol.First().BusinessHourGuid : Guid.Empty);
                            MailMessage mail = new MailMessage();

                            mail.Subject = "揪團失敗通知(" + deal.SellerName + ")";
                            mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                            mail.To.Add(MemberFacade.GetUserEmail(odCol.First().MemberEmail));
                            mail.Body = template.ToString();
                            mail.IsBodyHtml = true;
                            PostMan.Instance().Send(mail, SendPriorityType.Immediate);
                        }
                    }
                }
            }
        }

        public static void SendPponChargingFailMail(PaymentTransaction pt)
        {
            Order o = op.OrderGet(Order.Columns.Guid, pt.OrderGuid);
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            mail.To.Add(config.ServiceEmail);
            mail.Subject = string.Format("17Life授權失敗通知, 訂單編號:{0}", o.OrderId);
            mail.Body = "<html><body>" + "訂單編號:" + o.OrderId + "<br />" +
                                        "交易時間:" + pt.TransTime + "<br />" +
                                        "交易金額:" + pt.Amount + "</body></html>";
            mail.IsBodyHtml = true;
            PostMan.Instance().Send(mail, SendPriorityType.Immediate);
        }

        /// <summary>
        /// 寄 "換貨申請通知信" 給消費者
        /// </summary>
        public static void SendCustomerExchangeApplicationMail(int exchangeLogId)
        {
            var exchangeLog = op.ViewOrderReturnListGet(exchangeLogId); ;

            if (exchangeLog == null)
            {
                return;
            }

            var mail = new MailMessage();

            //換貨申請通知信
            var template = TemplateFactory.Instance().GetTemplate<ExchangeApplicationMail>();
            template.MemberName = exchangeLog.MemberName;
            template.OrderID = exchangeLog.OrderId;
            template.ItemName = exchangeLog.ItemName;
            template.Reason = exchangeLog.Reason.Replace("\n", "<br/>");
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;
            mail.Body = template.ToString();

            try
            {
                mail.To.Add(MemberFacade.GetUserEmail(exchangeLog.MemberEmail));
                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                mail.IsBodyHtml = true;
                mail.Subject = "換貨申請通知";
                PostMan.Instance().Send(mail, SendPriorityType.Async);
            }
            catch (Exception ex)
            {
                logger.Error("Error occured in sending RefundNoticeMail", ex);
            }
        }

        /// <summary>
        /// 寄送 "換貨完成通知" 給消費者
        /// </summary>
        public static void SendCustomerExchangeCompleteMail(int exchangeLogId)
        {
            var exchangeLog = op.ViewOrderReturnListGet(exchangeLogId); ;

            if (exchangeLog == null)
            {
                return;
            }

            var mail = new MailMessage();

            //換貨完成通知信
            var template = TemplateFactory.Instance().GetTemplate<ExchangeCompleteMail>();
            template.MemberName = exchangeLog.MemberName;
            template.OrderID = exchangeLog.OrderId;
            template.ItemName = exchangeLog.ItemName;
            template.Reason = exchangeLog.Reason;
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;
            mail.Body = template.ToString();

            try
            {
                mail.To.Add(MemberFacade.GetUserEmail(exchangeLog.MemberEmail));
                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                mail.IsBodyHtml = true;
                mail.Subject = "換貨完成通知";
                PostMan.Instance().Send(mail, SendPriorityType.Async);
            }
            catch (Exception ex)
            {
                logger.Error("Error occured in sending RefundNoticeMail", ex);
            }
        }

        #endregion

        #region Shoppingcart to order detail

        private static OrderDetailCollection Cart2OrderDetail(ViewShoppingCartItemCollection carts, Guid orderGuid, string createId,
          out OrderDetailItemCollection odItemCol)
        {
            if (createId.Length > OrderDetail.CreateIdColumn.MaxLength)
            {
                createId = createId.Substring(0, OrderDetail.CreateIdColumn.MaxLength);
            }
            OrderDetailCollection ordDtlCol = new OrderDetailCollection();
            odItemCol = new OrderDetailItemCollection();
            foreach (ViewShoppingCartItem cart in carts)
            {
                OrderDetail od = new OrderDetail();

                od.Status = (int)OrderDetailStatus.None;
                od.ItemGuid = cart.ItemGuid;

                od.ItemName = cart.ItemName.Split('|')[0];
                od.ItemQuantity = cart.ItemQuantity;
                od.ItemUnitPrice = cart.ItemUnitPrice;
                od.Total = od.ItemQuantity * od.ItemUnitPrice;
                od.ItemId = cart.ItemId;
                od.StoreGuid = cart.StoreGuid;

                od.CreateId = createId;
                od.CreateTime = DateTime.Now;
                od.Guid = Helper.GetNewGuid(od);
                od.OrderGuid = orderGuid;

                ordDtlCol.Add(od);

                //處理 OrderDetailItem
                ItemEntry item = ser.Deserialize<ItemEntry>(cart.ItemData);
                if (item.SubItems.Count > 0)
                {
                    //成套販售
                    foreach (var subItem in item.SubItems)
                    {
                        odItemCol.Add(new OrderDetailItem
                        {
                            ItemName = subItem.Name,
                            Options = subItem.GetOptions(),
                            Quantity = 1,
                            OrderDetailGuid = od.Guid
                        });
                    }
                }
                else
                {
                    odItemCol.Add(new OrderDetailItem
                    {
                        ItemName = item.Name,
                        Options = item.GetOptions(),
                        Quantity = cart.ItemQuantity,
                        OrderDetailGuid = od.Guid
                    });
                }
            }
            return ordDtlCol;
        }

        private static void Cart2OrderProduct(ViewShoppingCartItemCollection carts, Guid bid, Guid orderGuid, int multiSaleBase = 1)
        {
            //The key of OrderProduct is a database-generated identity.
            //OrderProductOption needs the key of OrderProduct, so OrderProduct must be persisted first.
            //Therefore, we cannot defer the persistance of OrderProduct at the end of MakeOrder.
            DealProperty dp = pp.DealPropertyGet(bid);
            foreach (ViewShoppingCartItem cart in carts)
            {
                ItemEntry item = ser.Deserialize<ItemEntry>(cart.ItemData);

                // 是否一般商品 (非多選項)
                bool isNonMultiOption = item.SelectedAccessory.Count == item.SubItems.Count;

                // 是否多選項商品
                bool isMultiOption = item.SelectedAccessory.Count > 0;

                // 是否成套販售
                bool isComboPack = item.SubItems.Count > 0;

                if (!(isNonMultiOption ^ isMultiOption ^ isComboPack))
                {
                    // 應該只有一個條件會成立, 不是的話就有問題!
                    throw new Exception("Something is wrong here! Check deal settings!!");
                }

                #region 一般商品 (非多選項)

                if (isNonMultiOption)
                {
                    for (int i = 0; i < cart.ItemQuantity * multiSaleBase; i++)
                    {
                        var product = new OrderProduct();
                        product.OrderGuid = orderGuid;
                        product.Name = cart.ItemItemName;
                        product.IsOriginal = true;
                        product.IsCurrent = true;
                        product.IsReturning = false;
                        product.IsExchanging = false;

                        op.OrderProductSet(product);
                    }
                }

                #endregion 一般商品 (非多選項)

                #region 多選項商品

                if (isMultiOption)
                {
                    var options = new OrderProductOptionCollection();   //所有 product 的 options

                    for (int i = 0; i < cart.ItemQuantity; i++)
                    {
                        var product = new OrderProduct();
                        product.OrderGuid = orderGuid;
                        product.Name = cart.ItemItemName;
                        product.IsOriginal = true;
                        product.IsCurrent = true;
                        product.IsReturning = false;
                        product.IsExchanging = false;

                        op.OrderProductSet(product);

                        foreach (AccessoryEntry acc in item.SelectedAccessory)
                        {
                            if (ProposalFacade.IsVbsProposalNewVersion() && dp.IsHouseNewVer && dp.DeliveryType == (int)DeliveryType.ToHouse)
                            {
                                options.Add(new OrderProductOption
                                {
                                    OrderProductId = product.Id,
                                    OptionId = FindOptionIdByOriOption(acc.AccessoryGroupMemberGUID, bid, orderGuid),
                                    IsOldProduct = false
                                });
                            }
                            else
                            {
                                options.Add(new OrderProductOption
                                {
                                    OrderProductId = product.Id,
                                    OptionId = FindOptionIdByAccessoryGroupMemberGuid(acc.AccessoryGroupMemberGUID, bid, orderGuid),
                                    IsOldProduct = false
                                });
                            }
                        }

                    }

                    op.OrderProductOptionSetList(options);
                }

                #endregion 多選項商品

                #region 成套販售

                if (isComboPack)
                {
                    var options = new OrderProductOptionCollection(); //所有 product 的 options

                    foreach (var subItem in item.SubItems)
                    {
                        var product = new OrderProduct();
                        product.OrderGuid = orderGuid;
                        product.Name = cart.ItemItemName;
                        product.IsOriginal = true;
                        product.IsCurrent = true;
                        product.IsReturning = false;
                        product.IsExchanging = false;

                        op.OrderProductSet(product);

                        foreach (AccessoryEntry acc in subItem.SelectedAccessory)
                        {
                            if (ProposalFacade.IsVbsProposalNewVersion() && dp.IsHouseNewVer && dp.DeliveryType == (int)DeliveryType.ToHouse)
                            {
                                options.Add(new OrderProductOption
                                {
                                    OrderProductId = product.Id,
                                    OptionId = FindOptionIdByOriOption(acc.AccessoryGroupMemberGUID, bid, orderGuid),
                                    IsOldProduct = false
                                });
                            }
                            else
                            {
                                options.Add(new OrderProductOption
                                {
                                    OrderProductId = product.Id,
                                    OptionId = FindOptionIdByAccessoryGroupMemberGuid(acc.AccessoryGroupMemberGUID, bid, orderGuid),
                                    IsOldProduct = false
                                });
                            }

                        }
                    }

                    op.OrderProductOptionSetList(options);
                }

                #endregion

            }
        }

        private static int FindOptionIdByAccessoryGroupMemberGuid(Guid accessoryGroupMemberGuid, Guid bid, Guid orderGuid)
        {
            int? result = PponOption.FindId(accessoryGroupMemberGuid, bid);

            if (!result.HasValue)
            {
                string message = string.Format(@"找不到 ppon_option!
accessory_group_member_guid: {0}
bid: {1}
order_guid: {3}

處理方式:
請到檔次設定頁變更選項內容 (example: 把其中一個選項數量連同中括弧拿掉, 儲存, 再加回來)
({2}/controlroom/ppon/setup.aspx?bid={1})"
                    , accessoryGroupMemberGuid.ToString()
                    , bid.ToString()
                    , config.SiteUrl
                    , orderGuid.ToString());


                //logger.Warn(message);

                MailMessage msg = new MailMessage();
                msg.To.Add(config.ItTesterEmail);
                msg.Subject = Environment.MachineName + ": 找不到 ppon_option!";
                msg.From = new MailAddress(config.AdminEmail);
                msg.Body = message;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);

                return 0;
            }
            else
            {
                return result.Value;
            }
        }

        private static int FindOptionIdByOriOption(Guid accessoryGroupMemberGuid, Guid bid, Guid orderGuid)
        {
            int result = 0;
            string result_id = pp.PponOptionGetIdByGuid(accessoryGroupMemberGuid);
            int.TryParse(result_id, out result);

            if (result == 0)
            {
                string message = string.Format(@"找不到 ppon_option!
ppon_option_guid: {0}
bid: {1}
order_guid: {3}

處理方式:
請到檔次設定頁變更選項內容 (example: 把其中一個選項數量連同中括弧拿掉, 儲存, 再加回來)
({2}/controlroom/ppon/setup.aspx?bid={1})"
                    , accessoryGroupMemberGuid.ToString()
                    , bid.ToString()
                    , config.SiteUrl
                    , orderGuid.ToString());


                //logger.Warn(message);

                MailMessage msg = new MailMessage();
                msg.To.Add(config.ItTesterEmail);
                msg.Subject = Environment.MachineName + ": 找不到 ppon_option!";
                msg.From = new MailAddress(config.AdminEmail);
                msg.Body = message;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);

                return 0;
            }
            else
            {
                return result;
            }
        }

        /// <summary>
        /// 0 元活動沒有選項
        /// </summary>
        private static void MakeZeroOrderProduct(Guid orderGuid, string itemName, int itemQuantity)
        {
            OrderProductCollection products = new OrderProductCollection();

            for (int i = 0; i < itemQuantity; i++)
            {
                var product = new OrderProduct();
                product.OrderGuid = orderGuid;
                product.Name = itemName;
                product.IsOriginal = true;
                product.IsCurrent = true;
                product.IsReturning = false;
                product.IsExchanging = false;

                products.Add(product);
            }

            op.OrderProductSetList(products);
        }

        private static OrderDetailCollection MakeOrderDetail(Guid itemGuid, string itemName, int itemQuantity, decimal itemPrice, Guid orderGuid, string createId, string consumerName, string consumerDept, string consumerTelExt, Guid storeGuid)
        {
            OrderDetailCollection ordDtlCol = new OrderDetailCollection();
            OrderDetail od = new OrderDetail();

            od.Status = (int)OrderDetailStatus.None;
            od.ItemGuid = itemGuid;

            od.ItemName = itemName.Split('|')[0];
            od.ItemQuantity = itemQuantity;
            od.ItemUnitPrice = itemPrice;
            od.Total = od.ItemQuantity * od.ItemUnitPrice;
            //od.ItemId = item.ItemId;
            if (!string.IsNullOrEmpty(consumerTelExt))
            {
                if (consumerTelExt.Length > OrderDetail.ConsumerTeleExtColumn.MaxLength)
                {
                    consumerTelExt = consumerTelExt.Substring(0, OrderDetail.ConsumerTeleExtColumn.MaxLength);
                }

                od.ConsumerTeleExt = consumerTelExt;
            }
            if (!string.IsNullOrEmpty(consumerDept))
            {
                if (consumerDept.Length > OrderDetail.ConsumerGroupColumn.MaxLength)
                {
                    consumerDept = consumerDept.Substring(0, OrderDetail.ConsumerGroupColumn.MaxLength);
                }

                od.ConsumerGroup = consumerDept;
            }
            if (!string.IsNullOrEmpty(consumerName))
            {
                if (consumerName.Length > OrderDetail.ConsumerNameColumn.MaxLength)
                {
                    consumerName = consumerName.Substring(0, OrderDetail.ConsumerNameColumn.MaxLength);
                }

                od.ConsumerName = consumerName;
            }
            else if (itemName.Split('|').Length > 1)
            {
                od.ConsumerName = itemName.Split('|')[1];
            }

            if (createId.Length > OrderDetail.CreateIdColumn.MaxLength)
            {
                createId = createId.Substring(0, OrderDetail.CreateIdColumn.MaxLength);
            }

            od.CreateId = createId;
            od.CreateTime = DateTime.Now;
            od.Guid = Helper.GetNewGuid(od);
            od.OrderGuid = orderGuid;
            od.StoreGuid = storeGuid;
            ordDtlCol.Add(od);

            return ordDtlCol;
        }

        #endregion

        #region Shopping cart management

        /// <summary>
        /// 將購買項目放到購物車，如果有資料己存在，會清除
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="item"></param>
        public static void PutItemToCart(string ticketId, ItemEntry item)
        {
            PutItemToCart(ticketId, new List<ItemEntry> { item });
        }

        /// <summary>
        /// 將購買項目放到購物車，如果有資料己存在，會清除
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        public static void PutItemToCart(string ticketId, List<ItemEntry> items)
        {
            //淨空，如果有己存在資料
            op.ShoppingCartDeleteByTicketId(ticketId);

            foreach (ItemEntry item in items)
            {
                // don't re-arrange the order of the following two lines and don't insert anything between it, we hope this can minimize the
                // over-ordering caused by simultaneous clicks to put items into cart by customers
                Item iq = ip.ItemGet(item.Id);

                string itemName = string.IsNullOrEmpty(item.Name) ? iq.ItemName : item.ToString("&nbsp");

                ShoppingCart cart = new ShoppingCart();
                cart.ItemGuid = item.Id;
                cart.ItemName = itemName;
                cart.ItemUnitPrice = iq.ItemPrice;
                cart.TicketId = ticketId;
                cart.CreateTime = DateTime.Now;
                cart.StoreGuid = item.StoreGuid;
                cart.ItemQuantity = item.OrderQuantity;
                cart.ItemData = ser.Serialize(item);
                op.ShoppingCartSet(cart);
            }
        }

        public static bool PponSetCloseTimeIfJustReachMinimum(IViewPponDeal deal, int orderedQty)
        {
            if (orderedQty <= 0 || deal.GroupOrderGuid == null)
            {
                return false;
            }

            int origQty = deal.OrderedQuantity ?? 0;
            if (origQty < deal.BusinessHourOrderMinimum && deal.BusinessHourOrderMinimum <= (origQty + orderedQty))
            {
                GroupOrder go = op.GroupOrderGet(deal.GroupOrderGuid.Value);
                go.CloseTime = DateTime.Now;
                op.GroupOrderSet(go);

                return true;
            }
            return false;
        }

        public static void DeleteItemInCart(string ticketid)
        {
            op.ShoppingCartDeleteByTicketId(ticketid);
        }

        #endregion

        #region Ppon related
        public static int GenerateCoupon(Guid bizHourGuid)
        {
            ViewPponOrderDetailCollection odc = pp.ViewPponOrderDetailGetList(ViewPponOrderDetail.Columns.BusinessHourGuid, bizHourGuid,
                                                                              ViewPponOrderDetail.Columns.OrderDetailCreateTime);
            IEnumerable<ViewPponOrderDetail> odCol = from x in odc where (x.OrderDetailStatus != (int)OrderDetailStatus.SystemEntry) select x;
            if (odCol.Count() == 0 || odCol.ElementAt(0).ParentOrderId == null)
            {
                return 0;
            }

            ViewMemberGroupOrder parentOrder = op.ViewMemberGroupOrderGet(ViewMemberGroupOrder.Columns.OrderGuid, odCol.ElementAt(0).ParentOrderId.Value);
            CouponCollection couponCol = new CouponCollection();
            foreach (var od in odCol)
            {
                if (Helper.IsFlagSet(od.OrderStatus, OrderStatus.Cancel)) // skip cancelled order (let's not skip test order for now...)
                {
                    continue;
                }

                for (int i = 0; i < od.ItemQuantity; i++)
                {
                    Coupon c = new Coupon();
                    c.OrderDetailId = od.OrderDetailGuid;
                    c.SequenceNumber = GenerateCouponSequence(parentOrder.OrderId, od.MemberEmail, couponCol.Count + 1);
                    c.Code = GenerateCouponVerification(c.SequenceNumber, parentOrder.OrderId);
                    couponCol.Add(c);
                }
            }

            if (couponCol.Count > 0)
            {
                pp.CouponSetBulk(couponCol);
                GroupOrder go = op.GroupOrderGet(parentOrder.GroupOrderGuid);
                go.Status = (int)Helper.SetFlag(true, go.Status ?? 0, GroupOrderStatus.PponCouponGenerated);
                op.GroupOrderSet(go);
            }

            return couponCol.Count;
        }

        public static void GenerateCouponByCouponCol(ViewPponCouponCollection cc)
        {
            int si = -1;

            IEnumerable<ViewPponCoupon> odCol = from x in cc where (x.OrderDetailStatus != (int)OrderDetailStatus.SystemEntry) select x;
            if (odCol.Count() != 0 && odCol.ElementAt(0).ParentOrderId != null)
            {
                foreach (var od in odCol)
                {
                    CouponCollection CouponCol = new CouponCollection();
                    //取得成套部分已產生憑證
                    CouponCol.AddRange(pp.CouponGetList(od.OrderDetailGuid));

                    for (int i = 0; i < od.ItemQuantity; i++)
                    {
                        Coupon c = new Coupon();

                        try
                        {
                            lock (thisLock)
                            {
                                c.OrderDetailId = (Guid)od.OrderDetailId;

                                if (Helper.IsFlagSet(od.GroupOrderStatus ?? 0, GroupOrderStatus.PEZevent))
                                {
                                    Peztemp pez;
                                    if (Helper.IsFlagSet(od.GroupOrderStatus ?? 0, GroupOrderStatus.PEZeventWithUserId))
                                    {
                                        string pez_external_userid = MemberFacade.CheckIsPEZUserID(od.MemberEmail);
                                        pez = OrderFacade.GetPezTemp(od.BusinessHourGuid, pez_external_userid);
                                    }
                                    else
                                    {
                                        pez = OrderFacade.GetPezTemp(od.BusinessHourGuid);
                                    }

                                    c.Code = c.SequenceNumber = pez.PezCode;
                                    pez.OrderDetailGuid = c.OrderDetailId;
                                    pez.OrderGuid = od.OrderGuid;
                                    pez.Status = (int)PeztempStatus.Using;
                                    SetPezTemp(pez);
                                }
                                else
                                {
                                    si = GetLastCouponSi(od.BusinessHourGuid.ToString(), od.BusinessHourStatus);
                                    c.SequenceNumber = GenerateCouponSequence(od.ParentOrderId.ToString(), od.MemberEmail, si);
                                    c.Code = GenerateCouponVerification(c.SequenceNumber, od.ParentOrderId.ToString());

                                    if (od.StoreGuid != null)
                                    {
                                        c.StoreSequence = OrderFacade.GenerateStoreCouponSequence(od.BusinessHourGuid.ToString(), od.StoreGuid.ToString());
                                    }
                                }
                                c.Id = Convert.ToInt32(GetCouponID(c));    //get coupon_id for SMS
                            }

                            CouponCol.Add(c);
                        }
                        catch (Exception e)
                        {
                            logger.Error("Something wrong while OrderFacade.GenerateCouponByCouponCol: " + Helper.Object2String(c, "\n"), e);
                        }
                    }

                    #region verification staff
                    CashTrustLogCollection ctl = GetTrustByOrderDetail(od.OrderDetailId.Value);
                    if (ctl.Count == CouponCol.Count) //only update if count match
                    {
                        for (int i = 0; i < ctl.Count; i++)
                        {
                            SaveTrustCoupon(ctl[i], CouponCol[i].Id, CouponCol[i].SequenceNumber);
                        }
                    }
                    #endregion
                }
            }
        }

        public static bool FixFamiGroupCoupon(Guid orderGuid)
        {
            var order = op.OrderGet(orderGuid);
            var odc = op.OrderDetailGetList(order.Guid);

            if (!Helper.IsFlagSet(order.OrderStatus, OrderStatus.Complete) || !odc.Any() || odc.Count != 1 || odc.First().Status == (int)OrderDetailStatus.SystemEntry)
            {
                return false;
            }

            List<Guid> executedTrustId = new List<Guid>();
            List<int> ctlStatus = new List<int> { (int)TrustStatus.Initial, (int)TrustStatus.Trusted };
            var od = pp.ViewPponOrderDetailGet(odc.First().Guid);
            var ctlc = mp.CashTrustLogGetListByOrderGuid(order.Guid).ToList().Where(x => ctlStatus.Contains(x.Status)).ToList();
            var pincodeTran = fami.GetFamilyNetPincodeTrans(order.Guid);

            if (pincodeTran.ReplyCode != ((int)FamilyNetReplyCode.Code00).ToString().PadLeft(2, '0') || ctlc.Count != pincodeTran.Qty)
            {
                return false;
            }

            var pinCol = pincodeTran.Pincode.Split(',');

            for (int i = 0; i < pincodeTran.Qty; i++)
            {
                var pin = FamiGroupCoupon.DecryptPincode(pinCol[i]);
                var couponList = pp.CouponGetListWithNolock(od.OrderDetailGuid).ToList();
                Coupon c = couponList.FirstOrDefault(x => x.SequenceNumber == pin) ?? new Coupon();

                try
                {
                    lock (thisLock)
                    {
                        using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                        {
                            c.OrderDetailId = od.OrderDetailGuid;
                            Peztemp pez = pp.PeztempGetByCode(od.OrderDetailGuid, pin);
                            if (!pez.IsLoaded)
                            {
                                pez = new Peztemp
                                {
                                    PezCode = pin,
                                    Bid = od.BusinessHourGuid,
                                    UserId = order.UserId.ToString(),
                                    IsUsed = false,
                                    ServiceCode = (int)PeztempServiceCode.FamilynetPincode,
                                    IsTest = false,
                                    IsBackupCode = false,
                                };
                            }

                            c.Code = c.SequenceNumber = pez.PezCode;
                            pez.OrderDetailGuid = c.OrderDetailId;
                            pez.OrderGuid = od.OrderGuid;
                            pez.Status = (int)PeztempStatus.Using;
                            OrderFacade.SetPezTemp(pez);
                            if (c.IsLoaded)
                            {
                                pp.CouponSet(c);
                            }
                            else
                            {
                                c.Id = OrderFacade.GetCouponID(c);
                            }

                            var ct = ctlc.FirstOrDefault(x => x.CouponId == c.Id) ?? new CashTrustLog();
                            if (!ct.IsLoaded)
                            {
                                ct = ctlc.FirstOrDefault(x => !executedTrustId.Contains(x.TrustId) && x.CouponId == null) ?? new CashTrustLog();
                            }

                            if (ct.IsNew)
                            {
                                return false;
                            }

                            OrderFacade.SaveTrustCoupon(ct, c.Id, c.SequenceNumber);
                            executedTrustId.Add(ct.TrustId);

                            ts.Complete();
                        }

                        if (config.ActiveMQEnable)
                        {
                            IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
                            var famiMq = new FamiGroupCouponMq
                            {
                                PeztempCode = pinCol[i],
                                OrderGuid = order.Guid,
                                CouponId = c.Id
                            };
                            mqp.Send(FamiGroupCouponMq._QUEUE_NAME, famiMq);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("fixFamiCoupon error. " + Helper.Object2String(c, "\n"), ex);
                }
            }
            return true;
        }

        public static string GenerateCouponByOrderDetail(Guid orderDetail, int count)
        {
            string result = string.Empty;
            int si = -1;

            ViewPponOrderDetailCollection odc = pp.ViewPponOrderDetailGetList(ViewPponOrderDetail.Columns.OrderDetailGuid, orderDetail,
                                                                                ViewPponOrderDetail.Columns.OrderDetailCreateTime);

            IEnumerable<ViewPponOrderDetail> odCol = from x in odc where (x.OrderDetailStatus != (int)OrderDetailStatus.SystemEntry) select x;
            if (odCol.Count() != 0 && odCol.ElementAt(0).ParentOrderId != null)
            {
                foreach (var od in odCol)
                {
                    for (int i = 0; i < count; i++)
                    {
                        Coupon c = new Coupon();
                        QueryCommandCollection qcCol = new QueryCommandCollection();
                        c.OrderDetailId = (Guid)od.OrderDetailGuid;
                        try
                        {
                            lock (thisLock)
                            {
                                if (Helper.IsFlagSet(od.GroupOrderStatus ?? 0, GroupOrderStatus.PEZevent))
                                {
                                    Peztemp pez;
                                    if (Helper.IsFlagSet(od.GroupOrderStatus ?? 0, GroupOrderStatus.PEZeventWithUserId))
                                    {
                                        string pez_external_userid = MemberFacade.CheckIsPEZUserID(od.MemberEmail);
                                        pez = OrderFacade.GetPezTemp(od.BusinessHourGuid.Value, pez_external_userid);
                                    }
                                    else
                                    {
                                        pez = OrderFacade.GetPezTemp(od.BusinessHourGuid.Value);
                                    }

                                    c.Code = c.SequenceNumber = pez.PezCode;
                                    pez.OrderDetailGuid = c.OrderDetailId;
                                    pez.OrderGuid = od.OrderGuid;
                                    pez.Status = (int)PeztempStatus.Using;
                                    OrderFacade.SetPezTemp(pez);
                                }
                                else
                                {
                                    ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(od.BusinessHourGuid.Value);
                                    si = GetLastCouponSi(od.BusinessHourGuid.ToString(), deal.BusinessHourStatus);
                                    c.SequenceNumber = GenerateCouponSequence(od.ParentOrderId.ToString(), od.MemberEmail, si);
                                    c.Code = GenerateCouponVerification(c.SequenceNumber, od.ParentOrderId.ToString());
                                }
                                AddOrNot(qcCol, GetQryCmd(c));
                                if (qcCol.Count > 0)
                                {
                                    DataService.ExecuteTransaction(qcCol);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            result += ("error:" + Helper.Object2String(c, "\n") + Helper.Object2String(qcCol, "\n"));
                            logger.Error("Something wrong while OrderFacade.GenerateCouponByOrderDetail: " + Helper.Object2String(c, "\n") + Helper.Object2String(qcCol, "\n"), e);
                        }
                    }

                    CouponCollection fix_coupons = pp.CouponGetListWithNolock(od.OrderDetailGuid);
                    var ctls = mp.CashTrustLogListGetByOrderId(od.OrderGuid);
                    if (fix_coupons.Count == ctls.Count)
                    {
                        var newCtls = new CashTrustLogCollection();
                        foreach (var ctl in ctls.Where(x => x.CouponId != null))
                        {
                            var c = fix_coupons.FirstOrDefault(x => x.Id == ctl.CouponId && x.SequenceNumber == ctl.CouponSequenceNumber);
                            if (c.IsLoaded)
                            {
                                fix_coupons.Remove(c);
                            }
                        }

                        if (fix_coupons.Count > 0)
                        {
                            int fixQty = 0;
                            foreach (var ctl in ctls.Where(x => x.CouponId == null))
                            {
                                if (ctl.IsLoaded && ctl.TrustId != Guid.Empty)
                                {
                                    ctl.CouponId = fix_coupons[fixQty].Id;
                                    ctl.CouponSequenceNumber = fix_coupons[fixQty].SequenceNumber;
                                    ctl.TrustSequenceNumber = fix_coupons[fixQty].SequenceNumber;
                                    newCtls.Add(ctl);
                                    fixQty++;
                                }
                            }
                            if (newCtls.Count > 0)
                            {
                                mp.CashTrustLogCollectionSet(newCtls);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static string GenerateCouponSequence(string orderId, string userName, int order)
        {
            MersenneTwister mt = new MersenneTwister((uint)(orderId + userName).GetHashCode());
            uint hash = mt.NextUIntAtOrderN(1000, 9999, (uint)order);
            return string.Format("{0:0000}-{1:####}", order, hash);
        }

        public static string GenerateHidealCouponSequence(string productGuid, string storeGuid, int order)
        {
            MersenneTwister mt = new MersenneTwister((uint)(productGuid + storeGuid).GetHashCode());
            uint hash = mt.NextUIntAtOrderN(1000, 9999, (uint)order);

            return string.Format("{0:000000}-{1:####}", order, hash);
        }

        public static string GenerateHidealCouponSequence(string productGuid, int order)
        {
            MersenneTwister mt = new MersenneTwister((uint)(productGuid).GetHashCode());
            uint hash = mt.NextUIntAtOrderN(1000, 9999, (uint)order);

            return string.Format("{0:000000}-{1:####}", order, hash);
        }

        public static string GenerateCouponVerification(string couponSequence, string orderId)
        {
            MersenneTwister mt = new MersenneTwister((uint)(couponSequence + orderId).GetHashCode());
            return mt.Next(100000, 999999).ToString(); // 6 digits
        }

        public static int GetLastCouponSi(string bid, int ty)
        {
            string result = pp.CouponGetMaxSequence(bid);
            string si = result.Split('-')[0];
            if (!Helper.IsNumeric(si) && si.Length > 4)
            {
                si = si.Substring(2, si.Length - 2);
            }
            if (Helper.IsNumeric(si))
            {
                return Convert.ToInt32(si) + 1;
            }
            else
            {
                return 1;
            }
        }
        /// <summary>
        /// 一個較快的版本
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static int GetLastCouponSiV2(string bid)
        {
            string snKey = string.Format("DealCouponSeed://{0}", bid);
            int nextValue = sysp.SerialNumberGetNext(snKey, 1);
            return nextValue;
        }

        public static int GetAncestorCouponCount(Guid bid)
        {
            DealProperty dp = pp.DealPropertyGet(bid);
            if (null != dp && dp.IsLoaded && dp.AncestorSequenceBusinessHourGuid.HasValue && dp.IsContinuedSequence)
            {
                BusinessHour bh = sp.BusinessHourGet(dp.AncestorSequenceBusinessHourGuid.Value);
                if (null != bh && bh.IsLoaded)
                {
                    string result = pp.CouponGetMaxSequence(dp.AncestorSequenceBusinessHourGuid.Value.ToString());
                    string si = result.Split('-')[0];
                    if (!Helper.IsNumeric(si) && si.Length > 4)
                    {
                        si = si.Substring(2, si.Length - 2);
                    }

                    if (Helper.IsNumeric(si))
                    {
                        return Convert.ToInt32(si);
                    }
                }
            }

            return 0;
        }

        public static void SetDealGenerated(GroupOrder go)
        {
            go.Status = (int)Helper.SetFlag(true, go.Status ?? 0, GroupOrderStatus.PponCouponGenerated);
            op.GroupOrderSet(go);
        }

        public static int CheckCouponCountByOrderDetail(Guid odid)
        {
            return pp.CheckCouponCount(odid);
        }

        public static int GetCouponID(Coupon c)
        {
            int id;
            int.TryParse(DataService.ExecuteScalar(pp.GetInsertCouponCmd(c)).ToString(), out id);
            return id;
        }

        public static void ClearCouponByCouponID(int id)
        {
            pp.ClearCouponByCouponID(id);
        }

        protected static QueryCommand GetQryCmd<T>(T data) where T : RecordBase<T>, new()
        {
            return (data.IsNew)
                       ? ActiveHelper<T>.GetInsertCommand(data, null)
                       : ActiveHelper<T>.GetUpdateCommand(data, null);
        }
        protected static QueryCommandCollection AddOrNot(QueryCommandCollection myself, QueryCommand qc)
        {
            if (qc != null)
            {
                myself.Add(qc);
            }

            return myself;
        }

        public static CashTrustLogCollection GetTrustByOrderDetail(Guid odid, OrderClassification oc = OrderClassification.LkSite)
        {
            return mp.CashTrustLogGetListByOrderDetailGuid(odid, oc);
        }

        public static void SaveTrustCoupon(CashTrustLog ctl, int coupon, string sequence)
        {
            UserCashTrustLogCollection uctlc = mp.UserCashTrustLogGet(ctl.TrustId);
            EinvoiceMain invoice = op.EinvoiceMainUninvoicedOnlyGetListByOrderGuid(ctl.OrderGuid).Where(x => x.OrderAmount == ctl.UninvoicedAmount).FirstOrDefault();
            bool isGroupCoupon = Helper.IsFlagSet(pp.BusinessHourGet(ctl.BusinessHourGuid ?? Guid.NewGuid()).BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon);

            ctl.CouponId = coupon;
            ctl.CouponSequenceNumber = sequence;
            ctl.ModifyTime = DateTime.Now;
            ctl.TrustSequenceNumber = sequence;
            mp.CashTrustLogSet(ctl);

            if (invoice != null && !isGroupCoupon)
            {
                invoice.MarkOld();
                invoice.CouponId = coupon;
                op.EinvoiceSetMain(invoice);
            }
            else
            {
                EntrustSellReceipt esr = op.EntrustSellReceiptGetByTrustId(ctl.TrustId);
                if (esr.IsLoaded)
                {
                    esr.CouponId = coupon;
                    op.EntrustSellReceiptSet(esr);
                }
            }
        }

        public static ViewPponDealCollection GetRandomTodayPponDeal(int num, int cityId)
        {
            return pp.GetRandomPponDealByCity(num, cityId);
        }

        public static DealTimeSlotCollection DealTimeSlotGetCol(Guid bid)
        {
            return pp.DealTimeSlotGetCol(bid);
        }

        public static string SmsGetPhone(Guid orderDetailGuid)
        {
            return SmsGetPhoneFromWhere(orderDetailGuid, Guid.Empty);
        }
        /// <summary>
        /// 判斷是否有分店，有分店:show分店電話(phone)，無分店:show賣家電話(seller_tel)
        /// </summary>
        /// <param name="vhdc">ViewHiDealCoupon物件</param>
        /// <returns></returns>
        public static string HiDealSmsGetPhone(ViewHiDealCoupon vhdc)
        {
            ViewHiDealStoresInfoCollection vhdsi = hp.ViewHiDealDealStoreInfoGetByHiDealId(vhdc.DealId);
            if (vhdc.StoreGuid != null)
            {
                string result = string.Empty;
                if (vhdsi.Count > 0)
                {
                    //result += sp.StoreGet(vhdc.StoreGuid ?? Guid.Empty).Phone;
                    var storePhone = vhdsi.Where(x => Guid.Equals(vhdc.StoreGuid.GetValueOrDefault(Guid.Empty), x.Guid));
                    if (storePhone.Count() > 0)
                    {
                        result = storePhone.First().Phone;
                    }
                    //result += vhdsi[0].Phone;
                }

                if (string.IsNullOrEmpty(result))
                {
                    result = sp.SellerGet(vhdc.SellerGuid).SellerTel;
                }

                return "，預約專線" + result;
            }
            else
            {
                return string.Empty;
            }
        }

        public static string SmsGetPhoneFromWhere(Guid orderdetail, Guid bid)
        {
            //TODO bid 己無使用，可視情況移掉，並與SmsGetPhone合併
            ViewPponOrderDetail vpod = op.ViewPponOrderDetailGet(orderdetail);
            string result = GetOrderDetailItemName(OrderDetailItemModel.Create(vpod),
                new PponItemNameShowOption
                {
                    ShowPhone = true,
                    StoreInfoInParenthesis = false,
                    HideItemName = true,
                    RemoveOptionsParentheses = false
                });
            return result;
        }
        /// <summary>
        /// 抓取簡訊內容
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static SmsContent SMSContentGet(Guid bid)
        {
            return pp.SMSContentGet(bid);
        }

        #region 萊爾富相關


        /// <summary>
        /// 萊爾富寄杯核銷
        /// </summary>
        /// <param name="pin"></param>
        /// <param name="d"></param>
        /// <param name="p"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public static bool SetHiLifePincodeVerify(Core.Models.Entities.HiLifePincode pin, Core.Models.Entities.Peztemp p, string ipAddress, string createId, out string msg)
        {
            msg = string.Empty;
            ViewPponCoupon vpc = pp.ViewPponCouponGet(p.OrderDetailGuid ?? Guid.Empty, p.PezCode);
            if (!vpc.IsLoaded)
            {
                return false;
            }

            try
            {
                using (TransactionScope scope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    VerifyCoupon(vpc.CouponId ?? 0, vpc.SequenceNumber, vpc.SellerGuid, string.Empty, ipAddress, false
                        , string.Format("{0}_{1}", Helper.GetEnumDescription(PeztempServiceCode.HiLifenetPincode), createId)
                        , OrderClassification.LkSite, true);

                    pin.IsVerified = true;
                    hiLife.UpdateHiLifePincode(pin);
                    scope.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                logger.ErrorFormat("萊爾富寄杯 ERROR({0}):{1}, {2}", pin.CouponId, ex.Message, ex.StackTrace);

                return false;
            }
        }

        /// <summary>
        /// 萊爾富退貨註銷Pincode
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="bid"></param>
        /// <param name="errorMsg"></param>
        /// <param name="returnType"></param>
        /// <returns></returns>
        public static bool HiLifePincodeReturn(Guid orderGuid, out string errorMsg, HiLifeReturnType returnType = HiLifeReturnType.UserReturn)
        {
            errorMsg = "";
            var pincodes = hiLife.GetHiLifePincode(orderGuid).Where(x => !x.IsVerified && (x.ReturnStatus == (byte)HiLifeReturnStatus.Default || x.ReturnStatus == (byte)HiLifeReturnStatus.Fail)).ToList();
            if (!pincodes.Any())
            {
                errorMsg = "此訂單所有憑證已核銷/退貨，請客服確認。";
                return false;
            }
            else
            {
                Order o = op.OrderGet(orderGuid);

                //先更新為成退貨中的狀態
                pincodes.ForEach(x => x.ReturnStatus = (int)HiLifeReturnStatus.InProcess);
                hiLife.UpdateHiLifePincodeReturnStatus(pincodes);
                if (!HiLifeFacade.ReturnPincode(pincodes, o, returnType))
                {
                    errorMsg = "退貨失敗";
                }
            }

            return errorMsg.Length == 0;
        }


        #endregion

        /// <summary>
        /// To Do 期望整合多家廠商退貨規則在同一function
        /// To Do FamilyNetReturnType 整成OrderPincodeReturnType
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="bid"></param>
        /// <param name="errorMsg"></param>
        /// <param name="returnType"></param>
        /// <returns></returns>
        public static bool VendorGroupCouponPincodeReturn(Guid orderGuid, Guid? bid, out string errorMsg, FamilyNetReturnType returnType = FamilyNetReturnType.UserReturn)
        {
            errorMsg = string.Empty;
            //判斷是哪家串接廠商的檔次
            ViewCouponListMain vclm = mp.GetCouponListMainByOid(orderGuid);

            //成套&ItemPrice>0
            if (Helper.IsFlagSet(vclm.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon) && vclm.ItemPrice > 0)
            {
                bid = bid ?? vclm.BusinessHourGuid;
                if (Helper.IsFlagSet(vclm.GroupOrderStatus.Value, (int)GroupOrderStatus.FamiDeal))
                {
                    #region FamiDeal

                    if (!FamilyNetPincodeReturnAPIEnabled())
                    {
                        errorMsg = string.Format("API更新中({0}~{1})，請稍後在試", config.FamilyNetReturnApiStopTimeS, config.FamilyNetReturnApiStopTimeE);
                        return false;
                    }

                    List<CashTrustLog> ctlc = ReturnService.GetRefundableCouponCtlogs(orderGuid, BusinessModel.Ppon);
                    Order o = op.OrderGet(orderGuid);

                    var famiEvent = fami.GetFamilyNetEvent(bid.Value);

                    if (famiEvent == null)
                    {
                        errorMsg = "無此活動資訊";
                        return false;
                    }

                    var pincodes = fami.GetFamilyNetListPincode(orderGuid).Where(x => x.Status == (byte)FamilyNetPincodeStatus.Completed && (x.ReturnStatus == (int)FamilyNetPincodeReturnStatus.Init || x.ReturnStatus == (int)FamilyNetPincodeReturnStatus.Returning) && !x.IsVerified && !x.IsVerifying).ToList();

                    if (pincodes.Any(x => x.IsLock))
                    {
                        errorMsg = "尚有憑證鎖定中，請稍後在申請退貨。";
                        return false;
                    }

                    //先更新為成退貨中的狀態
                    pincodes.Where(x => x.ReturnStatus == (int)FamilyNetPincodeReturnStatus.Init).ForEach(x => x.ReturnStatus = (int)FamilyNetPincodeReturnStatus.Returning);
                    fami.UpdateFamilyNetPincodeReturnStatus(pincodes);

                    return FamilyNetPincodeListReturn(ctlc, pincodes, o, bid, famiEvent.ActCode, out errorMsg, returnType);

                    #endregion
                }
                else if (Helper.IsFlagSet(vclm.GroupOrderStatus.Value, (int)GroupOrderStatus.HiLifeDeal))
                {
                    #region HiLifeDeal
                    var pincodes = hiLife.GetHiLifePincode(orderGuid).Where(x => !x.IsVerified
                    && x.ReturnStatus == (byte)HiLifeReturnStatus.Default).ToList();
                    if (!pincodes.Any())
                    {
                        errorMsg = "此訂單所有憑證已核銷/退貨，請客服確認。";
                        return false;
                    }
                    else
                    {
                        Order o = op.OrderGet(orderGuid);
                        //return HiLifeFacade.ReturnPincode(pincodes, o, returnType);
                        return false;
                    }
                    #endregion
                }
            }
            return false;
        }

        /// <summary>
        /// 全家寄杯核銷
        /// </summary>
        /// <param name="pin"></param>
        /// <param name="d"></param>
        /// <param name="p"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public static bool SetFamilyNetPincodeVerify(FamilyNetPincode pin, FamilyNetPincodeDetail d, Core.Models.Entities.Peztemp p, string ipAddress, out string msg)
        {
            msg = string.Empty;
            ViewPponCoupon vpc = pp.ViewPponCouponGet(p.OrderDetailGuid ?? Guid.Empty, p.PezCode);
            if (!vpc.IsLoaded)
            {
                return false;
            }

            try
            {
                using (TransactionScope scope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    VerifyCoupon(vpc.CouponId ?? 0, vpc.SequenceNumber, vpc.SellerGuid, string.Empty, ipAddress, false
                        , string.Format("{0}_{1}", Helper.GetEnumDescription(PeztempServiceCode.FamilynetPincode), d.TranNo)
                        , OrderClassification.LkSite, true);
                    fami.UpdateFamilyNetPincode(pin);
                    fami.UpdateFamilyNetPincodeDetail(d);
                    scope.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                logger.ErrorFormat("全家寄杯 ERROR({0}):{1}, {2}", pin.CouponId, ex.Message, ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// 全家退貨
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="bid"></param>
        /// <param name="errorMsg"></param>
        /// <param name="returnType"></param>
        /// <returns></returns>
        public static bool FamilyNetPincodeReturn(Guid orderGuid, Guid bid, out string errorMsg, FamilyNetReturnType returnType = FamilyNetReturnType.UserReturn)
        {
            errorMsg = "";
            if (!FamilyNetPincodeReturnAPIEnabled())
            {
                errorMsg = string.Format("API更新中({0}~{1})，請稍後在試", config.FamilyNetReturnApiStopTimeS, config.FamilyNetReturnApiStopTimeE);
                return false;
            }

            List<CashTrustLog> ctlc = ReturnService.GetRefundableCouponCtlogs(orderGuid, BusinessModel.Ppon);
            Order o = op.OrderGet(orderGuid);

            var famiEvent = fami.GetFamilyNetEvent(bid);

            if (famiEvent == null)
            {
                errorMsg = "無此活動資訊";
                return false;
            }

            var pincodes = fami.GetFamilyNetListPincode(orderGuid).Where(x => x.Status == (byte)FamilyNetPincodeStatus.Completed && !x.IsVerified && !x.IsVerifying).ToList();

            if (pincodes.Any(x => x.IsLock))
            {
                errorMsg = "尚有憑證鎖定中，請稍後在申請退貨。";
                return false;
            }

            //先更新為成退貨中的狀態
            pincodes.Where(x => x.ReturnStatus == (int)FamilyNetPincodeReturnStatus.Init).ForEach(x => x.ReturnStatus = (int)FamilyNetPincodeReturnStatus.Returning);
            fami.UpdateFamilyNetPincodeReturnStatus(pincodes);

            return FamilyNetPincodeListReturn(ctlc, pincodes, o, bid, famiEvent.ActCode, out errorMsg, returnType);

        }

        /// <summary>
        /// 新版多筆退貨
        /// </summary>
        /// <param name="ctlc"></param>
        /// <param name="pincodes"></param>
        /// <param name="o"></param>
        /// <param name="bid"></param>
        /// <param name="actCode"></param>
        /// <param name="returnType"></param>
        /// <returns></returns>
        public static bool FamilyNetPincodeListReturn(List<CashTrustLog> ctlc, List<FamilyNetPincode> pincodes, Order o, Guid? bid, string actCode, out string errorMsg, FamilyNetReturnType returnType = FamilyNetReturnType.UserReturn)
        {
            var detail = new List<PincodeReturnInputDetail>();
            var col = ctlc;
            //一次最多僅能退10筆
            int maxReturn = 10;
            errorMsg = "";
            for (int i = 1; i <= col.Count(); i++)
            {
                detail.Add(new PincodeReturnInputDetail
                {
                    SerialNo = (i % maxReturn == 0 ? maxReturn : i % maxReturn).ToString(),
                    ActCode = actCode,
                    PinCode = System.Web.HttpUtility.UrlEncode(FamiGroupCoupon.EncryptPincode(ctlc[i - 1].CouponSequenceNumber)),
                    CouponId = ctlc[i - 1].CouponId ?? 0
                });

                if (i % maxReturn == 0 || i == col.Count())
                {
                    var input = new PincodeReturnInput2
                    {
                        Detail = detail
                    };

                    input.SetOrderDate(o.CreateTime);
                    input.SetTransStatus(FamilyNetTransStatus.Code04);

                    var result = FamiGroupCoupon.VoidFamilyNetPincode2(input, ctlc, o.Guid, bid, returnType);

                    if (result != null)
                    {
                        foreach (var item in result.Detail)
                        {
                            var pincode = pincodes.FirstOrDefault(x => x.CouponId == item.CouponId);
                            FamilyNetReplyCode replyCode = item.GetReplyCode();

                            if (replyCode == FamilyNetReplyCode.Code00 || replyCode == FamilyNetReplyCode.Code11 || replyCode == FamilyNetReplyCode.Code13)
                            {
                                pincode.ReturnStatus = (byte)FamilyNetPincodeReturnStatus.Completed;
                            }
                            else if (replyCode == FamilyNetReplyCode.Initial)
                            {
                                //維持退貨中，等待重送 //先觀察暫不考慮重送次數
                            }
                            else
                            {
                                pincode.ReturnStatus = (byte)FamilyNetPincodeReturnStatus.Fail;
                                errorMsg += string.Format("憑證編號{0}退貨失敗，代碼{1}({2})\\n", ctlc.FirstOrDefault(x => x.CouponId == item.CouponId).CouponSequenceNumber, replyCode, Helper.GetDescription(replyCode));
                            }
                        }
                    }
                    fami.UpdateFamilyNetPincodeReturnStatus(pincodes);
                    detail.Clear();
                }
            }

            return string.IsNullOrEmpty(errorMsg);
        }

        public static bool FamilyNetPincodeReturnAPIEnabled()
        {
            if (DateTime.Now.IsBetween(config.FamilyNetReturnApiStopTimeS, config.FamilyNetReturnApiStopTimeE))
            {
                return false;
            }
            return true;
        }

        public static void VerifyCoupon(int couponId, Guid sid, string description, string ip, string createId, OrderClassification orderClassification)
        {
            VerifyCoupon(couponId, string.Empty, sid, description, ip, false, createId, orderClassification);
        }

        public static void VerifyCoupon(int couponId, string couponCode, Guid sid, string description, string ip,
            bool isForced, string createId, OrderClassification orderClassification, bool isMulti = false, bool isJf = false)
        {
            //bool setUnrecovered = false;

            #region 強制核銷
            //if (isForced)
            //{
            //ViewPponCoupon coupon = pp.ViewPponCouponGet(couponId);
            //CashTrustLog ct = mp.CashTrustLogGetByCouponId(couponId);
            //bool isUndoSuccessful = PaymentFacade.UndoCancelPaymentTransaction(ct, description);

            //if (coupon != null && ct != null)
            //{
            //    if (isUndoSuccessful)
            //    {
            //        #region 部分退貨紀錄
            //        if (mp.CashTrustLogGetListByOrderGuid(ct.OrderGuid).Count(x=>x.Status==(int)TrustStatus.Returned||x.Status==(int)TrustStatus.Refunded)==0)
            //        {
            //            op.OrderSetStatus(coupon.OrderGuid, description, OrderStatus.Cancel, false);
            //            op.OrderSetStatus(coupon.OrderGuid, description, OrderStatus.PartialCancel, false);
            //            op.OrderSetStatus(coupon.OrderGuid, description, OrderStatus.CancelBeforeClosed, false);
            //            CommonFacade.AddAudit(coupon.OrderGuid, AuditType.Order, "訂單已恢復", description, true);
            //        }
            //        #endregion
            //    }
            //    else
            //        setUnrecovered = true;
            //}
            //CommonFacade.AddAudit(coupon.OrderGuid, AuditType.Order, "強制核銷憑證:#" + ct.CouponSequenceNumber, description, true);
            //}
            #endregion

            
            int logCouponId = 0;
            string logSequenceNumber;

            #region Find CouponCode

            if (string.IsNullOrEmpty(couponCode))
            {
                couponCode = pp.ViewPponCouponGet(couponId).CouponCode;
            }

            #endregion

            var tsScope = isMulti ? TransactionScopeOption.Required : TransactionScopeOption.RequiresNew;

            CashTrustLog ctl = mp.CashTrustLogGetByCouponId(Convert.ToInt32(couponId), orderClassification);

            bool isGroupCoupon = Helper.IsFlagSet(op.GetHourStatusBybid(ctl.BusinessHourGuid ?? Guid.NewGuid()).BusinessHourStatus, BusinessHourStatus.GroupCoupon);

            var sw = new Stopwatch();
            sw.Reset();

            var recordCostTime = new StringBuilder();
            long startTime, endTime;

            using (TransactionScope trans = new TransactionScope(tsScope,
                new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted, Timeout = new TimeSpan(0, 5, 0) }))
            {
                sw.Start();
                # region 成套票券檔次挑選最大面額核銷
                CashTrustLogCollection newCtlc = new CashTrustLogCollection();
                CashTrustLog oldCtl = null;
                //強制退貨&強制核銷走ChangeCtlCouponNumber
                if (isGroupCoupon && !isForced)
                {
                    var ctlc = mp.CashTrustLogListGetByOrderId(ctl.OrderGuid);
                    //將能調換的ctl先排除存在於mgm中，剩下就是未送禮可以置換的
                    if (ctlc.Count > 1 && ctl.Amount < ctlc.Where(x => x.Status < (int)TrustStatus.Verified).Max(m => m.Amount))
                    {
                        oldCtl = ctl;
                        ctl = ctlc.Where(x => x.Status < (int)TrustStatus.Verified).OrderByDescending(x => x.Amount).ThenBy(x => x.DiscountAmount).FirstOrDefault();
                        int? newCouponId = ctl.CouponId;
                        string newSeqNum = ctl.CouponSequenceNumber;
                        ctl.CouponId = oldCtl.CouponId;
                        ctl.CouponSequenceNumber = oldCtl.CouponSequenceNumber;
                        oldCtl.CouponId = newCouponId;
                        oldCtl.CouponSequenceNumber = newSeqNum;
                    }
                }
                #endregion

                #region Set CashTrustLog or OldCashTrustLog


                if (ctl.TrustId == Guid.Empty)
                {
                    OldCashTrustLog objOctl = mp.OldCashTrustLogGetByCouponId(Convert.ToInt32(couponId));

                    #region OldCashTrustLog

                    logCouponId = objOctl.CouponId;
                    logSequenceNumber = objOctl.SequenceNumber.ToString();
                    objOctl.VerifyTime = DateTime.Now;
                    objOctl.Status = (int)TrustStatus.Verified;
                    mp.OldCashTrustLogSet(objOctl);

                    #endregion
                }
                else
                {
                    #region CashTrustLog
                    logCouponId = Convert.ToInt32(ctl.CouponId);
                    logSequenceNumber = ctl.CouponSequenceNumber;

                    ctl.ModifyTime = DateTime.Now;
                    ctl.Status = (int)TrustStatus.Verified;
                    ctl.UsageVerifiedTime = DateTime.Now;
                    ctl.VerifiedStoreGuid = GetVerifiedStoreGuid(Convert.ToInt32(couponId), orderClassification, createId);
                    ctl.UsageVerifyId = createId;
                    if (isForced)
                    {
                        ctl.SpecialStatus |= (int)TrustSpecialStatus.VerificationForced;
                        ctl.SpecialOperatedTime = DateTime.Now;
                        if (isForced)
                        {
                            ctl.SpecialStatus |= (int)TrustSpecialStatus.VerificationUnrecovered;
                            CommonFacade.AddAudit(ctl.OrderGuid, AuditType.Order, "強制核銷憑證:#" + ctl.CouponSequenceNumber + "尚未收回款項", description, true);
                        }

                        //TODO 品生活還沒有強制核銷這件事，哪天有了這邊可能要補code
                        if (orderClassification != OrderClassification.HiDeal)
                        {
                            ViewPponCoupon coupon = pp.ViewPponCouponGet(couponId);
                            CommonFacade.AddAudit(coupon.OrderGuid, AuditType.Order,
                                                  "強制核銷憑證:#" + ctl.CouponSequenceNumber, description, true);
                        }
                    }
                    #region Set CashTrustStatusLog

                    CashTrustStatusLog ctsl = new CashTrustStatusLog();
                    ctsl.TrustId = ctl.TrustId;
                    ctsl.TrustProvider = ctl.TrustProvider;
                    ctsl.Amount = ctl.Scash + ctl.CreditCard + ctl.Atm;
                    ctsl.Status = ctl.Status;
                    ctsl.ModifyTime = ctl.ModifyTime;
                    ctsl.CreateId = createId;
                    if (isForced)
                    {
                        ctsl.SpecialStatus = ctl.SpecialStatus;
                        ctsl.SpecialOperatedTime = DateTime.Now;
                    }

                    #endregion

                    #region MGM

                    var gift = mgmp.GetActiveGiftByCouponId(ctl.OrderGuid, ctl.CouponId.Value);
                    if (gift.IsLoaded)
                    {
                        MgmGift mg = mgmp.GiftGetById(gift.Id);
                        if (ctl.Status == (int)TrustStatus.Verified && mg.IsLoaded)
                        {
                            mg.IsUsed = true;
                            mg.UsedTime = ctl.UsageVerifiedTime;
                            mgmp.MgmGiftSet(mg);
                        }
                    }


                    #endregion

                    if (oldCtl != null)
                    {
                        newCtlc.Add(ctl);
                        newCtlc.Add(oldCtl);
                        if (mp.CashTrustLogColSetForVerifyCoupons(newCtlc))
                        {
                            mp.CashTrustStatusLogSet(ctsl);
                            SetCashTrustChangeCouponLog(ctl, oldCtl, "Verify");
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        sw.Stop();
                        startTime = sw.ElapsedMilliseconds;
                        sw.Start();

                        var isSucceed = mp.CashTrustLogSet(ctl);

                        sw.Stop();
                        endTime = sw.ElapsedMilliseconds;
                        sw.Start();
                        recordCostTime.Append(string.Format("CashTurstLogSet:{0}ms. \n", endTime - startTime));

                        if (isSucceed)
                        {
                            sw.Stop();
                            startTime = sw.ElapsedMilliseconds;
                            sw.Start();

                            mp.CashTrustStatusLogSet(ctsl);

                            sw.Stop();
                            endTime = sw.ElapsedMilliseconds;
                            sw.Start();
                            recordCostTime.Append(string.Format("CashTrustStatusLogSet:{0}ms. \n", endTime - startTime));
                        }
                    }


                    #endregion
                }

                #endregion

                #region Set VerificationLog

                VerificationLog vLog = new VerificationLog();

                vLog.SellerGuid = sid;
                vLog.CouponId = logCouponId;
                vLog.SequenceNumber = logSequenceNumber;
                vLog.CouponCode = couponCode;
                vLog.Status = (int)VerificationStatus.CouponOk;
                vLog.Description = description;
                vLog.CreateTime = DateTime.Now;
                vLog.IsHideal = orderClassification == OrderClassification.HiDeal ? true : false;
                vLog.Ip = ip;
                vLog.DeviceType = (int)Helper.GetOrderFromType();

                sw.Stop();
                startTime = sw.ElapsedMilliseconds;
                sw.Start();

                mp.VerificationLogSet(vLog);

                sw.Stop();
                endTime = sw.ElapsedMilliseconds;
                sw.Start();
                recordCostTime.Append(string.Format("VerificationLogSet:{0}ms. \n", endTime - startTime));

                #endregion

                sw.Stop();
                startTime = sw.ElapsedMilliseconds;
                sw.Start();

                try
                {
                    EinvoiceFacade.SetEinvoiceCouponVerifiedTime(couponId, orderClassification, vLog.CreateTime.Value, isJf);
                }
                catch (Exception ex)
                {
                    sw.Stop();
                    endTime = sw.ElapsedMilliseconds;
                    logger.Error(string.Format("SetEinvoiceCouponVerifiedTime Failed. CostTime={0}ms", endTime - startTime), ex);
                    throw;
                }

                sw.Stop();
                endTime = sw.ElapsedMilliseconds;
                sw.Start();
                recordCostTime.Append(string.Format("SetEinvoiceCouponVerifiedTime:{0}ms. \n", endTime - startTime));

                if (orderClassification == OrderClassification.HiDeal)
                {
                    Guid orderGuid = hp.ViewHiDealCouponGet(couponId).HiDealOrderGuid;
                    HiDealOrderFacade.HiDealOrderShowUpdateRemainCount(orderGuid);
                }

                //如果為資策會的訂單
                if (ctl.UserId == ApiLocationDealManager.IDEASMember.UniqueId)
                {
                    //異動CompanyUserOrder,WaitUpdate欄位
                    op.CompanyUserOrderUpdateWaitUpdate(ctl.OrderGuid, true);
                }

                //即時通知代銷商
                ChannelFacade.NotifyVerify(ctl.BusinessHourGuid, ctl.OrderGuid, couponId, ctl.Amount, NotifyVoucherStatus.Verifing, logSequenceNumber, couponCode, ctl.VerifiedStoreGuid);

                sw.Stop();
                var totalCost = sw.ElapsedMilliseconds;
                recordCostTime.Append(string.Format("TransactionScopeTotal:{0}ms. \n", totalCost));

                if (totalCost / (60000) >= 1)
                {
                    logger.Error(string.Format("CouponCode:{0},TimeOut,{1}", couponCode, recordCostTime));
                }

                trans.Complete();
            }

            if (ctl.TrustId != Guid.Empty)
            {
                try
                {
                    PayEvents.OnVerify(ctl.TrustId);
                }
                catch (Exception e)
                {
                    logger.Error("PayEvents.OnVerify Failed", e);
                    throw;
                }

            }

        }

        public static VerificationStatus VerifyCouponForSkm(Guid trustId, string createId, string shopCode, string brandCounterCode)
        {
            var status = VerifyCoupon(trustId, false, createId);
            if (status != VerificationStatus.CouponOk)
            {
                return status;
            }

            var shoppe = skm.SkmShoppeGet(shopCode, brandCounterCode);
            if (shoppe.IsLoaded && shoppe.StoreGuid != null)
            {
                mp.CashTrustLogUpdateStoreGuidByTrustId(trustId, (Guid)shoppe.StoreGuid);
            }

            return status;
        }

        public static VerificationStatus VerifyCouponForSkm(Guid trustId, string createId, Guid StoreGuid)
        {
            var status = VerifyCoupon(trustId, false, createId);
            if (status != VerificationStatus.CouponOk)
            {
                return status;
            }

            if (StoreGuid != Guid.Empty)
            {
                mp.CashTrustLogUpdateStoreGuidByTrustId(trustId, (Guid)StoreGuid);
            }

            return status;
        }


        /// <summary>
        /// 核銷 (ppon 和 hideal 皆可用)
        /// </summary>
        /// <param name="trustId"></param>
        /// <param name="isForced"></param>
        /// <param name="createId"></param>
        /// <returns></returns>
        public static VerificationStatus VerifyCoupon(Guid trustId, string createId,bool isJf = false)
        {
            return VerifyCoupon(trustId, false, createId,isJf);
        }

        /// <summary>
        /// 核銷 (ppon 和 hideal 皆可用)
        /// </summary>
        /// <param name="trustId"></param>
        /// <param name="isForced"></param>
        /// <param name="createId"></param>
        /// <returns></returns>
        public static VerificationStatus VerifyCoupon(Guid trustId, bool isForced, string createId,bool isJf = false)
        {
            string ip = "";
            if (HttpContext.Current != null)
            {
                ip = HttpContext.Current.Request.UserHostAddress;
            }
            return VerifyCoupon(trustId, isForced, createId, ip, "",false,isJf);
        }

        /// <summary>
        /// 核銷 (ppon 和 hideal 皆可用)
        /// </summary>
        /// <param name="trustId"></param>
        /// <param name="isForced">強制核銷</param>
        /// <param name="createId"></param>
        /// <param name="ip"></param>
        /// <param name="desc"></param>
        /// <param name="isMulti">多張憑證</param>
        /// <returns></returns>
        public static VerificationStatus VerifyCoupon(Guid trustId, bool isForced, string createId, string ip, string desc
            , bool isMulti = false,bool isJf = false)
        {
            if (trustId == Guid.Empty)
            {
                return VerificationStatus.CouponNone;
            }
            CashTrustLog ctl = mp.CashTrustLogGet(trustId);
            if (ctl.IsLoaded == false)
            {
                return VerificationStatus.CouponNone;
            }
            int couponId = ctl.CouponId.Value;
            ViewPponCoupon coupon = null;
            ViewHiDealCoupon hidealCoupon = null;
            if ((OrderClassification)ctl.OrderClassification == OrderClassification.HiDeal)
            {
                hidealCoupon = hp.ViewHiDealCouponGet(couponId);
                if (hidealCoupon.CouponId == null || hidealCoupon.CouponId == 0)
                {
                    return VerificationStatus.CouponNone;
                }
            }

            coupon = pp.ViewPponCouponGet(couponId);
            if (coupon != null)
            {
                if (coupon.CouponId == null || coupon.CouponId == 0)
                {
                    return VerificationStatus.CouponNone;
                }
            }
            else
            {
                logger.WarnFormat("核到不是p好康也不是p生活 orderId = {0}", trustId);
                return VerificationStatus.CashTrustLogErr;
            }

            TrustStatus status = (TrustStatus)ctl.Status;

            if (status == TrustStatus.Verified)
            {
                return VerificationStatus.CouponUsed;
            }
            if ((status == TrustStatus.Refunded || status == TrustStatus.Returned) && isForced == false)
            {
                return VerificationStatus.CouponReted;
            }
            if (coupon.CouponStatus == (int)CouponStatus.Locked)
            {
                return VerificationStatus.CouponLocked;
            }
            if (status == TrustStatus.Initial || status == TrustStatus.Trusted)
            {
                Guid sid = (OrderClassification)ctl.OrderClassification == OrderClassification.HiDeal
                                      ? hidealCoupon.SellerGuid
                                      : coupon.SellerGuid;

                string couponCode = (OrderClassification)ctl.OrderClassification == OrderClassification.HiDeal
                                        ? hidealCoupon.Code
                                        : coupon.CouponCode;

                var sw = new Stopwatch();
                sw.Reset();
                sw.Start();

                try
                {
                    VerifyCoupon(couponId, couponCode, sid, createId /* is description*/, ip, isForced, createId, (OrderClassification)ctl.OrderClassification, isMulti,isJf);
                    try
                    {
                        UnlockCouponAndSendSMS(ctl.OrderGuid);
                    }
                    catch (Exception e)
                    {
                        logger.Error("UnlockCouponAndSendSMS Failed", e);
                        throw;
                    }

                    return VerificationStatus.CouponOk;
                }
                catch (Exception ex)
                {
                    sw.Stop();
                    logger.Error(string.Format("核銷失敗: orderId={0}, isForce={1}, createId={2}, ip={3}, costTime={4}ms",
                        trustId, isForced, createId, ip, sw.ElapsedMilliseconds), ex);
                    return VerificationStatus.CouponError;
                }
            }
            return VerificationStatus.CouponError;
        }

        public static void DealVerificationLostSet(Guid bid, Guid sid, string description, string createId)
        {
            try
            {
                // Set DealAccounting
                pp.DealAccountingSetFlag(bid, AccountingFlag.VerificationLost);

                // Set CashTrustLog SpecialFlag of 'DealVerificationLost'
                CashTrustLogCollection ctlc = mp.CashTrustLogGetListByBid(bid);
                foreach (CashTrustLog ctl in ctlc)
                {
                    if (ctl.Status.EqualsAny((int)TrustStatus.Initial, (int)TrustStatus.Trusted))
                    {
                        // 註記 SpecialStatus 為清冊遺失(VerificationLost)
                        ctl.SpecialStatus = (int)TrustSpecialStatus.VerificationLost;
                        mp.CashTrustLogSet(ctl);

                        // 清冊遺失視為已核銷
                        VerifyCoupon(Convert.ToInt32(ctl.CouponId), sid, description, string.Empty, createId, OrderClassification.LkSite);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public static bool IsRequestInvoice(string InvoiceNumber)
        {
            EinvoiceMain einv = op.EinvoiceMainGet(InvoiceNumber);
            if (einv.InvoiceRequestTime == null)
            {
                return false;
            }
            return true;
        }

        public static bool IsPaperedTime(string InvoiceNumber)
        {
            EinvoiceMain einv = op.EinvoiceMainGet(InvoiceNumber);
            if (einv.InvoicePaperedTime == null)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 單筆憑證取消狀態&檢查
        /// </summary>
        /// <param name="trustId"></param>
        /// <param name="createId"></param>
        /// <param name="classification"></param>
        /// <returns></returns>
        public static bool UndoVerifiedStatus(Guid trustId, string createId, OrderClassification classification = OrderClassification.LkSite)
        {
            if (trustId == Guid.Empty)
            {
                return false;
            }

            CashTrustLog ctlog = mp.CashTrustLogGet(trustId);



            #region 成套票券檔次挑選最小面額反核銷
            bool isGroupCoupon = Helper.IsFlagSet(op.GetHourStatusBybid(ctlog.BusinessHourGuid ?? Guid.NewGuid()).BusinessHourStatus, BusinessHourStatus.GroupCoupon);
            CashTrustLog oldCtl = null;
            //todo:交換可能會會影響到對帳單~先取消~
            if (isGroupCoupon)
            {
                //CashTrustLogCollection ctlc = mp.CashTrustLogListGetByOrderId(ctlog.OrderGuid);
                //if (ctlc.Count > 1 && ctlog.Amount > ctlc.Where(x => x.Status >= (int)TrustStatus.Verified).Min(m => m.Amount))
                //{
                //    oldCtl = ctlog;
                //    ctlog = ctlc.Where(x => x.Status >= (int)TrustStatus.Verified).OrderBy(x => x.Amount).FirstOrDefault();
                //    int? newCouponId = ctlog.CouponId;
                //    string newSeqNum = ctlog.CouponSequenceNumber;
                //    ctlog.CouponId = oldCtl.CouponId;
                //    ctlog.CouponSequenceNumber = oldCtl.CouponSequenceNumber;
                //    oldCtl.CouponId = newCouponId;
                //    oldCtl.CouponSequenceNumber = newSeqNum;
                //}
            }

            #endregion

            if (ctlog == null || !ctlog.IsLoaded)
            {
                return false;
            }

            #region 只有核銷狀態才可以反核銷, 其它狀況人工處理

            if ((TrustStatus)ctlog.Status != TrustStatus.Verified)
            {
                return false;
            }
            if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.VerificationForced))
            {
                return false;
            }

            #endregion

            Guid? bid = null;
            int pid = 0;

            #region 憑證型的才可以反核銷

            switch (classification)
            {
                case OrderClassification.LkSite:
                    ViewPponCashTrustLog pponTrust = pp.ViewPponCashTrustLogGet(trustId);
                    if (!pponTrust.DeliveryType.HasValue || pponTrust.DeliveryType.Value != (int)DeliveryType.ToShop)
                    {
                        return false;
                    }
                    bid = pponTrust.Bid;

                    break;
                case OrderClassification.HiDeal:
                    HiDealOrderDetail piinDetail = hp.HiDealOrderDetailGet(ctlog.OrderDetailGuid);
                    if (piinDetail.DeliveryType != (int)DeliveryType.ToShop)
                    {
                        return false;
                    }
                    pid = piinDetail.ProductId;
                    break;
                default:
                    return false;
            }

            #endregion

            #region 商家對帳方式的檢查

            VendorBillingModel vendorBillingModel;
            if (classification == OrderClassification.LkSite)
            {
                DealAccounting da = pp.DealAccountingGet(bid.Value);
                if (!da.IsLoaded)
                {
                    return false;
                }
                vendorBillingModel = (VendorBillingModel)da.VendorBillingModel;
            }
            else
            {
                HiDealProduct product = hp.HiDealProductGet(pid);
                if (!product.IsLoaded)
                {
                    return false;
                }
                vendorBillingModel = (VendorBillingModel)product.VendorBillingModel;
            }

            if (vendorBillingModel != VendorBillingModel.BalanceSheetSystem)
            {
                // 不使用商家對帳系統:
                // 避免多給錢, 檢查 => 已從信託把錢取出的, 已匯錢給賣家的, 太久沒變化的 不可反核銷
                bool cannotUndo = DateTime.Now.Date > ctlog.ModifyTime.AddMonths(1).Date
                                || ctlog.ReportVerifiedGuid != null   // money taken from bank and may have gone to seller or back to user
                                || ctlog.StoreVerifiedGuid != null;   // money already gave to seller
                if (cannotUndo)
                {
                    return false;
                }
            }

            #endregion

            int memberUniqueId = MemberFacade.GetUniqueId(createId);

            ctlog = ChangeTrustLogDataForUnverify(ctlog);
            CashTrustStatusLog ctStatusLog = NewStatusLog(ctlog, createId);

            using (TransactionScope txScope = new TransactionScope())
            {
                BalanceSheetService.UndoDetail(trustId, memberUniqueId, UndoType.UnVerify);
                CashTrustLogCollection newCtlc = new CashTrustLogCollection();
                newCtlc.Add(ctlog);
                if (oldCtl != null)
                {
                    newCtlc.Add(oldCtl);
                }

                if (mp.CashTrustLogColSetForVerifyCoupons(newCtlc))
                {
                    mp.CashTrustStatusLogSet(ctStatusLog);
                    if (ctlog.CouponId.HasValue)
                    {
                        vp.UndoVerificationLog(ctlog.CouponId.Value, OrderClassification.LkSite);
                        op.CancelEinvoiceSetByUndo(ctlog.OrderGuid, ctlog.CouponId.Value, isGroupCoupon);
                        var gift = mgmp.GetActiveGiftByCouponId(ctlog.OrderGuid, ctlog.CouponId.Value);
                        if (gift.IsLoaded)
                        {
                            MgmGift mg = mgmp.GiftGetById(gift.Id);
                            if (mg.IsLoaded)
                            {
                                mg.IsUsed = false;
                                mg.UsedTime = null;
                                mgmp.MgmGiftSet(mg);
                            }
                        }

                    }

                    //即時通知代銷商
                    ChannelFacade.NotifyVerify(ctlog.BusinessHourGuid, ctlog.OrderGuid, ctlog.CouponId.Value, ctlog.Amount, NotifyVoucherStatus.UndoVerified, string.Empty, string.Empty, null);
                    txScope.Complete();

                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// 成套禮券欲取消的狀態[檢查]
        /// </summary>
        /// <param name="sellerUserId"></param>
        /// <param name="trustIds"></param>
        /// <param name="storeCode"></param>
        /// <param name="resultCode"></param>
        /// <param name="classification"></param>
        /// <returns></returns>
        public static bool UndoGroupCouponVerifiedStatusCheck(List<CashTrustLog> ctlogList, int sellerUserId, List<Guid> trustIds, string storeCode, ref ApiResultCode resultCode, OrderClassification classification = OrderClassification.LkSite)
        {
            Guid storeGuid = new Guid("00000000-0000-0000-0000-000000000000");

            //check storeCode碼並取得userId 
            var userId = SellerFacade.GetUserIdBySellerUserId(sellerUserId, storeCode, ref storeGuid);
            if (userId == null)
            {
                resultCode = ApiResultCode.PcpStoreNotFound;
                return false;
            }

            //檢查是否同一筆訂單
            if (ctlogList.ToList().GroupBy(p => p.OrderGuid).Count() != 1)
            {
                resultCode = ApiResultCode.PcpSerialsNotSameOrder;
                return false;
            }

            //確定有N筆trustLog
            if (ctlogList.Count() != trustIds.Count())
            {
                resultCode = ApiResultCode.InvalidCoupon;
                return false;
            }

            //檢查是否為同一個店家所核銷 
            if (ctlogList.Where(p => p.StoreGuid == storeGuid).Count() != trustIds.Count())
            {
                resultCode = ApiResultCode.PcpSerialsNotSameStore;
                return false;
            }

            //檢查所有憑證都為已核銷狀態
            if (ctlogList.Where(p => p.Status == (int)TrustStatus.Verified).Count() != trustIds.Count())
            {
                resultCode = ApiResultCode.CouponUnused;
                return false;
            }

            //不能有任何一筆是強迫核銷
            if (ctlogList.Where(p => p.SpecialStatus == (int)TrustSpecialStatus.VerificationForced).Any())
            {
                return false;
            }

            //檢查bid是否為有效憑證
            var bidList = ctlogList.Select(p => (p.BusinessHourGuid ?? new Guid("00000000-0000-0000-0000-000000000000"))).ToList();
            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetListByGuidList(bidList);
            if (!vpd.Any() || vpd.Count != trustIds.Count)
            {
                resultCode = ApiResultCode.InvalidCoupon;
                return false;
            }

            //check 檔次與核銷帳號間的關係
            var memOriginal = mp.VbsMembershipGetByUserId(sellerUserId);
            var acl = mp.ResourceAclGetListByAccountId(memOriginal.AccountId);
            if (!acl.Any())
            {
                resultCode = ApiResultCode.InvalidCoupon;
                return false;
            }
            if (acl.All(x => x.ResourceGuid != vpd.FirstOrDefault().SellerGuid))
            {
                resultCode = ApiResultCode.InvalidCoupon;
                return false;
            }

            foreach (var ctlog in ctlogList)
            {
                Guid? bid = null;
                int pid = 0;

                #region 憑證型的才可以反核銷

                switch (classification)
                {
                    case OrderClassification.LkSite:
                        ViewPponCashTrustLog pponTrust = pp.ViewPponCashTrustLogGet(ctlog.TrustId);
                        if (!pponTrust.DeliveryType.HasValue || pponTrust.DeliveryType.Value != (int)DeliveryType.ToShop)
                        {
                            return false;
                        }
                        bid = pponTrust.Bid;

                        break;
                    case OrderClassification.HiDeal:
                        HiDealOrderDetail piinDetail = hp.HiDealOrderDetailGet(ctlog.OrderDetailGuid);
                        if (piinDetail.DeliveryType != (int)DeliveryType.ToShop)
                        {
                            return false;
                        }
                        pid = piinDetail.ProductId;
                        break;
                    default:
                        return false;
                }

                #endregion

                #region 商家對帳方式的檢查

                VendorBillingModel vendorBillingModel;
                if (classification == OrderClassification.LkSite)
                {
                    DealAccounting da = pp.DealAccountingGet(bid.Value);
                    if (!da.IsLoaded)
                    {
                        return false;
                    }
                    vendorBillingModel = (VendorBillingModel)da.VendorBillingModel;
                }
                else
                {
                    HiDealProduct product = hp.HiDealProductGet(pid);
                    if (!product.IsLoaded)
                    {
                        return false;
                    }
                    vendorBillingModel = (VendorBillingModel)product.VendorBillingModel;
                }

                if (vendorBillingModel != VendorBillingModel.BalanceSheetSystem)
                {
                    // 不使用商家對帳系統:
                    // 避免多給錢, 檢查 => 已從信託把錢取出的, 已匯錢給賣家的, 太久沒變化的 不可反核銷
                    bool cannotUndo = DateTime.Now.Date > ctlog.ModifyTime.AddMonths(1).Date
                                    || ctlog.ReportVerifiedGuid != null   // money taken from bank and may have gone to seller or back to user
                                    || ctlog.StoreVerifiedGuid != null;   // money already gave to seller
                    if (cannotUndo)
                    {
                        return false;
                    }
                }

                #endregion
            }

            return true;

        }

        /// <summary>
        /// 成套禮券[取消]核銷檢查
        /// </summary>
        /// <param name="trustIds"></param>
        /// <param name="createId"></param>
        /// <param name="msg"></param>
        /// <param name="classification"></param>
        /// <returns></returns>
        public static bool UndoGroupCouponVerifiedStatus(List<Guid> trustIds, string createId, ref string msg, OrderClassification classification = OrderClassification.LkSite)
        {
            msg = string.Empty;
            var ctlogList = mp.CashTrustLogByTrustId(trustIds);
            int memberUniqueId = MemberFacade.GetUniqueId(createId);


            //取消核銷
            using (TransactionScope txScope = new TransactionScope())
            {
                bool isSuccessUndo = false;
                foreach (var oldCtlog in ctlogList)
                {
                    var ctlog = ChangeTrustLogDataForUnverify(oldCtlog);
                    CashTrustStatusLog ctStatusLog = NewStatusLog(ctlog, createId);
                    BalanceSheetService.UndoDetail(ctlog.TrustId, memberUniqueId, UndoType.UnVerify);

                    CashTrustLogCollection newCtlc = new CashTrustLogCollection();
                    newCtlc.Add(ctlog);

                    if (mp.CashTrustLogColSetForVerifyCoupons(newCtlc))
                    {
                        mp.CashTrustStatusLogSet(ctStatusLog);
                        if (ctlog.CouponId.HasValue)
                        {
                            vp.UndoVerificationLog(ctlog.CouponId.Value, OrderClassification.LkSite);

                            bool isGroupCoupon = Helper.IsFlagSet(op.GetHourStatusBybid(ctlog.BusinessHourGuid ?? Guid.NewGuid()).BusinessHourStatus, BusinessHourStatus.GroupCoupon);
                            op.CancelEinvoiceSetByUndo(ctlog.OrderGuid, ctlog.CouponId.Value, isGroupCoupon);
                            var gift = mgmp.GetActiveGiftByCouponId(ctlog.OrderGuid, ctlog.CouponId.Value);
                            if (gift.IsLoaded)
                            {
                                MgmGift mg = mgmp.GiftGetById(gift.Id);
                                if (mg.IsLoaded)
                                {
                                    mg.IsUsed = false;
                                    mg.UsedTime = null;
                                    mgmp.MgmGiftSet(mg);
                                }
                            }
                            isSuccessUndo = true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    if (!isSuccessUndo)
                    {
                        return false;//只要有一筆失敗就return false
                    }
                }
                if (isSuccessUndo)
                {
                    txScope.Complete();
                    return true;
                }
            }
            return false;
        }

        private static CashTrustLog ChangeTrustLogDataForUnverify(CashTrustLog ctlog)
        {
            if (Helper.IsFlagSet(ctlog.BankStatus, TrustBankStatus.Trusted)
                && !Helper.IsFlagSet(ctlog.BankStatus, TrustBankStatus.Verified)
                )
            {
                ctlog.Status = (int)TrustStatus.Trusted;
            }
            else
            {
                ctlog.Status = (int)TrustStatus.Initial;
                ctlog.ReportTrustedGuid = null;
                ctlog.ReportVerifiedGuid = null;
                ctlog.TrustedTime = null;
                ctlog.TrustedBankTime = null;
                ctlog.VerifiedTime = null;
                ctlog.VerifiedBankTime = null;
                ctlog.BankStatus = (int)TrustBankStatus.Initial;
            }

            ctlog.UsageVerifiedTime = null;
            ctlog.VerifiedStoreGuid = null;
            ctlog.UsageVerifyId = null;
            ctlog.ModifyTime = DateTime.Now;

            return ctlog;
        }
        private static CashTrustStatusLog NewStatusLog(CashTrustLog ctlog, string createId)
        {
            CashTrustStatusLog ctStatusLog = new CashTrustStatusLog();
            ctStatusLog.TrustId = ctlog.TrustId;
            ctStatusLog.TrustProvider = ctlog.TrustProvider;
            ctStatusLog.Amount = ctlog.Scash + ctlog.CreditCard + ctlog.Atm;
            ctStatusLog.Status = ctlog.Status;
            ctStatusLog.ModifyTime = ctlog.ModifyTime;
            ctStatusLog.CreateId = createId;
            return ctStatusLog;
        }

        public static void VerificationSatisticsLogSetRightNow(Guid bid, string username, VerificationStatisticsLogStatus status)
        {
            IEnumerable<ViewPponCashTrustLog> ctCol = pp.ViewPponCashTrustLogGetList(bid).Where(x => !Helper.IsFlagSet((TrustSpecialStatus)x.CashTrustLogSpecialStatus, TrustSpecialStatus.Freight));

            VerificationStatisticsLog vsl = new VerificationStatisticsLog();
            vsl.Unverified = ctCol.Where(x => x.CashTrustLogStatus == (int)TrustStatus.Initial || x.CashTrustLogStatus == (int)TrustStatus.Trusted).Count();
            vsl.Verified = ctCol.Where(x => x.CashTrustLogStatus == (int)TrustStatus.Verified).Count();
            vsl.Returned = ctCol.Where(x => x.CashTrustLogStatus == (int)TrustStatus.Returned || x.CashTrustLogStatus == (int)TrustStatus.Refunded).Count();
            vsl.ForcedVerified = ctCol.Where(x => Helper.IsFlagSet((TrustSpecialStatus)x.CashTrustLogSpecialStatus, TrustSpecialStatus.VerificationForced)).Count();
            vsl.ForcedReturned = ctCol.Where(x => Helper.IsFlagSet((TrustSpecialStatus)x.CashTrustLogSpecialStatus, TrustSpecialStatus.ReturnForced)).Count();
            vsl.Lost = ctCol.Where(x => Helper.IsFlagSet((TrustSpecialStatus)x.CashTrustLogSpecialStatus, TrustSpecialStatus.VerificationLost)).Count();
            vsl.Adjustment = 0;
            vsl.Status = (int)status;

            pp.VerificationStatisticsLogInsert(vsl, bid, username);
        }

        public static DateTime? GetCouponVerifiedTime(int couponId, OrderClassification orderClassification)
        {
            Guid trustId = mp.CashTrustLogGetByCouponId(couponId, orderClassification).TrustId;
            ViewVbsCashTrustLogStatusLogInfo info = vp.ViewVbsCashTrustLogStatusLogInfoGet(trustId);
            return info.VerifyTime;
        }

        public static bool SetOrderReturList(OrderReturnList otl, string modifyMessage = null)
        {
            if (otl.Status != (int)OrderReturnStatus.ExchangeProcessing
                && otl.Status != (int)OrderReturnStatus.ExchangeSuccess
                && otl.Status != (int)OrderReturnStatus.ExchangeFailure
                && otl.Status != (int)OrderReturnStatus.ExchangeCancel
                && otl.Status != (int)OrderReturnStatus.SendToSeller)
            {
                return true;
            }

            try
            {
                op.OrderReturnListSet(otl);
                otl.Message = !string.IsNullOrEmpty(modifyMessage) ? modifyMessage : otl.Message;
                PaymentFacade.SaveOrderStatusLog(otl);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static OrderReturnStatus TransferOrderLogStatusToOrderReturnStatus(OrderLogStatus status)
        {
            var orderReturnStatus = OrderReturnStatus.SendToSeller;

            switch (status)
            {
                case OrderLogStatus.ExchangeProcessing: // 換貨處理中
                    orderReturnStatus = OrderReturnStatus.ExchangeProcessing;
                    break;
                case OrderLogStatus.ExchangeSuccess: // 換貨成功
                    orderReturnStatus = OrderReturnStatus.ExchangeSuccess;
                    break;
                case OrderLogStatus.ExchangeFailure: // 換貨失敗
                    orderReturnStatus = OrderReturnStatus.ExchangeFailure;
                    break;
                case OrderLogStatus.ExchangeCancel: //換貨取消
                    orderReturnStatus = OrderReturnStatus.ExchangeCancel;
                    break;
                case OrderLogStatus.SendToSeller: //已通報廠商
                    orderReturnStatus = OrderReturnStatus.SendToSeller;
                    break;
                default:
                    break;
            }

            return orderReturnStatus;
        }

        public static OrderLogStatus TransferOrderReturnStatusToOrderLogStatus(OrderReturnStatus status)
        {
            var orderLogStatus = OrderLogStatus.SendToSeller;

            switch (status)
            {
                case OrderReturnStatus.ExchangeProcessing:
                    orderLogStatus = OrderLogStatus.ExchangeProcessing;
                    break;
                case OrderReturnStatus.ExchangeSuccess:
                    orderLogStatus = OrderLogStatus.ExchangeSuccess;
                    break;
                case OrderReturnStatus.ExchangeFailure:
                    orderLogStatus = OrderLogStatus.ExchangeFailure;
                    break;
                case OrderReturnStatus.ExchangeCancel:
                    orderLogStatus = OrderLogStatus.ExchangeCancel;
                    break;
                case OrderReturnStatus.SendToSeller:
                    orderLogStatus = OrderLogStatus.SendToSeller;
                    break;
                default:
                    break;
            }

            return orderLogStatus;
        }

        public static CashTrustLogCollection MakeCashTrust(CashTrustInfo cashTrustInfo)
        {
            // 支付優先順序為：sCash, pCash, bCash, creditCard, discount, atm, isp
            #region local variables

            int creditCardAmount = cashTrustInfo.CreditCardAmount;
            int sCashAmount = cashTrustInfo.SCashAmount;
            int psCashAmount = cashTrustInfo.PscashAmount;
            int pCashAmount = cashTrustInfo.PCashAmount;
            int bCashAmount = cashTrustInfo.BCashAmount;
            int lCashAmount = cashTrustInfo.LCashAmount;
            int deliveryCharge = cashTrustInfo.DeliveryCharge;
            int discountAmount = cashTrustInfo.DiscountAmount;
            int atmAmount = cashTrustInfo.AtmAmount;
            int familyIspAmount = cashTrustInfo.FamilyIspAmount;
            int sevenIspAmount = cashTrustInfo.SevenIspAmount;
            int thirdPartyAmount = cashTrustInfo.ThirdPartyCashAmount;
            int totalAmount = creditCardAmount + sCashAmount + psCashAmount + pCashAmount + bCashAmount + discountAmount + atmAmount + familyIspAmount + sevenIspAmount
                + lCashAmount + thirdPartyAmount - deliveryCharge;
            int PresentQuantity = cashTrustInfo.PresentQuantity;
            int itemPrice = cashTrustInfo.ItemPrice;
            int specialPrice = 0; //成套票券憑證特殊面額
            int specialZeroPriceCount = 0; //成套票券特殊面額為0的數量
            switch (cashTrustInfo.GroupCouponType)
            {
                case GroupCouponDealType.AvgAssign:
                    itemPrice = decimal.ToInt32(Math.Ceiling((decimal)itemPrice / (cashTrustInfo.Quantity - cashTrustInfo.PresentQuantity)));
                    break;
                case GroupCouponDealType.CostAssign:
                    int costCount = 0;
                    //無條件進位取單張最大面額
                    itemPrice = decimal.ToInt32(Math.Ceiling((decimal)cashTrustInfo.ItemOriPrice / cashTrustInfo.Quantity));
                    //無條件捨去取固定面額張數
                    costCount = decimal.ToInt32(Math.Floor((decimal)totalAmount / itemPrice));
                    //扣除特殊面額張數，算面額0的
                    specialZeroPriceCount = (cashTrustInfo.Quantity - cashTrustInfo.PresentQuantity) - costCount - 1;
                    specialPrice = totalAmount - (itemPrice * costCount);
                    //特殊面額如果是0
                    specialZeroPriceCount = specialPrice > 0 ? specialZeroPriceCount : specialZeroPriceCount + 1;
                    break;
                default:
                    break;
            }

            CashTrustLogCollection ctlc = new CashTrustLogCollection();
            CashTrustStatusLogCollection ctslc = new CashTrustStatusLogCollection();
            UserCashTrustLogCollection uctlc = new UserCashTrustLogCollection();

            int uninvoiced = (-1) * Convert.ToInt32(op.ViewScashTransactionGetList(0, 0, ViewScashTransaction.Columns.CreateTime,
                    ViewScashTransaction.Columns.Amount + "<0", ViewScashTransaction.Columns.OrderGuid + "=" + cashTrustInfo.OrderGuid,
                    ViewScashTransaction.Columns.Invoiced + "=false").Sum(x => x.Amount).Value);

            #endregion

            TrustStatus trustStatus;
            if (atmAmount > 0)
            {
                trustStatus = TrustStatus.ATM;
            }
            else if (familyIspAmount > 0)
            {
                trustStatus = TrustStatus.FamilyIsp;
            }
            else if (sevenIspAmount > 0)
            {
                trustStatus = TrustStatus.SevenIsp;
            }
            else
            {
                trustStatus = TrustStatus.Initial;
            }

            #region create scash trust

            int index = 0;
            int subCount = cashTrustInfo.OrderDetails[index].ItemQuantity;
            Guid orderDetailGuid = cashTrustInfo.OrderDetails[index].Guid;
            Guid? storeGuid = cashTrustInfo.OrderDetails[index].StoreGuid;
            for (int i = 0; i < cashTrustInfo.Quantity; i++)
            {
                if (i + 1 > subCount)
                {
                    index++;
                    if (cashTrustInfo.IsGroupCoupon)
                    {
                        orderDetailGuid = cashTrustInfo.OrderDetails[0].Guid;
                        storeGuid = cashTrustInfo.OrderDetails[0].StoreGuid;
                    }
                    else
                    {
                        subCount += cashTrustInfo.OrderDetails[index].ItemQuantity;
                        orderDetailGuid = cashTrustInfo.OrderDetails[index].Guid;
                        storeGuid = cashTrustInfo.OrderDetails[index].StoreGuid;
                    }
                }

                CashTrustLog ctl = new CashTrustLog();
                ctl.CreateTime = DateTime.Now;
                ctl.ItemName = cashTrustInfo.ItemName;
                ctl.ModifyTime = DateTime.Now;
                ctl.OrderGuid = cashTrustInfo.OrderGuid;
                ctl.OrderDetailGuid = orderDetailGuid;
                ctl.Status = (int)trustStatus;
                ctl.TrustId = Helper.GetNewGuid(ctl);
                ctl.TrustProvider = (int)cashTrustInfo.TrustProvider;
                ctl.UserId = cashTrustInfo.User.UniqueId;
                ctl.OrderClassification = (int)cashTrustInfo.OrderClass;
                //for weeklypay add column business_hour_guid,checkout_type
                ctl.BusinessHourGuid = cashTrustInfo.BusinessHourGuid;
                ctl.CheckoutType = cashTrustInfo.CheckoutType;
                ctl.DeliveryType = (int)cashTrustInfo.DeliveryType;
                ctl.StoreGuid = storeGuid;
                ctl.ThirdPartyPayment = (byte)cashTrustInfo.ThirdPartySystem;
                ctlc.Add(ctl);
            }

            #endregion

            #region 分錢
            // 支付優先順序為：sCash, pCash, bCash, creditCard, discount, atm, isp
            // 聽說用除的永遠都會有不準的情況發生，所以改為最笨的一塊一塊分

            #region 先分完錢

            bool done = false;
            Hashtable completeSet = new Hashtable();
            int kx = 0;
            int sp = specialPrice;
            int sc = specialZeroPriceCount;
            CashTrustLog tempCtl;
            do
            {
                tempCtl = ctlc[kx % (ctlc.Count - PresentQuantity)];

                //成套票券憑證特殊面額
                if (sc > 0 && tempCtl.Amount == 0)
                {
                    completeSet.Add(tempCtl.TrustId, 1);
                    tempCtl.SpecialStatus = (int)Helper.SetFlag(true, tempCtl.SpecialStatus, TrustSpecialStatus.SpecialDiscount);
                    sc--;
                }

                //成套票券憑證特殊面額
                if (sp > 0 && tempCtl.Amount == sp)
                {
                    completeSet.Add(tempCtl.TrustId, 1);
                    tempCtl.SpecialStatus = (int)Helper.SetFlag(true, tempCtl.SpecialStatus, TrustSpecialStatus.SpecialDiscount);
                    sp = 0;
                }

                if (completeSet.ContainsKey(tempCtl.TrustId))
                {
                    kx++;
                    continue;
                }

                if (tempCtl.Amount != itemPrice && sCashAmount > 0)
                {
                    tempCtl.Scash++;
                    sCashAmount--;
                    tempCtl.Amount++;
                    totalAmount--;
                    if (tempCtl.Amount == sp && sp > 0)
                    {
                        continue;
                    }
                }

                if (tempCtl.Amount != itemPrice && psCashAmount > 0)
                {
                    tempCtl.Pscash++;
                    psCashAmount--;
                    tempCtl.Amount++;
                    totalAmount--;
                    if (tempCtl.Amount == sp && sp > 0)
                    {
                        continue;
                    }
                }

                if (tempCtl.Amount != itemPrice && pCashAmount > 0)
                {
                    tempCtl.Pcash++;
                    pCashAmount--;
                    tempCtl.Amount++;
                    totalAmount--;
                    if (tempCtl.Amount == sp && sp > 0)
                    {
                        continue;
                    }
                }

                if (tempCtl.Amount != itemPrice && bCashAmount > 0)
                {
                    tempCtl.Bcash++;
                    bCashAmount--;
                    tempCtl.Amount++;
                    totalAmount--;
                    if (tempCtl.Amount == sp && sp > 0)
                    {
                        continue;
                    }
                }

                if (tempCtl.Amount != itemPrice && creditCardAmount > 0)
                {
                    tempCtl.CreditCard++;
                    creditCardAmount--;
                    tempCtl.Amount++;
                    totalAmount--;
                    if (tempCtl.Amount == sp && sp > 0)
                    {
                        continue;
                    }
                }

                if (tempCtl.Amount != itemPrice && discountAmount > 0)
                {
                    tempCtl.DiscountAmount++;
                    discountAmount--;
                    tempCtl.Amount++;
                    totalAmount--;
                    if (tempCtl.Amount == sp && sp > 0)
                    {
                        continue;
                    }
                }

                if (tempCtl.Amount != itemPrice && atmAmount > 0)
                {
                    tempCtl.Atm++;
                    atmAmount--;
                    tempCtl.Amount++;
                    totalAmount--;
                    if (tempCtl.Amount == sp && sp > 0)
                    {
                        continue;
                    }
                }

                if (tempCtl.Amount != itemPrice && familyIspAmount > 0)
                {
                    tempCtl.FamilyIsp++;
                    familyIspAmount--;
                    tempCtl.Amount++;
                    totalAmount--;
                    if (tempCtl.Amount == sp && sp > 0)
                    {
                        continue;
                    }
                }

                if (tempCtl.Amount != itemPrice && sevenIspAmount > 0)
                {
                    tempCtl.SevenIsp++;
                    sevenIspAmount--;
                    tempCtl.Amount++;
                    totalAmount--;
                    if (tempCtl.Amount == sp && sp > 0)
                    {
                        continue;
                    }
                }

                if (tempCtl.Amount != itemPrice && lCashAmount > 0)
                {
                    tempCtl.Lcash++;
                    lCashAmount--;
                    tempCtl.Amount++;
                    totalAmount--;
                    if (tempCtl.Amount == sp && sp > 0)
                    {
                        continue;
                    }
                }

                if (tempCtl.Amount != itemPrice && thirdPartyAmount > 0)
                {
                    tempCtl.Tcash++;
                    thirdPartyAmount--;
                    tempCtl.Amount++;
                    totalAmount--;
                    if (tempCtl.Amount == sp && sp > 0)
                    {
                        continue;
                    }
                }

                if (int.Equals(itemPrice, tempCtl.Amount) && !completeSet.Contains(tempCtl.TrustId))
                {
                    completeSet.Add(tempCtl.TrustId, 1);
                }

                //成套票券贈送憑證票面價值為0(注意0元檔次)
                if (totalAmount == 0 && !int.Equals(ctlc.Count, completeSet.Count) && PresentQuantity > 0)
                {
                    foreach (CashTrustLog c in ctlc.Where(x => x.Amount == 0 && x.SpecialStatus == 0))
                    {
                        //tempCtl.Status = (int)TrustStatus.None; -1
                        c.SpecialStatus = (int)Helper.SetFlag(true, c.SpecialStatus, TrustSpecialStatus.Giveaway);
                        c.Amount = 0;
                        completeSet.Add(c.TrustId, 1);
                    }

                }

                if (int.Equals(ctlc.Count, completeSet.Count))
                {
                    done = true;
                }

                kx++;
            } while (!done && totalAmount > 0);

            #region 改用payment_type_amount
            PaymentTypeAmountCollection pCol = new PaymentTypeAmountCollection();
            int[,] tmpPayments = new int[0, 0];
            //排除0元
            int ctlcCount = ctlc.Count() - PresentQuantity - specialZeroPriceCount;
            if (cashTrustInfo.Payments != null)
            {
                #region tmpPayments初始化
                tmpPayments = new int[ctlcCount + 1, cashTrustInfo.Payments.Count() + 1];

                int b = 0;
                foreach (var payment in cashTrustInfo.Payments)
                {
                    for (int a = 0; a < ctlcCount; a++)
                    {
                        tmpPayments[a, b] = 0;
                    }
                    tmpPayments[ctlcCount, b] = payment.Value;                              //最後一列當作該付款別總數暫存變數
                    tmpPayments[ctlcCount, cashTrustInfo.Payments.Count()] += payment.Value; //最後一列最後一欄當作總金額暫存項
                    b++;
                }
                tmpPayments[ctlcCount, cashTrustInfo.Payments.Count()] -= cashTrustInfo.DeliveryCharge;
                #endregion tmpPayments初始化

                #region 分錢機制
                int c = 0;
                int d = 0;
                do
                {
                    d = c % ctlcCount;
                    int e = 0;
                    foreach (var payment in cashTrustInfo.Payments)
                    {
                        if (tmpPayments[d, cashTrustInfo.Payments.Count()] != (d == 0 && specialPrice > 0 ? specialPrice : itemPrice) && tmpPayments[ctlcCount, e] > 0)
                        {
                            tmpPayments[d, e]++;
                            tmpPayments[ctlcCount, e]--;
                            tmpPayments[d, cashTrustInfo.Payments.Count()]++;               //最後一欄當作該coupon總金額累積項
                            tmpPayments[ctlcCount, cashTrustInfo.Payments.Count()]--;
                        }
                        e++;
                    }
                    c++;
                } while (tmpPayments[ctlcCount, cashTrustInfo.Payments.Count()] > 0);
                #endregion 分錢機制

                #region 紀錄payment_type_amount
                for (int f = 0; f < ctlcCount; f++)
                {
                    int g = 0;
                    foreach (var payment in cashTrustInfo.Payments)
                    {
                        if (tmpPayments[f, g] > 0)
                        {
                            PaymentTypeAmount p = new PaymentTypeAmount();
                            p.TrustId = ctlc.Where(z => z.Amount > 0).ToList()[f].TrustId;
                            p.OrderGuid = ctlc.Where(z => z.Amount > 0).ToList()[f].OrderGuid;
                            p.PaymentType = (int)payment.Key;
                            p.Amount = tmpPayments[f, g];
                            pCol.Add(p);
                        }
                        g++;
                    }
                }

                ////成套贈送
                //for(int h = 0;h < PresentQuantity;h++)
                //{
                //    PaymentTypeAmount p = new PaymentTypeAmount();
                //    p.TrustId = ctlc.Where(z => Helper.IsFlagSet(z.SpecialStatus, TrustSpecialStatus.Prezzie)).ToList()[h].TrustId;
                //    p.OrderGuid = ctlc.Where(z => Helper.IsFlagSet(z.SpecialStatus, TrustSpecialStatus.Prezzie)).ToList()[h].OrderGuid;
                //    p.PaymentType = (int)PaymentType.PrezzieCash;
                //    p.Amount = itemPrice;
                //    pCol.Add(p);
                //}
                #endregion 紀錄payment_type_amount
            }
            #endregion 改用payment_type_amount

            #endregion 先分完錢

            #region 再檢查有沒有用到 scash

            foreach (CashTrustLog ct in ctlc)
            {
                if (ct.Scash > 0)
                {
                    UserCashTrustLog addScash = new UserCashTrustLog();
                    addScash.Amount = 0 - ct.Scash;
                    addScash.CreateTime = DateTime.Now;
                    addScash.UserId = cashTrustInfo.User.UniqueId;
                    addScash.Message = "使用購物金結帳";
                    addScash.TrustProvider = (int)TrustProvider.TaiShin;
                    addScash.TrustId = ct.TrustId;
                    addScash.Status = (int)trustStatus; ;

                    uctlc.Add(addScash);
                }

                if (ct.Pscash > 0)
                {
                    UserCashTrustLog addPscash = new UserCashTrustLog();
                    addPscash.Amount = 0 - ct.Pscash;
                    addPscash.CreateTime = DateTime.Now;
                    addPscash.UserId = cashTrustInfo.User.UniqueId;
                    addPscash.Message = "使用Payeasy兌換的購物金結帳";
                    addPscash.TrustProvider = (int)TrustProvider.TaiShin;
                    addPscash.TrustId = ct.TrustId;
                    addPscash.Status = (int)trustStatus;

                    uctlc.Add(addPscash);
                }

                if (ct.CreditCard > 0)
                {
                    UserCashTrustLog addScash = new UserCashTrustLog();
                    addScash.Amount = ct.CreditCard;
                    addScash.CreateTime = DateTime.Now;
                    addScash.UserId = cashTrustInfo.User.UniqueId;
                    addScash.Message = "刷卡轉購物金";
                    addScash.TrustProvider = (int)TrustProvider.TaiShin;
                    addScash.TrustId = ct.TrustId;
                    addScash.Status = (int)TrustStatus.Initial;

                    uctlc.Add(addScash);

                    UserCashTrustLog useScash = new UserCashTrustLog();
                    useScash.Amount = 0 - ct.CreditCard;
                    useScash.CreateTime = DateTime.Now;
                    useScash.UserId = cashTrustInfo.User.UniqueId;
                    useScash.Message = "使用購物金換憑證";
                    useScash.TrustProvider = (int)TrustProvider.TaiShin;
                    useScash.TrustId = ct.TrustId;
                    useScash.Status = (int)TrustStatus.Initial;

                    uctlc.Add(useScash);
                }

                if (ct.Atm > 0)
                {
                    UserCashTrustLog addScash = new UserCashTrustLog();
                    addScash.Amount = ct.Atm;
                    addScash.CreateTime = DateTime.Now;
                    addScash.UserId = cashTrustInfo.User.UniqueId;
                    addScash.Message = "ATM轉購物金";
                    addScash.TrustProvider = (int)TrustProvider.TaiShin;
                    addScash.TrustId = ct.TrustId;
                    addScash.Status = (int)TrustStatus.ATM;

                    uctlc.Add(addScash);

                    UserCashTrustLog useScash = new UserCashTrustLog();
                    useScash.Amount = 0 - ct.Atm;
                    useScash.CreateTime = DateTime.Now;
                    useScash.UserId = cashTrustInfo.User.UniqueId;
                    useScash.Message = "使用購物金換憑證";
                    useScash.TrustProvider = (int)TrustProvider.TaiShin;
                    useScash.TrustId = ct.TrustId;
                    useScash.Status = (int)TrustStatus.ATM;

                    uctlc.Add(useScash);
                }

                if (ct.FamilyIsp > 0)
                {
                    UserCashTrustLog addScash = new UserCashTrustLog();
                    addScash.Amount = ct.FamilyIsp;
                    addScash.CreateTime = DateTime.Now;
                    addScash.UserId = cashTrustInfo.User.UniqueId;
                    addScash.Message = "全家超商付款";
                    addScash.TrustProvider = (int)TrustProvider.TaiShin;
                    addScash.TrustId = ct.TrustId;
                    addScash.Status = (int)TrustStatus.FamilyIsp;

                    uctlc.Add(addScash);

                    UserCashTrustLog useScash = new UserCashTrustLog();
                    useScash.Amount = 0 - ct.FamilyIsp;
                    useScash.CreateTime = DateTime.Now;
                    useScash.UserId = cashTrustInfo.User.UniqueId;
                    useScash.Message = "使用購物金換憑證";
                    useScash.TrustProvider = (int)TrustProvider.TaiShin;
                    useScash.TrustId = ct.TrustId;
                    useScash.Status = (int)TrustStatus.FamilyIsp;

                    uctlc.Add(useScash);
                }

                if (ct.SevenIsp > 0)
                {
                    UserCashTrustLog addScash = new UserCashTrustLog();
                    addScash.Amount = ct.SevenIsp;
                    addScash.CreateTime = DateTime.Now;
                    addScash.UserId = cashTrustInfo.User.UniqueId;
                    addScash.Message = "7-11超商付款";
                    addScash.TrustProvider = (int)TrustProvider.TaiShin;
                    addScash.TrustId = ct.TrustId;
                    addScash.Status = (int)TrustStatus.SevenIsp;

                    uctlc.Add(addScash);

                    UserCashTrustLog useScash = new UserCashTrustLog();
                    useScash.Amount = 0 - ct.SevenIsp;
                    useScash.CreateTime = DateTime.Now;
                    useScash.UserId = cashTrustInfo.User.UniqueId;
                    useScash.Message = "使用購物金換憑證";
                    useScash.TrustProvider = (int)TrustProvider.TaiShin;
                    useScash.TrustId = ct.TrustId;
                    useScash.Status = (int)TrustStatus.SevenIsp;

                    uctlc.Add(useScash);
                }

                // 紀錄刷卡未開立發票金額
                if (uninvoiced > 0)
                {
                    ct.UninvoicedAmount = (uninvoiced - (ct.CreditCard + ct.Tcash) > 0) ? (ct.CreditCard + ct.Tcash) : uninvoiced;
                    uninvoiced -= ct.UninvoicedAmount;
                }

                CashTrustStatusLog ctsl = new CashTrustStatusLog();
                ctsl.Amount = ct.CreditCard + ct.Scash;
                ctsl.ModifyTime = ct.ModifyTime;
                ctsl.Status = ct.Status;
                ctsl.TrustId = ct.TrustId;
                ctsl.TrustProvider = ct.TrustProvider;
                ctsl.CreateId = cashTrustInfo.User.UserName;
                ctslc.Add(ctsl);
            }

            #endregion

            #region 計算運費

            if (deliveryCharge > 0)
            {
                // 計算運費成本
                int freightCost = 0;
                CouponFreightCollection cfc = pp.CouponFreightGetList(cashTrustInfo.BusinessHourGuid, CouponFreightType.Cost);

                if (null != cfc && cfc.Count > 0)
                {
                    foreach (CouponFreight cf in cfc)
                    {
                        if (totalAmount >= cf.StartAmount && cf.EndAmount > totalAmount)
                        {
                            freightCost += (int)(cf.FreightAmount);
                        }
                    }
                }

                IEnumerable<OrderDetail> odc = op.OrderDetailGetList(0, 0, null, cashTrustInfo.OrderGuid, OrderDetailTypes.System).Where(x => string.Equals("運費", x.ItemName));
                foreach (OrderDetail odt in odc)
                {
                    CashTrustLog ctl = new CashTrustLog();
                    ctl.Amount = deliveryCharge;
                    ctl.CreateTime = DateTime.Now;
                    ctl.ItemName = "運費";
                    ctl.ModifyTime = DateTime.Now;
                    ctl.OrderGuid = cashTrustInfo.OrderGuid;
                    ctl.OrderDetailGuid = odt.Guid;
                    ctl.Status = (int)trustStatus;
                    ctl.TrustId = Helper.GetNewGuid(ctl);
                    ctl.TrustProvider = (int)cashTrustInfo.TrustProvider;
                    ctl.UserId = cashTrustInfo.User.UniqueId;
                    ctl.Cost = freightCost;
                    ctl.OrderClassification = (int)OrderClassification.LkSite;
                    ctl.SpecialStatus = (int)TrustSpecialStatus.Freight;
                    ctl.DeliveryType = (int)cashTrustInfo.DeliveryType;

                    //for weeklypay add column business_hour_guid,checkout_type
                    ctl.BusinessHourGuid = cashTrustInfo.BusinessHourGuid;
                    ctl.CheckoutType = cashTrustInfo.CheckoutType;

                    #region 找出運費支付方式

                    // 支付優先順序為：sCash, pCash, bCash, creditCard, discount, atm, familyIsp, sevenIsp
                    bool checkAll = false;
                    do
                    {
                        if (deliveryCharge > 0 && sCashAmount > 0)
                        {
                            int xyz = sCashAmount > deliveryCharge ? deliveryCharge : sCashAmount;
                            deliveryCharge -= xyz;
                            sCashAmount -= xyz;
                            totalAmount -= xyz;
                            ctl.Scash = xyz;
                        }

                        if (deliveryCharge > 0 && psCashAmount > 0)
                        {
                            int xyz = psCashAmount > deliveryCharge ? deliveryCharge : psCashAmount;
                            deliveryCharge -= xyz;
                            psCashAmount -= xyz;
                            totalAmount -= xyz;
                            ctl.Pscash = xyz;
                        }

                        if (deliveryCharge > 0 && pCashAmount > 0)
                        {
                            int xyz = pCashAmount > deliveryCharge ? deliveryCharge : pCashAmount;
                            deliveryCharge -= xyz;
                            pCashAmount -= xyz;
                            totalAmount -= xyz;
                            ctl.Pcash = xyz;
                        }

                        if (deliveryCharge > 0 && bCashAmount > 0)
                        {
                            int xyz = bCashAmount > deliveryCharge ? deliveryCharge : bCashAmount;
                            deliveryCharge -= xyz;
                            bCashAmount -= xyz;
                            totalAmount -= xyz;
                            ctl.Bcash = xyz;
                        }

                        if (deliveryCharge > 0 && creditCardAmount > 0)
                        {
                            int xyz = creditCardAmount > deliveryCharge ? deliveryCharge : creditCardAmount;
                            deliveryCharge -= xyz;
                            creditCardAmount -= xyz;
                            totalAmount -= xyz;
                            ctl.CreditCard = xyz;
                        }

                        if (deliveryCharge > 0 && thirdPartyAmount > 0)
                        {
                            int xyz = thirdPartyAmount > deliveryCharge ? deliveryCharge : thirdPartyAmount;
                            deliveryCharge -= xyz;
                            thirdPartyAmount -= xyz;
                            totalAmount -= xyz;
                            ctl.Tcash = xyz;
                            ctl.ThirdPartyPayment = (byte)cashTrustInfo.ThirdPartySystem;
                        }

                        if (deliveryCharge > 0 && discountAmount > 0)
                        {
                            int xyz = discountAmount > deliveryCharge ? deliveryCharge : discountAmount;
                            deliveryCharge -= xyz;
                            discountAmount -= xyz;
                            totalAmount -= xyz;
                            ctl.DiscountAmount = xyz;
                        }

                        if (deliveryCharge > 0 && atmAmount > 0)
                        {
                            int xyz = atmAmount > deliveryCharge ? deliveryCharge : atmAmount;
                            deliveryCharge -= xyz;
                            atmAmount -= xyz;
                            totalAmount -= xyz;
                            ctl.Atm = xyz;
                        }

                        if (deliveryCharge > 0 && familyIspAmount > 0)
                        {
                            int xyz = familyIspAmount > deliveryCharge ? deliveryCharge : familyIspAmount;
                            deliveryCharge -= xyz;
                            familyIspAmount -= xyz;
                            totalAmount -= xyz;
                            ctl.FamilyIsp = xyz;
                        }

                        if (deliveryCharge > 0 && sevenIspAmount > 0)
                        {
                            int xyz = sevenIspAmount > deliveryCharge ? deliveryCharge : sevenIspAmount;
                            deliveryCharge -= xyz;
                            sevenIspAmount -= xyz;
                            totalAmount -= xyz;
                            ctl.SevenIsp = xyz;
                        }

                        if (deliveryCharge > 0 && lCashAmount > 0)
                        {
                            int xyz = lCashAmount > deliveryCharge ? deliveryCharge : lCashAmount;
                            deliveryCharge -= xyz;
                            atmAmount -= xyz;
                            totalAmount -= xyz;
                            ctl.Lcash = xyz;
                        }

                        checkAll = true;
                    } while (!checkAll && deliveryCharge > 0);

                    #region 運費改用payment_type_amount
                    if (cashTrustInfo.Payments != null)
                    {
                        #region tmpDelivery初始化
                        int[] tmpDelivery = new int[cashTrustInfo.Payments.Count() + 1];
                        int h = 0;
                        foreach (var payment in cashTrustInfo.Payments)
                        {
                            tmpDelivery[h] = 0;
                            h++;
                        }
                        tmpDelivery[cashTrustInfo.Payments.Count()] = cashTrustInfo.DeliveryCharge; //最後一欄當作總運費暫存項
                        #endregion tmpDelivery初始化

                        #region 分錢機制
                        bool finish = false;
                        do
                        {
                            int i = 0;
                            foreach (var payment in cashTrustInfo.Payments)
                            {
                                if (tmpDelivery[cashTrustInfo.Payments.Count()] > 0 && tmpPayments[ctlc.Count(), i] > 0)
                                {
                                    int j = (tmpPayments[ctlc.Count(), i] > tmpDelivery[cashTrustInfo.Payments.Count()]) ? tmpDelivery[cashTrustInfo.Payments.Count()] : tmpPayments[ctlc.Count(), i];
                                    tmpDelivery[cashTrustInfo.Payments.Count()] -= j;
                                    tmpPayments[ctlc.Count(), i] -= j;
                                    tmpDelivery[i] = j;
                                }
                                i++;
                            }
                            finish = true;
                        } while (!finish && tmpDelivery[cashTrustInfo.Payments.Count()] > 0);
                        #endregion 分錢機制

                        #region 紀錄payment_type_amount
                        PaymentTypeAmount dPayment = new PaymentTypeAmount();
                        int y = 0;
                        foreach (var payment in cashTrustInfo.Payments)
                        {
                            if (tmpDelivery[y] > 0)
                            {
                                PaymentTypeAmount p = new PaymentTypeAmount();
                                p.TrustId = ctl.TrustId;
                                p.OrderGuid = ctl.OrderGuid;
                                p.PaymentType = (int)payment.Key;
                                p.Amount = tmpDelivery[y];
                                pCol.Add(p);
                            }
                            y++;
                        }
                        #endregion 紀錄payment_type_amount
                    }
                    #endregion 運費改用payment_type_amount

                    #endregion 找出運費支付方式

                    if (ctl.Scash > 0)
                    {
                        UserCashTrustLog addScash = new UserCashTrustLog();
                        addScash.Amount = 0 - ctl.Scash;
                        addScash.CreateTime = DateTime.Now;
                        addScash.UserId = cashTrustInfo.User.UniqueId;
                        addScash.Message = "使用購物金結帳";
                        addScash.TrustProvider = (int)TrustProvider.TaiShin;
                        addScash.TrustId = ctl.TrustId;

                        uctlc.Add(addScash);
                    }

                    if (ctl.Pscash > 0)
                    {
                        UserCashTrustLog addPscash = new UserCashTrustLog();
                        addPscash.Amount = 0 - ctl.Pscash;
                        addPscash.CreateTime = DateTime.Now;
                        addPscash.UserId = cashTrustInfo.User.UniqueId;
                        addPscash.Message = "使用購物金結帳";
                        addPscash.TrustProvider = (int)TrustProvider.TaiShin;
                        addPscash.TrustId = ctl.TrustId;

                        uctlc.Add(addPscash);
                    }

                    if (ctl.CreditCard > 0)
                    {
                        UserCashTrustLog addScash = new UserCashTrustLog();
                        addScash.Amount = ctl.CreditCard;
                        addScash.CreateTime = DateTime.Now;
                        addScash.UserId = cashTrustInfo.User.UniqueId;
                        addScash.Message = "刷卡轉購物金";
                        addScash.TrustProvider = (int)TrustProvider.TaiShin;
                        addScash.TrustId = ctl.TrustId;

                        uctlc.Add(addScash);

                        UserCashTrustLog useScash = new UserCashTrustLog();
                        useScash.Amount = 0 - ctl.CreditCard;
                        useScash.CreateTime = DateTime.Now;
                        useScash.UserId = cashTrustInfo.User.UniqueId;
                        useScash.Message = "使用購物金轉運費";
                        useScash.TrustProvider = (int)TrustProvider.TaiShin;
                        useScash.TrustId = ctl.TrustId;

                        uctlc.Add(useScash);
                    }

                    #region 紀錄刷卡未開立發票金額
                    if (uninvoiced > 0)
                    {
                        ctl.UninvoicedAmount = (uninvoiced - (ctl.CreditCard + ctl.Tcash) > 0) ? (ctl.CreditCard + ctl.Tcash) : uninvoiced;
                        uninvoiced -= ctl.UninvoicedAmount;
                    }
                    #endregion

                    ctlc.Add(ctl);

                    CashTrustStatusLog ctsl = new CashTrustStatusLog();
                    ctsl.Amount = ctl.CreditCard + ctl.Scash + ctl.Pscash + ctl.Atm + ctl.FamilyIsp + ctl.SevenIsp;
                    ctsl.ModifyTime = ctl.ModifyTime;
                    ctsl.Status = ctl.Status;
                    ctsl.TrustId = ctl.TrustId;
                    ctsl.TrustProvider = ctl.TrustProvider;
                    ctsl.CreateId = cashTrustInfo.User.UserName;
                    ctslc.Add(ctsl);
                }
            }

            #endregion

            // 增加一個檢查
            if (totalAmount > 0)
            {
                logger.Error("Something Wrong!!! 份數與付款金額不符，OrderGuid = " + cashTrustInfo.OrderGuid.ToString());
            }

            #endregion

            #region 紀錄購物金未開立發票金額

            foreach (var ctl in ctlc)
            {
                if (uninvoiced > 0)
                {
                    int newScash = (uninvoiced - ctl.Scash > 0) ? ctl.Scash : uninvoiced;
                    ctl.UninvoicedAmount += newScash;
                    uninvoiced -= newScash;
                }
            }

            #endregion

            mp.CashTrustLogCollectionSet(ctlc);
            mp.CashTrustStatusLogCollectionSet(ctslc);
            mp.UserCashTrustLogCollectionSet(uctlc);

            #region 儲存payment_type_amount
            if (cashTrustInfo.Payments != null)
            {
                op.PaymentTypeAmountCollectionSet(pCol);
            }
            #endregion 儲存payment_type_amount

            return ctlc;
        }

        public static Guid? GetVerifiedStoreGuid(int couponId, OrderClassification orderClassification, string account)
        {
            var ctl = mp.CashTrustLogGetByCouponId(couponId, orderClassification);
            if (ctl.IsLoaded && ctl.BusinessHourGuid.HasValue)
            {
                var bh = pp.BusinessHourGet(ctl.BusinessHourGuid.Value);
                if (bh.IsLoaded)
                {
                    //通用券檔次才需判斷商家帳號之核銷分店身分 其餘類型檔次 則統一和購買選擇分店相同
                    if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore))
                    {
                        //抓取分店Guid規則
                        // 1.若賣家及分店權限同時存在 則抓取賣家權限
                        // 2.若有多個分店權限 則抓取其中一間分店權限 
                        // 3.若為內部帳號核銷 則抓取其中一筆可核銷權限

                        //抓取檔次可核銷權限
                        var verifyInfo = pp.PponStoreGetListByBusinessHourGuid(bh.Guid)
                            .Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Verify))
                            .ToDictionary(x => x.ResourceGuid, x => x.StoreGuid);

                        if (!verifyInfo.Any())
                        {
                            return null;
                        }

                        //檢查是否為內部帳號
                        var accountInfo = mp.VbsMembershipGetByAccountId(account);
                        if (accountInfo.AccountType != (int)VbsMembershipAccountType.VendorAccount)
                        {
                            return verifyInfo.Values.OrderBy(x => x).First();
                        }

                        var resourceGuids = new List<Guid>();
                        resourceGuids.AddRange(verifyInfo.Keys);
                        resourceGuids.AddRange(verifyInfo.Values);

                        //抓取帳號權限
                        var resourceAcls = mp.ResourceAclGetListByResourceGuidList(resourceGuids)
                                            .Where(x => x.AccountId.Equals(account)
                                                && ((x.ResourceType == (int)ResourceType.PponStore
                                                 || x.ResourceType == (int)ResourceType.Store)
                                                 || (x.ResourceType == (int)ResourceType.Seller
                                                  && x.ResourceGuid == bh.SellerGuid))
                                                && ((PermissionType)x.PermissionType).HasFlag(PermissionType.Read)
                                                && ((PermissionType)x.PermissionSetting).HasFlag(PermissionType.Read))
                                            .ToList();

                        var resourceAcl = resourceAcls.OrderBy(x => x.ResourceType).FirstOrDefault();

                        // 1.商家帳號抓取不到分店資訊
                        // 2.商家帳號對應多筆分店資訊 
                        //發信通知給技術 請企服確認商家帳號權限
                        if (resourceAcl == null || resourceAcls.Count(x => x.ResourceType == (int)resourceAcl.ResourceType) > 1)
                        {
                            SendCheckVendorAccountMail(account, bh.Guid, ctl.TrustId);
                            return null;
                        }

                        if (resourceAcl.ResourceType == (int)ResourceType.PponStore)
                        {
                            if (verifyInfo.ContainsKey(resourceAcl.ResourceGuid))
                            {
                                return verifyInfo[resourceAcl.ResourceGuid];
                            }

                            return null;
                        }

                        return resourceAcl.ResourceGuid;
                    }

                    return ctl.StoreGuid;
                }
            }

            return null;
        }

        private static void SendCheckVendorAccountMail(string account, Guid bid, Guid trustId)
        {
            try
            {
                var msg = new MailMessage { From = new MailAddress(config.AdminEmail) };
                msg.To.Add(config.ItPAD);

                msg.Subject = "通用券檔次商家帳號核銷權限異常通知";
                msg.Body = string.Format(@"Web主機名稱 ： {3}<br/>商家帳號 : {0} <br/>核銷檔次Bid : {1} 憑證trust_id : {2} 時出現無法對應核銷分店異常狀況，請聯繫企服確認商家帳號權限設定是否有異常，<br/>
並於確認完成後補上cash_trust_log verified_store_guid 正確核銷分店Guid資訊，以避免影響該檔次後續請款事宜。"
                                , account, bid, trustId, Environment.MachineName);
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Async);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        #endregion

        #region Auxiliary functions
        public static string BindFaces(OrderReviewType score, string imageBaseUrl)
        {
            string faceUrl = imageBaseUrl.TrimEnd('/') + "/";
            switch (score)
            {
                case OrderReviewType.Bad:
                    faceUrl += "face3.jpg";
                    break;
                case OrderReviewType.Normal:
                    faceUrl += "face2.jpg";
                    break;
                case OrderReviewType.Good:
                    faceUrl += "face1.jpg";
                    break;
                default:
                    break;
            }
            return faceUrl;
        }

        /// <summary>
        /// Temporarily for Delicacies department.
        /// </summary>
        /// <param name="score"></param>
        /// <param name="imageBaseUrl"></param>
        /// <returns></returns>
        public static string DelicaciesBindFaces(OrderReviewType score, string imageBaseUrl)
        {
            string faceUrl = imageBaseUrl.TrimEnd('/') + "/";
            switch (score)
            {
                case OrderReviewType.Bad:
                    faceUrl += "D9_Face_2_s.jpg";
                    break;
                case OrderReviewType.Normal:
                    faceUrl += "D9_Face_1_s.jpg";
                    break;
                case OrderReviewType.Good:
                    faceUrl += "D9_Face_3_s.jpg";
                    break;
                default:
                    break;
            }
            return faceUrl;
        }

        public static string MakeRegularTicketId(string sessionId, Guid bhGuid)
        {
            return string.Format("{0}_{1}", sessionId, bhGuid.ToString());
        }

        public static string MakeRegularTicketId()
        {
            return Guid.NewGuid().ToString();
        }

        private static string GetNewOrderId(string prefix, DateTime dateTime)
        {
            string oid = prefix + string.Format("-{0:yyMMdd}-", dateTime);
            int id = sysp.SerialNumberGetNext(oid);
            oid += id.ToString().PadLeft(3, '0');
            return oid;
        }

        public static int GetOrderItemQuantity(Guid orderGuid)
        {
            int q = 0;

            foreach (var entry in op.OrderDetailGetListAggregate(orderGuid, OrderDetailTypes.Regular))
            {
                q += entry.ItemQuantity;
            }

            return q;
        }
        #endregion

        #region DealTimeSlot
        public static bool SetSeqToDealTimeSlotData(Guid guid, int cityId, DateTime effectiveStart, int newSeq)
        {
            DealTimeSlot slot = pp.DealTimeSlotGet(guid, cityId, effectiveStart);
            if (slot.IsLoaded)
            {
                slot.Sequence = newSeq;
                pp.DealTimeSlotUpdate(slot);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 大量更新 DealTimeSlot Seq
        /// </summary>
        /// <param name="sortDealCol"></param>
        /// <returns></returns>
        public static void SaveSeqToDealTimeSlotData(SortDealTimeSlotCollection sortDealCol)
        {
            pp.SortDealTimeSlotBulkInsert(sortDealCol);
            pp.UpdateDealTimeSlotFromSortDealTimeSlotCol();
            pp.TruncateSortDealTimeSlot();
        }

        public static bool SetStatusToDealTimeSlotData(Guid guid, int cityId, DateTime effectiveStart, DealTimeSlotStatus newStatus, bool isSetFuture)
        {
            if (isSetFuture)
            {
                pp.DealTimeSlotUpdateStatusWithTransaction(guid, cityId, effectiveStart, (int)newStatus);
                return true;
            }
            else
            {
                DealTimeSlot slot = pp.DealTimeSlotGet(guid, cityId, effectiveStart);
                if (slot != null)
                {
                    slot.Status = (int)newStatus;
                    pp.DealTimeSlotUpdate(slot);
                    return true;
                }
            }
            return false;
        }

        public static bool SetInTurnToDealTimeSlotData(Guid guid, int cityId, DateTime effectiveStart, bool isInTurn)
        {
            DealTimeSlot slot = pp.DealTimeSlotGet(guid, cityId, effectiveStart);
            if (slot != null)
            {
                slot.IsInTurn = isInTurn;
                pp.DealTimeSlotUpdate(slot);
                return true;
            }
            return false;
        }

        #endregion DealTimeSlot

        #region Transaction

        public static PaymentTransaction MakeTransaction(string transId, PaymentType thePaymentType, int theCharge,
            DepartmentTypes theDepartment, string userName, Guid orderGuid)
        {
            int status = 0;
            switch (thePaymentType)
            {
                case PaymentType.Creditcard:
                    return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                        DateTime.Now, userName, null, Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment));
                case PaymentType.LinePay:
                    return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                        DateTime.Now, userName, "LinePay", Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment));
                case PaymentType.TaishinPay:
                    return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                        DateTime.Now, userName, "TaishinPay", Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment));
                case PaymentType.ByPass:
                    status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Successful);
                    return PaymentFacade.NewTransaction(transId, orderGuid, PaymentType.Creditcard, theCharge, null, PayTransType.Authorization,
                        DateTime.Now, userName, "測試", status, PayTransResponseType.OK, OrderClassification.LkSite, null, PaymentAPIProvider.Mock);
                case PaymentType.BonusPoint:
                    status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Successful);
                    return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                        DateTime.Now, userName, "以紅利折抵", status, PayTransResponseType.OK);
                case PaymentType.SCash:
                    status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Successful);
                    return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                        DateTime.Now, userName, "17購物金折抵", status, PayTransResponseType.OK);
                case PaymentType.Pscash:
                    status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Successful);
                    return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                        DateTime.Now, userName, "由Payeasy兌換的的購物金折抵", status, PayTransResponseType.OK);
                case PaymentType.DiscountCode:
                    status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Successful);
                    return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                        DateTime.Now, userName, "DiscountCode折抵", status, PayTransResponseType.OK);
                case PaymentType.ATM:
                    status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Requested);
                    return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                        DateTime.Now, userName, "ATM轉帳", status, PayTransResponseType.OK);
                case PaymentType.FamilyIsp:
                    status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Requested);
                    return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                        DateTime.Now, userName, "全家超商付款", status, PayTransResponseType.OK);
                case PaymentType.SevenIsp:
                    status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Requested);
                    return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                        DateTime.Now, userName, "7-11超商付款", status, PayTransResponseType.OK);
                default:
                    break;
            }
            return null;
        }

        public static PaymentTransaction MakeTransaction(string transId, PaymentType thePaymentType, int theCharge,
            DepartmentTypes theDepartment, string userName, Guid orderGuid, string pezId)
        {
            if (thePaymentType == PaymentType.PCash)
            {
                #region PostToPEZ

                string authCode = string.Empty;
                string pezResult = PCashWorker.CheckOut(pezId, transId, theCharge, theDepartment, out authCode);

                #endregion PostToPEZ

                PayTransResponseType responseType = PayTransResponseType.OK;
                PayTransPhase transPhase = PayTransPhase.Created;

                if (pezResult == "0000")
                {
                    transPhase = PayTransPhase.Successful;
                }
                else
                {
                    transPhase = PayTransPhase.Failed;
                    responseType = PayTransResponseType.GenericError;
                }
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), transPhase);
                return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, authCode, PayTransType.Authorization,
                    DateTime.Now, userName, string.Empty, status, responseType);
            }
            else if (thePaymentType == PaymentType.Creditcard)
            {
                return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                    DateTime.Now, userName, null, Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment));
            }
            else if (thePaymentType == PaymentType.ByPass)
            {
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Successful);
                return PaymentFacade.NewTransaction(transId, orderGuid, PaymentType.Creditcard, theCharge, null, PayTransType.Authorization,
                    DateTime.Now, userName, "測試", status, PayTransResponseType.OK,
                    OrderClassification.LkSite, null, PaymentAPIProvider.Mock);
            }
            else if (thePaymentType == PaymentType.BonusPoint)
            {
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Successful);
                return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                    DateTime.Now, userName, "以紅利折抵", status, PayTransResponseType.OK);
            }
            else if (thePaymentType == PaymentType.SCash)
            {
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Successful);
                return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                    DateTime.Now, userName, "17購物金折抵", status, PayTransResponseType.OK);
            }
            else if (thePaymentType == PaymentType.Pscash)
            {
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Successful);
                return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                    DateTime.Now, userName, "由Payeasy兌換的的購物金折抵", status, PayTransResponseType.OK);
            }
            else if (thePaymentType == PaymentType.DiscountCode)
            {
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Successful);
                return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                    DateTime.Now, userName, "DiscountCode折抵", status, PayTransResponseType.OK);
            }
            else if (thePaymentType == PaymentType.ATM)
            {
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Requested);
                return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                    DateTime.Now, userName, "ATM轉帳", status, PayTransResponseType.OK);
            }
            else if (thePaymentType == PaymentType.FamilyIsp)
            {
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Requested);
                return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                    DateTime.Now, userName, "全家超商付款", status, PayTransResponseType.OK);
            }
            else if (thePaymentType == PaymentType.SevenIsp)
            {
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Requested);
                return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                    DateTime.Now, userName, "7-11超商付款", status, PayTransResponseType.OK);
            }
            else if (thePaymentType == PaymentType.LCash)
            {
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, theDepartment), PayTransPhase.Successful);
                return PaymentFacade.NewTransaction(transId, orderGuid, thePaymentType, theCharge, null, PayTransType.Authorization,
                    DateTime.Now, userName, "外部付款折抵", status, PayTransResponseType.OK);
            }

            return null;
        }

        #endregion

        #region BuySCash

        /// <summary>
        /// 信用卡/Line Pay轉購物金
        /// 
        /// 轉購物金會將原本信用卡 payment_transaction 的 order_guid 換成指定到 CashpointOrder.id
        /// 
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="ptOri"></param>
        /// <param name="ptScash"></param>
        /// <param name="o"></param>
        /// <param name="theDeal"></param>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <param name="ptOriCharging">授權後已進行請款，會有值，一併調整訂單編號指向購物金訂單</param>
        public static void ExchangeSCash(string transactionId, PaymentTransaction ptOri, PaymentTransaction ptScash, Order o,
            IViewPponDeal theDeal, int userId, string userName, PaymentTransaction ptOriCharging = null)
        {
            Guid sOrderGuid = OrderFacade.MakeSCachOrder(ptOri.Amount, ptOri.Amount,
                "17Life購物金購買(系統)",
                CashPointStatus.Approve, o.UserId, theDeal.ItemName, CashPointListType.Income,
                userName, o.Guid);

            bool invoiced = (DateTime.Now < config.NewInvoiceDate);
            //Scash will not be invoiced initially after the NewInvoiceDate.
            int depositId = OrderFacade.NewScashDeposit(sOrderGuid, ptOri.Amount, userId,
                o.Guid,
                "購物金購買(系統):" + theDeal.SellerName, userName, invoiced,
                OrderClassification.CashPointOrder);
            OrderFacade.NewScashWithdrawal(depositId, ptOri.Amount, o.Guid,
                "購物金折抵(系統):" + theDeal.SellerName, userName, OrderClassification.CashPointOrder);

            ptOri.OrderGuid = sOrderGuid;
            ptOri.Message += "|" + o.Guid.ToString();
            op.PaymentTransactionSet(ptOri);
            if (ptOriCharging != null)
            {
                ptOriCharging.OrderGuid = sOrderGuid;
                ptOriCharging.Message += "|" + o.Guid.ToString();
                op.PaymentTransactionSet(ptOriCharging);
            }

            if (ptScash == null)
            {
                // 沒有 ptScash，意味著沒有使用購物金
                // 此時需要新增一筆 PaymentType為 SCash 的 PaymentTransaction，做為信用卡轉購物金
                MakeTransaction(transactionId, PaymentType.SCash,
                    int.Parse(ptOri.Amount.ToString("F0")),
                    (DepartmentTypes)theDeal.Department, userName, o.Guid);
            }
            else
            {
                // 有 ptScash ，意味著有使用購物金
                // 此時需要更新 ptSCash 的金額，合併2筆筆金額，即使用的購物金、信用卡轉購物金
                PaymentFacade.UpdateTransaction(ptScash.TransId, ptScash.OrderGuid,
                    PaymentType.SCash, ptScash.Amount + ptOri.Amount, ptScash.AuthCode,
                    PayTransType.Authorization, DateTime.Now, ptScash.Message, ptScash.Status,
                    ptScash.Result.Value);
            }
        }

        /// <summary>
        /// 17Life購物金購買
        /// </summary>
        public static Guid MakeSCachOrder(decimal total, decimal subtotal, string message, CashPointStatus status, int userId,
            string itemname, CashPointListType type, string createuser, Guid orderguid)
        {
            #region CashPoint
            CashpointOrder sorder = new CashpointOrder();
            sorder.Id = Guid.NewGuid();
            sorder.Total = total;
            sorder.Subtotal = subtotal;
            sorder.Status = (int)status;
            sorder.Message = message;
            if (createuser != null && createuser.Length > 50)
            {
                createuser = createuser.Substring(0, 50);
            }
            sorder.CreateId = createuser;
            sorder.CreateTime = DateTime.Now;

            op.CashPointOrderSet(sorder);
            #endregion

            #region CashPointOrderDetail
            CashpointOrderDetail sorderdetail = new CashpointOrderDetail();
            sorderdetail.Amount = total;
            sorderdetail.CashpointOrderid = sorder.Id;
            sorderdetail.ItemName = itemname;

            op.CashPointOrderDetailSet(sorderdetail);
            #endregion

            return sorder.Id;
        }

        /// <summary>
        /// 購物金折抵、轉刷退，使用掉購物金
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="userId"></param>
        /// <param name="orderGuid"></param>
        /// <param name="message"></param>
        /// <param name="createId"></param>
        /// <param name="orderClassification"></param>
        /// <returns></returns>
        public static int ScashUsed(decimal amount, int userId, Guid orderGuid, string message, string createId,
            OrderClassification orderClassification)
        {
            ViewScashTransactionCollection usefulDeposits =
                op.ViewScashTransactionGetListByNotBalanceOrderByInvoicedFirst(userId);
            decimal withdrawalAmount = amount;
            int uninvoiced = 0;
            foreach (var deposit in usefulDeposits)
            {
                if (withdrawalAmount > 0)
                {
                    decimal thisWithdrawal = (withdrawalAmount <= deposit.Balance.Value) ? withdrawalAmount : deposit.Balance.Value;
                    NewScashWithdrawal(deposit.DepositId, thisWithdrawal, orderGuid, message, createId, OrderClassification.CashPointOrder);
                    if (!deposit.Invoiced)
                    {
                        uninvoiced = uninvoiced + (int)thisWithdrawal;
                    }

                    withdrawalAmount = withdrawalAmount - thisWithdrawal;
                }
                else
                {
                    return uninvoiced;
                }
            }
            return uninvoiced;
        }


        public static void PortionGeneralScash(decimal amount, int userId, out decimal scash, out decimal pscash)
        {
            decimal userScash;
            decimal userPscash;
            OrderFacade.GetSCashSum2(userId, out userScash, out userPscash);

            if (userPscash > amount)
            {
                pscash = amount;
                scash = 0;
            }
            else if (userPscash > 0)
            {
                pscash = userPscash;
                scash = amount - userPscash;
            }
            else
            {
                pscash = 0;
                scash = amount;
            }
        }

        /// <summary>
        /// 以購物金退貨
        /// </summary>
        public static bool ScashReturned(decimal amount, int userId, Guid orderGuid, string message, string createId, OrderClassification orderClassification, int uninvoiced, bool invoiced = false)
        {
            if (uninvoiced > 0)
            {
                Guid s1 = MakeSCachOrder(uninvoiced, uninvoiced, message, CashPointStatus.Approve, userId,
                    message, CashPointListType.Income, createId, orderGuid);
                NewScashDeposit(s1, uninvoiced, userId, orderGuid, message, createId, false, orderClassification);
                if (amount - uninvoiced > 0)
                {
                    Guid s2 = MakeSCachOrder(amount - uninvoiced, amount - uninvoiced, message, CashPointStatus.Approve, userId, message,
                        CashPointListType.Income, createId, orderGuid);
                    NewScashDeposit(s2, amount - uninvoiced, userId, orderGuid, message, createId, invoiced, orderClassification);
                }
            }
            else
            {
                Guid s3 = MakeSCachOrder(amount, amount, message, CashPointStatus.Approve, userId, message,
                    CashPointListType.Income, createId, orderGuid);
                NewScashDeposit(s3, amount, userId, orderGuid, message, createId, invoiced, orderClassification);
            }
            return true;
        }

        public static int NewScashDeposit(Guid cashpointOrderid, decimal amount, int userId, Guid orderGuid, string message,
            string createId, bool invoiced, OrderClassification orderClassification)
        {
            ScashDeposit deposit = new ScashDeposit();
            deposit.CashpointOrderid = cashpointOrderid;
            deposit.Amount = amount;
            deposit.OrderGuid = orderGuid;
            deposit.Message = message;
            deposit.CreateId = createId;
            deposit.CreateTime = DateTime.Now;
            deposit.Invoiced = invoiced;
            deposit.OrderClassification = (int)orderClassification;
            deposit.UserId = userId;

            op.ScashDepositSet(deposit);
            return deposit.Id;
        }

        public static bool NewScashWithdrawal(int depositId, decimal amount, Guid orderGuid, string message, string createId, OrderClassification orderClassification)
        {
            ScashWithdrawal withdrawal = new ScashWithdrawal();
            withdrawal.DepositId = depositId;
            withdrawal.Amount = amount;
            withdrawal.OrderGuid = orderGuid;
            withdrawal.Message = message;
            withdrawal.CreateId = createId;
            withdrawal.CreateTime = DateTime.Now;
            withdrawal.OrderClassification = (int)orderClassification;

            op.ScashWithdrawalSet(withdrawal);
            return true;
        }

        /// <summary>
        /// 前台消費者付款使用:計算消費者購物金餘額
        /// 依refundLock決定是否須扣除購物金轉退現金退貨單且退款處理中之保留購物金金額
        /// </summary>
        /// <param name="username"></param>
        /// <param name="refundLock"></param>
        public static decimal GetSCashSum(string username, bool refundLock = true)
        {
            var refundLockScash = 0;
            var remainScash = op.ScachTransactionSum(username);
            if (refundLock)
            {
                //ToDo:抓取該消費者購物金轉退現金之保留金額
            }

            return remainScash > refundLockScash ? remainScash - refundLockScash : 0;
        }

        /// <summary>
        /// 系統退款使用:計算消費者購物金餘額
        /// </summary>
        /// <param name="userId"></param>
        public static decimal GetSCashSum(int userId)
        {
            return op.ScachTransactionSum(userId);
        }
        /// <summary>
        /// 紅利金查詢，可查從pcash儲值的紅利金
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="e7Scash"></param>
        /// <param name="pezScash"></param>
        /// <returns></returns>
        public static decimal GetSCashSum2(int userId, out decimal e7scash, out decimal pscash)
        {
            e7scash = GetSCashSum(userId);
            pscash = GetPscashBalanceSum(userId);
            return e7scash + pscash;
        }
        /// <summary>
        /// 取得使用者目前的Pscash可用金額
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static decimal GetPscashBalanceSum(int userId)
        {
            return op.PscashDepositBalanceGetListForWithdrawal(userId).Sum(t => t.Balance);
        }

        public static decimal GetSCashSum2ByOrder(Guid orderGuid, out decimal scash, out decimal pscash)
        {
            return op.ScashGetByOrder(orderGuid, out scash, out pscash);
        }

        /// <summary>
        /// 轉換Pcash到17Life的購物金
        /// </summary>
        /// <param name="pcashOrderNo">Payeasy的訂單序號，跟Payeasy對帳與退回時使用</param>
        /// <param name="amount">轉換金額</param>
        /// <param name="message"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool ExchangePcashToPscash(Guid pxOrderGuid, string pezAuthCode, int amount, int userId)
        {
            string logMsg = Newtonsoft.Json.JsonConvert.SerializeObject(
                new { pezAuthCode, amount, userId }, Newtonsoft.Json.Formatting.Indented);
            logger.Info(logMsg);
            string message = "轉入";

            try
            {
                Member mem = MemberFacade.GetMember(userId);
                if (mem.IsLoaded == false)
                {
                    return false;
                }
                var pxOrder = op.PscashOrderGetByGuid(pxOrderGuid);
                if (pxOrder.IsLoaded == false)
                {
                    return false;
                }
                if (pxOrder.IsCompleted)
                {
                    throw new Exception("這筆訂單已存在");
                }
                using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                {
                    //pcash轉過來的紅利金，不需要再開發票
                    //bool invoiced = true;
                    //int depositId = OrderFacade.NewScashDeposit(sOrderGuid, amount, userId, Guid.Empty, message,
                    //    mem.UserName, invoiced, OrderClassification.PcashXchOrder, ScashSource.Payeasy);
                    int depositId = OrderFacade.NewPscashDeposit(pxOrder.Id, amount, userId, null, message,
                        mem.UserName);

                    pxOrder.Amount = amount;
                    pxOrder.PezAuthCode = pezAuthCode;
                    pxOrder.IsCompleted = true;
                    pxOrder.CompletedTime = DateTime.Now;
                    op.PcashXchOrderSet(pxOrder);


                    UserCashTrustLog ctUser = new UserCashTrustLog();
                    ctUser.UserId = userId;
                    ctUser.Amount = amount;
                    ctUser.BankStatus = (int)TrustBankStatus.Initial;
                    ctUser.CreateTime = DateTime.Now;
                    ctUser.Message = "轉入Payeasy兌換的購物金";
                    ctUser.TrustProvider = (int)TrustProvider.TaiShin;
                    ctUser.TrustId = default(Guid);
                    ctUser.Type = (int)UserTrustType.Buy;
                    ctUser.MarkNew();
                    mp.UserCashTrustLogSet(ctUser);


                    tx.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.WarnFormat("ExchangePayeasyCashToScash 發生錯誤, {0}, {1}", logMsg, ex);
                throw;
            }
        }

        public static PcashXchOrder MakeNewPscashOrder(Guid guid, int userId)
        {
            PcashXchOrder pxOrder = new PcashXchOrder
            {
                Guid = guid,
                Amount = 0,
                UserId = userId,
                PezAuthCode = null,
                CreateTime = DateTime.Now,
                IsCompleted = false,
                CompletedTime = null,
                IsRefund = false,
                RefundAmount = null,
                RefundTime = null,
                RefundCreateId = null
            };
            op.PcashXchOrderSet(pxOrder);
            return pxOrder;
        }

        public static int NewPscashDeposit(int pscashOrderId, decimal amount, int userId, Guid? orderGuid, string message,
            string createId)
        {
            PscashDeposit deposit = new PscashDeposit();
            deposit.PcashXchOrderId = pscashOrderId;
            deposit.Amount = amount;
            deposit.OrderGuid = orderGuid;
            deposit.Message = message;
            deposit.CreateId = createId;
            deposit.CreateTime = DateTime.Now;
            deposit.UserId = userId;

            op.PscashDepositSet(deposit);
            return deposit.Id;
        }

        public static bool NewPscashWithdrawal(int depositId, decimal amount, Guid orderGuid, string message, string createId)
        {
            if (amount == 0)
            {
                return false;
            }
            PscashWithdrawal withdrawal = new PscashWithdrawal();
            withdrawal.DepositId = depositId;
            withdrawal.Amount = amount;
            withdrawal.OrderGuid = (orderGuid == Guid.Empty ? (Guid?)null : orderGuid);
            withdrawal.Message = message;
            withdrawal.CreateId = createId;
            withdrawal.CreateTime = DateTime.Now;

            op.PscashWithdrawalSet(withdrawal);
            return true;
        }
        /// <summary>
        /// 購物金折抵、轉刷退，使用掉購物金
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="userId"></param>
        /// <param name="orderGuid"></param>
        /// <param name="message"></param>
        /// <param name="createId"></param>
        /// <param name="orderClassification"></param>
        /// <returns></returns>
        public static void PscashUsed(decimal amount, int userId, Guid orderGuid, string message, string createId)
        {
            var usefulDeposits = op.PscashDepositBalanceGetListForWithdrawal(userId);
            decimal withdrawalAmount = amount;
            foreach (PscashDepositBalance deposit in usefulDeposits)
            {
                if (withdrawalAmount > 0)
                {
                    decimal thisWithdrawal = (withdrawalAmount <= deposit.Balance) ? withdrawalAmount : deposit.Balance;
                    NewPscashWithdrawal(deposit.DepositId, thisWithdrawal, orderGuid, message, createId);
                    withdrawalAmount = withdrawalAmount - thisWithdrawal;
                }
                else
                {
                    break;
                }
            }
        }
        /// <summary>
        /// 以Pscash退貨
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="userId"></param>
        /// <param name="orderGuid"></param>
        /// <param name="message"></param>
        /// <param name="createId"></param>
        /// <param name="orderClassification"></param>
        /// <param name="uninvoiced"></param>
        /// <param name="invoiced"></param>
        /// <returns></returns>
        public static bool PscashReturned(decimal amount, int userId, Guid orderGuid, string message, string createId)
        {
            decimal depositAmount = amount;
            var usedWithdrawals = op.PscashWithdrawalViewGetByOrder(orderGuid);

            foreach (var withdrawal in usedWithdrawals)
            {
                if (depositAmount > 0)
                {
                    decimal thisDeposit = depositAmount > withdrawal.Amount ? withdrawal.Amount : depositAmount;
                    NewPscashDeposit(withdrawal.PcashXchOrderId, thisDeposit, withdrawal.UserId, withdrawal.OrderGuid, message, createId);
                    depositAmount = depositAmount - thisDeposit;
                }
                else
                {
                    break;
                }
            }
            return true;
        }


        /// <summary>
        /// 將Pcash購買的Scash，從Scash退回給Pcash
        /// 退的話就是該會員，還沒用掉的餘額全退掉
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="createId">操作者</param>
        /// <returns></returns>
        public static void ReturnPscashToPayeasy(int userId, string createId)
        {
            List<PscashDepositBalance> usefulDeposits = op.PscashDepositBalanceGetListForWithdrawal(userId);
            foreach (PscashDepositBalance deposit in usefulDeposits)
            {
                using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                {
                    var pxo = op.PcashXchOrderGet(deposit.PezAuthCode);
                    if (pxo == null || pxo.IsLoaded == false)
                    {
                        continue;
                    }
                    if (pxo.IsRefund && (pxo.RefundAmount + deposit.Balance) > (pxo.Amount))
                    {
                        CommonFacade.AddAudit(pxo.Guid, AuditType.PayeasyPcashAPI,
                            string.Format("退P購物金給Pasyeasy失敗，因為超額, 會員:{0}, 最多可退: {1}, 卻要退回: {2}",
                                userId, pxo.Amount - pxo.RefundAmount, deposit.Balance), createId, false);
                        continue;
                    }
                    int actualCancelPoint = 0;
                    bool result = PayeasyFacade.RefundToPasyeasyAPI(deposit.Guid, deposit.PezAuthCode, deposit.Balance, out actualCancelPoint);
                    CommonFacade.AddAudit(pxo.Guid, AuditType.PayeasyPcashAPI,
                        string.Format("退P購物金給Pasyeasy{0}, 會員:{1}, 預計: {2}, 實際退回: {3}",
                            result ? "成功" : "失敗",
                            userId, deposit.Balance, actualCancelPoint), createId, false);
                    if (result)
                    {
                        OrderFacade.PscashUsed(actualCancelPoint, userId, Guid.Empty, "轉出", createId);
                        pxo.RefundAmount = pxo.RefundAmount.GetValueOrDefault() + actualCancelPoint;
                        pxo.RefundCreateId = createId;
                        pxo.RefundTime = DateTime.Now;
                        pxo.IsRefund = true;
                        op.PcashXchOrderSet(pxo);

                        UserCashTrustLog ctUser = new UserCashTrustLog();
                        ctUser.UserId = userId;
                        ctUser.Amount = -actualCancelPoint;
                        ctUser.BankStatus = (int)TrustBankStatus.Initial;
                        ctUser.CreateTime = DateTime.Now;
                        ctUser.Message = "轉出Payeasy兌換的購物金";
                        ctUser.TrustProvider = (int)TrustProvider.TaiShin;
                        ctUser.TrustId = default(Guid);
                        ctUser.Type = (int)UserTrustType.Refund;
                        ctUser.Status = (int)TrustStatus.Initial;
                        ctUser.MarkNew();
                        mp.UserCashTrustLogSet(ctUser);

                        tx.Complete();
                    }
                }
            }
        }

        /// <summary>
        /// 取得使用者自Payeasy兌換的購物金使用狀況
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static List<PezScashTransaction> GetPezScashTransactions(int userId)
        {
            /*
	                pxo.create_time as create_time,  --0
	                pxo.pez_auth_code, --1
	                pd.message, --2
	                pd.amount, --3
	                pd.create_id, --4
	                pw.message, --5
	                pw.amount, --6
	                pw.create_time as withdrawal_time , --7
	                pw.create_id as withdrawal_create_id --8


select * from (

select 
	'deposit' as [action] --0,
	pd.id as deposit_id  --1,	
	null as withdrawal_id  --2,	
    pxo.pez_auth_code as pez_auth_code -- 3,
	pd.message as message --4, 
	pd.amount as amount --5, 
	pd.create_id as create_id --6,
	pd.create_time as create_time --7
from pscash_deposit pd 
inner join pcash_xch_order pxo on pxo.id = pd.pcash_xch_order_id
where pd.user_id = @userId

union

select 
	'withdrawal' as [action], --0
	pd.id as deposit_id, --1
	pw.id as withdrawal_id, --2
    pxo.pez_auth_code, --3
	pw.message as message, --4 
	pw.amount * -1 as amount, --5
	pw.create_id as create_id, --6
	pw.create_time as create_time --7
from pscash_withdrawal pw
inner join pscash_deposit pd on pd.id = pw.deposit_id
inner join pcash_xch_order pxo on pxo.id = pd.pcash_xch_order_id
where pd.user_id = @userId


) as t
order by t.create_time
             */
            List<PezScashTransaction> result = new List<PezScashTransaction>();
            DataTable dt = op.PezScashRecordsDataTableGet(userId);
            HashSet<string> pezAuthCodes = new HashSet<string>();
            decimal balance = 0;
            foreach (DataRow dr in dt.Rows)
            {
                var tran = new PezScashTransaction
                {
                    Operation = (string)dr[0],
                    DepositId = (int)dr[1],
                    WithdrawalId = dr[2] == DBNull.Value ? (int?)null : (int)dr[2],
                    PezAuthCode = (string)dr[3],
                    Title = (string)dr[4],
                    Amount = (decimal)dr[5],
                    Creator = (string)dr[6],
                    TheTime = (DateTime)dr[7]
                };
                result.Add(tran);
                balance += tran.Amount;
                tran.Balance = balance;
            }
            return result;
        }

        #endregion

        #region for special deals having coupon code already
        public static Peztemp GetPezTemp(Guid bid)
        {
            return pp.PeztempGet(bid);
        }
        public static Peztemp GetPezTemp(Guid bid, string userid)
        {
            return pp.PeztempGet(bid, userid);
        }

        public static Peztemp GetPezTemp(Guid bid, string userName, bool isPezMember)
        {
            int retries = 5;
            Peztemp pez = null;
            PeztempCoupon pCoupon = new PeztempCoupon
            {
                UserName = userName,
                BusinessHourGuid = bid,
                CreateTime = DateTime.Now
            };
            string userid = isPezMember ? MemberFacade.CheckIsPEZUserID(userName) : string.Empty;

            int tries = 0;

            do
            {
                pez = isPezMember ? pp.PeztempGet(bid, userid) : pp.PeztempGet(bid);
                pCoupon.PezCode = pez.PezCode;
                if (!pez.IsLoaded)
                {
                    return null;
                }

                if (SetPeztempCoupon(pCoupon))
                {
                    return pez;
                }

                tries++;
                System.Threading.Thread.Sleep(500);
            } while (tries < retries);

            return null;
        }

        public static void SetPezTemp(Peztemp pez)
        {
            pp.PeztempSet(pez);
        }

        public static bool SetPeztempCoupon(PeztempCoupon peztempCoupon)
        {
            try
            {
                pp.PeztempCouponSet(peztempCoupon);
                return true;
            }
            catch (Exception ex)
            {
                logger.InfoFormat("SetPeztempCoupon Error. Message:{0}, pezcode:{1}, bid:{2}, {3}", ex.Message, peztempCoupon.PezCode, peztempCoupon.BusinessHourGuid.ToString(), ex.StackTrace);
                return false;
            }
        }

        public static void DeletePeztempCoupon(string pezCode, Guid bid)
        {
            pp.PeztempCouponDelete(pezCode, bid);
        }

        #endregion for special deals having coupon code already

        #region ATM

        /// <summary>
        /// ATM訂單成立通知信
        /// </summary>
        /// <param name="username"></param>
        /// <param name="memMail"></param>
        /// <param name="dealName"></param>
        /// <param name="orderAccount"></param>
        /// <param name="orderTotal"></param>
        /// <param name="bid"></param>
        public static void SendATMOrderNotifyMail(string username, string memMail, string dealName, string orderAccount,
            string orderTotal, IViewPponDeal deal, Order o, int qty)
        {
            MailMessage msg = new MailMessage();
            ATMOrderNotifyMail template = TemplateFactory.Instance().GetTemplate<ATMOrderNotifyMail>();
            template.OrderAccount = orderAccount;
            template.OrderTotal = orderTotal;
            template.UserName = username;
            template.SiteUrl = config.SiteUrl;
            template.SiteServiceUrl = config.SiteServiceUrl;
            string mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(deal.BusinessHourGuid).ToString();
            string orderLink = string.Format("{0}?oid={1}",
                    Helper.CombineUrl(config.SiteUrl, "User/CouponDetail.aspx"), o.Guid);
            template.OrderInfos.Add(new ATMOrderNotifyMail.ATMOrderInfo
            {
                DealName = dealName,
                OrderId = o.OrderId,
                Price = deal.ItemPrice.ToString("0"),
                Qty = qty.ToString(),
                OrderLink = orderLink,
                DealLink = Helper.CombineUrl(config.SiteUrl, "deal", mainBid)
            });
            template.PromoDeals = GetPromoDealsForNoticeMail(deal.BusinessHourGuid);
            template.ServerConfig = config;
            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(memMail);
            msg.Subject = "ATM 付款資料通知(" + dealName + ")";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate, MailTemplateType.MemberPponATMPaymentSuccessful);
        }

        public static CtAtmRefund CtAtmRefundGetBySi(int si)
        {
            return op.CtAtmRefundGetList(CtAtmRefund.Columns.Si, CtAtmRefund.Columns.Si + " = " + si.ToString()).FirstOrDefault();
        }

        public static CtAtmRefund GetCtAtmRefundByOid(Guid order_guid)
        {
            return op.CtAtmRefundGetLatest(order_guid);
        }
        #endregion

        #region viewpponcoupon
        /// <summary>
        /// [coupon]:StoreSequence:序號
        /// [ppon_store]:CouponSequenceCount:分店已產出coupon數量
        /// </summary>
        /// <returns></returns>
        public static int? GenerateStoreCouponSequence(string businessHourGuId, string storeGuid)
        {
            if (storeGuid != null)
            {
                //取此分店至今產出Coupon數，(Coupon_sequence_count)
                PponStore getPponStore = pp.PponStoreGet(Guid.Parse(businessHourGuId), Guid.Parse(storeGuid));
                //[PponStore]把取出CouponSequenceCount值+1存回ppon_store
                int couponsequenceCount = getPponStore.CouponSequenceCount + 1;
                getPponStore.CouponSequenceCount = couponsequenceCount;
                //欄位值存回ppon_store
                pp.PponStoreSet(getPponStore);

                return getPponStore.CouponSequenceCount;
            }

            var couponCount = pp.ViewPponCashTrustLogGetList(Guid.Parse(businessHourGuId))
                .Count(x => x.CouponId.HasValue);

            return couponCount > 0 ? couponCount + 1 : (int?)null;
        }

        /// <summary>
        /// 匯出賣家CouponList清冊Excel
        /// </summary>
        /// <param name="Bid">Ppon: BusinessHourGuid; HiDeal: ProductGuid</param>
        /// <param name="CouponUsage">Ppon: CouponUsage; HiDeal: ProductName</param>
        /// <param name="Introduction">CouponEventContent.Introduction</param>
        public static void ExportCouponList(Guid Bid, string CouponUsage, string Introduction, DateTime OrderTimeS, DateTime OrderTimeE, DateTime UseTimeS, DateTime UseTimeE,
            DataTable sdt, int UniqueId, string sellerName, string storeName, string allSaleCounts)
        {
            if (sdt.Rows.Count > 0)
            {
                #region 宣告

                DataTable dt = new DataTable();
                Workbook workbook = new HSSFWorkbook();
                Sheet sheet = workbook.CreateSheet("活動清冊");
                Cell c; Row BRow;

                int maxCouponUsageLength = 27;      //好康券名稱的最大長度
                int headerRowCounts = 0;    //首頁的行數
                int rowIndex = 0;   //目前到第幾行
                int rowPerPage = 22;  //內容頁每頁預定幾行
                storeName = string.IsNullOrEmpty(storeName) ? "所有分店" : storeName;  //店名
                NPOI.SS.Util.CellRangeAddress reg;  //NPOI 用於合併儲存格的區域宣告
                //格式:結檔日期【店名《分店名》-好康券短標】憑證清冊.xls
                //特別說明: 使用 FireFox下載檔案時, FireFox 預設會用空白字元截斷檔名. 為解決這問題, 在檔名前後用跳脫字元加上雙引號
                string fileName = "\"" + OrderTimeE.ToString("yyyy-MM-dd") + "【" + sellerName + " " + storeName + "-" + CouponUsage + "】憑證清冊.xls" + "\"";
                Introduction = Introduction == null ? string.Empty : Introduction;
                //是否有選項 (選項就是什麼藍色綠色之流的)
                bool hasOption = false;
                foreach (DataRow dr in sdt.Rows)
                {
                    if (!string.IsNullOrEmpty(dr["選項"].ToString()))
                    {
                        hasOption = true;
                        break;
                    }
                }
                string[] tableColumns = { "憑證編號", "訂購人姓名", hasOption ? "選項" : string.Empty, "好康憑證確認碼", "使用日期", "簽名" };   //欄位名稱

                #endregion

                #region 樣式
                short basicRowHeight = 605;
                short basicColumnWidth = 15 * 256;

                //首頁樣式
                CellStyle firstPageStyle = workbook.CreateCellStyle();
                firstPageStyle.Alignment = HorizontalAlignment.LEFT;
                firstPageStyle.VerticalAlignment = VerticalAlignment.CENTER;
                firstPageStyle.BorderBottom = CellBorderType.THICK;
                firstPageStyle.BorderLeft = CellBorderType.THICK;
                firstPageStyle.BorderRight = CellBorderType.THICK;
                firstPageStyle.BorderTop = CellBorderType.THICK;
                Font firstPageCellFont = workbook.CreateFont();
                firstPageCellFont.Boldweight = (short)FontBoldWeight.BOLD;
                firstPageCellFont.FontHeightInPoints = 12;
                firstPageStyle.SetFont(firstPageCellFont);
                firstPageStyle.WrapText = true;

                //首頁說明文字樣式
                CellStyle firstPageIntroStyle = workbook.CreateCellStyle();
                firstPageIntroStyle.Alignment = HorizontalAlignment.LEFT;
                firstPageIntroStyle.VerticalAlignment = VerticalAlignment.CENTER;
                firstPageIntroStyle.BorderBottom = CellBorderType.THICK;
                firstPageIntroStyle.BorderLeft = CellBorderType.THICK;
                firstPageIntroStyle.BorderRight = CellBorderType.THICK;
                firstPageIntroStyle.BorderTop = CellBorderType.THICK;
                Font firstPageIntroCellFont = workbook.CreateFont();
                firstPageIntroCellFont.Boldweight = (short)FontBoldWeight.BOLD;
                firstPageIntroCellFont.FontHeightInPoints = 10;
                firstPageIntroStyle.SetFont(firstPageIntroCellFont);
                firstPageIntroStyle.WrapText = true;

                //內容頁首行樣式
                CellStyle headerStyle = workbook.CreateCellStyle();
                headerStyle.Alignment = HorizontalAlignment.LEFT;
                headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
                Font headerCellFont = workbook.CreateFont();
                headerCellFont.Boldweight = (short)FontBoldWeight.BOLD;
                headerCellFont.FontHeightInPoints = 12;
                headerStyle.SetFont(headerCellFont);

                //內容頁首行CouponUsage樣式
                CellStyle headerCouponUsageStyle = workbook.CreateCellStyle();
                headerCouponUsageStyle.Alignment = HorizontalAlignment.LEFT;
                headerCouponUsageStyle.VerticalAlignment = VerticalAlignment.CENTER;
                Font headerCouponUsageFont = workbook.CreateFont();
                headerCouponUsageFont.Boldweight = (short)FontBoldWeight.BOLD;
                headerCouponUsageFont.FontHeightInPoints = 10;
                headerCouponUsageStyle.SetFont(headerCouponUsageFont);

                //內文樣式
                CellStyle style = workbook.CreateCellStyle();
                style.Alignment = HorizontalAlignment.CENTER;
                style.VerticalAlignment = VerticalAlignment.CENTER;
                style.BorderBottom = CellBorderType.THICK;
                style.BorderLeft = CellBorderType.THICK;
                style.BorderRight = CellBorderType.THICK;
                style.BorderTop = CellBorderType.THICK;
                style.WrapText = true;

                Font cellFont = workbook.CreateFont();
                cellFont.Boldweight = (short)FontBoldWeight.BOLD;
                cellFont.FontHeightInPoints = 10;
                style.SetFont(cellFont);
                #endregion

                #region 欄列設定

                //欄
                foreach (string cName in tableColumns)
                {
                    dt.Columns.Add(new DataColumn(cName));
                }

                //欄寬
                for (int i = 0; i < tableColumns.Length; i++)
                {
                    sheet.SetColumnWidth(i, basicColumnWidth);
                }

                //列
                foreach (DataRow sdr in sdt.Rows)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = sdr["憑證編號"].ToString();
                    dr[1] = sdr["姓名"].ToString();
                    if (hasOption)
                    {
                        dr[2] = sdr["選項"].ToString();
                    }
                    dt.Rows.Add(dr);
                }

                #endregion

                #region 首頁輸出

                #region 首列 (清冊名稱)
                BRow = sheet.CreateRow(rowIndex);
                BRow.Height = basicRowHeight;

                for (int i = 0; i < tableColumns.Length; i++)
                {
                    c = BRow.CreateCell(i);
                    c.SetCellValue(i == 0 ? "17Life商家專用憑證清冊" : string.Empty);
                    c.CellStyle = firstPageStyle;
                }
                reg = new NPOI.SS.Util.CellRangeAddress(rowIndex, rowIndex, 0, tableColumns.Length - 1);
                sheet.AddMergedRegion(reg);
                headerRowCounts++;
                rowIndex++;
                #endregion

                #region 次列 (店名)
                BRow = sheet.CreateRow(rowIndex);
                BRow.Height = basicRowHeight;

                c = BRow.CreateCell(0);
                c.SetCellValue("店名");
                c.CellStyle = firstPageStyle;

                for (int i = 1; i < tableColumns.Length; i++)
                {
                    c = BRow.CreateCell(i);
                    c.SetCellValue(i == 1 ? sellerName + " " + storeName : string.Empty);
                    c.CellStyle = firstPageStyle;
                }
                reg = new NPOI.SS.Util.CellRangeAddress(rowIndex, rowIndex, 1, tableColumns.Length - 1);
                sheet.AddMergedRegion(reg);
                headerRowCounts++;
                rowIndex++;
                #endregion

                #region 三列 (檔次)
                BRow = sheet.CreateRow(rowIndex);
                BRow.Height = basicRowHeight;

                c = BRow.CreateCell(0);
                c.SetCellValue("檔次");
                c.CellStyle = firstPageStyle;

                for (int i = 1; i < tableColumns.Length; i++)
                {
                    c = BRow.CreateCell(i);
                    c.SetCellValue(i == 1 ? CouponUsage : string.Empty);
                    c.CellStyle = firstPageStyle;
                }
                reg = new NPOI.SS.Util.CellRangeAddress(rowIndex, rowIndex, 1, tableColumns.Length - 1);
                sheet.AddMergedRegion(reg);
                headerRowCounts++;
                rowIndex++;
                #endregion

                #region 四列 (檔號)
                BRow = sheet.CreateRow(rowIndex);
                BRow.Height = basicRowHeight;

                c = BRow.CreateCell(0);
                c.SetCellValue("檔號");
                c.CellStyle = firstPageStyle;

                for (int i = 1; i < tableColumns.Length; i++)
                {
                    c = BRow.CreateCell(i);
                    c.SetCellValue(i == 1 ? UniqueId.ToString() : string.Empty);
                    c.CellStyle = firstPageStyle;
                }
                reg = new NPOI.SS.Util.CellRangeAddress(rowIndex, rowIndex, 1, tableColumns.Length - 1);
                sheet.AddMergedRegion(reg);
                headerRowCounts++;
                rowIndex++;
                #endregion

                #region 五列 (憑證有效期限)
                BRow = sheet.CreateRow(rowIndex);
                BRow.Height = basicRowHeight;

                c = BRow.CreateCell(0);
                c.SetCellValue("憑證有效期限");
                c.CellStyle = firstPageStyle;

                for (int i = 1; i < tableColumns.Length; i++)
                {
                    c = BRow.CreateCell(i);
                    c.SetCellValue(i == 1 ? UseTimeS.ToString("yyyy-MM-dd") + " ~ " + UseTimeE.ToString("yyyy-MM-dd") : string.Empty);
                    c.CellStyle = firstPageStyle;
                }
                reg = new NPOI.SS.Util.CellRangeAddress(rowIndex, rowIndex, 1, tableColumns.Length - 1);
                sheet.AddMergedRegion(reg);
                headerRowCounts++;
                rowIndex++;
                #endregion

                #region 六列 (好康特別使用說明)

                BRow = sheet.CreateRow(rowIndex);
                BRow.Height = (short)(12 * basicRowHeight);

                c = BRow.CreateCell(0);
                c.SetCellValue("好康特別使用說明");
                c.CellStyle = firstPageStyle;

                for (int i = 1; i < tableColumns.Length; i++)
                {
                    c = BRow.CreateCell(i);
                    c.SetCellValue(i == 1 ? Regex.Replace(Introduction.Replace("&auml;", "ä"), "<.*?>", string.Empty) : string.Empty);
                    c.CellStyle = firstPageStyle;
                }
                reg = new NPOI.SS.Util.CellRangeAddress(rowIndex, rowIndex, 1, tableColumns.Length - 1);
                sheet.AddMergedRegion(reg);
                headerRowCounts++;
                rowIndex++;

                #endregion

                #region 七列 (制式說明)

                BRow = sheet.CreateRow(rowIndex);
                BRow.Height = (short)(5 * basicRowHeight);

                //string Intro = string.Format(@"※　提醒您！「結檔後」下載的清冊，才會包含所有的憑證；結檔前下載的清冊，不包含所有憑證。{0} ※　此為店家機密文件，請相關人員務必妥善保管，並不得就其內容複製、重製、轉載或公開散佈等。亦不得任意洩露予他人。{1} ※　好康憑證不限本人使用，店家請核對憑證編號，一組編號限用一次。{2} ※　消費者持本憑證時，店家不需開立發票給消費者，除非消費超過抵用金額才需開立發票。"
                //, Environment.NewLine, Environment.NewLine, Environment.NewLine);
                string Intro = string.Format(@"※　提醒您！「結檔後」下載的清冊，才會包含所有的憑證；結檔前下載的清冊，不包含所有憑證。{0}※　清冊只是臨時記錄的輔助工具，正確的憑證清單請以「商家系統」→「核銷查詢」上的清單為準。{0}※　記錄在清冊上的憑證及確認碼，請務必於消費者使用當天（當下）進行核銷，以避免消費者套利，造成請款爭議唷！{0}※　此為店家機密文件，請相關人員務必妥善保管，並不得就其內容複製、重製、轉載或公開散佈等。亦不得任意洩露予他人。{0}※　好康憑證不限本人使用，店家請核對憑證編號，一組編號限用一次。{0}※　消費者持本憑證時，店家不需開立發票給消費者，除非消費超過抵用金額才需開立發票。"
                    , Environment.NewLine);
                for (int i = 0; i < tableColumns.Length; i++)
                {
                    c = BRow.CreateCell(i);
                    c.SetCellValue(i == 0 ? Intro : string.Empty);
                    c.CellStyle = firstPageIntroStyle;
                }
                reg = new NPOI.SS.Util.CellRangeAddress(rowIndex, rowIndex, 0, tableColumns.Length - 1);
                sheet.AddMergedRegion(reg);
                headerRowCounts++;
                rowIndex++;

                #endregion

                #endregion

                #region 內容輸出

                foreach (DataRow row in dt.Rows)
                {
                    string Unique_Id = "檔號:" + UniqueId;

                    #region 內容首頁首列
                    if (int.Equals(headerRowCounts, rowIndex % rowPerPage))
                    {
                        //首頁首列, 包括檔號/總銷售數/好康券名稱
                        if (rowIndex == headerRowCounts)
                        {
                            BRow = sheet.CreateRow(rowIndex);
                            BRow.Height = basicRowHeight;

                            //檔號
                            c = BRow.CreateCell(0);
                            c.SetCellValue(Unique_Id);
                            c.CellStyle = headerStyle;
                            c = null;

                            //總銷售數 (以前是結檔份數)
                            string totalSale = "總銷售數: " + allSaleCounts;
                            c = BRow.CreateCell(1);
                            c.SetCellValue(totalSale);
                            c.CellStyle = headerStyle;
                            c = null;

                            //好康券名稱
                            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowIndex, rowIndex, 2, tableColumns.Length - 1));
                            string CouponUsageTemp = CouponUsage.Length > maxCouponUsageLength ? CouponUsage.Substring(0, maxCouponUsageLength) : CouponUsage;
                            c = BRow.CreateCell(2);
                            c.SetCellValue(CouponUsageTemp);
                            c.CellStyle = headerCouponUsageStyle;
                        }
                        else
                        {
                            //分頁首列, 只有檔號
                            BRow = sheet.CreateRow(rowIndex);
                            BRow.Height = basicRowHeight;

                            Unique_Id = "檔號:" + UniqueId;
                            c = BRow.CreateCell(0);
                            c.SetCellValue(Unique_Id);
                            c.CellStyle = headerStyle;
                        }
                        c = null;
                        rowIndex++;
                    }
                    #endregion

                    #region 內容首頁次列，顯示各欄位的名稱
                    if (int.Equals(headerRowCounts + 1, rowIndex % rowPerPage))
                    {
                        BRow = sheet.CreateRow(rowIndex);
                        BRow.Height = basicRowHeight;

                        for (int i = 0; i < tableColumns.Length; i++)
                        {
                            c = BRow.CreateCell(i);
                            c.SetCellValue(tableColumns[i]);
                            c.CellStyle = style;
                            c = null;
                        }
                        rowIndex++;
                    }
                    #endregion

                    #region 憑證內容

                    //退貨狀態檢查
                    //強制退貨視為核銷 故狀態為強制退貨之憑證 不顯示退貨訊息
                    bool isReturned = false;
                    DateTime returnTime = DateTime.MinValue;
                    DataRow coupon = sdt.AsEnumerable().Where(x => x.Field<string>("憑證編號") == row["憑證編號"].ToString()).FirstOrDefault();
                    if (coupon != null)
                    {
                        isReturned = int.Parse(coupon["status"].ToString()).EqualsAny((int)TrustStatus.Refunded, (int)TrustStatus.Returned) &&
                                     !Helper.IsFlagSet(int.Parse(coupon["special_status"].ToString()), TrustSpecialStatus.ReturnForced);
                        returnTime = DateTime.Parse(coupon["modify_time"].ToString());
                    }

                    Row dataRow = sheet.CreateRow(rowIndex);
                    dataRow.Height = basicRowHeight;
                    int combineCells = 3;   //合併幾個儲存格來顯示退貨的字樣
                    foreach (DataColumn column in dt.Columns)
                    {
                        if (column.Ordinal == 3)
                        {
                            //判斷是否退貨. 如果是, 合併三個儲存格並顯示退貨字樣
                            if (isReturned)
                            {
                                for (int i = column.Ordinal; i <= column.Ordinal + combineCells - 1; i++)
                                {
                                    c = dataRow.CreateCell(i);
                                    c.SetCellValue("此憑證已於 " + returnTime.ToString("yyyy-MM-dd HH:mm:ss") + " 退貨完成！");
                                    c.CellStyle = style;
                                }
                                reg = new NPOI.SS.Util.CellRangeAddress(rowIndex, rowIndex, column.Ordinal, column.Ordinal + combineCells - 1);
                                sheet.AddMergedRegion(reg);
                                break;
                            }
                        }
                        c = dataRow.CreateCell(column.Ordinal);
                        c.CellStyle = style;
                        c.SetCellValue(row[column].ToString());
                    }
                    #endregion

                    rowIndex++;
                }
                #endregion

                #region 表頭設定
                string browser = HttpContext.Current.Request.UserAgent.ToUpper();
                ////HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName)));
                ////修正Excel中文檔名顯示問題 
                if (browser.Contains("IE") == true || browser.Contains("CHROME") == true)
                {
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlPathEncode(fileName)));
                }
                else if (browser.Contains("FIREFOX") == true || browser.Contains("SAFARI") == true)
                {
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName));
                }
                else if (browser.Contains("OPERA") == true)
                {
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName));
                    HttpContext.Current.Response.ContentType = "application/ms-excel";  //Opera 無此行會變成Html
                }
                else
                {
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlPathEncode(fileName)));
                }
                #endregion

                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
        }

        #endregion

        #region Goods return status in order
        /// <summary>
        /// 傳入一個CashTrustLogCollection, 判斷其為: 無退貨 / 部份退貨 / 全部退貨
        /// </summary>
        /// <param name="ctCol"></param>
        /// <returns></returns>
        public static int GoodsReturnStatusGet(CashTrustLogCollection ctCol)
        {
            int AllCounts = ctCol.Count(); int ReturnCounts = 0;
            foreach (CashTrustLog ct in ctCol)
            {
                if (ct.Status == (int)TrustStatus.Returned || ct.Status == (int)TrustStatus.Refunded)
                {
                    ReturnCounts += 1;
                }
            }
            if (ReturnCounts == 0)
            {
                return (int)OrderGoodsReturnStatus.NoneReturn;
            }
            else
            {
                if (ReturnCounts < AllCounts)
                {
                    return (int)OrderGoodsReturnStatus.PartialReturned;
                }
                else
                {
                    return (int)OrderGoodsReturnStatus.AllReturned;
                }
            }
        }

        #endregion

        public static void EntrustSellReceiptSet(PponDeliveryInfo deliveryInfo, Order o, OrderDetailCollection odCol,
                CashTrustLogCollection ctCol, string itemName, string userName)
        {
            //P好康跟P商品的差別是，如果一筆訂單買了多筆，P好康是一個憑證開一張收據，P商品是多商品開在同一張收據
            if (deliveryInfo.DeliveryType == DeliveryType.ToShop)
            {
                foreach (var ct in ctCol)
                {
                    if (ct.UninvoicedAmount > 0)
                    {
                        EntrustSellReceipt r = new EntrustSellReceipt();
                        r.OrderGuid = o.Guid;
                        r.ItemName = itemName;
                        r.ItemQuantity = 1;
                        r.ItemUnitPrice = odCol.First().ItemUnitPrice;
                        r.Amount = ct.UninvoicedAmount;
                        r.Tax = 0.05M;
                        r.BuyerName = deliveryInfo.InvoiceBuyerName;
                        r.BuyerAddress = deliveryInfo.InvoiceBuyerAddress;
                        r.ComId = deliveryInfo.UnifiedSerialNumber;
                        r.ForBusiness = string.IsNullOrEmpty(r.ComId) == false;
                        r.IsPhysical = deliveryInfo.IsEinvoice == false;
                        r.TrustId = ct.TrustId;
                        r.CreateTime = DateTime.Now;
                        r.CreateId = userName;
                        r.Type = (int)deliveryInfo.EntrustSell;
                        op.EntrustSellReceiptSet(r);
                    }
                }
            }
            else if (deliveryInfo.DeliveryType == DeliveryType.ToHouse)
            {
                int uninvoicedAmount = ctCol.Sum(t => t.UninvoicedAmount);
                EntrustSellReceipt r = new EntrustSellReceipt();
                r.OrderGuid = o.Guid;
                r.ItemName = itemName;
                r.ItemQuantity = odCol.Sum(t => t.ItemQuantity);
                r.ItemUnitPrice = odCol.First().ItemUnitPrice;
                r.Amount = uninvoicedAmount;
                r.Tax = 0.05M;
                r.BuyerName = deliveryInfo.InvoiceBuyerName;
                r.BuyerAddress = deliveryInfo.InvoiceBuyerAddress;
                r.ComId = deliveryInfo.UnifiedSerialNumber;
                r.ForBusiness = string.IsNullOrEmpty(r.ComId) == false;
                r.IsPhysical = deliveryInfo.IsEinvoice == false;
                r.CreateTime = DateTime.Now;
                r.CreateId = userName;
                r.Type = (int)deliveryInfo.EntrustSell;
                op.EntrustSellReceiptSet(r);
            }
        }

        #region 墨攻核銷

        /// <summary>
        /// 上傳墨攻產品檔(含拆分檔)
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        public static int UploadMohistProduct(DateTime dateStart, DateTime dateEnd)
        {
            int count = 0;
            string mohistId = config.MohistId2;
            StringBuilder prodcutSb = new StringBuilder();
            StringBuilder prodcutSplitSb = new StringBuilder();
            int flag = (int)MohistRedoFlag.Product;

            #region ppon
            ViewPponDealCollection vpdc = pp.ViewPponDealGetListByPeriod(dateStart, dateEnd);
            List<ViewPponDeal> vpdList = vpdc.Where(x => Helper.GetBusinessHourTrustProvider(x.BusinessHourStatus).Equals(TrustProvider.Sunny)).Where(x => !Helper.IsFlagSet(x.BusinessHourStatus, BusinessHourStatus.ComboDealMain)).ToList();
            MohistVerifyInfoCollection RedoMvic = op.MohistVerifyInfoGetByRedoFlag(flag);
            foreach (MohistVerifyInfo mvi in RedoMvic)
            {
                if (!vpdList.Any(x => x.UniqueId.Value.ToString() == mvi.MohistProductId))
                {
                    vpdList.Add(pp.ViewPponDealGetByUniqueId(int.Parse(mvi.MohistProductId)));
                }
            }

            foreach (var vpd in vpdList)
            {
                prodcutSb.AppendLine(@"""" + vpd.ItemName + @"""" + "," + vpd.ItemPrice.ToString("F0") + "," + mohistId + "," + vpd.UniqueId + "," + vpd.CompanyId + "," + "1" + "," + string.Empty + "," + string.Empty + "," + string.Empty + "," + string.Empty);
                List<DealCost> dcc = pp.DealCostGetList(vpd.BusinessHourGuid).OrderBy(x => x.CumulativeQuantity).ToList();
                for (int i = 0; i < dcc.Count(); i++)
                {
                    int quantity = dcc[i].LowerCumulativeQuantity != null ? dcc[i].LowerCumulativeQuantity.Value : 0;
                    prodcutSplitSb.AppendLine(mohistId + "," + vpd.UniqueId + "," + (quantity.Equals(0) ? 0 : quantity + 1) + "," + ((vpd.ItemPrice - dcc[i].Cost) / vpd.ItemPrice * 100).Value.ToString("F0") + "," + "0" + "," + "1");
                    count++;
                }
            }
            RedoMvic.ForEach(x =>
            {
                x.RedoFlag = (int)Helper.SetFlag(false, x.RedoFlag ?? 0, flag);
                x.RedoTime = DateTime.Now;
                op.MohistVerifyInfoSet(x);
            });
            #endregion

            #region piinlife
            HiDealDealCollection hddc = hp.HiDealDealGetListByPeriod(dateStart, dateEnd);
            foreach (var hdd in hddc)
            {
                Seller seller = sp.SellerGet(hdd.SellerGuid);
                List<HiDealProduct> hdpc = hp.HiDealProductCollectionGet(hdd.Id).Where(x => ((TrustProvider)x.TrustType).Equals(TrustProvider.Sunny)).ToList();
                foreach (HiDealProduct product in hdpc)
                {
                    prodcutSb.AppendLine(@"""" + product.Name + @"""" + "," + product.Price.Value.ToString("F0") + "," + mohistId + "," + (string.Format("{0:00000000}", product.Id)).ToString() + "," + seller.CompanyID + "," + "1" + "," + string.Empty + "," + string.Empty + "," + string.Empty + "," + string.Empty);
                    List<HiDealProductCost> hdpcc = hp.HiDealProductCostCollectionGetByProductId(product.Id).OrderBy(x => x.CumulativeQuantitiy).ToList();
                    for (int i = 0; i < hdpcc.Count; i++)
                    {
                        prodcutSplitSb.AppendLine(mohistId + "," + (string.Format("{0:00000000}", product.Id)).ToString() + "," + (hdpcc[i].CumulativeQuantitiy - hdpcc[i].Quantity) + "," + ((product.Price - hdpcc[i].Cost) / product.Price * 100).Value.ToString("F0") + "," + "0" + "," + "1");
                        count++;
                    }
                }
            }
            #endregion

            // upload
            //增加config避免本機誤傳
            if (config.EnabledMohistUpload)
            {
                UploadFileToFtp(Encoding.UTF8.GetBytes(prodcutSb.ToString()), string.Format("{0}_product.csv", dateStart.ToString("yyyyMMdd")), config.MohistVerifyFtpPath2, config.MohistVerifyFtpAccountId2, config.MohistVerifyFtpPassWord2);
                UploadFileToFtp(Encoding.UTF8.GetBytes(prodcutSplitSb.ToString()), string.Format("{0}_product_split.csv", dateStart.ToString("yyyyMMdd")), config.MohistVerifyFtpPath2, config.MohistVerifyFtpAccountId2, config.MohistVerifyFtpPassWord2);
            }
            return count;
        }

        /// <summary>
        /// 寫入墨攻交易資訊
        /// </summary>
        /// <param name="dateStart">訂單日期(起)</param>
        /// <param name="dateEnd">訂單日期(迄)</param>
        public static void SaveMohistVerifyInfo(DateTime dateStart, DateTime dateEnd)
        {
            string mohistId = config.MohistId;
            string mohistId2 = config.MohistId2;
            int sequence = 1;

            CashTrustLogCollection ctlc = mp.CashTrustLogGetListByOrderCreateTime(dateStart, dateEnd, TrustProvider.Sunny);
            string date = string.Empty;
            string orderId = string.Empty;

            // 取得這些cash_trust_log的bid清單
            IEnumerable<CashTrustLog> pponCtlc = ctlc.Where(x => x.OrderClassification.Equals((int)OrderClassification.LkSite));
            Dictionary<Guid, ViewPponDeal> dataLsit = new Dictionary<Guid, ViewPponDeal>();
            foreach (Guid bid in pponCtlc.GroupBy(x => x.BusinessHourGuid, x => x.BusinessHourGuid).Select(g => g.Key.Value))
            {
                ViewPponDeal vpdItem = pp.ViewPponDealGetByBusinessHourGuid(bid);
                if (!Helper.IsFlagSet(vpdItem.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                {
                    dataLsit.Add(bid, vpdItem);
                }
            }

            foreach (CashTrustLog ctl in ctlc)
            {
                if (ctl.CouponId != null)
                {
                    MohistVerifyInfo info = new MohistVerifyInfo();
                    info.Amount = ctl.Amount;
                    info.Bonus = ctl.Bcash + ctl.DiscountAmount;
                    info.Ecash = ctl.Pcash + ctl.Scash;
                    info.AuthCode = string.Empty;
                    info.CouponId = ctl.CouponId.ToString();
                    info.Status = (int)MohistVerifyStatus.NotUsed;
                    info.VerifyType = (int)MohistVerifyType.Verify;

                    switch ((OrderClassification)ctl.OrderClassification)
                    {
                        case OrderClassification.LkSite:

                            Coupon coupon = pp.CouponGetList(ctl.OrderDetailGuid).FirstOrDefault(x => x.SequenceNumber == ctl.CouponSequenceNumber);
                            Order order = op.OrderGet(ctl.OrderGuid);
                            // 成立單才需拋給墨攻系統
                            if (order.IsLoaded && Helper.IsFlagSet(order.OrderStatus, OrderStatus.Complete))
                            {
                                MohistVerifyInfo item = op.MohistVerifyInfoGet(coupon.Id.ToString(), coupon.SequenceNumber);
                                if (!orderId.Equals(order.OrderId))
                                {
                                    sequence = date.Equals(order.CreateTime.ToString("yyyyMMddHH")) ? ++sequence : 1;
                                }

                                date = order.CreateTime.ToString("yyyyMMddHH");
                                orderId = order.OrderId;
                                ViewPponDeal vpd = dataLsit.Where(x => x.Key.Equals(ctl.BusinessHourGuid)).Select(g => g.Value).FirstOrDefault();
                                if (item.IsNew && vpd != null)
                                {
                                    info.OrderDate = order.CreateTime;
                                    info.MohistDealsId = string.Format("{0}{1}{2}", info.OrderDate < config.PayeasyToContact ? mohistId : mohistId2, order.CreateTime.ToString("yyyyMMddHH"), string.Format("{0:0000}", sequence));
                                    info.MohistProductId = vpd.UniqueId.ToString();
                                    info.ItemPrice = vpd.ItemPrice;
                                    info.CouponId = coupon.Id.ToString();
                                    info.SequenceNumber = ctl.TrustSequenceNumber;
                                    info.Barcode = string.Format("{0}-{1}", ctl.CouponSequenceNumber, coupon.IsLoaded ? coupon.Code : string.Empty);
                                    info.CouponCode = coupon.IsLoaded ? coupon.Code : string.Empty;
                                    info.OrderId = order.OrderId;
                                    op.MohistVerifyInfoSet(info);
                                }
                            }
                            break;
                        case OrderClassification.HiDeal:
                            ViewHiDealCoupon hidealCoupon = hp.ViewHiDealCouponGet(ctl.CouponId.Value);
                            MohistVerifyInfo hidealItem = op.MohistVerifyInfoGet(ctl.CouponId.ToString(), hidealCoupon.Prefix + hidealCoupon.Sequence);
                            HiDealOrder ho = hp.HiDealOrderGet(ctl.OrderGuid);
                            if (!orderId.Equals(ho.OrderId))
                            {
                                sequence = date.Equals(ho.CreateTime.ToString("yyyyMMddHH")) ? ++sequence : 1;
                            }

                            date = ho.CreateTime.ToString("yyyyMMddHH");
                            orderId = ho.OrderId;
                            if (hidealItem.IsNew)
                            {
                                info.OrderDate = ho.CreateTime;
                                info.MohistDealsId = string.Format("{0}{1}{2}", info.OrderDate < config.PayeasyToContact ? mohistId : mohistId2, ho.CreateTime.ToString("yyyyMMddHH"), string.Format("{0:0000}", sequence));
                                info.MohistProductId = (string.Format("{0:00000000}", hidealCoupon.ProductId)).ToString();
                                info.ItemPrice = hidealCoupon.Price.Value;
                                info.SequenceNumber = hidealCoupon.Prefix + hidealCoupon.Sequence;
                                info.Barcode = string.Format("{0}-{1}", hidealCoupon.Prefix + hidealCoupon.Sequence, hidealCoupon != null ? hidealCoupon.Code : string.Empty);
                                info.CouponCode = hidealCoupon != null ? hidealCoupon.Code : string.Empty;
                                info.OrderId = ho.OrderId;
                                op.MohistVerifyInfoSet(info);
                            }
                            break;
                        case OrderClassification.CashPointOrder:
                        case OrderClassification.Other:
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// 註記異動狀態(單筆)
        /// </summary>
        public static void UpdateMohistVerifyInfoStatus(string CouponId, string SequenceNumber, string memo)
        {
            MohistVerifyInfo info = op.MohistVerifyInfoGet(CouponId, SequenceNumber);
            info.Memo = memo;
            op.MohistVerifyInfoSet(info);
        }

        public static void UpdateMohistVerifyInfoRedo(string CouponId, string SequenceNumber, MohistRedoFlag RedoFlag)
        {
            MohistVerifyInfo info = op.MohistVerifyInfoGet(CouponId, SequenceNumber);
            if (!info.IsNew)
            {
                info.RedoFlag = (int)Helper.SetFlag(true, info.RedoFlag ?? 0, RedoFlag);
                info.RedoTime = DateTime.Now;
                op.MohistVerifyInfoSet(info);
            }
        }

        /// <summary>
        /// 註記異動狀態
        /// </summary>
        public static List<string> UpdateMohistVerifyInfoStatusByPeriod(DateTime dateStart, DateTime dateEnd)
        {
            List<string> unusualCoupon = new List<string>();
            CashTrustLogCollection ctlc = mp.CashTrustLogGreaterThanStatusGetListByPeriod(dateStart, dateEnd, TrustProvider.Sunny, TrustStatus.Initial);
            foreach (CashTrustLog ctl in ctlc)
            {
                if (ctl.CouponId != null)
                {
                    MohistVerifyInfo info = op.MohistVerifyInfoGet(ctl.BusinessHourGuid.Value, ctl.Prefix + (string.IsNullOrEmpty(ctl.TrustSequenceNumber) ? ctl.CouponSequenceNumber : ctl.TrustSequenceNumber));
                    if (info.IsLoaded)
                    {
                        if (info.Status < (int)MohistVerifyStatus.Used && ctl.Status >= (int)TrustStatus.Verified)
                        {
                            // 核銷(未使用=>核銷)/作廢(未使用=>退貨)
                            switch ((TrustStatus)ctl.Status)
                            {
                                case TrustStatus.Verified:
                                    info.Status = (int)MohistVerifyStatus.Used;
                                    info.VerifyType = (int)MohistVerifyType.Verify;
                                    break;
                                case TrustStatus.Returned:
                                case TrustStatus.Refunded:
                                    info.Status = (int)MohistVerifyStatus.Invalid;
                                    info.VerifyType = (int)MohistVerifyType.Invalid;
                                    break;
                                default:
                                    break;
                            }
                            info.ModifyTime = ctl.ModifyTime;
                            op.MohistVerifyInfoSet(info);
                        }
                        else
                        {
                            string statusDesc = string.Empty;
                            if (info.Status.Equals((int)MohistVerifyStatus.Used) && !ctl.Status.Equals((int)TrustStatus.Verified))
                            {
                                info.Status = ctl.Status;
                                info.SpecialTime = ctl.ModifyTime;
                                info.SpecialStatus = ctl.SpecialStatus;
                                op.MohistVerifyInfoSet(info);

                                if (ctl.Status > (int)TrustStatus.Verified)
                                {
                                    statusDesc = "強制退貨";
                                }
                                else if (ctl.Status < (int)TrustStatus.Verified)
                                {
                                    statusDesc = "反核銷";
                                }
                            }
                            else if (info.Status > (int)MohistVerifyStatus.Used && ctl.Status <= (int)TrustStatus.Verified)
                            {
                                info.Status = ctl.Status;
                                info.SpecialTime = ctl.ModifyTime;
                                info.SpecialStatus = ctl.SpecialStatus;
                                op.MohistVerifyInfoSet(info);

                                if (ctl.Status < (int)TrustStatus.Verified)
                                {
                                    statusDesc = "恢復訂單";
                                }
                                else if (ctl.Status.Equals((int)TrustStatus.Verified))
                                {
                                    statusDesc = "強制核銷";
                                }
                            }

                            if (!string.IsNullOrEmpty(statusDesc))
                            {
                                unusualCoupon.Add(info.Barcode + " " + statusDesc);
                            }
                        }
                    }
                }
            }
            return unusualCoupon;
        }

        /// <summary>
        /// 上傳墨攻交易檔(含驗證檔)
        /// </summary>
        /// <param name="uniqueId"></param>
        public static int UploadMohistDeals(DateTime dateStart, DateTime dateEnd)
        {
            string mohistId = config.MohistId2;
            StringBuilder dealsSb = new StringBuilder();
            StringBuilder dealsAllSb = new StringBuilder();
            decimal amount = 0;
            decimal bonus = 0;
            int verifyCount = 0;
            int flag = (int)MohistRedoFlag.OrderDeal;

            MohistVerifyInfoCollection mvic = op.MohistVerifyInfoGetByOrderDatePeriod(dateStart, dateEnd);
            MohistVerifyInfoCollection RedoMvic = op.MohistVerifyInfoGetByRedoFlag(flag);
            foreach (MohistVerifyInfo mvi in RedoMvic)
            {
                if (!mvic.Any(x => x == mvi))
                {
                    mvic.Add(mvi);
                }
            }
            foreach (var mvi in mvic)
            {
                dealsSb.AppendLine(mvi.MohistDealsId + "," + mvi.OrderDate.ToString("yyyy-MM-dd") + "," + mvi.MohistProductId + "," + mvi.ItemPrice.ToString("F0") + "," + string.Empty + "," + mohistId + "," + "c" + "," + "1" + "," + string.Empty + "," + string.Empty + "," + "bbb" + "," + mvi.OrderDate.ToString("HH:mm:ss") + "," + mvi.Amount.ToString("F0") + "," + string.Empty + "," + mvi.AuthCode + "," + mvi.OrderId + "," + "0" + "," + mvi.Barcode + "," + mvi.CouponCode + "," + mvi.Ecash.ToString("F0"));

                amount += mvi.Amount;
                //bonus += mvi.Bonus;
                verifyCount++;
            }
            if (verifyCount > 0)
            {
                dealsAllSb.AppendLine(amount.ToString("F0") + "," + bonus.ToString("F0") + "," + verifyCount.ToString() + "," + "0");
            }

            RedoMvic.ForEach(x =>
            {
                x.RedoFlag = (int)Helper.SetFlag(false, x.RedoFlag ?? 0, flag);
                x.RedoTime = DateTime.Now;
                op.MohistVerifyInfoSet(x);
            });

            // upload
            //增加config避免本機誤傳
            if (config.EnabledMohistUpload)
            {
                UploadFileToFtp(Encoding.UTF8.GetBytes(dealsSb.ToString()), string.Format("{0}_deals.csv", dateStart.ToString("yyyyMMdd")), config.MohistVerifyFtpPath2, config.MohistVerifyFtpAccountId2, config.MohistVerifyFtpPassWord2);
                UploadFileToFtp(Encoding.UTF8.GetBytes(dealsAllSb.ToString()), string.Format("{0}_deals_all.csv", dateStart.ToString("yyyyMMdd")), config.MohistVerifyFtpPath2, config.MohistVerifyFtpAccountId2, config.MohistVerifyFtpPassWord2);
            }

            return verifyCount;
        }

        /// <summary>
        /// 上傳墨攻核銷檔(含驗證檔)
        /// </summary>
        /// <param name="uniqueId"></param>
        public static int UploadMohistConfirms(DateTime dateStart, DateTime dateEnd)
        {
            //string mohistId = config.MohistId;
            string mohistId2 = config.MohistId2;
            //StringBuilder confirmSb = new StringBuilder();
            StringBuilder confirmSb2 = new StringBuilder();
            //StringBuilder confirmAllSb = new StringBuilder();
            StringBuilder confirmAllSb2 = new StringBuilder();
            //decimal amount = 0;
            decimal amount2 = 0;
            //decimal bonus = 0;
            decimal bonus2 = 0;
            //int verifyCount = 0;
            int verifyCount2 = 0;
            int flag = (int)MohistRedoFlag.Confirm;

            List<MohistVerifyInfo> mvic = op.MohistVerifyInfoGetByPeriod(dateStart, dateEnd).Where(x => x.Status > (int)MohistVerifyStatus.NotUsed && x.SpecialTime == null).ToList();
            MohistVerifyInfoCollection RedoMvic = op.MohistVerifyInfoGetByRedoFlag(flag);
            foreach (MohistVerifyInfo mvi in RedoMvic)
            {
                if (!mvic.Any(x => x == mvi))
                {
                    mvic.Add(mvi);
                }
            }

            foreach (var mvi in mvic)
            {
                string useTime = string.Empty;
                string invalidTime = string.Empty;

                switch ((MohistVerifyStatus)mvi.Status)
                {
                    case MohistVerifyStatus.Used:
                        useTime = mvi.ModifyTime.Value.ToString("yyyy-MM-dd");
                        invalidTime = string.Empty;
                        break;
                    case MohistVerifyStatus.Invalid:
                        useTime = string.Empty;
                        invalidTime = mvi.ModifyTime.Value.ToString("yyyy-MM-dd");
                        break;
                    case MohistVerifyStatus.NotUsed:
                    case MohistVerifyStatus.Initial:
                    default:
                        break;

                }
                confirmSb2.AppendLine(mohistId2 + "," + mvi.Status + "," + useTime + "," + invalidTime + "," + mvi.Barcode + "," + mvi.MohistProductId + "," + mvi.OrderId + "," + mvi.CouponCode + "," + mvi.VerifyType);
                amount2 += mvi.Amount;
                verifyCount2++;
            }
            if (verifyCount2 > 0)
            {
                confirmAllSb2.AppendLine(amount2.ToString("F0") + "," + bonus2.ToString("F0") + "," + verifyCount2.ToString() + "," + "0");
            }

            RedoMvic.ForEach(x =>
            {
                x.RedoFlag = (int)Helper.SetFlag(false, x.RedoFlag ?? 0, flag);
                x.RedoTime = DateTime.Now;
                op.MohistVerifyInfoSet(x);
            });

            // upload
            //增加config避免本機誤傳
            if (config.EnabledMohistUpload)
            {
                UploadFileToFtp(Encoding.UTF8.GetBytes(confirmSb2.ToString()), string.Format("{0}_confirm.csv", dateStart.ToString("yyyyMMdd")), config.MohistVerifyFtpPath2, config.MohistVerifyFtpAccountId2, config.MohistVerifyFtpPassWord2);
                UploadFileToFtp(Encoding.UTF8.GetBytes(confirmAllSb2.ToString()), string.Format("{0}_confirm_all.csv", dateStart.ToString("yyyyMMdd")), config.MohistVerifyFtpPath2, config.MohistVerifyFtpAccountId2, config.MohistVerifyFtpPassWord2);
            }
            return verifyCount2;
        }

        /// <summary>
        /// 上傳ftp
        /// </summary>
        /// <param name="fileContents"></param>
        /// <param name="filename"></param>
        /// <param name="ftp_dir"></param>
        public static void UploadFileToFtp(byte[] fileContents, string filename, string ftp_dir, string ftp_account, string ftp_pw)
        {
            if (!string.IsNullOrEmpty(ftp_dir))
            {
                string uri = string.Format("ftp://{0}/{1}", ftp_dir, filename);
                try
                {
                    // Get the object used to communicate with the server.
                    FtpWebRequest reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));
                    reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

                    // This example assumes the FTP site uses anonymous logon.
                    reqFTP.Credentials = new NetworkCredential(ftp_account, ftp_pw);
                    reqFTP.ContentLength = fileContents.Length;

                    Stream requestStream = reqFTP.GetRequestStream();
                    requestStream.Write(fileContents, 0, fileContents.Length);
                    requestStream.Close();
                }
                catch (Exception ex)
                {
                    MailMessage msg = new MailMessage();
                    msg.IsBodyHtml = true;
                    msg.From = new MailAddress(config.AdminEmail);
                    msg.To.Add(config.AdminEmail);
                    msg.Subject = "[系統] 墨攻核銷資訊上傳失敗";
                    msg.Body = string.Format("墨攻核銷資訊上傳失敗，請重新上傳，檔案:{0}；路徑:{1}<br />錯誤訊息:{2}", filename, uri, ex);
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }
            }
        }

        public static string VerifyCouponByMohist(MohistVerifyInfo mohistInfo, int couponId, string ip, string createId)
        {
            string msg = string.Empty;
            Guid sid;
            string couponCode = string.Empty;
            OrderClassification orderClassification;
            ViewPponCoupon coupon = pp.ViewPponCouponGet(couponId);
            if (coupon.IsLoaded)
            {
                sid = coupon.SellerGuid;
                couponCode = coupon.CouponCode;
                orderClassification = OrderClassification.LkSite;
            }
            else
            {
                ViewHiDealCoupon hidealCoupon = hp.ViewHiDealCouponGet(couponId);
                sid = hidealCoupon.SellerGuid;
                couponCode = hidealCoupon.Code;
                orderClassification = OrderClassification.HiDeal;
            }

            // 檢查CashTrustLog狀態是否未使用
            CashTrustLog ctl = mp.CashTrustLogGetByCouponId(couponId, orderClassification);
            if (ctl.Status < (int)TrustStatus.Verified)
            {
                try
                {
                    OrderFacade.VerifyCoupon(couponId, sid, mohistInfo.MohistDealsId, ip, createId, orderClassification);
                    // 註記 MohistVerifyInfo 為 web service 核銷，並異動狀態
                    if (mohistInfo.IsLoaded && ((MohistVerifyStatus)mohistInfo.Status).Equals(MohistVerifyStatus.NotUsed))
                    {
                        mohistInfo.Status = (int)MohistVerifyStatus.Used;
                        mohistInfo.VerifyType = (int)MohistVerifyType.Verify;
                        mohistInfo.IsWebserviceVerify = true;
                        mohistInfo.ModifyTime = DateTime.Now;
                        op.MohistVerifyInfoSet(mohistInfo);

                        msg = "核銷憑證已完成!";
                    }
                    else
                    {
                        msg = "核銷紀錄不符，請通知 17life 技術人員。";
                    }
                }
                catch (Exception e)
                {
                    logger.Error("SetMohistVerifyInfo Error: " + mohistInfo.MohistDealsId + "," + ctl.CouponSequenceNumber + "," + couponCode, e);
                }
            }
            else
            {
                msg = "憑證已使用，無法核銷憑證。";
            }

            return msg;
        }
        #endregion

        #region iChannel導購
        /// <summary>
        /// 寫入iChannel資訊
        /// </summary>
        /// <param name="o"></param>
        public static void SaveIChannelInfo(Order o, Guid bid, int? deliveryType, string gid)
        {
            if (config.EnableIChannel)
            {
                if (deliveryType.HasValue)
                {
                    var ctlCollection = mp.CashTrustLogGetListByOrderGuid(o.Guid, OrderClassification.LkSite).Where(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight));
                    foreach (var ctl in ctlCollection)
                    {
                        var paidAmount = GetCashTustLogPaidAmount(ctl);
                        if (paidAmount > 0)
                        {
                            IchannelInfo info = new IchannelInfo();
                            info.TrustId = ctl.TrustId;
                            info.OrderGuid = o.Guid;
                            info.Bid = bid;
                            info.DeliveryType = deliveryType.Value;
                            //判斷Gid字尾是否有?
                            if (gid.Length > 0 && string.Equals(gid.Substring(gid.Length - 1, 1), "?"))
                            {
                                gid = gid.Substring(0, gid.Length - 1);
                            }
                            info.Gid = gid;
                            info.Amount = paidAmount;
                            info.CreateTime = DateTime.Now;

                            op.IChannelInfoSet(info);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 現金回餽型導購, 實付金額計算方式
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        private static int GetCashTustLogPaidAmount(CashTrustLog log)
        {
            return log.Amount - log.DiscountAmount - log.Bcash;
        }

        /// <summary>
        /// 根據訂單GUID取得iChannel資訊
        /// </summary>
        public static IEnumerable<ViewIchannelInfo> GetViewIChannelInfoByOrderId(Guid guid)
        {
            ViewIchannelInfoCollection vic = op.ViewIchannelInfoGet(guid);
            return vic;
        }

        /// <summary>
        /// 根據訂單時間取得iChannel資訊(會排除ATM尚未付款)
        /// </summary>
        public static IEnumerable<ViewIchannelInfo> GetViewIChannelInfoByOrderTimePeriod(DateTime startTime, DateTime endTime)
        {
            ViewIchannelInfoCollection vic = op.ViewIchannelInfoGetList(startTime, endTime);
            logger.InfoFormat("查詢IChannelInfo, 時間區間 {0}~{1}, 回傳筆數:{2}", startTime.ToString("yyyy/MM/dd HH:mm:ss"),
                endTime.ToString("yyyy/MM/dd HH:mm:ss"), vic.Count);
            return vic;
        }

        /// <summary>
        /// IChannels S2S API Call
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="forTest"></param>
        public static string IChannelAPI(DateTime startTime, DateTime endTime, bool forTest = false)
        {
            string result = string.Empty;
            var vic = OrderFacade.GetViewIChannelInfoByOrderTimePeriod(startTime, endTime);
            vic = vic.Where(x => x.SendType == 0 && IchannelCheckPass(x)); //還沒回拋 與 回拋錯誤要重送的
            foreach (var ico in vic.GroupBy(x => new { Id = x.Id, OrderGuid = x.OrderGuid, DeliveryType = x.DeliveryType, OrderId = x.OrderId, OrderTime = x.OrderTime, Gid = x.Gid, Bid = x.Bid }, y => y))
            {
                if (ico.Key.DeliveryType == (int)DeliveryType.ToShop)
                {
                    #region 憑證 (因憑證狀態不同，以多筆訂單型態送出)

                    var returnCode = GetCommissionRuleReturnCode(ChannelSource.iChannel, DeliveryType.ToShop, ico.Key.Bid, ico.Key.OrderTime);

                    foreach (var item in ico)
                    {
                        result += IChannelAPICall(item.Id, config.IChannelMcode, ico.Key.Gid, item.Amount.ToString("F0"), returnCode, ico.Key.OrderId + "-" + item.CouponId, ico.Key.OrderTime, forTest) + "\n";
                    }

                    #endregion
                }
                else if (ico.Key.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    #region 宅配 (因宅配狀態一致，多筆商品金額以"|"合併資料送出)

                    var returnCode = GetCommissionRuleReturnCode(ChannelSource.iChannel, DeliveryType.ToHouse, ico.Key.Bid, ico.Key.OrderTime);

                    result += IChannelAPICall(ico.Key.Id, config.IChannelMcode, ico.Key.Gid, string.Join("|", ico.Select(x => x.Amount.ToString("F0"))), string.Join("|", ico.Select(x => returnCode)), ico.Key.OrderId, ico.Key.OrderTime, forTest) + "\n";

                    #endregion
                }
            }

            logger.InfoFormat("\niChannel API Log:\n{0}", result);

            return result;
        }

        public static bool IchannelCheckPass(ViewIchannelInfo info)
        {
            var checkOldData = new DateTime(2017, 7, 1, 0, 0, 0);
            if (info.OrderTime >= checkOldData)
            {
                return true;
            }

            var checkWangSteakData = new DateTime(2015, 12, 4, 0, 0, 0);
            if (info.ItemName.IndexOf("王品集團餐廳") > -1 && info.OrderTime >= checkWangSteakData)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 分潤邏輯
        /// </summary>
        /// <param name="source"></param>
        /// <param name="type"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static string GetCommissionRuleReturnCode(ChannelSource source, DeliveryType type, Guid bid, DateTime orderTime)
        {
            var checkOldData = new DateTime(2017, 7, 1, 0, 0, 0);

            if (orderTime < checkOldData || !config.EnableCommissionRule)
            {
                return type == DeliveryType.ToHouse ? config.IChannelItemToHouse : config.IChannelItemToShop;
            }

            var rules = cp.GetChannelCommissionRules(source);

            #region 取得預設值

            var defaultRule = type == DeliveryType.ToHouse
                ? rules.FirstOrDefault(x => x.RuleType == (byte)CommissionRuleType.DeliveryDefault)
                : rules.FirstOrDefault(x => x.RuleType == (byte)CommissionRuleType.CouponDefault);

            var returnCode = defaultRule == null ? string.Empty : defaultRule.ReturnCode;

            #endregion

            #region 分類一般條件

            var ruleDealType =
                rules.Where(
                    x =>
                        x.RuleType == (byte)CommissionRuleType.NormalCondition && x.DealType != 0 &&
                        x.KeyWord == string.Empty);

            var ruleKeyWord =
                rules.Where(
                    x =>
                        x.RuleType == (byte)CommissionRuleType.NormalCondition && x.DealType == 0 &&
                        !string.IsNullOrEmpty(x.KeyWord));

            var ruleCombo = rules.Where(x => x.RuleType == (byte)CommissionRuleType.NormalCondition && x.DealType != 0 && !string.IsNullOrEmpty(x.KeyWord));

            #endregion

            #region 取得檔次資訊

            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            var checkContent = new List<string> { vpd.CouponUsage.ToLower() };

            if ((vpd.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0 && vpd.MainBid != null)
            {
                var mainVpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vpd.MainBid ?? Guid.Empty);
                checkContent.Add(mainVpd.CouponUsage.ToLower());
            }

            #endregion

            var meetRule = new List<ChannelCommissionRule>();

            var dt = ruleDealType.FirstOrDefault(x => x.DealType == vpd.DealType);
            if (dt != null)
            {
                meetRule.Add(dt);
            }

            meetRule.AddRange(
                from rule in ruleKeyWord
                let keywords = rule.KeyWord.Split(',')
                from k in keywords
                where checkContent.Any(x => x.IndexOf(k.ToLower(), StringComparison.Ordinal) > -1)
                select rule);

            meetRule.AddRange(
                from rule in ruleCombo
                where rule.DealType == vpd.DealType
                from k in rule.KeyWord.Split(',')
                where checkContent.Any(x => x.IndexOf(k.Trim().ToLower(), StringComparison.Ordinal) > -1)
                select rule);

            if (meetRule.Any())
            {
                returnCode = meetRule.OrderBy(x => x.GrossMarginLimit).First().ReturnCode;
            }

            return returnCode;
        }

        private static string IChannelAPICall(int id, string mcode, string gid, string amount, string bid, string oid, DateTime datetime, bool forTest = false)
        {
            int result = 0;
            string responseFromServer = string.Empty;
            string url = string.Format("{0}?mcode={1}&gid={2}&oid={3}&amount={4}&bid={5}&date={6}"
                                        , config.IChannelApiUrl
                                        , mcode
                                        , gid
                                        , oid
                                        , HttpUtility.UrlEncode(amount)
                                        , HttpUtility.UrlEncode(bid)
                                        , HttpUtility.UrlEncode(datetime.ToString("yyyy-MM-dd HH:mm:ss")));
            var requestStartTime = DateTime.Now;

            if (!forTest)
            {
                try
                {
                    WebRequest request = WebRequest.Create(url);
                    request.Method = "GET";
                    request.ContentLength = 0;
                    request.Timeout = 180000;

                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        using (Stream dataStream = response.GetResponseStream())
                        {
                            using (StreamReader reader = new StreamReader(dataStream))
                            {
                                responseFromServer = reader.ReadToEnd();

                                if (!int.TryParse(responseFromServer, out result))
                                {
                                    logger.InfoFormat("iChannel API 回傳錯誤訊息。{0}", responseFromServer);
                                }
                                else if (result == 0)
                                {
                                    logger.InfoFormat("iChannel API 回傳失敗。{0}", responseFromServer);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var requestEndTime = DateTime.Now;
                    var spendtime = ((TimeSpan)(requestEndTime - requestStartTime)).TotalMilliseconds.ToString();
                    log4net.LogManager.GetLogger("LogToDb").Error(
                        string.Format(" IChannelAPICall Exception=>{0},url={1},spend time={2}",
                            ex.ToString(),
                            url,
                            spendtime.ToString()
                            )
                        );

                }
            }
            else
            {
                responseFromServer = "1";
            }

            IchannelInfo vic = op.IchannelInfoGetById(id);
            vic.ModifyTime = DateTime.Now;
            vic.SendCount++;
            vic.SendType = result;
            vic.ReturnCode = bid;
            op.IChannelInfoSet(vic);

            return responseFromServer + "=" + url;
        }

        /// <summary>
        /// IChannels Send
        /// </summary>
        /// <param name="vic"></param>
        public static string IChannelSend(Guid guid)
        {
            string result = string.Empty;
            var vic = OrderFacade.GetViewIChannelInfoByOrderId(guid);
            foreach (var ico in vic.GroupBy(x => new { Id = x.Id, OrderGuid = x.OrderGuid, DeliveryType = x.DeliveryType, OrderId = x.OrderId, OrderTime = x.OrderTime, Gid = x.Gid }, y => y))
            {
                if (ico.Key.DeliveryType == (int)DeliveryType.ToShop)
                {
                    #region 憑證 (因憑證狀態不同，以多筆訂單型態送出)

                    foreach (var item in ico)
                    {
                        result += IChannelAPICall(item.Id, config.IChannelMcode, ico.Key.Gid, item.Amount.ToString("F0"), config.IChannelItemToShop, ico.Key.OrderId + "-" + item.CouponId, ico.Key.OrderTime) + "\n";
                    }

                    #endregion
                }
                else if (ico.Key.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    #region 宅配 (因宅配狀態一致，多筆商品金額以"|"合併資料送出)

                    result += IChannelAPICall(ico.Key.Id, config.IChannelMcode, ico.Key.Gid, string.Join("|", ico.Select(x => x.Amount.ToString("F0"))), string.Join("|", ico.Select(x => config.IChannelItemToHouse)), ico.Key.OrderId, ico.Key.OrderTime) + "\n";

                    #endregion
                }
            }

            logger.InfoFormat("\niChannel API Log:\n{0}", result);

            return result;
        }


        #endregion



        #region 全家Fami

        /// <summary>
        /// 根據bid自動匯入Peztemp
        /// </summary>
        /// <param name="batchId">batch_id</param>
        /// <param name="bid">bid</param>
        /// <param name="couponCount">需匯入序號數</param>
        /// <param name="serviceCode">服務代碼</param>
        /// <param name="isBackupCode"></param>
        /// <returns>回傳需匯入序號數是否與異動資料數相等</returns>
        public static bool GeneratePezTempRandomCodes(int batchId, Guid bid, int couponCount, int serviceCode, bool isBackupCode)
        {
            const int batchlimit = 2000;

            try
            {
                var codeCount = 0;
                var remainder = couponCount % batchlimit;

                do
                {
                    var generateCount = (codeCount + batchlimit) > couponCount ? remainder : batchlimit;
                    pp.PeztempSetWithVerification(batchId, bid, serviceCode, generateCount, isBackupCode);
                    codeCount += generateCount;
                } while (codeCount < couponCount);

                if (!int.Equals(couponCount, codeCount))
                {
                    logger.ErrorFormat("Generate PezTemp RandomCodes Error!! <br /> bid:{0}, batchId:{1}, coupon_count:{2}", bid, batchId, couponCount);
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Generate PezTemp RandomCodes Error!! <br /> bid:{0}, message:{1}, exception:{2}", bid, ex.Message, ex.StackTrace);
                return false;
            }

            return true;
        }

        public static Peztemp PeztempCheck(string pezCode, int serviceCode)
        {
            DateTime now = DateTime.Now;
            return pp.PeztempGetWithDate(pezCode, serviceCode, now);
        }

        public static void SetPeztempStatusExpiredWithDeliverTimeEndForFami(int service_code)
        {
            pp.SetPeztempStatusExpiredWithDeliverTimeEndForFami(service_code);
        }

        public static int FamiImportSequenceNumber(int id)
        {
            //解壓縮後放置到的文件夾
            string unzip_folder = "UnZip";
            //只取今日的壓縮檔(依檔名)
            string date_mark = DateTime.Now.ToString("_Sequecne_yyyyMMdd");
            //Fami上傳壓縮檔的路徑
            string target = config.FamiDailyPath;
            //解壓縮後的檔案放置路徑
            string result_target = Path.Combine(config.TemporaryStoragePath, Path.Combine(unzip_folder, date_mark));
            Dictionary<string, string> filetypes = new Dictionary<string, string>();
            filetypes["seq"] = ".txt.zip";
            filetypes["txt"] = ".txt";
            Famiport fami = pp.FamiportGet(id);
            if (!fami.IsImportSequencenumber)
            {
                Famiport default_fami = pp.FamiportGet(Famiport.Columns.BusinessHourGuid, Guid.Empty);
                //處理壓縮檔
                if (Directory.Exists(target))
                {
                    foreach (var file in Directory.GetFiles(target, fami.EventId + "*" + filetypes["seq"]))
                    {
                        FileInfo info = new FileInfo(file);
                        if (info.Name.Length >= 12)
                        {
                            try
                            {
                                using (ZipFile zip = new ZipFile(info.FullName))
                                {
                                    foreach (var item in zip)
                                    {
                                        item.ExtractWithPassword(result_target, ExtractExistingFileAction.OverwriteSilently, default_fami.Password + fami.Password);
                                    }
                                }
                            }
                            catch (Exception error)
                            {
                                MailMessage mail = new MailMessage();
                                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                                mail.To.Add(config.AdminEmail);
                                mail.Subject = "Fami Daily Import Error";
                                mail.Body = error.Message;
                                mail.IsBodyHtml = true;
                                PostMan.Instance().Send(mail, SendPriorityType.Immediate);
                            }
                        }
                    }
                }
                //塞入序號
                int i = 0;
                int pez_total = pp.PeztempGetCount(fami.BusinessHourGuid, false);
                if (pez_total == 0)
                {
                    if (Directory.Exists(result_target))
                    {
                        foreach (var file in Directory.GetFiles(result_target, fami.EventId + "*" + filetypes["txt"]))
                        {
                            using (StreamReader reader = new StreamReader(file, Encoding.GetEncoding(950)))
                            {
                                string line;
                                while ((line = reader.ReadLine()) != null)
                                {
                                    if (line.Length == 10)
                                    {
                                        Peztemp pez = new Peztemp();
                                        pez.Bid = fami.BusinessHourGuid;
                                        pez.PezCode = line;
                                        pp.PeztempSet(pez);
                                        i++;
                                    }
                                }
                            }
                        }
                        fami.IsImportSequencenumber = true;
                        pp.FamiportSet(fami);
                        return i;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }

        public static int FamiUpdatePrintDate(DateTime d)
        {
            //20130620 單一主機執行此Method時異常[美國隊長單一主機job問題(無Exception回傳0)]，新增文字Log協助判斷
            ILog famiLogger = LogManager.GetLogger("FamiUpdatePrintDate");
            //解壓縮後放置到的文件夾
            string unzip_folder = "UnZip";
            //檔案種類 壓縮檔和記事本
            Dictionary<string, string> filetypes = new Dictionary<string, string>();
            filetypes["zip"] = ".zip";
            filetypes["txt"] = ".txt";
            //Fami上傳壓縮檔的路徑
            string target = config.FamiDailyPath;
            //只取今日的壓縮檔(依檔名)
            string date_mark = d.ToString("_S_yyyyMMdd");
            //解壓縮後的檔案放置路徑
            string result_target = Path.Combine(config.TemporaryStoragePath, Path.Combine(unzip_folder, date_mark));
            string error_message = string.Empty;
            //解壓縮執行次數
            int zipExtractCount = 0;
            //處理壓縮檔
            famiLogger.Info("Update Start. FamiDailyPath:" + target + ", result_target:" + result_target);
            if (!Directory.Exists(target))
            {
                famiLogger.Info("FamiDailyPath(" + target + ") does not Exists.");
            }
            else
            {
                string[] FilesPath = Directory.GetFiles(target, "*" + date_mark + filetypes["zip"]);
                foreach (var file in FilesPath)
                {
                    FileInfo info = new FileInfo(file);
                    string event_id = info.Name.Replace(date_mark + filetypes["zip"], string.Empty);
                    Famiport fami = pp.FamiportGet(Famiport.Columns.EventId, event_id);
                    if (fami.IsLoaded && fami.Id != 0)
                    {
                        try
                        {
                            using (ZipFile zip = new ZipFile(info.FullName))
                            {
                                foreach (var item in zip)
                                {
                                    item.ExtractWithPassword(result_target, ExtractExistingFileAction.OverwriteSilently, fami.Password);
                                }
                            }
                            fami.LatestUpdateTime = d;
                            pp.FamiportSet(fami);
                            zipExtractCount++;
                        }
                        catch (Exception error)
                        {
                            error_message += string.Format("Bid: {0} , {1}。", fami.BusinessHourGuid, error.Message);
                        }
                    }
                }
                famiLogger.Info("Zip Files Count:" + FilesPath.Length + ", Zip Extract Count:" + zipExtractCount);
                if (!string.IsNullOrEmpty(error_message))
                {
                    famiLogger.Info(error_message);
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                    mail.To.Add(config.AdminEmail);
                    mail.Subject = "Fami Daily Update Error";
                    mail.Body = error_message;
                    mail.IsBodyHtml = true;
                    PostMan.Instance().Send(mail, SendPriorityType.Immediate);
                }
            }
            //更新列印時間
            int i = 0;
            if (!Directory.Exists(result_target))
            {
                famiLogger.Info("result_target(" + result_target + ") does not Exists.");
            }
            else
            {
                string[] famiExtractFiles = Directory.GetFiles(result_target, "*" + date_mark + filetypes["txt"]);
                foreach (var file in famiExtractFiles)
                {
                    try
                    {
                        using (StreamReader reader = new StreamReader(file, Encoding.GetEncoding(950)))
                        {
                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                if (line.Length == 60)
                                {
                                    DateTime print_time;
                                    string code = line.Substring(0, 10);
                                    string print = line.Substring(10, 14);
                                    int cvsid = PponFacade.CvsIdGet(line.Substring(24, 6));
                                    if (cvsid == 0)
                                    {
                                        PponFacade.PeztempErrorLogInsert(code, line.Substring(24, 6), 0);
                                    }
                                    else if (DateTime.TryParseExact(print, "yyyyMMddHHmmss", new CultureInfo("zh-TW", true), DateTimeStyles.None, out print_time))
                                    {
                                        if (PponFacade.PeztempValidator(code))
                                        {
                                            PponFacade.PeztempUpdatePrintTime(code, print_time, cvsid);
                                            i++;
                                        }
                                        else
                                        {
                                            PponFacade.PeztempErrorLogInsert(code, line.Substring(24, 6), 1);
                                        }

                                    }
                                }
                                else
                                {
                                    famiLogger.Info("Line does not meet the length. Length:" + line.Length + ", Full Path:" + file);
                                }
                            }
                        }
                    }
                    catch (Exception exceptionMessage)
                    {
                        famiLogger.Info("StackTrace:" + exceptionMessage.StackTrace + " Message:" + exceptionMessage.Message);
                    }

                }
                famiLogger.Info("famiExtractFilesCount:" + famiExtractFiles.Length + ", ExcuteLineCount:" + i);
            }
            famiLogger.Info("Update End.");
            return i;
        }

        public static Famiport FamiportGet(Guid bid)
        {
            return pp.FamiportGet(Famiport.Columns.BusinessHourGuid, bid);
        }

        public static void SendFamiDealDeliverTimeAlert(int dayNumber)
        {
            ViewFamiDealDeliverTimeCollection famiDealDeliverTimeCol = pp.GetViewFamiDealDeliverTimeByBeforeDay(dayNumber);
            List<int> uniqueIdList = famiDealDeliverTimeCol.Select(x => x.UserId).Distinct().ToList();

            foreach (int uniqueId in uniqueIdList)
            {
                Member m = mp.MemberGetbyUniqueId(uniqueId);
                if (!m.IsLoaded)
                {
                    continue;
                }

                StringBuilder dealTag = new StringBuilder();
                var memberFamiDeals = famiDealDeliverTimeCol.Where(x => x.UserId == m.UniqueId).OrderByDescending(x => x.CouponCodeType);

                foreach (ViewFamiDealDeliverTime famiDeal in memberFamiDeals)
                {
                    dealTag.Append(MakeFamiDealTag(famiDeal.PezCode, famiDeal.CouponUsage, (CouponCodeType)famiDeal.CouponCodeType, famiDeal.CouponId));
                }

                SendFamiDealDeliverTimeAlertMail(m.UserEmail, System.DateTime.Now.AddDays((double)dayNumber).ToString("yyyy/MM/dd"), m.DisplayName, dealTag.ToString());
                dealTag.Clear();
            }
        }

        private static void SendFamiDealDeliverTimeAlertMail(string mailto, string deliverTime, string memberName, string dealTag)
        {
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                mail.To.Add(mailto);
                mail.Subject = string.Format(@"好康兌換到期通知(全家商品將於{0}到期，請儘速兌換)", deliverTime);

                FamiDealDeliverTimeAlert template = TemplateFactory.Instance().GetTemplate<FamiDealDeliverTimeAlert>();
                template.MemberName = memberName;
                template.DeliverTime = deliverTime;
                template.DealTag = dealTag;

                mail.Body = template.ToString();
                mail.IsBodyHtml = true;
                PostMan.Instance().Send(mail, SendPriorityType.Immediate);
            }
        }

        private static string MakeFamiDealTag(string pezCode, string couponUsage, CouponCodeType type, int couponId)
        {
            if (type == CouponCodeType.Pincode)
            {
                return string.Format(@"
                                       <tr>
                                         <td width='10' style='border-bottom:solid 1px #FFE066;'></td>
                                         <td align='left' valign='top' style='border-bottom:solid 1px #FFE066; padding-top:5px; padding-bottom:5px;'>{0}</td>
                                         <td width='10' style='border-bottom:solid 1px #FFE066; border-right:solid 1px #FFE066;'></td>
                                         <td align='center' style='border-bottom:solid 1px #FFE066;'>{1}</td>
                                       </tr>", couponUsage, pezCode);
            }
            else
            {
                return string.Format(@"<tr>
                                         <td width='10' style='border-bottom:solid 1px #FFE066;'></td>
                                         <td align='left' valign='top' style='border-bottom:solid 1px #FFE066; padding-top:5px; padding-bottom:5px;'>{0}</td>
                                         <td width='10' style='border-bottom:solid 1px #FFE066; border-right:solid 1px #FFE066;'></td>
                                         <td align='center' style='border-bottom:solid 1px #FFE066;'> 
                                             <a href='https://www.17life.com/User/FamiCouponDetail.aspx?cid={1}' style='color:#08C;'>請至會員專區索取條碼</a>
                                         </td>
                                      </tr>", couponUsage, couponId);
            }
        }

        public static int SetFamiportPeztempLink(FamiportPeztempLink fpl, bool isVerified = false)
        {
            if (!isVerified)
            {
                return pp.FamiportPeztempLinkSet(fpl);
            }

            Peztemp peztemp = pp.PeztempGet(fpl.PeztempId);
            VerifyPeztempCashTrustLog(peztemp.Bid ?? Guid.Empty, peztemp.PezCode, PeztempServiceCode.Famiport, fpl.MmkId);
            fpl.UpdatePayTime = DateTime.Now;
            return pp.FamiportPeztempLinkSet(fpl);
        }

        private static bool VerifyPeztempCashTrustLog(Guid bid, string pezCode, PeztempServiceCode serviceCode, string createId)
        {
            CashTrustLog log = mp.CashTrustLogGet(bid, pezCode);
            if (log.IsLoaded)
            {
                CashTrustStatusLog ctsl = new CashTrustStatusLog();
                ctsl.TrustId = log.TrustId;
                ctsl.TrustProvider = log.TrustProvider;
                ctsl.Amount = log.Scash + log.CreditCard + log.Atm + log.Tcash;
                ctsl.CreateId = string.Format("{0}_{1}", Helper.GetEnumDescription(serviceCode), createId);

                log.VerifiedStoreGuid = log.StoreGuid;
                log.UsageVerifiedTime = log.ModifyTime = DateTime.Now;
                ctsl.ModifyTime = DateTime.Now;
                ctsl.Status = log.Status = (int)TrustStatus.Verified;

                if (mp.CashTrustLogSet(log))
                {
                    mp.CashTrustStatusLogSet(ctsl);
                }
                return true;
            }
            return false;
        }

        public static FamiportPeztempLink GetFamiportPeztempLink(string tranNo)
        {
            return pp.FamiportPeztempLinkGetByTranNo(tranNo);
        }

        public static FamiportPeztempLink GetFamiportPeztempLink(int peztempId)
        {
            return pp.FamiportPeztempLinkGetByPeztempId(peztempId);
        }

        public static bool FamiposBarcodeSet(FamiposBarcode fami, bool isVerified = false)
        {
            if (!isVerified)
            {
                return pp.FamiposBarcodeSet(fami);
            }

            var peztemp = pp.PeztempGet(fami.PeztempId);
            var log = mp.CashTrustLogGet((Guid)peztemp.Bid, peztemp.PezCode);
            if (log.IsLoaded)
            {
                var ctsl = new CashTrustStatusLog
                {
                    TrustId = log.TrustId,
                    TrustProvider = log.TrustProvider,
                    Amount = log.Scash + log.CreditCard + log.Atm,
                    CreateId = string.Format("{0}_{1}", PeztempServiceCode.Famiport.ToString(), fami.MmkId)
                };

                log.VerifiedStoreGuid = log.StoreGuid;
                log.UsageVerifiedTime = log.ModifyTime = DateTime.Now;
                ctsl.ModifyTime = DateTime.Now;
                ctsl.Status = log.Status = (int)TrustStatus.Verified;

                if (mp.CashTrustLogSet(log))
                {
                    mp.CashTrustStatusLogSet(ctsl);
                }

                //Update Peztemp
                peztemp.IsUsed = true;
                peztemp.Remark = fami.Id.ToString();
                peztemp.UsedTime = DateTime.Now;
                SetPezTemp(peztemp);
            }
            return pp.FamiposBarcodeSet(fami);
        }

        public static FamiposBarcode FamiposBarcodeGet(string tranNo)
        {
            return pp.FamiposBarcodeGet(tranNo);
        }

        public static FamiposBarcode FamiposBarcodeGet(int peztempId)
        {
            return pp.FamiposBarcodeGet(peztempId);
        }

        /// <summary>
        /// 取得或建立一筆全家條碼
        /// </summary>
        /// <param name="peztempId"></param>
        /// <param name="vpd"></param>
        /// <returns></returns>
        public static FamiposBarcode FamiposBarcodeGetCreate(Guid orderDetailGuid, Guid businessHourGuid, IViewPponDeal vpd)
        {
            string tranNoHeader = System.DateTime.Now.ToString("yy")
                + FamiposTranNoHeaderComparison(System.DateTime.Now.Month) + FamiposTranNoHeaderComparison(System.DateTime.Now.Day);
            return pp.FamiposBarcodeGetCreate(tranNoHeader, orderDetailGuid, businessHourGuid, vpd);
        }

        public static Peztemp PeztempGet(int peztempId)
        {
            return pp.PeztempGet(peztempId);
        }

        public static int PeztempGetUsingMemberUserId(Peztemp pez)
        {
            return pp.PeztempGetUsingMemberUserId(pez);
        }

        public static Famiport FamiportGet(int famiportId)
        {
            return pp.FamiportGet(Famiport.Columns.Id, famiportId);
        }

        public static string FamiposTranNoHeaderComparison(int c)
        {
            if (c < 0)
            {
                return string.Empty;
            }

            if (c < 10)
            {
                return c.ToString();
            }
            else
            {
                if (c > 31)
                {
                    return string.Empty;
                }
                string[] table = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V" };
                return table[c - 10];
            }
        }

        public static void FamiApiLogSet(FamiApiLog apiLog)
        {
            IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
            var famiApiLogMq = new FamiApiLogMq
            {
                FamiApiLog = apiLog
            };
            mqp.Send(FamiApiLogMq._QUEUE_NAME, famiApiLogMq);
        }

        #endregion

        #region 訂單商品名稱

        public static string GetOrderDetailItemName(OrderDetailItemModel orderDetail)
        {
            return GetOrderDetailItemName(orderDetail, new PponItemNameShowOption { ShowPhone = false });
        }

        public static string GetOrderDetailItemName(OrderDetailItemModel orderDetail, PponItemNameShowOption showOption)
        {
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(orderDetail.BusinessHourGuid, true);
            var vpsCol = pp.ViewPponStoreGetListByBidWithNoLock(orderDetail.BusinessHourGuid)
                                                .Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.VerifyShop));
            ViewPponStore store = null;
            if (orderDetail.StoreGuid != null && vpsCol.Any())
            {
                store = vpsCol.FirstOrDefault(t => t.StoreGuid == orderDetail.StoreGuid.Value);
                if (store == null && (vpd.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0)
                {
                    store = vpsCol.First();
                }
            }
            return GetOrderDetailItemName(orderDetail, vpd.ComboPackCount.GetValueOrDefault() > 1, (DeliveryType)vpd.DeliveryType.Value,
                                          showOption, vpsCol.Count() > 1, store, vpd.ItemPrice,
                                          Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.TmallDeal) && !Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore));
        }

        private static string GetOrderDetailItemName(OrderDetailItemModel orderDetail, bool isComboPack, DeliveryType deliveryType,
            PponItemNameShowOption showOption, bool notOnlyOneStore, ViewPponStore store, decimal itemPrice, bool is_store_shown)
        {
            string result = string.Empty;
            if (deliveryType == DeliveryType.ToShop)
            {
                //針對憑證檔顯示分店名稱
                if (store != null && (itemPrice != 0 || is_store_shown))
                {
                    //result = orderDetail.ItemName + showOption.ToString(store, notOnlyOneStore);
                    result = showOption.GetItemName(orderDetail.ItemName) + showOption.GetStoreInfo(store, notOnlyOneStore);
                }
                else
                {
                    result = showOption.GetItemName(orderDetail.ItemName);
                }
            }
            else if (isComboPack)
            {
                OrderDetailItemCollection odiCol =
                    op.OrderDetailItemCollectionGetByOrderDetailGuid(orderDetail.OrderDetailGuid);
                if (odiCol.Count > 0)
                {
                    string optName;
                    if (odiCol.Count == 1)
                    {
                        optName = odiCol.First().Options;
                    }
                    else
                    {
                        optName = odiCol.GroupBy(x => x.Options).Select(x => x.Key + "*" + x.Count())
                                        .Aggregate((first, next) => first + "," + next);
                    }
                    if (string.IsNullOrEmpty(optName))
                    {
                        if (showOption.HideItemName)
                        {
                            return string.Empty;
                        }
                        else
                        {
                            result = odiCol.First().ItemName;
                        }
                    }
                    else
                    {
                        if (showOption.HideItemName)
                        {
                            return optName;
                        }
                        else
                        {
                            result = string.Format("{0} {1}", odiCol.First().ItemName, optName);
                        }
                    }
                }
                else
                {
                    //早期OrderDetail沒有OrderDetailitem
                    result = orderDetail.ItemName.Replace("&nbsp", " ");
                }
            }
            else
            {
                int pos = orderDetail.ItemName.IndexOf("&nbsp");
                if (showOption.HideItemName && pos != -1)
                {
                    result = orderDetail.ItemName.Substring(pos + 5); //&nbsp has 5 chars
                }
                else if (showOption.HideItemName && pos == -1)
                {
                    result = string.Empty;
                }
                else if (showOption.HideItemName == false && pos != -1)
                {
                    result = orderDetail.ItemName.Replace("&nbsp", " ");
                }
                else if (showOption.HideItemName == false && pos == -1)
                {
                    result = orderDetail.ItemName;
                }
            }
            return result;
        }
        #endregion

        /// <summary>
        /// 產生折價券通知信
        /// </summary>
        /// <param name="memberName"></param>
        /// <param name="eventName">活動名稱</param>
        /// <param name="startDate"></param>
        /// <param name="endDate">折價券使用截止日期</param>
        /// <param name="discount"></param>
        /// <param name="minimumAmount"></param>
        /// <param name="promoInfo"></param>
        /// <returns></returns>
        public static string GetDiscountCodeSendMailTemplate(string memberName, string eventName, DiscountCode discount, DateTime startDate, DateTime endDate, int minimumAmount, string promoInfo)
        {
            DiscountCodeSendMail template = TemplateFactory.Instance().GetTemplate<DiscountCodeSendMail>();
            template.ServerConfig = config;
            template.MemberName = !string.IsNullOrWhiteSpace(memberName) ? memberName : I18N.Phrase.Member;
            template.CityDeal = GetRandomDeal(PponCityGroup.DefaultPponCityGroup.Travel.CityId, config.PromoMailDealsCount, Convert.ToInt32(config.PromoMailDealsCount * config.PromoMailDealsProportion));
            template.EventName = eventName;
            template.Amount = discount.Amount.Value.ToString("N0");
            template.Code = discount.Code;
            template.StartDate = startDate.ToString("yyyy/MM/dd HH:mm");
            template.EndDate = endDate.AddSeconds(-1).ToString("yyyy/MM/dd HH:mm");
            template.DiscountLimit = !int.Equals(minimumAmount, 0) ? string.Format(I18N.Message.DiscountCodeSendMailTemplateDiscountLimit, minimumAmount, discount.Amount.Value.ToString("N0")) : string.Empty;
            if (string.IsNullOrWhiteSpace(promoInfo))
            {
                template.PromoInfo = "<a href='" + config.SiteUrl + "/ppon/default.aspx' style='color: #F60;'>點此前往17Life逛逛，火速折抵</a>";
            }
            else
            {
                template.PromoInfo = promoInfo;
            }
            return template.ToString();
        }

        public static ViewPponOrder ViewPponOrderGetByOrderGuid(Guid orderGuid)
        {
            return pp.ViewPponOrderGet(orderGuid);
        }

        #region 資策會IDEAS 通知信部分
        public static void SendIDEASMail(IDEASMailContentType mailType, Order order, CashTrustLogCollection ctCol)
        {
            if (order == null || !order.IsLoaded || !order.ParentOrderId.HasValue)
            {
                return;
            }
            CompanyUserOrder companyUserOrder = op.CompanyUserOrderGet(order.Guid);
            //查無統一帳號類型訂單的資料表示不應該呼叫此函式，直接跳出
            if (!companyUserOrder.IsLoaded)
            {
                return;
            }

            ViewPponDeal deal = pp.ViewPponDealGet(order.ParentOrderId.Value);
            ViewPponOrderDetailCollection odc = pp.ViewPponOrderDetailGetList(ViewPponOrderDetail.Columns.OrderGuid, order.Guid, ViewPponOrderDetail.Columns.OrderDetailCreateTime);
            IEnumerable<ViewPponOrderDetail> regularEntries = from i in odc
                                                              where (i.OrderDetailStatus & (int)OrderDetailStatus.SystemEntry) == 0
                                                              select i;

            if (odc.Count > 0)
            {
                foreach (var item in odc)
                {
                    item.MemberName = companyUserOrder.PurchaserName;
                }
            }
            ViewPponOrderDetailCollection odcR = new ViewPponOrderDetailCollection();
            foreach (var od in regularEntries)
            {
                odcR.Add(od);
            }
            MailMessage mail = GetIDEASOrderMailSubjectAndBody(mailType, order, deal, odcR, ctCol);
            try
            {
                mail.From = new MailAddress(config.ServiceEmail, "【雙北行動購物牆】" + config.ServiceName);
                mail.To.Add(companyUserOrder.PurchaserEmail);
                mail.IsBodyHtml = true;
                PostMan.Instance().Send(mail, SendPriorityType.Immediate);
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Something wrong while trying to send SendPponMail: Mail Address={0}, {1}, ex={2}",
                    string.Join(",", mail.To.Select(t => t.Address)), Helper.Object2String(mail, "\n"), e);
            }
        }

        public static MailMessage GetIDEASOrderMailSubjectAndBody(IDEASMailContentType mailType, Order order, ViewPponDeal deal, ViewPponOrderDetailCollection odcR, CashTrustLogCollection ctCol)
        {
            MailMessage mail = new MailMessage();
            switch (mailType)
            {
                case IDEASMailContentType.PaymentCompleted: //付款成功
                    mail.Subject = string.Format("付款成功通知({0})", deal.ItemName);
                    mail.Body = IDEASPaymentSuccessfulConfirmMsg(deal, odcR, order.Guid);
                    break;
                case IDEASMailContentType.ReturnAll: //退貨完成
                    mail.Subject = "退貨訂單處理通知";
                    mail.Body = IDEASReturnAllEmail(deal, odcR, order, ctCol);
                    break;
            }
            return mail;
        }

        private static string IDEASPaymentSuccessfulConfirmMsg(ViewPponDeal deal, ViewPponOrderDetailCollection orderDetailCol, Guid orderGuid)
        {
            IdeasPaymentSuccessful t1 = TemplateFactory.Instance().GetTemplate<IdeasPaymentSuccessful>();
            t1.TheDeal = deal;
            t1.TheOrderDetailCol = orderDetailCol;
            t1.TheCashTrustLogCol = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            t1.ServerConfig = config;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < orderDetailCol.Count; i++)
            {
                var od = orderDetailCol[i];
                if (i > 0)
                {
                    sb.Append("<br />");
                }
                sb.Append(GetOrderDetailItemName(OrderDetailItemModel.Create(od)));
                if (od.ItemQuantity > 1)
                {
                    sb.Append(" * ");
                    sb.Append(od.ItemQuantity);
                }
            }
            t1.ItemName = sb.ToString();
            return t1.ToString();
        }
        private static string IDEASReturnAllEmail(ViewPponDeal deal, ViewPponOrderDetailCollection orderDetailCol, Order order, CashTrustLogCollection ctCol)
        {
            IDEASPponReturnAllMail t1 = TemplateFactory.Instance().GetTemplate<IDEASPponReturnAllMail>();
            t1.TheDeal = deal;
            t1.TheOrderDetailCol = orderDetailCol;
            t1.TheCashTrustLogCol = ctCol;
            t1.Order = order;
            t1.ServerConfig = config;
            return t1.ToString();
        }
        #endregion 資策會IDEAS 通知信部分

        public static bool SetOrderStatus(Guid orderGuid, int userId, OrderStatus status, bool setIt, SalesInfoArg sia = null)
        {
            string userName = MemberFacade.GetUserName(userId);
            return SetOrderStatus(orderGuid, userName, status, setIt, sia);
        }

        public static bool SetOrderStatus(Guid orderGuid, string userName, OrderStatus status, bool setIt, Guid bid)
        {
            return SetOrderStatus(orderGuid, userName, status, setIt, new SalesInfoArg
            {
                BusinessHourGuid = bid
            });
        }

        public static bool SetOrderStatus(Guid orderGuid, string userName, OrderStatus status, bool setIt, SalesInfoArg sia = null)
        {
            userName = MemberFacade.GetShortUserName(userName);
            bool result = op.OrderSetStatus(orderGuid, userName, status, setIt);
            //讓成套票券檔次能計算憑證數
            if (sia != null)
            {
                DealProperty dp = pp.DealPropertyGet(sia.BusinessHourGuid);
                sia.Quantity = sia.Quantity * ((dp.SaleMultipleBase ?? 0) > 0 ? dp.SaleMultipleBase.Value : 1);
            }
            if (result && (status == OrderStatus.Complete) && setIt && sia != null)
            {
                PayEvents.OnOrdered(sia.BusinessHourGuid, sia.Quantity, sia.Total, orderGuid);
            }
            else if (result && status == OrderStatus.ATMOrder && setIt && sia != null)
            {
                PayEvents.OnAtmOrderCreated(sia.BusinessHourGuid, sia.Quantity, sia.Total, orderGuid);
            }
            else if (result && (status == OrderStatus.FamilyIspOrder || status == OrderStatus.SevenIspOrder) && setIt && sia != null)
            {
                PayEvents.OnIspOrderCreated(sia.BusinessHourGuid, sia.Quantity, sia.Total, orderGuid);
            }
            else if (result && status == OrderStatus.Cancel && setIt)
            {
                Guid bid = Guid.Empty;
                if (sia == null)
                {
                    Order o = op.OrderGet(orderGuid);
                    if (o.IsLoaded && o.ParentOrderId != null)
                    {
                        bid = op.GroupOrderGetByOrderGuid(o.ParentOrderId.Value).BusinessHourGuid;
                    }
                }
                else
                {
                    bid = sia.BusinessHourGuid;
                }

                //主要想觀察Atm退款完成訂單 更新deal_sales_info過程是否有異常 之後若觀察無異常可拔除
                CommonFacade.AddAudit(bid, AuditType.DealSalesInfo, "準備執行PayEvents.OnRefund程序", "sys", false);

                if (bid != Guid.Empty)
                {
                    PayEvents.OnRefund(bid);
                }

                CommonFacade.AddAudit(bid, AuditType.DealSalesInfo, "PayEvents.OnRefund程序執行結束", "sys", false);
            }
            return result;
        }

        #region 退貨相關

        public static string GetRefundType(ReturnFormEntity form)
        {
            if (form != null)
            {
                switch (form.RefundType)
                {
                    case RefundType.Scash:
                        return (form.IsSysRefund != null && form.IsSysRefund.Value) ? "[自動退購物金]" : "[退購物金]";
                    case RefundType.Cash:
                    case RefundType.ScashToCash:
                        return "[刷退]";
                    case RefundType.Atm:
                    case RefundType.ScashToAtm:
                        return "[退ATM]";
                    case RefundType.Tcash:
                    case RefundType.ScashToTcash:
                        return string.Format("[退{0}]", Helper.GetEnumDescription(form.ThirdPartyPaymentSystem));
                    default:
                        return "資料有問題, 請聯絡技術部";
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetRefundStatus(ReturnFormEntity form)
        {
            if (form != null)
            {
                switch (form.ProgressStatus)
                {
                    case ProgressStatus.Processing:
                    case ProgressStatus.AtmQueueing:
                    case ProgressStatus.AtmQueueSucceeded:
                        return "退貨處理中";
                    case ProgressStatus.AtmFailed:
                        return "退貨處理中-ATM匯款失敗";
                    case ProgressStatus.Completed:
                    case ProgressStatus.CompletedWithCreditCardQueued:
                        return "退貨已完成";
                    case ProgressStatus.Unreturnable:
                        return "退貨失敗";
                    case ProgressStatus.Canceled:
                        return "取消退貨";
                    case ProgressStatus.Retrieving:
                        return "已前往收件";
                    case ProgressStatus.ConfirmedForCS:
                        return "客服待處理";
                    case ProgressStatus.RetrieveToPC:
                        return "前往PC倉庫收件";
                    case ProgressStatus.RetrieveToCustomer:
                        return "前往消費者處收件";
                    case ProgressStatus.ConfirmedForVendor:
                        return "廠商待確認";
                    case ProgressStatus.ConfirmedForUnArrival:
                        WmsRefundOrder order = wp.WmsRefundOrderGet(form.Id);
                        return "客服待處理" + "(已前往" + Helper.GetEnumDescription((WmsRefundFrom)order.RefundFrom) + "收件)";
                    default:
                        return "資料有問題, 請聯絡技術部";
                }
            }

            return string.Empty;
        }

        public static ChannelProgressStatus ChannelProgressStatus(ReturnFormEntity form)
        {
            ChannelProgressStatus result = Core.ChannelProgressStatus.Unknown;
            if (form == null)
            {
                return result;
            }

            switch (form.ProgressStatus)
            {
                case ProgressStatus.Processing:
                case ProgressStatus.AtmQueueing:
                case ProgressStatus.AtmQueueSucceeded:
                case ProgressStatus.AtmFailed:
                    result = Core.ChannelProgressStatus.Processing;
                    break;
                case ProgressStatus.Completed:
                case ProgressStatus.CompletedWithCreditCardQueued:
                    result = Core.ChannelProgressStatus.Completed;
                    break;
                case ProgressStatus.Unreturnable:
                    result = Core.ChannelProgressStatus.Unreturnable;
                    break;
                case ProgressStatus.Canceled:
                    result = Core.ChannelProgressStatus.Canceled;
                    break;

            }
            return result;
        }

        /// <summary>
        /// 可否申請退貨(前台)
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="userName"></param>
        /// <param name="allowGeneralRefund"></param>
        /// <param name="cannotReason"></param>
        /// <returns></returns>
        public static bool CanApplyRefund(Guid orderGuid, string userName, bool allowGeneralRefund, out string cannotReason)
        {
            IList<ReturnFormEntity> returnForms = ReturnFormRepository.FindAllByOrder(orderGuid);
            CashTrustLogCollection ctlc = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);

            if (ctlc.All(x => x.Status == (int)TrustStatus.Verified)) //全部已使用
            {
                cannotReason = "憑證已使用完畢，無法退貨";
                return false;
            }
            else if (returnForms.Count > 0 && returnForms.All(x => x.ProgressStatus == ProgressStatus.Completed && x.RefundType != RefundType.Scash))
            {
                cannotReason = "已全數退貨完畢，無法再申請";
                return false;
            }
            else if (returnForms.Where(x => x.ProgressStatus == ProgressStatus.Processing).Any())
            {
                cannotReason = "已有正在處理中的退貨";
                return false;
            }
            else if (fami.GetFamilyNetListPincode(orderGuid).Where(p => p.IsLock == true).Any())//全家成套票券,若有任一pincode lock則不給退
            {
                cannotReason = "目前部分商品使用狀態資料處理中，待資料處理完畢後再重新按下退貨申請。";
                return false;
            }
            else if (ctlc.Where(x => x.Status < (int)TrustStatus.Verified && x.ReturnedTime == null).Any())
            {
                //判斷訂單狀態處於 已使用 或 退貨狀態 才會去判斷是否走退購物金流程
                cannotReason = string.Empty;
                return allowGeneralRefund;
            }
            else if (ctlc.Where(x => x.Status == (int)TrustStatus.Returned).Count() > 0) //含有退購物金的訂單
            {
                foreach (ReturnFormEntity entity in returnForms)
                {
                    //檢查購物金餘額是否充足
                    ReturnFormEntity form = ReturnFormRepository.FindById(entity.Id);
                    RefundedAmount refundedAmount = form.GetRefundedSCashToCashAmount();
                    if (GetSCashSum(userName) < refundedAmount.Atm + refundedAmount.CreditCard + refundedAmount.TCash + refundedAmount.IspPay)
                    {
                        cannotReason = "此筆訂單已轉購物金，您目前購物金餘額不足，無法轉刷退或帳戶退款。";
                        return false;
                    }
                }
                cannotReason = "此筆訂單已轉購物金，是否轉申請刷退或帳戶退款？";
                return true;
            }
            else
            {
                //是否走一般退貨
                cannotReason = string.Empty;
                return allowGeneralRefund;
            }
        }
        #endregion

        #region 折價券活動相關

        /// <summary>
        /// 依照傳入名單匯入discount_user
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="users"></param>
        /// <param name="username"></param>
        public static void ImportDiscountUsers(int eventId, List<int> users, string username)
        {
            if (users.Count > 0)
            {
                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    // step1. 匯入所有名單
                    SetDiscountUsersByDataTable(eventId, users, username);

                    // step2. 刪除原本未發送的名單當中，沒有出現在新上傳的名單者
                    op.DiscountUserDeleteOld(eventId);

                    // step3. 刪除新上傳的名單，重複出現於原本的名單當中者
                    op.DiscountUserDeleteDuplicate(eventId);

                    // step4. 更新狀態為未發送
                    op.DiscountUserUpdateStatus(eventId);

                    scope.Complete();
                }

                var discountEvent = op.DiscountEventGet(eventId);
                if (discountEvent.IsLoaded)
                {
                    discountEvent.Qty = users.Count;
                    op.DiscountEventSet(discountEvent);
                }
            }
        }

        /// <summary>
        /// 匯入全會員名單
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="users"></param>
        /// <param name="username"></param>
        public static void ImportDiscountUserForAllMembers(int eventId, string username)
        {
            op.DiscountUserDeleteList(eventId);

            int affectedCount = op.DiscountUserSetAllMembers(eventId, username);
            var discountEvent = op.DiscountEventGet(eventId);
            if (discountEvent.IsLoaded)
            {
                discountEvent.Qty = affectedCount;
                op.DiscountEventSet(discountEvent);
            }
        }

        /// <summary>
        /// 匯入所有折價券活動名單
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="users"></param>
        /// <param name="username"></param>
        private static void SetDiscountUsersByDataTable(int eventId, List<int> users, string username)
        {
            DiscountUserCollection discountUsers = new DiscountUserCollection();
            foreach (int user in users)
            {
                if (user != 0)
                {
                    DiscountUser item = new DiscountUser();
                    item.UserId = user;
                    item.EventId = eventId;
                    item.Status = (int)DiscountUserStatus.None;
                    item.CreateId = username;
                    item.CreateTime = DateTime.Now;
                    discountUsers.Add(item);
                }
            }
            op.DiscountUserCollectionBulkInsert(discountUsers);


        }

        /// <summary>
        /// 設定折價券主題活動對應關係
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="campaigns"></param>
        public static void SetDiscountEventCampaignList(int eventId, Dictionary<int, int> campaigns)
        {
            foreach (KeyValuePair<int, int> item in campaigns)
            {
                op.DiscountEventCampaignSet(new DiscountEventCampaign()
                {
                    EventId = eventId,
                    CampaignId = item.Key,
                    Qty = item.Value
                });
            }
        }

        /// <summary>
        /// 取得在活動中並未領取的折價券清單
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static List<ViewDiscountUser> GetUnsentDiscountUserList(int userId)
        {
            return op.ViewDiscountUserGetList(userId, DateTime.Now)
                .Where(x => x.DiscountUserStatus == (int)DiscountUserStatus.Initial).ToList();
        }

        public static decimal GetUnsentDiscountUserListSum(int userId)
        {
            return GetUnsentDiscountUserList(userId).Sum(t => t.Amount * t.Qty).GetValueOrDefault();
        }

        /// <summary>
        /// 領取可發送的折價券
        /// </summary>
        /// <param name="userId"></param>
        public static List<string> SendUncollectedDiscountList(int userId, string userName)
        {
            List<string> discountList = new List<string>();
            if (config.DiscountUserCollectType == DiscountUserCollectType.Manually ||
                config.DiscountUserCollectType == DiscountUserCollectType.Auto)
            {
                // 取得在活動中尚未領取的折價券清單
                bool isFull = true;
                ViewDiscountUserCollection discounts = op.ViewDiscountUserGetList(userId, DateTime.Now);
                foreach (var discountEvent in discounts.GroupBy(x => x.EventId))
                {
                    InstantGenerateDiscountCampaignResult result = InstantGenerateDiscountCampaignResult.NotExist;
                    foreach (var campaign in discountEvent)
                    {
                        if (Helper.IsFlagSet(campaign.Flag, DiscountCampaignUsedFlags.InstantGenerate))
                        {
                            int camQty = campaign.Qty;
                            DiscountCodeCollection sendDiscount;
                            if (campaign.DiscountUserStatus == (int)DiscountUserStatus.Shortage)
                            {
                                // 檢查上次發送不足者，進行補發
                                sendDiscount = op.DiscountCodeGetListByOwner(campaign.CampaignId, userId);
                                camQty = campaign.Qty - sendDiscount.Count();
                            }

                            for (int i = 0; i < camQty; i++)
                            {
                                // 即時產折價券
                                string code;
                                int amount, discountCodeId, minimumAmount;
                                DateTime? endTime;
                                result = PromotionFacade.InstantGenerateDiscountCode(campaign.CampaignId, userId, false, out code, out amount, out endTime, out discountCodeId, out minimumAmount);
                                if (result == InstantGenerateDiscountCampaignResult.Success)
                                {
                                    discountList.Add(code);
                                }
                            }

                            // 紀錄是否有發送不足的資料，若有需註記為發送不足，以供下次進行補發
                            sendDiscount = op.DiscountCodeGetListByOwner(campaign.CampaignId, userId);
                            if (campaign.Qty > sendDiscount.Count)
                            {
                                isFull = false;
                            }
                        }
                    }

                    // 檢查並更新折價券名單發送狀態
                    op.DiscountUserUpdateStatus(discountEvent.Key, userId, isFull ? DiscountUserStatus.Sent : DiscountUserStatus.Shortage, userName);
                }
            }
            return discountList;
        }



        #endregion

        #region 訂單狀態 / 憑證狀態

        /// <summary>
        /// 取得付款完成日，未付款則回傳 NULL
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static DateTime? GetPaymentCompletedDate(Guid orderGuid)
        {
            var od = op.OrderGet(orderGuid);

            if (!Helper.IsFlagSet(od.OrderStatus, (int)OrderStatus.Complete))
            {
                return null;
            }

            if (Helper.IsFlagSet(od.OrderStatus, (int)OrderStatus.ATMOrder))
            {
                CtAtm atm = op.CtAtmGet(orderGuid);
                return atm.PayTime;
            }

            //超取超付付款時間等於領取時間
            if (Helper.IsFlagSet(od.OrderStatus, OrderStatus.FamilyIspOrder) ||
                Helper.IsFlagSet(od.OrderStatus, OrderStatus.SevenIspOrder))
            {
                var os = op.OrderShipGet(orderGuid);
                return (os.IsReceipt ?? false) ? os.ReceiptTime : null;
            }

            return od.CreateTime;
        }

        /// <summary>
        /// 訂單資訊
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static CouponListMainInfo GetCouponListMainStatus(ViewCouponListMain main)
        {
            var mainInfo = new CouponListMainInfo();
            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.None; //未登入查詢信件裡的分類
            mainInfo.MainOrderStatus = string.Empty;
            mainInfo.MainCouponStatus = string.Empty;
            mainInfo.IsDownloadCouponPdf = true;
            mainInfo.IsPayAtm = false;
            mainInfo.IsPpon = (!main.DeliveryType.HasValue || int.Equals((int)DeliveryType.ToShop, main.DeliveryType.Value));
            mainInfo.IsZeroPponDeal = decimal.Equals(0, main.ItemPrice);
            mainInfo.IsShowPayAtm = false;
            mainInfo.IsShowOrderStatus = true;
            mainInfo.IsShowBookingBtn2 = false;
            mainInfo.IsSetBooking = false;
            mainInfo.IsEnableBooking = false;

            var iscancel = false; //訂單已取消
            var isPartialCancel = (main.TotalCount > GetReturnedQuantity(main.Guid)); //部分退貨
            var isExchangeOrder = (main.ExchangeStatus == (int)OrderLogStatus.ExchangeCancel ||
                                   main.ExchangeStatus == (int)OrderLogStatus.ExchangeFailure ||
                                   main.ExchangeStatus == (int)OrderLogStatus.ExchangeProcessing ||
                                   main.ExchangeStatus == (int)OrderLogStatus.ExchangeSuccess ||
                                   main.ExchangeStatus == (int)OrderLogStatus.SendToSeller); ////換貨訂單
            var isReturnOrder = (main.ReturnStatus != (int)ProgressStatus.Canceled && main.ReturnStatus != -1); //退貨訂單
            var now = DateTime.Now;

            if (!mainInfo.IsPpon)
            {
                mainInfo.MainCouponStatus = "宅配商品";
            }

            #region 退貨完成

            if ((main.OrderStatus & ((int)OrderStatus.Cancel)) > 0) //退貨完成
            {
                iscancel = true;
                mainInfo.IsDownloadCouponPdf = false;
                mainInfo.IsPayAtm = false;

                if (isPartialCancel)
                {
                    mainInfo.MainOrderStatus = "部份退貨";
                    mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                }
                else
                {
                    mainInfo.MainOrderStatus = "訂單已取消";
                    mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Refund;
                }

                if (mainInfo.IsPpon && main.TotalCount != 0)
                {
                    if (isPartialCancel)
                    {
                        mainInfo.MainCouponStatus = string.Empty;

                        if (main.RemainCount > 0)
                        {
                            mainInfo.MainCouponStatus = string.Format("未使用:{0}", main.RemainCount);
                            mainInfo.IsDownloadCouponPdf = true;

                            mainInfo.IsShowBookingBtn = IsEnableBookingSystem(main.Guid, (BookingType)main.BookingSystemType);
                            mainInfo.IsShowBookingBtn2 = mainInfo.IsShowBookingBtn;

                            if (IsPastFinalExpireDate(main))
                            {
                                mainInfo.MainCouponStatus = "憑證已過期";
                                mainInfo.IsDownloadCouponPdf = false;
                                mainInfo.IsSetBooking = true;
                            }
                            else
                            {
                                mainInfo.IsEnableBooking = true;
                            }
                        }
                    }
                }
            }

            #endregion

            #region 退換貨訂單

            else if (isExchangeOrder || isReturnOrder) //退or換貨訂單
            {
                mainInfo.IsDownloadCouponPdf = false;
                mainInfo.IsPayAtm = false;
                mainInfo.IsShowPayAtm = false;
                iscancel = isReturnOrder;

                var returnModifyTime = main.ReturnModifyTime ?? DateTime.MinValue;
                var exchangeModifyTime = main.ExchangeModifyTime ?? DateTime.MinValue;

                //依最新一筆退換貨單紀錄之異動時間 抓取最新一筆 顯示訂單退換貨狀態
                if (exchangeModifyTime > returnModifyTime)
                {

                    #region 換貨部分

                    switch (main.ExchangeStatus)
                    {
                        case (int)OrderLogStatus.SendToSeller:
                        case (int)OrderLogStatus.ExchangeProcessing:
                            mainInfo.MainOrderStatus = "換貨處理中";
                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                            break;
                        case (int)OrderLogStatus.ExchangeSuccess:
                            mainInfo.MainOrderStatus = "換貨完成";
                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                            break;
                        case (int)OrderLogStatus.ExchangeFailure:
                            mainInfo.MainOrderStatus = "換貨失敗";
                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                            break;
                        case (int)OrderLogStatus.ExchangeCancel:
                            mainInfo.MainOrderStatus = "換貨取消";
                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                            break;
                    }

                    #endregion 換貨部分

                }
                else
                {

                    #region 退貨部分

                    switch (main.ReturnStatus)
                    {
                        case (int)ProgressStatus.Processing:
                        case (int)ProgressStatus.AtmQueueing:
                        case (int)ProgressStatus.AtmQueueSucceeded:
                        case (int)ProgressStatus.AtmFailed:
                            mainInfo.MainOrderStatus = "退貨處理中";
                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                            break;

                        #region Completed

                        case (int)ProgressStatus.Completed:
                        case (int)ProgressStatus.CompletedWithCreditCardQueued:
                            mainInfo.MainOrderStatus = "訂單已取消";
                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Refund;
                            if (mainInfo.IsPpon)
                            {
                                if (main.TotalCount != 0)
                                {
                                    if (isPartialCancel)
                                    {
                                        mainInfo.MainCouponStatus = string.Empty;
                                        if (main.RemainCount > 0)
                                        {
                                            mainInfo.MainCouponStatus = string.Format("未使用:{0}", main.RemainCount);
                                            mainInfo.IsDownloadCouponPdf = true;
                                            if (IsPastFinalExpireDate(main))
                                            {
                                                mainInfo.MainCouponStatus = "憑證已過期";
                                                mainInfo.IsDownloadCouponPdf = false;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (isPartialCancel)
                                {
                                    mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Refund;
                                    mainInfo.MainOrderStatus = "部分退貨<br/>";
                                    if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                                    {
                                        mainInfo.MainOrderStatus += " 已過鑑賞期";
                                        mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                                    }
                                    else
                                    {
                                        //訂單狀態顯示出貨資訊
                                        //檢查是否已出貨
                                        if (PponOrderManager.CheckIsShipped(main.Guid, OrderClassification.LkSite))
                                        {
                                            mainInfo.MainOrderStatus += " 已出貨";
                                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                                        }
                                        else
                                        {
                                            if (main.BusinessHourDeliverTimeE != null)
                                            {
                                                mainInfo.MainOrderStatus += " 最後出貨日<br/>" + main.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd");
                                                mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                                            }
                                        }
                                    }
                                }
                            }
                            break;

                        #endregion Completed

                        #region Unreturnable

                        case (int)ProgressStatus.Unreturnable:
                            mainInfo.MainOrderStatus = "退貨失敗";
                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                            mainInfo.IsShowBookingBtn = IsEnableBookingSystem(main.Guid, (BookingType)main.BookingSystemType);
                            mainInfo.IsShowBookingBtn2 = mainInfo.IsShowBookingBtn;
                            if (IsPastFinalExpireDate(main))
                            {
                                mainInfo.IsSetBooking = true;
                            }
                            else
                            {
                                mainInfo.IsEnableBooking = true;
                            }
                            break;

                            #endregion Unreturnable
                    }

                    #endregion 退貨部分

                }
            }

            #endregion

            #region 已結檔

            else if (main.BusinessHourOrderTimeE <= now) //已結檔
            {
                int slug;
                if (int.TryParse(main.Slug, out slug) && slug < main.BusinessHourOrderMinimum)
                {
                    mainInfo.MainOrderStatus = "未達門檻";
                    mainInfo.IsDownloadCouponPdf = false;
                    mainInfo.IsPayAtm = false;
                }
                else
                {
                    if (mainInfo.IsPpon)
                    {
                        if (main.TotalCount != 0)
                        {
                            mainInfo.IsDownloadCouponPdf = (main.RemainCount > 0);
                            mainInfo.IsShowBookingBtn = IsEnableBookingSystem(main.Guid, (BookingType)main.BookingSystemType);
                            mainInfo.IsShowBookingBtn2 = mainInfo.IsShowBookingBtn;
                            if (main.RemainCount > 0)
                            {
                                mainInfo.MainCouponStatus = "未使用:" + main.RemainCount;
                                if (IsPastFinalExpireDate(main))
                                {
                                    mainInfo.MainCouponStatus = "憑證已過期";
                                    mainInfo.IsDownloadCouponPdf = false;
                                    mainInfo.IsSetBooking = true;
                                }
                                else
                                {
                                    mainInfo.IsEnableBooking = true;
                                }
                            }
                            else
                            {
                                mainInfo.MainCouponStatus = "使用完畢";
                                mainInfo.IsSetBooking = true;
                            }
                        }
                        else if (IsPastFinalExpireDate(main))
                        {
                            mainInfo.MainCouponStatus = "憑證已過期";
                            mainInfo.IsDownloadCouponPdf = false;
                        }
                    }
                    else
                    {
                        mainInfo.IsDownloadCouponPdf = false;
                        if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                        {
                            mainInfo.MainOrderStatus = "已過鑑賞期";
                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                        }
                        else
                        {
                            //訂單狀態顯示出貨資訊
                            //檢查是否已出貨
                            if (PponOrderManager.CheckIsShipped(main.Guid, OrderClassification.LkSite))
                            {
                                mainInfo.MainOrderStatus = "已出貨";
                                mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                            }
                            else
                            {
                                if (main.BusinessHourDeliverTimeE != null)
                                {
                                    mainInfo.MainOrderStatus = "最後出貨日<br/>" + main.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd");
                                    mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                                }
                            }
                        }
                    }
                    if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 &&
                        (main.OrderStatus & (int)Core.OrderStatus.Complete) == 0) //ATM未付款，已達門檻不顯示退款
                    {
                        if (mainInfo.IsPpon)
                        {
                            mainInfo.MainCouponStatus = string.Empty;
                        }
                        mainInfo.IsDownloadCouponPdf = false;
                        if (main.CreateTime.Year == now.Year && main.CreateTime.Month == now.Month &&
                            main.CreateTime.Day == now.Day)
                        {
                            mainInfo.IsPayAtm = true;
                            mainInfo.IsShowPayAtm = true;
                        }
                    }
                }
            }

            #endregion

            #region 搶購中

            else //尚在搶購中
            {
                if (mainInfo.IsPpon)
                {
                    if (main.TotalCount != 0)
                    {
                        mainInfo.IsDownloadCouponPdf = (main.RemainCount > 0);
                        mainInfo.IsShowBookingBtn = IsEnableBookingSystem(main.Guid, (BookingType)main.BookingSystemType);
                        mainInfo.IsShowBookingBtn2 = mainInfo.IsShowBookingBtn;
                        if (main.RemainCount > 0)
                        {
                            mainInfo.MainCouponStatus = string.Format("未使用:{0}", main.RemainCount);
                            if (IsPastFinalExpireDate(main))
                            {
                                mainInfo.MainCouponStatus = "憑證已過期";
                                mainInfo.IsDownloadCouponPdf = false;
                                mainInfo.IsSetBooking = true;
                            }
                            else
                            {
                                mainInfo.IsEnableBooking = true;
                            }
                        }
                        else
                        {
                            mainInfo.MainCouponStatus = "使用完畢";
                            mainInfo.IsSetBooking = true;
                        }
                    }

                    //ATM
                    if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 &&
                        (main.OrderStatus & (int)Core.OrderStatus.Complete) == 0)
                    {
                        mainInfo.IsPayAtm = true;
                        mainInfo.MainCouponStatus = string.Empty;
                        mainInfo.IsDownloadCouponPdf = false;
                        if ((main.CreateTime.Year == now.Year && main.CreateTime.Month == now.Month &&
                             main.CreateTime.Day == now.Day))
                        {
                            mainInfo.IsShowPayAtm = true;
                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.NotUsed;
                        }
                        else
                        {
                            mainInfo.IsShowPayAtm = false;
                            mainInfo.MainOrderStatus = "逾期未付款";
                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.None;
                        }
                    }
                }
                else
                {
                    mainInfo.IsDownloadCouponPdf = false;
                    if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                    {
                        mainInfo.MainOrderStatus = "已過鑑賞期";
                        mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                    }
                    else
                    {
                        //訂單狀態顯示出貨資訊
                        //檢查是否已出貨                                
                        if (PponOrderManager.CheckIsShipped(main.Guid, OrderClassification.LkSite))
                        {
                            mainInfo.MainOrderStatus = "已出貨";
                            mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                        }
                        else
                        {
                            if (main.BusinessHourDeliverTimeE != null)
                            {
                                mainInfo.MainOrderStatus = "最後出貨日<br/>" + main.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd");
                                mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                            }
                        }

                        //ATM
                        if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 &&
                            (main.OrderStatus & (int)Core.OrderStatus.Complete) == 0)
                        {
                            mainInfo.IsPayAtm = true;

                            if (main.CreateTime.Year == now.Year && main.CreateTime.Month == now.Month &&
                                main.CreateTime.Day == now.Day)
                            {
                                mainInfo.IsShowPayAtm = true;
                                mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.NotUsed;
                                mainInfo.IsShowOrderStatus = false; //ATM未付款無須顯示出貨資訊
                            }
                            else
                            {
                                mainInfo.IsShowPayAtm = false;
                                mainInfo.MainOrderStatus = "逾期未付款";
                                mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.None;
                            }
                        }
                    }
                }
            }

            #endregion

            #region 退貨條件顯示

            //不接受退貨
            if ((main.Status & (int)GroupOrderStatus.NoRefund) > 0)
            {
                mainInfo.MainOrderStatus = I18N.Phrase.NoRefund;
                mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
            }

            //結檔後七天不接受退貨
            if ((main.Status & (int)GroupOrderStatus.DaysNoRefund) > 0 &&
                main.BusinessHourOrderTimeE.AddDays(config.ProductRefundDays) < DateTime.Now)
            {
                mainInfo.MainOrderStatus = I18N.Phrase.NoRefund;
                mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
            }

            //過期不接受退貨
            if ((main.Status & (int)GroupOrderStatus.ExpireNoRefund) > 0)
            {
                if (main.BusinessHourDeliverTimeE != null && main.BusinessHourDeliverTimeE.Value.AddDays(1) < now)
                {
                    if (!iscancel)
                    {
                        mainInfo.MainOrderStatus = I18N.Phrase.NoRefund;
                        mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
                    }
                }
            }

            //演出時間前十日過後不能退貨 -conf.PponNoRefundBeforeDays
            if ((main.Status & (int)GroupOrderStatus.NoRefundBeforeDays) > 0 &&
                now.Date >= ((DateTime)main.BusinessHourDeliverTimeS).AddDays(-config.PponNoRefundBeforeDays).Date)
            {
                mainInfo.MainOrderStatus = I18N.Phrase.NoRefund;
                mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
            }

            #endregion

            #region 0元好康

            if (mainInfo.IsZeroPponDeal) //0元好康
            {
                mainInfo.MainCouponStatus = string.Empty;
                if ((main.Status & (int)GroupOrderStatus.PEZevent) > 0 ||
                    (main.Status & (int)GroupOrderStatus.FamiDeal) > 0)
                {
                    if (main.PinType == (int)PinType.Single)
                    {
                        //如果只有一組序號則不顯示使用狀態
                        mainInfo.MainCouponStatus = (IsPastFinalExpireDate(main)) ? "已過期" : "可使用";
                        mainInfo.IsDownloadCouponPdf = !(IsPastFinalExpireDate(main));
                    }
                    else if (main.PrintCount == 0)
                    {
                        if (IsPastFinalExpireDate(main))
                        {
                            mainInfo.MainCouponStatus = "已過期";
                            mainInfo.IsDownloadCouponPdf = false;
                        }
                        else
                        {
                            mainInfo.MainCouponStatus = (main.CouponCodeType == (int)CouponCodeType.Pincode) ? "未使用" : "可使用";
                            mainInfo.IsDownloadCouponPdf = true;
                        }
                    }
                    else
                    {
                        mainInfo.MainCouponStatus = "使用完畢";
                        mainInfo.IsDownloadCouponPdf = false;
                    }
                }
            }

            #endregion

            #region 公益檔

            if ((main.Status & (int)GroupOrderStatus.KindDeal) > 0) //公益檔
            {
                mainInfo.MainCouponStatus = "公益檔次";
                mainInfo.MainOrderStatus = string.Empty;
                mainInfo.MainOrderStatusType = (int)MailCouponListStatusType.Used;
            }

            #endregion

            return mainInfo;
        }

        /// <summary>
        /// 憑證明細資訊
        /// </summary>
        /// <param name="main"></param>
        /// <param name="coupon"></param>
        /// <returns></returns>
        public static CouponListDetailInfo GetCouponListDetailStatus(ViewCouponListMain main, ViewCouponListSequence coupon)
        {
            if (main == null)
            {
                main = mp.GetCouponListMainByOid(coupon.Guid);
            }

            var cDetailInfo = new CouponListDetailInfo();
            cDetailInfo.Status = string.Empty;
            cDetailInfo.StatusType = (int)MailCouponListStatusType.None; //未登入查詢信件裡的分類
            cDetailInfo.IsEnabledDownLoad = false;
            cDetailInfo.IsEnabledPrint = false;
            cDetailInfo.IsEnabledEmail = false;
            cDetailInfo.IsEnabledSMS = false;

            var now = DateTime.Now;
            var slug = int.Parse(main.Slug);
            var currentQuantity = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(main.BusinessHourGuid).OrderedQuantity ?? 0;

            if (coupon.Status < 0)
            {
                #region no trust data

                if ((main.OrderStatus & (int)Core.OrderStatus.Cancel) > 0)
                {
                    cDetailInfo.Status = "已退貨";
                    cDetailInfo.StatusType = (int)MailCouponListStatusType.Refund;
                }
                else if (IsPastFinalExpireDate(coupon))
                {
                    cDetailInfo.Status = "憑證過期";
                    cDetailInfo.StatusType = (int)MailCouponListStatusType.Expired;
                }
                else if (main.BusinessHourOrderTimeE < now)
                {
                    if (slug > main.BusinessHourOrderMinimum)
                    {
                        cDetailInfo.Status = "未使用";
                        cDetailInfo.StatusType = (int)MailCouponListStatusType.NotUsed;
                        cDetailInfo.IsEnabledDownLoad = true;
                        cDetailInfo.IsEnabledPrint = true;
                        cDetailInfo.IsEnabledEmail = true;
                        cDetailInfo.IsEnabledSMS = true;
                    }
                    else
                    {
                        cDetailInfo.Status = "未達門檻";
                        cDetailInfo.StatusType = (int)MailCouponListStatusType.None;
                    }
                }
                else
                {
                    if (main.BusinessHourOrderTimeE > now && currentQuantity < main.BusinessHourOrderMinimum && currentQuantity != 0)
                    {
                        cDetailInfo.IsEnabledDownLoad = false;
                        cDetailInfo.IsEnabledPrint = false;
                        cDetailInfo.IsEnabledEmail = false;
                        cDetailInfo.IsEnabledSMS = false;
                    }
                    else
                    {
                        cDetailInfo.Status = "未使用";
                        cDetailInfo.StatusType = (int)MailCouponListStatusType.NotUsed;
                        cDetailInfo.IsEnabledDownLoad = true;
                        cDetailInfo.IsEnabledPrint = true;
                        cDetailInfo.IsEnabledEmail = true;
                        cDetailInfo.IsEnabledSMS = true;
                    }
                }

                #endregion
            }
            else
            {
                var cdInfo = GetCouponStatus(coupon.Id, main.Guid);
                cDetailInfo.Status = cdInfo.Status;
                cDetailInfo.StatusType = cdInfo.StatusType;

                if (coupon.Status < 2)
                {
                    #region unused

                    if (cDetailInfo.Status == "未使用")
                    {
                        if (main.BusinessHourOrderTimeE < now)
                        {
                            if (slug >= main.BusinessHourOrderMinimum)
                            {
                                cDetailInfo.IsEnabledDownLoad = true;
                                cDetailInfo.IsEnabledPrint = true;
                                cDetailInfo.IsEnabledEmail = true;
                                cDetailInfo.IsEnabledSMS = true;
                            }
                        }
                        else
                        {
                            if (currentQuantity >= main.BusinessHourOrderMinimum && currentQuantity != 0)
                            {
                                cDetailInfo.IsEnabledDownLoad = true;
                                cDetailInfo.IsEnabledPrint = true;
                                cDetailInfo.IsEnabledEmail = true;
                                cDetailInfo.IsEnabledSMS = true;
                            }
                        }
                    }

                    #endregion unused
                }
            }

            #region pez events

            if ((main.Status & (int)GroupOrderStatus.PEZevent) > 0)
            {
                if ((main.Status & (int)GroupOrderStatus.PEZeventCouponDownload) > 0) //&& chyp_rpCouponSequenceDownLoad.Enabled
                {
                    cDetailInfo.IsEnabledDownLoad = true;
                    cDetailInfo.IsEnabledPrint = true;
                    cDetailInfo.IsEnabledEmail = true;
                    cDetailInfo.IsEnabledSMS = true;
                }
                //else
                //{
                //    cDetailInfo.IsEnabledDownLoad = false;
                //    cDetailInfo.IsEnabledPrint = false;
                //    cDetailInfo.IsEnabledEmail = false;
                //    cDetailInfo.IsEnabledSMS = false;
                //}
            }

            #endregion pez events

            #region $0 events

            if (main.ItemPrice == 0)
            {
                var isSkm = PponFacade.DealPropertyCityListGetByGuid(main.BusinessHourGuid).Contains(PponCityGroup.DefaultPponCityGroup.Skm.CityId);

                if ((main.Status & (int)GroupOrderStatus.PEZeventCouponDownload) > 0 || isSkm)
                {
                    cDetailInfo.IsEnabledDownLoad = true;
                    cDetailInfo.IsEnabledPrint = true;
                    cDetailInfo.IsEnabledEmail = true;
                    cDetailInfo.IsEnabledSMS = false;
                }
                else
                {
                    cDetailInfo.IsEnabledDownLoad = false;
                    cDetailInfo.IsEnabledPrint = false;
                    cDetailInfo.IsEnabledEmail = false;
                    cDetailInfo.IsEnabledSMS = false;
                }

                if ((main.Status & (int)GroupOrderStatus.PEZevent) > 0)
                {
                    if (coupon.IsUsed.HasValue)
                    {
                        cDetailInfo.Status = coupon.IsUsed.Value ? "已列印" : "未使用";
                    }
                }
            }

            #endregion $0 events

            #region forced download

            if (coupon.Available != null && coupon.Available.Value)
            {// ClearDetailText(clab_rpUseCount);
                cDetailInfo.IsEnabledDownLoad = true;
                cDetailInfo.IsEnabledPrint = true;
                cDetailInfo.IsEnabledEmail = true;
                cDetailInfo.IsEnabledSMS = true;
            }

            #endregion forced download

            #region sms unable

            if ((main.Status & (int)GroupOrderStatus.DisableSMS) > 0)
            {
                cDetailInfo.IsEnabledSMS = false;
            }

            #endregion sms unable


            return cDetailInfo;
        }

        private static int GetReturnedQuantity(Guid orderGuid)
        {
            var ctCol = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            return
                ctCol.Count(
                    x =>
                        (x.Status == (int)TrustStatus.Returned || x.Status == (int)TrustStatus.Refunded) &&
                        !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight));
        }

        /// <summary>
        /// 判斷憑證是否過期. 過期日會考慮到店家倒店以及變更使用期限的情況.
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        private static bool IsPastFinalExpireDate(ViewCouponListMain main)
        {
            if (main == null)
            {
                return false;
            }

            if (main.Department > 3)
            {
                return false;
            }

            ExpirationDateSelector selector = GetExpirationComponent(main.Guid);

            if (selector == null)
            {
                return false;
            }

            return selector.IsPastExpirationDate(DateTime.Now);
        }

        private static bool IsPastFinalExpireDate(ViewCouponListSequence view)
        {
            ExpirationDateSelector selector = GetExpirationComponent(view.Guid);

            if (selector == null)
            {
                return false;
            }

            return selector.IsPastExpirationDate(DateTime.Now);
        }

        private static ExpirationDateSelector GetExpirationComponent(Guid orderGuid)
        {
            var expirationComponents = new Dictionary<Guid, ExpirationDateSelector>();
            if (expirationComponents.ContainsKey(orderGuid))
            {
                return expirationComponents[orderGuid];
            }
            var selector = new ExpirationDateSelector { ExpirationDates = new PponUsageExpiration(orderGuid) };
            expirationComponents.Add(orderGuid, selector);
            return selector;
        }

        private static CouponListDetailInfo GetCouponStatus(int couponId, Guid orderGuid) //View.OrderGuid
        {
            var ctlogs = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            var ctlog = ctlogs.FirstOrDefault(x => x.CouponId == couponId);

            if (ctlog != null)
            {
                if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                {
                    return new CouponListDetailInfo { Status = "ATM退款中", StatusType = (int)MailCouponListStatusType.Used };
                }

                switch ((TrustStatus)ctlog.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        ExpirationDateSelector selector = GetExpirationComponent(orderGuid);
                        if (GetExpirationComponent(orderGuid) != null && selector.IsPastExpirationDate(DateTime.Now))
                        {
                            return new CouponListDetailInfo { Status = "憑證過期", StatusType = (int)MailCouponListStatusType.Expired };
                        }
                        else
                        {
                            return new CouponListDetailInfo { Status = "未使用", StatusType = (int)MailCouponListStatusType.NotUsed };
                        }

                    case TrustStatus.Verified:
                        return new CouponListDetailInfo { Status = "已使用", StatusType = (int)MailCouponListStatusType.Used };

                    case TrustStatus.Returned:
                    case TrustStatus.Refunded:
                        return new CouponListDetailInfo { Status = "已退貨", StatusType = (int)MailCouponListStatusType.Refund };

                    default:
                        return new CouponListDetailInfo { Status = "-", StatusType = (int)MailCouponListStatusType.None };
                }
            }
            return new CouponListDetailInfo { Status = string.Empty, StatusType = (int)MailCouponListStatusType.None };
        }

        /// <summary>
        /// 取得訂單所屬分店有設訂位系統且該檔次有配合訂位服務
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="bookingType"></param>
        /// <returns></returns>
        private static bool IsEnableBookingSystem(Guid orderGuid, BookingType bookingType)
        {
            ViewPponOrderDetail vpod = op.ViewPponOrderDetailGetListByOrderGuid(orderGuid).FirstOrDefault(x => x.OrderDetailStatus == (int)OrderDetailStatus.None);
            if (vpod.StoreGuid != null)
            {
                var dealProperty = pp.DealPropertyGetByOrderGuid(orderGuid);
                if (BookingSystemFacade.GetBookingSystemStoreSettingByStoreGuid((Guid)vpod.StoreGuid, bookingType) && dealProperty.BookingSystemType > 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 期間內排除國定假日及周末假日
        /// </summary>
        /// <param name="recentHolidays"></param>
        /// <param name="dateTimeStart"></param>
        /// <param name="dateTimeEnd"></param>
        /// <returns></returns>
        public static DateTime ExcludeHolidaysByPeriod(VacationCollection recentHolidays, DateTime dateTimeStart, DateTime dateTimeEnd)
        {
            DateTime tmpDateTimeEnd = dateTimeEnd;
            var inclue = recentHolidays.Where(x => x.Holiday >= dateTimeStart && x.Holiday <= tmpDateTimeEnd && !x.Holiday.DayOfWeek.Equals(System.DayOfWeek.Saturday) && !x.Holiday.DayOfWeek.Equals(System.DayOfWeek.Sunday)).ToList();

            // 排除國定假日
            dateTimeEnd = dateTimeEnd.AddDays(recentHolidays.Where(x => x.Holiday >= dateTimeStart && x.Holiday <= dateTimeEnd &&
                                                                    !x.Holiday.DayOfWeek.Equals(System.DayOfWeek.Saturday) &&
                                                                    !x.Holiday.DayOfWeek.Equals(System.DayOfWeek.Sunday)).Count());

            if ((dateTimeEnd - dateTimeStart).Days > 0)
            {
                for (int i = 0; i < (dateTimeEnd - dateTimeStart).Days; i++)
                {
                    // 排除週六、周日
                    if (dateTimeStart.AddDays(i + 1).DayOfWeek.Equals(System.DayOfWeek.Saturday))
                    {
                        dateTimeEnd = dateTimeEnd.AddDays(1);
                    }

                    if (dateTimeStart.AddDays(i + 1).DayOfWeek.Equals(System.DayOfWeek.Sunday))
                    {
                        dateTimeEnd = dateTimeEnd.AddDays(1);
                    }
                    // 排除國定假日且非假日且一開始尚未排除(假日在上面就排掉了)
                    if (!dateTimeStart.AddDays(i + 1).DayOfWeek.Equals(System.DayOfWeek.Saturday) &&
                        !dateTimeStart.AddDays(i + 1).DayOfWeek.Equals(System.DayOfWeek.Sunday) &&
                        (inclue.Count() == 0 || inclue.Any(x => x.Holiday.Date != dateTimeStart.AddDays(i + 1).Date)) && recentHolidays.Any(x => x.Holiday.Date == dateTimeStart.AddDays(i + 1).Date))
                    {
                        dateTimeEnd = dateTimeEnd.AddDays(1);
                    }
                }
            }
            else
            {
                //剛好同一天
                int i = 0;
                while (dateTimeStart.AddDays(i).DayOfWeek.Equals(System.DayOfWeek.Saturday) ||
                       dateTimeStart.AddDays(i).DayOfWeek.Equals(System.DayOfWeek.Sunday) ||
                       (inclue.Count() == 0 || inclue.Any(x => x.Holiday.Date != dateTimeStart.AddDays(i).Date)) && recentHolidays.Any(x => x.Holiday.Date == dateTimeStart.AddDays(i).Date))
                {
                    dateTimeEnd = dateTimeEnd.AddDays(1);
                    i++;
                }
            }

            return dateTimeEnd;
        }

        public static ViewDiscountDetailCollection GetViewDiscountDetailByOrderGuid(Guid orderGuid)
        {
            return op.ViewDiscountDetailByOrderGuid(orderGuid) ?? new ViewDiscountDetailCollection();
        }

        /// <summary>
        /// 是否逾期自動核銷
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static bool IsExpiredVerifiedDeal(ViewCouponListMain main)
        {
            bool flag = false;
            Order order = op.OrderGet(Order.Columns.OrderId, main.OrderId);
            if (order != null && order.IsLoaded)
            {
                CashTrustLogCollection ctls = mp.CashTrustLogListGetByOrderId(order.Guid);
                foreach (CashTrustLog ctl in ctls)
                {
                    if (IsExpiredVerifiedDeal(ctl))
                    {
                        flag = true; ;
                        break;
                    }
                }
            }
            return flag;
        }
        public static bool IsExpiredVerifiedDeal(CashTrustLog ctl)
        {
            if (ctl.BusinessHourGuid != null && ctl.BusinessHourGuid.Value != Guid.Empty)
            {
                DealProperty dp = pp.DealPropertyGet((Guid)ctl.BusinessHourGuid);
                bool is_force_verify = dp.IsPromotionDeal || dp.IsExhibitionDeal;
                if (Helper.IsFlagSet(ctl.Status, TrustStatus.Verified) && 
                                     ctl.UsageVerifyId == "sys" &&
                                     is_force_verify)
                {
                    return true;
                }
            }
            return false;
        }
        public static List<CashTrustLog> GetExpiredVerifiedCouponCtlogs(Guid orderGuid, BusinessModel bizModel, int? classificationValue = null)
        {
            OrderClassification classification = bizModel == BusinessModel.Ppon
                                                     ? OrderClassification.LkSite
                                                     : OrderClassification.HiDeal;
            classification = classificationValue != null ? (OrderClassification)classificationValue : classification;
            CashTrustLogCollection allCtlogs = mp.CashTrustLogGetListByOrderGuid(orderGuid, classification);

            List<CashTrustLog> result = new List<CashTrustLog>();

            //鎖定中的憑證
            //List<int> vpcc = _pp.ViewPponCouponGetListByOrderGuid(orderGuid)
            //            .Where(x => x.IsReservationLock.Value && x.CouponId != null).Select(s => s.CouponId.Value).ToList();

            foreach (CashTrustLog ctlog in allCtlogs)
            {
                if ((ctlog.Status == (int)TrustStatus.Initial ||
                     ctlog.Status == (int)TrustStatus.Trusted))
                {
                    result.Add(ctlog);
                }
            }

            return result;
        }
        #endregion

        public static void UnlockCouponAndSendSMS(Guid oGuid)
        {
            ViewCouponListSequenceCollection vclsc = mp.GetCouponListSequenceListByOid(oGuid, OrderClassification.LkSite);
            if (!vclsc.Where(x => x.Guid == oGuid).Any(x => (x.Status == (int)TrustStatus.Initial || x.Status == (int)TrustStatus.Trusted) && x.CouponStatus != (int)CouponStatus.Locked))
            {
                foreach (ViewCouponListSequence vcls in vclsc.Where(x => x.CouponStatus == (int)CouponStatus.Locked && x.Status < (int)TrustStatus.Verified))
                {
                    int couponid = vcls.Id;
                    Coupon c = pp.CouponGet(couponid);
                    c.Status = (int)CouponStatus.None;
                    pp.CouponSet(c);

                    ViewPponCoupon vpc = pp.ViewPponCouponGet(couponid);
                    Member m = mp.MemberGet(vpc.MemberEmail);

                    SMS sms = new SMS();
                    SmsContent sc = pp.SMSContentGet(vpc.BusinessHourGuid);
                    string[] msgs = sc.Content.Split('|');
                    if (vpc.CouponId != null)
                    {
                        //判斷天貓檔次
                        if ((vpc.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
                        {
                            OrderCorresponding order_corresponding = op.OrderCorrespondingListGetByOrderGuid(vpc.OrderGuid);
                            m.Mobile = order_corresponding.Mobile;
                        }
                        //DateTime finalExpireDate = GetFinalExpireDate(vpc.OrderGuid.ToString());//
                        DateTime finalExpireDate = vpc.CouponChangedExpiredDate != null ? (vpc.BusinessHourDeliverTimeE.Value < vpc.CouponChangedExpiredDate.Value ? vpc.BusinessHourDeliverTimeE.Value : vpc.CouponChangedExpiredDate.Value) : vpc.BusinessHourDeliverTimeE.Value;
                        string branch = OrderFacade.SmsGetPhone(pp.CouponGet(vpc.CouponId.Value).OrderDetailId);    //分店資訊 & 多重選項
                        //因為Sony手機的簡訊, 在碰到 - 8 這樣的字元組合時, 會替換成音符..故把半行的 dash 改換為全形 dash
                        string msg = string.Format(msgs[0] + msgs[1] + branch + msgs[2], c.SequenceNumber.Replace("-", "－"), vpc.CouponCode, vpc.BusinessHourDeliverTimeS.Value.ToString("yyyyMMdd"), finalExpireDate.ToString("yyyyMMdd"), (vpc.CouponStoreSequence != null && vpc.CouponStoreSequence != 0) ? "，序號" + vpc.CouponStoreSequence.Value.ToString("0000") : string.Empty);
                        sms.SendMessage("", msg, "", m.Mobile, m.UserName, SmsType.Ppon, vpc.BusinessHourGuid, vpc.CouponId.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// 判斷成套票券憑證是否要依面額置換憑證編號
        /// </summary>
        /// <param name="ctl"></param>
        /// <param name="status"></param>
        /// <param name="isForce"></param>
        /// <param name="isCouponLock"></param>
        public static bool ChangeCtlCouponNumber(CashTrustLog ctlog, int? status, bool isForce = false, bool isCouponLock = false)
        {
            //CashTrustLog newCtl = new CashTrustLog();
            //int amount = ctlog.Amount;
            //switch (status)
            //{
            //    case (int)TrustStatus.Verified:
            //        if (isForce)
            //        {
            //            newCtl = mp.CashTrustLogGetListByOrderGuid(ctlog.OrderGuid).Where(x => (x.Status == (int)TrustStatus.Refunded || x.Status == (int)TrustStatus.Returned) && x.SpecialStatus == 0 && x.Amount > amount).OrderByDescending(x => x.Amount).FirstOrDefault();
            //        }
            //        break;
            //    case (int)TrustStatus.Returned:
            //    case (int)TrustStatus.Refunded:
            //        if (isForce)
            //        {
            //            newCtl = mp.CashTrustLogGetListByOrderGuid(ctlog.OrderGuid).Where(x => x.Status == (int)TrustStatus.Verified && x.SpecialStatus == 0 && x.Amount < amount).OrderBy(x => x.Amount).FirstOrDefault();
            //        }
            //        break;
            //    default:
            //        break;
            //}

            //todo:有預約訂位先不准退貨處理，之後再看是否要依面額置換
            if (isCouponLock)
            {

            }

            //if (newCtl != null && newCtl.IsLoaded)
            //{
            //    CashTrustLog oldCtl = ctlog;
            //    CashTrustLogCollection newCtlc = new CashTrustLogCollection();
            //    int? newCouponId = newCtl.CouponId;
            //    string newSeqNum = newCtl.CouponSequenceNumber;
            //    Guid? newVerifiedStoreGuid = newCtl.VerifiedStoreGuid;
            //    DateTime? newUsageVerifiedTime = newCtl.UsageVerifiedTime;
            //    string newUsageVerifyId = newCtl.UsageVerifyId;
            //    newCtl.CouponId = oldCtl.CouponId;
            //    newCtl.CouponSequenceNumber = oldCtl.CouponSequenceNumber;
            //    //反核銷憑證資訊都要跟著換
            //    if (status == (int)TrustStatus.Returned || status == (int)TrustStatus.Refunded)
            //    {
            //        newCtl.VerifiedStoreGuid = oldCtl.VerifiedStoreGuid;
            //        newCtl.UsageVerifiedTime = oldCtl.UsageVerifiedTime;
            //        newCtl.UsageVerifyId = oldCtl.UsageVerifyId;
            //        oldCtl.VerifiedStoreGuid = newVerifiedStoreGuid;
            //        oldCtl.UsageVerifiedTime = newUsageVerifiedTime;
            //        oldCtl.UsageVerifyId = newUsageVerifyId;
            //    }
            //    oldCtl.CouponId = newCouponId;
            //    oldCtl.CouponSequenceNumber = newSeqNum;
            //    newCtlc.Add(newCtl);
            //    newCtlc.Add(oldCtl);
            //    mp.CashTrustLogColSetForVerifyCoupons(newCtlc);
            //}
            return true;
        }

        /// <summary>
        /// 紀錄成套票券憑證換號
        /// </summary>
        /// <param name="ctl1"></param>
        /// <param name="ctl2"></param>
        /// <param name="momo"></param>
        /// <returns></returns>
        public static bool SetCashTrustChangeCouponLog(CashTrustLog ctl1, CashTrustLog ctl2, string memo)
        {
            CashTrustCouponChangeLogCollection ctcc = new CashTrustCouponChangeLogCollection();
            CashTrustCouponChangeLog ctc = new CashTrustCouponChangeLog();
            DateTime now = DateTime.Now;

            ctc.TrustId = ctl1.TrustId;
            ctc.OriCouponId = ctl2.CouponId;
            ctc.OriSequenceNumber = ctl2.CouponSequenceNumber;
            ctc.NewCouponId = ctl1.CouponId;
            ctc.NewSequenceNumber = ctl1.CouponSequenceNumber;
            ctc.OrderGuid = ctl1.OrderGuid;
            ctc.Memo = memo;
            ctc.ModifyTime = now;
            ctcc.Add(ctc);

            ctc = new CashTrustCouponChangeLog();
            ctc.TrustId = ctl2.TrustId;
            ctc.OriCouponId = ctl1.CouponId;
            ctc.OriSequenceNumber = ctl1.CouponSequenceNumber;
            ctc.NewCouponId = ctl2.CouponId;
            ctc.NewSequenceNumber = ctl2.CouponSequenceNumber;
            ctc.OrderGuid = ctl2.OrderGuid;
            ctc.Memo = memo;
            ctc.ModifyTime = now;
            ctcc.Add(ctc);

            mp.CashTrustCouponChangeLogCollectionSet(ctcc);
            return true;
        }

        #region 電子票券

        public static string GetOrderCorrespondingTypeName(Guid orderGuid)
        {
            var oc = op.OrderCorrespondingListGetByOrderGuid(orderGuid);
            return !oc.IsLoaded ? string.Empty : Helper.GetEnumDescription((OrderClassification)oc.Type);
        }

        #endregion 電子票券

        #region 信用卡相關

        /// <summary>
        /// 取得信用卡別
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        public static CreditCardType GetCreditCardType(string cardNumber)
        {
            /*
            * (16) Visa    4-
            * (16) Master  51- ~ 55-
            * (16) JCB     3-
            * (15) JCB     1800-, 2131-         
            * (15) AE      34- ~ 37-    
            */

            int num;
            string cardPrefixes;
            if (cardNumber.Length == 16)
            {
                //前1位
                cardPrefixes = cardNumber.Substring(0, 1);
                if (int.TryParse(cardPrefixes, out num))
                {
                    if (num == 3)
                    {
                        return CreditCardType.JCBCard;
                    }
                    if (num == 4)
                    {
                        return CreditCardType.VisaCard;
                    }
                }
                //前2位
                cardPrefixes = cardNumber.Substring(0, 2);
                if (int.TryParse(cardPrefixes, out num))
                {
                    if (num >= 51 && num <= 55)
                    {
                        return CreditCardType.MasterCard;
                    }
                }
            }
            else if (cardNumber.Length == 15)
            {
                //前2位
                cardPrefixes = cardNumber.Substring(0, 2);
                if (int.TryParse(cardPrefixes, out num))
                {
                    if (num >= 34 && num <= 37)
                    {
                        return CreditCardType.AmericanExpress;
                    }
                }
                //前4位
                cardPrefixes = cardNumber.Substring(0, 4);
                if (int.TryParse(cardPrefixes, out num))
                {
                    if (num == 1800 || num == 2131)
                    {
                        return CreditCardType.JCBCard;
                    }
                }
            }
            return CreditCardType.None;
        }

        #endregion

        #region 購物車
        public static PponDeliveryInfo PponDeliveryInfoPut()
        {
            PponDeliveryInfo pponDeliveryInfo = new PponDeliveryInfo();

            return pponDeliveryInfo;
        }
        public static PaymentDTO PaymentDTOPut(PponDeliveryInfo deliveryInfo)
        {
            PaymentDTO paymentDTO = new PaymentDTO();
            paymentDTO.DeliveryInfo = deliveryInfo;
            return paymentDTO;
        }
        public static List<SmallCart> CartGroup(List<CartPaymentDTO> paymentDTO)
        {
            List<SmallCart> smallCarts = new List<SmallCart>();

            List<Guid> bids = paymentDTO.Select(x => x.BusinessHourGuid).ToList();
            List<IViewPponDeal> vpds = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bids);

            List<ShoppingCartFreightsItem> items = sp.ShoppingCartFreightsItemGetByBids(bids).ToList();
            List<ShoppingCartFreight> freights = sp.ShoppingCartFreightsGet(items.Select(x => x.FreightsId).ToList()).ToList();

            foreach (ShoppingCartFreight freight in freights)
            {
                SmallCart subCart = new SmallCart()
                {
                    SellerGuid = freight.SellerGuid,
                    FreightsId = freight.Id,
                    PaymentDTOList = filterPaymentDTO(paymentDTO, freight.Id, items)
                };
                smallCarts.Add(subCart);
            }

            return smallCarts;
        }
        private static List<CartPaymentDTO> filterPaymentDTO(List<CartPaymentDTO> dto, int FreightsId, List<ShoppingCartFreightsItem> items)
        {
            List<Guid> Bids = items
                .Where(x => x.FreightsId == FreightsId)
                .Select(x => x.BusinessHourGuid).ToList();
            return dto.Where(x => Bids.Contains(x.BusinessHourGuid)).ToList();
        }
        #endregion 購物車


        public static void SetBlogReferrerUrl(string origUrl, string shortUrl, int userId, Guid blogId)
        {
            BlogReferrerUrl bru = new BlogReferrerUrl();
            bru.OriUrl = origUrl;
            bru.ShortUrl = shortUrl;
            bru.Type = 0;
            bru.CreateTime = DateTime.Now;
            if (userId != 0)
            {
                Member mem = mp.MemberGetbyUniqueId(userId);
                if (mem.IsLoaded)
                {
                    bru.CreateId = mem.UserName;
                }
            }

            bru.BlogId = blogId;
            op.BlogReferrerUrlSet(bru);
        }

        public static void AddOrderLog(int userId, string dtoString, OrderFromType orderFromType)
        {
            if (HttpContext.Current == null)
            {
                return;
            }

            PaymentDTO paymentDTO = null;
            bool isValid = false;
            try
            {
                paymentDTO = serializer.Deserialize<PaymentDTO>(dtoString);
                paymentDTO.CreditcardNumber = Helper.MaskCreditCard(paymentDTO.CreditcardNumber);
                paymentDTO.CreditcardSecurityCode = Helper.MaskCreditCardSecurityCode(paymentDTO.CreditcardSecurityCode);
                dtoString = serializer.Serialize(paymentDTO);
                isValid = true;
            }
            catch
            {

            }
            OrderLog log = new OrderLog
            {
                CreateTime = DateTime.Now,
                Content = dtoString,
                UserId = userId,
                IsValid = isValid,
                OrderFromType = (int)orderFromType,
                ServerName = Environment.MachineName
            };
            op.OrderLogSet(log);
            HttpContext.Current.Items[LkSiteContextItem._ORDER_LOG_ID] = log.Id;
        }

        public static void UpdateOrderLog(ApiReturnCode code)
        {
            try
            {
                if (HttpContext.Current == null)
                {
                    throw new Exception("UpdateOrderLog fail, HttpContext.Current is null.");
                }
                if (HttpContext.Current.Items[LkSiteContextItem._ORDER_LOG_ID] == null)
                {
                    throw new Exception("UpdateOrderLog fail, no order_log_id was assigned.");
                }
                int orderLogId = (int)HttpContext.Current.Items[LkSiteContextItem._ORDER_LOG_ID];
                OrderLog log = op.OrderLogGet(orderLogId);
                if (log.IsLoaded)
                {
                    log.Result = (int)code;
                    log.ModifyTime = DateTime.Now;
                    op.OrderLogSet(log);
                }
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
            }
        }

        #region 超取相關

        /// <summary>
        /// 是否為超取
        /// </summary>
        /// <param name="productDeliveryType"></param>
        /// <returns></returns>
        public static bool IsShipByIsp(ProductDeliveryType productDeliveryType)
        {
            return productDeliveryType == ProductDeliveryType.FamilyPickup ||
                productDeliveryType == ProductDeliveryType.SevenPickup;
        }

        /// <summary>
        /// 記錄最後取貨資訊
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="di"></param>
        public static void SaveMemberCheckInfo(int uniqueId, DeliveryInfo di)
        {
            var checkInfo = mp.MemberCheckoutInfoGet(uniqueId);
            checkInfo.UserId = uniqueId;

            //全家
            if (di.ProductDeliveryType == ProductDeliveryType.FamilyPickup)
            {
                checkInfo.FamilyStoreId = di.PickupStore.StoreId;
                checkInfo.FamilyStoreName = di.PickupStore.StoreName;
                checkInfo.FamilyStoreTel = di.PickupStore.StoreTel;
                checkInfo.FamilyStoreAddr = di.PickupStore.StoreAddr;
            }
            //Seven
            if (di.ProductDeliveryType == ProductDeliveryType.SevenPickup)
            {
                checkInfo.SevenStoreId = di.PickupStore.StoreId;
                checkInfo.SevenStoreName = di.PickupStore.StoreName;
                checkInfo.SevenStoreTel = di.PickupStore.StoreTel;
                checkInfo.SevenStoreAddr = di.PickupStore.StoreAddr;
            }

            checkInfo.LastProductDeliveryType = (int)di.ProductDeliveryType;
            if (di.DeliveryId > 0)
            {
                checkInfo.DeliveryId = di.DeliveryId;
            }
            checkInfo.CreateTime = DateTime.Now;
            checkInfo.ModifyTime = DateTime.Now;
            mp.MemberCheckoutInfoSet(checkInfo);
        }

        /// <summary>
        /// 取得訂單出貨資訊
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static ShipInfo GetOrderShipInfo(ViewCouponListMain main)
        {
            if (main.DeliveryType != (int)DeliveryType.ToHouse)
            {
                return null;
            }

            var shipCols = op.ViewOrderShipListGetListByOrderGuid(main.Guid).OrderByDescending(x => x.ShipTime);
            ViewOrderShipList orderShip = shipCols.FirstOrDefault();
            if (orderShip == null || orderShip.ShipTime == null || orderShip.ShipTime >= DateTime.Now.Date.AddDays(1))
            {
                orderShip = null;
            }

            ShipInfo shipInfo = new ShipInfo();
            shipInfo.ProductDeliveryType = main.ProductDeliveryType;

            //最後出貨日
            if (main.LastShipDate != null)
            {
                shipInfo.LastShippingDate = ((DateTime)main.LastShipDate).ToString("yyyy/MM/dd");
            }

            if (orderShip != null)
            {
                shipInfo.ShipCompanyName = orderShip.ShipCompanyName ?? string.Empty;
                shipInfo.ShipNo = orderShip.ShipNo;
                shipInfo.ShipWebsite = orderShip.ShipWebsite;
                shipInfo.CustomerReceivedTime = orderShip.ReceiptTime == null
                    ? string.Empty : ((DateTime)orderShip.ReceiptTime).ToString("yyyy/MM/dd HH:mm");

                //實際出貨日
                if (orderShip.ShipTime != null)
                {
                    shipInfo.ActualShippingDate = ((DateTime)orderShip.ShipTime).ToString("yyyy/MM/dd");
                }
            }

            #region wms(pchome)
            if (main.IsWms)
            {
                shipInfo = GetWmsShipInfo(main, shipInfo);
            }
            #endregion
            #region 一般宅配
            else if (main.ProductDeliveryType == (int)ProductDeliveryType.Normal)
            {
                shipInfo.ShipStatus = orderShip == null ? ShippingStatus.ReadyToShip : ShippingStatus.Shipped;
            }
            #endregion
            #region 超商取貨
            else
            {

                shipInfo.ShipStatus = GetIspGoodsStatus(main);
                shipInfo.ShipNo = main.PreShipNo;

                //表示商家未回報出貨(未壓OrderShip)，但實際已出貨
                bool isShowShipNo = !string.IsNullOrEmpty(shipInfo.ShipNo) && string.IsNullOrEmpty(shipInfo.ShipCompanyName);

                if (main.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
                {
                    shipInfo.PickupStore.StoreId = main.FamilyStoreId ?? string.Empty;
                    shipInfo.PickupStore.StoreName = main.FamilyStoreName ?? string.Empty;
                    shipInfo.PickupStore.StoreTel = main.FamilyStoreTel ?? string.Empty;
                    shipInfo.PickupStore.StoreAddr = main.FamilyStoreAddr ?? string.Empty;
                    if (isShowShipNo)
                    {
                        shipInfo.ShipWebsite = config.FamilyIspWebSide;
                        shipInfo.ShipCompanyName = "全家";
                    }
                }
                if (main.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
                {
                    shipInfo.PickupStore.StoreId = main.SevenStoreId ?? string.Empty;
                    shipInfo.PickupStore.StoreName = main.SevenStoreName ?? string.Empty;
                    shipInfo.PickupStore.StoreTel = main.SevenStoreTel ?? string.Empty;
                    shipInfo.PickupStore.StoreAddr = main.SevenStoreAddr ?? string.Empty;
                    if (isShowShipNo)
                    {
                        shipInfo.ShipWebsite = config.SevenIspWebSide;
                        shipInfo.ShipCompanyName = "7-11";
                    }
                }
            }
            #endregion


            if (main.IsCanceling || Helper.IsFlagSet(main.OrderStatus, (int)OrderStatus.Cancel))
            {
                //超取有退貨，出貨狀態同訂單狀態
                shipInfo.ShipStatus = ShippingStatus.None;
                shipInfo.ShipStatusDesc = PponDealApiManager.GetOrderTypeDesc(main);
            }
            else if (main.IsWms)
            {
                shipInfo.ShipStatusDesc = shipInfo.ShipStatusDesc;
            }
            else
            {
                shipInfo.ShipStatusDesc = Helper.GetEnumDescription(shipInfo.ShipStatus);
            }



            return shipInfo;
        }

        /// <summary>
        /// 多物流單號使用
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static List<ShipInfo> GetOrderShipInfoList(ViewCouponListMain main)
        {
            if (main.DeliveryType != (int)DeliveryType.ToHouse)
            {
                return null;
            }

            var shipCols = op.ViewOrderShipListGetListByOrderGuid(main.Guid).OrderByDescending(x => x.ShipTime);
            List<ShipInfo> shipInfoList = new List<ShipInfo>();
            foreach (ViewOrderShipList item in shipCols)
            {
                var orderShip = item;
                if (orderShip == null || orderShip.ShipTime == null || orderShip.ShipTime >= DateTime.Now.Date.AddDays(1))
                {
                    orderShip = null;
                }

                ShipInfo shipInfo = new ShipInfo();
                shipInfo.ProductDeliveryType = main.ProductDeliveryType;

                //最後出貨日
                if (main.LastShipDate != null)
                {
                    shipInfo.LastShippingDate = ((DateTime)main.LastShipDate).ToString("yyyy/MM/dd");
                }

                if (orderShip != null)
                {
                    shipInfo.ShipCompanyName = orderShip.ShipCompanyName ?? string.Empty;
                    shipInfo.ShipNo = orderShip.ShipNo;
                    shipInfo.ShipWebsite = orderShip.ShipWebsite;
                    shipInfo.CustomerReceivedTime = orderShip.ReceiptTime == null
                        ? string.Empty : ((DateTime)orderShip.ReceiptTime).ToString("yyyy/MM/dd HH:mm");

                    //實際出貨日
                    if (orderShip.ShipTime != null)
                    {
                        shipInfo.ActualShippingDate = ((DateTime)orderShip.ShipTime).ToString("yyyy/MM/dd");
                    }
                }

                #region wms(pchome)
                if (main.IsWms)
                {
                    shipInfo = GetWmsShipInfo(main, shipInfo);
                }
                #endregion
                #region 一般宅配
                else if (main.ProductDeliveryType == (int)ProductDeliveryType.Normal)
                {
                    shipInfo.ShipStatus = orderShip == null ? ShippingStatus.ReadyToShip : ShippingStatus.Shipped;
                }
                #endregion
                #region 超商取貨
                else
                {

                    shipInfo.ShipStatus = GetIspGoodsStatus(main);
                    shipInfo.ShipNo = main.PreShipNo;

                    //表示商家未回報出貨(未壓OrderShip)，但實際已出貨
                    bool isShowShipNo = !string.IsNullOrEmpty(shipInfo.ShipNo) && string.IsNullOrEmpty(shipInfo.ShipCompanyName);

                    if (main.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
                    {
                        shipInfo.PickupStore.StoreId = main.FamilyStoreId ?? string.Empty;
                        shipInfo.PickupStore.StoreName = main.FamilyStoreName ?? string.Empty;
                        shipInfo.PickupStore.StoreTel = main.FamilyStoreTel ?? string.Empty;
                        shipInfo.PickupStore.StoreAddr = main.FamilyStoreAddr ?? string.Empty;
                        if (isShowShipNo)
                        {
                            shipInfo.ShipWebsite = config.FamilyIspWebSide;
                            shipInfo.ShipCompanyName = "全家";
                        }
                    }
                    if (main.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
                    {
                        shipInfo.PickupStore.StoreId = main.SevenStoreId ?? string.Empty;
                        shipInfo.PickupStore.StoreName = main.SevenStoreName ?? string.Empty;
                        shipInfo.PickupStore.StoreTel = main.SevenStoreTel ?? string.Empty;
                        shipInfo.PickupStore.StoreAddr = main.SevenStoreAddr ?? string.Empty;
                        if (isShowShipNo)
                        {
                            shipInfo.ShipWebsite = config.SevenIspWebSide;
                            shipInfo.ShipCompanyName = "7-11";
                        }
                    }
                }
                #endregion

                if (main.IsCanceling || Helper.IsFlagSet(main.OrderStatus, (int)OrderStatus.Cancel))
                {
                    //超取有退貨，出貨狀態同訂單狀態
                    shipInfo.ShipStatus = ShippingStatus.None;
                    shipInfo.ShipStatusDesc = PponDealApiManager.GetOrderTypeDesc(main);
                }
                else if (main.IsWms)
                {
                    shipInfo.ShipStatusDesc = shipInfo.ShipStatusDesc;
                }
                else
                {
                    shipInfo.ShipStatusDesc = Helper.GetEnumDescription(shipInfo.ShipStatus);
                }

                shipInfoList.Add(shipInfo);
            }

            return shipInfoList;
        }

        /// <summary>
        /// 使用Wms(pchome) 的出貨說明，並把兩者的出貨狀況對應
        /// </summary>
        /// <param name="main"></param>
        /// <param name="shipInfo"></param>
        /// <returns></returns>
        private static ShipInfo GetWmsShipInfo(ViewCouponListMain main, ShipInfo shipInfo)
        {
            shipInfo.ShipStatus = WmsFacade.GetShipStatusMapping((WmsDeliveryStatus)main.WmsOrderShipStatus);
            if (main.WmsOrderShipDate.HasValue)
            {
                shipInfo.ActualShippingDate = main.WmsOrderShipDate.ToString();
            }
            shipInfo.ShipStatusDesc = Helper.GetDescription((WmsDeliveryStatus)main.WmsOrderShipStatus);

            return shipInfo;
        }

        public static ShippingStatus GetIspGoodsStatus(ViewCouponListMain main)
        {
            var shippingStatus = ShippingStatus.ReadyToShip;
            int goodsStatus = main.GoodsStatus ?? (int)GoodsStatus.OrderValid;
            switch (goodsStatus)
            {
                case (int)GoodsStatus.OrderValid:
                case (int)GoodsStatus.OrderInProcess:
                    shippingStatus = ShippingStatus.ReadyToShip;
                    break;
                case (int)GoodsStatus.GoodsSentToDc:
                case (int)GoodsStatus.DcReceiveSuccess:
                    shippingStatus = ShippingStatus.Shipped;
                    break;
                case (int)GoodsStatus.StoreReceived:
                    shippingStatus = ShippingStatus.StoreReceived;
                    break;
                case (int)GoodsStatus.CustomerReceived:
                    shippingStatus = ShippingStatus.CustomerReceived;
                    break;
                case (int)GoodsStatus.DcReturn:
                    shippingStatus = ShippingStatus.CustomerOverdueNonePickup;
                    break;
            }
            return shippingStatus;
        }

        public static string GetIspGoodsStatusDesc(ViewCouponListMain main)
        {
            var shippingStatus = GetIspGoodsStatus(main);
            return Helper.GetEnumDescription(shippingStatus);
        }
        /// <summary>
        /// 完成超取訂單
        /// </summary>
        public static void CompleteISPOrder(Guid oid)
        {
            ViewPponOrder vpo = pp.ViewPponOrderGet(oid);

            if (!Helper.IsFlagSet(vpo.OrderStatus, OrderStatus.Complete))
            {
                PaymentType pType = Helper.IsFlagSet(vpo.OrderStatus, OrderStatus.FamilyIspOrder) ? PaymentType.FamilyIsp : PaymentType.SevenIsp;

                CashTrustLogCollection ctl = mp.CashTrustLogGetListByOrderGuid(oid, OrderClassification.LkSite);
                //isp pay amount 
                decimal amount = ctl.Sum(x => x.FamilyIsp + x.SevenIsp);

                using (TransactionScope ts = new TransactionScope())
                {
                    if (PaymentFacade.OrderComplete(vpo, amount, pType))
                    {
                        ts.Complete();
                    }
                }

            }


        }

        /// <summary>
        /// 取回超取+超付訂單，否則回傳 new Order()
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public static Order GetIspPaymentOrder(string orderId)
        {
            var od = op.OrderGet(Order.Columns.OrderId, orderId);
            if (od.IsLoaded && (Helper.IsFlagSet(od.OrderStatus, OrderStatus.FamilyIspOrder) || Helper.IsFlagSet(od.OrderStatus, OrderStatus.SevenIspOrder)))
            {
                return od;
            }

            return new Order();
        }

        /// <summary>
        /// 取回超取+超付訂單，否則回傳 new Order()
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public static Order GetIspPaymentOrder(Guid orderGuid)
        {
            var od = op.OrderGet(orderGuid);
            if (od.IsLoaded && (Helper.IsFlagSet(od.OrderStatus, OrderStatus.FamilyIspOrder) || Helper.IsFlagSet(od.OrderStatus, OrderStatus.SevenIspOrder)))
            {
                return od;
            }

            return new Order();
        }

        /// <summary>
        /// 判斷訂單是否超取+超付
        /// </summary>
        /// <param name="orderStatus"></param>
        /// <returns></returns>
        public static bool IsIspPaymentOrder(int orderStatus)
        {
            if (Helper.IsFlagSet(orderStatus, OrderStatus.FamilyIspOrder) || Helper.IsFlagSet(orderStatus, OrderStatus.SevenIspOrder))
            {
                return true;
            }

            return false;
        }

        public static string GetShipDate(DateTime orderDate)
        {
            string result = string.Empty;

            int hour = int.Parse(orderDate.ToString("HH"));
            int min = int.Parse(orderDate.ToString("mm"));
            int second = int.Parse(orderDate.ToString("ss"));

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Monday:
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                case DayOfWeek.Sunday:
                    if (hour < 10)
                        result = orderDate.AddDays(1).ToString("yyyy/MM/dd");
                    if (hour >= 22)
                        result = orderDate.AddDays(2).ToString("yyyy/MM/dd");
                    if (hour >= 10 && hour < 22)
                        result = orderDate.AddDays(1).ToString("yyyy/MM/dd");
                    break;
                case DayOfWeek.Friday:
                    if (hour < 10)
                        result = orderDate.AddDays(1).ToString("yyyy/MM/dd");
                    if (hour >= 22)
                        result = orderDate.AddDays(3).ToString("yyyy/MM/dd");
                    if (hour >= 10 && hour < 22)
                        result = orderDate.AddDays(1).ToString("yyyy/MM/dd");
                    break;
                case DayOfWeek.Saturday:
                    result = orderDate.AddDays(2).ToString("yyyy/MM/dd");
                    break;
                default:
                    if (hour < 10)
                        result = orderDate.AddDays(1).ToString("yyyy/MM/dd");
                    if (hour >= 22)
                        result = orderDate.AddDays(2).ToString("yyyy/MM/dd");
                    if (hour >= 10 && hour < 22)
                        result = orderDate.AddDays(1).ToString("yyyy/MM/dd");
                    break;
            }

            return result;
        }

        #endregion


        /// <summary>
        /// 訂單申請取消/取消成功/取消失敗通知信
        /// </summary>
        /// <param name="orderGuid"></param>
        public static void SendOrderCancelMail(Guid orderGuid, bool applyCancel, bool cancelSuccess)
        {
            if (config.EnableCancelPaidByIspOrder)
            {
                OrderCancelMail template;
                MailMessage mail = new MailMessage();
                ViewPponOrder o = pp.ViewPponOrderGet(orderGuid);
                ViewPponDeal d = pp.ViewPponDealGetByBusinessHourGuid(o.BusinessHourGuid);
                CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);


                int itemCount = ctCol.Count(x => !Helper.IsFlagSet(x.SpecialStatus, (int)TrustSpecialStatus.Freight));
                int comboCount = ReturnService.QueryComboPackCount(orderGuid);   //為了顯示 "組" 這個字


                template = TemplateFactory.Instance().GetTemplate<OrderCancelMail>();
                template.MemberName = o.MemberName;
                template.OrderGuid = o.OrderGuid.ToString();
                template.OrderID = o.OrderId;
                template.ItemName = d.ItemName;
                template.CouponCounts = string.Format("{0}{1}", itemCount.ToString(), comboCount > 1 ? "組" : string.Empty);
                template.ReturnCouponCounts = string.Format("{0}{1}", itemCount.ToString(), comboCount > 1 ? "組" : string.Empty);
                template.ApplyCancel = applyCancel;
                template.CancelSuccess = cancelSuccess;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;
                mail.Body = template.ToString();


                try
                {
                    mail.To.Add(MemberFacade.GetUserEmail(o.MemberEmail));
                    mail.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                    mail.IsBodyHtml = true;
                    if (applyCancel)
                    {
                        mail.Subject = "取消訂單申請通知";
                    }
                    else if (!applyCancel && cancelSuccess)
                    {
                        mail.Subject = "取消訂單成功通知";
                    }
                    else if (!applyCancel && !cancelSuccess)
                    {
                        mail.Subject = "取消訂單失敗通知";
                    }

                    PostMan.Instance().Send(mail, SendPriorityType.Async);
                }
                catch (Exception ex)
                {
                    logger.Error("Error occured in sending OrderCancelMail", ex);
                }
            }

        }


        #region ShopBack導購


        public static void SaveShopBackInfo(Order o, IViewPponDeal theDeal, string gid)
        {
            if (config.EnableShopBack)
            {
                if (theDeal.DeliveryType.HasValue)
                {
                    var ctlCollection = mp.CashTrustLogGetListByOrderGuid(o.Guid, OrderClassification.LkSite).Where(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight));
                    foreach (var ctl in ctlCollection)
                    {
                        var paidAmount = GetCashTustLogPaidAmount(ctl);
                        if (paidAmount > 0)
                        {
                            ShopbackInfo info = new ShopbackInfo();
                            info.TrustId = ctl.TrustId;
                            info.OrderGuid = o.Guid;
                            info.Bid = theDeal.BusinessHourGuid;
                            info.DeliveryType = theDeal.DeliveryType.Value;
                            //判斷Gid字尾是否有?
                            if (gid.Length > 0 && string.Equals(gid.Substring(gid.Length - 1, 1), "?"))
                            {
                                gid = gid.Substring(0, gid.Length - 1);
                            }
                            info.Gid = gid;
                            info.Amount = paidAmount;
                            info.CreateTime = DateTime.Now;

                            oep.InsertShopBackInfo(info);
                        }
                    }
                }
            }
        }

        //訂單完成 or 取消
        public static string ShopBackValidationAPI(DateTime startTime, DateTime endTime)
        {
            var contractShipTimeStart = startTime.ReduceWorkDays(config.ShopBackEffectiveShipDays);
            var contractShipTimeEnd = endTime.ReduceWorkDays(config.ShopBackEffectiveShipDays);
            var contractOrderTimeStart = startTime.ReduceWorkDays(config.ShopBackEffectiveOrderDays);
            var contractOrderTimeEnd = endTime.ReduceWorkDays(config.ShopBackEffectiveOrderDays);

            var viewShopbackValidationInfoOrderGroup = opEntities
                .GetValidationOrderViewShopbackInfoList(startTime, endTime, contractShipTimeStart, contractShipTimeEnd,
                    contractOrderTimeStart, contractOrderTimeEnd).GroupBy(o => o.OrderId).ToList();

            string result = string.Empty;
            string exceptionMessage = string.Empty;

            //將訂單的cash_trust_log內amount抓出來
            foreach (var infos in viewShopbackValidationInfoOrderGroup)
            {
                try
                {
                    if (IsAllInfoComplete(infos))
                    {
                        result += CallShopBackValidationProcess(infos);
                    }
                    else if (FillToHouseContract(infos, contractOrderTimeStart, contractOrderTimeEnd)) //訂單產生日起60個工作天後仍未確認的訂單，系統將自動判定為有效訂單
                    {
                        result += CallShopBackValidationProcess(infos, true);
                    }
                }
                catch (Exception ex)
                {
                    exceptionMessage += ex.ToString() + "\n";
                }
            }

            //避免一次exception就發一次mail，一次匯總才寄
            if (!string.IsNullOrEmpty(exceptionMessage))
            {
                logger.Warn(string.Format(" ShopbackValidationAPI Exception=>{0}", exceptionMessage));
            }

            return result;
        }

        /// <summary>
        /// 符合宅配訂單產生日起n個工作天仍未確認的訂單
        /// </summary>
        /// <param name="infos"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        private static bool FillToHouseContract(IGrouping<string, ViewShopbackValidationInfo> infos, DateTime contractOrderTimeStart, DateTime contractOrderTimeEnd)
        {
            return infos.First().DeliveryType == (int)DeliveryType.ToHouse && infos.First().OrderTime >= contractOrderTimeStart && infos.First().OrderTime <= contractOrderTimeEnd;
        }

        /// <summary>
        /// 訂單內的cash_trust_log全部都完成核銷or退貨(全部商品都適用，含宅配、票券...)
        /// </summary>
        /// <param name="infos"></param>
        /// <returns></returns>
        private static bool IsAllInfoComplete(IGrouping<string, ViewShopbackValidationInfo> infos)
        {
            var reslut = infos.All(x => (
                 CheckCashTrustLogIsReject(x) || CheckCashTrustLogIsVerified(x)));

            return reslut;
        }

        private static string CallShopBackValidationProcess(IGrouping<string, ViewShopbackValidationInfo> infos, bool setShopbackValidationStatusIsApproved = false)
        {
            string result = string.Empty;

            var responseMessage = CallShopBackValidationAPI(infos.Key,
                infos.Where(x =>
                    (TrustStatus)x.CashTrustLogStatus == TrustStatus.Verified).Sum(x => x.Amount).ToString(), //只有核銷、已信託才算
                infos.First().Gid, GetShopbackCommissionType(infos.First().Bid, infos.First().OrderGuid).ToString()
                , setShopbackValidationStatusIsApproved ? ShopbackValidationStatus.approved
                    : CheckCashTrustLogIsReject(infos.FirstOrDefault()) ? ShopbackValidationStatus.rejected : ShopbackValidationStatus.approved);
            //將回傳結果存起來並更新訂單send_type狀態
            opEntities.SaveShopBackInfo(infos.First().OrderGuid, (int)ShopbackSendType.ValidationAPIFinish,
                GetShopbackCommissionType(infos.First().Bid, infos.First().OrderGuid).ToString(), responseMessage, DateTime.Now);

            result += responseMessage + "\n";
            return result;
        }

        private static bool CheckCashTrustLogIsVerified(ViewShopbackValidationInfo info)
        {
            return (TrustStatus)info.CashTrustLogStatus == TrustStatus.Verified;
        }

        private static bool CheckCashTrustLogIsReject(ViewShopbackValidationInfo info)
        {
            return (TrustStatus)info.CashTrustLogStatus == TrustStatus.Refunded
                   || (TrustStatus)info.CashTrustLogStatus == TrustStatus.Returned;
        }

        private static string CallShopBackValidationAPI(string orderId, string amount, string gid
            , string commissionType, ShopbackValidationStatus status)
        {
            string responseFromServer = string.Empty;
            string url = string.Format("{0}?offer_id={1}&adv_sub={2}&amount={3}&adv_sub2={4}&aff_id={5}&status={6}"
                                        , config.ShopbackApiUrl
                                        , config.ShopbackValidationAPIOfferId
                                        , HttpUtility.UrlEncode(orderId)
                                        , HttpUtility.UrlEncode(amount)
                                        , HttpUtility.UrlEncode(commissionType.ToString())
                                        , config.ShopbackAffId
                                        , HttpUtility.UrlEncode(status.ToString())
                );
            var requestStartTime = DateTime.Now;

            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseFromServer = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Warn(string.Format(" ShopbackOrderValidationAPICall Exception=>{0},url={1}", ex.ToString(), url));

                throw ex;
            }

            return responseFromServer + "=" + url;
        }

        private static IEnumerable<ViewShopbackInfo> GetViewShopbackInfoByOrderTimePeriod(DateTime startTime, DateTime endTime)
        {
            IEnumerable<ViewShopbackInfo> viewShopbackInfos = opEntities.GetCompleteOrderViewShopbackInfoList(startTime, endTime);
            return viewShopbackInfos;
        }

        private static IEnumerable<ViewShopbackInfo> GetViewShopbackInfos(Guid orderGuid)
        {
            IEnumerable<ViewShopbackInfo> viewShopbackInfos = opEntities.GetCompleteOrderViewShopbackInfoList(orderGuid);
            return viewShopbackInfos;
        }

        public static string ShopBackOrderAPI(DateTime startTime, DateTime endTime)
        {
            string result = string.Empty;
            string exceptionMessage = string.Empty;

            var readyToSendValiInfos = OrderFacade.GetViewShopbackInfoByOrderTimePeriod(startTime, endTime).Where(x => x.SendType == (int)ShopbackSendType.ReadySend);
            foreach (var infos in readyToSendValiInfos.GroupBy(x => x.OrderId))
            {
                string commissionType = GetShopbackCommissionType(infos.First().Bid, infos.First().OrderGuid).ToString();
                string responseMessage = string.Empty;
                try
                {
                    var CallSuccessful = CallShopBackOrderAPI(infos.Key, infos.Sum(x => x.Amount).ToString(), infos.First().Gid, commissionType,
                        out responseMessage);
                    opEntities.SaveShopBackInfo(infos.First().OrderGuid, (int)(CallSuccessful ? ShopbackSendType.OrderAPIFinish : ShopbackSendType.Abort), commissionType, responseMessage, DateTime.Now);
                }
                catch (Exception ex)
                {
                    exceptionMessage += ex.ToString() + "\n";
                }

                result += responseMessage + "\n";
            }

            //避免一次exception就發一次mail，一次匯總才寄
            if (!string.IsNullOrEmpty(exceptionMessage))
            {
                logger.Warn(string.Format(" ShopbackOrderAPI Exception=>{0}", exceptionMessage));
            }

            return result;
        }

        //宅配(不含家電)有用券-d7
        //宅配(不含家電)沒用券-d3
        //家電-a3
        //票券有用券-t5
        //票券沒用券-t2
        /// <summary>
        /// 得到shopback佣金代碼
        /// </summary>
        /// <param name="infoBid"></param>
        /// <param name="infoOrderGuid"></param>
        /// <returns></returns>
        private static ShopbackCommissionType GetShopbackCommissionType(Guid infoBid, Guid infoOrderGuid)
        {
            if (IsElectronics(infoBid))
            {
                return ShopbackCommissionType.a3;
            }

            bool isDiscount = IsDiscount(infoOrderGuid);
            return IsToHouse(infoBid) ? (isDiscount ? ShopbackCommissionType.d7 : ShopbackCommissionType.d3)
                : (isDiscount ? ShopbackCommissionType.t5 : ShopbackCommissionType.t2);
        }

        private static bool IsToHouse(Guid infoBid)
        {
            DealProperty dealProperty = pp.DealPropertyGet(infoBid);
            if ((DeliveryType)dealProperty.DeliveryType == DeliveryType.ToHouse)
            {
                return true;
            }

            return false;
        }

        private static bool IsElectronics(Guid infoBid)
        {
            CategoryDealCollection cdc = pp.CategoryDealsGetList(infoBid);
            foreach (var item in cdc)
            {
                //SELECT [id], [name] FROM [category] where name like N'%家電%' or name like N'%3C%'
                //或是看畫面連結的參數 https://www.17life.com/channel/88,0,117?cid=477&cpa=17_category 的117
                if (item.Cid == config.ShopbackCommissionElectronic || item.Cid == config.ShopbackCommission3C)
                {
                    return true;
                }
            }

            return false;
        }

        private static bool IsDiscount(Guid orderGuid)
        {
            CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            if (ctCol.Count > 0)
            {
                return ctCol.Sum(x => x.DiscountAmount) > 0;
            }

            return false;
        }

        private static bool CallShopBackOrderAPI(string orderId, string amount, string gid, string commissionType, out string responseMessage)
        {
            string responseFromServer = string.Empty;
            string url = string.Format("{0}?offer_id={1}&adv_sub={2}&amount={3}&transaction_id={4}&adv_sub2={5}"
                                        , config.ShopbackApiUrl
                                        , config.ShopbackOrderAPIOfferId
                                        , HttpUtility.UrlEncode(orderId)
                                        , HttpUtility.UrlEncode(amount)
                                        , HttpUtility.UrlEncode(gid.ToString())
                                        , HttpUtility.UrlEncode(commissionType.ToString())
                );
            var requestStartTime = DateTime.Now;

            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseFromServer = reader.ReadToEnd();
                            responseMessage = responseFromServer + "=" + url;
                            if (!ShopBackResponseSuccess(responseFromServer))
                            {
                                logger.InfoFormat("ShopBack Order API 回傳錯誤訊息。{0}", responseFromServer);
                                return false;
                            }

                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static bool ShopBackResponseSuccess(string responseFromServer)
        {
            return responseFromServer.Contains("success=true;");
        }

        #endregion

        #region Payeasy 導購

        public static void SavePezChannel(Order o, Guid bid, int? deliveryType, string gid)
        {
            if (config.EnablePezChannel)
            {
                if (deliveryType.HasValue)
                {
                    var ctlCollection = mp.CashTrustLogGetListByOrderGuid(o.Guid, OrderClassification.LkSite).Where(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight));
                    foreach (var ctl in ctlCollection)
                    {
                        PezChannel info = new PezChannel();
                        info.TrustId = ctl.TrustId;
                        info.OrderGuid = o.Guid;
                        info.Bid = bid;
                        info.DeliveryType = deliveryType.Value;
                        //判斷Gid字尾是否有?
                        if (gid.Length > 0 && string.Equals(gid.Substring(gid.Length - 1, 1), "?"))
                        {
                            gid = gid.Substring(0, gid.Length - 1);
                        }
                        info.Gid = gid;
                        info.Amount = ctl.Amount;
                        info.CreateTime = DateTime.Now;

                        op.PezChannelSet(info);
                    }
                }
            }
        }

        public static string PezChannelOrderAPI(DateTime startTime, DateTime endTime)
        {
            string result = string.Empty;
            var vic = OrderFacade.GetViewPezChannelByOrderTimePeriod(startTime, endTime);
            vic = vic.Where(x => x.SendType == 0);
            foreach (var ico in vic.GroupBy(x => new { Id = x.Id, OrderGuid = x.OrderGuid, DeliveryType = x.DeliveryType, OrderId = x.OrderId, OrderTime = x.OrderTime, Gid = x.Gid, Bid = x.Bid }, y => y))
            {
                if (ico.Key.DeliveryType == (int)DeliveryType.ToShop)
                {
                    #region 憑證 (因憑證狀態不同，以多筆訂單型態送出)

                    var returnCode = GetCommissionRuleReturnCode(ChannelSource.Payeasy, DeliveryType.ToShop, ico.Key.Bid, ico.Key.OrderTime);

                    foreach (var item in ico)
                    {
                        result += CallPezChannelOrderAPI(item.Id, config.PezChannelVenId, ico.Key.Gid, item.Amount.ToString("F0")
                            , returnCode, ico.Key.OrderId + "-" + item.CouponId, ico.Key.OrderTime) + "\n";
                    }

                    #endregion
                }
                else if (ico.Key.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    #region 宅配 (因宅配狀態一致，多筆商品金額以"||"合併資料送出)

                    var returnCode = GetCommissionRuleReturnCode(ChannelSource.Payeasy, DeliveryType.ToHouse, ico.Key.Bid, ico.Key.OrderTime);

                    result += CallPezChannelOrderAPI(ico.Key.Id, config.PezChannelVenId, ico.Key.Gid
                        , string.Join("||", ico.Select(x => x.Amount.ToString("F0")))
                        , string.Join("||", ico.Select(x => returnCode)), ico.Key.OrderId, ico.Key.OrderTime) + "\n";

                    #endregion
                }
            }

            logger.InfoFormat("\nPezChannel Order API Log:\n{0}", result);

            return result;
        }

        private static string CallPezChannelOrderAPI(int id, string venId, string sessionId, string price, string catId, string orderId, DateTime orderDateTime)
        {
            string url = string.Format("{0}?ven_id={1}&order_id={2}&price={3}&session_id={4}&order_date={5}"
                                        , config.PezChannelOrderApiUrl
                                        , venId
                                        , orderId
                                        , HttpUtility.UrlEncode(price)
                                        , sessionId
                                        , HttpUtility.UrlEncode(orderDateTime.ToString("yyyy-MM-dd")));

            url += !string.IsNullOrEmpty(catId) ? string.Format("&cat_id={0}", HttpUtility.UrlEncode(catId)) : string.Empty;


            var requestStartTime = DateTime.Now;
            var responseFromServer = string.Empty;
            bool sendType = false;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";
                request.ContentLength = 0;
                request.Timeout = 180000;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseFromServer = reader.ReadToEnd();
                            dynamic result = new Core.JsonSerializer().Deserialize<dynamic>(responseFromServer);

                            if (!bool.TryParse(result.status.ToString(), out sendType))
                            {
                                logger.InfoFormat("PezChannel Order API 回傳錯誤訊息。{0}", result.data.ToString());
                            }
                            else if (!sendType)
                            {
                                logger.InfoFormat("PezChannel Order API 回傳失敗。{0}", result.data.ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var requestEndTime = DateTime.Now;
                var spendtime = ((TimeSpan)(requestEndTime - requestStartTime)).TotalMilliseconds.ToString();
                logger.ErrorFormat("CallPezChannelOrderAPI Exception=>{0},url={1},spend time={2}"
                    , ex.ToString(), url, spendtime.ToString());
            }

            PezChannel vic = op.PezChannelGetById(id);
            vic.ModifyTime = DateTime.Now;
            vic.SendCount++;
            vic.SendType = Convert.ToInt32(sendType);
            vic.ReturnCode = catId;
            op.PezChannelSet(vic);

            return responseFromServer + "=" + url;
        }

        public static IEnumerable<ViewPezChannel> GetViewPezChannelByOrderTimePeriod(DateTime startTime, DateTime endTime)
        {
            ViewPezChannelCollection vpc = op.ViewPezChannelGetList(startTime, endTime);
            logger.InfoFormat("查詢PezChannel, 時間區間 {0}~{1}, 回傳筆數:{2}", startTime.ToString("yyyy/MM/dd HH:mm:ss"),
                endTime.ToString("yyyy/MM/dd HH:mm:ss"), vpc.Count);
            return vpc;
        }

        #endregion

        #region Line導購
        /// <summary>
        /// 寫入由Line導購的訂單
        /// </summary>
        /// <param name="o"></param>
        /// <param name="theDeal"></param>
        /// <param name="ecid"></param>
        public static void SaveLineShopInfo(Order o, IViewPponDeal theDeal, string ecid)
        {
            if (!config.EnableLineShop) return;

            if (PponFacade.ShouldExcludeIViewPponDeal(theDeal)) return;

            if (theDeal.DeliveryType.HasValue)
            {
                var ctlCollection = mp.CashTrustLogGetListByOrderGuid(o.Guid).Where(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight));
                List<LineshopInfo> infoList = new List<LineshopInfo>();
                foreach (var ctl in ctlCollection)
                {
                    var paidAmount = GetCashTustLogPaidAmount(ctl);
                    if (paidAmount > 0)
                    {
                        LineshopInfo info = new LineshopInfo();
                        info.TrustId = ctl.TrustId;
                        info.OrderGuid = o.Guid;
                        info.Bid = theDeal.BusinessHourGuid;
                        info.DeliveryType = theDeal.DeliveryType.Value;
                        if (ecid.Length > 0 && string.Equals(ecid.Substring(ecid.Length - 1, 1), "?"))
                        {
                            ecid = ecid.Substring(0, ecid.Length - 1);
                        }
                        info.Gid = ecid;
                        info.Amount = paidAmount;
                        info.CreateTime = DateTime.Now;
                        infoList.Add(info);
                    }
                }
                oep.BulkInsertLineShopInfo(infoList);
            }
        }
        /// <summary>
        /// Line導購首拋訂單組資料
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static void LineShopOrderInfoAPI(DateTime startTime, DateTime endTime)
        {
            string result = string.Empty;
            string exceptionMsg = string.Empty;
            //先查OrderInfoList
            var orderInfoList = opEntities.GetOrderInfoForLineShop(startTime, endTime);
            if (orderInfoList.Count > 0)
            {
                //用Guid查要拋單的CashTrustLog(明細)
                var cashTrustLogList = mp.GetCashTrustLogCollectionByOid(orderInfoList.Select(o => o.OrderGuid).ToList());
                var ctlDic = cashTrustLogList
                    .GroupBy(g => new { g.OrderGuid, g.BusinessHourGuid, g.ItemName })
                    .Select(c1 => new LineShopOrderListInfo
                    {
                        OrderGuid = c1.Key.OrderGuid,
                        Bid = Guid.Parse(c1.Key.BusinessHourGuid.ToString()),
                        ItemName = c1.Key.ItemName,
                        OrderAmount = c1.Sum(c => c.Amount) - (c1.Sum(c => c.DiscountAmount) + c1.Sum(c => c.Bcash)),
                        CashTrustLogAmount = c1.Sum(c => c.Amount),
                        CashTrustLogDiscount = c1.Sum(c => c.DiscountAmount),
                        CashTrustLogBcash = c1.Sum(c => c.Bcash)

                    }).ToList()
                    .GroupBy(r => r.OrderGuid)
                    .ToDictionary(d => d.Key, d => d.ToList());

                //逐筆組要拋的資料, call api-CallLineOrderFirstAPI
                foreach (var orderInfo in orderInfoList) // OrderGuid List 
                {
                    //從ctlDic取這筆訂單的所有CashTrustLog
                    var ctlListByOrderGuid = ctlDic[orderInfo.OrderGuid];
                    orderInfo.Ordertotal = ctlListByOrderGuid.Sum(c => c.OrderAmount);

                    int timestamp = Helper.GetCurrentUnixTimestamp();
                    #region 組hash_hamc
                    string ordertime_hashkey = Security.MD5HashCSP(orderInfo.OrderTime.ToString("yyyy-MM-dd HH:mm:ss"));
                    string hashinput = string.Format("orderid={0}&ordertotal={1}&timestamp={2}", orderInfo.OrderId, (int)orderInfo.Ordertotal, timestamp);
                    string hash_hamc = Security.HmacSHA256ToBit(hashinput, ordertime_hashkey);
                    #endregion
                    //先組order_list以外的資料
                    LineShopOrderInfoRequest infoRequest = new LineShopOrderInfoRequest(orderInfo, timestamp, hash_hamc);

                    //組product
                    foreach (var ctlDetail in ctlListByOrderGuid)
                    {
                        LineShopOrderListItem product = new LineShopOrderListItem();
                        product.Product = new LineShopOrderListProduct(ctlDetail);
                        infoRequest.OrderList.Add(product);
                    }

                    string responseMsg = string.Empty;
                    var callSuccessful = CallLineOrderInfoAPI(infoRequest, out responseMsg);
                    //更新此筆拋單狀態
                    opEntities.UpdateLineShopInfoSendType(orderInfo.OrderGuid,
                        (int)(callSuccessful ? LineShopSendType.OrderInfoAPIFinish : LineShopSendType.Abort), DateTime.Now, responseMsg);
                }
            }
        }

        /// <summary>
        /// Line導購首拋訂單Call Line API
        /// </summary>
        /// <param name="orderRequest"></param>
        /// <param name="responseMsg"></param>
        /// <returns></returns>
        private static bool CallLineOrderInfoAPI(LineShopOrderInfoRequest orderRequest, out string responseMsg)
        {
            string sendInf = string.Format("site={0}&shopid={1}&authkey={2}&orderid={3}&ordertime={4}&ordertotal={5}&order_list={6}&ecid={7}&timestamp={8}&hash={9}"
                                    , config.LineShopSite
                                    , config.LineShopId
                                    , config.LineShopAuthKey
                                    , orderRequest.OrderId
                                    , orderRequest.OrderTime
                                    , orderRequest.OrderTotal
                                    , JsonConvert.SerializeObject(orderRequest.OrderList)
                                    , orderRequest.Ecid
                                    , orderRequest.Timestamp
                                    , orderRequest.Hash);
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                HttpContent contentPost = new StringContent(sendInf, Encoding.UTF8, "application/x-www-form-urlencoded");
                HttpResponseMessage response = client.PostAsync(config.LineShopOrderFirstApiUrl, contentPost).GetAwaiter().GetResult();

                if (response.IsSuccessStatusCode)
                {
                    var Msg = Regex.Unescape(response.Content.ReadAsStringAsync().Result);
                    responseMsg = Msg.Substring(0, Math.Min(200, Msg.Length));
                    SystemFacade.SetApiLog("OrderFacade.CallLineOrderInfoAPI", "", sendInf, responseMsg);
                    if (!LineShopResponseSuccess(responseMsg))
                    {
                        logger.Error(string.Format("LineShopOrderInfoAPI 回傳錯誤訊息。{0}", responseMsg));
                        return false;
                    }
                    return true;
                }
                else
                {
                    responseMsg = response.StatusCode.ToString();
                    logger.Error(string.Format("LineShopOrderInfoAPI 連線錯誤。{0}", response.StatusCode));
                    return false;
                }
            }
        }
        /// <summary>
        /// Line導購請款/發點訂單組資料
        /// </summary>
        /// <param name="finishDate"></param>
        /// <returns></returns>
        public static void LineShopOrderFinishAPI(DateTime finishDate)
        {
            string result = string.Empty;
            string exceptionMsg = string.Empty;

            DateTime orderStartDate = DateTime.Parse(finishDate.AddDays(-(config.LineShopEffectiveOrderDays)).ToString("yyyy/MM/dd"));
            DateTime shipStartDate = DateTime.Parse(finishDate.AddDays(-(config.LineShopEffectiveShipDays)).ToString("yyyy/MM/dd"));

            //先查OrderFinishList
            var orderFinishList = opEntities.GetOrderFinishForLineShop(orderStartDate, shipStartDate);
            if (orderFinishList.Count > 0)
            {
                //用Guid查要拋單的CashTrustLog(明細)
                var cashTrustLogList = mp.GetCashTrustLogCollectionByOid(orderFinishList.Select(o => o.OrderGuid).ToList());
                var ctlDic = cashTrustLogList.GroupBy(c => c.OrderGuid).ToDictionary(d => d.Key, d => d.ToList());

                // 逐筆組要拋的資料, call api-CallLineOrderFinishAPI
                foreach (var orderFinish in orderFinishList)
                {
                    //從ctlDic取這筆訂單的所有CashTrustLog
                    var ctlListByOrderGuid = ctlDic[orderFinish.OrderGuid];
                    //憑證且訂單成立+60天未全部核銷,則不計算
                    if (ctlListByOrderGuid.Where(o => o.Status != (int)TrustStatus.Verified).Any())
                    {
                        continue;
                    }
                    //查已核銷的單
                    var ctlIsVerifiedList = ctlListByOrderGuid.Where(c => c.Status == (int)TrustStatus.Verified);
                    //已核銷的商品總額、折扣金、紅利金
                    int vAmountTotal = (ctlIsVerifiedList.Select(c => c.Amount).Sum());
                    int vDiscountTotal = (ctlIsVerifiedList.Select(c => c.DiscountAmount).Sum());
                    int vBcashTotal = (ctlIsVerifiedList.Select(c => c.Bcash).Sum());
                    //核銷的 交易付款總金額=總額-(折扣金+紅利金)
                    int feetotal = vAmountTotal - (vDiscountTotal + vBcashTotal);
                    int timestamp = Helper.GetCurrentUnixTimestamp();

                    #region 若是測試 FeeTime強制給當下時間
                    if (config.LineShopForTest)
                    {
                        orderFinish.FeeTime = DateTime.Now;
                    }
                    #endregion

                    #region 組hash_hamc
                    string feetime_hashkey = Security.MD5HashCSP(orderFinish.FeeTime.ToString("yyyy-MM-dd HH:mm:ss"));
                    string hashinput = string.Format("orderid={0}&feetime={1}&feetotal={2}&timestamp={3}", orderFinish.OrderId, orderFinish.FeeTime.ToString("yyyy-MM-dd HH:mm:ss"), feetotal, timestamp);
                    string hash_hamc = Security.HmacSHA256ToBit(hashinput, feetime_hashkey);
                    #endregion

                    //先組fee_list以外的資料
                    LineShopOrderFinishRequest infoRequest = new LineShopOrderFinishRequest(orderFinish, feetotal, timestamp, hash_hamc);

                    //distinct bid
                    var bidList = ctlIsVerifiedList.Select(c => c.BusinessHourGuid).Distinct();
                    //組product
                    foreach (Guid vBid in bidList)
                    {
                        //查此bid的商品總額、折扣金、紅利金、名稱
                        int bidAmountTotal = (ctlIsVerifiedList.Where(c => c.BusinessHourGuid == vBid).Select(c => c.Amount).Sum());
                        int bidDiscountTotal = ctlIsVerifiedList.Where(c => c.BusinessHourGuid == vBid).Select(c => c.DiscountAmount).Sum();
                        int bidBcashTotal = (ctlIsVerifiedList.Where(c => c.BusinessHourGuid == vBid).Select(c => c.Bcash).Sum());
                        string productName = ctlIsVerifiedList.Where(c => c.BusinessHourGuid == vBid).Select(c => c.ItemName).FirstOrDefault();
                        //單品總額=amount-(discount+bcash)
                        int productFee = (bidAmountTotal - (bidDiscountTotal + bidBcashTotal));
                        //組fee_list
                        LineShopFeeListItem product = new LineShopFeeListItem();
                        product.Product = new LineShopFeeListProduct(productName, vBid, productFee);
                        infoRequest.FeeList.Add(product);
                    }
                    string responseMsg = string.Empty;
                    var callSuccessful = CallLineOrderFinishAPI(infoRequest, out responseMsg);
                    //更新此筆拋單狀態
                    opEntities.UpdateLineShopInfoSendType(orderFinish.OrderGuid,
                        (int)(callSuccessful ? LineShopSendType.OrderFinishAPIFinish : LineShopSendType.Abort), DateTime.Now, responseMsg);
                }
            }
        }

        /// <summary>
        /// Line導購請款/發點訂單Call Line API
        /// </summary>
        /// <param name="orderRequest"></param>
        /// <param name="responseMsg"></param>
        /// <returns></returns>
        private static bool CallLineOrderFinishAPI(LineShopOrderFinishRequest orderRequest, out string responseMsg)
        {
            string sendInf = string.Format("site={0}&shopid={1}&authkey={2}&orderid={3}&ordertime={4}&fee_list={5}&feetotal={6}&feetime={7}&ecid={8}&timestamp={9}&hash={10}"
                                    , config.LineShopSite
                                    , config.LineShopId
                                    , config.LineShopAuthKey
                                    , orderRequest.OrderId
                                    , orderRequest.OrderTime
                                    , JsonConvert.SerializeObject(orderRequest.FeeList)
                                    , orderRequest.FeeTotal
                                    , orderRequest.FeeTime
                                    , orderRequest.Ecid
                                    , orderRequest.Timestamp
                                    , orderRequest.Hash);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                HttpContent contentPost = new StringContent(sendInf, Encoding.UTF8, "application/x-www-form-urlencoded");
                HttpResponseMessage response = client.PostAsync(config.LineShopOrderFinishApiUrl, contentPost).GetAwaiter().GetResult();

                if (response.IsSuccessStatusCode)
                {
                    var Msg = Regex.Unescape(response.Content.ReadAsStringAsync().Result);
                    responseMsg = Msg.Substring(0, Math.Min(200, Msg.Length));
                    SystemFacade.SetApiLog("OrderFacade.CallLineOrderFinishAPI", "", sendInf, responseMsg);
                    if (!LineShopResponseSuccess(responseMsg))
                    {
                        logger.Error(string.Format("LineShopOrderFinishAPI 回傳錯誤訊息。{0}", responseMsg));
                        return false;
                    }
                    return true;
                }
                else
                {
                    responseMsg = response.StatusCode.ToString();
                    logger.Error(string.Format("LineShopOrderFinishAPI 連線錯誤。{0}", response.StatusCode));
                    return false;
                }
            }
        }

        /// <summary>
        /// Line導購Call Line API成功狀態
        /// </summary>
        /// <param name="responseMsg"></param>
        /// <returns></returns>
        private static bool LineShopResponseSuccess(string responseMsg)
        {
            return responseMsg.Contains("OK");
        }
        #endregion


        #region Affiliates 導購

        public static void SaveAffiliatesInfo(Order o, Guid bid, int? deliveryType, string gid)
        {
            if (config.EnableAffiliates)
            {
                if (deliveryType.HasValue)
                {
                    var ctlCollection = mp.CashTrustLogGetListByOrderGuid(o.Guid, OrderClassification.LkSite).Where(x => !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight));
                    foreach (var ctl in ctlCollection)
                    {
                        var paidAmount = GetCashTustLogPaidAmount(ctl);
                        if (paidAmount > 0)
                        {
                            AffiliatesInfo info = new AffiliatesInfo();
                            info.TrustId = ctl.TrustId;
                            info.OrderGuid = o.Guid;
                            info.Bid = bid;
                            info.DeliveryType = deliveryType.Value;
                            //判斷Gid字尾是否有?
                            if (gid.Length > 0 && string.Equals(gid.Substring(gid.Length - 1, 1), "?"))
                            {
                                gid = gid.Substring(0, gid.Length - 1);
                            }
                            info.Gid = gid;
                            info.Amount = paidAmount;
                            info.CreateTime = DateTime.Now;

                            oep.InsertAffiliatesInfo(info);
                        }
                    }
                }
            }
        }

        public static string AffiliatesOrderAPI(DateTime startTime, DateTime endTime)
        {
            string result = string.Empty;
            string exceptionMessage = string.Empty;

            var readyToSendValiInfos = OrderFacade.GetViewAffiliatesInfoByOrderTimePeriod(startTime, endTime).Where(x => x.SendType == (int)AffiliatesSendType.ReadySend);
            foreach (var infos in readyToSendValiInfos.GroupBy(x => x.OrderId))
            {
                string responseMessage = string.Empty;
                try
                {
                    var CallSuccessful = CallAffiliatesOrderAPI(infos.Key, infos.Sum(x => x.Amount).ToString(), infos.First().Gid, out responseMessage);
                    opEntities.SaveAffiliatesInfo(infos.First().OrderGuid, (int)(CallSuccessful ? AffiliatesSendType.OrderAPIFinish : AffiliatesSendType.Abort), responseMessage, DateTime.Now);
                }
                catch (Exception ex)
                {
                    exceptionMessage += ex.ToString() + "\n";
                }

                result += responseMessage + "\n";
            }

            //避免一次exception就發一次mail，一次匯總才寄
            if (!string.IsNullOrEmpty(exceptionMessage))
            {
                logger.Warn(string.Format(" AffiliatesOrderAPI Exception=>{0}", exceptionMessage));
            }

            return result;
        }

        private static bool CallAffiliatesOrderAPI(string orderId, string amount, string subid, out string responseMessage)
        {
            string responseFromServer = string.Empty;
            string url = string.Format("{0}?order={1}&order_total={2}&server_subid={3}&step=sale"
                                        , config.AffiliatesApiUrl
                                        , HttpUtility.UrlEncode(orderId)
                                        , HttpUtility.UrlEncode(amount)
                                        , HttpUtility.UrlEncode(subid)
                );
            var requestStartTime = DateTime.Now;

            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseFromServer = reader.ReadToEnd();
                            responseMessage = responseFromServer + "=" + url;
                            if (!AffiliatesResponseSuccess(responseFromServer))
                            {
                                logger.InfoFormat("Affiliates Order API 回傳錯誤訊息。{0}", responseFromServer);
                                return false;
                            }

                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static bool AffiliatesResponseSuccess(string responseFromServer)
        {
            return responseFromServer.Contains("OK");
        }

        private static IEnumerable<ViewAffiliatesInfo> GetViewAffiliatesInfoByOrderTimePeriod(DateTime startTime, DateTime endTime)
        {
            IEnumerable<ViewAffiliatesInfo> viewAffiliatesInfos = opEntities.GetCompleteOrderViewAffiliatesInfoList(startTime, endTime);
            return viewAffiliatesInfos;
        }

        #endregion
    }
}
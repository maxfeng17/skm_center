using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade.Models;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using WmsModel = LunchKingSite.BizLogic.Models.Wms;

namespace LunchKingSite.BizLogic.Facade
{
    public class PponBuyFacade
    {
        private static ISerializer serializer = ProviderFactory.Instance().GetSerializer();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        private static IWmsProvider wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public const string _PPON_PAYMENT_DTO_KEY = "pponPaymentDto";
        private static ILog logger = LogManager.GetLogger(typeof(PponBuyFacade));

        public static PponPaymentDTO GetPponPaymentDTOFromSession(string ticketId)
        {
            var ts = pp.TempSessionGet(ticketId, _PPON_PAYMENT_DTO_KEY);
            if (ts == null || string.IsNullOrEmpty(ts.ValueX))
            {
                return null;
            }
            try
            {
                return serializer.Deserialize<PponPaymentDTO>(ts.ValueX);
            }
            catch (Exception ex)
            {
                logger.Warn("GetPponDeliveryInfoFromSession error, content=" + ts.ValueX, ex);
                try
                {
                    pp.TempSessionDelete(ts.Id);
                }
                catch
                {
                    // ignored
                }
            }
            return null;
        }

        public static void SetPponPaymentDTOToSession(string ticketId, PponPaymentDTO paymentDTO)
        {
            var ts = pp.TempSessionGet(ticketId, _PPON_PAYMENT_DTO_KEY);
            if (ts == null)
            {
                ts = new TempSession();
                ts.SessionId = ticketId;
                ts.Name = _PPON_PAYMENT_DTO_KEY;
            }
            ts.ValueX = serializer.Serialize(paymentDTO);
            pp.TempSessionSet(ts);
        }


        public static void DeletePponDeliveryInfoAtSession(string ticketId)
        {
            pp.TempSessionDelete(ticketId);
        }

        /// <summary>
        /// 取得檔次運費
        /// </summary>
        /// <param name="orderedAmount"></param>
        /// <param name="deal"></param>
        public static int GetDealDeliveryCharge(decimal orderedAmount, IViewPponDeal deal)
        {
            CouponFreightCollection cfc = pp.CouponFreightGetList(deal.BusinessHourGuid, CouponFreightType.Income);

            if (cfc != null)
            {
                foreach (CouponFreight cf in cfc)
                {
                    if (orderedAmount >= cf.StartAmount && cf.EndAmount > orderedAmount)
                    {
                        return (int)(cf.FreightAmount);
                    }
                }
            }
            return (int)deal.BusinessHourDeliveryCharge;
        }

        public static bool IsDealATMAvailable(IViewPponDeal deal)
        {
            bool isATMAvailable = config.TurnATMOn && PaymentFacade.CtAtmCheck(deal) &&
                deal.BusinessHourOrderTimeE.Date != DateTime.Now.Date;
            return isATMAvailable;
        }

        /// <summary>
        /// 是否在黑名單
        /// </summary>
        /// <param name="authObject"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool CheckBlackCreditcardOrderTimesMoreThan3(PponCreditCardInfo authObject, CreditCardAuthResult result)
        {
            int orderTimes = PaymentFacade.GetBlackCreditcardOrderTimes(authObject.CardNumber);
            if (orderTimes >= 3)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 紀錄收件人或修改會員資料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="order"></param>
        /// <param name="info">收件地址資訊</param>
        /// <param name="deliveryType"></param>
        /// <param name="carrierType">發票載具類型</param>
        /// <param name="carrierId">發票載具號碼</param>
        /// <param name="invoiceBuyerName"></param>
        /// <param name="invoiceBuyerAddress"></param>
        public static void SaveDeliveryInfo(int userId, Order order, BuyAddressInfo info,
            int? deliveryType, CarrierType carrierType, string carrierId, string invoiceBuyerName = "", string invoiceBuyerAddress = "")
        {
            if (info == null)
            {
                return;
            }
            Member mem = mp.MemberGet(userId);
            if (mem.IsLoaded == false)
            {
                return;
            }

            //會員資料一律同步更新
            MemberFacade.MemberPurchaserSet(
                mem.UserName,
                info.FamilyName,
                info.GivenName,
                info.BuyerMobile,
                info.UserEmail,
                info.BuyerAreaId,
                info.BuyerAddress,
                carrierType, carrierId, invoiceBuyerName, invoiceBuyerAddress);
            mem.FirstName = info.GivenName;
            mem.LastName = info.FamilyName;

            // fix order's buyer name
            if (deliveryType.HasValue && int.Equals((int)DeliveryType.ToShop, deliveryType.Value))
            {
                order.MemberName = mem.DisplayName;
                op.OrderSet(order);
            }

            if (info.AddReceiverInfo && !string.IsNullOrWhiteSpace(info.ReceiverName))
            {
                MemberFacade.MemberAddresseeSet(mem.UniqueId, info.ReceiverName, info.ReceiverMobile, info.ReceiverAreaId, info.ReceiverAddress);
            }
        }

        public static List<OptionItemLimit> GetGetAccessoryGroupList(Guid guid)
        {
            AccessoryGroupCollection viagc = ip.AccessoryGroupGetListByItem(guid);
            ViewItemAccessoryGroupCollection vigc = new ViewItemAccessoryGroupCollection();
            foreach (AccessoryGroup ag in viagc)
            {
                ag.members = new List<ViewItemAccessoryGroup>();
                vigc = ip.ViewItemAccessoryGroupGetList(guid, ag.Guid);
            }
            List<ViewItemAccessoryGroup> listViagc = vigc.ToList();
            List<OptionItemLimit> itemLimitList = new List<OptionItemLimit>();
            if (listViagc.Count > 0)
            {
                for (int i = 0; i < listViagc.Count; i++)
                {
                    ViewItemAccessoryGroup v = listViagc[i];
                    OptionItemLimit oil = new OptionItemLimit() { AccessoryName = v.AccessoryName, AccessoryId = v.AccessoryGroupMemberGuid.ToString() };
                    if (v.Quantity == null || v.Quantity > 0)
                    {
                        if (v.Quantity > 0)
                        {
                            oil.Limit = v.Quantity.Value;
                        }
                    }
                    else
                    {
                        oil.Limit = 0;

                    }
                    if (v.Quantity != null)
                    {
                        itemLimitList.Add(oil);
                    }
                }
            }

            return itemLimitList;
        }

        public static List<OptionItemLimit> GetProductItemList(IViewPponDeal vpd, out int optionCount)
        {
            int option_count = 0;
            //AccessoryGroupCollection viagc = ip.AccessoryGroupGetListByItem(guid);
            AccessoryGroupCollection viagc = new AccessoryGroupCollection();
            //新版(統一改抓ppon_option)
            PponOptionCollection pos = pp.PponOptionGetList(vpd.BusinessHourGuid);
            List<string> catgNames = pos.Select(x => x.CatgName).Distinct().ToList();
            ProductItemCollection pic = pp.ProductItemGetList(pos.Select(x => x.ItemGuid.GetValueOrDefault()).ToList());
            option_count = pos.Select(x => x.CatgSeq).Distinct().Count();
            foreach (string cName in catgNames)
            {
                AccessoryGroup ag = new AccessoryGroup()
                {
                    AccessoryGroupName = cName
                };
                List<PponOption> oplist = pos.Where(x => x.CatgName == cName).OrderBy(y => y.OptionSeq).ToList();
                ag.members = new List<ViewItemAccessoryGroup>();
                foreach (PponOption op in oplist)
                {
                    ProductItem piOption = pic.Where(x => x.Guid == op.ItemGuid.GetValueOrDefault()).FirstOrDefault();
                    int quantity = 0;
                    if (piOption != null)
                    {
                        quantity = piOption.Stock;
                    }
                    ViewItemAccessoryGroup viag = new ViewItemAccessoryGroup()
                    {
                        ItemName = vpd.ItemName,
                        ItemPrice = vpd.ItemPrice,
                        AccessoryGroupMemberGuid = op.Guid,
                        AccessoryGroupMemberSequence = op.OptionSeq,
                        AccessoryGroupName = op.CatgName,
                        AccessoryName = op.OptionName,
                        Quantity = quantity
                    };
                    ag.members.Add(viag);
                }

                viagc.Add(ag);
            }

            List<ViewItemAccessoryGroup> listViagc = viagc.SelectMany(x=>x.members).ToList();
            List<OptionItemLimit> itemLimitList = new List<OptionItemLimit>();
            if (listViagc.Count > 0)
            {
                for (int i = 0; i < listViagc.Count; i++)
                {
                    ViewItemAccessoryGroup v = listViagc[i];
                    OptionItemLimit oil = new OptionItemLimit() { AccessoryName = v.AccessoryName, AccessoryId = v.AccessoryGroupMemberGuid.ToString() };
                    if (v.Quantity == null || v.Quantity > 0)
                    {
                        if (v.Quantity > 0)
                        {
                            oil.Limit = v.Quantity.Value;
                        }
                    }
                    else
                    {
                        oil.Limit = 0;

                    }
                    if (v.Quantity != null)
                    {
                        itemLimitList.Add(oil);
                    }
                }
            }
            optionCount = option_count;
            return itemLimitList;
        }

        /// <summary>
        ///  取得目前儲存於DeliveryAddress的字串
        /// </summary>
        /// <returns></returns>
        public static string GetOrderDesc(PponDeliveryInfo info)
        {
            string rtn = string.Empty;

            if (info.DeliveryType == DeliveryType.ToHouse)
            {
                rtn = string.Empty;
            }
            else if (info.IsForFriend)
            {
                rtn = "To:" + info.FriendName + "<br />From:" + info.Genus + "<br />內容:" + info.ToFriendNote + "<hr />|" + info.FriendEMail;
            }
            else
            {
                rtn = "買給自己";
            }

            return rtn;
        }

        //實際扣pcash 回傳是否成功 (和authcode) //在CheckOut的method中沒有使用到theDepartment所不做變動(tun)
        public static bool GetPcashAuthCode(string pezId, string transId, decimal theCharge, DepartmentTypes theDepartment, out string authCode)
        {
            string pezResult = PCashWorker.CheckOut(pezId, transId, theCharge, theDepartment, out authCode);
            if (pezResult == "0000")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 取得可購買數量
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="theDeal"></param>
        /// <param name="qty">可購買數量</param>
        /// <param name="message">錯誤訊息</param>
        public static void CheckAlreadyBoughtRestriction(int userId, IViewPponDeal theDeal, out int qty, out string message)
        {
            qty = theDeal.MaxItemCount ?? 10;
            message = string.Empty;
            //此會員本檔次已購買數。
            int boughtCount = pp.ViewPponOrderDetailGetCountByUser(userId, theDeal.BusinessHourGuid,
                theDeal.IsDailyRestriction.GetValueOrDefault());

            if (theDeal.ItemDefaultDailyAmount != null)
            {
                if (boughtCount < theDeal.ItemDefaultDailyAmount.Value)
                {
                    qty = theDeal.ItemDefaultDailyAmount.Value - boughtCount;
                }
                else
                {
                    qty = 0;
                    if (theDeal.IsDailyRestriction.GetValueOrDefault())
                    {
                        message = string.Format(I18N.Message.DailyRestrictionLimitForApp, boughtCount);
                    }
                    else
                    {
                        message = string.Format(I18N.Message.AlreadyboughtcountForApp, boughtCount);
                    }
                }
            }
        }

        public static void SaveWmsOrder(Guid orderGuid)
        {
            op.WmsOrderSet(new WmsOrder()
            {
                OrderGuid = orderGuid,
                CreateTime = DateTime.Now,
                Status = (int)WmsOrderSendStatus.Init,
            });
        }


        public static bool EnablePchomeDeductingInventory(Guid businessHourGuid)
        {
            if (config.EnablePchomeDeductingInventory)
            {
                return true;
            }
            ComboDeal cd = pp.GetComboDeal(businessHourGuid);
            IList<ViewComboDeal> vcds = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(cd.MainBusinessHourGuid.GetValueOrDefault());
            List<Guid> comboDeals = vcds.Select(x => x.BusinessHourGuid).ToList();
            if (cd.MainBusinessHourGuid != null && !comboDeals.Contains(cd.MainBusinessHourGuid.Value))
            {
                comboDeals.Add(cd.MainBusinessHourGuid.Value);
            }
            if (!config.EnablePchomeDeductingInventory)
            {
                string[] bids = config.PchomeDeductingInventoryBids.Split(",");
                foreach (string bid in bids)
                {
                    Guid b = Guid.Empty;
                    Guid.TryParse(bid, out b);
                    if (b != Guid.Empty && businessHourGuid == b)
                    {
                        return true;
                    }
                    if (b != Guid.Empty && comboDeals.Contains(b))
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public static bool UpdatePchomeInventory(IViewPponDeal theDeal, ProductItem pdi)
        {
            if (theDeal.IsWms)
            {
                try
                {
                    if (!EnablePchomeDeductingInventory(theDeal.BusinessHourGuid))
                    {
                        return true;
                    }

                    List<BizLogic.Models.PchomeWarehouse.WmsInventory> inventories = PchomeWmsAPI.GetInventories(pdi.PchomeProdId);

                    BizLogic.Models.PchomeWarehouse.WmsInventory model = inventories.Where(x => x.VendorPId == pdi.Guid.ToString()).FirstOrDefault();
                    if (PchomeWmsAPI.IsDevelopment)
                    {
                        //因為pchome回傳為假資料，故若為測試，抓第一筆更新即可
                        model = inventories.FirstOrDefault();
                    }

                    if (model != null)
                    {
                        var inv = wp.WmsProductItemInventoryGet(pdi.Guid);
                        if (inv.IsLoaded == false)
                        {
                            inv = new DataOrm.WmsProductItemInventory();
                        }

                        int checking = model.Status.First(t => t.Message == "Checking").Qty;
                        int goodOnsale = model.Status.First(t => t.Message == "Good_OnSale").Qty;
                        int goodWaiting = model.Status.First(t => t.Message == "Good_Waiting").Qty;
                        int bad = model.Status.First(t => t.Message == "Bad").Qty;
                        int lost = model.Status.First(t => t.Message == "Lost").Qty;
                        int refund = model.Status.First(t => t.Message == "Refund").Qty;


                        inv.ItemGuid = pdi.Guid;
                        inv.Checking = checking;
                        inv.GoodOnsale = goodOnsale;
                        inv.GoodWaiting = goodWaiting;
                        inv.Bad = bad;
                        inv.Lost = lost;
                        inv.Refund = refund;
                        inv.ModifyTime = DateTime.Now;

                        wp.WmsProductItemInventorySet(inv);

                        ProductItemStock pdis = new ProductItemStock()
                        {
                            Guid = Guid.NewGuid(),
                            InfoGuid = pdi.InfoGuid,
                            ItemGuid = pdi.Guid,
                            BeginningStock = pdi.Stock,
                            AdjustStock = goodOnsale - pdi.Stock,
                            EndingStock = goodOnsale,
                            CreateId = "sys",
                            CreateTime = DateTime.Now,
                            ModifyId = "sys",
                            ModifyTime = DateTime.Now
                        };
                        pp.ProductItemStockSet(pdis);

                        pdi.Stock = pdi.Stock + pdis.AdjustStock;
                        pp.ProductItemSet(pdi);

                    }
                }
                catch
                {
                }
            }
            return true;
        }

        public static bool AddPchomeOrder(IViewPponDeal theDeal, Guid oid, Order o)
        {
            if (theDeal.IsWms)
            {
                if (!EnablePchomeDeductingInventory(theDeal.BusinessHourGuid))
                {
                    return true;
                }
                try
                {
                    Order dbOrder = op.OrderGet(oid);
                    if (Helper.IsFlagSet(dbOrder.OrderStatus, OrderStatus.Complete) == false)
                    {
                        return true;
                    }
                    var vwoList = wp.GetSuccessfulViewWmsOrdersByOrderGuid(oid).ToList();
                    var viewWmsOrders = vwoList.GroupBy(x => x.OrderId).FirstOrDefault();

                    List<WmsModel.WmsOrder> wmsOrders = new List<WmsModel.WmsOrder>();
                    if (viewWmsOrders != null)
                    {
                        var wmsOrder = new WmsModel.WmsOrder();
                        ViewWmsOrder viewWmsOrder = viewWmsOrders.FirstOrDefault();
                        wmsOrder.VendorOrderId = viewWmsOrder.OrderGuid.ToString("N");

                        wmsOrder.Detail = viewWmsOrders.GroupBy(g => new { g.VendorOrderNo, g.PchomeProdId })
                            .Select(v => new WmsModel.WmsOrder.OrderDetail()
                            {
                                VendorOrderNo = v.Key.VendorOrderNo.ToString(),
                                ProdId = v.Key.PchomeProdId,
                                Qty = v.Count()
                            }).ToList();

                        wmsOrder.ReceiveContact = new WmsModel.WmsPerson()
                        {
                            Name = viewWmsOrder.MemberName,
                            Tel = viewWmsOrder.PhoneNumber,
                            Mobile = viewWmsOrder.MobileNumber,
                            Email = viewWmsOrder.MemberEmail,
                            Zip = WmsFacade.GetZipCode(viewWmsOrder.DeliveryAddress),
                            Address = viewWmsOrder.DeliveryAddress,
                        };

                        wmsOrders.Add(wmsOrder);
                    }

                    var order = wmsOrders.FirstOrDefault();

                    try
                    {
                        string content;

                        if (PchomeWmsAPI.AddOrder(order, out content))
                        {
                            var wo = wp.WmsOrderGet(Guid.Parse(order.VendorOrderId));
                            wo.SyncTime = DateTime.Now;
                            wo.Status = (int)WmsOrderSendStatus.Success;
                            wo.WmsOrderId = order.Id;
                            wp.WmsOrderSet(wo);
                        }
                        else
                        {
                            var wo = wp.WmsOrderGet(Guid.Parse(order.VendorOrderId));
                            wo.SyncTime = DateTime.Now;
                            wo.Status = (int)WmsOrderSendStatus.failure;
                            wo.Message = content;
                            wp.WmsOrderSet(wo);
                            logger.WarnFormat("AddPchomeOrder add order failed. order_guid={0}, message={1}", order.VendorOrderId, content);
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.ErrorFormat("AddPchomeOrder error. order_guid={0}, ex={1}", oid.ToString(), ex);
                        return false;
                    }

                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("AddPchomeOrder error. order_guid={0}, ex={1}", oid.ToString(), ex);
                    return false;
                }
            }
            return true;
        }
    }
}

namespace LunchKingSite.BizLogic.Facade.Models
{
    public class OptionItemLimit
    {
        public string AccessoryName { get; set; }

        public string AccessoryId { get; set; }

        public int Limit { get; set; }
    }
}
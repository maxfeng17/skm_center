﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Facade
{
    public class CmsRandomFacade
    {
        private static ICmsProvider cp;
        private static ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof(CmsRandomFacade));

        static CmsRandomFacade()
        {
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        private static ViewCmsRandomCollection GetDataFromCache(string contentName)
        {
            return ((ViewCmsRandomCollection)HttpRuntime.Cache[contentName]);
        }

        private static void PutDataToCache(string contentName, int durationMinutes, ViewCmsRandomCollection data)
        {
            HttpRuntime.Cache.Remove(contentName);
            HttpRuntime.Cache.Insert(contentName, data, null, DateTime.Now.AddMinutes(durationMinutes),
                TimeSpan.Zero, CacheItemPriority.NotRemovable, null);
        }

        private static void SwitchCurationLink(ViewCmsRandomCollection collection)
        {
            if (CommonFacade.ToMobileVersion())
            {
                SwitchCurationLinkToMobileVersion(collection);
            }
        }

        private static void SwitchCurationLinkToMobileVersion(ViewCmsRandomCollection collection)
        {
            foreach (var item in collection)
            {
                item.Body = item.Body.Replace("Exhibition.aspx", "ExhibitionMobile.aspx");
            }
        }

        public static ViewCmsRandomCollection GetOrAddData(string contentName, RandomCmsType cmsType)
        {
            DateTime now = DateTime.Now;
            ViewCmsRandomCollection dataCol;
            if (HttpRuntime.Cache[contentName] == null)
            {
                // 因應宅配玩美頁面為delivery.aspx，需取代為default.aspx才可取得ViewCmsRandom資料
                dataCol = cp.GetViewCmsRandomCollection(
                    contentName.Replace("delivery", "default"), cmsType, config.RandomParagraphCacheMinuteSet);
                PutDataToCache(contentName, config.RandomParagraphCacheMinuteSet, dataCol);
                logger.DebugFormat("{0} get {1} records from db at {2:0.00}", contentName, dataCol.Count, (DateTime.Now - now).TotalSeconds);
            }
            else
            {
                dataCol = GetDataFromCache(contentName);
                logger.DebugFormat("{0} get {1} records from cache at {2:0.00}", contentName, dataCol.Count, (DateTime.Now - now).TotalSeconds);
            }

            ViewCmsRandomCollection dataColCopy = dataCol.Clone();//避免call by reference造成cache的資料被修改
            if (contentName == "curation")
            {
                SwitchCurationLink(dataColCopy);
            }
            
            return dataColCopy;
        }

        /// <summary>
        /// 目前是用來取得策展活動的內容
        /// 排序 (新的放前面)
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="cid"></param>
        /// <returns></returns>
        public static List<ViewCmsRandom> GetViewCmsRandomsByCity(RandomCmsType type, string name, int cid)
        {
            ViewCmsRandomCollection data = GetOrAddData(name, type);
            List<ViewCmsRandom> rtn = new List<ViewCmsRandom>();

            if (cid > 0)
            {
                foreach (ViewCmsRandom item in data)
                {
                    if (item.CityId == cid)
                    {
                        item.Body = ImageFacade.BindingDataByHtmlUrlString(item.Body, "&cid=" + cid);
                        rtn.Add(item);
                    }
                }
            }
            else
            {
                //首頁 Banner
                int? tempEventId = null;
                int? tempBrandId = null;
                foreach (ViewCmsRandom item in data)
                {
                    if (!(tempEventId == item.EventPromoId && tempBrandId == item.BrandId))
                    {
                        rtn.Add(item);
                    }
                    tempEventId = item.EventPromoId;
                    tempBrandId = item.BrandId;
                }
            }
            return rtn.OrderBy(t=>t.Seq).ThenBy(t => t.StartTime).ToList();
        }

        /// <summary>
        /// 取得策展子分類設定在哪些頻道顯示
        /// </summary>
        /// <param name="eventPromoId"></param>
        /// <returns></returns>
        public static List<string> GetEventPromoCityNames(int eventPromoId)
        {
            List<string> result = new List<string>();
            foreach (var item in cp.ViewCmsRandomGetCollectionByEventPromoId(eventPromoId))
            {
                result.Add(item.CityName);
            }
            return result;
        }

        /// <summary>
        /// 取得廠商品牌館設定在哪些頻道顯示
        /// </summary>
        /// <param name="brandId"></param>
        /// <returns></returns>
        public static List<string> GetBrandCityNames(int brandId)
        {
            List<string> result = new List<string>();
            foreach (var item in cp.ViewCmsRandomGetCollectionByBrandId(brandId))
            {
                result.Add(item.CityName);
            }
            return result;
        }

        public static string RenderCurationAsMobileHtml(List<ViewCmsRandom> curations)
        {
            StringBuilder sb = new StringBuilder();
            if (curations.Any())
            {
                sb.Append("<div class='market_wrap'>");
                //sb.Append("    <div class='btn_more'><a href=" + ImageFacade.GetUrlByHtmlString(curations[0].Body) + ">更多</a></div>");
                sb.Append("    <div class='btn_more'><a href='/event/exhibitionlistmobile.aspx'>更多</a></div>"); //1111結束需移除
                sb.Append("    <ul>");
                for (int i = 0; i < curations.Count; i++)
                {
                    if (i % 2 == 0)
                    {
                        sb.Append("<li>" + curations[i].Body.Substring(0, curations[i].Body.IndexOf("</a>")));
                    }
                    else
                    {
                        sb.Append(curations[i].Body.Substring(0, curations[i].Body.IndexOf("</a>")) + "</li>");
                    }
                }
                sb.Append("    </ul>");
                sb.Append("</div>");
            }

            return sb.ToString();
        }
    }
}

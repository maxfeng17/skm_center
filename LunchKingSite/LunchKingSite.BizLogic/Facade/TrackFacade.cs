﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.BizLogic.Facade
{
    public class TrackFacade
    {
        protected static IMessageQueueProvider mqp;

        static TrackFacade()
        {
            mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
        }

        public static void RecordTrack(
            string[] model, string userAgent, string userName, string rsrcCookie, Guid? visitorIdentity,
            string userIp, OrderFromType deviceType
            )
        {
            DealPageView pageView = new DealPageView
            {
                DealId = Guid.Parse(model[2]),
                UserIp = userIp,
                UserId = MemberFacade.GetUniqueId(userName),
                DeviceType = (int)deviceType,
                StartTime = DateTime.Now,
                RawUrl = model[3],
                UrlReference = model[4],
                UserAgent = userAgent,
                ServerBy = Environment.MachineName,
                VisitorIdentity = visitorIdentity,
                ReferrerSourceId = rsrcCookie
            };
            mqp.Send("DealPageView", pageView);
        }

        public static void RecordAppTrack(Guid dealId, string userName, string rawUrl, string urlReference,
            string userAgent, string rsrcCookie, Guid? visitorIdentity, string userIp, OrderFromType deviceType)
        {
            DealPageView pageView = new DealPageView
            {
                DealId = dealId,
                UserIp = userIp,
                UserId = MemberFacade.GetUniqueId(userName),
                DeviceType = (int)deviceType,
                StartTime = DateTime.Now,
                RawUrl = rawUrl,
                UrlReference = urlReference,
                UserAgent = userAgent,
                ServerBy = Environment.MachineName,
                VisitorIdentity = visitorIdentity,
                ReferrerSourceId = rsrcCookie
            };
            mqp.Send("DealPageView", pageView);
        }

        public class DealPageView
        {
            public DealPageView()
            {
                UserIp = string.Empty;
                ReferrerSourceId = string.Empty;
                UserAgent = string.Empty;
                ServerBy = string.Empty;
            }

            public Guid DealId { get; set; }

            public Guid? VisitorIdentity { get; set; }

            public int UserId { get; set; }

            public string UserIp { get; set; }

            public string ReferrerSourceId { get; set; }

            public int DeviceType { get; set; }

            public string RawUrl { get; set; }

            public string UrlReference { get; set; }

            public string UserAgent { get; set; }

            public string ServerBy { get; set; }

            public DateTime StartTime { get; set; }
        }


        #region Line Page Record

        public static void RecordLinePageRecord(Guid dealGuid, int userId, string topic)
        {


        }

        #endregion
    }
}


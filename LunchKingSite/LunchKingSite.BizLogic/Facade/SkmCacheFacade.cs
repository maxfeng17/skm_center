﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;

namespace LunchKingSite.BizLogic.Facade
{
    public class SkmCacheFacade
    {
        private static ILog logger = LogManager.GetLogger("Skm");
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISkmProvider skmMp = ProviderFactory.Instance().GetProvider<ISkmProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetProvider<ISysConfProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();

        /// <summary>
        /// 正在架上的ViewExternal
        /// </summary>
        /// <param name="onlineTimeSlotBids">再架上的BID</param>
        /// <param name="sellerGuid">分店GUID</param>
        /// <returns></returns>
        public static ViewExternalDealCollection GetOnlineViewExternalDeals(List<Guid> onlineTimeSlotBids, Guid sellerGuid)
        {
            CacheDataStatus dataStatus;
            ViewExternalDealCollection veds =
                MemoryCache2.Get<ViewExternalDealCollection>("ViewExternalDeals/" + sellerGuid.ToString(), out dataStatus);
            if (dataStatus != CacheDataStatus.OK)
            {
                veds = pp.ViewExternalDealGetListByBid(onlineTimeSlotBids);
                MemoryCache2.Set("ViewExternalDeals/" + sellerGuid.ToString(), veds, config.SkmCacheDataMinute);
            }
            logger.Info("GetOnlineViewExternalDeals.veds.dataStatus=" + dataStatus.ToString() + "/ veds.Count=" + veds.Count);

            return veds;
        }

        public static ViewExternalDeal GetOnlineViewExternalDeal(Guid bid, bool isAvailable = true)
        {
            CacheDataStatus dataStatus;
            string cacheKey = string.Format("ViewExternalDeal/{0}/{1}", bid, isAvailable);
            ViewExternalDeal ved =
                MemoryCache2.Get<ViewExternalDeal>(cacheKey, out dataStatus);
            if (dataStatus != CacheDataStatus.OK)
            {
                ved = pp.ViewExternalDealGetByBid(bid, isAvailable);
                MemoryCache2.Set(cacheKey, ved, config.SkmCacheDataMinute);
            }
            return ved;
        }

        /// <summary>
        /// 正在架上的檔次相關燒點事件
        /// </summary>
        /// <param name="onlineVeds">正在架上的檔次GUID</param>
        /// <param name="sellerGuid">分店GUID</param>
        /// <returns></returns>
        public static List<SkmBurningEvent> GetOnlineSkmBurningEvents(List<Guid> onlineVeds, Guid sellerGuid)
        {
            CacheDataStatus dataStatus;
            List<SkmBurningEvent> sbes =
                MemoryCache2.Get<List<SkmBurningEvent>>("SkmBurningEvent/" + sellerGuid.ToString(), out dataStatus);
            if (dataStatus != CacheDataStatus.OK)
            {
                sbes = skmMp.SkmBurningEventListGet(onlineVeds, false);
                MemoryCache2.Set("SkmBurningEvent/" + sellerGuid.ToString(), sbes, config.SkmCacheDataMinute);
            }
            logger.Info("GetOnlineSkmBurningEvents.sbes.dataStatus=" + dataStatus.ToString() + "/ sbes.Count=" + sbes.Count);
            return sbes;
        }

        /// <summary>
        /// 取分店下的Shoppe
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="isShoppe"></param>
        /// <returns></returns>
        public static SkmShoppeCollection GetSkmShoppesBySeller(Guid sellerGuid, bool isShoppe)
        {
            CacheDataStatus dataStatus;
            SkmShoppeCollection skmShopps =
                MemoryCache2.Get<SkmShoppeCollection>("SkmShoppe/" + sellerGuid.ToString() + "/" + isShoppe, out dataStatus);
            if (dataStatus != CacheDataStatus.OK)
            {
                skmShopps = sp.SkmShoppeCollectionGetBySeller(sellerGuid, isShoppe);
                MemoryCache2.Set("SkmShoppe/" + sellerGuid.ToString() + "/" + isShoppe, skmShopps, config.SkmCacheDataMinute);
            }
            logger.Info("GetSkmShoppesBySeller.skmShopps.dataStatus=" + dataStatus.ToString() + "/ skmShopps.Count=" + skmShopps.Count);
            return skmShopps;
        }

        /// <summary>
        /// 取各分店Seller
        /// </summary>
        /// <returns></returns>
        public static SkmShoppeCollection GetSkmSellers()
        {
            CacheDataStatus dataStatus;
            SkmShoppeCollection skmSellers =
                MemoryCache2.Get<SkmShoppeCollection>("SkmSellers", out dataStatus);
            if (dataStatus != CacheDataStatus.OK)
            {
                skmSellers = sp.SkmShoppeCollectionGetAllSeller(false);
                MemoryCache2.Set("SkmSellers", skmSellers, config.SkmCacheDataMinute);
            }
            logger.Info("GetSkmSellers.skmSellers.dataStatus=" + dataStatus.ToString() + "/ skmShopps.Count=" + skmSellers.Count);
            return skmSellers;
        }
        
        /// <summary>
        /// 取得檔次櫃位列表<快取>
        /// </summary>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public static List<SkmPponStore> GetSkmPponStoreListFromCache(Guid businessHourGuid)
        {
            string cacheKey = "skm://SkmPponStore/" + businessHourGuid;
            List<SkmPponStore> items =
                System.Runtime.Caching.MemoryCache.Default.Get(cacheKey)
                    as List<SkmPponStore>;
            if (items == null)
            {
                items = SkmFacade.GetSkmPponStoreList(businessHourGuid);
                System.Runtime.Caching.MemoryCache.Default.Set(cacheKey, items, DateTime.Now.AddMinutes(5));
            }
            return items;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using log4net;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.BizLogic.Facade
{
    public static class DealTypeFacade
    {
        private static ISystemProvider _sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ISerializer serializer = ProviderFactory.Instance().GetSerializer();
        private static ILog logger = LogManager.GetLogger(typeof(DealTypeFacade));

        const string googleProductCategoryUrl = "https://www.google.com/basepages/producttype/taxonomy-with-ids.en-US.txt";

        public const string _CODE_GROUP_DEAL_TYPE = "DealType";
        public const string _CODE_GROUP_DEAL_TYPE_NAME = "好康上檔分類";

        public static List<DealTypeNode> GetDealTypeNodes(bool enabledOnly = false)
        {
            var items = _sp.SystemCodeGetListByCodeGroup("DealType");
            List<SystemCode> filteredItems = items.ToList();
            if (enabledOnly)
            {
                filteredItems = filteredItems.Where(t => t.Enabled == true).ToList();
            }
            // 必需很特意的排除 其它宅配 1999
            List<DealTypeNode> categories = DealTypeNode.ConvertToNestedNodes(filteredItems)
                .Where(t => t.CodeId != 1999).ToList();

            return categories;
        }
        public static Dictionary<int, string> GetAllGoogleCodes()
        {
            Dictionary<int, string> googleCategoryDict = 
                MemoryCache2.Get<Dictionary<int, string>>("GoogleProductCategory");
            if (googleCategoryDict == null)
            {
                googleCategoryDict = new Dictionary<int, string>();
                try
                {
                    //byte[] buff = new System.Net.WebClient().DownloadData(googleProductCategoryUrl);
                    string gsaFilePath = Helper.MapPath("~/share/googleProductCategoryCodes.txt");
                    byte[] buff = File.ReadAllBytes(gsaFilePath);
                    string[] lines = Encoding.UTF8.GetString(buff)
                        .Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string line in lines)
                    {
                        int id;
                        string title;

                        string[] parts = line.Split(" - ");
                        if (parts.Length == 2)
                        {
                            if (int.TryParse(parts[0], out id) && string.IsNullOrWhiteSpace(parts[1]) == false)
                            {
                                title = parts[1].Trim();
                                if (googleCategoryDict.ContainsKey(id) == false)
                                {
                                    googleCategoryDict.Add(id, title);
                                }
                            }
                        }
                    }
                    MemoryCache2.Set("GoogleProductCategory", googleCategoryDict, 60);
                }
                catch (Exception ex)
                {
                    logger.Error("無法取得或解析google的分類定義檔, " + googleProductCategoryUrl, ex);
                    throw;
                }
            }
            return googleCategoryDict;
        }
        public static bool CheckInGoogleCategory(int googleCategoryId)
        {
            return GetAllGoogleCodes().ContainsKey(googleCategoryId);
        }

        public static void GetConfirmedLog(List<DealTypeNode> newCategories, List<DealTypeNode> categories, 
            List<ProductCategoryModifiedLog> logs, out List<string> messages)
        {
            messages = new List<string>();
            List<DealTypeNode> flatedNewCategories = DealTypeNode.ConvertToFlatNodes(newCategories);
            foreach (var newCagegory in flatedNewCategories)
            {
                string message;
                var modifiedStatus = newCagegory.CheckModifiedStatus(categories, out message);

                //檢查GSA碼有沒有填到不存在的
                //如果GSA不存在，視為有問題的資料跳過不處理
                if (string.IsNullOrEmpty(newCagegory.GoogleCode) == false)
                {
                    int gsaCode;
                    if (int.TryParse(newCagegory.GoogleCode, out gsaCode) == false ||
                        CheckInGoogleCategory(gsaCode) == false)
                    {
                        modifiedStatus = ModifiedStatusType.Error;
                        messages.Add(string.Format("{0}{1} 找不到GSA碼: {2}",
                            newCagegory.CodeId == 0 ? string.Empty : newCagegory.CodeId.ToString(), 
                            newCagegory.CodeName, newCagegory.GoogleCode));
                    }
                }

                if (modifiedStatus != ModifiedStatusType.None)
                {
                    logs.Add(new ProductCategoryModifiedLog
                    {
                        Target = newCagegory,
                        Action = modifiedStatus
                    });
                }
            }
        }

        public static List<DealTypeNode> GetDealTypeNodesFromExcel(
            Stream inputStream)
        {
            List<DealTypeNode> result = new List<DealTypeNode>();

            Workbook workbook;
            try
            {
                workbook = new HSSFWorkbook(inputStream);
            }
            catch (Exception ex)
            {
                throw new Exception("無法解析xls");
            }
            var sheet = workbook.GetSheetAt(0);
            if (sheet == null)
            {
                throw new Exception("找不到sheet");
            }

            DealTypeNode[] relativeParents = new DealTypeNode[4];
            for (int i = sheet.FirstRowNum + 1; i <= sheet.LastRowNum; i++)
            {
                var row = sheet.GetRow(i);

                for (int lvl = 0; lvl <= 3; lvl++)
                {
                    int x0 = lvl * 3;
                    int x1 = lvl * 3 + 1;
                    int x2 = lvl * 3 + 2;
                    if (x2 > row.LastCellNum)
                    {
                        continue;
                    }
                    Cell cell0 = row.GetCell(x0);
                    Cell cell1 = row.GetCell(x1);
                    Cell cell2 = row.GetCell(x2);

                    if (cell1 == null || string.IsNullOrEmpty(cell1.ToString()))
                    {
                        continue;
                    }

                    string sid = cell0 == null ? string.Empty : cell0.ToString();
                    string name = cell1 == null ? string.Empty : cell1.ToString();
                    string scode = cell2 == null ? string.Empty : cell2.ToString();

                    int id = 0;
                    int code = 0;
                    //check valid row
                    if (string.IsNullOrEmpty(sid) == false && int.TryParse(sid, out id) == false)
                    {
                        continue;
                    }
                    if (string.IsNullOrEmpty(scode) == false && int.TryParse(scode, out code) == false)
                    {
                        continue;
                    }

                    DealTypeNode category = null;
                    if (relativeParents[lvl] != null && relativeParents[lvl].RoughMatch(id, name))
                    {
                        category = relativeParents[lvl];
                    }
                    else
                    {
                        category = new DealTypeNode();
                        category.CodeId = id;
                        category.CodeName = name;
                        category.GoogleCode = code == 0 ? string.Empty : code.ToString();
                        category.Enabled = true;
                    }

                    if (lvl == 0)
                    {
                        category.Parent = null;
                        result.Add(category);
                    }
                    else
                    {
                        var parentNode = relativeParents[lvl - 1];
                        category.Parent = parentNode;
                        category.ParentCodeId = parentNode.CodeId;
                        if (parentNode.Children.FirstOrDefault(
                            t => t.RoughMatch(category.CodeId, category.CodeName)) == null)
                        {
                            parentNode.Children.Add(category);
                        }
                    }
                    relativeParents[lvl] = category;

                }

            }
            return result;
        }

        public static int SaveDirectly(string id, string title, string googleCode, bool enabled, 
            IEnumerable<string> childLines, string createId, DateTime now)
        {
            int affectedCount = 0;

            var systemCode = _sp.SystemCodeGetByCodeGroupId(_CODE_GROUP_DEAL_TYPE, int.Parse(id));
            if (systemCode.CodeId == 0)
            {
                throw new Exception("It doesn't allow insert data at SaveDirectly");
            }
            int gsaCode;
            if (string.IsNullOrEmpty(googleCode) == false &&
                (int.TryParse(googleCode, out gsaCode) == false || CheckInGoogleCategory(gsaCode) == false))
            {
                throw new Exception("不存在的GSA: " + googleCode);
            }
            bool categoryWasChanged = systemCode.CodeName != title || 
                (systemCode.Code ?? string.Empty) != (googleCode ?? string.Empty) || 
                systemCode.Enabled != enabled;
            systemCode.CodeName = title;
            systemCode.Code = googleCode;
            systemCode.Enabled = enabled;
            systemCode.ModifyId = createId;
            systemCode.ModifyTime = now;

            if (categoryWasChanged)
            {
                affectedCount += _sp.SystemCodeSet(systemCode);
            }

            if (childLines == null)
            {
                return affectedCount;
            }
            int chId;
            string chTitle = string.Empty;
            string chGoogleCode = string.Empty;
            char seperatorChar;
            foreach (string line in childLines)
            {
                if (line.Contains(','))
                {
                    seperatorChar = ',';
                } else
                {
                    seperatorChar = ' ';
                }
                bool wasChanged = false;
                string[] childData = line.Split(seperatorChar);
                if (childData.Length == 0 || childData.Length > 3)
                {
                    continue;
                }
                //如果只有填一筆資料，預期是新增，所以CodeName不能為數字
                bool hasChId = int.TryParse(childData[0], out chId);
                if (childData.Length == 1)
                {
                    if (hasChId)
                    {
                        continue;
                    }
                    else
                    {
                        //新增模式
                        chTitle = childData[0];
                    }
                }
                else if (childData.Length == 2)
                {
                    if (hasChId)
                    {
                        chTitle = childData[1];
                    }
                    else
                    {
                        //新增模式
                        chTitle = childData[0];
                        chGoogleCode = childData[1];
                    }
                }
                else if (childData.Length == 3)
                {
                    if (hasChId == false)
                    {
                        continue;
                    }
                    //修改模式
                    chTitle = childData[1];
                    chGoogleCode = childData[2];
                }
                if (chTitle != string.Empty)
                {
                    chTitle = chTitle.Trim();
                }
                if (chGoogleCode != string.Empty)
                {
                    chGoogleCode = chGoogleCode.Trim();
                }
                if (string.IsNullOrEmpty(chTitle))
                {
                    continue;
                }
                int chGsaCode;
                if (string.IsNullOrEmpty(chGoogleCode) == false &&
                    (int.TryParse(chGoogleCode, out chGsaCode) == false || CheckInGoogleCategory(chGsaCode) == false))
                {
                    throw new Exception("不存在的GSA: " + chGsaCode);
                }

                SystemCode chShsystemCode = null;
                if (chId == 0)
                {
                    SystemCodeCollection childDeaTypes = _sp.SystemCodeGetListByParentId(_CODE_GROUP_DEAL_TYPE, systemCode.CodeId);
                    SystemCode childDeaType = childDeaTypes.FirstOrDefault(t => t.CodeName == chTitle);
                    if (childDeaType != null)
                    {
                        chId = childDeaType.CodeId;
                    }
                }
                if (chId == 0)
                {
                    chShsystemCode = new SystemCode();
                    chShsystemCode.CodeGroup = _CODE_GROUP_DEAL_TYPE;
                    chShsystemCode.CodeGroupName = _CODE_GROUP_DEAL_TYPE_NAME;
                    chShsystemCode.CodeId = _sp.GetNextCodeId(_CODE_GROUP_DEAL_TYPE);
                    chShsystemCode.CodeName = chTitle;
                    chShsystemCode.ParentCodeId = systemCode.CodeId;
                    chShsystemCode.Code = chGoogleCode;
                    chShsystemCode.Enabled = true;
                    chShsystemCode.CreateId = createId;
                    chShsystemCode.CreateTime = now;
                    wasChanged = true;
                }
                else
                {
                    chShsystemCode = _sp.SystemCodeGetByCodeGroupId(_CODE_GROUP_DEAL_TYPE, chId);
                    if (chShsystemCode.ParentCodeId != systemCode.CodeId)
                    {
                        continue;
                    }
                    wasChanged = chShsystemCode.CodeName != chTitle || 
                        (chShsystemCode.Code ?? string.Empty) != (chGoogleCode ?? string.Empty);
                    chShsystemCode.CodeName = chTitle;
                    chShsystemCode.Code = chGoogleCode;
                    chShsystemCode.ModifyId = createId;
                    chShsystemCode.ModifyTime = now;
                }
                if (wasChanged)
                {
                    affectedCount += _sp.SystemCodeSet(chShsystemCode);
                }
            }
            return affectedCount;
        }

        public static int SaveDealTypeNodes(List<DealTypeNode> productCategories, string createId, DateTime now)
        {
            int result = 0;
            foreach (var productCategory in productCategories)
            {
                result = result + SaveDealTypeNodes(productCategory, createId, now);
            }
            return result;
        }

        public static int SaveDealTypeNodes(DealTypeNode productCategory, string createId, DateTime now)
        {
            int rowCount = 0;

            if (productCategory.CodeId == 0)
            {
                productCategory.CodeId = _sp.GetNextCodeId(_CODE_GROUP_DEAL_TYPE);
            }

            var systemCode = _sp.SystemCodeGetByCodeGroupId(_CODE_GROUP_DEAL_TYPE, productCategory.CodeId);

            if (systemCode.IsLoaded == false)
            {
                systemCode = new DataOrm.SystemCode
                {
                    CodeId = productCategory.CodeId,
                    CodeName = productCategory.CodeName,
                    Seq = productCategory.Seq,
                    Enabled = productCategory.Enabled,
                    ShortName = productCategory.CodeName,
                    ExpiredTime = null,
                    ParentCodeId = productCategory.Parent == null ? (int?)null : productCategory.Parent.CodeId,
                    CreateId = createId,
                    CreateTime = now,
                    ModifyId = null,
                    ModifyTime = null,
                    Code = productCategory.GoogleCode,
                    CodeGroup = _CODE_GROUP_DEAL_TYPE,
                    CodeGroupName = _CODE_GROUP_DEAL_TYPE_NAME
                };
                rowCount = _sp.SystemCodeSet(systemCode);
            }
            else
            {
                systemCode.CodeName = productCategory.CodeName;
                systemCode.Seq = productCategory.Seq;
                systemCode.Enabled = productCategory.Enabled;
                systemCode.ShortName = productCategory.CodeName;
                systemCode.ParentCodeId = productCategory.Parent == null ? (int?)null : productCategory.Parent.CodeId;
                systemCode.ModifyId = createId;
                systemCode.ModifyTime = now;
                systemCode.Code = productCategory.GoogleCode;
                rowCount = _sp.SystemCodeSet(systemCode);
            }
            return rowCount;
        }

        public static void ExportExcelToMemoryStream(MemoryStream ms, List<DealTypeNode> categories)
        {
            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("產品分類");
            var columnRow = sheet.CreateRow(0);
            columnRow.CreateCell(0).SetValue("分類1SID");
            columnRow.CreateCell(1).SetValue("分類1");
            columnRow.CreateCell(2).SetValue("分類1GSA");
            columnRow.CreateCell(3).SetValue("分類2SID");
            columnRow.CreateCell(4).SetValue("分類2");
            columnRow.CreateCell(5).SetValue("分類2GSA");
            columnRow.CreateCell(6).SetValue("分類2SID");
            columnRow.CreateCell(7).SetValue("分類3");
            columnRow.CreateCell(8).SetValue("分類3GSA");
            columnRow.CreateCell(9).SetValue("分類4SID");
            columnRow.CreateCell(10).SetValue("分類4");
            columnRow.CreateCell(11).SetValue("分類4GSA");

            sheet.SetColumnWidth(0, 4 * 512);
            sheet.SetColumnWidth(1, 12 * 512);
            sheet.SetColumnWidth(2, 4 * 512);
            sheet.SetColumnWidth(3, 4 * 512);
            sheet.SetColumnWidth(4, 12 * 512);
            sheet.SetColumnWidth(5, 4 * 512);
            sheet.SetColumnWidth(6, 4 * 512);
            sheet.SetColumnWidth(7, 12 * 512);
            sheet.SetColumnWidth(8, 4 * 512);
            sheet.SetColumnWidth(9, 4 * 512);
            sheet.SetColumnWidth(10, 12 * 512);
            sheet.SetColumnWidth(11, 4 * 512);
            var style1 = workbook.CreateCellStyle();            
            style1.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.LEMON_CHIFFON.index;

            sheet.SetDefaultColumnStyle(0, style1);
            sheet.SetDefaultColumnStyle(1, style1);
            sheet.SetDefaultColumnStyle(2, style1);
            sheet.SetDefaultColumnStyle(6, style1);
            sheet.SetDefaultColumnStyle(7, style1);
            sheet.SetDefaultColumnStyle(8, style1);

            int idx = 1;

            Action<List<DealTypeNode>, int> renderSheetAction = null;
            renderSheetAction = delegate (List<DealTypeNode> argCategories, int lvl)
            {
                foreach (DealTypeNode category in argCategories)
                {
                    if (category.Enabled == false)
                    {
                        continue;
                    }
                    var row = sheet.CreateRow(idx);
                    Cell cell0 = row.CreateCell(lvl * 3);
                    cell0.SetValue(category.CodeId);
                    Cell cell1 = row.CreateCell(lvl * 3 + 1);
                    cell1.SetValue(category.CodeName);
                    Cell cell2 = row.CreateCell(lvl * 3 + 2);
                    cell2.SetValue(category.GoogleCode);
                    idx++;
                    renderSheetAction(category.Children, lvl + 1);
                }
            };
              

            
            renderSheetAction(categories, 0);



            workbook.Write(ms);
        }

        public static DealTypeNode Find(List<DealTypeNode> categories, int codeId)
        {
            DealTypeNode result = null;
            foreach (var category in categories)
            {
                if (category.CodeId == codeId)
                {
                    result = category;
                }
                else if (result == null && category.Children.Count > 0)
                {
                    result = Find(category.Children, codeId);
                }
            }
            return result;
        }

        public static List<int> FindAllNodeCodeId(DealTypeNode node, Guid bid)
        {
            try
            {
                List<int> dealTypes = new List<int>();

                List<DealTypeNode> parents = node.GetParentsContainSelf();
                foreach (DealTypeNode parent in parents)
                {
                    if (parent.Enabled)
                    {
                        dealTypes.Add(parent.CodeId);
                    }
                    else
                    {
                        return dealTypes;
                    }
                }
                
                return dealTypes;
            }
            catch (Exception ex)
            {
                logger.Error(bid.ToString(), ex);
                return new List<int>();
            }
            
        }

        public static int? GetLvl2ParentValue(int? codeId)
        {
            if (codeId == null)
            {
                return null;
            }
            List<DealTypeNode> allNodes = DealTypeFacade.GetDealTypeNodes();
            var node = Find(allNodes, codeId.Value);
            if (node == null || node.Level == 1)
            {
                return 0;
            }
            if (node.Level == 2)
            {
                return node.CodeId;
            }
            
            var parentLvl2Node = node.GetParents().FirstOrDefault(t=>t.Level == 2);
            if (parentLvl2Node == null)
            {
                return null;
            }
            return parentLvl2Node.CodeId;
        }
        /// <summary>
        /// 檢查本身與父節點們，是否都設定可顯示。
        /// </summary>
        /// <param name="codeId"></param>
        /// <returns></returns>
        public static bool CheckSelfAndParentsAllEnabled(int codeId)
        {
            if (codeId == 0)
            {
                return false;
            }
            List<DealTypeNode> allNodes = DealTypeFacade.GetDealTypeNodes();
            var node = Find(allNodes, codeId);
            if (node == null || node.Enabled == false)
            {
                return false;
            }
            var parentNodes = node.GetParents();
            foreach(var parentNode in parentNodes)
            {
                if (parentNode.Enabled == false)
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 取得最接近且未設隱藏的節點CodeId
        /// 即Enabled = false
        /// </summary>
        /// <param name="codeId"></param>
        /// <returns></returns>
        public static int GetClosestNode(int codeId)
        {
            if (codeId == 0)
            {
                return 0;
            }
            List<DealTypeNode> allNodes = DealTypeFacade.GetDealTypeNodes();
            var theNode = Find(allNodes, codeId);
            if (theNode == null)
            {
                return 0;
            }

            int closestCodeId = 0;

            List<DealTypeNode> nodes = new List<DealTypeNode>();
            nodes.AddRange(theNode.GetParents());
            nodes.Add(theNode);
            foreach(var node in nodes)
            {
                if (node.Enabled == false)
                {
                    break;
                }
                closestCodeId = node.CodeId;
            }
            return closestCodeId;
        }

        public static int GetClosestParentNode(int codeId)
        {
            if (codeId == 0)
            {
                return 0;
            }
            List<DealTypeNode> allNodes = DealTypeFacade.GetDealTypeNodes();
            var theNode = Find(allNodes, codeId);
            if (theNode == null)
            {
                return 0;
            }

            int closestCodeId = 0;

            List<DealTypeNode> nodes = new List<DealTypeNode>();
            nodes.AddRange(theNode.GetParents());
            nodes.Add(theNode);
            int? rootId = null;
            foreach (var node in nodes)
            {
                if (node.Enabled == false)
                {
                    break;
                }
                if(node.Parent == null)
                {
                    rootId = node.CodeId;
                }
                if(rootId != null)
                {
                    if(node.ParentCodeId == rootId)
                    {
                        closestCodeId = node.CodeId;
                        break;
                    }
                }
            }
            return closestCodeId;
        }

        public static int GetRootNode(int codeId)
        {
            List<DealTypeNode> allNodes = DealTypeFacade.GetDealTypeNodes();
            var theNode = Find(allNodes, codeId);
            if (theNode == null)
            {
                return 0;
            }

            return theNode.GetParents()[0].CodeId;
        }
    }
}

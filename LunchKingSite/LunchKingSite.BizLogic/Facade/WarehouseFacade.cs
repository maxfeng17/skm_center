﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.BizLogic.Facade
{
    public class WarehouseFacade
    {
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(WarehouseFacade));
        /// <summary>
        /// 先處理單一規格
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public static int GetQtyInStock(Guid bid)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            //無多重選項
            if (deal != null && deal.IsHouseDealNewVersion())
            {
                ProposalMultiDeal multiDeal = sp.ProposalMultiDealGetAllByBid(deal.BusinessHourGuid);
                if (multiDeal.IsLoaded)
                {
                    List<ProposalMultiDealsSpec> specs =
                        new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(multiDeal.Options);
                    if (specs == null)
                    {
                        string errorMessage = string.Format("{0} 沒有設定產品選項，購買失敗。", deal.BusinessHourGuid);
                        logger.Error(errorMessage);
                        throw new Exception(errorMessage);
                    }

                    ProposalMultiDealsSpec spc = specs.FirstOrDefault();
                    if (spc != null)
                    {
                        Guid itemGuid = spc.Items.FirstOrDefault().item_guid;
                        ProductItem pdi = pp.ProductItemGet(itemGuid);
                        if (pdi == null && pdi.IsLoaded == false)
                        {
                            string msg = string.Format("找不到商品選項 itemGuid={0} ", itemGuid);
                            logger.Warn(msg);
                            throw new Exception(msg);
                        }
                        return pdi.Stock / deal.QuantityMultiplier.GetValueOrDefault(1);
                    }
                }
            }            
            return 0;
        }

        public static List<BuyPorductOptionGroup> GetProductOptionQty(Guid bid)
        {
            List<BuyPorductOptionGroup> result = new List<BuyPorductOptionGroup>();
            int stock = 0;
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);

            foreach (var opt in pp.PponOptionGetList(bid).OrderBy(t=>t.CatgSeq).OrderBy(t => t.OptionSeq))
            {
                if (opt.IsLoaded == false || opt.ItemGuid == null || opt.IsDisabled)
                {
                    continue;
                }
                ProductItem pdi = pp.ProductItemGet(opt.ItemGuid.Value);
                BuyPorductOptionGroup grp = result.FirstOrDefault(t => t.Title == opt.CatgName);
                if (grp == null)
                {
                    grp = new BuyPorductOptionGroup();
                    grp.Title =  opt.CatgName;
                    grp.Items = new List<BuyPorductOption>();
                    result.Add(grp);
                }
                grp.Items.Add(new BuyPorductOption { Title = opt.OptionName, Stock = pdi.Stock, Id = pdi.Guid.ToString() });
            }            
            return result;
        }
    }

    public class BuyPorductOptionGroup
    {
        public string Title { get; set; }
        public List<BuyPorductOption> Items { get; set; }
    }

    public class BuyPorductOption
    {
        public string Title { get; set; }
        public string Id { get; set; }
        public int Stock { get; set; }
    }
}

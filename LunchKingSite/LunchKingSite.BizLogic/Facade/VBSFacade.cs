﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Amazon.IdentityManagement.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Enumeration;
using PCP = LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.VBS;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Component.API;
using System.Transactions;
using System.Web;
using System.IO;

namespace LunchKingSite.BizLogic.Facade
{
    public class VBSFacade
    {
        private static IPCPProvider _pcp;
        private static IMemberProvider _memberProvider;
        private static ISysConfProvider _config;
        private static IVerificationProvider _vp;
        private static IMGMProvider _mgmp;
        private static IOrderProvider _op;

        private static IVbsProvider _vbs;
        private static ISellerProvider _sp;

        static VBSFacade()
        {
            _pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            _memberProvider = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            _vbs = ProviderFactory.Instance().GetProvider<IVbsProvider>();
            _mgmp = ProviderFactory.Instance().GetProvider<IMGMProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        }

        #region VbsMemberShipBindAccount
        /// <summary>
        /// 綁定/解除綁定  自訂帳號(利用17life會員登入)
        /// </summary>
        /// <param name="bindAccount">帳號/手機號碼</param>
        /// <param name="isBind">綁定/解除綁定</param>
        /// <param name="vbsAccountId">商家帳號</param>
        /// <param name="errorMsg">錯誤訊息回傳</param>
        /// <returns></returns>
        public static bool BindAccount(string bindAccount, bool isBind, string vbsAccountId, out VbsBindAccountErrorType errorType)
        {
            errorType = VbsBindAccountErrorType.NONE;
            try
            {
                if (isBind)//預設為綁定帳號
                {
                    //做帳號/手機號碼檢查
                    if (CheckBindAccountIsCorrect(bindAccount))
                    {
                        //帳號重覆檢查
                        if (!CheckBindAccountIsRepeat(bindAccount))
                        {
                            _memberProvider.VbsMemebershipBindAccountUpdateAllDelete(vbsAccountId, 0);//將其失效
                            _memberProvider.VbsMembershipBindAccountSet(new VbsMembershipBindAccount()
                            {
                                //綁定一筆新的
                                IsDelete = false,
                                UserId = GetBindAccountUserIdByEmailOrMobile(bindAccount),
                                VbsAccountId = vbsAccountId,
                                CreateTime = DateTime.Now,
                                ModifyTime = DateTime.Now,
                                BindType = bindAccount.Contains("@") ? (int)VbsBindAccountType.Email : (int)VbsBindAccountType.MobileNumber
                            });
                        }
                        else
                        {
                            errorType = VbsBindAccountErrorType.AccountIsRepeat;
                        }
                    }
                    else
                    {
                        errorType = VbsBindAccountErrorType.AccountIsNotExist;
                    }
                }
                else //解除綁定
                {
                    _memberProvider.VbsMemebershipBindAccountUpdateAllDelete(vbsAccountId, 0);
                }

            }
            catch (Exception e)
            {
                errorType = VbsBindAccountErrorType.ElseException;
            }

            return errorType == VbsBindAccountErrorType.NONE;
        }

        /// <summary>
        /// 檢查17life會員帳號是否重複綁定
        /// </summary>
        /// <param name="bindAccount">可能是</param>
        /// <returns></returns>
        public static bool CheckBindAccountIsRepeat(string bindAccount)
        {
            var data = _memberProvider.VbsMembershipBindAccountGetByUserId(GetBindAccountUserIdByEmailOrMobile(bindAccount));
            return data.IsLoaded;
        }

        /// <summary>
        /// 檢查17Life會員帳號是否正確
        /// </summary>
        /// <param name="bindAccount"></param>
        /// <returns></returns>
        public static bool CheckBindAccountIsCorrect(string bindAccount)
        {
            if (bindAccount.Contains("@"))//user_name
            {
                return _memberProvider.MemberGetByUserName(bindAccount).IsLoaded;
            }
            else//mobile
            {
                return _memberProvider.MobileMemberGet(bindAccount).IsLoaded;
            }
        }


        /// <summary>
        /// 藉由綁定的UserId找到綁定的帳號(Mobile/Email)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="bindType">當初綁定的是email/mobileNumber</param>
        /// <returns></returns>
        public static string GetVbsBindAccountEmailOrMobileByUserId(int userId, VbsBindAccountType bindType)
        {
            if (bindType == VbsBindAccountType.Email)//user_name
            {
                return _memberProvider.MemberGetbyUniqueId(userId).UserName;
            }
            else
            {
                return _memberProvider.MobileMemberGet(userId).MobileNumber;
            }
        }

        /// <summary>
        /// 藉由商家帳號找到綁定的帳號(Mobile/Email)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="bindType">當初綁定的是email/mobileNumber</param>
        /// <returns></returns>
        public static void GetVbsBindAccountEmailOrMobileByVbsAccount(string vbsAccount, out string email, out string mobile)
        {
            email = string.Empty;
            mobile = string.Empty;
            var d = _memberProvider.VbsMembershipBindAccountGetByVbsAccountId(vbsAccount);
            switch (d.BindType)
            {
                case (int)VbsBindAccountType.Email://當初bind的是Email
                    var m = _memberProvider.MemberGetbyUniqueId(d.UserId);
                    email = m.UserName;
                    mobile = m.Mobile;
                    //mobile =_memberProvider.MobileMemberGet(d.UserId).MobileNumber;
                    break;
                case (int)VbsBindAccountType.MobileNumber:
                    email = _memberProvider.MemberGetbyUniqueId(d.UserId).UserName;
                    mobile = _memberProvider.MobileMemberGet(d.UserId).MobileNumber;
                    break;
            }
        }

        /// <summary>
        /// 藉由電話或email找到userId
        /// </summary>
        /// <param name="bindAccount"></param>
        /// <returns></returns>
        public static int GetBindAccountUserIdByEmailOrMobile(string bindAccount)
        {
            int userId = 0;
            if (bindAccount.Contains("@"))//email
            {
                userId = _memberProvider.MemberGetByUserName(bindAccount).UniqueId;

            }
            else//mobile
            {
                userId = _memberProvider.MobileMemberGet(bindAccount).UserId;
            }
            return userId;
        }


        /// <summary>
        /// 找尋綁定資料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static VbsMembershipBindAccount VbsMembershipBindAccountGet(string bindAccount)
        {
            return _memberProvider.VbsMembershipBindAccountGetByUserId(GetBindAccountUserIdByEmailOrMobile(bindAccount));
        }

        #endregion

        /// <summary>
        /// 傳入商家的會員編號(member.unique_id)
        /// 分店需為顯示狀態
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static SellerCollection GetVBSMemberAllowStoreList(int userId)
        {
            var result = new SellerCollection();

            foreach (var item in _pcp.AllowStoreGetList(userId))
            {
                if (item.StoreStatus == (int)StoreStatus.Available)
                {
                    result.Add(item);
                }
            }

            return result;
        }

        public static VbsMembership GetVbsMembershipByUserId(int userId)
        {
            return _memberProvider.VbsMembershipGetByUserId(userId);
        }
        /// <summary>
        /// 依據生日月份搜尋商家自建會員，查無資料或傳入參數錯誤時，回傳空的collection
        /// </summary>
        /// <param name="sellerUserId"></param>
        /// <param name="month"></param>
        /// <param name="orderBy">排序條件</param>
        /// <returns></returns>
        public static SellerMemberCollection GetSellerMemberByBirthMonth(int sellerUserId, int month)
        {
            if (month < 1 || month > 12)
            {
                return new SellerMemberCollection();
            }
            return _pcp.SellerMemberGetByBirthMonth(sellerUserId, month);
        }

        #region seller

        public static bool TryUploadSellerMember(int cardId, HttpPostedFileBase FileUpload, string userName, out string msg)
        {
            var result = false;
            msg = "";

            int sellerUserId = _pcp.ViewMembershipCardGet(cardId).SellerUserId;

            StreamReader reader = new StreamReader(FileUpload.InputStream, System.Text.Encoding.Default);

            var sellerMemberColl = new SellerMemberCollection();
            var mobileList = _pcp.SellerMemberGet(new string[] { }).Select(x => x.Mobile).ToList<string>();

            int i = 0;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                //pass header
                if (++i == 1) { continue; }

                var values = line.Split(',');

                //會員姓氏,會員名字,會員手機（必填）,會員 Email,會員性別（男 or 女）,會員生日_年分,會員生日_月份,會員生日_日期,備註
                //王,1,0917389473,test123@17life.com,男,,12,24,
                var lastName = values[0];
                var firstName = values[1];

                int tmp;
                if (!int.TryParse(values[2], out tmp))
                {
                    msg += string.Format("第{0}列的手機有誤:{1}\n", i, values[2]);
                    continue;
                }
                if (mobileList.Any(x => x == values[2]))
                {
                    msg += string.Format("第{0}列的手機重複:{1}\n", i, values[2]);
                    continue;
                }

                var phone = values[2];
                mobileList.Add(phone);

                var email = values[3];

                int gender = (int)GenderType.Unknown;
                if (values[4] == "男" || values[4] == "1")
                {
                    gender = (int)GenderType.Male;
                }
                else if (values[4] == "女" || values[4] == "2")
                {
                    gender = (int)GenderType.Female;
                }

                string sYYYY = values[5], sMM = values[6], sDD = values[7];
                int iYYYY = 1753, iMM = 1, iDD = 1;
                if (sYYYY.Length == 2)
                {
                    if (!int.TryParse(sYYYY, out iYYYY))
                    {
                        msg += string.Format("第{0}列的生日年份有誤:{1}\n", i, sYYYY);
                        continue;
                    }
                    iYYYY = iYYYY + 1911;
                }
                else if (sYYYY.Length == 4)
                {
                    if (!int.TryParse(sYYYY, out iYYYY))
                    {
                        msg += string.Format("第{0}列的生日年份有誤:{1}\n", i, sYYYY);
                        continue;
                    }
                }

                if (!string.IsNullOrWhiteSpace(sMM))
                {
                    if (!int.TryParse(sMM, out iMM))
                    {
                        msg += string.Format("第{0}列的生日月份有誤:{1}\n", i, sMM);
                        continue;
                    }
                }

                if (!string.IsNullOrWhiteSpace(sDD))
                {
                    if (!int.TryParse(sDD, out iDD))
                    {
                        msg += string.Format("第{0}列的生日日期有誤:{1}\n", i, sDD);
                        continue;
                    }
                }

                var memBirth = new DateTime(iYYYY, iMM, iDD);


                var memo = values[8];

                var mem = new SellerMember
                {
                    SellerUserId = sellerUserId,
                    FirstName = firstName,
                    LastName = lastName,
                    Mobile = phone,
                    ContactEmail = email,
                    Gender = gender,
                    Birthday = memBirth,
                    Remarks = memo,
                    CreateTime = DateTime.Now,
                    CreateId = MemberFacade.GetUniqueId(userName),
                };

                sellerMemberColl.Add(mem);
            }

            result = _pcp.SellerMemberSet(sellerMemberColl) != 0;

            return result;
        }

        #region seller image

        public static bool DeleteSellerImage(int sellerUserId, string editUser, int imageId, PcpImageType imageType)
        {
            int editUserId;

            if (!int.TryParse(editUser, out editUserId))
            {
                var member = _memberProvider.MemberGet(editUser);
                if (!member.IsLoaded) { return false; }

                editUserId = member.UniqueId;
            }

            bool result = true;
            if (imageType == PcpImageType.None && imageId > 0)
            {
                result = _pcp.PcpImageDelete(imageId, editUserId);
            }
            else if (imageType != PcpImageType.None && imageId == 0)
            {
                result = _pcp.PcpImageDelete(sellerUserId, editUserId, imageType);
            }
            else
            {
                result = false;
            }


            return result;
        }

        public static bool DeleteSellerImageBySeq(int groupId, int editUser, int seq, PcpImageType imageType)
        {
            bool result;

            using (TransactionScope scope = TransactionScopeBuilder.CreateReadCommitted())
            {
                try
                {
                    //註記刪除
                    _pcp.PcpImageDelete(groupId, editUser, imageType, seq);

                    //重新排序
                    var images = _pcp.PcpImageGetByCardGroupId(groupId, imageType);

                    int index = 1;
                    foreach (var image in images)
                    {
                        image.Sequence = index;
                        index++;
                    }

                    _pcp.PcpImageSet(images);
                    result = true;
                    scope.Complete();
                }
                catch (Exception)
                {
                    result = false;
                }
            }

            return result;
        }

        public static bool DeleteSellerImageById(int groupId, int editUser, List<int> ids, PcpImageType imageType)
        {
            bool result;

            using (TransactionScope scope = TransactionScopeBuilder.CreateReadCommitted())
            {
                try
                {
                    //註記刪除
                    _pcp.PcpImageDelete(groupId, editUser, imageType, ids);

                    //重新排序
                    var images = _pcp.PcpImageGetByCardGroupId(groupId, imageType);

                    int index = 1;
                    foreach (var image in images)
                    {
                        image.Sequence = index;
                        index++;
                    }

                    _pcp.PcpImageSet(images);
                    result = true;
                    scope.Complete();
                }
                catch (Exception)
                {
                    result = false;
                }
            }

            return result;
        }

        public static List<PCP.PcpImageInfo> GetSellerImage(int cardGroupId)
        {
            return GetSellerImage(new List<int> { cardGroupId });
        }

        public static List<PCP.PcpImageInfo> GetSellerImage(List<int> cardGroupIds)
        {
            var groupIds = cardGroupIds.Distinct().ToList();
            PcpImageCollection pcpImageCollection = new PcpImageCollection();

            int eachQueryCount = 2000;
            int startIndex = 0;
            while (startIndex < groupIds.Count)
            {
                var extractId = groupIds.Skip(startIndex).Take(eachQueryCount).ToList();
                //groupIds 如果超過2100筆，在 PcpImageGet 查詢使用where in 拋出Exception
                pcpImageCollection.AddRange(_pcp.PcpImageGet(extractId, PcpImageType.None));
                startIndex += eachQueryCount;
            }

            var result = (from item in pcpImageCollection
                          select new PCP.PcpImageInfo
                          {
                              GroupId = item.GroupId,
                              SellerUserId = item.SellerUserId,
                              ImagePathUrl = item.ImageUrl,
                              ImageCompressedPathUrl = item.ImageUrlCompressed ?? item.ImageUrl,
                              ImgType = (PcpImageType)item.ImageType,
                              Seq = item.Sequence,
                          }
                          ).ToList();
            return result;
        }

        public static object GetImagePathByType(List<PCP.PcpImageInfo> datas, PcpImageType imgType, bool isCompressed = false)
        {
            switch (imgType)
            {
                case PcpImageType.Logo:
                case PcpImageType.Card:
                case PcpImageType.CorporateImage:
                    if (datas.Where(x => x.ImgType == imgType).FirstOrDefault() == null)
                    {
                        return new { result = "" };
                    }

                    var tmpUrlPath = "";
                    if (isCompressed)
                    {
                        tmpUrlPath = datas.Where(x => x.ImgType == imgType).FirstOrDefault().ImageCompressedPathUrl ??
                            datas.Where(x => x.ImgType == imgType).FirstOrDefault().ImagePathUrl;
                    }
                    else
                    {
                        tmpUrlPath = datas.Where(x => x.ImgType == imgType).First().ImagePathUrl;
                    }

                    return new { result = ImageFacade.GetMediaPath(tmpUrlPath, MediaType.PCPImage) };

                case PcpImageType.Service:
                case PcpImageType.EnvironmentAroundStore:

                    if (datas.Where(x => x.ImgType == imgType).FirstOrDefault() == null)
                    {
                        return new { result = new string[] { } };
                    }

                    var tmpResult = new List<string>();

                    if (isCompressed)
                    {
                        tmpResult = datas.Where(x => x.ImgType == imgType).OrderBy(x => x.Seq)
                            .Select(x => x.ImageCompressedPathUrl ?? x.ImagePathUrl).ToList();
                    }
                    else
                    {
                        tmpResult = datas.Where(x => x.ImgType == imgType).OrderBy(x => x.Seq).Select(x => x.ImagePathUrl).ToList();
                    }

                    var res = (from item in tmpResult
                               let path = ImageFacade.GetMediaPath(item, MediaType.PCPImage)
                               select path).ToArray();

                    return new { result = res };
                default:

                    return new { };
            }
        }

        public static object GetImagePathByType(List<PCP.PcpImageInfo> datas, int groupId, PcpImageType imgType, bool isCompressed = false)
        {
            if (!datas.Any(x => x.GroupId == groupId))
            {
                return new { result = "" };
            }

            return GetImagePathByType(datas.Where(x => x.GroupId == groupId).ToList(), imgType, isCompressed);
        }

        #endregion

        #region seller contract

        public static bool UpdateSellerContractVersion(string accountId, bool argee, int deliveryType)
        {
            var sellers = _sp.SellerGetListBySellerGuidList(ProposalFacade.GetVbsSeller(accountId, true).Select(x => x.Key).ToList());
            SellerCollection newSeller = new SellerCollection();

            if (deliveryType == (int)DeliveryType.ToShop)
            {
                if (sellers.Count > 0)
                {
                    var version = _config.SellerContractVersionPpon;
                    var now = DateTime.Now;
                    foreach (var s in sellers)
                    {
                        if (s.ContractVersionPpon != version)
                        {
                            s.ContractVersionPpon = argee ? version : s.ContractVersionPpon;
                            s.ContractCkeckTimePpon = now;
                            newSeller.Add(s);
                        }
                    }
                    _sp.SellerSaveAll(newSeller);
                }
            }
            else if (deliveryType == (int)DeliveryType.ToHouse)
            {
                if (sellers.Count > 0)
                {
                    var version = _config.SellerContractVersionHouse;
                    var now = DateTime.Now;
                    foreach (var s in sellers)
                    {
                        if (s.ContractVersionHouse != version)
                        {
                            s.ContractVersionHouse = argee ? version : s.ContractVersionHouse;
                            s.ContractCkeckTimeHouse = now;
                            newSeller.Add(s);
                        }
                    }
                    _sp.SellerSaveAll(newSeller);
                }
            }
            
            return true;
        }

        #endregion

        #endregion

        #region VBS Home

        public static VBSHomeModel GetVbsHomeInfo(VBSQueryModel query)
        {
            var result = new VBSHomeModel();

            #region 核銷功能-分店的下拉選單(resouce_acl)
            var allowStores = GetVBSMemberAllowStoreList(query.SellerUserId);
            var storeList = allowStores.Select(
                x => new StoreDetail()
                {
                    StoreGuid = x.Guid.ToString(),
                    StoreName = x.SellerName
                }).ToList();

            //前端指定預設分店放置於第一個
            if (storeList.Any(x => x.StoreGuid == query.DefaultStoreGuid))
            {
                var index = storeList.FindIndex(x => x.StoreGuid == query.DefaultStoreGuid);
                var item = storeList[index];
                storeList[index] = storeList[0];
                storeList[0] = item;
            }
            result.StoeList = storeList;
            #endregion

            var seller = GetVbsMembershipByUserId(query.SellerUserId);

            if (seller.IsLoaded)
            {
                result.VBSName = seller.Name;
                result.SellerUserId = seller.UserId == null ? 0 : seller.UserId.Value;
            }

            #region 熟客卡相關

            var cardGroupId = 0;

            #region 取pcp角色
            var perE = Enum.GetNames(typeof(VendorRole)).ToList();
            VbsRoleCollection rc = _memberProvider.VbsRoleGetList(query.SellerUserId);
            var pcpRoles = rc.Select(a => a.RoleName).ToList().Intersect(perE).ToList();
            result.VbsMemberStatus |= pcpRoles.Any() ?
                (int)VbsMemberStatus.IsPcpMember : (int)VbsMemberStatus.None;
            #endregion

            #region 切換熟客系統的下拉選單 (下拉選單只有一張的時候看不到)
            List<ViewMembershipCardGroup> cardGroupList = PcpFacade.GetSellerListWhichEnabledRegularsSystemByAccountId(seller.AccountId);
            result.CardGroupList = cardGroupList.Select(x => new CardGroupInfo()
            {
                CardGroupId = x.CardGroupId,
                CardGroupName = string.Format("{0} 熟客系統", x.SellerName)
            }).ToList();

            if (cardGroupList != null && cardGroupList.Count() > 0)
            {
                //預設第一個
                cardGroupId = cardGroupList.FirstOrDefault().CardGroupId;

                //有傳CardGroupId的話
                if (query.GroupId != 0)
                {
                    var cardGroup = cardGroupList.Where(x => x.CardGroupId == query.GroupId).FirstOrDefault();//驗證是否是該帳號擁有權限的熟客系統，有的話就換
                    if (cardGroup != null && cardGroup.CardGroupId != 0)
                    {
                        cardGroupId = cardGroup.CardGroupId;
                    }
                }
            }
            #endregion

            #region 熟客卡相關
            var membershipcard = _pcp.ViewMembershipCardGetListByDatetime(cardGroupId, DateTime.Now);
            if (membershipcard.Any())
            {
                result.GroupId = cardGroupId;

                #region result.VbsMemberStatus
                //保留VbsMemberStatus是為了相容舊版本
                //熟客卡現在是綁在店家上而不是帳號，與VbsMemberStatus.IsCardOwner這邏輯有衝突
                result.VbsMemberStatus |=
                membershipcard.Any(x => x.Status == (int)MembershipCardStatus.Open) ?
                (int)VbsMemberStatus.IsCardOn : (int)VbsMemberStatus.None;
                result.VbsMemberStatus |= (int)VbsMemberStatus.IsCardOwner;
                #endregion

                var imageData = GetSellerImage(cardGroupId);
                result.SellerImageUrl = Helper.GetPropValue(
                                  VBSFacade.GetImagePathByType(imageData, cardGroupId, PcpImageType.Card, true), "result"
                                  ).ToString();

                #region [統計圖表]每日新增會員與累積人數相關紀錄
                result.TotalMemberLineChart = new LineChart()
                {
                    Description = "累積人次",
                    DataList = new List<LineChartPoint>()
                };
                result.DailyNewMemberLineChart = new LineChart()
                {
                    Description = "今日人次",
                    DataList = new List<LineChartPoint>()
                };

                DateTime endTime = DateTime.Now.Date;
                DateTime startTime = endTime.AddDays(-6);
                //old:靠Job每日統計結果 (changeset #38573以前) (缺點：當天的資料無法得到、測試站沒有資料)
                //UserGetCardDailyReportCollection reportDatas = BonusFacade.GetUserGetCardDailyReport(cardGroupId, startTime, endTime);

                //new:即時算(速度如果夠快、效能也還可以的話，就沿用，否則改回舊方法)
                var reportDatas = PcpFacade.ImmediatelyCountUserGetCard(cardGroupId, startTime, endTime).ToArray();

                for (int i = 0; i < reportDatas.Count(); i++)
                {
                    //累積人數
                    result.TotalMemberLineChart.DataList.Add(new LineChartPoint
                    {
                        XAxisDate = reportDatas[i].ReportDate,
                        YAxisNumber = reportDatas[i].TotalMemberCount
                    });
                    //每日新增
                    result.DailyNewMemberLineChart.DataList.Add(new LineChartPoint
                    {
                        XAxisDate = reportDatas[i].ReportDate,
                        YAxisNumber = reportDatas[i].DailyNewMemberCount
                    });
                }
                #endregion
            }
            #endregion

            #endregion

            return result;
        }

        #endregion

        #region Checkout 核銷相關

        /// <summary>
        /// 取得有權限核銷檔次 
        /// </summary>
        /// <param name="mem"></param>
        /// <returns></returns>
        public static List<IVbsVendorAce> GetAllowedDealUnits(VbsMembership mem)
        {
            if (mem.VbsMembershipAccountType == VbsMembershipAccountType.VerificationAccount) //is17LifeEmployee
            {
                return new List<IVbsVendorAce>();
            }
            List<IVbsVendorAce> allowDeals = (new VbsVendorAclMgmtModel(mem.AccountId)).GetAllowedDeals(VbsRightFlag.Verify, DeliveryType.ToShop, false).ToList();
            return allowDeals;
        }

        public static Tuple<ApiResultCode, List<VbsApiVerifyCouponInfo>, string> VerifyCheckInfo(
            VbsMembership mem, List<VerifyCode> verifyData, bool isPezEvent = false, VbsCouponVerifyState verifyState = VbsCouponVerifyState.UnUsed)
        {
            var code = ApiResultCode.Success;
            var returnData = new List<VbsApiVerifyCouponInfo>();
            var msg = "";

            List<IVbsVendorAce> allowedDealUnits = GetAllowedDealUnits(mem);
            foreach (var item in verifyData)
            {
                var checkCanUse = new List<VbsApiVerifyCouponInfo>();
                var couponResponse = VerificationFacade.GetAccessibleCoupon(mem, item.CouponSerial, item.CouponCode, item.TrustId, allowedDealUnits, isPezEvent).ToList();

                bool isDuInTime = false;  //判斷Coupon是否在可核銷時限區間

                if (couponResponse.Any())
                {
                    #region 撈取可核銷Coupon
                    foreach (var couponinfo in couponResponse)
                    {
                        //憑證為未使用並於有效使用期限內 即回傳檔次及憑證購買資料 供使用者確認是否進行核銷
                        if (couponinfo.VerifyState == verifyState
                            && couponinfo.ExchangePeriodStartTime <= DateTime.Now.Date
                            && DateTime.Now.Date <= couponinfo.ExchangePeriodEndTime.AddDays(_config.VerificationBuffer))
                        {
                            //判斷是否已過使用期限 卻仍在可核銷時限區間內
                            if (couponinfo.ExchangePeriodEndTime < DateTime.Now.Date
                                && DateTime.Now.Date <= couponinfo.ExchangePeriodEndTime.AddDays(_config.VerificationBuffer))
                            {
                                isDuInTime = true;
                            }

                            checkCanUse.Add(new VbsApiVerifyCouponInfo
                            {
                                DealId = couponinfo.DealId,
                                DealName = couponinfo.DealName,
                                CouponId = couponinfo.CouponSequence,
                                CouponCode = couponinfo.CouponCode,
                                DealType = couponinfo.DealType,
                                VerifyState = couponinfo.VerifyState,
                                PayerName = VerificationFacade.SetPayerNameHidden(couponinfo.BuyerName),
                                ExchangePeriodStartTime = ApiSystemManager.DateTimeToDateTimeString(couponinfo.ExchangePeriodStartTime),
                                ExchangePeriodEndTime = ApiSystemManager.DateTimeToDateTimeString(couponinfo.ExchangePeriodEndTime),
                                ReturnDateTime = (couponinfo.ModifyTime.HasValue) ? ApiSystemManager.DateTimeToDateTimeString(couponinfo.ModifyTime.Value) : "",
                                TrustId = couponinfo.TrustId.ToString(),
                                IsDuInTime = isDuInTime,
                                IsCouponAvailable = true,
                            });
                        }
                    }
                    #endregion 撈取可核銷Coupon

                    //無可核銷憑證，取vdlModel回傳錯誤訊息
                    if (!checkCanUse.Any())
                    {
                        code = ApiResultCode.CouponPartialFail;
                        //憑證為已使用、退貨或逾期狀態
                        if (couponResponse.First().VerifyState == VbsCouponVerifyState.Return)
                        {
                            //code = ApiResultCode.CouponReturned;
                            msg = item.CouponSerial + "," + item.CouponCode + ",此憑證已退貨\n退貨時間："
                                + couponResponse.First().ModifyTime.ToString();
                        }
                        else if (couponResponse.First().VerifyState == VbsCouponVerifyState.Verified)
                        {
                            //code = ApiResultCode.CouponUsed;
                            msg = item.CouponSerial + "," + item.CouponCode + ",此憑證已使用過了，\n核銷時間"
                                + couponResponse.First().ModifyTime;
                        }
                        else if (couponResponse.First().VerifyState == VbsCouponVerifyState.UnUsed && verifyState == VbsCouponVerifyState.Verified)
                        {
                            msg = item.CouponSerial + "," + item.CouponCode + " 此憑證未使用";
                        }
                        else if (DateTime.Now.Date >
                                 couponResponse.First().ExchangePeriodEndTime.AddDays(_config.VerificationBuffer))
                        {
                            //code = ApiResultCode.InvalidInterval;
                            msg = item.CouponSerial + "," + item.CouponCode + ",此憑證不在使用期限內。\n未開放使用，或已超過使用期限";
                        }
                        break;
                    }
                    else
                    {
                        returnData.AddRange(checkCanUse);
                    }
                }
                else
                {
                    code = ApiResultCode.CouponPartialFail;
                    //returnData.Add(new VbsApiVerifyCouponInfo { });
                    //code = ApiResultCode.InvalidCoupon;
                    msg = item.CouponSerial + "," + item.CouponCode + ",查無此憑證。可能為無效憑證，\n或非本店所能核銷的憑證。";
                    break;
                }
            }

            return Tuple.Create(code, returnData, msg);
        }

        public static bool GroupCouponIsExist(Guid orderGuid, string timestamp)
        {
            return _vp.GroupCouponCheckExist(orderGuid, timestamp);
        }

        public static ApiResultCode MultiCouponVerify(VbsMembership mem, List<Guid> tids, Guid orderGuid, string timestamp, string clientIp,
            out MultiCouponVerifyResult resultData)
        {
            var groupCode = ApiResultCode.Success;
            resultData = new MultiCouponVerifyResult() { VerifyDetail = new List<PCP.ApiResult>() };
            var groupCouponLog = new GroupCouponLog
            {
                OrderGuid = orderGuid,
                VerifyCount = tids.Count(),
                Timestamp = timestamp,
                CreateTime = DateTime.Now
            };

            using (TransactionScope scope = TransactionScopeBuilder.CreateReadCommitted())
            {
                if (!_vp.GroupCouponLogSet(groupCouponLog))
                {
                    groupCode = ApiResultCode.CouponUsed;
                }
                else
                {
                    foreach (var item in tids)
                    {
                        var status = OrderFacade.VerifyCoupon(item, false, mem.AccountId, clientIp, "", true);

                        CashTrustLog cashTrustLog;
                        switch (status)
                        {
                            case VerificationStatus.CouponOk:
                                resultData.VerifyDetail.Add(new PCP.ApiResult
                                {
                                    Code = ApiResultCode.Success,
                                    Data = new { tid = item.ToString() },
                                });
                                break;
                            case VerificationStatus.CouponUsed:
                                groupCode = ApiResultCode.CouponPartialFail;
                                cashTrustLog = VerificationFacade.GetCashTrustLog(item);
                                resultData.VerifyDetail.Add(new PCP.ApiResult
                                {
                                    Code = ApiResultCode.CouponUsed,
                                    Data = new { Tid = item.ToString() },
                                    Message = "此憑證已使用過了，\n核銷時間：" + cashTrustLog.ModifyTime,
                                });
                                break;
                            case VerificationStatus.CouponReted:
                                groupCode = ApiResultCode.CouponPartialFail;
                                cashTrustLog = VerificationFacade.GetCashTrustLog(item);
                                resultData.VerifyDetail.Add(new PCP.ApiResult
                                {
                                    Code = ApiResultCode.CouponReturned,
                                    Data = new { tid = item.ToString() },
                                    Message = "此憑證已退貨\n退貨時間：" + cashTrustLog.ModifyTime,
                                });
                                break;
                            case VerificationStatus.CouponError:
                            case VerificationStatus.CashTrustLogErr:
                            case VerificationStatus.CashTrustStatusLogErr:
                            default:
                                groupCode = ApiResultCode.CouponPartialFail;
                                resultData.VerifyDetail.Add(new PCP.ApiResult
                                {
                                    Code = ApiResultCode.SystemBusy,
                                    Data = new { tid = item.ToString() },
                                });
                                break;
                        }

                        //一筆失敗就算全部失敗
                        if (groupCode != ApiResultCode.Success)
                        {
                            break;
                        }
                    }
                }

                //不是全部成功就不更新
                if (groupCode == ApiResultCode.Success && resultData.VerifyDetail.Count(x => x.Code == ApiResultCode.Success) == tids.Count())
                {
                    scope.Complete();
                }
            }

            return groupCode;
        }

        public static GroupCouponVerifyCheckType CheckCouponGroupTrustIdQuantity(int userId, Guid orderGuid, int verifyCount, out List<Guid> trustIds)
        {
            trustIds = new List<Guid>();
            var ctlc = _memberProvider.CashTrustLogGetCouponSequencesWithUnused(orderGuid);
            var selfCtlList = GetSelfCashTrustLog(userId, ctlc.ToList());

            if (selfCtlList.Any())
            {
                if (selfCtlList.Count < verifyCount)
                {
                    return GroupCouponVerifyCheckType.CouponNotEnough;
                }

                trustIds.AddRange(selfCtlList.Take(verifyCount).Select(x => x.TrustId));
            }

            return GroupCouponVerifyCheckType.Success;
        }

        /// <summary>
        /// 取得自己的cashTrustLog
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ctlList"></param>
        /// <returns></returns>
        public static List<CashTrustLog> GetSelfCashTrustLog(int userId, List<CashTrustLog> ctlList)
        {
            var selfCtlList = new List<CashTrustLog>();
            var firstCtl = ctlList.FirstOrDefault();
            
            if (firstCtl != null)
            {
                //禮物
                var mgmgift = _mgmp.GiftGetByOid(firstCtl.OrderGuid).Where(x => x.IsActive).ToList();
                bool isSelf = _op.OrderGet(firstCtl.OrderGuid).UserId == userId;
                foreach (var ct in ctlList)
                {
                    if (isSelf)
                    {
                        if (!mgmgift.Any(x => x.CouponId == ct.CouponId && x.CouponSequenceNumber == ct.CouponSequenceNumber))
                        {
                            selfCtlList.Add(ct);
                        }
                    }
                    else
                    {
                        if (mgmgift.Any(x => x.CouponId == ct.CouponId && x.CouponSequenceNumber == ct.CouponSequenceNumber 
                            && x.ReceiverId == userId && !x.IsUsed && x.Accept == (int)MgmAcceptGiftType.Accept))
                        {
                            selfCtlList.Add(ct);
                        }
                    }
                }
            }
            //面額大的優先
            return selfCtlList.OrderByDescending(x => x.Amount).ToList();
        }


        #endregion

        #region VbsMembershipPermission

        public static List<VbsMembershipPermission> GetVbsMembershipPermissionList(string accountId)
        {
            var result = new List<VbsMembershipPermission>();
            var vbsMembership = _memberProvider.VbsMembershipGetByAccountId(accountId);

            if (vbsMembership.IsLoaded)
            {
                var permissionList = _vbs.VbsMembershipPermissionGetList(vbsMembership.Id).ToList();
                if (permissionList != null)
                {
                    result = permissionList;
                }
            }

            return result;
        }

        public static bool CheckVbsMembershipPermissionScope(int vbsMembershipId, VbsMembershipPermissionScope scope)
        {
            bool result = false;
            var permissionScope = _vbs.VbsMembershipPermissionGet(vbsMembershipId, scope);
            if (permissionScope.IsLoaded)
            {
                result = true;
            }

            return result;
        }

        public static bool CheckVbsMembershipPermissionScope(string accountId, VbsMembershipPermissionScope scope)
        {
            bool result = false;

            var vbs = _memberProvider.VbsMembershipGetByAccountId(accountId);
            if (vbs.IsLoaded)
            {
                var permissionScope = _vbs.VbsMembershipPermissionGet(vbs.Id, scope);
                if (permissionScope.IsLoaded)
                {
                    result = true;
                }
            }

            return result;
        }

        #endregion VbsMembershipPermission

        #region Vbs Menu
        public static string[] GetVbsMenuByPageName(string[] pageName, int d)
        {
            //商家新版注意事項：共用頁面(首頁/對帳管理/檔次管理/幫助中心)包含子頁
            //連結都應附加參數d(1:在地憑證, 2:宅配商品)，以確保上方下拉選單能正確顯示(商家舊版則不必)
            //menu[0]: 上方選單 (Default: 宅配)
            //menu[1]: 左側選單CSS name (Default: 首頁)
            //menu[2]: 左側選單子項排序 (0~n, 無子項請填null)
            string[] menu = new string[3];

            switch (pageName[0])
            {
                case "bulletinboardlist":  //首頁
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liHome";
                    menu[2] = null;
                    break;
                case "serviceprocess"://客服系統
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liService";
                    menu[2] = null;
                    break;
                case "verify":  //線上核銷
                    menu[0] = ((int)DeliveryType.ToShop).ToString();
                    menu[1] = "liVerify";
                    menu[2] = null;
                    break;

                case "verificationcouponlistbystore":  //核銷查詢
                case "verificationdeallist":
                case "verificationstorelist":
                case "verificationcouponlist":
                case "employeeverificationdeallist":
                case "norecord":
                    menu[0] = ((int)DeliveryType.ToShop).ToString();
                    menu[1] = "liVerification";
                    menu[2] = null;
                    break;

                case "ship":  //訂單管理
                case "shipdeallist":
                case "shiporderlist":
                case "shipproductinventory":
                case "noshipdeal":
                case "batchinventoryimport":
                case "employeeshipdeallist":
                    menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    menu[1] = "liShip";
                    menu[2] = null;

                    if (pageName[1] != null)
                    {
                        switch (pageName[1].ToLower())
                        {
                            case "shiporderstatuslist":
                                menu[2] = "0";
                                break;
                            case "ispshiporderlist":    //待出貨訂單-全家
                                if (d == 1)
                                {
                                    menu[1] = "liIspShipList";
                                }
                                else if (d == 2)
                                {
                                    menu[1] = "liIspShipPrint";
                                }
                                else if(d == 3)
                                {
                                    menu[1] = "liIspShipComplete";
                                }
                                break;
                            case "ispshiporderrefund":
                                menu[1] = "IspShipOrderRefund";
                                break;
                            case "exchangeorderprocesslist":
                                menu[1] = "liExchange";
                                break;
                            case "returnorderprocesslist":
                                menu[1] = "liReturn";
                                break;
                            case "wmscontract":
                                menu[1] = "liWmsContract";
                                break;
                            case "wmscontact":
                                menu[1] = "liWmsContact";
                                break;
                            case "wmspurchaseorder":
                            case "wmsprint":
                            case "wmsprogressstatus":
                                menu[1] = "liWmsPurchaseOrder";
                                break;
                            case "wmsreturnorder":
                            case "wmsreturnorderquery":
                                menu[1] = "liWmsReturnOrder";
                                break;
                            case "wmsorderdelivery":
                                menu[1] = "liWmsOrderDelivery";
                                break;
                            default:
                                menu[2] = null;
                                break;
                        }
                    }
                    break;

                case "shiporderstatuslist":
                    menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    menu[1] = "liShip";
                    menu[2] = "0";
                    break;

                case "balancedeallist":  //對帳查詢
                case "balancestorelist":
                case "balancesheetsweek":
                case "balancesheetsmonth":
                case "inspectioncodelogin":
                case "balancesheetsdeliver":
                case "balancesheetpack":
                case "balancesheetstohouse":
                case "employeebalancedeallist":
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liBalance";
                    menu[2] = null;
                    break;

                case "bookingsystem":  //預約管理
                case "bookingfirst":
                case "bookingreservationsetup":
                case "bookingreservationdetailsetup":
                case "bookingtravelsetup":
                case "bookingnopermission":
                case "bookingreservationlist":
                case "bookingstore":
                case "bookingsystemsearch":
                case "couponbooking":
                case "coupondetailbooking":
                case "travelbooking":
                case "nopermissionbooking":
                case "bookinglist":
                    menu[0] = ((int)DeliveryType.ToShop).ToString();
                    menu[1] = "liBooking";
                    menu[2] = null;
                    break;

                case "reservelock":  //憑證鎖定
                    menu[0] = ((int)DeliveryType.ToShop).ToString();
                    menu[1] = "liReserveLock";
                    menu[2] = null;
                    break;

                case "userevaluation":  //評價管理
                    menu[0] = ((int)DeliveryType.ToShop).ToString();
                    menu[1] = "liEvaluate";
                    menu[2] = null;
                    break;


                case "proposallist":  //提案功能
                case "proposalcontent":
                    menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    menu[1] = "liProposal";
                    menu[2] = null;
                    break;

                case "productoption":  //商品與庫存管理
                case "productoptioncontent":
                    menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    menu[1] = "liProposalProduct";
                    menu[2] = null;
                    break;

                case "others":  //幫助中心
                                //case "setpassword":
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liHelp";
                    menu[2] = null;
                    break;
                case "vbsdocumentdownloadsys":
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liSysDoc";
                    break;
                case "vbsdocumentdownloadother":
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liOtherDoc";
                    break;
                case "sellerbasicinfo":
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liSellerBasicInfo";
                    break;
                case "sellercontact":
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liSellerContact";
                    break;
                case "sellerfinance":
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liSellerFinance";
                    break;
                case "setpassword":
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liSellerPassword";
                    break;
                case "sellerbindaccount":
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liSellerBindAccount";
                    break;
                case "sellerlocation":
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liSellerLocation";
                    break;
                case "sellerchangelog":
                    if (d == (int)DeliveryType.ToShop)
                    {
                        menu[0] = ((int)DeliveryType.ToShop).ToString();
                    }
                    else if (d == (int)DeliveryType.ToHouse)
                    {
                        menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    }
                    else
                    {
                        menu[0] = "0";
                    }
                    menu[1] = "liSellerChangeLog";
                    break;
                default:
                    menu[0] = ((int)DeliveryType.ToHouse).ToString();
                    menu[1] = "liHome";
                    menu[2] = null;
                    break;
            }
            return menu;
        }
        public static bool IsEnabledVbsShipRule2k18()
        {
            return _config.VbsShipRule2k18;
        }
        #endregion
    }
}

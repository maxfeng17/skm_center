﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelCustom
{
    [ComplexType]
    public class SkmCurationDeals
    {
        public SkmCurationDeals()
        {
            curation_name = string.Empty;
            item_name = string.Empty;
        }
        /// <summary>
        /// 策展ID
        /// </summary>
        [JsonProperty("CurationId")]
        public int curation_id { get; set; }
        /// <summary>
        /// 策展名稱
        /// </summary>
        [JsonProperty("CurationName")]
        public string curation_name { get; set; }
        /// <summary>
        /// 商品建檔編號
        /// </summary>
        [JsonProperty("ItemId")]
        public Guid item_id { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        [JsonProperty("ItemName")]
        public string item_name { get; set; }
        /// <summary>
        /// 商品建檔時間
        /// </summary>
        [JsonProperty("OpDate")]
        public DateTime op_date { get; set; }

    }
}

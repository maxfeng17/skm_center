﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Core.ModelCustom
{

    public class PagerModel
    {
        public int PageCount { get; set; }
        public int Page { get; set; }
        public int TotalCount { get; set; }

        public int LastPage
        {
            get
            {
                if (TotalCount > 0)
                {
                    return TotalCount / PageCount + (TotalCount % PageCount > 0 ? 1 : 0);
                }
                else
                {
                    return 1;
                }
            }
        }

        public int Skip
        {
            get
            {
                if (Page <= 1)
                {
                    return 0;
                }
                else
                {
                    return (Page - 1) * PageCount;
                }
            }
        }

        public int Take
        {
            get
            {
                return PageCount;
            }
        }

        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((double)TotalCount / (double)PageCount);
            }
        }

        public PagerModel()
        {
            PageCount = 10;
            Page = 1;
        }
    }

    public class PrizeDrawEventListModel : PagerModel
    {
        public SkmPrizeDrawEventOrderField OrderField { get; set; }
        public int Sort { get; set; }
        public List<PrizeDrawEventModel> PrizeDrawEventList { get; set; }

        public PrizeDrawEventListModel()
        {
            OrderField = SkmPrizeDrawEventOrderField.Default;
            Sort = 0;
        }
    }

    public class PrizeDrawEventModel
    {
        public int Id { get; set; }

        public int ChannelOdmId { get; set; }

        public string Token { get; set; }

        public DateTime SD { get; set; }

        public DateTime ED { get; set; }

        public string StartDate { get; set; }

        public string StartTime { get; set; }

        public string EndDate { get; set; }

        public string EndTime { get; set; }

        public string Subject { get; set; }
        /// <summary> 注意事項 </summary>
        public string Notice { get; set; }
        public string EventContent { get; set; }

        public int JoinFee { get; set; }
        /// <summary> 活動獎品分段設定 </summary>
        public int PeriodHour { get; set; }
        /// <summary> 每日參加上限次數 </summary>
        public int DailyLimit { get; set; }
        /// <summary> 活動參加上限次數 </summary>
        public int TotalLimit { get; set; }

        public byte Status { get; set; }

        public SkmPrizeDrawTemple Temple { get; set; }

        public string PrizeImg { get; set; }
        public HttpPostedFileBase PrizeImgFile { get; set; }

        public string FullImg { get; set; }
        public HttpPostedFileBase FullImgFile { get; set; }

        public string SmallImg { get; set; }
        public HttpPostedFileBase SmallImgFile { get; set; }

        public string WinningSmallImg { get; set; }
        public HttpPostedFileBase WinningSmallImgFile { get; set; }

        public string WinningBackgroundImg { get; set; }
        public HttpPostedFileBase WinningBackgroundImgFile { get; set; }

        public decimal EventPrize { get; set; }

        /// <summary>
        /// add json string
        /// </summary>
        public string PrizeDrawItems { get; set; }

        /// <summary>
        /// edit list 
        /// </summary>
        public List<PrizeDrawEditItemModel> PrizeDrawItemList { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime? NoticeDate { get; set; }
        public string PreviewUrl { get; set; }
        public string EventUrl { get; set; }
        public string DeepLink { get; set; }
        public string SellerGuid { get; set; }
        public List<SelectListItem> SellerGuidDDL { get; set; }

        public PrizeDrawEventModel()
        {
            PrizeDrawItemList = new List<PrizeDrawEditItemModel>();
            Temple = SkmPrizeDrawTemple.Red;
        }
    }

    public class PrizeDrawEditItemModel
    {
        public int Id { get; set; }
        public DateTime? SD { get; set; }
        public DateTime? ED { get; set; }
        public string EffectiveStartDate { get; set; }
        public string EffectiveEndDate { get; set; }
        public string Name { get; set; }
        public decimal Rate { get; set; }
        public int Count { get; set; }
        public int DrawOutCount { get; set; }
        public Guid Bid { get; set; }
        public bool IsThanksFlag { get; set; }
        public string Memo { get; set; }
        public bool IsUse { get; set; }
    }

    public class PrizeDrawEventDDLModel
    {
        /// <summary>
        /// 燒點
        /// </summary>
        public int Point { get; set; }
        /// <summary>
        /// 名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 檔次剩餘數量
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 檔次起始時間
        /// </summary>
        public DateTime BusinessHourOrderTimeS { get; set; }
        /// <summary>
        /// 檔次結束時間
        /// </summary>
        public DateTime BusinessHourOrderTimeE { get; set; }
        /// <summary>
        /// 檔次BID
        /// </summary>
        public Guid Bid { get; set; }

    }

    public class PrizeDrawAddItemModel
    {
        public int id { get; set; }
        public string bid { get; set; }
        public string rate { get; set; }
        public string effectiveStartDate { get; set; }
        public string effectiveEndDate { get; set; }
        public string count { get; set; }
        /// <summary>
        /// 是否為銘謝惠顧的獎項
        /// </summary>
        public bool isthanks { get; set; }
        public string name { get; set; }
        public bool IsUse { get; set; }
    }


    public class PrizeDrawEventBaseModel
    {

        public string UserName { get; set; }
        public string CardNo { get; set; }
        public string DeviceType { get; set; }
        public string DeviceCode { get; set; }
        public int MemStatus { get; set; }
        public string AccessToken { get; set; }
        public string ApiSiteUrl { get; set; }
        public string SellerGuid { get; set; }
    }

    public class PrizeDrawEventViewModel : PrizeDrawEventBaseModel
    {
        public string NoticeDate { get; set; }
        public string Subject { get; set; }
        public int JoinFee { get; set; }
        public string EventToken { get; set; }
        public string EventHtmlContent { get; set; }
        public string Bid { get; set; }
        public bool IsCanJoinPrizeDraw { get; set; }
        /// <summary>
        /// 分店下拉選單
        /// </summary>
        public Dictionary<string, string> SellerDpList { get; set; }
        public Dictionary<string, string> StoreDpList { get; set; }
        public ApiResultCode ErrorCode { get; set; }
        public string PrizeImg { get; set; }
        public string WinningSmallImg { get; set; }
        public string WinningBackgroundImg { get; set; }
        /// <summary>
        /// 抽獎次數
        /// </summary>
        public int TotalDrawQty { get; set; }
        public int DailyLimit { get; set; }
        public int TotalLimit { get; set; }
        /// <summary>
        /// 剩餘抽獎次數
        /// </summary>
        public int RemainDrawQty { get; set; }
        /// <summary>
        /// 中獎Record
        /// </summary>
        public List<ViewPrizeDrawRecord> DrawRecord { get; set; }

        public PrizeDrawEventViewModel()
        {
            DrawRecord = new List<ViewPrizeDrawRecord>();
            EventToken = string.Empty;
            SellerDpList = new Dictionary<string, string>();
            StoreDpList = new Dictionary<string, string>();
        }
    }

    public class PrizeDrawBoxItem
    {
        public int EventId { get; set; }
        public int ItemId { get; set; }
        public Guid Bid { get; set; }
        public int RandomVal { get; set; }
        public int RandomIndex { get; set; }
        public bool IsThank { get; set; }
    }

    /// <summary> 設定獎品是否使用需求資訊 </summary>
    public class SetPrizeItemUseOrNotModel
    {
        /// <summary> 獎品編號 </summary>
        public int Id { get; set; }
    }
}

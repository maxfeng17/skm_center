﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.DataOrm
{
    public partial class AccessoryGroup
    {
        public List<KeyValuePair<Accessory, AccessoryGroupMember>> AccessoryMembers { get; set; }
        public List<ViewItemAccessoryGroup> members { get; set; }
    }

    public partial class Accessory
    {
        public AccessoryCategory Category { get; set; }
    }
}

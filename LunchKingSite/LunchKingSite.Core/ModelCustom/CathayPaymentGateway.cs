﻿using LunchKingSite.Core.Component;

namespace LunchKingSite.Core.ModelCustom
{
    public interface ICathayPaymentResultBase<T>
    {
        T CUBXML { get; set; }
        bool Verify(string CubKey);
    }

    public class CubXmlBase
    {
        public string MSGID { get; set; }
        public string CAVALUE { get; set; }
        
    }

    public abstract class CathayPaymentResultBase<T>
    {
        public abstract T CUBXML { get; set; }
        public abstract bool Verify(string CubKey);
    }
   
    public class AuthCubXml : CubXmlBase
    {
        public AuthorderInfo AUTHORDERINFO { get; set; }
        public class AuthorderInfo
        {
            public string STOREID { get; set; }
            public string ORDERNUMBER { get; set; }
            public string AMOUNT { get; set; }
            public string PERIODNUMBER { get; set; }
            public string AUTHSTATUS { get; set; }
            public string AUTHCODE { get; set; }
            public string AUTHTIME { get; set; }
            public string AUTHMSG { get; set; }
        }
    }

    public class RefundCubXml : CubXmlBase
    {
        public RefundorderInfo REFUNDORDERINFO { get; set; }
        public class RefundorderInfo
        {
            public string STOREID { get; set; }
            public string ORDERNUMBER { get; set; }
            public string AMOUNT { get; set; }
            public string AUTHCODE { get; set; }
            public string STATUS { get; set; }
        }
    }

    public class RefundReverseCubXml : CubXmlBase
    {
        public CancelRefundInfo CANCELREFUNDINFO { get; set; }
        public class CancelRefundInfo
        {
            public string STOREID { get; set; }
            public string ORDERNUMBER { get; set; }
            public string AUTHCODE { get; set; }
            public string STATUS { get; set; }
        }
    }

    public class CaptureCubXml : CubXmlBase
    {
        public CaptureOrderInfo CAPTUREORDERINFO { get; set; }
        public class CaptureOrderInfo
        {
            public string STOREID { get; set; }
            public string ORDERNUMBER { get; set; }
            public string AMOUNT { get; set; }
            public string AUTHCODE { get; set; }
            public string STATUS { get; set; }
        }
    }

    public class CaptureReverseCubXml : CubXmlBase
    {
        public CancelCaptureInfo CANCELCAPTUREINFO { get; set; }
        public class CancelCaptureInfo
        {
            public string STOREID { get; set; }
            public string ORDERNUMBER { get; set; }
            public string AUTHCODE { get; set; }
            public string STATUS { get; set; }
        }
    }

    public class AuthReverseCubXml : CubXmlBase
    {
        public CancelOrderInfo CANCELORDERINFO { get; set; }
        public class CancelOrderInfo
        {
            public string STOREID { get; set; }
            public string ORDERNUMBER { get; set; }
            public string AUTHCODE { get; set; }
            public string STATUS { get; set; }
        }
    }

    public class QueryCubXml : CubXmlBase
    {
        public OrderInfo ORDERINFO { get; set; }
        public class OrderInfo
        {
            public string STOREID { get; set; }
            public string ORDERNUMBER { get; set; }
            public string AMOUNT { get; set; }
            public string STATUS { get; set; }
            public string AUTHCODE { get; set; }
            public string AUTHTIME { get; set; }
            public string SETTLEAMOUNT { get; set; }
            public string SETTLETIME { get; set; }
            public string ORDERTIME { get; set; }
        }
    }

    public class OtpAuthInfoCubXml
    {
        public string CAVALUE { get; set; }
        public OrderInfo ORDERINFO { get; set; }
        public AuthInfo AUTHINFO { get; set; }
        public class OrderInfo
        {
            public string STOREID { get; set; }
            public string ORDERNUMBER { get; set; }
            public string AMOUNT { get; set; }
        }
        public class AuthInfo
        {
            public string AUTHSTATUS { get; set; }
            public string AUTHCODE { get; set; }
            public string AUTHTIME { get; set; }
            public string AUTHMSG { get; set; }
        }
    }

    public class OtpOrderInfoCubXml
    {
        public string CAVALUE { get; set; }
        public OrderInfo ORDERINFO { get; set; }
        public class OrderInfo
        {
            public string STOREID { get; set; }
            public string ORDERNUMBER { get; set; }
        }
    }

    public class CathayPaymentAuthResult : CathayPaymentResultBase<AuthCubXml>
    {
        public override AuthCubXml CUBXML { get; set; }
        public override bool Verify(string CubKey)
        {
            string content = this.CUBXML.AUTHORDERINFO.STOREID + this.CUBXML.AUTHORDERINFO.ORDERNUMBER + this.CUBXML.AUTHORDERINFO.AMOUNT + this.CUBXML.AUTHORDERINFO.AUTHSTATUS + this.CUBXML.AUTHORDERINFO.AUTHCODE + CubKey;
            return this.CUBXML.CAVALUE.Equals(Security.MD5Hash(content, false));
        }
    }

    public class CathayPaymentRefundResult : CathayPaymentResultBase<RefundCubXml>
    {
        public override RefundCubXml CUBXML { get; set; }
        public override bool Verify(string CubKey)
        {
            string content = this.CUBXML.REFUNDORDERINFO.STOREID + this.CUBXML.REFUNDORDERINFO.ORDERNUMBER + this.CUBXML.REFUNDORDERINFO.AMOUNT + this.CUBXML.REFUNDORDERINFO.STATUS + CubKey;
            return this.CUBXML.CAVALUE.Equals(Security.MD5Hash(content, false));
        }
    }

    public class CathayPaymentRefundReverseResult : CathayPaymentResultBase<RefundReverseCubXml>
    {
        public override RefundReverseCubXml CUBXML { get; set; }
        public override bool Verify(string CubKey)
        {
            string content = this.CUBXML.CANCELREFUNDINFO.STOREID + this.CUBXML.CANCELREFUNDINFO.ORDERNUMBER + this.CUBXML.CANCELREFUNDINFO.AUTHCODE + this.CUBXML.CANCELREFUNDINFO.STATUS + CubKey;
            return this.CUBXML.CAVALUE.Equals(Security.MD5Hash(content, false));
        }
    }

    public class CathayPaymentCaptureResult : CathayPaymentResultBase<CaptureCubXml>
    {
        public override CaptureCubXml CUBXML { get; set; }
        public override bool Verify(string CubKey)
        {
            string content = this.CUBXML.CAPTUREORDERINFO.STOREID + this.CUBXML.CAPTUREORDERINFO.ORDERNUMBER + this.CUBXML.CAPTUREORDERINFO.AMOUNT + this.CUBXML.CAPTUREORDERINFO.STATUS + CubKey;
            return this.CUBXML.CAVALUE.Equals(Security.MD5Hash(content, false));
        }
    }

    public class CathayPaymentCaptureReverseResult : CathayPaymentResultBase<CaptureReverseCubXml>
    {
        public override CaptureReverseCubXml CUBXML { get; set; }
        public override bool Verify(string CubKey)
        {
            string content = this.CUBXML.CANCELCAPTUREINFO.STOREID + this.CUBXML.CANCELCAPTUREINFO.ORDERNUMBER + this.CUBXML.CANCELCAPTUREINFO.AUTHCODE + this.CUBXML.CANCELCAPTUREINFO.STATUS + CubKey;
            return this.CUBXML.CAVALUE.Equals(Security.MD5Hash(content, false));
        }
    }
    
    public class CathayPaymentAuthReverseResult : CathayPaymentResultBase<AuthReverseCubXml>
    {
        public override AuthReverseCubXml CUBXML { get; set; }
        public override bool Verify(string CubKey)
        {
            string content = this.CUBXML.CANCELORDERINFO.STOREID + this.CUBXML.CANCELORDERINFO.ORDERNUMBER + this.CUBXML.CANCELORDERINFO.AUTHCODE + this.CUBXML.CANCELORDERINFO.STATUS + CubKey;
            return this.CUBXML.CAVALUE.Equals(Security.MD5Hash(content, false));
        }
    }

    public class CathayPaymentQueryResult : CathayPaymentResultBase<QueryCubXml>
    {
        public override QueryCubXml CUBXML { get; set; }
        public override bool Verify(string CubKey)
        {
            string content = this.CUBXML.ORDERINFO.STOREID + this.CUBXML.ORDERINFO.ORDERNUMBER + this.CUBXML.ORDERINFO.AMOUNT + this.CUBXML.ORDERINFO.STATUS + CubKey;
            return this.CUBXML.CAVALUE.Equals(Security.MD5Hash(content, false));
        }
    }
    public class CathayPaymentOtpFirstResult : CathayPaymentResultBase<OtpAuthInfoCubXml>
    {
        public override OtpAuthInfoCubXml CUBXML { get; set; }
        public override bool Verify(string CubKey)
        {
            string content = this.CUBXML.ORDERINFO.STOREID + this.CUBXML.ORDERINFO.ORDERNUMBER + this.CUBXML.ORDERINFO.AMOUNT + this.CUBXML.AUTHINFO.AUTHSTATUS + this.CUBXML.AUTHINFO.AUTHCODE + CubKey;
            return this.CUBXML.CAVALUE.Equals(Security.MD5Hash(content, false));
        }
    }

    public class CathayPaymentOtpFinalResult : CathayPaymentResultBase<OtpOrderInfoCubXml>
    {
        public override OtpOrderInfoCubXml CUBXML { get; set; }
        public override bool Verify(string CubKey)
        {
            string content = this.CUBXML.ORDERINFO.STOREID + this.CUBXML.ORDERINFO.ORDERNUMBER + CubKey;
            return this.CUBXML.CAVALUE.Equals(Security.MD5Hash(content, false));
        }
    }
}

﻿using System;

namespace LunchKingSite.Core.ModelCustom
{
    public class FamiBarcodeData
    {
        /// <summary>
        /// Family_Net_Pincode.Order_no
        /// </summary>
        public int OrderNo { get; set; }
        /// <summary>
        /// 憑證編號
        /// </summary>
        public int CouponId { get; set; }

        /// <summary>
        /// 全家Pincode
        /// </summary>
        public string PezCode { get; set; }

        /// <summary>
        /// 全家四段式條碼
        /// </summary>
        public string Barcode1 { get; set; }
        public string Barcode2 { get; set; }
        public string Barcode3 { get; set; }
        public string Barcode4 { get; set; }
    }

    public class FamiUserCouponBarcodeInfo
    {
        public int CouponId { get; set; }
        public string Pincode { get; set; }
        public string Barcode1 { get; set; }
        public string Barcode2 { get; set; }
        public string Barcode3 { get; set; }
        public string Barcode4 { get; set; }
        public string Barcode5 { get; set; }
        public bool IsLock { get; set; }
        public DateTime? LockTime { get; set; }
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
    }

    public class FamiOrderExgParam
    {
        public string PezCode { get; set; }
        public Guid OrderGuid { get; set; }
        public int CouponId { get; set; }
    }
}

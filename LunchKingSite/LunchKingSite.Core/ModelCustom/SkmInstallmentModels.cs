﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NPOI.HSSF.Record.Formula.Functions;

namespace LunchKingSite.Core.ModelCustom
{
    public class GetInstallEventListModel
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int TotalPages
        {
            get
            {
                if (TotalCount > 0)
                {
                    return TotalCount / PageSize + (TotalCount % PageSize > 0 ? 1 : 0);
                }
                else
                {
                    return 1;
                }
            }
        }
        public string SearchTitle { get; set; }
        public string SortBy { get; set; }
        public string SortType { get; set; }
        public List<SkmInstallEventViewModel> SkmInstallEvents { get; set; }
    }
    public class SkmInstallEventViewModel
    {
        public SkmInstallEventViewModel(ViewSkmInstallEventList data)
        {
            Id = data.Id;
            Title = data.Title;
            StartDate = data.StartDate.ToString("yyyy/MM/dd HH:mm:ss");
            EndDate = data.EndDate.ToString("yyyy/MM/dd HH:mm:ss");
            BankAmount = data.BankAmount ?? default(int);
            ModifyTime = data.ModifyTime == null ? data.CreateTime.ToString("yyyy/MM/dd HH:mm:ss")
                : ((DateTime)data.ModifyTime).ToString("yyyy/MM/dd HH:mm:ss");

            var now = DateTime.Now;
            if (now >= data.StartDate && now <= data.EndDate)
            {
                Status = data.IsAvailable ? Helper.GetDescription(SkmInstallEventStatus.Avaliable)
                    : Helper.GetDescription(SkmInstallEventStatus.Disable);

            }
            else if (now > data.EndDate)
            {
                Status = Helper.GetDescription(SkmInstallEventStatus.Expired);
            }
            else
            {
                Status = Helper.GetDescription(SkmInstallEventStatus.Waiting);
            }

        }
        /// <summary>
        /// 活動編號
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 活動名稱
        /// </summary>
        public string Title { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }
        /// <summary>
        /// 活動底下銀行總數
        /// </summary>
        public int BankAmount { get; set; }

        public string ModifyTime { get; set; }
        /// <summary>
        /// 狀態(SkmInstallEventStatus)
        /// </summary>
		public string Status { get; set; }
    }
    public class SaveInstallEventInfoModel
    {
        public SaveInstallEventInfoModel()
        {

        }
        public SaveInstallEventInfoModel(SkmInstallEvent data)
        {
            Id = data.Id;
            if (Id != 0)
            {
                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                Title = data.Title;
                StartDate = data.StartDate.ToString("yyyy/MM/dd", culture);
                StartTime = data.StartDate.ToString("h:mm tt", culture);
                EndDate = data.EndDate.ToString("yyyy/MM/dd", culture);
                EndTime = data.EndDate.ToString("h:mm tt", culture);
            }
        }
        public int Id { get; set; }
        /// <summary>
        /// 活動名稱
        /// </summary>
        [Required]
        public string Title { get; set; }
        /// <summary>
        /// 開始日期
        /// </summary>
        [Required]
        public string StartDate { get; set; }
        /// <summary>
        /// 開始時間
        /// </summary>
        [Required]
        public string StartTime { get; set; }
        /// <summary>
        /// 結束日期
        /// </summary>
        [Required]
        public string EndDate { get; set; }
        /// <summary>
        /// 結束時間
        /// </summary>
        [Required]
        public string EndTime { get; set; }
    }

    public class EventBankListViewModel
    {
        public EventBankListViewModel()
        {

        }
        public int Id { get; set; }
        /// <summary>
        /// 銀行代碼
        /// </summary>
        public string BankNo { get; set; }
        /// <summary>
        /// 銀行名稱
        /// </summary>
        public string BankName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        /// <summary>
        /// 最後更新時間
        /// </summary>
        public string ModifyTime { get; set; }
        /// <summary>
        /// 有效分期數，頓號分隔
        /// </summary>
        public string Periods { get; set; }
        /// <summary>
        /// 是否為聯名卡
        /// </summary>
        public bool IsCoBranded { get; set; }
    }

    public class SaveInstallBankPeriodInfoModel
    {
        public SaveInstallBankPeriodInfoModel()
        {

        }

        public SaveInstallBankPeriodInfoModel(List<ViewSkmInstallEventBank> data)
        {
            var temp = data.FirstOrDefault();
            if (temp != null)
            {
                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                Id = temp.EventBankId ?? 0;
                EventId = temp.EventId;
                BankNo = temp.BankNo;
                StartDate = ((DateTime)temp.BankStartDate).ToString("yyyy/MM/dd", culture);
                StartTime = ((DateTime)temp.BankStartDate).ToString("h:mm tt", culture);
                EndDate = ((DateTime)temp.BankEndDate).ToString("yyyy/MM/dd", culture);
                EndTime = ((DateTime)temp.BankEndDate).ToString("h:mm tt", culture);
                IsCoBranded = temp.IsCoBranded ?? false;
            }

            InstallBankPeriodList = new List<InstallBankPeriod>();

            foreach (var item in data)
            {
                InstallBankPeriodList.Add(new InstallBankPeriod(item));
            }

        }

        public SaveInstallBankPeriodInfoModel(SkmInstallEvent data)
        {
            if (data != null)
            {
                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                Id = 0;
                EventId = data.Id;
                StartDate = ((DateTime)data.StartDate).ToString("yyyy/MM/dd", culture);
                StartTime = ((DateTime)data.StartDate).ToString("h:mm tt", culture);
                EndDate = ((DateTime)data.EndDate).ToString("yyyy/MM/dd", culture);
                EndTime = ((DateTime)data.EndDate).ToString("h:mm tt", culture);
            }
        }
        /// <summary>
        /// Event銀行編號
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 活動編號
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 銀行代碼
        /// </summary>
        public string BankNo { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string EndDate { get; set; }
        public string EndTime { get; set; }
        /// <summary>
        /// 是否為聯名卡
        /// </summary>
        public bool IsCoBranded { get; set; }
        public List<InstallBankPeriod> InstallBankPeriodList { get; set; }
    }

    public class InstallBankPeriod
    {
        public InstallBankPeriod()
        {

        }
        public InstallBankPeriod(ViewSkmInstallEventBank data)
        {
            Id = data.PeriodId ?? default(int);
            InstallPeriod = data.InstallPeriod ?? default(int);
            Amount = data.Amount ?? default(int);
            PayFee = data.PayFee ?? default(int);
            InterestRate = data.InterestRate ?? default(decimal);
            IsAvailable = data.PeriodIsAvailable ?? false;
        }
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// 分期期數
        /// </summary>
        [JsonProperty("installPeriod")]
        public int InstallPeriod { get; set; }
        /// <summary>
        /// 分期門檻
        /// </summary>
        [JsonProperty("amount")]
        public int Amount { get; set; }
        /// <summary>
        /// 手續費
        /// </summary>
        [JsonProperty("payFee")]
        public int PayFee { get; set; }
        /// <summary>
        /// 利息
        /// </summary>
        [JsonProperty("interestRate")]
        public decimal InterestRate { get; set; }
        [JsonProperty("isAvailable")]
        public bool IsAvailable { get; set; }
    }

    public class SkmInstallmentInfoViewModel
    {
        public SkmInstallmentInfoViewModel()
        {
            InstallmentInfos = new Dictionary<string, InstallmentInfo>();
        }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        /// <summary>
        /// 消費金額
        /// </summary>
        public int PayAmount { get; set; }

        public Dictionary<string, InstallmentInfo> InstallmentInfos { get; set; }
    }

    public class InstallmentInfo
    {
        /// <summary>
        /// 期數
        /// </summary>
        public int InstallPeriod { get; set; }
        /// <summary>
        /// 每期金額
        /// </summary>
        public int InstallPay { get; set; }
        /// <summary>
        /// 分期門檻
        /// </summary>
        public int Amount { get; set; }
        /// <summary>
        /// 銀行資訊
        /// </summary>
        public List<BankDetail> BankDetails { get; set; }
    }

    public class BankDetail
    {
        public string BankName { get; set; }
        /// <summary>
        /// 手續費
        /// </summary>
        public int PayFee { get; set; }
        /// <summary>
        /// 是否為聯名卡
        /// </summary>
        public bool IsCoBranded { get; set; }
        /// <summary>
        /// 最後修改時間(排序條件)
        /// </summary>
        public DateTime? Modifytime {get;set;}
    }

}
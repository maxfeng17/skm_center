﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelCustom
{
    public class SubscriptionModel
    {
        public SubscriptionModel()
        {

        }
        public int Id { get; set; }
        public string Email { get; set; }
        public bool? Reliable { get; set; }
        public int? Status { get; set; }
    }
}

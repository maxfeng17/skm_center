﻿using System;

namespace LunchKingSite.Core.ModelCustom
{
    /// <summary>
    /// Seller Tree Custom Model
    /// </summary>
    public class SellerTreeModel
    {
        /// <summary>
        /// Seller Guid
        /// </summary>
        public Guid SellerGuid { set; get; }
        /// <summary>
        /// Parent Seller Guid
        /// </summary>
        public Guid ParentSellerGuid { set; get; }
        /// <summary>
        /// Root seller Guid
        /// </summary>
        public Guid RootSellerGuid { set; get; }
        /// <summary>
        /// Sort
        /// </summary>
        public int Sort { set; get; }
    }
}

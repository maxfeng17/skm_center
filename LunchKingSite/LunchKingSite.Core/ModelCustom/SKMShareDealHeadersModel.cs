﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelCustom
{
    public class SKMShareDealHeadersModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageSrc { get; set; }
        public string AlIosUrl { get; set; }
        public string AlIosAppStoreId { get; set; }
        public string AlIosAppName { get; set; }
        public string AlAndroidUrl { get; set; }
        public string AlAndroidPackage { get; set; }
        public string AlAndroidAppName { get; set; }
        public string OgTitle { get; set; }
        public string OgType { get; set; }
        public string OgUrl { get; set; }
        public string OgImage { get; set; }
        public string OgSiteName { get; set; }
        public string OgDescription { get; set; }
    }
}

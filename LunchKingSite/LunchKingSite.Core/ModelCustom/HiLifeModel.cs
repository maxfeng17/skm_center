﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelCustom
{
    public class GetPincodeApiInput
    {
        /// <summary>
        /// 訂單GUID
        /// </summary>
        public Guid OrderGuid { get; set; }

        /// <summary>
        /// 兌換張數
        /// </summary>
        public int ExchangeCount { get; set; }
    }

    public class GetHiLifePincodeByGiftInput
    {
        /// <summary>
        /// 訂單GUID
        /// </summary>
        public Guid OrderGuid { get; set; }

        /// <summary>
        /// 兌換張數
        /// </summary>
        public int ExchangeCount { get; set; }

        /// <summary>
        /// 禮物連結
        /// </summary>
        public string AsCode { get; set; }
    }

    public class HiLifePincodeModel
    {
        /// <summary>
        /// 憑證編號
        /// </summary>
        public int CouponId { get; set; }
       
        /// <summary>
        /// 萊爾富兩段式條碼
        /// </summary>
        public List<string> PincodeList { get; set; }
    }

    public class ReturnPincodeApiInput
    {
        public Guid Bid { get; set; }
        public Guid OrderGuid { get; set; }
    }


    #region HiLifeApi 規範格式
    public class ApiVerifyPincodeApiInput
    {
        public string Head { get; set; }
        public string Body { get; set; }

        public bool IsBatch { get; set; }
    }

    public class PincodeReturnApiResult
    {
        public List<ReturnDetail> PinCodeList { get; set; }
    }

    public class ReturnDetail
    {
        public string Code { get; set; }
        public string Active { get; set; }
        public string Reason { get; set; }
    }



    #endregion
}

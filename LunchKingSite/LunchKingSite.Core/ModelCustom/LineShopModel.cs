﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelCustom
{
    public class LineShopModel
    {
    }

    public class LineShopOrderListInfo
    {
        public Guid OrderGuid { get; set; }
        public Guid Bid { get; set; }
        public string ItemName { get; set; }
        public int OrderAmount { get; set; }
        public int CashTrustLogAmount { get; set; }
        public int CashTrustLogDiscount { get; set; }
        public int CashTrustLogBcash { get; set; }
    }
}

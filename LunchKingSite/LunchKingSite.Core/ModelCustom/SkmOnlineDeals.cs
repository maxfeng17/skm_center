﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelCustom
{
    [ComplexType]
    public class SkmOnlineDeals
    {
        public SkmOnlineDeals()
        {
            item_name = string.Empty;
            plu_id = string.Empty;
            god_no = string.Empty;
            is_skmpay = string.Empty;
            store_no = string.Empty;
            sap_tnt_id = string.Empty;
            category = string.Empty;
            sub_category = string.Empty;
        }
        /// <summary>
        /// 商品建檔編號
        /// </summary>
        [JsonProperty("ItemId")]
        public Guid item_id { get; set; }
        /// <summary>
        /// 品名
        /// </summary>
        [JsonProperty("ItemName")]
        public string item_name { get; set; }
        /// <summary>
        /// 商品條碼
        /// </summary>
        [JsonProperty("PluId")]
        public string plu_id { get; set; }
        /// <summary>
        /// 商品貨號
        /// </summary>
        [JsonProperty("GodNo")]
        public string god_no { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        [JsonProperty("Price")]
        public decimal price { get; set; }
        /// <summary>
        /// 條碼原價
        /// </summary>
        [JsonProperty("PluPrice")]
        public decimal? plu_price { get; set; }
        /// <summary>
        /// 售價
        /// </summary>
        [JsonProperty("SellPrice")]
        public int sell_price { get; set; }
        /// <summary>
        /// 是否用skmpay
        /// </summary>
        [JsonProperty("IsSkmpay")]
        public string is_skmpay { get; set; }
        /// <summary>
        /// 蝙蝠埋碼編號
        /// </summary>
        [JsonProperty("BatNo")]
        public Guid bat_no { get; set; }
        /// <summary>
        /// 上架時間
        /// </summary>
        [JsonProperty("StartDate")]
        public DateTime start_date { get; set; }
        /// <summary>
        /// 下架時間
        /// </summary>
        [JsonProperty("EndDate")]
        public DateTime end_date { get; set; }
        /// <summary>
        /// 館別代號
        /// </summary>
        [JsonProperty("StoreNo")]
        public string store_no { get; set; }
        /// <summary>
        /// 7X櫃號
        /// </summary>
        [JsonProperty("SapTntId")]
        public string sap_tnt_id { get; set; }
        /// <summary>
        /// 主分類
        /// </summary>
        [JsonProperty("Category")]
        public string category { get; set; }
        /// <summary>
        /// 子分類
        /// </summary>
        [JsonProperty("SubCategory")]
        public string sub_category { get; set; }
        /// <summary>
        /// 建檔時間
        /// </summary>
        [JsonProperty("OpDate")]
        public DateTime op_date { get; set; }
    }
}

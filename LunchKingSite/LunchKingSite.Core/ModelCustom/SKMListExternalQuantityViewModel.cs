﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.BizLogic.Models.Skm
{
    public class SKMListExternalQuantityViewModel
    {
        public string ShopName { get; set; }
        public string ShopCode { get; set; }
        //分店Guid
        public Guid SellerGuid { get; set; }
        //上架數量
        public decimal OrderTotalLimit { get; set; }
        public Guid ExternalDealGuid { get; set; }
        //訂購數量
        public int OrderedQuantity { get; set; }
        //已取貨數量
        public int VerifiedQuantity { get; set; }
        //未取貨數量
        public int NotVerifiedQuantity { get; set; }
        //取消數量(未核銷退貨)
        public int CanceledNotVerifiedQuantity { get; set; }
        //退貨數量(已核銷退貨)
        public int CanceledVerifiedQuantity { get; set; }
        //剩下數量
        public int RemainingQuantity { get; set; }
    }
}

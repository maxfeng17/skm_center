﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelCustom
{
    [ComplexType]
    public class SkmCurationEvents
    {
        public SkmCurationEvents()
        {
            curation_typena = string.Empty;
            curation_name = string.Empty;
            shop_no = string.Empty;
        }
        /// <summary>
        /// 策展ID
        /// </summary>
        [JsonProperty("CurationId")]
        public int curation_id { get; set; }
        /// <summary>
        /// 策展類型代碼
        /// </summary>
        [JsonProperty("CurationTypeId")]
        public byte curation_typeid { get; set; }
        /// <summary>
        /// 策展類型
        /// </summary>
        [JsonProperty("CurationTypeNa")]
        public string curation_typena { get; set; }
        /// <summary>
        /// 策展名稱
        /// </summary>
        [JsonProperty("CurationName")]
        public string curation_name { get; set; }
        /// <summary>
        /// 策展開始時間
        /// </summary>
        [JsonProperty("StartDate")]
        public DateTime start_date { get; set; }
        /// <summary>
        /// 策展結束時間
        /// </summary>
        [JsonProperty("EndDate")]
        public DateTime end_date { get; set; }
        /// <summary>
        /// 策展店別
        /// </summary>
        [JsonProperty("ShopNo")]
        public string shop_no { get; set; }
        /// <summary>
        /// 策展建立時間
        /// </summary>
        [JsonProperty("OpDate")]
        public DateTime op_date { get; set; }
    }
}

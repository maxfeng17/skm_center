﻿using System;

namespace LunchKingSite.Core.ModelCustom
{
    public class DevicePushRecordReadModel
    {
        public int RecordId { get; set; }
        public int ActionId { get; set; }
        public int PushAppId { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime ReadTime { get; set; }
    }
}

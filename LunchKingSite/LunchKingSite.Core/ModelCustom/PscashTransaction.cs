﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelCustom
{
    public class PscashDepositBalance
    {
        public int DepositId { get; set; }

        public decimal Balance { get; set; }

        public string PezAuthCode { get; set; }

        public Guid Guid { get; set; }
    }

    public class PscashWithdrawalView
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public int DepositId { get; set; }
        public Guid OrderGuid { get; set; }
        public int UserId { get; set; }
        public int PcashXchOrderId { get; set; }
    }

}

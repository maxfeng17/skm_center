﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Models
{
    public class PponDeal
    {
        public Seller Store { get; set; }
        public BusinessHour Deal { get; set; }
        public Item ItemDetail { get; set; }
        public CouponEventContent DealContent { get; set; }
        public DealTimeSlotCollection TimeSlotCollection { get; set; }
        public AccessoryGroupCollection AccessoryGroups { get; set; }
        public ItemAccessoryGroupListCollection ItemAccessory { get; set; }
        public DealProperty Property { get; set; }
        public DealPayment Payment { get; set; }

        public PponDeal()
        {
            Init(false);
        }

        public PponDeal(bool isNew)
        {
            Init(isNew);
        }

        private void Init(bool isNew)
        {
            Store = new Seller() {IsNew = isNew, IsLoaded = !isNew};
            Deal = new BusinessHour() { IsNew = isNew, IsLoaded = !isNew };
            ItemDetail = new Item() { IsNew = isNew, IsLoaded = !isNew };
            DealContent = new CouponEventContent() { IsNew = isNew, IsLoaded = !isNew };
            TimeSlotCollection = new DealTimeSlotCollection();
            AccessoryGroups = new AccessoryGroupCollection();
            ItemAccessory = new ItemAccessoryGroupListCollection();
            Property = new DealProperty();
            Payment = new DealPayment();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Store = [\n").Append(Helper.Object2String(Store, "\n", 1, "  ")).Append("]\n");
            sb.Append("Deal = [\n").Append(Helper.Object2String(Deal, "\n", 1, "  ")).Append("]\n");
            sb.Append("ItemDetail = [\n").Append(Helper.Object2String(ItemDetail, "\n", 1, "  ")).Append("]\n");
            sb.Append("DealContent = [\n").Append(Helper.Object2String(DealContent, "\n", 1, "  ")).Append("]\n");
            sb.Append("DealProperty = [\n").Append(Helper.Object2String(Property, "\n", 1, "  ")).Append("]\n");

            sb.Append("TimeSlotCollection = [");
            if(TimeSlotCollection != null)
            {
                sb.Append('\n');
                foreach (DealTimeSlot dealTimeSlot in TimeSlotCollection)
                    sb.Append(Helper.Object2String(dealTimeSlot, "\n", 1, "  "));
            }
            else
                sb.Append("null");
            sb.Append("]\n");

            return sb.ToString();
        }
    }

    /// <summary>
    /// this class is only used to store availability and will be persisted as json, therefore its property name is all abbreviated
    /// </summary>
    public sealed class AvailableInformation
    {
        /// <summary>
        /// Name
        /// </summary>
        public string N { get; set; }
        /// <summary>
        /// Phone
        /// </summary>
        public string P { get; set; }
        /// <summary>
        /// Address
        /// </summary>
        public string A { get; set; }
        /// <summary>
        /// 經度
        /// </summary>
        public string Longitude { get; set; }
        /// <summary>
        /// 緯度
        /// </summary>
        public string Latitude { get; set; }
        /// <summary>
        /// Opening hours
        /// </summary>
        public string OT { get; set; }
        /// <summary>
        /// Opening days
        /// </summary>
        public string OD { get; set; }
        /// <summary>
        /// Use Time
        /// </summary>
        public string UT { get; set; }
        /// <summary>
        /// Url of the website
        /// </summary>
        public string U { get; set; }
        /// <summary>
        /// Special remarks
        /// </summary>
        public string R { get; set; } // remark
        /// <summary>
        /// Close Date
        /// </summary>
        public string CD { get; set; }
        /// <summary>
        /// MRT
        /// </summary>
        public string MR { get; set; }
        /// <summary>
        /// Car
        /// </summary>
        public string CA { get; set; }
        /// <summary>
        /// Bus
        /// </summary>
        public string BU { get; set; }
        /// <summary>
        /// Other Vehicles
        /// </summary>
        public string OV { get; set; }
        /// <summary>
        /// Facebook Url
        /// </summary>
        public string FB { get; set; }
        /// <summary>
        /// Plurk Url
        /// </summary>
        public string PL { get; set; }
        /// <summary>
        /// Blog Url
        /// </summary>
        public string BL { get; set; }
        /// <summary>
        /// Other Url
        /// </summary>
        public string OL { get; set; }

        public bool HasLongLat
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Longitude) ||
                    Longitude.Equals("null", StringComparison.OrdinalIgnoreCase) ||
                    Longitude.Equals("0") || string.IsNullOrWhiteSpace(Latitude) ||
                    string.IsNullOrWhiteSpace(Latitude) ||
                    Latitude.Equals("null", StringComparison.OrdinalIgnoreCase) ||
                    Latitude.Equals("0"))
                {
                    return false;
                }
                return true;
            }
        }
    }
    public static class PponModelExtension
    {
        #region Ppon
        public static PponDealStage GetDealStage(this IViewPponDeal deal, DateTime currentTime)
        {
            if (deal != null)// && deal.IsLoaded)
            {
                if (deal.GroupOrderGuid.HasValue && currentTime >= deal.BusinessHourOrderTimeS)
                {
                    //已結檔
                    if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.Completed) || currentTime >= deal.BusinessHourOrderTimeE)
                    {
                        int orderedquantity = 0;
                        int slug;
                        if (int.TryParse(deal.Slug, out slug))
                            orderedquantity = slug;
                        else
                            orderedquantity = (deal.OrderedQuantity ?? 0);

                        return orderedquantity >= deal.BusinessHourOrderMinimum
                                   ? Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.PponCouponGenerated)
                                         ? PponDealStage.CouponGenerated
                                         : PponDealStage.ClosedAndOn
                                   : PponDealStage.ClosedAndFail;
                    }
                    
                    //是否接續其他檔次銷售數字
                    if (deal.IsContinuedQuantity??false)
                    {
                        int ordered_quantity_with_continued = (deal.OrderedQuantity ?? 0) + (deal.ContinuedQuantity ?? 0);
                        return ordered_quantity_with_continued >= deal.BusinessHourOrderMinimum
                                   ? deal.OrderedQuantity >= (deal.OrderTotalLimit ?? decimal.MaxValue)
                                         ? PponDealStage.RunningAndFull
                                         : PponDealStage.RunningAndOn
                                   : PponDealStage.Running;
                    }
                    else
                    {
                        return (deal.OrderedQuantity ?? 0) >= deal.BusinessHourOrderMinimum
                                   ? deal.OrderedQuantity >= (deal.OrderTotalLimit ?? decimal.MaxValue)
                                         ? PponDealStage.RunningAndFull
                                         : PponDealStage.RunningAndOn
                                   : PponDealStage.Running;
                    }
                }
                else if (deal.GroupOrderGuid.HasValue && currentTime < deal.BusinessHourOrderTimeS)
                {
                    return PponDealStage.Ready;
                }
                else
                {

                    return PponDealStage.Created;
                }
            }
            return PponDealStage.NotExist;
        }

        public static PponDealStage GetDealStage(this IViewPponDeal deal)
        {
            return GetDealStage(deal, DateTime.Now);
        }

        public static bool DealIsOrderable(this IViewPponDeal deal)
        {
            PponDealStage stage = GetDealStage(deal);
            return stage == PponDealStage.Running || stage == PponDealStage.RunningAndOn;
        }
        #endregion
    }
}

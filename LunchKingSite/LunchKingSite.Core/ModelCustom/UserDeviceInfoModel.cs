﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelCustom
{
    public class UserDeviceInfoModel
    {
        public string Token { get; set; }
        public string ShortToken { get; set; }
        public int DeviceId { get; set; }
        public string Model { get; set; }
        public string OsVer { get; set; }
        public string AppVer { get; set; }
        public MobileOsType OsType { get; set; }
        public DateTime LastUsedTime { get; set; }
    }
}

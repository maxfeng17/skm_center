﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.Core.ModelCustom
{
    /// <summary>
    /// skmpay訂單交易明細
    /// </summary>
    [ComplexType]
    public class SkmpayOrderInfo
    {
        public SkmpayOrderInfo()
        {
            shop_no = string.Empty;
            order_no = string.Empty;
            inv_no = string.Empty;
            item_name = string.Empty;
            sap_tnt_id = string.Empty;
        }

        /// <summary>
        /// 館別代號
        /// </summary>
        [JsonProperty("ShopNo")]
        public string shop_no { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        [JsonProperty("OrderNo")]
        public string order_no { get; set; }
        /// <summary>
        /// 發票號碼
        /// </summary>
        [JsonProperty("InvNo")]
        public string inv_no { get; set; }
        /// <summary>
        /// 蝙蝠埋碼編號 (MainBid)
        /// </summary>
        [JsonProperty("BatNo")]
        public Guid bat_no { get; set; }
        /// <summary>
        /// 建檔代號 (ExternalDealGuid)
        /// </summary>
        [JsonProperty("ItemNo")]
        public Guid item_no { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        [JsonProperty("ItemName")]
        public string item_name { get; set; }
        /// <summary>
        /// 訂購狀態 (1:下訂單 2:核銷 3:未核銷退貨 4:己核銷退貨)
        /// </summary>
        [JsonProperty("StatusNo")]
        public int status_no { get; set; }
        /// <summary>
        /// 訂單金額
        /// </summary>
        [JsonProperty("Amount")]
        public int amount { get; set; }
        /// <summary>
        /// 事件產生時間
        /// </summary>
        [JsonProperty("OpDate")]
        public DateTime? op_date { get; set; }
        /// <summary>
        /// 核銷櫃位
        /// </summary>
        [JsonProperty("SapTntId")]
        public string sap_tnt_id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelCustom
{

    public class BeaconTriggerModel
    {
        /// <summary>
        /// 裝置識別碼
        /// </summary>
        public string identifierCode { get; set; }
        /// <summary>
        /// Beacon觸發UUID
        /// </summary>
        public string uuId { get; set; }
        /// <summary>
        /// major
        /// </summary>
        public int major { get; set; }
        /// <summary>
        /// minor
        /// </summary>
        public int minor { get; set; }
    }

    #region Beacon & MessageBox Class

    /// <summary>
    /// 事件觸發欄位
    /// </summary>
    public abstract class BaseActivityParameter
    {
        /// <summary>
        /// 事件觸發欄位
        /// </summary>
        protected BaseActivityParameter()
        {
            Uuids = new List<string>();
            Bid = string.Empty;
            Url = string.Empty;
            MessageId = string.Empty;
            Message = string.Empty;
            MessageTitle = string.Empty;
            TriggerType = PokeballTriggerType.None;
        }

        /// <summary>
        /// 訊息標題(通知中心標題)
        /// </summary>
        public string MessageTitle { get; set; }

        /// <summary>
        /// 檔次Bid
        /// </summary>
        public string Bid { get; set; }
        /// <summary>
        /// 事件觸發自訂網址
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 訊息中心-訊息Id
        /// </summary>
        public string MessageId { get; set; }
        /// <summary>
        /// 訊息中心-訊息內容
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 觸發事件
        /// </summary>
        public PokeballTriggerType TriggerType { get; set; }
        /// <summary>
        /// Beacon觸發監聽Id-UUID
        /// </summary>
        public List<string> Uuids { get; set; }
    }

    /// <summary>
    /// App-觸發事件Pokeball
    /// </summary>
    public class Pokeball : BaseActivityParameter
    {
        /// <summary>
        /// App-觸發事件Pokeball
        /// </summary>
        public Pokeball() : base()
        {
            ActionList = new List<Action>();
        }

        /// <summary>
        /// 事件觸發行為
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 後續觸發行為List
        /// </summary>
        public List<Action> ActionList { get; set; }
    }

    public enum PokeballType
    {
        localPush = 1,
        FullscreenWebView = 2,
        MarkMessageRead = 3,
        showDealDetail = 4,
        beaconListener = 5
    }

    public enum PokeballTriggerType
    {
        None = 0,
        BeaconMessageRead = 1,
        MessageBoxRead = 2
    }

    public enum PokeballActionType
    {
        None = 0,
        /// <summary>
        /// 標註已讀=閱
        /// </summary>
        [Description("markMessageRead")]
        MarkMessageRead = 1,
        /// <summary>
        /// 顯示檔次內容
        /// </summary>
        [Description("showDealDetail")]
        ShowDealDetail = 2,
        /// <summary>
        /// 網址推播
        /// </summary>
        [Description("fullscreenWebView")]
        FullscreenWebView = 3,
        /// <summary>
        /// 子訊息(群組訊息)
        /// </summary>
        [Description("groupMessage")]
        GroupMessage = 4,
        /// <summary>
        /// 純文字
        /// </summary>
        [Description("pureText")]
        PureText = 5,
        /// <summary>
        /// 訂單列表未領取
        /// </summary>
        [Description("orderListUnused")]
        OrderListUnused = 6,
        /// <summary>
        /// 訂單列表退貨
        /// </summary>
        [Description("orderListReturn")]
        OrderListReturn = 7,
        /// <summary>
        /// DeepLink(NoAction)
        /// </summary>
        [Description("Deeplink")]
        DeepLink = 8
    }

    /// <summary>
    /// 點擊觸發行為
    /// </summary>
    public class Action : BaseActivityParameter
    {
        /// <summary>
        /// 點擊觸發行為
        /// </summary>
        public Action() : base()
        {
            ActionType = string.Empty;
            MessageTitle = string.Empty;
        }
        /// <summary>
        /// 點擊觸發行為類型
        /// </summary>
        public string ActionType { get; set; }

    }

    #endregion Beacon & MessageBox Class

    public class BeaconMessageModel
    {
        public int Id { get; set; }
        public bool IsRead { get; set; }
        public bool IsRemove { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string ActionUrl { get; set; }
        public int MemberId { get; set; }
        public DateTime CreateTime { get; set; }
        public int EventType { get; set; }
        public Guid ActionBid { get; set; }
    }

    public class BeaconGroupIdDeivceId
    {
        public int GroupId { get; set; }
        public int DeviceId { get; set; }
    }

    public class BeaconChangeManageInfo
    {
        public int DeviceId { get; set; }
        public string DeviceName { get; set; }
        public int Major { get; set; }
        public int Minor { get; set; }
        public string GroupRemark { get; set; }
        public string GroupCode { get; set; }
        public string AuxiliaryCode { get; set; }
    }

    public class BeaconTriggerLogInfo
    {
        public int EventId { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public int DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string EventSubject { get; set; }
        public string GroupRemark { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
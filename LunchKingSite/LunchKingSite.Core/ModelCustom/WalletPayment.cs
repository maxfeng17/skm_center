﻿using System.Collections.Generic;
namespace LunchKingSite.DataOrm
{
    public class TaishinECPayCreditCardResponse
    {
        /// <summary>
        /// 卡片的Key值
        /// </summary>
        public string CardKey { get; set; }
        /// <summary>
        /// 卡片對應的X
        /// </summary>
        public string CardToken { get; set; }
        /// <summary>
        /// 遮蔽後的信用卡號0000-XXXX-XXXX-0000
        /// </summary>
        public string CardNumberShelter { get; set; }
        /// <summary>
        /// 卡片名稱
        /// </summary>
        public string CardName { get; set; }
        /// <summary>
        /// 卡片類型 VisaCard / MasterCard
        /// </summary>
        public int CardType { get; set; }
        /// <summary>
        /// 到期年月
        /// </summary>
        public string ExpiredDate { get; set; }
        public string MemberId { get; set; }

    }
    public class TaishinCardInfo
    {
        public string MemberId { get; set; }
        public string CardName { get; set; }
        public string CardNumber { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }

    }

    public class TaishinWalletCardQryResp
    {
        /// <summary>
        /// 遮蔽後的信用卡號0000-XXXX-XXXX-0000
        /// </summary>
        public string CardNumberShelter { get; set; }
        /// <summary>
        /// 卡片名稱
        /// </summary>
        public string CardName { get; set; }
        /// <summary>
        /// 卡片狀態
        /// </summary>
        public int CardStatus { get; set; }
        /// <summary>
        /// 卡片綁定時間
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 銀行名稱
        /// </summary>
        public string BankName { get; set; }

    }

    public class TaishinECPayAuthResp
    {
        /// <summary>
        /// 授權Key
        /// </summary>
        public string AuthKey { get; set; }
        /// <summary>
        /// RedirectUrl
        /// </summary>
        public string RedirectUrl { get; set; }
        /// <summary>
        /// PostBackUrl
        /// </summary>
        public string PostBackUrl { get; set; }
        /// <summary>
        /// 卡片是否已存在
        /// </summary>
        public bool IsCardExist { get; set; }
    }

    public class TaishinECPayBarcode
    {
        /// <summary>
        /// 條碼
        /// </summary>
        public string Barcode { get; set; }
        /// <summary>
        /// 卡片對應的X
        /// </summary>
        public string CardToken { get; set; }
    }

    public class TaishinECPayTradeResponse
    {
        /// <summary>
        /// 門市名稱
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 門市電話
        /// </summary>
        public string StoreTel { get; set; }
        /// <summary>
        /// 門市地址
        /// </summary>
        public string StoreAddress { get; set; }
        /// <summary>
        /// 交易日期
        /// </summary>
        public string MerchantTradeDate { get; set; }
        /// <summary>
        /// 交易金額
        /// </summary>
        public decimal TradeAmount { get; set; }
        /// <summary>
        /// 遮蔽後的卡號
        /// </summary>
        public string CardNumberShelter { get; set; }
        /// <summary>
        /// 卡片名稱
        /// </summary>
        public string CardName { get; set; }
        /// <summary>
        /// 交易狀態
        /// </summary>
        public string TradeStatus { get; set; }
        /// <summary>
        /// 交易狀態名稱
        /// </summary>
        public string TradeStatusName { get; set; }
    }
    public class TaishinECPayTradeApp
    {
        public int TotalCount { get; set; }
        public List<WalletPymentTradeList> WalletPaymentList { get; set; }
    }
    public class WalletPymentTradeList
    {
        /// <summary>
        /// 特店代碼
        /// </summary>
        public string MerchantID { get; set; }
        /// <summary>
        /// 店名
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 電話
        /// </summary>
        public string StoreTel { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string StoreAddress { get; set; }
        /// <summary>
        /// 交易時間
        /// </summary>
        public string MerchantTradeDate { get; set; }
        public string ServiceTradeDate { get; set; }
        public string ServiceTradeNo { get; set; }
        /// <summary>
        /// 交易金額
        /// </summary>
        public decimal TradeAmount { get; set; }
        /// <summary>
        /// 交易狀態
        /// </summary>
        public int TradeStatus { get; set; }
        public string TradeStatusName { get; set; }
        /// <summary>
        /// 遮蔽後的信用卡號0000-XXXX-XXXX-0000
        /// </summary>
        public string CardNumberShelter { get; set; }
        /// <summary>
        /// 卡片名稱
        /// </summary>
        public string CardName { get; set; }
        /// <summary>
        /// 銀行客服電話
        /// </summary>
        public string BankServicePhone { get; set; }
        /// <summary>
        /// 發卡銀行名稱
        /// </summary>
        public string BankName { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 交易明細
        /// </summary>
        public List<TradeDetail> TradeDetail { get; set; }
        /// <summary>
        /// 交易類型
        /// </summary>
        public int Platform { get; set; }
        /// <summary>
        /// 退貨日期
        /// </summary>
        public string RtnTradeDate { get; set; }
        /// <summary>
        /// 授權碼
        /// </summary>
        public string AuthIdResp { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
    }



    public class WalletPaymentPlatform
    {
        public WalletPaymentPlatform()
        {
            Merchant = new WalletPymentCardECPayResponse();
        }
        public string BankTokenId { get; set; }
        public string MerchantTokenId { get; set; }
        public WalletPymentCardECPayResponse Merchant { get; set; }

    }

    public class WalletPymentCardECPayResponse
    {
        /// <summary>
        /// 通路代號
        /// </summary>
        public string MerchantID { get; set; }
        /// <summary>
        /// 店號
        /// </summary>
        public string StoreID { get; set; }
        /// <summary>
        /// 店名
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// POS交易序號
        /// </summary>
        public string MerchantTradeNo { get; set; }
        /// <summary>
        /// POS機號
        /// </summary>
        public string TID { get; set; }
        /// <summary>
        /// POS交易日期
        /// </summary>
        public string MerchantTradeDate { get; set; }
        /// <summary>
        /// POS交易時間
        /// </summary>
        public string MerchantTradeTime { get; set; }
        /// <summary>
        /// 支付條碼
        /// </summary>
        public string BarCode { get; set; }
        public string BarCode2 { get; set; }
        public string BarCode3 { get; set; }
        public string BarCode4 { get; set; }
        public string BarCode5 { get; set; }
        /// <summary>
        /// 交易金額
        /// </summary>
        public string TradeAmount { get; set; }
        /// <summary>
        /// 扣款金流類型
        /// </summary>
        public string PaymentType { get; set; }
        /// <summary>
        /// 交易處理狀態
        /// </summary>
        public string RtnCode { get; set; }
        /// <summary>
        /// 交易回覆訊息
        /// </summary>
        public string RtnMsg { get; set; }
        /// <summary>
        /// 17Life交易編號
        /// </summary>
        public string ServiceTradeNo { get; set; }
        /// <summary>
        /// 17Life回傳日期
        /// </summary>
        public string ServiceTradeDate { get; set; }
        /// <summary>
        /// 17Life回傳時間
        /// </summary>
        public string ServiceTradeTime { get; set; }
        /// <summary>
        /// 交易後餘額
        /// </summary>
        public int AvailableAmount { get; set; }
        /// <summary>
        /// 交易類別
        /// </summary>
        public int TradeType { get; set; }
        /// <summary>
        /// 單筆交易金額上限
        /// </summary>
        public int TradeAmountLimit { get; set; }
        /// <summary>
        /// 退貨交易序號
        /// </summary>
        public string RTNTradeNo { get; set; }
        /// <summary>
        /// 退貨交易日期
        /// </summary>
        public string RTNTradeDate { get; set; }
        /// <summary>
        /// 退貨交易時間
        /// </summary>
        public string RTNTradeTime { get; set; }
        /// <summary>
        /// 17Life備註一
        /// </summary>
        public string ServiceInfo1 { get; set; }
        /// <summary>
        /// 17Life備註二
        /// </summary>
        public string ServiceInfo2 { get; set; }
        /// <summary>
        /// 17Life備註三
        /// </summary>
        public string ServiceInfo3 { get; set; }
        /// <summary>
        /// 備註一
        /// </summary>
        public string Remark1 { get; set; }
        /// <summary>
        /// 備註二
        /// </summary>
        public string Remark2 { get; set; }
        /// <summary>
        /// 備註三
        /// </summary>
        public string Remark3 { get; set; }
        /// <summary>
        /// 備註一【退款用】
        /// </summary>
        public string OrderExtraInfo1 { get; set; }
        /// <summary>
        /// 備註二【退款用】
        /// </summary>
        public string OrderExtraInfo2 { get; set; }
        /// <summary>
        /// 備註三【退款用】
        /// </summary>
        public string OrderExtraInfo3 { get; set; }
        /// <summary>
        /// 連線狀態
        /// </summary>
        public int Status { get; set; }
    }

    public class TaishinWalletQry
    {
        public string MemberId { get; set; }
        public string CardStatus { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
    }
    public class MerchantTradeECPayData
    {
        /// <summary>
        /// 台新Key
        /// </summary>
        public string BankKey { get; set; }
        /// <summary>
        /// 通路資料
        /// </summary>
        public MerchantTradeData MerchantTrade { get; set; }
    }

    public class ThinkPowerMerchantTradeECPayData
    {
        /// <summary>
        /// 台新Key
        /// </summary>
        public string BankKey { get; set; }
        /// <summary>
        /// 通路資料
        /// </summary>
        public ThinkPowerMerchantTradeData MerchantTrade { get; set; }
    }

    public class ThinkPowerMerchantTradeData
    {
        /// <summary>
        /// 通路代號
        /// </summary>
        public string MerchantID { get; set; }
        /// <summary>
        /// 店號
        /// </summary>
        public string StoreID { get; set; }
        /// <summary>
        /// 店名
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 店家電話
        /// </summary>
        public string StoreTel { get; set; }
        /// <summary>
        /// 店家地址
        /// </summary>
        public string StoreAddress { get; set; }
        /// <summary>
        /// POS交易序號
        /// </summary>
        public string MerchantTradeNo { get; set; }
        /// <summary>
        /// POS交易日期
        /// </summary>
        public string MerchantTradeDate { get; set; }
        /// <summary>
        /// POS交易時間
        /// </summary>
        public string MerchantTradeTime { get; set; }
        /// <summary>
        /// POS機號
        /// </summary>
        public string TID { get; set; }
        /// <summary>
        /// 支付條碼
        /// </summary>
        public string BarCode { get; set; }
        /// <summary>
        /// 支付條碼2
        /// </summary>
        public string BarCode2 { get; set; }
        /// <summary>
        /// 支付條碼3
        /// </summary>
        public string BarCode3 { get; set; }
        /// <summary>
        /// 支付條碼4
        /// </summary>
        public string BarCode4 { get; set; }
        /// <summary>
        /// 支付條碼5
        /// </summary>
        public string BarCode5 { get; set; }
        /// <summary>
        /// 交易金額
        /// </summary>
        public string TradeAmount { get; set; }
        /// <summary>
        /// 扣款金流類型  04 信用卡
        /// </summary>
        public string PaymentType { get; set; }
        /// <summary>
        /// 交易處理狀態
        /// </summary>
        public string RtnCode { get; set; }
        /// <summary>
        /// 交易回覆訊息
        /// </summary>
        public string RtnMsg { get; set; }
        /// <summary>
        /// 廠商交易編號
        /// </summary>
        public string ServiceTradeNo { get; set; }
        /// <summary>
        /// 廠商交易日期
        /// </summary>
        public string ServiceTradeDate { get; set; }
        /// <summary>
        /// 廠商交易時間
        /// </summary>
        public string ServiceTradeTime { get; set; }
        /// <summary>
        /// 交易後餘額
        /// </summary>
        public string AvailableAmount { get; set; }
        /// <summary>
        /// 單筆交易金額上限
        /// </summary>
        public string TradeAmountLimit { get; set; }
        /// <summary>
        /// 處理狀態 1:連線online
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 交易類別110扣款 交易取消220   退貨210
        /// </summary>
        public string TradeType { get; set; }
        /// <summary>
        /// 備註一
        /// </summary>
        public string Remark1 { get; set; }
        /// <summary>
        /// 備註二
        /// </summary>
        public string Remark2 { get; set; }
        /// <summary>
        /// 備註三
        /// </summary>
        public string Remark3 { get; set; }
        /// <summary>
        /// 保留欄位1
        /// </summary>
        public string OrderExtraInfo1 { get; set; }
        /// <summary>
        /// 保留欄位2
        /// </summary>
        public string OrderExtraInfo2 { get; set; }
        /// <summary>
        /// 保留欄位3
        /// </summary>
        public string OrderExtraInfo3 { get; set; }
        /// <summary>
        /// 錢包回傳額外資訊1
        /// </summary>
        public string ServiceInfo1 { get; set; }
        /// <summary>
        /// 錢包回傳額外資訊2
        /// </summary>
        public string ServiceInfo2 { get; set; }
        /// <summary>
        /// 錢包回傳額外資訊3
        /// </summary>
        public string ServiceInfo3 { get; set; }
        /// <summary>
        /// 隨機產生的16位數字 
        /// </summary>
        public string Random { get; set; }
        /// <summary>
        /// CheckSum
        /// </summary>
        public string CheckSum { get; set; }
        public string RTNTradeNo { get; set; }
        public string RTNTradeDate { get; set; }
        public string RTNTradeTime { get; set; }

        public string TransmittalDate { get; set; }
        public string CheckCode { get; set; }
        public string OrgDebitServiceTradeNo { get; set; }
        /// <summary>
        /// 建議POS處理方式
        /// </summary>
        public string RtnPOSActionCode { get; set; }
        /// <summary>
        /// 建議POS處理方式說明
        /// </summary>
        public string RtnPOSActionCodeMsg { get; set; }

        /// <summary>
        /// 會員編號
        /// </summary>
        public string MemberId { get; set; }
        /// <summary>
        /// 扣款使用的信用卡
        /// </summary>
        public string CardToken { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 交易明細
        /// </summary>
        public List<TradeDetail> TradeDetail { get; set; }
        /// <summary>
        /// 重覆執行
        /// </summary>
        public string IsRepeatAction { get; set; }
        public int Platform { get; set; }
        /// <summary>
        /// 授權碼
        /// </summary>
        public string AuthIdResp { get; set; }
    }

    public class MerchantTradeData
    {
        /// <summary>
        /// 通路代號
        /// </summary>
        public string MerchantID { get; set; }
        /// <summary>
        /// 店號
        /// </summary>
        public string StoreID { get; set; }
        /// <summary>
        /// 店名
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 店家電話
        /// </summary>
        public string StoreTel { get; set; }
        /// <summary>
        /// 店家地址
        /// </summary>
        public string StoreAddress { get; set; }
        /// <summary>
        /// POS交易序號
        /// </summary>
        public string MerchantTradeNo { get; set; }
        /// <summary>
        /// POS交易日期
        /// </summary>
        public string MerchantTradeDate { get; set; }
        /// <summary>
        /// POS交易時間
        /// </summary>
        public string MerchantTradeTime { get; set; }
        /// <summary>
        /// POS機號
        /// </summary>
        public string TID { get; set; }
        /// <summary>
        /// 支付條碼
        /// </summary>
        public string BarCode { get; set; }
        /// <summary>
        /// 支付條碼2
        /// </summary>
        public string BarCode2 { get; set; }
        /// <summary>
        /// 支付條碼3
        /// </summary>
        public string BarCode3 { get; set; }
        /// <summary>
        /// 支付條碼4
        /// </summary>
        public string BarCode4 { get; set; }
        /// <summary>
        /// 支付條碼5
        /// </summary>
        public string BarCode5 { get; set; }
        /// <summary>
        /// 交易金額
        /// </summary>
        public string TradeAmount { get; set; }
        /// <summary>
        /// 扣款金流類型  04 信用卡
        /// </summary>
        public string PaymentType { get; set; }
        /// <summary>
        /// 交易處理狀態
        /// </summary>
        public string RtnCode { get; set; }
        /// <summary>
        /// 交易回覆訊息
        /// </summary>
        public string RtnMsg { get; set; }
        /// <summary>
        /// 廠商交易編號
        /// </summary>
        public string ServiceTradeNo { get; set; }
        /// <summary>
        /// 廠商交易日期
        /// </summary>
        public string ServiceTradeDate { get; set; }
        /// <summary>
        /// 廠商交易時間
        /// </summary>
        public string ServiceTradeTime { get; set; }
        /// <summary>
        /// 交易後餘額
        /// </summary>
        public string AvailableAmount { get; set; }
        /// <summary>
        /// 單筆交易金額上限
        /// </summary>
        public string TradeAmountLimit { get; set; }
        /// <summary>
        /// 處理狀態 1:連線online
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 交易類別110扣款 交易取消220   退貨210
        /// </summary>
        public string TradeType { get; set; }
        /// <summary>
        /// 備註一
        /// </summary>
        public string Remark1 { get; set; }
        /// <summary>
        /// 備註二
        /// </summary>
        public string Remark2 { get; set; }
        /// <summary>
        /// 備註三
        /// </summary>
        public string Remark3 { get; set; }
        /// <summary>
        /// 保留欄位1
        /// </summary>
        public string OrderExtraInfo1 { get; set; }
        /// <summary>
        /// 保留欄位2
        /// </summary>
        public string OrderExtraInfo2 { get; set; }
        /// <summary>
        /// 保留欄位3
        /// </summary>
        public string OrderExtraInfo3 { get; set; }
        /// <summary>
        /// 錢包回傳額外資訊1
        /// </summary>
        public string ServiceInfo1 { get; set; }
        /// <summary>
        /// 錢包回傳額外資訊2
        /// </summary>
        public string ServiceInfo2 { get; set; }
        /// <summary>
        /// 錢包回傳額外資訊3
        /// </summary>
        public string ServiceInfo3 { get; set; }
        /// <summary>
        /// 隨機產生的16位數字 
        /// </summary>
        public string Random { get; set; }
        /// <summary>
        /// CheckSum
        /// </summary>
        public string CheckSum { get; set; }
        public string RTNTradeNo { get; set; }
        public string RTNTradeDate { get; set; }
        public string RTNTradeTime { get; set; }

        public string TransmittalDate { get; set; }
        public string CheckCode { get; set; }
        public string OrgDebitServiceTradeNo { get; set; }
        /// <summary>
        /// 建議POS處理方式
        /// </summary>
        public string RtnPOSActionCode { get; set; }
        /// <summary>
        /// 建議POS處理方式說明
        /// </summary>
        public string RtnPOSActionCodeMsg { get; set; }

        /// <summary>
        /// 會員編號
        /// </summary>
        public string MemberId { get; set; }
        /// <summary>
        /// 扣款使用的信用卡
        /// </summary>
        public string CardToken { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 交易明細
        /// </summary>
        public List<TradeDetail> TradeDetail { get; set; }
        /// <summary>
        /// 重覆執行
        /// </summary>
        public string IsRepeatAction { get; set; }
    }

    public class TradeDetail
    {
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 商品數量
        /// </summary>
        public string Quantity { get; set; }
        /// <summary>
        /// 商品售價
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
    }


    public class TaishinForwardTradeECPayData
    {
        /// <summary>
        /// 台新Key
        /// </summary>
        public string BankKey { get; set; }
        /// <summary>
        /// 通路資料
        /// </summary>
        public TaishinForwardTradeData MerchantTrade { get; set; }
    }

    public class TaishinForwardTradeData
    {
        /// <summary>
        /// 通路代號
        /// </summary>
        public string MerchantID { get; set; }
        /// <summary>
        /// POS交易序號
        /// </summary>
        public string MerchantTradeNo { get; set; }
        /// <summary>
        /// POS交易日期
        /// </summary>
        public string MerchantTradeDate { get; set; }
        /// <summary>
        /// POS交易時間
        /// </summary>
        public string MerchantTradeTime { get; set; }
        /// <summary>
        /// 訂單編號(墨攻票券)
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 支付條碼
        /// </summary>
        public string BarCode { get; set; }
        /// <summary>
        /// 支付條碼2
        /// </summary>
        public string BarCode2 { get; set; }
        /// <summary>
        /// 支付條碼3
        /// </summary>
        public string BarCode3 { get; set; }
        /// <summary>
        /// 支付條碼4
        /// </summary>
        public string BarCode4 { get; set; }
        /// <summary>
        /// 支付條碼5
        /// </summary>
        public string BarCode5 { get; set; }
        /// <summary>
        /// 1:連線 Online 0:Offline
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 交易金額
        /// </summary>
        public string TradeAmount { get; set; }
        /// <summary>
        /// 扣款金流類型  04 信用卡
        /// </summary>
        public string PaymentType { get; set; }
        /// <summary>
        /// 交易類別110扣款 交易取消220   退貨210
        /// </summary>
        public string TradeType { get; set; }
        /// <summary>
        /// 廠商交易編號
        /// </summary>
        public string ServiceTradeNo { get; set; }
        /// <summary>
        /// 交易日期
        /// </summary>
        public string ServiceTradeDate { get; set; }
        /// <summary>
        /// 交易時間
        /// </summary>
        public string ServiceTradeTime { get; set; }
        /// <summary>
        /// 退貨交易序號【退款用】
        /// </summary>
        public string RTNTradeNo { get; set; }
        /// <summary>
        /// 退貨交易日期【退款用】
        /// </summary>
        public string RTNTradeDate { get; set; }
        /// <summary>
        /// 退貨交易時間【退款用】
        /// </summary>
        public string RTNTradeTime { get; set; }
        /// <summary>
        /// 交易成功回傳000
        /// </summary>
        public string RtnCode { get; set; }
        /// <summary>
        /// 交易回覆訊息，成功
        /// </summary>
        public string RtnMsg { get; set; }
        /// <summary>
        /// 建議昕力處理方式
        /// </summary>
        public string RtnPOSActionCode { get; set; }
        /// <summary>
        /// 建議昕力處理方式說明
        /// </summary>
        public string RtnPOSActionCodeMsg { get; set; }
        /// <summary>
        /// 錢包回傳額外資訊1
        /// </summary>
        public string ServiceInfo1 { get; set; }
        /// <summary>
        /// 錢包回傳額外資訊2
        /// </summary>
        public string ServiceInfo2 { get; set; }
        /// <summary>
        /// 錢包回傳額外資訊3
        /// </summary>
        public string ServiceInfo3 { get; set; }
        /// <summary>
        /// 備註一
        /// </summary>
        public string Remark1 { get; set; }
        /// <summary>
        /// 備註二
        /// </summary>
        public string Remark2 { get; set; }
        /// <summary>
        /// 備註三
        /// </summary>
        public string Remark3 { get; set; }
        /// <summary>
        /// 授權碼
        /// </summary>
        public string AuthIdResp { get; set; }
        /// <summary>
        /// 信用卡有效截止日
        /// </summary>
        public string CardExpiredDate { get; set; }
    }


    public class NavData
    {
        public string Path { get; set; }
        public List<string> Funcs { get; set; }
    }

    #region POS搜尋回傳資料
    public class WalletPaymentCardPyment
    {
        public string MerchantID { get; set; }
        public string MerchantTradeNo { get; set; }
        public string ServiceTradeNo { get; set; }
        public string StoreID { get; set; }
        public string TID { get; set; }
        public string Random { get; set; }
        public string CheckSum { get; set; }
    }
    #endregion
}

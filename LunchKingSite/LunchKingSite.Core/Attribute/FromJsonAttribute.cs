﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.Core.Component
{
    
    /// <summary>
    /// 解析傳進Controller中的model，只要屬性值為json字串將自動被轉為物件
    /// </summary>
    public class FromJsonAttribute : CustomModelBinderAttribute
    {

        private readonly static JsonSerializer _serializer = new JsonSerializer();

        public override IModelBinder GetBinder()
        {
            return new JsonModelBinder();
        }

        private class JsonModelBinder : IModelBinder
        {
       
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {

                var obj = Activator.CreateInstance(bindingContext.ModelType);
                if (obj == null)
                {
                    return null;
                }

                foreach (var key in controllerContext.HttpContext.Request.Form.AllKeys)
                {
                    var value = controllerContext.HttpContext.Request.Form[key];
                    PropertyInfo property = obj.GetType().GetProperty(key);

                    //try pasre json
                    try
                    {
                        if (value != null) property.SetValue(obj, _serializer.Deserialize(value, property.PropertyType));
                    }
                    catch (Exception)
                    {
                        property.SetValue(obj, value);
                    }
                }
                return obj;
            }
        }
    }
}

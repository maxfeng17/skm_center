﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class SystemCodeAttribute : Attribute
    {
        public SystemCodeAttribute(string groupName , int key)
        {
            Key = key;
            GroupName = groupName;
        }

        public int Key { get; set; }

        public string GroupName { get; set; }
    }
}

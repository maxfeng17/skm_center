﻿using System;

namespace LunchKingSite.Core.Component
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class AssemblyDatabaseSchemaAttribute : Attribute
    {
        private string _schemaVersion;
        public string SchemaVersion
        {
            get { return _schemaVersion; }
        }

        public AssemblyDatabaseSchemaAttribute(string schVer)
        {
            _schemaVersion = schVer;
        }
    }

    [AttributeUsage(AttributeTargets.Assembly)]
    public class AssemblyBranchDatabaseSchemaAttribute : Attribute
    {
        private int _schemaVersion;
        public int SchemaVersion
        {
            get { return _schemaVersion; }
        }

        public AssemblyBranchDatabaseSchemaAttribute(int schVer)
        {
            _schemaVersion = schVer;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class MapToClassAttribute : System.Attribute
    {
        public Type MappedClass { get; private set; }

        public MapToClassAttribute(Type classType)
        {
            MappedClass = classType;
        }
    }
}

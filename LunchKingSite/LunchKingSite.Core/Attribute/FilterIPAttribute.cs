﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LunchKingSite.Core.Component
{
    /// <summary>
    /// 
    /// </summary>
    public class FilterIPAttribute: AuthorizeAttribute
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private bool isAuthorize { get; set; }
        public string AllowedIps { get; set; }

        public FilterIPAttribute()
        {
            AllowedIps = config.AlibabaAPIWhiteIps;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("actionContext");

            string userIpAddress = httpContext.Request.UserHostAddress;
            
            try
            {
                bool ipAllowed = false;
                if (!string.IsNullOrEmpty(AllowedIps))
                {
                    ipAllowed = CheckAllowedIPs(userIpAddress);
                }

                // Only allowed if allowed and not denied
                bool finallyAllowed = ipAllowed;

                return finallyAllowed;
            }
            catch
            {
                throw new HttpException(404, "");
                // Log the exception, probably something wrong with the configuration
            }
        }

        private bool CheckAllowedIPs(string userIpAddress)
        {
            this.isAuthorize = false;
            var splitSingleIPs = AllowedIps.Split(',');

            foreach (string ip in splitSingleIPs)
            {
                if (userIpAddress== ip)
                {
                    this.isAuthorize = true;
                }

            }
            return this.isAuthorize;
        }



    }
}

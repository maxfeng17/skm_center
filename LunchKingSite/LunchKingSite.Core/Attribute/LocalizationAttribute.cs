using System;

namespace LunchKingSite.Core.Component
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class LocalizationAttribute : Attribute
    {
        private string _key;
        
        public LocalizationAttribute(string key)
        {
            _key = key;
        }

        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }
    }
}

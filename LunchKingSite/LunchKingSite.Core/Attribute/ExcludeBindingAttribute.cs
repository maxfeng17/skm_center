﻿using System;

namespace LunchKingSite.Core.Component
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ExcludeBindingAttribute : Attribute
    {
        public ExcludeBindingAttribute() {}
    }
}

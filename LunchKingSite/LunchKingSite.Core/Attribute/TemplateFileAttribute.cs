﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TemplateFileAttribute : Attribute
    {
        public string FileName { get; private set; }

        public TemplateFileAttribute(string fileName)
        {
            FileName = fileName;
        }
    }
}

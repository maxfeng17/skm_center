﻿using System;

namespace LunchKingSite.Core.Component
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class XmlElementNameAttribute : Attribute
    {
        public XmlElementNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}

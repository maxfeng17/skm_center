﻿using System;

namespace LunchKingSite.Core.Component
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false)]
    public class ConfigParameterAttribute : Attribute
    {
        public string Key { get; set; }

        public object DefaultValue { get; set; }

        public string Description { get; set; }

        public ConfigTypes ConfigType { get; set; }

        public ConfigCategories ConfigCategory { get; set; }

        public enum ConfigCategories
        {
            None,
            System,
            Payment,
            Email,
            Ppon,
            PiinLife,
            Lunchking,
            EDM,
            Discount,
            Mobile,
            APP,
            PayEasy,
            ThirdParty,
            Tmall,
            WeChat,
            PCP,
            ACH,
            LionTravel,
            TaishinBank,
            Skm,
            AutoTest,
            /// <summary>
            /// 超取設定
            /// </summary>
            Isp,
            /// <summary>
            /// 行銷
            /// </summary>
            Marketing,
            Entrepot
        }

        public enum ConfigTypes
        {
            normal = 0,
            option = 1,
            mulit = 2
        }

        public ConfigParameterAttribute(string key, object defVal, string desc = "", ConfigCategories category = ConfigCategories.None, ConfigTypes type = ConfigTypes.normal)
        {
            Key = key;
            DefaultValue = defVal;
            Description = desc;
            ConfigType = type;
            ConfigCategory = category;
        }
    }
}
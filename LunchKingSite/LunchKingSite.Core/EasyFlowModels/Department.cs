using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.EasyFlow.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the Department class.
    /// </summary>
    [Serializable]
    public partial class DepartmentCollection : ActiveList<Department, DepartmentCollection>
    {
        public DepartmentCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DepartmentCollection</returns>
        public DepartmentCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Department o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the Department table.
    /// </summary>
    [Serializable]
    public partial class Department : ActiveRecord<Department>, IActiveRecord
    {
        #region .ctors and Default Settings

        public Department()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public Department(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        public Department(object keyID)
        {
            SetSQLProps();
            InitSetDefaults();
            LoadByKey(keyID);
        }

        public Department(string columnName, object columnValue)
        {
            SetSQLProps();
            InitSetDefaults();
            LoadByParam(columnName, columnValue);
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("Department", TableType.Table, DataService.GetInstance("EasyFlowDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarDepartmentId = new TableSchema.TableColumn(schema);
                colvarDepartmentId.ColumnName = "DepartmentId";
                colvarDepartmentId.DataType = DbType.Guid;
                colvarDepartmentId.MaxLength = 0;
                colvarDepartmentId.AutoIncrement = false;
                colvarDepartmentId.IsNullable = false;
                colvarDepartmentId.IsPrimaryKey = true;
                colvarDepartmentId.IsForeignKey = false;
                colvarDepartmentId.IsReadOnly = false;
                colvarDepartmentId.DefaultSetting = @"";
                colvarDepartmentId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDepartmentId);

                TableSchema.TableColumn colvarShortName = new TableSchema.TableColumn(schema);
                colvarShortName.ColumnName = "ShortName";
                colvarShortName.DataType = DbType.String;
                colvarShortName.MaxLength = 200;
                colvarShortName.AutoIncrement = false;
                colvarShortName.IsNullable = true;
                colvarShortName.IsPrimaryKey = false;
                colvarShortName.IsForeignKey = false;
                colvarShortName.IsReadOnly = false;
                colvarShortName.DefaultSetting = @"";
                colvarShortName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShortName);

                TableSchema.TableColumn colvarDescribe = new TableSchema.TableColumn(schema);
                colvarDescribe.ColumnName = "Describe";
                colvarDescribe.DataType = DbType.String;
                colvarDescribe.MaxLength = 1073741823;
                colvarDescribe.AutoIncrement = false;
                colvarDescribe.IsNullable = true;
                colvarDescribe.IsPrimaryKey = false;
                colvarDescribe.IsForeignKey = false;
                colvarDescribe.IsReadOnly = false;
                colvarDescribe.DefaultSetting = @"";
                colvarDescribe.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDescribe);

                TableSchema.TableColumn colvarRepealRemark = new TableSchema.TableColumn(schema);
                colvarRepealRemark.ColumnName = "RepealRemark";
                colvarRepealRemark.DataType = DbType.String;
                colvarRepealRemark.MaxLength = 200;
                colvarRepealRemark.AutoIncrement = false;
                colvarRepealRemark.IsNullable = true;
                colvarRepealRemark.IsPrimaryKey = false;
                colvarRepealRemark.IsForeignKey = false;
                colvarRepealRemark.IsReadOnly = false;
                colvarRepealRemark.DefaultSetting = @"";
                colvarRepealRemark.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRepealRemark);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "Type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = true;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                colvarType.DefaultSetting = @"";
                colvarType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarCorporationId = new TableSchema.TableColumn(schema);
                colvarCorporationId.ColumnName = "CorporationId";
                colvarCorporationId.DataType = DbType.Guid;
                colvarCorporationId.MaxLength = 0;
                colvarCorporationId.AutoIncrement = false;
                colvarCorporationId.IsNullable = true;
                colvarCorporationId.IsPrimaryKey = false;
                colvarCorporationId.IsForeignKey = false;
                colvarCorporationId.IsReadOnly = false;
                colvarCorporationId.DefaultSetting = @"";
                colvarCorporationId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCorporationId);

                TableSchema.TableColumn colvarParentId = new TableSchema.TableColumn(schema);
                colvarParentId.ColumnName = "ParentId";
                colvarParentId.DataType = DbType.String;
                colvarParentId.MaxLength = 200;
                colvarParentId.AutoIncrement = false;
                colvarParentId.IsNullable = true;
                colvarParentId.IsPrimaryKey = false;
                colvarParentId.IsForeignKey = false;
                colvarParentId.IsReadOnly = false;
                colvarParentId.DefaultSetting = @"";
                colvarParentId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarParentId);

                TableSchema.TableColumn colvarCostCenterId = new TableSchema.TableColumn(schema);
                colvarCostCenterId.ColumnName = "CostCenterId";
                colvarCostCenterId.DataType = DbType.Guid;
                colvarCostCenterId.MaxLength = 0;
                colvarCostCenterId.AutoIncrement = false;
                colvarCostCenterId.IsNullable = true;
                colvarCostCenterId.IsPrimaryKey = false;
                colvarCostCenterId.IsForeignKey = false;
                colvarCostCenterId.IsReadOnly = false;
                colvarCostCenterId.DefaultSetting = @"";
                colvarCostCenterId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCostCenterId);

                TableSchema.TableColumn colvarOrderNumber = new TableSchema.TableColumn(schema);
                colvarOrderNumber.ColumnName = "OrderNumber";
                colvarOrderNumber.DataType = DbType.Int32;
                colvarOrderNumber.MaxLength = 0;
                colvarOrderNumber.AutoIncrement = false;
                colvarOrderNumber.IsNullable = true;
                colvarOrderNumber.IsPrimaryKey = false;
                colvarOrderNumber.IsForeignKey = false;
                colvarOrderNumber.IsReadOnly = false;
                colvarOrderNumber.DefaultSetting = @"";
                colvarOrderNumber.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderNumber);

                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "Code";
                colvarCode.DataType = DbType.String;
                colvarCode.MaxLength = 200;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                colvarCode.DefaultSetting = @"";
                colvarCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCode);

                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "Name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 200;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                colvarName.DefaultSetting = @"";
                colvarName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarName);

                TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
                colvarFlag.ColumnName = "Flag";
                colvarFlag.DataType = DbType.Boolean;
                colvarFlag.MaxLength = 0;
                colvarFlag.AutoIncrement = false;
                colvarFlag.IsNullable = true;
                colvarFlag.IsPrimaryKey = false;
                colvarFlag.IsForeignKey = false;
                colvarFlag.IsReadOnly = false;
                colvarFlag.DefaultSetting = @"";
                colvarFlag.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFlag);

                TableSchema.TableColumn colvarIsERP = new TableSchema.TableColumn(schema);
                colvarIsERP.ColumnName = "IsERP";
                colvarIsERP.DataType = DbType.Boolean;
                colvarIsERP.MaxLength = 0;
                colvarIsERP.AutoIncrement = false;
                colvarIsERP.IsNullable = true;
                colvarIsERP.IsPrimaryKey = false;
                colvarIsERP.IsForeignKey = false;
                colvarIsERP.IsReadOnly = false;
                colvarIsERP.DefaultSetting = @"";
                colvarIsERP.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsERP);

                TableSchema.TableColumn colvarCreateDate = new TableSchema.TableColumn(schema);
                colvarCreateDate.ColumnName = "CreateDate";
                colvarCreateDate.DataType = DbType.DateTime;
                colvarCreateDate.MaxLength = 0;
                colvarCreateDate.AutoIncrement = false;
                colvarCreateDate.IsNullable = true;
                colvarCreateDate.IsPrimaryKey = false;
                colvarCreateDate.IsForeignKey = false;
                colvarCreateDate.IsReadOnly = false;
                colvarCreateDate.DefaultSetting = @"";
                colvarCreateDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateDate);

                TableSchema.TableColumn colvarLastModifiedDate = new TableSchema.TableColumn(schema);
                colvarLastModifiedDate.ColumnName = "LastModifiedDate";
                colvarLastModifiedDate.DataType = DbType.DateTime;
                colvarLastModifiedDate.MaxLength = 0;
                colvarLastModifiedDate.AutoIncrement = false;
                colvarLastModifiedDate.IsNullable = true;
                colvarLastModifiedDate.IsPrimaryKey = false;
                colvarLastModifiedDate.IsForeignKey = false;
                colvarLastModifiedDate.IsReadOnly = false;
                colvarLastModifiedDate.DefaultSetting = @"";
                colvarLastModifiedDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastModifiedDate);

                TableSchema.TableColumn colvarCreateBy = new TableSchema.TableColumn(schema);
                colvarCreateBy.ColumnName = "CreateBy";
                colvarCreateBy.DataType = DbType.Guid;
                colvarCreateBy.MaxLength = 0;
                colvarCreateBy.AutoIncrement = false;
                colvarCreateBy.IsNullable = true;
                colvarCreateBy.IsPrimaryKey = false;
                colvarCreateBy.IsForeignKey = false;
                colvarCreateBy.IsReadOnly = false;
                colvarCreateBy.DefaultSetting = @"";
                colvarCreateBy.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateBy);

                TableSchema.TableColumn colvarLastModifiedBy = new TableSchema.TableColumn(schema);
                colvarLastModifiedBy.ColumnName = "LastModifiedBy";
                colvarLastModifiedBy.DataType = DbType.Guid;
                colvarLastModifiedBy.MaxLength = 0;
                colvarLastModifiedBy.AutoIncrement = false;
                colvarLastModifiedBy.IsNullable = true;
                colvarLastModifiedBy.IsPrimaryKey = false;
                colvarLastModifiedBy.IsForeignKey = false;
                colvarLastModifiedBy.IsReadOnly = false;
                colvarLastModifiedBy.DefaultSetting = @"";
                colvarLastModifiedBy.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastModifiedBy);

                TableSchema.TableColumn colvarAssignReason = new TableSchema.TableColumn(schema);
                colvarAssignReason.ColumnName = "AssignReason";
                colvarAssignReason.DataType = DbType.String;
                colvarAssignReason.MaxLength = 200;
                colvarAssignReason.AutoIncrement = false;
                colvarAssignReason.IsNullable = true;
                colvarAssignReason.IsPrimaryKey = false;
                colvarAssignReason.IsForeignKey = false;
                colvarAssignReason.IsReadOnly = false;
                colvarAssignReason.DefaultSetting = @"";
                colvarAssignReason.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAssignReason);

                TableSchema.TableColumn colvarOwnerId = new TableSchema.TableColumn(schema);
                colvarOwnerId.ColumnName = "OwnerId";
                colvarOwnerId.DataType = DbType.String;
                colvarOwnerId.MaxLength = 200;
                colvarOwnerId.AutoIncrement = false;
                colvarOwnerId.IsNullable = true;
                colvarOwnerId.IsPrimaryKey = false;
                colvarOwnerId.IsForeignKey = false;
                colvarOwnerId.IsReadOnly = false;
                colvarOwnerId.DefaultSetting = @"";
                colvarOwnerId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOwnerId);

                TableSchema.TableColumn colvarAliasX = new TableSchema.TableColumn(schema);
                colvarAliasX.ColumnName = "Alias";
                colvarAliasX.DataType = DbType.String;
                colvarAliasX.MaxLength = 200;
                colvarAliasX.AutoIncrement = false;
                colvarAliasX.IsNullable = true;
                colvarAliasX.IsPrimaryKey = false;
                colvarAliasX.IsForeignKey = false;
                colvarAliasX.IsReadOnly = false;
                colvarAliasX.DefaultSetting = @"";
                colvarAliasX.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAliasX);

                TableSchema.TableColumn colvarOrgType = new TableSchema.TableColumn(schema);
                colvarOrgType.ColumnName = "OrgType";
                colvarOrgType.DataType = DbType.String;
                colvarOrgType.MaxLength = 200;
                colvarOrgType.AutoIncrement = false;
                colvarOrgType.IsNullable = true;
                colvarOrgType.IsPrimaryKey = false;
                colvarOrgType.IsForeignKey = false;
                colvarOrgType.IsReadOnly = false;
                colvarOrgType.DefaultSetting = @"";
                colvarOrgType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrgType);

                TableSchema.TableColumn colvarIsShowInOrg = new TableSchema.TableColumn(schema);
                colvarIsShowInOrg.ColumnName = "IsShowInOrg";
                colvarIsShowInOrg.DataType = DbType.Boolean;
                colvarIsShowInOrg.MaxLength = 0;
                colvarIsShowInOrg.AutoIncrement = false;
                colvarIsShowInOrg.IsNullable = true;
                colvarIsShowInOrg.IsPrimaryKey = false;
                colvarIsShowInOrg.IsForeignKey = false;
                colvarIsShowInOrg.IsReadOnly = false;
                colvarIsShowInOrg.DefaultSetting = @"";
                colvarIsShowInOrg.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsShowInOrg);

                TableSchema.TableColumn colvarIsAccounting = new TableSchema.TableColumn(schema);
                colvarIsAccounting.ColumnName = "IsAccounting";
                colvarIsAccounting.DataType = DbType.Boolean;
                colvarIsAccounting.MaxLength = 0;
                colvarIsAccounting.AutoIncrement = false;
                colvarIsAccounting.IsNullable = true;
                colvarIsAccounting.IsPrimaryKey = false;
                colvarIsAccounting.IsForeignKey = false;
                colvarIsAccounting.IsReadOnly = false;
                colvarIsAccounting.DefaultSetting = @"";
                colvarIsAccounting.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsAccounting);

                TableSchema.TableColumn colvarCostTypeId = new TableSchema.TableColumn(schema);
                colvarCostTypeId.ColumnName = "CostTypeId";
                colvarCostTypeId.DataType = DbType.String;
                colvarCostTypeId.MaxLength = 200;
                colvarCostTypeId.AutoIncrement = false;
                colvarCostTypeId.IsNullable = true;
                colvarCostTypeId.IsPrimaryKey = false;
                colvarCostTypeId.IsForeignKey = false;
                colvarCostTypeId.IsReadOnly = false;
                colvarCostTypeId.DefaultSetting = @"";
                colvarCostTypeId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCostTypeId);

                TableSchema.TableColumn colvarDeptLevel = new TableSchema.TableColumn(schema);
                colvarDeptLevel.ColumnName = "DeptLevel";
                colvarDeptLevel.DataType = DbType.Int32;
                colvarDeptLevel.MaxLength = 0;
                colvarDeptLevel.AutoIncrement = false;
                colvarDeptLevel.IsNullable = true;
                colvarDeptLevel.IsPrimaryKey = false;
                colvarDeptLevel.IsForeignKey = false;
                colvarDeptLevel.IsReadOnly = false;
                colvarDeptLevel.DefaultSetting = @"";
                colvarDeptLevel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDeptLevel);

                TableSchema.TableColumn colvarDirectDeptId = new TableSchema.TableColumn(schema);
                colvarDirectDeptId.ColumnName = "DirectDeptId";
                colvarDirectDeptId.DataType = DbType.Guid;
                colvarDirectDeptId.MaxLength = 0;
                colvarDirectDeptId.AutoIncrement = false;
                colvarDirectDeptId.IsNullable = true;
                colvarDirectDeptId.IsPrimaryKey = false;
                colvarDirectDeptId.IsForeignKey = false;
                colvarDirectDeptId.IsReadOnly = false;
                colvarDirectDeptId.DefaultSetting = @"";
                colvarDirectDeptId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDirectDeptId);

                TableSchema.TableColumn colvarWorkingCenterId = new TableSchema.TableColumn(schema);
                colvarWorkingCenterId.ColumnName = "WorkingCenterId";
                colvarWorkingCenterId.DataType = DbType.Guid;
                colvarWorkingCenterId.MaxLength = 0;
                colvarWorkingCenterId.AutoIncrement = false;
                colvarWorkingCenterId.IsNullable = true;
                colvarWorkingCenterId.IsPrimaryKey = false;
                colvarWorkingCenterId.IsForeignKey = false;
                colvarWorkingCenterId.IsReadOnly = false;
                colvarWorkingCenterId.DefaultSetting = @"";
                colvarWorkingCenterId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarWorkingCenterId);

                TableSchema.TableColumn colvarPrincipal = new TableSchema.TableColumn(schema);
                colvarPrincipal.ColumnName = "Principal";
                colvarPrincipal.DataType = DbType.Guid;
                colvarPrincipal.MaxLength = 0;
                colvarPrincipal.AutoIncrement = false;
                colvarPrincipal.IsNullable = true;
                colvarPrincipal.IsPrimaryKey = false;
                colvarPrincipal.IsForeignKey = false;
                colvarPrincipal.IsReadOnly = false;
                colvarPrincipal.DefaultSetting = @"";
                colvarPrincipal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPrincipal);

                TableSchema.TableColumn colvarLevelCode = new TableSchema.TableColumn(schema);
                colvarLevelCode.ColumnName = "LevelCode";
                colvarLevelCode.DataType = DbType.Int32;
                colvarLevelCode.MaxLength = 0;
                colvarLevelCode.AutoIncrement = false;
                colvarLevelCode.IsNullable = true;
                colvarLevelCode.IsPrimaryKey = false;
                colvarLevelCode.IsForeignKey = false;
                colvarLevelCode.IsReadOnly = false;
                colvarLevelCode.DefaultSetting = @"";
                colvarLevelCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLevelCode);

                TableSchema.TableColumn colvarFloorCode = new TableSchema.TableColumn(schema);
                colvarFloorCode.ColumnName = "FloorCode";
                colvarFloorCode.DataType = DbType.String;
                colvarFloorCode.MaxLength = 200;
                colvarFloorCode.AutoIncrement = false;
                colvarFloorCode.IsNullable = true;
                colvarFloorCode.IsPrimaryKey = false;
                colvarFloorCode.IsForeignKey = false;
                colvarFloorCode.IsReadOnly = false;
                colvarFloorCode.DefaultSetting = @"";
                colvarFloorCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFloorCode);

                TableSchema.TableColumn colvarPrincipalJobId = new TableSchema.TableColumn(schema);
                colvarPrincipalJobId.ColumnName = "PrincipalJobId";
                colvarPrincipalJobId.DataType = DbType.Guid;
                colvarPrincipalJobId.MaxLength = 0;
                colvarPrincipalJobId.AutoIncrement = false;
                colvarPrincipalJobId.IsNullable = true;
                colvarPrincipalJobId.IsPrimaryKey = false;
                colvarPrincipalJobId.IsForeignKey = false;
                colvarPrincipalJobId.IsReadOnly = false;
                colvarPrincipalJobId.DefaultSetting = @"";
                colvarPrincipalJobId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPrincipalJobId);

                TableSchema.TableColumn colvarDisplayLever = new TableSchema.TableColumn(schema);
                colvarDisplayLever.ColumnName = "DisplayLever";
                colvarDisplayLever.DataType = DbType.String;
                colvarDisplayLever.MaxLength = 200;
                colvarDisplayLever.AutoIncrement = false;
                colvarDisplayLever.IsNullable = true;
                colvarDisplayLever.IsPrimaryKey = false;
                colvarDisplayLever.IsForeignKey = false;
                colvarDisplayLever.IsReadOnly = false;
                colvarDisplayLever.DefaultSetting = @"";
                colvarDisplayLever.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDisplayLever);

                TableSchema.TableColumn colvarBeginEndDateBeginDate = new TableSchema.TableColumn(schema);
                colvarBeginEndDateBeginDate.ColumnName = "BeginEndDate_BeginDate";
                colvarBeginEndDateBeginDate.DataType = DbType.DateTime;
                colvarBeginEndDateBeginDate.MaxLength = 0;
                colvarBeginEndDateBeginDate.AutoIncrement = false;
                colvarBeginEndDateBeginDate.IsNullable = true;
                colvarBeginEndDateBeginDate.IsPrimaryKey = false;
                colvarBeginEndDateBeginDate.IsForeignKey = false;
                colvarBeginEndDateBeginDate.IsReadOnly = false;
                colvarBeginEndDateBeginDate.DefaultSetting = @"";
                colvarBeginEndDateBeginDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBeginEndDateBeginDate);

                TableSchema.TableColumn colvarBeginEndDateEndDate = new TableSchema.TableColumn(schema);
                colvarBeginEndDateEndDate.ColumnName = "BeginEndDate_EndDate";
                colvarBeginEndDateEndDate.DataType = DbType.DateTime;
                colvarBeginEndDateEndDate.MaxLength = 0;
                colvarBeginEndDateEndDate.AutoIncrement = false;
                colvarBeginEndDateEndDate.IsNullable = true;
                colvarBeginEndDateEndDate.IsPrimaryKey = false;
                colvarBeginEndDateEndDate.IsForeignKey = false;
                colvarBeginEndDateEndDate.IsReadOnly = false;
                colvarBeginEndDateEndDate.DefaultSetting = @"";
                colvarBeginEndDateEndDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBeginEndDateEndDate);

                TableSchema.TableColumn colvarIsNullBeginEndDate = new TableSchema.TableColumn(schema);
                colvarIsNullBeginEndDate.ColumnName = "IsNull_BeginEndDate";
                colvarIsNullBeginEndDate.DataType = DbType.Boolean;
                colvarIsNullBeginEndDate.MaxLength = 0;
                colvarIsNullBeginEndDate.AutoIncrement = false;
                colvarIsNullBeginEndDate.IsNullable = true;
                colvarIsNullBeginEndDate.IsPrimaryKey = false;
                colvarIsNullBeginEndDate.IsForeignKey = false;
                colvarIsNullBeginEndDate.IsReadOnly = false;
                colvarIsNullBeginEndDate.DefaultSetting = @"";
                colvarIsNullBeginEndDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsNullBeginEndDate);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["EasyFlowDB"].AddSchema("Department", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("DepartmentId")]
        [Bindable(true)]
        public Guid DepartmentId
        {
            get { return GetColumnValue<Guid>(Columns.DepartmentId); }
            set { SetColumnValue(Columns.DepartmentId, value); }
        }

        [XmlAttribute("ShortName")]
        [Bindable(true)]
        public string ShortName
        {
            get { return GetColumnValue<string>(Columns.ShortName); }
            set { SetColumnValue(Columns.ShortName, value); }
        }

        [XmlAttribute("Describe")]
        [Bindable(true)]
        public string Describe
        {
            get { return GetColumnValue<string>(Columns.Describe); }
            set { SetColumnValue(Columns.Describe, value); }
        }

        [XmlAttribute("RepealRemark")]
        [Bindable(true)]
        public string RepealRemark
        {
            get { return GetColumnValue<string>(Columns.RepealRemark); }
            set { SetColumnValue(Columns.RepealRemark, value); }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int? Type
        {
            get { return GetColumnValue<int?>(Columns.Type); }
            set { SetColumnValue(Columns.Type, value); }
        }

        [XmlAttribute("CorporationId")]
        [Bindable(true)]
        public Guid? CorporationId
        {
            get { return GetColumnValue<Guid?>(Columns.CorporationId); }
            set { SetColumnValue(Columns.CorporationId, value); }
        }

        [XmlAttribute("ParentId")]
        [Bindable(true)]
        public string ParentId
        {
            get { return GetColumnValue<string>(Columns.ParentId); }
            set { SetColumnValue(Columns.ParentId, value); }
        }

        [XmlAttribute("CostCenterId")]
        [Bindable(true)]
        public Guid? CostCenterId
        {
            get { return GetColumnValue<Guid?>(Columns.CostCenterId); }
            set { SetColumnValue(Columns.CostCenterId, value); }
        }

        [XmlAttribute("OrderNumber")]
        [Bindable(true)]
        public int? OrderNumber
        {
            get { return GetColumnValue<int?>(Columns.OrderNumber); }
            set { SetColumnValue(Columns.OrderNumber, value); }
        }

        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code
        {
            get { return GetColumnValue<string>(Columns.Code); }
            set { SetColumnValue(Columns.Code, value); }
        }

        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name
        {
            get { return GetColumnValue<string>(Columns.Name); }
            set { SetColumnValue(Columns.Name, value); }
        }

        [XmlAttribute("Flag")]
        [Bindable(true)]
        public bool? Flag
        {
            get { return GetColumnValue<bool?>(Columns.Flag); }
            set { SetColumnValue(Columns.Flag, value); }
        }

        [XmlAttribute("IsERP")]
        [Bindable(true)]
        public bool? IsERP
        {
            get { return GetColumnValue<bool?>(Columns.IsERP); }
            set { SetColumnValue(Columns.IsERP, value); }
        }

        [XmlAttribute("CreateDate")]
        [Bindable(true)]
        public DateTime? CreateDate
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateDate); }
            set { SetColumnValue(Columns.CreateDate, value); }
        }

        [XmlAttribute("LastModifiedDate")]
        [Bindable(true)]
        public DateTime? LastModifiedDate
        {
            get { return GetColumnValue<DateTime?>(Columns.LastModifiedDate); }
            set { SetColumnValue(Columns.LastModifiedDate, value); }
        }

        [XmlAttribute("CreateBy")]
        [Bindable(true)]
        public Guid? CreateBy
        {
            get { return GetColumnValue<Guid?>(Columns.CreateBy); }
            set { SetColumnValue(Columns.CreateBy, value); }
        }

        [XmlAttribute("LastModifiedBy")]
        [Bindable(true)]
        public Guid? LastModifiedBy
        {
            get { return GetColumnValue<Guid?>(Columns.LastModifiedBy); }
            set { SetColumnValue(Columns.LastModifiedBy, value); }
        }

        [XmlAttribute("AssignReason")]
        [Bindable(true)]
        public string AssignReason
        {
            get { return GetColumnValue<string>(Columns.AssignReason); }
            set { SetColumnValue(Columns.AssignReason, value); }
        }

        [XmlAttribute("OwnerId")]
        [Bindable(true)]
        public string OwnerId
        {
            get { return GetColumnValue<string>(Columns.OwnerId); }
            set { SetColumnValue(Columns.OwnerId, value); }
        }

        [XmlAttribute("AliasX")]
        [Bindable(true)]
        public string AliasX
        {
            get { return GetColumnValue<string>(Columns.AliasX); }
            set { SetColumnValue(Columns.AliasX, value); }
        }

        [XmlAttribute("OrgType")]
        [Bindable(true)]
        public string OrgType
        {
            get { return GetColumnValue<string>(Columns.OrgType); }
            set { SetColumnValue(Columns.OrgType, value); }
        }

        [XmlAttribute("IsShowInOrg")]
        [Bindable(true)]
        public bool? IsShowInOrg
        {
            get { return GetColumnValue<bool?>(Columns.IsShowInOrg); }
            set { SetColumnValue(Columns.IsShowInOrg, value); }
        }

        [XmlAttribute("IsAccounting")]
        [Bindable(true)]
        public bool? IsAccounting
        {
            get { return GetColumnValue<bool?>(Columns.IsAccounting); }
            set { SetColumnValue(Columns.IsAccounting, value); }
        }

        [XmlAttribute("CostTypeId")]
        [Bindable(true)]
        public string CostTypeId
        {
            get { return GetColumnValue<string>(Columns.CostTypeId); }
            set { SetColumnValue(Columns.CostTypeId, value); }
        }

        [XmlAttribute("DeptLevel")]
        [Bindable(true)]
        public int? DeptLevel
        {
            get { return GetColumnValue<int?>(Columns.DeptLevel); }
            set { SetColumnValue(Columns.DeptLevel, value); }
        }

        [XmlAttribute("DirectDeptId")]
        [Bindable(true)]
        public Guid? DirectDeptId
        {
            get { return GetColumnValue<Guid?>(Columns.DirectDeptId); }
            set { SetColumnValue(Columns.DirectDeptId, value); }
        }

        [XmlAttribute("WorkingCenterId")]
        [Bindable(true)]
        public Guid? WorkingCenterId
        {
            get { return GetColumnValue<Guid?>(Columns.WorkingCenterId); }
            set { SetColumnValue(Columns.WorkingCenterId, value); }
        }

        [XmlAttribute("Principal")]
        [Bindable(true)]
        public Guid? Principal
        {
            get { return GetColumnValue<Guid?>(Columns.Principal); }
            set { SetColumnValue(Columns.Principal, value); }
        }

        [XmlAttribute("LevelCode")]
        [Bindable(true)]
        public int? LevelCode
        {
            get { return GetColumnValue<int?>(Columns.LevelCode); }
            set { SetColumnValue(Columns.LevelCode, value); }
        }

        [XmlAttribute("FloorCode")]
        [Bindable(true)]
        public string FloorCode
        {
            get { return GetColumnValue<string>(Columns.FloorCode); }
            set { SetColumnValue(Columns.FloorCode, value); }
        }

        [XmlAttribute("PrincipalJobId")]
        [Bindable(true)]
        public Guid? PrincipalJobId
        {
            get { return GetColumnValue<Guid?>(Columns.PrincipalJobId); }
            set { SetColumnValue(Columns.PrincipalJobId, value); }
        }

        [XmlAttribute("DisplayLever")]
        [Bindable(true)]
        public string DisplayLever
        {
            get { return GetColumnValue<string>(Columns.DisplayLever); }
            set { SetColumnValue(Columns.DisplayLever, value); }
        }

        [XmlAttribute("BeginEndDateBeginDate")]
        [Bindable(true)]
        public DateTime? BeginEndDateBeginDate
        {
            get { return GetColumnValue<DateTime?>(Columns.BeginEndDateBeginDate); }
            set { SetColumnValue(Columns.BeginEndDateBeginDate, value); }
        }

        [XmlAttribute("BeginEndDateEndDate")]
        [Bindable(true)]
        public DateTime? BeginEndDateEndDate
        {
            get { return GetColumnValue<DateTime?>(Columns.BeginEndDateEndDate); }
            set { SetColumnValue(Columns.BeginEndDateEndDate, value); }
        }

        [XmlAttribute("IsNullBeginEndDate")]
        [Bindable(true)]
        public bool? IsNullBeginEndDate
        {
            get { return GetColumnValue<bool?>(Columns.IsNullBeginEndDate); }
            set { SetColumnValue(Columns.IsNullBeginEndDate, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)



        #region ObjectDataSource support


        /// <summary>
        /// Inserts a record, can be used with the Object Data Source
        /// </summary>
        public static void Insert(Guid varDepartmentId, string varShortName, string varDescribe, string varRepealRemark, int? varType, Guid? varCorporationId, string varParentId, Guid? varCostCenterId, int? varOrderNumber, string varCode, string varName, bool? varFlag, bool? varIsERP, DateTime? varCreateDate, DateTime? varLastModifiedDate, Guid? varCreateBy, Guid? varLastModifiedBy, string varAssignReason, string varOwnerId, string varAliasX, string varOrgType, bool? varIsShowInOrg, bool? varIsAccounting, string varCostTypeId, int? varDeptLevel, Guid? varDirectDeptId, Guid? varWorkingCenterId, Guid? varPrincipal, int? varLevelCode, string varFloorCode, Guid? varPrincipalJobId, string varDisplayLever, DateTime? varBeginEndDateBeginDate, DateTime? varBeginEndDateEndDate, bool? varIsNullBeginEndDate)
        {
            Department item = new Department();

            item.DepartmentId = varDepartmentId;

            item.ShortName = varShortName;

            item.Describe = varDescribe;

            item.RepealRemark = varRepealRemark;

            item.Type = varType;

            item.CorporationId = varCorporationId;

            item.ParentId = varParentId;

            item.CostCenterId = varCostCenterId;

            item.OrderNumber = varOrderNumber;

            item.Code = varCode;

            item.Name = varName;

            item.Flag = varFlag;

            item.IsERP = varIsERP;

            item.CreateDate = varCreateDate;

            item.LastModifiedDate = varLastModifiedDate;

            item.CreateBy = varCreateBy;

            item.LastModifiedBy = varLastModifiedBy;

            item.AssignReason = varAssignReason;

            item.OwnerId = varOwnerId;

            item.AliasX = varAliasX;

            item.OrgType = varOrgType;

            item.IsShowInOrg = varIsShowInOrg;

            item.IsAccounting = varIsAccounting;

            item.CostTypeId = varCostTypeId;

            item.DeptLevel = varDeptLevel;

            item.DirectDeptId = varDirectDeptId;

            item.WorkingCenterId = varWorkingCenterId;

            item.Principal = varPrincipal;

            item.LevelCode = varLevelCode;

            item.FloorCode = varFloorCode;

            item.PrincipalJobId = varPrincipalJobId;

            item.DisplayLever = varDisplayLever;

            item.BeginEndDateBeginDate = varBeginEndDateBeginDate;

            item.BeginEndDateEndDate = varBeginEndDateEndDate;

            item.IsNullBeginEndDate = varIsNullBeginEndDate;


            if (System.Web.HttpContext.Current != null)
                item.Save(System.Web.HttpContext.Current.User.Identity.Name);
            else
                item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
        }

        /// <summary>
        /// Updates a record, can be used with the Object Data Source
        /// </summary>
        public static void Update(Guid varDepartmentId, string varShortName, string varDescribe, string varRepealRemark, int? varType, Guid? varCorporationId, string varParentId, Guid? varCostCenterId, int? varOrderNumber, string varCode, string varName, bool? varFlag, bool? varIsERP, DateTime? varCreateDate, DateTime? varLastModifiedDate, Guid? varCreateBy, Guid? varLastModifiedBy, string varAssignReason, string varOwnerId, string varAliasX, string varOrgType, bool? varIsShowInOrg, bool? varIsAccounting, string varCostTypeId, int? varDeptLevel, Guid? varDirectDeptId, Guid? varWorkingCenterId, Guid? varPrincipal, int? varLevelCode, string varFloorCode, Guid? varPrincipalJobId, string varDisplayLever, DateTime? varBeginEndDateBeginDate, DateTime? varBeginEndDateEndDate, bool? varIsNullBeginEndDate)
        {
            Department item = new Department();

            item.DepartmentId = varDepartmentId;

            item.ShortName = varShortName;

            item.Describe = varDescribe;

            item.RepealRemark = varRepealRemark;

            item.Type = varType;

            item.CorporationId = varCorporationId;

            item.ParentId = varParentId;

            item.CostCenterId = varCostCenterId;

            item.OrderNumber = varOrderNumber;

            item.Code = varCode;

            item.Name = varName;

            item.Flag = varFlag;

            item.IsERP = varIsERP;

            item.CreateDate = varCreateDate;

            item.LastModifiedDate = varLastModifiedDate;

            item.CreateBy = varCreateBy;

            item.LastModifiedBy = varLastModifiedBy;

            item.AssignReason = varAssignReason;

            item.OwnerId = varOwnerId;

            item.AliasX = varAliasX;

            item.OrgType = varOrgType;

            item.IsShowInOrg = varIsShowInOrg;

            item.IsAccounting = varIsAccounting;

            item.CostTypeId = varCostTypeId;

            item.DeptLevel = varDeptLevel;

            item.DirectDeptId = varDirectDeptId;

            item.WorkingCenterId = varWorkingCenterId;

            item.Principal = varPrincipal;

            item.LevelCode = varLevelCode;

            item.FloorCode = varFloorCode;

            item.PrincipalJobId = varPrincipalJobId;

            item.DisplayLever = varDisplayLever;

            item.BeginEndDateBeginDate = varBeginEndDateBeginDate;

            item.BeginEndDateEndDate = varBeginEndDateEndDate;

            item.IsNullBeginEndDate = varIsNullBeginEndDate;

            item.IsNew = false;
            if (System.Web.HttpContext.Current != null)
                item.Save(System.Web.HttpContext.Current.User.Identity.Name);
            else
                item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
        }
        #endregion



        #region Typed Columns


        public static TableSchema.TableColumn DepartmentIdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ShortNameColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn DescribeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn RepealRemarkColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CorporationIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ParentIdColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CostCenterIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn OrderNumberColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn FlagColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn IsERPColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn CreateDateColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn LastModifiedDateColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn CreateByColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn LastModifiedByColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn AssignReasonColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn OwnerIdColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn AliasXColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn OrgTypeColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn IsShowInOrgColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn IsAccountingColumn
        {
            get { return Schema.Columns[22]; }
        }



        public static TableSchema.TableColumn CostTypeIdColumn
        {
            get { return Schema.Columns[23]; }
        }



        public static TableSchema.TableColumn DeptLevelColumn
        {
            get { return Schema.Columns[24]; }
        }



        public static TableSchema.TableColumn DirectDeptIdColumn
        {
            get { return Schema.Columns[25]; }
        }



        public static TableSchema.TableColumn WorkingCenterIdColumn
        {
            get { return Schema.Columns[26]; }
        }



        public static TableSchema.TableColumn PrincipalColumn
        {
            get { return Schema.Columns[27]; }
        }



        public static TableSchema.TableColumn LevelCodeColumn
        {
            get { return Schema.Columns[28]; }
        }



        public static TableSchema.TableColumn FloorCodeColumn
        {
            get { return Schema.Columns[29]; }
        }



        public static TableSchema.TableColumn PrincipalJobIdColumn
        {
            get { return Schema.Columns[30]; }
        }



        public static TableSchema.TableColumn DisplayLeverColumn
        {
            get { return Schema.Columns[31]; }
        }



        public static TableSchema.TableColumn BeginEndDateBeginDateColumn
        {
            get { return Schema.Columns[32]; }
        }



        public static TableSchema.TableColumn BeginEndDateEndDateColumn
        {
            get { return Schema.Columns[33]; }
        }



        public static TableSchema.TableColumn IsNullBeginEndDateColumn
        {
            get { return Schema.Columns[34]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string DepartmentId = @"DepartmentId";
            public static string ShortName = @"ShortName";
            public static string Describe = @"Describe";
            public static string RepealRemark = @"RepealRemark";
            public static string Type = @"Type";
            public static string CorporationId = @"CorporationId";
            public static string ParentId = @"ParentId";
            public static string CostCenterId = @"CostCenterId";
            public static string OrderNumber = @"OrderNumber";
            public static string Code = @"Code";
            public static string Name = @"Name";
            public static string Flag = @"Flag";
            public static string IsERP = @"IsERP";
            public static string CreateDate = @"CreateDate";
            public static string LastModifiedDate = @"LastModifiedDate";
            public static string CreateBy = @"CreateBy";
            public static string LastModifiedBy = @"LastModifiedBy";
            public static string AssignReason = @"AssignReason";
            public static string OwnerId = @"OwnerId";
            public static string AliasX = @"Alias";
            public static string OrgType = @"OrgType";
            public static string IsShowInOrg = @"IsShowInOrg";
            public static string IsAccounting = @"IsAccounting";
            public static string CostTypeId = @"CostTypeId";
            public static string DeptLevel = @"DeptLevel";
            public static string DirectDeptId = @"DirectDeptId";
            public static string WorkingCenterId = @"WorkingCenterId";
            public static string Principal = @"Principal";
            public static string LevelCode = @"LevelCode";
            public static string FloorCode = @"FloorCode";
            public static string PrincipalJobId = @"PrincipalJobId";
            public static string DisplayLever = @"DisplayLever";
            public static string BeginEndDateBeginDate = @"BeginEndDate_BeginDate";
            public static string BeginEndDateEndDate = @"BeginEndDate_EndDate";
            public static string IsNullBeginEndDate = @"IsNull_BeginEndDate";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}
